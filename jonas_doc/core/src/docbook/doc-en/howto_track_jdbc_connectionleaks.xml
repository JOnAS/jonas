<?xml version="1.0" encoding="UTF-8"?>
<chapter version="5.0" xml:id="Tracking-JDBC-leaks" xml:lang="en"
         xmlns="http://docbook.org/ns/docbook"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:xi="http://www.w3.org/2001/XInclude"
         xmlns:svg="http://www.w3.org/2000/svg"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         xmlns:html="http://www.w3.org/1999/xhtml"
         xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <title>Tracking JDBC Connection Leaks in Java EE Applications</title>
  </info>

  <para>When an application is managing itself the access to the database
  through JDBC Datasources, the release of JDBC resources needs to be done. In
  some cases, the close of the connection is not done. As the JOnAS
  application server is using a pool to manage these accesses to the database,
  it means that the pool may reach its maximum size as the connections are not
  closed (and then put back into the pool). And if the pool reach its maximum
  size, new requests will go in the wait state or wll be aborted. Thus, this
  kind of problems is a huge problem in a production system.</para>

  <para>Fortunately, there are some features provided by JOnAS to handle this
  case.</para>

  <itemizedlist>
    <listitem>
      <para>For example there are JDBC Pooling mechanisms that will kill
      connections if they've not be used since a long time (that can be
      configured) so the pool can lowered its size.</para>
    </listitem>

    <listitem>
      <para>There is also a new feature that will close automatically
      connections if they are not closed after their access. This feature is
      provided through the JDBC JNDI Interceptor which may be configured with
      <filename>JONAS_BASE/conf/jndi-interceptors.xml</filename> file. By
      default, all connections that are not closed will be closed
      automatically. This can be changed with the
      <emphasis>forceClose</emphasis> option. Also, by default this mechanism
      is applied on all datasources. This can be changed by updating the
      <emphasis>jndiRegexp</emphasis> element.</para>

      <para>Here is an example disabling the automatic close of the connection
      leaks and enabling the check on all the JDBC datasources by using
      <option>.*</option> pattern.</para>

      <programlisting>&lt;jndi-interceptors xmlns="http://org.ow2.jonas.jndi.interceptors.impl.mapping"&gt;

    &lt;!-- Define interceptors --&gt;
    &lt;interceptors&gt;
    
        &lt;!-- Detect leaks of JDBC connections --&gt;
        &lt;ds-leak-detector resourceCheckerManager="#RCManager" forceClose="true" jndiRegExp=".*" /&gt;
       

    &lt;/interceptors&gt;
&lt;/jndi-interceptors&gt;</programlisting>
    </listitem>

    <listitem>
      <para>JOnAS is providing informations that allows to track the root of
      the problem in the source code of the application. For example, when
      there is a connection leak, JOnAS is able to print in the log or to show
      in the JOnAS Admin console (With the JDBC Connection Leaks module) the
      line of code where the connection was opened. Thus, a quick review of
      the code needs to be done in order to know why the close() instruction
      has not be done.</para>
    </listitem>
  </itemizedlist>
</chapter>
