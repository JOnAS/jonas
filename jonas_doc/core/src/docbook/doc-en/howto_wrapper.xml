<?xml version="1.0" encoding="UTF-8"?>
<chapter version="5.0" xml:id="jonas-as-os-service"
         xmlns="http://docbook.org/ns/docbook"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:xi="http://www.w3.org/2001/XInclude"
         xmlns:svg="http://www.w3.org/2000/svg"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         xmlns:html="http://www.w3.org/1999/xhtml"
         xmlns:db="http://docbook.org/ns/docbook">

  <title>Use JOnAS as an OS service</title>

  <section xml:id="wrapper-introduction">
    <title>Introduction</title>

    <para>
      This chapter is intended for users who want to use JOnAS as an operating system service. It's dedicated to
      <link xlink:href="http://yajsw.sourceforge.net/"> YAJSW</link> integration.
    </para>
  </section>

  <section xml:id="wrapper-installation">
    <title>Installation</title>

    <para>
      Firstly download the JOnAS wrapper zip archive <link xlink:href="http://my.repo/jonas-wrapper-core-1.0.zip">here</link>.
      Unzip it in the location where you want to install the wrapper root configuration. The default installation directory
      is in <envar>JONAS_ROOT</envar> directory. Then you need to build the wrapper configuration dedicated to your
      <envar>JONAS_BASE</envar> instance.

      <section xml:id="wrapper-base-installation-with-deployme">
        <title>Using DeployME tools</title>

        <para>
          Please, see DeployME documentation.
        </para>
      </section>

      <section xml:id="wrapper-base-installation-with-ant-tasks">
        <title>Using Wrapper ANT task</title>
        <para>
          The JOnAS wrapper archive provides ANT task in order to build the specific wrapper configuration dedicated to
          your <envar>JONAS_BASE</envar> instance.

          The ANT script is located in ${JONAS_WRAPPER_ROOT}/templates/ant/ directory.

          <example>
            <title>Example of a wrapper ANT task.</title>
            <screen>
              &lt;project name="JOnAS_BASE tools"
                          default="apply_configuration"
                          basedir=""
                          xmlns:jant="http://jonas.objectweb.org/ant">

                &lt;target name="init">

                  &lt;property environment="myenv" />
                  &lt;property name="jonas.root" value="${myenv.JONAS_ROOT}" />
                  &lt;property name="jonas.base" value="${myenv.JONAS_BASE}" />

                  &lt;taskdef name="wrapper"
                           classname="org.ow2.jonas.antmodular.jonasbase.wrapper.Wrapper"
                           classpath="../../lib/ant/ow2_jonas_wrapper_ant.jar"/>
                &lt;/target>

                &lt;target name="apply_configuration" depends="init">
                  &lt;wrapper <co xml:id="wrapper.service.name"/> serviceName="jonas"
                              <co xml:id="wrapper.service.description"/> serviceDescription="JOnAS OS service"
                              <co xml:id="wrapper.verbosity"/> verbosity="false"
                              <co xml:id="wrapper.logfile"/> logfile="path/to/logfile"
                              <co xml:id="wrapper.logfile.rollmode"/> logfileRollmode="JVM"
                              <co xml:id="wrapper.home"/> wrapperHome="path/to/wrapperHome"
                              <co xml:id="wrapper.java.home"/> javaHome="path/to/javaHome" />
                &lt;/target>
              &lt;/project>
            </screen>
          </example>

          <calloutlist>
            <callout arearefs="wrapper.service.name">
              <para><literal>serviceName</literal> property - optional. Specifies the name of the OS service.</para>
            </callout>
            <callout arearefs="wrapper.service.description">
              <para><literal>serviceDescription</literal> property - optional. Specifies the description of the OS service.</para>
            </callout>
            <callout arearefs="wrapper.verbosity">
              <para><literal>verbosity</literal> property - optional. Specifies the verbosity mode. Default value is false.</para>
            </callout>
            <callout arearefs="wrapper.logfile">
              <para><literal>logfile</literal> property - optional. Specifies the wrapper log file. Default value is set
              to '${JONAS_BASE}/logs/wrapper.log'</para>
            </callout>
            <callout arearefs="wrapper.logfile.rollmode">
              <para><literal>logfileRollmode</literal> property - optional. Specifies the log file rollmode. Possible values
              are 'JVM' (only one logfile per service) or 'DATE' (a log file per day). Default value is 'DATE'.</para>
            </callout>
            <callout arearefs="wrapper.home">
              <para><literal>wrapperHome</literal> property - optional. Specifies the home of the wrapper root. Default
              value is '${JONAS_ROOT}/wrapper'.</para>
            </callout>
            <callout arearefs="wrapper.java.home">
              <para><literal>javaHome</literal> property - optional. Specifies the <envar>JAVA_HOME</envar>. If not set, the wrapper'll
              use your <envar>JAVA_HOME</envar> system environment variable.</para>
            </callout>
          </calloutlist>

          <para>Don't forget to set your <envar>JONAS_ROOT</envar> and <envar>JONAS_BASE</envar> environment variable.</para>
          <para>To create a wrapper configuration for your <envar>JONAS_BASE</envar> instance:</para>
          <screen>
            <prompt>bash$</prompt><userinput> ant -file $JONAS_WRAPPER_ROOT/templates/ant/build-wrapper.xml</userinput>
          </screen>
        </para>
      </section>

      <section xml:id="wrapper-base-manual-installation">
        <title>Manual installation</title>

        <para>
          <itemizedlist>
            <listitem>
              Create a 'wrapper' directory in the JONAS_BASE directory.
            </listitem>
            <listitem>
              Copy wrapper templates conf and binaries files (locate in ${JONAS_WRAPPER_ROOT}/templates/conf/conf/ and
              ${JONAS_WRAPPER_ROOT}/templates/bat/bat) to your wrapper base directory (${JONAS_BASE}/wrapper/').
            </listitem>
            <listitem>
              Set your own configuration (see Advanced configuration section)
            </listitem>
          </itemizedlist>
        </para>
      </section>
    </para>
  </section>

  <section xml:id="wrapper-usage">
    <title>Usage</title>

    <para>
      The JOnAS wrapper provides a jonas command in order to manage JOnAS as an Operating Service. The jonas command  -
      called jonas - can be found in 'JONAS_BASE/wrapper/bat/' directory.

      If the wrapper root installation directory is different than '<envar>JONAS_ROOT</envar>/wrapper' directory, please set the
      <envar>JONAS_WRAPPER_ROOT</envar> environment variable.

      <itemizedlist>
        <listitem>
          <para>Unix platforms</para>

          <para>Open a new terminal and proceed as follows:</para>

          <screen>
            <prompt>bash&gt;</prompt> <userinput>export JONAS_WRAPPER_ROOT=&lt;<filename class="directory">your_install_dir</filename>&gt;
            </userinput>
          </screen>
        </listitem>
        <listitem>
          <para>Windows platforms</para>

          <para>Open a new DOS window and proceed as follows</para>

          <screen><prompt>C:&gt;</prompt> <userinput>set JONAS_WRAPPER_ROOT=&lt;<filename
            class="directory">your_install_dir</filename>&gt;</userinput>
          </screen>
        </listitem>
      </itemizedlist>

      <section xml:id="wrapper-usage-console">
        <title>jonas console</title>
        <para>
          Test the startup of the wrapped server in a console.
        </para>
      </section>

      <section xml:id="wrapper-usage-install">
        <title>jonas install</title>
        <para>
          Install JOnAS as an operating system service. You need root privileges to install JOnAS as an operating system
          service.
        </para>
      </section>

      <section xml:id="wrapper-usage-start">
        <title>jonas start</title>
        <para>
          Start JOnAS OS service. You need root privileges to start JOnAS OS service.
        </para>
      </section>

      <section xml:id="wrapper-usage-stop">
        <title>jonas stop</title>
        <para>
          Stop JOnAS OS service. You need root privileges to stop JOnAS OS service.
        </para>
      </section>

      <section xml:id="wrapper-usage-state">
        <title>jonas query</title>
        <para>
          Check the state of the JOnAS OS service (installed, running, ...).
        </para>
      </section>

      <section xml:id="wrapper-usage-uninstall">
        <title>jonas uninstall</title>
        <para>
          Uninstall JOnAS OS service. You need root privileges to install JOnAS as an operating system
          service.
        </para>
      </section>
    </para>
  </section>

  <section xml:id="wrapper-advanced-configuration">
    <title>Advanced configuration</title>

    <para>
      For advanced users, you can set you own configuration by editing ${JONAS_WRAPPER_BASE}/conf/jonas_wrapper_base.conf
      configuration file. For more information, please see <link xlink:href="http://yajsw.sourceforge.net/"> YAJSW
      documentation</link>.
    </para>
  </section>
</chapter>
