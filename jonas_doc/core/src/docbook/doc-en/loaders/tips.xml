<?xml version="1.0" encoding="UTF-8"?>
<chapter version="5.0" xml:id="tips" xmlns="http://docbook.org/ns/docbook"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:xi="http://www.w3.org/2001/XInclude"
         xmlns:svg="http://www.w3.org/2000/svg"
         xmlns:m="http://www.w3.org/1998/Math/MathML"
         xmlns:html="http://www.w3.org/1999/xhtml"
         xmlns:db="http://docbook.org/ns/docbook">
  <title>Tips</title>

  <section xml:id="abstract-factory-pattern">
    <title>Abstract Factory Pattern</title>

    <section xml:id="abstract-factory-pattern-error">
      <title>Error pattern</title>

      <para>In an eye blink, suspect all static
      <function>Factory.newInstance()</function> methods.</para>

      <para>The AbstractFactory pattern is a well known and quite used
      pattern. There is an abstract Factory class with an implemented static
      method (<methodname>newInstance()</methodname>). This method is in
      charge of finding a suitable implementation that will be returned to the
      consumer. JAXP API are using this pattern.</para>

      <para>Usually this method performs the following operations:</para>

      <orderedlist>
        <listitem>
          <para>Try to find the name of a class (the concrete Factory) to
          load.</para>

          <para>Usually, the search is done in some dedicated property files,
          system properties, default hardcoded value, ...</para>
        </listitem>

        <listitem>
          <para>Try to load the discovered class using a guessed
          loader.</para>

          <para>Used loaders depends on the code but usually involves (ordre
          is not significant here):</para>

          <itemizedlist>
            <listitem>
              <para>Thread Context ClassLoader</para>
            </listitem>

            <listitem>
              <para>ClassLoader of the Factory</para>
            </listitem>

            <listitem>
              <para>System ClassLoader</para>
            </listitem>

            <listitem>
              <para>A given ClassLoader (could be passed as parameter with
              some luck)</para>
            </listitem>
          </itemizedlist>
        </listitem>

        <listitem>
          <para>Creates an instance of the loaded Class (if one could be
          loaded)</para>
        </listitem>
      </orderedlist>

      <para><important>
          <para>This kind of code make the assumption that they can load any
          class.</para>

          <para>This is not true (even completely wrong) in a modular
          world</para>
        </important></para>
    </section>

    <section xml:id="abstract-factory-pattern-solution">
      <title>Solutions</title>

      <para>The environment has to be adapted to what is expected by the
      code:</para>

      <itemizedlist>
        <listitem>
          <para>Give an appropriate ClassLoader (if possible)</para>
        </listitem>

        <listitem>
          <para>Set a Thread Context ClassLoader to a ClassLoader that will be
          able to load the Class.<note>
              <para>Do not forget to reset to old ClassLoader after the call
              (<literal>try</literal>/<literal>finally</literal>) !</para>
            </note></para>

          <programlisting>ClassLoader expected = ...
ClassLoader old = Thread.currentThread().getContextClassLoader();
Thread.currentThread.setContextClassLoader(expected);
try {
  // Do whatever you want in this block
  Factory factory = Factory.newInstance();
} finally {
  // Reset the TCCL
  Thread.currentThread.setContextClassLoader(old);
}</programlisting>
        </listitem>
      </itemizedlist>
    </section>
  </section>

  <section xml:id="class-cast-exception">
    <title>ClassCastException</title>

    <section xml:id="class-cast-exception-error">
      <title>Error pattern</title>

      <para>Conflicting libraries are found in JOnAS and in an
      application.</para>

      <para>Delegation mechanism, and the logic used to find an
      implementation, produces an instance from a <emphasis>Class incompatible
      with expected type</emphasis>.</para>

      <para>For example, the loaded type comes from the System ClassLoader but
      the expected type comes from the webapp ClassLoader.</para>
    </section>

    <section xml:id="class-cast-exception-solution">
      <title>Solutions</title>

      <para>Use the stacktrace to extract faulty classnames: that's the main
      suspects.</para>

      <para>Use the console to find from where theses classes/packages are
      loaded.</para>

      <para>Display Right/Left ClassLoader of the assignation to discover the
      ClassLoader sources. -</para>

      <itemizedlist>
        <listitem>
          <para>Interesting values: Expected type/loader, Returned
          type/loader, TCCL</para>
        </listitem>

        <listitem>
          <para>Similar to the info the console provides</para>
        </listitem>
      </itemizedlist>

      <para>Filter the package(s) to force resolution in your application
      codebase.</para>

      <para>Usually go back to first bullet until no more Exceptions .</para>
    </section>
  </section>

  <section xml:id="code-rules">
    <title>Code rules !</title>

    <para>To understand what happen, knowing the ClassLoader hierarchies helps
    a lot but is not always sufficent.</para>

    <para>Reading the source code gives the final clues explaining the
    observed behavior.</para>

    <para><note>
        <para>People do a lot of things in Java with class loading (some even
        weird)</para>
      </note></para>
  </section>

  <section xml:id="boot-delegation-tips">
    <title>Boot Delegation</title>

    <para>Format of boot delegation pattern is somehow sensible. Here are
    examples and explanations.</para>

    <itemizedlist>
      <listitem>
        <para>No wildcard (ex: <package>com.sun.xml</package>): Only matches
        classes directly in the given package</para>
      </listitem>

      <listitem>
        <para>Dot and wildcard (ex: <package>com.sun.xml.*</package>): Only
        matches classes in sub packages (direct content does not match)</para>
      </listitem>

      <listitem>
        <para>Only wildcard (ex: <package>com.sun.xml*</package>): Matches
        both direct package and sub packages. But also matches for packages
        sharing the same chars at the beginning of their names.</para>
      </listitem>
    </itemizedlist>

    <table>
      <title>Boot Delegation Patterns Examples</title>

      <tgroup cols="4">
        <thead>
          <row>
            <entry>Pattern / Match ?</entry>

            <entry>com.sun.xml.Parser ?</entry>

            <entry>com.sun.xml.mine.MineParser ?</entry>

            <entry>com.sun.xmlaa.AaParser ?</entry>
          </row>
        </thead>

        <tbody>
          <row>
            <entry>com.sun.xml</entry>

            <entry>YES</entry>

            <entry>NO</entry>

            <entry>NO</entry>
          </row>

          <row>
            <entry>com.sun.xml.*</entry>

            <entry>NO</entry>

            <entry>YES</entry>

            <entry>NO</entry>
          </row>

          <row>
            <entry>com.sun.xml*</entry>

            <entry>YES</entry>

            <entry>YES</entry>

            <entry>YES</entry>
          </row>
        </tbody>
      </tgroup>
    </table>
  </section>
</chapter>
