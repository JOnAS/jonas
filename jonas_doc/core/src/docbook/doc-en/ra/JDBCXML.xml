<?xml version="1.0" encoding="UTF-8"?>
<section version="5.0" xml:id="config.jdbcxml" xml:lang="en"
  xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:svg="http://www.w3.org/2000/svg"
  xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:html="http://www.w3.org/1999/xhtml"
  xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <title>Configuring JDBC XML datasources</title>
  </info>

  <para>This section describes how to configure quickly JDBC datasources
    in a XML file.</para>

  <para>
    The database connections are obtained from a JDBC RA which is
    generated automatically from a
    XML definition. The temporary RAR file is generated under the
    <filename class="directory">work</filename>
    directory.
    Only the
    <emphasis role="bold">Driver</emphasis>
    interface is supported with the JDBC xml deployable. For more advanced uses,
    an explicit <link
    xlink:href="configuration_guide.html#config.ra">JDBC RA</link>
    must be configured.
  </para>

  <para>
    The datasources are deployed by dropping the XML file into the
    <filename class="directory">deploy</filename>
    directory and are undeployed
    by removing it.
  </para>

  <para>
    The root element of the XML file is named
    <emphasis role="bold">datasources</emphasis>
    . It contains a list of
    <emphasis role="bold">datasource</emphasis>
    element, structured in two elements
    <emphasis role="bold">datasource-configuration</emphasis>
    and
    <emphasis role="bold">connectionManager-configuration</emphasis>
    . The namespace has to be set to
    <emphasis role="bold">xmlns="http://jonas.ow2.org/ns/datasource/1.1"
    </emphasis>
    . An example is given below with
    a datasource configuration for connecting the embedded HSQL database.
  </para>

  <screen>
    &lt;datasources xmlns="http://jonas.ow2.org/ns/datasource/1.1"&gt;
      &lt;datasource&gt;
        &lt;datasource-configuration&gt;
          &lt;name&gt;jdbc_1&lt;/name&gt;
          &lt;url&gt;jdbc:hsqldb:hsql://localhost:9001/db_jonas&lt;/url&gt;
          &lt;classname&gt;org.hsqldb.jdbcDriver&lt;/classname&gt;
          &lt;username&gt;jonas&lt;/username&gt;
          &lt;password&gt;jonas&lt;/password&gt;
          &lt;mapper&gt;rdb.hsql&lt;/mapper&gt;
        &lt;/datasource-configuration&gt;
        &lt;connectionManager-configuration&gt;
          &lt;connchecklevel&gt;2&lt;/connchecklevel&gt;
          &lt;connteststmt&gt;select count(1) from information_schema.system_tables&lt;/connteststmt&gt;
          &lt;connmaxage&gt;1448&lt;/connmaxage&gt;
          &lt;maxopentime&gt;60&lt;/maxopentime&gt;
          &lt;initconpool&gt;1&lt;/initconpool&gt;
          &lt;minconpool&gt;5&lt;/minconpool&gt;
          &lt;maxconpool&gt;10&lt;/maxconpool&gt;
          &lt;pstmtmax&gt;100&lt;/pstmtmax&gt;
          &lt;pstmtcachepolicy&gt;Map&lt;/pstmtcachepolicy&gt;
          &lt;maxwaittime&gt;30&lt;/maxwaittime&gt;
          &lt;maxwaiters&gt;100&lt;/maxwaiters&gt;
          &lt;samplingperiod&gt;20&lt;/samplingperiod&gt;
        &lt;/connectionManager-configuration&gt;
      &lt;/datasource&gt;
    &lt;/datasources&gt;
  </screen>

  <itemizedlist>
    <listitem>
      <para>
        The <emphasis>datasource-configuration</emphasis> element
        defines the required parameters to connect the database:
      </para>

      <informaltable>
        <tgroup cols="3">
          <tbody>
            <row>
              <entry align="center">property name</entry>

              <entry align="center">description</entry>

              <entry align="center">possible values</entry>
            </row>

            <row>
              <entry>name</entry>

              <entry>JNDI name the datasource will be registered as. This
                property is
                required.</entry>

              <entry>
                <itemizedlist>
                  <listitem>
                    <para>Anyname (for example jdbc_1)</para>
                  </listitem>
                </itemizedlist>
              </entry>
            </row>

            <row>
              <entry>url</entry>

              <entry>Database url of the form
                jdbc:&lt;database_vendor_subprotocol&gt;.</entry>

              <entry>
                <itemizedlist>
                  <listitem>
                    <para>Any url valid for a database provider
                      (example:jdbc:postgresql://localhost:5432/mydb)</para>
                  </listitem>
                </itemizedlist>
              </entry>
            </row>

            <row>
              <entry>classname</entry>

              <entry>
                Name of the class implementing
                <classname>java.sql.Driver</classname>
                interface in the JDBC driver.
              </entry>

              <entry>
                <itemizedlist>
                  <listitem>
                    <para>any classname representing a JDBC driver
                      (example:org.postgresql.Driver)</para>
                  </listitem>
                </itemizedlist>
              </entry>
            </row>

            <row>
              <entry>username</entry>

              <entry>Database user name</entry>

              <entry>
                <itemizedlist>
                  <listitem>
                    <para>any name</para>
                  </listitem>
                </itemizedlist>
              </entry>
            </row>

            <row>
              <entry>password</entry>

              <entry>Database password</entry>

              <entry>
                <itemizedlist>
                  <listitem>
                    <para>any string</para>
                  </listitem>
                </itemizedlist>
              </entry>
            </row>

            <row>
              <entry>mapperName</entry>

              <entry>Name of the JORM mapper</entry>

              <entry>
                The possible values can be found in the
                <link xlink:href="http://jorm.objectweb.org/doc/mappers.html">List
                  of available mappers in JORM documentation</link>
                .
              </entry>
            </row>

          </tbody>
        </tgroup>
      </informaltable>
    </listitem>

    <listitem>
      <para>
        The <emphasis>connectionManager-configuration</emphasis> element
        defines the
        required parameters to manage the connection (pool configuration, JDBC
        connection checker, ...):
      </para>

      <informaltable>
        <tgroup cols="3">
          <tbody>
            <row>
              <entry>connchecklevel</entry>

              <entry>Level of checking that will be done for the jdbc
                connection.</entry>

              <entry>
                <itemizedlist>
                  <listitem>
                    <para>0 : no check (default value)</para>
                  </listitem>

                  <listitem>
                    <para>1: check connection still open</para>
                  </listitem>

                  <listitem>
                    <para>2 : send the test statement before reusing a
                      connection from the pool</para>
                  </listitem>

                  <listitem>
                    <para>3: (keep-alive feature) send the test statement
                      on each connection every pool-sampling-period</para>
                  </listitem>
                </itemizedlist>
              </entry>
            </row>

            <row>
              <entry>connteststmt</entry>

              <entry>Test SQL statement sent on the connection if the
                jdbc-check-level is greater than 1.</entry>

              <entry>
                <itemizedlist>
                  <listitem>
                    <para>A SQL statement</para>
                  </listitem>
                </itemizedlist>
              </entry>
            </row>

            <row>
              <entry>initconpool</entry>

              <entry>Initial size of the managed connection pool</entry>

              <entry>
                <itemizedlist>
                  <listitem>
                    <para>0 (default value)</para>
                  </listitem>

                  <listitem>
                    <para>n</para>
                  </listitem>
                </itemizedlist>
              </entry>
            </row>

            <row>
              <entry>minconpool</entry>

              <entry>Minimum size of the managed connection pool.</entry>

              <entry>
                <itemizedlist>
                  <listitem>
                    <para>0 (default value)</para>
                  </listitem>

                  <listitem>
                    <para>n</para>
                  </listitem>
                </itemizedlist>
              </entry>
            </row>

            <row>
              <entry>maxconpool</entry>

              <entry>Maximum size of the managed connection pool.</entry>

              <entry>
                <itemizedlist>
                  <listitem>
                    <para>n</para>
                  </listitem>

                  <listitem>
                    <para>-1 = unlimited (default value)</para>
                  </listitem>
                </itemizedlist>
              </entry>
            </row>

            <row>
              <entry>connmaxage</entry>

              <entry>Maximum number of minutes to keep the managed
                connection in the pool.</entry>

              <entry>
                <itemizedlist>
                  <listitem>
                    <para>0 = an unlimited amount of time.</para>
                  </listitem>

                  <listitem>
                    <para>n in minutes</para>
                  </listitem>
                </itemizedlist>
              </entry>
            </row>

            <row>
              <entry>pstmtmax</entry>

              <entry>Maximum number of PreparedStatements per managed
                connection in the pool. Value of 0 is
                unlimited and -1 disables the cache.</entry>

              <entry>
                <itemizedlist>
                  <listitem>
                    <para>0 = unlimited</para>
                  </listitem>

                  <listitem>
                    <para>n (default value = 10)</para>
                  </listitem>

                  <listitem>
                    <para>-1 = cache disabled</para>
                  </listitem>
                </itemizedlist>
              </entry>
            </row>

            <row>
              <entry>pstmtcachepolicy</entry>

              <entry>Prepared statement cache policy to use.</entry>

              <entry>
                <itemizedlist>
                  <listitem>
                    <para>List (default value) = Array based implementation of the cache</para>
                  </listitem>

                  <listitem>
                    <para>Map = Hashmap based implementation of the cache</para>
                  </listitem>

                </itemizedlist>
              </entry>
            </row>

            <row>
              <entry>maxopentime</entry>

              <entry>Identifies the maximum number of minutes that a
                managed connection can be left busy.</entry>

              <entry>
                <itemizedlist>
                  <listitem>
                    <para>0 = an unlimited amount of time (default
                      value).</para>
                  </listitem>

                  <listitem>
                    <para>n in minutes</para>
                  </listitem>
                </itemizedlist>
              </entry>
            </row>

            <row>
              <entry>maxwaiters</entry>

              <entry>identifies the maximum number of waiters for a
                managed connection. Default value is 0.</entry>

              <entry>
                <itemizedlist>
                  <listitem>
                    <para>0 (default value)</para>
                  </listitem>

                  <listitem>
                    <para>n</para>
                  </listitem>
                </itemizedlist>
              </entry>
            </row>

            <row>
              <entry>maxwaittime</entry>

              <entry>identifies the maximum number of seconds that a
                waiter will wait for a managed connection. Default value is
                0.</entry>

              <entry>
                <itemizedlist>
                  <listitem>
                    <para>0 (default value)</para>
                  </listitem>

                  <listitem>
                    <para>n in seconds</para>
                  </listitem>
                </itemizedlist>
              </entry>
            </row>

            <row>
              <entry>samplingperiod</entry>

              <entry>identifies the number of seconds that will occur
                between statistics samplings of the pool. Default is 30
                seconds.</entry>

              <entry>
                <itemizedlist>
                  <listitem>
                    <para>n in seconds (default value = 30s)</para>
                  </listitem>
                </itemizedlist>
              </entry>
            </row>

          </tbody>
        </tgroup>
      </informaltable>
    </listitem>
  </itemizedlist>
</section>
