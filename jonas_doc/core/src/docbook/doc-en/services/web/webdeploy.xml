<?xml version="1.0" encoding="UTF-8"?>
<section version="5.0" xml:id="web.deployment" xml:lang="en"
         xmlns="http://docbook.org/ns/docbook"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:xi="http://www.w3.org/2001/XInclude"
         xmlns:svg="http://www.w3.org/2000/svg"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         xmlns:html="http://www.w3.org/1999/xhtml"
         xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <title>Defining the Web Deployment Descriptor</title>
  </info>

  <section xml:id="webdeploy.principles">
    <info>
      <title>Principles</title>
    </info>

    <para>The Web component programmer is responsible for providing the
    deployment descriptor associated with the developed web components. The
    Web component provider's responsibilities and the application assembler's
    responsibilities are to provide an XML deployment descriptor that conforms
    to the deployment descriptor's XML schema as defined in the
    <trademark>Java</trademark> Servlet Specification Version 2.4. (Refer to
    <code> $JONAS_ROOT/xml/web-app_2_4.xsd </code> or <link
    xlink:href="http://java.sun.com/xml/ns/j2ee/web-app_2_4.xsd">
    http://java.sun.com/xml/ns/j2ee/web-app_2_4.xsd </link> ).</para>

    <para>To customize the Web components, information not defined in the
    standard XML deployment descriptor may be needed. For example, the
    information may include the mapping of the name of referenced resources to
    its JNDI name. This information can be specified during the deployment
    phase, within another XML deployment descriptor that is specific to JOnAS.
    The JOnAS-specific deployment descriptor's XML schema is located in <code>
    $JONAS_ROOT/xml/jonas-web-app_X_Y.xsd </code> . The file name of the
    JOnAS-specific XML deployment descriptor must be the file name of the
    standard XML deployment descriptor prefixed by ' <literal> jonas-
    </literal> '.</para>

    <para>The parser gets the specified schema via the classpath (schemas are
    packaged in the $JONAS_ROOT/lib/common/ow_jonas.jar file).</para>

    <para>The standard deployment descriptor (web.xml) should contain
    structural information that includes the following:</para>

    <itemizedlist>
      <listitem>
        <para>The Servlet's description (including Servlet's name, Servlet's
        class or jsp-file, Servlet's initialization parameters),</para>
      </listitem>

      <listitem>
        <para>Environment entries,</para>
      </listitem>

      <listitem>
        <para>EJB references,</para>
      </listitem>

      <listitem>
        <para>EJB local references,</para>
      </listitem>

      <listitem>
        <para>Resource references,</para>
      </listitem>

      <listitem>
        <para>Resource env references.</para>
      </listitem>
    </itemizedlist>

    <para>The JOnAS-specific deployment descriptor (jonas-web.xml) may contain
    information that includes:</para>

    <itemizedlist>
      <listitem>
        <para>The JNDI name of the external resources referenced by a Web
        component,</para>
      </listitem>

      <listitem>
        <para>The JNDI name of the external resources environment referenced
        by a Web component,</para>
      </listitem>

      <listitem>
        <para>The JNDI name of the referenced bean's by a Web
        component,</para>
      </listitem>

      <listitem>
        <para>The name of the virtual host on which to deploy the
        servlets,</para>
      </listitem>

      <listitem>
        <para>The name of the context root on which to deploy the
        servlets,</para>
      </listitem>

      <listitem>
        <para>A mapping between some security role name and security
        principals names, that overrides the <link
        xlink:href="configuration_guide.html#security.config">server
        configuration</link>,</para>
      </listitem>

      <listitem>
        <para>The compliance of the web application classloader to the java 2
        delegation model or not.</para>
      </listitem>
    </itemizedlist>

    <para>&lt;host&gt; element: If the configuration file of the web container
    contains virtual hosts, the host on which the WAR file is deployed can be
    set.</para>

    <para>&lt;context-root&gt; element: The name of the context on which the
    application will be deployed should be specified. If it is not specified,
    the context-root used can be one of the following:</para>

    <itemizedlist>
      <listitem>
        <para>If the war is packaged into an EAR file, the context-root used
        is the context specified in the application.xml file.</para>
      </listitem>

      <listitem>
        <para>If the war is standalone, the context-root is the name of the
        war file (i.e, the context-root is jonasAdmin for
        jonasAdmin.war).</para>
      </listitem>
    </itemizedlist>

    <para>If the context-root is / or empty, the web application is deployed
    as ROOT context (i.e., http://localhost:9000/).</para>

    <para>&lt;java2-delegation-model&gt; element: Set the compliance to the
    java 2 delegation model.</para>

    <itemizedlist>
      <listitem>
        <para>If true: the web application context uses a classloader, using
        the Java 2 delegation model (ask parent classloader first).</para>
      </listitem>

      <listitem>
        <para>If false: the class loader searches inside the web application
        first, before asking parent class loaders.</para>
      </listitem>
    </itemizedlist>

    <para/>
  </section>

  <section xml:id="webdeploy.examples">
    <info>
      <title>Examples of Web Deployment Descriptors</title>
    </info>

    <itemizedlist>
      <listitem>
        <para>Example of a standard Web Deployment Descriptor (web.xml):
        <screen>
&lt;?xml version="1.0" encoding="ISO-8859-1"?&gt;

&lt;web-app xmlns="http://java.sun.com/xml/ns/j2ee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://java.sun.com/xml/ns/j2ee http://java.sun.com/xml/ns/j2ee/web-app_2_4.xsd"
         version="2.4"&gt;

  &lt;servlet&gt;
    &lt;servlet-name&gt;Op&lt;/servlet-name&gt;
      &lt;servlet-class&gt;org.objectweb.earsample.servlets.ServletOp&lt;/servlet-class&gt;
  &lt;/servlet&gt;

  &lt;servlet-mapping&gt;
    &lt;servlet-name&gt;Op&lt;/servlet-name&gt;
    &lt;url-pattern&gt;/secured/Op&lt;/url-pattern&gt;
  &lt;/servlet-mapping&gt;

  &lt;security-constraint&gt;
    &lt;web-resource-collection&gt;
      &lt;web-resource-name&gt;Protected Area&lt;/web-resource-name&gt;
        &lt;!-- Define the context-relative URL(s) to be protected --&gt;
        &lt;url-pattern&gt;/secured/*&lt;/url-pattern&gt;
        &lt;!-- If you list http methods, only those methods are protected --&gt;
        &lt;http-method&gt;DELETE&lt;/http-method&gt;
        &lt;http-method&gt;GET&lt;/http-method&gt;
        &lt;http-method&gt;POST&lt;/http-method&gt;
        &lt;http-method&gt;PUT&lt;/http-method&gt;
    &lt;/web-resource-collection&gt;
    &lt;auth-constraint&gt;
      &lt;!-- Anyone with one of the listed roles may access this area --&gt;
      &lt;role-name&gt;tomcat&lt;/role-name&gt;
      &lt;role-name&gt;role1&lt;/role-name&gt;
    &lt;/auth-constraint&gt;
  &lt;/security-constraint&gt;

  &lt;!-- Default login configuration uses BASIC authentication --&gt;
  &lt;login-config&gt;
    &lt;auth-method&gt;BASIC&lt;/auth-method&gt;
    &lt;realm-name&gt;Example Basic Authentication Area&lt;/realm-name&gt;
  &lt;/login-config&gt;

  &lt;env-entry&gt;
    &lt;env-entry-name&gt;envEntryString&lt;/env-entry-name&gt;
    &lt;env-entry-value&gt;This is a string from the env-entry&lt;/env-entry-value&gt;
    &lt;env-entry-type&gt;java.lang.String&lt;/env-entry-type&gt;
  &lt;/env-entry&gt;

  &lt;!-- reference on a remote bean without ejb-link--&gt;
  &lt;ejb-ref&gt;
    &lt;ejb-ref-name&gt;ejb/Op&lt;/ejb-ref-name&gt;
    &lt;ejb-ref-type&gt;Session&lt;/ejb-ref-type&gt;
    &lt;home&gt;org.objectweb.earsample.beans.secusb.OpHome&lt;/home&gt;
    &lt;remote&gt;org.objectweb.earsample.beans.secusb.Op&lt;/remote&gt;
  &lt;/ejb-ref&gt;

  &lt;!-- reference on a remote bean using ejb-link--&gt;
  &lt;ejb-ref&gt;
    &lt;ejb-ref-name&gt;ejb/EjbLinkOp&lt;/ejb-ref-name&gt;
    &lt;ejb-ref-type&gt;Session&lt;/ejb-ref-type&gt;
    &lt;home&gt;org.objectweb.earsample.beans.secusb.OpHome&lt;/home&gt;
    &lt;remote&gt;org.objectweb.earsample.beans.secusb.Op&lt;/remote&gt;
    &lt;ejb-link&gt;secusb.jar#Op&lt;/ejb-link&gt;
  &lt;/ejb-ref&gt;

  &lt;!-- reference on a local bean --&gt;
  &lt;ejb-local-ref&gt;
    &lt;ejb-ref-name&gt;ejb/OpLocal&lt;/ejb-ref-name&gt;
    &lt;ejb-ref-type&gt;Session&lt;/ejb-ref-type&gt;
    &lt;local-home&gt;org.objectweb.earsample.beans.secusb.OpLocalHome&lt;/local-home&gt;
    &lt;local&gt;org.objectweb.earsample.beans.secusb.OpLocal&lt;/local&gt;
    &lt;ejb-link&gt;secusb.jar#Op&lt;/ejb-link&gt;
  &lt;/ejb-local-ref&gt;
&lt;/web-app&gt;</screen></para>
      </listitem>

      <listitem>
        <para>Example of a specific Web Deployment Descriptor (jonas-web.xml):
        <screen>
&lt;?xml version="1.0" encoding="ISO-8859-1"?&gt;

&lt;jonas-web-app xmlns="http://www.objectweb.org/jonas/ns"
	       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	       xsi:schemaLocation="http://www.objectweb.org/jonas/ns
	       http://www.objectweb.org/jonas/ns/jonas-web-app_4_0.xsd" &gt;

  &lt;!-- Mapping between the referenced bean and its JNDI name, override the ejb-link if
       there is one in the associated ejb-ref in the standard Web Deployment Descriptor --&gt;
  &lt;jonas-ejb-ref&gt;
    &lt;ejb-ref-name&gt;ejb/Op&lt;/ejb-ref-name&gt;
    &lt;jndi-name&gt;OpHome&lt;/jndi-name&gt;
  &lt;/jonas-ejb-ref&gt;

  &lt;!-- the virtual host on which deploy the web application --&gt;
  &lt;host&gt;localhost&lt;/host&gt;

  &lt;!-- the context root on which deploy the web application --&gt;
  &lt;context-root&gt;web-application&lt;/context-root&gt;
  
  &lt;!-- Defines a mapping between some security roles names and security principals names, that overrides the default server configuration --&gt;
  &lt;jonas-security&gt;
    &lt;security-role-mapping&gt;
      &lt;role-name&gt;Administrator&lt;/role-name&gt;
      &lt;principal-name&gt;admin&lt;/principal-name&gt;
    &lt;/security-role-mapping&gt;
    &lt;security-role-mapping&gt;
      &lt;role-name&gt;Employee&lt;/role-name&gt;
      &lt;principal-name&gt;admin&lt;/principal-name&gt;
      &lt;principal-name&gt;johndoe&lt;/principal-name&gt;
    &lt;/security-role-mapping&gt;
  &lt;/jonas-security&gt;

&lt;/jonas-web-app&gt;</screen></para>
      </listitem>
    </itemizedlist>

    <para>For advices about xml file writing, refer to <xref
    linkend="xmltips"/>.</para>
  </section>
</section>
