<?xml version="1.0" encoding="UTF-8"?>
<book version="5.0" xml:lang="en" xmlns="http://docbook.org/ns/docbook"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:xi="http://www.w3.org/2001/XInclude"
      xmlns:svg="http://www.w3.org/2000/svg"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      xmlns:html="http://www.w3.org/1999/xhtml"
      xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <title>Multitenancy Guide</title>

    <authorgroup>
      <author>
        <orgname>JOnAS Team</orgname>
      </author>
    </authorgroup>

    <legalnotice>
      <para>This work is licensed under the Creative Commons
        Attribution-ShareAlike License. To view a copy of this license,visit
        <link
          xlink:href="http://creativecommons.org/licenses/by-sa/2.0/deed.en">http://creativecommons.org/licenses/by-sa/2.0/deed.en</link>
        or send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford,
        California 94305, USA.</para>
    </legalnotice>

    <copyright>
      <year>2012</year>

      <holder>OW2 Consortium</holder>
    </copyright>

    <date>Jun 2012</date>
  </info>

  <chapter>
    <info>
      <title>Introduction</title>
    </info>

    <section>
      <info>
        <title>Multitenancy</title>
      </info>

      <para>JOnAS hosts and deploys applications written in Java. However, an application can not be natively
        deployed more than once in an instance of JOnAS. If necessary, the application will be deployed on
        another server instance because there is a risk of collision. One solution of this problem is multitenancy.
        This new feature provides the ability to deploy the same application multiple times on a single instance
        of JOnAS without prior configuration. </para>
    </section>

    <section>
      <info>
        <title>Shared application server</title>
      </info>

      <para>
        Tenants will run on the same application server (JOnAS) each with an instance of the application.
        Thus, in one instance of JOnAS, there will be many instances of an application than tenants using it.
        This multitenancy level is not without impact on the JOnAS application server because it will make changes in
        order to deploy the same application multiple times for multiple tenants in ensuring customization
        of resources and security among tenants.
        <mediaobject>
          <imageobject>
            <imagedata align="center"
                       fileref="../../resources/images/sharedASMultitenantLevel.png"
                       scalefit="1" width="10%" />
          </imageobject>
        </mediaobject>
        According to JavaEE7 specifications, for each tenant, an instance of the application is deployed.
      </para>
    </section>
  </chapter>

  <chapter>
    <info>
      <title>Tenant context</title>
    </info>
    <para>
      Each tenant is identified by a tenant identifier following the pattern <literal>T&lt;id></literal> when
      <literal>id</literal> is numeric. This identifier is defined in web descriptor, application descriptor or even
      addon descriptor with the hierarchy : <programlisting>Addon > EAR > WAR</programlisting>
      It was necessary to define a default identifier (<literal>defaultTenantId = T0</literal>) for applications
      that do not give a specific tenant-id. it is simply used to enforce the policy where each instance of an
      application is linked to an identifier of tenant, and will not be used for customizing data (no changes will
      be made).
    </para>

    <para>
      The identifier of the tenant must be present when deploying the application but also during its execution. In fact,
      during deployment, several services operate to save the settings and application data in the JOnAS environment.
      These services have a dependence on the multitenant service and will use it for customizing data. Therefore, it
      is necessary that the information "tenant-id" is constantly present throughout the duration of the deployment.
    </para>

    <para>
      The tenant context is composed of :
      <itemizedlist>
        <listitem>
          <para>TenantId : tenant identifier</para>
        </listitem>
        <listitem>
          <para>InstanceName</para>
        </listitem>
      </itemizedlist>
      To access the context of the current tenant, use :
      <programlisting language="java">TenantCurrent.getCurrent().getTenantContext();</programlisting>
    </para>

    <para>
      Tenant-id is stored in a variable associated to the ThreadLocal. When running the application, an HTTP filter is
      set up, it sets all contexts associated with the thread, including the context of tenant, before the
      server responds to the client request.
      <mediaobject>
        <imageobject>
          <imagedata align="left"
                     fileref="../../resources/images/tenantContext.png"
                     scalefit="1" width="40%" />
        </imageobject>
      </mediaobject>

<programlisting>
// Save the current context
old = TenantCurrent.getCurrent().getTenantContext();
TenantCurrent.getCurrent().setTenantContext(this.ctx);
</programlisting>
      Next, execute the request. And finally, restore the old context :
<programlisting>
// Restore the old context
TenantCurrent.getCurrent().setTenantContext(old);
</programlisting>


      This filter is created by calling the multitenant service. The valve is set in Tomcat7Service as follows :

<programlisting>
// For the tenantId
Filter tenantIdHttpFilter = null;
String tenantId = null;
if (getMultitenantService() != null){
  // get an instance of the filtre
  tenantId = super.getTenantId(war.getWarDeployable());
  tenantIdHttpFilter = getMultitenantService().getTenantIdFilter(tenantId);
  // needs to add this filter on the context
  jStdCtx.addValve(new FilterValveWrapper(tenantIdHttpFilter));
}
</programlisting>

    </para>



  </chapter>

  <chapter>
    <info>
      <title>Customization</title>
    </info>

    <section>
      <info>
        <title>Context root customization</title>
      </info>

      <para>
        Context root is defined in the web descriptor of the application. This context must be unique for each tenant.
        However, because tenants are instances of the same application, context root is the same for all. During
        deployment, context root of each instance is prefixed by the instance name and the tenant-id.
        <mediaobject>
          <imageobject>
            <imagedata align="left"
                       fileref="../../resources/images/contextRootCustomization.png"
                       scalefit="1" width="40%" />
          </imageobject>
        </mediaobject>

        <para>
          This customization is done during the deployment of the webapp.
        </para>
<programlisting>
protected String updateContextRoot(String contextRoot, IDeployable deployable) {
  String tenantId = getTenantId(deployable);
  String instanceName = multitenantService.getInstanceNameFromContext();

  if (instanceName != null) {
    contextRoot = instanceName + "/" + contextRoot;
  }

  if (tenantId != null) {
    contextRoot = tenantId + "/" + contextRoot;
  }
  return contextRoot;
}
</programlisting>
      </para>
    </section>
    <section>
      <info>
        <title>Data isolation in database</title>
      </info>
      <sect2>
        <info>
          <title>Shared database and shared schema</title>
        </info>
        <para>
          <mediaobject>
            <imageobject>
              <imagedata align="left"
                         fileref="../../resources/images/sharedDatabaseAndSchema.png"
                         scalefit="1" width="20%" />
            </imageobject>
          </mediaobject>
        </para>
      </sect2>

      <sect2>
        <info>
          <title>Propagation of the tenantId to eclipselink</title>
        </info>
        <para>
          To persist the tenantId in database, we have to set the <literal>eclipselink.tenant-id</literal> property in
          persistence.xml file. To automatize the propagation of the tenantId to eclipselink we need to add this
          property automatically when the application is added. Then, we will use the method :

<programlisting>
// This property will propagate the tenantId to eclipselink
// This value will be added to entities tables
String tenantIdProperty = "eclipselink.tenant-id";
String tenantIdValue = tenantId;
persistenceUnitManager.setProperty(tenantIdProperty, tenantIdValue);
</programlisting>
        </para>
      </sect2>

      <sect2>
        <info>
          <title>EJB Entities configured as multitenant</title>
        </info>
        <para>
          Entities must be configured as multitenant to enable adding tenant-id in the database. For that, we have to
          add <literal>@Multitenant</literal> annotation in each class but we need to do that automatically (when
          multitenant service is activated). A solution is to use a Session Customizer
          (cf <link xlink:href="http://wiki.eclipse.org/Customizing_the_EclipseLink_Application_(ELUG)">
          http://wiki.eclipse.org/Customizing_the_EclipseLink_Application_(ELUG)</link>). It is a simple class
          with only one method (customize) and take one parameter (Session session). In this method, we will set all
          entity classes as multitenant as follows :

<programlisting>
public void customize(Session session) throws Exception {
  Map&lt;Class, ClassDescriptor> descs = session.getDescriptors();
  // For each entity class ...
  for(Map.Entry&lt;Class, ClassDescriptor> desc : descs.entrySet()){
    // Create a multitenant policy (Single table)
    SingleTableMultitenantPolicy policy = new SingleTableMultitenantPolicy(desc.getValue());
    // Tell that column descriminator is TENANT_ID (it will be added in the database)
    policy.addTenantDiscriminatorField("eclipselink.tenant-id", new DatabaseField("TENANT_ID"));
    // Add this policy in class derscriptor
    desc.getValue().setMultitenantPolicy(policy);
  }
}
</programlisting>
        </para>

        <para>
          Then, during the deployment of the application, an eclipselink property is set to use this session customizer :
<programlisting>
// This property will configure entities as multitenant
// It is the equivalent of @Multitenant
String sessionCustomizerProperty = "eclipselink.session.customizer";
String sessionCustomizerClass = "org.ow2.easybeans.persistence.eclipselink.MultitenantEntitiesSessionCustomizer";
persistenceUnitManager.setProperty(sessionCustomizerProperty, sessionCustomizerClass);
</programlisting>
        </para>

        <para>
          Because tenants share the same database and the same tables, it is important to ensure that a tenant does not
          drop an create tables. For that, verify if the <literal>drop-and-create-tables</literal> eclipselink property
          is not set. Otherwise, change this property to <literal>create-tables</literal> only :
<programlisting>
// If eclipselink was enabled to drop and create tables
// change this property to only create tables
String createTablesProperty = "eclipselink.ddl-generation";
String dropAndCreateTablesValue = "drop-and-create-tables";
String createTablesValue = "create-tables";
Map&lt;String, String> properties = persistenceUnitManager.getProperty(createTablesProperty);
for (Map.Entry&lt;String, String> property : properties.entrySet()){
  if (property.getValue().equals(dropAndCreateTablesValue)) {
    logger.warn("This tenant was enabled to drop and create tables. Eclipselink property is changed to only create tables");
    persistenceUnitManager.setProperty(createTablesProperty, createTablesValue, property.getKey());
  }
}
</programlisting>
        </para>
      </sect2>
    </section>

    <section>
      <info>
        <title>JNDI names customization</title>
      </info>
      <para>
        When an application is deployed in multitenant mode, we take the risk of having a conflict between bound names
        of each tenant. A solution is to add a prefix before each name. This prefix is the tenantId of the tenant which
        names are related.
      </para>
      <sect2>
        <info>
          <title>Naming strategy</title>
        </info>
        <para>
           During the deployment, a name is prefixed the syntax : <literal>T&lt;id>/name</literal> when
          <literal>T&lt;id></literal> is the application's tenant-id. The naming strategy is set by:
<programlisting>
newNamingStrategies.add(ejb3Service.getNamingStrategy(prefix, oldNamingStrategy));
</programlisting>

          Example: MyInitializerBean will be T1/MyInitializerBean.
          In addition, as for versioning service, a virtual JNDI binding is made. It will remove the prefix and rebind
          the old name to the same object. Then, we will have 2 names (MyInitializeBean and T1/MyInitializerBean)
          linked to the same object.
        </para>

      </sect2>

      <sect2>
        <info>
          <title>JNDI interceptor</title>
        </info>

        <para>
          To customize JNDI names bound by the application by adding the tenant-id as prefix, an interceptor is set.

          <mediaobject>
            <imageobject>
              <imagedata align="left"
                         fileref="../../resources/images/jndiInterceptor.png"
                         scalefit="1" width="40%" />
            </imageobject>
          </mediaobject>

          This interceptor (<literal>org.ow2.jonas.lib.tenant.interceptor.jndi.JNDITenantIdInterceptor</literal>) is an
          implementation of <literal>org.ow2.carol.jndi.intercept.ContextInterceptor</literal> and is registered in
          Carol Interceptor Manager when Multitenant service is activated. Then, all JNDI calls are intercepted.
<programlisting>
// Add tenantId JNDI interceptor
jndiTenantIdInterceptor = new JNDITenantIdInterceptor(JNDI_SEPARATOR);
SingletonInterceptorManager.getInterceptorManager().registerContextInterceptor(jndiTenantIdInterceptor);
</programlisting>

          Two operations are made by this interceptor :
          <itemizedlist>
            <listitem>
              Distinguish calls from multitenant application to JNDI
            </listitem>
            <listitem>
              Prefix these names by adding the tenant-id
            </listitem>
          </itemizedlist>
        </para>
      </sect2>
    </section>
    <section>
      <info>
        <title>MBeans customization</title>
      </info>

      <para>
        When we deploy a same application two times for two different tenants, the problem is that application's MBeans
        will have the same identifier which will create a case of conflict. To avoid it, a solution is to add an attribute
        in the MBean's ObjectName named tenantId :

        <programlisting>Domaine:name=MBeanName;tenantId=T1</programlisting>

        To do that, we need to intercept all MBeanServer methods call since majority of these methods use the ObjectName.
        A solution is to set a proxy of the principal MBeanServer (which is returned by
        <literal>ManagementFactory.getPlatformMBeanServer()</literal>)
      </para>

      <sect2>
        <info>
          <title>JMX interceptor</title>
        </info>
        <para>
          To customize MBeans, a JMX interceptor is set to add a <literal>tenant-id</literal> property to the MBean ObjectName.
          <mediaobject>
            <imageobject>
              <imagedata align="left"
                         fileref="../../resources/images/jmxInterceptor.png"
                         scalefit="1" width="40%" />
            </imageobject>
          </mediaobject>
          This interceptor (<literal>org.ow2.jonas.lib.tenant.interceptor.jmx.JMXTenantIdInterceptor</literal>) implements
          <literal>org.ow2.jonas.jmx.Interceptor</literal> and is added to the InvocationHandler (<literal>
          org.ow2.jonas.jmx.internal.interceptor.InvocationHandlerImpl</literal>) by multitenant service :
<programlisting>
// Add tenantId JMX interceptor
jmxTenantIdInterceptor = new JMXTenantIdInterceptor(tenantIdAttributeName, allowToAccessPlatformMBeans);
jmxService.addInterceptor(jmxTenantIdInterceptor);
</programlisting>
          and will be called before querying the MBeanServer.
        </para>
      </sect2>

      <sect2>
        <info>
          <title>Customized MBeanServerBuilder</title>
        </info>

        <para>
          In order to create a "proxified" MBeanServer, a new class
          <literal>org.ow2.jonas.services.bootstrap.mbeanbuilder.JOnASMBeanServerBuilder</literal> which extends
          <literal>javax.management.MBeanServerBuilder</literal> is used and set as a system property :
<programlisting>
// MBeanServerBuilder
System.setProperty("javax.management.builder.initial", "org.ow2.jonas.services.bootstrap.mbeanbuilder.JOnASMBeanServerBuilder");
</programlisting>
          Then, the first MBeanServer created in the platform is a proxy with a default interceptor. This default interceptor
          is always the last called and will call the MBeanServer method originally invoked (before interception).
<programlisting>
// Create real MBeanServer with outerProxy
MBeanServer origin = super.newMBeanServer(defaultDomain, outerProxy, delegate);

// Create handler for MBeanServer proxy and add
// the default interceptor
InvocationHandlerImpl invocationHandler = new InvocationHandlerImpl();
invocationHandler.addInterceptor(new MBeanServerDelegateInterceptor(origin));

// Create the MBeanServer proxy
MBeanServer proxy = (MBeanServer) Proxy.newProxyInstance(origin.getClass().getClassLoader(),
                                                         new Class&lt;?>[]{MBeanServer.class},
                                                         invocationHandler);
</programlisting>
          It is possible to add as many interceptors that it is desired and they will be called one by one.
          <mediaobject>
            <imageobject>
              <imagedata align="left"
                         fileref="../../resources/images/customizedMBeanServer.png"
                         scalefit="1" width="50%" />
            </imageobject>
          </mediaobject>
          Using a customized MBeanServerBuilder can be problematic. In fact, as described in
          <link xlink:href="http://jira.ow2.org/browse/JONAS-867">JONAS-867</link>, if the system property
          <literal>com.sun.management.jmxremote</literal> is set before JOnAS startup, this has the effect of
          creating some MBeans and then initialize the MBeanServer. However, the system property
          <literal>javax.management.builder.initial</literal> which is set when JOnAS starts and define the class
          which is used to build the platform MBeanServer, this one is present in
          <literal>org.ow2.jonas.services.bootstrap.mbeanbuilder.JOnASMBeanServerBuilder</literal>
          and is not known by the classloader at this step.
          If the system property <literal>com.sun.management.jmxremote</literal> is not set, this error should not appear.
        </para>
      </sect2>
    </section>
    <section>
      <info>
        <title>Tenants administration isolation</title>
      </info>

      <para>
        All administrators are defined in a special realm. Tenants administration isolation is done by defining two profiles :
      </para>

      <sect2>
        <info>
          <title>Tenant administrator profile</title>
        </info>
        <para>
          This profile is define by the <literal>super-admin</literal> role :
          <programlisting>&lt;role name="super-admin" description="Role allowing access to all tenants" /></programlisting>
          and will be assigned to a unique user in the platform :
          <programlisting>&lt;user name="superAdmin" password="admin" roles="superAdmin"/></programlisting>
        </para>
        <para>
          This user will have a full access to all MBeans when connecting on a administration tool such as JConsole.
        </para>
      </sect2>
      <sect2>
        <info>
          <title>
            Super administrator profile
          </title>
        </info>

        <para>
          For each tenant, a specific role is defined according to the pattern <literal>TenantId[T&lt;id>]</literal> :
          <programlisting>&lt;role name="TenantId[T1]" description="Role allowing access to tenant T1 information" /></programlisting>
          and will be assigned to a unique user in the group of user belonging to the tenant.
          <programlisting>&lt;user name="admin@client" password="admin" roles="TenantId[T1]"/></programlisting>
        </para>
        <para>
          This user will have access to MBeans which contain the property <literal>tenant-id=T1</literal>.
        </para>
      </sect2>

      <para>
        <mediaobject>
          <imageobject>
            <imagedata align="left"
                       fileref="../../resources/images/tenantAdminIsolation.png"
                       scalefit="1" width="40%" />
          </imageobject>
        </mediaobject>
      </para>

      <para>
        This isolation is made when JMX Security is activated. For that, set JMX Security to true in jonas.properties :
        <programlisting>jonas.service.jmx.secured    true</programlisting>
        Then, change authentication method and parameter to :
<programlisting>
jonas.service.jmx.authentication.method    jmx.remote.x.login.config
jonas.service.jmx.authentication.parameter    jaas-jmx
</programlisting>
        Next, activate TenantIdLoginModule for jaas-jmx in jaas.config :
<programlisting>
jaas-jmx {
  // Use LoginModule for JMX authentication
  org.ow2.jonas.security.auth.spi.JResourceLoginModule required
  resourceName="memrlm_1"
  ;

  // Use the login module to add tenantId in TenantContext
  org.ow2.jonas.lib.tenant.loginmodule.TenantIdLoginModule required;
};
</programlisting>
      During tenant administrator login, TenantIdLoginModule will propagate tenantContext in order to filter MBeans
      by the <literal>tenant-id</literal> property presents in the ObjectName. Only MBeans which their objectName
      contains the same tenant-id than the connected user will appear.
      </para>
    </section>

    <section>
      <info>
        <title>Logs customization</title>
      </info>

      <para>
        JOnAS use <link xlink:href="http://monolog.ow2.org/">Monolog</link> for logging. Monolog is a very static
        project and it was necessary to make it extensible for logging other information than those predefined (as
        date, classname, etc). One solution is to write an interface in monolog 
        <literal>org.objectweb.util.monolog.api.LogInfo</literal>:
<programlisting>
package org.objectweb.util.monolog.api;

/**
* This interface allows to add an extension to Monolog
* @author Mohammed Boukada
*/
public interface LogInfo {

  /**
  * Gets the info value
  * @return info value
  */
  String getValue ();
}
</programlisting>
        that will be implemented by JOnAS services.
      </para>
      <para>
        In this case, this interface is implemented by multitenant service and provides the tenant-id of the
        current tenant.
        <mediaobject>
          <imageobject>
            <imagedata align="left"
                       fileref="../../resources/images/monologExtension.png"
                       scalefit="1" width="30%" />
          </imageobject>
        </mediaobject>
      </para>
      <para>
        An ipojo component is defined in <literal>modules/libraries/externals/monolog</literal> and is responsible of
        registration of monolog's extensions. When an implementation of this interface is registered in OSGi platform :
<programlisting>
&lt;provides specifications="org.objectweb.util.monolog.api.LogInfo">
  &lt;property field="pattern" name="pattern" type="java.lang.Character"/>
&lt;/provides>
</programlisting>
        This component will add the extension to monolog :
<programlisting>
&lt;component classname="org.ow2.jonas.monolog.MonologExtension"
           immediate="false"
           name="MonologExtension">

  &lt;requires optional="true"
            specification="org.objectweb.util.monolog.api.LogInfo"
            aggregate="true"
            proxy="false"
            nullable="false">
    &lt;callback type="bind" method="addExtension" />
    &lt;callback type="unbind" method="removeExtension" />
  &lt;/requires>

  &lt;!-- LifeCycle Callbacks -->
  &lt;callback method="start" transition="validate" />
  &lt;callback method="stop" transition="invalidate" />

&lt;/component>
</programlisting>
        <para>MonologExtension class contains method which are called as callback when a service implementing LogInfo interface
        is registered:</para>
<programlisting>
/**
* Add an extension to Monolog
* @param logInfoProvider
*/
public void addExtension(final LogInfo logInfoProvider, ServiceReference ref) {
  Character pattern = (Character) ref.getProperty("pattern");
  Monolog.monologFactory.addLogInfo(pattern, logInfoProvider);
  logger.info("Extension ''{0}'' was added by ''{1}'' to Monolog", pattern, logInfoProvider.getClass().getName());
}

/**
* Remove an extension from Monolog
*/
public void removeExtension(ServiceReference ref) {
  Character pattern = (Character) ref.getProperty("pattern");
  Monolog.monologFactory.removeLogInfo(pattern);
  logger.info("Extension ''{0}'' was removerd from Monolog.", pattern);
}
</programlisting>

        <para>To use monolog extension, you need to make a dependency on :</para>
<programlisting>
  &lt;dependency>
    &lt;groupId>org.ow2.monolog&lt;/groupId>
    &lt;artifactId>monolog&lt;/artifactId>
    &lt;version>2.2.1-SNAPSHOT&lt;/version>
  &lt;/dependency>
</programlisting>
        or any later version.
      </para>

      <para>
        For seeing tenant-id in log messages, add <literal>%T</literal> to the wanted handler (tty, logf, ...) in
        trace.properties. Example :

        <programlisting>handler.tty.pattern  %T %d : %O{1}.%M : %m%n</programlisting>
        In this example, tenant-id will be added at the beginning of the log message. If tenant-id is not set in
        tenantContext or its value is <literal>T0</literal> (which is default tenant-id) then nothing will be printed.
      </para>
    </section>
  </chapter>

</book>
