<?xml version="1.0" encoding="UTF-8"?>
<section version="5.0" xml:id="config.jonas_root_structure" xml:lang="en"
         xmlns="http://docbook.org/ns/docbook"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:xi="http://www.w3.org/2001/XInclude"
         xmlns:svg="http://www.w3.org/2000/svg"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         xmlns:html="http://www.w3.org/1999/xhtml"
         xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <title>JONAS_ROOT structure</title>
  </info>

  <para>The installation directory (JONAS_ROOT) has the following
  structure:</para>

  <para><itemizedlist>
      <listitem>
        <para>the <filename class="directory">deploy/</filename>
        directory</para>

        <para>The main location used for deployment.</para>

        <para>At JOnAS startup, all deployment plans, <xi:include
        href="../variables.xml" xpointer="j2ee" /> archives and OSGi bundles
        are deployed in the following order:</para>

        <para><orderedlist>
            <listitem>
              <para>Deployment plan repositories</para>
            </listitem>

            <listitem>
              <para>OSGi bundles</para>
            </listitem>

            <listitem>
              <para>RAR archives</para>
            </listitem>

            <listitem>
              <para>Deployment plan resources</para>
            </listitem>

            <listitem>
              <para>EJB archives</para>
            </listitem>

            <listitem>
              <para>WAR archives</para>
            </listitem>

            <listitem>
              <para>EAR archives</para>

              <para><note>
                  <para>For each category, file names are chosen in
                  alphabetical order</para>
                </note></para>
            </listitem>
          </orderedlist>This directory is periodically polled in order to
        deploy new archives. For more information have a look at the <link
        xlink:href="configuration_guide.html#services.depmonitor.config">depmonitor
        service configuration</link></para>
      </listitem>

      <listitem>
        <para>the <filename class="directory">bin/</filename> directory</para>

        <para>contains the scripts used to launch JOnAS (Unix and Windows
        scripts).</para>
      </listitem>

      <listitem>
        <para>the <filename class="directory">conf/</filename>
        directory</para>

        <para>contains the <xi:include href="../variables.xml"
        xpointer="element(appserver)" /> configuration files.</para>
      </listitem>

      <listitem>
        <para>the <filename class="directory">examples/</filename>
        directory</para>

        <para>this sub tree containing all the JOnAS examples that are
        described in <xref linkend="getting.started.learning" /></para>
      </listitem>

      <listitem>
        <para>the <filename class="directory">lib/</filename> directory
        <footnote>
            <para>see <link
            xlink:href="j2eeprogrammerguide.html#j2ee.pg.classloader">Understanding
            class loader hierarchy </link>for a complete description of the
            classloader mechanism.</para>
          </footnote></para>

        <para>Used for extending class loaders. It contains five sub
        directories:</para>

        <informaltable>
          <tgroup cols="2">
            <thead>
              <row>
                <entry align="center">Directory</entry>

                <entry align="center">Description</entry>
              </row>
            </thead>

            <tbody>
              <row>
                <entry align="center"><filename
                class="directory">bootstrap/</filename></entry>

                <entry align="center">Jars loaded by the JOnAS
                bootstrap</entry>
              </row>

              <row>
                <entry align="center"><filename
                class="directory">common/</filename></entry>

                <entry align="center">Legacy directory where Ant tasks are
                stored</entry>
              </row>

              <row>
                <entry align="center"><filename
                class="directory">endorsed/</filename></entry>

                <entry align="center">Jars overridding JVM libraries</entry>
              </row>

              <row>
                <entry align="center"><filename
                class="directory">ext/</filename></entry>

                <entry align="center">For non-bundle extensions</entry>
              </row>

              <row>
                <entry align="center"><filename
                class="directory">internal-ee-tld/</filename></entry>

                <entry align="center">Internal use only !</entry>
              </row>
            </tbody>
          </tgroup>
        </informaltable>
      </listitem>

      <listitem>
        <para>the <filename class="directory">logs/</filename>
        directory</para>

        <para>where the log files are created at run-time (when the JONAS_ROOT
        is used as a JONAS_BASE)</para>
      </listitem>

      <listitem>
        <para>the <filename class="directory">templates/</filename>
        directory</para>

        <para>this sub tree contains the following subdirectories used by
        <xi:include href="../variables.xml" xpointer="appserver" /> during the
        generation process (eg, JONAS_BASE generation).</para>

        <itemizedlist>
          <listitem>
            <para><filename class="directory">conf/</filename> is an empty
            template of the JONAS_BASE structure used by tools able to create
            a JONAS_BASE environment.</para>
          </listitem>

          <listitem>
            <para><filename class="directory">newjb/</filename> contains the
            configuration files for creating a JONAS_BASE environment.</para>
          </listitem>

          <listitem>
            <para><filename class="directory">newjc/</filename> contains the
            configuration files for creating a cluster environment.</para>
          </listitem>
        </itemizedlist>
      </listitem>

      <listitem>
        <para>the <filename class="directory">repositories/</filename>
        directory</para>

        <para>this sub tree contains the following repositories used to store
        OSGi bundles, <xi:include href="../variables.xml" xpointer="j2ee" />
        applications and deployment plans.</para>

        <itemizedlist>
          <listitem>
            <para><filename class="directory">maven2-internal/</filename>
            contains both OSGi bundles and applications (jonasAdmin,
            documentation, ...) for JOnAS. It is used for internal purpose and
            should not be modified. This directory is structured as a Maven2
            repository.</para>
          </listitem>

          <listitem>
            <para><filename class="directory">url-internal/</filename>
            contains the deployment plans of each JOnAS services. It is used
            for internal purpose and should not be modified.</para>
          </listitem>

          <listitem>
            <para><filename
            class="directory">&lt;repository-id&gt;/</filename> contains
            archives downloaded through deployment plans from this
            repository.</para>
          </listitem>
        </itemizedlist>
      </listitem>
    </itemizedlist></para>
</section>
