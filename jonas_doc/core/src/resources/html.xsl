<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:d="http://docbook.org/ns/docbook"
                xmlns="http://www.w3.org/1999/xhtml"
                exclude-result-prefixes="d"
                version='1.0'>

    <xsl:import href="../../tools/docbook-xsl-ns-1.76.1/xhtml/docbook.xsl" />
    <xsl:import href="common.xml" />
    
    <xsl:output method="xml"
                encoding="UTF-8"
                indent="yes"
                media-type="application/xhtml+xml" />

       <!--  use graphics in admonitions -->
    <xsl:param name="admon.graphics" select="1" />
    <xsl:param name="admon.graphics.path" select="'../../resources/images/'" />
    <xsl:param name="callout.graphics.path" select="'../../resources/images/callouts/'" />
    <!--  chapters will be numbered -->
    <xsl:param name="chapter.autolabel" select="1" />
    <!--  sections will be numbered -->
    <xsl:param name="section.autolabel" select="1" />
    <!--  section numbers will include the chapter number -->
    <xsl:param name="section.label.includes.component.label" select="1" />
    <!--  parts will be numbered (Uppercase roman numeration )  -->
    <xsl:param name="part.autolabel" select="'I'"></xsl:param>
    <!--  component labels include the part label  -->
    <xsl:param name="component.label.includes.part.label" select="1"></xsl:param>
    <!--  stylesheet to use in the generated HTML  -->
    <xsl:param name="html.stylesheet">common.css</xsl:param>
    <!--  empty paragraphs will be inserted in several contexts -->
    <xsl:param name="spacing.paras" select="'1'"></xsl:param>
    <!-- depth to which recursive sections should appear in the TOC -->
    <xsl:param name="toc.section.depth">2</xsl:param>
    <xsl:param name="simplesect.in.toc" select="0"></xsl:param>
    <!--
      - fix the build for thoses who couldn't build
      - the doc anymore -->
    <xsl:param name="language">en</xsl:param>
    <xsl:param name="annotation.support" select="1"></xsl:param>

    <xsl:param name="keep.relative.image.uris" select="0"></xsl:param>

    <xsl:param name="generate.toc">
      book      toc
      chapter   toc
    </xsl:param>



</xsl:stylesheet>
