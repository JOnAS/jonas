<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:d="http://docbook.org/ns/docbook"
                xmlns="http://www.w3.org/1999/xhtml"
                exclude-result-prefixes="d"
                version='1.0'>
                
    <xsl:import href="../../tools/docbook-xsl-ns-1.76.1/xhtml/chunk.xsl" />
    <xsl:import href="common.xml" />
    
    <xsl:output method="xml"
                encoding="UTF-8"
                indent="yes"
                media-type="application/xhtml+xml" />

    <xsl:param name="admon.graphics" select="1" />
    <xsl:param name="admon.graphics.path" select="'../../../resources/images/'" />
    <xsl:param name="callout.graphics.path" select="'../../../resources/images/callouts/'" />
    <xsl:param name="chapter.autolabel" select="1" />
    <xsl:param name="section.autolabel" select="1" />
    <xsl:param name="part.autolabel" select="'I'"></xsl:param>
    <xsl:param name="section.label.includes.component.label" select="1" />
    <xsl:param name="component.label.includes.part.label" select="1"></xsl:param>
    <xsl:param name="html.stylesheet">common.css</xsl:param>
    <xsl:param name="chunker.output.indent" select="'yes'" />
    <xsl:param name="chunk.section.depth">2</xsl:param>
    <!-- depth of the TOC -->
    <xsl:param name="toc.section.depth">2</xsl:param>
    <!-- enable navigational icons, -->
    <xsl:param name="navig.graphics">1</xsl:param>
    <xsl:param name="navig.graphics.extension">.png</xsl:param>
    <!--  Filename use id -->
    <xsl:param name="use.id.as.filename" select="1" />
    <xsl:param name="language">en</xsl:param>
    <xsl:param name="keep.relative.image.uris" select="1"></xsl:param>
    <xsl:param name="img.src.path">../</xsl:param>

</xsl:stylesheet>
