<?xml version="1.0"?>
<!DOCTYPE xsl:stylesheet [
     <!ENTITY db_xsl_path "../../tools/docbook-xsl-ns-1.76.1">
     <!ENTITY admon_gfx_path "../../../../tools/docbook-xsl-ns-1.76.1/images/">
 ]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:d="http://docbook.org/ns/docbook"
                exclude-result-prefixes="d"
                version='1.0'>

  <!-- Import the docbook5 -> fo converter stylesheet -->
  <xsl:import href="&db_xsl_path;/fo/docbook.xsl" />
  <xsl:import href="common.xml" />


  <!--###################################################
    Custom Title Page
    ################################################### -->

  <xsl:template name="book.titlepage.recto">
    <fo:block>
      <fo:table table-layout="fixed" width="175mm">
        <fo:table-column column-width="175mm" />
        <fo:table-body>
          <fo:table-row>
            <fo:table-cell text-align="center">
              <fo:block>
                <fo:external-graphic src="file:../../resources/images/jonas-header.jpg" content-width="60%" />
              </fo:block>
            </fo:table-cell>
          </fo:table-row>
          <fo:table-row>
            <fo:table-cell text-align="center">
              <fo:block font-family="Helvetica" font-size="32pt" padding-before="10mm">
                <xsl:value-of select="d:info/d:title" />
              </fo:block>
              <fo:block font-family="Helvetica" font-size="12pt" font-style="italic" padding="10mm">
                <xsl:value-of select="d:info/d:abstract" />
              </fo:block>
            </fo:table-cell>
          </fo:table-row>
          <fo:table-row>
            <fo:table-cell text-align="center">
              <fo:block font-family="Helvetica" font-size="10pt">
                <xsl:value-of select="d:info/d:authorgroup/d:author/d:orgname" />
                <xsl:text> (</xsl:text>
                <xsl:for-each select="d:info/d:authorgroup/d:author/d:personname">
                  <!-- > 2 because we do not print the JOnAS Team author -->
                  <xsl:if test="position() &gt; 1">
                    <xsl:text>, </xsl:text>
                  </xsl:if>
                  <xsl:value-of select="d:firstname" />
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="d:surname" />
                </xsl:for-each>
                <xsl:text>)</xsl:text>
              </fo:block>

              <fo:block font-family="Helvetica" font-size="8pt" padding="2mm">
                <xsl:text>- </xsl:text>
                <xsl:value-of select="d:info/d:date" />
                <xsl:text> -</xsl:text>
              </fo:block>
            </fo:table-cell>
          </fo:table-row>
          <fo:table-row>
            <fo:table-cell text-align="center">
              <fo:block font-family="Helvetica" font-size="12pt" padding="10mm">
                <xsl:text>Copyright © </xsl:text>
                <xsl:value-of select="d:info/d:copyright/d:holder" />
                <xsl:text> </xsl:text>
                <xsl:value-of select="d:info/d:copyright/d:year" />
              </fo:block>
              <fo:block font-family="Helvetica" font-size="10pt" padding="1mm">
                <xsl:value-of select="d:info/d:legalnotice" />
              </fo:block>
            </fo:table-cell>
          </fo:table-row>

        </fo:table-body>
      </fo:table>
    </fo:block>
  </xsl:template>

  <!-- Prevent blank pages in output -->
  <xsl:template name="book.titlepage.before.verso"></xsl:template>
  <xsl:template name="book.titlepage.verso"></xsl:template>
  <xsl:template name="book.titlepage.separator"></xsl:template>

  <!--###################################################
    Header
    ################################################### -->

  <!-- More space in the center header for long text -->
  <xsl:attribute-set name="header.content.properties">
    <xsl:attribute name="font-family">
      <xsl:value-of select="$body.font.family" />
    </xsl:attribute>
    <xsl:attribute name="margin-left">-5em</xsl:attribute>
    <xsl:attribute name="margin-right">-5em</xsl:attribute>
  </xsl:attribute-set>

  <!--###################################################
    Custom Footer
    ################################################### -->

  <!-- This footer prints the version number on the left side -->
  <xsl:template name="footer.content">
    <xsl:param name="pageclass" select="''" />
    <xsl:param name="sequence" select="''" />
    <xsl:param name="position" select="''" />
    <xsl:param name="gentext-key" select="''" />

    <xsl:variable name="Version">
      <xsl:choose>
        <xsl:when test="//d:releaseinfo">
          <xsl:text>JOnAS </xsl:text>
          <xsl:value-of select="//d:releaseinfo" />
        </xsl:when>
        <xsl:otherwise>
          <!-- nop -->
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$sequence='blank'">
        <xsl:choose>
          <xsl:when test="$double.sided != 0 and $position = 'left'">
            <xsl:value-of select="$Version" />
          </xsl:when>

          <xsl:when test="$double.sided = 0 and $position = 'center'">
            <!-- nop -->
          </xsl:when>

          <xsl:otherwise>
            <fo:page-number />
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>

      <xsl:when test="$pageclass='titlepage'">
        <!-- nop: other titlepage sequences have no footer -->
      </xsl:when>

      <xsl:when test="$double.sided != 0 and $sequence = 'even' and $position='left'">
        <fo:page-number />
      </xsl:when>

      <xsl:when test="$double.sided != 0 and $sequence = 'odd' and $position='right'">
        <fo:page-number />
      </xsl:when>

      <xsl:when test="$double.sided = 0 and $position='right'">
        <fo:page-number />
      </xsl:when>

      <xsl:when test="$double.sided != 0 and $sequence = 'odd' and $position='left'">
        <xsl:value-of select="$Version" />
      </xsl:when>

      <xsl:when test="$double.sided != 0 and $sequence = 'even' and $position='right'">
        <xsl:value-of select="$Version" />
      </xsl:when>

      <xsl:when test="$double.sided = 0 and $position='left'">
        <xsl:value-of select="$Version" />
      </xsl:when>

      <xsl:otherwise>
        <!-- nop -->
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="*" mode="admon.graphic.width">
    <xsl:param name="node" select="."/>
    <xsl:text>24pt</xsl:text>
  </xsl:template>
  
  <!-- Admonition (warning/note) box. -->
  <xsl:attribute-set name="graphical.admonition.properties">
    <xsl:attribute name="border-style">solid</xsl:attribute>
    <xsl:attribute name="border-width">1pt</xsl:attribute>
    <xsl:attribute name="border-color">grey</xsl:attribute>
    <xsl:attribute name="padding">2pt</xsl:attribute>
    <xsl:attribute name="background-color">#E6E6E6</xsl:attribute>
  </xsl:attribute-set>

  <xsl:param name="paper.type" select="'A4'" />

  <!--###################################################
    Fonts & Styles
    ################################################### -->

  <!-- Left aligned text and no hyphenation -->
  <xsl:param name="alignment">justify</xsl:param>
  <xsl:param name="hyphenate">false</xsl:param>

  <!--  use graphics in admonitions -->
  <xsl:param name="admon.graphics" select="1" />
  <xsl:param name="admon.graphics.path" select="'&admon_gfx_path;'" />
  <!-- don't use graphics for callout -->
  <xsl:param name="callout.graphics" select="0" />
  <!-- depth to which recursive sections should appear in the TOC -->
  <xsl:param name="toc.section.depth">2</xsl:param>
  <!--  chapters will be numbered -->
  <xsl:param name="chapter.autolabel" select="1" />
  <!--  Link's URL is displayed in the footnote -->
  <xsl:param name="ulink.footnotes" select="1" />
  <!--  sections will be numbered -->
  <xsl:param name="section.autolabel" select="1" />
  <!--  section numbers will include the chapter number -->
  <xsl:param name="section.label.includes.component.label" select="1" />
  <!-- ProgramListing/Screen has a background color -->
  <xsl:param name="shade.verbatim">1</xsl:param>
  <xsl:attribute-set name="shade.verbatim.style">
    <xsl:attribute name="background-color">#edf8fd</xsl:attribute>
  </xsl:attribute-set>
  <!-- Reduce size of program listing font and add a border -->
  <xsl:attribute-set name="verbatim.properties">
    <xsl:attribute name="space-before.minimum">1em</xsl:attribute>
    <xsl:attribute name="space-before.optimum">1em</xsl:attribute>
    <xsl:attribute name="space-before.maximum">1em</xsl:attribute>
    <xsl:attribute name="font-size">7pt</xsl:attribute>
    <xsl:attribute name="border-width">1px</xsl:attribute>
    <xsl:attribute name="border-style">dashed</xsl:attribute>
    <xsl:attribute name="border-color">#9999cc</xsl:attribute>
    <xsl:attribute name="padding-top">0.5em</xsl:attribute>
    <xsl:attribute name="padding-left">0.5em</xsl:attribute>
    <xsl:attribute name="padding-right">0.5em</xsl:attribute>
    <xsl:attribute name="padding-bottom">0.5em</xsl:attribute>
    <xsl:attribute name="margin-left">0.5em</xsl:attribute>
    <xsl:attribute name="margin-right">0.5em</xsl:attribute>
  </xsl:attribute-set>
  <!-- Allow to wrap long lines for program listing -->
  <!--
    <xsl:param name="hyphenate.verbatim" select="1"/>
    -->
    <xsl:attribute-set name="monospace.verbatim.properties">
    <xsl:attribute name="wrap-option">wrap</xsl:attribute>
    <xsl:attribute name="hyphenation-character">\</xsl:attribute>
    </xsl:attribute-set>

  <!-- for getting bookmarks in pdf document -->
  <xsl:param name="fop1.extensions" select="1" />

</xsl:stylesheet>


