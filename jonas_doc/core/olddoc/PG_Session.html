<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"
                      "http://www.w3.org/TR/REC-html40/loose.dtd">
<!--
- JOnAS: Java(TM) Open Application Server
- Copyright (C) 2003 Bull S.A.
- Contact: jonas-team@objectweb.org

- This work is licensed under the Creative Commons
- Attribution-ShareAlike License. To view a copy of this license,
- visit http://creativecommons.org/licenses/by-sa/2.0/deed.en or
- send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford,
- California 94305, USA.

- Author: Fran�ois EXERTIER

- $Id$
-->
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <link rel="stylesheet" href="common.css">
  <title>Developing Session Beans</title>
</head>

<body lang="en">
<h1><a name="PG_Session"></a>EJB Programmer's Guide: Developing Session
Beans</h1>

<h2><a name="PG_Session-Target"></a>Target Audience and Content</h2>

<p>The target audience for this guide is the Enterprise Bean provider, i.e.
the person in charge of developing the software components on the server side
and, more specifically, the Session Beans.</p>

<p>The content of this guide is the following:</p>
<ol>
  <li><a href="#PG_Session-Target">Target Audience and Content</a></li>
  <li><a href="#PG_Session-Introducti">Introduction</a></li>
  <li><a href="#PG_Session-Home">The Home Interface</a></li>
  <li><a href="#PG_Session-Remote">The Component Interface</a></li>
  <li><a href="#PG_Session-Enterprise">The Enterprise Bean Class</a></li>
  <li><a href="#PG_Session-Tuning">Tuning Stateless Session Bean Pool</a></li>
</ol>

<h2><a name="PG_Session-Introducti"></a>Introduction</h2>

<p>A Session Bean is composed of the following parts, which are developed by
the Enterprise Bean Provider:</p>
<ul>
  <li>The <b>Component Interface</b> is the client view of the bean. It
    contains all the "business methods" of the bean.</li>
  <li>The <b>Home Interface</b> contains all the methods for the bean life
    cycle (creation, suppression) used by the client application.</li>
  <li>The <b>bean implementation class</b> implements the business methods
    and all the methods (described in the EJB specification), allowing the
    bean to be managed in the container.</li>
  <li>The <b>deployment descriptor</b> contains the bean properties that can
    be edited at assembly or deployment time.</li>
</ul>

<p>Note that, according to the EJB 2.0 specification, the couple "Component
Interface and Home Interface" may be either local or remote. <b>Local
Interfaces</b> (Home and Component) are to be used by a client running in the
same JVM as the EJB component. Create and finder methods of a local or remote
home interface return local or remote component interfaces respectively. An
EJB component can have both remote and local interfaces, even if typically
only one type of interface is provided.</p>

<p>The description of these elements is provided in the following
sections.</p>

<p>Note: in this documentation, the term "Bean" always means "Enterprise
Bean."</p>

<p>A session bean object is a short-lived object that executes on behalf of a
single client.There are <b>stateless </b>and <b>stateful session beans</b>.
Stateless beans do not maintain state across method calls. Any instance of
stateless beans can be used by any client at any time. Stateful session beans
maintain state within and between transactions. Each stateful session bean
object is associated with a specific client. A stateful session bean with
container-managed transaction demarcation can optionally implement the
<b>SessionSynchronization </b>interface. In this case, the bean objects will
be informed of transaction boundaries. A rollback could result in a session
bean object's state being inconsistent; in this case, implementing the
SessionSynchronization interface may enable the bean object to update its
state according to the transaction completion status.</p>

<h2><a name="PG_Session-Home"></a>The Home Interface</h2>

<p>A Session bean's home interface defines one or more <i>create(...)
</i>methods. Each <i>create </i>method must be named <tt>create</tt> and must
match one of the ejbCreate methods defined in the enterprise Bean class. The
return type of a create method must be the enterprise Bean's remote interface
type.<br>
The home interface of a stateless session bean must have one <i>create</i>
method that takes no arguments.</p>

<p>All the exceptions defined in the throws clause of an <i>ejbCreate
</i>method must be defined in the throws clause of the matching <i>create
</i>method of the home interface.</p>

<p>A <b>remote home interface</b> extends the <code>javax.ejb.EJBHome</code>
interface, while a <b>local home interface</b> extends the
<code>javax.ejb.EJBLocalHome</code> interface.</p>

<h3>Example:</h3>

<p>The following examples use a Session Bean named Op.</p>
<pre>    public interface <b>OpHome</b> extends EJBHome {
        Op <b>create</b>(String user) throws CreateException, RemoteException;
    }
    </pre>

<p>A local home interface could be defined as follows (<code>LocalOp</code>
being the local component interface of the bean):</p>
<pre>    public interface <b>LocalOpHome</b> extends EJBLocalHome {
        LocalOp <b>create</b>(String user) throws CreateException;
    }
    </pre>

<h2><a name="PG_Session-Remote"></a>The Component Interface</h2>

<p>The Component Interface is the client's view of an instance of the session
bean. This interface contains the business methods of the enterprise bean.
The interface must extend the <tt>javax.ejb.EJBObject</tt> interface if it is
remote, or the <tt>javax.ejb.EJBLocalObject</tt> if it is local. The methods
defined in a remote component interface must follow the rules for Java RMI
(this means that their arguments and return value must be valid types for
java RMI, and their throws clause must include the
<tt>java.rmi.RemoteException</tt>). For each method defined in the component
interface, there must be a matching method in the enterprise Bean's class
(same name, same arguments number and types, same return type, and same
exception list, except for RemoteException).</p>

<h3>Example:</h3>
<pre>    public interface <b>Op</b> extends EJBObject {
        public void <b>buy</b> (int Shares)  throws RemoteException;
        public int  <b>read</b> ()           throws RemoteException;
    }
    </pre>

<p>The same type of component interface could be defined as a local interface
(even if it is not considered good design to define the same interface as
both local and remote):</p>
<pre>    public interface <b>LocalOp</b> extends EJBLocalObject {
        public void <b>buy</b> (int Shares);
        public int  <b>read</b> ();
    }
    </pre>

<h2><a name="PG_Session-Enterprise"></a>The Enterprise Bean Class</h2>

<p>This class implements the Bean's business methods of the component
interface and the methods of the <i>SessionBean </i>interface, which are
those dedicated to the EJB environment. The class must be defined as public
and may not be abstract. The <i>Session Bean </i>interface methods that the
EJB provider must develop are the following:</p>
<ul>
  <li><em>public void</em><em><b>
    setSessionContext</b></em><em>(SessionContext ic);</em>
    <p>This method is used by the container to pass a reference to the
    SessionContext to the bean instance. The container invokes this method on
    an instance after the instance has been created. Generally, this method
    stores this reference in an instance variable.</p>
  </li>
  <li><em>public void</em><em><b> ejbRemove</b></em><em>();</em>
    <p>This method is invoked by the container when the instance is in the
    process of being removed by the container. Since most session Beans do
    not have any resource state to clean up, the implementation of this
    method is typically left empty.</p>
  </li>
  <li><em>public void</em><em><b> ejbPassivate(</b></em><em>);</em>
    <p>This method is invoked by the container when it wants to passivate the
    instance. After this method completes, the instance must be in a state
    that allows the container to use the Java Serialization protocol to
    externalize and store the instance's state.</p>
  </li>
  <li><em>public void</em><em><b>ejbActivate</b>();</em>
    <p>This method is invoked by the container when the instance has just
    been reactivated. The instance should acquire any resource that it has
    released earlier in the ejbPassivate() method.</p>
  </li>
</ul>

<p>A stateful session Bean with container-managed transaction demarcation can
optionally implement the<tt> javax.ejb.SessionSynchronization </tt>interface.
This interface can provide the Bean with transaction synchronization
notifications. The <i>Session Synchronization </i>interface methods that the
EJB provider must develop are the following:</p>
<ul>
  <li><em>public void</em><em><b> afterBegin</b></em><em>();</em>
    <p>This method notifies a session Bean instance that a new transaction
    has started. At this point the instance is already in the transaction and
    can do any work it requires within the scope of the transaction.</p>
  </li>
  <li><em>public void</em><em><b> afterCompletion(</b></em><em>boolean
    committed);</em>
    <p>This method notifies a session Bean instance that a transaction commit
    protocol has completed and tells the instance whether the transaction has
    been committed or rolled back.</p>
  </li>
  <li><em>public void <b>beforeCompletion</b>();</em>
    <p>This method notifies a session Bean instance that a transaction is
    about to be committed.</p>
  </li>
</ul>

<h3><a name="PG_Session-Example"></a>Example:</h3>
<pre>package sb;

import java.rmi.RemoteException;
import javax.ejb.EJBException;
import javax.ejb.EJBObject;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.ejb.SessionSynchronization;
import javax.naming.InitialContext;
import javax.naming.NamingException;

// This is an example of Session Bean, stateful, and synchronized.

public class <b>OpBean</b> implements SessionBean, SessionSynchronization {

    protected int total = 0;        // actual state of the bean
    protected int newtotal = 0;        // value inside Tx, not yet committed.
    protected String clientUser = null;
    protected SessionContext sessionContext = null;

    public void  <b>ejbCreate</b>(String user) {
        total = 0;
        newtotal = total;
        clientUser = user;
    }

    public void <b>ejbActivate</b>() {
        // Nothing to do for this simple example
    }    

    public void <b>ejbPassivate</b>() {
        // Nothing to do for this simple example
    }

    public void <b>ejbRemove</b>() {
        // Nothing to do for this simple example
    }

    public void <b>setSessionContext</b>(SessionContext sessionContext) {
        this.sessionContext = sessionContext;
    }

    public void <b>afterBegin</b>() {
        newtotal = total;
    }

    public void <b>beforeCompletion</b>() {
        // Nothing to do for this simple example

        // We can access the bean environment everywhere in the bean,
        // for example here!
        try {
            InitialContext ictx = new InitialContext();
            String value = (String) ictx.lookup("java:comp/env/prop1");
            // value should be the one defined in ejb-jar.xml
        } catch (NamingException e) {
            throw new EJBException(e);
        }
    }

    public void <b>afterCompletion</b>(boolean committed) {
        if (committed) {
            total = newtotal;
        } else {
            newtotal = total;
        }
    }

    public void <b>buy</b>(int s) {
        newtotal = newtotal + s;
        return;
    }

    public int <b>read</b>() {
        return newtotal;
    }
}
    </pre>

<h2><a name="PG_Session-Tuning"></a>Tuning Stateless Session Bean Pool</h2>
JOnAS handles a pool for each stateless session bean. The pool can be
configured in the JOnAS-specific deployment descriptor with the following
tags:

<h3>min-pool-size</h3>
This optional integer value represents the minimum instances that will be
created in the pool when the bean is loaded. This will improve bean instance
creation time, at least for the first beans. The default value is 0.

<h3>max-cache-size</h3>
This optional integer value represents the maximum of instances in memory.
The purpose of this value is to keep JOnAS scalable. The policy is the
following: <br>
At bean creation time, an instance is taken from the pool of free instances.
If the pool is empty, a new instance is always created. When the instance
must be released (at the end of a business method), it is pushed into the
pool, except if the current number of instances created exceeds the
<tt>max-cache-size</tt>, in which case this instance is dropped. The default
value is no limit.

<h3>example</h3>
<pre>  &lt;jonas-ejb-jar&gt;
    &lt;jonas-session&gt;
      &lt;ejb-name&gt;SessSLR&lt;/ejb-name&gt;
      &lt;jndi-name&gt;EJB/SessHome&lt;/jndi-name&gt;
      &lt;max-cache-size&gt;20&lt;/max-cache-size&gt;
      &lt;min-pool-size&gt;10&lt;/min-pool-size&gt;
    &lt;/jonas-session&gt;
  &lt;/jonas-ejb-jar&gt;
   </pre>
</body>
</html>
