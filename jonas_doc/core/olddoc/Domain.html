<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html>
<head>
  <!--
  - JOnAS: Java(TM) Open Application Server
  - Copyright (C) 2004 Bull S.A.
  - Contact: jonas-team@objectweb.org

  - This library is free software; you can redistribute it and/or
  - modify it under the terms of the GNU Lesser General Public
  - License as published by the Free Software Foundation; either
  - version 2.1 of the License, or any later version.

  - This library is distributed in the hope that it will be useful,
  - but WITHOUT ANY WARRANTY; without even the implied warranty of
  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  - Lesser General Public License for more details.

  - You should have received a copy of the GNU Lesser General Public
  - License along with this library; if not, write to the Free Software
  - Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
  - USA

  - Author: Adriana Danes
  -->
  <!--
  - $Id$
  -->
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <title>Domain Management in JOnAS</title>
  <link rel="stylesheet" href="common.css">
</head>

<body lang="en">
  <h1><a name="Domain"></a>Domain and Cluster Management in JOnAS</h1>

  <p>The content of this guide is the following:</p>
  <ol>
    <li><a href="#Domain-Target">Target Audience and Rationale</a></li>
    <li><a href="#Domain-Intro">Domain and Cluster Management Functions</a></li>
    <li><a href="#Domain-Mechanisms">Mechanisms used by Domain Management</a></li>
    <li><a href="#Domain-J2EEDomain">J2EEDomain MBean</a></li>
    <li><a href="#Domain-Monitoring">MBeans for domain/cluster monitoring</a></li>
    <li><a href="#Domain-Usage">How to start Servers in a Domain</a></li>
    <li><a href="#Domain-Topology">How to define domain configuration using domain.xml file</a></li>
  </ol>

  <h2><a name="Domain-Target"></a>Target Audience and Rationale</h2>
  <p>This guide is intended for JOnAS administrators responsible for the configuration
 and administration of JOnAS servers running within a management domain.
  </p>

  <h2><a name="Domain-Intro"></a>Domain and Cluster Management Functions</h2>
  <p>A JOnAS management domain is composed of a set of JOnAS servers that are running under the same
 management authority. All the servers in the domain must have a distinct <i>server name</i> and
 a common <i>domain name</i>.
  </p>
  <p>The servers in a domain can be administered by a management application running on a server
    playing the role of administrator or <i>master</i>. The managed servers play the role of <i>slaves</i>.
    Note that it is possible to have several masters in a domain. Also note that default configuration
    corresponds to a slave server running without the discovery service (the role of this service is
    described below and its configuration <a href="configuration_guide.html#N10C40">here</a>).
  </p>
  <p>
 Typically, when deploying the JonasAdmin application on a <i>slave</i>, the administrator can manage only
 the server on which the application is running. When deploying JonasAdmin on a <i>master</i> server,
 the administrator can manage all the known servers in the domain:
    <ul>
   <li>the administrator can see the state of the servers belonging to the domain;</li>
   <li>can apply management operations to
 one or more servers in the domain;</li>
   <li>switch management from the master to any other running server
 in the domain, then switch back to the master.</li>
   <li>Starting with JOnAS 4.8, administration is enriched with domain and cluster monitoring features,
   and remote control.</li>
    </ul>
  </p>
  <p>
 <strong>Cluster</strong> management facilities was introduced in JOnAS 4.7.
 A cluster is a group of servers having common properties within a domain. A cluster may be the target of a
 domain level management operation - currently applications and J2EE modules deployment only.
  </p>
  <p>
 The domain topology (servers and clusters composing the domain) can be defined using a new <strong>domain.xml</strong>
 configuration file. Also, servers and clusters may be dynamically added and removed to the domain via JonasAdmin.
  </p>
  <p>
 Starting with JOnAS 4.8, domain and cluster management is enhanced with monitoring functions available in
 JonasAdmin. As in the previous version, clusters may be created by an administrator in a static (via domain.xml)
 or dynamic (via JonasAdmin) way. These clusters are now called <i>logical cluster</i>. A new class of clusters
 is supported in the current version, the <i>physical clusters</i> that are detected automatically by the management
 infrastructure. Several types of physical clusters are supported: Mod JK clusters, Tomcat clusters, CMI clusters, etc.
 All the members of a type of physical cluster share specific properties which depend on the cluster type, e.g., all
 the members of a CMI cluster have the same JGroups configuration. Note that a given JOnAS instance may belong to
 several physical and logical clusters.
  </p>
  <p>
 An important domain level administration operation introduced in JOnAS 4.8, is the possiblity to start/stop
 the managed servers via a so called <a href="./clusterd.html">cluster daemon</a>.
    Cluster daemons can be defined and associated
 to servers using the domain.xml configuration file. A cluster daemon has to be collocalized (located on the same
 machine) with the servers it controls.
  </p>

  <h2><a name="Domain-Mechanisms"></a>Mechanisms used by Domain Management</h2>
  <p>Basically, domain management in JOnAS relies on JSR 160 specification. When a JOnAS server is started,
 it creates at least one JSR 160 connector server as explained in
  <a href="./howto/JSR160_support.html#JSR160_support">JSR 160 support in JOnAS</a>.
  </p>
  <p>
 A connector server makes it possible for a JMX remote API client to access and manage the MBeans exposed
 through the MBean server running in a remote JVM. In order to establish a connection, the remote client
 needs to know the address of the connector server. JSR 160 does not provide any specific API that would
 make it possible for a client to find the address of a connector server. The standard suggests using
 existing discovery and lookup infrastructures, for instance, JNDI API with an LDAP back end.
  </p>
  <p>
 A new service added to JOnAS, the <i>Discovery Service</i>, allows JOnAS servers running over
 a LAN within the same management domain to communicate to each other the connector-server addresses they create
 at start-up. All the servers in the domain having the discovery service in the <i>services</i> list, will
 <i>publish</i> their connector-server address at start-up. The goal is to allow <i>master</i> servers to
 discover which servers are running in their domain, and to establish connections allowing them to remotely
 manage the <i>slave</i> servers by a management application deployed on a <i>master</i> server.
  </p>
  <p>
 Starting with JOnAS 4.7, a server can be added to a domain via a management operation, thus allowing servers which
 cannot use the multicast-based discovery service to join a management domain.
  </p>
  <p>
 The current discovery service implementation is based on MBeans (called discovery MBeans) which use:
 <ul>
   <li>IP Multicast protocol to communicate their connector-server addresses and state transitions to each other.</li>
    <li>JMX notifications to update domain management information within a <i>master</i> server.</li>
 </ul>
  </p>
  <h2><a name="Domain-J2EEDomain"></a>J2EEDomain MBean</h2>
 Domain management information can be retrieved from <code>J2EEDomain</code> MBean.
  <p>
 The <code>J2EEDomain</code> MBean has the following JMX ObjectName:
    <pre>
  domainName:j2eeType=J2EEDomain,name=domainName
    </pre>
 Where <i>domainName</i> is the name of the domain.
  </p>
  <p>
 A <code>J2EEDomain</code> MBean contains several management attributes and operations related to servers management
 in a domain:
 <ul>
   <li>If the <code>J2EEDomain</code> is registered in a <i>master</i> server's MBean server, the <i>servers</i>
   attribute keeps the list of the servers known in the domain. Otherwise, it contains only one element corresponding to itself.
   The elements in the <i>servers</i> list are Strings representing <code>J2EEServer</code> ObjectNames
   associated to the running servers. By iterating over the <i>servers</i>
   list, a management application can determine the name of the servers in the domain.
      </li>
      <li>The <i>getConnectorServerURLs</i> operation returns the connector server addresses for a server in the list.
  In order to administer a given server in the domain, any management application has to create an
  <code>MBeanServerConnection</code> object using one of the connector-server addresses corresponding to that server
  (see the <code>j2eemanagement</code> sample).
      </li>
 </ul>
  </p>
  <p>
  </p>
  <p>
 Starting with JOnAS 4.7, the <code>J2EEDomain</code> MBean was enriched in order to support adding/removing a server to/from the domain,
 creating a cluster in the domain, listing the clusters in a domain.
  </p>
  <p>
 The following is a list of some of the new management attributes and operations exposed by a <code>J2EEDomain</code> MBean:
 <ul>
   <li><i>addServer(String serverName, String[] connectorServerURLs)</i></li>
   <li><i>getServerState(serverName)</i></li>
   <li><i>createCluster(String clusterName)</i></li>
   <li><i>clusters</i> attribute gives the String representation of the <code>J2EEDomain</code> ObjectNames 
      associated to the clusters created in this domain 
      (or in this cluster if the current <code>J2EEDomain</code> MBean is associated to a cluster).</li>
 </ul>
  </p>
  <p>
 In JOnAS 4.7, clusters are implemented as "sub-domains", they have associated <code>J2EEDomain</code>
    type MBeans. For example, if a cluster named <strong>clust</strong> is created in the domain named
    <strong>domainName</strong>, it has an associated <code>MBean</code> with the following JMX ObjectName:
 <pre>
  domainName:j2eeType=J2EEDomain,name=domainName,clusterName=clust,parentClusterName=domainName
 </pre>

  </p>

  <h2><a name="Domain-Monitoring"></a>MBeans for domain/cluster monitoring</h2>
  <p>
 An important re-engineering of domain management mechanisms was conducted in JOnAS 4.8 to support
 cluster and domain monitoring. Clusters aren't implemented as sub-domains anymore; there is only
 one <code>J2EEDomain</code> MBean registered in the master's MBean server. Another important point
    is that the servers' state is no longer accessible in the J2EEDomain MBean (<i>getServerState(serverName)</i>
    operation doesn't exist anymore). On the other hand, new MBeans were created:
  </p>
  <ul>
 <li><code>ServerProxy</code> MBeans. For every server known in the domain, a ServerProxy MBean is created with the
    following JMX ObjectName:
 <pre>
  domainName:type=ServerProxy,name=serverName
 </pre>
    It contains a <code>State</code> attribute which gives the server state as known by the master, and
 a set of monitoring information like memory usage, threads, transactions, etc. which are periodically
    updated. The servers may be in one of the following states:
      <ul>
        <li>INIT - corresponds to a server that was added to the domain. This is a temporary state. </li>
        <li>UNREACHABLE - corresponds to a server that was added to the domain but the master
        is unable to access the managed server (the <code>MBeanServerConnection</code> could
  not be established, or it doesn't work anymore) </li>
        <li>RUNNING - corresponds to a server that is running and controlled by the master </li>
        <li>STOPPED - corresponds to a server which was stopped with the <code>jonas stop</code>
      command (if the server has enabled its discovery service), or if the server was stopped
      using the remote control mechanisms (via the <a href="./clusterd.html">cluster daemon</a>). </li>
        <li>FAILED - corresponds to a particular case of UNREACHABLE state. The master decides to pass
      a server in the FAILED state if it is UNREACHABLE for a while, and if it has some information
      allowing to infer that the server is failed: First, the managed server had its discovery service
      enabled (otherwise, the server stop can't be detected, so can't distinguish STOPPED from FAILED).
      Moreover, the master can use information provided by the cluster daemon, if the
      server has an associated, running, cluster daemon.  </li>
      </ul>
    </li>
 <li><code>Cluster</code> MBeans. For every cluster created in the domain, a <code>LogicalCluster</code>
    MBean is created with the following JMX ObjectName:
   <pre>
  domainName:type=LogicalCluster,name=clusterName
   </pre>
      <p>
    The master may have registered other types of cluster MBeans which correspond to physical clusters.
    They are automatically  created by the management system if managed servers are identified as
    being members of supported physical cluster types.
      </p>
    A cluster MBean contains a <code>State</code> attribute and a member's list.
    A cluster may be in one of the following states,
    which depends on the state of its members.
      <ul>
        <li>INIT - corresponds to a cluster that was created in the domain. This is a temporary state.</li>
        <li>UP - all the members are in RUNNING state</li>
        <li>DOWN - all the members are STOPPED</li>
        <li>FAILED - all the members are FAILED</li>
        <li>PARTIALLY_FAILED - at least one member is FAILED</li>
        <li>PARTIALLY_UP - at least one member is RUNNING, there is no failed member</li>
        <li>PARTIALLY_DOWN- at least one member is STOPPED, there is no FAILED member, there is no RUNNING member</li>
      </ul>
    </li>
 <li><code>ClusterMember</code> MBeans. A member MBean's role is to make the link between a cluster MBean and
    the ServerProxy MBean
    corresponding to the server which belongs to that cluster. Any member MBean has a <i>ServerProxy</i> attribute
    equal to the corresponding ServerProxy MBean OBJECT_NAME. If a server belongs to several clusters, there is a
    member MBean for each cluster, and all these members have the same value for the <i>ServerProxy</i> attribute.
    There are several types of member MBeans. They correspond to the several cluster types. The
    <code>LogicalClusterMember</code>
    MBeans are used to represent <code>LogicalCluster</code> members.
    </li>
  </ul>

  <h3>Physical clusters</h3>
Logical clusters are created by the administrator to facilitate management administration on a group of
servers. Physical clusters are created automatically if servers running in the domain have particular
properties which depend on the cluster type.
  <ul>
    <li><code>Mod_jk</code> load balancing clusters. The members of such a cluster are workers defined in
the mod_jk configuration file workers.properties. The workers correspond to JOnAS server instances
having the web service based on Tomcat and a specific configuration (server.xml having a Connector
XML element for AJP1.3 protocol defined, as well as the Engine element having the jvmRoute attribute
set to the worker name).
      <p>The ObjectNames corresponding to Mod_jk clusters and Mod_jk cluster members are:
     <pre>
  domainName:type=JkCluster,name=clusterName
     </pre>
     <pre>
  domainName:type=JkClusterMember,name=workerName,JkCluster=clusterName
     </pre>
        The <i>workerName</i> and <i>clusterName</i> are defined in the workers.properties configuration file.
      </p>
    </li>
    <li><code>Tomcat</code> session replication clusters. The members are JOnAS-Tomcat cluster servers having
a <code>Cluster</code> element defined in server.xml file. The management infrastructure detects
Tomcat cluster members by looking for Tomcat Cluster MBeans in the managed server's MBean server.
      <p>The ObjectNames corresponding to Tomcat session replication clusters and Tomcat cluster members are:
     <pre>
  domainName:type=TomcatCluster,name=clusterName
     </pre>
     <pre>
  domainName:type=TomcatClusterMember,name=serverName,TomcatCluster=clusterName
     </pre>
    The <i>clusterName</i> can be defined in the server.xml configuration file.
      </p>
    </li>
    <li><code>CMI</code> clusters. The members are JOnAS cluster servers sharing the same CMI configuration
for the JNDI replication and the EJB clustering.
      <p>The ObjectNames corresponding to the CMI clusters and cluster members are:
     <pre>
  domainName:type=CmiCluster,name=clusterName
     </pre>
     <pre>
  domainName:type=CmiClusterMember,name=serverName,CmiCluster=clusterName
     </pre>
    The <i>clusterName</i> can be defined in the carol.properties configuration file and
    represents a JavaGroups group name.
      </p>
    </li>
    <li><code>HA</code> clusters. The members are JOnAS cluster servers sharing the same HA service configuration
for the EJB replication.
      <p>The ObjectNames corresponding to the HA clusters and cluster members are:
     <pre>
  domainName:type=HaCluster,name=clusterName
     </pre>
     <pre>
  domainName:type=HaClusterMember,name=serverName,HaCluster=clusterName
     </pre>
 The <i>clusterName</i> can be defined in the jonas.properties configuration file and
    represents a JavaGroups group name.
      </p>
    </li>
  </ul>

  <h2><a name="Domain-Usage"></a>How to start manageable servers in a Domain</h2>
<p>The servers must adhere to these rules:</p>
<ul>
<li>The servers must be started on hosts connected by a LAN.</li>
<li>The servers must be configured with the discovery service activated (see
 Configuring Discovery Service).
</li>
<li>There must be at least one <i>master</i> server in the domain.
 The role of a server, <i>slave</i>
 or <i>master</i>, is determined by the discovery service configuration.
</li>
<li>The servers must have different names, but the same domain name.</li>
</ul>
<p>Also note the following:</p>
<ul>
 <li>Servers may be started in any order.</li>
 <li>Servers may join or leave the domain at any time. Domain management information is updated on the <i>master</i> server(s).</li>
 <li>A <i>slave</i> server may become <i>master</i> by invoking the <i>startDiscoveryMaster</i> management operation exposed by
  the discovery service. This operation can be invoked using the JonasAdmin management console deployed on a <i>master</i>.</li>
</ul>
<h3>Example</h3>
<p>Consider a scenario in which there are three JOnAS servers named <i>jonas</i>, <i>j1</i> and <i>j2</i>.
   Assume that they have discovery service configured with at least one of the servers playing the role of <i>master</i>.
</p>
<ul>
 <li>Start the server named <i>jonas</i> in domain <i>jonas</i> (by default, the domain name is given by the server's name)
 <pre>
  jonas start
 </pre></li>
 <li>Start the server named <i>j1</i> in domain <i>jonas</i>
 <pre>
  jonas start -n j1 -Ddomain.name=jonas
 </pre></li>
 <li>Start the server named <i>j2</i> in domain <i>jonas</i>
 <pre>
  jonas start -n j2 -Ddomain.name=jonas
 </pre></li>
</ul>

<h2><a name="Domain-Topology"></a>How to define domain configuration using domain.xml file</h2>
<p>A default domain configuration is provided in <code>$JONAS_ROOT/conf/domain.xml</code>.
This configuration corresponds to a domain named <strong>jonas</strong> managed by a <i>master</i>
server also named <strong>jonas</strong>. The <code>location</code> tag defines a JMX remote
connector server URL for this server.
</p>

</body>
</html>
