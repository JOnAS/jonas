<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<!--
- JOnAS: Java(TM) Open Application Server
- Copyright (C) 2003 Bull S.A.
- Contact: jonas-team@objectweb.org

- This work is licensed under the Creative Commons
- Attribution-ShareAlike License. To view a copy of this license,
- visit http://creativecommons.org/licenses/by-sa/2.0/deed.en or
- send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford,
- California 94305, USA.

- Author: H�l�ne JOANIN
-->
<html>
<head>
  <!--
  - $Id$
  -->
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <title>JOnAS Services</title>
  <link rel="stylesheet" href="common.css">
</head>

<body lang="en">
<h1><a name="Services"></a>Creating a New JOnAS Service</h1>

<p>The content of this guide is the following:</p>
<ol>
  <li><a href="#Services-Target">Target Audience and Rationale</a></li>
  <li><a href="#Services-Intro">Introducing a new Service</a></li>
  <li><a href="#Services-Advan">Advanced Understanding</a></li>
</ol>

<h2><a name="Services-Target"></a>Target Audience and Rationale</h2>

<p>This chapter is intended for advanced JOnAS users who require that some
"external" services run along with the JOnAS server. A service is something
that may be initialized, started, and stopped. JOnAS itself already defines a
set of services, some of which are cornerstones of the JONAS Server. The
JOnAS pre-defined services are listed in <a
href="configuration_guide.html#config.services">Configuring JOnAS services</a>.</p>

<p>J2EE application developers may need to access other services, for example
another Web container or a Versant container, for their components. Thus, it
is important that such services be able to run along with the application
server. To achieve this, it is possible to define them as JOnAS services.</p>

<p>This chapter describes how to define a new JOnAS service and how to
specify which service should be started with the JOnAS server.</p>

<h2><a name="Services-Intro"></a>Introducing a new Service</h2>

<p>The customary way to define a new JOnAS service is to encapsulate it in a
class whose interface is known by JOnAS. More precisely, such a class
provides a way to initialize, start, and stop the service. Then, the
jonas.properties file must be modified to make JOnAS aware of this
service.</p>

<h3>Defining the Service class</h3>

<p>A JOnAS service is represented by a class that implements the interface
<code>org.objectweb.jonas.service.Service</code>, and, thus should implement
the following methods:</p>
<ul>
  <li><code>public void init(Context ctx) throws ServiceException;</code></li>
  <li><code>public void start() throws ServiceException;</code></li>
  <li><code>public void stop() throws ServiceException;</code></li>
  <li><code>public boolean isStarted();</code></li>
  <li><code>public String getName();</code></li>
  <li><code>public void setName(String name);</code></li>
</ul>

<p>It should also define a public constructor with no argument.</p>

<p>These methods will be called by JOnAS for initializing, starting, and
stopping the service. Configuration parameters are provided to the
initialization method through a naming context. This naming context is built
from properties defined in the <code>jonas.properties</code> file as
explained in the <a href="#Services-Modif">following section</a>.</p>

<p>The Service class should look like the following:</p>
<pre>package a.b;<br>import javax.naming.Context;<br>import javax.naming.NamingException;<br>import org.objectweb.jonas.service.Service;<br>import org.objectweb.jonas.service.ServiceException;<br>.....<br>public class MyService implements Service {<br>    private String name = null;<br>    private boolean started = false;<br>    .....<br>    public void init(Context ctx) throws ServiceException {<br>        try {<br>            String p1 = (String) ctx.lookup("jonas.service.serv1.p1");<br>            .....<br>        } catch (NamingException e) {<br>            throw new ServiceException("....", e);<br>        }<br>        .....<br>    }<br>    public void start() throws ServiceException {<br>        .....<br>        this.started = true;<br>    }<br>    public void stop() throws ServiceException {<br>        if (this.started) {<br>            this.started = false;<br>            .....<br>        }<br>    }<br>    public boolean isStarted() {<br>        return this.started;<br>    }<br>    public String getName() {<br>        return this.name;<br>    }<br>    public void setName(String name) {<br>        this.name = name;<br>    }<br>}<br>    </pre>

<h3><a name="Services-Modif"></a>Modifying the jonas.properties file</h3>

<p>The service is defined and its initialization parameters specified in the
<code>jonas.properties</code> file. First, choose a name for the service
(e.g. "serv1"), then do the following:</p>
<ul>
  <li>add this name to the <code>jonas.services</code> property; this
    property defines the set of services (comma-separated) that will be
    started with JOnAS, <strong>in the order of this list</strong>.</li>
  <li>add a <code>jonas.service.serv1.class</code> property specifying the
    service class.</li>
  <li>add as many <code>jonas.service.serv1.XXX</code> properties specifying
    the service initialization parameters, as will be made available to the
    service class via the Context argument of the <code>init</code>
  method.</li>
</ul>

<p>This is illustrated as follows:</p>
<pre>  jonas.services                   .......,serv1<br>  jonas.service.serv1.class        a.b.MyService<br>  jonas.service.serv1.p1           value<br>    </pre>

<h3><a name="Services-Using"></a>Using the New Service</h3>
The new service has been given a name in <code>jonas.properties</code>. With
this name, it is possible to get a reference on the service implementation
class by using the ServiceManager method: <code>getService(name)</code>. The
following is an example of accessing a Service:
<pre>import org.objectweb.jonas.service.ServiceException;<br>import org.objectweb.jonas.service.ServiceManager;<br><br>    MyService sv = null;<br><br>        // Get a reference on MyService.<br>        try {<br>            sv = (MyService) ServiceManager.getInstance().getService("serv1");<br>        } catch (ServiceException e) {<br>            Trace.errln("Cannot find MyService:"+e);<br>        }<br>    </pre>

<h3>Adding the class of the new service to JOnAS</h3>

<p>Package the class of the service into a .jar file and add the jar in the
<code>JONAS_ROOT/lib/ext</code> directory.<br>
All the libraries required by the service can also be placed in this
directory.</p>

<h2><a name="Services-Advan"></a>Advanced Understanding</h2>

<p>Refer to the JOnAS sources for more details about the classes mentioned in
this section.</p>

<h3>JOnAS built-in services</h3>

<p>The existing JOnAS services are the following:</p>

<table border="1">
  <tbody>
    <tr>
      <th><strong>Service name</strong></th>
      <th><strong>Service class</strong></th>
    </tr>
    <tr>
      <td>registry</td>
      <td>RegistryServiceImpl</td>
    </tr>
    <tr>
      <td>ejb</td>
      <td>EJBServiceImpl</td>
    </tr>
    <tr>
      <td>web</td>
      <td>CatalinaJWebContainerServiceImpl /
      JettyJWebContainerServiceImpl</td>
    </tr>
    <tr>
      <td>ear</td>
      <td>EarServiceImpl</td>
    </tr>
    <tr>
      <td>dbm</td>
      <td>DataBaseServiceImpl</td>
    </tr>
    <tr>
      <td>jms</td>
      <td>JmsServiceImpl</td>
    </tr>
    <tr>
      <td>jmx</td>
      <td>JmxServiceImpl</td>
    </tr>
    <tr>
      <td>jtm</td>
      <td>TransactionServiceImpl</td>
    </tr>
    <tr>
      <td>mail</td>
      <td>MailServiceImpl</td>
    </tr>
    <tr>
      <td>resource</td>
      <td>ResourceServiceImpl</td>
    </tr>
    <tr>
      <td>security</td>
      <td>JonasSecurityServiceImpl</td>
    </tr>
    <tr>
      <td style="vertical-align: top;">ws<br>
      </td>
      <td style="vertical-align: top;">AxisWSService<br>
      </td>
    </tr>
  </tbody>
</table>

<p>If all of these services are required, they will be launched in the
following order: <i>registry</i>, <i>jmx</i>, <i>security</i>,
<i>jtm</i>,<i>dbm</i>,<i>mail</i>,<i>jms</i>,<i>resource</i>,<i>ejb</i>, ws,
<i>web</i>, <i>ear</i>. <br>
<i>jmx</i>, <i>security</i>,<i> dbm</i>, <i>mail</i>, <i>resource</i> are
optional when you are using  service <i>ejb</i>.</p>

<p><i>registry</i> must be launched first. <br>
(Note that for reasons of compatability with previous versions of JOnAS, if
<i>registry</i> is unintentionally not set as the first service to launch,
JOnAS will automatically launch the <i>registry</i> service.)</p>
<br>
Note that <i>dbm</i>, <i>jms</i>, <i>resource</i>, and <i>ejb</i> depend on
<i>jtm</i>. <br>
Note that <i>ear</i> depends on <i>ejb</i> and <i>web</i> (that provide the
ejb and web containers), thus these services must be launched before the
<i>ear</i> service.<br>
Note that <span style="font-style: italic;">ear</span> and <span
style="font-style: italic;">web</span> depends on <span
style="font-style: italic;">ws,</span> thus the <span
style="font-style: italic;">ws</span> service must be launched before the
<i>ear</i> and <span style="font-style: italic;">web</span> service.<br>
<br>
It is possible to launch a stand-alone Transaction Manager with only the
<i>registry</i> and <i>jtm</i> services.

<p>A <code>jonas.properties</code> file looks like the following:</p>
<pre>  .....<br>  .....<br><br>  jonas.services                registry,jmx,security,jtm,dbm,mail,jms,ejb,resource,serv1 <br><br>  jonas.service.registry.class  org.objectweb.jonas.registry.RegistryServiceImpl<br>  jonas.service.registry.mode        automatic<br><br>  jonas.service.dbm.class       org.objectweb.jonas.dbm.DataBaseServiceImpl<br>  jonas.service.dbm.datasources Oracle1<br><br>  jonas.service.ejb.class       org.objectweb.jonas.container.EJBServiceImpl<br>  jonas.service.ejb.descriptors ejb-jar.jar<br>  jonas.service.ejb.parsingwithvalidation  true<br>  jonas.service.ejb.mdbthreadpoolsize      10<br><br>  jonas.service.web.class        org.objectweb.jonas.web.catalina.CatalinaJWebContainerServiceImpl<br>  jonas.service.web.descriptors        war.war<br>  jonas.service.web.parsingwithvalidation  true<br><br>  jonas.service.ear.class       org.objectweb.jonas.ear.EarServiceImpl<br>  jonas.service.ear.descriptors j2ee-application.ear<br>  jonas.service.ear.parsingwithvalidation  true<br><br>  jonas.service.jms.class       org.objectweb.jonas.jms.JmsServiceImpl<br>  jonas.service.jms.mom         org.objectweb.jonas_jms.JmsAdminForJoram<br>  jonas.service.jms.collocated  true<br>  jonas.service.jms.url         joram://localhost:16010<br><br>  jonas.service.jmx.class       org.objectweb.jonas.jmx.JmxServiceImpl<br><br>  jonas.service.jtm.class       org.objectweb.jonas.jtm.TransactionServiceImpl<br>  jonas.service.jtm.remote      false<br>  jonas.service.jtm.timeout     60<br><br>  jonas.service.mail.class        org.objectweb.jonas.mail.MailServiceImpl<br>  jonas.service.mail.factories  MailSession1<br><br>  jonas.service.security.class  org.objectweb.jonas.security.JonasSecurityServiceImpl<br><br>  jonas.service.resource.class        org.objectweb.jonas.resource.ResourceServiceImpl<br>  jonas.service.resource.resources         MyRA<br><br>  jonas.service.serv1.class     a.b.MyService<br>  jonas.service.serv1.p1        John<br>    </pre>

<h3>The ServiceException</h3>

<p>The <code>org.objectweb.jonas.service.ServiceException</code> exception is
defined for Services. Its type is <code>java.lang.RuntimeException.</code>
and it can encapsulate any <code>java.lang.Throwable</code>.</p>

<h3>The ServiceManager</h3>

<p>The <code>org.objectweb.jonas.service.ServiceManager</code> class is
responsible for creating, initializing, and launching the services. It can
also return a service from its name and list all the services.</p>
</body>
</html>
