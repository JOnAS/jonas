<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html>
<head>
  <!--
  - JOnAS: Java(TM) Open Application Server
  - Copyright (C) 2004 Bull S.A.
  - Contact: jonas-team@objectweb.org

  - This work is licensed under the Creative Commons
  - Attribution-ShareAlike License. To view a copy of this license,
  - visit http://creativecommons.org/licenses/by-sa/2.0/deed.en or
  - send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford,
  - California 94305, USA.

  - Author: Adriana Danes
  -->
  <!--
  - $Id$
  -->
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <title>Working with Management Beans</title>
  <link rel="stylesheet" href="common.css">
</head>

<body lang="en">
<h1><a name="MBeans"></a>Working with Management Beans</h1>

<p>The content of this guide is the following:</p>
<ol>
  <li><a href="#MBeans-Target">Target Audience and Rationale</a></li>
  <li><a href="#MBeans-Intro">About JOnAS MBeans and their use in
    JonaSAdmin</a></li>
  <li><a href="#MBeans-Usage">Using JOnAS MBeans in a Management
    Application</a></li>
  <li><a href="#MBeans-Register">Registering User MBeans</a></li>
</ol>

<h2><a name="MBeans-Target"></a>Target Audience and Rationale</h2>

<p>This chapter is intended for advanced JOnAS users who are interested in
understanding management facilities that JOnAS provides, and possibly
extending these facilities for their application needs.</p>

<p>JOnAS management facilities are based on Management Beans (MBeans)
compliant to <a href="http://java.sun.com/products/JavaManagement/"> JMX
Specification</a>. Moreover, JOnAS implements JSR 77 specification, which
defines the management model for J2EE platforms.</p>

<h2><a name="MBeans-Intro"></a>About JOnAS MBeans and their use in
JonaSAdmin</h2>

<p>MBeans provide access to management functions such as configuration,
deployment/undeployment, monitoring of resources and application modules.</p>

<p>MBeans are created not only by the different JOnAS services, but also by the
components integrated in JOnAS (Web server Tomcat or Jetty, JORAM MOM, etc.).
They are registered in the current MBean Server, which is started by each
JOnAS server instance. Remote access to the MBean Server is facilitated by
JMX remote connectors compliant to the JSR 160 specification. See more
information about connectors <a
href="./howto/JSR160_support.html#JSR160_support">here</a>.</p>

<p>JonasAdmin application implements the management functions listed above 
using the different MBeans registered in the MBeanServer of the JOnAS
instance currently being managed. This is usually the server on which
JonasAdmin is deployed, but it may be another server running in the same
management domain.</p>

<p>JonasAdmin also presents, in a structured way, all the registered MBeans,
their attributes and operations. In the future, JonasAdmin will probably be 
extended to allow setting attributes and invoking operations.</p>

<h2><a name="MBeans-Usage"></a>Using JOnAS MBeans in a Management
Application</h2>

<p>In order to invoke a management operation on a MBean, the caller must 
access to the MBean server.</p>

<h3><a name="Local-MBeanServer"></a>Local MBean Server</h3>

<p>When the caller is located in the same JVM as the MBean Server, it can use
<code>javax.management.MBeanServerFactory</code> class to obtain a reference
to the MBean Server:</p>
<pre>                List mbeanServers = MBeanServerFactory.findMBeanServer(null);
                if (mbeanServers != null &amp;&amp; mbeanServers.size() &gt; 0) {
                        return (MBeanServer) mbeanServers.get(0);
                }
        </pre>

<p></p>

<h3><a name="Remote-MBeanServer"></a>Using Remote Connectors</h3>

<p>When the caller is remote, it can use a JMX remote connector to establish
a connection with the MBean Server and obtain a
<code>javax.management.MBeanServerConnection</code> object.</p>

<p>Suppose that the connector server has the following address:
<code>service:jmx:jrmp://host/jndi/jrmp://host:1099/jrmpconnector_jonas</code>,
which is the default for a JOnAS server called <code>jonas</code>.</p>
<pre>                JMXServiceURL connURL = new JMXServiceURL("service:jmx:jrmp://host/jndi/jrmp://host:1099/jrmpconnector_jonas");
                JMXConnector connector = JMXConnectorFactory.newJMXConnector(connURL, null);
                connector.connect(null);
                MBeanServerConnection conn = connector.getMBeanServerConnection();
                return conn;
        </pre>

<p></p>

<h3><a name="MEJB"></a>Using a Management EJB</h3>

<p>A remote caller can also use the MEJB provided by the JOnAS distribution.
A Management EJB implementation, compliant to the JSR 77, is packed in the
<code>mejb.ear</code> installed in the
<code>JONAS_ROOT/ejbjars/autoload</code> directory. Thus, the MEJB is
automatically deployed at JOnAS start-up. Its <code>Home</code> is registered
in JNDI under the name <code>ejb/mgmt/MEJB</code>. JOnAS distribution also
contains an example using the MEJB in a J2EE application, the
<code>j2eemanagement</code> sample.</p>

<p></p>

<h3><a name="MEJB"></a>Using the Management Web Service Endpoint</h3>

<p>
A remote caller can use the Management Web Service endpoint packaged
as a part of the <code>mejb.ear</code> installed in the
<code>JONAS_ROOT/ejbjars/autoload</code> directory. Thus, the management
endpoint is automatically deployed at JOnAS start-up. Check 
<code> http://&lt;hostname&gt;:&lt;port&gt;/mejb/ManagementEndpoint/ManagementEndpoint?wsdl
</code> for the WSDL file on a running JOnAS instance. The endpoint allows light-weight
clients to query JOnAS MBeans from virtually any platform by leveraging the
platform-independant nature of Web Services. 
</p>

<h2><a name="MBeans-Register"></a>Registering User MBeans</h2>

<p>Application MBeans can be created and registered in the MBean server by
calling <code>registerMBean</code> method on the <code>MBeanServer</code>
object or <code>createMBean</code> method on the
<code>MBeanServerConnection</code> object. Also, MBeans can be loaded using
the m-let service, a dynamic loading mechanism provided by the JMX
implementation. This mechanism allows loading MBeans from a remote URL. The
information on the MBeans to load is provided in a m-let text file. Refer to
JMX implementation documentation for details concerning this file. In addition,
the howto document <a href="./howto/JonasMBeansHowTo.html#JOnASandJMX">
JOnAS and JMX, registering and manipulating MBeans</a>
illustrates the use of this m-let mechanism. In order
to make an m-let text file accessible to JOnAS applications, it can be installed 
in the <code>JONAS_ROOT/conf</code> directory.</p>
</body>
</html>
