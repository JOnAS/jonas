<!--
- JOnAS: Java(TM) Open Application Server
- Copyright (C) 2003-2006 Bull S.A.
- Contact: jonas-team@objectweb.org

- This work is licensed under the Creative Commons
- Attribution-ShareAlike License. To view a copy of this license,
- visit http://creativecommons.org/licenses/by-sa/2.0/deed.en or
- send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford,
- California 94305, USA.

- Author: Michael Giroux
-->
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html">
  <title>Execute JOnAS as WIN32 Service</title>
  <link rel="stylesheet" href="common.css">
</head>

<body lang="en">
<h1><a name="Win32Service"></a>Howto: Execute JOnAS as a WIN32 Service</h1>

<p>This document describes the procedures necessary to run JOnAS as a system
service on Microsoft Windows platforms. This applies starting from JOnAS
3.3.2.</p>

<h2>Instructions</h2>

<p>This procedure uses ANT targets that are introduced in JOnAS 3.3.2. The
procedure also uses the Java Service Wrapper open source project which must
be downloaded and installed separately.</p>

<h3>Download and Install Java Service Wrapper</h3>
<ol>
  <li>Download <a href="http://wrapper.tanukisoftware.org/doc/english/index.html">Java
    Service Wrapper</a> version 3.0.5 or later, and unzip the package to a
    directory in the local filesystem.</li>
  <li>Set <code>WRAPPER_HOME</code> environment variable to the root
    directory for Java Service Wrapper.
    <p>For example, if the package for Wrapper version 3.0.5 is unzipped into
    c:\jsw, then SET WRAPPER_HOME=c:\jsw\wrapper_win32_3.0.5</p>
  </li>
</ol>

<h3><a name="Win32Service-create"></a>create_win32service</h3>

<p>Before JOnAS can be run as a WIN32 service, it is necessary to create a
Java Service Wrapper configuration file. Prior to executing the steps in this
section, it is necessary to create a JONAS_BASE directory as described in the
JOnAS <a href="Config.html#Config">Configuration Guide</a>.</p>
<ol>
  <li>Verify that JAVA_HOME is set as a system environment variable.</li>
  <li>Verify that JONAS_BASE and WRAPPER_HOME environment variables are
  set.</li>
  <li>Set %JONAS_ROOT% as the current directory.</li>
  <li>Set environment variables required for JOnAS execution
  (see list below).</li>
  <li>Execute <tt><kbd>ant [-Djonas.name=&lt;server_name&gt;]
    create_win32service</kbd></tt>.</li>
</ol>

<blockquote style="margin-left: 2em; margin-right: 2em;">
  <p>The <tt>-Djonas.name=&lt;server_name&gt;</tt> parameter is optional. If
  not specified, the default server name is 'jonas'.</p>
</blockquote>

<h4>Environment Variables</h4>
<p>
Prior to executing <tt>create_win32service</tt> it is necessary to 
set environment variables as required for JOnAS execution.  The values
of the environment variables are saved in the wrapper_ext.conf file
as wrapper properties.
</p>
<ul>
<li>CLASSPATH (*)</li>
<li>JAVA_OPTS</li>
<li>JONAS_OPTS</li>
<li>TOMCAT_OPTS</li>
<li>JETTY_OPTS</li>
</ul>
<p style="background-color: #f0f0f0;">
*&nbsp;The config_env.bat file is used to configure JDBC drivers
in the CLASSPATH.  Any changes to config_env.bat will affect the
CLASSPATH environment variable, and therefore requires that
the wrapper configuration be updated by running
<tt>create_win32service</tt>.
</p>
<p>The wrapper configuration must be updated whenever any of these
environment variables are modified. 
Refer to the <a href="#Win32Service-Modify">Modify JOnAS
Configuration</a> section for more information.</p>


<h3><a name="Win32Service-install"></a>install_win32service</h3>

<p>After the %JONAS_BASE% directory has been updated for use with Java
Service Wrapper, JOnAS can be installed as a WIN32 service using the
<tt>install_win32service</tt> ant target. Prior to installing the
configuration as a WIN32 service, the configuration can be tested as a
standard console application. Refer to the 
<a href="#Win32Service-Testing">Testing configuration</a> section for more
information. The following steps will install the service.</p>
<ol>
  <li>Verify that JONAS_BASE and WRAPPER_HOME environment variables are
  set.</li>
  <li>Set %JONAS_ROOT% as the current directory.</li>
  <li>Execute <kbd>ant install_win32service</kbd>.  As an alternative, 
      the service may be installed from a DOS command window using the 
      <kbd>jonas ntservice install</kbd> command.</li>
</ol>

<p>By default, the service is configured to start automatically each time
Windows starts. If the administrator would prefer to start the service
manually, modify the <tt>wrapper.ntservice.starttype</tt> parameter in the
%JONAS_BASE%\conf\wrapper.conf file. Set the value as described in the
comments found in the wrapper.conf file.</p>

<h3><a name="Win32Service-uninstall"></a>uninstall_win32service</h3>

<p>When it is no longer desirable to run JOnAS as a Windows service, the
service can be uninstalled using the <tt>uninstall_win32service</tt> ant
target.</p>
<ol>
  <li>Verify that JONAS_BASE and WRAPPER_HOME environment variables are
  set.</li>
  <li>Set %JONAS_ROOT% as the current directory.</li>
  <li>Verify that the service has been stopped.</li>
  <li>Execute <kbd>ant uninstall_win32service</kbd>.  As an alternative, 
      the service may be uninstalled from a DOS command window using the 
      <kbd>jonas ntservice uninstall</kbd> command.</li>
</ol>

<h3>Start JOnAS Service</h3>

<p>To start the JOnAS service, open the Service Control Manager (Control
Panel Services) window, select the JOnAS service and start the service.
As an alternative, the service may be started from a DOS command window
using the <kbd>jonas ntservice start</kbd> command.</p>

<p>By default, JOnAS will be started automatically each time Windows is
started. After installing the service, it can be started manually to avoid
the need to reboot Windows.</p>

<p style="margin-left: 2em; margin-right: 2em;"><b>NOTE:</b> 
Any environment variables referenced within either of the 
wrapper.conf and wrapper_ext.conf files must be defined as system
environment variables.
</p>

<h3>Stop JOnAS Service</h3>

<p>To stop the JOnAS service, open the Service Control Manager window, select
the JOnAS service and stop the service.  As an alternative, the service may be stopped
from a DOS command window using the 
<kbd>jonas ntservice stop</kbd> command.</p>

<h3>Status of JOnAS Service</h3>

<p>The status of the JOnAS service may be obtained from a DOS command window using the 
<kbd>jonas ntservice status</kbd> command.</p>

<h2><a name="Win32Service-Modify"></a>Files Managed by
create_win32service</h2>

<p>The <tt>create_win32service</tt> ant target copies files from
the Java Service Wrapper installation directory and generates a configuration
file in the %JONAS_BASE% directory. The following files are managed by the
<tt>create_win32service</tt> ant target.</p>
<ul>
  <li>lib\wrapper.jar</li>
  <li>lib\wrapper.dll</li>
</ul>
<ul>
  <li>conf\wrapper.conf (*)</li>
  <li>conf\wrapper_ext.conf (**)</li>
</ul>

<p>*&nbsp;&nbsp;&nbsp;<tt>wrapper.conf</tt> contains Java Service Wrapper
configuration properties.  This file is copied to the conf directory
by the CREATE_JONASBASE command.  Changes made to this file are
not affected by subsequent execution of the <tt>create_win32service</tt> target.</p>
<p>**&nbsp;<tt>wrapper_ext.conf</tt> contains Java Service Wrapper
configuration properties specific to the JOnAS service. This file is
generated by the <tt>create_win32service</tt> ant target. Any changes made to
this file will be lost when the <tt>create_win32service</tt> target is
executed.</p>

<h2><a name="Win32Service-Modify1"></a>Modify JOnAS Configuration</h2>

<p>In addition to the files located in the conf directory, JOnAS
configuration is affected by the contents of
%JONAS_ROOT%\bin\nt\config_env.bat, and by the following environment variables,
CLASSPATH, JAVA_OPTS, JONAS_OPTS, TOMCAT_OPTS, JETTY_OPTS.
If changes are made to config_env.bat, or any of these
environment variables, it is necessary to update the
Java Service Wrapper configuration files.</p>
<p>
JOnAS memory usage is controlled by the -Xms and -Xms JVM arguments. Prior
to running <tt>create_win32service</tt>, set the JAVA_OPTS environment variable with
the desired values for -Xms and -Xmx.  The <tt>create_win32service</tt> target will
generate these values as wrapper.java.additional parameters in the
wrapper_ext.conf file.
</p>
<h3>Update Wrapper Configuration</h3>
<ol>
  <li>Stop the JOnAS service using the Windows Service Control Manager,
  or the <code>jonas ntservice stop</code> command line.</li>
  <li>Make changes to <code>config_env.bat</code>, and the 
  <code>CLASSPATH, JAVA_OPTS, JONAS_OPTS, TOMCAT_OPTS, JETTY_OPTS</code>
  environment variables as needed.</li>
  <li>Update the Java Service Wrapper configuration. Refer to the <a
    href="#Win32Service-create">create_win32service</a> section for
  details.</li>
  <li>Test the updated configuration. Refer to the <a
    href="#Win32Service-Testing">Testing configuration</a> section for more
    information.</li>
  <li>Restart the JOnAS service using the Windows Service Control Manager,
  or the <kbd>jonas ntservice start </kbd> command line. </li>
</ol>

<p>
Note: Changes to the JOnAS configuration files located in the
%JONAS_BASE%\conf directory do not affect the contents of the 
wrapper_ext.conf file.  When making changes to the files located in the
conf directroy, it is only necessary to stop the service and restart
the service for the changes to take effect.
</p>


<h2><a name="Win32Service-Testing"></a>Testing Configuration</h2>

<p>After the Java Service Wrapper configuration files have been generated, it
is possible to test the configuration in a console window before installing
the configuration as a WIN32 service.</p>
<ol>
  <li>Verify that JONAS_BASE environment variable is set.</li>
  <li>Execute <kbd>jonas ntservice console</kbd>.</li>
</ol>

<p>The Java Service Wrapper will start as a console application and load
JOnAS using the configuration generated by the <tt>create_win32service</tt>
ant target.</p>

<blockquote style="margin-left: 2em; margin-right: 2em;">Note that this test procedure is using environment variables that
are set for the current user.  Verify that any environment
variables used in the wrapper.conf and/or wrapper_ext.conf files
are also set as system environment variables.
</blockquote>

<p>Enter <kbd>CTRL-C</kbd> to terminate JOnAS. After pressing Ctrl-C, the
Java Service Wrapper displays the following messages to the execution report,
and/or log file.</p>
<pre><span style="font-size: 10pt">wrapper  | CTRL-C trapped.  Shutting down.
jvm 1    | 2003-12-02 15:25:20,578 : AbsJWebContainerServiceImpl.unRegisterWar 
                                   : War /G:/w32svc/webapps/autoload/ctxroot.war no longer available
jvm 1    | Stopping service Tomcat-JOnAS.
wrapper  | JVM exited unexpectedly while stopping the application.
wrapper  | &lt;-- Wrapper Stopped.</span></pre>
</body>
</html>
