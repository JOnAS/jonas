<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"
                      "http://www.w3.org/TR/REC-html40/loose.dtd">
<!--
- JOnAS: Java(TM) Open Application Server
- Copyright (C) 2003 Bull S.A.
- Contact: jonas-team@objectweb.org

- This work is licensed under the Creative Commons
- Attribution-ShareAlike License. To view a copy of this license,
- visit http://creativecommons.org/licenses/by-sa/2.0/deed.en or
- send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford,
- California 94305, USA.

- Author: Frederic Maistre

-->
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html">
  <title>Distributed Message Beans in JOnAS 4.1</title>
  <link rel="stylesheet" href="common.css">
</head>

<body>
<h1>Howto: Distributed Message Beans in JOnAS 4.x</h1>

<p>JOnAS release 4.1 dramatically simplifies the use of a distributed JORAM
platform from within JOnAS servers. For example, such a configuration allows
a bean hosted by JOnAS instance "A" to send messages on a JORAM queue, to
which a MDB hosted by JOnAS instance "B" listens.</p>

<p>This advancement is due to the following:</p>
<ul>
  <li>JORAM Resource Adapter allows a much more refined configuration than
    the JMS service did.</li>
  <li>JORAM provides a distributed JNDI server which allows JOnAS instances
    to share information.</li>
</ul>

<p>Before going through this chapter, it is highly recommended that the <a
href="../Config.html#Config-JORAM-RA">JORAM Resource Adapter</a>
configuration guide be reviewed.</p>

<h2>Scenario and general architecture</h2>

<p>The following scenario and general settings are proposed:</p>
<ul>
  <li>Two instances of JOnAS are run (JOnAS "A" and JOnAS "B"). JOnAS A hosts
    a simple bean providing a method for sending a message on a JORAM queue.
    JOnAS B hosts a message-driven bean listening on the same JORAM
  queue.</li>
  <li>Each JOnAS instance has a dedicated collocated JORAM server: server
    "s0" for JOnAS A, "s1" for JOnAS B. Those two servers are aware of each
    other.</li>
  <li>The queue is hosted by JORAM server s1.</li>
  <li>An additional JNDI service is provided by the JORAM servers that will
    be used for storing the shared information (basically, the queue's naming
    reference).</li>
</ul>

<h2>Common configuration</h2>

<p>The JORAM servers are part of the same JORAM platform described by the
following <strong><code>a3servers.xml</code></strong> configuration file:</p>
<pre>&lt;?xml version="1.0"?&gt;
&lt;config&gt;
    &lt;domain name="D1"/&gt;
    &lt;server id="0" name="S0" hostname="hostA"&gt;
      &lt;network domain="D1" port="16301"/&gt;
      &lt;service class="org.objectweb.joram.mom.proxies.ConnectionManager"
               args="root root"/&gt;
      &lt;service class="org.objectweb.joram.mom.proxies.tcp.TcpProxyService"
               args="16010"/&gt;
      &lt;service class="fr.dyade.aaa.jndi2.distributed.DistributedJndiServer"
               args="16400 0"/&gt;
    &lt;/server&gt;
    &lt;server id="1" name="S1" hostname="hostB"&gt;
      &lt;network domain="D1" port="16301"/&gt;
      &lt;service class="org.objectweb.joram.mom.proxies.ConnectionManager"
               args="root root"/&gt;
      &lt;service class="org.objectweb.joram.mom.proxies.tcp.TcpProxyService"
               args="16010"/&gt;
      &lt;service class="fr.dyade.aaa.jndi2.distributed.DistributedJndiServer"
               args="16400 0 1"/&gt;

    &lt;/server&gt;
&lt;/config&gt;</pre>

<p>This configuration describes a platform made up of two servers, "s0" and
"s1", hosted by machines "hostA" and "hostB", listening on ports 16010,
providing a distributed JNDI service (more info on JORAM's JNDI may be found
<a
href="http://joram.objectweb.org/current/doc/joram4_0_JNDI.pdf">here</a>).</p>

<p>Each JOnAS server must hold a copy of this file in its <code>conf/</code>
directory. In its <code>jonas.properties</code> file, each must declare the
<code>joram_for_jonas_ra.rar</code> as a resource to be deployed (and each
should remove <em>jms</em> from its list of services).</p>

<h2>Specific configuration</h2>

<p><strong>JOnAS A</strong> embeds JORAM server <strong>s0</strong>. The
<code>jonas-ra.xml</code> descriptor packaged in the
<code>joram_for_jonas_ra.rar</code> archive file must provide the following
information:</p>
<pre>&lt;jonas-config-property&gt;
    &lt;jonas-config-property-name&gt;HostName&lt;/jonas-config-property-name&gt;
    &lt;jonas-config-property-value&gt;hostA&lt;/jonas-config-property-value&gt;
&lt;/jonas-config-property&gt;  </pre>

<p>The other default settings do not need to be changed.</p>

<p><strong>JOnAS B</strong> embedds JORAM server <strong>s1</strong>. The
<code>jonas-ra.xml</code> descriptor packaged in the
<code>joram_for_jonas_ra.rar</code> archive file must provide the following
properties values:</p>
<pre>&lt;jonas-config-property&gt;
    &lt;jonas-config-property-name&gt;ServerId&lt;/jonas-config-property-name&gt;
    &lt;jonas-config-property-value&gt;1&lt;/jonas-config-property-value&gt;
&lt;/jonas-config-property&gt;
&lt;jonas-config-property&gt;
    &lt;jonas-config-property-name&gt;ServerName&lt;/jonas-config-property-name&gt;
    &lt;jonas-config-property-value&gt;s1&lt;/jonas-config-property-value&gt;
&lt;/jonas-config-property&gt;
&lt;jonas-config-property&gt;
    &lt;jonas-config-property-name&gt;HostName&lt;/jonas-config-property-name&gt;
    &lt;jonas-config-property-value&gt;hostB&lt;/jonas-config-property-value&gt;
&lt;/jonas-config-property&gt;</pre>

<p>The other default settings do not need to be changed.</p>

<p>The <strong>shared queue</strong> will be hosted by JORAM server s1. It
must then be declared in the JOnAS B's <code>joramAdmin.xml</code> file as
follows:</p>
<pre>
        &lt;Queue name="sharedQueue"&gt;
          &lt;freeReader/&gt;
          &lt;freeWriter/&gt;
          &lt;jndi name="scn:comp/sharedQueue"/&gt;
        &lt;/Queue&gt;
</pre>

<p>The <code>scn:comp/</code> prefix is a standard way to specify which JNDI
provider should be used. In this case, the shared queue will be bound to
JORAM's distributed JNDI server, and may be retrieved from both JOnAS A and
JOnAS B. To provide this mechanism, both JOnAS servers must provide access to
a standard <code>jndi.properties</code> file. For JOnAS A, the file looks as
follows, and should be placed in its <code>conf/</code> directory:</p>
<pre>java.naming.factory.url.pkgs    org.objectweb.jonas.naming:fr.dyade.aaa.jndi2
scn.naming.factory.host         hostA
scn.naming.factory.port         16400</pre>

<p>For JOnAS B, the file looks as follows, and should be placed in the right
<code>conf/</code> directory:</p>
<pre>java.naming.factory.url.pkgs    org.objectweb.jonas.naming:fr.dyade.aaa.jndi2
scn.naming.factory.host         hostB
scn.naming.factory.port         16400</pre>

<h2>And now, the beans!</h2>

<p>The <strong>simple bean</strong> on JOnAS A needs to connect to its local
JORAM server and access the remote queue. The following is an example of
consistent resource definitions in the deployment descriptors:</p>

<p>Standard deployment descriptor:</p>
<pre>&lt;resource-ref&gt;
  &lt;res-ref-name&gt;jms/factory&lt;/res-ref-name&gt;
  &lt;res-type&gt;javax.jms.ConnectionFactory&lt;/res-type&gt;
  &lt;res-auth&gt;Container&lt;/res-auth&gt;
&lt;/resource-ref&gt;
&lt;resource-env-ref&gt;
  &lt;resource-env-ref-name&gt;jms/sharedQueue&lt;/resource-env-ref-name&gt;
  &lt;resource-env-ref-type&gt;javax.jms.Queue&lt;/resource-env-ref-type&gt;
&lt;/resource-env-ref&gt;</pre>

<p>Specific deployment descriptor:</p>
<pre>&lt;jonas-resource&gt;
  &lt;res-ref-name&gt;jms/factory&lt;/res-ref-name&gt;
  &lt;jndi-name&gt;CF&lt;/jndi-name&gt;
&lt;/jonas-resource&gt;
&lt;jonas-resource-env&gt;
  &lt;resource-env-ref-name&gt;jms/sharedQueue&lt;/resource-env-ref-name&gt;
  &lt;jndi-name&gt;scn:comp/sharedQueue&lt;/jndi-name&gt;
&lt;/jonas-resource-env&gt;</pre>

<p>The ConnectionFactory is retrieved from the local JNDI registry of the
bean. However, the Queue is retrieved from the distributed JORAM JNDI server,
because its name starts with the <em>scn:comp/</em> prefix. It is the same
queue to which the <strong>message-driven bean</strong> on JOnAS B listens.
For doing so, its activation properties should be set as follows:</p>
<pre>&lt;activation-config&gt;
   &lt;activation-config-property&gt;
      &lt;activation-config-property-name&gt;destination&lt;/activation-config-property-name&gt;
      &lt;activation-config-property-value&gt;scn:comp/sharedQueue&lt;/activation-config-property-value&gt;
   &lt;/activation-config-property&gt;
   &lt;activation-config-property&gt;
      &lt;activation-config-property-name&gt;destinationType&lt;/activation-config-property-name&gt;
      &lt;activation-config-property-value&gt;javax.jms.Queue&lt;/activation-config-property-value&gt;
   &lt;/activation-config-property&gt;
&lt;/activation-config&gt;</pre>
</body>
</html>
