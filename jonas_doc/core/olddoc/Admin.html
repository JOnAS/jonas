<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"
                      "http://www.w3.org/TR/REC-html40/loose.dtd">
<!--
- JOnAS: Java(TM) Open Application Server
- Copyright (C) 2003-2004 Bull S.A.
- Contact: jonas-team@objectweb.org

- This work is licensed under the Creative Commons
- Attribution-ShareAlike License. To view a copy of this license,
- visit http://creativecommons.org/licenses/by-sa/2.0/deed.en or
- send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford,
- California 94305, USA.

- Author: Adriana DANES

- $Id$
-->
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <link rel="stylesheet" href="common.css">
  <title>Administration Guide</title>
</head>

<body lang="en">
<h1 title="Administration Guide"><a name="Admin"></a>Administration Guide</h1>

<p>The target audience for this guide is the JOnAS server administrator.</p>

<p>JOnAS provides the following two tools for performing some administration
tasks on a running JOnAS Server:</p>
<ul>
  <li><a href="#Admin-jonas admin"><code><b>jonas admin</b></code></a>, a
    command line tool</li>
  <li><a href="#Admin-JonasAdmin"><code><b>JonasAdmin</b></code></a>, a
    graphical tool based on the Struts framework and the JMX technology
    <ul>
      <li><a href="#Admin-JonasAdminInstall">Installing JonasAdmin</a></li>
      <li><a href="#Admin-JonasAdminUse">Using JonasAdmin</a></li>
    </ul>
  </li>
</ul>
<p>These tools also allow administration of several JOnAS Servers. Each JOnAS
Server is identified by a name, which is the value of the <code>-n</code>
option used in the <code>jonas start</code> command (the default name is
<code>jonas</code>).</p>

<!--
Additionally, since the <a href="http://mx4j.sourceforge.net/">MX4J</a> open
source JMX implementation is integrated into JOnAS, the server administrator
can use the <a href="http://sourceforge.net/projects/mc4j/">MC4J</a> generic
administration console.</p>
-->

Begining with JOnAS 4, we also provide the <a href="#Admin-MEJB"><strong>
J2EE Management EJB component (MEJB)</strong></a>, as specified by the
J2EE Management Specification which defines the J2EE Management Model.

<h2><a name="Admin-jonas admin"></a>jonas admin</h2>

<p><em>jonas admin</em> is described in the <a href="command_guide.html">JOnAS
Commands</a> chapter.</p>

<h2><a name="Admin-JonasAdmin"></a>JonasAdmin</h2>

<p>This chapter provides information about installing, configuring, and using
the <em>JonasAdmin</em> administration console.</p>

<p>JonasAdmin is the new administration tool for JOnAS and replaces the
deprecated <em>Jadmin</em> tool.</p>

<p>JonasAdmin was developed using the <a
href="http://jakarta.apache.org/struts/">Struts</a> framework; it uses
standard technologies such as Java Servlets and JavaServer Pages. JonasAdmin
is more ergonomic than Jadmin and provides integrated administration
facilities for a Tomcat server running embedded in JOnAS.</p>

<h3><a name="Admin-JonasAdminInstall"></a>Installing JonasAdmin</h3>

<p>Designed as a web application, JonasAdmin is packed in a WAR and installed
under the <code>JONAS_ROOT/webapps/autoload/</code> directory. This WAR can
be installed in <code>JONAS_BASE/webapps/autoload</code> if a JONAS_BASE
variable has been defined in the environment. When installed in the
<code>autoload</code> directory, JonasAdmin is deployed when starting the
JOnAS server, thus the administration console is automatically accessible.</p>

<p>As with any web application, JonasAdmin requires a servlet server to be
installed. Additionally, the JOnAS server running JonasAdmin must have the
web container service present in the list of services defined in the
<code>jonas.properties</code> configuration file.</p>

<p>When accessing JonasAdmin, the administrator must provide identification
and authentication.<br>
The <code>jonas-realm.xml</code> configuration file contains a memory realm
definition named <code>memrlm_1</code>, which is referenced in both
<code>server.xml</code> (for Tomcat) and <code>jetty.xml</code> (for Jetty) configuration files. The
default user name (<strong>jonas</strong>) and
password (<strong>jonas</strong>) corresponding to the <code>admin</code>
role can be modified here.<br>
</p>

<h3><a name="Admin-JonasAdminUse"></a>Using JonasAdmin</h3>

<p>Once started, JonasAdmin can administer the JOnAS server in which it is
running, as well as other JOnAS servers with which it shares the same
registry. Typically, this is used to administer JOnAS servers running without
the WEB container service.<br>
Note that the administered JOnAS servers can be running on the same host or
on different hosts. Also, if Tomcat is used as the WEB container service
implementation, it can be administered using JonasAdmin.</p>

<h4><a name="Admin-JonasAdminRun"></a>Running JonasAdmin</h4>

<p>Ensure that the <code>web</code> service is listed in the
<code>jonas.services</code> property in the <code>jonas.properties</code>
configuration file. If you are not using a jonas-tomcat or jonas-jetty
package, depending on the Servlet container being used, the
<code>CATALINA_HOME</code> or the <code>JETTY_HOME</code> environment
variable must have been previously set. Note that when running the Servlet
container on top of Unix, the <code>DISPLAY</code> environment variable must
be set in order to use the JOnAS server monitoring feature of JonasAdmin.</p>

<p>Once JOnAS is launched, JonasAdmin must be loaded if it was not installed
in the <code>autoload</code> directory. The administration console is
accessible at the URL:
<code>http://<i>&lt;hostname&gt;</i>:<i>&lt;portnumber&gt;</i>/jonasAdmin/</code>
using any web browser.</p>

<p><i>&lt;hostname&gt;</i> is the name of the host where the Servlet
container is running and <i>&lt;portnumber&gt;</i> is the http port number
(default is 9000).</p>

<p>After logging in, the left-hand frame in the Welcome page displays the
management tree associated with the JOnAS server running JonasAdmin.
Starting with JOnAS 4.6, the management tree's root is <code>Domain</code>, which
corresponds to the new domain management facilities.
</p>
<p>In the image below, JonasAdmin is running on the master server named <strong>jonas</strong>
within a domain also named <strong>jonas</strong>. It is immediately apparent that this is a
master server, as we have a <code>Deployment</code> sub-tree under the <code>Domain</code>
root node.
</p>

<center>
<img alt="JonasAdmin " src="../../resources/images/domain_new.gif">
</center>

<p>The management tree in this figure allows access to the following main
management facilities:</p>
<ul>
  <li>Domain administration with domain level deployment facilities.</li>
  <li>Current server administration</li>
  <ul>
  	<li>Server monitoring</li>
  	<li>Logging management</li>
  	<li>Communication protocols management</li>
  	<li>Active services presentation and configuration</li>
  	<li>Dynamic deployment at the current server level</li>
  	<li>Resources management</li>
  	<li>Security management</li>
  </ul>
  <li>Joram platform administration</li>
  <li>MBeans browsing</li>
</ul>

<h4>Server management</h4>

<p>Displays general information about the administered JOnAS server,
including the JMX server and the WEB server, and provides the capability of
listing the content of the Registry.</p>

<h5>Server monitoring</h5>

<p>Presents memory usage, a count of the threads created by JOnAS, and other
monitoring information concerning managed services and resources.</p>

<h5>Logging management</h5>

<p>Allows the administrator to configure the JOnAS Logging system.
Additionally, if Tomcat is used as the WEB container service implementation,
it allows creation of new access log valves.</p>

<h5>Communication protocols management</h5>

<p>This management facility relates to the integration of Tomcat management
in JonasAdmin.<br>
It currently presents connectors defined in the Tomcat configuration and
allows for the creation of new HTTP, HTTPS, or AJP connectors.<br>
Note that the <code>Protocols</code> sub-tree is not presented if Jetty is
used as the WEB container service implementation.</p>

<h5>Active services presentation and configuration</h5>

<p>All the active services have a corresponding sub-tree in the
<code>Services</code> tree.</p>

<p>Managing the various container services consists of presenting information
about the components deployed in these containers. New components can be
deployed using the dynamic deployment facilities presented in the next
section.</p>
<p>
Creation of a new context for WEB components to be deployed in the Tomcat server
is deprecated since JOnAS 4.6 (the <code>New web application</code> button is
removed).
</p>

<p>Similarly, the services that allow management of the different types of
resources (DataSources, Resource Adapters, Jms and Mail resources) also
provide information about the resources being deployed. Additionally,
deployed resources (DataSources or MailFactories) can be reconfigured and
their new configuration made persistent by using a <code>Save</code>
button.</p>

<p>The transaction service management allows reconfiguration (possibly
persistent) and presents monitoring information about transactions managed by
JOnAS.</p>

<h5>Dynamic deployment with JonasAdmin</h5>

<p>A very useful management operation is the capability of loading
stand-alone J2EE components (JAR, WAR, RAR packages) or J2EE applications
(EAR packages) in the administered server using the <code>Deployment</code>
sub-tree.
</p>
<p>
The administrator's task is facilitated by the display of the list of
deployable modules, the list of deployed modules, and the capability of
transferring modules from one list to another (which corresponds to
<code>deploy/undeploy</code> operations.
</p>
<p>
The deployable modules are files installed in directories specific to their type. For example, the
deployable JARs are un-deployed JARs installed in
<code>JONAS_BASE/ejbjars/</code> or in a
<code>JONAS_BASE/ejbjars/<em>autoload</em>/</code> directory.
</p>
<p>
The <code>Deployment</code> sub-tree also allows a J2EE package to be uploaded from
the local file system to the corresponding directory of the administered server
(<code>install</code> operation), and the opposite <code>remove</code> operation.
</p>

<h5>Resources management</h5>
The <code>Resources</code> sub-tree provides the capability of loading or
creating new resources managed by the active services. For example, if the
JMS service is running, the JMS sub-tree in <code>Resources</code> presents
the existing JMS destinations (Topics and Queues), and allows the removal of
unused destinations and the creation of new JMS destinations.<br>
Adding or removing resources implies reconfiguration of the corresponding
service. If this new configuration is saved using the <code>Save</code>
button, the JOnAS configuration file is updated. As in the JMS service
example, the removed topics are deleted from the list assigned to the
<code>jonas.service.jms.topics</code> property and the newly created topics
are added to this list.

<h5>Security management</h5>
The <code>Security</code> sub-tree presents existing security realms and
allows the creation of new realms of different types: memory, datasource, and
ldap realms.

<h4>Domain management</h4>
<p>
First recall that domain management functions are accessible only when JonasAdmin
is deployed on a master server. The <code>Domain</code> tree contains only one
<code>Server</code> sub-tree, the currently administered server, which is initially
the server hosting JonasAdmin.</p>
<p>
Domain management principal function is to present the domain topology: list all the servers and clusters
belonging to the domain. It also allows modification of the domain topology by adding new servers and clusters
to the domain, removing servers and moving servers to/from clusters.</p>
<p>
In JOnAS 4.6 there is no cluster support, and the domain topology cannot be modified. The servers presented as belonging
to the domain are those that have been started with their <a href="./configuration_guide.html#N10C40">discovery service</a> enabled.</p>
<p>
In JOnAS 4.7, domain management page also presents servers that are not yet started but are specified as
belonging to the domain in the new configuration file named <strong><code>domain.xml</code></strong>.
Also, a server can be added to the domain when it has been started without having the discovery service enabled.
</p>
<p>
An essential domain management function is that the administrator can switch from the master to any of the
other servers in the domain. Currently, JonasAdmin allows only one category of global domain level management
operation, the <code>deployment</code> operation. Using any other management operation requires switching
to the server to be administered.</p>

<p>Domain level deployment allows for <code>deploying</code> one or more J2EE packages (JARs, WARs, RARs or EARs), which
are installed in the corresponding master directory (ejbjars, webaps, rars or apps), into any running server in the domain.
A <code>deployment</code> operation target may be a server but also a cluster. In this case, the <code>deployment</code>
operation addresses all the running servers in the cluster, including servers in the embedded clusters, if any.
<br>
The <code>deploy</code> operation may have three semantics:
<ul>
<li>deploy only (create container) - which is useful when the package is already installed
on the target server.</li>
<li>distribute only - which means install the package in the target's corresponding directory</li>
<li>distribute and (re)deploy the package, with optionally replacing the current package
with the new one.</li>
</ul>
Note that at domain level deployment the <code>Upload</code> and <code>Remove</code> operations
are only related to the master server itself.
</p>

<h4>Cluster and Domain management in JOnAS 4.8</h4>

<h4>Note regarding persistent reconfiguration facilities</h4>
It is important to note that JOnAS and Tomcat have different approaches to
reconfiguration persistency. In JOnAS, every <code>Save</code> operation is
related to a service or a resource reconfiguration. For example, the
administrator can reconfigure a service and a resource, but choose to save
only the new resource configuration.<br>
In Tomcat, the <code>Save</code> operation is global to all configuration
changes that have been performed. For example, if a new HTTP connector is
reconfigured and a new context created for a web application, both
configuration changes are saved when using the <code>Save</code>
button.


<h3><a name="Admin-MEJB"></a>Management EJB Component</h3>
<p>
The MEJB component exposes the managed objects within the JOnAS
platform as JMX manageable resources. It is packed in an ejb-jar
file installed in the <i>$JONAS_ROOT/ejbjars/autoload</i> directory,
and therefor it is loaded at server start-up.</p>
<p>
The MEJB component is registered under the name
<code>java:comp/env/ejb/MEJB</code>.</p>
<p>
The current implementation allows access only to the manageable
resources within the current server (the server containing the
MEJB's container).</p>
<p>
The JOnAS distribution was enriched with a new example called
<code>j2eemanagement</code>, which shows how the MEJB can be used.
You can find details about this management application in
<i>$JONAS_ROOT/j2eemanagement/README</i> file.</p>

</body>
</html>
