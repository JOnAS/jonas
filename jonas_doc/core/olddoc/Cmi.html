<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html>
<head>
  <!--
  - JOnAS: Java(TM) Open Application Server
  - Copyright (C) 2006 Bull S.A.S.
  - Contact: jonas-team@objectweb.org
  - Copyright (C) 2006 Distributed Systems Lab.
  - Universidad Politecnica de Madrid (Spain)
  - Contact: http://lsd.ls.fi.upm.es/lsd
  - This work is licensed under the Creative Commons
  - Attribution-ShareAlike License. To view a copy of this license,
  - visit http://creativecommons.org/licenses/by-sa/2.0/deed.en or
  - send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford,
  - California 94305, USA.
  -->
  <!--
  - $Id$
  -->
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <title>CMI Cluster Method Invocation</title>
  <link rel="stylesheet" href="common.css">
</head>

<body lang="en">
<h1><strong>CMI (Clustered Method Invocation)</strong></h1>

<h2>Introduction</h2>

<p>CMI is the protocol cluster for JOnAS ensuring:</p>
<ul>
  <li>the jndi high availability through the registry replication and the
    multi-target lookup</li>
  <li>the load-balancing and fail-over at the EJB level (RMI flow) through
    the CMI cluster stub
    <ul>
      <li>for the Home interface of the EJB's SSB, SFSB, EB</li>
      <li>for the Remote interface of the EJB's SSB</li>
    </ul>
  </li>
  <li>the SFSB high availability with the JOnAS HA service</li>
</ul>

<p><img alt="CMI architecture" src="../../resources/images/cmi.gif"></p>

<h2>Getting started with the CMI Protocol</h2>

<p>CMI can be enabled in JOnAS by:</p>
<ul>
  <li>setting the cmi protocol in the $JONAS_BASE/conf/carol.properties
  file</li>
  <li>compiling an application with the CMI protocol</li>
</ul>

<h2>Clustered objects and CMI Registry</h2>

<p>CMI brings its own registry for implementing the jndi replication. Each
CMI registry instance over the cluster contains two areas:</p>
<ul>
  <li>a local area for hosting the local objects that must not be
  replicated</li>
  <li>a distributed area hosting the global objects (cluster objects) that
    must be replicated</li>
</ul>

<p>When an object is registered in the registry, the routing to the local or
global area is done according to:</p>
<ul>
  <li>the type of the object: only the remote objects are replicated</li>
  <li>the presence of the 'CMI class' having the same name as the object
    class with the suffix '_CMI' and inherited from the
    org.objectweb.carol.cmi.Distributor class</li>
  <li>one of the two following methods returning true:
    <ul>
      <li>equivAtBind() if the replication has to take place at the bind()
        time. For example, this could be the case for the EJB Remote Home
        objects.</li>
      <li>equivAtExport() if the replication has to take place at the
        export() time. Typically, this could be the case for the SSB Remote
        objects.</li>
    </ul>
  </li>
</ul>

<p>The entries of the distributed area are lists providing the ability, for
example, to gather several stubs for the the same jndi-name and thus to
return a stubs list.</p>

<h2>JNDI HA</h2>

<h3>Registry Replication</h3>

<p>CMI relies on JGroups group-communication protocol for ensuring the global
registry replication. The parameters are gathered in the:</p>
<ul>
  <li>$JONAS_BASE/conf/carol.properties for specifying the JGroups
    configuration file name and the JGroups group name.</li>
  <li>$JONAS_BASE/conf/jgroups-cmi.xml file for the settings of the jgroups
    protocol stack. By default, the JGroups configuration uses the UDP
    protocol and the multicast IP for broadcasting the registry updates. A
    TCP-based stack can be used in a network environment that does not allow
    the use of multicast IP or when a cluster is distributed over a WAN.</li>
</ul>

<p><img alt="CMI registry" src="../../resources/images/cmi-jndi.gif"></p>

<p>All the members of a cluster share the same JGroups configuration.</p>

<p>If several cluster partitions are required over a single LAN, several
JGroups configurations must be configured with different values for the
following parameters:</p>
<ul>
  <li>JGroups group name</li>
  <li>JGroups multicast address</li>
  <li>JGroups multicast port</li>
</ul>

<p>When a new node appears in the cluster, its registry content is
synchronized automatically.</p>

<p>When a node disappears, JGroups notifies the other's member of the node
leaving and the registry entries related to this node are removed.</p>

<h3>Registry Fail-over</h3>

<p>On the client side, the high availability of the registry is provided by
the capability to set several JOnAS instances in the registry url. At the
lookup time, the client chooses (round-robin algorithm) one of the available
servers to get the home stub. If the server fails, the request is sent to
another server. The CMI url registry is specified in the
$JONAS_BASE/conf/carol.properties file using the following syntax:</p>
<pre>carol.cmi.url=cmi://server1:port1[,server2:port2...] </pre>

<h2>CMI Cluster Stub or Cluster-aware Stub</h2>

<p>Load-balancing and fail-over on the client side are provided through
cluster-aware stubs. These stubs are generated on the fly through ASM and
rely on:</p>
<ul>
  <li>the RMI/JRMP protocol for communicating with the server side,</li>
  <li>the CMI class associated with the EJB for the load-balancing and
    fail-over logic.</li>
</ul>

<p><img alt="CMI cluster stub" src="../../resources/images/cmi-cluster-stub.gif"></p>

<h3><strong>Cluster Map on the Client Side</strong></h3>

<p>The CMI cluster stub handles a cluster map on the client side. The CMI
cluster stub is created:</p>
<ul>
  <li>at lookup time for the SSB, SFSB, EB Home objects if the retrieved
    object is replicated and located in the global registry</li>
  <li>at create time for the SSB Remote objects if the retrieved object is
    replicated and located in the global registry</li>
</ul>

<p>In these two cases, the call gets a stubs list from the global registry
and the CMI cluster stub updates the local cluster map. Afterwards, the local
cluster map can be updated dynamically during the invocation of the business methods calls (through the HA interceptors) when a new view is detected in
the cluster.</p>

<p>If a communication error with a server occurs during a remote call
invocation, the server is removed from the cluster map.</p>

<h3>CMI Class</h3>

<p>CMI classes are generated by GenIC when compiling with the protocol CMI.
They are built from the velocity templates located in
$JONAS_ROOT/templates/genic directory. By default the templates used are:</p>
<ul>
  <li>ClusterHomeDistributor.vm for the home interface of the SSB, SFSB,
  EB</li>
  <li>ClusterRemoteSLSBDistributor.vm for the remote interface of the SSB</li>
</ul>

<p>The templates inherit the org.objectweb.carol.cmi.Distributor class and
contain the following methods:</p>
<ul>
  <li><b>choose(method, params)</b>: this method is called by the CMI cluster stub
    at each remote method call to choose the next stub to use for invoking
    the call. By default, this method implements the weighted round-robin
    algorithm with a local preference, i.e., if there is a collocated server,
    it will be selected in priority. The servers factors are set for each
    JOnAS instance in the $JONAS_BASE/conf/carol.properties file.Below is
    given the code of the choose() method implementing a simple RR algorithm :
    <pre>
    public StubData choose(Method method, Object[] parameters) throws NoServerException {

        Set stubs = getCurrentState();
        if (lastSet != stubs) {
            lastSet = stubs;
            rr.update(stubs);
        }

        return rr.get();
    }
    </pre>
  </li>
  <li><b>onException(method, params, stub, exception)</b>: this method is called by
    the CMI cluster stub when an exception occurs and returns a decision of
    RETRY, RETURN, or THROW for indicating the behavior to the CMI smart
    stub. By default, this method returns a RETRY decision if the exception
    is related to a network error, and otherwise it returns a THROW
   decision. Below is given the code of the mustFailover() method that
   is used by default for analysing the exception and deciding if the fail-over
   must occur :
   <pre>
     protected static boolean mustFailover(Exception ex) {
        if (ex instanceof UnmarshalException) {
            Throwable cause = ((UnmarshalException) ex).getCause();
            if (( cause instanceof EOFException) ||
                    (cause instanceof SocketException)) {
                return true;
            }
        }

        if ((ex instanceof ConnectException) ||
                (ex instanceof ConnectIOException) ||
                (ex instanceof NoSuchObjectException)) {
              return true;
        }
       return false;
     }
    </pre>

  </li>
  <li><b>onReturn(method, params, stub, returnValue)</b>: this method is called by
    the CMI cluster stub after having invoked the call and before returning
    to the application. This call back enables implementation of a
    post-processing on the return value. By default, this method does nothing.
    <p></p>
  </li>
</ul>

<h3>Customizing the Load-balancing and Fail-over Logic</h3>

<p>The user has the ability to customize the load-balancing and fail-over
logic for each EJB by specifying the velocity template to use in the JOnAS-
specific descriptor on deployment of the ejb-jar file. The XML elements
are:</p>
<pre>&lt;cluster-home-distributor&gt;MyHomeDistributor.vm&lt;/cluster-home-distributor&gt;</pre>
<pre>&lt;cluster-remote-distributor&gt;MyRemoteDistributor.vm&lt;/cluster-remote-distributor&gt;</pre>

<p>If not set, the default velocity templates are used.</p>

<p>If set with the value 'disabled', the CMI classes are not generated and
the EJB will not be distributed.</p>

<p>If set with a file name, this file must be located in the
$JONAS_ROOT/templates/genic directory.</p>

<p>The 'cluster-home-distributor' element is valid for the SSB, SFSB and
EB.</p>

<p>The 'cluster-remote-distributor' element is valid for the SSB.</p>

<h2>High Availability with Horizontal Replication</h2>

<p>Stateful session beans (SFSBs) can be replicated since JOnAS 4.7 in order
to provide high availability in the case of failures in clustered environments.
A new service called High Availability (HA) has been included in JOnAS to
provide replication mechanishms. JOnAS HA also requires the cluster
method invocation (CMI) protocol.</p>

<p>Compared to JOnAS 4.7, JOnAS 4.8 implements a new replication algorithm
based on a horizontal replication approach. The algorithm improves the
algorithm implemented for JOnAS 4.7 with the following enhancements:</p>

<li>Replication of SFSBs with references to EBs: The algorithm can replicate
SFSBs that reference EB by means of both, local or remote interfaces.</li>
<li>Transaction awareness: The algorithm is transaction aware, meaning that
the state is not replicated if the transaction aborts.</li>
<li>Exactly-once semantics: Each transaction is committed exactly once at
the DB if the client does not fail. If the client fails, each transaction
is committed at most once at the DB</li>

<h3>EJB replication Description</h3>

<h4>Update-everywhere mode</h4>

<p>JOnAS implements an update-everywhere replication protocol according to
the database replication terminology (See the  J. Gray et al.'s paper ''The
dangers of replication and a solution'' in proceedings of the ACM SIGMOD 96's
conference, Canada). In this protocol, a client can connect to any server.
When the client calls the create() method on the SFSB's Home interface, the
server the client connects to is selected following a round-robin scheme. All
the requests from the client to the SFSB will be processed by this server
until the client calls the remove() method on the remote interface. The rest
of the servers will act as backups for that client. Before sending the
response to the client, the SFSB's state is sent to the backups.</p>

<p>If the server fails, another server among the backups will be selected to
serve the client requests, first restoring the current state of the SFSBs
from the state information stored in the HA local service. From this point
on, this server will receive the new client requests.</p>

<p>The supported replication scenarios are shown in the following figure:</p>

<center>
<p><img alt="Replication scenarios in JOnAS 4.8" src="../../resources/images/jonas48repscenarios.jpg"></p>
</center>

<h4>Transaction aware fail-over</h4>

<p>The horizontal approach aims to guarantee that the transactions are kept
consistent when a fail-over occurs. They are either aborted or restored
for ensuring the exactly-once semantics. During a fail-over, the new primary
uses a special table in the database for storing the transaction identifier
and enabling to find out if the transaction was committed or not.
<ul>
<li>If  the transaction is aborted due to the primary failure, then the new primary will
    not find the transaction identifier in the special table. The request will be replayed.</li>
<li>If the transaction is committed, then the new primary will find the transaction identifier,
    which means that the transaction was committed. The request won't be replayed; the replicated
    result is returned.
</li>
</ul>
Beyond the SFSB replication, the algorithm enables the building of applications
(stateful or stateless) with a high level of reliability and integrity.
</p>

<h3>Configuring JOnAS for EJB Replication</h3>

<p>The High Availability (HA) service is required in JOnAS in order to
replicate SFSBs. The HA service must be included in the list of available
services in JOnAS. This is done in the jonas.properties file placed in
$JONAS_BASE/conf.</p>
<pre>...
jonas.services registry,jmx,jtm,db,dbm,security,resource,ejb,ws,web,ear,<b>ha</b>
...</pre>

<p>The HA service must also be configured in the jonas.properties file:</p>
<pre>...
jonas.service.ha.class org.objectweb.jonas.ha.HaServiceImpl
jonas.service.ha.gcl jgroups
...
</pre>

<p>The HA service uses JGroups as a group communication layer (GCL). JGroups
behavior is specified by means of a stack of properties configured through an
XML file (See JGroups documentation for more
information: http://www.jgroups.org). The default configuration of the HA
service uses the $JONAS_BASE/conf/jgroups-ha.xml file and the sfsb-rep group name.  The HA
service can be told to use a particular stack configuration or a particular group name by modifying the following
lines in jonas.properties:</p>
<pre>...
jonas.service.ha.jgroups.conf jgroups-ha.xml
jonas.service.ha.jgroups.groupname jonas-rep
...</pre>
Finally, the CMI protocol must be specified in the carol.properties file in
$JONAS_BASE/conf:
<pre>...
carol.protocols=cmi...
...</pre>

<h3>Transaction Table Configuration</h3>
<p>The new horizontal replication algorithm uses a database table
to keep track of current running transactions. This table is accessed from the new
elected node during fail-over to detect whether or not the current transaction committed at
the former local node, ensuring <i>exactly-once</i> semantics.
The table contains only one column: the transaction identifier (txid).</p>
<p>In JOnAS 4.8 this table must be created manually with the following SQL command:</p>
<pre>create TABLE ha_transactions (txid varchar(60));</pre>
<p>This table should be located preferably in the database used by the replicated
application, but it is not mandatory. If the table is not created in the database
used by the replicated application, it is necessary to configure a new datasource
for the database that contains this transaction table. This datasource must be
configured to use the <i>serializable</i> transaction isolation level.</p>

<p>The database that holds the transaction table is accessed by the replication service with
the JNDI name configured in jonas.properties.</p>
<pre>...
jonas.service.ha.datasource tx_table_ds
...</pre>

<h3>Configuring Garbage Collection</h3>
Due to the fact that the replication algorithm stores information associated with
clients' transactions and that the server is not notified when a client dies,
the HA service might have been storing unnecessary replication information over
time. In order to automatically clean this unnecessary replication information,
the HA service includes a garbage collection mechanism. It is possible
to configure the number of seconds the system waits to execute this mechanism
by changing the following property in the jonas.properties file:
<pre>...
jonas.service.ha.timeout 600
...</pre>

<h3>Configuring an Application for Replication</h3>

<h4>jonas-ejb-jar.xml</h4>

<p>In order to configure an application for replication, the &lt;cluster-replicated/&gt; element must be added to the bean definition of every
bean requiring high availability in the jonas-ejb-jar.xml deployment
descriptor file. This element can have two possible
values: true or false (default value). In addition, if the programmer wants
to change the behavior of the CMI stubs (e.g., the server selection policy),
it is possible to specify different distributor implementations by means of
&lt;cluster-home-distributor/&gt; and &lt;cluster-remote-distributor/&gt;
elements. In this case, the value corresponds to the .vm file that implements
the distributor in its home and remote parts respectively. If the
&lt;cluster-replicated/&gt; element is present without the
&lt;cluster-*-distributor/&gt; elements, the default values are used
(ClusterHomeSFSBRepDistributor.vm and ClusterRemoteSFSBRepDistributor.vm).</p>

<p>The following is an example description for a replicated SFSB in
jonas-ejb-jar.xml file:</p>
<pre>...
&lt;jonas-session&gt;
   &lt;ejb-name&gt;DummySFSB&lt;/ejb-name&gt;
   &lt;jndi-name&gt;DummySFSB&lt;/jndi-name&gt;
   ...
   &lt;cluster-replicated&gt;true&lt;/cluster-replicated&gt;
   &lt;cluster-home-distributor&gt;Dummy_HomeDistributor.vm&lt;/cluster-home-distributor&gt;
   &lt;cluster-remote-distributor&gt;Dummy_RemoteDistributor.vm&lt;/cluster-remote-distributor&gt;
&lt;/jonas-session&gt;
...</pre>

<p>The &lt;cluster-replicated/&gt; element can also be set in the SSB or EB for
<ul>
	<li>enabling the transaction checking mechanism ensuring the exactly-once semantic at
	   fail-over</li>
	<li>supporting the EB references replication</li>
</ul>
</p>

<p>Note: When set in the SSB, the mechanism inhibits the load-balancing at the remote
interface. After the home create() method call, all the requests are sent to the same
instance.</p>

<h4>Entity Beans lock policy</h4>

<p>The lock policy for the Entity Beans in a replicated application must be configured
as <i>database</i> in the jonas-ejb-jar.xml deployment descriptor file.</p>
<p>The following is an example description for a replicated EB in the jonas-ejb-jar.xml:</p>
<pre>...
&lt;jonas-entity&gt;
    &lt;ejb-name&gt;MyEntitySLR&lt;/ejb-name&gt;
    &lt;jndi-name&gt;MyEntityHome&lt;/jndi-name&gt;
    &lt;cluster-replicated&gt;true&lt;/cluster-replicated&gt;
    &lt;shared&gt;true&lt;/shared&gt;
    &lt;jdbc-mapping&gt;
        &lt;jndi-name&gt;example_ds&lt;/jndi-name&gt;
    &lt;/jdbc-mapping&gt;
    &lt;lock-policy&gt;database&lt;/lock-policy&gt;
&lt;/jonas-entity&gt;
...</pre>

<h4>Datasource used by the application</h4>

<p>The datasources used by replicated applications must be configured to use the
<i>serializable</i> transaction isolation level.</p>
<p>The following is an example for a datasource configuration file for the Postgres DBMS:</p>
<pre>...
datasource.name         example_ds
datasource.url          jdbc:postgresql://xxx.xxx.xxx.xxx:xxxx/database
datasource.classname    org.postgresql.Driver
datasource.username     jonas
datasource.password
datasource.mapper       rdb.postgres
datasource.isolationlevel       serializable
...</pre>

<p>Finally, when compiling the application that includes the replicated
beans, the CMI protocol must be specified in order to generate the classes
that include the replication logic.</p>

<h3>Status and Management Information in Admin. Console for the HA Service's Replication Algorithm</h3>

The JOnAS administration console offers several items of information about the HA
service's replication algorithm and allows the configuring of several parameters
related to its behaviour. The related information and parameters include:

<ul>
  <li>The name of the service.</li>
  <li>The binded name for the MBean. The name can be changed.</li>
  <li>The number of replicated messages sent by the algorithm to the cluster's
  	replicas.</li>
  <li>The average size of the replicated messages sent.</li>
  <li>The total size of the replicated messages sent.</li>
  <li>The current JGroups configuration file name used.</li>
  <li>The current timeout established to clean in memory information related
   to SFSBs required by the algorithm. When this timeout expires, the
   information is garbage-collected. This avoids increasing the memory used
   by the algorithm. The administrator can set a different
   timeout if required.</li>
  <li>The datasource name required by the algorithm to keep track of current
   running transactions (See Transaction Table Configuration section above).
   The default datasource is setted through the "jonas.service.ha.datasource"
   parameter in the "jonas.properties" configuration file, but the administrator
   can configure different datasources and can set here, the name of the one
   that will be used by the algorithm, once JOnAS has started.
   <p>NOTE: It is recomended to not change the Datasource once the HA service
   is running.</p></li>
</ul>


</body>
</html>
