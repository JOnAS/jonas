<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"
                      "http://www.w3.org/TR/REC-html40/loose.dtd">
<!--
- JOnAS: Java(TM) Open Application Server
- Copyright (C) 2003 Bull S.A.
- Contact: jonas-team@objectweb.org

- This work is licensed under the Creative Commons
- Attribution-ShareAlike License. To view a copy of this license,
- visit http://creativecommons.org/licenses/by-sa/2.0/deed.en or
- send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford,
- California 94305, USA.

- Authors: Philippe DURIEUX

- $Id$
-->
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <link rel="stylesheet" href="common.css">
  <title>Enterprise Bean Environment</title>
</head>

<body lang="en">
<h1><a name="PG_Environment"></a>EJB Programmer's Guide: Enterprise Bean
Environment</h1>

<h2><a name="PG_Environment-Target"></a>Target Audience and Content</h2>

<p>The target audience for this guide is the Enterprise Bean provider, i.e.
the person in charge of developing the software components on the server
side. It describes how an enterprise component can refer to values,
resources, or other components in a way that is configurable at deployment
time.</p>

<p>The content of this guide is the following:</p>
<ol>
  <li><a href="#PG_Environment-Target">Target Audience and Content</a></li>
  <li><a href="#PG_Environment-Introducti">Introduction</a></li>
  <li><a href="#PG_Environment-Entries">Environment Entries</a></li>
  <li><a href="#PG_Environment-Resource">Resource References</a></li>
  <li><a href="#PG_Environment-ResEnv">Resource Environment
  References</a></li>
  <li><a href="#PG_Environment-EJBRef">EJB References</a></li>
  <li><a href="#PG_Environment-Deprec">Deprecated EJBContext.getEnvironment()
    method</a></li>
</ol>

<h2><a name="PG_Environment-Introducti"></a>Introduction</h2>

<p>The enterprise bean environment is a mechanism that allows customization
of the enterprise bean's business logic during assembly or deployment. The
environment is a way for a bean to refer to a value, to a resource, or to
another component so that the code will be independent of the actual referred
object. The actual value of such environment references (or variables) is set
at deployment time, according to what is contained in the deployment
descriptor. The enterprise bean's environment allows the enterprise bean to
be customized without the need to access or change the enterprise bean's
source code.</p>

<p>The enterprise bean environment is provided by the container (i.e. the
JOnAS server) to the bean through the JNDI interface as a JNDI context. The
bean code accesses the environment using JNDI with names starting with
"java:comp/env/".</p>

<h2><a name="PG_Environment-Entries"></a>Environment Entries</h2>

<p>The bean provider declares all the bean environment entries in the
deployment descriptor via the <em>env-entry</em> element. The deployer can
set or modify the values of the environment entries.</p>

<p>A bean accesses its environment entries with a code similar to the
following:</p>
<pre>    InitialContext ictx = new InitialContext();
    Context myenv = ictx.lookup("java:comp/env");
    Integer min = (Integer) myenv.lookup("minvalue");
    Integer max = (Integer) myenv.lookup("maxvalue");
    </pre>

<p>In the standard deployment descriptor, the declaration of these variables
are as follows:</p>
<pre>    &lt;env-entry&gt;
      &lt;env-entry-name&gt;minvalue&lt;/env-entry-name&gt;
      &lt;env-entry-type&gt;java.lang.Integer&lt;/env-entry-type&gt;
      &lt;env-entry-value&gt;12&lt;/env-entry-value&gt;
    &lt;/env-entry&gt;
    &lt;env-entry&gt;
      &lt;env-entry-name&gt;maxvalue&lt;/env-entry-name&gt;
      &lt;env-entry-type&gt;java.lang.Integer&lt;/env-entry-type&gt;
      &lt;env-entry-value&gt;120&lt;/env-entry-value&gt;
    &lt;/env-entry&gt;
    </pre>

<h2><a name="PG_Environment-Resource"></a>Resource References</h2>

<p>The resource references are another examples of environment entries. For
such entries, using subcontexts is recommended:</p>
<ul>
  <li><em>java:comp/env/jdbc</em> for references to DataSources objects.</li>
  <li><em>java:comp/env/jms</em> for references to JMS connection
  factories.</li>
</ul>

<p>In the standard deployment descriptor, the declaration of a resource
reference to a JDBC connection factory is:</p>
<pre>    &lt;resource-ref&gt;
      &lt;res-ref-name&gt;jdbc/AccountExplDs&lt;/res-ref-name&gt;
      &lt;res-type&gt;javax.sql.DataSource&lt;/res-type&gt;
      &lt;res-auth&gt;Container&lt;/res-auth&gt;
    &lt;/resource-ref&gt;
    </pre>

<p>And the bean accesses the datasource as in the following:</p>
<pre>    InitialContext ictx = new InitialContext();
    DataSource ds = ictx.lookup("java:comp/env/jdbc/AccountExplDs");
    </pre>

<p>Binding of the resource references to the actual resource manager
connection factories that are configured in the EJB server is done in the
JOnAS-specific deployment descriptor using the <em>jonas-resource</em>
element.</p>
<pre>    &lt;jonas-resource&gt;
      &lt;res-ref-name&gt;jdbc/AccountExplDs&lt;/res-ref-name&gt;
      &lt;jndi-name&gt;jdbc_1&lt;/jndi-name&gt;
    &lt;/jonas-resource&gt;
    </pre>

<h2><a name="PG_Environment-ResEnv"></a>Resource Environment References</h2>

<p>The resource environment references are another example of environment
entries. They allow the Bean Provider to refer to administered objects that
are associated with resources (for example, JMS Destinations), by using
<i>logical</i> names. Resource environment references are defined in the
standard deployment descriptor.</p>
<pre>    &lt;resource-env-ref&gt;
      &lt;resource-env-ref-name&gt;jms/stockQueue&lt;/resource-env-ref-name&gt;
      &lt;resource-env-ref-type&gt;javax.jms.Queue&lt;/resource-env-ref-type&gt;
    &lt;/resource-env-ref&gt;
    </pre>
Binding of the resource environment references to administered objects in the
target operational environment is done in the JOnAS-specific deployment
descriptor using the <em>jonas-resource-env</em> element.
<pre>    &lt;jonas-resource-env&gt;
      &lt;resource-env-ref-name&gt;jms/stockQueue&lt;/resource-env-ref-name&gt;
      &lt;jndi-name&gt;myQueue&lt;jndi-name&gt;
    &lt;/jonas-resource-env&gt;
    </pre>

<h2><a name="PG_Environment-EJBRef"></a>EJB References</h2>

<p>The EJB reference is another special entry in the enterprise bean's
environment. EJB references allow the Bean Provider to refer to the homes of
other enterprise beans using <i>logical</i> names. For such entries, using
the subcontext <em>java:comp/env/ejb</em> is recommended.</p>

<p>The declaration of an EJB reference used for accessing the bean through
its <b>remote</b> home and component interfaces in the standard deployment
descriptor is shown in the following example:</p>
<pre>    &lt;ejb-ref&gt;
      &lt;ejb-ref-name&gt;ejb/ses1&lt;/ejb-ref-name&gt;
      &lt;ejb-ref-type&gt;session&lt;/ejb-ref-type&gt;
      &lt;home&gt;tests.SS1Home&lt;/home&gt;
      &lt;remote&gt;tests.SS1&lt;/remote&gt;
    &lt;/ejb-ref&gt;
    </pre>

<p>The declaration of an EJB reference used for accessing the bean through
its <b>local</b> home and component interfaces in the standard deployment
descriptor is shown in the following example:</p>
<pre>    &lt;ejb-local-ref&gt;
      &lt;ejb-ref-name&gt;ejb/locses1&lt;/ejb-ref-name&gt;
      &lt;ejb-ref-type&gt;session&lt;/ejb-ref-type&gt;
      &lt;local-home&gt;tests.LocalSS1Home&lt;/local-home&gt;
      &lt;local&gt;tests.LocalSS1&lt;/local&gt;
      &lt;ejb-link&gt;LocalBean&lt;/ejb-link&gt;
    &lt;/ejb-local-ref&gt;
    </pre>

<p>
Local interfaces are available in the same JVM as the bean providing this interface.
The use of these interfaces also implies that the classloader of the component performing a lookup
 (bean or servlet component) is a child of the EJB classloader providing the local interface.<br>
 Local interfaces, then, are not available to outside WARs or outside EJB-JARs
 even if they run in the same JVM. This is due to the fact that classes of
 the local interfaces are not visible on the client side.
 Putting them under the WEB-INF/lib folder of a WAR would not change
 anything as the two classes would be loaded by different classloaders,
 which will throw a "ClassCastException".
<br><br></p>
 To summarize, local interfaces are available only for
 <ul>
   <li>beans in a same ejb jar file.</li>
   <li>from servlets to beans or ejbs to ejbs but in the same ear file.</li>
 </ul>


<p>If the referred bean is defined in the same ejb-jar or EAR file, the
optional <b>ejb-link</b> element of the ejb-ref element can
be used to specify the actual referred bean. The value of the ejb-link
element is the name of the target enterprise bean, i.e. the name defined in
the ejb-name element of the target enterprise bean. If the target enterprise
bean is in the same EAR file, but in a different ejb-jar file, the name of
the ejb-link element may be the name of the target bean, prefixed by the
name of the containing ejb-jar file followed by '#' (e.g.
"My_EJBs.jar#bean1"); prefixing by the name of the ejb-jar file is necessary
only if some ejb-name conflicts occur, otherwise the name of the target bean
is enough. In the following example, the ejb-link element has been
added to the ejb-ref (in the referring bean SSA) and a part of the
description of the target bean (SS1) is shown:</p>
<pre>    &lt;session&gt;
      &lt;ejb-name&gt;SSA&lt;/ejb-name&gt;
      ...
      &lt;ejb-ref&gt;
        &lt;ejb-ref-name&gt;ejb/ses1&lt;/ejb-ref-name&gt;
        &lt;ejb-ref-type&gt;session&lt;/ejb-ref-type&gt;
        &lt;home&gt;tests.SS1Home&lt;/home&gt;
        &lt;remote&gt;tests.SS1&lt;/remote&gt;
        &lt;ejb-link&gt;SS1&lt;/ejb-link&gt;
      &lt;/ejb-ref&gt;
    ...
    &lt;/session&gt;
    ...
    &lt;session&gt;
      &lt;ejb-name&gt;SS1&lt;/ejb-name&gt;
      &lt;home&gt;tests.SS1Home&lt;/home&gt;
      &lt;local-home&gt;tests.LocalSS1Home&lt;/local-home&gt;
      &lt;remote&gt;tests.SS1&lt;/remote&gt;
      &lt;local&gt;tests.LocalSS1&lt;/local&gt;
      &lt;ejb-class&gt;tests.SS1Bean&lt;/ejb-class&gt;
      ...
    &lt;/session&gt;
     ...
    </pre>

<p>If the bean SS1 was not in the same ejb-jar file as SSA, but in another
file named product_ejbs.jar, the ejb-link element could have been:</p>
<pre>        &lt;ejb-link&gt;product_ejbs.jar#SS1&lt;/ejb-link&gt;
    </pre>

<p>If the referring component and the referred bean are in separate files and
not in the same EAR, the current JOnAS implementation does not allow use of
the ejb-link element. In this case, to resolve the reference, the
<em>jonas-ejb-ref</em> element in the JOnAS-specific deployment descriptor
would be used to bind the environment JNDI name of the EJB reference to the
actual JNDI name of the associated enterprise bean home. In the following
example, it is assumed that the JNDI name of the SS1 bean home is
SS1Home_one.</p>
<pre>    &lt;jonas-session&gt;
      &lt;ejb-name&gt;SSA&lt;/ejb-name&gt;
      &lt;jndi-name&gt;SSAHome&lt;/jndi-name&gt;
      &lt;jonas-ejb-ref&gt;
        &lt;ejb-ref-name&gt;ejb/ses1&lt;/ejb-ref-name&gt;
        &lt;jndi-name&gt;SS1Home_one&lt;/jndi-name&gt;
      &lt;/jonas-ejb-ref&gt;
    &lt;/jonas-session&gt;
    ...
    &lt;jonas-session&gt;
      &lt;ejb-name&gt;SS1&lt;/ejb-name&gt;
      &lt;jndi-name&gt;SS1Home_one&lt;/jndi-name&gt;
      &lt;jndi-local-name&gt;SS1LocalHome_one&lt;/jndi-local-name&gt;
    &lt;/jonas-session&gt;
    ...
    </pre>

<p>The bean locates the home interface of the other enterprise bean using the
EJB reference with the following code:</p>
<pre>    InitialContext ictx = new InitialContext();
    Context myenv = ictx.lookup("java:comp/env");
    SS1Home home = (SS1Home)javax.rmi.PortableRemoteObject.narrow(myEnv.lookup("ejb/ses1"),
                    SS1Home.class);
    </pre>

<h2><a name="PG_Environment-Deprec"></a>Deprecated
EJBContext.getEnvironment() method</h2>

<p>JOnAS provides support for EJB 1.0-style definition of environment
properties. EJB1.0 environment must be declared in the
<b>ejb10-properties</b> sub-context. For example:</p>
<pre>    &lt;env-entry&gt;
      &lt;env-entry-name&gt;ejb10-properties/foo&lt;/env-entry-name&gt;
      &lt;env-entry-type&gt;java.lang.String&lt;/env-entry-type&gt;
      &lt;env-entry-value&gt;foo value&lt;/env-entry-value&gt;
    &lt;/env-entry&gt;
    </pre>

<p>The bean can retrieve its environment with the following code:</p>
<pre>    SessionContext ctx;
    Properties prop;
    public void setSessionContext(SessionContext sc) {
        ctx = sc;
        prop = ctx.getEnvironment();
    }
    public mymethod() {
        String foo = prop.getProperty("foo");
        ...
    }
    </pre>
</body>
</html>
