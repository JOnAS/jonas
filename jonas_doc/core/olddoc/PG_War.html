<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"
                      "http://www.w3.org/TR/REC-html40/loose.dtd">
<!--
- JOnAS: Java(TM) Open Application Server
- Copyright (C) 2003-2005 Bull S.A.
- Contact: jonas-team@objectweb.org

- This work is licensed under the Creative Commons
- Attribution-ShareAlike License. To view a copy of this license,
- visit http://creativecommons.org/licenses/by-sa/2.0/deed.en or
- send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford,
- California 94305, USA.

- Author: Florent BENOIT

- $Id$
-->
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <link rel="stylesheet" href="common.css">
  <title>Web Application Programmer's Guide</title>
</head>

<body lang="en">
<h1><a name="PG_War"></a>Web Application Programmer's Guide</h1>

<h2><a name="PG_War-Target"></a>Target Audience and Content</h2>

<p>The target audience for this guide is the Web component provider, i.e. the
person in charge of developing the Web components on the server side. It
describes how the Web component provider should build the deployment
descriptors of its Web components and how the web components should be
packaged.</p>

<p>The content of this guide is the following:</p>
<ol>
  <li><a href="#PG_War-Target">Target Audience and Content</a></li>
  <li><a href="#PG_War-Web">Developing Web Components</a>
    <ul>
      <li><a href="#PG_War-Intro">Introduction</a></li>
      <li><a href="#PG_War-JSPs">The JSP Pages</a></li>
      <li><a href="#PG_War-Servlets">The Servlets</a></li>
      <li><a href="#PG_War-ServBeans">Accessing an EJB from a Servlet or JSP
        page</a></li>
    </ul>
  </li>
  <li><a href="#PG_War-Deployment">Defining the Web Deployment Descriptor</a>
    <ul>
      <li><a href="#PG_War-Princip">Principles</a></li>
      <li><a href="#PG_War-Exampl">Examples of Web Deployment
      Descriptors</a></li>
      <li><a href="#PG_War-Tips">Tips</a></li>
    </ul>
  </li>
  <li><a href="#PG_War-Packaging">WAR Packaging</a></li>
</ol>

<h2><a name="PG_War-Web"></a>Developing Web Components</h2>

<h3><a name="PG_War-Intro"></a>Introduction</h3>

<p>A Web Component is a generic term which denotes both JSP pages and
Servlets. Web components are packaged in a <code>.war</code> file and can be
deployed in a JOnAS server via the <i>web container</i> service. Web
components can be integrated in a J2EE application by packing the
<code>.war</code> file in an <code>.ear</code> file (refer to the <a
href="PG_EarDeployment.html#PG_EarDeployment">J2EE Application Programmer's
Guide</a>).</p>

<p>The JOnAS distribution includes a Web application example: <code>The EarSample example.</code></p>

<p>The directory structure of this application is the following:</p>

<table>
  <tbody>
    <tr>
      <td><b>etc/xml</b></td>
      <td>contains the web.xml file describing the web application</td>
    </tr>
    <tr>
      <td><b>etc/resources/web</b></td>
      <td>contains html pages and images; JSP pages can also be placed
      here.</td>
    </tr>
    <tr>
      <td><b>src/org/objectweb/earsample/servlets</b></td>
      <td>servlet sources</td>
    </tr>
    <tr>
      <td><b>src/org/objectweb/earsample/beans</b></td>
      <td>beans sources</td>
    </tr>
  </tbody>
</table>

<p>The bean directory is not needed if beans coming from another application
will be used.</p>

<h3><a name="PG_War-JSPs"></a>The JSP pages</h3>

<p>Java Server Pages (JSP) is a technology that allows regular, static HTML,
to be mixed with dynamically-generated HTML written in Java programming
language for encapsulating the logic that generates the content for the page.
Refer to the <a href="http://java.sun.com/products/jsp/">Java Server
Pages<sup>TM</sup></a> and the <a
href="http://java.sun.com/products/jsp/docs.html">Quickstart guide</a> for
more details.</p>

<h4>Example:</h4>

<p>The following example shows a sample JSP page that lists the content of a
cart.</p>
<pre>    &lt;!-- Get the session --&gt;
    &lt;%@ page session="true" %&gt;

    &lt;!-- The import to use --&gt;
    &lt;%@ page import="java.util.Enumeration" %&gt;
    &lt;%@ page import="java.util.Vector"      %&gt;

    &lt;html&gt;
    &lt;body bgcolor="white"&gt;
      &lt;h1&gt;Content of your cart&lt;/h1&gt;&lt;br&gt;
      &lt;table&gt;
        &lt;!-- The header of the table --&gt;
        &lt;tr bgcolor="black"&gt;
          &lt;td&gt;&lt;font color="lightgreen"&gt;Product Reference&lt;/font&gt;&lt;/td&gt;
          &lt;td&gt;&lt;font color="lightgreen"&gt;Product Name&lt;/font&gt;&lt;/td&gt;
          &lt;td&gt;&lt;font color="lightgreen"&gt;Product Price&lt;/font&gt;&lt;/td&gt;
        &lt;/tr&gt;

        &lt;!-- Each iteration of the loop display a line of the table --&gt;
        &lt;%
          Cart cart = (Cart) session.getAttribute("cart");
          Vector products = cart.getProducts();
          Enumeration enum = products.elements();
          // loop through the enumeration
          while (enum.hasMoreElements()) {
              Product prod = (Product) enum.nextElement();
        %&gt;
        &lt;tr&gt;
          &lt;td&gt;&lt;%=prod.getReference()%&gt;&lt;/td&gt;
          &lt;td&gt;&lt;%=prod.getName()%&gt;&lt;/td&gt;
          &lt;td&gt;&lt;%=prod.getPrice()%&gt;&lt;/td&gt;
        &lt;/tr&gt;
        &lt;%
        } // end loop
        %&gt;
      &lt;/table&gt;
    &lt;/body&gt;
    &lt;/html&gt;
    </pre>

<p>It is a good idea to hide all the mechanisms for accessing EJBs from JSP
pages by using a proxy java bean, referenced in the JSP page by the
<code>usebean</code> special tag. This technique is shown in the <code>alarm example</code>, where the .jsp files
communicate with the EJB via a proxy java bean <code>ViewProxy.java</code>.</p>

<h3><a name="PG_War-Servlets"></a>The Servlets</h3>

<p>Servlets are modules of Java code that run in an application server for
answering client requests. Servlets are not tied to a specific client-server
protocol. However, they are most commonly used with HTTP, and the word
"Servlet" is often used as referring to an "HTTP Servlet."</p>

<p>Servlets make use of the Java standard extension classes in the packages
<code>javax.servlet</code> (the basic Servlet framework) and
<code>javax.servlet.http</code> (extensions of the Servlet framework for
Servlets that answer HTTP requests).</p>

<p>Typical uses for HTTP Servlets include:</p>
<ul>
  <li>processing and/or storing data submitted by an HTML form,</li>
  <li>providing dynamic content generated by processing a database query,</li>
  <li>managing information of the HTTP request.</li>
</ul>

<p>For more details refer to the <a
href="http://java.sun.com/products/servlet/">Java<sup>TM</sup> Servlet
Technology</a> and the<a
href="http://java.sun.com/docs/books/tutorial/servlets/TOC.html"> Servlets
tutorial</a>.</p>

<h4>Example:</h4>

<p>The following example is a sample of a Servlet that lists the content of a
cart. <br>
This example is the servlet version of the previous JSP page example.</p>
<pre>    import java.util.Enumeration;
    import java.util.Vector;
    import java.io.PrintWriter;
    import java.io.IOException;
    import javax.servlet.ServletException;
    import javax.servlet.http.HttpServlet;
    import javax.servlet.http.HttpServletRequest;
    import javax.servlet.http.HttpServletResponse;
    import javax.servlet.http.HttpSession;

    public class GetCartServlet extends HttpServlet {

        protected void doGet(HttpServletRequest req, HttpServletResponse res)
                             throws ServletException, IOException {

            res.setContentType("text/html");
            PrintWriter out = res.getWriter();

            out.println("&lt;html&gt;&lt;head&gt;&lt;title&gt;Your cart&lt;/title&gt;&lt;/head&gt;");
            out.println("&lt;body&gt;");
            out.println("&lt;h1&gt;Content of your cart&lt;/h1&gt;&lt;br&gt;");
            out.println("&lt;table&gt;");

            // The header of the table
            out.println("&lt;tr&gt;");
            out.println("&lt;td&gt;&lt;font color="lightgreen"&gt;Product Reference&lt;/font&gt;&lt;/td&gt;");
            out.println("&lt;td&gt;&lt;font color="lightgreen"&gt;Product Name&lt;/font&gt;&lt;/td&gt;");
            out.println("&lt;td&gt;&lt;font color="lightgreen"&gt;Product Price&lt;/font&gt;&lt;/td&gt;");
            out.println("&lt;/tr&gt;");

            // Each iteration of the loop display a line of the table
            HttpSession session = req.getSession(true);
            Cart cart = (Cart) session.getAttribute("cart");
            Vector products = cart.getProducts();
            Enumeration enum = products.elements();
            while (enum.hasMoreElements()) {
                Product prod = (Product) enum.nextElement();
                int prodId = prod.getReference();
                String prodName = prod.getName();
                float prodPrice = prod.getPrice();
                out.println("&lt;tr&gt;");
                out.println("&lt;td&gt;" + prodId + &lt;/td&gt;);
                out.println("&lt;td&gt;" + prodName + &lt;/td&gt;);
                out.println("&lt;td&gt;" + prodPrice + &lt;/td&gt;);
                out.println("&lt;/tr&gt;");
            }

            out.println("&lt;/table&gt;");
            out.println("&lt;/body&gt;");
            out.println("&lt;/html&gt;");
            out.close();
        }
    }
    </pre>

<h3><a name="PG_War-ServBeans"></a>Accessing an EJB from a Servlet or JSP
page</h3>
Through the JOnAS <i>web container</i> service, it is possible to access an
enterprise java bean and its environment in a J2EE-compliant way.

<p>The following sections describe:</p>
<ol>
  <li>How to access the Remote Home interface of a bean.</li>
  <li>How to access the Local Home interface of a bean.</li>
  <li>How to access the environment of a bean.</li>
  <li>How to start transactions in servlets.</li>
</ol>
<b>Note</b> that all the following code examples are taken from the <code>The EarSample example</code> provided in the JOnAS
distribution.

<h4>Accessing the Remote Home interface of a bean:</h4>
In this example the servlet gets the Remote Home interface <i>OpHome</i>
registered in JNDI using an EJB reference, then creates a new instance of the
session bean:
<pre>import javax.naming.Context;
import javax.naming.InitialContext;

//remote interface
import org.objectweb.earsample.beans.secusb.Op;
import org.objectweb.earsample.beans.secusb.OpHome;

        Context initialContext = null;
        try {
            initialContext = new InitialContext();
        } catch (Exception e) {
            out.print("&lt;li&gt;Cannot get initial context for JNDI: ");
            out.println(e + "&lt;/li&gt;");
            return;
        }
      // Connecting to OpHome thru JNDI
        OpHome opHome = null;
        try {
            opHome = (OpHome) PortableRemoteObject.narrow(initialContext.lookup
                     ("java:comp/env/ejb/Op"),OpHome.class);
        } catch (Exception e) {
            out.println("&lt;li&gt;Cannot lookup java:comp/env/ejb/Op: " + e + "&lt;/li&gt;");
            return;
        }
        // OpBean creation
        Op op = null;
        try {
            op = opHome.create("User1");
        } catch (Exception e) {
            out.println("&lt;li&gt;Cannot create OpBean: " + e + "&lt;/li&gt;");
            return;
        }</pre>
Note that the following elements must be set in the <code>web.xml</code> file
tied to this web application:
<pre>  &lt;ejb-ref&gt;
    &lt;ejb-ref-name&gt;ejb/Op&lt;/ejb-ref-name&gt;
    &lt;ejb-ref-type&gt;Session&lt;/ejb-ref-type&gt;
    &lt;home&gt;org.objectweb.earsample.beans.secusb.OpHome&lt;/home&gt;
    &lt;remote&gt;org.objectweb.earsample.beans.secusb.Op&lt;/remote&gt;
    &lt;ejb-link&gt;secusb.jar#Op&lt;/ejb-link&gt;
  &lt;/ejb-ref&gt;</pre>

<h4>Accessing the Local Home of a bean:</h4>
The following example shows how to obtain a local home interface
<i>OpLocalHome</i> using an EJB local reference:
<pre>//local interfaces
import org.objectweb.earsample.beans.secusb.OpLocal;
import org.objectweb.earsample.beans.secusb.OpLocalHome;


      // Connecting to OpLocalHome thru JNDI
        OpLocalHome opLocalHome = null;
        try {
            opLocalHome = (OpLocalHome)
                initialContext.lookup("java:comp/env/ejb/OpLocal");
        } catch (Exception e) {
            out.println("&lt;li&gt;Cannot lookup java:comp/env/ejb/OpLocal: " + e + "&lt;/li&gt;");
            return;
        }</pre>
This is found in the <code>web.xml</code> file:
<pre>  &lt;ejb-local-ref&gt;
    &lt;ejb-ref-name&gt;ejb/OpLocal&lt;/ejb-ref-name&gt;
    &lt;ejb-ref-type&gt;Session&lt;/ejb-ref-type&gt;
    &lt;local-home&gt;org.objectweb.earsample.beans.secusb.OpLocalHome&lt;/local-home&gt;
    &lt;local&gt;org.objectweb.earsample.beans.secusb.OpLocal&lt;/local&gt;
    &lt;ejb-link&gt;secusb.jar#Op&lt;/ejb-link&gt;
  &lt;/ejb-local-ref&gt;</pre>

<h4>Accessing the environment of the component:</h4>
In this example, the servlet seeks to access the component's environment:
<pre>       String envEntry = null;
        try {
            envEntry = (String) initialContext.lookup("java:comp/env/envEntryString");
        } catch (Exception e) {
            out.println("&lt;li&gt;Cannot get env-entry on JNDI " + e + "&lt;/li&gt;");
            return;
        }</pre>
This is the corresponding part of the <code>web.xml</code> file:
<pre>  &lt;env-entry&gt;
    &lt;env-entry-name&gt;envEntryString&lt;/env-entry-name&gt;
    &lt;env-entry-value&gt;This is a string from the env-entry&lt;/env-entry-value&gt;
    &lt;env-entry-type&gt;java.lang.String&lt;/env-entry-type&gt;
  &lt;/env-entry&gt;</pre>

<h4>Starting transactions in servlets:</h4>
The servlet wants to start transactions via the <i>UserTransaction</i>:
<pre>import javax.transaction.UserTransaction;

       // We want to start transactions from client: get UserTransaction
        UserTransaction utx = null;
        try {
            utx = (UserTransaction) initialContext.lookup("java:comp/UserTransaction");
        } catch (Exception e) {
            out.println("&lt;li&gt;Cannot lookup java:comp/UserTransaction: " + e + "&lt;/li&gt;");
            return;
        }

        try {
            utx.begin();
            opLocal.buy(10);
            opLocal.buy(20);
            utx.commit();

        } catch (Exception e) {
            out.println("&lt;li&gt;exception during 1st Tx: " + e + "&lt;/li&gt;");
            return;
        }</pre>

<h2><a name="PG_War-Deployment"></a>Defining the Web Deployment
Descriptor</h2>

<h3><a name="PG_War-Princip"></a>Principles</h3>

<p>The Web component programmer is responsible for providing the deployment
descriptor associated with the developed web components. The Web component
provider's responsibilities and the application assembler's responsibilities
are to provide an XML deployment descriptor that conforms to the deployment
descriptor's XML schema as defined in the Java <sup>TM</sup> Servlet
Specification Version 2.4. (Refer to
<code>$JONAS_ROOT/xml/web-app_2_4.xsd</code>&nbsp;or&nbsp;
<a href="http://java.sun.com/xml/ns/j2ee/web-app_2_4.xsd">http://java.sun.com/xml/ns/j2ee/web-app_2_4.xsd</a>).</p>


<p>To customize the Web components, information not defined in the standard
XML deployment descriptor may be needed. For example, the information may
include the mapping of the name of referenced resources to its JNDI name.
This information can be specified during the deployment phase, within another
XML deployment descriptor that is specific to JOnAS. The JOnAS-specific
deployment descriptor's XML schema is located in
<code>$JONAS_ROOT/xml/jonas-web-app_X_Y.xsd</code>. The file name of the
JOnAS-specific XML deployment descriptor must be the file name of the
standard XML deployment descriptor prefixed by '<tt>jonas-</tt>'.</p>

<p>The parser gets the specified schema via the classpath (schemas are packaged
in the $JONAS_ROOT/lib/common/ow_jonas.jar file).</p>

<p>The standard deployment descriptor (web.xml) should contain structural
information that includes the following:</p>
<ul>
  <li>The Servlet's description (including Servlet's name, Servlet's class or
    jsp-file, Servlet's initialization parameters),</li>
  <li>Environment entries,</li>
  <li>EJB references,</li>
  <li>EJB local references,</li>
  <li>Resource references,</li>
  <li>Resource env references.</li>
</ul>

<p>The JOnAS-specific deployment descriptor (jonas-web.xml) may contain
information that includes:</p>
<ul>
  <li>The JNDI name of the external resources referenced by a Web
  component,</li>
  <li>The JNDI name of the external resources environment referenced by a Web
    component,</li>
  <li>The JNDI name of the referenced bean's by a Web component,</li>
  <li>The name of the virtual host on which to deploy the servlets,</li>
  <li>The name of the context root on which to deploy the servlets,</li>
  <li>The compliance of the web application classloader to the java 2
    delegation model or not.</li>
</ul>

<p>&lt;host&gt; element: If the configuration file of the web container
contains virtual hosts, the host on which the WAR file is deployed can be
set.</p>

<p>&lt;context-root&gt; element: The name of the context on which the
application will be deployed should be specified. If it is not specified, the
context-root used can be one of the following:</p>
<ul>
  <li>If the war is packaged into an EAR file, the context-root used is the
    context specified in the application.xml file.</li>
  <li>If the war is standalone, the context-root is the name of the war file
    (i.e, the context-root is jonasAdmin for jonasAdmin.war).</li>
</ul>
If the context-root is / or empty, the web application is deployed as ROOT
context (i.e., http://localhost:9000/).

<p>&lt;java2-delegation-model&gt; element: Set the compliance to the java 2
delegation model.</p>
<ul>
  <li>If true: the web application context uses a classloader, using the Java
    2 delegation model (ask parent classloader first).</li>
  <li>If false: the class loader searches inside the web application first,
    before asking parent class loaders.</li>
</ul>

<p></p>

<h3><a name="PG_War-Exampl">Examples of Web Deployment Descriptors</a></h3>
<ul>
  <li>Example of a standard Web Deployment Descriptor (web.xml):
    <pre>&lt;?xml version=&quot;1.0&quot; encoding=&quot;ISO-8859-1&quot;?&gt;

&lt;web-app xmlns=&quot;http://java.sun.com/xml/ns/j2ee&quot;
         xmlns:xsi=&quot;http://www.w3.org/2001/XMLSchema-instance&quot;
         xsi:schemaLocation=&quot;http://java.sun.com/xml/ns/j2ee http://java.sun.com/xml/ns/j2ee/web-app_2_4.xsd&quot;
         version=&quot;2.4&quot;&gt;

  &lt;servlet&gt;
    &lt;servlet-name&gt;Op&lt;/servlet-name&gt;
      &lt;servlet-class&gt;org.objectweb.earsample.servlets.ServletOp&lt;/servlet-class&gt;
  &lt;/servlet&gt;

  &lt;servlet-mapping&gt;
    &lt;servlet-name&gt;Op&lt;/servlet-name&gt;
    &lt;url-pattern&gt;/secured/Op&lt;/url-pattern&gt;
  &lt;/servlet-mapping&gt;

  &lt;security-constraint&gt;
    &lt;web-resource-collection&gt;
      &lt;web-resource-name&gt;Protected Area&lt;/web-resource-name&gt;
        &lt;!-- Define the context-relative URL(s) to be protected --&gt;
        &lt;url-pattern&gt;/secured/*&lt;/url-pattern&gt;
        &lt;!-- If you list http methods, only those methods are protected --&gt;
        &lt;http-method&gt;DELETE&lt;/http-method&gt;
        &lt;http-method&gt;GET&lt;/http-method&gt;
        &lt;http-method&gt;POST&lt;/http-method&gt;
        &lt;http-method&gt;PUT&lt;/http-method&gt;
    &lt;/web-resource-collection&gt;
    &lt;auth-constraint&gt;
      &lt;!-- Anyone with one of the listed roles may access this area --&gt;
      &lt;role-name&gt;tomcat&lt;/role-name&gt;
      &lt;role-name&gt;role1&lt;/role-name&gt;
    &lt;/auth-constraint&gt;
  &lt;/security-constraint&gt;

  &lt;!-- Default login configuration uses BASIC authentication --&gt;
  &lt;login-config&gt;
    &lt;auth-method&gt;BASIC&lt;/auth-method&gt;
    &lt;realm-name&gt;Example Basic Authentication Area&lt;/realm-name&gt;
  &lt;/login-config&gt;

  &lt;env-entry&gt;
    &lt;env-entry-name&gt;envEntryString&lt;/env-entry-name&gt;
    &lt;env-entry-value&gt;This is a string from the env-entry&lt;/env-entry-value&gt;
    &lt;env-entry-type&gt;java.lang.String&lt;/env-entry-type&gt;
  &lt;/env-entry&gt;

  &lt;!-- reference on a remote bean without ejb-link--&gt;
  &lt;ejb-ref&gt;
    &lt;ejb-ref-name&gt;ejb/Op&lt;/ejb-ref-name&gt;
    &lt;ejb-ref-type&gt;Session&lt;/ejb-ref-type&gt;
    &lt;home&gt;org.objectweb.earsample.beans.secusb.OpHome&lt;/home&gt;
    &lt;remote&gt;org.objectweb.earsample.beans.secusb.Op&lt;/remote&gt;
  &lt;/ejb-ref&gt;

  &lt;!-- reference on a remote bean using ejb-link--&gt;
  &lt;ejb-ref&gt;
    &lt;ejb-ref-name&gt;ejb/EjbLinkOp&lt;/ejb-ref-name&gt;
    &lt;ejb-ref-type&gt;Session&lt;/ejb-ref-type&gt;
    &lt;home&gt;org.objectweb.earsample.beans.secusb.OpHome&lt;/home&gt;
    &lt;remote&gt;org.objectweb.earsample.beans.secusb.Op&lt;/remote&gt;
    &lt;ejb-link&gt;secusb.jar#Op&lt;/ejb-link&gt;
  &lt;/ejb-ref&gt;

  &lt;!-- reference on a local bean --&gt;
  &lt;ejb-local-ref&gt;
    &lt;ejb-ref-name&gt;ejb/OpLocal&lt;/ejb-ref-name&gt;
    &lt;ejb-ref-type&gt;Session&lt;/ejb-ref-type&gt;
    &lt;local-home&gt;org.objectweb.earsample.beans.secusb.OpLocalHome&lt;/local-home&gt;
    &lt;local&gt;org.objectweb.earsample.beans.secusb.OpLocal&lt;/local&gt;
    &lt;ejb-link&gt;secusb.jar#Op&lt;/ejb-link&gt;
  &lt;/ejb-local-ref&gt;
&lt;/web-app&gt;
        </pre>
  </li>
  <li>Example of a specific Web Deployment Descriptor (jonas-web.xml):
    <pre>&lt;?xml version=&quot;1.0&quot; encoding=&quot;ISO-8859-1&quot;?&gt;

&lt;jonas-web-app xmlns=&quot;http://www.objectweb.org/jonas/ns&quot;
	       xmlns:xsi=&quot;http://www.w3.org/2001/XMLSchema-instance&quot;
	       xsi:schemaLocation=&quot;http://www.objectweb.org/jonas/ns
	       http://www.objectweb.org/jonas/ns/jonas-web-app_4_0.xsd&quot; &gt;

  &lt;!-- Mapping between the referenced bean and its JNDI name, override the ejb-link if
       there is one in the associated ejb-ref in the standard Web Deployment Descriptor --&gt;
  &lt;jonas-ejb-ref&gt;
    &lt;ejb-ref-name&gt;ejb/Op&lt;/ejb-ref-name&gt;
    &lt;jndi-name&gt;OpHome&lt;/jndi-name&gt;
  &lt;/jonas-ejb-ref&gt;

  &lt;!-- the virtual host on which deploy the web application --&gt;
  &lt;host&gt;localhost&lt;/host&gt;

  &lt;!-- the context root on which deploy the web application --&gt;
  &lt;context-root&gt;web-application&lt;/context-root&gt;
&lt;/jonas-web-app&gt;

        </pre>
  </li>
</ul>

<h3><a name="PG_War-Tips"></a>Tips</h3>

<p>Although some characters, such as "&gt;", are legal, it is good practice
to replace them with XML entity references. The following is a list of the
predefined entity references for XML:</p>

<table border="1">
  <tbody>
    <tr>
      <td>&amp;lt;</td>
      <td>&lt;</td>
      <td>less than</td>
    </tr>
    <tr>
      <td>&amp;gt;</td>
      <td>&gt;</td>
      <td>greater than</td>
    </tr>
    <tr>
      <td>&amp;amp;</td>
      <td>&amp;</td>
      <td>ampersand</td>
    </tr>
    <tr>
      <td>&amp;apos;</td>
      <td>'</td>
      <td>apostrophe</td>
    </tr>
    <tr>
      <td>&amp;quot;</td>
      <td>"</td>
      <td>quotation mark</td>
    </tr>
  </tbody>
</table>

<p></p>

<h2><a name="PG_War-Packaging"></a>WAR Packaging</h2>

<p>Web components are packaged for deployment in a standard Java programming
language Archive file called a <em>war</em> file (Web ARchive), which is a
<em>jar</em> similar to the package used for Java class libraries. A
<em>war</em> has a specific hierarchical directory structure. The top-level
directory of a <em>war</em> is the document root of the application.</p>

<p>The document root is where JSP pages, client-side classes and archives,
and static web resources are stored. The document root contains a
subdirectory called <em>WEB-INF</em>, which contains the following files and
directories:</p>
<ul>
  <li><em>web.xml</em>: The standard xml deployment descriptor in the format
    defined in the Java Servlet 2.4 Specification. Refer to
    <em>$JONAS_ROOT/xml/web-app_2_4.xsd</em>.</li>
  <li><em>jonas-web.xml</em>: The optional JOnAS-specific XML deployment
    descriptor in the format defined in
    <em>$JONAS_ROOT/xml/jonas-web_X_Y.xsd</em>.</li>
  <li><em>classes</em>: a directory that contains the servlet classes and
    utility classes.</li>
  <li><em>lib</em>: a directory that contains jar archives of libraries (tag
    libraries and any utility libraries called by server-side classes). If
    the Web application uses Enterprise Beans, it can also contain
    <i>ejb-jars</i>. This is necessary to give to the Web components the
    visibility of the EJB classes.<br>
    However, if the <i>war</i> is intended to be packed in a <i>ear</i>, the
    <i>ejb-jars</i> must not be placed here. In this case, they are directly
    included in the <i>ear</i>. Due to the use of the class loader hierarchy,
    Web components have the visibility of the EJB classes.<br>
    Details about the class loader hierarchy are described in <a
    href="PG_J2eeApps.html#PG_J2eeApps-ClassLoader">JOnAS class loader
    hierarchy</a>.</li>
</ul>

<h3>Example</h3>

<p>Before building a <i>war</i> file, the java source files must be compiled
to obtain the class files (located in the <em>WEB-INF/classes</em> directory)
and the two XML deployment descriptors must be written.</p>

<p>Then, the <i>war</i> file (&lt;web-application&gt;.war) is built using the
<em>jar</em> command:</p>
<pre>    cd &lt;your_webapp_directory&gt;
    jar cvf &lt;web-application&gt;.war *
    </pre>

<p>During the development process, an 'unpacked version' of the war file can
be used. Refer to <a href="configuration_guide.html#N10791">Configuring Web
Container Service </a>for information about how to use directories for the
web application.</p>
</body>
</html>
