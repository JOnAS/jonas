<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"
                      "http://www.w3.org/TR/REC-html40/loose.dtd">
<!--
- JOnAS: Java(TM) Open Application Server
- Copyright (C) 2003-2004 Bull S.A.
- Contact: jonas-team@objectweb.org

- This work is licensed under the Creative Commons
- Attribution-ShareAlike License. To view a copy of this license,
- visit http://creativecommons.org/licenses/by-sa/2.0/deed.en or
- send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford,
- California 94305, USA.

- Authors: Philippe COQ, Fran�ois EXERTIER

- $Id$
-->
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <title>Message-driven Beans</title>
  <link rel="stylesheet" href="common.css">
</head>

<body lang="en">
<h1><a name="PG_MsgDrvBean"></a>EJB Programmer's Guide: Message-driven
Beans</h1>

<p>The content of this guide is the following:</p>
<ol>
  <li><a href="#PG_MsgDrvBean-What">Description of a Message-driven
  Bean</a></li>
  <li><a href="#PG_MsgDrvBean-Developing">Developing a Message-driven
  Bean</a></li>
  <li><a href="#PG_MsgDrvBean-Admin">Administration aspects</a></li>
  <li><a href="#PG_MsgDrvBean-Running">Running a Message-driven Bean</a></li>
  <li><a href="#PG_MsgDrvBean-Transact">Transactional aspects</a></li>
  <li><a href="#PG_MsgDrvBean-Example">Example</a></li>
  <li><a href="#PG_MsgDrvBean-Tuning">Tuning Message-driven Bean Pool</a></li>
</ol>

<p>The EJB 2.1 specification defines a new kind of EJB component for
receiving asynchronous messages. This implements some type of "asynchronous
EJB component method invocation" mechanism. The Message-driven Bean (also
referred to as MDB in the following) is an Enterprise JavaBean, not an Entity
Bean or a Session Bean, which plays the role of a JMS MessageListener.</p>

<p>The EJB 2.1 specification contains detailed information about MDB.  The Java
Message Service Specification 1.1 contains detailed information about JMS.
This chapter focuses on the use of Message-driven beans within the JOnAS
server.</p>

<h2><a name="PG_MsgDrvBean-What"></a>Description of a Message-driven Bean</h2>
A Message-driven Bean is an EJB component that can be considered as a JMS
MessageListener, i.e., processing JMS messages asynchronously; it implements
the <i>onMessage(javax.jms.Message)</i> method, defined in the
<i>javax.jms.MessageListener</i> interface. It is associated with a JMS
destination, i.e., a Queue for "point-to-point" messaging or a Topic for
"publish/subscribe." The <i>onMessage</i> method is activated on receipt of
messages sent by a client application to the corresponding JMS destination.
It is possible to associate a JMS message selector to filter the messages
that the Message-driven Bean should receive.

<p>JMS messages do not carry any context, thus the <i>onMessage</i> method
will execute without pre-existing transactional context. However, a new
transaction can be initiated at this moment (refer to the "<a
href="#PG_MsgDrvBean-Transact">Transactional aspects</a>" section for more
details). The <i>onMessage</i> method can call other methods on the MDB
itself or on other beans, and can involve other resources by accessing
databases or by sending messages. Such resources are accessed the same way as
for other beans (entity or session), i.e., through resource references
declared in the deployment descriptor.</p>

<p>The JOnAS container maintains a pool of MDB instances, allowing large
volumes of messages to be processed concurrently. An MDB is similar in some
ways to a stateless session bean: its instances are relatively short-lived,
it retains no state for a specific client, and several instances may be
running at the same time.</p>

<h2><a name="PG_MsgDrvBean-Developing"></a>Developing a Message-driven
Bean</h2>

<p>The MDB class must implement the <i>javax.jms.MessageListener</i> and the
<i>javax.ejb.MessageDrivenBean</i> interfaces. In addition to the
<i>onMessage</i> method, the following must be implemented:</p>
<ul>
  <li>A public constructor with no argument.</li>
  <li><i>public void ejbCreate()</i>: with no arguments, called at the bean
    instantiation time. It may be used to allocate some resources, such as
    connection factories, for example if the bean sends messages, or
    datasources or if the bean accesses databases.</li>
  <li><i>public void ejbRemove()</i>: usually used to free the resources
    allocated in the ejbCreate method.</li>
  <li><i>public void setMessageDrivenContext(MessageDrivenContext mdc)</i>:
    called by the container after the instance creation, with no transaction
    context. The JOnAS container provides the bean with a container context
    that can be used for transaction management, e.g., for calling
    <i>setRollbackOnly(), getRollbackOnly(), getUserTransaction()</i>.</li>
</ul>

<p>The following is an example of an MDB class:</p>
<pre>public class MdbBean  implements MessageDrivenBean, MessageListener {

    private transient MessageDrivenContext mdbContext;

    public MdbBean() {}

    public void setMessageDrivenContext(MessageDrivenContext ctx) {
        mdbContext = ctx;
    }

    public void ejbRemove() {}

    public void ejbCreate() {}

    public void onMessage(Message message) {
        try {
            TextMessage mess = (TextMessage)message;
            System.out.println( "Message received: "+mess.getText());
        }catch(JMSException ex){
            System.err.println("Exception caught: "+ex);
        }
    }
}
    </pre>

<p>The destination associated to an MDB is specified in the deployment
descriptor of the bean. A destination is a JMS-administered object,
accessible via JNDI. The description of an MDB in the EJB 2.0 deployment
descriptor contains the following elements, which are specific to MDBs:</p>
<ul>
  <li>the JMS acknowledgment mode: auto-acknowledge or dups-ok-acknowledge
    (refer to the JMS specification for the definition of these modes)</li>
  <li>an eventual JMS message selector: this is a JMS concept which allows
    the filtering of the messages sent to the destination</li>
  <li>a message-driven-destination, which contains the destination type
    (Queue or Topic) and the subscription durability (in case of Topic)</li>
</ul>

<p>The following example illustrates such a deployment descriptor:</p>
<pre>  &lt;enterprise-beans&gt;
    &lt;message-driven&gt;
      &lt;description&gt;Describe here the message driven bean Mdb&lt;/description&gt;
      &lt;display-name&gt;Message Driven Bean Mdb&lt;/display-name&gt;
      &lt;ejb-name&gt;Mdb&lt;/ejb-name&gt;
      &lt;ejb-class&gt;samplemdb.MdbBean&lt;/ejb-class&gt;
      &lt;transaction-type&gt;Container&lt;/transaction-type&gt;
      &lt;message-selector&gt;Weight &gt;= 60.00 AND LName LIKE 'Sm_th'&lt;/message-selector&gt;
      &lt;message-driven-destination&gt;
        &lt;destination-type&gt;javax.jms.Topic&lt;/destination-type&gt;
        &lt;subscription-durability&gt;NonDurable&lt;/subscription-durability&gt;
      &lt;/message-driven-destination&gt;
      &lt;acknowledge-mode&gt;Auto-acknowledge&lt;/acknowledge-mode&gt;
    &lt;/message-driven&gt;
  &lt;/enterprise-beans&gt;
    </pre>

<p>If the transaction type is "container," the transactional behavior of the
MDB's methods are defined as for other enterprise beans in the deployment
descriptor, as in the following example:</p>
<pre>  &lt;assembly-descriptor&gt;
    &lt;container-transaction&gt;
      &lt;method&gt;
        &lt;ejb-name&gt;Mdb&lt;/ejb-name&gt;
        &lt;method-name&gt;*&lt;/method-name&gt;
      &lt;/method&gt;
      &lt;trans-attribute&gt;Required&lt;/trans-attribute&gt;
    &lt;/container-transaction&gt;
  &lt;/assembly-descriptor&gt;
    </pre>

<p>For the <i>onMessage</i> method, only the <i>Required</i> or
<i>NotSupported</i> transaction attributes must be used, since there can be
no pre-existing transaction context.</p>

<p>For the message selector specified in the previous example, the sent JMS
messages are expected to have two properties, "Weight" and "LName," for
example assigned in the JMS client program sending the messages, as
follows:</p>
<pre>        message.setDoubleProperty("Weight",75.5);
        message.setStringProperty("LName","Smith");
        </pre>

<p>Such a message will be received by the Message-driven bean. The message
selector syntax is based on a subset of the SQL92. Only messages whose
headers and properties match the selector are delivered. Refer to the JMS
specification for more details.</p>

<p>The JNDI name of a destination associated with an MDB is defined in the
JOnAS-specific deployment descriptor, within a <em>jonas-message-driven</em>
element, as illustrated in the following:</p>
<pre>  &lt;jonas-message-driven&gt;
    &lt;ejb-name&gt;Mdb&lt;/ejb-name&gt;
    &lt;jonas-message-driven-destination&gt;
      &lt;jndi-name&gt;sampleTopic&lt;/jndi-name&gt;
    &lt;/jonas-message-driven-destination&gt;
  &lt;/jonas-message-driven&gt;
    </pre>

<p>Once the destination is established, a client application can send
messages to the MDB through a destination object obtained via JNDI as
follows:</p>

<p><code>Queue q = context.lookup("sampleTopic");</code></p>

<p>If the client sending messages to the MDB is an EJB component itself, it
is preferable that it use a resource environment reference to obtain the
destination object. The use of resource environment references is described
in the <a href="PG_JmsGuide.html#PG_JmsGuide-Writing" rel="Chapter">JMS
User's Guide</a> (Writing JMS operations within an application component /
Accessing the destination object section).</p>

<h2><a name="PG_MsgDrvBean-Admin"></a>Administration aspects</h2>
It is assumed at this point that the JOnAS server will make use of an
existing JMS implementation, e.g., Joram, SwiftMQ.

<p>The default policy is that the MDB developer and deployer are not
concerned with JMS administration. This means that the developer/deployer
will not create or use any JMS Connection factories and will not create a JMS
destination (which is necessary for performing JMS operations within an EJB
component, refer to the <a href="PG_JmsGuide.html#PG_JmsGuide"
rel="Chapter">JMS User's Guide</a>); they will simply define the type of
destination in the deployment descriptor and identify its JNDI name in the
JOnAS-specific deployment descriptor, as described in the previous section.
This means that JOnAS will implicitly create the necessary administered
objects by using the proprietary administration APIs of the JMS
implementation (since the administration APIs are not standardized). To
perform such administration operations, JOnAS uses wrappers to the JMS
provider administration API. For Joram, the wrapper is
<i>org.objectweb.jonas_jms.JmsAdminForJoram</i> (which is the default wrapper
class defined by the <i>jonas.service.jms.mom</i> property in the
<strong>jonas.properties</strong> file). For SwiftMQ, a
<i>com.swiftmq.appserver.jonas.JmsAdminForSwiftMQ</i> class can be obtained
from the <a href="http://www.swiftmq.com/">SwiftMQ</a> site.</p>

<p>For the purpose of this implicit administration phase, the deployer must
add the 'jms' service in the list of the JOnAS services. For the example
provided, the jonas.properties file should contain the following:</p>
<pre>
jonas.services                 registry,security,jtm,dbm,jms,ejb // The jms service must be added
jonas.service.ejb.descriptors  samplemdb.jar
jonas.service.jms.topics       sampleTopic    // not mandatory</pre>

<p>The destination objects may or may not pre-exist. The EJB server will not
create the corresponding JMS destination object if it already exists. (Refer
also to <a href="PG_JmsGuide.html#PG_JmsGuide-Admin" rel="Chapter">JMS
administration</a>). The <code>sampleTopic</code> should be explicitly
declared only if the JOnAS Server is going to create it first, even if the
Message-driven bean is not loaded, or if it is used by another client before
the Message-driven bean is loaded. In general, it is not necessary to declare
the <code>sampleTopic</code>.</p>

<p>JOnAS uses a <strong>pool of threads</strong> for executing Message-driven
bean instances on message reception, thus allowing large volumes of messages
to be processed concurrently. As previously explained, MDB instances are
stateless and several instances may execute concurrently on behalf of the
same MDB. The default size of the pool of thread is 10, and it may be
customized via the jonas property
<strong>jonas.service.ejb.mdbthreadpoolsize</strong>, which is specified in
the <strong>jonas.properties</strong> file as in the following example:</p>
<pre>    jonas.service.ejb.mdbthreadpoolsize   50
    </pre>

<h2><a name="PG_MsgDrvBean-Running"></a>Running a Message-driven Bean</h2>

<p>To deploy and run a Message-driven Bean, perform the following steps:</p>
<ul>
  <li>Verify that a registry is running.</li>
  <li><p>Start the Message-Oriented Middleware (the JMS provider
    implementation). Refer to the section "<a
    href="#PG_MsgDrvBean-LaunchingMom">Launching the Message-Oriented
    Middleware</a>."</p>
  </li>
  <li><p>Create and register in JNDI the JMS destination object that will be
    used by the MDB.</p>
    <p>This can be done automatically by the JMS service or explicitly by the
    proprietary administration facilities of the JMS provider (<a
    href="PG_JmsGuide.html#PG_JmsGuide-Admin" rel="Chapter">JMS
    administration</a>). The JMS service creates the destination object if
    this destination is declared in the <strong>jonas.properties</strong>
    file (as specified in the previous section).</p>
  </li>
  <li><p>Deploy the MDB component in JOnAS.</p>
    <p>Note that, if the destination object is not already created when
    deploying an MDB, the container asks the JMS service to create it based
    on the deployment descriptor content.</p>
  </li>
  <li>Run the EJB client application.</li>
  <li>Stop the application.
    <p>When using JMS, it is very important to stop JOnAS using the
    <strong>jonas stop</strong> command; it should not be stopped directly by
    killing it.</p>
  </li>
</ul>

<h3><a name="PG_MsgDrvBean-LaunchingMom"></a>Launching the Message-Oriented
Middleware</h3>

<p>If the configuration property <strong>jonas.services</strong> contains the
jms service, then the JOnAS JMS service will be launched and may try to
launch a JMS implementation (a MOM).</p>

<p>For launching the MOM, three possibilities can be considered:</p>
<ol>
  <li><strong>Launching the MOM in the same JVM as JOnAS</strong>
    <p>This is the default situation obtained by assigning the
    <code>true</code> value to the configuration property
    <strong>jonas.service.jms.collocated</strong> in the
    <strong>jonas.properties</strong> file.</p>
    <pre>
jonas.services                security,jtm,dbm,jms,ejb // The jms service must be in the list
jonas.service.jms.collocated  true</pre>
    <p>In this case, the MOM is automatically launched by the JOnAS JMS
    service at the JOnAS launching time (command <code>jonas
    start</code>).</p>
  </li>
  <li><strong>Launching the MOM in a separate JVM</strong>
    <p>The Joram MOM can be launched using the command:</p>
    <p><code>JmsServer</code></p>
    <p>For other MOMs, the proprietary command should be used.</p>
    <p>The configuration property
    <strong>jonas.service.jms.collocated</strong> must be set to
    <code>false</code> in the <strong>jonas.properties</strong> file. Setting
    this property is sufficient if the JORAM's JVM runs on the same host as
    JONAS, and if the MOM is launched with its default options (unchanged
    <strong>a3servers.xml</strong> configuration file under JONAS_BASE/conf
    or JONAS_ROOT/conf if JONAS_BASE not defined).</p>
    <pre>
jonas.services               security,jtm,dbm,jms,ejb // The jms service must be in the list
jonas.service.jms.collocated false</pre>
    <p>To use a specific configuration for the MOM, such as changing the
    default host (which is localhost) or the default connection port number
    (which is 16010), requires defining the additional
    <strong>jonas.service.jms.url</strong> configuration property as
    presented in the following case.</p>
  </li>
  <li><strong>Launching the MOM on another host</strong>
    <p>This requires defining the <strong>jonas.service.jms.url</strong>
    configuration property. When using Joram, its value should be the Joram
    URL <code>joram://host:port</code> where <code>host</code> is the host
    name, and <code>port</code> is the connection port (by default, 16010).
    For SwiftMQ, the value of the URL is similar to the following:
    <tt>smqp://host:4001/timeout=10000</tt>.</p>
    <pre>
jonas.services                security,jtm,dbm,jms,ejb // The jms service must be in the list
jonas.service.jms.collocated  false
jonas.service.jms.url         joram://host2:16010
 </pre>
  </li>
</ol>
<ul>
  <li><strong>Change Joram default configuration</strong>
    <p>As mentioned previously, the default host or default connection port
    number may need to be changed. This requires modifying the
    <strong>a3servers.xml</strong> configuration file provided by the JOnAS
    delivery in JONAS_ROOT/conf directory. For this, JOnAS must be configured
    with the property <strong>jonas.service.jms.collocated</strong> set to
    <code>false</code>, and the property
    <strong>jonas.service.jms.url</strong> set to
    <code>joram://host:port</code>. Additionally, the MOM must have been
    previously launched with the JmsServer command. This command defines a
    <code>Transaction</code> property set to
    <code>fr.dyade.aaa.util.NullTransaction</code>. If the messages need to
    be persistent, replace the
    <code>-DTransaction=fr.dyade.aaa.util.NullTransaction</code> option with
    the <code>-DTransaction=fr.dyade.aaa.util.NTransaction</code> option.
    Refer to the Joram documentation for more details about this command. To
    define a more complex configuration (e.g., distribution, multi-servers),
    refer to the Joram documentation on <a
    href="http://joram.objectweb.org">http://joram.objectweb.org</a>.</p>
  </li>
</ul>

<h2><a name="PG_MsgDrvBean-Transact"></a>Transactional aspects</h2>
Because a transactional context cannot be carried by a message (according to
the EJB 2.0 specification), an MDB will never execute within an existing
transaction. However, a transaction may be started during the onMessage
method execution, either due to a "required" transaction attribute
(container-managed transaction) or because it is explicitly started within
the method (if the MDB is bean-managed transacted). In the second case, the
message receipt will not be part of the transaction. In the first case,
container-managed transaction, the container will start a new transaction
before de-queueing the JMS message (the receipt of which will, thus, be part
of the started transaction), then enlist the resource manager associated with
the arriving message and all the resource managers accessed by the onMessage
method. If the onMessage method invokes other enterprise beans, the container
passes the transaction context with the invocation. Therefore, the
transaction started at the <i>onMessage</i> method execution may involve
several operations, such as accessing a database (via a call to an entity
bean, or by using a "datasource" resource), or sending messages (by using a
"connection factory" resource).

<h2><a name="PG_MsgDrvBean-Example"></a>Example</h2>
JOnAS provides examples that are located in the
<strong>examples/src/mdb</strong> install directory. <br>
<strong>samplemdb</strong> is a very simple example, the code of which is
used in the previous topics for illustrating how to use Message-driven beans.
<br>
<strong>sampleappli</strong> is a more complex example that shows how the
sending of JMS messages and updates in a database via JDBC may be involved in
the same distributed transaction. <br>
The following figure illustrates the architecture of this example
application. <br>
<br>
<img src="../../resources/images/eb_img_31.gif" alt="eb"><br>


<p>There are two Message-driven beans in this example:</p>
<ul>
  <li><strong><code>StockHandlerBean</code></strong>
    is a Message-driven bean listening to a topic and receiving Map messages.
    The <tt>onMessage</tt> method runs in the scope of a transaction started
    by the container. It sends a Text message on a Queue (OrdersQueue) and
    updates a Stock element by decreasing the stock quantity. If the stock
    quantity becomes negative, an exception is received and the current
    transaction is marked for rollback.</li>
  <li><p><strong><code>OrderBean</code></strong>
    is another Message-driven bean listening on the OrdersQueue Queue. On
    receipt of a Text message on this queue, it writes the corresponding
    String as a new line in a file ("Order.txt").</p>
  </li>
</ul>

<p>The example also includes a CMP entity bean <strong><code>Stock</code></strong> that
handles a stock table.</p>

<p>A Stock item is composed of a Stockid (String), which is the primary key,
and a Quantity (int). The method decreaseQuantity(int qty) decreases the
quantity for the corresponding stockid, but can throw a RemoteException
"Negative stock."</p>

<p>The client application <code><strong>SampleAppliClient</strong></code>
is a JMS Client that sends several messages on the topic
<i>StockHandlerTopic</i>. It uses Map messages with three fields:
"CustomerId," "ProductId," "Quantity." Before sending messages, this client
calls the <code><strong>EnvBean</strong></code>
for creating the <i>StockTable</i> in the database with known values in order
to check the results of updates at the end of the test. Eleven messages are
sent, the corresponding transactions are committed, and the last message sent
causes the transaction to be rolled back.</p>

<h3>Compiling this example</h3>

<p>To compile <code>examples/src/mdb/sampleappli</code>, use <em>Ant</em>
with the <code>$JONAS_ROOT/examples/src/build.xml</code> file.</p>

<h3>Running this example</h3>
<br>
The default configuration of the JMS service in <code>jonas.properties</code>
is the following:
<pre>
 jonas.services                 jmx,security,jtm,dbm,jms,ejb  // The jms service must be added
 jonas.service.ejb.descriptors  sampleappli.jar
 jonas.service.jms.topics       StockHandlerTopic
 jonas.service.jms.queues       OrdersQueue
 jonas.service.jms.collocated   true
 </pre>
This indicates that the JMS Server will be launched in the same JVM as the
JOnAS Server, and the JMS-administered objects <code>StockHandlerTopic</code>
(Topic) and <code>OrdersQueue</code> (Queue) will be created and registered
in JNDI, if not already existing.
<ul>
  <li>Run the JOnAS Server. <br>

    <pre>jonas start</pre>
  </li>
  <li>Deploy the sampleappli container. <br>

    <pre>jonas admin -a sampleappli.jar</pre>
  </li>
  <li>Run the EJB client. <br>

    <pre>jclient sampleappli.SampleAppliClient</pre>
  </li>
  <li>Stop the server. <br>

    <pre>jonas stop</pre>
  </li>
</ul>

<h2><a name="PG_MsgDrvBean-Tuning"></a>Tuning Message-driven Bean Pool</h2>
A pool is handled by JOnAS for each Message-driven bean. The pool can be
configured in the JOnAS-specific deployment descriptor with the following
tags:

<h3>min-pool-size</h3>
This optional integer value represents the minimum instances that will be
created in the pool when the bean is loaded. This will improve bean instance
creation time, at least for the first beans. The default value is 0.

<h3>max-cache-size</h3>
This optional integer value represents the maximum number of instances of
<i>ServerSession</i> that may be created in memory. The purpose of this value
is to keep JOnAS scalable. The policy is the following: <br>
When the <i>ConnectionConsumer</i> ask for a <i>ServerSession</i> instance
(in order to deliver a new message) JOnAS tries to give an instance from  the
<i>ServerSessionPool</i>. If the pool is empty, a new instance is created
only if the number of yet created instances is smaller  than the max-cache-size
parameter. When the max-cache-size is reached, the <i>ConnectionConsumer</i>
is blocked and it cannot deliver new messages until a <i>ServerSession</i> is
eventually returned in the pool. A <i>ServerSession</i> is pushed into the
pool at the end of the <i>onMessage</i> method.<br>
The default value is no limit (this means that a new instance of
<i>ServerSession</i> is always created when the pool is empty).
<p>
The values for <code>max-cache-size</code> should be set accordingly to <code>jonas.service.ejb.maxworkthreads</code> value.
See <a href="configuration_guide.html#N10D43">Configuring JMS Service</a>.
</p>
<h3>example</h3>
<pre>  &lt;jonas-ejb-jar&gt;
    &lt;jonas-message-driven&gt;
      &lt;ejb-name&gt;Mdb&lt;/ejb-name&gt;
      &lt;jndi-name&gt;mdbTopic&lt;/jndi-name&gt;
      &lt;max-cache-size&gt;20&lt;/max-cache-size&gt;
      &lt;min-pool-size&gt;10&lt;/min-pool-size&gt;
    &lt;/jonas-message-driven&gt;
  &lt;/jonas-ejb-jar&gt;
   </pre>
</body>
</html>
