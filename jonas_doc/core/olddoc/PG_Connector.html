<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"
                      "http://www.w3.org/TR/REC-html40/loose.dtd">
<!--
- JOnAS: Java(TM) Open Application Server
- Copyright (C) 2003 Bull S.A.
- Contact: jonas-team@objectweb.org

- This work is licensed under the Creative Commons
- Attribution-ShareAlike License. To view a copy of this license,
- visit http://creativecommons.org/licenses/by-sa/2.0/deed.en or
- send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford,
- California 94305, USA.

- Author: Eric HARDESTY

- $Id$
-->
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <link rel="stylesheet" href="common.css">
  <title>J2EE Connector Programmer's Guide</title>
</head>

<body lang="en">
<h1><a name="PG_Connector"></a>J2EE Connector Programmer's Guide</h1>

<p>The content of this guide is the following:</p>
<ol>
  <li><a href="#PG_Connector-Target">Target Audience and Content</a></li>
  <li><a href="#PG_Connector-Princip">Principles</a></li>
  <li><a href="#PG_Connector-Descriptor">Defining the JOnAS Connector
    Deployment Descriptor</a></li>
  <li><a href="#PG_Connector-Packaging">Resource Adapter (RAR)
  Packaging</a></li>
  <li><a href="#PG_Connector-Use">Use and Deployment of a Resource
  Adapter</a></li>
  <li><a href="#PG_Connector-JDBC">JDBC Resource Adapters</a></li>
  <li><a href="#PG_Connector-JCA">Appendix: Connector Architecture
    Principles</a></li>
</ol>

<h2><a name="PG_Connector-Target"></a>Target Audience and Content</h2>

<p>This chapter is provided for advanced JOnAS users concerned with EAI
(Enterprise Application Integration) and using the J2EE Connector
Architecture principles (refer to the <a
href="#PG_Connector-JCA">Appendix</a> for an introduction to the connectors).
The target audience for this guide is the Resource Adapter deployer and
programmer. It describes the JOnAS specific deployment file
(<em>jonas-ra.xml</em>) and the sample code to access deployed RARs.</p>

<h2><a name="PG_Connector-Princip"></a>Principles</h2>

<p>Resource Adapters are packaged for deployment in a standard Java
programming language Archive file called a <em>rar</em> file (Resource
ARchive), which is described in the J2EE Connector Architecture
specification.</p>

<p>The standard method for creating the <em>jonas-ra.xml</em> file is to use
the RAConfig tool. For a complete description refer to <a
href="command_guide.html#N10328">RAConfig</a>.</p>

<h2><a name="PG_Connector-Descriptor"></a>Defining the JOnAS Connector
Deployment Descriptor</h2>

<p>The <em>jonas-ra.xml</em> contains JOnAS specific information describing
deployment information, logging, pooling, jdbc connections, and RAR config
property values.</p>
<ul>
  <li><em>Deployment Tags</em>:
    <ul>
      <li>jndiname: (Required) Name the RAR will be registered as. This value
        will be used in the resource-ref section of an EJB.</li>
      <li>rarlink: Jndiname of a base RAR file.  Useful for deploying
        multiple connection factories without having to deploy the complete
        RAR file again.  When this is used, the only entry in RAR is a
        <em>META-INF/jonas-ra.xml</em>.</li>
      <li>native-lib: Directory where additional files in the RAR should be
        deployed.</li>
    </ul>
  </li>
  <li><em>Logging Tags</em>:
    <ul>
      <li>log-enabled: Determines if logging should be enabled for the
      RAR.</li>
      <li>log-topic: Log topic to use for the PrintWriter logger, which
        allows a separate handler for each deployed RAR.</li>
    </ul>
  </li>
  <li><em>Pooling Tags</em>:
    <ul>
      <li>pool-init: Initial size of the managed connection pool.</li>
      <li>pool-min: Minimum size of the managed connection pool.</li>
      <li>pool-max: Maximum size of the managed connection pool.  Value of -1
        is unlimited.</li>
      <li>pool-max-age: Maximum number of milliseconds to keep the managed
        connection in the pool.  Value of 0 is an unlimited amount of
      time.</li>
      <li>pstmt-max: Maximum number of PreparedStatements per managed
        connection in the pool. Only needed with the JDBC RA of JOnAS or
        another database vendor's RAR. Value of 0 is unlimited and -1
        disables the cache.</li>
    </ul>
  </li>
  <li><em>JDBC Connection Tags</em>: Only valid with a Connection
    implementation of java.sql.Connection.
    <ul>
      <li>jdbc-check-level: Level of checking that will be done for the jdbc
        connection.  Values are 0 for no checking, 1 to validate that the
        connection is not closed before returning it, and greater than 1 to
        send the jdbc-test-statement.</li>
      <li>jdbc-test-statement: Test SQL statement sent on the connection if
        the jdbc-check-level is set accordingly.</li>
    </ul>
  </li>
  <li><em>Config Property Value Tags</em>:
    <ul>
      <li>Each entry must correspond to the config-property specified in the
        <em>ra.xml</em> of the RAR file.  The default values specified in the <em>ra.xml</em> will
        be loaded first and any values set in the <em>jonas-ra.xml</em> will
        override the specified defaults.</li>
    </ul>
  </li>
</ul>

<h3>Deployment Descriptor Examples</h3>

<p>The following portion of a <em>jonas-ra.xml</em> file shows the linking to
a base RAR file named BaseRar. All properties from the base RAR will be
inherited and any values given in this <em>jonas-ra.xml</em> will override
the other values.</p>
<pre>  &lt;jonas-resource&gt;
    &lt;jndiname&gt;rar1&lt;/jndiname&gt;
    &lt;rarlink&gt;BaseRar&lt;/rarlink&gt;
    &lt;native-lib&gt;nativelib&lt;/native-lib&gt;
    &lt;log-enabled&gt;false&lt;/log-enabled&gt;
    &lt;log-topic&gt;com.xxx.rar1&lt;/log-topic&gt;
    &lt;jonas-config-property&gt;
      &lt;jonas-config-property-name&gt;ip&lt;/jonas-config-property-name&gt;
      &lt;jonas-config-property-value&gt;www.xxx.com&lt;/jonas-config-property-value&gt;
    &lt;/jonas-config-property&gt;
    .
    .
  &lt;/jonas-resource&gt;</pre>

<p>The following portion of a <em>jonas-ra.xml</em> file shows the
configuration of a jdbc rar file.</p>
<pre>  &lt;jonas-resource&gt;
    &lt;jndiname&gt;jdbc1&lt;/jndiname&gt;
    &lt;rarlink&gt;&lt;/rarlink&gt;
    &lt;native-lib&gt;nativelib&lt;/native-lib&gt;
    &lt;log-enabled&gt;false&lt;/log-enabled&gt;
    &lt;log-topic&gt;com.xxx.jdbc1&lt;/log-topic&gt;
    &lt;pool-params&gt;
      &lt;pool-init&gt;0&lt;/pool-init&gt;
      &lt;pool-min&gt;0&lt;/pool-min&gt;
      &lt;pool-max&gt;100&lt;/pool-max&gt;
      &lt;pool-max-age&gt;0&lt;/pool-max-age&gt;
      &lt;pstmt-max&gt;20&lt;/pstmt-max&gt;
    &lt;/pool-params&gt;
    &lt;jdbc-conn-params&gt;
      &lt;jdbc_check-level&gt;2&lt;/jdbc_check-level&gt;
      &lt;jdbc-test-statement&gt;select 1&lt;/jdbc-test-statement&gt;
    &lt;/jdbc-conn-params&gt;
    &lt;jonas-config-property&gt;
      &lt;jonas-config-property-name&gt;url&lt;/jonas-config-property-name&gt;
      &lt;jonas-config-property-value&gt;jdbc:oracle:thin:@test:1521:DB1&lt;/jonas-config-property-value&gt;
    &lt;/jonas-config-property&gt;
    .
    .
  &lt;/jonas-resource&gt;</pre>

<h2><a name="PG_Connector-Packaging"></a>Resource Adapter (RAR) Packaging</h2>

<p>Resource Adapters are packaged for deployment in a standard Java
programming language Archive file called an <em>RAR</em> file (Resource
Adapter ARchive). This file can contain the following:</p>
<dl>
  <dt><strong>Resource Adapters' deployment descriptor</strong></dt>
    <dd>The RAR file must contain the deployment descriptors, which are made
      up of:
      <ul>
        <li>The standard xml deployment descriptor, in the format defined in
          the J2EE 1.4 specification. Refer to
          <code>$JONAS_ROOT/xml/connector_1_5.xsd</code>&nbsp;or&nbsp;
          <a href="http://java.sun.com/xml/ns/j2ee/connector_1_5.xsd">http://java.sun.com/xml/ns/j2ee/connector_1_5.xsd</a>.
          This deployment descriptor must be stored with the name <em>META-INF/ra.xml</em> in
          the RAR file.</li>
        <li>The JOnAS-specific XML deployment descriptor in the format
          defined in <em>$JONAS_ROOT/xml/jonas-ra_X_Y.xsd</em>. This JOnAS
          deployment descriptor must be stored with the name
          <em>META-INF/jonas-ra.xml</em> in the RAR file.</li>
      </ul>
    </dd>
</dl>
<dl>
  <dt><strong>Resource adapter components (jar)</strong></dt>
    <dd>One or more jars which contain the java interfaces, implementation,
      and utility classes required by the resource adapter.</dd>
  <dt><strong>Platform-specific native libraries</strong></dt>
    <dd>One or more native libraries used by the resource adapter</dd>
  <dt><strong>Misc</strong></dt>
    <dd>One or more html, image files, or locale files used by the resource
      adapter.</dd>
</dl>

<p>Before deploying an RAR file, the JOnAS-specific XML must be configured
and added. Refer to the <code><b><a href="#PG_Connector-RAConfig">RAConfig
section</a></b></code> for information.</p>

<h2><a name="PG_Connector-Use"></a>Use and Deployment of a Resource
Adapter</h2>

<p>Accessing Resource Adapter involves the following steps:</p>
<ul>
  <li>The bean provider must specify the connection factory requirements by
    declaring a <i>resource manager connection factory reference</i> in its
    EJB deployment descriptor. For example:
    <pre>      &lt;resource-ref&gt;
        &lt;res-ref-name&gt;eis/MyEIS&lt;/res-ref-name&gt;
        &lt;res-type&gt;javax.resource.cci.ConnectionFactory&lt;/res-type&gt;
        &lt;res-auth&gt;Container&lt;/res-auth&gt;
      &lt;/resource-ref&gt;
    </pre>
    The mapping to the actual JNDI name of the <i>connection factory</i>
    (here <code>adapt_1</code>) is done in the JOnAS-specific deployment
    descriptor with the following element:
    <pre>      &lt;jonas-resource&gt;
        &lt;res-ref-name&gt;eis/MyEIS&lt;/res-ref-name&gt;
        &lt;jndi-name&gt;adapt_1&lt;/jndi-name&gt;
      &lt;/jonas-resource&gt;</pre>
    <p>This means that the bean programmer will have access to a
    <i>connection factory</i> instance using the JNDI interface via the
    <em>java:comp/env/eis/MyEIS</em> name:</p>
    <pre>     // obtain the initial JNDI naming context
     Context inictx = new InitialContext();

     // perform JNDI lookup to obtain the connection factory
     javax.resource.cci.ConnectionFactory cxf =
             (javax.resource.cci.ConnectionFactory)
                  inictx .lookup("java:comp/env/eis/MyEIS");</pre>
    The bean programmer can then get a connection by calling the method
    <code>getConnection</code> on the <i>connection factory</i>.
    <pre>      javax.resource.cci.Connection cx = cxf.getConnection();
    </pre>
    The returned connection instance represents an application-level handle
    to a physical connection for accessing the underlying EIS. <br>
    After finishing with the connection, it must be closed using the
    <code>close</code> method on the <code>Connection</code> interface:
    <pre>         cx.close();
    </pre>
  </li>
  <li>
   <ul>
    <li><a name="PG_Connector-RAConfig"></a>The resource adapter must be deployed before being used by the
      application. Deploying the resource adapter requires the following:
      <ul>
        <li><p>Build a <i>JOnAS-specific resource adapter configuration</i>
          file that will be included in the resource adapter. <br>
          This jonas-ra XML file is used to configure the resource adapter in
          the operational environment and reflects the values of all
          properties declared in the deployment descriptor for the resource
          adapter, plus additional JOnAS-specific configuration properties.
          JOnAS provides a deployment tool <code><b><a
          href="command_guide.html#N10328">RAConfig</a></b></code> that is
          capable of building this XML file from an RA deployment descriptor
          inside an RAR file. Example:</p>
          <pre>           RAConfig -path . -j adap_1 ra
        </pre>
          These properties may be specific for each resource adapter and its
          underlying EIS. They are used to configure the resource adapter via
          its <code>managedConnectionFactory</code> class. It is mandatory
          that this class provide getter and setter method for each of its
          supported properties (as it is required in the Connector
          Architecture specification).
          <p>After configuring the jonas-ra.xml file created above, it can be
          added to the resource adapter by executing the following:</p>
          <pre>           RAConfig -u jonas-ra.xml ra
        </pre>
          This will add the xml file to the ra.rar file, which is now ready
          for deployment.</li>
      </ul>
    </li>
    <li>The JOnAS <i>resource service</i> must be configured and started at
      JOnAS launching time: <br>
      In the <code>jonas.properties</code> file:
      <ul>
        <li>Verify that the name <code>resource</code> is included in the
          <code>jonas.services</code> property.</li>
        <li>Use one of the following methods to deploy an RAR file:
          <ul>
            <li>The names of the <i>resource adapter</i> files (the '.rar'
              suffix is optional) must be added in the list of Resource
              Adapters to be used in the
              <code>jonas.service.resource.resources</code> property. If the
              '.rar' suffix is not used on the property, it will be used when
              trying to allocate the specified Resource Adapter.
              <pre>         jonas.service.resource.resources  MyEIS.rar, MyEIS1
      </pre>
            </li>
            <li>Place the RAR file in the connectors autoload directory of
              $JONAS_BASE, default value is $JONAS_BASE/rars/autoload. Note
              that it may be different if
              <code>jonas.service.resource.autoload</code> in
              jonas.properties is configured differently.</li>
            <li>Add the RAR via the "<code>jonas admin -a xxx.rar</code>"
              command.</li>
            <li>Add the RAR via the JonasAdmin console.</li>
          </ul>
        </li>
      </ul>
    </li>
   </ul>
  </li>
</ul>

<h2><a name="PG_Connector-JDBC"></a>JDBC Resource Adapters</h2>

<p>These generic JDBC resource adapters are supplied by JOnAS and are a
replacement for the DBM service.  Refer to <code><b><a
href="configuration_guide.html#config.ra">Configuring JDBC Resource
Adapters</a></b></code> for a complete description and usage guide.</p>

<h2><a name="PG_Connector-JCA"></a>Appendix: Connector Architecture
Principles</h2>

<p>The Java Connector Architecture allows the connection of different
Enterprise Information Systems (EIS) to an application server such as JOnAS.
It defines a way for enterprise applications (based on EJB, servlet, JSP or
J2EE clients) to communicate with existing EIS. This requires the use of a
third party software component called "Resource Adapter" for each type of
EIS, which should be previously deployed on the application server. The
Resource Adapter is an architecture component comparable to a software
driver, which connects the EIS, the application server, and the enterprise
application (J2EE components in the case of JOnAS as application server). The
RA provides an interface (the Common Client Interface or CCI) to the
enterprise application (J2EE components) for accessing the EIS. The RA also
provides standard interfaces for plugging into the application server, so
that they can collaborate to keep all system-level mechanisms (transactions,
security, and connection management) transparent from the application
components.</p>
<img src="../../resources/images/jcaarch.gif" width="720" height="540" border="0"
alt="JCA Architecture">

<p>The resource adapter plugs into JOnAS and provides connectivity between
the EIS, JOnAS, and the application. The application performs "business
logic" operations on the EIS data using the RA client API (CCI), while
transactions, connections (including pooling), and security on the EIS is
managed by JOnAS through the RA (system contract).</p>
</body>
</html>
