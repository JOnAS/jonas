<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"
                      "http://www.w3.org/TR/REC-html40/loose.dtd">
<!--
- JOnAS: Java(TM) Open Application Server
- Copyright (C) 2003 Bull S.A.
- Contact: jonas-team@objectweb.org

- This work is licensed under the Creative Commons
- Attribution-ShareAlike License. To view a copy of this license,
- visit http://creativecommons.org/licenses/by-sa/2.0/deed.en or
- send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford,
- California 94305, USA.

- Authors: Fran�ois Exertier, Jean-Fr�d�ric Mesnil

- $Id$
-->
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <link rel="stylesheet" href="common.css">
  <title>Security Management</title>
</head>

<body lang="en">
<h1><a name="PG_Security"></a>EJB Programmer's Guide: Security Management</h1>

<h2><a name="PG_Security-Target"></a>Target Audience and Content</h2>

<p>The target audience for this guide is the Enterprise Bean provider, i.e.
the person in charge of developing the software components on the server
side. It explains how security behavior should be defined.</p>

<p>The content of this guide is the following:</p>
<ol>
  <li><a href="#PG_Security-Target">Target Audience and Content</a></li>
  <li><a href="#PG_Security-Introducti">Introduction</a></li>
  <li><a href="#PG_Security-Sec_Declarative">Declarative Security 
    Management</a></li>
  <li><a href="#PG_Security-Sec_Programmatic">Programmatic Security
    Management</a></li>
</ol>

<h2><a name="PG_Security-Introducti"></a>Introduction</h2>

<p>The EJB architecture encourages the Bean programmer to implement the
enterprise bean class without hard-coding the security policies and
mechanisms into the business methods.</p>

<h2><a name="PG_Security-Sec_Declarative"></a>Declarative Security
Management</h2>

<p>The application assembler can define a <em>security view</em> of the
enterprise beans contained in the ejb-jar file.<br>
The security view consists of a set of <em>security roles</em>. A security
role is a semantic grouping of permissions for a given type of application
user that allows that user to successfully use the application.<br>
The application assembler can define (declaratively in the deployment
descriptor) <em>method permissions</em> for each security role. A method
permission is a permission to invoke a specified group of methods for the
enterprise beans' home and remote interfaces.<br>
The security roles defined by the application assembler present this
simplified security view of the enterprise beans application to the deployer;
the deployer's view of security requirements for the application is the small
set of security roles, rather than a large number of individual methods.</p>

<h3>Security roles</h3>

<p>The application assembler can define one or more <em>security roles</em>
in the deployment descriptor. The application assembler then assigns groups
of methods of the enterprise beans' home and remote interfaces to the
security roles in order to define the security view of the application.</p>

<p>The scope of the security roles defined in the <code>security-role</code>
elements is the ejb-jar file level, and this includes all the enterprise
beans in the ejb-jar file.</p>
<pre>   ...
   &lt;assembly-descriptor&gt;
      &lt;security-role&gt;
         &lt;role-name&gt;tomcat&lt;/role-name&gt;
      &lt;/security-role&gt;
      ...
   &lt;/assembly-descriptor&gt;
    </pre>

<h3>Method permissions</h3>

<p>After defining security roles for the enterprise beans in the ejb-jar
file, the application assembler can also specify the methods of the remote
and home interfaces that each security role is allowed to invoke.</p>

<p>Method permissions are defined as a binary relationship in the deployment
descriptor from the set of security roles to the set of methods of the home
and remote interfaces of the enterprise beans, including all their super
interfaces (including the methods of the <code>javax.ejb.EJBHome</code> and
<code>javax.ejb.EJBObject</code> interfaces). The method permissions
relationship includes the pair <em>(R, M)</em> only if the security role
<em>R</em> is allowed to invoke the method <em>M</em>.</p>

<p>The application assembler defines the method permissions relationship in
the deployment descriptor using the <code>method-permission</code> element as
follows:</p>
<ul>
  <li>Each <code>method-permission</code> element includes a list of one or
    more security roles and a list of one or more methods. All the listed
    security roles are allowed to invoke all the listed methods. Each
    security role in the list is identified by the <code>role-name</code>
    element, and each method is identified by the <code>method</code>
  element.</li>
  <li>The method permissions relationship is defined as the union of all the
    method permissions defined in the individual
    <code>method-permission</code> elements.</li>
  <li>A security role or a method can appear in multiple
    <code>method-permission</code> elements.</li>
</ul>

<p>It is possible that some methods are not assigned to any security roles.
This means that these methods can be accessed by anyone.</p>

<p>The following example illustrates how security roles are assigned to
methods' permissions in the deployment descriptor:</p>
<pre>   ...
   &lt;method-permission&gt;
      &lt;role-name&gt;tomcat&lt;/role-name&gt;
      &lt;method&gt;
         &lt;ejb-name&gt;Op&lt;/ejb-name&gt;
         &lt;method-name&gt;*&lt;/method-name&gt;
      &lt;/method&gt;
   &lt;/method-permission&gt;
   ...
    </pre>

<h2><a name="PG_Security-Sec_Programmatic"></a>Programmatic Security
Management</h2>

<p>Because not all security policies can be expressed declaratively, the EJB
architecture also provides a simple programmatic interface that the Bean
programmer can use to access the security context from the business
methods.</p>

<p>The <code>javax.ejb.EJBContext</code> interface provides two methods that
allow the Bean programmer to access security information about the enterprise
bean's caller.</p>
<pre>public interface javax.ejb.EJBContext {
   ...
   //
   // The following two methods allow the EJB class
   // to access security information
   //
   java.security.Principal getCallerPrincipal() ;
   boolean isCallerInRole (String roleName) ;
   ...
}
    </pre>

<h3>Use of getCallerPrincipal()</h3>

<p>The purpose of the <code>getCallerPrincipal()</code> method is to allow
the enterprise bean methods to obtain the current caller principal's name.
The methods might, for example, use the name as a key to access information
in a database.</p>

<p>An enterprise bean can invoke the <code>getCallerPrincipal()</code> method
to obtain a <code>java.security.Principal</code> interface representing the
current caller. The enterprise bean can then obtain the distinguished name of
the caller principal using the <code>getName()</code> method of the
<code>java.security.Principal</code> interface.</p>

<h3>Use of isCallerInRole(String roleName)</h3>

<p>The main purpose of the <code>isCallerInRole(String roleName)</code>
method is to allow the Bean programmer to code the security checks that
cannot be easily defined declaratively in the deployment descriptor using
method permissions. Such a check might impose a role-based limit on a
request, or it might depend on information stored in the database.</p>

<p>The enterprise bean code uses the <code>isCallerInRole(String
roleName)</code> method to test whether the current caller has been assigned
to a given security role or not. Security roles are defined by the
application assembler in the deployment descriptor and are assigned to
principals by the deployer.</p>

<h3>Declaration of security roles referenced from the bean's code</h3>

<p>The Bean programmer must declare in the <code>security-role-ref</code>
elements of the deployment descriptor all the security role names used in the
enterprise bean code. Declaring the security roles' references in the code
allows the application assembler or deployer to link the names of the
security roles used in the code to the actual security roles defined for an
assembled application through the <code>security-role</code> elements.</p>
<pre>   ...
   &lt;enterprise-beans&gt;
      ...
      &lt;session&gt;
         &lt;ejb-nameOp&lt;/ejb-name&gt;
         &lt;ejb-class&gt;sb.OpBean&lt;/ejb-class&gt;
         ...
         &lt;security-role-ref&gt;
            &lt;role-name&gt;role1&lt;/role-name&gt;
         &lt;/security-role-ref&gt;
         ...
      &lt;/session&gt;
      ...
   &lt;/enterprise-beans&gt;
   ...
    </pre>

<p>The deployment descriptor in this example indicates that the enterprise
bean <code>Op</code> makes the security checks using
<code>isCallerInRole("role1")</code> in at least one of its business
methods.</p>

<h3>Linking security role references and security roles</h3>

<p>If the <code>security-role</code> elements have been defined in the
deployment descriptor, all the security role references declared in the
<code>security-role-ref</code> elements must be linked to the security roles
defined in the <code>security-role</code> elements.</p>

<p>The following deployment descriptor example shows how to link the security
role references named <code>role1</code> to the security role named
<code>tomcat</code>.</p>

<p></p>
<pre>   ...
   &lt;enterprise-beans&gt;
      ...
      &lt;session&gt;
         &lt;ejb-name&gt;Op&lt;/ejb-name&gt;
         &lt;ejb-class&gt;sb.OpBean&lt;/ejb-class&gt;
         ...
         &lt;security-role-ref&gt;
            &lt;role-name&gt;role1&lt;/role-name&gt;
            &lt;role-link&gt;tomcat&lt;/role-link&gt;
         &lt;/security-role-ref&gt;
         ...
      &lt;/session&gt;
      ...
   &lt;/enterprise-beans&gt;
   ...
    </pre>

<p>In summary, the role names used in the EJB code (in the isCallerInRole
method) are, in fact, references to actual security roles, which makes the
EJB code independent of the security configuration described in the
deployment descriptor. The programmer makes these role references available
to the Bean deployer or application assembler via the
<code>security-role-ref</code> elements included in the <code>session</code>
or <code>entity</code> elements of the deployment descriptor. Then, the Bean
deployer or application assembler must map the security roles defined in the
deployment descriptor to the "specific" roles of the target operational
environment (e.g. groups on Unix systems). However, this last mapping step is
not currently available in JOnAS.</p>
</body>
</html>
