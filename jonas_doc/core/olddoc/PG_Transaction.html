<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"
                      "http://www.w3.org/TR/REC-html40/loose.dtd">
<!--
- JOnAS: Java(TM) Open Application Server
- Copyright (C) 2003 Bull S.A.
- Contact: jonas-team@objectweb.org

- This work is licensed under the Creative Commons
- Attribution-ShareAlike License. To view a copy of this license,
- visit http://creativecommons.org/licenses/by-sa/2.0/deed.en or
- send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford,
- California 94305, USA.

- Author: Philippe DURIEUX

- $Id$
-->
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <link rel="stylesheet" href="common.css">
  <title>Transactional Behaviour</title>
</head>

<body lang="en">
<h1><a name="PG_Transaction"></a>EJB Programmer's Guide: Transactional
Behaviour</h1>

<h2><a name="PG_Transaction-Target"></a>Target Audience and Content</h2>

<p>The target audience for this guide is the Enterprise Bean provider, i.e.
the person in charge of developing the software components on the server
side. It describes how to define the transactional behaviour of an EJB
application.</p>

<p>The content of this guide is the following:</p>
<ol>
  <li><a href="#PG_Transaction-Target">Target Audience and Content</a></li>
  <li><a href="#PG_Transaction-Declarativ">Declarative Transaction
    Management</a></li>
  <li><a href="#PG_Transaction-BeanMana">Bean-managed Transaction</a></li>
  <li><a href="#PG_Transaction-Distribute">Distributed Transaction
    Management</a></li>
</ol>

<h2><a name="PG_Transaction-Declarativ"></a>Declarative Transaction
Management</h2>

<p>For container-managed transaction management, the transactional behaviour
of an enterprise bean is defined at configuration time and is part of the
assembly-descriptor element of the standard deployment descriptor. It is
possible to define a common behaviour for all the methods of the bean, or to
define the behaviour at the method level. This is done by specifying a
transactional attribute, which can be one of the following:</p>
<ul>
  <li><strong>NotSupported:</strong> if the method is called within a
    transaction, this transaction is suspended during the time of the method
    execution.</li>
  <li><strong>Required</strong>: if the method is called within a
    transaction, the method is executed in the scope of this transaction,
    else, a new transaction is started for the execution of the method, and
    committed before the method result is sent to the caller.</li>
  <li><strong>RequiresNew</strong>: the method will always be executed within
    the scope of a new transaction. The new transaction is started for the
    execution of the method, and committed before the method result is sent
    to the caller. If the method is called within a transaction, this
    transaction is suspended before the new one is started and resumed when
    the new transaction has completed.</li>
  <li><strong>Mandatory</strong>: the method should always be called within
    the scope of a transaction, else the container will throw the
    <i>TransactionRequired</i> exception.</li>
  <li><strong>Supports</strong>: the method is invoked within the caller
    transaction scope; if the caller does not have an associated transaction,
    the method is invoked without a transaction scope.</li>
  <li><strong>Never</strong>: The client is required to call the bean without
    any transaction context; if it is not the case, a
    java.rmi.RemoteException is thrown by the container.</li>
</ul>
This is illustrated in the following table:

<table border="1">
  <tbody>
    <tr>
      <th><b>Transaction Attribute</b>&nbsp;</th>
      <th><b>Client transaction</b>&nbsp;</th>
      <th><b>Transaction associated with enterprise Bean's
      method</b>&nbsp;</th>
    </tr>
    <tr>
      <td>NotSupported&nbsp;</td>
      <td>-&nbsp; 

        <p>T1</p>
      </td>
      <td>-&nbsp; 

        <p>-</p>
      </td>
    </tr>
    <tr>
      <td>Required&nbsp;</td>
      <td>-&nbsp; 

        <p>T1</p>
      </td>
      <td>T2&nbsp; 

        <p>T1</p>
      </td>
    </tr>
    <tr>
      <td>RequiresNew&nbsp;</td>
      <td>-&nbsp; 

        <p>T1</p>
      </td>
      <td>T2&nbsp; 

        <p>T2</p>
      </td>
    </tr>
    <tr>
      <td>Mandatory&nbsp;</td>
      <td>-&nbsp; 

        <p>T1</p>
      </td>
      <td>error&nbsp; 

        <p>T1</p>
      </td>
    </tr>
    <tr>
      <td>Supports</td>
      <td>-&nbsp; 

        <p>T1</p>
      </td>
      <td>-&nbsp; 

        <p>T1</p>
      </td>
    </tr>
    <tr>
      <td>Never</td>
      <td>-

        <p>T1</p>
      </td>
      <td>-

        <p>error</p>
      </td>
    </tr>
  </tbody>
</table>

<p></p>

<p>In the deployment descriptor, the specification of the transactional
attributes appears in the assembly-descriptor as follows:</p>
<pre>  &lt;assembly-descriptor&gt;
    &lt;container-transaction&gt;
      &lt;method&gt;
      &lt;ejb-name&gt;AccountImpl&lt;/ejb-name&gt;
      &lt;method-name&gt;*&lt;/method-name&gt;
      &lt;/method&gt;
      &lt;trans-attribute&gt;Supports&lt;/trans-attribute&gt;
    &lt;/container-transaction&gt;
    &lt;container-transaction&gt;
      &lt;method&gt;
      &lt;ejb-name&gt;AccountImpl&lt;/ejb-name&gt;
      &lt;method-name&gt;getBalance&lt;/method-name&gt;
      &lt;/method&gt;
      &lt;trans-attribute&gt;Required&lt;/trans-attribute&gt;
    &lt;/container-transaction&gt;
    &lt;container-transaction&gt;
      &lt;method&gt;
      &lt;ejb-name&gt;AccountImpl&lt;/ejb-name&gt;
      &lt;method-name&gt;setBalance&lt;/method-name&gt;
      &lt;/method&gt;
      &lt;trans-attribute&gt;Mandatory&lt;/trans-attribute&gt;
    &lt;/container-transaction&gt;
  &lt;/assembly-descriptor&gt;
    </pre>

<p>In this example, for all methods of the AccountImpl bean which are not
explicitly specified in a container-transaction element, the default
transactional attribute is Supports (defined at the bean-level), and the
transactional attributes are Required and Mandatory (defined at the
method-name level) for the methods getBalance and setBalance respectively.</p>

<h2><a name="PG_Transaction-BeanMana"></a>Bean-managed Transaction</h2>

<p>A bean that manages its transactions itself must set the
<tt>transaction-type</tt> element in its standard deployment descriptor
to:</p>
<pre>  &lt;transaction-type&gt;Bean&lt;/transaction-type&gt;
    </pre>

<p>To demarcate the transaction boundaries in a bean with bean-managed
transactions, the bean programmer should use the
<em>javax.transaction.UserTransaction</em> interface, which is defined on an
EJB server object that may be obtained using the
<em>EJBContext.getUserTransaction()</em> method (the SessionContext object or
the EntityContext object depending on whether the method is defined on a
session or on an entity bean). The following example shows a session bean
method "doTxJob" demarcating the transaction boundaries; the UserTransaction
object is obtained from the sessionContext object, which should have been
initialized in the setSessionContext method (refer to the <a
href="PG_Session.html#PG_Session-Example">example of the session
bean</a>).</p>
<pre>public void doTxJob() throws  RemoteException {
     UserTransaction ut = sessionContext.getUserTransaction();
     ut.begin();
     ... // transactional operations
     ut.commit();
}</pre>

<p>Another way to do this is to use JNDI and to retrieve UserTransaction with
the name <tt>java:comp/UserTransaction</tt> in the initial context.</p>

<h2><a name="PG_Transaction-Distribute"></a>Distributed Transaction
Management</h2>

<p>As explained in the previous section, the transactional behaviour of an
application can be defined in a declarative way or coded in the bean and/or
the client itself (transaction boundaries demarcation). In any case, the
distribution aspects of the transactions are completely transparent to the
bean provider and to the application assembler. This means that a transaction
may involve beans located on several JOnAS servers and that the platform
itself will handle management of the global transaction. It will perform the
two-phase commit protocol between the different servers, and the bean
programmer need do nothing.</p>

<p>Once the beans have been developed and the application has been assembled,
it is possible for the deployer and for the administrator to configure the
distribution of the different beans on one or several machines, and within
one or several JOnAS servers. This can be done without impacting either the
beans code or their deployment descriptors. The distributed configuration is
specified at launch time. In the environment properties of an EJB server, the
following can be specified:</p>
<ul>
  <li>which enterprise beans the JOnAS server will handle,</li>
  <li>if a Java Transaction Monitor will be located in the same Java Virtual
    Machine (JVM) or not.</li>
</ul>

<p>To achieve this goal, two properties must be set in the <em>
jonas.properties</em> file, <em>jonas.service.ejb.descriptors</em> and
<em>jonas.service.jtm.remote</em>. The first one lists the beans that will be
handled on this server (by specifying the name of their ejb-jar files), and
the second one sets the Java Transaction Monitor (JTM) launching mode:</p>
<ul>
  <li>if set to <tt>true</tt>, the JTM is remote, i.e. the JTM must be
    launched previously in another JVM,</li>
  <li>if set to <tt>false</tt>, the JTM is local, i.e. it will run in the
    same JVM as the EJB Server.</li>
</ul>

<p>Example:</p>
<pre>  jonas.service.ejb.descriptors       Bean1.jar, Bean2.jar
  jonas.service.jtm.remote            false
    </pre>

<p>The Java Transaction Monitor can run outside any EJB server, in which case
it can be launched in a stand-alone mode using the following command:</p>
<pre>TMServer</pre>

<p></p>

<p>Using these configuration facilities, it is possible to adapt the beans
distribution to the resources (cpu and data) location, for optimizing
performance.</p>

<p>The following figure illustrates four cases of distribution configuration
for three beans.</p>

<p><img src="../../resources/images/WP_distr.gif" alt="Figure illustrating beans distr"
width="450" height="337"></p>
<ol>
  <li>Case 1: The three beans B1, B2, and B3 are located on the same JOnAS
    server, which embeds a Java Transaction Monitor.</li>
  <li>Case 2: The three beans are located on different JOnAS servers, one of
    them running the Java Transaction Monitor, which manages the global
    transaction.</li>
  <li>Case 3: The three beans are located on different JOnAS servers, the
    Java Transaction Monitor is running outside of any JOnAS server.</li>
  <li>Case 4: The three beans are located on different JOnAS servers. Each
    server is running a Java Transaction Monitor. One of the JTM acts as the
    master monitor, while the two others are slaves.</li>
</ol>

<p>These different configuration cases may be obtained by launching the JOnAS
servers and eventually the JTM (case 3) with the adequate properties. The
rational when choosing one of these configurations is resources location and
load balancing. However, consider the following pointers:</p>
<ul>
  <li>if the beans should run on the same machine, with the same server
    configuration, case 1 is the more appropriate;</li>
  <li>if the beans should run on different machines, case 4 is the more
    appropriate, since it favours local transaction management;</li>
  <li>if the beans should run on the same machine, but require different
    server configurations, case 2 is a good approach.</li>
</ul>
</body>
</html>
