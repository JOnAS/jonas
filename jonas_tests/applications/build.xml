<?xml version="1.0"?>

<!--
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 - JOnAS: Java(TM) Open Application Server
 - Copyright (C) 2005 Bull S.A.
 - Contact: jonas-team@objectweb.org
 -
 - This library is free software; you can redistribute it and/or
 - modify it under the terms of the GNU Lesser General Public
 - License as published by the Free Software Foundation; either
 - version 2.1 of the License, or any later version.
 -
 - This library is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 - Lesser General Public License for more details.
 -
 - You should have received a copy of the GNU Lesser General Public
 - License along with this library; if not, write to the Free Software
 - Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 - USA
 -
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 - $Id$
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 -->

<project name="JOnAS Junit Test Suite - Applications" default="report" basedir=".">


    <!-- Set the customizable properties -->
    <property file="../build.properties" />

    <property environment="myenv" />
    <property name="jonas.root" value="${myenv.JONAS_ROOT}" />
    <property file="${jonas.root}/build.properties" />
    <property name="http.port" value="${myenv.HTTP_PORT}" />
    <property name="httpunit.home" value="${myenv.HTTPUNIT_HOME}" />
    <property name="javac.encoding" value="iso8859-1"/>

    <!-- Set the uncustomizable properties -->
    <property name="src.dir" value="src" />
    <property name="classes.dir" value="classes" />
    <property name="report.dir" value="report" />
    <property name="lib.dir" value="${jonas.root}/lib" />
    <property name="style.name" value="standalone" />

    <target name="init">
        <mkdir dir="${classes.dir}" />
        <condition property="jonas.base" value="${myenv.JONAS_BASE}">
            <available file="${myenv.JONAS_BASE}" />
        </condition>
        <condition property="jonas.base" value="${jonas.root}">
            <available file="${jonas.root}" />
        </condition>
        <property file="${jonas.base}/conf/carol.properties" />
        <property file="${jonas.base}/conf/jonas.properties" />
        <property file="${jonas.root}/.package" />
        <!-- base.classpath -->
        <path id="base.classpath">
            <pathelement location="${classes.dir}" />
            <pathelement path="${jonas.base}/conf" />
            <pathelement path="${myenv.CLASSPATH}" />
            <fileset dir="${httpunit.home}/jars">
                <include name="*.jar" />
            </fileset>
            <fileset dir="${httpunit.home}/lib">
                <include name="*.jar" />
            </fileset>
            <fileset dir="${lib.dir}">
                <include name="client.jar" />
            </fileset>
        </path>
        <tstamp>
            <format property="dateofday" pattern="d-MMMM-yyyy" locale="en" />
        </tstamp>
    </target>

    <target name="clean" description="Removes all the generated files or directories">
        <delete dir="${classes.dir}" />
        <delete dir="${report.dir}" />
    </target>

    <target name="compile" description="Compile all the JOnAS JUnit Tests" depends="init">
        <javac srcdir="${src.dir}"
               destdir="${classes.dir}"
               debug="${opt.javac.debug}"
               optimize="${opt.javac.optimize}"
               encoding="${javac.encoding}">
            <include name="**" />
            <!-- should be base.classpath, but tests use some specific classes like TransactionService -->
            <classpath refid="base.classpath" />
        </javac>
    </target>

    <property name="test.name" value="**/F_*.class" />
    <target name="dotests" depends="compile">
        <junit printsummary="yes" haltonfailure="no" haltonerror="no" fork="yes">
            <classpath refid="base.classpath" />
            <sysproperty key="jonas.root"
                         value="${jonas.root}" />
            <sysproperty key="jonas.base"
                         value="${jonas.base}" />
            <sysproperty key="http.port"
                         value="${http.port}" />
            <sysproperty key="java.security.policy"
                         value="${jonas.base}/conf/java.policy" />
            <sysproperty key="javax.rmi.CORBA.PortableRemoteObjectClass"
                         value="org.ow2.carol.rmi.multi.MultiPRODelegate" />
            <sysproperty key="java.naming.factory.initial"
                         value="org.ow2.jonas.client.naming.ClientInitialContextFactory" />
            <sysproperty key="java.naming.factory.url.pkgs"
                         value="org.ow2.jonas.lib.naming" />
            <sysproperty key="org.omg.PortableInterceptor.ORBInitializerClass.org.ow2.jonas.tm.jotm.ots.OTSORBInitializer"
                         value="" />
            <sysproperty key="org.omg.PortableInterceptor.ORBInitializerClass.org.ow2.jonas.security.interceptors.iiop.SecurityInitializer"
                         value="" />
            <sysproperty key="org.omg.PortableInterceptor.ORBInitializerClass.standard_init"
                         value="org.jacorb.orb.standardInterceptors.IORInterceptorInitializer" />
            <sysproperty key="javax.rmi.CORBA.PortableRemoteObjectClass"
                         value="org.ow2.carol.rmi.multi.MultiPRODelegate" />
            <sysproperty key="org.omg.CORBA.ORBClass"
                         value="org.jacorb.orb.ORB" />
            <sysproperty key="org.omg.CORBA.ORBSingletonClass"
                         value="org.jacorb.orb.ORBSingleton" />
            <sysproperty key="java.endorsed.dirs"
                         value="${jonas.root}/lib/endorsed" />
            <sysproperty key="java.security.manager"
                         value="" />
            <sysproperty key="registry"
                               value="${carol.jrmp.url}/" />
            <formatter type="xml" />
            <formatter type="plain" usefile="no" />
            <batchtest fork="yes" todir="${report.dir}">
                <fileset dir="${classes.dir}">
                    <include name="${test.name}" />
                </fileset>
            </batchtest>
        </junit>
    </target>

    <target name="install" depends="compile" description="Install the Monitoring tests" />

    <target name="report" depends="compile" description="Run the junit tests">
        <delete dir="${report.dir}" />
        <mkdir dir="${report.dir}/html" />
        <java classname="org.ow2.jonas.Version" output="${report.dir}/temp">
            <classpath>
                <pathelement path="${lib.dir}/bootstrap/jonas-version.jar" />
                <pathelement path="${lib.dir}/client.jar" />
            </classpath>
        </java>
        <loadfile property="jonas.version" srcFile="${report.dir}/temp" />
        <delete file="${report.dir}/temp" />
        <antcall target="dotests" inheritRefs="true" />
        <junitreport todir="${report.dir}" tofile="TESTS-TestApplications.xml">
            <fileset dir="${report.dir}">
                <include name="TEST-*.xml" />
            </fileset>
            <report format="noframes" todir="${report.dir}/html" styledir="../etc/style/${style.name}" />
        </junitreport>
        <property name="applications.filename" value="${carol.protocols}-${os.name}-${jonas.service.dbm.datasources}-${java.version}-${webcontainer.name}.html" />
        <move file="${report.dir}/html/junit-noframes.html" tofile="${report.dir}/html/${applications.filename}" />
    </target>

</project>
