@Echo off
Rem  ---------------------------------------------------------------------------
Rem  JOnAS: Java(TM) Open Application Server
Rem  Copyright (C) 1999 Bull S.A.
Rem  Contact: jonas-team@objectweb.org
Rem
Rem  This library is free software; you can redistribute it and/or
Rem  modify it under the terms of the GNU Lesser General Public
Rem  License as published by the Free Software Foundation; either
Rem  version 2.1 of the License, or any later version.
Rem
Rem  This library is distributed in the hope that it will be useful,
Rem
Rem  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Rem  Lesser General Public License for more details.
Rem
Rem  You should have received a copy of the GNU Lesser General Public
Rem  License along with this library; if not, write to the Free Software
Rem  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
Rem  USA
Rem
Rem  Initial developer(s): Philippe Durieux
Rem  Contributor(s):
Rem  ---------------------------------------------------------------------------
Rem  $Id$
Rem  ---------------------------------------------------------------------------

Rem Usage:	junit [ test-type [ dir-name [ suite-name [ test-name ] ] ] ]
Rem
Rem This script allows to run the jonas-test all the tests or a sub-set of the tests:
Rem - the tests of the given directory
Rem - the tests of the given suite of the given directory
Rem - only the given test of the given suite of the given directory
Rem for each type test (conformance, stress or ws)
Rem
setlocal ENABLEDELAYEDEXPANSION

set TYPE=conf
set PACK=org.objectweb.jonas.jtests.clients
set DIR=suite
set SUITE=C_suite
set TEST=
set CLIENT_OPTS=
set TEST_CLASSES=%TEST_BASE%\conformance\classes;%TEST_BASE%\examples\classes

if ["%JONAS_BASE%"]==[""] goto set_jonas_base
set CLIENT_OPTS=%CLIENT_OPTS%  -Djonas.base="%JONAS_BASE%"

:set_jonas_base
set CLIENT_OPTS=%CLIENT_OPTS% -Djonas.base="%JONAS_ROOT%"
if ["%REGISTRY%"]==[] goto set_registry
set CLIENT_OPTS=%CLIENT_OPTS% -Dregistry="%REGISTRY%"
:set_registry
set CLIENT_OPTS=%CLIENT_OPTS% -Dregistry="rmi://localhost:1099"
if ["%PROTOCOL%"]==[] goto set_protocol
set CLIENT_OPTS=%CLIENT_OPTS%   -Dprotocol="%PROTOCOL%"
:set_protocol
set CLIENT_OPTS=%CLIENT_OPTS% -Dprotocol=jrmp

if [%HTTP_PORT%]==[] set HTTP_PORT=9000
if not [%1]==[] goto loop_on_args
:loop_on_args


if [%1]==[] goto endargs
if ["%1"]==["-debug"]   goto debug_arg
set TYPE=%1
shift
if [%1]==[] goto endargs
set DIR=%1
set SUITE=C_%DIR%
shift
if [%1]==[] goto endargs
set SUITE=%1
shift
if [%1%]==[] goto varinit
set TEST=-n %1
goto varinit



:debug_arg
set DEBUG_OPTS=%DEBUG_OPTS% %1
shift
if not ["%1"]==["-p"] goto debug_usage
set DEBUG_OPTS=%DEBUG_OPTS% %1
shift
set DEBUG_OPTS=%DEBUG_OPTS% %1
shift
if ["%1"]==["-s"] goto set_debug_opts
goto loop_on_args

:set_debug_opts
set DEBUG_OPTS=%DEBUG_OPTS% %1
shift
set DEBUG_OPTS=%DEBUG_OPTS% %1
shift
goto loop_on_args

:debug_usage
echo -debug option parameters are : "-debug -p <debug-port> [-s <suspend:y/n>]"
goto :EOF

:endargs
echo %TYPE%
echo %DIR%
echo %SUITE%
echo %DEBUG_OPTS%

:varinit
if [%TYPE%]==[stress] goto varstress
if [%TYPE%]==[ws] goto varws
if [%TYPE%]==[examples] goto varexa
:varconf
set PACK=org.objectweb.jonas.jtests.clients
set TEST_CLASSES=%TEST_BASE%\conformance\classes
goto calllabel
:varstress
set PACK=org.objectweb.jonas.stests
set TEST_CLASSES=%TEST_BASE%\stress\classes
goto calllabel
:varws
set PACK=org.objectweb.ws.clients
set TEST_CLASSES=%TEST_BASE%\ws\classes
set JAVA_OPTS=%JAVA_OPTS% -Dhttp.port=%HTTP_PORT% -Daddressbook.dir=%TEST_BASE%\ws\src\org\objectweb\ws\ab_web -Dordering.dir=%TEST_BASE%\ws\src\org\objectweb\ws\ordering_app
goto calllabel
:varexa
set PACK=org.objectweb.jonas.examples.clients
set TEST_CLASSES=%TEST_BASE%\examples\classes
goto calllabel

:calllabel

Set JONAS_CLASSPATH=%TEST_CLASSES%;%ANT_HOME%\lib\junit.jar;%HTTPUNIT_HOME%\lib\httpunit.jar;%HTTPUNIT_HOME%\jars\nekohtml.jar;%HTTPUNIT_HOME%\jars\js.jar;%JONAS_ROOT%\examples\classes

echo on
call jclient %PROPS% %PACK%.%DIR%.%SUITE% %TEST% -security %CLIENT_OPTS% %DEBUG_OPTS%





