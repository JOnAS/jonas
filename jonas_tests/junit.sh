#!/bin/sh
# ---------------------------------------------------------------------------
# JOnAS: Java(TM) Open Application Server
# Copyright (C) 1999 Bull S.A.
# Contact: jonas-team@objectweb.org
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
# USA
#
# ---------------------------------------------------------------------------
# $Id$
# ---------------------------------------------------------------------------

#
# Usage:	junit  [ test-type [ dir-name [ suite-name [ test-name ] ] ] ]
#
# This script allows to run the jonas-test all the tests or a sub-set of the tests:
# - the tests of the given directory
# - the tests of the given suite of the given directory
# - only the given test of the given suite of the given directory
# for each type test (conformance, examples or ws)
#set -x
TYPE=conf
PACK=org.objectweb.jonas.jtests.clients
DIR=suite
SUITE=C_suite
TEST=

# Try to find the directory where the jonas tests are
[ -d $PROJ_BASE/jonas_tests ] && TEST_BASE=$PROJ_BASE/jonas_tests
[ -d $JPROJ_BASE/jonas_tests ] && TEST_BASE=$JPROJ_BASE/jonas_tests

TEST_CLASSES=$TEST_BASE/conformance/classes

PROPS=

# variable needed for ws tests
if [ -z "$HTTP_PORT" ]
then
	HTTP_PORT=9000
fi

# registry url for JMX access
if [ -z "$REGISTRY" ]
then
	REGISTRY=rmi://localhost:1099
fi
# protocol used
if [ -z "$PROTOCOL" ]
then
	PROTOCOL=jrmp
fi
if [ $# -gt 0 ]
then
	TYPE=$1
	shift
fi

case "$TYPE" in
"conf")
	;;
"stress")
	TEST_CLASSES=$TEST_BASE/stress/classes
	PACK=org.objectweb.jonas.stests
	;;
"ws")
	TEST_CLASSES=$TEST_BASE/ws/classes
    	PROPS="-Dhttp.port=$HTTP_PORT"
    	PROPS="$PROPS -Daddressbook.dir=$TEST_BASE/ws/src/org/objectweb/ws/ab_web"
    	PROPS="$PROPS -Dordering.dir=$TEST_BASE/ws/src/org/objectweb/ws/ordering_app"
	PROPS="$PROPS -Djonas.base.client=$JONAS_BASE -Djonas.root=$JONAS_ROOT"

	;;
"examples")
	TEST_CLASSES=$TEST_BASE/examples/classes:$TEST_BASE/examples/base/examples/classes
	PACK=org.objectweb.jonas.examples.clients
	PROPS="-Dhttp.port=$HTTP_PORT"
     	PROPS="$PROPS -Daddressbook.dir=$TEST_BASE/ws/src/org/objectweb/ws/ab_web"
    	PROPS="$PROPS -Dordering.dir=$TEST_BASE/ws/src/org/objectweb/ws/ordering_app"


	;;
*)	
	echo "USAGE : $0 [ test-type [ dir-name [ suite-name [ test-name ] ] ] ]"
	exit
	;;
esac

if [ $# -gt 0 ]
then
	DIR=$1
	SUITE=C_$DIR
	shift
fi

if [ $# -gt 0 ]
then
	SUITE=$1
	shift
fi

if [ $# -gt 0 ]
then
	PREFIX=`echo "$SUITE" | cut -b 1-2`
	if [ "$PREFIX" != "F_" ]  && [ "$PREFIX" != "G_" ]
	then
		echo "ERROR: a test-name may be specified only for a final suite (F_xxx)or (G_xxx)"
		exit 2
	fi
	case $1 in
	-*)
		TEST=$1
		;;
	*)
	    TEST="-n $1"
		;;
	esac
	shift
fi


# update classpath for junit tests and httpunit 
# update the following lines depending on your own environment
for CLS in \
$ANT_HOME/lib/junit.jar \
$HTTPUNIT_HOME/lib/httpunit.jar \
$HTTPUNIT_HOME/jars/nekohtml.jar \
$HTTPUNIT_HOME/jars/js.jar \
$TEST_CLASSES
do
    echo $JONAS_CLASSPATH | grep $CLS >/dev/null || JONAS_CLASSPATH=$JONAS_CLASSPATH:$CLS
done

# important to be seen in jclient script
export JONAS_CLASSPATH


echo $CLASSPATH
echo "Running junit test : $TYPE $DIR/$SUITE $TEST"

jclient $PROPS $PACK.$DIR.$SUITE $TEST -security -Djonas.base=$JONAS_BASE -Dregistry=$REGISTRY -Dprotocol=$PROTOCOL
#jclient -debug -p 4142 -s y $PROPS $PACK.$DIR.$SUITE $TEST -security -Djonas.base=$JONAS_BASE -Dregistry=$REGISTRY -Dprotocol=$PROTOCOL
