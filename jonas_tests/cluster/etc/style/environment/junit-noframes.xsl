<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" indent="yes" encoding="US-ASCII" />
<xsl:decimal-format decimal-separator="." grouping-separator="," />
<!--
 The Apache Software License, Version 1.1

 Copyright (c) 2001-2002 The Apache Software Foundation.  All rights
 reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:

 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution.

 3. The end-user documentation included with the redistribution, if
    any, must include the following acknowlegement:
       "This product includes software developed by the
        Apache Software Foundation (http://www.apache.org/)."
    Alternately, this acknowlegement may appear in the software itself,
    if and wherever such third-party acknowlegements normally appear.

 4. The names "Ant" and "Apache Software
    Foundation" must not be used to endorse or promote products derived
    from this software without prior written permission. For written
    permission, please contact apache@apache.org.

 5. Products derived from this software may not be called "Apache"
    nor may "Apache" appear in their names without prior written
    permission of the Apache Group.

 THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED.  IN NO EVENT SHALL THE APACHE SOFTWARE FOUNDATION OR
 ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.
 ====================================================================

 This software consists of voluntary contributions made by many
 individuals on behalf of the Apache Software Foundation.  For more
 information on the Apache Software Foundation, please see
 <http://www.apache.org/>.
 -->

<!--

 Sample stylesheet to be used with An JUnitReport output.

 It creates a non-framed report that can be useful to send via
 e-mail or such.

 @author Stephane Bailliez <a href="mailto:sbailliez@apache.org"/>
 @author Erik Hatcher <a href="mailto:ehatcher@apache.org"/>

 This sample stylesheet has been customized for the JOnAS test suite
 @author Philippe Coq

-->
<xsl:template match="testsuites">

            <!-- Environment Part -->
            <xsl:call-template name="environment"/>

</xsl:template>





<!-- Environment Part -->
<!--
<xsl:template name="environment">
<h2>Environment</h2>
  <xsl:apply-templates select="testsuite" mode="env"/>
</xsl:template>
 -->

<xsl:template name="environment" >
  <h2>Environment</h2>
  <table width="95%" cellspacing="2" cellpadding="5" border="0" class="details">
  <xsl:variable name="jonasver" select="testsuite[1]/properties/property[@name='jonas.version']/@value"/>
  <xsl:variable name="webcontainer"  select="testsuite[1]/properties/property[@name='webcontainer.name']/@value"/>
  <xsl:variable name="webcpath"  select="testsuite[1]/properties/property[@name='webcontainer.path']/@value"/>
  <xsl:variable name="jdkver" select="testsuite[1]/properties/property[@name='java.version']/@value"/>
  <xsl:variable name="jdkvendor" select="testsuite[1]/properties/property[@name='java.vendor']/@value"/>
  <xsl:variable name="antver" select="testsuite[1]/properties/property[@name='ant.version']/@value"/>
  <xsl:variable name="orb" select="testsuite[1]/properties/property[@name='carol.protocols']/@value"/>
  <xsl:variable name="datasource" select="testsuite[1]/properties/property[@name='jonas.service.dbm.datasources']/@value"/>
  <xsl:variable name="osname" select="testsuite[1]/properties/property[@name='os.name']/@value"/>
  <xsl:variable name="osversion" select="testsuite[1]/properties/property[@name='os.version']/@value"/>
  <xsl:variable name="osarch" select="testsuite[1]/properties/property[@name='os.arch']/@value"/>
  <xsl:variable name="host" select="testsuite[1]/properties/property[@name='myenv.HOSTNAME']/@value"/>
  <xsl:variable name="user" select="testsuite[1]/properties/property[@name='user.name']/@value"/>
  <xsl:variable name="jonasbase" select="testsuite[1]/properties/property[@name='myenv.JONAS_BASE']/@value"/>
  <xsl:variable name="jonasroot" select="testsuite[1]/properties/property[@name='myenv.JONAS_ROOT']/@value"/>
  <xsl:variable name="date" select="testsuite[1]/properties/property[@name='dateofday']/@value"/>
  <tr valign="top">
  <th>JOnAS Version</th><td> <xsl:value-of select="$jonasver" /> </td>
  </tr>
  <tr valign="top">
  <th>Packaging</th><td> <xsl:value-of select="concat($webcontainer,' ', $webcpath)" /> </td>
  </tr>
  <tr valign="top">
  <th>JONAS_ROOT</th><td> <xsl:value-of select="$jonasroot" /> </td>
  </tr>
  <xsl:if test="not(contains($jonasroot,$jonasbase))" >
   <tr valign="top">
  <th>JONAS_BASE</th><td> <xsl:value-of select="$jonasbase" /> </td>
  </tr>
 </xsl:if>
  <tr valign="top">
  <th>Version JDK</th><td> <xsl:value-of select="concat($jdkver,'  (', $jdkvendor,')')" /> </td>
  </tr>
  <tr valign="top">
  <th>Version ANT</th><td> <xsl:value-of select="$antver" /> </td>
  </tr>
  <tr valign="top">
  <th>Protocol</th><td> <xsl:value-of select="$orb" /> </td>
  </tr>
  <tr valign="top">
  <th>DataSource</th><td> <xsl:value-of select="$datasource" /> </td>
  </tr>
  <tr valign="top">
  <th>System</th><td> <xsl:value-of select="concat($osname , '  (', $osversion , '/', $osarch, ')')" /> </td>
  </tr>
  <tr valign="top">
  <th>Host</th><td> <xsl:value-of select="$host" /> </td>
  </tr>
  <tr valign="top">
  <th>Author</th><td> <xsl:value-of select="$user" /> </td>
  </tr>
  <tr valign="top">
  <th>Date</th><td> <xsl:value-of select="$date" /> </td>
  </tr>
  </table>

</xsl:template>



</xsl:stylesheet>

