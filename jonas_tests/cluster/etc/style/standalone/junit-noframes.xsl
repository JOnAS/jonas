<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" indent="yes" encoding="US-ASCII"
  doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN" />
<xsl:decimal-format decimal-separator="." grouping-separator="," />
<!--
 The Apache Software License, Version 1.1

 Copyright (c) 2001-2002 The Apache Software Foundation.  All rights
 reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:

 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution.

 3. The end-user documentation included with the redistribution, if
    any, must include the following acknowlegement:
       "This product includes software developed by the
        Apache Software Foundation (http://www.apache.org/)."
    Alternately, this acknowlegement may appear in the software itself,
    if and wherever such third-party acknowlegements normally appear.

 4. The names "Ant" and "Apache Software
    Foundation" must not be used to endorse or promote products derived
    from this software without prior written permission. For written
    permission, please contact apache@apache.org.

 5. Products derived from this software may not be called "Apache"
    nor may "Apache" appear in their names without prior written
    permission of the Apache Group.

 THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED.  IN NO EVENT SHALL THE APACHE SOFTWARE FOUNDATION OR
 ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.
 ====================================================================

 This software consists of voluntary contributions made by many
 individuals on behalf of the Apache Software Foundation.  For more
 information on the Apache Software Foundation, please see
 <http://www.apache.org/>.
 -->

<!--

 Sample stylesheet to be used with An JUnitReport output.

 It creates a non-framed report that can be useful to send via
 e-mail or such.

 @author Stephane Bailliez <a href="mailto:sbailliez@apache.org"/>
 @author Erik Hatcher <a href="mailto:ehatcher@apache.org"/>

 This sample stylesheet has been customized for the JOnAS test suite
 @author Philippe Coq

-->
<xsl:template match="testsuites">
    <html>
     <head>
      <title>Test Results</title>
      <style type="text/css">
      body {
        background:#ffffff;
        font:normal 68% verdana,arial,helvetica;
        color:#000000;
      }

      table tr td, table tr th {
          font-size: 68%;
      }

      table.details tr th{
        color: #FFFFFF;
        font-weight: bold;
        text-align:left;
        background:#433C7B;
      }

      table.details tr td{
        background:#CCC;
      }

      p {
        line-height:1.5em;
        margin-top:0.5em; margin-bottom:1.0em;
      }

      a
      {
      color: #3F3975;
      background-color: transparent;
      text-decoration: underline;
      }

      a:visited
      {
      color: #9898CB;
      background-color: transparent;
      text-decoration: underline;
      }

      a:hover
      {
      color: #E06611;
      background-color: transparent;
      text-decoration: underline;
      }


      a:active
      {
      color: #FFFFFF;
      background-color: #E06611;
      text-decoration: underline;
      }


      h1
      {
      color: #E06611;
      font-family: Arial, Helvetica, sans-serif;
      font-size: 22px;
      line-height: 27px;
      font-weight: bold;
      border-color: #99C;
      border-width: 0 0 4px 0;
      border-style: none none solid none;
      margin: 10px 0px 5px 0px;
      }

      h2
      {
      color: #99C;
      font-family: Arial, Helvetica, sans-serif;
      font-size: 20px;
      line-height: 27px;
      font-weight: normal;
      border-color: #E06611;
      border-width: 0 0 4px 0;
      border-style: none none solid none;
      margin: 10px 0px 5px 0px;
      }

      h3
      {
      color: #E06611;
      font-family: Arial, Helvetica, sans-serif;
      font-size: 16px;
      line-height: 27px;
      font-weight: bold;
      border-color: #E8EAF0;
      border-width: 0 0 2px 0;
      border-style: none none solid none;
      margin: 10px 0px 5px 0px;
      }

      h4
      {
      color: #99C;
      font-family: Arial, Helvetica, sans-serif;
      font-size: 13px;
      line-height: 24px;
      font-weight: bold;
      border-color: #E8EAF0;
      border-width: 0 0 2px 0;
      border-style: none none solid none;
      margin: 10px 0px 5px 0px;
      }

      h5
      {
      color: #E06611;
      font-family: Arial, Helvetica, sans-serif;
      font-size: 14px;
      line-height: 24px;
      font-weight: normal;
      border-color: #E8EAF0;
      border-width: 0 0 2px 0;
      border-style: none none solid none;
      margin: 10px 0px 5px 0px;
      }

      h6
      {
      color: #99C;
      font-family: Arial, Helvetica, sans-serif;
      font-size: 14px;
      line-height: 22px;
      font-weight: normal;
      border-color: #E8EAF0;
      border-width: 0 0 2px 0;
      border-style: none none solid none;
      margin: 10px 0px 5px 0px;
      }

      .Error {
        font-weight:bold; color:red;
      }
      .Failure {
      font-weight:bold; color:purple;
      }
      .Properties {
      text-align:right;
      }
      </style>
        </head>
        <body>
            <a name="top"></a>
            <xsl:call-template name="pageHeader"/>

            <!-- Environment Part -->
            <xsl:call-template name="environment"/>

            <!-- Summary part -->
            <xsl:call-template name="summary"/>
            <hr size="1" width="95%" align="left"/>
            <!-- Package List part -->
            <xsl:call-template name="packagelist"/>
            <hr size="1" width="95%" align="left"/>

            <!-- For each package create its part -->
            <xsl:call-template name="packages"/>
            <hr size="1" width="95%" align="left"/>

            <!-- For each class create the  part -->
            <xsl:call-template name="classes"/>

        </body>
    </html>
</xsl:template>





<!-- Environment Part -->
<!--
<xsl:template name="environment">
<h2>Environment</h2>
  <xsl:apply-templates select="testsuite" mode="env"/>
</xsl:template>
 -->

<xsl:template name="environment" >
  <h2>Environment</h2>
  <table width="95%" cellspacing="2" cellpadding="5" border="0" class="details">
  <xsl:variable name="jonasver" select="testsuite[1]/properties/property[@name='jonas.version']/@value"/>
  <xsl:variable name="webcontainer"  select="testsuite[1]/properties/property[@name='webcontainer.name']/@value"/>
  <xsl:variable name="webcpath"  select="testsuite[1]/properties/property[@name='webcontainer.path']/@value"/>
  <xsl:variable name="jdkver" select="testsuite[1]/properties/property[@name='java.version']/@value"/>
  <xsl:variable name="jdkvendor" select="testsuite[1]/properties/property[@name='java.vendor']/@value"/>
  <xsl:variable name="antver" select="testsuite[1]/properties/property[@name='ant.version']/@value"/>
  <xsl:variable name="orb" select="testsuite[1]/properties/property[@name='carol.protocols']/@value"/>
  <xsl:variable name="datasource" select="testsuite[1]/properties/property[@name='jonas.service.dbm.datasources']/@value"/>
  <xsl:variable name="osname" select="testsuite[1]/properties/property[@name='os.name']/@value"/>
  <xsl:variable name="osversion" select="testsuite[1]/properties/property[@name='os.version']/@value"/>
  <xsl:variable name="osarch" select="testsuite[1]/properties/property[@name='os.arch']/@value"/>
  <xsl:variable name="host" select="testsuite[1]/properties/property[@name='myenv.HOSTNAME']/@value"/>
  <xsl:variable name="user" select="testsuite[1]/properties/property[@name='user.name']/@value"/>
  <xsl:variable name="jonasbase" select="testsuite[1]/properties/property[@name='myenv.JONAS_BASE']/@value"/>
  <xsl:variable name="jonasroot" select="testsuite[1]/properties/property[@name='myenv.JONAS_ROOT']/@value"/>
  <xsl:variable name="date" select="testsuite[1]/properties/property[@name='dateofday']/@value"/>
  <tr valign="top">
  <th>JOnAS Version</th><td> <xsl:value-of select="$jonasver" /> </td>
  </tr>
  <tr valign="top">
  <th>Packaging</th><td> <xsl:value-of select="concat($webcontainer,' ', $webcpath)" /> </td>
  </tr>
  <tr valign="top">
  <th>JONAS_ROOT</th><td> <xsl:value-of select="$jonasroot" /> </td>
  </tr>
  <xsl:if test="not(contains($jonasroot,$jonasbase))" >
   <tr valign="top">
  <th>JONAS_BASE</th><td> <xsl:value-of select="$jonasbase" /> </td>
  </tr>
 </xsl:if>
  <tr valign="top">
  <th>Version JDK</th><td> <xsl:value-of select="concat($jdkver,'  (', $jdkvendor,')')" /> </td>
  </tr>
  <tr valign="top">
  <th>Version ANT</th><td> <xsl:value-of select="$antver" /> </td>
  </tr>
  <tr valign="top">
  <th>Protocol</th><td> <xsl:value-of select="$orb" /> </td>
  </tr>
  <tr valign="top">
  <th>DataSource</th><td> <xsl:value-of select="$datasource" /> </td>
  </tr>
  <tr valign="top">
  <th>System</th><td> <xsl:value-of select="concat($osname , '  (', $osversion , '/', $osarch, ')')" /> </td>
  </tr>
  <tr valign="top">
  <th>Host</th><td> <xsl:value-of select="$host" /> </td>
  </tr>
  <tr valign="top">
  <th>Author</th><td> <xsl:value-of select="$user" /> </td>
  </tr>
  <tr valign="top">
  <th>Date</th><td> <xsl:value-of select="$date" /> </td>
  </tr>
  </table>

</xsl:template>




    <!-- ================================================================== -->
    <!-- Write a list of all packages with an hyperlink to the anchor of    -->
    <!-- of the package name.                                               -->
    <!-- ================================================================== -->
    <xsl:template name="packagelist">
        <h2>Packages</h2>
        Note: package statistics are not computed recursively, they only sum up all of its testsuites numbers.
        <table class="details" border="0" cellpadding="5" cellspacing="2" width="95%">
            <xsl:call-template name="testsuite.test.header"/>
            <!-- list all packages recursively -->
            <xsl:for-each select="./testsuite[not(./@package = preceding-sibling::testsuite/@package)]">
                <xsl:sort select="@package"/>
                <xsl:variable name="testsuites-in-package" select="/testsuites/testsuite[./@package = current()/@package]"/>
                <xsl:variable name="testCount" select="sum($testsuites-in-package/@tests)"/>
                <xsl:variable name="errorCount" select="sum($testsuites-in-package/@errors)"/>
                <xsl:variable name="failureCount" select="sum($testsuites-in-package/@failures)"/>
                <xsl:variable name="timeCount" select="sum($testsuites-in-package/@time)"/>

                <!-- write a summary for the package -->
                <tr valign="top">
                    <!-- set a nice color depending if there is an error/failure -->
                    <xsl:attribute name="class">
                        <xsl:choose>
                            <xsl:when test="$failureCount &gt; 0">Failure</xsl:when>
                            <xsl:when test="$errorCount &gt; 0">Error</xsl:when>
                        </xsl:choose>
                    </xsl:attribute>
                    <td><a href="#{@package}"><xsl:value-of select="@package"/></a></td>
                    <td><xsl:value-of select="$testCount"/></td>
                    <td><xsl:value-of select="$errorCount"/></td>
                    <td><xsl:value-of select="$failureCount"/></td>
                    <td>
                    <xsl:call-template name="display-time">
                        <xsl:with-param name="value" select="$timeCount"/>
                    </xsl:call-template>
                    </td>
                </tr>
            </xsl:for-each>
        </table>
    </xsl:template>


    <!-- ================================================================== -->
    <!-- Write a package level report                                       -->
    <!-- It creates a table with values from the document:                  -->
    <!-- Name | Tests | Errors | Failures | Time                            -->
    <!-- ================================================================== -->
    <xsl:template name="packages">
        <!-- create an anchor to this package name -->
        <xsl:for-each select="/testsuites/testsuite[not(./@package = preceding-sibling::testsuite/@package)]">
            <xsl:sort select="@package"/>
                <a name="{@package}"></a>
                <h3>Package <xsl:value-of select="@package"/></h3>

                <table class="details" border="0" cellpadding="5" cellspacing="2" width="95%">
                    <xsl:call-template name="testsuite.test.header"/>

                    <!-- match the testsuites of this package -->
                    <xsl:apply-templates select="/testsuites/testsuite[./@package = current()/@package]" mode="print.test"/>
                </table>
                <a href="#top">Back to top</a>
                <p/>
                <p/>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="classes">
        <xsl:for-each select="testsuite">
            <xsl:sort select="@name"/>
            <!-- create an anchor to this class name -->
            <a name="{@name}"></a>
            <h3>TestCase <xsl:value-of select="@name"/></h3>

            <table class="details" border="0" cellpadding="5" cellspacing="2" width="95%">
              <xsl:call-template name="testcase.test.header"/>
              <!--
              test can even not be started at all (failure to load the class)
              so report the error directly
              -->
                <xsl:if test="./error">
                    <tr class="Error">
                        <td colspan="4"><xsl:apply-templates select="./error"/></td>
                    </tr>
                </xsl:if>
                <xsl:apply-templates select="./testcase" mode="print.test"/>
            </table>
            <p/>

            <a href="#top">Back to top</a>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="summary">
        <h2>Summary</h2>

        <xsl:variable name="testCount" select="sum(testsuite/@tests)"/>
        <xsl:variable name="errorCount" select="sum(testsuite/@errors)"/>
        <xsl:variable name="failureCount" select="sum(testsuite/@failures)"/>
        <xsl:variable name="timeCount" select="sum(testsuite/@time)"/>
        <xsl:variable name="successRate" select="($testCount - $failureCount - $errorCount) div $testCount"/>
        <table class="details" border="0" cellpadding="5" cellspacing="2" width="95%">
        <tr valign="top">
            <th>Tests</th>
            <th>Failures</th>
            <th>Errors</th>
            <th>Success rate</th>
            <th>Time</th>
        </tr>
        <tr valign="top">
            <xsl:attribute name="class">
                <xsl:choose>
                    <xsl:when test="$failureCount &gt; 0">Failure</xsl:when>
                    <xsl:when test="$errorCount &gt; 0">Error</xsl:when>
                </xsl:choose>
            </xsl:attribute>
            <td><xsl:value-of select="$testCount"/></td>
            <td><xsl:value-of select="$failureCount"/></td>
            <td><xsl:value-of select="$errorCount"/></td>
            <td>
                <xsl:call-template name="display-percent">
                    <xsl:with-param name="value" select="$successRate"/>
                </xsl:call-template>
            </td>
            <td>
                <xsl:call-template name="display-time">
                    <xsl:with-param name="value" select="$timeCount"/>
                </xsl:call-template>
            </td>

        </tr>
        </table>
        <table border="0" width="95%">
        <tr>
        <td style="text-align: justify;">
        Note: <i>failures</i> are anticipated and checked for with assertions while <i>errors</i> are unanticipated.
        </td>
        </tr>
        </table>
    </xsl:template>

<!-- Page HEADER -->
<xsl:template name="pageHeader">
   <xsl:variable name="project" select="./testsuite/properties/property[@name='ant.project.name']/@value"/>
    <h1><xsl:value-of select="$project" /></h1>
</xsl:template>

<xsl:template match="testsuite" mode="header">
    <tr valign="top">
        <th width="80%">Name</th>
        <th>Tests</th>
        <th>Errors</th>
        <th>Failures</th>
        <th nowrap="nowrap">Time(s)</th>
    </tr>
</xsl:template>

<!-- class header -->
<xsl:template name="testsuite.test.header">
    <tr valign="top">
        <th width="80%">Name</th>
        <th>Tests</th>
        <th>Errors</th>
        <th>Failures</th>
        <th nowrap="nowrap">Time(s)</th>
    </tr>
</xsl:template>

<!-- method header -->
<xsl:template name="testcase.test.header">
    <tr valign="top">
        <th>Testcase</th>
	    <th width="40%">Test</th>
        <th>Status</th>
        <th width="60%">Type</th>
        <th nowrap="nowrap">Time(s)</th>
    </tr>
</xsl:template>


<!-- class information -->
<xsl:template match="testsuite" mode="print.test">
    <tr valign="top">
        <!-- set a nice color depending if there is an error/failure -->
        <xsl:attribute name="class">
            <xsl:choose>
                <xsl:when test="@failures[.&gt; 0]">Failure</xsl:when>
                <xsl:when test="@errors[.&gt; 0]">Error</xsl:when>
            </xsl:choose>
        </xsl:attribute>
        <!-- print testsuite information -->
		      <td><a href="#{@name}"><xsl:value-of select="concat(@name,@classename)"/></a></td>
        <td><xsl:value-of select="@tests"/></td>
        <td><xsl:value-of select="@errors"/></td>
        <td><xsl:value-of select="@failures"/></td>
        <td>
            <xsl:call-template name="display-time">
                <xsl:with-param name="value" select="@time"/>
            </xsl:call-template>
        </td>
    </tr>
</xsl:template>

<xsl:template match="testcase" mode="print.test">
    <tr valign="top">
        <xsl:attribute name="class">
            <xsl:choose>
                <xsl:when test="failure | error">Error</xsl:when>
            </xsl:choose>
        </xsl:attribute>
		<!--
        <td><xsl:value-of select="concat(@name,' (',substring-after(@classname,'org.objectweb.jonas.jtests.clients.'),')')"/></td>
		-->
		<td><xsl:value-of select="@name" />	</td>
	    <!--  cut the first 5 tokens of the package name -->
	    <td><xsl:value-of select="substring-after(substring-after(substring-after(substring-after(substring-after(@classname,'.'),'.'),'.'),'.'),'.')"/></td>
        <xsl:choose>
            <xsl:when test="failure">
                <td>Failure</td>
                <td><xsl:apply-templates select="failure"/></td>
            </xsl:when>
            <xsl:when test="error">
                <td>Error</td>
                <td><xsl:apply-templates select="error"/></td>
            </xsl:when>
            <xsl:otherwise>
                <td>Success</td>
                <td></td>
            </xsl:otherwise>
        </xsl:choose>
        <td>
            <xsl:call-template name="display-time">
                <xsl:with-param name="value" select="@time"/>
            </xsl:call-template>
        </td>
    </tr>
</xsl:template>


<xsl:template match="failure">
    <xsl:call-template name="display-failures"/>
</xsl:template>

<xsl:template match="error">
    <xsl:call-template name="display-failures"/>
</xsl:template>

<!-- Style for the error and failure in the tescase template -->
<xsl:template name="display-failures">
    <xsl:choose>
        <xsl:when test="not(@message)">N/A</xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="@message"/>
        </xsl:otherwise>
    </xsl:choose>
    <!-- display the stacktrace -->
</xsl:template>

<xsl:template name="display-time">
    <xsl:param name="value"/>
    <xsl:value-of select="format-number($value,'0.000')"/>
</xsl:template>

<xsl:template name="display-percent">
    <xsl:param name="value"/>
    <xsl:value-of select="format-number($value,'0.00%')"/>
</xsl:template>
</xsl:stylesheet>

