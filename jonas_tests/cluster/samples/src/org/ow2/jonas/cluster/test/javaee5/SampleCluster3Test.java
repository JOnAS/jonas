/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): eyindanga
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.cluster.test.javaee5;

import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.meterware.httpunit.GetMethodWebRequest;
import com.meterware.httpunit.WebConversation;
import com.meterware.httpunit.WebRequest;
import com.meterware.httpunit.WebResponse;

/**
 * the aim of this class is to check the different calls made in sampleCluster3
 * example.
 * @author Nicolas Duvauchel
 */
public class SampleCluster3Test extends TestCase {

    /**
     * the link with the web page
     */
    private static WebConversation conversation = null;
    /**
     * default HTTP ports.
     */
    private static final String[] default_http_ports = {"9000", "9200"};
    /**
     * @param args
     */
    public static void main(String args[]) {
        junit.textui.TestRunner.run(suite());
    }

    /**
     * @return the test suite
     */
    public static TestSuite suite() {
        return new TestSuite(SampleCluster3Test.class);
    }

    /**
     * @param s
     */
    public SampleCluster3Test(String s) {
        super(s);
    }

    /**
     * set up parameters for the tests
     */
    protected void setUp() throws Exception {
        try {
             if (conversation == null) {
                    System.out.println("setting up conversation.");
                    conversation = new WebConversation();
                }
        } catch (Exception e) {
            fail("Unable to set up web tests " + e);
        }

    }

    /**
     * test if the page is accessible
     * @throws Exception
     */
    public void testReachable() throws Exception {

        try {
            for (String port: default_http_ports) {
                 WebRequest request = new GetMethodWebRequest("http://localhost:" + port + "/sampleCluster3");
                 WebResponse response = conversation.getResponse(request);
                 System.out.println(response.getText());
            }

        } catch (Exception e) {
            fail("on test Reachable: " + e);
        }

    }

    /**
     * test and create a session
     * @throws Exception
     */
    public void testSession() throws Exception {
        try {
            for (String port: default_http_ports) {
                 WebRequest request = new GetMethodWebRequest("http://localhost:" + port + "/sampleCluster3/servlet/session");

                 WebResponse response = conversation.getResponse(request);
                 System.out.println(response.getText());
            }

        } catch (Exception e) {
            fail("on test Session: " + e);
        }
    }

    /**
     * test and check the session
     * @throws Exception
     */
    public void testCheck() throws Exception {
        try {
            for (String port: default_http_ports) {
                WebRequest request = new GetMethodWebRequest("http://localhost:" + port + "/sampleCluster3/servlet/check");

                WebResponse response = conversation.getResponse(request);
                System.out.println(response.getText());
            }

        } catch (Exception e) {
            fail("on test Check: " + e);
        }
    }

    /**
     * test and release the session
     * @throws Exception
     */
    public void testRelease() throws Exception {
        try {
            for (String port: default_http_ports) {
                  WebRequest request = new GetMethodWebRequest("http://localhost:" + port + "/sampleCluster3/servlet/release");
                  WebResponse response = conversation.getResponse(request);
                  System.out.println(response.getText());
            }

        } catch (Exception e) {
            fail("on test Release: " + e);
        }
    }

    /**
     * test the exception
     * @throws Exception
     */
    public void testException() throws Exception {
        try {
            for (String port: default_http_ports) {
                  WebRequest request = new GetMethodWebRequest("http://localhost:" + port + "/sampleCluster3/servlet/exception");
                  WebResponse response = conversation.getResponse(request);
                  System.out.println(response.getText());
            }

        } catch (Exception e) {
            fail("on test Exception: " + e);
        }
    }

}
