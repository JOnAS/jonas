/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.cluster.test.javaee5.client;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.ow2.jonas.examples.cluster.javaee5.client.ClientBase;
import org.ow2.jonas.examples.cluster.javaee5.beans.samplecluster3.MyStateful;

/**
 * Fat client :
 * - test Fail Over of the SFSB
 */
public class ClientFOSFSB extends ClientBase {


    /**
     * Iteration number
     */
    private static final int ITERATION_NB = 50;

    /**
     * Default constructor.
     */
    private ClientFOSFSB() {

    }
    /**
     * Constructor.
     * @param args the arguments.
     */
    private ClientFOSFSB(final String[] args) {
        super(args);
    }

    private int start() {
        try {
            out.println(clientName + ": starting...");
            Context ctx = new InitialContext();
            MyStateful myStateful = (MyStateful) ctx.lookup("org.ow2.jonas.examples.cluster.javaee5.beans.samplecluster3.MyStatefulBean"
                    + "_" + MyStateful.class.getName() +"@Remote");
            out.println("Bean created -> " + myStateful);
            for (int i = 1; i < ITERATION_NB + 1; i++) {


                String l = "log entry " + i;
                // add in the bean state
                myStateful.logWithJOnASInstance(l);

                out.println("Add " +  l);

                if (0 == i % 10) {
                    StringBuffer sb = myStateful.getLogTextDump();
                    out.println("Get the log entries from the bean state " + sb);
                    out.println("Sleep 10s");
                    try {
                        Thread.sleep(10000);
                    } catch (Exception e) {
                        out.println(clientName  + " KO. " + e);
                        out.close();
                        return -1;
                    }
                }


            }
            myStateful.cleanup();
            //Close the context.
            ctx.close();
        } catch (Exception e) {
            out.println(clientName  + ": KO. " + e);
            out.close();
            return -1;
        }
        out.println(clientName + ": OK. Exiting.");
        out.close();
        return 0;
    }

    /**
     * Main method
     * @param args arguments of the client
     */
    public static int main(final String[] args) {
        ClientFOSFSB client = new ClientFOSFSB(args);
        return client.start();
    }
}
