/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.cluster.test.j2ee14;

import java.io.File;
import java.util.ArrayList;

import junit.framework.TestSuite;

import org.ow2.jonas.cluster.test.common.AbsTestClient;
import org.ow2.jonas.cluster.test.common.ClientTask;
import org.ow2.jonas.cluster.test.common.RunnableTask;
import org.ow2.jonas.cluster.test.common.Worker;
/**
 *
 * @author Nicolas Duvauchel
 *
 */
public class ClientTest extends AbsTestClient {

    /**
     * The worker
     */
    protected static Worker jworker = null;

    public static void main(String args[]) {
        junit.textui.TestRunner.run(suite());
    }

    public static TestSuite suite() {
        return new TestSuite(ClientTest.class);
    }

    public ClientTest(String s) {
        super(s);
    }

    protected void setUp() throws Exception {
        super.setUp();
//        if (jworker == null) {
//            jworker = new Worker();
//            ArrayList<String> arg = new ArrayList<String>();
//            arg.add("-out");
//            arg.add(outputDir + File.separator + "cluster-j2ee14.jonasTask.txt");
//            arg.add("-clientName");
//            arg.add("cluster-j2ee14.jonasTask");
//            arg.add("-clusterConfig");
//            arg.add(clusterConfig);
//            arg.add("-clusterRoot");
//            arg.add(clusterRoot);
//            arg.add("-jonasRoot");
//            arg.add(jonasRoot);
//            arg.add("-domainName");
//            arg.add(domainName);
//            RunnableTask task = new ClientTask("cluster-j2ee14.jonasTask", "org.ow2.jonas.cluster.test.common.JOnASTask", arg.toArray(new String[arg.size()]));
//            jworker.addTask(task);
//            jworker.run();
//        }
    }

    public void testLBHome() throws Exception {
        LOGGER.info("cluster-j2ee14.testLBHome ");
        RunnableTask task = null;

        ArrayList<String> arg = new ArrayList<String>();
        for(int i = 0; i < clientsNumber; i++) {
            arg.add("-clientName");
            arg.add("cluster-j2ee14.testLBHome_" + i);
            arg.add("-out");
            arg.add(outputDir + File.separator + "cluster-j2ee14.testLBHome_" + i + ".txt");
            task = new ClientTask("cluster-j2ee14.testLBHome_" + i,
                    "org.ow2.jonas.examples.cluster.j2ee14.client.ClientLBHome", arg.toArray(new String[arg.size()]));
            worker.addTask(task);
        }
        worker.run();
        /**start the ckecker **/
        checker.start();
        while(!checker.stopped()) {
            // sleep.
            Thread.currentThread().sleep(checkerSleepTime);
        }
        if(worker.hasFailure()) {
            fail("on testLBHome" + worker.getTasksFailures());
        }
        worker.clear();
        LOGGER.info("cluster-j2ee14.testLBHome is ok");
    }

    public void testFOSFSB() throws Exception {
        LOGGER.info("cluster-j2ee14.testFOSFSB");
        RunnableTask task = null;
        ArrayList<String> arg = new ArrayList<String>();
        for(int i = 0; i < clientsNumber; i++) {
            arg.add("-clientName");
            arg.add("cluster-j2ee14.testFOSFSB_" + i);
            arg.add("-out");
            arg.add(outputDir + File.separator + "cluster-j2ee14.testFOSFSB_" + i + ".txt");
            task = new ClientTask("cluster-j2ee14.testFOSFSB_" + i,
                    "org.ow2.jonas.examples.cluster.j2ee14.client.ClientFOSFSB", arg.toArray(new String[arg.size()]));
            worker.addTask(task);
        }
        worker.run();
        /**start the ckecker **/
        checker.start();
        while(!checker.stopped()) {
            // sleep.
            Thread.currentThread().sleep(checkerSleepTime);
        }
        if(worker.hasFailure()) {
            fail("on cluster-j2ee14.testFOSFSB" + worker.getTasksFailures());
        }
        worker.clear();
        LOGGER.info("cluster-j2ee14.testFOSFSB is ok");
    }

    public void testLBLookup() throws Exception {
        LOGGER.info("cluster-j2ee14.testLBLookup");
        RunnableTask task = null;
        ArrayList<String> arg = new ArrayList<String>();
        for(int i = 0; i < clientsNumber; i++) {
            arg.add("-clientName");
            arg.add("cluster-j2ee14.testLBLookup_" + i);
            arg.add("-out");
            arg.add(outputDir + File.separator + "cluster-j2ee14.testLBLookup_" + i + ".txt");
            task = new ClientTask("cluster-j2ee14.testLBLookup_" + i,
                    "org.ow2.jonas.examples.cluster.j2ee14.client.ClientLBLookup", arg.toArray(new String[arg.size()]));
            worker.addTask(task);
        }
        worker.run();
        /**start the ckecker **/
        checker.start();
        while(!checker.stopped()) {
            // sleep.
            Thread.currentThread().sleep(checkerSleepTime);
        }
        if(worker.hasFailure()) {
            fail("on cluster-j2ee14.testLBLookup" + worker.getTasksFailures());
        }
        worker.clear();
        LOGGER.info("cluster-j2ee14.testLBLookup is ok");
    }

    public void testLBRemote() throws Exception {
        LOGGER.info("cluster-j2ee14.testLBRemote");
        RunnableTask task = null;
        ArrayList<String> arg = new ArrayList<String>();
        for(int i = 0; i < clientsNumber; i++) {
            arg.add("-clientName");
            arg.add("cluster-j2ee14.testLBRemote_" + i);
            arg.add("-out");
            arg.add(outputDir + File.separator + "cluster-j2ee14.testLBRemote_" + i + ".txt");
            task = new ClientTask("cluster-j2ee14.testLBRemote_" + i,
                    "org.ow2.jonas.examples.cluster.j2ee14.client.ClientLBRemote", arg.toArray(new String[arg.size()]));
            worker.addTask(task);
        }
        worker.run();
        /**start the ckecker **/
        checker.start();
        while(!checker.stopped()) {
            // sleep.
            Thread.currentThread().sleep(checkerSleepTime);
        }
        if(worker.hasFailure()) {
            fail("on cluster-j2ee14.testLBRemote" + worker.getTasksFailures());
        }
        worker.clear();
        LOGGER.info("cluster-j2ee14.testLBRemote is ok");
    }

}
