/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.cluster.test.javaee5.client;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.ow2.jonas.examples.cluster.javaee5.client.ClientBase;
import org.ow2.jonas.examples.cluster.javaee5.beans.samplecluster3.MyException;
import org.ow2.jonas.examples.cluster.javaee5.beans.samplecluster3.MyStateless;

public class ClientException extends ClientBase {
    /**
     * Private constructor for utility class
     *
     */
    private ClientException() {
    }
    /**
     * Constructor.
     * @param args the arguments
     */
    private ClientException(final String[] args) {
        super(args);
    }

    private int start() {
         try {
             out.println(clientName + ": starting...");
             Context ctx = new InitialContext();
             MyStateless myStateless = (MyStateless) ctx.lookup("org.ow2.jonas.examples.cluster.javaee5.beans.samplecluster3.MyStatelessBean"
             + "_" + MyStateless.class.getName() +"@Remote");
             out.println("Bean created -> " + myStateless);
             out.println("Throw an exception at the server side");
             myStateless.throwMyException();
             //Close the context
             ctx.close();
         } catch (MyException e) {
             out.println("Exception caught : invocation successful");
         }catch (Exception e) {
             out.println(clientName + " KO. " + e);
             out.close();
             return -1;
         }
         out.println(clientName + ": OK. Exiting.");
         out.close();
         return 0;
    }
    /**
     * Main method
     * @param args arguments of the client
     */
    public static int main(final String[] args) {
        ClientException client = new ClientException(args);
        return client.start();

    }

}
