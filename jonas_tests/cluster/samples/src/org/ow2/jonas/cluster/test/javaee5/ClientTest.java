/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.cluster.test.javaee5;

import java.io.File;
import java.util.ArrayList;

import junit.framework.TestSuite;

import org.ow2.jonas.cluster.test.common.AbsTestClient;
import org.ow2.jonas.cluster.test.common.ClientTask;
import org.ow2.jonas.cluster.test.common.RunnableTask;
import org.ow2.jonas.cluster.test.common.Worker;
/**
 *
 * @author eyindanga
 *
 */
public class ClientTest extends AbsTestClient {
    /**
     * The worker
     */
    protected static Worker jworker = null;

    public static void main(String args[]) {
        junit.textui.TestRunner.run(suite());
    }

    public static TestSuite suite() {
        return new TestSuite(ClientTest.class);
    }

    public ClientTest(String s) {
        super(s);
    }

    protected void setUp() throws Exception {
        super.setUp();
//        if (jworker == null) {
//            jworker = new Worker();
//            ArrayList<String> arg = new ArrayList<String>();
//            arg.add("-out");
//            arg.add(outputDir + File.separator + "cluster-javaee5.jonasTask.txt");
//            arg.add("-clientName");
//            arg.add("cluster-javaee5.jonasTask");
//            arg.add("-clusterConfig");
//            arg.add(clusterConfig);
//            arg.add("-clusterRoot");
//            arg.add(clusterRoot);
//            arg.add("-domainName");
//            arg.add(domainName);
//            RunnableTask task = new ClientTask("cluster-javaee5.jonasTask", "org.ow2.jonas.cluster.test.common.JOnASTask", arg.toArray(new String[arg.size()]));
//            jworker.addTask(task);
//            jworker.run();
//        }
    }

    /**
     * Test Exception.
     * @throws Exception any.
     */
    public void testException() throws Exception {
        LOGGER.info("cluster-javaee5.testException");
        RunnableTask task = null;
        ArrayList<String> arg = new ArrayList<String>();
        for(int i = 0; i < clientsNumber; i++) {
            arg.add("-clientName");
            arg.add("cluster-javaee5.testException_" + i);
            arg.add("-out");
            arg.add(outputDir + File.separator + "cluster-javaee5.testException_" + i + ".txt");
            task = new ClientTask("cluster-javaee5.testException_" + i,
                    "org.ow2.jonas.cluster.test.javaee5.client.ClientException", arg.toArray(new String[arg.size()]));
            worker.addTask(task);
        }
        System.out.println("running the worker");
        worker.run();
        /**start the checker **/
        checker.start();
        while(!checker.stopped()) {
            // sleep.
            Thread.currentThread().sleep(checkerSleepTime);
        }
        System.out.println("Worker has terminated");
        if(worker.hasFailure()) {
            fail("on cluster-javaee5.testException" + worker.getTasksFailures());
        }
        worker.clear();
        LOGGER.info("cluster-javaee5.testException is ok");
    }
    /**
     * Test Fail over of Stateful session Bean.
     * @throws Exception any.
     */
    public void testFOSFSB() throws Exception {
        LOGGER.info("cluster-javaee5.testFOSFSB");
        RunnableTask task = null;
        ArrayList<String> arg = new ArrayList<String>();
        for(int i = 0; i < clientsNumber; i++) {
            arg.add("-clientName");
            arg.add("cluster-javaee5.testFOSFSB_" + i);
            arg.add("-out");
            arg.add(outputDir + File.separator + "cluster-javaee5.testFOSFSB_" + i + ".txt");
            task = new ClientTask("cluster-javaee5.testFOSFSB_" + i,
                    "org.ow2.jonas.cluster.test.javaee5.client.ClientFOSFSB", arg.toArray(new String[arg.size()]));
            worker.addTask(task);
        }
        worker.run();
        /**start the ckecker **/
        checker.start();
        while(!checker.stopped()) {
            // sleep.
            Thread.currentThread().sleep(checkerSleepTime);
        }
        if(worker.hasFailure()) {
            fail("on cluster-javaee5.testFOSFSB" + worker.getTasksFailures());
        }
        worker.clear();
        LOGGER.info("cluster-javaee5.testFOSFSB is ok");
    }

    /**
     * Test Load Balancing
     * @throws Exception any.
     */
    public void testLBRemote() throws Exception {
         LOGGER.info("cluster-javaee5.testLBRemote");
         RunnableTask task = null;
         ArrayList<String> arg = new ArrayList<String>();
         for(int i = 0; i < clientsNumber; i++) {
             arg.add("-clientName");
             arg.add("cluster-javaee5.testLBRemote_" + i);
             arg.add("-out");
             arg.add(outputDir + File.separator + "cluster-javaee5.testLBRemote_" + i + ".txt");
             task = new ClientTask("cluster-javaee5.testLBRemote_" + i,
                     "org.ow2.jonas.cluster.test.javaee5.client.ClientLBRemote", arg.toArray(new String[arg.size()]));
             worker.addTask(task);
         }
         worker.run();
         /**start the ckecker **/
         checker.start();
         while(!checker.stopped()) {
             // sleep.
             Thread.currentThread().sleep(checkerSleepTime);
         }
         if(worker.hasFailure()) {
             fail("on cluster-javaee5.testLBRemote" + worker.getTasksFailures());
         }
         worker.clear();
         LOGGER.info("cluster-javaee5.testLBRemote is ok.");
    }

}
