/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.cluster.test.common;

public enum TestEnum {
    /**
     * Key for sleeping time of a checker
     */
    CKECKER_SLEEP_TIME_KEY("checker.sleep.time"),
    /**
     * Key for sleeping time of a worker
     */
    WORKER_SLEEP_TIME_KEY("worker.sleep.time"),
    /**
     * Key for properties file.
     */
    PROPERTIES_FILE_KEY("test.properties.file"),
    /**
     * Key for cluster properties file.
     */
    CLUSTER_PROPERTIES_FILE_KEY("cluster.properties.file"),
    /**
     * Default properties file.
     */
    DEF_PROPERTIES_FILE("defConf.properties"),

    /**
     * Key for sleeping time of a worker
     */
    EJB_VERSION_KEY("ejb.version"),

    /**
     * Key for sleeping time of a worker
     */
    PROVIDER_URL_KEY("cmiclient.provider.url"),

    /**
     * Key for sleeping time of a worker
     */
    TEST_CMIURL_KEY("cmi.config.url"),

    /**
     * Key for sleeping time of a worker
     */
    CMI_URL_KEY("cmi.conf.url"),

    /**
     * Key for output folder.
     */
     OUTPUT_FOLDER_KEY("testejb.output.dir"),
    /**
     * key for sleeping time of a worker
     */
    CLIENT_NUMBER_KEY("clients.number"),

    /**
     * key for cluster root.
     */
    CLUSTER_ROOT_KEY("cluster.root"),
    
    /**
     * key for cluster root.
     */
    CLUSTER_JOnASROOT_KEY("cluster.jonas.root"),

    /**
     * key for cluster configuration.
     */
    CLUSTER_CONFIG_KEY("cluster.config"),

    /**
     * key for domain name.
     */
    DOMAIN_NAME_KEY("domain.name"),
    
    /**
     * Default properties file.
     */
    DEF_NEWJC_FILE("build-jc.properties"),
    /**
     * key for default prefix of cluster instances.
     */
    JMX_PORTRANGE_KEY("carol.portrange"),
    /**
     * key for default prefix of cluster instances.
     */
    CLUSTER_DEFPREFIX_KEY("jonas.base.defaultprefix");

    /**
     * A name of property.
     */
    private final String propertyName;

    private TestEnum(final String propertyName) {
        this.propertyName = propertyName;
    }

    /**
     * @return the name of property
     */
    @Override
    public String toString() {
        return propertyName;
    }

    /**
     * @return the name of property
     */
    public String getPropertyName() {
        return propertyName;
    }
}
