/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.cluster.test.common;

import java.util.Iterator;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;
public class Checker extends Thread {
    /**
     * Logger.
     */
    protected static final Log LOGGER = LogFactory.getLog(Checker.class);
    /**
     * Sleep for 10 seconds
     */
    private long time2sleep = 10000;
    /**
     * the worker.
     */
    private Worker worker = null;
    /**
     * Stop this thread if true
     */
    private boolean stop = false;

    public Checker() {
        super();
        // TODO Auto-generated constructor stub
    }
    /**
     * Contructor using fields.
     * @param time2sleep
     * @param worker
     */
    public Checker(long time2sleep) {
        super();
        this.time2sleep = time2sleep;
    }

    @Override
    public void run() {
        LOGGER.info("Running the checker");
        while(!stop) {
            try {
                LOGGER.debug("Do not stop the checker .....");
                boolean breakLoop = true;
                for (Iterator<RunnableTask> iterator = worker.getTasks().iterator(); iterator
                        .hasNext();) {
                    RunnableTask task = (RunnableTask) iterator.next();
                    LOGGER.debug("Is the task :{0} terminated ? {1}", task.getName(), task.isTerminated());
                    breakLoop = (breakLoop && task.isTerminated());
                }
                stop = breakLoop;
                LOGGER.debug("Stop the checker ? {0}", stop);
                if(!stop) {
                    LOGGER.debug("Sleeping for :{0} ", time2sleep);
                    Thread.currentThread().sleep(time2sleep);
                }
            } catch (InterruptedException e) {
                stop = true;
            }
        }
    }
    /**
     * Set the worker.
     * @param worker
     */
    public void setWorker(final Worker worker) {
        this.worker = worker;
    }
    /**
     * Set the time to sleep.
     * @param time2sleep
     */
    public void setTime2Sleep(long time2sleep) {
        this.time2sleep = time2sleep;
    }
    /**
     * True if the worker is stopped.
     * @return true if the worker is stopped.
     */
    public boolean stopped() {
        return stop;
    }

}
