/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.cluster.test.common;

public interface RunnableTask extends Runnable {
    /**
     * Stops the task.
     * @throws Exception
     */
    public void stop();
    /**
     * Set the name of the task.
     * @param name the name to set.
     */
    public void setName(String name);
    /**
     * Get the name of the task
     * @return
     */
    public String getName();
    /**
     * True if the task has ended.
     * @return true if the task has ended.
     */
    public boolean isTerminated();

    /**
     * Returns any exception thrown on task execution.
     * @return any exception thrown on task execution.
     */
    public Exception getException();
}
