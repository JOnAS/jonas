/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.cluster.test.common;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;

import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

public class ClientTask implements RunnableTask {
    /**
     * Logger.
     */
    protected static final Log LOGGER = LogFactory.getLog(ClientTask.class);
    /**
     * Any exception thrown on task execution.
     */
    protected Exception exception = null;
    /**
     * Name of the task.
     */
    protected String name = null;
    /**
     * The class to run
     */
    private String class2Run = null;
    /**
     * True if the task is terminated
     */
    protected boolean terminated = false;

    /**
     * Arguments.
     */
    ArrayList args = new ArrayList();
    /**
     * return exception thrown on execution.
     */
    public Exception getException() {
        return exception;
    }
    /**
     * default constructor.
     */
    public ClientTask () {

    }

    /**
     * Constructor using fields.
     */
    public ClientTask(final String name, final String class2run, final String[] vargs) {
        this.name = name;
        if (vargs != null) {
            args.addAll(Arrays.asList(vargs));
        }
        this.class2Run = class2run;
    }
    /**
     * Get the name.
     */
    public String getName() {
        return name;
    }
    /**
     * Return true if the task has ended.
     */
    public boolean isTerminated() {
        return terminated;
    }

    public void setName(final String name) {
        this.name = name;

    }

    public void stop() {
        Thread.currentThread().interrupt();
    }
    /**
     * Executes this task.
     */
    public void run() {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        Class<?> clazz;
        try {
            //create the output.
            clazz = loader.loadClass(class2Run);
            LOGGER.info("Invoking {0} with args: {1}", class2Run, args);
            String[] ar = {};
            Class<?>[] argList = new Class[] {ar.getClass()};
            Method meth = clazz.getMethod("main", argList);
            meth.invoke(null, new Object[] {args.toArray(new String[args.size()])});
        } catch (Exception e) {
            //Consider the task is terminated.
            terminated = true;
            exception = e;
            LOGGER.info("Running {0}, {1} terminated with Exception {2} ", name, class2Run, e);
            return;
        }
        LOGGER.info("Running {0}, {1} terminated.", name, class2Run);
        terminated = true;
    }
    /**
     * Additional arguments.
     * @param args the arguments.
     */
    public void addArguments(final String[] vargs) {
        if (vargs != null) {
            args.addAll(Arrays.asList(vargs));
        }

    }

}
