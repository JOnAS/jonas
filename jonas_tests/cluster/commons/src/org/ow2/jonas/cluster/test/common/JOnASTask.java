/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.cluster.test.common;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

public class JOnASTask {
    /**
     * Output to be used.
     */
    protected PrintStream out = null;
    /**
     * the name of the client.
     */
    protected String clientName = "JOnAS Client for cluster-javaee5";

    /**
     *  Time to sleep each 10 iterations.
     */
    protected long time2sleep = TIME2SLEEP;

    /**
     * The configuration file
     */
    String clusterConfigFile;

    /**
     * The cluster root.
     */
    String clusterRoot = null;

    /**
     * The JOnAS root.
     */
    String jonasRoot = null;

    /**
     * Cluster foncfiguration.
     */
    private Properties clusterConfig;
    /**
     * Domain name.
     */
    private String domainName = "sampleClusterDomain";

    List<String> jmxPorts = null;

    /**
     * Default time to sleep.
     */
    private static final long TIME2SLEEP = 10000;

    public JOnASTask() {

    }

    public JOnASTask(final String[] args) throws Exception {
        out = System.out;
        clusterConfig = new Properties();
        String url = null;
        if(args != null && args.length != 0) {
            for (int argn = 0; argn < args.length; argn++) {
                String arg = args[argn];
                if("-out".equals(arg)) {
                    url = args[++argn];
                    if (url != null) {
                        initOut(url);
                    }
                    continue;
                }else if ("-clientName".equals(arg)) {
                    clientName = args[++argn];
                    out.println("Client name set to : " + clientName);
                    continue;
                }else if ("-time2sleep".equals(arg)) {
                    String time = args[++argn];
                    try {
                        time2sleep = Long.parseLong(time);
                    } catch (NumberFormatException e) {
                        time2sleep = TIME2SLEEP;
                    }
                    out.println("Time to sleep set to : " + time2sleep);
                    continue;
                }else if ("-clusterConfig".equals(arg)) {
                    clusterConfigFile = args[++argn];
                    out.println("Cluster configuration file set to : " + clusterConfigFile);
                    continue;
                }else if ("-clusterRoot".equals(arg)) {
                    clusterRoot = args[++argn];
                    out.println("Cluster root set to : " + clusterRoot);
                    continue;
                }else if ("-jonasRoot".equals(arg)) {
                    jonasRoot = args[++argn];
                    out.println("JOnAS root set to : " + jonasRoot);
                    continue;
                }else if ("-domainName".equals(arg)) {
                    domainName = args[++argn];
                    out.println("Domain name root set to : " + clusterRoot);
                    continue;
                }
            }
        }
        if (clusterConfigFile != null) {
            initClusterConfig(clusterConfigFile);
        }
    }

    private void initClusterConfig(final String clusterConfigFile) throws Exception {
        InputStream in = null;
        File f = new File(clusterConfigFile);
        if (f.exists()) {
            try {
                in = f.getCanonicalFile().toURL().openStream();
            } catch (Exception e) {
                // TODO: handle exception
            }
        }
        if (in == null) {
            in = Thread.currentThread().getContextClassLoader().getResourceAsStream(f.toString());
        }
        if (in == null) {
            in = getClass().getClassLoader().getResourceAsStream(f.toString());
        }
        if (in == null) {
            in = getClass().getClassLoader().getResourceAsStream(
                    TestEnum.DEF_PROPERTIES_FILE.getPropertyName());
        }
        //load the properties.
        clusterConfig.load(in);
        getJmxPorts();
        in.close();

    }
    private void getJmxPorts() {
        // TODO Auto-generated method stub
        String ports = clusterConfig.getProperty(TestEnum.JMX_PORTRANGE_KEY.getPropertyName());
        jmxPorts = new ArrayList<String>();
        while(ports != null) {
            String port = ports.substring(0, ports.indexOf(","));
            jmxPorts.add(port);
            ports = ports.substring(ports.indexOf(",") + 1);
        }

    }

    /**
     * Runs the client.
     * @return 0 if ok, -1 otherwise.
     * @throws Exception any.
     */
    private int run() throws Exception {
        Thread.currentThread().sleep(time2sleep);
        execute("stop", 3, null, null);
        Thread.currentThread().sleep(time2sleep);
        execute("start", 3, null, null);
        return 0;
    }
    private void execute(String operation, int i, Object[] params, String[] signature)
    throws IOException, InstanceNotFoundException, MBeanException, ReflectionException {
        String jbPrefix = clusterConfig.getProperty(TestEnum.CLUSTER_DEFPREFIX_KEY.getPropertyName());
        String nodeName = jbPrefix + i;
        String serviceURL = "service:jmx:rmi://localhost/jndi/rmi://localhost:"
            + jmxPorts.get(i - 1) + "/jrmpconnector_" + nodeName;
        JMXServiceURL url = new JMXServiceURL(serviceURL);
        JMXConnector connector = JMXConnectorFactory.connect(url);
        MBeanServerConnection mbeanSrv = connector.getMBeanServerConnection();
        mbeanSrv.invoke(J2EEServer(domainName, nodeName), operation, params, signature);
    }

    /**
     * Initializes client output.
     * @param url Stringified url of the client output file.
     */
    private void initOut(final String url) throws Exception {
        try {
            FileOutputStream file =  new FileOutputStream(url, true);
            out.println("Initializing output '" + url + "' for " + clientName);
            out = new PrintStream(file, true);
        } catch (Exception e) {
            out.println("Unable to initialize output for " + clientName + " because : " + e);
            throw new Exception("Unable to initialize output for " + clientName, e);
        }
    }
    /**
     * Main method.
     * @param args the arguments
     * @return 0 if ok, -1 otherwise.
     * @throws Exception
     */
    public static int main(String[] args) throws Exception {
        JOnASTask jtask = new JOnASTask(args);
        return jtask.run();
    }

    /**
     * Create ObjectName for a J2EEServer MBean.
     * @param pDomain domain name
     * @param pServer server name
     * @return the created ObjectName
     */
    public static ObjectName J2EEServer(final String pDomain, final String pServer) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=J2EEServer");
            sb.append(",name=");
            sb.append(pServer);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }


}
