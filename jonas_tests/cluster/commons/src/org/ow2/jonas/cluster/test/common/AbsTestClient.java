/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.cluster.test.common;
import java.io.File;
import java.io.InputStream;
import java.util.Properties;

import junit.framework.TestCase;

import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

/**
 * @author eyindanga
 *
 */
public abstract class AbsTestClient extends TestCase {
    /**
     * Logger.
     */
    protected static final Log LOGGER = LogFactory.getLog(AbsTestClient.class);

    /**
     * Number of simultaneous clients to be executed.
     */
    protected int clientsNumber = 0;
    /**
     * The worker
     */
    protected Worker worker = null;

    /**
     * Check if invocation are terminated.
     */
    protected Checker checker = null;
    /**
     * test properties.
     */
    protected Properties testConfig = null;

    /**
     * Name of the EJB version
     */
    protected String ejbVersion = "ejb2";

    /**
     * Name of the EJB version
     */
    protected long checkerSleepTime = 0;

    /**
     * Name of the EJB version
     */
    protected long workerSleepTime = 0;
    /**
     * Directory where the test outputs will be printed.
     */
    protected String outputDir = null;
    /**
     * Domain name.
     */
    protected String domainName;
    /**
     * Cluster root stringifiedurl.
     */
    protected String clusterRoot;

    /**
     * JOnAS root stringifiedurl.
     */
    protected String jonasRoot;
    /**
     * Cluster configuration stringifiedurl.
     */
    protected String clusterConfig;

    public AbsTestClient() {
        super();
    }

    public AbsTestClient(String s) {
        super(s);
    }
    protected void setUp() throws Exception {

        String propertiesFile = TestEnum.DEF_PROPERTIES_FILE.getPropertyName();
        String usrPropertiesFile = System.getProperty(TestEnum.PROPERTIES_FILE_KEY.getPropertyName());
        ejbVersion = System.getProperty(TestEnum.EJB_VERSION_KEY.getPropertyName());
        outputDir = System.getProperty(TestEnum.OUTPUT_FOLDER_KEY.getPropertyName());
        domainName = System.getProperty(TestEnum.DOMAIN_NAME_KEY.getPropertyName());
        clusterRoot = System.getProperty(TestEnum.CLUSTER_ROOT_KEY.getPropertyName());
        jonasRoot =  System.getProperty(TestEnum.CLUSTER_JOnASROOT_KEY.getPropertyName());
        clusterConfig = System.getProperty(TestEnum.CLUSTER_CONFIG_KEY.getPropertyName());
        try {
            testConfig = new Properties();
            if(usrPropertiesFile != null) {
                //initializes default properties
                propertiesFile = usrPropertiesFile;
            }
            //load the properties.
            InputStream in = null;
            File f = new File(propertiesFile);
            if (f.exists()) {
                try {
                    in = f.getCanonicalFile().toURL().openStream();
                } catch (Exception e) {
                    // TODO: handle exception
                }
            }
            if (in == null) {
                in = Thread.currentThread().getContextClassLoader().getResourceAsStream(f.toString());
            }
            if (in == null) {
                in = getClass().getClassLoader().getResourceAsStream(f.toString());
            }
            if (in == null) {
                in = getClass().getClassLoader().getResourceAsStream(
                        TestEnum.DEF_PROPERTIES_FILE.getPropertyName());
            }
            //FileInputStream in = new FileInputStream(propertiesFile);
            testConfig.load(in);
            in.close();
            String value = testConfig.getProperty(TestEnum.CLIENT_NUMBER_KEY.getPropertyName());
            clientsNumber = Integer.parseInt(value);
            LOGGER.debug("Number of clients to be launched : " + clientsNumber);
            //get sleep time for checkers and workers.

            value = testConfig.getProperty(TestEnum.CKECKER_SLEEP_TIME_KEY.getPropertyName());
            checkerSleepTime = Long.parseLong(value);
            value = testConfig.getProperty(TestEnum.WORKER_SLEEP_TIME_KEY.getPropertyName());
            workerSleepTime = Long.parseLong(value);
            worker = new Worker();
            checker = new Checker(checkerSleepTime);
            checker.setWorker(worker);
        } catch (Exception e) {
            LOGGER.error("Exception on setup of {0}, {1}", ejbVersion, e);
            // TODO: handle exception
        }
        LOGGER.debug("Successfully set up client tests for {0}", ejbVersion);
    }

}

