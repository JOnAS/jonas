<?xml version="1.0" encoding="UTF-8"?>
<!--
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 - JOnAS: Java(TM) Open Application Server
 - Copyright (C) 2010 Bull S.A.S.
 - Contact: jonas-team@ow2.org
 -
 - This library is free software; you can redistribute it and/or
 - modify it under the terms of the GNU Lesser General Public
 - License as published by the Free Software Foundation; either
 - version 2.1 of the License, or any later version.
 -
 - This library is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 - Lesser General Public License for more details.
 -
 - You should have received a copy of the GNU Lesser General Public
 - License along with this library; if not, write to the Free Software
 - Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 - USA
 -
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 - $Id$
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 -->

<project xmlns:jonas="http://jonas.ow2.org"
         name="JOnAS Java EE 5 Ear example"
         default="install"
         basedir=".">

    <!-- Import common rules -->
    <import file="../jonas-common.xml" />

    <!-- Build exploded archives or not ? -->
    <property name="archives.exploded" value="false" />

    <path id="base.classpath">
      <pathelement location="${jonas.base}/conf" />
      <pathelement location="${common.classes}" />
      <fileset dir="${junit.lib.dir}">
         <include name="*.jar" />
      </fileset>
      <fileset dir="${jonas.root}">
          <!-- In that bundle we have the @OSGiResource -->
          <include name="repositories/maven2-internal/**/easybeans-*.jar" />
          <include name="repositories/maven2-internal/**/javaee-api-*.jar" />
          <include name="repositories/maven2-internal/**/ow2-util-log-*.jar" />
          <include name="repositories/maven2-internal/**/jonas-commons-*.jar" />
          <include name="repositories/maven2-internal/**/cmi-all-*.jar" />
          <include name="repositories/maven2-internal/**/*-management-*.jar" />
          <include name="lib/common/util-ant-tasks.jar"/>
          <include name="lib/bootstrap/felix-launcher.jar"/>
      </fileset>
    </path>
    
    <echo message="jonas root ${jonas.root}"></echo>

    <!-- Properties for the Client (name + deployment descriptor + pattern set)
    <property name="client.main.class"
              value="org.ow2.jonas.cluster.test.jmx.client.ClientJMXTester" />
    <property name="client.name" value="javaee5-application-client" />
    <property name="client.dd" value="etc/application-client.xml" />
    <property name="client.specificdd" value="etc/jonas-client.xml" />
    <patternset id="client.patternset">
        <include name="org/ow2/jonas/examples/ear/client/ApplicationClient.class" />
    </patternset>
    <property name="examples.resources.dir" value="${basedir}/etc" />-->

    <target name="ear" depends="compile">

        <!-- Package the EAR Sample -->
        <jonas:ear dest="${ear.dir}/cluster-test-jmx.ear"
                   dd="etc/application.xml">

            <!-- The EjbJars with the following EJB3:
                 * SLSB
             -->
            <ejb dest="ejb3.jar" manifest="etc/META-INF/MANIFEST.MF">
                <fileset dir="${examples.classes.dir}">
                    <patternset>
                        <include name="org/ow2/jonas/cluster/test/jmx/ejb/*.class" />
                    </patternset>
                </fileset>
            </ejb>
            <!-- JMS interacting Application Client -->
            <client dest="cmi-jmx-client.jar"
                        mainclass="org.ow2.jonas.cluster.test.jmx.client.ClientJMXTester"
                        dd="etc/cmi-jmx-client-application.xml"
                            specificdd="etc/cmi-jmx-client-jonas.xml">
                   <fileset dir="${examples.classes.dir}">
                     <patternset>
                         <include name="org/ow2/jonas/cluster/test/jmx/client/*.class" />
                         <include name="org/ow2/jonas/cluster/test/jmx/common/*.class" />
                         <include name="org/ow2/jonas/cluster/test/jmx/ejb/ClusterJMXTester.class" />
                     </patternset>
                   </fileset>
             </client>

        </jonas:ear>
    </target>

    <!-- Install example -->
    <target name="install"
            depends="init, compile, ear"
            description="Install this example in a deployable directory">
    </target>
    <macrodef name="jonas-client">
            <attribute name="ear"/>
            <attribute name="client"/>
            <sequential>

                <!-- JOnAS Client Container Classpath -->
                <path id="client.path">
                    <pathelement location="${jonas.root}/lib/client.jar"/>
                    <pathelement location="${jonas.root}/lib/jonas-client.jar"/>
                </path>

                <!-- Execute the client in another JVM -->
                <java fork="true"
                      classpathref="client.path"
                      classname="org.ow2.jonas.client.ClientContainer">

                    <!-- Uncomment for debugging -->
                    <!--jvmarg line="-Xint -Xdebug -Xnoagent -Xrunjdwp:transport=dt_socket,server=y,address=4142,suspend=y"/-->

                    <!-- JVM Options -->
                    <sysproperty key="java.security.auth.login.config"
                                 value="${jonas.base}/conf/jaas.config" />
                    <sysproperty key="java.endorsed.dirs"
                                 value="${jonas.root}/lib/endorsed" />
                    <sysproperty key="jonas.root"
                                 value="${jonas.root}" />

                    <!-- Maybe that one can be avoided -->
                    <arg value="-nowsgen" />

                    <!-- The Application to execute -->
                    <arg path="@{ear}" />

                    <!-- Protocol properties -->
                    <arg value="-carolFile" />
                    <arg path="${jonas.base}/conf/carol.properties"/>

                    <!-- The Java EE Application client -->
                    <arg line="-jarClient @{client}" />
                </java>
            </sequential>
        </macrodef>
    <target name="run:cmi-jmx-client" description="Execute the JMX client for CMI">
            <jonas-client ear="${ear.dir}/cluster-test-jmx.ear"
                          client="cmi-jmx-client.jar" />
    </target>

</project>

