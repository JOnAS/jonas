/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2010 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.cluster.test.jmx.ejb;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.InvalidAttributeValueException;
import javax.management.MBeanException;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.remote.JMXServiceURL;

/**
 * Remote interface for the bean Writer.
 *
 * @author JOnAS team
 */
public interface ClusterJMXTester {

    void init() throws Exception;

    /**
     * Returns the ObjectName binded in the MBean Server.
     *
     * @return the ObjectName binded in the MBean Server
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     * @throws AttributeNotFoundException
     */
    ObjectName getObjectName() throws InstanceNotFoundException,
            MBeanException, ReflectionException, IOException, AttributeNotFoundException;

    /**
     * @param protocolName
     *            a name of protocol
     * @return the JMX service URL to access to this MBean
     * @throws IllegalArgumentException
     *             if no object is bound with the given name
     * @throws UnsupportedOperationException
     *             if the used manager is at client-side
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     */
    JMXServiceURL getJMXServiceURL(String protocolName)
            throws IllegalArgumentException, UnsupportedOperationException, InstanceNotFoundException, MBeanException, ReflectionException, IOException;

    /**
     * @return the protocols registered in the manager
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     * @throws AttributeNotFoundException
     */
    Set<String> getProtocols() throws InstanceNotFoundException,
            MBeanException, ReflectionException, IOException, AttributeNotFoundException;

    /**
     * @param protocolName
     *            a name of protocol
     * @return the reference on the local registry for the given protocol
     * @throws IllegalArgumentException
     *             if the given protocol name doesn't exist
     * @throws UnsupportedOperationException
     *             if the used manager is at client-side
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     */
    String getRefOnLocalRegistry(String protocolName)
            throws IllegalArgumentException, UnsupportedOperationException,
            InstanceNotFoundException, MBeanException, ReflectionException,
            IOException;

    /**
     * Returns a name of interface of this object.
     *
     * @param objectName
     *            a name of object
     * @return a name of interface of this object
     * @throws IllegalArgumentException
     *             if no object is bound with the given name
     * @throws UnsupportedOperationException
     *             if the used manager is at client-side
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     */
    String getItfName(String objectName) throws IllegalArgumentException,
            UnsupportedOperationException, InstanceNotFoundException,
            MBeanException, ReflectionException, IOException;

    /**
     * Returns the business interface name of an object bound with the given
     * name (for ejb2 only). If the object is not an ejb2, null is returned.
     *
     * @param objectName
     *            a name of object
     * @return the business interface name of an object bound with the given
     *         name
     * @throws IllegalArgumentException
     *             if none object has the given name
     * @throws UnsupportedOperationException
     *             if the used manager is at client-side
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     */
    String getBusinessName(String objectName) throws IllegalArgumentException,
            UnsupportedOperationException, InstanceNotFoundException,
            MBeanException, ReflectionException, IOException;

    /**
     * @param objectName
     *            a name of object
     * @return true if the object with the given name is replicated
     * @throws IllegalArgumentException
     *             if none object has the given name
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     */
    boolean isReplicated(String objectName) throws IllegalArgumentException,
            InstanceNotFoundException, MBeanException, ReflectionException,
            IOException;

    /**
     * @param protocolName
     *            a name of protocol
     * @return the set of references on server connected to this server
     * @throws IllegalArgumentException
     *             if the given protocol name doesn't exist
     * @throws UnsupportedOperationException
     *             if the used manager is at client-side
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     */
    Set<String> getServerRefsForProtocol(String protocolName)
            throws IllegalArgumentException, UnsupportedOperationException,
            InstanceNotFoundException, MBeanException, ReflectionException,
            IOException;

    /**
     * @return the set of clustered object names
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     * @throws AttributeNotFoundException
     */
    Set<String> getObjectNames() throws InstanceNotFoundException,
            MBeanException, ReflectionException, IOException, AttributeNotFoundException;

    /**
     * Returns the name of class of policy for the object with the given name.
     *
     * @param objectName
     *            name of the object
     * @return the name of class of policy for the object with the given name
     * @throws IllegalArgumentException
     *             if none object has the given name
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     */
    String getPolicyClassName(String objectName)
            throws IllegalArgumentException, InstanceNotFoundException,
            MBeanException, ReflectionException, IOException;

    /**
     * Sets a new policy for a given object.
     *
     * @param objectName
     *            a name of object
     * @param policyClassName
     *            a name of class of policy
     * @throws IllegalArgumentException
     *             if none object has the given name
     * @throws UnsupportedOperationException
     *             if the used manager is at client-side
     * @throws ClassNotFoundException
     *             if the class is missing
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     */
    void setPolicyClassName(String objectName, String policyClassName)
            throws IllegalArgumentException, UnsupportedOperationException,
            ClassNotFoundException, InstanceNotFoundException, MBeanException,
            ReflectionException, IOException;

    /**
     * Returns the name of class of strategy for the object with the given name.
     *
     * @param objectName
     *            name of the object
     * @return the name of class of strategy for the object with the given name
     * @throws IllegalArgumentException
     *             if none object has the given name
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     */
    String getStrategyClassName(String objectName)
            throws IllegalArgumentException, InstanceNotFoundException,
            MBeanException, ReflectionException, IOException;

    /**
     * Sets a new strategy for a given object.
     *
     * @param objectName
     *            a name of object
     * @param strategyClassName
     *            a name of class of strategy
     * @throws IllegalArgumentException
     *             if none object has the given name
     * @throws UnsupportedOperationException
     *             if the used manager is at client-side
     * @throws ClassNotFoundException
     *             if the class is missing
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     */
    void setStrategyClassName(String objectName, String strategyClassName)
            throws IllegalArgumentException, UnsupportedOperationException,
            ClassNotFoundException, InstanceNotFoundException, MBeanException,
            ReflectionException, IOException;

    /**
     * Returns the set of property names for the object with the given name.
     *
     * @param objectName
     *            a name of object
     * @return the set of property names for the object with the given name
     * @throws IllegalArgumentException
     *             if none object has the given name
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     */
    Set<String> getPropertiesNamesForPolicy(String objectName)
            throws IllegalArgumentException, InstanceNotFoundException,
            MBeanException, ReflectionException, IOException;

    /**
     * Returns the set of property names (for which value is a list) for the
     * object with the given name.
     *
     * @param objectName
     *            a name of object
     * @return the set of property names for the object with the given name
     * @throws IllegalArgumentException
     *             if none object has the given name
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     */
    Set<String> getListPropertiesNamesForPolicy(String objectName)
            throws IllegalArgumentException, InstanceNotFoundException,
            MBeanException, ReflectionException, IOException;

    /**
     * Returns the value of the property with the given name.
     *
     * @param objectName
     *            a name of object
     * @param propertyName
     *            a name of property
     * @return the value of the property with the given name, or null if there
     *         is not property for this name
     * @throws IllegalArgumentException
     *             if none object has the given name, or if the value is a list
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     */
    String getPropertyForPolicy(String objectName, String propertyName)
            throws IllegalArgumentException, InstanceNotFoundException,
            MBeanException, ReflectionException, IOException;

    /**
     * Returns the list of value of the property with the given name.
     *
     * @param objectName
     *            a name of object
     * @param propertyName
     *            a name of property
     * @return the list of value of the property with the given name, or null if
     *         there is not property for this name
     * @throws IllegalArgumentException
     *             if none object has the given name, or if the value is not a
     *             list
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     */
    List<String> getListPropertyForPolicy(String objectName, String propertyName)
            throws IllegalArgumentException, InstanceNotFoundException,
            MBeanException, ReflectionException, IOException;

    /**
     * Sets a property for a given object. A property is either a String or a
     * list of String.
     *
     * @param objectName
     *            a name of object
     * @param propertyName
     *            a name of property
     * @param propertyValue
     *            a value for the given name of property
     * @throws IllegalArgumentException
     *             if none object has the given name, or if the property doesn't
     *             exist or has an invalid type
     * @throws UnsupportedOperationException
     *             if the used manager is at client-side
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     */
    void setPropertyForPolicy(String objectName, String propertyName,
            String propertyValue) throws IllegalArgumentException,
            UnsupportedOperationException, InstanceNotFoundException,
            MBeanException, ReflectionException, IOException;

    /**
     * Sets a property for a given object. A property is either a String or a
     * list of String.
     *
     * @param objectName
     *            a name of object
     * @param propertyName
     *            a name of property
     * @param propertyValues
     *            a list of value for the given name of property
     * @throws IllegalArgumentException
     *             if none object has the given name, or if the property doesn't
     *             exist or has an invalid type
     * @throws UnsupportedOperationException
     *             if the used manager is at client-side
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     */
    void setListPropertyForPolicy(String objectName, String propertyName,
            List<String> propertyValues) throws IllegalArgumentException,
            UnsupportedOperationException, InstanceNotFoundException,
            MBeanException, ReflectionException, IOException;

    /**
     * Sets the properties for a given object. A property is either a String or
     * a list of String.
     *
     * @param objectName
     *            a name of object
     * @param properties
     *            properties a set of properties
     * @throws IllegalArgumentException
     *             if none object has the given name, or if a property doesn't
     *             exist or has an invalid type
     * @throws UnsupportedOperationException
     *             if the used manager is at client-side
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     */
    void setPropertiesForPolicy(String objectName,
            Map<String, Object> properties) throws IllegalArgumentException,
            UnsupportedOperationException, InstanceNotFoundException,
            MBeanException, ReflectionException, IOException;

    /**
     * Sets the algorithm of load-balancing for the object with the given name.
     *
     * @param objectName
     *            a name of object
     * @param policyClassName
     *            a name of class of policy
     * @param strategyClassName
     *            a name of class of strategy
     * @param properties
     *            a set of properties
     * @throws IllegalArgumentException
     *             if none object has the given name, or if a property doesn't
     *             exist or has an invalid type
     * @throws UnsupportedOperationException
     *             if the used manager is at client-side
     * @throws ClassNotFoundException
     *             if the class is missing
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     */
    void setAlgorithmForPolicy(String objectName, String policyClassName,
            String strategyClassName, Map<String, Object> properties)
            throws IllegalArgumentException, UnsupportedOperationException,
            ClassNotFoundException, InstanceNotFoundException, MBeanException,
            ReflectionException, IOException;

    /**
     * Returns a list of String representing a ServerRef for an object with the
     * given name and protocol.
     *
     * @param objectName
     *            a name of object
     * @param protocolName
     *            a name of protocol
     * @return a list of String representing a ServerRef for an object with the
     *         given name and protocol
     * @throws IllegalArgumentException
     *             if none object has the given name
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     */
    List<String> getServerRefs(String objectName, String protocolName)
            throws IllegalArgumentException, InstanceNotFoundException,
            MBeanException, ReflectionException, IOException;

    /**
     * Returns a list of String representing a ServerRef for an object with the
     * given name.
     *
     * @param objectName
     *            a name of object
     * @return a list of String representing a ServerRef for an object with the
     *         given name
     * @throws IllegalArgumentException
     *             if none object has the given name
     * @throws UnsupportedOperationException
     *             if the used manager is at client-side
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     */
    List<String> getServerRefs(String objectName)
            throws IllegalArgumentException, UnsupportedOperationException,
            InstanceNotFoundException, MBeanException, ReflectionException,
            IOException;

    /**
     * Returns the set of name of cluster.
     *
     * @return the set of name of cluster
     * @throws UnsupportedOperationException
     *             if the used manager is at client-side
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     * @throws AttributeNotFoundException
     */
    Set<String> getClusterNames() throws UnsupportedOperationException,
            InstanceNotFoundException, MBeanException, ReflectionException,
            IOException, AttributeNotFoundException;

    /**
     * Returns the set of object names included in the given cluster.
     *
     * @param clusterName
     *            The cluster name
     * @return a set of object names included in the given cluster
     * @throws UnsupportedOperationException
     *             if the used manager is at client-side
     * @throws IllegalArgumentException
     *             if none cluster has the given name
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     */
    Set<String> getObjectNames(String clusterName)
            throws UnsupportedOperationException, IllegalArgumentException,
            InstanceNotFoundException, MBeanException, ReflectionException,
            IOException;

    /**
     * Returns the time between each update of the cluster view by clients.
     *
     * @return the time between each update of the cluster view by clients
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     * @throws AttributeNotFoundException
     */
    Integer getDelayToRefresh() throws InstanceNotFoundException,
            MBeanException, ReflectionException, IOException, AttributeNotFoundException;

    /**
     * Sets the time between each update of the cluster view by clients.
     *
     * @param delay
     *            the time between each update of the cluster view by clients
     * @throws UnsupportedOperationException
     *             if the used manager is at client-side
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     * @throws InvalidAttributeValueException
     * @throws AttributeNotFoundException
     */
    void setDelayToRefresh(Integer delay) throws UnsupportedOperationException,
            InstanceNotFoundException, MBeanException, ReflectionException,
            IOException, AttributeNotFoundException, InvalidAttributeValueException;

    /**
     * Returns the name of cluster for the object with the given name.
     *
     * @param objectName
     *            a name of object
     * @return the name of cluster for a object with the given name
     * @throws IllegalArgumentException
     *             if none object has the given name
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     */
    String getClusterName(String objectName) throws IllegalArgumentException,
            InstanceNotFoundException, MBeanException, ReflectionException,
            IOException;

    /**
     * Returns the minimal size of pool of CMIReferenceable for a object with
     * the given name.
     *
     * @param objectName
     *            a name of object
     * @return the minimal size of pool of CMIReferenceable for a object with
     *         the given name
     * @throws IllegalArgumentException
     *             if none object has the given name
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     */
    Integer getMinPoolSize(String objectName) throws IllegalArgumentException,
            InstanceNotFoundException, MBeanException, ReflectionException,
            IOException;

    /**
     * Returns the maximal size of pool of CMIReferenceable for a object with
     * the given name.
     *
     * @param objectName
     *            a name of object
     * @return the maximal size of pool of CMIReferenceable for a object with
     *         the given name
     * @throws IllegalArgumentException
     *             if none object has the given name
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     */
    Integer getMaxPoolSize(String objectName) throws IllegalArgumentException,
            InstanceNotFoundException, MBeanException, ReflectionException,
            IOException;

    /**
     * Sets the minimal size of pool of CMIReferenceable for a object with the
     * given name.
     *
     * @param objectName
     *            a name of object
     * @param minPoolSize
     *            the minimal size of pool of CMIReferenceable for a object with
     *            the given name
     * @throws IllegalArgumentException
     *             if no object is bound with the given name
     * @throws UnsupportedOperationException
     *             if the used manager is at client-side
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     */
    void setMinPoolSize(String objectName, Integer minPoolSize)
            throws IllegalArgumentException, UnsupportedOperationException,
            InstanceNotFoundException, MBeanException, ReflectionException,
            IOException;

    /**
     * Sets the maximal size of pool of CMIReferenceable for a object with the
     * given name.
     *
     * @param objectName
     *            a name of object
     * @param maxPoolSize
     *            the maximal size of pool of CMIReferenceable for a object with
     *            the given name
     * @throws IllegalArgumentException
     *             if no object is bound with the given name
     * @throws UnsupportedOperationException
     *             if the used manager is at client-side
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     */
    void setMaxPoolSize(String objectName, Integer maxPoolSize)
            throws IllegalArgumentException, UnsupportedOperationException,
            InstanceNotFoundException, MBeanException, ReflectionException,
            IOException;

    /**
     * Adds a server to the blacklist.
     *
     * @param serverName
     *            a reference on a server
     * @throws UnsupportedOperationException
     *             if the used manager is at client-side
     * @throws MalformedURLException
     *             if the URL is malformed
     * @throws UnknownHostException
     *             if the given host cannot be resolved
     * @throws RemoteException
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     */
    void addServerToBlackList(String serverName)
            throws UnsupportedOperationException, MalformedURLException,
            UnknownHostException, RemoteException, InstanceNotFoundException,
            MBeanException, ReflectionException, IOException;

    /**
     * Removes a server from the blacklist.
     *
     * @param serverName
     *            a reference on a server
     * @throws UnsupportedOperationException
     *             if the used manager is at client-side
     * @throws MalformedURLException
     *             if the URL is malformed
     * @throws UnknownHostException
     *             if the given host cannot be resolved
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     */
    void removeServerFromBlackList(String serverName)
            throws UnsupportedOperationException, MalformedURLException,
            UnknownHostException, InstanceNotFoundException, MBeanException,
            ReflectionException, IOException;

    /**
     * Returns true the server with the given reference if blacklisted.
     *
     * @param serverName
     *            a reference on a server
     * @return true the server with the given reference if blacklisted
     * @throws UnsupportedOperationException
     *             if the used manager is at client-side
     * @throws MalformedURLException
     *             if the URL is malformed
     * @throws UnknownHostException
     *             if the given host cannot be resolved
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     */
    boolean isServerBlackListed(String serverName)
            throws UnsupportedOperationException, MalformedURLException,
            UnknownHostException, InstanceNotFoundException, MBeanException,
            ReflectionException, IOException;

    /**
     * Adds the pool of the object with the given name of the list of pool that
     * should be empty.
     *
     * @param objectName
     *            a name of object
     * @throws IllegalArgumentException
     *             if no object is bound with the given name
     * @throws UnsupportedOperationException
     *             if the used manager is at client-side
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     */
    void addPooltoEmpty(String objectName) throws IllegalArgumentException,
            UnsupportedOperationException, InstanceNotFoundException,
            MBeanException, ReflectionException, IOException;

    /**
     * Removes the pool of the object with the given name of the list of pool
     * that should be empty.
     *
     * @param objectName
     *            a name of object
     * @throws IllegalArgumentException
     *             if no object is bound with the given name
     * @throws UnsupportedOperationException
     *             if the used manager is at client-side
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     */
    void removePoolToEmpty(String objectName) throws IllegalArgumentException,
            UnsupportedOperationException, InstanceNotFoundException,
            MBeanException, ReflectionException, IOException;

    /**
     * Returns true if the pool for object with the given name should be empty.
     *
     * @param objectName
     *            a name of object
     * @return true if the pool for object with the given name should be empty
     * @throws IllegalArgumentException
     *             if no object is bound with the given name
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     */
    boolean isPoolToEmpty(String objectName) throws IllegalArgumentException,
            InstanceNotFoundException, MBeanException, ReflectionException,
            IOException;

    /**
     * Returns the load-factor for the server with the given address.
     *
     * @param serverRef
     *            a reference on a server
     * @return the load-factor for the server with the given address
     * @throws IllegalArgumentException
     *             if none server has the given address
     * @throws MalformedURLException
     *             if the URL is malformed
     * @throws UnknownHostException
     *             if the given host cannot be resolved
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     */
    Integer getLoadFactor(String serverRef) throws IllegalArgumentException,
            MalformedURLException, UnknownHostException,
            InstanceNotFoundException, MBeanException, ReflectionException,
            IOException;

    /**
     * Sets the load-factor for the server with the given address.
     *
     * @param serverRef
     *            a reference on a server
     * @param loadFactor
     *            the load-factor for the server with the given address
     * @throws UnsupportedOperationException
     *             if the used manager is at client-side
     * @throws MalformedURLException
     *             if the URL is malformed
     * @throws UnknownHostException
     *             if the given host cannot be resolved
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     */
    void setLoadFactor(String serverRef, String loadFactor)
            throws UnsupportedOperationException, MalformedURLException,
            UnknownHostException, InstanceNotFoundException, MBeanException,
            ReflectionException, IOException;

    /**
     * @return the numbers of clients connected to a provider of the cluster
     *         view
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     * @throws AttributeNotFoundException
     */
    Integer getNbClientsConnectedToProvider() throws InstanceNotFoundException,
            MBeanException, ReflectionException, IOException, AttributeNotFoundException;

    /**
     * Gets objects hosted on a given server started with a given protocol.
     *
     * @param serverUrl
     *            a server reference
     * @param protocolName
     *            a name of protocol
     * @return set of objects hosted on the server
     * @throws IllegalArgumentException
     *             if the given protocol name doesn't exist or the server is not
     *             registered for the given protocol
     * @throws UnsupportedOperationException
     *             if the used manager is at client-side
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     */
    public Set<String> getServerObjectsForProtocol(final String serverUrl,
            final String protocolName) throws IllegalArgumentException,
            UnsupportedOperationException, InstanceNotFoundException,
            MBeanException, ReflectionException, IOException;

    /**
     * Gets available load balancing policies and strategies.
     *
     * @return available policies and strategies
     * @throws UnsupportedOperationException
     *             if the operation is not supported
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     */
    public Map<String, Set<String>> retrieveAvailablePoliciesAndStrategies()
            throws UnsupportedOperationException, InstanceNotFoundException,
            MBeanException, ReflectionException, IOException;

}
