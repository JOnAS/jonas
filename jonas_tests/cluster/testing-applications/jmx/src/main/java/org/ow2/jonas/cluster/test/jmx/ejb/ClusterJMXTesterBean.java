/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2010 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.cluster.test.jmx.ejb;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.management.Attribute;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.InvalidAttributeValueException;
import javax.management.MBeanException;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.ow2.jonas.lib.management.extensions.manager.ManagementEntryPoint;
import org.ow2.jonas.lib.util.JonasObjectName;

/**
 * This is an example of Session Bean, stateless, secured, available
 * with a Local and a Remote interface (with the same methods).
 * @author JOnAS team
 */
@Stateless(name="clusterJMXTester")
@Remote(RemoteClusterJMXTester.class)
@Local(LocalClusterJMXTester.class)
@TransactionAttribute(TransactionAttributeType.NEVER)
public class ClusterJMXTesterBean implements LocalClusterJMXTester, RemoteClusterJMXTester {

    private String domainName;
    private String serverName;
    private String url;
    private JMXServiceURL jmxService;
    private JMXConnector connector;
    private MBeanServerConnection mbeanserver;
    private ObjectName objectName = null;

    /**
     * Checks if CMI MBean is registered.
     * @throws IOException
     * @throws MalformedObjectNameException
     * @throws RemoteException.
     */
    public void checkCMIMBean() throws MalformedObjectNameException, IOException {
        System.out.println("checkCMIMBean");
        boolean registered = mbeanserver.isRegistered(JonasObjectName.cmiServer(domainName, serverName));
        if (!registered) {
            throw new RemoteException(" CMI ObjectName not registered"
                    + " for domain: "+ domainName
                    + "server: " + serverName);
        }
    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#addPooltoEmpty(java.lang.String)
     */
    public void addPooltoEmpty(String objectName)
    throws IllegalArgumentException, UnsupportedOperationException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
        mbeanserver.invoke(this.objectName, "addPooltoEmpty",
                new String[]{objectName}, new String[]{"java.lang.String"});
        if (!isPoolToEmpty(objectName)) {
            throw new RemoteException("Unable to reset pool for " + objectName);
        }

    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#addServerToBlackList(java.lang.String)
     */
    public void addServerToBlackList(String serverName)
    throws UnsupportedOperationException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
        mbeanserver.invoke(this.objectName, "addPooltoEmpty",
                new String[]{serverName}, new String[]{"java.lang.String"});
        if (!isServerBlackListed(serverName)) {
            throw new RemoteException("Unable blacklist server " + serverName);
        }

    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#getBusinessName(java.lang.String)
     */
    public String getBusinessName(String objectName)
    throws IllegalArgumentException, UnsupportedOperationException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
        return (String)mbeanserver.invoke(this.objectName, "getBusinessName",
                new String[]{objectName}, new String[]{"java.lang.String"});
    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#getClusterName(java.lang.String)
     */
    public String getClusterName(String objectName)
    throws IllegalArgumentException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
        return (String)mbeanserver.invoke(this.objectName, "getClusterName",
                new String[]{objectName}, new String[]{"java.lang.String"});
    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#getClusterNames()
     */
    public Set<String> getClusterNames() throws UnsupportedOperationException, InstanceNotFoundException,
    MBeanException, ReflectionException, IOException, AttributeNotFoundException {
        return(Set<String>) mbeanserver.getAttribute(this.objectName, "ClusterNames");
    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#getDelayToRefresh()
     */
    public Integer getDelayToRefresh() throws InstanceNotFoundException, MBeanException,
    ReflectionException, IOException, AttributeNotFoundException {
        return (Integer) mbeanserver.getAttribute(this.objectName, "DelayToRefresh");
    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#getItfName(java.lang.String)
     */
    public String getItfName(String objectName)
    throws IllegalArgumentException, UnsupportedOperationException, InstanceNotFoundException,
    MBeanException, ReflectionException, IOException {
        return (String)mbeanserver.invoke(this.objectName, "getItfName",
                new String[]{objectName}, new String[]{"java.lang.String"});
    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#getJMXServiceURL(java.lang.String)
     */
    public JMXServiceURL getJMXServiceURL(String protocolName)
    throws IllegalArgumentException, UnsupportedOperationException, InstanceNotFoundException,
    MBeanException, ReflectionException, IOException {
        // TODO Auto-generated method stub
        return (JMXServiceURL)mbeanserver.invoke(this.objectName, "getJMXServiceURL",
                new String[]{protocolName}, new String[]{"java.lang.String"});
    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#getListPropertiesNamesForPolicy(java.lang.String)
     */
    public Set<String> getListPropertiesNamesForPolicy(String objectName)
    throws IllegalArgumentException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
        return (Set<String>)mbeanserver.invoke(this.objectName,
                "getListPropertiesNamesForPolicy",
                new String[]{objectName}, new String[]{"java.lang.String"});
    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#getListPropertyForPolicy(java.lang.String,
     * java.lang.String)
     */
    public List<String> getListPropertyForPolicy(String objectName,
            String propertyName) throws IllegalArgumentException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
        return (List<String>)mbeanserver.invoke(this.objectName,
                "getListPropertyForPolicy",
                new String[]{objectName}, new String[]{"java.lang.String"});
    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#getLoadFactor(java.lang.String)
     */
    public Integer getLoadFactor(String serverRef)
    throws IllegalArgumentException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
        return (Integer)mbeanserver.invoke(this.objectName,
                "getLoadFactor",
                new String[]{serverRef}, new String[]{"java.lang.String"});
    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#getMaxPoolSize(java.lang.String)
     */
    public Integer getMaxPoolSize(String objectName)
    throws IllegalArgumentException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
        return (Integer)mbeanserver.invoke(this.objectName,
                "getMaxPoolSize",
                new String[]{objectName}, new String[]{"java.lang.String"});
    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#getMinPoolSize(java.lang.String)
     */
    public Integer getMinPoolSize(String objectName)
    throws IllegalArgumentException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
        return (Integer)mbeanserver.invoke(this.objectName,
                "getMinPoolSize",
                new String[]{objectName}, new String[]{"java.lang.String"});
    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#getNbClientsConnectedToProvider()
     */
    public Integer getNbClientsConnectedToProvider() throws InstanceNotFoundException,
    MBeanException, ReflectionException, IOException, AttributeNotFoundException {
        return (Integer)mbeanserver.getAttribute(this.objectName, "NbClientsConnectedToProvider");
    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#getObjectName()
     */
    public ObjectName getObjectName() throws InstanceNotFoundException, MBeanException,
    ReflectionException, IOException, AttributeNotFoundException {
        return (ObjectName)mbeanserver.getAttribute(this.objectName, "ObjectName");
    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#getObjectNames()
     */
    public Set<String> getObjectNames() throws InstanceNotFoundException, MBeanException,
    ReflectionException, IOException, AttributeNotFoundException {
        return (Set<String>)mbeanserver.getAttribute(this.objectName, "ObjectNames");
    }

    public Set<String> getObjectNames(String clusterName)
    throws UnsupportedOperationException, IllegalArgumentException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
        return (Set<String>)mbeanserver.invoke(this.objectName,
                "getObjectNames",
                new String[]{clusterName}, new String[]{"java.lang.String"});
    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#getPolicyClassName(java.lang.String)
     */
    public String getPolicyClassName(String objectName)
    throws IllegalArgumentException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
        System.out.println("getPolicyClassName ObjectName" + this.objectName);
        return (String)mbeanserver.invoke(this.objectName,
                "getPolicyClassName",
                new String[]{objectName}, new String[]{"java.lang.String"});
    }

    public Set<String> getPropertiesNamesForPolicy(String objectName)
    throws IllegalArgumentException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
        return (Set<String>)mbeanserver.invoke(this.objectName,
                "getPropertiesNamesForPolicy",
                new String[]{objectName}, new String[]{"java.lang.String"});
    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#getPropertyForPolicy(java.lang.String,
     * java.lang.String)
     */
    public String getPropertyForPolicy(String objectName, String propertyName)
    throws IllegalArgumentException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
        return (String)mbeanserver.invoke(this.objectName,
                "getPropertyForPolicy",
                new String[]{objectName, propertyName},
                new String[]{"java.lang.String", "java.lang.String"});
    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#getProtocols()
     */
    public Set<String> getProtocols() throws InstanceNotFoundException,
    MBeanException, ReflectionException, IOException, AttributeNotFoundException {
        return (Set<String>)mbeanserver.getAttribute(this.objectName, "Protocols");
    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#getRefOnLocalRegistry(java.lang.String)
     */
    public String getRefOnLocalRegistry(String protocolName)
    throws IllegalArgumentException, UnsupportedOperationException, InstanceNotFoundException,
    MBeanException, ReflectionException, IOException {
        return (String)mbeanserver.invoke(this.objectName,
                "getRefOnLocalRegistry",
                new String[]{protocolName}, new String[]{"java.lang.String"});
    }

    public Set<String> getServerObjectsForProtocol(String serverUrl,
            String protocolName) throws IllegalArgumentException,
            UnsupportedOperationException, InstanceNotFoundException,
            MBeanException, ReflectionException, IOException {
        return (Set<String>)mbeanserver.invoke(this.objectName,
                "getServerObjectsForProtocol",
                new String[]{serverUrl, protocolName},
                new String[]{"java.lang.String", "java.lang.String"});
    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#getServerRefs(java.lang.String,
     * java.lang.String)
     */
    public List<String> getServerRefs(String objectName, String protocolName)
    throws IllegalArgumentException, InstanceNotFoundException, MBeanException,
    ReflectionException, IOException {
        return (List<String>)mbeanserver.invoke(this.objectName,
                "getServerRefs",
                new String[]{objectName, protocolName},
                new String[]{"java.lang.String", "java.lang.String"});
    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#getServerRefs(java.lang.String)
     */
    public List<String> getServerRefs(String objectName)
    throws IllegalArgumentException, UnsupportedOperationException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
        return (List<String>)mbeanserver.invoke(this.objectName,
                "getServerRefs",
                new String[]{objectName}, new String[]{"java.lang.String"});
    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#getServerRefsForProtocol(java.lang.String)
     */
    public Set<String> getServerRefsForProtocol(String protocolName)
    throws IllegalArgumentException, UnsupportedOperationException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
        return (Set<String>)mbeanserver.invoke(this.objectName,
                "getServerRefsForProtocol",
                new String[]{protocolName}, new String[]{"java.lang.String"});
    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#getStrategyClassName(java.lang.String)
     */
    public String getStrategyClassName(String objectName)
    throws IllegalArgumentException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
        return (String)mbeanserver.invoke(this.objectName,
                "getStrategyClassName",
                new String[]{objectName}, new String[]{"java.lang.String"});
    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#isPoolToEmpty(java.lang.String)
     */
    public boolean isPoolToEmpty(String objectName)
    throws IllegalArgumentException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
        return (Boolean)mbeanserver.invoke(this.objectName,
                "isPoolToEmpty",
                new String[]{objectName}, new String[]{"java.lang.String"});
    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#isReplicated(java.lang.String)
     */
    public boolean isReplicated(String objectName)
    throws IllegalArgumentException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
        return (Boolean)mbeanserver.invoke(this.objectName,
                "isReplicated",
                new String[]{objectName}, new String[]{"java.lang.String"});
    }

    public boolean isServerBlackListed(String serverName)
    throws UnsupportedOperationException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
        // TODO Auto-generated method stub
        return (Boolean)mbeanserver.invoke(this.objectName,
                "isServerBlackListed",
                new String[]{serverName}, new String[]{"java.lang.String"});
    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#removePoolToEmpty(java.lang.String)
     */
    public void removePoolToEmpty(String objectName)
    throws IllegalArgumentException, UnsupportedOperationException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
        mbeanserver.invoke(this.objectName,
                "removePoolToEmpty",
                new String[]{objectName}, new String[]{"java.lang.String"});
    }

    public void removeServerFromBlackList(String serverName)
    throws UnsupportedOperationException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
        mbeanserver.invoke(this.objectName,
                "removeServerFromBlackList",
                new String[]{serverName}, new String[]{"java.lang.String"});

    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#retrieveAvailablePoliciesAndStrategies()
     */
    public Map<String, Set<String>> retrieveAvailablePoliciesAndStrategies()
    throws UnsupportedOperationException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
        System.out.println("retrieveAvailablePoliciesAndStrategies ObjectName " + this.objectName);
        return (Map<String, Set<String>>)mbeanserver.invoke(this.objectName,
                "retrieveAvailablePoliciesAndStrategies",
                null, null);
    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#setAlgorithmForPolicy(java.lang.String,
     * java.lang.String, java.lang.String, java.util.Map)
     */
    public void setAlgorithmForPolicy(String objectName,
            String policyClassName, String strategyClassName,
            Map<String, Object> properties) throws IllegalArgumentException,
            UnsupportedOperationException, ClassNotFoundException, InstanceNotFoundException,
            MBeanException, ReflectionException, IOException {
        mbeanserver.invoke(this.objectName,
                "setAlgorithmForPolicy",
                new String[]{objectName, policyClassName, strategyClassName},
                new String[]{"java.lang.String", "java.lang.String", "java.lang.String"});
    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#setDelayToRefresh(java.lang.Integer)
     */
    public void setDelayToRefresh(Integer delay)
    throws UnsupportedOperationException, InstanceNotFoundException, MBeanException,
    ReflectionException, IOException, AttributeNotFoundException, InvalidAttributeValueException {
        mbeanserver.setAttribute(this.objectName, new Attribute("DelayToRefresh", delay));
    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#setListPropertyForPolicy(java.lang.String,
     * java.lang.String, java.util.List)
     */
    public void setListPropertyForPolicy(String objectName,
            String propertyName, List<String> propertyValues)
    throws IllegalArgumentException, UnsupportedOperationException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
        mbeanserver.invoke(this.objectName,
                "setListPropertyForPolicy",
                new Object[]{objectName, propertyName, propertyValues},
                new String[]{"java.lang.String", "java.lang.String", "java.util.List"});

    }

    public void setLoadFactor(String serverRef, String loadFactor)
    throws UnsupportedOperationException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
        mbeanserver.invoke(this.objectName,
                "setLoadFactor",
                new String[]{serverRef, loadFactor},
                new String[]{"java.lang.String", "java.lang.String"});

    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#setMaxPoolSize(java.lang.String, java.lang.Integer)
     */
    public void setMaxPoolSize(String objectName, Integer maxPoolSize)
    throws IllegalArgumentException, UnsupportedOperationException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
        mbeanserver.invoke(this.objectName,
                "setMaxPoolSize",
                new Object[]{objectName, maxPoolSize},
                new String[]{"java.lang.String", "java.lang.Integer"});

    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#setMinPoolSize(java.lang.String, java.lang.Integer)
     */
    public void setMinPoolSize(String objectName, Integer minPoolSize)
    throws IllegalArgumentException, UnsupportedOperationException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
        mbeanserver.invoke(this.objectName,
                "setMinPoolSize",
                new Object[]{objectName, minPoolSize},
                new String[]{"java.lang.String", "java.lang.Integer"});

    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#setPolicyClassName(java.lang.String, java.lang.String)
     */
    public void setPolicyClassName(String objectName, String policyClassName)
    throws IllegalArgumentException, UnsupportedOperationException,
    ClassNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
        mbeanserver.invoke(this.objectName,
                "setPolicyClassName",
                new String[]{objectName, policyClassName},
                new String[]{"java.lang.String", "java.lang.String"});

    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#setPropertiesForPolicy(java.lang.String, java.util.Map)
     */
    public void setPropertiesForPolicy(String objectName,
            Map<String, Object> properties) throws IllegalArgumentException,
            UnsupportedOperationException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
        mbeanserver.invoke(this.objectName,
                "setPropertiesForPolicy",
                new Object[]{objectName, properties},
                new String[]{"java.lang.String", "java.util.Map"});

    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#setPropertyForPolicy(java.lang.String, java.lang.String, java.lang.String)
     */
    public void setPropertyForPolicy(String objectName, String propertyName,
            String propertyValue) throws IllegalArgumentException,
            UnsupportedOperationException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
        mbeanserver.invoke(this.objectName,
                "setPropertyForPolicy",
                new Object[]{objectName, propertyName, propertyValue},
                new String[]{"java.lang.String", "java.lang.String", "java.lang.String"});

    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester#setStrategyClassName(java.lang.String, java.lang.String)
     */
    public void setStrategyClassName(String objectName, String strategyClassName)
    throws IllegalArgumentException, UnsupportedOperationException,
    ClassNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
        mbeanserver.invoke(this.objectName,
                "setStrategyClassName",
                new Object[]{objectName, strategyClassName},
                new String[]{"java.lang.String", "java.lang.String"});


    }
    /**
     * Initializes the JMX connection.
     */
    public void init() throws Exception {
        System.out.println("initializing testerBean");
        domainName = ManagementEntryPoint.getInstance().getDomainName();
        serverName = ManagementEntryPoint.getInstance().getServerName();
        url = ManagementEntryPoint.getInstance().getConnectionUrl(null);
        jmxService = new JMXServiceURL(url);
        connector = JMXConnectorFactory.connect(jmxService);
        mbeanserver = connector.getMBeanServerConnection();
        objectName = JonasObjectName.cmiServer(domainName, serverName);
        checkCMIMBean();
    }



}
