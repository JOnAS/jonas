/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2010 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.cluster.test.jmx.client;


import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.ow2.jonas.cluster.test.jmx.common.ClientBase;
import org.ow2.jonas.cluster.test.jmx.common.ClientUtility;
import org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester;
import org.ow2.jonas.cluster.test.jmx.ejb.RemoteClusterFangBean;

public class ClientJMXTester extends ClientBase{

    /**
     * Link to the tester bean.
     */
    private ClusterJMXTester testerBean;
    /**
     * Link to the fang bean.
     */
    RemoteClusterFangBean fangBean = null;
    /**
     * @param args
     * @param args Command line arguments
     * @throws Exception
     * @throws Exception InitialContext creation failure / JMS Exception
     */

    public ClientJMXTester(String[] args) throws Exception {
        super(args);
        // Print Header
        out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
        out.println("OW2 JOnAS :: Cluster Examples :: JMX client for CMI     ");
        out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
        testerBean = ClientUtility.getJMXTesterBean();
        fangBean = ClientUtility.getFangBean();
        out.println("Init successful for " + clientName + ".");

    }

    private void run() throws Exception {
        out.println(clientName + ", initiliazing JMX connection to CMI");
        out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
        testerBean.init();
        out.println("CMI objectName: " + testerBean.getObjectName());
        out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
        Set<String> names = testerBean.getObjectNames();
        out.println("Registered objects: " + names);
        out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
        Set<String> clusters = testerBean.getClusterNames();
        out.println("Cluster names: " + clusters);
        out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
        out.println("Is " + clusterName + " present in cluster names ?: "
                + clusters.contains(clusterName));
        out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
        out.println("Delay to refresh: " + testerBean.getDelayToRefresh());
        out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
        out.println("Connected clients: " +
                testerBean.getNbClientsConnectedToProvider());
        out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
        out.println("policies and strategies: " +
                testerBean.retrieveAvailablePoliciesAndStrategies());
        out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
        out.println("Is " + fangBeanName + " replicated ?: " +
                testerBean.isReplicated(fangBeanName));
        out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
        out.println("Fang description: " + fangBean.getFangDescription());
        out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
        out.println("Fang web sites: " + fangBean.getFangWebsites());
        out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
        setPolicyStrategy();
        addPoolToEmpty();
        //blacklistServers();
        getBusinessName();
        setDelay2Refresh();

    }
    /**
     * Set delay for refreshing clients view.
     * @return
     * @throws Exception
     */
    private void setDelay2Refresh() throws Exception {
        testerBean.setDelayToRefresh(delayToRefresh);
    }

    private void changePolicy(String clusteredObject, String policyClass)
    throws Exception {
        testerBean.setPolicyClassName(clusteredObject, policyClass);
        out.println(clientName +
                ". Clustered object: "
                + clusteredObject +  ". Policy:"
                + testerBean.getPolicyClassName(clusteredObject));
    }

    private void changeStrategy(String clusteredObject, String strategyClass)
    throws Exception {
        testerBean.setStrategyClassName(clusteredObject, strategyClass);
        out.println(clientName +". Clustered object: "
                + clusteredObject +  ". Strategy:"
                + testerBean.getStrategyClassName(clusteredObject));
        out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
    }


    /**
     * Clears the pool.
     * @throws Exception, any.
     */
    private void addPoolToEmpty() throws Exception {
        out.println(clientName +". Clustered object: "
                + fangBeanName +  ". Clear pool:");
        testerBean.addPooltoEmpty(fangBeanName);
        if (!testerBean.isPoolToEmpty(fangBeanName)) {
            throw new Exception(clientName + ". Unable clear pool for object "
                    + fangBeanName);
        }
    }


    /**
     * Try to blacklist/unblacklist each server.
     * @throws Exception, any.
     */
    private void blacklistServers() throws Exception{
        out.println(clientName + ". blacklist servers.");
        Set<String> protos = testerBean.getProtocols();
        for (String proto : protos) {
            Set<String> srvRefs = testerBean.getServerRefsForProtocol(proto);
            for (String ref : srvRefs) {
                out.println(clientName + ". blacklisting server "
                        + ref + " on protocol " + proto);
                testerBean.addServerToBlackList(ref);
                Thread.currentThread().sleep(time2sleep);
                if (!testerBean.isServerBlackListed(ref)) {
                    throw new Exception(clientName + ". unable to blacklist server "
                            + ref + " on protocol " + proto);
                }
                out.println(clientName + ". unblacklisting server "
                        + ref + " on protocol " + proto);
                testerBean.removeServerFromBlackList(ref);
                Thread.currentThread().sleep(time2sleep);
                if (testerBean.isServerBlackListed(ref)) {
                    throw new Exception(clientName + ". unable to blacklist server "
                            + ref + " on protocol " + proto);
                }

            }
        }
    }

    private void getBusinessName() throws Exception {
        out.println(clientName +". Clustered object: "
                + fangBeanName +  ". Business name:" +
                testerBean.getBusinessName(fangBeanName));
    }

    /**
     * Try to set each strategy/policy, available in CMI
     * to the clustered object <code>beanName</code>.
     * @throws Exception, any.
     */
    @SuppressWarnings("unchecked")
    private void setPolicyStrategy() throws Exception {
        Map<String, Set<String>> lb = testerBean.retrieveAvailablePoliciesAndStrategies();
        Set<String> policies = null;
        Set<String> strategies = null;
        Set<Entry<String, Set<String>>> set = lb.entrySet();
        for (Iterator iterator = set.iterator(); iterator.hasNext();) {
            Entry<String, Set<String>> entry = (Entry<String, Set<String>>) iterator
            .next();
            if (entry.getKey().contains("poli")) {
                policies = entry.getValue();
            }else {
                strategies = entry.getValue();
            }
        }

        for(String policy: policies) {
            changePolicy(fangBeanName, policy);
            for(String strategy: strategies) {
                changeStrategy(fangBeanName, strategy);
            }
        }
    }

    public static void main(final String[] args) throws Exception {
        ClientJMXTester jmxTester = new ClientJMXTester(args);
        jmxTester.run();
    }
}
