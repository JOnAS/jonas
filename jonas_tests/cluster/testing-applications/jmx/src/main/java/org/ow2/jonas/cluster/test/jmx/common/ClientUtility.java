/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2010 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.cluster.test.jmx.common;


import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.ow2.jonas.cluster.test.jmx.ejb.ClusterJMXTester;
import org.ow2.jonas.cluster.test.jmx.ejb.RemoteClusterFangBean;

/**
 * Fat client : generic methods
 */
public class ClientUtility {

    /**
     * Constructor. Hide constructor as it is an utility class
     */
    private ClientUtility() {
    }

    /**
     * @return MyEjb1 home object
     * @throws Exception any.
     */
    public static ClusterJMXTester getJMXTesterBean() throws Exception {

        Context ctx = null;
        ClusterJMXTester bean = null;
        try {
            ctx = new InitialContext();
        } catch (NamingException e) {
            throw new Exception("Exception in getJMXTesterBean. Unable to create context ", e);
        }

        // Lookup bean.
        try {
            bean = (ClusterJMXTester) ctx.lookup("org.ow2.jonas.cluster.test.jmx.ejb." +
                    "ClusterJMXTesterBean_org.ow2.jonas.cluster." +
                    "test.jmx.ejb.RemoteClusterJMXTester@Remote");
            ctx.close();
        } catch (Exception e) {
            //e.printStackTrace();
            //System.exit(2);
            throw new Exception("Exception in getJMXTesterBean. Unable to get bean reference ", e);
        }

        return bean;
    }

    /**
     * @return MyEjb1 home object
     * @throws Exception any.
     */
    public static RemoteClusterFangBean getFangBean() throws Exception {

        Context ctx = null;
        RemoteClusterFangBean bean = null;
        try {
            ctx = new InitialContext();
        } catch (NamingException e) {
            throw new Exception("Exception in getFangBean. Unable to create context ", e);
        }

        // Lookup bean.
        try {
            bean = (RemoteClusterFangBean) ctx.lookup("fangbean");
            ctx.close();
        } catch (Exception e) {
            throw new Exception("Exception in getFangBean. Unable to get bean reference ", e);
        }

        return bean;
    }
}

