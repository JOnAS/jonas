/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2010 Bull S.A.S
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.cluster.test.jmx.client;

import java.io.File;
import java.util.ArrayList;

import junit.framework.TestSuite;

import org.ow2.jonas.cluster.test.common.AbsTestClient;
import org.ow2.jonas.cluster.test.common.ClientTask;
import org.ow2.jonas.cluster.test.common.RunnableTask;
import org.ow2.jonas.cluster.test.common.Worker;

public class JMXTest extends AbsTestClient {
    /**
     * The worker
     */
    protected static Worker jworker = null;

    public static void main(String args[]) {
        junit.textui.TestRunner.run(suite());
    }

    public static TestSuite suite() {
        return new TestSuite(JMXTest.class);
    }

    public JMXTest(String s) {
        super(s);
    }

    protected void setUp() throws Exception {
        super.setUp();
    }

    /**
     * Test Exception.
     * @throws Exception any.
     */
    public void testJMXCmi() throws Exception {
        LOGGER.info("cluster-jmx-cmi.testJMX");
        RunnableTask task = null;
        ArrayList<String> arg = new ArrayList<String>();
        for(int i = 0; i < clientsNumber; i++) {
            arg.add("-clientName");
            arg.add("cluster-jmx-cmi.testJMX_" + i);
            arg.add("-out");
            arg.add(outputDir + File.separator + "cluster-jmx-cmi.testJMX_" + i + ".txt");
            arg.add("-time2sleep");
            arg.add("20000");
            task = new ClientTask("cluster-jmx-cmi.testJMX_" + i,
                    "org.ow2.jonas.cluster.test.jmx.client.ClientJMXTester", arg.toArray(new String[arg.size()]));
            worker.addTask(task);
        }
        System.out.println("running the worker");
        worker.run();
        /**start the checker **/
        checker.start();
        while(!checker.stopped()) {
            // sleep.
            Thread.currentThread().sleep(checkerSleepTime);
        }
        LOGGER.info("Worker has terminated");
        if(worker.hasFailure()) {
            fail("on cluster-jmx-cmi.testJMX" + worker.getTasksFailures());
        }
        worker.clear();
        LOGGER.info("cluster-jmx-cmi.testJMX is ok");
    }
}
