/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2010 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.cluster.test.jmx.ejb;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.ow2.cmi.annotation.Cluster;
import org.ow2.cmi.annotation.Policy;
import org.ow2.cmi.annotation.Strategy;
import org.ow2.cmi.lb.policy.FirstAvailable;
import org.ow2.cmi.lb.strategy.NoStrategy;

/**
 * @author eyindanga
 *
 */
@Cluster(name="fangcluster")
@Policy(FirstAvailable.class)
@Strategy(NoStrategy.class)
@Remote(RemoteClusterFangBean.class)
@Stateful(mappedName="fangbean")
@TransactionAttribute(TransactionAttributeType.NEVER)
public class ClusterFangBean implements RemoteClusterFangBean{

    /**
     * Web sites.
     */
    List<String> webSites = new ArrayList<String>();
    /**
     * Description.
     */
    String description = "Fang is an ethnic group of central africa";

    public void addFangWebsite(String wSite) throws RemoteException {
        webSites.add(wSite);
    }

    public List<String> getFangWebsites() throws RemoteException {
        return webSites;
    }

    public String getFangDescription() throws RemoteException {
        return description;
    }

    public void removeFangWebsite(String wSite) throws RemoteException {
        webSites.remove(wSite);
    }

    public void setDescription(String desc) throws RemoteException {
        description = desc;
    }

}
