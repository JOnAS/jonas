/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2010 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.cluster.test.jmx.ejb;

import java.rmi.RemoteException;
import java.util.List;

/**
 * @author zeng-ls
 *
 */
public interface RemoteClusterFangBean {
    /**
     * Gets description of fang group.
     * @return Description of fang group
     * @throws RemoteException, any.
     */
    String getFangDescription() throws RemoteException;

    /**
     * Sets description.
     * @param desc, the description to set.
     * @throws RemoteException, any.
     */
    void setDescription(String desc) throws RemoteException;

    /**
     * Gets fang web sites.
     * @return fang web sites.
     * @throws RemoteException, any.
     */
    List<String> getFangWebsites() throws RemoteException;

    /**
     * Adds fang web site.
     * @param wSite the web site to add
     * @throws RemoteException, any.
     */
    void addFangWebsite(String wSite) throws RemoteException;

    /**
     * Removes fang web site.
     * @param wSite the web site to remove
     * @throws RemoteException, any.
     */
    void removeFangWebsite(String wSite) throws RemoteException;
}
