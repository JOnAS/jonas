#!/bin/sh
# ---------------------------------------------------------------------------
# Protocols test
# Copyright (C) 2007 Bull S.A.S.
# Contact: jonas-team@objectweb.org
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
#
# ---------------------------------------------------------------------------
# $Id: $
# ---------------------------------------------------------------------------

# Launches a simple test
# Usage : client.sh multi <clientnb> <iterationNb> 0 <type> with
#  clientnb : the threads nb
#  iterationNb : the loop nb
#  type : the sort of test among  Remote CreateRemote LookupCreateRemote MetaData
#  Ex : client.sh multi 5 200 0 Remote

java -version
export JAVA_OPTS="-Dmonolog.wrappers=org.ow2.carol.cmi.configuration.TraceCmi $JAVA_OPTS"
export APPLI=$JONAS_BASE/apps/autoload/applicall.ear
java $JAVA_OPTS -jar $JONAS_ROOT/lib/client.jar -carolFile ./clientconf/carol.properties -traceFile ./clientconf/traceclient.properties $APPLI $*
