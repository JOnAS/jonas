/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id:  $
 * --------------------------------------------------------------------------
 */

package org.objectweb.j2eemanagement.servlets;

import java.util.ArrayList;
import java.util.Date;
import java.util.logging.LogRecord;

import javax.management.Notification;

/**
 * @author Adriana Danes
 */
public class MonologListener extends MyListener {

    private final static String monologNotifType = "Monolog.JMXHandler.Log";

//  ---------------------------------------------------------- Public methods
    /**
     * Create a new element and add it to the list.
     * @param notification received notification
     * @param handback received handback
     */
    public void handleNotification(Notification notification, Object handback) {
        String type = notification.getType();
        if (type.equals(monologNotifType)) {
            String message = notification.getMessage();
            LogRecord record = (LogRecord) notification.getUserData();
            String timestamp =  new Date(record.getMillis()).toString();
            String loggerName =  record.getLoggerName();
            String level = record.getLevel().toString();
            String sourceClass = record.getSourceClassName();
            String sourceMethod = record.getSourceMethodName();
            String resourceBundle = record.getResourceBundleName();
            StringBuffer buf = new StringBuffer();
            buf.append("Notification message = ");
            buf.append(message);
            buf.append('\n');

            buf.append("Timestamp = ");
            buf.append(timestamp);
            buf.append('\n');

            buf.append("LoggerName = ");
            buf.append(loggerName);
            buf.append('\n');

            buf.append("Level = ");
            buf.append(level);
            buf.append('\n');

            buf.append("SourceClass = ");
            buf.append(sourceClass);
            buf.append('\n');
            String s = new String(buf);
            getListNotifications().add(s);
        } else {
            super.handleNotification(notification, handback);
        }
    }


}
