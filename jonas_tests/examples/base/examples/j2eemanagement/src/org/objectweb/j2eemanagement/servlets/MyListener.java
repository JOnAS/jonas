/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.j2eemanagement.servlets;

import java.util.ArrayList;

import javax.management.NotificationListener;
import javax.management.Notification;

/**
 * @author Adriana Danes
 * @author Michel-Ange Anton
 */
public class MyListener implements NotificationListener {

// ---------------------------------------------------------- Properties variables
    /**
     * Each element in this list contains a received notification + infos about it
     */
    private ArrayList listNotifications = null;

//  ---------------------------------------------------------- Constructor
    /**
     * Create list.
     */
    public MyListener() {
        listNotifications = new ArrayList();
    }

//  ---------------------------------------------------------- Public methods
    /**
     * Create a new element and add it to the list.
     * @param notification received notification
     * @param handback received handback
     */
    public void handleNotification(Notification notification, Object handback) {
        String s = "Notification = " + notification.toString()
                   + ", Handback = " + handback.toString();
        listNotifications.add(s);
    }

//  ---------------------------------------------------------- Properties methods
    /**
     * @return notifications list
     */
    public ArrayList getListNotifications() {
        return listNotifications;
    }

}
