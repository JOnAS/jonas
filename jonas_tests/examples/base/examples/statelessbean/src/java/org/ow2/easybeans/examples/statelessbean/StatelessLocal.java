/**
 * EasyBeans
 * Copyright (C) 2006 Bull S.A.S.
 * Contact: easybeans@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.easybeans.examples.statelessbean;

/**
 * Simple interface for a stateless bean.
 * It acts as a local interface.
 * @author Florent Benoit
 */
public interface StatelessLocal {

    /**
     * Hello world.
     */
    void helloWorld();

    /**
     * Compute a + b.
     * @param a first int
     * @param b second int
     * @return a + b
     */
    int add(final int a, final int b);

    /**
     * Divide a by b.
     * @param a first int
     * @param b second int
     * @return a / b
     */
    int div(final int a, final int b);


    /**
     * Methods without interceptors.
     */
    void notInterceptedMethod();
}
