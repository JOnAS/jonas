/**
 * EasyBeans
 * Copyright (C) 2006 Bull S.A.S.
 * Contact: easybeans@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.easybeans.examples.statelessbean;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.interceptor.AroundInvoke;
import javax.interceptor.ExcludeClassInterceptors;
import javax.interceptor.Interceptors;
import javax.interceptor.InvocationContext;


import org.ow2.cmi.annotation.Cluster;
import org.ow2.cmi.annotation.Policy;
import org.ow2.cmi.lb.policy.RoundRobin;

/**
 * Simple stateless bean.
 * @author Florent Benoit
 */
@Stateless
@Local(StatelessLocal.class)
@Remote(StatelessRemote.class)
@Interceptors({StatelessInterceptor.class})
@Cluster(name="test_cluster")
@Policy(RoundRobin.class)
public class StatelessBean implements StatelessRemote {

    /**
     * Hello world.
     */
    public void helloWorld() {
        System.out.println("Hello world !");
    }

    /**
     * Compute a + b.
     * @param a first int
     * @param b second int
     * @return a + b
     */
    @Interceptors({StatelessOnlyAddMethodInterceptor.class})
    public int add(final int a, final int b) {
        return a + b;
    }

    /**
     * Divide a by b.
     * @param a first int
     * @param b second int
     * @return a / b
     */
    public int div(final int a, final int b) {
        if (b == 0) {
            throw new IllegalArgumentException("cannot divide by 0");
        }
        return a / b;
    }

    /**
     * Methods without interceptors.
     */
    @ExcludeClassInterceptors
    public void notInterceptedMethod() {

    }


    /**
     * Trace method's time.
     * @param invocationContext contains attributes of invocation
     * @return method's invocation result
     * @throws Exception if invocation fails
     */
    @AroundInvoke
    public Object trace(final InvocationContext invocationContext) throws Exception {
        System.out.println("TraceInterceptor : method '" + invocationContext.getMethod().getName()
                + "'.");
        long startPeriod = System.nanoTime();
        try {
            return invocationContext.proceed();
        } finally {
            long elapsed = System.nanoTime() - startPeriod;
            System.out.println("TraceInterceptor : Elapsed time = " + elapsed + " ns");
        }
    }
}
