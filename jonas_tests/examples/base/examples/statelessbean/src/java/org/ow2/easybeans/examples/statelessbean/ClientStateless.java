/**
 * EasyBeans
 * Copyright (C) 2006 Bull S.A.S.
 * Contact: easybeans@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.easybeans.examples.statelessbean;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * Simple client of the stateless.
 * @author Florent Benoit
 */
public final class ClientStateless {

    /**
     * Default InitialContextFactory to use.
     */
    private static final String DEFAULT_INITIAL_CONTEXT_FACTORY = "org.ow2.carol.jndi.spi.MultiOrbInitialContextFactory";

    /**
     * Utility class.
     */
    private ClientStateless() {

    }

    /**
     * Main method.
     * @param args the arguments (not required)
     * @throws Exception if exception is found.
     */
    public static void main(final String[] args) throws Exception {

        Context initialContext = getInitialContext();

        // Thread.currentThread().sleep(5000);

        StatelessRemote statelessBean = (StatelessRemote) initialContext
                .lookup("org.ow2.easybeans.examples.statelessbean.StatelessBean"
                        + "_" + StatelessRemote.class.getName() + "@Remote");

        System.out.println("Calling the first method...");
        System.out.println("Calling helloWorld method...");
        statelessBean.helloWorld();

        System.out.println("-----------------------------");
        System.out.println("-----------------------------");
        System.out.println("Calling the secode method...");
        System.out.println("Add 1 + 2...");
        int resultAdd = statelessBean.add(1, 2);
        System.out.println("Sum = '" + resultAdd + "'.");

        System.out.println("-----------------------------");
        System.out.println("-----------------------------");
        System.out.println("Calling the third method...");
        System.out.println("Div 6 / 3 (expect exception)...");
        int resultDiv = statelessBean.div(6, 3);
        System.out.println("Div = '" + resultDiv + "'.");
    }

    /**
     * @return Returns the InitialContext.
     * @throws NamingException If the Context cannot be created.
     */
    private static Context getInitialContext() throws NamingException {

        // if user don't use jclient/client container
        // we can specify the InitialContextFactory to use
        // But this is *not recommended*.
        Hashtable<String, Object> env = new Hashtable<String, Object>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, getInitialContextFactory());

        // Usually a simple new InitialContext() without any parameters is sufficent.
        // return new InitialContext();

        return new InitialContext(env);
    }

    /**
     * Returns a configurable InitialContextFactory classname.<br/>
     * Can be configured with the <code>easybeans.client.initial-context-factory</code> System property.
     * @return Returns a configurable InitialContextFactory classname.
     */
    private static String getInitialContextFactory() {
        String prop = System.getProperty("easybeans.client.initial-context-factory");
        // If not found, use the default
        if (prop == null) {
            prop = DEFAULT_INITIAL_CONTEXT_FACTORY;
        }
        return prop;
    }


}
