/**
 * EasyBeans
 * Copyright (C) 2006 Bull S.A.S.
 * Contact: easybeans@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.easybeans.examples.statelessbean;

import java.lang.reflect.Method;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;


/**
 * Simple interceptor class which should be launched only on add method.
 * @author Florent Benoit
 */
public class StatelessOnlyAddMethodInterceptor {

    /**
     * Another class interceptor.
     * @param invocationContext contains attributes of invocation
     * @return method's invocation result
     * @throws Exception if invocation fails
     */
    @AroundInvoke
    public Object onlyAdd(final InvocationContext invocationContext) throws Exception {
        System.out.println("Add interceptor");
        Method m = invocationContext.getMethod();
        // Checks the method name
        if (m.getName().equals("add")) {
            System.out.println("Applied on the correct method");
        } else {
            throw new Exception("Error, should be only on add method while it is applied on '" + m.getName() + "' method.");
        }
        return invocationContext.proceed();
    }

}
