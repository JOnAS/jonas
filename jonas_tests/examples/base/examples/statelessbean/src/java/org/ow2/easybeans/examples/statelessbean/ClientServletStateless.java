/**
 * EasyBeans
 * Copyright (C) 2006 Bull S.A.S.
 * Contact: easybeans@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.easybeans.examples.statelessbean;

import java.io.IOException;
import java.io.PrintWriter;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet's client for the stateless session bean.
 * @author Florent Benoit
 */
public class ClientServletStateless extends HttpServlet {

    /**
     * Serializable class uid.
     */
    private static final long serialVersionUID = 6893863749912962928L;

    /**
     * Called by the server (via the service method) to allow a servlet to
     * handle a GET request.
     * @param request an HttpServletRequest object that contains the request the
     *        client has made of the servlet
     * @param response an HttpServletResponse object that contains the response
     *        the servlet sends to the client
     * @throws IOException if an input or output error is detected when the
     *         servlet handles the GET request
     * @throws ServletException if the request for the GET could not be handled
     */
    @Override
    public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head>");
        out.println("<title>");
        out.println("Client of stateless session bean</title>");
        out.println("</head>");
        out.println("<body>");

        // no operation ? displays button for hello world and calculator
        String operation = request.getParameter("operation");
        if (operation != null) {
            if (operation.equals("helloWorld")) {
                displayHelloWorld(out);
            } else if (operation.equals("add")) {
                // get two parameters
                String param1 = request.getParameter("p1");
                String param2 = request.getParameter("p2");
                if (param1 != null && param2 != null) {
                    int v1 = Integer.parseInt(param1);
                    int v2 = Integer.parseInt(param2);
                    displayResult(out, v1, v2);
                } else {
                    out.println("Missing values for operation add");
                }
            }
        }
        out.println("<hr width=\"80%\"/>");
        displayDefault(out);


        out.println("</body>");
        out.println("</html>");
        out.close();
    }

    /**
     * Call HelloWorld method.
     * @param out the given writer
     */
    private void displayHelloWorld(final PrintWriter out) {
        out.println("Calling helloWorld() method");
        out.println("<br>");
        try {
            getBean().helloWorld();
            out.println("helloWorld() method called OK.");
        } catch (Exception e) {
            displayException(out, "Cannot call helloworld on the bean", e);
        }
    }

    /**
     * By default, call helloWorld method.
     * @param out the given writer
     */
    private void displayDefault(final PrintWriter out) {
        out.println("<form method=get action=\"\" enctype=\"multipart/form-data\">");
        out.println("sum of a + b :");
        out.println("<p><input type=hidden name=\"operation\" value=\"add\"></p>");
        out.println("<p><input type=text name=p1 value=\"1\"></p>");
        out.println("<p><input type=text name=p2 value=\"2\"></p>");
        out.println("<p><input type=submit value=\"add !\"></p>");
        out.println("</form>");
        out.println("<form method=get action=\"\" enctype=\"multipart/form-data\">");
        out.println("<p><input type=hidden name=\"operation\" value=\"helloWorld\"></p>");
        out.println("<p><input type=submit value=\"hello world !\"></p>");
        out.println("</form>");
    }

    /**
     * Prints he result of the sum of val1 and val2.
     * @param out the given writer
     * @param val1 first arg for add method
     * @param val2 second arg for add method
     */
    private void displayResult(final PrintWriter out, final int val1, final int val2) {
        out.println("<br> Sum of '" + val1 + "' and '" + val2 + "' = ");
        try {
            int sum = getBean().add(val1, val2);
            out.println(sum);
        } catch (Exception e) {
            displayException(out, "<br>Cannot call add() method on the bean", e);
        }
    }

    /**
     * If there is an exception, print the exception.
     * @param out the given writer
     * @param errMsg the error message
     * @param e the content of the exception
     */
    private void displayException(final PrintWriter out, final String errMsg, final Exception e) {
        out.println("<p>Exception : " + errMsg);
        out.println("<pre>");
        e.printStackTrace(out);
        out.println("</pre></p>");
    }

    /**
     * Lookup the stateless bean and gets a reference on it.
     * @return the stateless bean business interface.
     * @throws Exception if the bean cannot be retrieved.
     */
    private StatelessRemote getBean() throws Exception {
        Context initialContext = new InitialContext();
        Object o = initialContext.lookup("org.ow2.easybeans.examples.statelessbean.StatelessBean" + "_"
                + StatelessRemote.class.getName() + "@Remote");

        if (o instanceof StatelessRemote) {
            StatelessRemote statelessBean = (StatelessRemote) o;
            return statelessBean;
        }
        throw new Exception("Cannot cast object into StatelessRemote");

    }

}
