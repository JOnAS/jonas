/**
 * EasyBeans
 * Copyright (C) 2006 Bull S.A.S.
 * Contact: easybeans@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.easybeans.examples.statelessbean;

import java.lang.reflect.Method;

import javax.annotation.PostConstruct;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

/**
 * Simple interceptor class. Mix of business interceptors and lifecycle interceptors.
 * @author Florent Benoit
 */
public class StatelessInterceptor {

    /**
     * Another class interceptor.
     * @param invocationContext contains attributes of invocation
     * @return method's invocation result
     * @throws Exception if invocation fails
     */
    @SuppressWarnings("finally")
    @AroundInvoke
    public Object toto(final InvocationContext invocationContext) throws Exception {
        System.out.println("Another class interceptor");
        Method m = invocationContext.getMethod();
        // Change the returned value for add method
        Object returnedValue = null;
        try {
            returnedValue = invocationContext.proceed();
        } finally {
            if (m.getName().equals("add")) {
                // Uncomment the following line to change the returned value
                //return Integer.valueOf(-1);
                return returnedValue;
            }
            return returnedValue;
        }
    }

    /**
     * Callback in an external class.
     * @param invocationContext the invocationcontext.
     */
    @PostConstruct
    public void myPostConst(final InvocationContext invocationContext) {
        System.out.println("postConstruct in class Statelesslistener of bean " + invocationContext.getTarget());
        try {
            invocationContext.proceed();
        } catch (Exception e) {
            throw new RuntimeException("Cannot proceed invocationContext", e);
        }
    }

}
