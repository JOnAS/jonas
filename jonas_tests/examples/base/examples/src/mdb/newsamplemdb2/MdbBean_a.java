/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): ____________________________________.
 * Contributor(s): ______________________________________.
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

// MdbBean_a.java
// Message Driven  bean

package newsamplemdb2;

import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;



/**
 * Superclass of the MessageDrivenBean class MdbBean
 * Note that this class is not public but ejbCreate is public
 * (it's only for reproducing the 300387 bug!)
 */
 class MdbBean_a  implements MessageDrivenBean {

    private transient MessageDrivenContext mdbContext;



    // ------------------------------------------------------------------
    // MessageDrivenBean implementation
    // ------------------------------------------------------------------

    /**
     * Default constructor
     *
     */
    public MdbBean_a() {
    }

    /**
     * Set the associated context. The container call this method
     * after the instance creation.
     * The enterprise Bean instance should store the reference to the context
     * object in an instance variable.
     * This method is called with no transaction context.
     *
     * @param ctx A MessageDrivenContext interface for the instance.
     */

    public void setMessageDrivenContext(MessageDrivenContext ctx) {
    System.out.println("MdbBean_a setMessageDrivenContext");
    mdbContext = ctx;
    }

    /**
     * A container invokes this method before it ends the life of the message-driven object.
     * This happens when a container decides to terminate the message-driven object.
     *
     * This method is called with no transaction context.
     *
     */
    public void ejbRemove() {
    System.out.println("MdbBean_a ejbRemove");
    }

    /**
     * The Message driven  bean must define an ejbCreate methods with no args.
     *
     */
    public void ejbCreate() {
    System.out.println("MdbBean_a ejbCreate");
    }


}
