/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): ____________________________________.
 * Contributor(s): ______________________________________.
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

// MdbClient.java
// mini Client for accessing bean Mdb

package newsamplemdb2;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.*;
import javax.jms.*;

/**
 *
 */
public class MdbClient {

    static Context ictx = null;
    static QueueConnectionFactory qcf = null;
    static QueueSender qp = null;
    static Queue queue = null;
    // JNDI name of the Queue
    static String queueName = "mdbQueue";
    // JNDI name of the default connection factory
    static String conFactName = "JQCF";

    public static void main(String[] arg) {
        // Get InitialContext
        try {
            ictx = new InitialContext();
            // lookup the QueueConnectionFactory through its JNDI name
            qcf = (QueueConnectionFactory) ictx.lookup(conFactName );
            System.out.println("JMS client: qcf = " + qcf);
            // lookup the Queue through its JNDI name
            queue = (Queue) ictx.lookup(queueName);
            System.out.println("JMS client: queue = " + queue);
        } catch (NamingException e) {
            e.printStackTrace();
            System.exit(2);
        }

        QueueConnection qc = null;
        QueueSession session = null;
        try {
            qc = qcf.createQueueConnection();
            System.out.println("JMS client: qc = " + qc.toString());
            session = qc.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
            qp = session.createSender(queue);
        }catch (Exception e) {
            e.printStackTrace();
            System.exit(2);
        }

        // publish 10 messages to the queue
        int nbmess = 10;
        try {
            TextMessage message;
            for (int i=0;i<nbmess;i++){
                message = session.createTextMessage();
                message.setText("Message"+i);
                qp.send(message);

            }
            session.close();
            qc.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(2);
        }
        System.out.println("MDBsample is Ok");
    }
}
