/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): ____________________________________.
 * Contributor(s): ______________________________________.
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

// MdbBean.java
// Message Driven  bean

package newsamplemdb2;


import javax.jms.Message;
import javax.jms.TextMessage;
import javax.jms.JMSException;



/**
 * Example of MessageDrivenBean on a Queue. The transactions are container managed (Required)
 * this class have superclasses in which are defined ejbCreate() and onMessage()
 * this strange structure is only for reproducing bug 300387
 * for a more normal structure see the samplemdb sample
 */
public class MdbBean  extends MdbBean_b {


    /**
     * Default constructor
     *
     */
    public MdbBean() {
    }



    /**
     *  onMessage1 method
     */
    public void onMessage1(Message message) {
	System.out.println( "MdbBean onMessage1");
	try{
	    TextMessage mess = (TextMessage)message;
	    System.out.println( "Message received: "+mess.getText());
	}catch(JMSException ex){
	    System.err.println("Exception caught: "+ex);
	}
    }
}
