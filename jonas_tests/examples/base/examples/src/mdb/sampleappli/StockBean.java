/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): ____________________________________.
 * Contributor(s): ______________________________________.
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

// StockBean.java
// Entity Bean
package sampleappli;

import java.rmi.RemoteException;

import javax.ejb.CreateException;
import javax.ejb.DuplicateKeyException;
import javax.ejb.EJBException;
import javax.ejb.EntityBean;
import javax.ejb.EntityContext;
import javax.ejb.FinderException;
import javax.ejb.ObjectNotFoundException;
import javax.ejb.RemoveException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.transaction.NotSupportedException;

/**
 *
 */
public class StockBean implements EntityBean {

    EntityContext ejbContext;

    // ------------------------------------------------------------------
    // State of the bean.
    // They must be public for Container Managed Persistance.
    // ------------------------------------------------------------------
    public String stockid;

    public int stockqty;

    // ------------------------------------------------------------------
    // EntityBean implementation
    // ------------------------------------------------------------------

    /**
     * Set the associated entity context. The container invokes this method on
     * an instance after the instance has been created. This method is called in
     * an unspecified transaction context.
     * @param ctx - An EntityContext interface for the instance. The instance
     *        should store the reference to the context in an instance variable.
     * @throws EJBException Thrown by the method to indicate a failure caused by
     *         a system-level error.
     */
    public void setEntityContext(EntityContext ctx) {
        ejbContext = ctx;
    }

    /**
     * Unset the associated entity context. The container calls this method
     * before removing the instance. This is the last method that the container
     * invokes on the instance. The Java garbage collector will eventually
     * invoke the finalize() method on the instance. This method is called in an
     * unspecified transaction context.
     * @throws EJBException Thrown by the method to indicate a failure caused by
     *         a system-level error.
     */
    public void unsetEntityContext() {
        ejbContext = null;
    }

    /**
     * A container invokes this method before it removes the EJB object that is
     * currently associated with the instance. This method is invoked when a
     * client invokes a remove operation on the enterprise Bean's home interface
     * or the EJB object's remote interface. This method transitions the
     * instance from the ready state to the pool of available instances. This
     * method is called in the transaction context of the remove operation.
     * @throws RemoveException The enterprise Bean does not allow destruction of
     *         the object.
     * @throws EJBException - Thrown by the method to indicate a failure caused
     *         by a system-level error.
     */
    public void ejbRemove() throws RemoveException {
    }

    /**
     * A container invokes this method to instruct the instance to synchronize
     * its state by loading it state from the underlying database. This method
     * always executes in the proper transaction context.
     * @throws EJBException Thrown by the method to indicate a failure caused by
     *         a system-level error.
     */
    public void ejbLoad() {
    }

    /**
     * A container invokes this method to instruct the instance to synchronize
     * its state by storing it to the underlying database. This method always
     * executes in the proper transaction context.
     * @throws EJBException Thrown by the method to indicate a failure caused by
     *         a system-level error.
     */
    public void ejbStore() {
    }

    /**
     * There must be an ejbPostCreate par ejbCreate method
     * @throws CreateException Failure to create an entity EJB object.
     */
    public void ejbPostCreate(String id, int qty) throws CreateException {
    }

    /**
     * The Entity bean can define 0 or more ejbCreate methods.
     * @throws CreateException Failure to create an entity EJB object.
     * @throws DuplicateKeyException An object with the same key already exists.
     */
    public java.lang.String ejbCreate(String id, int qty) throws CreateException, DuplicateKeyException {

        // Init here the bean fields
        stockid = id;
        stockqty = qty;

        // In CMP, should return null.
        return null;
    }

    /**
     * A container invokes this method on an instance before the instance
     * becomes disassociated with a specific EJB object.
     */
    public void ejbPassivate() {
    }

    /**
     * A container invokes this method when the instance is taken out of the
     * pool of available instances to become associated with a specific EJB
     * object.
     */
    public void ejbActivate() {
    }

    // ------------------------------------------------------------------
    // Stock implementation
    // ------------------------------------------------------------------

    /**
     * increaseQuantity
     */
    public void increaseQuantity(int qty) {
        stockqty += qty;
    }

    /**
     * decreaseStockQuantity
     */
    public void decreaseQuantity(int qty) throws RemoteException {
        stockqty = stockqty - qty;
        if (stockqty < 0) {
            throw new RemoteException("Negative stock");
        }
    }

    /**
     * getStockQuantity
     */
    public int getQuantity() throws RemoteException {
        return stockqty;
    }

    /**
     * setStockQuantity
     */
    public void setQuantity(int qty) throws RemoteException {
        stockqty = qty;
    }

    /**
     * getStockQuantity
     */
    public String getId() throws RemoteException {
        return stockid;
    }
}