/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): ____________________________________.
 * Contributor(s): ______________________________________.
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

// OrderBean.java
// Message Driven bean
package sampleappli;

import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import javax.naming.Context;
import javax.naming.InitialContext;

import java.io.DataOutputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

/**
 *
 */
public class OrderBean implements MessageDrivenBean, MessageListener {

    private transient MessageDrivenContext mdbContext;

    String filename = null;

    // ------------------------------------------------------------------
    // MessageDrivenBean implementation
    // ------------------------------------------------------------------

    /**
     * Default constructor
     */
    public OrderBean() {
    }

    /**
     * Set the associated context. The container call this method after the
     * instance creation. The enterprise Bean instance should store the
     * reference to the context object in an instance variable. This method is
     * called with no transaction context.
     * @param MessageDrivenContext A MessageDrivenContext interface for the
     *        instance.
     * @throws EJBException Thrown by the method to indicate a failure caused by
     *         a system-level error.
     */

    public void setMessageDrivenContext(MessageDrivenContext ctx) {
        mdbContext = ctx;
        Context initialContext = null;
        if (filename == null) {
            try {
                initialContext = new InitialContext();
                // Check that the SessionContext is the good one.
                filename = (String) initialContext.lookup("java:comp/env/orderfilename");
            } catch (Exception e) {
                System.err.println("cannot lookup environment");
            }
        }
    }

    /**
     * A container invokes this method before it ends the life of the
     * message-driven object. This happens when a container decides to terminate
     * the message-driven object. This method is called with no transaction
     * context.
     * @throws EJBException Thrown by the method to indicate a failure caused by
     *         a system-level error.
     */
    public void ejbRemove() {
    }

    /**
     * The Message driven bean must define an ejbCreate methods with no args.
     */
    public void ejbCreate() {
    }

    /**
     * onMessage method
     */
    public void onMessage(Message message) {
        TextMessage msg = (TextMessage) message;
        try {
            File f = new File(System.getProperty("java.io.tmpdir") + File.separator + System.getProperty("user.name")
                    + "_" + filename);
            DataOutputStream fileout = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(f
                    .getCanonicalPath(), true)));
            fileout.writeBytes(msg.getText() + System.getProperty("line.separator"));
            fileout.close();
        } catch (Exception e) {
            System.err.println("OrderBean onMessage Exception caught: " + e);
        }
    }
}