/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): ____________________________________.
 * Contributor(s): ______________________________________.
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

// MdbBean.java
// Message Driven bean
package samplemdb;

import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.jms.JMSException;

/**
 * Example of MessageDrivenBean on a Topic. The transactions are container
 * managed (Required)
 */
public class MdbBean implements MessageDrivenBean, MessageListener {

    private transient MessageDrivenContext mdbContext;

    // ------------------------------------------------------------------
    // MessageDrivenBean implementation
    // ------------------------------------------------------------------

    /**
     * Default constructor
     */
    public MdbBean() {
    }

    /**
     * Set the associated context. The container call this method after the
     * instance creation. The enterprise Bean instance should store the
     * reference to the context object in an instance variable. This method is
     * called with no transaction context.
     * @param MessageDrivenContext A MessageDrivenContext interface for the
     *        instance.
     * @throws EJBException Thrown by the method to indicate a failure caused by
     *         a system-level error.
     */

    public void setMessageDrivenContext(MessageDrivenContext ctx) {
        System.out.println("MdbBean setMessageDrivenContext");
        mdbContext = ctx;
    }

    /**
     * A container invokes this method before it ends the life of the
     * message-driven object. This happens when a container decides to terminate
     * the message-driven object. This method is called with no transaction
     * context.
     * @throws EJBException Thrown by the method to indicate a failure caused by
     *         a system-level error.
     */
    public void ejbRemove() {
        System.out.println("MdbBean ejbRemove");
    }

    /**
     * The Message driven bean must define an ejbCreate methods with no args.
     */
    public void ejbCreate() {
        System.out.println("MdbBean ejbCreate");
    }

    /**
     * onMessage method
     */
    public void onMessage(Message message) {
        System.out.println("MdbBean onMessage");
        try {
            TextMessage mess = (TextMessage) message;
            System.out.println("Message received: " + mess.getText());
        } catch (JMSException ex) {
            System.err.println("Exception caught: " + ex);
        }
    }
}