/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): ____________________________________.
 * Contributor(s): ______________________________________.
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

// MdbClient.java
// mini Client for accessing bean Mdb
package samplemdb;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.*;
import javax.jms.*;

/**
 *
 */
public class MdbClient {

    static Context ictx = null;

    static TopicConnectionFactory tcf = null;

    static TopicPublisher tp = null;

    static Topic topic = null;

    // JNDI name of the Topic
    static String topicName = "mdbTopic";

    // JNDI name of the default connection factory
    static String conFactName = "JTCF";

    public static void main(String[] arg) {
        // Get InitialContext
        try {
            ictx = new InitialContext();
            // lookup the TopicConnectionFactory through its JNDI name
            tcf = (TopicConnectionFactory) ictx.lookup(conFactName);
            System.out.println("JMS client: tcf = " + tcf.toString());
            // lookup the Topic through its JNDI name
            topic = (Topic) ictx.lookup(topicName);
        } catch (NamingException e) {
            e.printStackTrace();
            System.exit(2);
        }

        TopicConnection tc = null;
        TopicSession session = null;
        try {
            tc = tcf.createTopicConnection();
            System.out.println("JMS client: tc = " + tc.toString());
            session = tc.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
            tp = session.createPublisher(topic);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(2);
        }

        // publish 10 messages to the topic
        int nbmess = 10;
        try {
            TextMessage message;
            for (int i = 0; i < nbmess; i++) {
                message = session.createTextMessage();
                message.setText("Message" + i);
                tp.publish(message);

            }
            session.close();
            tc.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(2);
        }
        System.out.println("MDBsample is Ok");
    }
}