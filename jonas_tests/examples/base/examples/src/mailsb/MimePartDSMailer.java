/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Florent BENOIT & Ludovic BERT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package mailsb;

import java.rmi.RemoteException;
import javax.ejb.EJBObject;

/**
 * Remote interface for the bean Mailer Work with
 * javax.mail.internet.MimePartDataSource
 * @author Florent Benoit
 * @author Ludovic Bert
 */
public interface MimePartDSMailer extends EJBObject {

    /**
     * Set the message with a specific recipient, subject and content.
     * @param content the content of the message.
     * @throws Exception if a problem occurs.
     * @throws RemoteException if the call failed.
     */
    void setMessage(String content) throws Exception, RemoteException;

    /**
     * Send the message by creating it with the mail factory properties.
     * @throws Exception if a problem occurs.
     * @throws RemoteException if the call failed.
     */
    void send() throws Exception, RemoteException;

}