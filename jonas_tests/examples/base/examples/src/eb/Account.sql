drop table accountsample_;
set autocommit true;
create table accountsample_ (
	accno_ 		integer primary key,
	customer_ 	varchar(30),
	balance_ 	number(15, 4)
);
insert into accountsample_ values(101, 'Antoine de St Exupery', 200.00);
insert into accountsample_ values(102, 'alexandre dumas fils', 400.00);
insert into accountsample_ values(103, 'conan doyle', 500.00);
insert into accountsample_ values(104, 'alfred de musset', 100.00);
insert into accountsample_ values(105, 'phileas lebegue', 350.00);
insert into accountsample_ values(106, 'alphonse de lamartine', 650.00);
