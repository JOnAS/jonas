/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2005 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package eb;

import java.util.Enumeration;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.rmi.PortableRemoteObject;
import javax.transaction.UserTransaction;

/**
 * Sample for entity beans. Usage: implicit persistance: java eb.ClientAccount
 * AccountImplHome explicit persistance: java eb.ClientAccount AccountExplHome
 * @author JOnAS team
 */
public class ClientAccount {

    /**
     * UserTransaction object
     */
    private static UserTransaction utx = null;

    private static void accountList(AccountHome h) {
        Enumeration alist;
        Account acc;
        try {
            utx.begin(); // faster if made inside a Tx
            alist = h.findAllAccounts();
            while (alist.hasMoreElements()) {
                acc = (Account) PortableRemoteObject.narrow(alist.nextElement(), Account.class);
                System.out.println(acc.getNumber() + " " + acc.getCustomer() + " " + acc.getBalance());
            }
            utx.commit();
        } catch (Exception e) {
            System.err.println("Exception getting account list: " + e);
            System.exit(2);
        }
    }

    public static void main(String[] args) {

        // 1st arg. is the bean home (AccountImplHome or AccountExplHome)
        String beanName = args[0];

        // get JNDI initial context
        Context initialContext = null;
        try {
            initialContext = new InitialContext();
        } catch (Exception e) {
            System.err.println("Cannot get initial context for JNDI: " + e);
            System.exit(2);
        }

        // We want to start transactions from client: get UserTransaction
        System.out.println("Getting a UserTransaction object from JNDI");
        try {

            // Comment the following lines if you want to use a David Client:
            utx = (UserTransaction) initialContext.lookup("javax.transaction.UserTransaction");

        } catch (Exception e) {
            System.err.println("Cannot lookup UserTransaction: " + e);
            System.exit(2);
        }

        // Connecting to Home thru JNDI
        System.out.println("Connecting to the AccountHome");
        AccountHome home = null;
        try {
            home = (AccountHome) PortableRemoteObject.narrow(initialContext.lookup(beanName), AccountHome.class);
        } catch (Exception e) {
            System.err.println("Cannot lookup " + beanName + ": " + e);
            System.exit(2);
        }

        // List existing Accounts
        System.out.println("Getting the list of existing accounts in database");
        accountList(home);

        // Create a first Account
        System.out.println("Creating a new Account in database");
        Account a1 = null;
        try {
            a1 = home.create(109, "John Smith", 0);
        } catch (Exception e) {
            System.err.println("Cannot create Account: " + e);
            System.exit(2);
        }

        // Find a second Account
        System.out.println("Finding an Account by its number in database");
        Account a2 = null;
        try {
            a2 = home.findByNumber(102);
        } catch (Exception e) {
            System.err.println("Cannot find Account: " + e);
            System.exit(2);
        }

        // First transaction (committed):
        // transfert 100 from a2 to a1
        System.out.println("Starting a first transaction, that will be committed");
        try {
            double value = 100.00;
            utx.begin();
            a1.setBalance(value);
            a2.setBalance(-value);
            utx.commit();
        } catch (Exception e) {
            System.err.println("exception during 1st Tx: " + e);
            System.exit(2);
        }

        // Start another transaction (rolled back):
        // transfert 20 from a1 to a2
        System.out.println("Starting a second transaction, that will be rolled back");
        try {
            double value = 20.00;
            utx.begin();
            a1.setBalance(-value);
            a2.setBalance(value);
            utx.rollback();
        } catch (Exception e) {
            System.err.println("exception during 2nd Tx: " + e);
            System.exit(2);
        }

        // List existing Accounts
        System.out.println("Getting the new list of accounts in database");
        accountList(home);

        // Delete account
        System.out.println("Removing Account previously created in database");
        try {
            a1.remove();
        } catch (Exception e) {
            System.err.println("exception during remove: " + e);
            System.exit(2);
        }

        // Exit program
        System.out.println("ClientAccount terminated");
    }

}

