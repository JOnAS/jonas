/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package eb;

/**
 * Accountbean is an entity bean with "container managed persistence version 2".
 * The state of an instance is stored into a relational database. The following
 * table should exist: create table ACCOUNT (ACCNO integer primary key, CUSTOMER
 * varchar(30), BALANCE number(15,4));
 * @author Christophe Ney cney@batisseurs.com
 */


import javax.ejb.CreateException;
import javax.ejb.EntityBean;
import javax.ejb.EntityContext;
import javax.ejb.RemoveException;

public abstract class AccountImpl2Bean implements EntityBean {

    // Keep the reference on the EntityContext
    protected EntityContext entityContext;

    /* ========================= ejbCreate methods ============================ */

    /**
     * There must be one ejbCreate() method per create() method on the Home
     * interface, and with the same signature.
     * @param accno account number
     * @param customer customer name
     * @param balance initial balance
     * @return pk primary key set to null
     */
    public Integer ejbCreate(int val_accno, String val_customer, double val_balance) throws CreateException {

        // Init object state
        setAccno(val_accno);
        setCustomer(val_customer);
        setBalance(val_balance);
        return null;
    }

    /**
     * Each ejbCreate method should have a matching ejbPostCreate method
     */
    public void ejbPostCreate(int val_accno, String val_customer, double val_balance) {
        // Nothing to be done for this simple example.
    }

    /*
     * ====================== javax.ejb.EntityBean implementation
     * =================
     */

    /**
     * A container invokes this method when the instance is taken out of the
     * pool of available instances to become associated with a specific EJB
     * object. This method transitions the instance to the ready state. This
     * method executes in an unspecified transaction context.
     * @exception EJBException Thrown by the method to indicate a failure caused
     *            by a system-level error.
     */
    public void ejbActivate() {
        // Nothing to be done for this simple example.
    }

    /**
     * A container invokes this method to instruct the instance to synchronize
     * its state by loading it state from the underlying database. This method
     * always executes in the proper transaction context.
     * @exception EJBException Thrown by the method to indicate a failure caused
     *            by a system-level error.
     * @exception RemoteException - This exception is defined in the method
     *            signature to provide backward compatibility for enterprise
     *            beans written for the EJB 1.0 specification. Enterprise beans
     *            written for the EJB 1.1 and higher specification should throw
     *            the javax.ejb.EJBException instead of this exception.
     */
    public void ejbLoad() {
        // Nothing to be done for this simple example, in implicit persistance.
    }

    /**
     * A container invokes this method on an instance before the instance
     * becomes disassociated with a specific EJB object. After this method
     * completes, the container will place the instance into the pool of
     * available instances. This method executes in an unspecified transaction
     * context.
     * @exception EJBException Thrown by the method to indicate a failure caused
     *            by a system-level error.
     */
    public void ejbPassivate() {
        // Nothing to be done for this simple example.
    }

    /**
     * A container invokes this method before it removes the EJB object that is
     * currently associated with the instance. This method is invoked when a
     * client invokes a remove operation on the enterprise Bean's home interface
     * or the EJB object's remote interface. This method transitions the
     * instance from the ready state to the pool of available instances. This
     * method is called in the transaction context of the remove operation.
     * @exception EJBException Thrown by the method to indicate a failure caused
     *            by a system-level error.
     * @exception RemoteException - This exception is defined in the method
     *            signature to provide backward compatibility for enterprise
     *            beans written for the EJB 1.0 specification. Enterprise beans
     *            written for the EJB 1.1 and higher specification should throw
     *            the javax.ejb.EJBException instead of this exception.
     * @exception RemoveException The enterprise Bean does not allow destruction
     *            of the object.
     */
    public void ejbRemove() throws RemoveException {
        // Nothing to be done for this simple example, in implicit persistance.
    }

    /**
     * A container invokes this method to instruct the instance to synchronize
     * its state by storing it to the underlying database. This method always
     * executes in the proper transaction context.
     * @exception EJBException Thrown by the method to indicate a failure caused
     *            by a system-level error.
     * @exception RemoteException - This exception is defined in the method
     *            signature to provide backward compatibility for enterprise
     *            beans written for the EJB 1.0 specification. Enterprise beans
     *            written for the EJB 1.1 and higher specification should throw
     *            the javax.ejb.EJBException instead of this exception.
     */
    public void ejbStore() {
        // Nothing to be done for this simple example, in implicit persistance.
    }

    /**
     * Sets the associated entity context. The container invokes this method on
     * an instance after the instance has been created. This method is called in
     * an unspecified transaction context.
     * @param ctx - An EntityContext interface for the instance. The instance
     *        should store the reference to the context in an instance variable.
     * @exception EJBException Thrown by the method to indicate a failure caused
     *            by a system-level error.
     * @exception RemoteException - This exception is defined in the method
     *            signature to provide backward compatibility for enterprise
     *            beans written for the EJB 1.0 specification. Enterprise beans
     *            written for the EJB 1.1 and higher specification should throw
     *            the javax.ejb.EJBException instead of this exception.
     */
    public void setEntityContext(EntityContext ctx) {

        // Keep the entity context in object
        entityContext = ctx;
    }

    /**
     * Unsets the associated entity context. The container calls this method
     * before removing the instance. This is the last method that the container
     * invokes on the instance. The Java garbage collector will eventually
     * invoke the finalize() method on the instance. This method is called in an
     * unspecified transaction context.
     * @exception EJBException Thrown by the method to indicate a failure caused
     *            by a system-level error.
     * @exception RemoteException - This exception is defined in the method
     *            signature to provide backward compatibility for enterprise
     *            beans written for the EJB 1.0 specification. Enterprise beans
     *            written for the EJB 1.1 and higher specification should throw
     *            the javax.ejb.EJBException instead of this exception.
     */
    public void unsetEntityContext() {
        entityContext = null;
    }

    /**
     * Business method to get the Account number
     */
    public int getNumber() {
        return getAccno();
    }

    /*
     * ========================= Account implementation
     * ============================
     */

    public abstract String getCustomer();

    public abstract void setCustomer(String customer);

    public abstract double getBalance();

    public abstract void setBalance(double balance);

    public abstract int getAccno();

    public abstract void setAccno(int accno);

}