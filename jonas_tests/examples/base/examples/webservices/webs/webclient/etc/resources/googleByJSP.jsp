<%@page contentType="text/html"%>

<%@page import="org.objectweb.wssample.gen.google.*"%>
<%@page import="javax.naming.*"%>
<html>
<head>
  <title>Using Google WebServices</title>
  <style type="text/css"> @import "style/ow_jonas.css"; </style>
</head>
<body class="bodywelcome">
   <div class="logos">
      <a href="http://jakarta.apache.org">
        <img title="Jakarta Tomcat" alt="Jakarta Tomcat" 
          src="images/tomcat.gif" />
      </a>
     <a href="http://jetty.mortbay.org/jetty/"><img title="Mortbay Jetty" alt="Mortbay Jetty" src="images/jetty.gif" /></a>
     <a href="http://jonas.objectweb.org"><img title="JOnAS WebSite" alt="JOnAS WebSite" src="images/ow_jonas_logo.gif" /></a>
   </div>
    

<%
    if (request.getParameter("search") == null) {
%>
    <div class="titlepage"> Google WebServices Access via JSP </div>

<div class="links">
Enter your search request as usual in Google : <br/>

<form method="POST" action="./googleByJSP.jsp">

  <input type="text" name="search"/>
  <input type="submit" name="execute" value="Search"/>

</form>
</div>

<%
  } else {
%>
    <div class="titlepage"> Results </div>
    <div class="links">
<%
    // Get and Display results
    //-------------------------------------------

	// Get the InitialContext where retrieve Service
    Context ctx = new InitialContext();

    String key = (String) ctx.lookup("java:comp/env/key");

	// Lookup the service
    GoogleSearchService google = (GoogleSearchService) ctx.lookup("java:comp/env/service/google");

	// get Google Port
    GoogleSearchPort search = google.getGoogleSearchPort();

	// Execute the query
    GoogleSearchResult result = search.doGoogleSearch(  key, 
                            request.getParameter("search"),
                            0,     // first index
                            10,    // maxresult
                            false, //filter
                            null, //restrict
                            false, // safesearch
                            null, //lr
                            null, //ie
                            null  //oe
                            );
	// Get the results
    ResultElement[] elements = result.getResultElements();

	// Display each result
    for (int i = 0; i < elements.length; i++) {
%>
    <br/>
    <i><a href="<%= elements[i].getURL()%>"><%= elements[i].getTitle()%></a></i><br/>
    <%= elements[i].getSummary()%><br/>
<%
    }
%>
<br/>
<form method="POST" action="./googleByJSP.jsp">
  <input type="submit" name="RETURN" value="Back"/>
</form>
</div>
<%
 }
%>
</body>
</html>
