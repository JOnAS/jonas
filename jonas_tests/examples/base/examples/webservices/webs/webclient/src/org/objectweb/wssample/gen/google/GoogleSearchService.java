/**
 * GoogleSearchService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2RC2 Dec 15, 2004 (10:53:17 CET) WSDL2Java emitter.
 */

package org.objectweb.wssample.gen.google;

public interface GoogleSearchService extends javax.xml.rpc.Service {
    public java.lang.String getGoogleSearchPortAddress();

    public org.objectweb.wssample.gen.google.GoogleSearchPort getGoogleSearchPort() throws javax.xml.rpc.ServiceException;

    public org.objectweb.wssample.gen.google.GoogleSearchPort getGoogleSearchPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
