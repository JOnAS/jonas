/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.objectweb.wssample.servlets.ws;

import org.ow2.jonas.lib.util.Log;

import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * Service implementation of JaxRpcEndpointInterface. This class is a JaxRpc
 * endpoint. It is declared in the web.xml. And it will run inside the web
 * container.
 *
 * Notice that a default class constructor is REQUIRED.
 * Notice that JaxRpcEndpoint does not implements JaxRpcEndpointInterface
 * This is not a requirement.
 *
 * This JAXRPC service endpoint class may implements <code>javax.xml.rpc.server.ServiceLifecycle</code>
 * interface.
 *
 * @see org.objectweb.wssample.servlets.ws.JaxRpcEndpointInterface
 *
 * @author Guillaume Sauthier
 */
public class JaxRpcEndpoint {

    /**
     * return value of getCotes()
     */
    private static final int GET_COTES_VALUE = 12;

    /**
     * logger
     */
    private static Logger logger = Log.getLogger("org.ow2.jonas.ws");

    /**
     * default constructor needed for JaxRpc Service Endpoint.
     */
    public JaxRpcEndpoint() {
    }

    /**
     * @param name String to append to "Hello "
     * @return Returns "Hello " + name
     */
    public String sayHello(String name) {
        logger.log(BasicLevel.INFO, "sayHello(" + name + ") invoked.");
        return "Hello " + name;
    }

    /**
     * @return Returns an arbitrary integer
     */
    public int getCotes() {
        logger.log(BasicLevel.INFO, "getCotes() invoked.");
        return GET_COTES_VALUE;
    }

}