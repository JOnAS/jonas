/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.objectweb.wssample.security;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.ws.security.WSPasswordCallback;

/**
 * WSS4J Sample CallbackHandler. It is intended to be used from a JOnAS server (servlet/ejb client & JAX-RPC/SSB endpoint).
 * It will get the 'memrlm_1' instance and try to get the needed password from that JResource.
 *
 * @author Guillaume Sauthier
 */
public class ServerSideCallbackHandler implements CallbackHandler {

    /**
     * encryptionUser value (used to access keystore - look in crypto.properties)
     */
    private static final String ENCRYPTION_USER = "jonas-ws";

    /**
     * encryptionUser password : password used to access keystore
     */
    private static final String ENCRYPTION_PASSWORD = "security";

    /**
     * File name which contains credentials
     */
    private static final String CREDENTIALS_FILE_NAME = "credentials.properties";

    /**
     * @see javax.security.auth.callback.CallbackHandler#handle(javax.security.auth.callback.Callback[])
     */
    public void handle(Callback[] callbacks) throws IOException,
            UnsupportedCallbackException {
        for (int i = 0; i < callbacks.length; i++) {
            if (callbacks[i] instanceof WSPasswordCallback) {
                WSPasswordCallback pc = (WSPasswordCallback) callbacks[i];
                // set the password given a username

                if (!ENCRYPTION_USER.equals(pc.getIdentifer())) {
                    // Get hash password for this identifier
                    InputStream is = getClass().getResourceAsStream("/" + CREDENTIALS_FILE_NAME);
                    Properties props  = new Properties();
                    props.load(is);
                    String password = props.getProperty(pc.getIdentifer());
                    // Set the password
                    pc.setPassword(password);
                } else {
                    // this is the ecryption username
                    // we must return the correct password (stored in crypto.properties)
                    pc.setPassword(ENCRYPTION_PASSWORD);
                }

            } else {
                throw new UnsupportedCallbackException(callbacks[i], "Unrecognized Callback");
            }
        }
    }

}
