/**
 * JaxRpcEndpointInterfaceService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2RC2 Dec 15, 2004 (10:53:17 CET) WSDL2Java emitter.
 */

package org.objectweb.wssample.gen.jaxrpc;

public interface JaxRpcEndpointInterfaceService extends javax.xml.rpc.Service {
    public java.lang.String getJaxRpcEndpoint1Address();

    public org.objectweb.wssample.gen.jaxrpc.JaxRpcEndpointInterface getJaxRpcEndpoint1() throws javax.xml.rpc.ServiceException;

    public org.objectweb.wssample.gen.jaxrpc.JaxRpcEndpointInterface getJaxRpcEndpoint1(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;

    public org.objectweb.wssample.gen.jaxrpc.JaxRpcEndpointInterface getEncryptedJaxRpcEndpoint() throws javax.xml.rpc.ServiceException;

    public org.objectweb.wssample.gen.jaxrpc.JaxRpcEndpointInterface getEncryptedJaxRpcEndpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
