/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.objectweb.wssample.servlets.ws;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * The Service interface.
 * Notice that it extends Remote and all methods are throwing RemoteException
 * @see org.objectweb.wssample.servlets.ws.JaxRpcEndpoint
 * @author Guillaume Sauthier
 */
public interface JaxRpcEndpointInterface extends Remote {

    /**
     * @param name String to append to "Hello "
     * @return Returns "Hello " + name
     * @throws RemoteException required because of the Remote interface
     */
    String sayHello(String name) throws RemoteException;

    /**
     * @return Returns an arbitrary integer
     * @throws RemoteException required because of the Remote interface
     */
    int getCotes() throws RemoteException;
}
