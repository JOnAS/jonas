/**
 * JaxRpcEndpointInterface.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2RC2 Dec 15, 2004 (10:53:17 CET) WSDL2Java emitter.
 */

package org.objectweb.wssample.gen.jaxrpc;

public interface JaxRpcEndpointInterface extends java.rmi.Remote {
    public java.lang.String sayHello(java.lang.String in0) throws java.rmi.RemoteException;
    public int getCotes() throws java.rmi.RemoteException;
}
