webendpoint sample README
----------------------------

This sample show you how to use secured endpoint :
* how to configure the endpoint for Authentication
* how to configure your endpoint for Encryption

You can directly use the authentication endpoint, but
some preliminary steps are required for the Encryption,
as it use a keystore and because Encryption needs BouncyCastle
JCE provider that is not packaged inside JOnAS (please
read the following).

IMPORTANT :
-----------
If you want to use advanced capabilities of WSS4J, in particular Encryption, ...
You have to download BouncyCastle JCE Provider.
You can download latest version here :
http://www.bouncycastle.org/latest_releases.html
Choose the bcprov-jdk**-XXX.jar according to the JDK version you're running JOnAS on.
Place that downloaded jar in JONAS_ROOT/lib/commons/jonas/ws-security.
BouncyCastle provider will be available for the next JOnAS reboot.

Please take the time to read the following web page :
http://www.bouncycastle.org/specifications.html
Especially the patent statement.

How to create a keystore ?
---------------------------

Here are the steps :
1. create the keystore with an single certificate :
$> $JAVA_HOME/bin/keytool -genkey -alias jonas-ws -keyalg RSA -keystore $JONAS_BASE/conf/my-wssample-keystore

2. You will be prompted for the keystore password, type "security" (without the quote)
3. Enter the certificate information (Name, Company, ...)
4. Reuse the SAME password ("security") for the certificate (just press enter)

This will create a keystore in JONAS_BASE/conf with 1 certificate for the user 'jonas-ws'.
This keystore is used by WSS4Jhandler.
