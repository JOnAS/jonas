<%@page contentType="text/html"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Prepare Local JaxRpc WebService Call</title>
    <style type="text/css"> @import "style/ow_jonas.css"; </style>
  </head>

  <body class="bodywelcome">
   <div class="logos">
      <a href="http://jakarta.apache.org">
        <img title="Jakarta Tomcat" alt="Jakarta Tomcat"
          src="images/tomcat.gif" />
      </a>
     <a href="http://jetty.mortbay.org/jetty/"><img title="Mortbay Jetty" alt="Mortbay Jetty" src="images/jetty.gif" /></a>
     <a href="http://jonas.objectweb.org"><img title="JOnAS WebSite" alt="JOnAS WebSite" src="images/ow_jonas_logo.gif" /></a>
   </div>

    <div class="titlepage"> Prepare Local JaxRpc WebService Call </div>
    <div class="links">

  This page enable you to test the WebServices you just deployed.<br/>
  Some informations are needed to view the WebServices call results : <br/>

  Web Service with authentication :<br/>
  <form name="prepare" method="POST" action="jaxrpcTest.do">
    Param to use in sayHello method : <input name="name" type="text" value="enter your name here"/><br/>
    <input name="exec" type="submit" value="Test WebService"/>
    <input name="encrypt" type="hidden" value="false"/>
  </form>

  Web Service with encryption :<br/>
  <% try {
    // if that BC file is here, the provider is available
    Class.forName("org.bouncycastle.LICENSE");
  } catch (ClassNotFoundException cnfe) {
    out.println("<font color=\"red\">bcprov-jdkXX-YYY.jar not found in JOnAS ClassLoader.<br/>");
    out.println("Please see the README file in the sample directory to know where BC can be downloaded</font>");
  }
  %>
  <form name="prepare" method="POST" action="jaxrpcTest.do">
    Param to use in sayHello method : <input name="name" type="text" value="enter your name here"/><br/>
    <input name="exec" type="submit" value="Test Encrypted WebService"/>
    <input name="encrypt" type="hidden" value="true"/>
  </form>
  </div>

  </body>
</html>
