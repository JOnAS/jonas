/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.objectweb.wssample.beans.ws;

import javax.ejb.CreateException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

import org.ow2.jonas.lib.util.Log;
import org.objectweb.util.monolog.api.Logger;
import org.objectweb.util.monolog.api.BasicLevel;

/**
 *
 */
public class WSBeanEndpointSLL implements SessionBean {

    /**
     * logger
     */
    private static Logger logger = null;

    /**
     * EJB SessionContext
     */
    private SessionContext ejbContext;

    // ------------------------------------------------------------------
    // SessionBean implementation
    // ------------------------------------------------------------------

    /**
     * Set the SessionContext
     * @param ctx SessionContext
     */
    public void setSessionContext(SessionContext ctx) {
        if (logger == null) {
            logger = Log.getLogger("org.objectweb.jonas_tests");
        }
        logger.log(BasicLevel.DEBUG, "");
        ejbContext = ctx;
    }

    /**
     * ejbRemove
     */
    public void ejbRemove() {
        logger.log(BasicLevel.DEBUG, "");
    }

    /**
     * ejbRemove
     * @throws CreateException CreateException
     */
    public void ejbCreate() throws CreateException {
        logger.log(BasicLevel.DEBUG, "");
    }

    /**
     * ejbPassivate
     */
    public void ejbPassivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    /**
     * ejbActivate
     */
    public void ejbActivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    // ------------------------------------------------------------------
    // WSBeanEndpoint implementation
    // ------------------------------------------------------------------

    /**
     * @param name name
     * @return Returns "Hello " + name
     */
    public String sayHello(String name) {
        logger.log(BasicLevel.INFO, "sayHello(" + name + ") invokation.");
        return "Hello " + name;
    }

    /**
     * @return Returns integer
     */
    public int getCotes() {
        logger.log(BasicLevel.INFO, "getCotes invokation");
        return 12;
    }
}

