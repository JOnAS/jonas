/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Guillaume Sauthier______________.
 * Contributor(s): ______________________________________.
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.objectweb.wssample.beans.wsclient;

import java.rmi.RemoteException;

import javax.ejb.CreateException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.ow2.jonas.lib.util.Log;
import org.objectweb.util.monolog.api.Logger;
import org.objectweb.util.monolog.api.BasicLevel;

import org.objectweb.wssample.genbean.google.GoogleSearchResult;
import org.objectweb.wssample.genbean.google.GoogleSearchService;
import org.objectweb.wssample.genbean.google.GoogleSearchPort;

import javax.xml.rpc.ServiceException;

/**
 * GoogleClientBean implementation. This bean use Google WebServices API
 * to execute a given Query.
 *
 * @author Guillaume Sauthier
 */
public class GoogleClientBeanSLR implements SessionBean {

    /**
     * logger
     */
    private static Logger logger = null;

    /**
     * Session Context
     */
    private SessionContext ejbContext;

    // ------------------------------------------------------------------
    // SessionBean implementation
    // ------------------------------------------------------------------

    /**
     * Set the SessionContext
     * @param ctx the SessionContext
     */
    public void setSessionContext(SessionContext ctx) {
        if (logger == null) {
            logger = Log.getLogger("org.objectweb.jonas_tests");
        }
        logger.log(BasicLevel.DEBUG, "");
        ejbContext = ctx;
    }

    /**
     * ejbRemove
     */
    public void ejbRemove() {
        logger.log(BasicLevel.DEBUG, "");
    }

    /**
     * ejbCreate
     * @throws CreateException CreateException
     */
    public void ejbCreate() throws CreateException {
        logger.log(BasicLevel.DEBUG, "");
    }

    /**
     * ejbPassivate
     */
    public void ejbPassivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    /**
     * ejbActivate
     */
    public void ejbActivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    // ------------------------------------------------------------------
    // GoogleClientBean implementation
    // ------------------------------------------------------------------

    /**
     * Execute a Query via Google WS api.
     *
     * @param query Query String
     * @return query results
     */
    public GoogleSearchResult executeQuery(java.lang.String query) {

        logger.log(BasicLevel.INFO, "Executing Google Query '" + query + "'");

        try {

            // Get the InitialContext where retrieve Service
            Context ctx = new InitialContext();

            // Lookup the service
            GoogleSearchService google =
                (GoogleSearchService) ctx.lookup("java:comp/env/service/google");

            // get Google Port
            GoogleSearchPort search = google.getGoogleSearchPort();

            // get Google Key
            String key = (String) ctx.lookup("java:comp/env/googleKey");

            // Execute the query
            return search.doGoogleSearch(key,
                                         query,
                                         0,     // first index
                                         10,  // maxresult
                                         false, //filter
                                         null, //restrict
                                         false, // safesearch
                                         null, //lr
                                         null, //ie
                                         null  //oe
                                         );
        } catch (NamingException ne) {
            return null;
        } catch (RemoteException re) {
            return null;
        } catch (ServiceException se) {
            return null;
        }
    }
}

