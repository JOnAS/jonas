/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Guillaume Sauthier______________.
 * Contributor(s): ______________________________________.
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.objectweb.wssample.beans.wsclient;

import java.rmi.RemoteException;

import javax.ejb.EJBObject;

import org.objectweb.wssample.genbean.google.GoogleSearchResult;

/**
 * GoogleClientBean local interface
 */
public interface GoogleClientBean extends EJBObject {
    /**
     * Execute a Query via Google WS api.
     *
     * @param query Query String
     * @return Result of the Search
     *
     * @throws RemoteException When query went wrong
     */
    GoogleSearchResult executeQuery(java.lang.String query) throws RemoteException;
}
