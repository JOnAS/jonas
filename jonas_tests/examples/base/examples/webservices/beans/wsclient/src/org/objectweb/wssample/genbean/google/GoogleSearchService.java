/**
 * GoogleSearchService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2RC1 Sep 29, 2004 (08:29:40 EDT) WSDL2Java emitter.
 */

package org.objectweb.wssample.genbean.google;

public interface GoogleSearchService extends javax.xml.rpc.Service {
    public java.lang.String getGoogleSearchPortAddress();

    public org.objectweb.wssample.genbean.google.GoogleSearchPort getGoogleSearchPort() throws javax.xml.rpc.ServiceException;

    public org.objectweb.wssample.genbean.google.GoogleSearchPort getGoogleSearchPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
