<%@page contentType="text/html"%>

<%@page import="org.objectweb.wssample.beans.wsclient.*"%>
<%@page import="org.objectweb.wssample.genbean.google.*"%>

<%@page import="javax.naming.*"%>
<html>
<head>
  <title>Using Google WebServices</title>
    <style type="text/css">
      @import "style/ow_jonas.css";
    </style>
    <link type="image/x-icon" href="./images/jonas.ico" rel="icon">
    <link type="image/x-icon" href="./images/jonas.ico" rel="shortcut icon">
 </head>
<body class="bodywelcome">
          <div class="logos">
               <a href="http://jakarta.apache.org"><img title="Jakarta Tomcat" alt="Jakarta Tomcat" src="images/tomcat.gif" /></a>
               <a href="http://jetty.mortbay.org/jetty/"><img title="Mortbay Jetty" alt="Mortbay Jetty" src="images/jetty.gif" /></a>
               <a href="http://jonas.objectweb.org"><img title="JOnAS WebSite" alt="JOnAS WebSite" src="images/ow_jonas_logo.gif" /></a>
           </div>
<%
    if (request.getParameter("search") == null) {
%>
    <div class="titlepage">
    Google Search
    </div>

<div class="links">
Enter your search request as usual in Google : <br/>

<form method="POST" action="./search-google.jsp">

  <input type="text" name="search"/>
  <input type="submit" name="execute"/>

</form>
</div>
<%
  } else {
%>
    <div class="titlepage">
    Results
    </div>
<div class="links">
<%
    // Get and Display results
    //-------------------------------------------

	// Get the InitialContext where retrieve Service
    Context ctx = new InitialContext();


	// Lookup the SB
    GoogleClientBeanHome ghome = (GoogleClientBeanHome) ctx.lookup("java:comp/env/ejb/GoogleClient");

	// get Bean
    GoogleClientBean bean = ghome.create();

	// Execute the query
    GoogleSearchResult result = bean.executeQuery(request.getParameter("search"));

	// Get the results
    ResultElement[] elements = result.getResultElements();

	// Display each results
    for (int i = 0; i < elements.length; i++) {
%>
    <br/>
    <i><a href="<%= elements[i].getURL()%>"><%= elements[i].getTitle()%></a></i><br/>
    <%= elements[i].getSummary()%><br/>
<%
    }
%>
<br/>
<form method="POST" action="./search-google.jsp">
  <input type="submit" name="RETURN" value="Back"/>
</form>
</div>
<%
 }
%>
</body>
</html>
