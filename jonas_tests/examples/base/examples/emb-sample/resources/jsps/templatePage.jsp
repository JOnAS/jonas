<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<%@ page isErrorPage="true"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>JOnAS Enterprise Media Beans (JSR 86) - Sample</title>
<link rel="stylesheet" href="theme/style.css" type="text/css" />
</head>
<body>
<div id="main-div">


<h1>JOnAS Enterprise Media Beans (JSR 86) - Sample</h1>


<div id="menu">
<%@ include file="navigation.jsp"%>

<%
Object addMenuBox = request.getAttribute("newMenuBox");
if (addMenuBox != null) {
    out.println(addMenuBox.toString());
}
%>
<p>
  <a href="http://validator.w3.org/check?uri=referer">
  <img src="icons/valid-xhtml11.png" alt="Valid XHTML 1.1" height="31" width="88" />
  </a>
  <a href="http://jigsaw.w3.org/css-validator">
  <img src="icons/vcss.png" alt="Valid CSS" height="31" width="88" />
  </a>
</p>
</div>


<div id="workspace">
<%
if (exception != null) {
    out.println("<h2>There was an Error</h2>");
    out.println("<pre class=\"error\">");
    java.io.ByteArrayOutputStream ostr = new java.io.ByteArrayOutputStream();
    exception.printStackTrace(new java.io.PrintStream(ostr));
    out.print(ostr);
    out.println("</pre>");
}

Object workspaceContent = request.getAttribute("workspaceContent");
if (workspaceContent != null) {
    out.println(workspaceContent.toString());
}

Object errorMessage = request.getAttribute("errorMessage");
if (errorMessage != null) {
    out.println("<h2>There was an Error</h2>");
    out.println("<pre class=\"error\">");
    out.println(errorMessage.toString());
    out.println("</pre>");
}
%>
</div>

</div>
</body>
</html>

