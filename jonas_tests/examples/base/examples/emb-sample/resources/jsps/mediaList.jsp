<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<%@ page import="emb.sample.servlet.BaseSampleServlet"%>
<%@ page errorPage="templatePage.jsp"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>JOnAS Enterprise Media Beans (JSR 86) - Sample</title>
<link rel="stylesheet" href="theme/style.css" type="text/css" />
</head>
<body>
<div id="main-div">
<form action="ActionDispatcher" method="get">
<h1>JOnAS Enterprise Media Beans (JSR 86) - Sample</h1>


<div id="menu">
<%@ include file="navigation.jsp"%>

<div class="menu-box">

<h3>Media Processing Option</h3>
<input type="radio" name="action" value="retrieve" checked="checked" />retrive Media<br />
<input type="radio" name="action" value="extractHeader" />extract Header<br />

<h4>Image</h4>
<input type="radio" name="action" value="convertOverlayed" />add Logo(not GIF)<br />
<input type="radio" name="action" value="convertFrame" />add Frame (notGIF)<br />
<input type="radio" name="action" value="convertBW" />to B&amp;W (notGIF)<br />
<input type="radio" name="action" value="convertHalfSize" />tohalf-size (not GIF)<br />
<input type="radio" name="action" value="convertFormat" />* -&gt; PNG<br />
<input type="radio" name="action" value="convertChained" />* -&gt; PNG-&gt; B&amp;W -&gt; size<br />

<%if (request.getAttribute("drawConversion").equals(Boolean.TRUE)) {%>
<h4>Draw</h4>
<input type="radio" name="action" value="convertSvgToPng" />SVG -&gt;PNG<br />
<%}%>

<%if (request.getAttribute("videoConversion").equals(Boolean.TRUE)) {%>
<h4>Video</h4>
<input type="radio" name="action" value="convertVideoToMpeg" />*-&gt; Mpg (ffmpeg)<br />
<input type="radio" name="action" value="convertVideoToMpegTs" />*-&gt; MpegTs (ffmpeg)<br />
<input type="radio" name="action" value="convertVideoTo3GPP" />* -&gt;3GPP (ffmpeg)<br />
<%}%>

<div class="submit"><input type="submit" value="Process..." /></div>
</div>

<p><a href="http://validator.w3.org/check?uri=referer">
<img src="icons/valid-xhtml11.png" alt="Valid XHTML 1.1" height="31" width="88" />
</a>
<a href="http://jigsaw.w3.org/css-validator">
<img src="icons/vcss.png" alt="Valid CSS" height="31" width="88" />
</a>
</p>
</div>

<div id="workspace">
<table>
	<thead>
		<tr>
			<th colspan="5">Usable Media List</th>
		</tr>
	</thead>
	<tbody>
		<%
			boolean lineParity = true;

            // get medias
            javax.emb.MediaEntityLocal[] medias = (javax.emb.MediaEntityLocal[]) request.getAttribute("samplesMedia");
            String[] proxiesUrl = (String[]) request.getAttribute("proxiesUrl");

            for (int i = 0; i < medias.length; i++) {

                String lineKind = "even";
                if (lineParity) {
                    lineKind = "odd";
                }
                lineParity = !lineParity;

                %>
		<tr class="<%= lineKind %>">
			<td class="media-checkbox" rowspan="2" valign="middle">
				<input type="radio" name="identity" value="<%= medias[i].getPrimaryKey() %>" />
			</td>
			<td class="media-proxy" align="center" rowspan="2">
				<a title="Retrive media" href="ActionDispatcher?action=retrieve&amp;identity=<%= medias[i].getPrimaryKey() %>">
					<img src="<%= proxiesUrl[i] %>" alt="<%= medias[i].getName() %>" />
				</a>
			</td>
			<td class="media-name" rowspan="2"><%=medias[i].getName()%></td>
			<td class="media-mimetype"><%=medias[i].getMimeType()%></td>
			<td class="media-size" align="right" rowspan="2"><%= BaseSampleServlet.SIZE_FORMAT.format(medias[i].getSize() / BaseSampleServlet.BYTE_IN_KB) %>&nbsp;kB</td>
		</tr>
		<tr class="<%= lineKind %>">
			<td class="media-desc"><%=(medias[i].getDescription() == null ? "" : medias[i].getDescription())%></td>
		</tr>
		<%}%>
	</tbody>
</table>
</div>


</form>
</div>
</body>
</html>
