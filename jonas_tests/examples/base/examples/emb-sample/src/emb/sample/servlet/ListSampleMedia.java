/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2005 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Brice Ruzand
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package emb.sample.servlet;

import java.io.IOException;
import java.net.URL;

import javax.emb.FormatNotFoundException;
import javax.emb.MediaEntityLocal;
import javax.emb.MediaEntityLocalHome;
import javax.emb.MediaFormatRegistry;
import javax.emb.ProtocolConstraints;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import emb.sample.session.MediaSampleSessionLocal;

/**
 * @version 1.0
 * @author
 */
public class ListSampleMedia extends BaseSampleServlet {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 3257289140767830576L;

    /**
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest,
     *      javax.servlet.http.HttpServletResponse)
     * @inheritDoc
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {

            MediaSampleSessionLocal session = getSession(request);

            MediaEntityLocal[] medias = session.getMediaList();

            if (medias.length > 0) {
                try {
                    MediaFormatRegistry.SINGLETON.lookup("svg");
                    request.setAttribute("drawConversion", Boolean.TRUE);
                } catch (FormatNotFoundException e) {
                    request.setAttribute("drawConversion", Boolean.FALSE);
                }

                try {
                    MediaFormatRegistry.SINGLETON.lookup("ts");
                    request.setAttribute("videoConversion", Boolean.TRUE);
                } catch (FormatNotFoundException e) {
                    request.setAttribute("videoConversion", Boolean.FALSE);
                }

                String[] proxiesUrl = new String[medias.length];
                for (int i = 0; i < medias.length; i++) {
                    URL url = null;
                    try {
                        url = getMebHome().publishContent(medias[i].getProxy(),
                                MediaEntityLocalHome.TRANSFER_TYPE_STREAM, new ProtocolConstraints());
                    } catch (Throwable e) {
                        // do nothing
                    }

                    proxiesUrl[i] = "";
                    if (url != null) {
                        proxiesUrl[i] = url.toExternalForm();
                    }
                }

                request.setAttribute("samplesMedia", medias);
                request.setAttribute("proxiesUrl", proxiesUrl);
                this.getServletContext().getRequestDispatcher("/jsps/mediaList.jsp").forward(request, response);

            } else {

                StringPrinter workspaceContent = new StringPrinter();

                // message
                workspaceContent.println("<h2>No Sample Media has been initialized.</h2>");

                request.setAttribute("workspaceContent", workspaceContent);
                getServletConfig().getServletContext().getRequestDispatcher(TEMPLATE_JSP).forward(request, response);

            }

        } catch (Throwable e) {
            exceptionHandler(e, getClass(), request, response);
        }

    }
}
