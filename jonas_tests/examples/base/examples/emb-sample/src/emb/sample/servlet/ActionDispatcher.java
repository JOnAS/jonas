/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2005 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Brice Ruzand
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package emb.sample.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Query dispatcher to implement an MVC model
 *
 * @author Brice Ruzand
 */
public class ActionDispatcher extends BaseSampleServlet {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 3257285816379389239L;

    /**
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest,
     *      javax.servlet.http.HttpServletResponse)
     * @inheritDoc
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // get the task to perform
            String action = request.getParameter("action");

            // Query the media entity EJB instance.
            String identity = request.getParameter("identity");

            if (action == null) {
                getServletContext().getRequestDispatcher("/HomeSample").forward(request, response);
            } else if (action.equals("home")) {
                getServletContext().getRequestDispatcher("/HomeSample").forward(request, response);
            } else if (action.equals("load")) {
                getServletContext().getRequestDispatcher("/LoadSampleMedia").forward(request, response);
            } else if (action.equals("list")) {
                getServletContext().getRequestDispatcher("/ListSampleMedia").forward(request, response);
            } else if (action.equals("upload")) {
                getServletContext().getRequestDispatcher("/UploadSampleMedia").forward(request, response);
            } else {

                if (identity == null) {

                    StringPrinter workspaceContent = new StringPrinter();

                    // message
                    workspaceContent.println("<h2>No media selected</h2>");
                    workspaceContent.println("Please return to the list and select a media to process...");

                    request.setAttribute("workspaceContent", workspaceContent);
                    getServletConfig().getServletContext().getRequestDispatcher(TEMPLATE_JSP)
                            .forward(request, response);

                } else if (action.equals("retrieve")) {
                    getServletContext().getRequestDispatcher("/RetrieveSampleMedia").forward(request, response);
                } else if (action.equals("extractHeader")) {
                    getServletContext().getRequestDispatcher("/ExtractHeaderData").forward(request, response);
                } else if (action.equals("convertBW")) {
                    getServletContext().getRequestDispatcher("/ConvertSampleImageBW").forward(request, response);
                } else if (action.equals("convertHalfSize")) {
                    getServletContext().getRequestDispatcher("/ConvertSampleImageHalfSize").forward(request, response);
                } else if (action.equals("convertFormat")) {
                    getServletContext().getRequestDispatcher("/ConvertSampleImageFormat").forward(request, response);
                } else if (action.equals("convertChained")) {
                    getServletContext().getRequestDispatcher("/ConvertSampleImageChained").forward(request, response);
                } else if (action.equals("convertSvgToPng")) {
                    getServletContext().getRequestDispatcher("/ConvertSampleImageSvgToPng").forward(request, response);
                } else if (action.equals("convertVideoTo3GPP")) {
                    getServletContext().getRequestDispatcher("/ConvertSampleVideoTo3GPP").forward(request, response);
                } else if (action.equals("convertVideoToMpegTs")) {
                    getServletContext().getRequestDispatcher("/ConvertSampleVideoToMpegTs").forward(request, response);
                } else if (action.equals("convertVideoToMpeg")) {
                    getServletContext().getRequestDispatcher("/ConvertSampleVideoToMpeg").forward(request, response);
                } else if (action.equals("convertOverlayed")) {
                    getServletContext().getRequestDispatcher("/ConvertSampleImageOverlayed").forward(request, response);
                } else if (action.equals("convertFrame")) {
                    getServletContext().getRequestDispatcher("/ConvertSampleImageFrame").forward(request, response);
                } else {
                    throw new Exception("Invalid Action dispatecher param.");
                }
            }
        } catch (Throwable e) {
            exceptionHandler(e, getClass(), request, response);
        }
    }
}
