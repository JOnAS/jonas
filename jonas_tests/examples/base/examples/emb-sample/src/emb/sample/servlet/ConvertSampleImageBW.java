/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2005 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Brice Ruzand
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package emb.sample.servlet;

import javax.emb.MediaConverterSpec;
import javax.emb.MediaEntityLocal;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.objectweb.jonas.emb.mfb.converter.image.ImageRGBtoGrayscaleConverterSpec;
import org.objectweb.jonas.emb.mfb.formats.image.ImageMediaFormat;

import emb.sample.MediaSampleException;
import emb.sample.session.MediaSampleSessionLocal;

/**
 * @author Brice Ruzand
 */
public class ConvertSampleImageBW extends BaseSampleServlet {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 3687815080511141944L;

    /**
     * @see javax.servlet.http.HttpServlet#service(javax.servlet.http.HttpServletRequest,
     *      javax.servlet.http.HttpServletResponse)
     * @inheritDoc
     */
    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        try {
            MediaSampleSessionLocal session = getSession(request);

            // Query the media entity EJB instance.
            String identity = request.getParameter("identity");
            MediaEntityLocal mediaEntity = session.getMediaFromPrimaryKey(identity);

            if (!(mediaEntity.getFormat() instanceof ImageMediaFormat)) {
                throw new MediaSampleException("The selected media must be an image.");
            }

            MediaEntityLocal media = session.copyMediaEntity(mediaEntity);
            media.setDescription("Black and White conversion  of: " + mediaEntity.getName());

            // Convert the MediaEntityToBlack&White
            MediaConverterSpec[] specs = new MediaConverterSpec[] {new ImageRGBtoGrayscaleConverterSpec()};
            media.convert(specs);

            // forward onto the retrieve servlet
            request.setAttribute("identity", media.getPrimaryKey());
            this.getServletContext().getRequestDispatcher(RETRIEVE_SERVLET).forward(request, response);

        } catch (Throwable e) {
            exceptionHandler(e, getClass(), request, response);
        }
    }

}
