/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2005 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Brice Ruzand
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package emb.sample.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import emb.sample.MediaSampleException;

/**
 * The Home of emb Sample
 *
 * @author Brice Ruzand
 */
public class HomeSample extends BaseSampleServlet {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -4450606249375127461L;

    /**
     * @see javax.servlet.http.HttpServlet#service(javax.servlet.http.HttpServletRequest,
     *      javax.servlet.http.HttpServletResponse)
     * @inheritDoc
     */
    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {

            try {
                // init session
                getSession(request);
            } catch (MediaSampleException e) {
                getSession(request);
            }

            StringPrinter workspaceContent = new StringPrinter();

            workspaceContent.println("<h2>Welcome to JOnAS Enterprise Media Beans Sample application</h2>");
            workspaceContent
                    .println("<p>This application provide a small sample of Enterprise Media Beans use with JOnAS.</p>");
            workspaceContent
                    .println("<h4>First you have to load media sample : <a href=\"ActionDispatcher?action=load\">Load</a></h4>");
            workspaceContent
                    .println("<h4>Then view the media list and work with it : <a href=\"ActionDispatcher?action=list\">Media list</a></h4>");
            workspaceContent
                    .println("<h4>Add your own media  : <a href=\"ActionDispatcher?action=upload\">Upload</a></h4>");

            request.setAttribute("workspaceContent", workspaceContent);
            getServletConfig().getServletContext().getRequestDispatcher(TEMPLATE_JSP).forward(request,
                    response);

        } catch (Throwable e) {
            exceptionHandler(e, getClass(), request, response);
        }
    }

}
