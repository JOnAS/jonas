/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2005 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Brice Ruzand
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package emb.sample;

/**
 * This exception extends {@link Exception} and is thrown whenever sample app
 * got a trouble
 */
public class MediaSampleException extends Exception {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 493847692100239586L;

    /**
     * This is the default constructor taking no arguments.
     */
    public MediaSampleException() {
        super();
    }

    /**
     * This is the default exception constructor taking a String argument.
     *
     * @param message the message.
     */
    public MediaSampleException(String message) {
        super(message);
    }

    /**
     * This is the default exception constructor taking a Throwable argument.
     *
     * @param cause the throwable.
     */
    public MediaSampleException(Throwable cause) {
        super(cause);
    }

    /**
     * This is the default exception constructor taking both a Throwable and a
     * String argument.
     *
     * @param message the message.
     * @param cause the throwable.
     */
    public MediaSampleException(String message, Throwable cause) {
        super(message, cause);
    }
}
