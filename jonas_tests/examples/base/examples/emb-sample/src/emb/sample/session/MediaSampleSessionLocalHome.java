/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2005 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Brice Ruzand
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package emb.sample.session;

import javax.ejb.CreateException;
import javax.ejb.EJBLocalHome;

/**
 * Home interface for the bean MediaSampleSessionLocal
 *
 * @author Brice Ruzand
 */
public interface MediaSampleSessionLocalHome extends EJBLocalHome {

    /**
     * Create an instance of the MediaSampleSessionLocal bean.
     *
     * @return the Remote interface of the bean MediaSampleSessionLocal.
     * @throws CreateException if the creation failed.
     */
    MediaSampleSessionLocal create() throws CreateException;

}