/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): ____________________________________.
 * Contributor(s): ______________________________________.
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package emb.sample.session;

import java.io.InputStream;
import java.net.URL;

import javax.ejb.EJBLocalObject;
import javax.emb.Media;
import javax.emb.MediaEntityLocal;

import emb.sample.MediaSampleException;

/**
 * Local interface for the bean MediaSampleSessionLocal
 */
public interface MediaSampleSessionLocal extends EJBLocalObject {

    /**
     * Set if the media has to be show in media list
     */
    byte SHOW_IN_LIST = 0;

    /**
     * Set if the media has to be show in media list and if the media has been
     * upload
     */
    byte USER_ADDED = 1;

    /**
     * Set if the media is not showed is media list
     */
    byte DONT_SHOW = -1;

    /**
     * Create a new MediaEntity from a stream
     *
     * @param contentStream content of the MediaEntity
     * @param name name of the MediaEntity
     * @param description description of the MediaEntity
     * @param status is ones of {@link MediaSampleSessionLocal#DONT_SHOW},
     *            {@link MediaSampleSessionLocal#SHOW_IN_LIST},
     *            {@link MediaSampleSessionLocal#USER_ADDED}
     * @param registeredName name to acces it with
     *            {@link #getMediaFromRegistredName(String)}
     * @return MediaEntity
     * @throws MediaSampleException if the mediaEntity cannot be create
     */
    MediaEntityLocal createMediaEntity(InputStream contentStream, String name, String description, byte status,
            String registeredName) throws MediaSampleException;

    /**
     * Create a new MediaEntity from an URL
     *
     * @param urlToImport of the media to import
     * @param description description of the MediaEntity
     * @param status is ones of {@link MediaSampleSessionLocal#DONT_SHOW},
     *            {@link MediaSampleSessionLocal#SHOW_IN_LIST},
     *            {@link MediaSampleSessionLocal#USER_ADDED}
     * @return MediaEntity
     * @throws MediaSampleException if the mediaEntity cannot be create
     */
    MediaEntityLocal createMediaEntity(URL urlToImport, String description, byte status) throws MediaSampleException;

    /**
     * Create a new MediaEntity from a Media
     *
     * @param media to copy
     * @return MediaEntity
     * @throws MediaSampleException if the mediaEntity cannot be create
     */
    MediaEntityLocal copyMediaEntity(Media media) throws MediaSampleException;

    /**
     * Get media list (only status {@link MediaSampleSessionLocal#SHOW_IN_LIST},
     * {@link MediaSampleSessionLocal#USER_ADDED}
     *
     * @return MediaEntity array
     * @throws MediaSampleException if media cannot be retrive
     */

    MediaEntityLocal[] getMediaList() throws MediaSampleException;

    /**
     * Get media from its premary key
     *
     * @param primaryKey the primary key of the wanted media
     * @return MediaEntity
     * @throws MediaSampleException if the mediaEntity cannot be retrive
     */
    MediaEntityLocal getMediaFromPrimaryKey(String primaryKey) throws MediaSampleException;

    /**
     * Get media from registered name
     *
     * @param registeredName the registeredName of the wanted media
     * @return MediaEntity
     * @throws MediaSampleException if the mediaEntity cannot be retrive
     */
    MediaEntityLocal getMediaFromRegistredName(String registeredName) throws MediaSampleException;

    /**
     * Remove default media to reinit the database, except uploaded media
     */
    void removeDefaultMedia();
}