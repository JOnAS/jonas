/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2005 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Brice Ruzand
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package emb.sample.servlet;

import java.io.IOException;

import javax.emb.MediaEntityLocal;
import javax.emb.MediaHeader;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import emb.sample.session.MediaSampleSessionLocal;

/**
 * @author Brice Ruzand
 */
public class ExtractHeaderData extends BaseSampleServlet {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 3544675070548261174L;

    /**
     * @see javax.servlet.http.HttpServlet#service(javax.servlet.http.HttpServletRequest,
     *      javax.servlet.http.HttpServletResponse)
     * @inheritDoc
     */
    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            MediaSampleSessionLocal session = getSession(request);

            // Query the media entity EJB instance.
            String identity = request.getParameter("identity");
            MediaEntityLocal mediaEntity = session.getMediaFromPrimaryKey(identity);

            MediaHeader header = mediaEntity.getHeader();
            String[] fieldNames = header.getFieldNames();

            StringPrinter newMenuBox = new StringPrinter();
            StringPrinter workspaceContent = new StringPrinter();

            newMenuBox.println("<div class=\"menu-box\">\r\n" + "<h3 class=\"menu-title\">Media</h3>\r\n");
            // add info
            displayInfo(newMenuBox, mediaEntity);

            workspaceContent.println("<h2>Header information for Media: " + mediaEntity.getName() + "</h2>\n");
            workspaceContent.println("<table>");
            workspaceContent.println("<thead><tr><th>Header tag name</th><th>Value</th></tr></thead>");
            workspaceContent.println("<tbody>");

            if (fieldNames.length == 0) {
                workspaceContent
                        .println("<tr><td colspan=\"2\" align=\"center\">There is no specific header for this media</td></tr>\n");
            } else {

                for (int index = 0; index < fieldNames.length; index++) {
                    String fieldName = fieldNames[index];
                    String fieldContent = "null";
                    if (header.getField(fieldNames[index]) != null) {
                        header.getField(fieldNames[index]).toString();
                    }
                    workspaceContent.println("<tr><td valign=\"top\" >");
                    workspaceContent.println(fieldName);
                    workspaceContent.println("</td><td>");
                    workspaceContent.println(fieldContent);
                    workspaceContent.println("</td></tr>\n");
                }
            }
            workspaceContent.println("</tbody>");
            workspaceContent.println("</table>\n");

            newMenuBox.println("</div>");

            request.setAttribute("newMenuBox", newMenuBox);
            request.setAttribute("workspaceContent", workspaceContent);
            getServletConfig().getServletContext().getRequestDispatcher(TEMPLATE_JSP).forward(request,
                    response);

        } catch (Throwable e) {
            exceptionHandler(e, getClass(), request, response);
        }
    }

}
