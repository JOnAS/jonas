/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2005 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Brice Ruzand
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package emb.sample.servlet;

import javax.emb.MediaConverterSpec;
import javax.emb.MediaEntityLocal;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.objectweb.jonas.emb.mfb.converter.image.ImageFrameConverterSpec;
import org.objectweb.jonas.emb.mfb.formats.image.ImageMediaFormat;

import emb.sample.MediaSampleException;
import emb.sample.session.MediaSampleSessionLocal;

/**
 * @author Brice Ruzand
 */
public class ConvertSampleImageFrame extends BaseSampleServlet {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -3697654416111319491L;

    /**
     * The quick acces to the frame, name in the property file
     */
    private static final String FRAME_ACCESS = "sampleFrame1";

    /**
     * The quick acces to the mask, name in the property file
     */
    private static final String MASK_ACCESS = "sampleMask1";

    /**
     * @see javax.servlet.http.HttpServlet#service(javax.servlet.http.HttpServletRequest,
     *      javax.servlet.http.HttpServletResponse)
     * @inheritDoc
     */
    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        try {
            MediaSampleSessionLocal session = getSession(request);

            // Query the media entity EJB instance.
            String identity = request.getParameter("identity");
            MediaEntityLocal mediaEntity = session.getMediaFromPrimaryKey(identity);

            // name in th property file
            MediaEntityLocal frame = null;
            MediaEntityLocal mask = null;

            try {
                frame = session.getMediaFromRegistredName(FRAME_ACCESS);
                mask = session.getMediaFromRegistredName(MASK_ACCESS);
            } catch (MediaSampleException e) {
                throw new MediaSampleException("(no mask and frame in the dataBase). Please init the database.");

            }

            if (!(mediaEntity.getFormat() instanceof ImageMediaFormat)) {
                throw new MediaSampleException("The selected media must be an image.");
            }

            // Create a copy
            MediaEntityLocal media = session.copyMediaEntity(mediaEntity);
            media.setDescription("Added frame to: " + mediaEntity.getName());

            // Convert the add specifed frame to the other image

            // well know position offset
            final int posX = 50;
            final int posY = 58;

            MediaConverterSpec[] specs = new MediaConverterSpec[] {new ImageFrameConverterSpec(frame, mask, posX, posY)};
            media.convert(specs);
            identity = (String) media.getPrimaryKey();

            // forward onto the retrieve servlet
            request.setAttribute("identity", media.getPrimaryKey());
            this.getServletContext().getRequestDispatcher(RETRIEVE_SERVLET).forward(request, response);

        } catch (Throwable e) {
            exceptionHandler(e, getClass(), request, response);
        }
    }
}
