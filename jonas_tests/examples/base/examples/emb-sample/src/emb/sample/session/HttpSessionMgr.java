/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2005 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Brice Ruzand
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package emb.sample.session;

import javax.ejb.RemoveException;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * The session manager to destroy used media
 */
public class HttpSessionMgr implements HttpSessionListener {

    /**
     * @see javax.servlet.http.HttpSessionListener#sessionCreated(javax.servlet.http.HttpSessionEvent)
     * @inheritDoc
     */
    public void sessionCreated(HttpSessionEvent se) {
        // do nothing
    }

    /**
     * Remove all media created during this session
     *
     * @see javax.servlet.http.HttpSessionListener#sessionDestroyed(javax.servlet.http.HttpSessionEvent)
     * @inheritDoc
     */
    public void sessionDestroyed(HttpSessionEvent se) {

        // get session bean
        MediaSampleSessionLocal sessionBean = (MediaSampleSessionLocal) se.getSession().getAttribute("sessionBean");

        if (sessionBean != null) {
            try {

                // the session bean
                sessionBean.remove();

            } catch (RemoveException e) {
                // do nothing
            }
            se.getSession().setAttribute("sessionBean", null);
        }

    }
}
