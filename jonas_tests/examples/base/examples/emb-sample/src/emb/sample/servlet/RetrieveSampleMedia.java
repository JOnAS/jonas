/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2005 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Brice Ruzand
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package emb.sample.servlet;

import java.net.URL;

import javax.emb.MediaEntityLocal;
import javax.emb.MediaEntityLocalHome;
import javax.emb.MediaFormatRegistry;
import javax.emb.ProtocolConstraints;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.objectweb.jonas.emb.mfb.formats.image.ImageMediaFormat;

import emb.sample.session.MediaSampleSessionLocal;

/**
 * Generate an HTML web page to present the media
 *
 * @author Brice Ruzand
 */
public class RetrieveSampleMedia extends BaseSampleServlet {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 3762250859580634161L;

    /**
     * @see javax.servlet.http.HttpServlet#service(javax.servlet.http.HttpServletRequest,
     *      javax.servlet.http.HttpServletResponse)
     * @inheritDoc
     */
    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        try {

            // Query the media entity EJB instance.
            String identity = (String) request.getAttribute("identity");
            if (identity == null) {
                identity = request.getParameter("identity");
            }

            MediaSampleSessionLocal session = getSession(request);

            // Query the media entity EJB instance.
            MediaEntityLocal mediaEntity = session.getMediaFromPrimaryKey(identity);

            // publish it
            URL url = null;

            if (mediaEntity.getFormat().equals(MediaFormatRegistry.SINGLETON.lookup("mp4"))) {
                url = getMebHome().publishContent(mediaEntity, MediaEntityLocalHome.TRANSFER_TYPE_STREAM,
                        new ProtocolConstraints());
            } else {
                url = getMebHome().publishContent(mediaEntity, MediaEntityLocalHome.TRANSFER_TYPE_BURST,
                        new ProtocolConstraints());
            }

            StringPrinter newMenuBox = new StringPrinter();
            StringPrinter workspaceContent = new StringPrinter();

            newMenuBox.println("<div class=\"menu-box\">\r\n" + "<h3 class=\"menu-title\">Media</h3>\r\n");

            workspaceContent.println("<h2>Media content : " + mediaEntity.getName() + "</h2>");

            workspaceContent.println("<span class=\"url\"> URL : " + url + "</span><br/>");

            if (mediaEntity.getFormat() instanceof ImageMediaFormat) {
                newMenuBox.println("<h2>Image</h2>");
                workspaceContent.println("<div class=media><img src=\"" + url + "\" alt=\"" + mediaEntity.getName()
                        + "\" /></div>");
            } else {
                newMenuBox.println("<h2>Standard Object</h2>");
                workspaceContent.println("<div class=\"media\"><object data=\"" + url + "\" type=\""
                        + mediaEntity.getMimeType()
                        + "\" width=\"500\" height=\"350\" standby=\"Loading in progress...\">"
                        + "Unfortunately your browser cannot display this media !!</object></div>");
            }

            // add info in menu
            displayInfo(newMenuBox, mediaEntity);

            newMenuBox
                    .println("<span>If your browser can not display this media, please select the link below for accessing the media.... ");
            newMenuBox.println("<a href=\"" + url + "\" >" + mediaEntity.getName() + "</a></span>");

            newMenuBox.println("</div>");

            request.setAttribute("newMenuBox", newMenuBox);
            request.setAttribute("workspaceContent", workspaceContent);
            getServletConfig().getServletContext().getRequestDispatcher(TEMPLATE_JSP).forward(request, response);

        } catch (Throwable e) {
            exceptionHandler(e, getClass(), request, response);
        }
    }

}
