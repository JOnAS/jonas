create table CAT (

	cat_id	 		VARCHAR(32)	NOT NULL,
	name			VARCHAR(255),
	sex			VARCHAR(1),
	weight			REAL,
	PRIMARY KEY(cat_id)
);
