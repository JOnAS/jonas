# ---------------------------------------------------------------------------
# JOnAS: Java(TM) Open Application Server
# Copyright (C) 2005 Bull S.A.
# Contact: jonas-team@objectweb.org
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
# USA
#
# ---------------------------------------------------------------------------
# $Id$
# ---------------------------------------------------------------------------

Example Overview
================
This example is the one provided with Hibernate documentation :
http://www.hibernate.org/hib_docs/reference/en/html_single/

The JOnAS hibernate war has been made by St�phane TRAUMAT
for the Sourcebeat book : JOnAS Live
( http://www.sourcebeat.com/TitleAction.do?id=9 )

The ant script provided will create the needed table, package
the application in a .war file and deploy it to $JONAS_BASE/webapps.

Directories structure
=====================
There is three directories :

- src : Contains the servlet and the Cat class.

- etc : Contains all non-java resources
- etc/resources : Hibernate configuration files
- etc/sql       : HSQL init/reset scripts
- etc/xml       : Deployment descriptors

- lib : Contains all the needed librairies for Hibernate.
  They will be added in the war.

Building and running this example
=====================
Take care to change the hsql.port properties (in build.properties)
if you do not use default one (9001)

To make the CAT example works, the first thing is to create
a table CAT to store the data in the HSQLDB engine included
with JOnAS.

note : The SQL to create the needed table will be executed
by the ant script.

1) To start the database, we need to run JOnAS using the
"jonas start" command.

2) Init the HSQL Database, type "ant initdb".

3) Then, under the hibernate sample directory, type: "ant".
  That will compile, package the webapp and place it
  under $JONAS_BASE/webapps.

4) And, to deploy the war on JOnAS, just type:
"jonas admin -a hibernate-sample.war"
You should see a message looking like :
2004-11-19 12:29:37,405 : AbsJWebContainerServiceImpl.registerWar :
War /C:/java/plateforme/JONAS/webapps/hibernate-sample.war available
at the context /hibernate-sample on the host .

5) Connect to http://localhost:9000/hibernate-sample/testHibernate/

Connecting to HSQLdb
=====================
1) type "ant dbmanager"
Press enter and you should be able to see the CAT table.

Notes
=====================
To develop this sample, we used
- JOnAS 4.4.2
- Hibernate 3.0.5
Because of some conflicts, we had to replace cglib-2.1.jar
with cglib-nodep-2.1_2.jar which is the same library without
the dependencies on ASM.