/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Initial developer(s): jonas team
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package jaasclient;

import jaasclient.beans.secusb.JAASOp;
import jaasclient.beans.secusb.JAASOpHome;

import java.awt.event.ActionEvent;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.rmi.PortableRemoteObject;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.WindowConstants;
import javax.transaction.UserTransaction;

/**
 * Sample for Session Bean.
 * @author jonas team
 * @author Florent Benoit
 * @author Markus Karg : Add Swing interface
 */
public class ClientJAASOpContClientSwing {

    /**
     * First amount to buy
     */
    private static final int FIRST_BUY_AMOUNT = 10;

    /**
     * Second amount to buy
     */
    private static final int SECOND_BUY_AMOUNT = 20;

    /**
     * Third amount to buy (will be rollback)
     */
    private static final int THIRD_BUY_AMOUNT = 50;

    /**
     * Constructor. Hide constructor as it is an utility class
     */
    private ClientJAASOpContClientSwing() {

    }

    /**
     * Main method
     * @param args the arguments
     */
    public static void main(String[] args) {


        final JFrame jf = new JFrame("Test Application");
        jf.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        final JButton jb = new JButton();
        final Action a = new AbstractAction("Click here to test!") {
                public final void actionPerformed(final ActionEvent v) {


                    Context initialContext = null;
                    try {
                        initialContext = new InitialContext();
                    } catch (Exception e) {
                        System.err.println("Cannot get initial context for JNDI: " + e);
                        System.exit(2);
                    }

                    // We want to start transactions from client: get UserTransaction
                    UserTransaction utx = null;
                    try {
                        utx = (UserTransaction) initialContext.lookup("java:comp/UserTransaction");
                    } catch (Exception e) {
                        System.err.println("Cannot lookup java:comp/UserTransaction: " + e);
                        System.exit(2);
                    }

                    // Connecting to JAASOpHome thru JNDI
                    JAASOpHome home = null;
                    try {
                        home = (JAASOpHome) PortableRemoteObject.narrow(initialContext.lookup("java:comp/env/ejb/JAASOp"), JAASOpHome.class);
                    } catch (Exception e) {
                        System.err.println("Cannot lookup java:comp/env/ejb/JAASOp: " + e + ". Maybe you haven't do the 'jonas admin -a jaasop.jar'");
                        System.exit(2);
                    }

                    // JAASOpBean creation
                    JAASOp t1 = null;
                    try {
                        System.out.println("Create a bean");
                        t1 = home.create("User1");
                    } catch (Exception e) {
                        System.err.println("Cannot create JAASOpBean: " + e);
                        System.exit(2);
                    }

                    // First transaction (committed)
                    try {
                        System.out.println("Start a first transaction");
                        utx.begin();
                        System.out.println("First request on the new bean");
                        t1.buy(FIRST_BUY_AMOUNT);
                        System.out.println("Second request on the bean");
                        t1.buy(SECOND_BUY_AMOUNT);
                        System.out.println("Commit the transaction");
                        utx.commit();
                    } catch (Exception e) {
                        System.err.println("exception during 1st Tx: " + e);
                        System.exit(2);
                    }
                    // Start another transaction (rolled back)
                    try {
                        System.out.println("Start a second transaction");
                        utx.begin();
                        t1.buy(THIRD_BUY_AMOUNT);
                        System.out.println("Rollback the transaction");
                        utx.rollback();
                    } catch (Exception e) {
                        System.err.println("exception during 2nd Tx: " + e);
                        System.exit(2);
                    }

                    // Get the total bought, outside the transaction
                    int val = 0;
                    try {
                        System.out.println("Request outside any transaction");
                        val = t1.read();
                    } catch (Exception e) {
                        System.err.println("Cannot read value on t1 : " + e);
                        System.exit(2);
                    }
                    if (val != FIRST_BUY_AMOUNT + SECOND_BUY_AMOUNT) {
                        System.err.println("Bad value read: " + val);
                        System.exit(2);
                    }

                    // Remove Session bean
                    try {
                        t1.remove();
                    } catch (Exception e) {
                        System.out.println("Exception on buy: " + e);
                        System.exit(2);
                    }
                    System.out.println("ClientJAASOpContClientSwing OK. Exiting.");
                    System.exit(0);



                }
            };
        jb.setAction(a);
        jf.getContentPane().add(jb);
        jf.pack();
        jf.setVisible(true);
    }
}
