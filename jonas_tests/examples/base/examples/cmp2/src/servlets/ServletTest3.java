/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2005 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s):
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.titan.address.AddressHomeLocal;
import com.titan.address.AddressLocal;
import com.titan.cabin.CabinHomeLocal;
import com.titan.cabin.CabinLocal;
import com.titan.cruise.CruiseHomeLocal;
import com.titan.cruise.CruiseLocal;
import com.titan.customer.CreditCardHomeLocal;
import com.titan.customer.CreditCardLocal;
import com.titan.customer.CustomerHomeLocal;
import com.titan.customer.CustomerLocal;
import com.titan.customer.Name;
import com.titan.phone.PhoneHomeLocal;
import com.titan.reservation.ReservationHomeLocal;
import com.titan.reservation.ReservationLocal;
import com.titan.ship.ShipHomeLocal;
import com.titan.ship.ShipLocal;

/**
 * This servlet is used to test O'Reilly examples
 * @author JOnAS team
 */
public class ServletTest3 extends HttpServlet {

    /**
     * Called by the server (via the service method) to allow a servlet to
     * handle a GET request.
     * @param request an HttpServletRequest object that contains the request the
     *        client has made of the servlet
     * @param response an HttpServletResponse object that contains the response
     *        the servlet sends to the client
     * @throws IOException if an input or output error is detected when the
     *         servlet handles the GET request
     * @throws ServletException if the request for the GET could not be handled
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        boolean ok = true;
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        out.println("<html>");
        out.println("<head>");
        out.println("<title>");
        out.println("O'Reilly Examples</title>");
        out.println("<link rel=\"stylesheet\" href=\"style.css\" />");
        out.println("</head>");
        out.println("<body style=\"background : white; color : black;\">");
        out.println("<h1>EJB QL Examples from Chapter 8</h1>");

        Context initialContext = null;
        try {
            initialContext = new InitialContext();
        } catch (Exception e) {
            out.print("<p>ERROR: Cannot get initial context for JNDI: " + e + "</p>");
            return;
        }

        out.println("<a href=\"index.html\"><b>Back to Menu</b></a>");

        // Connecting to CustomerHomeLocal and CreditCardHomeLocal thru JNDI
        CustomerHomeLocal customerhome = null;
        CreditCardHomeLocal cardhome = null;
        AddressHomeLocal addresshome = null;
        ShipHomeLocal shiphome = null;
        CruiseHomeLocal cruisehome = null;
        ReservationHomeLocal reservationhome = null;
        CabinHomeLocal cabinhome = null;
        PhoneHomeLocal phonehome = null;
        try {
            customerhome = (CustomerHomeLocal) initialContext.lookup("java:comp/env/ejb/CustomerHomeLocal");
            cardhome = (CreditCardHomeLocal) initialContext.lookup("java:comp/env/ejb/CreditCardHomeLocal");
            addresshome = (AddressHomeLocal) initialContext.lookup("java:comp/env/ejb/AddressHomeLocal");
            shiphome = (ShipHomeLocal) initialContext.lookup("java:comp/env/ejb/ShipHomeLocal");
            cruisehome = (CruiseHomeLocal) initialContext.lookup("java:comp/env/ejb/CruiseHomeLocal");
            reservationhome = (ReservationHomeLocal) initialContext.lookup("java:comp/env/ejb/ReservationHomeLocal");
            cabinhome = (CabinHomeLocal) initialContext.lookup("java:comp/env/ejb/CabinHomeLocal");
            phonehome = (PhoneHomeLocal) initialContext.lookup("java:comp/env/ejb/PhoneHomeLocal");
        } catch (Exception e) {
            out.println("<p>ERROR: Cannot lookup java:comp/env/ejb/XXHomeLocal: " + e + "</p>");
            return;
        }

        String cities[] = new String[6];
        cities[0] = "Minneapolis";
        cities[1] = "St. Paul";
        cities[2] = "Rochester";
        cities[3] = "Winona";
        cities[4] = "Wayzata";
        cities[5] = "Eagan";

        out.println("<h2>Example showing Sample EJB-QL</h2>");

        out.print("<H3>Creating a Ship and Cruise</H3>");
        ShipLocal shipA = null;
        CruiseLocal cruiseA = null;
        try {
            shipA = shiphome.create(new Integer(10772), "Ship A", 30000.0);
            cruiseA = cruisehome.create("Cruise A", shipA);
        } catch (Exception ex) {
            ok = false;
            out.print("<p>ERROR: Exception caught : " + ex + "</p>");
        }
        out.println("<ul>");
        out.println("<li>cruise.getName() = '" + cruiseA.getName() + "'</li>");
        out.println("<li>ship.getName() = '" + shipA.getName() + "'</li>");
        out.println("<li>cruise.getShip().getName() = '" + cruiseA.getShip().getName() + "'</li>");
        out.println("</ul>");

        out.print("<H3>Creating Ship Beans with Various Tonnage Values</H3>");
        out.print("<ul>");
        for (int jj = 1; jj <= 10; jj++) {
            try {
                ShipLocal ship = shiphome.create(new Integer(jj), "Ship " + jj, 30000.0 + (10000.0 * jj));
                printShip(out, ship);
            } catch (Exception ex) {
                ok = false;
                out.print("<p>ERROR: Exception caught : " + ex + "</p>");
            }

        }
        out.print("</ul>");

        out.print("<H3>Finding Ships with Exactly 100K Tonnage</H3>");
        out.print("<p><code>SELECT OBJECT(s) FROM JE2_Ship AS s WHERE s.tonnage = ?1</code></p>");
        Collection ships100k = null;
        try {
            ships100k = shiphome.findByTonnage(new Double(100000.0));
        } catch (Exception ex) {
            ok = false;
            out.print("<p>ERROR: Exception caught : " + ex + "</p>");
        }
        out.print("<p><i>Number of ships = " + ships100k.size() + " <br>Expected = 1 ships</i></p>");
        if (ships100k.size() != 1) {
            ok = false;
        }
        Iterator iterator = ships100k.iterator();
        out.print("<ul>");
        while (iterator.hasNext()) {
            ShipLocal ship = (ShipLocal) (iterator.next());
            printShip(out, ship);
        }
        out.print("</ul>");

        out.print("<H3>Finding Ships with Tonnage between 50K and 110K</H3>");
        out.print("<p><code>SELECT OBJECT(s) FROM JE2_Ship AS s WHERE s.tonnage BETWEEN ?1 AND ?2</code></p>");
        Collection ships50110k = null;
        try {
            ships50110k = shiphome.findByTonnage(new Double(50000.0), new Double(110000.0));
        } catch (Exception ex) {
            ok = false;
            out.print("<p>ERROR: Exception caught : " + ex + "</p>");
        }
        out.print("<p><i>Number of ships = " + ships50110k.size() + "<br>Expected = 7 ships</i></p>");
        if (ships50110k.size() != 7) {
            ok = false;
        }
        iterator = ships50110k.iterator();
        out.print("<ul>");
        while (iterator.hasNext()) {
            ShipLocal ship = (ShipLocal) (iterator.next());
            printShip(out, ship);
        }
        out.print("</ul>");

        // creating cruise for future use
        cruiseA = null;
        CruiseLocal cruiseB = null;
        try {
            ShipLocal ship1 = shiphome.findByPrimaryKey(new Integer(1));
            cruiseA = cruisehome.create("Alaska Cruise", ship1);
            cruiseB = cruisehome.create("Bohemian Cruise", ship1);
        } catch (Exception ex) {
            ok = false;
            out.print("<p>ERROR: Exception caught : " + ex + "<p>");
        }

        out.println("<h3>Creating Customers</h3>");
        for (int kk = 80; kk <= 99; kk++) {
            CustomerLocal customer = null;
            AddressLocal addr = null;
            try {
                customer = customerhome.create(new Integer(kk));
                customer.setName(new Name("Smith" + kk, "John"));
                customer.addPhoneNumber("612-555-12" + kk, (byte) 1);
                addr = addresshome.createAddress("10" + kk + " Elm Street", cities[(kk - 80) % 6], (kk % 2 == 0 ? "MN"
                        : "CA"), "5540" + (kk % 5 + 1));
            } catch (Exception e) {
                ok = false;
                out.println("<p>ERROR: exception caught = " + e + "<p>");
            }
            customer.setHomeAddress(addr);
            customer.setHasGoodCredit((kk % 4 == 0));
            //printCustomer(out, customer, addr);
        }

        // Creating Customers 1-6, each with 2 reservations for 2 cabins
        Calendar date = Calendar.getInstance();
        date.set(2002, 10, 1);
        try {

            for (int kk = 201; kk < 207; kk++) {
                Collection customers = new ArrayList();
                CustomerLocal cust = customerhome.create(new Integer(kk));
                cust.setName(new Name("Customer " + kk, "Mike"));
                cust.setHasGoodCredit((kk % 2 == 0)); // odds are bums
                AddressLocal addr = addresshome.createAddress("50" + kk + " Main Street", "Minneapolis", "MN", "5510"
                        + kk);
                cust.setHomeAddress(addr);
                //out.print("<li> Customer name=
                // "+cust.getName().getLastName()+"</li>");
                customers.add(cust); // put this single customer in the
                // collection

                //printCustomer(out, cust,addr);

                Collection reservations = new ArrayList();

                for (int jj = 0; jj < 2; jj++) {

                    ReservationLocal reservation = reservationhome.create(cruiseA, customers);
                    reservation.setDate(date.getTime());
                    reservation.setAmountPaid(1000 * kk + 100 * jj + 2000);
                    date.add(Calendar.DAY_OF_MONTH, 7);

                    Set cabins = new HashSet();
                    CabinLocal cabin = cabinhome.create(new Integer(1000 + kk * 100 + jj));
                    cabin.setDeckLevel(kk - 200);
                    cabin.setName("Cabin " + kk + "0" + jj + "1");

                    cabins.add(cabin);
                    cabin = cabinhome.create(new Integer(1000 + kk * 100 + 10 + jj));
                    cabin.setDeckLevel(kk - 200);
                    cabin.setName("Cabin " + kk + "0" + jj + "2");

                    cabins.add(cabin);

                    reservation.setCabins(cabins); // this reservation has 2
                    // cabins
                    //out.print("<ul>");
                    //printReservation(out, reservation);
                    //out.print("</ul>");
                }
            }
        } catch (Exception ex) {
            ok = false;
            out.print("<p>ERROR: Exception caught : " + ex + "</p>");
        }

        String fnames[] = new String[5];
        fnames[0] = "John";
        fnames[1] = "Paul";
        fnames[2] = "Ringo";
        fnames[3] = "Joe";
        fnames[4] = "Roger";

        String[] lnames = new String[3];
        lnames[0] = "Smith";
        lnames[1] = "Johnson";
        lnames[2] = "Star";

        // Creating Customers 50-69
        try {
            for (int kk = 50; kk <= 69; kk++) {
                CustomerLocal customer = customerhome.create(new Integer(kk));
                customer.setName(new Name(lnames[(kk - 50) % 3], fnames[(kk - 50) % 5]));
                customer.addPhoneNumber("612-555-12" + kk, (byte) 1);
                AddressLocal addr = addresshome.createAddress("10" + kk + " Elm Street", cities[(kk - 50) % 6],
                        (kk % 2 == 0 ? "MN" : "CA"), "5540" + (kk % 5 + 1));
                customer.setHomeAddress(addr);
                customer.setHasGoodCredit((kk % 4 == 0));

                //printCustomer(out, customer, addr);

                // Some customers will have reservations already on one of the
                // two cruises..
                if (kk % 3 != 0) {
                    Collection customers = new ArrayList();
                    customers.add(customer); // put this single customer in the
                    // collection
                    ReservationLocal reservation = reservationhome.create((kk % 3 == 1 ? cruiseA : cruiseB), customers);
                    reservation.setDate(date.getTime());
                    reservation.setAmountPaid(10 * kk + 2000);
                    //out.print("<ul>");
                    //printReservation(out, reservation);
                    //out.print("</ul>");

                    date.add(Calendar.DAY_OF_MONTH, 7);
                }
            }
        } catch (Exception ex) {
            ok = false;
            out.print("<p>ERROR: Exception caught : " + ex + "</p>");
        }

        // Creating Customers 100-109
        try {
            for (int kk = 100; kk <= 109; kk++) {
                CustomerLocal customer = customerhome.create(new Integer(kk));
                customer.setName(new Name("Lennon" + kk, "Paul"));
                customer.addPhoneNumber("666-543-12" + kk, (byte) 1);
                AddressLocal addr = addresshome.createAddress("10" + kk + " Abbey Road", cities[(kk - 100) % 6],
                        (kk % 2 == 0 ? "FL" : "WA"), "5540" + (kk % 5 + 1));

                customer.setHomeAddress(addr);
                customer.setHasGoodCredit((kk % 4 == 0));

                //printCustomer(out, customer, addr);
            }
        } catch (Exception ex) {
            ok = false;
            out.print("<p>ERROR: Exception caught : " + ex + "</p>");
        }

        try {
            listCustomers(out, customerhome);
        } catch (Exception e) {
            ok = false;
            out.println("<p>ERROR: exception caught = " + e + "</p>");
        }

        out.print("<H3>Finding Customer having name 'John Smith85'</H3>");
        out
                .print("<p><code>SELECT OBJECT(c) FROM JE2_Customer AS c WHERE c.lastName = ?1 AND c.firstName = ?2</code></p>");
        CustomerLocal customer85 = null;
        try {
            customer85 = customerhome.findByExactName("Smith85", "John");
        } catch (Exception ex) {
            ok = false;
            out.print("<p>ERROR: Exception caught : " + ex + "</p>");
        }
        AddressLocal addr85 = customer85.getHomeAddress();
        printCustomer(out, customer85, addr85);

        out.print("<H3>Finding Customer 'Smith90'</H3>");
        out.print("<p><code>SELECT OBJECT(c) FROM JE2_Customer AS c WHERE c.lastName = 'Smith90' </code></p>");
        CustomerLocal customer90 = null;
        try {
            customer90 = customerhome.findSmith90();
        } catch (Exception ex) {
            ok = false;
            out.print("<p>ERROR: Exception caught : " + ex + "</p>");
        }

        AddressLocal addr90 = customer90.getHomeAddress();
        printCustomer(out, customer90, addr90);

        out.print("<H3>Finding Customers having GoodCredit</H3>");
        out.print("<p><code>SELECT OBJECT(c) FROM JE2_Customer AS c WHERE c.hasGoodCredit = TRUE</code></p>");
        Collection mplscustomers = null;
        try {
            mplscustomers = customerhome.findByGoodCredit();
        } catch (Exception ex) {
            ok = false;
            out.print("<p>ERROR: Exception caught : " + ex + "</p>");
        }

        iterator = mplscustomers.iterator();
        out.print("<p><i>Number of customers = " + mplscustomers.size() + "<br>Expected = 16 customers</i></p>");
        if (mplscustomers.size() != 16) {
            ok = false;
        }
        while (iterator.hasNext()) {
            CustomerLocal customer = (CustomerLocal) (iterator.next());
            AddressLocal addr = customer.getHomeAddress();
            printCustomer(out, customer, addr);
        }

        out.print("<H3>Finding Customers having City = 'Minneapolis' and State = 'MN'</H3>");
        out
                .print("<p><code>SELECT OBJECT(c) FROM JE2_Customer AS c WHERE c.homeAddress.city = ?1 AND c.homeAddress.state = ?2</code></p>");
        try {
            mplscustomers = customerhome.findByCity("Minneapolis", "MN");
        } catch (Exception ex) {
            ok = false;
            out.print("<p>ERROR: Exception caught : " + ex + "</p>");
        }
        out.print("<p><i>Number of customers = " + mplscustomers.size() + "<br>Expected = 14 customers</i></p>");
        if (mplscustomers.size() != 14) {
            ok = false;
        }
        iterator = mplscustomers.iterator();
        while (iterator.hasNext()) {
            CustomerLocal customer = (CustomerLocal) (iterator.next());
            AddressLocal addr = customer.getHomeAddress();
            printCustomer(out, customer, addr);
        }

        out.print("<H3>Cabins Table Content</H3>");
        try {
            listCabins(out, cabinhome);
        } catch (Exception ex) {
            ok = false;
            out.print("<p>ERROR: Exception caught : " + ex + "</p>");
        }

        out.print("<H3>Retrieve a collection of all cabins on deck '3'</H3>");
        out.print("<p><code>SELECT OBJECT(c) FROM Cabin AS c WHERE c.deckLevel = ?1</code></p>");
        Collection cabins = null;
        try {
            cabins = cabinhome.findAllOnDeckLevel(new Integer(3));
            out.print("<p><i>Number of cabins = " + cabins.size() + "<br>Expected = 4 cabins</i></p>");
            Iterator iter = cabins.iterator();
            out.print("<ul>");
            while (iter.hasNext()) {
                CabinLocal cabin = (CabinLocal) (iter.next());
                out.print("<li>cabin '" + cabin.getName() + "' on deck '" + cabin.getDeckLevel() + "'</li>");
            }
            out.print("</ul>");
        } catch (Exception ex) {
            ok = false;
            out.print("<p>ERROR: Exception caught : " + ex + "</p>");
        }

        try {
            out.print("<H3>Finding Customer having a name exactly matching 'Joe Star'</H3>");
            out
                    .print("<p><code>SELECT OBJECT(c) FROM JE2_Customer AS c WHERE c.lastName = ?1 AND c.firstName = ?2</code></p>");
            CustomerLocal customer = customerhome.findByExactName("Star", "Joe");
            AddressLocal addr = customer.getHomeAddress();
            printCustomer(out, customer, addr);
            Collection customers = null;

            out.print("<H3>Finding Customers having a name like 'Jo S' (no wildcards)</H3>");
            out
                    .print("<p><code>SELECT OBJECT(c) FROM JE2_Customer AS c WHERE c.lastName LIKE ?1 AND c.firstName LIKE ?2</code></p>");

            customers = customerhome.findByName("S", "Jo");
            iterator = customers.iterator();
            while (iterator.hasNext()) {
                customer = (CustomerLocal) (iterator.next());
                addr = customer.getHomeAddress();
                printCustomer(out, customer, addr);
            }
            out.print("<p><i>Number of customers = " + customers.size() + "<br>Expected = 0 customers</i></p>");
            if (customers.size() != 0) {
                ok = false;
            }

            out.print("<H3>Finding Customers having a name like 'Jo% S%' (with wildcards)</H3>");
            out
                    .print("<p><code>SELECT OBJECT(c) FROM JE2_Customer AS c WHERE c.lastName LIKE ?1 AND c.firstName LIKE ?2</code></p>");
            customers = customerhome.findByName("S%", "Jo%");
            iterator = customers.iterator();
            out.print("<p><i>Number of customers = " + customers.size() + "<br>Expected = 26 customers</i></p>");
            if (customers.size() != 26) {
                ok = false;
            }
            while (iterator.hasNext()) {
                customer = (CustomerLocal) (iterator.next());
                addr = customer.getHomeAddress();
                printCustomer(out, customer, addr);
            }

            out.print("<H2>Finding Customers having a name like 'Jo% S%' and living in 'MN'</H2>");
            out
                    .print("<p><code>SELECT OBJECT(c) FROM JE2_Customer AS c <br>WHERE c.lastName LIKE ?1 AND c.firstName LIKE ?2 AND c.homeAddress.state = ?3</code></p>");
            customers = customerhome.findByNameAndState("S%", "Jo%", "MN");
            iterator = customers.iterator();
            out.print("<p><i>Number of customers = " + customers.size() + "<br>Expected = 13 customers</i></p>");
            if (customers.size() != 13) {
                ok = false;
            }
            while (iterator.hasNext()) {
                customer = (CustomerLocal) (iterator.next());
                addr = customer.getHomeAddress();
                printCustomer(out, customer, addr);
            }

            out.print("<H2>Finding Customers Living in Warm Climates</H2>");
            out
                    .print("<p><code>SELECT OBJECT(c) FROM JE2_Customer AS c WHERE c.homeAddress.state IN ('FL','TX','AZ','CA')</code></p>");
            customers = customerhome.findInHotStates();
            out.print("<p><i>Number of customers = " + customers.size() + "<br>Expected = 25 customers</i></p>");
            if (customers.size() != 25) {
                ok = false;
            }
            iterator = customers.iterator();
            while (iterator.hasNext()) {
                customer = (CustomerLocal) (iterator.next());
                addr = customer.getHomeAddress();
                printCustomer(out, customer, addr);
            }

            out.print("<H2>Finding Customers With No Reservation</H2>");
            out.print("<p><code>SELECT OBJECT(c) FROM JE2_Customer AS c WHERE c.reservations IS EMPTY</code></p>");
            customers = customerhome.findWithNoReservations();
            out.print("<p><i>Number of customers = " + customers.size() + "<br>Expected = 37 customers</i></p>");
            if (customers.size() != 37) {
                ok = false;
            }
            iterator = customers.iterator();
            while (iterator.hasNext()) {
                customer = (CustomerLocal) (iterator.next());
                addr = customer.getHomeAddress();
                printCustomer(out, customer, addr);
            }

            out.print("<H2>Finding Customers On Alaska Cruise</H2>");
            out
                    .print("<p><code>SELECT OBJECT(c) FROM JE2_Customer AS cust, Cruise AS cr, IN (cr.reservations) AS res <br>WHERE cr = ?1 AND cust MEMBER OF res.customers</code></p>");
            CruiseLocal crA = cruisehome.findByName("Alaska Cruise");
            customers = customerhome.findOnCruise(cruiseA);
            out.print("<p><i>Number of customers = " + customers.size() + "<br>Expected = 12 customers</i></p>");
            if (customers.size() != 12) {
                ok = false;
            }
            iterator = customers.iterator();
            while (iterator.hasNext()) {
                customer = (CustomerLocal) (iterator.next());
                addr = customer.getHomeAddress();
                printCustomer(out, customer, addr);
            }

            out.print("<H2>Select the zip codes of the state 'FL'</H2>");
            out.print("<p><code>SELECT a.zip FROM JE2_Address AS a  WHERE a.state = ?1</code></p>");
            Collection zipCodes = null;
            zipCodes = addresshome.selectZipCodes("FL");
            out.print("<p><i>Number of zip codes = " + zipCodes.size()
                    + "<br>Expected = 5 zip codes (from 55401 to 55405)</i></p>");
            if (zipCodes.size() != 5) {
                ok = false;
            }
            Iterator iZipCodes = zipCodes.iterator();
            out.println("<ul>");
            while (iZipCodes.hasNext()) {
                out.print("<li>zip code = '" + iZipCodes.next() + "'</li>");
            }
            out.println("</ul>");

            out.print("<H2>Select the customer which have the address 'addr85' </H2>");
            out.print("<p><code>SELECT OBJECT(c) FROM JE2_Customer AS c WHERE c.homeAddress = ?1</code></p>");
            CustomerLocal cust = null;
            cust = addresshome.selectCustomer(addr85);
            addr = cust.getHomeAddress();
            printCustomer(out, cust, addr);

        } catch (Exception ex) {
            ok = false;
            out.print("<p>ERROR: Exception caught : " + ex + "</p>");
        }
        out.print("<H2>Cleaning all tables</H2>");

        try {
            Collection clc = customerhome.findAllCustomers();
            iterator = clc.iterator();
            while (iterator.hasNext()) {
                CustomerLocal cl = (CustomerLocal) iterator.next();
                cl.remove();
            }
            clc = shiphome.findAllShips();
            iterator = clc.iterator();
            while (iterator.hasNext()) {
                ShipLocal sl = (ShipLocal) iterator.next();
                sl.remove();
            }
            clc = cruisehome.findAllCruises();
            iterator = clc.iterator();
            while (iterator.hasNext()) {
                CruiseLocal cl = (CruiseLocal) iterator.next();
                cl.remove();
            }
            clc = reservationhome.findAllReservations();
            iterator = clc.iterator();
            while (iterator.hasNext()) {
                ReservationLocal rl = (ReservationLocal) iterator.next();
                rl.remove();
            }
            clc = cabinhome.findAllCabins();
            iterator = clc.iterator();
            while (iterator.hasNext()) {
                CabinLocal cl = (CabinLocal) iterator.next();
                cl.remove();
            }
        } catch (Exception ex) {
            ok = false;
            out.println("<p>ERROR: during cleaning exception caught = " + ex + "</p>");
        }

        if (ok) {
            out.println("<p align=\"center\"><strong>Servlet is OK.</strong></p>");
        }
        out.println("<a href=\"index.html\"><b>Back to Menu</b></a>");
        out.println("</body>");
        out.println("</html>");

    }

    private void listCabins(PrintWriter out, CabinHomeLocal chl) throws Exception {
        out.println("<p><b>Cabins</b> Table Content:</p>");
        out.println("<ul>");
        Collection clc = chl.findAllCabins();
        if (clc == null) {
            out.println("<li>Cabins table is empty</li>");
        } else {
            Iterator iterator = clc.iterator();
            while (iterator.hasNext()) {
                CabinLocal cl = (CabinLocal) iterator.next();
                String name = cl.getName();
                int deck = cl.getDeckLevel();
                int bd = cl.getBedCount();
                out.println("<li>cabinName = '" + name + "', deckLevel = '" + deck + "', bedCount = '" + bd + "'</li>");
            }
        }
        out.println("</ul>");
    }

    private void listCustomers(PrintWriter out, CustomerHomeLocal chl) throws Exception {
        out.println("<p><b>Customers</b> Table Content:</p>");
        out.println("<ul>");
        java.util.Collection clc = chl.findAllCustomers();
        if (clc == null) {
            out.println("<li>Customers table is empty</li>");
        } else {
            java.util.Iterator iterator = clc.iterator();
            while (iterator.hasNext()) {
                CustomerLocal cl = (CustomerLocal) iterator.next();
                String name = cl.getName().getLastName();
                String fname = cl.getName().getFirstName();
                String number = " No Card! ";
                boolean gc = cl.getHasGoodCredit();
                if (cl.getCreditCard() != null) {
                    number = cl.getCreditCard().getNumber();
                }
                out.print("<li>firstName = '" + fname + "', lastName = '" + name + "'");
                out.println("<ul>");
                out.print("<li>creditCardNumber = '" + number + "', goodCredit = '" + gc + "'</li>");
                if (cl.getHomeAddress() != null) {
                    String city = cl.getHomeAddress().getCity();
                    String state = cl.getHomeAddress().getState();
                    out.print("<li>city> = '" + city + "', state = '" + state + "'</li>");
                }
                out.println("</li>");
                out.println("</ul>");
            }
        }
        out.println("</ul>");
    }

    private void listCreditcards(PrintWriter out, CreditCardHomeLocal cchl) throws Exception {
        out.println("<p><b>CreditCards</b> Table Content:</p>");
        out.println("<ul>");
        java.util.Collection clc = cchl.findAllCreditCards();
        if (clc == null) {
            out.println("<li>CreditCards table is empty</li>");
        } else {
            java.util.Iterator iterator = clc.iterator();
            while (iterator.hasNext()) {
                CreditCardLocal ccl = (CreditCardLocal) iterator.next();
                String number = ccl.getNumber();
                String name = ccl.getNameOnCard();
                out.println("<li>creditCardNumber = '" + number + "', nameOnCard '= '" + name + "'</li>");
            }
        }
        out.println("</ul>");
    }

    private void listAddress(PrintWriter out, AddressHomeLocal cchl) throws Exception {
        out.println("<p><b>Addresses</b> Table Content:</p>");
        out.println("<ul>");
        java.util.Collection clc = cchl.findAllAddress();
        if (clc == null) {
            out.println("<li>Addresses table is empty</li>");
        } else {
            java.util.Iterator iterator = clc.iterator();
            while (iterator.hasNext()) {
                AddressLocal al = (AddressLocal) iterator.next();
                String city = al.getCity();
                String street = al.getStreet();
                out.println("<li>adressCity = '" + city + "', street = '" + street + "'</li>");
            }
        }
        out.println("</ul>");
    }

    private void listPhones(PrintWriter out, Vector vv) {
        out.println("<ul>");
        for (int jj = 0; jj < vv.size(); jj++) {
            String ss = (String) (vv.get(jj));
            out.println("<li> " + ss + "</li>");
        }
        out.println("</ul>");
    }

    private void printShip(PrintWriter out, ShipLocal ship) {
        out.print("<li> shipId = '" + ship.getId() + "', name = '" + ship.getName() + "', tonnage = '"
                + ship.getTonnage() + "'</li>");
    }

    private void printCustomer(PrintWriter out, CustomerLocal customer, AddressLocal addr) {
        out.print("<ul>");
        out.print("<li>firstName = '" + customer.getName().getFirstName() + "', lastName = '"
                + customer.getName().getLastName() + "', goodCredit = '" + customer.getHasGoodCredit() + "'</li>");
        out.print("<li>street = '" + addr.getStreet() + "', city = '" + addr.getCity() + "', state = '"
                + addr.getState() + "', zip = '" + addr.getZip() + "'</li>");
        out.print("</ul>");
    }

    private void printReservation(PrintWriter out, ReservationLocal reservation) {
        out.print("<ul>");
        String cru = "no Cruise!";
        if (reservation.getCruise() != null) {
            cru = reservation.getCruise().getName();
        }
        out.print("<li>reservationDate: '" + reservation.getDate() + "' on '" + cru + "' amountPaid: '"
                + reservation.getAmountPaid() + "'</li>");
        Set cabinset = reservation.getCabins();
        String customerinfo = "";
        CustomerLocal cust = null;
        String cabininfo = "";
        Set customerset = reservation.getCustomers();
        Iterator iterator = customerset.iterator();
        while (iterator.hasNext()) {
            cust = (CustomerLocal) iterator.next();
            customerinfo += cust.getName().getLastName() + " ";
        }
        iterator = cabinset.iterator();
        while (iterator.hasNext()) {
            CabinLocal cabin = (CabinLocal) iterator.next();
            cabininfo += cabin.getName() + " ";
        }
        out.print("<ul>");
        if (!customerinfo.equals("")) {
            out.print("<li>customers: " + customerinfo);
        }
        if (!cabininfo.equals("")) {
            out.print("<li>cabins: " + cabininfo + "</li>");
        }
        out.print("</ul>");
        out.print("</ul>");
    }

}