/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s):
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import com.titan.cabin.CabinHomeLocal;
import com.titan.cabin.CabinLocal;
import com.titan.cruise.CruiseHomeLocal;
import com.titan.cruise.CruiseLocal;
import com.titan.customer.CustomerHomeLocal;
import com.titan.customer.CustomerLocal;
import com.titan.customer.Name;
import com.titan.reservation.ReservationHomeLocal;
import com.titan.reservation.ReservationLocal;
import com.titan.ship.ShipHomeLocal;
import com.titan.ship.ShipLocal;

/**
 * This servlet is used to test O'Reilly examples
 * @author JOnAS team
 */
public class ServletTest2 extends HttpServlet {

    /**
     * Called by the server (via the service method) to allow a servlet to
     * handle a GET request.
     * @param request an HttpServletRequest object that contains the request the
     *        client has made of the servlet
     * @param response an HttpServletResponse object that contains the response
     *        the servlet sends to the client
     * @throws IOException if an input or output error is detected when the
     *         servlet handles the GET request
     * @throws ServletException if the request for the GET could not be handled
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        boolean ok = true;
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head>");
        out.println("<title>");
        out.println("O'Reilly Examples (Suite)</title>");
        out.println("<link rel=\"stylesheet\" href=\"style.css\" />");
        out.println("</head>");
        out.println("<body style=\"background : white; color : black;\">");
        out.println("<h1>Example Showing Cruise/Ship and Cruise/Reservation Relationships</h1>");
        Context initialContext = null;
        try {
            initialContext = new InitialContext();
        } catch (Exception e) {
            out.println("<p>ERROR: Cannot get initial context for JNDI: " + e + "</p>");
            return;
        }

        // Connecting to ShipHomeLocal thru JNDI
        ShipHomeLocal shiphome = null;
        CruiseHomeLocal cruisehome = null;
        ReservationHomeLocal reservationhome = null;
        CustomerHomeLocal customerhome = null;
        CabinHomeLocal cabinhome = null;
        UserTransaction tran = null;
        try {
            shiphome = (ShipHomeLocal) initialContext.lookup("java:comp/env/ejb/ShipHomeLocal");
            cruisehome = (CruiseHomeLocal) initialContext.lookup("java:comp/env/ejb/CruiseHomeLocal");
            reservationhome = (ReservationHomeLocal) initialContext.lookup("java:comp/env/ejb/ReservationHomeLocal");
            customerhome = (CustomerHomeLocal) initialContext.lookup("java:comp/env/ejb/CustomerHomeLocal");
            cabinhome = (CabinHomeLocal) initialContext.lookup("java:comp/env/ejb/CabinHomeLocal");
            tran = (UserTransaction) initialContext.lookup("java:comp/UserTransaction");
        } catch (Exception e) {
            out.println("<p>ERROR: Cannot lookup java:comp/env/ejb/XXHomeLocal: " + e + "</p>");
            return;

        }

        out.println("<a href=\"index.html\"><b>Back to Menu</b></a>");

        out.println("<H2>Example Showing Cruise/Ship Relationship (Fig 7-12)</H2>");
        out.println("<p>(ie Sharing a bean reference in a Many-to-One Relationship)</p>");

        out.println("<H3>Creating Ships</H3>");
        // Create some Ship beans - manually set key
        ShipLocal shipA = null;
        ShipLocal shipB = null;
        try {
            shipA = shiphome.create(new Integer(1001), "Ship A", 30000.0);
            shipB = shiphome.create(new Integer(1002), "Ship B", 40000.0);
        } catch (Exception e) {
            out.println("<p>ERROR: Exception caught " + e + "</p>");
            return;
        }
        out.println("<ul>");
        out.println("<li>id = '" + shipA.getId() + "', name = '" + shipA.getName() + "', tonnage = '"
                + shipA.getTonnage() + "'</li>");
        out.println("<li>id = '" + shipB.getId() + "', name=" + shipB.getName() + "', tonnage = '" + shipB.getTonnage()
                + " </li>");
        out.println("</ul>");

        out.println("<H3>Creating Cruises</H3>");
        // Create some Cruise beans - automatic key generation by CMP engine
        // Link 1-3 to Ship A, 4-6 to Ship B
        CruiseLocal[] cruises = new CruiseLocal[7];
        try {
            cruises[1] = cruisehome.create("Cruise 1", shipA);
            cruises[2] = cruisehome.create("Cruise 2", shipA);
            cruises[3] = cruisehome.create("Cruise 3", shipA);
            cruises[4] = cruisehome.create("Cruise 4", shipB);
            cruises[5] = cruisehome.create("Cruise 5", shipB);
            cruises[6] = cruisehome.create("Cruise 6", shipB);
        } catch (Exception e) {
            out.println("<p>ERROR: Exception caught " + e + "</p>");
            return;
        }
        out.println("<ul>");
        for (int jj = 1; jj < 7; jj++) {
            CruiseLocal cc = cruises[jj];
            out.println("<li>'" + cc.getName() + "' is using '" + cc.getShip().getName() + "'</li>");
        }
        out.println("</ul>");

        out.print("<H3>Changing 'Cruise 1' to use same ship as 'Cruise 4'</H3>");
        ShipLocal newship = cruises[4].getShip();
        cruises[1].setShip(newship);
        out.println("<ul>");
        for (int jj = 1; jj < 7; jj++) {
            CruiseLocal cc = cruises[jj];
            out.println("<li>'" + cc.getName() + "' is using '" + cc.getShip().getName() + "'</li>");
        }
        out.println("</ul>");

        out.println("<H3>Content of Tables</H3>");
        try {
            listCruises(out, cruisehome);
            listShips(out, shiphome);
        } catch (Exception e) {
            out.println("<p>ERROR: exception caught = " + e + " </p>");
        }

        out.println("<H2>Example Showing Cruise/Reservation Relationship Using set() (Fig 7-14)</H2>");
        out
                .println("<p>(ie Sharing an entire collection in a One-to-Many bidirectional Relationship Using set() )</p>");
        out.println("<H3>Creating some Cruise beans</H3>");
        // Create some Cruise beans - leave ship reference empty since we don't
        // care
        CruiseLocal cruiseA = null;
        CruiseLocal cruiseB = null;
        try {
            cruiseA = cruisehome.create("Cruise A", null);
            cruiseB = cruisehome.create("Cruise B", null);
        } catch (Exception e) {
            out.println("<p>ERROR: exception caught = " + e + " </p>");
        }
        out.println("<ul>");
        out.print("<li>name = '" + cruiseA.getName() + "'</li>");
        out.print("<li>name = '" + cruiseB.getName() + "'</li>");
        out.println("</ul>");

        out.print("<H3>Creating Reservations</H3>");
        // Create some Reservation beans - automatic key generation by CMP
        // engine
        // Link 1-3 to Cruise A, 4-6 to Cruise B
        ReservationLocal reservations[] = new ReservationLocal[7];
        Calendar date = Calendar.getInstance();
        date.set(2002, 10, 1);
        // Leave the Customers collection null in the create() call,
        // we don't care about it right now
        try {
            reservations[1] = reservationhome.create(cruiseA, null);
            reservations[1].setDate(date.getTime());
            reservations[1].setAmountPaid(4000.0);
            date.add(Calendar.DAY_OF_MONTH, 7);

            reservations[2] = reservationhome.create(cruiseA, null);
            reservations[2].setDate(date.getTime());
            reservations[2].setAmountPaid(5000.0);
            date.add(Calendar.DAY_OF_MONTH, 7);

            reservations[3] = reservationhome.create(cruiseA, null);
            reservations[3].setDate(date.getTime());
            reservations[3].setAmountPaid(6000.0);
            date.add(Calendar.DAY_OF_MONTH, 7);

            reservations[4] = reservationhome.create(cruiseB, null);
            reservations[4].setDate(date.getTime());
            reservations[4].setAmountPaid(7000.0);
            date.add(Calendar.DAY_OF_MONTH, 7);

            reservations[5] = reservationhome.create(cruiseB, null);
            reservations[5].setDate(date.getTime());
            reservations[5].setAmountPaid(8000.0);
            date.add(Calendar.DAY_OF_MONTH, 7);

            reservations[6] = reservationhome.create(cruiseB, null);
            reservations[6].setDate(date.getTime());
            reservations[6].setAmountPaid(9000.0);
            date.add(Calendar.DAY_OF_MONTH, 7);

        } catch (Exception e) {
            out.println("<p>ERROR: Exception caught " + e + "</p>");
            return;
        }
        out.println("<ul>");
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        for (int jj = 1; jj < 7; jj++) {
            ReservationLocal rr = reservations[jj];
            CruiseLocal thiscruise = rr.getCruise();
            String cruisename = (thiscruise != null ? thiscruise.getName() : "No Cruise!");
            out.println("<li>reservationDate = '" + df.format(rr.getDate()) + "', amountPaid = '" + rr.getAmountPaid()
                    + "', for '" + cruisename + "'</li> ");
        }
        out.println("</ul>");

        out.println("<H3>Testing CruiseB.setReservations(CruiseA.getReservations())</H3>");
        // show the effect of a simple "setReservations" on a cruise
        Collection areservations = cruiseA.getReservations();
        cruiseB.setReservations(areservations);
        // Report information on the 6 reservations
        String[] cruisename = new String[7];
        out.println("<ul>");
        for (int jj = 1; jj < 7; jj++) {
            ReservationLocal rr = reservations[jj];
            CruiseLocal thiscruise = rr.getCruise();
            cruisename[jj] = (thiscruise != null ? thiscruise.getName() : "No Cruise!");
            out.print("<li>reservationDate = '" + df.format(rr.getDate()) + "', amount paid = '" + rr.getAmountPaid()
                    + "', for '" + cruisename[jj] + "'</li>");
        }
        out.println("</ul>");
        if (!cruisename[4].equals("No Cruise!") || !cruisename[5].equals("No Cruise!")
                || !cruisename[6].equals("No Cruise!")) {
            ok = false;
        }

        out.print("<H3>Content of Tables</H3>");
        try {
            listCruises(out, cruisehome);
            listReservations(out, reservationhome);
        } catch (Exception e) {
            out.println("<p>ERROR: exception caught = " + e + " </p>");
            return;
        }

        out.println("<H2>Example Showing Cruise/Reservation Relationship Using addAll(..) (Fig 7-15)</H2>");
        out.print("<p>(ie Using Collection.addAll() in a One-to-Many bidirectional relationship)</p>");
        // Show how to combine reservations using Collection methods
        // Operations such as this must be done in a transaction, usually done
        // within a
        // stateless session EJB using declarative transactions rather than
        // manually like this

        date.set(2002, 10, 1);
        // Remove previous reservation
        for (int jj = 1; jj < 7; jj++) {
            try {
                reservations[jj].remove();
            } catch (Exception ex) {
                out.println("<p>ERROR: exception caught = " + ex + "</p>");
            }
        }
        // Reinit the reservation as before the previous test
        try {
            reservations[1] = reservationhome.create(cruiseA, null);
            reservations[1].setDate(date.getTime());
            reservations[1].setAmountPaid(4000.0);
            date.add(Calendar.DAY_OF_MONTH, 7);

            reservations[2] = reservationhome.create(cruiseA, null);
            reservations[2].setDate(date.getTime());
            reservations[2].setAmountPaid(5000.0);
            date.add(Calendar.DAY_OF_MONTH, 7);

            reservations[3] = reservationhome.create(cruiseA, null);
            reservations[3].setDate(date.getTime());
            reservations[3].setAmountPaid(6000.0);
            date.add(Calendar.DAY_OF_MONTH, 7);

            reservations[4] = reservationhome.create(cruiseB, null);
            reservations[4].setDate(date.getTime());
            reservations[4].setAmountPaid(7000.0);
            date.add(Calendar.DAY_OF_MONTH, 7);

            reservations[5] = reservationhome.create(cruiseB, null);
            reservations[5].setDate(date.getTime());
            reservations[5].setAmountPaid(8000.0);
            date.add(Calendar.DAY_OF_MONTH, 7);

            reservations[6] = reservationhome.create(cruiseB, null);
            reservations[6].setDate(date.getTime());
            reservations[6].setAmountPaid(9000.0);
            date.add(Calendar.DAY_OF_MONTH, 7);

        } catch (Exception e) {
            out.println("<p>ERROR: Exception caught " + e + "<p>");
            return;
        }

        Collection breservations = null;
        try {
            tran.begin();
            areservations = cruiseA.getReservations();
            breservations = cruiseB.getReservations();
            breservations.addAll(areservations);
            tran.commit();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                tran.rollback();
            } catch (Exception ex) {
                ex.printStackTrace();
                return;
            }
            return;
        }
        // Report information on the 6 reservations
        out.println("<ul>");
        for (int jj = 1; jj < 7; jj++) {
            ReservationLocal rr = reservations[jj];
            CruiseLocal thiscruise = rr.getCruise();
            cruisename[jj] = (thiscruise != null ? thiscruise.getName() : "No Cruise!");
            out.print("<li>reservationDate = '" + df.format(rr.getDate()) + "', amount paid = '" + rr.getAmountPaid()
                    + "', for '" + cruisename[jj] + "'</li>");
        }
        out.println("</ul>");

        out.print("<H3>Contents of Tables</H3>");
        try {
            listCruises(out, cruisehome);
            listReservations(out, reservationhome);
        } catch (Exception e) {
            out.println("<p>ERROR: exception caught = " + e + "</p>");
            return;
        }

        out.println("<H2>Example Showing Reservation/Customer Relationships (Fig 7-17)</H2>");
        out.print("<p>(ie Using Collection.addAll() in a Many-to-Many bidirectional relationship)</p>");

        out.println("<H3>Using 'ShipA' and 'CruiseA'</H3>");
        cruiseA.setShip(shipA);
        out.println("<ul>");
        out.println("<li>cruise.getName() = '" + cruiseA.getName() + "'</li>");
        out.println("<li>ship.getName() = '" + shipA.getName() + "'</li>");
        out.println("<li>cruise.getShip().getName() = '" + cruiseA.getShip().getName() + "'</li>");
        out.println("</ul>");

        out.println("<H3>Creating two sets of customers, one with 1-3, and one with 4-6</H3>");
        // create two sets of customers, one with customers 1-3 and one with 4-6
        Set lowcustomers = new HashSet();
        Set highcustomers = new HashSet();
        CustomerLocal cust = null;
        out.println("<ul>");
        for (int kk = 1; kk < 7; kk++) {
            try {
                cust = customerhome.create(new Integer(kk));
            } catch (Exception e) {
                out.println("<p>ERROR: exception caught = " + e + "</p>");
            }
            cust.setName(new Name("Customer " + kk, "mike"));
            if (kk <= 3) {
                lowcustomers.add(cust);
            } else {
                highcustomers.add(cust);
            }
            out.print("<li>customer '" + cust.getName().getLastName() + "' created </li>");
        }
        out.println("</ul>");
        // Remove previous reservation
        for (int jj = 1; jj < 7; jj++) {
            try {
                reservations[jj].remove();
            } catch (Exception ex) {
                out.println("<p>ERROR: exception caught = " + ex + " </p>");
            }
        }

        out.print("<H3>Creating Reservations '1' and '2', each with 3 customers</H3>");
        try {
            reservations[1] = reservationhome.create(cruiseA, lowcustomers);
            reservations[1].setDate(date.getTime());
            reservations[1].setAmountPaid(4000.0);
            date.add(Calendar.DAY_OF_MONTH, 7);

            reservations[2] = reservationhome.create(cruiseA, highcustomers);
            reservations[2].setDate(date.getTime());
            reservations[2].setAmountPaid(5000.0);
            date.add(Calendar.DAY_OF_MONTH, 7);
        } catch (Exception e) {
            out.println("<p>ERROR: Exception caught " + e + "</p>");
            return;
        }
        // report information on the reservations
        reportReservations(out, reservations, 2, tran);

        out.print("<H3>Performing customers_a.addAll(customers_b) test</H3>");
        // Finally we can perform the test shown in Figure 7-17
        // Operations such as this must be done in a transaction, usually done
        // within a
        // stateless session EJB using declarative transactions rather than
        // manually like this
        try {
            tran.begin();
            Set customers_a = reservations[1].getCustomers();
            Set customers_b = reservations[2].getCustomers();
            customers_a.addAll(customers_b);
            tran.commit();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                tran.rollback();
            } catch (Exception ex) {
                out.println("<p>ERROR: Exception caught " + ex + "</p>");
                return;
            }
        }
        // report information on the reservations
        reportReservations(out, reservations, 2, tran);

        out.print("<H2>Sharing an entire collection in a Many-to-Many bidirectional relationship (Fig 7-18)</H2>");
        out.println("<H3>Creating four sets of customers 1-3, 2-4, 3-5, 4-6 </H3>");
        // create four sets of customers, 1-3, 2-4, 3-5, 4-6
        Set customers13 = new HashSet();
        Set customers24 = new HashSet();
        Set customers35 = new HashSet();
        Set customers46 = new HashSet();
        out.println("<ul>");
        for (int kk = 1; kk < 7; kk++) {
            try {
                cust = customerhome.create(new Integer(kk + 500));
                cust.setName(new Name("Customer_1 " + kk, "bill"));
            } catch (Exception e) {
                out.println("<p>ERROR: exception caught = " + e + " </p></ul>");
                return;
            }
            if (kk <= 3) {
                customers13.add(cust);
            }
            if (kk >= 2 && kk <= 4) {
                customers24.add(cust);
            }
            if (kk >= 3 && kk <= 5) {
                customers35.add(cust);
            }
            if (kk >= 4) {
                customers46.add(cust);
            }
            out.print("<li>customer '" + cust.getName().getLastName() + "' created</li>");
        }
        out.println("</ul>");

        out.print("<H3>Creating Reservations 1-4 using three customers each</H3>");
        ReservationLocal reservations1[] = new ReservationLocal[5];
        try {
            reservations1[1] = reservationhome.create(cruiseA, customers13);
            reservations1[1].setDate(date.getTime());
            reservations1[1].setAmountPaid(4000.0);
            date.add(Calendar.DAY_OF_MONTH, 7);

            reservations1[2] = reservationhome.create(cruiseA, customers24);
            reservations1[2].setDate(date.getTime());
            reservations1[2].setAmountPaid(5000.0);
            date.add(Calendar.DAY_OF_MONTH, 7);

            reservations1[3] = reservationhome.create(cruiseA, customers35);
            reservations1[3].setDate(date.getTime());
            reservations1[3].setAmountPaid(6000.0);
            date.add(Calendar.DAY_OF_MONTH, 7);

            reservations1[4] = reservationhome.create(cruiseA, customers46);
            reservations1[4].setDate(date.getTime());
            reservations1[4].setAmountPaid(7000.0);
            date.add(Calendar.DAY_OF_MONTH, 7);
        } catch (Exception e) {
            out.println("<p>ERROR: Exception caught " + e + "</p>");
            return;
        }
        reportReservations(out, reservations1, 4, tran);

        out.print("<H3>Performing reservationD.setCustomers(customersA) (Fig 7-18)</H3>");
        try {
            tran.begin();
            Set customers_a = reservations1[1].getCustomers();
            reservations1[4].setCustomers(customers_a);
            tran.commit();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                tran.rollback();
            } catch (Exception ex) {
                out.println("<p>ERROR: Exception caught " + ex + "</p>");
                return;
            }
        }
        reportReservations(out, reservations1, 4, tran);

        out.print("<H2>Example Showing Reservation/Cabin Relationships (Fig 7-20)</H2>");
        out.print("<p>(ie Removing beans in a Many-to-Many unidirectional relationship)</p>");
        out.print("<H3>Creating Cabins four sets of cabins, 1-3, 2-4, 3-5, 4-6</H3>");
        // create four sets of cabins, 1-3, 2-4, 3-5, 4-6
        Set cabins13 = new HashSet();
        Set cabins24 = new HashSet();
        Set cabins35 = new HashSet();
        Set cabins46 = new HashSet();
        CabinLocal cabin = null;
        out.println("<ul>");
        for (int kk = 1; kk < 7; kk++) {
            try {
                cabin = cabinhome.create(new Integer(kk));
            } catch (Exception e) {
                out.println("<p>ERROR: exception caught = " + e + " </p></ul>");
            }
            cabin.setName("Cabin " + kk);
            if (kk <= 3) {
                cabins13.add(cabin);
            }
            if (kk >= 2 && kk <= 4) {
                cabins24.add(cabin);
            }
            if (kk >= 3 && kk <= 5) {
                cabins35.add(cabin);
            }
            if (kk >= 4) {
                cabins46.add(cabin);
            }
            out.print("<li>cabin '" + cabin.getName() + "' created</li>");
        }
        out.println("</ul>");

        out.print("<H3>Creating Reservations 1-4 using three cabins each</H2>");
        ReservationLocal reservations2[] = new ReservationLocal[5];
        // leave Customers collection null, we dont care about it for this
        // example
        try {
            reservations2[1] = reservationhome.create(cruiseA, null);
            reservations2[1].setCabins(cabins13);
            reservations2[1].setDate(date.getTime());
            reservations2[1].setAmountPaid(4000.0);
            date.add(Calendar.DAY_OF_MONTH, 7);

            reservations2[2] = reservationhome.create(cruiseA, null);
            reservations2[2].setCabins(cabins24);
            reservations2[2].setDate(date.getTime());
            reservations2[2].setAmountPaid(5000.0);
            date.add(Calendar.DAY_OF_MONTH, 7);

            reservations2[3] = reservationhome.create(cruiseA, null);
            reservations2[3].setCabins(cabins35);
            reservations2[3].setDate(date.getTime());
            reservations2[3].setAmountPaid(6000.0);
            date.add(Calendar.DAY_OF_MONTH, 7);

            reservations2[4] = reservationhome.create(cruiseA, null);
            reservations2[4].setCabins(cabins46);
            reservations2[4].setDate(date.getTime());
            reservations2[4].setAmountPaid(7000.0);
            date.add(Calendar.DAY_OF_MONTH, 7);
        } catch (Exception e) {
            out.println("<p>ERROR: Exception caught " + e + "</p>");
            return;
        }
        reportReservations(out, reservations2, 4, tran);

        out.print("<H3>Performing cabins_a collection iterator.remove() test</H3>");
        // Finally we can perform the test shown in Figure 7-20
        try {
            tran.begin();
            Set cabins_a = reservations2[1].getCabins();
            Iterator iterator = cabins_a.iterator();
            out.print("<ul>");
            while (iterator.hasNext()) {
                CabinLocal cc = (CabinLocal) iterator.next();
                out.print("<li>Removing '" + cc.getName() + "' from 'cabins_a'" + "</li>");
                iterator.remove();
            }
            out.print("</ul>");
            tran.commit();
        } catch (Exception e) {
            try {
                tran.rollback();
            } catch (Exception ex) {
                out.println("<p>ERROR: Exception caught " + ex + "</p>");
                return;
            }
        }
        reportReservations(out, reservations2, 4, tran);

        out.print("<H2>Example Showing Reservation/Cabin Relationships </H2>");

        out.print("<H3>Creating Cabins in ShipA</H3>");
        CabinLocal cabins[] = new CabinLocal[4];
        try {
            cabins[1] = cabinhome.create(new Integer(10));
            cabins[1].setShip(shipA);
            cabins[1].setDeckLevel(1);
            cabins[1].setName("Minnesota Suite");
            cabins[1].setBedCount(2);

            cabins[2] = cabinhome.create(new Integer(11));
            cabins[2].setShip(shipA);
            cabins[2].setDeckLevel(2);
            cabins[2].setName("California Suite");
            cabins[2].setBedCount(2);

            cabins[3] = cabinhome.create(new Integer(12));
            cabins[3].setShip(shipA);
            cabins[3].setDeckLevel(3);
            cabins[3].setName("Missouri Suite");
            cabins[3].setBedCount(2);
        } catch (Exception e) {
            out.println("<p>ERROR: Exception caught " + e + "</p>");
            return;
        }
        out.println("<ul>");
        for (int jj = 1; jj < 4; jj++) {
            CabinLocal cc = cabins[jj];
            out.print("<li>cabin '" + cc.getName() + "' is on ship '" + cc.getShip().getName() + "'</li>");
        }
        out.println("</ul>");

        out.print("<H3>Creating Reservations</H3>");
        ReservationLocal reservations3[] = new ReservationLocal[3];
        try {
            reservations3[1] = reservationhome.create(cruiseA, null);
            reservations3[1].setDate(date.getTime());
            reservations3[1].setAmountPaid(4000.0);
            date.add(Calendar.DAY_OF_MONTH, 7);

            reservations3[2] = reservationhome.create(cruiseA, null);
            reservations3[2].setDate(date.getTime());
            reservations3[2].setAmountPaid(5000.0);
            date.add(Calendar.DAY_OF_MONTH, 7);
        } catch (Exception e) {
            out.println("<p>ERROR: Exception caught " + e + "</p>");
            return;
        }
        reportReservations(out, reservations3, 2, tran);

        out.print("<H3>Creating Links from Reservations to Cabins</H3>");
        Set cabins1 = new HashSet(2);
        cabins1.add(cabins[1]);
        cabins1.add(cabins[2]);
        Set cabins2 = new HashSet(2);
        cabins2.add(cabins[2]);
        cabins2.add(cabins[3]);
        reservations3[1].setCabins(cabins1);
        reservations3[2].setCabins(cabins2);
        reportReservations(out, reservations3, 2, tran);

        out.print("<H3>Testing reservation_b.setCabins(reservation_a.getCabins())</H3>");
        try {
            tran.begin();
            Set cabins_a = reservations3[1].getCabins();
            reservations3[2].setCabins(cabins_a);
            tran.commit();
        } catch (Exception e) {
            try {
                tran.rollback();
            } catch (Exception ex) {
                out.println("<p>ERROR: Exception caught " + ex + "</p>");
                return;
            }
        }
        reportReservations(out, reservations3, 2, tran);

        out.print("<H2>Cleaning all tables</H2>");
        try {
            Collection clc = customerhome.findAllCustomers();
            java.util.Iterator iterator = clc.iterator();
            while (iterator.hasNext()) {
                CustomerLocal cl = (CustomerLocal) iterator.next();
                cl.remove();
            }
            clc = shiphome.findAllShips();
            iterator = clc.iterator();
            while (iterator.hasNext()) {
                ShipLocal sl = (ShipLocal) iterator.next();
                sl.remove();
            }
            clc = cruisehome.findAllCruises();
            iterator = clc.iterator();
            while (iterator.hasNext()) {
                CruiseLocal cl = (CruiseLocal) iterator.next();
                cl.remove();
            }

            clc = reservationhome.findAllReservations();
            iterator = clc.iterator();
            while (iterator.hasNext()) {
                ReservationLocal rl = (ReservationLocal) iterator.next();
                rl.remove();
            }
            clc = cabinhome.findAllCabins();
            iterator = clc.iterator();
            while (iterator.hasNext()) {
                CabinLocal cl = (CabinLocal) iterator.next();
                cl.remove();
            }

        } catch (Exception ex) {
            out.println("<p>ERROR: during cleaning exception caught = " + ex + "</p>");
        }
        if (ok) {
            out.println("<p align=\"center\"><strong>Servlet is OK.</strong></p>");
        }
        out.println("<a href=\"index.html\"><b>Back to Menu</b></a>");
        out.println("</body>");
        out.println("</html>");
    }

    private void listCruises(PrintWriter out, CruiseHomeLocal chl) throws Exception {
        out.println("<p><b>Cruises</b> Table Content:</p>");
        out.println("<ul>");
        java.util.Collection clc = chl.findAllCruises();
        if (clc == null) {
            out.println("<li>Cruises table is empty</li>");
        } else {
            java.util.Iterator iterator = clc.iterator();
            while (iterator.hasNext()) {
                CruiseLocal cl = (CruiseLocal) iterator.next();
                String cname = cl.getName();
                String sname = (cl.getShip() != null ? cl.getShip().getName() : "No Ship!");
                out.println("<li>cruiseName = '" + cname + "', shipName = '" + sname + "'</li>");

            }
        }
        out.println("</ul>");
    }

    private void listShips(PrintWriter out, ShipHomeLocal cchl) throws Exception {
        out.println("<p><b>Ships</b> Table Content:</p>");
        out.println("<ul>");
        java.util.Collection clc = cchl.findAllShips();
        if (clc == null) {
            out.println("<li>Ships table is empty </li>");
        } else {
            java.util.Iterator iterator = clc.iterator();
            while (iterator.hasNext()) {
                ShipLocal ccl = (ShipLocal) iterator.next();
                double tonnage = ccl.getTonnage();
                String name = ccl.getName();
                out.println("<li>shipName = '" + name + "', tonnage = '" + tonnage + "'</li>");
            }
        }
        out.println("</ul>");
    }

    private void listReservations(PrintWriter out, ReservationHomeLocal cchl) throws Exception {
        out.println("<p><b>Reservations</b> Table Content:</p>");
        out.println("<ul>");
        java.util.Collection clc = cchl.findAllReservations();
        if (clc == null) {
            out.println("<li>Reservations table is empty</li>");
            out.println("</ul>");
        } else {
            java.util.Iterator iterator = clc.iterator();
            while (iterator.hasNext()) {
                ReservationLocal ccl = (ReservationLocal) iterator.next();
                Date date = ccl.getDate();
                out.println("<li>reservation date = '" + date + "'</li>");
            }
            out.println("</ul>");
        }
    }

    private void reportReservations(PrintWriter out, ReservationLocal reservations[], int nb, UserTransaction tran) {
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        // report information on the reservations
        out.println("<ul>");
        for (int jj = 1; jj < nb + 1; jj++) {
            ReservationLocal rr = reservations[jj];
            CruiseLocal thiscruise = rr.getCruise();
            String cruisename = (thiscruise != null ? thiscruise.getName() : "No Cruise!");
            // Operations such as this must be done in a transaction, usually
            // done within a
            // stateless session EJB using declarative transactions rather than
            // manually like this
            String customerinfo = "";
            CustomerLocal cust = null;
            String cabininfo = "";
            try {
                tran.begin();
                Set customerset = rr.getCustomers();
                Iterator iterator = customerset.iterator();
                while (iterator.hasNext()) {
                    cust = (CustomerLocal) iterator.next();
                    customerinfo += cust.getName().getLastName() + " ";
                }
                Set cabinset = rr.getCabins();
                iterator = cabinset.iterator();
                while (iterator.hasNext()) {
                    CabinLocal cabin = (CabinLocal) iterator.next();
                    cabininfo += cabin.getName() + " ";
                }
                tran.commit();
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    tran.rollback();
                } catch (Exception ex) {
                    out.println("<p>ERROR: Exception caught " + ex + "</p>");
                    return;
                }
            }
            out.print("<li>reservationDate = '" + df.format(rr.getDate()) + "', amountPaid = '" + rr.getAmountPaid()
                    + "', for '" + cruisename + "' with :</li>");
            out.print("<ul>");
            if (!customerinfo.equals("")) {
                out.print("<li>customers: " + customerinfo);
            }
            if (!cabininfo.equals("")) {
                out.print("<li>cabins: " + cabininfo + "</li>");
            }
            out.print("</ul>");
        }
        out.println("</ul>");
    }

}