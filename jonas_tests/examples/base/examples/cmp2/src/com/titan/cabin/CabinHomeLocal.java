package com.titan.cabin;

import com.titan.customer.CustomerLocal;

import java.util.Collection;
import java.util.Set;
import javax.ejb.EJBException;
import javax.ejb.CreateException;
import javax.ejb.FinderException;

public interface CabinHomeLocal extends javax.ejb.EJBLocalHome {

    public CabinLocal create(Integer id)
        throws CreateException, EJBException;

    public CabinLocal findByPrimaryKey(Integer pk)
        throws FinderException, EJBException;

    public Collection findAllOnDeckLevel(Integer level)
        throws FinderException;

    public Collection findAllCabins()
        throws FinderException;

    // Home method, requires ejbHomeSelectAllForCustomer method in bean class
    public Set selectAllForCustomer(CustomerLocal cust) 
        throws FinderException;
}
