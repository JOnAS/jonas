package com.titan.phone;

import javax.ejb.EntityContext;
import java.util.Date;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public abstract class PhoneBean implements javax.ejb.EntityBean {

    public Object ejbCreate(String number, byte type) throws javax.ejb.CreateException {
        setNumber(number);
        setType(type);

        return null;
    }

    public void ejbPostCreate(String number, byte type) {
    }

    // persistent fields
    public abstract String getNumber();
    public abstract void setNumber(String number);
    public abstract byte getType();
    public abstract void setType(byte type);

    // standard call back methods
    
    public void setEntityContext(EntityContext ec){}
    public void unsetEntityContext(){}
    public void ejbLoad(){}
    public void ejbStore(){}
    public void ejbActivate(){}
    public void ejbPassivate(){}
    public void ejbRemove() throws javax.ejb.RemoveException{}

}
