package com.titan.cruise;

import com.titan.ship.*;
import javax.ejb.CreateException;
import javax.ejb.FinderException;
import java.util.Collection;


// Cruise EJB's local home interface
public interface CruiseHomeLocal extends javax.ejb.EJBLocalHome {

    public CruiseLocal create(String name, ShipLocal ship)
        throws javax.ejb.CreateException;

    public CruiseLocal findByPrimaryKey(Object primaryKey)
        throws javax.ejb.FinderException;

    public Collection findAllCruises() 
        throws FinderException;

    public CruiseLocal findByName(String name)
        throws FinderException;
}
