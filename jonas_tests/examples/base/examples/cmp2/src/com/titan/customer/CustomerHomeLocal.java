package com.titan.customer;

import java.rmi.RemoteException;
import javax.ejb.CreateException;
import javax.ejb.FinderException;
import java.util.Collection;
import com.titan.cruise.CruiseLocal;

public interface CustomerHomeLocal extends javax.ejb.EJBLocalHome {

    public CustomerLocal create(Integer id) 
        throws CreateException;
    public CustomerLocal findByPrimaryKey(Integer id) 
        throws FinderException;

    public Collection findAllCustomers() 
        throws FinderException;

    public Collection findByName(String lastName, String firstName)
        throws FinderException;

    public CustomerLocal findByExactName(String lastName, String firstName)
        throws FinderException;

    public Collection findByNameAndState(String lastName, String firstName, String state)
        throws FinderException;

    public CustomerLocal findSmith90() 
        throws FinderException;

    public Collection findByGoodCredit()
        throws FinderException;

    public Collection findByCity(String city, String state)
        throws FinderException;

    public Collection findInHotStates()
        throws FinderException;

    public Collection findWithNoReservations()
        throws FinderException;

    public Collection findOnCruise(CruiseLocal cruise)
        throws FinderException;

    public Collection findByState(String state)
        throws FinderException;
}
