package com.titan.travelagent;

import java.rmi.RemoteException;
import javax.ejb.CreateException;

public interface RTravelAgentHomeRemote extends javax.ejb.EJBHome {

    public RTravelAgentRemote create()
        throws RemoteException, CreateException;

}
