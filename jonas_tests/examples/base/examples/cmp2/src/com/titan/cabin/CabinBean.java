package com.titan.cabin;

import java.util.Set;

import javax.ejb.EntityContext;
import javax.ejb.FinderException;

import com.titan.customer.CustomerLocal;
import com.titan.ship.ShipLocal;

public abstract class CabinBean implements javax.ejb.EntityBean {

    public Integer ejbCreate(Integer id) throws javax.ejb.CreateException {
        this.setId(id);
        return null;
    }

    public void ejbPostCreate(Integer id) {

    }

    public abstract Set ejbSelectAllForCustomer(CustomerLocal cust) throws FinderException;

    // Public Home method required to test the private ejbSelectAllForCustomer
    // method
    public Set ejbHomeSelectAllForCustomer(CustomerLocal cust) throws FinderException {
        return this.ejbSelectAllForCustomer(cust);
    }

    public abstract void setId(Integer id);

    public abstract Integer getId();

    public abstract void setShip(ShipLocal ship);

    public abstract ShipLocal getShip();

    public abstract void setName(String name);

    public abstract String getName();

    public abstract void setBedCount(int count);

    public abstract int getBedCount();

    public abstract void setDeckLevel(int level);

    public abstract int getDeckLevel();

    public int getShipId() {
        return getShip().getId().intValue();
    }

    public void setShipId(int sp) {
        ShipLocal sl = getShip();
        sl.setId(new Integer(sp));
        setShip(sl);
    }

    public void setEntityContext(EntityContext ctx) {
        // Not implemented.
    }

    public void unsetEntityContext() {
        // Not implemented.
    }

    public void ejbActivate() {
        // Not implemented.
    }

    public void ejbPassivate() {
        // Not implemented.
    }

    public void ejbLoad() {
        // Not implemented.
    }

    public void ejbStore() {
        // Not implemented.
    }

    public void ejbRemove() throws javax.ejb.RemoveException {
        // Not implemented.
    }
}