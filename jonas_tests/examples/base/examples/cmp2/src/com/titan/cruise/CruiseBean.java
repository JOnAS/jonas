package com.titan.cruise;

import javax.ejb.CreateException;
import javax.ejb.RemoveException;
import javax.ejb.EntityContext;
import com.titan.ship.*;
import java.util.Collection;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public abstract class CruiseBean implements javax.ejb.EntityBean {

    public Integer ejbCreate(String name, ShipLocal ship)  throws CreateException {
        setName(name);
        return null;
    }
	
    public void ejbPostCreate(String name, ShipLocal ship) {
        setShip(ship);
    }

    // persistent fields
    public abstract void setName(String name);
    public abstract String getName( );

    public abstract void setShip(ShipLocal ship);
    public abstract ShipLocal getShip( );

    // relationship fields

    public abstract void setReservations(Collection res);
    public abstract Collection getReservations( );

    // standard call back methods
    
    public void setEntityContext(EntityContext ec){}

    public void unsetEntityContext(){}
    public void ejbLoad(){}
    public void ejbStore(){}
    public void ejbActivate(){}
    public void ejbPassivate(){}
    public void ejbRemove() throws RemoveException  {}

}
