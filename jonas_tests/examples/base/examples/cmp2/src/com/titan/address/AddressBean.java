package com.titan.address;

import java.util.Collection;
import java.util.Set;

import javax.ejb.EntityContext;
import javax.ejb.FinderException;

import com.titan.customer.CustomerLocal;

public abstract class AddressBean implements javax.ejb.EntityBean {

    public Integer ejbCreateAddress(String street, String city, String state, String zip)
        throws javax.ejb.CreateException {
        setStreet(street);
        setCity(city);
        setState(state);
        setZip(zip);
        return null;
    }

    public void ejbPostCreateAddress(String street, String city, String state, String zip) {

    }

    // select methods
    public abstract Set ejbSelectZipCodes(String state) throws FinderException;

    public abstract Collection ejbSelectAll() throws FinderException;

    public abstract CustomerLocal ejbSelectCustomer(AddressLocal addr) throws FinderException;

    // Public Home method required to test the private ejbSelectZipCodes method
    public Collection ejbHomeSelectZipCodes(String state) throws FinderException {
        return this.ejbSelectZipCodes(state);
    }

    // Public Home method required to test the private ejbSelectCustomer method
    public CustomerLocal ejbHomeSelectCustomer(AddressLocal addr) throws FinderException {
        return (CustomerLocal) (this.ejbSelectCustomer(addr));
    }

    // persistent fields
    public abstract String getStreet();

    public abstract void setStreet(String street);

    public abstract String getCity();

    public abstract void setCity(String city);

    public abstract String getState();

    public abstract void setState(String state);

    public abstract String getZip();

    public abstract void setZip(String zip);

    // standard call back methods

    public void setEntityContext(EntityContext ec) {
    }

    public void unsetEntityContext() {
    }

    public void ejbLoad() {
    }

    public void ejbStore() {
    }

    public void ejbActivate() {
    }

    public void ejbPassivate() {
    }

    public void ejbRemove() throws javax.ejb.RemoveException {
    }

}
