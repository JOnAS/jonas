/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package xdoclet;

import java.rmi.RemoteException;
import javax.ejb.EntityBean;
import javax.ejb.EntityContext;
import javax.ejb.ObjectNotFoundException;
import javax.ejb.RemoveException;
import javax.ejb.CreateException;
import javax.ejb.EJBException;
import java.util.Date;
import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;
import javax.naming.InitialContext;
import javax.naming.NamingException;
/**
 * CustomerBean is an entity bean with "container managed persistence version 2".
 * The state of an instance is stored into a relational database.
 *
 * @ejb.bean
 *      name="CustomerBean"
 *      description="Deployment descriptor for the CustomerBean bean with CMP2 JOnAS example"
 *      type="CMP"
 *      view-type="both"
 *      primkey-field="id"
 *      jndi-name="CustomerBeanHome"
 *      cmp-version="2.x"
 *      reentrant="false"
 * @ejb.home
 *      remote-class="xdoclet.CustomerHomeRemote"
 *      local-class="xdoclet.CustomerHomeLocal"
 * @ejb.interface
 *      remote-class="xdoclet.CustomerRemote"
 *      local-class="xdoclet.CustomerLocal"
 * @ejb:pk
 *      class="java.lang.Integer"
 * @ejb.ejb-ref
 *	    ejb-name="PhoneBean"
 *	    view-type="local"
 *	    ref-name="ejb/PhoneHomeLocal"
 *
 * @jonas.jdbc-mapping
 *       jdbc-table-name = "MyCustomer"
 *
 * @ejb:finder
 *	     signature= "java.util.Collection findByName(java.lang.String lastName, java.lang.String firstName)"
 *	     query ="SELECT OBJECT(c) FROM CustomerBean c WHERE c.lastName LIKE ?1 AND c.firstName LIKE ?2"
 * @ejb:finder
 *	     signature= "java.util.Collection findAllCustomers()"
 *	     query ="SELECT OBJECT(c) FROM CustomerBean AS c"
 *
 * @jonas.session-timeout
 *       session-timeout="60"
 * @ejb:transaction
 *       type="Required"
 */
public abstract class CustomerBean implements EntityBean {

    // Keep the reference on the EntityContext
    protected EntityContext entityContext;

     /**
     * EJBCREATE
     * @return pk primary key set to null
     *
     * @ejb.create-method
     */
    public Integer ejbCreate(Integer id, Name name)
        throws CreateException {
        setLastName(name.getLastName());
        setFirstName(name.getFirstName());

	// Init object state
	this.setId(id);
	return null;
    }

    public void ejbPostCreate(Integer id, Name name) {}

    // business methods
    /**
     * @ejb.interface-method
     *
     **/
    public Name getName() {
        Name name = new Name(getLastName(),getFirstName());
        return name;
    }
    /**
     * @ejb.interface-method
     **/
    public void setName(Name name) {
        setLastName(name.getLastName());
        setFirstName(name.getFirstName());
    }


    /**
     * @ejb.interface-method
     **/
    public void addPhoneNumber(String number, byte type) throws NamingException, CreateException {
        InitialContext jndiEnc = new InitialContext();
        PhoneHomeLocal phoneHomeL = (PhoneHomeLocal) (jndiEnc.lookup("java:comp/env/ejb/PhoneHomeLocal"));

        PhoneLocal phone = phoneHomeL.create(number, type);
        Collection phoneNumbers = this.getPhoneNumber();
        phoneNumbers.add(phone);
    }

    /**
     * @ejb.interface-method
     **/
    public void removePhoneNumber(byte typeToRemove) {
        Collection phoneNumbers = this.getPhoneNumber();
        Iterator iterator = phoneNumbers.iterator();

        while (iterator.hasNext()) {
            PhoneLocal phone = (PhoneLocal) iterator.next();
            if (phone.getType() == typeToRemove) {
                phoneNumbers.remove(phone);
                try {
                    phone.remove();
                } catch (Exception e) {
                    System.out.println("Problem during removing phone");
                }
            }

        }
    }

    /**
     * @ejb.interface-method
     **/
    public void updatePhoneNumber(String number, byte typeToUpdate) {
        Collection phoneNumbers = this.getPhoneNumber();
        Iterator iterator = phoneNumbers.iterator();
        while (iterator.hasNext()) {
            PhoneLocal phone = (PhoneLocal) iterator.next();
            if (phone.getType() == typeToUpdate) {
                phone.setNumber(number);
                break;
            }
        }
    }

    /**
     * @ejb.interface-method
     **/
    public Vector getPhoneList() {

        Vector vv = new Vector();
        Collection phoneNumbers = this.getPhoneNumber();

        Iterator iterator = phoneNumbers.iterator();
        while (iterator.hasNext()) {
            PhoneLocal phone = (PhoneLocal) iterator.next();
            String ss = "Type=" + phone.getType() + "  Number=" + phone.getNumber();
            vv.add(ss);
        }
        return vv;
    }

   // persistent relationships
   /**
    * @ejb.relation
    * name="CUSTOMER-PHONE"
    * role-name="PHONE-has-CUSTOMER"
    * target-ejb="PhoneBean"
    * target-role-name="CUSTOMER-has-PHONE"
    *
    * @jonas.field-mapping
    *    foreign-key-jdbc-name="CUSTOMER_FK_PHONE"
    *    key-jdbc-name="CustomerId"
    **/
    public abstract Collection getPhoneNumber( );
    public abstract void setPhoneNumber(Collection phones);

    // abstract accessor methods
    /**
     * @ejb.persistent-field
     * @ejb.interface-method
     * @jonas.cmp-field-jdbc-mapping
     *	field-name="id"
     *	jdbc-field-name="CustomerId"
    */
     public abstract Integer getId();

    /**
     * @ejb.persistent-field
     * @ejb.interface-method
     */
    public abstract void setId(Integer id);
    /**
     * @ejb.persistent-field
     * @ejb.interface-method
     */
    public abstract String getLastName( );
    /**
     * @ejb.persistent-field
     * @ejb.interface-method
     */
    public abstract void setLastName(String lname);

    /**
     * @ejb.persistence
     * 		column-name="CustomerFirstName"
     * 		jdbc-type="VARCHAR"
     * @ejb.persistent-field
     * @ejb.interface-method
     */
    public abstract String getFirstName( );
    /**
     * @ejb.persistent-field
     * @ejb.interface-method
     */
    public abstract void setFirstName(String fname);

    // standard call back methods

    public void ejbActivate() {}
    public void ejbLoad() {}
    public void ejbPassivate() {}
    public void ejbRemove() throws RemoveException {}

    public void ejbStore() {}


    public void setEntityContext(EntityContext ctx) {

	// Keep the entity context in object
	entityContext = ctx;
    }

    public void unsetEntityContext()  {
	entityContext = null;
    }

}
