/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package xdoclet;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Hashtable;
import java.util.Properties;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.transaction.UserTransaction;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.rmi.PortableRemoteObject;
import xdoclet.CustomerHomeRemote;
import xdoclet.CustomerRemote;

/**
 * Sample for entity beans CMP2 xdoclet
 */

public class ClientCustomer {

    private static UserTransaction utx = null;

    private static void PrintAllCustomerAndPhones(CustomerHomeRemote h) {
        Iterator alist;
        CustomerRemote customer;
        String phone;
        try {
            utx.begin(); // faster if made inside a Tx
            alist = h.findAllCustomers().iterator();
            while (alist.hasNext()) {
                customer = (CustomerRemote) alist.next();
                System.out.println("Customer name is :" + customer.getLastName() + " " + customer.getFirstName());
                Iterator phonelist = customer.getPhoneList().iterator();
                while (phonelist.hasNext()) {
                    phone = (String) phonelist.next();
                    System.out.println("Phone number is: " + phone);
                }
            }
            utx.commit();
        } catch (Exception e) {
            System.err.println("Exception getting all Customer phone list: " + e);
            System.exit(2);
        }
    }

    public static void main(String[] args) {

        String beanName = "CustomerBeanHome";

        // get JNDI initial context
        Context initialContext = null;
        try {
            initialContext = new InitialContext();
        } catch (Exception e) {
            System.err.println("Cannot get initial context for JNDI: " + e);
            System.exit(2);
        }

        // We want to start transactions from client: get UserTransaction
        System.out.println("Getting a UserTransaction object from JNDI");
        try {

            // Comment the following lines if you want to use a David Client:
            utx = (UserTransaction) initialContext.lookup("javax.transaction.UserTransaction");

        } catch (Exception e) {
            System.err.println("Cannot lookup UserTransaction: " + e);
            System.exit(2);
        }

        // Connecting to Home thru JNDI
        System.out.println("Connecting to the CustomerHome");
        CustomerHomeRemote home = null;
        try {
            home = (CustomerHomeRemote) PortableRemoteObject.narrow(initialContext.lookup(beanName),
                    CustomerHomeRemote.class);
        } catch (Exception e) {
            System.err.println("Cannot lookup " + beanName + ": " + e);
            System.exit(2);
        }

        // List existing customer
        System.out.println("Getting the list of existing Customer in database");
        PrintAllCustomerAndPhones(home);

        // Create a first customer
        System.out.println("Creating a new customer in database");
        CustomerRemote c1 = null;
        try {
            c1 = home.create(new Integer(1), new Name("jerome", "camilleri"));
        } catch (Exception e) {
            System.err.println("Cannot create Customer: " + e);
            System.exit(2);
        }

        // Add phone number
        try {
            c1.addPhoneNumber("0476254717", (byte) 0);
            c1.addPhoneNumber("0625785511", (byte) 0);
        } catch (Exception e) {
            System.out.println("Problem during add phone number");
            e.printStackTrace(System.out);
        }
        // List existing customer
        System.out.println("Getting the list of customer with Phone number in database");
        PrintAllCustomerAndPhones(home);

        // Delete Account2
        System.out.println("Removing Customer and phone previously created in database");
        try {
            c1.removePhoneNumber((byte) 0);
            c1.remove();
        } catch (Exception e) {
            System.err.println("exception during remove: " + e);
            System.exit(2);
        }

        // Exit program
        System.out.println("ClientCustomer terminated");
    }

}

