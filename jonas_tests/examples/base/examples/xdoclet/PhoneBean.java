/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package xdoclet;

import javax.ejb.EntityContext;
import java.util.Date;
import javax.naming.InitialContext;
import javax.naming.NamingException;
/**
 * PhoneBean is an entity bean with "container managed persistence version 2".
 * The state of an instance is stored into a relational database. 
 *
 * @ejb.bean
 *      name="PhoneBean" 
 *      description="Deployment descriptor for the PhoneBean bean with CMP2 JOnAS example"
 *      type="CMP"
 *      view-type="local"
 *      jndi-name="PhoneBeanHome"
 *      cmp-version="2.x"
 *      reentrant="false"
 * @ejb.home
 *	    local-class="xdoclet.PhoneHomeLocal"
 * @ejb.interface
 *      local-class="xdoclet.PhoneLocal"
 * @ejb:pk
 *      class="java.lang.Object"
 * @jonas.jdbc-mapping
 *      automatic-pk-field-name="pkphone"
 * @ejb:finder
 *	signature= "java.util.Collection findAllPhones()"
 *	query ="SELECT OBJECT(o) FROM PhoneBean o"
 * @jonas.session-timeout
 *       session-timeout="60"
 * @ejb:transaction type="Required"
 */

public abstract class PhoneBean implements javax.ejb.EntityBean {
	
    /**
     * EJBCREATE
     * @return pk primary key set to null
     *
     * @ejb.create-method
     */
    public Object ejbCreate( String number, byte type) throws javax.ejb.CreateException {
	setNumber(number);
	setType(type);
	return null;
    }

    public void ejbPostCreate(String number, byte type) {
	System.out.println("PhoneBean ejbPostCreate number= "+number);
    }

    	
    
    /**
     * @ejb.persistent-field
     * @ejb.interface-method
     * @jonas.cmp-field-jdbc-mapping
     *	field-name="number"
     *	jdbc-field-name="PhoneNumber"  
     */
    public abstract String getNumber();
    /**
     * @ejb.persistent-field
     * @ejb.interface-method
     */
    public abstract void setNumber(String number);
    /**
     * @ejb.persistent-field
     * @ejb.interface-method
     */
    public abstract byte getType();
   /**
     * @ejb.persistent-field
     * @ejb.interface-method
    */
    public abstract void setType(byte type);

    // standard call back methods
    
    public void setEntityContext(EntityContext ec){}
    public void unsetEntityContext(){}
    public void ejbLoad(){}
    public void ejbStore(){}
    public void ejbActivate(){}
    public void ejbPassivate(){}
    public void ejbRemove() throws javax.ejb.RemoveException{}

}
