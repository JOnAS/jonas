/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS Team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.alarm.beans;

import java.rmi.RemoteException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

/**
 * proxy used to access the "View" session bean. must be instantiated by JSP's
 * with a tag like this: <jsp:useBean id="myview" scope=session
 * class=org.objectweb.alarm.beans.ViewProxy >
 */
public class ViewProxy {

/**
 * Initial context
 */
    private Context ictx = null;

    /**
     * Home of the bean View
     */
    private ViewHome vh = null;

    /**
     * Bean View
     */
    private View ejbview = null;

    /**
     * Name of my profil
     */
    private String myprofil = null;

    /**
     * Error message
     */
    private String errorMessage = null;

    private View getEjbView() {
        if (ejbview == null) {
            // Create the Session Bean
            try {
                ejbview = vh.create();
            } catch (Exception e) {
                errorMessage = "ViewProxy : Cannot create EJB:" + e.toString();
            }
        }
        return ejbview;
    }

    /**
     * Init procedure
     * Gets the bean and create one
     */
    private void init() {
        // Get the ViewHome
        try {
            ictx = new InitialContext();
            vh = (ViewHome) PortableRemoteObject.narrow(ictx.lookup("viewhome"), ViewHome.class);
        } catch (NamingException e) {
            errorMessage = "ViewProxy : Cannot get ViewHome:" + e.toString();
            return;
        }
        getEjbView();
    }

    /**
     * constructor: Create here the EJB session.
     */
    public ViewProxy() {
        init();
    }

    /**
     * @return RO attribute: Get all Profils already created by Alarm Manager.
     */
    public String[] getProfils() {
        String[] ret = new String[0];
        try {
            ret = getEjbView().getProfils();
            errorMessage = null;
        } catch (RemoteException e) {
            errorMessage = "ViewProxy : Cannot get Profil list:" + e.toString();
            ejbview = null;
        }
        return ret;
    }

    /**
     * @return RW attribute: Profil used.
     */
    public String getProfil() {
        errorMessage = null;
        return myprofil;
    }

    /**
     * Set the current profil
     * @param p the name of the profil
     */
    public void setProfil(String p) {
        try {
            getEjbView().setProfil(p);
            errorMessage = null;
            // keep it on local for efficiency
            myprofil = p;
        } catch (RemoteException e) {
            errorMessage = "ViewProxy : Cannot set Profil:" + e.toString();
            ejbview = null;
        }
    }

    /**
     * @return RO attribute: list of new alarms on the current Profil
     */
    public AlarmData[] getNewAlarms() {
        AlarmData[] ret = null;
        try {
            ret = getEjbView().getNewAlarms();
            errorMessage = null;
        } catch (RemoteException e) {
            errorMessage = "ViewProxy : Cannot get new alarms:" + e.toString();
            ejbview = null;
        }
        return ret;
    }

    /**
     * @return RO attribute: list of all alarms on the current Profil
     */
    public AlarmData[] getAllAlarms() {
        AlarmData[] ret = null;
        try {
            ret = getEjbView().getAllAlarms();
            errorMessage = null;
        } catch (RemoteException e) {
            errorMessage = "ViewProxy : Cannot get all alarms:" + e.toString();
            ejbview = null;
        }
        return ret;
    }

    /**
     * @param profilName the name of the profil
     * @return the maximum level of alarm for this Profil.
     */
    public int alarmLevel(String profilName) {
        int level = 0;
        try {
            level = getEjbView().alarmLevel(profilName);
            errorMessage = null;
        } catch (RemoteException e) {
            errorMessage = "ViewProxy : Cannot get AlarmLevel:" + e.toString();
            ejbview = null;
        }
        return level;
    }

    /**
     * @return error message if any
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Forget this Alarm because it has been treated.
     * @param pk primary key
     */
    public void forgetAlarm(String pk) {
        try {
            getEjbView().forgetAlarm(pk);
            errorMessage = null;
        } catch (RemoteException e) {
            errorMessage = "ViewProxy : Cannot forget Alarm:" + e.toString();
            ejbview = null;
        }
    }

    /**
     * Creates a new Profil
     * @param device the name of the device
     * @param level the severity level
     * @return the newly created profil
     */
    public String newProfil(String device, String level) {
        String ret = null;
        try {
            ret = getEjbView().newProfil(device, level);
            errorMessage = null;
        } catch (RemoteException e) {
            errorMessage = "ViewProxy : Cannot create Profil:" + e.toString();
            ejbview = null;
        }
        return ret;
    }

    /**
     * remove a Profil
     * @param profil name of the profil to remove
     */
    public void removeProfil(String profil) {
        try {
            getEjbView().removeProfil(profil);
            errorMessage = null;
        } catch (RemoteException e) {
            errorMessage = "ViewProxy : Cannot remove Profil:" + e.toString();
            ejbview = null;
        }
    }
}