/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2005 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS Team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.alarm.beans;

import java.rmi.RemoteException;
import java.util.Collection;

import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

/**
 * Session bean View. Does not implement SessionSynchronization. We assume that
 * this bean and AlarmManager run in the same JVM. Each Session bean is
 * dedicated to 1 client: Here, the ViewProxy class. Each Session may be
 * connected to 1 Profil or not.
 */
public class ViewBean implements SessionBean {

    /**
     * Reference to the alarm manager
     */
    private static transient AlarmManager alarmManager = null;

    /**
     * Profil reference
     */
    private Profil prof = null;

    /**
     * Set the context for this bean
     * @param ctx the context
     * @see javax.ejb.SessionBean#setSessionContext(javax.ejb.SessionContext)
     */
    public void setSessionContext(SessionContext ctx) {

    }

    /**
     * Remove
     * @see javax.ejb.SessionBean#ejbRemove()
     */
    public void ejbRemove() {
    }

    /**
     * The Session bean must define 1 or more ejbCreate methods.
     */
    public void ejbCreate() {
        // Get a reference on the AlarmManager.
        alarmManager = AlarmManager.getInstance();
    }

    /**
     * Passivate
     * @see javax.ejb.SessionBean#ejbPassivate()
     */
    public void ejbPassivate() {
    }

    /**
     * Activate
     * @see javax.ejb.SessionBean#ejbActivate()
     */
    public void ejbActivate() {
    }

    /**
     * @return all messages for the current profil
     * @throws RemoteException if remote call failed
     */
    public AlarmData[] getAllAlarms() throws RemoteException {
        if (prof == null) {
            throw new RemoteException("No Profil defined for this session");
        }
        Collection alist = prof.getAlarms(true);
        return (AlarmData[]) alist.toArray(new AlarmData[0]);
    }

    /**
     * @return new messages for the current profil
     * @throws RemoteException if remote call failed
     */
    public AlarmData[] getNewAlarms() throws RemoteException {
        if (prof == null) {
            throw new RemoteException("No Profil defined for this session");
        }
        Collection alist = prof.getAlarms(false);
        return (AlarmData[]) alist.toArray(new AlarmData[0]);
    }

    /**
     * @return an enumeration of all available profils
     */
    public String[] getProfils() {
        return alarmManager.getProfilNames();
    }

    /**
     * Chooses to work on a particular profil.
     * @param name of the profil
     * @throws RemoteException if remote call failed
     */
    public void setProfil(String name) throws RemoteException {
        if (name == null) {
            prof = null;
            return;
        }
        prof = alarmManager.getProfil(name);
        if (prof == null) {
            throw new RemoteException("This Profil does not exist yet: " + name);
        }
    }

    /**
     * @return the Alarm Level for Profil specified
     * @param name of the profil
     * @throws RemoteException if remote call failed
     */
    public int alarmLevel(String name) throws RemoteException {
        Profil prof = alarmManager.getProfil(name);
        if (prof == null) {
            throw new RemoteException("This Profil does not exist yet: " + name);
        }
        return prof.getCurrentLevel();
    }

    /**
     * Forget this message because problem has been taken into account.
     * @param pk primary key
     * @throws RemoteException if remote call failed
     */
    public void forgetAlarm(String pk) throws RemoteException {
        alarmManager.forgetAlarm(pk);
    }

    /**
     * creates a new Profil
     * @param device the name of the device
     * @param level the severity level
     * @return the newly created profil
     * @throws RemoteException if remote call failed
     */
    public String newProfil(String device, String level) throws RemoteException {

        // Check arg validity
        if (device.length() == 0) {
            throw new RemoteException("null device string");
        }
        if (!level.startsWith("S") && !level.startsWith("W") && !level.startsWith("I")) {
            throw new RemoteException("severity must be one of S|W|I");
        }

        prof = alarmManager.newProfil(device, level);
        if (prof == null) {
            return null;
        }
        return prof.getName();
    }

    /**
     * remove a Profil
     * @param name the name of the profil
     * @throws RemoteException if remote call failed
     */
    public void removeProfil(String name) throws RemoteException {
        if (name == null) {
            return;
        }
        boolean ok = alarmManager.delProfil(name);
        if (!ok) {
            throw new RemoteException("This Profil does not exist yet: " + name);
        }
    }
}
