/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS Team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.alarm.beans;

import java.rmi.RemoteException;

import javax.ejb.EJBObject;

/**
 * View remote interface
 */
public interface View extends EJBObject {

    /**
     * @return
     * @throws RemoteException
     */
    AlarmData[] getAllAlarms() throws RemoteException;

    /**
     * @return
     * @throws RemoteException
     */
    AlarmData[] getNewAlarms() throws RemoteException;

    /**
     * @return
     * @throws RemoteException
     */
    String[] getProfils() throws RemoteException;

    /**
     * @param name
     * @throws RemoteException
     */
    void setProfil(String name) throws RemoteException;

    /**
     * @param name
     * @return
     * @throws RemoteException
     */
    int alarmLevel(String name) throws RemoteException;

    /**
     * @param pk
     * @throws RemoteException
     */
    void forgetAlarm(String pk) throws RemoteException;

    /**
     * @param device
     * @param level
     * @return
     * @throws RemoteException
     */
    String newProfil(String device, String level) throws RemoteException;

    /**
     * @param name
     * @throws RemoteException
     */
    void removeProfil(String name) throws RemoteException;
}