/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2005 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS Team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.alarm.beans;

import java.io.Serializable;

/**
 *
 * @author Florent Benoit
 */
/**
 * Data associated with an AlarmRecord. This is what will be passed to servlets
 * to be displayed.
 */
public class AlarmData implements Serializable {

    /**
     * AlarmRecord number (=pk)
     */
    private int num;

    /**
     * Severity Level
     */
    private int sev;

    /**
     * Device that generated Alarm
     */
    private String device;

    /**
     * Alarm Message
     */
    private String message;

    /**
     * Count of received messages
     */
    private int count;

    /**
     * State (1=received, 2=processed, 3=deleted)
     */
    private int state;

    /**
     * Date of first message
     */
    private java.sql.Date date;

    /**
     * @param n AlarmRecord number
     * @param s Severity Level
     * @param dev Device that generated Alarm
     * @param m Alarm Message
     * @param d Date of first message
     */
    public AlarmData(int n, int s, String dev, String m, java.sql.Date d) {
        num = n;
        sev = s;
        device = dev;
        message = m;
        count = 1;
        state = 1;
        date = d;
    }

    /**
     * @return the alarm record number
     */
    public int getNum() {
        return num;
    }

    /**
     * @return Severity Level
     */
    public int getSev() {
        return sev;
    }

    /**
     * @return the device
     */
    public String getDevice() {
        return device;
    }

    /**
     * @return the alarm message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @return nb of messages received
     */
    public int getCount() {
        return count;
    }

    /**
     * Set the nb of messages received
     * @param c Count of received messages
     */
    public void setCount(int c) {
        count = c;
    }

    /**
     * @return the state
     */
    public int getState() {
        return state;
    }

    /**
     * Set the current state
     * @param s State (1=received, 2=processed, 3=deleted)
     */
    public void setState(int s) {
        state = s;
    }

    /**
     * @return Date of first message
     */
    public java.util.Date getDate() {
        return date;
    }
}
