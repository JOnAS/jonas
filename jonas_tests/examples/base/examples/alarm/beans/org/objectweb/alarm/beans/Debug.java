/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS Team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.alarm.beans;

/**
 * Provide a ligth Debug.
 */
public class Debug {

    /**
     * No public constructor (Utility class)
     */
    private Debug() {

    }

    /**
     * True if only if use the debug.
     */
    private static boolean debug = false;

    /**
     * Log some debugging info, if debug is true.
     * @param msg the message to log.
     */
    public static void log(String msg) {
        if (debug) {
            System.out.println(msg);
        }
    }

    /**
     * Log some errors.
     * @param msg the error message to log.
     */
    public static void logError(String msg) {
        System.err.println(msg);
    }

}