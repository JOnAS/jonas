/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2005 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS Team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.alarm.beans;

import javax.ejb.EJBException;
import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;

/**
 *
 */
public class AlarmListenerBean implements MessageDrivenBean, MessageListener {

    /**
     * Reference to the alarm manager
     */
    private static transient AlarmManager alarmManager = null;

    /**
     * Default constructor
     */
    public AlarmListenerBean() {
    }

    /**
     * Set the associated context. The container call this method after the
     * instance creation. The enterprise Bean instance should store the
     * reference to the context object in an instance variable. This method is
     * called with no transaction context.
     * @param ctx A MessageDrivenContext interface for the
     *        instance.
     */

    public void setMessageDrivenContext(MessageDrivenContext ctx) {
    }

    /**
     * A container invokes this method before it ends the life of the
     * message-driven object. This happens when a container decides to terminate
     * the message-driven object. This method is called with no transaction
     * context.
     * @throws EJBException Thrown by the method to indicate a failure caused by
     *         a system-level error.
     */
    public void ejbRemove() throws EJBException {
    }

    /**
     * The Message driven bean must define an ejbCreate methods with no args. We
     * can do here all operations that will be common to all messages, i.e.
     * asking JNDI for bean homes, ...
     */
    public void ejbCreate() {
        // Get a reference on the AlarmManager.
        alarmManager = AlarmManager.getInstance();

    }

    /**
     * Called when there is a messgae
     * @param message the message
     * @see javax.jms.MessageListener#onMessage(javax.jms.Message)
     */
    public void onMessage(Message message) {

        int sev = 0;
        String from = null;
        String reason = null;

        // Decode the message (MapMessage)
        MapMessage msg = (MapMessage) message;
        try {
            sev = msg.getInt("Severity");
            from = msg.getString("From");
            reason = msg.getString("Reason");
        } catch (JMSException e) {
            Debug.logError("AlarmListenerBean exception:" + e);
        }
        // Give all messages to the AlarmServiceHelper.
        alarmManager.alarm(sev, from, reason);

    }
}
