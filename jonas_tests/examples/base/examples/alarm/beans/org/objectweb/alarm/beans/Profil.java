/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2005 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS Team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.alarm.beans;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import javax.ejb.FinderException;
import javax.rmi.PortableRemoteObject;


/**
 * Helper class Profil Profils are managed by AlarmManager and used by View
 * session beans We do not use an Entity because we need neither persistence nor
 * remote accesses. We cannot use a Session bean because it's accessed from
 * several clients, and from the AlarmManager.
 */
public class Profil {

    /**
     * Device name or "all"
     */
    private String device;

    /**
     * max severity level
     */
    private int maxsev; //

    /**
     * Name
     */
    private String name;

    /**
     * Is the device is all ?
     */
    private boolean fromAll;

    /**
     * Home of the bean AlarmRecord
     */
    private AlarmRecordHome arh;

    /**
     * A list of AlarmRecord objects
     */
    private LinkedList alarmList = new LinkedList();

    /**
     * constructor
     * @param device the device name
     * @param sev the severity level
     * @param arh the home of the eban
     */

    public Profil(String device, String sev, AlarmRecordHome arh) {

        // init object
        this.device = device;
        this.arh = arh;
        fromAll = device.equals("all");
        if (sev.startsWith("S")) {
            maxsev = 1;
            sev = "S";
        } else if (sev.startsWith("W")) {
            maxsev = 2;
            sev = "W";
        } else {
            maxsev = 3;
            sev = "I";
        }
        name = device + "-" + sev;

        // find alarms already arrived and matching this Profil
        Collection knal = null;
        try {
            if (fromAll) {
                knal = arh.findBySeverity(maxsev);
            } else {
                knal = arh.findByInterest(device, maxsev);
            }
            alarmList.addAll(knal);
        } catch (FinderException e) {
            System.out.println("Profil constructor: No Alarm found");
        } catch (RemoteException e) {
            System.out.println("Error getting AlarmRecords:" + e);
        }
    }

    /**
     * @return the device
     */
    public String getDevice() {
        return device;
    }

    /**
     * @return the severity watched
     */
    public String getSeverity() {
        switch (maxsev) {
            case 1:
                return "S";
            case 2:
                return "W";
            case 3:
                return "I";
            default:
                return "A";
        }
    }

    /**
     * @return the profil name
     */
    public String getName() {
        return name;
    }

    /**
     * get Alarms for this Profil.
     * @param  all true if get all alarms, false if get only new alarms
     * @return Alarms for this Profil.
     */
     public synchronized Collection getAlarms(boolean all) {
        LinkedList ret = new LinkedList();
        Iterator it = alarmList.iterator();
        while (it.hasNext()) {
            AlarmRecord arec = (AlarmRecord) PortableRemoteObject.narrow(it.next(), AlarmRecord.class);
            AlarmData ad = null;
            try {
                ad = arec.getAlarmData();
                if (all || (ad.getState() == 1)) {
                    ret.add(ad);
                }
            } catch (RemoteException e) {
                System.out.println("Error getting AlarmRecord:" + e);
            }
        }
        return ret;
    }

    /**
     * Gets current Alarm level
     * @return current Alarm level
     */
     public synchronized int getCurrentLevel() {
        int ret = 1000;
        Iterator it = alarmList.iterator();
        while (it.hasNext()) {
            AlarmRecord arec = (AlarmRecord) PortableRemoteObject.narrow(it.next(), AlarmRecord.class);
            try {
                AlarmData ad = arec.getAlarmData();
                if (ad.getState() == 1 && ad.getSev() <= ret) {
                    ret = ad.getSev();
                }
            } catch (RemoteException e) {
                System.out.println("Error getting AlarmRecord:" + e);
            }
        }
        return ret;
    }

    /**
     * @param ad alarm data object
     * @return true if this Profil is interested by this AlarmRecord.
     */
    public boolean interestedBy(AlarmData ad) {
        return ((fromAll || ad.getDevice().equals(device)) && ad.getSev() <= maxsev);
    }

    /**
     * add an Alarm Record to this Profil.
     * @param arec the record
     */
     public synchronized void noticeAlarm(AlarmRecord arec) {
        alarmList.add(arec);
    }
}