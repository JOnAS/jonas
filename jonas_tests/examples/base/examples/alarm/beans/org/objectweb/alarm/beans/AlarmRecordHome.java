/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS Team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.alarm.beans;

import java.rmi.RemoteException;
import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBHome;
import javax.ejb.FinderException;

/**
 * Home interface for the bean AlarmRecord
 */
public interface AlarmRecordHome extends EJBHome {

    /**
     * @return
     * @throws CreateException
     * @throws RemoteException
     */
    AlarmRecord create() throws CreateException, RemoteException;

    /**
     * @param ad
     * @return
     * @throws CreateException
     * @throws RemoteException
     */
    AlarmRecord create(AlarmData ad) throws CreateException, RemoteException;

    /**
     * @param pk
     * @return
     * @throws FinderException
     * @throws RemoteException
     */
    AlarmRecord findByPrimaryKey(String pk) throws FinderException, RemoteException;

    /**
     * @param from
     * @param mess
     * @return
     * @throws FinderException
     * @throws RemoteException
     */
    AlarmRecord findAlarm(String from, String mess) throws FinderException, RemoteException;

    /**
     * @return
     * @throws FinderException
     * @throws RemoteException
     */
    Collection findAll() throws FinderException, RemoteException;

    /**
     * @param from
     * @param sev
     * @return
     * @throws FinderException
     * @throws RemoteException
     */
    Collection findByInterest(String from, int sev) throws FinderException, RemoteException;

    /**
     * @param sev
     * @return
     * @throws FinderException
     * @throws RemoteException
     */
    Collection findBySeverity(int sev) throws FinderException, RemoteException;
}