/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2005 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS Team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.alarm.beans;

import java.rmi.RemoteException;

import javax.ejb.EJBObject;

/**
 * AlarmRecord remote interface
 */
public interface AlarmRecord extends EJBObject {

    /**
     * Update the record
     * @param s severity level
     * @throws RemoteException if remote call failed
     */
    void update(int s) throws RemoteException;

    /**
     * @throws RemoteException if remote call failed
     */
    void forget() throws RemoteException;

    /**
     * @throws RemoteException if remote call failed
     */
    void setProcessed() throws RemoteException;

    /**
     * @return the sender
     * @throws RemoteException if remote call failed
     */
    String getFrom() throws RemoteException;

    /**
     * @return the severity level
     * @throws RemoteException if remote call failed
     */
    int getSeverity() throws RemoteException;

    /**
     * @return the number of messages
     * @throws RemoteException if remote call failed
     */
    int getCount() throws RemoteException;

    /**
     * @return number of alarms
     * @throws RemoteException if remote call failed
     */
    int getAlarmCount() throws RemoteException;

    /**
     * @return the new ident
     * @throws RemoteException if remote call failed
     */
    int getNewIdent() throws RemoteException;

    /**
     * @return the data
     * @throws RemoteException if remote call failed
     */
    AlarmData getAlarmData() throws RemoteException;
}
