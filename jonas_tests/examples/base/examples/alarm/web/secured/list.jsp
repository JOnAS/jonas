<html>
<head><title>list page</title></head>
<%@ include file="top.html" %>
<jsp:useBean id="myview" scope="session" class="org.objectweb.alarm.beans.ViewProxy" />
<%
	String error = myview.getErrorMessage();
	if (error != null) {
%>
<p>
An error occured in the JOnAS Server:
<p>
<%=error%>
<p>
<%
	}
%>
<%
	String[] plist = myview.getProfils();
%>
You have been authenticated by web container and you can access now the list of 
alarms received for each filter.
The color used shows the current level of alarm:
<strong>
<table border=1>
      <tr>
	<td bgcolor=#ff8080>SEVERE ERROR</td>
      </tr>
      <tr>
	<td bgcolor=#ffc080>WARNING</td>
      </tr>
      <tr>
	<td bgcolor=#ffff80>INFO</td>
      </tr>
      <tr>
	<td bgcolor=#80ff80>NO ALARM</td>
      </tr>
</table>
</strong>
<p>
<table cellpadding=12>
<tr>
<td valign=top bgcolor="#eeffee">
You can see the list of alarm received for a special filter by
choosing in the list here after.
<p>
<table border=1 cellpadding=8>
  <%
	for (int i = 0; i < plist.length; i++) {
	 String profil = plist[i];
  %>
<%
	switch (myview.alarmLevel(profil)) {
	case 1:
%>
<tr bgcolor=#ff8080>
<%
		break;
	case 2:
%>
<tr bgcolor=#ffc080>
<%
		break;
	case 3:
%>
<tr bgcolor=#ffff80>
<%
		break;
	default:
%>
<tr bgcolor=#80ff80>
<%
	}
%>
<td>
<%=profil%>
</td>
<td>
<a href=setfilter.jsp?profil=<%=profil%>>See details</a>
</td>
<td>
<a href=removefilter.jsp?profil=<%=profil%>>Remove filter</a>
</td>
</tr>
  <%
      	}
  %>
</table>
</td>
<td valign=top bgcolor="#eeeeff"> 
You can create a new filter by using
the following form.
<form action="createfilter.jsp" method=get>
<p>
device name: <input type=text name=device size=12>
<pre>
<input type=radio name=level value="S"> Only Severe Alarms
<input type=radio name=level value="W"> Warnings and Severe Alarms
<input type=radio name=level value="I" checked> All Alarm Messages
<p>
<input type=submit value=CREATE>
</form>
</td>
</tr>
</table>
<a href="/alarm/index.html">Return to index page</a>
</body>
</html>
