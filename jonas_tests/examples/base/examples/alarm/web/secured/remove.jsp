<html>
<head><title>remove page</title></head>
<body>
<jsp:useBean id="myview" scope="session" class="org.objectweb.alarm.beans.ViewProxy" />
<%
	String alarmPK = request.getParameter("alarm");
	myview.forgetAlarm(alarmPK);
%>
<jsp:forward page="current.jsp">
	<jsp:param name="listType" value="News"/>
</jsp:forward>
</body>
</html>
