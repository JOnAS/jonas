<html>
<head><title>current page</title></head>
<%@ include file="top.html" %>
<%@ page import="org.objectweb.alarm.beans.AlarmData" %>
<jsp:useBean id="myview" scope="session" class="org.objectweb.alarm.beans.ViewProxy" />
<%
	String error = myview.getErrorMessage();
	if (error != null) {
%>
<p>
An error occured in the JOnAS Server:
<%=error%>
<%
	}
%>
<%
	AlarmData[] adlist = null;
	boolean history = request.getParameter("listType").equals("History");
%>
<font+1>
<%
	if (history) {
		adlist = myview.getAllAlarms();
%>
		Log of all
<%
	} else {
		adlist = myview.getNewAlarms();
%>
		Current list of
<%
	}
%>
		alarms received on <%=myview.getProfil()%>
<font-1>
<strong>
<table border=1 cellpadding=8>
<tr>
<th>ident</th>
<th>date</th>
<th>from</th>
<th>message</th>
<th>count</th>
<th>state</th>
</tr>
<%
	for (int i = 0; i < adlist.length; i++) {
		AlarmData ad = adlist[i];
		int sev = ad.getSev();
		int state = ad.getState();
%>
<tr>
<%	switch (sev) {
	case 1:
%>
<td bgcolor=#ff8080>
<%
		break;
	case 2:
%>
<td bgcolor=#ffc080>
<%
		break;
	case 3:
%>
<td bgcolor=#ffff80>
<%
		break;
	default:
%>
<td>
<%
		break;
	}
%>
<%=ad.getNum()%></td>
<td><%=ad.getDate()%></td>
<td><%=ad.getDevice()%></td>
<td><%=ad.getMessage()%></td>
<td><%=ad.getCount()%></td>
<td>
<%	switch (state) {
	case 1:
%>
<a href="remove.jsp?alarm=<%=ad.getNum()%>">received</a>
<%
		break;
	case 2:
%>
processed
<%
		break;
	default:
%>
removed
<%
		break;
	}
%></td>
</tr>
<%
      	}
%>
</table>
</strong>
<p>
<%	if (history) {	%>
View only the <a href="current.jsp?listType=News">current list</a> of Alarms for this filter.
<%	} else {	%>
View the <a href="current.jsp?listType=History">full log</a> of Alarms received for this filter.
<%	}		%>
<p>
Return to the <a href="unsetfilter.jsp">list of filters</a>
</body>
</html>
