// "plain/text"
package org.objectweb.alarm;

import java.io.*;
import javax.jms.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

/**
 * This servlet is used to send Alarms on a JMS topic
 */
public class Sender extends HttpServlet {

    public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {

	res.setContentType("text/html");
        PrintWriter out = res.getWriter();
	String device = req.getParameter("device");
	String level = req.getParameter("level");
	String message = req.getParameter("message");
	String sloop = req.getParameter("number");
	String stimeout = req.getParameter("timeout");
	int severity;
	String error = "";

	out.write("<html><head><title>Alarm Sender Servlet</title></head><body>");
	if (level.startsWith("S")) {
	    severity = 1;
	} else if (level.startsWith("W")) {
	    severity = 2;
	} else {
	    severity = 3;
	}
	int loop = (new Integer(sloop)).intValue();
	int timeout = (new Integer(stimeout)).intValue();
	
	Context ictx = null;
	try {
	    ictx = new InitialContext();
	} catch (Exception e) {
	    error += "Cannot get initial context:"+e;
	}

	// Lookup JMS resources
	TopicConnectionFactory tcf = null;
	Topic mytopic = null;
	TopicConnection mytc = null;
	try {
	    // lookup the TopicConnectionFactory through its JNDI name
	    tcf = (TopicConnectionFactory) ictx.lookup("java:comp/env/jms/TopicFactory");
	    // lookup the Topic through its JNDI name
	    mytopic = (Topic) ictx.lookup("java:comp/env/jms/AlarmTopic");
	} catch (NamingException e) {
	    error += "Cannot lookup JMS Resources:"+ e;
	}

	// Create Connection
	try {
	    mytc = tcf.createTopicConnection();
	 } catch (Exception e) {
	    error += "Cannot create JMS Connection:"+ e;
	}

	// Create Session + Publisher
	TopicSession session = null;
	TopicPublisher tp = null;
	try {
	    session = mytc.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);	
	    tp = session.createPublisher(mytopic);
	 } catch (Exception e) {
	    error += "Cannot create JMS Publisher:"+ e;
	}

	// publish messages to the topic
	try {
	    for (int i = 0 ; i < loop; i++) {
		if (timeout > 0) {
		    Thread.currentThread().sleep(1000*timeout);
		}
		MapMessage mess = session.createMapMessage();
		mess.setInt("Severity", severity);
		mess.setString("From", device);
		mess.setString("Reason", message);
		tp.publish(mess);
	    }
	} catch (JMSException e) {
	    String l = e.getLinkedException().toString();
	    error += " Exception while creating or sending message: "+ e;
	    error += " Linked Exception: "+l;
	} catch (java.lang.InterruptedException e) {
	    out.println("Alarm Generator Interrupted");
	}

	if (error.equals("")) {
	    out.println("Message sent");
	} else {
	    out.println(error);
	}
	out.println("<p>return to <a href=\"../generator.html\">Alarm Generator</a>");
	out.println("<p>go to <a href=\"list.jsp\">Alarm List</a>");

	// close connection
	try {
	    mytc.close();
	} catch (Exception e) {
	}

	out.println("</body>");
	out.println("</html>");
    }
}
