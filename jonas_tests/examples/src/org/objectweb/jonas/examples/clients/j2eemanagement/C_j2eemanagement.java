/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.examples.clients.j2eemanagement;

import junit.framework.TestSuite;

import org.objectweb.jonas.examples.util.JExampleTestCase;

import com.meterware.httpunit.WebResponse;

/**
 * Define a class to test the j2eemanagement example
 * Test authentication, and check the sample is ok
 * @author Adriana Danes
 */
public class C_j2eemanagement extends JExampleTestCase {

    /**
     * URL of the earsample page
     */
    private static final String URL_J2EEMANAGEMENT_DOMAIN = "/j2eemanagement/mgmt?domainName=jonas";

    /**
     * Main method
     * @param args the arguments
     */
    public static void main(String[] args) {

        String testtorun = null;
        // Get args
        for (int argn = 0; argn < args.length; argn++) {
            String sArg = args[argn];
            if (sArg.equals("-n")) {
                testtorun = args[++argn];
            }
        }

        if (testtorun == null) {
            junit.textui.TestRunner.run(suite());
        } else {
            junit.textui.TestRunner.run(new C_j2eemanagement(testtorun));
        }
    }

    /**
     * Get a new TestSuite for this class
     * @return a new TestSuite for this class
     */
    public static TestSuite suite() {
        return new TestSuite(C_j2eemanagement.class);
    }

    /**
     * Setup need for these tests
     * earsample is required
     * @throws Exception if it fails
     */
    protected void setUp() throws Exception {
        super.setUp();
        useEar("j2eemanagement");
    }

    /**
     * Constructor with a specified name
     * @param s name
     */
    public C_j2eemanagement(String s) {
        // Need to use login/password because the web pages we want to access are secured
        super(s, URL_J2EEMANAGEMENT_DOMAIN, "jonas", "jonas");
    }

    /**
     * Check if the sample is OK. Analyze response
     * @throws Exception if an error occurs
     */
    public void testIsSampleOk() throws Exception {
            WebResponse wr = wc.getResponse(url);
            String txt = wr.getText();

            if (txt.indexOf("Application is OK") == -1) {
                fail("The example was not ok : " + txt);
            }
    }


}
