/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.examples.clients.earsample;

import junit.framework.TestSuite;

import org.objectweb.jonas.examples.util.JExampleTestCase;

import com.meterware.httpunit.AuthorizationRequiredException;
import com.meterware.httpunit.WebResponse;

/**
 * Define a class to test the Alarm example
 * Test authentication, and check the sample is ok
 * @author Florent Benoit
 */
public class C_earsample extends JExampleTestCase {

    /**
     * URL of the earsample page
     */
    private static final String URL_EARSAMPLE = "/earsample/secured/Op";

    /**
     * Main method
     * @param args the arguments
     */
    public static void main(String[] args) {

        String testtorun = null;
        // Get args
        for (int argn = 0; argn < args.length; argn++) {
            String sArg = args[argn];
            if (sArg.equals("-n")) {
                testtorun = args[++argn];
            }
        }

        if (testtorun == null) {
            junit.textui.TestRunner.run(suite());
        } else {
            junit.textui.TestRunner.run(new C_earsample(testtorun));
        }
    }

    /**
     * Get a new TestSuite for this class
     * @return a new TestSuite for this class
     */
    public static TestSuite suite() {
        return new TestSuite(C_earsample.class);
    }

    /**
     * Setup need for these tests
     * earsample is required
     * @throws Exception if it fails
     */
    protected void setUp() throws Exception {
        super.setUp();
        useEar("earsample");
    }

    /**
     * Constructor with a specified name
     * @param s name
     */
    public C_earsample(String s) {
        super(s, URL_EARSAMPLE);
    }

    /**
     * Try to log in without authentication
     * @throws Exception if an error occurs
     */
    public void testTryWithoutAuth() throws Exception {
        try {
            WebResponse wr = wc.getResponse(url);
            fail("earsample is not protected");
        } catch (AuthorizationRequiredException e) {
            ;
        }
    }

    /**
     * Try to authenticate with a bad login/password
     * @throws Exception if an error occurs
     */
    public void testTryWithBadAuth() throws Exception {
        try {
            wc.setAuthorization("bad", "bad");
            WebResponse wr = wc.getResponse(url);
            fail("earsample is not protected");
        } catch (AuthorizationRequiredException e) {
            ;
        }
    }

     /**
     * Try to authenticate with a right login/password (jetty)
     * @throws Exception if an error occurs
     */
   public void testTryWithJettyAuth() throws Exception {
        try {
            wc.setAuthorization("jetty", "jetty");
            WebResponse wr = wc.getResponse(url);
        } catch (AuthorizationRequiredException e) {
            fail("earsample don't accept l/p jetty/jetty");
        }
    }

    /**
     * Try to authenticate with a right login/password (tomcat)
     * @throws Exception if an error occurs
     */
    public void testTryWithTomcatAuth() throws Exception {
        try {
            wc.setAuthorization("tomcat", "tomcat");
            WebResponse wr = wc.getResponse(url);
        } catch (AuthorizationRequiredException e) {
            fail("earsample don't accept l/p tomcat/tomcat");
        }
    }

    /**
     * Check if the sample is OK. Analyze response
     * @throws Exception if an error occurs
     */
    public void testIsSampleOk() throws Exception {
        try {
            wc.setAuthorization("tomcat", "tomcat");
            WebResponse wr = wc.getResponse(url);
            String txt = wr.getText();

            if (txt.indexOf("<strong>Sample is OK.</strong>") == -1) {
                fail("The example was not ok : " + txt);
            }

        } catch (AuthorizationRequiredException e) {
            fail("Authentication failed");
        }
    }


}
