/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.examples.clients.eb;

import java.lang.reflect.InvocationTargetException;
import junit.framework.*;

import org.objectweb.jonas.examples.util.JExampleTestCase;
import org.objectweb.jonas.examples.util.JPrintStream;
import org.objectweb.jonas.examples.util.NoExitSecurityManager;

/**
 * Define a class to test the eb example
 * Test the client of eb example (Check if the text is "ClientAccount terminated")
 *  - test case of AccountImplHome
 *  - test case of AccountExplHome
 * @author Florent Benoit
 */
public class C_eb extends JExampleTestCase {


    /**
     * Class of the eb.client
     */
    private static final String CLIENT_CLASS = "eb.ClientAccount";

    /**
     * Text to check
     */
    private static final String CLIENT_OK_TXT = "ClientAccount terminated" ;

    /**
     * Impl EJB1.x
     */
    private static final String ACC_IMPL = "AccountImplHome";

    /**
     * Impl EJB2.x
     */
    private static final String ACC_IMPL2 = "AccountImpl2Home";
    /**
     * Expl
     */
    private static final String ACC_EXPL = "AccountExplHome";



    /**
     * Main method
     * @param args the arguments
     */
    public static void main(String[] args) {

        String testtorun = null;
        // Get args
        for (int argn = 0; argn < args.length; argn++) {
            String sArg = args[argn];
            if (sArg.equals("-n")) {
                testtorun = args[++argn];
            }
        }

        if (testtorun == null) {
            junit.textui.TestRunner.run(suite());
        } else {
            junit.textui.TestRunner.run(new C_eb(testtorun));
        }
    }

    /**
     * Get a new TestSuite for this class
     * @return a new TestSuite for this class
     */
    public static TestSuite suite() {
        return new TestSuite(C_eb.class);
    }

    /**
     * Setup need for these tests
     * eb is required
     * @throws Exception if it fails
     */
    protected void setUp() throws Exception {
        super.setUp();
        useBeans("eb");
    }

    /**
     * Step to do at the end
     * unload eb
     * @throws Exception if it fails
     */
    protected void tearDown() throws Exception {
        super.tearDown();
        unUseBeans("eb");
    }

    /**
     * Constructor with a specified name
     * @param s name
     */
    public C_eb(String s) {
        super(s);
    }

    /**
     * Test the return text of the example eb
     * by using a specific mode (Impl or Expl)
     * @param mode Can be AccountImplHome or AccountExplHome
     */
    private void doTestEb(String mode) {
        JPrintStream jPrintStream = new JPrintStream(System.out);
        System.setOut(jPrintStream);
        String txt = null;
        try {
            // Define a SecurityManager to handle System.exit() case
            System.setSecurityManager(new NoExitSecurityManager());

            // Call Method
            callMainMethod(CLIENT_CLASS, "eb" , new String[] {mode});

            txt = jPrintStream.getStringBuffer().toString();
        } catch (InvocationTargetException ite) {
            fail("Fail when invoking the client. It can be due to a System.exit()");
        } catch (Exception e) {
            fail("Client was not ok" + e);
        } finally {
            System.setSecurityManager(new SecurityManager());
            jPrintStream.remove();
        }

        // Check the text
        if (txt.indexOf(CLIENT_OK_TXT) == -1) {
            fail("The text that the client sent was not " + CLIENT_OK_TXT);
        }
    }


    /**
     * Try to launch the client of the example eb
     * with the case AccountImpl2
     * @throws Exception if an error occurs
     */
    public void testClientAccountImpl2() throws Exception {
        doTestEb(ACC_IMPL2);
    }
    /**
     * Try to launch the client of the example eb
     * with the case AccountImpl
     * @throws Exception if an error occurs
     */
    public void testClientAccountImpl() throws Exception {
        doTestEb(ACC_IMPL);
    }

    /**
     * Try to launch the client of the example eb
     * with the case AccountExpl
     * @throws Exception if an error occurs
     */
    public void testClientAccountExpl() throws Exception {
        doTestEb(ACC_EXPL);
    }

}
