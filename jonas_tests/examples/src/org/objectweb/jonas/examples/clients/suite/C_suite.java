/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2005 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id:C_suite.java 10573 2007-06-08 10:12:46Z coqp $
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.examples.clients.suite;

import junit.framework.Test;
import junit.framework.TestSuite;

import org.objectweb.jonas.examples.clients.alarm.C_alarm;
import org.objectweb.jonas.examples.clients.cmp2.C_cmp2;
import org.objectweb.jonas.examples.clients.earsample.C_earsample;
import org.objectweb.jonas.examples.clients.eb.C_eb;
import org.objectweb.jonas.examples.clients.hibernate.C_hibernate;
import org.objectweb.jonas.examples.clients.j2eemanagement.C_j2eemanagement;
import org.objectweb.jonas.examples.clients.lb.C_lb;
import org.objectweb.jonas.examples.clients.mailsb.C_mailsb;
import org.objectweb.jonas.examples.clients.mdb.C_mdb;
import org.objectweb.jonas.examples.clients.sb.C_sb;
import org.objectweb.jonas.examples.clients.webservices.C_webservices;
import org.objectweb.jonas.examples.util.JExampleTestCase;

/**
 * Define a class to test all the examples
 *
 * @author Florent Benoit
 */
public class C_suite extends JExampleTestCase {

    /**
     * Constructor with a specified name
     * @param name the name
     */
    public C_suite(String name) {
        super(name);
    }

    /**
     * Get a new TestSuite for this class
     * It includes earsample, alarm and jonasAdmin
     * @return a new TestSuite for this class
     */
    public static Test suite() {
        TestSuite suite = new TestSuite();
        suite.addTest(C_earsample.suite());
        suite.addTest(C_alarm.suite());
        suite.addTest(C_cmp2.suite());
        suite.addTest(C_eb.suite());
        suite.addTest(C_hibernate.suite());
        suite.addTest(C_j2eemanagement.suite());
        suite.addTest(C_lb.suite());
        suite.addTest(C_mailsb.suite());
        suite.addTest(C_sb.suite());
        suite.addTest(C_webservices.suite());
        suite.addTest(C_mdb.suite());


        return suite;
    }

    /**
     * Main method
     * @param args the arguments
     */
    public static void main (String[] args) {
        junit.textui.TestRunner.run(suite());
    }
}
