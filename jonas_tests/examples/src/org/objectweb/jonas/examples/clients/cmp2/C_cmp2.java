/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.examples.clients.cmp2;

import junit.framework.TestSuite;

import org.objectweb.jonas.examples.util.JExampleTestCase;

import com.meterware.httpunit.WebForm;
import com.meterware.httpunit.WebResponse;

/**
 * Define a class to test the cmp2 example
 * Test the three servlet are ok
 * @author Philippe Coq
 */
public class C_cmp2 extends JExampleTestCase {

    /**
     * URL of the cmp2 page
     */
    private static final String URL_EARCMP2 = "/cmp2";

    /**
     * Main method
     * @param args the arguments
     */
    public static void main(String[] args) {

        String testtorun = null;
        // Get args
        for (int argn = 0; argn < args.length; argn++) {
            String sArg = args[argn];
            if (sArg.equals("-n")) {
                testtorun = args[++argn];
            }
        }

        if (testtorun == null) {
            junit.textui.TestRunner.run(suite());
        } else {
            junit.textui.TestRunner.run(new C_cmp2(testtorun));
        }
    }

    /**
     * Get a new TestSuite for this class
     * @return a new TestSuite for this class
     */
    public static TestSuite suite() {
        return new TestSuite(C_cmp2.class);
    }

    /**
     * Setup need for these tests
     * cmp2 is required
     * @throws Exception if it fails
     */
    protected void setUp() throws Exception {
        super.setUp();
        useEar("cmp2");
    }

    /**
     * Constructor with a specified name
     * @param s name
     */
    public C_cmp2(String s) {
        super(s, URL_EARCMP2);
    }

    /**
     * Test the examples showing 1-to-1 relationship
     * @throws Exception if an error occurs
     */
    public void testFirstServlet() throws Exception {

        WebResponse wr = wc.getResponse(url);

        WebForm[] webForms = wr.getForms();
        WebForm webForm = webForms[0];
        assertNotNull("There must be a form in the html page", webForm);
        WebResponse wFormRes = webForm.submit();
        String txt = wFormRes.getText();
        if (txt.indexOf("<strong>Servlet is OK.</strong>") == -1) {
            fail("The example was not ok : " + txt);
        }
    }


    /**
     * Test the examples showing n-to-1  1-to-n relationship
     * @throws Exception if an error occurs
     */
    public void testSecondServlet() throws Exception {

        WebResponse wr = wc.getResponse(url);

        WebForm[] webForms = wr.getForms();
        WebForm webForm = webForms[1];
        assertNotNull("There must be a form in the html page", webForm);
        WebResponse wFormRes = webForm.submit();
        String txt = wFormRes.getText();
        if (txt.indexOf("<strong>Servlet is OK.</strong>") == -1) {
            fail("The example was not ok : " + txt);
        }
    }

    /**
     * Test the examples using EJB-QL
     * @throws Exception if an error occurs
     */
    public void testThirdServlet() throws Exception {

        WebResponse wr = wc.getResponse(url);

        WebForm[] webForms = wr.getForms();
        WebForm webForm = webForms[2];
        assertNotNull("There must be a form in the html page", webForm);
        WebResponse wFormRes = webForm.submit();
        String txt = wFormRes.getText();
        if (txt.indexOf("<strong>Servlet is OK.</strong>") == -1) {
            fail("The example was not ok : " + txt);
        }
    }

}
