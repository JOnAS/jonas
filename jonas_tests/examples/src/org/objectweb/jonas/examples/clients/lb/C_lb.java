/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.examples.clients.lb;

import java.lang.reflect.InvocationTargetException;
import junit.framework.*;

import org.objectweb.jonas.examples.util.JExampleTestCase;
import org.objectweb.jonas.examples.util.JPrintStream;
import org.objectweb.jonas.examples.util.NoExitSecurityManager;

/**
 * Define a class to test the lb example
 * Test the client of lb example (Check if the text is "PASS when checking all accounts")
 * @author Florent Benoit
 */
public class C_lb extends JExampleTestCase {


    /**
     * Class of the lb.client
     */
    private static final String CLIENT_CLASS = "lb.Client";

    /**
     * Text to check
     */
    private static final String CLIENTOP_OK_TXT = "PASS when checking all accounts" ;


    /**
     * Main method
     * @param args the arguments
     */
    public static void main(String[] args) {

        String testtorun = null;
        // Get args
        for (int argn = 0; argn < args.length; argn++) {
            String sArg = args[argn];
            if (sArg.equals("-n")) {
                testtorun = args[++argn];
            }
        }

        if (testtorun == null) {
            junit.textui.TestRunner.run(suite());
        } else {
            junit.textui.TestRunner.run(new C_lb(testtorun));
        }
    }

    /**
     * Get a new TestSuite for this class
     * @return a new TestSuite for this class
     */
    public static TestSuite suite() {
        return new TestSuite(C_lb.class);
    }

    /**
     * Setup need for these tests
     * lb is required
     * @throws Exception if it fails
     */
    protected void setUp() throws Exception {
        super.setUp();
        useBeans("lb");
    }

    /**
     * Step to do at the end
     * unload lb
     * @throws Exception if it fails
     */
    protected void tearDown() throws Exception {
        super.tearDown();
        unUseBeans("lb");
    }


    /**
     * Constructor with a specified name
     * @param s name
     */
    public C_lb(String s) {
        super(s);
    }


    /**
     * Try to launch the client of the example lb
     * @throws Exception if an error occurs
     */
    public void testClient() throws Exception {
        JPrintStream jPrintStream = new JPrintStream(System.out);
        System.setOut(jPrintStream);
        String txt = null;
        try {
            //Define a SecurityManager to handle System.exit() case
            System.setSecurityManager(new NoExitSecurityManager());

            //Call Method
            callMainMethod(CLIENT_CLASS, "lb");

            txt = jPrintStream.getStringBuffer().toString();
        } catch (InvocationTargetException ite) {
            System.out.println("Error = " + ite);
            ite.printStackTrace();
            fail("Fail when invoking the client. It can be due to a System.exit()");
        } catch (Exception e) {
            fail("Client was not ok" + e);
        } finally {
            System.setSecurityManager(new SecurityManager());
            jPrintStream.remove();
        }

        // Check the text
        if (txt.indexOf(CLIENTOP_OK_TXT) == -1) {
            fail("The text that the client sent was not " + CLIENTOP_OK_TXT);
        }

    }

}
