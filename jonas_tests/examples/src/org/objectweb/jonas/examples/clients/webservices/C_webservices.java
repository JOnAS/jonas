/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2005 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id:C_webservices.java 10573 2007-06-08 10:12:46Z coqp $
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.examples.clients.webservices;

import junit.framework.Test;
import junit.framework.TestSuite;


import org.objectweb.jonas.examples.util.JExampleTestCase;

/**
 * Define a class to test all the webservices  examples
 * Test : - F_WsAccessTest
 *        - F_WsEarSample
 *        - F_WsWarSample
 * @author Philippe Coq
 */
public class C_webservices extends JExampleTestCase {

    /**
     * Constructor with a specified name
     * @param name the name
     */
    public C_webservices(String name) {
        super(name);
    }

    /**
     * Get a new TestSuite for this class
     * It includes earsample, alarm and jonasAdmin
     * @return a new TestSuite for this class
     */
    public static Test suite() {
        TestSuite suite = new TestSuite();
        // The WsAccessTest is using a SOAP Search WS from Google that have been retired on 07/09/2009
        // http://googlecode.blogspot.com/2009/08/well-earned-retirement-for-soap-search.html
        // Commented until a replacement WS has been found
        //suite.addTest(F_WsAccessTest.suite());
        suite.addTest(F_WsEarSample.suite());
        /**suite.addTest(F_WsWarSample.suite()); **/
        return suite;
    }

    /**
     * Main method
     * @param args the arguments
     */
    public static void main (String[] args) {
        junit.textui.TestRunner.run(suite());
    }
}
