/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2005 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.examples.clients.suite;

import junit.framework.Test;
import junit.framework.TestSuite;

import org.objectweb.jonas.examples.clients.alarm.C_alarm;
import org.objectweb.jonas.examples.clients.earsample.C_earsample;
import org.objectweb.jonas.examples.util.JExampleTestCase;

/**
 * Define a class to test all the examples
 * Test : - alarm
 *        - earsample
 * @author Florent Benoit
 */
public class C_WebExamples extends JExampleTestCase {

    /**
     * Constructor with a specified name
     * @param name the name
     */
    public C_WebExamples(String name) {
        super(name);
    }

    /**
     * Get a new TestSuite for this class
     * It includes earsample, alarm and jonasAdmin
     * @return a new TestSuite for this class
     */
    public static Test suite() {
        TestSuite suite = new TestSuite();
        suite.addTest(C_earsample.suite());
        suite.addTest(C_alarm.suite());
        return suite;
    }

    /**
     * Main method
     * @param args the arguments
     */
    public static void main (String[] args) {
        junit.textui.TestRunner.run(suite());
    }
}
