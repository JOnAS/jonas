/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 */

package org.objectweb.jonas.examples.clients.sampleCluster2;

import junit.framework.TestSuite;

import org.objectweb.jonas.examples.util.JExampleTestCase;

import com.meterware.httpunit.WebResponse;

/**
 * Define a class to test the sampleCluster2 example
 * @author Georges Goebel
 */
public class C_sampleCluster2 extends JExampleTestCase {

    /**
     * URL of the earsample page
     */
    private static final String URL_EARSAMPLE = "/sampleCluster2";

    /**
     * Constructor with a specified name
     * @param s name
     */
    public C_sampleCluster2(String s) {
        super(s, URL_EARSAMPLE);
    }

    /**
     * Main method
     * @param args the arguments
     */
    public static void main(String[] args) {

        String testtorun = null;
        // Get args
        for (int argn = 0; argn < args.length; argn++) {
            String sArg = args[argn];
            if (sArg.equals("-n")) {
                testtorun = args[++argn];
            }
        }

        if (testtorun == null) {
            junit.textui.TestRunner.run(suite());
        } else {
            junit.textui.TestRunner.run(new C_sampleCluster2(testtorun));
        }
    }

    /**
     * Get a new TestSuite for this class
     * @return a new TestSuite for this class
     */
    public static TestSuite suite() {
        return new TestSuite(C_sampleCluster2.class);
    }

    /**
     * Setup need for these tests sampleCluster2 is required
     * @throws Exception if it fails
     */
    protected void setUp() throws Exception {
        super.setUp();
        useEar("sampleCluster2");
    }

    /**
     * Check if the First Page is OK. Analyze response
     * @throws Exception if an error occurs
     */
    public void testIsFirstPageOk() throws Exception {
        WebResponse wr = wc.getResponse(url);
        String txt = wr.getText();

        if (txt.indexOf("<strong>Sample is OK.</strong>") == -1) {
            fail("The example was not ok : " + txt);
        }
    }

    /**
     * Check if the sessionServlet is OK. Analyze response
     * @throws Exception if an error occurs
     */
    public void testIsSessionOk() throws Exception {
        WebResponse wr = wc.getResponse(url + "/servlet/session");
        String txt = wr.getText();

        if (txt.indexOf("<strong>Sample is OK.</strong>") == -1) {
            fail("The example was not ok : " + txt);
        }
    }

    /**
     * Check if the checkServlet is OK. Analyze response
     * @throws Exception if an error occurs
     */
    public void testIsCheckOk() throws Exception {
        WebResponse wr = wc.getResponse(url + "/servlet/check");
        String txt = wr.getText();

        if (txt.indexOf("<strong>Sample is OK.</strong>") == -1) {
            fail("The example was not ok : " + txt);
        }
    }

    /**
     * Check if the releaseServlet is OK. Analyze response
     * @throws Exception if an error occurs
     */
    public void testIsReleaseOk() throws Exception {
        WebResponse wr = wc.getResponse(url + "/servlet/release");
        String txt = wr.getText();

        if (txt.indexOf("<strong>Sample is OK.</strong>") == -1) {
            fail("The example was not ok : " + txt);
        }
    }

}
