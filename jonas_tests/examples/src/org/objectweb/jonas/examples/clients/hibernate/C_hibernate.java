/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Guillaume Sauthier
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.examples.clients.hibernate;

import junit.framework.TestSuite;

import org.objectweb.jonas.examples.util.JExampleTestCase;

import com.meterware.httpunit.WebResponse;

/**
 * Define a class to test the Alarm example
 * Test authentication, and check the sample is ok
 * @author Florent Benoit
 */
public class C_hibernate extends JExampleTestCase {

    /**
     * URL of the hibernate page
     */
    private static final String URL_HIBERNATE = "/hibernate-sample/testHibernate";

    /**
     * Main method
     * @param args the arguments
     */
    public static void main(String[] args) {

        String testtorun = null;
        // Get args
        for (int argn = 0; argn < args.length; argn++) {
            String sArg = args[argn];
            if (sArg.equals("-n")) {
                testtorun = args[++argn];
            }
        }

        if (testtorun == null) {
            junit.textui.TestRunner.run(suite());
        } else {
            junit.textui.TestRunner.run(new C_hibernate(testtorun));
        }
    }

    /**
     * Get a new TestSuite for this class
     * @return a new TestSuite for this class
     */
    public static TestSuite suite() {
        return new TestSuite(C_hibernate.class);
    }

    /**
     * Setup need for these tests
     * earsample is required
     * @throws Exception if it fails
     */
    protected void setUp() throws Exception {
        super.setUp();
        useWar("hibernate-sample");
    }

    /**
     * Constructor with a specified name
     * @param s name
     */
    public C_hibernate(String s) {
        super(s, URL_HIBERNATE);
    }

    /**
     * Try to log in without authentication
     * @throws Exception if an error occurs
     */
    public void testAccessHibernatePage() throws Exception {
            WebResponse wr = wc.getResponse(url);
            String page = wr.getText();

            // check "Servlet is OK"
            if (page.indexOf("Servlet is OK") == -1) {
                fail("Missing 'Servlet is OK' ending text. Check on " + this.url);
            }

    }

}
