/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.tables;

import org.objectweb.util.monolog.api.BasicLevel;

import javax.naming.NamingException;
import java.sql.Statement;
import java.sql.Connection;
import java.rmi.RemoteException;

/**
 * Class to create tables for the 'jdbc' bean.
 * @author durieuxp
 */

public class Tjdbc extends Tmanager {
    /**
     * Entry point
     */
    public static void init() throws NamingException, RemoteException {
        mgrInit();
        createTable("jdbcSF");
    }

    /**
     * create a table for the pdouble bean of etype
     */
    private static void createTable(String name) throws RemoteException {

        // get connection
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
        } catch (Exception e) {
            throw new RemoteException("Cannot get Connection");
        }

        Statement stmt;
        try {
            stmt = conn.createStatement();
            stmt.execute("DROP TABLE " + name);
            stmt.close();
            logger.log(BasicLevel.INFO, "Table " + name + " dropped");
        } catch (Exception e) {
            logger.log(BasicLevel.DEBUG, "Exception in dropTable : \n" + e);
        }
        try {
            stmt = conn.createStatement();
            stmt.execute("create table " + name +
                         "(c_pk varchar(30) not null primary key,"+
                         "c_value integer)");
            stmt.execute("insert into " + name + " values('pk1', 0)");
            stmt.execute("insert into " + name + " values('pk2', 0)");
            stmt.execute("insert into " + name + " values('pk3', 0)");
            stmt.execute("insert into " + name + " values('pk4', 0)");

            stmt.close();
            conn.close();        // release connection
        } catch (Exception e) {
            logger.log(BasicLevel.ERROR, "Exception in create/init Table : " + e);
            throw new RemoteException("Exception in create/init Table : " + e);
        }
        logger.log(BasicLevel.INFO, "Table " + name + " created");
    }

}
