/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.clients.entity;

import java.util.Collection;
import java.util.Iterator;

import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

import junit.framework.Test;
import junit.framework.TestSuite;

import org.objectweb.jonas.jtests.beans.ebasic.E4Query;
import org.objectweb.jonas.jtests.beans.ebasic.E4QueryHome;
import org.objectweb.jonas.jtests.util.JTestCase;


/**
 * For testing basic EJB QL queries.
 * @author Helene Joanin
 */

public class S_BasicEjbqlEC2 extends JTestCase {

    private static String BEAN_HOME_E4QUERY = "ebasicE4QueryEC2Home";
    protected static E4QueryHome home = null;


    public S_BasicEjbqlEC2(String name) {
        super(name);
    }

    protected void setUp() {
        if (home == null) {
            // load bean if not loaded yet
            useBeans("ebasic", true);
            try {
                home = (E4QueryHome) PortableRemoteObject.narrow(ictx.lookup(BEAN_HOME_E4QUERY),
                                                                 E4QueryHome.class);
            } catch (NamingException e) {
                fail("Cannot get bean home: " + e.getMessage());
            }
            // check if tables have been initialized
            try {
                home.findByPrimaryKey("id1");
            } catch (Exception e) {
                try {
                    home.create("idnull", null, 0, 0.0);
                    home.create("id1","helene", 1959, 1959.0);
                    home.create("id2","ahelene", -1959, 1959.0);
                    home.create("id3","helene-bis", 1959*1959, 1959.0*1959.0);
                    home.create("id4","eric", 1957, 1957.0);
                    home.create("id4e","ric", 1957, 1957.0);
                } catch (Exception i) {
                    fail("InitialState creation problem:: "+i);
                }
            }
        }
    }

    /**
     * Test the EJB-QL: SELECT OBJECT(o) FROM jt2_e4query o WHERE o.fint = ?1 + ?2 - ?3
     */
    public void testIntEqualExpr() throws Exception {
        Collection cBeans = home.findByIntEqualExpr(1959, 255, 255);
        Iterator iBeans = cBeans.iterator();
        int nb=0;
        while(iBeans.hasNext()) {
            E4Query bean = (E4Query) javax.rmi.PortableRemoteObject.narrow(iBeans.next(),
                                                                           E4Query.class);
            int f = bean.getFint();
            assertEquals("Id="+bean.getId(), 1959, f);
            nb++;
        }
        assertEquals("Beans number: ", 1, nb);

    }



    public static Test suite() {
        return new TestSuite(S_BasicEjbqlEC2.class);
    }

    public static void main(String args[]) {
        String testtorun = null;
        // Get args
        for (int argn = 0; argn < args.length; argn++) {
            String s_arg = args[argn];
            Integer i_arg;
            if (s_arg.equals("-n")) {
                testtorun = args[++argn];
            }
        }
        if (testtorun == null) {
            junit.textui.TestRunner.run(suite());
        } else {
            junit.textui.TestRunner.run(new S_BasicEjbqlEC2(testtorun));
        }
    }

}
