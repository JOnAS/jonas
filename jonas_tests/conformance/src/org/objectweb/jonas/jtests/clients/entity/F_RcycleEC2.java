/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.clients.entity;

import java.util.Collection;
import java.util.Iterator;

import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

import junit.framework.Test;
import junit.framework.TestSuite;

import org.objectweb.jonas.jtests.beans.relation.rcycle.PersonRemote;
import org.objectweb.jonas.jtests.beans.relation.rcycle.PersonHomeRemote;
import org.objectweb.jonas.jtests.util.JTestCase;

/**
 * This is a test suite on CMP 2 : legacy, cycle in relations, relations between
 * the same bean.
 * @author Helene Joanin
 */
public class F_RcycleEC2 extends JTestCase {

    private static final int ID_L_ERIC = 1;

    private static final int ID_JL_HELENE = 2;

    private static final int ID_L_GUILHEM = 3;

    private static final int ID_L_MALVA = 4;

    private static PersonHomeRemote personhome = null;

    public F_RcycleEC2(String name) {
        super(name);
    }

    protected boolean isInit = false;

    protected void setUp() {
        super.setUp();
        if (!isInit) {
            useBeans("rcycle", true);
            try {
                personhome = (PersonHomeRemote) PortableRemoteObject.narrow(ictx.lookup("RcyclePersonHome"),
                        PersonHomeRemote.class);
            } catch (NamingException e) {
                fail("Cannot get bean home: " + e.getMessage());
            }
            isInit = true;
        }
    }

    /**
     * Test the findAll method.
     * @throws Exception
     */
    public void testFindAll() throws Exception {
        Collection cp = personhome.findAll();
        assertEquals("Number of Persons: ", 4, cp.size());
    }

    /**
     * (Bug #300533) Test the findQuery1 method.
     * @throws Exception
     */
    public void testFindQuery1() throws Exception {
        Collection cp = personhome.findQuery1();
        assertEquals("Number of Persons: ", 0, cp.size());
    }

    /**
     * Test the findQuery2 method.
     * @throws Exception
     */
    public void testFindQuery2() throws Exception {
        Collection cp = personhome.findQuery2();
        assertEquals("Number of Persons: ", 0, cp.size());
    }

    /**
     * (Bug #300526) Test the findSpouse3 method.
     * @throws Exception
     */
    public void testFindSpouse3() throws Exception {
        PersonRemote p = personhome.findSpouse3();
        assertNull("Laurent Guilhem spouse: ", p);
    }



    protected boolean initStateOK() throws Exception {
        return true;
    }

    public static Test suite() {
        return new TestSuite(F_RcycleEC2.class);
    }

    public static void main(String args[]) {
        String testtorun = null;
        // Get args
        for (int argn = 0; argn < args.length; argn++) {
            String sarg = args[argn];
            if (sarg.equals("-n")) {
                testtorun = args[++argn];
            }
        }
        if (testtorun == null) {
            junit.textui.TestRunner.run(suite());
        } else {
            junit.textui.TestRunner.run(new F_RcycleEC2(testtorun));
        }
    }
}
