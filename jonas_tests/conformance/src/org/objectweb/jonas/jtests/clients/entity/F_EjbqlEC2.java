/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2005 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty offind
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 */

package org.objectweb.jonas.jtests.clients.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

import junit.framework.Test;
import junit.framework.TestSuite;

import org.objectweb.jonas.jtests.beans.ejbql.*;
import org.objectweb.util.monolog.api.Logger;
import org.objectweb.util.monolog.api.BasicLevel;
import org.ow2.jonas.lib.util.Log;

/**
 * This is an advanced test suite for home interface on entity bean EJBQL.
 */
public class F_EjbqlEC2 extends A_Cmp2Util {

    private static String BEAN_HOME_SESSION_TEST = "EjbqlSessionTestHomeRemote";
    private static String BEAN_HOME_CUSTOMER = "CustomerHomeRemote";
    private static String BEAN_HOME_RESERVATION = "ReservationHomeRemote";
    private static String BEAN_HOME_CRUISE = "CruiseHomeRemote";
    private static String BEAN_HOME_SHIP = "ShipHomeRemote";
    private static String BEAN_HOME_CABIN = "CabinHomeRemote";
    private static String BEAN_HOME_ADDRESS = "AddressHomeRemote";

    protected static SessionTestHomeRemote sessiontesthome = null;
    protected static CustomerHomeRemote customerhome = null;
    protected static ReservationHomeRemote reservationhome = null;
    protected static CruiseHomeRemote cruisehome = null;
    protected static ShipHomeRemote shiphome = null;
    protected static CabinHomeRemote cabinhome = null;
    protected static AddressHomeRemote addresshome = null;
    protected static CreditCardHomeRemote creditcardhome = null;

    public F_EjbqlEC2(String name) {
        super(name);
    }

    protected boolean isInit = false;

    protected void setUp() {
        super.setUp();
        boolean ok = false;
        int nbtry = 0;
        while (!ok && nbtry < 3) {
            logger.log(BasicLevel.DEBUG, "Starting " + getName() + " try " + nbtry);
            if (!isInit) {
                // load bean if not loaded yet
                useBeans("ejbql", false);
                // lookup home used in the tests

                try {
                    sessiontesthome  = (SessionTestHomeRemote)
                        PortableRemoteObject.narrow(ictx.lookup(BEAN_HOME_SESSION_TEST),
                                                    SessionTestHomeRemote.class);
                    customerhome  = (CustomerHomeRemote)
                        PortableRemoteObject.narrow(ictx.lookup(BEAN_HOME_CUSTOMER),
                                                    CustomerHomeRemote.class);
                    shiphome  = (ShipHomeRemote)
                        PortableRemoteObject.narrow(ictx.lookup(BEAN_HOME_SHIP),
                                                    ShipHomeRemote.class);
                    cruisehome = (CruiseHomeRemote)
                        PortableRemoteObject.narrow(ictx.lookup(BEAN_HOME_CRUISE),
                                                    CruiseHomeRemote.class);
                    reservationhome = (ReservationHomeRemote)
                        PortableRemoteObject.narrow(ictx.lookup(BEAN_HOME_RESERVATION),
                                                    ReservationHomeRemote.class);
                    cabinhome = (CabinHomeRemote)
                        PortableRemoteObject.narrow(ictx.lookup(BEAN_HOME_CABIN),
                                                    CabinHomeRemote.class);
                    addresshome = (AddressHomeRemote)
                        PortableRemoteObject.narrow(ictx.lookup(BEAN_HOME_ADDRESS),
                                                    AddressHomeRemote.class);
                } catch (NamingException e) {
                    fail("Cannot get bean home: " + e.getMessage());
                }
                // check if tables have been initialized
                try {
                    customerhome.findByPrimaryKey(new Integer(80));
                } catch (Exception e) {
                    // Make the initialization needed for the tests
                    try {
                        logger.log(BasicLevel.DEBUG, getName() + ": init tables");

                        utx.begin();
                        String cities[] = new String[6];
                        cities[0] = "Minneapolis"; cities[1] = "St. Paul"; cities[2] = "Rochester";
                        cities[3] = "Winona"; cities[4] = "Wayzata"; cities[5] = "Eagan";
                        ShipRemote shipA = null;
                        CruiseRemote cruiseA = null;
                        shipA = shiphome.create(new Integer(10772), "Ship A", 30000.0);
                        cruiseA = cruisehome.create("Cruise A", new Integer(10772));
                        for (int jj = 1; jj <= 10; jj++) {
                            ShipRemote ship = shiphome.create(new Integer(jj), "Ship " + jj, 30000.0 + (10000.0 * jj));
                        }
                        cruiseA = null;
                        CruiseRemote cruiseB = null;
                        ShipRemote ship1 = shiphome.findByPrimaryKey(new Integer(1));
                        cruiseA = cruisehome.create("Alaska Cruise", new Integer(1));
                        cruiseB = cruisehome.create("Bohemian Cruise", new Integer(1));
                        for (int kk = 80; kk <= 99; kk++) {
                            CustomerRemote customer = null;

                            customer = customerhome.create(new Integer(kk));
                            customer.setName(new Name("Smith"+kk,"John") );
                            customer.addPhoneNumber("612-555-12"+kk,(byte) 1);

                            customer.setAddress("10"+kk+" Elm Street",
                                                cities[(kk-80)%6],
                                                (kk%2==0?"MN":"CA"),
                                                "5540"+(kk%5+1));
                            customer.setHasGoodCredit((kk%4 == 0));
                            if (kk%4 == 0)
                                customer.setCreditCard(new Date(System.currentTimeMillis()+100000), "100001"+kk, "MasterCard", "CA" ,"CETELEM", "rue de jeunet", cities[0], "France",  "3888" );
                        }
                        utx.commit();

                        // Creating Customers 1-6, each with 2 reservations for 2 cabins
                        utx.begin();
                        Calendar date = Calendar.getInstance();
                        date.set(2002,10,1);

                        for (int kk=201; kk<207; kk++) {
                            Collection customers = new ArrayList();
                            CustomerRemote cust = customerhome.create(new Integer(kk));
                            cust.setName(new Name("Customer "+kk,"Mike"));
                            cust.setHasGoodCredit( (kk%2==0) );  // odds are bums
                            cust.setAddress("50"+kk+" Main Street","Minneapolis","MN","5510"+kk);
                            customers.add(new Integer(kk));  // put this single customer in the collection

                            Collection reservations = new ArrayList();

                            for (int jj=0; jj<2; jj++) {

                                ReservationRemote reservation = reservationhome.create(cruiseA.getId(), customers);
                                reservation.setDate(date.getTime());
                                reservation.setAmountPaid(1000*kk+100*jj+2000);

                                date.add(Calendar.DAY_OF_MONTH, 7);

                                Set cabins = new HashSet();
                                CabinRemote cabin = cabinhome.create(new Integer(1000+kk*100+jj));
                                cabin.setDeckLevel(kk-200);
                                cabin.setName("Cabin "+kk+"0"+jj+"1");

                                cabins.add(cabin.getId());
                                cabin = cabinhome.create(new Integer(1000+kk*100+10+jj));
                                cabin.setDeckLevel(kk-200);
                                cabin.setName("Cabin "+kk+"0"+jj+"2");

                                cabins.add(cabin.getId());

                                reservation.setAllCabins(cabins);  // this reservation has 2 cabins

                            }
                        }
                        utx.commit();
                        utx.begin();
                        String fnames[] = new String[5];
                        fnames[0]="John"; fnames[1]="Paul"; fnames[2]="Ringo";
                        fnames[3]="Joe"; fnames[4]="Roger";

                        String lnames[] = new String[3];
                        lnames[0]="Smith"; lnames[1]="Johnson"; lnames[2]="Star";
                        // Creating Customers 50-69
                        for (int kk=50; kk<=69; kk++) {
                            CustomerRemote customer = customerhome.create(new Integer(kk));
                            customer.setName( new Name(lnames[(kk-50)%3], fnames[(kk-50)%5]) );
                            customer.addPhoneNumber("612-555-12"+kk,(byte)1);
                            customer.setAddress("10"+kk+" Elm Street",
                                                cities[(kk-50)%6],
                                                (kk%2==0?"MN":"CA"),
                                                "5540"+(kk%5+1));
                            customer.setHasGoodCredit((kk%4==0));

                            // Some customers will have reservations already on one of the two cruises..
                            if (kk%3!=0) {
                                Collection customers = new ArrayList();
                                customers.add(customer.getId());  // put this single customer in the collection
                                ReservationRemote reservation = reservationhome.create((kk%3==1?cruiseA.getId():cruiseB.getId()), customers);
                                reservation.setDate(date.getTime());
                                reservation.setAmountPaid(10*kk+2000);
                                date.add(Calendar.DAY_OF_MONTH, 7);
                            }
                        }
                        utx.commit();

                        // Creating Customers 100-109
                        utx.begin();
                        for (int kk=100; kk<=109; kk++) {
                            CustomerRemote customer = customerhome.create(new Integer(kk));
                            customer.setName( new Name("Lennon"+kk, "Paul") );
                            customer.addPhoneNumber("666-543-12"+kk,(byte)1);

                            customer.setAddress("10"+kk+" Abbey Road",
                                                cities[(kk-100)%6],
                                                (kk%2==0?"FL":"WA"),
                                                "5540"+(kk%5+1));
                            customer.setHasGoodCredit((kk%4==0));
                        }

                        logger.log(BasicLevel.DEBUG, getName() + ": init done");
                    } catch (Exception i) {
                        i.printStackTrace(System.out);
                        fail("InitialState creation problem: "+i);
                    } finally {
                        try {
                            utx.commit();
                        } catch (Exception ii) {
                        }
                    }
                }
                isInit = true;
            }
            // Check that all is OK. Sometimes, a test has failed and has corrupted
            // the bean state in the database. We must unload and reload the bean then.
            nbtry++;
            try {
                if (initStateOK()) {
                    ok = true;
                }
            } catch (Exception e) {
                e.printStackTrace(System.out);
            }
            if (!ok) {
                isInit = false;
                unloadBeans("ejbql");
            }
        }
    }


    /*
     * Check that we are in the same state as after the tables creation for thoses beans A and B
     * (ie if it is the initial state)
     */
    boolean initStateOK() throws Exception {
        // just verify that all ejb are correcly created. Nothing about relation here because we testing only ejbql
        boolean isOk = true;

        msgerror = new StringBuffer();

        ShipRemote ship10772 = shiphome.findByPrimaryKey(new Integer(10772));
        for (int i=1; i<=10; i++) {
            ShipRemote ship = shiphome.findByPrimaryKey(new Integer(i));
        }

        CruiseRemote cruiseA = cruisehome.findByName("Cruise A");
        CruiseRemote cruiseB = cruisehome.findByName("Alaska Cruise");
        CruiseRemote cruiseC = cruisehome.findByName("Bohemian Cruise");
        for (int j=10; j<=12; j++) {
            CruiseRemote cruise = cruisehome.findByPrimaryKey(new Integer(j));
        }

        for (int k=80; k<=109; k++) {
            CustomerRemote customer = customerhome.findByPrimaryKey(new Integer(k));
        }

        for (int l=201; l<=206; l++) {
            CustomerRemote customer1 = customerhome.findByPrimaryKey(new Integer(l));
        }

        for (int p=50; p<=69; p++) {
            CustomerRemote customer2 = customerhome.findByPrimaryKey(new Integer(p));
        }
        for (int kk=201;kk<207;kk++) {
            for (int jj=0;jj<2;jj++) {
                CabinRemote cabin = cabinhome.findByPrimaryKey(new Integer(1000+kk*100+jj));
                CabinRemote cabin1 = cabinhome.findByPrimaryKey(new Integer(1000+kk*100+10+jj));
            }
        }

        for (int kkh=10;kkh<=34;kkh++) {
            ReservationRemote reservation = reservationhome.findByPrimaryKey(new Integer(kkh));
        }

        return isOk;
    }

    /**
     * The execution of those two tests failed.
     */
    public void testSeries1() throws Exception {
        testWildcards();
        testWildcards();
    }

    /**
     * 	Finding Customer having name 'John Smith85'
     *  SELECT OBJECT(c) FROM jt2_Customer c WHERE c.lastName = ?1 AND c.firstName = ?2
     */
    public void testAnd1() throws Exception {
        CustomerRemote customer85 = customerhome.findByExactName("Smith85","John");
        assertEquals("Wrong Customer : ",new Integer(85), customer85.getId());
        // checkIsInitialState(); not necessary in this test
    }

    /**
     * 	Finding Customer 'Smith90'
     *  SELECT OBJECT(c) FROM jt2_Customer c WHERE c.lastName = 'Smith90'
     */
    public void testEgalString() throws Exception {
        CustomerRemote customer90 = customerhome.findSmith90();
        assertEquals("Wrong Customer : ",new Integer(90), customer90.getId());
        // checkIsInitialState(); not necessary in this test
    }

    /**
     * 	Finding Customers having GoodCredit
     *  SELECT OBJECT(c) FROM jt2_Customer c WHERE c.hasGoodCredit = TRUE
     */
    public void testEgalBoolean() throws Exception {
        Collection mplscustomers = customerhome.findByGoodCredit();
        ArrayList customers_id  = new ArrayList();
        Iterator customer = mplscustomers.iterator();
        while (customer.hasNext()) {
            CustomerRemote customer_remote = (CustomerRemote) PortableRemoteObject.narrow(customer.next(), CustomerRemote.class);
            customers_id.add(customer_remote.getId());
        }
        ArrayList result = new ArrayList();

        for (int ii=80;ii<=96;ii=ii+4) {
            result.add(new Integer(ii));
        }

        for (int ii=100;ii<=108;ii=ii+4) {
            result.add(new Integer(ii));
        }
        for (int jj=202;jj<=206;jj=jj+2) {
            result.add(new Integer(jj));
        }

        for (int kk=52;kk<=68;kk=kk+4) {
            result.add(new Integer(kk));
        }

        assertTrue("Wrong result EJBQL Customer.findByGoogCredit() (required:" + result + ", found:" +customers_id
                   + ")", isCollectionEqual(result, customers_id));

        // checkIsInitialState();  not necessary in this test
    }

    /**
     * 	Finding Customers having City = Minneapolis and STATE= MN
     *  SELECT OBJECT(c) FROM jt2_Customer c WHERE c.homeAddress.city = ?1 AND c.homeAddress.state = ?2
     */
    public void testAnd2() throws Exception {
        Collection mplscustomers = customerhome.findByCity("Minneapolis","MN");

        ArrayList customers_id  = new ArrayList();
        Iterator customer = mplscustomers.iterator();
        while (customer.hasNext()) {
            CustomerRemote customer_remote = (CustomerRemote) PortableRemoteObject.narrow(customer.next(), CustomerRemote.class);
            customers_id.add(customer_remote.getId());
        }
        ArrayList result = new ArrayList();
        result.add(new Integer(206));
        result.add(new Integer(50));
        result.add(new Integer(56));
        result.add(new Integer(62));
        result.add(new Integer(68));
        result.add(new Integer(80));
        result.add(new Integer(86));
        result.add(new Integer(92));
        result.add(new Integer(98));
        result.add(new Integer(201));
        result.add(new Integer(202));
        result.add(new Integer(203));
        result.add(new Integer(204));
        result.add(new Integer(205));

        assertTrue("Wrong result EJBQL Customer.findByCity(Minneapolis,MN) (required:" + result + ", found:" +customers_id
                   + ")", isCollectionEqual(result, customers_id));

        //checkIsInitialState();not necessary in this test
    }
    /**
     * 	Finding Customer having a name exactly matching 'Joe Star' &
     *  Finding Customers having a name like 'Jo S' (no wildcards) &
     *  Finding Customers having a name like 'Jo% S%' (with wildcards)
     *  Finding Customers having a name like 'Jo% S%' and living in MN
     *  SELECT OBJECT(c) FROM jt2_Customer c WHERE c.lastName = ?1 AND c.firstName = ?2 &
     *  SELECT OBJECT(c) FROM jt2_Customer c WHERE c.lastName LIKE ?1 AND c.firstName LIKE ?2 &
     *  SELECT OBJECT(c) FROM jt2_Customer c WHERE c.lastName LIKE ?1 AND c.firstName LIKE ?2
     *  SELECT OBJECT(c) FROM jt2_Customer c WHERE c.lastName LIKE ?1 AND c.firstName LIKE ?2 AND c.homeAddress.state = ?3
     */
    public void testWildcards() throws Exception {
        // first test : exactly matching
        CustomerRemote customer = customerhome.findByExactName("Star","Joe");
        assertEquals("Wrong Customer for customerhome.findByExactName(Star,Joe);: ",new Integer(58), customer.getId());

        // second test : find with no wildcards
        Collection customers = customerhome.findByName("S","Jo");
        ArrayList customers_id  = new ArrayList();
        Iterator customerit1 = customers.iterator();
        while (customerit1.hasNext()) {
            CustomerRemote customer_remote = (CustomerRemote) PortableRemoteObject.narrow(customerit1.next(), CustomerRemote.class);
            customers_id.add(customer_remote.getId());
        }
        ArrayList result = new ArrayList();
        assertTrue("Wrong result EJBQL customerhome.findByName(S,Jo) (required:" + result + ", found:" +customers_id
                   + ")", isCollectionEqual(result, customers_id));

        // thirdth test : find with no wildcards
        customers = customerhome.findByName("S%","Jo%");
        customers_id  = new ArrayList();
        Iterator customerit2 = customers.iterator();
        while (customerit2.hasNext()) {
            CustomerRemote customer_remote = (CustomerRemote) PortableRemoteObject.narrow(customerit2.next(), CustomerRemote.class);
            customers_id.add(customer_remote.getId());
        }
        result = new ArrayList();
        result.add(new Integer(50));
        result.add(new Integer(53));
        result.add(new Integer(55));
        result.add(new Integer(58));
        result.add(new Integer(65));
        result.add(new Integer(68));
        for (int i=80;i<=99;i++) {
            result.add(new Integer(i));
        }
        assertTrue("Wrong result EJBQL customerhome.findByName(S%,Jo%) (required:" + result + ", found:" +customers_id
                   + ")", isCollectionEqual(result, customers_id));

        customers = customerhome.findByNameAndState("S%","Jo%","MN");
        customers_id  = new ArrayList();
        Iterator customerit3 = customers.iterator();
        while (customerit3.hasNext()) {
            CustomerRemote customer_remote = (CustomerRemote) PortableRemoteObject.narrow(customerit3.next(), CustomerRemote.class);
            customers_id.add(customer_remote.getId());
        }
        result = new ArrayList();
        result.add(new Integer(50));
        result.add(new Integer(68));
        result.add(new Integer(58));
        for (int i=80;i<=99;i=i+2) {
            result.add(new Integer(i));
        }
        assertTrue("Wrong result EJBQL customerhome.findByNameAndState(S%,Jo%,MN) (required:" + result + ", found:" +customers_id
                   + ")", isCollectionEqual(result, customers_id));


        // checkIsInitialState(); not necessary in this test
    }

    /**
     * (Bug #300634)
     *  Finding Customers having a name not like 'Jo%' OR not like 'S%'
     *  SELECT OBJECT(c) FROM jt2_Customer c WHERE c.lastName NOT LIKE ?1 OR c.firstName NOT LIKE ?2
     */
    public void testNotLike() throws Exception {
        Collection customers = customerhome.findByNotName("S%","Jo%");
        ArrayList customers_id  = new ArrayList();
        Iterator customerit1 = customers.iterator();
        while (customerit1.hasNext()) {
            CustomerRemote customer_remote = (CustomerRemote) PortableRemoteObject.narrow(customerit1.next(), CustomerRemote.class);
            customers_id.add(customer_remote.getId());
        }
        ArrayList result = new ArrayList();
        result.add(new Integer(51));
        result.add(new Integer(52));
        result.add(new Integer(54));
        result.add(new Integer(56));
        result.add(new Integer(57));
        result.add(new Integer(59));
        for (int i=60;i<=64;i++) {
            result.add(new Integer(i));
        }
        result.add(new Integer(66));
        result.add(new Integer(67));
        result.add(new Integer(69));
        for (int i=100;i<=109;i++) {
            result.add(new Integer(i));
        }
        for (int i=201;i<=206;i++) {
            result.add(new Integer(i));
        }
        assertTrue("Wrong result EJBQL customerhome.findByNotName(S%,Jo%) (required:" + result + ", found:" +customers_id
                   + ")", isCollectionEqual(result, customers_id));

        // checkIsInitialState(); not necessary in this test
    }

    /**
     * 	Retrieve a collection of all cabins on deck 3
     *  SELECT OBJECT(c) FROM Cabin as c WHERE c.deckLevel = ?1
     */
    public void testEgalInteger() throws Exception {
        Collection cabins = cabinhome.findAllOnDeckLevel(new Integer(3));

        ArrayList cabins_id  = new ArrayList();
        Iterator cabin = cabins.iterator();
        while (cabin.hasNext()) {
            CabinRemote cabin_remote = (CabinRemote) PortableRemoteObject.narrow(cabin.next(), CabinRemote.class);
            cabins_id.add(cabin_remote.getId());
        }
        ArrayList result = new ArrayList();
        result.add(new Integer(21300));
        result.add(new Integer(21310));
        result.add(new Integer(21301));
        result.add(new Integer(21311));

        assertTrue("Wrong result EJBQL  cabinhome.findAllOnDeckLevel(new Integer(3))(required:" + result + ", found:" +cabins_id+
                   ")", isCollectionEqual(result,cabins_id));

        // checkIsInitialState();  not necessary in this test
    }

    /**
     * 	Findng Customers Living in Warm Climates
     *  SELECT OBJECT(c) FROM jt2_Customer c WHERE c.homeAddress.state IN ('FL','TX','AZ','CA')
     */
    public void testInWhere() throws Exception {
        Collection customers = customerhome.findInHotStates();

        ArrayList customers_id  = new ArrayList();
        Iterator customer = customers.iterator();
        while (customer.hasNext()) {
            CustomerRemote customer_remote = (CustomerRemote) PortableRemoteObject.narrow(customer.next(), CustomerRemote.class);
            customers_id.add(customer_remote.getId());
        }
        ArrayList result = new ArrayList();
        for (int i=51;i<=69;i=i+2) {
            result.add(new Integer(i));
        }
        for (int j=100;j<=109;j=j+2) {
            result.add(new Integer(j));
        }
        for (int k=81;k<=99;k=k+2) {
            result.add(new Integer(k));
        }

        assertTrue("Wrong result EJBQL customerhome.findInHotStates() (required:" + result + ", found:" +customers_id+
                   ")", isCollectionEqual(result,customers_id));

        // checkIsInitialState();  not necessary in this test
    }

    /**
     * 	Test an IN expression with literals integer
     *  SELECT OBJECT(c) FROM jt2_Cabin as c WHERE c.deckLevel IN ( 1 , 3 , 5 )
     */
    public void test2InWhere() throws Exception {
        Collection cabins = cabinhome.findAllOnDeckLevel1_3_5();

        ArrayList customers_id  = new ArrayList();
        Iterator icabins = cabins.iterator();
        int nb = 0;
        while (icabins.hasNext()) {
            CabinRemote cabin_remote = (CabinRemote) PortableRemoteObject.narrow(icabins.next(), CabinRemote.class);
            nb++;
            int level = cabin_remote.getDeckLevel();
            if ((level != 1) && (level != 3) && (level != 5)) {
                fail("Wrong result EJBQL cabinhome.findAllOnDeckLevel1_3_5(5) (level="
                     + level + ")");
            }
        }
        assertEquals("Wrong result EJBQL cabinhome.findAllOnDeckLevel1_3_5(5)", 12, nb);

        // checkIsInitialState();  not necessary in this test
    }

    /**
     * 	Test an IN expression with literals integer and an input parameter
     *  SELECT OBJECT(c) FROM jt2_Cabin as c WHERE c.deckLevel IN ( 1 , 3 , ?1 )
     */
    public void test3InWhere() throws Exception {
        Collection cabins = cabinhome.findAllOnDeckLevel1_3_X(5);

        ArrayList customers_id  = new ArrayList();
        Iterator icabins = cabins.iterator();
        int nb = 0;
        while (icabins.hasNext()) {
            CabinRemote cabin_remote = (CabinRemote) PortableRemoteObject.narrow(icabins.next(), CabinRemote.class);
            nb++;
            int level = cabin_remote.getDeckLevel();
            if ((level != 1) && (level != 3) && (level != 5)) {
                fail("Wrong result EJBQL cabinhome.findAllOnDeckLevel1_3_X(5) (level="
                     + level + ")");
            }
        }
        assertEquals("Wrong result EJBQL cabinhome.findAllOnDeckLevel1_3_X(5)", 12, nb);

        // checkIsInitialState();  not necessary in this test
    }

    /**
     * 	Finding Customers Without Reservations
     *  SELECT OBJECT(c) FROM jt2_Customer c WHERE c.reservations IS EMPTY
     */
    public void testIsEmpty() throws Exception {
        Collection customers = customerhome.findWithoutReservations();

        ArrayList customers_id  = new ArrayList();
        Iterator customer = customers.iterator();
        while (customer.hasNext()) {
            CustomerRemote customer_remote = (CustomerRemote) PortableRemoteObject.narrow(customer.next(), CustomerRemote.class);
            customers_id.add(customer_remote.getId());
        }
        ArrayList result = new ArrayList();
        for (int i=51;i<=69;i=i+3) {
            result.add(new Integer(i));
        }
        for (int i=80;i<=109;i++) {
            result.add(new Integer(i));
        }

        assertTrue("Wrong result EJBQL customerhome.findWithoutReservations() (required:"
                   + result + ", found:" + customers_id + ")",
                   isCollectionEqual(result,customers_id));

        //checkIsInitialState(); not necessary in this test
    }

    /**
     * 	Finding Customers With Reservations
     *  SELECT OBJECT(c) FROM jt2_Customer c WHERE c.reservations IS NOT EMPTY
     */
    public void testIsNotEmpty() throws Exception {
        Collection customers = customerhome.findWithReservations();

        ArrayList customers_id  = new ArrayList();
        Iterator customer = customers.iterator();
        while (customer.hasNext()) {
            CustomerRemote customer_remote = (CustomerRemote) PortableRemoteObject.narrow(customer.next(), CustomerRemote.class);
            customers_id.add(customer_remote.getId());
        }
        ArrayList result = new ArrayList();
        for (int i=201;i<=206;i++) {
            result.add(new Integer(i));
        }
        result.add(new Integer(50));
        result.add(new Integer(52));
        result.add(new Integer(53));
        result.add(new Integer(55));
        result.add(new Integer(56));
        result.add(new Integer(58));
        result.add(new Integer(59));
        result.add(new Integer(61));
        result.add(new Integer(62));
        result.add(new Integer(64));
        result.add(new Integer(65));
        result.add(new Integer(67));
        result.add(new Integer(68));

        assertTrue("Wrong result EJBQL customerhome.findWithReservations() (required:"
                   + result + ", found:" + customers_id + ")",
                   isCollectionEqual(result,customers_id));

        //checkIsInitialState(); not necessary in this test
    }

    /**
     * Test to reproduce bug #300525
     * 	Finding Customers Without or With Reservations (equivalent to finding all customers)
     *  SELECT OBJECT(c) FROM jt2_Customer c
     *    WHERE c.reservations IS EMPTY OR c.reservations IS NOT EMPTY
     */
    public void testIsEmptyIsNotEmpty() throws Exception {
        Collection customers = customerhome.findWithOrWithoutReservations();

        ArrayList customers_id  = new ArrayList();
        Iterator customer = customers.iterator();
        while (customer.hasNext()) {
            CustomerRemote customer_remote = (CustomerRemote) PortableRemoteObject.narrow(customer.next(), CustomerRemote.class);
            customers_id.add(customer_remote.getId());
        }
        ArrayList result = new ArrayList();
        for (int i=50;i<=69;i++) {
            result.add(new Integer(i));
        }
        for (int i=80;i<=109;i++) {
            result.add(new Integer(i));
        }
        for (int i=201;i<=206;i++) {
            result.add(new Integer(i));
        }

        assertTrue("Wrong result EJBQL customerhome.findWithOrWithoutReservations() (required:"
                   + result + ", found:" + customers_id + ")",
                   isCollectionEqual(result,customers_id));

        //checkIsInitialState(); not necessary in this test
    }

    /**
     * 	Finding Customers Without Reservations and with good credit
     *  SELECT OBJECT(c) FROM jt2_Customer c
     *    WHERE c.reservations IS EMPTY AND c.hasGoodCredit = true
     */
    public void testIsEmptyInExp1() throws Exception {
        Collection customers = customerhome.findWithoutReservationsAndWithGoodCredit(true);

        ArrayList customers_id  = new ArrayList();
        Iterator customer = customers.iterator();
        while (customer.hasNext()) {
            CustomerRemote customer_remote = (CustomerRemote) PortableRemoteObject.narrow(customer.next(), CustomerRemote.class);
            customers_id.add(customer_remote.getId());
        }
        ArrayList result = new ArrayList();
        result.add(new Integer(80));
        result.add(new Integer(84));
        result.add(new Integer(88));
        result.add(new Integer(92));
        result.add(new Integer(96));
        result.add(new Integer(60));
        result.add(new Integer(100));
        result.add(new Integer(104));
        result.add(new Integer(108));

        assertTrue("Wrong result EJBQL customerhome.findWithoutReservationsAndWithGoodCredit() (required:"
                   + result + ", found:" + customers_id + ")",
                   isCollectionEqual(result,customers_id));

        //checkIsInitialState(); not necessary in this test
    }

    /**
     * 	Finding Customers Without Reservations and with good credit
     *  Test equivalent to testIsEmptyInExp1
     *  SELECT OBJECT(c) FROM jt2_Customer c
     *    WHERE NOT (c.reservations IS NOT EMPTY OR c.hasGoodCredit <> true)
     */
    public void testIsEmptyInExp1Bis() throws Exception {
        Collection customers = customerhome.findBisWithoutReservationsAndWithGoodCredit(true);

        ArrayList customers_id  = new ArrayList();
        Iterator customer = customers.iterator();
        while (customer.hasNext()) {
            CustomerRemote customer_remote = (CustomerRemote) PortableRemoteObject.narrow(customer.next(), CustomerRemote.class);
            customers_id.add(customer_remote.getId());
        }
        ArrayList result = new ArrayList();
        result.add(new Integer(80));
        result.add(new Integer(84));
        result.add(new Integer(88));
        result.add(new Integer(92));
        result.add(new Integer(96));
        result.add(new Integer(60));
        result.add(new Integer(100));
        result.add(new Integer(104));
        result.add(new Integer(108));

        assertTrue("Wrong result EJBQL customerhome.findBisWithoutReservationsAndWithGoodCredit() (required:"
                   + result + ", found:" + customers_id + ")",
                   isCollectionEqual(result,customers_id));

        //checkIsInitialState(); not necessary in this test
    }

    /**
     * 	Finding Customers Without Reservations and with good credit
     *  Test equivalent to testIsEmptyInExp1
     *  SELECT OBJECT(c) FROM jt2_Customer c
     *    WHERE NOT ( NOT (c.reservations IS EMPTY AND c.hasGoodCredit = true) )
     */
    public void testIsEmptyInExp1Ter() throws Exception {
        Collection customers = customerhome.findTerWithoutReservationsAndWithGoodCredit(true);

        ArrayList customers_id  = new ArrayList();
        Iterator customer = customers.iterator();
        while (customer.hasNext()) {
            CustomerRemote customer_remote = (CustomerRemote) PortableRemoteObject.narrow(customer.next(), CustomerRemote.class);
            customers_id.add(customer_remote.getId());
        }
        ArrayList result = new ArrayList();
        result.add(new Integer(80));
        result.add(new Integer(84));
        result.add(new Integer(88));
        result.add(new Integer(92));
        result.add(new Integer(96));
        result.add(new Integer(60));
        result.add(new Integer(100));
        result.add(new Integer(104));
        result.add(new Integer(108));

        assertTrue("Wrong result EJBQL customerhome.findTerWithoutReservationsAndWithGoodCredit() (required:"
                   + result + ", found:" + customers_id + ")",
                   isCollectionEqual(result,customers_id));

        //checkIsInitialState(); not necessary in this test
    }

    /**
     * 	Finding Customers Without Reservations and with good credit
     *  Test equivalent to testIsEmptyInExp1
     *  SELECT OBJECT(c) FROM jt2_Customer c
     *    WHERE NOT (c.hasGoodCredit <> true) AND c.reservations IS EMPTY
     */
    public void testIsEmptyInExp1Quad() throws Exception {
        Collection customers = customerhome.findQuadWithoutReservationsAndWithGoodCredit(true);

        ArrayList customers_id  = new ArrayList();
        Iterator customer = customers.iterator();
        while (customer.hasNext()) {
            CustomerRemote customer_remote = (CustomerRemote) PortableRemoteObject.narrow(customer.next(), CustomerRemote.class);
            customers_id.add(customer_remote.getId());
        }
        ArrayList result = new ArrayList();
        result.add(new Integer(80));
        result.add(new Integer(84));
        result.add(new Integer(88));
        result.add(new Integer(92));
        result.add(new Integer(96));
        result.add(new Integer(60));
        result.add(new Integer(100));
        result.add(new Integer(104));
        result.add(new Integer(108));

        assertTrue("Wrong result EJBQL customerhome.findQuadWithoutReservationsAndWithGoodCredit() (required:"
                   + result + ", found:" + customers_id + ")",
                   isCollectionEqual(result,customers_id));

        //checkIsInitialState(); not necessary in this test
    }

    /**
     * Test to reproduce bug #300525
     * 	Finding Customers Without Reservations OR with good credit
     *  SELECT OBJECT(c) FROM jt2_Customer c
     *    WHERE c.reservations IS EMPTY OR c.hasGoodCredit == true
     */
    public void testIsEmptyInExp2() throws Exception {
        Collection customers = customerhome.findWithoutReservationsOrWithGoodCredit(true);

        ArrayList customers_id  = new ArrayList();
        Iterator customer = customers.iterator();
        while (customer.hasNext()) {
            CustomerRemote customer_remote = (CustomerRemote) PortableRemoteObject.narrow(customer.next(), CustomerRemote.class);
            customers_id.add(customer_remote.getId());
        }
        ArrayList result = new ArrayList();
        for (int i=51;i<=69;i=i+3) {
            result.add(new Integer(i));
        }
        for (int i=80;i<=109;i++) {
            result.add(new Integer(i));
        }
        result.add(new Integer(202));
        result.add(new Integer(204));
        result.add(new Integer(206));
        result.add(new Integer(52));
        result.add(new Integer(56));
        result.add(new Integer(64));
        result.add(new Integer(68));

        assertTrue("Wrong result EJBQL customerhome.findWithoutReservationsOrWithGoodCredit() (required:"
                   + result + ", found:" + customers_id + ")",
                   isCollectionEqual(result,customers_id));

        //checkIsInitialState(); not necessary in this test
    }

    /**
     * 	Finding the customer with a specific phone number
     *  SELECT OBJECT(c) FROM jt2_Customer cust WHERE ?1 IS MEMBER OF cust.phoneNumbers
     * This test is done via a session bean because it need to use Local interfaces.
     */
    public void testSimpleMemberOf() throws Exception {
        SessionTestRemote sTest = sessiontesthome.create();
        int custId = sTest.getCustomerWithPhone("612-555-1280").intValue();;
        assertEquals("Wrong result EJBQL session.getCustomerWithPhone(612-555-1280): ",
                     80, custId);

        // checkIsInitialState(); not necessary in this test
    }

    /**
     * 	Finding Customers On Alaska Cruise
     *  SELECT OBJECT(c) FROM jt2_Customer cust, Cruise cr, IN(cr.reservations) res
     *  WHERE cr = ?1 AND cust MEMBER OF res.customers
     */
    public void testMemberOfNavigate() throws Exception {
        CruiseRemote crA = cruisehome.findByName("Alaska Cruise");
        Collection customers_id = customerhome.callFindOnCruise(crA.getId());

        ArrayList result = new ArrayList();
        result.add(new Integer(201));
        result.add(new Integer(202));
        result.add(new Integer(203));
        result.add(new Integer(204));
        result.add(new Integer(205));
        result.add(new Integer(206));
        result.add(new Integer(52));
        result.add(new Integer(55));
        result.add(new Integer(58));
        result.add(new Integer(61));
        result.add(new Integer(64));
        result.add(new Integer(67));

        assertTrue("Wrong result EJBQL customerhome.findOnCruise(crA) (required:" + result + ", found:" +customers_id+
                   ")", isCollectionEqual(result,customers_id));

        // checkIsInitialState(); not necessary in this test
    }

    /**
     * 	Finding ship by tonnage
     *  SELECT OBJECT(c) FROM jt2_Ship s
     *  WHERE s.tonnage = ?1
     */
    public void testEgalDouble() throws Exception {
        Collection ships = shiphome.findByTonnage(new Double(100000));

        ArrayList ships_id  = new ArrayList();
        Iterator ship = ships.iterator();
        while (ship.hasNext()) {
            ShipRemote ship_remote = (ShipRemote) PortableRemoteObject.narrow(ship.next(), ShipRemote.class);
            ships_id.add(ship_remote.getId());
        }
        ArrayList result = new ArrayList();
        result.add(new Integer(7));


        assertTrue("Wrong result EJBQL shiphome.findByTonnage(new Double(100000)) (required:" + result + ", found:" +ships_id+ ")", isCollectionEqual(result,ships_id));

        // checkIsInitialState(); not necessary in this test
    }


    /**
     * 	Finding ship by tonnage
     *  SELECT OBJECT(c) FROM jt2_Ship s
     *  WHERE s.tonnage BETWEEN ?1 AND ?2
     */
    public void testBetweenDouble() throws Exception {
        Collection ships = shiphome.findByTonnage(new Double(100000),new Double(130000));

        ArrayList ships_id  = new ArrayList();
        Iterator ship = ships.iterator();
        while (ship.hasNext()) {
            ShipRemote ship_remote = (ShipRemote) PortableRemoteObject.narrow(ship.next(), ShipRemote.class);
            ships_id.add(ship_remote.getId());
        }
        ArrayList result = new ArrayList();
        result.add(new Integer(7));
        result.add(new Integer(8));
        result.add(new Integer(9));
        result.add(new Integer(10));

        assertTrue("Wrong result EJBQL shiphome.findByTonnage(new Double(100000),new Double(130000)) (required:" + result + ", found:" +ships_id+ ")", isCollectionEqual(result,ships_id));

        // checkIsInitialState(); not necessary in this test
    }

    /**
     * A complex navigation test
     * SELECT OBJECT(c) FROM jt2_Customer c WHERE c.creditCard.creditCompany.address.city = ?1
     */
    public void testNavigate() throws Exception {


        Collection customers = customerhome.findAllCreditCardAddress("Minneapolis");

        ArrayList customers_id  = new ArrayList();
        Iterator customer = customers.iterator();
        while (customer.hasNext()) {
            CustomerRemote customer_remote = (CustomerRemote) PortableRemoteObject.narrow(customer.next(), CustomerRemote.class);
            customers_id.add(customer_remote.getId());
        }
        ArrayList result = new ArrayList();
        result.add(new Integer(80));
        result.add(new Integer(84));
        result.add(new Integer(88));
        result.add(new Integer(92));
        result.add(new Integer(96));

        assertTrue("Wrong result EJBQL customerhome.findAllCreditCardAddress() (required:" + result + ", found:" +customers_id+
                   ")", isCollectionEqual(result,customers_id));

        checkIsInitialState();

    }

    /**
     * An other complex navigation test and the OR operator
     * SELECT OBJECT(c) FROM jt2_Customer AS c WHERE c.creditCard.number = ?1 OR c.homeAddress.zip = ?2
     */
    public void testNavigateOr() throws Exception {


        Collection customers = customerhome.findCustWithCCNumOrAddrZip("10000184", "55404");

        ArrayList customers_id  = new ArrayList();
        Iterator customer = customers.iterator();
        while (customer.hasNext()) {
            CustomerRemote customer_remote = (CustomerRemote) PortableRemoteObject.narrow(customer.next(), CustomerRemote.class);
            customers_id.add(customer_remote.getId());
        }
        ArrayList result = new ArrayList();
        result.add(new Integer(84));
        result.add(new Integer(88));

        assertTrue("Wrong result EJBQL customerhome.findCustWithCCNumOrAddrZip() (required:" + result + ", found:" +customers_id+
                   ")", isCollectionEqual(result,customers_id));

        checkIsInitialState();

    }

    /**
     * 	Finding all Customer with reservations (there will be duplication in result if customer has more than one reservation
     *
     *  SELECT OBJECT(c) FROM jt2_Reservation res, IN(res.customers) c
     */
    public void testInFrom() throws Exception {

        Collection customers = customerhome.findAllCustomersWithReservation();

        ArrayList customers_id  = new ArrayList();
        Iterator customer = customers.iterator();
        while (customer.hasNext()) {
            CustomerRemote customer_remote = (CustomerRemote) PortableRemoteObject.narrow(customer.next(), CustomerRemote.class);
            customers_id.add(customer_remote.getId());
        }
        ArrayList result = new ArrayList();

        result.add(new Integer(201));
        result.add(new Integer(201));
        result.add(new Integer(202));
        result.add(new Integer(202));
        result.add(new Integer(203));
        result.add(new Integer(203));
        result.add(new Integer(204));
        result.add(new Integer(204));
        result.add(new Integer(205));
        result.add(new Integer(205));
        result.add(new Integer(206));
        result.add(new Integer(206));
        result.add(new Integer(50));
        result.add(new Integer(52));
        result.add(new Integer(53));
        result.add(new Integer(55));
        result.add(new Integer(56));
        result.add(new Integer(58));
        result.add(new Integer(59));
        result.add(new Integer(61));
        result.add(new Integer(62));
        result.add(new Integer(64));
        result.add(new Integer(65));
        result.add(new Integer(67));
        result.add(new Integer(68));

        assertTrue("Wrong result EJBQL customerhome.findAllCustomersWithReservation (required:" + result + ", found:" +customers_id+
                   ")", isCollectionEqual(result,customers_id));

        // checkIsInitialState();  not necessary in this test
    }

    /**
     * 	Finding all Customer with reservations (there will not be duplication in result)
     *
     *  SELECT DISTINCT OBJECT(c) FROM jt2_Reservation res, IN(res.customers) c
     */
    public void testDistinctInFrom() throws Exception {

        Collection customers = customerhome.findAllCustomersWithReservationDistinct();

        ArrayList customers_id  = new ArrayList();
        Iterator customer = customers.iterator();
        while (customer.hasNext()) {
            CustomerRemote customer_remote = (CustomerRemote) PortableRemoteObject.narrow(customer.next(), CustomerRemote.class);
            customers_id.add(customer_remote.getId());
        }
        ArrayList result = new ArrayList();

        result.add(new Integer(201));
        result.add(new Integer(202));
        result.add(new Integer(203));
        result.add(new Integer(204));
        result.add(new Integer(205));
        result.add(new Integer(206));
        result.add(new Integer(50));
        result.add(new Integer(52));
        result.add(new Integer(53));
        result.add(new Integer(55));
        result.add(new Integer(56));
        result.add(new Integer(58));
        result.add(new Integer(59));
        result.add(new Integer(61));
        result.add(new Integer(62));
        result.add(new Integer(64));
        result.add(new Integer(65));
        result.add(new Integer(67));
        result.add(new Integer(68));

        assertTrue("Wrong result EJBQL customerhome.findAllCustomersWithReservationDistinct (required:" + result + ", found:" +customers_id+
                   ")", isCollectionEqual(result,customers_id));

        // checkIsInitialState(); not necessary in this test
    }

    /**
     * Test for feature #300040
     * 	Finding Customer by address
     *
     * 	SELECT OBJECT(c) FROM jt2_Customer c WHERE c.homeAddress = ?1
     */
    public void _testBeanAsParameter() throws Exception {

        AddressRemote add = addresshome.findByPrimaryKey(new Integer(10));

        Collection customers = customerhome.findByAddress(add);

        ArrayList customers_id  = new ArrayList();
        Iterator customer = customers.iterator();
        while (customer.hasNext()) {
            CustomerRemote customer_remote = (CustomerRemote) PortableRemoteObject.narrow(customer.next(), CustomerRemote.class);
            customers_id.add(customer_remote.getId());
        }
        ArrayList result = new ArrayList();

        result.add(new Integer(80));

        assertTrue("Wrong result EJBQL customerhome.findByAddress(add)(required:" + result + ", found:" +customers_id+
                   ")", isCollectionEqual(result,customers_id));

        // checkIsInitialState(); not necessary in this test
    }

    /**
     * Test for feature #300040
     * 	Finding Customer by address
     *
     * 	SELECT OBJECT(c) FROM jt2_Customer c WHERE c.homeAddress = ?1
     */
    public void _testBeanNullAsParameter() throws Exception {

        Collection customers = customerhome.findByAddress(null);

        ArrayList customers_id  = new ArrayList();
        Iterator customer = customers.iterator();
        while (customer.hasNext()) {
            CustomerRemote customer_remote = (CustomerRemote) PortableRemoteObject.narrow(customer.next(), CustomerRemote.class);
            customers_id.add(customer_remote.getId());
        }
        ArrayList result = new ArrayList();

        assertTrue("Wrong result EJBQL customerhome.findByAddress(add)(required:" + result + ", found:" +customers_id+
                   ")", isCollectionEqual(result,customers_id));

        // checkIsInitialState(); not necessary in this test
    }

    /**
     * 	Finding All Customer
     *
     * 	SELECT OBJECT(c) FROM jt2_Customer AS c
     */
    public void testFindCustomerAll() throws Exception {

        Collection customers = customerhome.findCustomersAll();

        ArrayList customers_id  = new ArrayList();
        Iterator customer = customers.iterator();
        while (customer.hasNext()) {
            CustomerRemote customer_remote = (CustomerRemote) PortableRemoteObject.narrow(customer.next(), CustomerRemote.class);
            customers_id.add(customer_remote.getId());
        }
        ArrayList result = new ArrayList();

        for (int i=80;i<=99;i++) {
            result.add(new Integer(i));
        }
        for (int j=201;j<=206;j++) {
            result.add(new Integer(j));
        }
        for (int k=50;k<=69;k++) {
            result.add(new Integer(k));
        }
        for (int l=100;l<=109;l++) {
            result.add(new Integer(l));
        }

        assertTrue("Wrong result EJBQL customerhome.findCustomersAll()(required:" + result + ", found:" +customers_id+
                   ")", isCollectionEqual(result,customers_id));

        // checkIsInitialState(); not necessary in this test
    }

    /**
     * 	Finding Customers by AddressId
     *  SELECT OBJECT(c) FROM jt2_Customer c WHERE c.homeAddress.id = ?1
     */
    public void testFindCustByAddrId() throws Exception {
        Collection customers = customerhome.findByAddressId(new Integer(10));

        ArrayList customers_id  = new ArrayList();
        Iterator customer = customers.iterator();
        while (customer.hasNext()) {
            CustomerRemote customer_remote = (CustomerRemote) PortableRemoteObject.narrow(customer.next(), CustomerRemote.class);
            customers_id.add(customer_remote.getId());
        }
        ArrayList result = new ArrayList();
        result.add(new Integer(80));

        assertTrue("Wrong result EJBQL customerhome.findByAddressId(10) (required:" + result + ", found:" +customers_id+
                   ")", isCollectionEqual(result,customers_id));

        // checkIsInitialState(); not necessary in this test
    }

    /**
     * Bug #300626
     * SELECT OBJECT(a) FROM jt2_Address AS a
     *		  WHERE a.homecustomer.lastName = 'Smith80'
     *		  AND a.homecustomer.creditCard.id = 10
     *		  AND a.homecustomer.creditCard.creditCompany.name = 'CETELEM'
     *		  AND a.homecustomer.creditCard.creditCompany.id = 10
     */
    public void testAddrQuery300626() throws Exception {
        AddressRemote a = addresshome.findQuery300626();
        assertEquals("Wrong result EJBQL : addresshome.findQuery300626()", 10, a.getId().intValue());
    }

    /**
     * 	Finding Customers by AddressLocal
     *  SELECT OBJECT(c) FROM jt2_Customer c WHERE c.homeAddress = ?1
     */
    public void testBeanLocalAsParameter() throws Exception {
        Collection customers = customerhome.callFindByAddressLocal(new Integer(10));

        ArrayList customers_id  = new ArrayList();
        Iterator customer = customers.iterator();
        while (customer.hasNext()) {
            customers_id.add((Integer)customer.next());
        }
        ArrayList result = new ArrayList();
        result.add(new Integer(80));

        assertTrue("Wrong result EJBQL customerhome.callFindByAddressLocal(10) (required:" + result + ", found:" +customers_id+
                   ")", isCollectionEqual(result,customers_id));

        // checkIsInitialState(); not necessary in this test
    }

    /**
     * 	Finding Customers by AddressLocal
     *  SELECT OBJECT(c) FROM jt2_Customer c WHERE c.homeAddress = ?1
     */
    public void testBeanLocalNullAsParameter() throws Exception {
        Collection customers = customerhome.callFindByAddressLocal(null);

        ArrayList customers_id  = new ArrayList();
        Iterator customer = customers.iterator();
        while (customer.hasNext()) {
            customers_id.add((Integer)customer.next());
        }
        ArrayList result = new ArrayList();

        assertTrue("Wrong result EJBQL customerhome.callFindByAddressLocal(null) (required:" + result + ", found:" +customers_id+
                   ")", isCollectionEqual(result,customers_id));

        // checkIsInitialState(); not necessary in this test
    }

    /**
     * Test a finder method
     * - which have no parameter and which have its name starts with 'findAll',
     * - but which not means "findAll".
     * SELECT OBJECT(c) FROM jt2_Customer c WHERE c.firstName LIKE 'Mike'
     */
    public void testFindAllMike() throws Exception {
        Collection customers = customerhome.findAllMike();
        ArrayList customers_id  = new ArrayList();
        Iterator customer = customers.iterator();
        while (customer.hasNext()) {
            CustomerRemote customer_remote = (CustomerRemote) PortableRemoteObject.narrow(customer.next(), CustomerRemote.class);
            assertEquals("findAllMike() wrong result: ",
                         "Mike", customer_remote.getName().getFirstName());
        }
    }

    /**
     * Test a finder method with the IS NOT NULL operator on a cmr field.
     * SELECT OBJECT(c) FROM jt2_Customer c WHERE c.creditCard IS NOT NULL
     */
    public void testIsNull1() throws Exception {
        Collection customers = customerhome.findCustomersWithCreditCard();
        ArrayList customers_id  = new ArrayList();
        Iterator customer = customers.iterator();
        while (customer.hasNext()) {
            CustomerRemote customer_remote = (CustomerRemote) PortableRemoteObject.narrow(customer.next(), CustomerRemote.class);
            customers_id.add(customer_remote.getId());
        }
        ArrayList resExpected = new ArrayList();
        resExpected.add(new Integer(80));
        resExpected.add(new Integer(84));
        resExpected.add(new Integer(88));
        resExpected.add(new Integer(92));
        resExpected.add(new Integer(96));
        assertTrue("Wrong result EJBQL customerhome.findCustomersWithCreditCard (required:"
                   + resExpected + ", found:" + customers_id + ")",
                   isCollectionEqual(resExpected,customers_id));
    }

    /**
     * Test a finder method with the IS NULL operator on a cmr field.
     * SELECT OBJECT(c) FROM jt2_Customer c WHERE c.creditCard IS NULL
     */
    public void testIsNull1bis() throws Exception {
        Collection customers = customerhome.findCustomersWithOutCreditCard();
        ArrayList customers_id  = new ArrayList();
        Iterator customer = customers.iterator();
        while (customer.hasNext()) {
            CustomerRemote customer_remote = (CustomerRemote) PortableRemoteObject.narrow(customer.next(), CustomerRemote.class);
            customers_id.add(customer_remote.getId());
        }
        ArrayList resExpected = new ArrayList();
        for (int i = 50; i <= 69; i++) {
            resExpected.add(new Integer(i));
        }
        for (int i = 81; i <= 83; i++) {
            resExpected.add(new Integer(i));
        }
        for (int i = 85; i <= 87; i++) {
            resExpected.add(new Integer(i));
        }
        for (int i = 89; i <= 91; i++) {
            resExpected.add(new Integer(i));
        }
        for (int i = 93; i <= 95; i++) {
            resExpected.add(new Integer(i));
        }
        for (int i = 97; i <= 109; i++) {
            resExpected.add(new Integer(i));
        }
        for (int i = 201; i <= 206; i++) {
            resExpected.add(new Integer(i));
        }
        assertTrue("Wrong result EJBQL customerhome.findCustomersWithOutCreditCard (required:"
                   + resExpected + ", found:" + customers_id + ")",
                   isCollectionEqual(resExpected,customers_id));
    }

    /**
     * Test a finder method with the IS NULL operator on a cmr field in an expression.
     * SELECT OBJECT(c) FROM jt2_Customer c WHERE c.firstName = ?1 AND c.creditCard IS NULL
     */
    public void testIsNull1ter() throws Exception {
        Collection customers = customerhome.findCustomersXWithOutCreditCard("John");
        assertEquals("Wrong result EJBQL customerhome.findCustomersXWithOutCreditCard: ", 19, customers.size());

    }

    /**
     * Test a finder method with the IS NOT NULL operator on a cmr field with a complex navigation.
     * SELECT OBJECT(c) FROM jt2_Customer c WHERE c.creditCard.creditCompany IS NOT NULL
     */
    public void testIsNull2() throws Exception {
        Collection customers = customerhome.findCustomersWithCreditCompany();
        ArrayList customers_id  = new ArrayList();
        Iterator customer = customers.iterator();
        while (customer.hasNext()) {
            CustomerRemote customer_remote = (CustomerRemote) PortableRemoteObject.narrow(customer.next(), CustomerRemote.class);
            customers_id.add(customer_remote.getId());
        }
        ArrayList resExpected = new ArrayList();
        resExpected.add(new Integer(80));
        resExpected.add(new Integer(84));
        resExpected.add(new Integer(88));
        resExpected.add(new Integer(92));
        resExpected.add(new Integer(96));
        assertTrue("Wrong result EJBQL customerhome.findCustomersWithCreditCompany (required:"
                   + resExpected + ", found:" + customers_id + ")",
                   isCollectionEqual(resExpected,customers_id));
    }

    /**
     * Test a finder method with the IS NULL operator on a parameter which is a bean null.
     * SELECT OBJECT(c) FROM jt2_Customer c WHERE ?1 IS NULL
     */
    public void testIsNull3() throws Exception {
        Collection customers = customerhome.callFindByParameterIsNull(null);
        assertEquals("Wrong result EJBQL customerhome.callFindByParameterIsNull(null): ",
                     56, customers.size());
    }

    /**
     * Test a finder method with the IS NULL operator on a parameter which is a bean not null.
     * SELECT OBJECT(c) FROM jt2_Customer c WHERE ?1 IS NULL
     */
    public void testIsNull3bis() throws Exception {
        Collection customers = customerhome.callFindByParameterIsNull(new Integer(10));
        assertEquals("Wrong result EJBQL customerhome.callFindByParameterIsNull(): ",
                     0, customers.size());
    }

    /**
     * Select the lastName of a customer
     * SELECT c.lastName FROM jt2_Customer AS c WHERE c.id = ?1
     */
    public void testCustEjbSelectLastName() throws Exception {
        assertEquals("Wrong ejbSelectLastName for c.id=80: ",
                     "Smith80", customerhome.selectLastName(new Integer(80)));
    }

    /**
     * Select the city of the address of a customer
     * SELECT c.homeAddress.city FROM jt2_Customer AS c WHERE c.id = ?1
     */
    public void testCustEjbSelectAddrCity() throws Exception {
        assertEquals("Wrong ejbSelectAddrCity for c.id=80: ",
                     "Minneapolis", customerhome.selectAddrCity(new Integer(80)));
    }

    /**
     * Select the deckLevel of a cabin
     * SELECT c.deckLevel FROM jt2_Cabin AS c WHERE c.id = ?1
     */
    public void testCabinEjbSelectDeckLevel() throws Exception {
        assertEquals("Wrong ejbSelectDeckLevel for c.id=21100: ",
                     1, cabinhome.selectDeckLevel(new Integer(21100)));
    }

    /**
     * Select the firstName of all the customers.
     * SELECT c.firstName FROM jt2_Customer AS c
     */
    public void testCustEjbSelectFirstName() throws Exception {
        Set lfn = customerhome.selectFirstName();
        Iterator ilfn = lfn.iterator();
        StringBuffer slfn = new StringBuffer();
        while (ilfn.hasNext()) {
            slfn = slfn.append((String) ilfn.next() + ",");
        }
        assertEquals("Wrong ejbSelectFirstName() number results (" + slfn.toString() + "): ",
                     6, lfn.size());
        if (!lfn.contains("John")
            || !lfn.contains("Mike")
            || !lfn.contains("Paul")
            || !lfn.contains("Ringo")
            || !lfn.contains("Roger")
            || !lfn.contains("Joe")) {
            fail("Wrong ejbSelectFirstName() result (" + slfn.toString()
                 + "): does not contain 'John', 'Mike' or 'Paul' or 'Ringo' or 'Roger' or 'Joe'");
        }
    }

    /**
     * Select the DISTINCT firstName of all the customers.
     * SELECT DISTINCT c.firstName FROM jt2_Customer AS c
     */
    public void testCustEjbSelectDistinctFirstName() throws Exception {
        Collection lfn = customerhome.selectDistinctFirstName();
        assertEquals("Wrong ejbSelectDistinctFirstName() number results (" + lfn.toString() + "): ",
                     6, lfn.size());
        if (!lfn.contains("John")
            || !lfn.contains("Mike")
            || !lfn.contains("Paul")
            || !lfn.contains("Ringo")
            || !lfn.contains("Roger")
            || !lfn.contains("Joe")) {
            fail("Wrong ejbSelectDistinctDistinctFirstName() result (" + lfn.toString()
                 + "): does not contain 'John', 'Mike' or 'Paul' or 'Ringo' or 'Roger' or 'Joe'");
        }
    }

    /**
     * Select the address of a customer
     * An ejbSelect with a local bean as result
     * SELECT c.homeAddress FROM jt2_Customer AS c WHERE c.id = ?1
     */
    public void testCustEjbSelectAddr() throws Exception {
        assertEquals("Wrong ejbSelectAddr for c.id=80: ",
                     new Integer(10), customerhome.selectAddr(new Integer(80)));
    }

    /**
     * Select the addresses of the company of the credit card of all the customers.
     * An ejbSelect with a collection of local bean as result
     * SELECT c.creditCard.creditCompany.address FROM jt2_Customer c
     */
    public void testCustEjbSelectAllCreditCartAddr() throws Exception {
        Collection addr = customerhome.selectAllCreditCardAddr();
        StringBuffer sAddr  = new StringBuffer();
        Iterator iAddr = addr.iterator();
        while (iAddr.hasNext()) {
            sAddr = sAddr.append(((Integer) iAddr.next()).toString() + ",");
        }
        ArrayList eAddr = new ArrayList();
        eAddr.add(new Integer(11));
        eAddr.add(new Integer(16));
        eAddr.add(new Integer(21));
        eAddr.add(new Integer(26));
        eAddr.add(new Integer(31));
        assertTrue("Wrong ejbSelectCreditCardAddr: (" + sAddr + "): ",
                   isCollectionEqual(eAddr, addr));
    }

    /**
     * Select the adresses of all 'roger' customers.
     * An ejbSelect with a collection of local bean as result
     * SELECT c.homeAddress FROM jt2_Customer c WHERE c.firstName = 'Roger'
     */
    public void testCustEjbSelectRogerAddr() throws Exception {
        Collection addr = customerhome.selectRogerAddr();
        StringBuffer sAddr  = new StringBuffer();
        Iterator iAddr = addr.iterator();
        while (iAddr.hasNext()) {
            sAddr = sAddr.append(((Integer) iAddr.next()).toString() + ",");
        }
        ArrayList eAddr = new ArrayList();
        eAddr.add(new Integer(45));
        eAddr.add(new Integer(50));
        eAddr.add(new Integer(55));
        eAddr.add(new Integer(60));
        assertTrue("Wrong ejbCustSelectRogerAddr: (" + sAddr + "): ",
                   isCollectionEqual(eAddr, addr));
    }

    /**
     * Find all the customers ordered by their lastname.
     * SELECT OBJECT(c) FROM jt2_Customer c ORDER BY c.lastName
     */
    public void testSimpleOrderBy() throws Exception {
        Collection customers = customerhome.findAllOrderByLastname();
        assertEquals("Wrong customers number for customerhome.findAllOrderByLastname(): ",
                     56, customers.size());
        Iterator ci = customers.iterator();
        String previous = ((CustomerRemote) PortableRemoteObject.narrow(ci.next(), CustomerRemote.class)).getName().getLastName();
        while (ci.hasNext()) {
            String lname = ((CustomerRemote) PortableRemoteObject.narrow(ci.next(), CustomerRemote.class)).getName().getLastName();
            assertTrue("Wrong ordered (previous=" + previous + ",current=" + lname + ")",
                       previous.compareTo(lname)<=0);
            previous = lname;
        }
    }

    /**
     * Find all the customers ordered by their lastname (desc) and firstname (asc).
     * SELECT OBJECT(c) FROM jt2_Customer c ORDER BY c.lastName DESC, c.firstName ASC
     */
    public void testOrderByDescAsc() throws Exception {
        Collection customers = customerhome.findAllOrderByDescLastnameAscFirstname();
        assertEquals("Wrong customers number for customerhome.findAllOrderByLastname(): ",
                     56, customers.size());
        Iterator ci = customers.iterator();
        Name previous = ((CustomerRemote) PortableRemoteObject.narrow(ci.next(), CustomerRemote.class)).getName();
        while (ci.hasNext()) {
            Name current = ((CustomerRemote) PortableRemoteObject.narrow(ci.next(), CustomerRemote.class)).getName();
            assertTrue("Wrong ordered (previous=" + previous + ",current=" + current + ")",
                       previous.getLastName().compareTo(current.getLastName())>=0);
            if (previous.getLastName().equals(current.getLastName())) {
                assertTrue("Wrong ordered (previous=" + previous + ",current=" + current + ")",
                           previous.getFirstName().compareTo(current.getFirstName())<=0);
            }
            previous = current;
        }

    }

    /**
     * Find the reservations ordered by their amountPaid.
     * The DISTINCT aggregate is used to reproduce the bug #300527
     * SELECT DISTINCT OBJECT(r) FROM jt2_Reservation AS r ORDER BY r.amountPaid
     * (bug #300527)
     */
    public void testOrderBy2() throws Exception {
        Collection reservations = reservationhome.findOrderedRsrv();
        assertEquals("Wrong reservations number for reservationhome.findOrderedRsrv(): ",
                     25, reservations.size());
    }

    /**
     * Get the average of the tonnage of all the ships.
     * SELECT AVG(s.tonnage) FROM jt2_Ship AS s
     */
    public void testAverageofTonnage() throws Exception {
        assertEquals("AverageofTonnage: ", 80000.0D, shiphome.getAverageOfTonnage(), 0.1);
    }

    /**
     * Get the count of reservations.
     * SELECT COUNT(r) FROM jt2_Reservations AS r
     */
    public void testCountOfReservations() throws Exception {
        assertEquals("CountOfReservations: ", 25L, reservationhome.getCountOfReservations());
    }

    /**
     * bug #300723
     * Get the count of customers with a given id
     * SELECT COUNT(c) FROM jt2_Customer AS c WHERE c.id = ?1
     */
    public void testCountOfCustomersWithId1() throws Exception {
        assertEquals("CountOfCustomersWithId1: ", 1L, customerhome.getCountCustomersWithId1(new Integer(82)));
    }

    /**
     * bug #300723
     * Get the count of customers with a given id
     * SELECT COUNT(c) FROM jt2_Customer AS c WHERE c.id = ?1
     */
    public void testCountOfCustomersWithId2() throws Exception {
        assertEquals("CountOfCustomersWithId2: ", 1L, customerhome.getCountCustomersWithId2(82));
    }

    /**
     * Get the count of customers with a firstname 'Ringo'
     * SELECT COUNT(c) FROM jt2_Customer AS c WHERE c.firstName = 'Ringo'
     */
    public void testCountOfCustomersRingo() throws Exception {
        assertEquals("CountOfCustomersRingo: ", 4L, customerhome.getCountCustomersRingo());
    }

    /**
     * Get the customer with a given id
     * SELECT c FROM jt2_Customer AS c WHERE c.id = ?1
     */
    public void testCustFindById() throws Exception {
        assertEquals("CustFindById: ", new Integer(82), customerhome.findById(new Integer(82)).getId());
    }

    /**
     * Get the min value of the amount of the reservations.
     * SELECT MIN(r.amountPaid) FROM jt2_Reservations AS r
     */
    public void testMinAmountOfReservations() throws Exception {
        assertEquals("testMinAmountOfReservations: ",
                     2500.0, reservationhome.getMinAmountOfReservations(), 0.1);
    }

    /**
     * (Bug #300635)
     * Get the min value of the amount of the reservations for the cruise name.
     * SELECT MIN(r.amountPaid) FROM jt2_Reservations AS r WHERE r.cruise.name = ?1
     */
    public void testMinAmountForCruise() throws Exception {
        assertEquals("testMinAmountForCruise(Alaska Cruise): ",
                     2520.0, reservationhome.getMinAmountForCruise("Alaska Cruise"), 0.1);
    }

    /**
     * Get the values of the amounts of the reservations for the cruise name.
     * SELECT r.amountPaid FROM jt2_Reservations AS r WHERE r.cruise.name = ?1
     */
    public void testAmountsForCruise() throws Exception {
        assertEquals("testAmountsForCruise(Alaska Cruise): ",
                     18, reservationhome.getAmountsForCruise("Alaska Cruise").size());
    }

    /**
     * Get the max value of the amount of the reservations.
     * SELECT MAX(r.amountPaid) FROM jt2_Reservations AS r
     */
    public void testMaxAmountOfReservations() throws Exception {
        assertEquals("testMaxAmountOfReservations: ",
                     208100.0, reservationhome.getMaxAmountOfReservations(), 0.1);
    }

    /**
     * Get the count of all the reservations for a specific customer.
     * SELECT COUNT(r) FROM jt2_Reservation AS r WHERE ?1 MEMBER OF r.customers
     * (bug #300280)
     */
    public void testCountOfReservationsForCustomer() throws Exception {
        assertEquals("CountOfReservationsForCustomer: ",
                     2L, reservationhome.getCountOfReservationsForCustomer(new Integer(202)));
    }

    /**
     * Get the amount paid of all the reservations for a specific customer.
     * SELECT SUM(r.amountPaid) FROM jt2_Reservation AS r WHERE ?1 MEMBER OF r.customers
     * (bug #300280)
     */
    public void testAmountOfReservationsForCustomer() throws Exception {
        assertEquals("AmountOfReservationsForCustomer: ",
                     408100.0, reservationhome.getAmountOfReservationsForCustomer(new Integer(202)), 0.1);
    }

    /**
     * Get the count of credit card for customers
     * SELECT COUNT(c.creditCard) FROM jt2_Customer AS c
     * The result must be equivalent to
     *    SELECT COUNT(c.creditCard) FROM jt2_Customer AS c WHERE c.creditCard IS NOT NULL
     */
    public void testCustomersCountCreditCard() throws Exception {
        assertEquals("CustomersCountCreditCard: ", 5, customerhome.getCountCreditCard());
    }

    /**
     * Get the list of the number of the credit card for customers
     * SELECT DISTINCT c.creditCard.number FROM jt2_Customer AS c ORDER BY c.creditCard.number
     */
    public void testCustomersCreditCardNumbers() throws Exception {
        assertEquals("CustomersCreditCardNumbers: ", 5, customerhome.getCreditCardNumbers().size());
    }


    /**
     * Find all the addresses
     * SELECT OBJECT(a) FROM jt2_Address AS a
     */
    public void testFindAllAddresses() throws Exception {
        Collection addresses = addresshome.findAllAddress();
        Iterator iAddr = addresses.iterator();
        int nbAddr = 0;
        while (iAddr.hasNext()) {
            nbAddr++;
            iAddr.next();
        }
        assertEquals("testFindAllAddresses: ", 61, nbAddr);
    }

    /**
     * Get all the cities of addresses
     * SELECT a.city FROM jt2_Address AS a
     */
    public void testAddrGetAllCities() throws Exception {
        assertEquals("testAddrGetAllCities: ", 61, addresshome.getAllCities().size());
    }

    /**
     * Get all the creditCompanies of addresses
     * SELECT a.creditCompany FROM jt2_Address AS a
     */
    public void testAddrGetAllCreditCompanies() throws Exception {
        assertEquals("testAddrGetAllCreditCompanies: ", 5, addresshome.getAllCreditCompanies().size());
    }

    /**
     * Get all the creditCompany names of addresses.
     * This is a test about Null Values in the Query Result.
     * If the result of the query corresponds to a cmr-field or cmp-field not defined in term of java primitive type,
     * the container must include null value in the result.
     * See the EJB Specification version 2.1, chapter 11.2.7.1
     * SELECT a.creditCompany.name FROM jt2_Address AS a
     * (bug #3001174)
     */
    public void testAddrGetAllCreditCompanyNames() throws Exception {
        assertEquals("testAddrGetAllCreditCompanyNames: ", 61, addresshome.getAllCreditCompanyNames().size());
    }

    /**
     * Get all the creditCompany id of addresses.
     * This is a test about Null Values in the Query Result.
     * If the result of the query corresponds to a cmp-field not defined in term of java primitive type,
     * the container must include null value in the result.
     * See the EJB Specification version 2.1, chapter 11.2.7.1
     * SELECT a.creditCompany.id FROM jt2_Address AS a
     * (bug #301174)
     */
    public void testAddrGetAllCreditCompanyIds() throws Exception {
        assertEquals("testAddrGetAllCreditCompanyIds: ", 61, addresshome.getAllCreditCompanyIds().size());
    }

    /**
     * Get all the creditCompany id of addresses.
     * This is a test about Null Values in the Query Result.
     * If the result of the query corresponds to a cmp-field defined in term of java primitive type,
     * the container must not include null value in the result.
     * See the EJB Specification version 2.1, chapter 11.2.7.1
     * SELECT a.creditCompany.num FROM jt2_Address AS a
     * (bug #301174)
     */
    public void testAddrGetAllCreditCompanyNums() throws Exception {
        assertEquals("testAddrGetAllCreditCompanyNums: ", 5, addresshome.getAllCreditCompanyNums().size());
    }


    /**
     * Get the count of cities for addresses
     * SELECT COUNT(a.city) FROM jt2_Address AS a
     */
    public void testAddrCountOfCities() throws Exception {
        assertEquals("testAddrCountOfCities: ", 61L, addresshome.getCountOfCities());
    }

    /**
     * Get the count of DISTINCT cities for addresses
     * SELECT COUNT(DISTINCT a.city) FROM jt2_Address AS a
     */
    public void testAddrCountOfDistinctCities() throws Exception {
        assertEquals("testAddrCountOfDistinctCities: ", 6L, addresshome.getCountOfDistinctCities());
    }

    /**
     * Find the reservations with lesser amount.
     * SELECT DISTINCT OBJECT(r1) FROM jt2_Reservation AS r1, jt2_Reservation AS r2
     * WHERE r1.amountPaid &lt; r2.amountPaid AND r2.id = 26
     */
    public void testFindResWithLesserAmount() throws Exception {
        Collection reservations = reservationhome.findReservationsWithLesserAmount();
        assertEquals("Wrong reservations number for reservationhome.findReservationsWithLesserAmount(): ",
                     4, reservations.size());
    }

    /**
     * 	Finding All Customer with a limiter startAt range
     *
     * 	SELECT OBJECT(c) FROM jt2_Customer AS c WHERE c.id > 0 ORDER BY c.id LIMIT ?1
     */
    public void testFindCustAllLimit_1() throws Exception {

        Collection customers = customerhome.findAllWithLimit_1(40);

        ArrayList customers_id  = new ArrayList();
        Iterator customer = customers.iterator();
        while (customer.hasNext()) {
            CustomerRemote customer_remote = (CustomerRemote) PortableRemoteObject.narrow(customer.next(), CustomerRemote.class);
            customers_id.add(customer_remote.getId());
        }
        ArrayList result = new ArrayList();
        for (int l=100;l<=109;l++) {
            result.add(new Integer(l));
        }
        for (int j=201;j<=206;j++) {
            result.add(new Integer(j));
        }

        assertTrue("Wrong result EJBQL customerhome.findAllWithLimit_1()(required:" + result + ", found:" +customers_id+
                   ")", isCollectionEqual(result,customers_id));

        // checkIsInitialState(); not necessary in this test
    }

    /**
     * 	Finding All Customer with a limiter startAt range and a size range
     *
     * 	SELECT OBJECT(c) FROM jt2_Customer AS c ORDER BY c.id LIMIT ?1, 5
     */
    public void testFindCustAllLimit_2() throws Exception {

        Collection customers = customerhome.findAllWithLimit_2(40);

        ArrayList customers_id  = new ArrayList();
        Iterator customer = customers.iterator();
        while (customer.hasNext()) {
            CustomerRemote customer_remote = (CustomerRemote) PortableRemoteObject.narrow(customer.next(), CustomerRemote.class);
            customers_id.add(customer_remote.getId());
        }
        ArrayList result = new ArrayList();
        for (int l=100;l<=104;l++) {
            result.add(new Integer(l));
        }

        assertTrue("Wrong result EJBQL customerhome.findAllWithLimit_2()(required:" + result + ", found:" +customers_id+
                   ")", isCollectionEqual(result,customers_id));

        // checkIsInitialState(); not necessary in this test
    }



    public static Test suite() {
        return new TestSuite(F_EjbqlEC2.class);
    }

    public static void main (String args[]) {
        String testtorun = null;
        // Get args
        for (int argn = 0; argn < args.length; argn++) {
            String s_arg = args[argn];
            Integer i_arg;
            if (s_arg.equals("-n")) {
                testtorun = args[++argn];
            }
        }
        if (testtorun == null) {
            junit.textui.TestRunner.run(suite());
        } else {
            junit.textui.TestRunner.run(new F_EjbqlEC2(testtorun));
        }
    }
}

