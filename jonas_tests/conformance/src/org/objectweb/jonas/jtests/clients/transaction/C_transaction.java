/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.clients.transaction;

import junit.framework.Test;
import junit.framework.TestSuite;
import org.objectweb.jonas.jtests.util.JTestCase;

/**
 * link all the suites from this directory
 */
public class C_transaction extends JTestCase {

    public C_transaction(String name) {
        super(name);
    }

    public static Test suite() {
        TestSuite suite = new TestSuite();

        // put here the list of suites to run
        suite.addTest(F_BeanManagedTx.suite());
        suite.addTest(F_ContManagedTx.suite());
        suite.addTest(C_TxAttribute.suite());
        suite.addTest(F_BeanToBeanTx.suite());
        suite.addTest(F_BeanToLocalTx.suite());
        suite.addTest(F_EntityCMT.suite());
        suite.addTest(F_Jotm.suite());
        suite.addTest(F_State.suite());

        return suite;
    }

    public static void main (String args[]) {
        junit.textui.TestRunner.run(suite());
    }
}
