/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.clients.jms;

import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;
import junit.framework.Test;
import junit.framework.TestSuite;
import org.objectweb.jonas.jtests.util.JTestCase;
import org.objectweb.jonas.jtests.beans.message.Sender;
import org.objectweb.jonas.jtests.beans.message.SenderHome;
import org.objectweb.jonas.jtests.beans.message.Sender1_1;
import org.objectweb.jonas.jtests.beans.message.Sender1_1Home;

public class F_RollbackMDB extends JTestCase {

    private static String BEAN_HOME = "messageSenderSFHome";

    private static String BEAN1_1_HOME = "messageSender1_1SFHome";

    protected static SenderHome home = null;

    protected static Sender1_1Home home1 = null;

    public F_RollbackMDB(String name) {
        super(name);
    }

    public SenderHome getHome() {
        if (home == null) {
            try {
                home = (SenderHome) PortableRemoteObject.narrow(ictx.lookup(BEAN_HOME), SenderHome.class);
            } catch (NamingException e) {
                fail("Cannot get bean home");
            }
        }
        return home;
    }

    public Sender1_1Home getHome1() {
        if (home1 == null) {
            try {
                home1 = (Sender1_1Home) PortableRemoteObject.narrow(ictx.lookup(BEAN1_1_HOME), Sender1_1Home.class);
            } catch (NamingException e) {
                fail("Cannot get bean home1");
            }
        }
        return home1;
    }

    /**
     * init environment: - load beans - create/init database for entities.
     */
    protected void setUp() {
        super.setUp();
        useBeans("message", true);
    }

    // --------------------------------------------------------
    // Tests Rollback
    // --------------------------------------------------------

    /**
     * send a message on a topic in a transaction rolled back
     */
    public void testRollbackSendOnTopic1() throws Exception {
        Sender s = getHome().create();
        int val = 204;
        utx.begin();
        s.sendOnTopic("JunitTopic1", val, 1);
        utx.rollback();
        assertEquals(0, s.check(val, 2, 4));
        s.remove();
    }

    /**
     * send a message on a topic in a transaction rolled back via Sender1_1SF
     * and JMS1.1 interfaces
     */
    public void testRollbackSendOnDestTopic1() throws Exception {
        Sender1_1 s = getHome1().create();
        int val = 204;
        utx.begin();
        s.sendOnDestination("JunitTopic1", val, 1);
        utx.rollback();
        assertEquals(0, s.check(val, 2, 4));
        s.remove();
    }

    /**
     * send a message on a queue in a transaction rolled back MDB transacted.
     */
    public void testRollbackSendOnQueue1() throws Exception {
        Sender s = getHome().create();
        int val = 104;
        utx.begin();
        s.sendOnQueue("JunitQueue1", val, 1);
        utx.rollback();
        assertEquals(0, s.check(val, 1, 4));
        s.remove();
    }

    /**
     * send a message on a queue in a transaction rolled back MDB transacted.
     * via Sender1_1SF and JMS1.1 interfaces
     */
    public void testRollbackSendOnDestQueue1() throws Exception {
        Sender1_1 s = getHome1().create();
        int val = 104;
        utx.begin();
        s.sendOnDestination("JunitQueue1", val, 1);
        utx.rollback();
        assertEquals(0, s.check(val, 1, 4));
        s.remove();
    }

    /**
     * Test that it's possible to set rollback only in a message driven 2 MDB
     * are listening queue3, but 1 of them rollback all messages. So, we should
     * receive all messages by the commiting MDB.
     */
    public void testRollbackOnlyOnQueue3() throws Exception {
        Sender s = getHome().create();
        int val = 114;
        int nb = 25;
        s.sendOnQueue("JunitQueue3", val, nb);
        assertEquals(nb, s.check(val, nb, 10));
        s.remove();
    }

    /**
     * Test that it's possible to set rollback only in a message driven 2 MDB
     * are listening queue3, but 1 of them rollback all messages. So, we should
     * receive all messages by the commiting MDB. via Sender1_1SF and JMS1.1
     * interfaces
     */
    public void testRollbackOnlyOnDestQueue3() throws Exception {
        Sender1_1 s = getHome1().create();
        int val = 114;
        int nb = 25;
        s.sendOnDestination("JunitQueue3", val, nb);
        assertEquals(nb, s.check(val, nb, 10));
        s.remove();
    }

    /**
     * Run all the tests
     */
    public static Test suite() {
        return new TestSuite(F_RollbackMDB.class);
    }

    public static void main(String args[]) {
        String testtorun = null;
        // Get args
        for (int argn = 0; argn < args.length; argn++) {
            String s_arg = args[argn];
            Integer i_arg;
            if (s_arg.equals("-n")) {
                testtorun = args[++argn];
            }
        }
        if (testtorun == null) {
            junit.textui.TestRunner.run(suite());
        } else {
            junit.textui.TestRunner.run(new F_RollbackMDB(testtorun));
        }
    }
}
