/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.clients.distribution;

import java.rmi.NoSuchObjectException;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

import junit.framework.Assert;
import junit.framework.Test;
import junit.framework.TestSuite;

import org.ow2.carol.util.perfs.CarolJRMPPerformanceHelper;
import org.objectweb.jonas.jtests.beans.folder.File;
import org.objectweb.jonas.jtests.beans.folder.Folder;
import org.objectweb.jonas.jtests.beans.folder.FolderHome;
import org.objectweb.jonas.jtests.util.JTestCase;

/**
 * Test a session bean remote accessing entities local.
 * Beans used: folder
 * @author Philippe Durieux
 */
public class F_Frontal extends JTestCase {

    protected static FolderHome fhome = null;
    protected Folder folder = null;

    public F_Frontal(String name) {
        super(name);
    }

    protected void setUp() {
        super.setUp();
        if (fhome == null) {
            useBeans("folder", true);
            try {
                fhome = (FolderHome) PortableRemoteObject.narrow(ictx.lookup("FolderSYHome"), FolderHome.class);
                assertNotNull(fhome);
            } catch (NamingException e) {
                fail("Cannot get bean home");
            }
        }
        if (folder == null) {
            try {
                folder = fhome.create();
                assertNotNull(folder);
            } catch (Exception e) {
                fail("Cannot create folder session " + e);
            }
        }
    }

    public void testSetRollbackOnly() throws Exception {
        Folder f1 = fhome.createForRollback();
        f1.getIntTS(2);
        f1.remove();
    }

    /**
     * create 1 entity and remove it by its remote reference.
     */
    public void testCreate1F() throws Exception {
        File f = folder.newFile("file1");
        assertNotNull(f);
        f.remove();
    }

    public void testAccessOnRemovedEntity() throws Exception {
        File f = folder.newFile("file1");
        File f2 = folder.getFile("file1");
        f2.remove();
        try {
            f.getName();
            fail("Should not access deleted object");
        } catch (NoSuchObjectException e) {
        } catch (Exception e) {
            fail("Bab Exception:" + e);
        }
    }

    /**
     * print serialized form of the reference
     */
    public void testSerializedReference() throws Exception {
        String mfolder = CarolJRMPPerformanceHelper.getMarshalBytes(folder);
        //System.out.println("Marshalled Ref = " + mfolder);
    }

    /**
     * send a reference as argument
     */
    public void testSendRef() throws Exception {
        for (int i = 0; i < 20; i++) {
            folder.sendRef(folder);
        }
    }

    /**
     * send an int as argument
     */
    public void testSendInt() throws Exception {
        for (int i = 0; i < 20; i++) {
            folder.sendInt(1);
        }
    }

    /**
     * send and get a reference as argument
     */
    public void testGetRef() throws Exception {
        for (int i = 0; i < 20; i++) {
            folder.getRef(folder);
        }
    }

    /**
     * send and get an int as argument
     */
    public void testGetInt() throws Exception {
        for (int i = 0; i < 20; i++) {
            folder.getInt(1);
        }
    }

    /**
     * send a reference as argument
     */
    public void testSendRefTS() throws Exception {
        for (int i = 0; i < 20; i++) {
            folder.sendRefTS(folder);
        }
    }

    /**
     * send an int as argument
     */
    public void testSendIntTS() throws Exception {
        for (int i = 0; i < 20; i++) {
            folder.sendIntTS(1);
        }
    }

    /**
     * send and get a reference as argument
     */
    public void testGetRefTS() throws Exception {
        for (int i = 0; i < 20; i++) {
            folder.getRefTS(folder);
        }
    }

    /**
     * send and get an int as argument
     */
    public void testGetIntTS() throws Exception {
        for (int i = 0; i < 20; i++) {
            folder.getIntTS(1);
        }
    }

    /**
     * to reproduce bug 305498
     * use ejbql query SELECT SUM (p.value) FROM jt2_paper AS p]
     */
    public void testModify() throws Exception {
        int result = folder.modify("aaaa", 100);
        Assert.assertEquals("bad result", 120, result);
    }

    /**
     * try to reproduce bug on ejbSelect #305711
     * use ejbql query SELECT SUM (p.valeur) FROM jt2_paper2 AS p WHERE p.paper3.valide = '1'
     */
    public void testModifyPaper2And3() throws Exception {
        int result = folder.modifypaper2And3("1", 55);
        Assert.assertEquals("bad result", 85, result);
    }

    /**
     * to see if the previous pb on ejbSelect exists on finder
     * Here we use a finder method with
     * SELECT OBJECT(o) FROM jt2_paper2  AS p WHERE p.paper3.valide = '1'
     */
    public void testModifyPaper2And3WithFinder() throws Exception {
        int result = folder.modifypaper2And3WithFinder("1", 55);
        Assert.assertEquals("bad result", 3, result);
    }


    public static Test suite() {
        return new TestSuite(F_Frontal.class);
    }

    public static void main (String args[]) {
        String testtorun = null;
        // Get args
        for (int argn = 0; argn < args.length; argn++) {
            String s_arg = args[argn];
            Integer i_arg;
            if (s_arg.equals("-n")) {
                testtorun = args[++argn];
            }
        }
        if (testtorun == null) {
            junit.textui.TestRunner.run(suite());
        } else {
            junit.textui.TestRunner.run(new F_Frontal(testtorun));
        }
    }
}
