/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.clients.session;

import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

import junit.framework.Test;
import junit.framework.TestSuite;

import org.objectweb.jonas.jtests.beans.bmt.Moscone;
import org.objectweb.jonas.jtests.beans.bmt.MosconeHome;
import org.objectweb.jonas.jtests.util.JTestCase;

public class F_Smt extends JTestCase {

    protected static MosconeHome home = null;

    public F_Smt(String name) {
        super(name);
    }

    protected void setUp() {
        super.setUp();
        useBeans("bmt", true); // must create tables (no entity bean)
        useBeans("applimet", true); // must create tables (no entity bean)
    }

    public MosconeHome getHome() throws Exception {
        if (home == null) {
            home = (MosconeHome) PortableRemoteObject.narrow(ictx.lookup("MosconeSTHome"), MosconeHome.class);
        }
        assertTrue(home != null);
        return home;
    }

    public void testSpanTx() throws Exception {
        Moscone m = getHome().create();
        m.tx_start();
        m.tx_commit();
        m.remove();
    }

    public void testEnlistConn1() throws Exception {
        Moscone m = getHome().create();
        m.moscone1();
        m.remove();
    }

    public void testEnlistConn2() throws Exception {
        Moscone m = getHome().create();
        m.moscone2();
        m.remove();
    }

    public static Test suite() {
        return new TestSuite(F_Smt.class);
    }

    public static void main (String args[]) {
        String testtorun = null;
        // Get args
        for (int argn = 0; argn < args.length; argn++) {
            String sarg = args[argn];
            if (sarg.equals("-n")) {
                testtorun = args[++argn];
            }
        }
        if (testtorun == null) {
            junit.textui.TestRunner.run(suite());
        } else {
            junit.textui.TestRunner.run(new F_Smt(testtorun));
        }
    }
}
