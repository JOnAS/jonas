/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.clients.transaction;

import java.rmi.RemoteException;

import javax.transaction.TransactionRequiredException;

import org.objectweb.jonas.jtests.beans.transacted.Simple;
import org.objectweb.jonas.jtests.util.JTestCase;

/**
 * Transactional attribute test cases
 * tests here are common to entity and session beans.
 * beans used : transacted
 * @author Ph.Coq, Ph.Durieux (jonas team)
 */
public abstract class A_TxAttribute  extends JTestCase {

    /**
     * constructor
     * @param name name of the test suite.
     */
    public A_TxAttribute(String name) {
        super(name);
    }

    /**
     * Sets up the fixture, here load the beans if not loaded yet.
     * This method is called before a test is executed.
     */
    protected void setUp() {
        super.setUp();
        useBeans("transacted", true);
    }

    /**
     * Get an instance of the bean.
     * This method depends on the home used to get it.
     * For entity bean, the arg is used to get a particular instance.
     * For session beans, we get any session bean from the pool.
     */
    public abstract Simple getSimple(int i) throws Exception;


    // --------------------------------------------------------------------
    // test cases
    // --------------------------------------------------------------------

    /**
	 * Test of NotSupported attribute
	 * A business method with NotSupported is called outside TX
	 * this method returns if the thread is associated to a transaction
	 * 
	 * the expected value is  false
	 */
	public void testNotSupported() throws Exception {

		Simple s = getSimple(210);
		try {
			assertEquals(false, s.opwith_notsupported());
		} finally {
			s.remove();
		}
	}

	/**
	 * Test of RequiresNew attribute
	 * A business method with RequiresNew is called outside TX
	 * this method returns if the thread is associated to a transaction
	 *
	 * the expected value is  true
	 */
	public void testRequiresNew() throws Exception {

		Simple s = getSimple(211);
		try {
			assertEquals(true, s.opwith_requires_new());
		} finally {
			s.remove();
		}
	}

	/**
	 * Test of Required attribute
	 * A business method with Required is called outside TX
	 * this method returns if the thread is associated to a transaction
	 *
	 * the expected value is  true
	 */
	public void testRequired() throws Exception {

		Simple s = getSimple(212);
		try {
			assertEquals(true, s.opwith_required());
		} finally {
			s.remove();
		}
	}

	/**
	 * Test a "Required" method calling a "Requires_new" method.
	 * the expected value is  true
	 */
	public void testRequiredRequiresNew() throws Exception {

		Simple s = getSimple(312);
		try {
			assertEquals(true, s.required_call_requires_new());
		} finally {
			s.remove();
		}
	}

	/**
	 * Test a "Required" method calling a "Requires_new" method on
	 * another bean instance.
	 * the expected value is  true
	 */
	public void testRequiredRequiresNew2() throws Exception {

		Simple s = getSimple(220);
		Simple s2 = getSimple(221);
		try {
			assertEquals(true, s.call_requires_new_on(s2));
		} finally {
			s2.remove();
			s.remove();
		}
	}

	/**
	 * Test of Mandatory attribute
	 * A business method with Mandatory is called outside TX
	 * this method returns if the thread is associated to a transaction
	 *
	 * A javax.transaction.TransactionRequiredException must be received
	 */

	public void testMandatory() throws Exception {

		Simple s = getSimple(213);
		try {
			s.opwith_mandatory();
			fail("mandatory: should raise exception");
		} catch (javax.transaction.TransactionRequiredException e) {
		} catch (RemoteException e) {
			assertTrue(e.detail instanceof TransactionRequiredException);
		} finally {
			s.remove();
		}
	}

	/**
	 * Test of Never  attribute
	 * A business method with Never is called outside TX
	 * this method returns if the thread is associated to a transaction
	 * 
	 * the expected value is  false
	 */
	public void testNever() throws Exception {

		Simple s = getSimple(214);
		try {
			assertEquals(false, s.opwith_never());
		} finally {
			s.remove();
		}
	}

	/**
	 * Test of Supports  attribute
	 * A business method with Supports is called outside TX
	 * this method returns if the thread is associated to a transaction
	 * 
	 * the expected value is  false
	 */
	public void testSupports() throws Exception {

		Simple s = getSimple(215);
		try {
			assertEquals(false, s.opwith_supports());
		} finally {
			s.remove();
		}
	}

	/**
	 * Test of NotSupported attribute
	 * A business method with NotSupported is called inside  TX
	 * this method returns if the thread is associated to a transaction
	 * 
	 * the expected value is  false
	 */
	public void testNotSupportedTx() throws Exception {

		Simple s = getSimple(220);
		utx.begin();
		try {
			assertEquals(false, s.opwith_notsupported());
		} finally {
			utx.rollback();
			s.remove();
		}
	}

	/**
	 * Test of RequiresNew attribute
	 * A business method with RequiresNew is called inside TX
	 * this method returns if the thread is associated to a transaction
	 *
	 * the expected value is  true
	 */
	public void testRequiresNewTx() throws Exception {
		Simple s = getSimple(221);
		utx.begin();
		try {
			assertEquals(true, s.opwith_requires_new());
		} finally {
			utx.rollback();
			s.remove();
		}
	}

	/**
	 * Test of Required attribute
	 * A business method with Required is called inside  TX
	 * this method returns if the thread is associated to a transaction
	 *
	 * the expected value is  true
	 */
	public void testRequiredTx() throws Exception {

		Simple s = getSimple(222);
		utx.begin();
		try {
			assertEquals(true, s.opwith_required());
		} finally {
			utx.rollback();
			s.remove();
		}

	}

	/**
	 * Test of Mandatory  attribute
	 * A business method with Mandatory is called inside  TX
	 * this method returns if the thread is associated to a transaction
	 *
	 * the expected value is  true
	 */
	public void testMandatoryTx() throws Exception {

		Simple s = getSimple(223);
		utx.begin();
		try {
			assertEquals(true, s.opwith_mandatory());
		} finally {
			utx.rollback();
			s.remove();
		}
	}

	/**
	 * Test of Never attribute
	 * A business method with Mandatory is called inside TX
	 * this method returns if the thread is associated to a transaction
	 *
	 * A java.rmi.RemoteException must be received
	 */
	public void testNeverTx() throws Exception {

		Simple s = getSimple(24);
		utx.begin();
		try {
			s.opwith_never();
			fail("never: should raise exception");
		} catch (RemoteException e) {
		} finally {
			utx.rollback();
			s.remove();
		}
	}

	/**
	 * Test of Supports  attribute
	 * A business method with Supports is called inside TX
	 * this method returns if the thread is associated to a transaction
	 * 
	 * the expected value is  true
	 */
	public void testSupportsTx() throws Exception {

		Simple s = getSimple(25);
		utx.begin();
		try {
			assertEquals(true, s.opwith_supports());
		} finally {
			utx.rollback();
			s.remove();
		}

	}

	/**
	 * Test the sequence of several calls to methods
	 * with different transactional contexts
	 */
	public void testNoTx() throws Exception {
		Simple s = getSimple(90);
		try {
			assertEquals(false, s.opwith_notsupported());
			assertEquals(true, s.opwith_requires_new());
			assertEquals(true, s.opwith_required());
			assertEquals(false, s.opwith_never());
			assertEquals(false, s.opwith_supports());
		} finally {
			s.remove();
		}
	}

}
