/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.clients.distribution;

import junit.framework.Test;
import junit.framework.TestSuite;
import org.objectweb.jonas.jtests.util.JTestCase;

/**
 * This test suites collects all the suites that have testcases that may fail in distribution directory
 *  failing is not systematic and may depend of the speed of the computer
 * Beans used:bank, cluster, folder
 * @author Philippe Coq, Philippe Durieux (jonas team)
 */
public class S_distribution extends JTestCase {

    public S_distribution(String name) {
        super(name);
    }

    public static Test suite() {
        TestSuite suite = new TestSuite();

        suite.addTest(S_bankCS.suite());
        suite.addTest(S_bankCSpf.suite());
        suite.addTest(S_bankCRW.suite());
        suite.addTest(S_bankCRC.suite());
        return suite;
    }

    public static void main (String args[]) {
        junit.textui.TestRunner.run(suite());
    }
}
