/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.clients.entity;

import junit.framework.Test;
import junit.framework.TestSuite;
import org.objectweb.jonas.jtests.util.JTestCase;

/**
 * This is a test suite about the different cases of the relationships bean's CMP2.
 * Beans used: relation/*
 * @author Jerome Camilleri
 */

public class C_Relation extends JTestCase {

    public C_Relation(String name) {
        super(name);
    }

    /*
     * tests on CS policy are deprecated in JONAS 5.
     * Some of them may fail with JDK 6 (reason is not clear)
     */
    public static Test suite() {
        TestSuite suite = new TestSuite();
        // relation oou
        //suite.addTest(F_oou_CS_EC2.suite());
        suite.addTest(F_oou_CRW_EC2.suite());
        // relation oob
        //suite.addTest(F_oob_CS_EC2.suite());
        suite.addTest(F_oob_CRW_EC2.suite());
        // relation omu
        //suite.addTest(F_omu_CS_EC2.suite());
        suite.addTest(F_omu_CRW_EC2.suite());
        // relation omb
        //suite.addTest(F_omb_CS_EC2.suite());
        suite.addTest(F_omb_CRW_EC2.suite());
        // relation mou
        //suite.addTest(F_mou_CS_EC2.suite());
        suite.addTest(F_mou_CRW_EC2.suite());
        // relation mnu
        //suite.addTest(F_mnu_CS_EC2.suite());
        suite.addTest(F_mnu_CRW_EC2.suite());
        // relation mnb
        //suite.addTest(F_mnb_CS_EC2.suite());
        suite.addTest(F_mnb_CRW_EC2.suite());
        // composite PK suites
        suite.addTest(F_Relation_pkcompEC2.suite());
        suite.addTest(F_Relation_s1pkcompEC2.suite());
        suite.addTest(F_Relation_s2pkcompEC2.suite());
        suite.addTest(F_Relation_s3pkcompEC2.suite());

        // Tables created with not null foreign-key
        suite.addTest(F_Relation2_mouEC2.suite());

        return suite;
    }

    public static void main (String args[]) {
        junit.textui.TestRunner.run(suite());
    }
}
