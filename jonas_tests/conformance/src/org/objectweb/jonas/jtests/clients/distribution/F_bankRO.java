/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.clients.distribution;

import javax.rmi.PortableRemoteObject;

import org.objectweb.jonas.jtests.beans.bank.Manager;
import org.objectweb.jonas.jtests.beans.bank.ManagerHome;
import org.objectweb.jonas.jtests.util.JTestCase;

import junit.framework.Test;
import junit.framework.TestSuite;

public class F_bankRO extends A_bankRead {

    protected static ManagerHome homecs = null;
    protected static Manager managercs = null;
    public static boolean threadfail;
    static final int accountNb = 150;
    public F_bankRO(String name) {
        super(name);
    }

    public String getManagerHomeName() {
        return "bankManagerROHome";
    }

    public boolean getPrefetch() {
        return false;
    }

    protected void setUp() {
        
        try {
            if (home == null) {
                useBeans("bank", false);
                home = (ManagerHome) PortableRemoteObject.narrow(ictx.lookup(getManagerHomeName()), ManagerHome.class);
            }
            if (manager == null) {
                manager = home.create(initialValue, getPrefetch());
            }
        } catch (Exception e) {
            fail("Exception raised in setup: " + e);
        }
        try {
            // Create all accounts with another manager (not the read-only one)
            if (homecs == null) {
                homecs = (ManagerHome) PortableRemoteObject.narrow(ictx.lookup("bankManagerCSHome"), ManagerHome.class);
            }
            if (managercs == null) {
                managercs = homecs.create(initialValue, false);
            }
            // Initializes the test by creating accounts
            
            managercs.createAll(accountNb);
            threadfail = false;
        } catch (Exception e) {
            fail("Exception raised in setup: " + e);
        }
    }


    public static Test suite() {
        return new TestSuite(F_bankRO.class);
    }

    public static void main (String args[]) {
        String testtorun = null;
        // Get args
        for (int argn = 0; argn < args.length; argn++) {
            String sarg = args[argn];
            if (sarg.equals("-n")) {
                testtorun = args[++argn];
            }
        }
        if (testtorun == null) {
            junit.textui.TestRunner.run(suite());
        } else {
            junit.textui.TestRunner.run(new F_bankRO(testtorun));
        }
    }
}
