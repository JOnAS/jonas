/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.clients.entity;

import javax.ejb.ObjectNotFoundException;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;
import junit.framework.Test;
import junit.framework.TestSuite;
import org.objectweb.jonas.jtests.beans.relation.oob.ARemote;
import org.objectweb.jonas.jtests.beans.relation.oob.AHomeRemote;
import org.objectweb.jonas.jtests.beans.relation.oob.BRemote;
import org.objectweb.jonas.jtests.beans.relation.oob.BHomeRemote;
import org.objectweb.jonas.jtests.beans.relation.oob.Front;
import org.objectweb.jonas.jtests.beans.relation.oob.FrontHome;

/**
 * This is an advanced test suite for home interface on entity bean CMP2.
 * Beans used: oob
 * @author Ph. Durieux
 */
public class F_oob_CS_EC2 extends A_oob {

    private static String BEAN_HOME_A = "relation_oob_AHome";
    private static String BEAN_HOME_B = "relation_oob_BHome";
    protected static AHomeRemote ahome = null;
    protected static BHomeRemote bhome = null;

    public F_oob_CS_EC2(String name) {
        super(name);
    }

    public AHomeRemote getAHome() {
        if (ahome == null) {
            try {
                ahome = (AHomeRemote) PortableRemoteObject.narrow(ictx.lookup(BEAN_HOME_A),
                        AHomeRemote.class);
            } catch (NamingException e) {
                fail("Cannot get bean home: " + e.getMessage());
            }
        }
        return ahome;
    }

    public BHomeRemote getBHome() {
        if (bhome == null) {
            try {
                bhome = (BHomeRemote) PortableRemoteObject.narrow(ictx.lookup(BEAN_HOME_B),
                        BHomeRemote.class);
            } catch (NamingException e) {
                fail("Cannot get bean home: " + e.getMessage());
            }
        }
        return bhome;
    }

    /*
     * Ensure the IllegalArgumentException is thrown when trying
     * to assign a deleted object as the value of a cmr-field
     * (Bug #300519)
     */
    public void testSetCmrWithDeleted() throws Exception {
        Front fb = fhome.create();
        fb.testSetCmrWithDeleted();
    }

    public static Test suite() {
        return new TestSuite(F_oob_CS_EC2.class);
    }

    public static void main (String args[]) {
        String testtorun = null;
        // Get args
        for (int argn = 0; argn < args.length; argn++) {
            String s_arg = args[argn];
            Integer i_arg;
            if (s_arg.equals("-n")) {
                testtorun = args[++argn];
            }
        }
        if (testtorun == null) {
            junit.textui.TestRunner.run(suite());
        } else {
            junit.textui.TestRunner.run(new F_oob_CS_EC2(testtorun));
        }
    }
}

