/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.clients.distribution;

import javax.rmi.PortableRemoteObject;
import org.objectweb.jonas.jtests.beans.bank.Manager;
import org.objectweb.jonas.jtests.beans.bank.ManagerHome;
import org.objectweb.jonas.jtests.util.JTestCase;

public abstract class A_bankRead extends JTestCase {

    protected static final int OP_READ = 1;
    protected static final int OP_MOVE = 2;
    protected static final int OP_CREATE = 3;
    protected static final int OP_REMOVE = 4;
    protected static final int OP_ONEMOVE = 5;
    protected static final int OP_READTX = 6;
    protected static final int OP_ONEMOVETX = 7;
    protected static final int OP_MOVETO = 8;

    static final int initialValue = 1000;
    static final int accountNb = 100;

    protected static ManagerHome home = null;
    protected static Manager manager = null;
    public static boolean threadfail;
    public static Throwable threadex = null;

    public A_bankRead(String name) {
        super(name);
    }

    public abstract String getManagerHomeName();
    public abstract boolean getPrefetch();

    protected void setUp() {
        super.setUp();
        try {
            if (home == null) {
                useBeans("bank", false);
                home = (ManagerHome) PortableRemoteObject.narrow(ictx.lookup(getManagerHomeName()), ManagerHome.class);
            }
            if (manager == null) {
                manager = home.create(initialValue, getPrefetch());
            }
            // Initializes the test by creating accounts
            manager.createAll(accountNb);
            threadfail = false;
            threadex = null;
        } catch (Exception e) {
            fail("Exception raised in setup: " + e);
        }
    }

    protected void tearDown() throws Exception {
        super.tearDown();
        if (manager != null) {
            try {
                try {
                    manager.reinitAll();
                } finally {
                    manager.remove();
                }
            } finally {
                manager = null;
            }
        }
    }

    /**
     * Create test accounts and run test ope
     */
    public void createOpe(int action, int accmin, int accmax, int loops, int threads) throws Exception {
        createOpe(action, accmin, accmax, loops, threads, 10);
    }

    /**
     * Create test accounts and run test ope
     */
    public void createOpe(int action, int accmin, int accmax, int loops, int threads, int amount) throws Exception {
        ope(OP_CREATE, accmin, accmax, (accmax - accmin + 1)*threads, 1, amount, true);
        ope(action, accmin, accmax, loops, threads, amount, true);
    }

    public void ope(int action, int accmin, int accmax, int loops, int threads) throws Exception {
        ope(action, accmin, accmax, loops, threads, 10, true);
    }

    public void ope(int action, int accmin, int accmax, int loops, int threads, int amount) throws Exception {
        ope(action, accmin, accmax, loops, threads, amount, true);
    }

    /**
     * generic operation.
     * @param accmin min range for account nb
     * @param accmax max range for account nb
     * @param loops nb of loop in each thread
     * @param threads nb of threads running the same operations
     */
    public void ope(int action, int accmin, int accmax, int loops, int threads, int amount, boolean verify) throws Exception {

        // Set transaction timeout in the server (global value!)
        if (amount > 10) {
            stopTxAt(20);
        }

        // Create and start all threads
        A_thread[] t_thr = new A_thread[threads];
        for (int i = 0; i < threads; i++) {
            t_thr[i] = new A_thread(getManagerHomeName(), i, ictx, action, accmin, accmax, loops, amount, getPrefetch());
            t_thr[i].start();
        }

        // Wait end of all threads
        for (int p = 0; p < threads; p++) {
            t_thr[p].join();
        }

        // Check if all threads run ok
        if (threadfail) {
            throw new RuntimeException("Error in a thread", threadex);
        }
        if (verify) {
            stopTxAt(60);
            if (!manager.checkAll()) {
                threadfail = true;  // to reinit the database
                fail("Bad global state");
            }
        }
    }


    public void testCheckAll() throws Exception {
        if (!manager.checkAll()) {
            fail("Bad state in database");
        }
    }

    /**
     * Operation= read
     * min range for account nb = 0
     * max range for account nb = 9
     * nb of loop in each thread = 1
     * nb of threads running the same operations = 1
     * no verification
     * @throws Exception
     */
    public void testBasicRead() throws Exception {
        ope(OP_READ, 0, 9, 1, 1, 10, false);
    }

    /**
     * Operation= read in transaction
     * min range for account nb = 0
     * max range for account nb = 9
     * nb of loop in each thread = 1
     * nb of threads running the same operations = 1
     * no verification
     * @throws Exception
     */
    public void testBasicReadTx() throws Exception {
        ope(OP_READTX, 0, 9, 1, 1, 10, false);
    }
    /**
     * Operation= read
     * min range for account nb = 0
     * max range for account nb = 9
     * nb of loop in each thread = 10
     * nb of threads running the same operations = 1
     * verification of base's state
     * @throws Exception
     * No more multithreadedsee S_bank.java
     */
    public void test3Readers() throws Exception {
        createOpe(OP_READ, 0, 9, 10, 1);
    }

    /**
     * Operation= read
     * min range for account nb = 0
     * max range for account nb = 9
     * nb of loop in each thread = 10
     * nb of threads running the same operations = 1
     * verification of base's state
     * @throws Exception
     * No more multithreadedsee S_bank.java
     */
    public void test4Readers() throws Exception {
        createOpe(OP_READ, 0, 9, 10, 1);
    }
    /**
     * Operation= read
     * min range for account nb = 0
     * max range for account nb = 9
     * nb of loop in each thread = 10
     * nb of threads running the same operations = 1
     * verification of base's state
     * @throws Exception
     * No more multithreadedsee S_bank.java
     */
    public void test5Readers() throws Exception {
        createOpe(OP_READ, 0, 9, 10, 1);
    }
    /**
     * Operation= read
     * min range for account nb = 0
     * max range for account nb = 9
     * nb of loop in each thread = 10
     * nb of threads running the same operations = 1
     * verification of base's state
     * @throws Exception
     * No more multithreadedsee S_bank.java
     */
    public void test8Readers() throws Exception {
        createOpe(OP_READ, 0, 9, 10, 1);
    }

    /**
     * Operation= read in transaction
     * min range for account nb = 0
     * max range for account nb = 9
     * nb of loop in each thread = 10
     * nb of threads running the same operations = 1
     * verification of base's state
     * @throws Exception
     * No more multithreadedsee S_bank.java
     */
    public void test3ReadersTx() throws Exception {
        createOpe(OP_READTX, 0, 9, 10, 1);
    }
    /**
     * Operation= read in transaction
     * min range for account nb = 0
     * max range for account nb = 9
     * nb of loop in each thread = 10
     * nb of threads running the same operations = 1
     * verification of base's state
     * @throws Exception
     * No more multithreadedsee S_bank.java
     */
    public void test4ReadersTx() throws Exception {
        createOpe(OP_READTX, 0, 9, 10, 1);
    }

    /**
     * Operation= read in transaction
     * min range for account nb = 0
     * max range for account nb = 9
     * nb of loop in each thread = 10
     * nb of threads running the same operations = 1
     * verification of base's state
     * @throws Exception
     * No more multithreadedsee S_bank.java
     */
    public void test5ReadersTx() throws Exception {
        createOpe(OP_READTX, 0, 9, 10, 1);
    }

    /**
     * Operation= read in transaction
     * min range for account nb = 0
     * max range for account nb = 9
     * nb of loop in each thread = 10
     * nb of threads running the same operations = 8
     * verification of base's state
     * @throws Exception
     * No more multithreadedsee S_bank.java
     */
    public void test8ReadersTx() throws Exception {
        createOpe(OP_READTX, 0, 9, 100, 1);
    }

    /**
     * Operation= read outside transaction
     * min range for account nb = 0
     * max range for account nb = 9
     * nb of loop in each thread = 1
     * nb of threads running the same operations = 1
     * verification of base's state
     * @throws Exception
     * No more multithreadedsee S_bank.java
     */

    public void testMultiRead() throws Exception {
        ope(OP_READ, 0, 9, 1, 1);
    }
    /**
     * Operation= read inside transaction
     * min range for account nb = 0
     * max range for account nb = 9
     * nb of loop in each thread = 1
     * nb of threads running the same operations = 1
     * verification of base's state
     * @throws Exception
     * No more multithreadedsee S_bank.java
     */

    public void testMultiReadTx() throws Exception {
        ope(OP_READTX, 0, 9, 1, 1);
    }

    /**
     * Operation= read inside transaction
     * min range for account nb = 0
     * max range for account nb = 9
     * nb of loop in each thread = 1
     * nb of threads running the same operations = 10
     * verification of base's state
     * @throws Exception
     * No more multithreadedsee S_bank.java
     */

    public void testMultiShortReadTx() throws Exception {
        createOpe(OP_READTX, 0, 9, 10, 1);
    }

}
