package org.objectweb.jonas.jtests.clients.hotdeploy;

import junit.framework.*;

import org.objectweb.jonas.jtests.util.JTestCase;
import org.objectweb.jonas.jtests.beans.hotdeploy.HotDeploy;
import org.objectweb.jonas.jtests.beans.hotdeploy.HotDeployHome;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.rmi.PortableRemoteObject;
import javax.naming.NamingException;



public class F_EjbJarHotDeploy extends JTestCase {

    private static String BEAN_HOME = "HotDeployHome";
    private HotDeployHome bhome1 = null;
    private HotDeployHome bhome2 = null;
    private String jonasbase = null;
    private String sep = File.separator;
    
    public F_EjbJarHotDeploy(String name) {
        super(name);
    }
    
    public static Test suite() {
        return new TestSuite(F_EjbJarHotDeploy.class);
    }

    public void setUp() {
	super.setUp();
        jonasbase = System.getProperty("jonas.base");
    }

    private void copy(String source, String dest) throws IOException {
        File src = new File(jonasbase + sep + "ejbjars" + sep + source);
        File dst = new File(jonasbase + sep + "ejbjars" + sep + dest);
        
        FileInputStream is = new FileInputStream(src);
        FileOutputStream os = new FileOutputStream(dst);

        byte[] b = new byte[1024];
        int t;
        while ((t = is.read(b, 0, b.length - 1)) != -1) {
            os.write(b, 0, t);
        }

    }

    public void testBeanHotDeployTwice() throws Exception {
        // copy $JONAS_BASE/ejbjars/hotdeploy1.jar -> $JONAS_BASE/ejbjars/hotdeploy.jar
        System.err.println("Start Test");
        copy("hotdeploy1.jar", "hotdeploy.jar");
        System.err.println("Copy executed");
        useBeans("hotdeploy", false);
        System.err.println("First Deployment OK");
	    try {
            bhome1 = (HotDeployHome) PortableRemoteObject.narrow(ictx.lookup(BEAN_HOME), HotDeployHome.class);
	    } catch (NamingException e) {
            fail("Cannot get HotDeployHome:" + e);
	    }
        System.err.println("Home retrieved");
        
        HotDeploy bean1 = bhome1.create();
        String envVal1 = bean1.getEnvString();
        int beanVal1 = bean1.getVersionNumber();
        int helperVal1 = bean1.getHelperClassVersionNumber();

        System.err.println("getValues OK");
        unloadBeans("hotdeploy");
        System.err.println("Unload first");

        // copy $JONAS_BASE/ejbjars/hotdeploy2.jar -> $JONAS_BASE/ejbjars/hotdeploy.jar
        copy("hotdeploy2.jar", "hotdeploy.jar");

        System.err.println("Copy Second jar");
        useBeans("hotdeploy", false);
        System.err.println("Seconf Load OK");
	    try {
            bhome2 = (HotDeployHome) PortableRemoteObject.narrow(ictx.lookup(BEAN_HOME), HotDeployHome.class);
	    } catch (NamingException e) {
            fail("Cannot get HotDeployHome:" + e);
	    }
        
        HotDeploy bean2 = bhome2.create();
        String envVal2 = bean2.getEnvString();
        int beanVal2 = bean2.getVersionNumber();
        int helperVal2 = bean2.getHelperClassVersionNumber();
        unloadBeans("hotdeploy");

        assertEquals("1.XML env String", "value1", envVal1);
        assertEquals("1.Static bean attr", 1, beanVal1);
        assertEquals("1.Static helper attr", 1, helperVal1);

        assertEquals("2.XML env String", "value2", envVal2);
        assertEquals("2.Static bean attr", 2, beanVal2);
        assertEquals("2.Static helper attr", 2, helperVal2);
        
    }
    
    public static void main (String args[]) {
        String testtorun = null;
        // Get args
        for (int argn = 0; argn < args.length; argn++) {
            String s_arg = args[argn];
            Integer i_arg;
            if (s_arg.equals("-n")) {
                testtorun = args[++argn];
            }
        }
        if (testtorun == null) {
            junit.textui.TestRunner.run(suite());
        } else {
            junit.textui.TestRunner.run(new F_EjbJarHotDeploy(testtorun));
        }
    }
}
