/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.clients.timer;

import org.objectweb.jonas.jtests.beans.transacted.Simple;
import org.objectweb.jonas.jtests.beans.transacted.SimpleEHome;

/**
 * Tests of the TimerService, specific to entity beans.
 * @author Philippe Durieux (jonas team)
 */
public abstract class A_TimerEntity extends A_Timer {

    /**
     * constructor
     * @param name name of the test suite.
     */
    public A_TimerEntity(String name) {
        super(name);
    }

    /**
     * return SimpleHome, that can be either BMP, CMP1, or CMP2 bean.
     */
    protected abstract SimpleEHome getHome();

    /**
     * Test interval timer
     */
    public void testTimer2() throws Exception {
        int duration = 4;
        int intervalduration = 3;
        Simple s = getSimple(random(500000));
        int expected = s.getTimerCount();
        int id = s.setTimer(duration, intervalduration);
        try {
            assertEquals("timer expired too quickly", expected, s.getTimerCount());
            sleep(1000 * duration + 1500);
            expected++;
            assertEquals("timer did not expire first", expected, s.getTimerCount());
            for (int i = 0; i < 3; i++) {
                sleep(1000 * intervalduration);
                expected++;
                assertEquals("timer did not expired at " + i, expected, s.getTimerCount());
            }
        } finally {
            s.remove();             // should remove the timer too.
        }
    }

    /**
     * Test for bug #305384
     * Create a timer for an entity bean
     * Remove this bean instance before timer expire
     * Check that the timer is removed.
     */
    public void test305384() throws Exception {
        int duration = 7;
        Simple s = getSimple(random(500000));
        for (int i = 0; i < 10; i++) {
            sleep(100);
            s.startInfoTimer(duration, "Lock");
        }
        sleep(1000);
        s.remove();             // should remove the timers too.
        System.out.println("Check that no exception is raised on server within 5 sec");
    }


    /**
     * Test this with a jonas restart to see if the timer
     * is correctly restarted.
     * @throws Exception
     */
    public void testPersistentTimer() throws Exception {
        int duration = 4;
        int intervalduration = 3;
        Simple s = getSimple(random(500000));
        try {
            int expected = s.getTimerCount();
            s.setTimer(duration, intervalduration);
            assertEquals("timer expired too quickly", expected, s.getTimerCount());
            sleep(1000 * duration + 1500);
            expected++;
            assertEquals("timer did not expire first", expected, s.getTimerCount());
            for (int i = 0; i < 3; i++) {
                sleep(1000 * intervalduration);
                expected++;
                assertEquals("timer did not expired at " + i, expected, s.getTimerCount());
            }
        } finally {
            // Remove this line for persistent timers tests
            s.remove();
        }
    }

    /**
     * test timer in ejbPostCreate
     */
    public void testTimerInEjbPostCreate() throws Exception {
        Simple s = getHome().createWithTimer(10, 2000);
        sleep(4000);
        s.remove();
    }

    /**
     * test immediate timer in ejbPostCreate (bug #300502)
     */
    public void testTimerInEjbPostCreate2() throws Exception {
        Simple s = getHome().createWithTimer(10, 2);
        sleep(1000);
        s.remove();
    }

    /**
     * test cancel in ejbTimeout (bug #300306)
     */
    public void testCancelInTimeout() throws Exception {
        int duration = 2;
        int intervalduration = -1;
        Simple s = getSimple(random(500000));
        int id = s.setTimer(duration, intervalduration);
        // timer should be cancelled at first expiration.
        // TODO: test this.
        sleep(6000);
        s.remove();
    }
}
