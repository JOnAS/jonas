/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.objectweb.jonas.jtests.clients.distribution;

import javax.rmi.PortableRemoteObject;
import org.objectweb.jonas.jtests.beans.bank.Manager;
import org.objectweb.jonas.jtests.beans.bank.ManagerHome;
import org.objectweb.jonas.jtests.util.JTestCase;

public abstract class A_bank extends A_bankRead {

    public A_bank(String name) {
        super(name);
    }

    /**
     * Operation= Move
     * min range for account nb = 2
     * max range for account nb = 2
     * nb of loop in each thread = 10
     * nb of threads running the same operations = 1
     * no verification
     * @throws Exception
     */
    public void testSameAccount() throws Exception {
        ope(OP_MOVETO, 2, 2, 10, 1, 10);
    }

    /**
     * Operation= Move
     * min range for account nb = 6
     * max range for account nb = 6
     * nb of loop in each thread = 1
     * nb of threads running the same operations = 1
     * amount = 12
     * no verification
     * @throws Exception
     * No more multheaded see S_bank.java
     */
    public void testMultiSameAccount() throws Exception {
        createOpe(OP_MOVETO, 6, 6, 1, 1, 12);
    }
}



