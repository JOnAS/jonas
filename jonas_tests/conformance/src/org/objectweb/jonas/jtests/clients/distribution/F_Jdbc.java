/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.clients.distribution;

import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

import junit.framework.Test;
import junit.framework.TestSuite;

import org.objectweb.jonas.jtests.beans.jdbc.Manager;
import org.objectweb.jonas.jtests.beans.jdbc.ManagerHome;
import org.objectweb.jonas.jtests.util.JTestCase;

/**
 * Test Jdbc accesses from a session bean
 * Beans used: jdbc
 * @author Philippe Durieux
 */
public class F_Jdbc extends JTestCase {

    protected static ManagerHome mhome = null;
    protected Manager manager = null;

    public F_Jdbc(String name) {
        super(name);
    }

    protected void tearDown() throws Exception {
        if (manager != null) {
            manager.remove();
        }
    }

    protected void setUp() {
        super.setUp();
        if (mhome == null) {
            useBeans("jdbc", true);
            try {
                mhome = (ManagerHome) PortableRemoteObject.narrow(ictx.lookup("jdbcManagerSYHome"), ManagerHome.class);
                assertNotNull(mhome);
            } catch (NamingException e) {
                fail("Cannot get bean home");
            }
        }
        if (manager == null) {
            try {
                manager = mhome.create();
                assertNotNull(manager);
            } catch (Exception e) {
                fail("Cannot create manager session");
            }
        }
    }

    /**
     * Test we can open a Connection
     */
    public void testOpenConnection() throws Exception {
        int cnb = manager.openConnection();
        assertTrue("Cannot open Connection", cnb > 0);
    }

    /**
     * Test we can close a Connection previously opened
     */
    public void testCloseConnection() throws Exception {
        int cnb = manager.openConnection();
        assertTrue("open connection failed", cnb > 0);
        assertTrue("isClosed() should return true after close()", manager.closeConnection(cnb));
    }

    /**
     * test we can create and close a Connection in the same method.
     */
    public void testOpenCloseConnection() throws Exception {
        assertTrue("basic open+close failed", manager.openCloseConnection());
    }

    /**
     * test that we cannot use a connection after close.
     * an SQLException should be raised (spec JDBC 2.0 p.21)
     */
    public void testAccessConnectionAfterClose() throws Exception {
        assertTrue("using conn after close must raise SQLException", manager.accessCloseConnection());
    }

    /**
     * Test simple jdbc access (write value)
     */
    public void testJdbcStoreNoTx() throws Exception {
        manager.storeValue("pk1", 1);
    }

    /**
     * Test simple jdbc access (write value) in committed tx
     */
    public void testJdbcStoreCommit() throws Exception {
        int val = manager.getValue("pk1");
        utx.begin();
        try {
            manager.storeValue("pk1", val + 1);
        } finally {
            utx.commit();
        }
        assertEquals("Commit did not work", val + 1, manager.getValue("pk1"));
    }

    /**
     * Test simple jdbc access (write value) in committed tx
     */
    public void testJdbcStore2Commit() throws Exception {
        int val = manager.getValue("pk1");
        int val2 = manager.getValue("pk2");
        utx.begin();
        try {
            manager.storeValue("pk1", val + 1);
            manager.storeValue("pk2", val2 + 1);
        } finally {
            utx.commit();
        }
        assertEquals("Commit did not work", val + 1, manager.getValue("pk1"));
        assertEquals("Commit did not work", val2 + 1, manager.getValue("pk2"));
    }

    /**
     * Test simple jdbc access (write value) in rollback tx
     */
    public void testJdbcStoreRollback() throws Exception {
        int val = manager.getValue("pk1");
        utx.begin();
        try {
            manager.storeValue("pk1", val + 1);
        } finally {
            utx.rollback();
        }
        assertEquals("Rollback did not work", val, manager.getValue("pk1"));
    }

    /**
     * Test with 2 resources
     */
    public void testJdbcStore2Rollback() throws Exception {
        int val1 = manager.getValue("pk1");
        int val2 = manager.getValue("pk2");
        utx.begin();
        try {
            manager.storeValue("pk1", val1 + 1);
            manager.storeValue("pk2", val2 + 1);
        } finally {
            utx.rollback();
        }
        assertEquals("Rollback did not work", val1, manager.getValue("pk1"));
        assertEquals("Rollback did not work", val2, manager.getValue("pk2"));
    }

    public static Test suite() {
        return new TestSuite(F_Jdbc.class);
    }

    public static void main (String args[]) {
        String testtorun = null;
        // Get args
        for (int argn = 0; argn < args.length; argn++) {
            String s_arg = args[argn];
            Integer i_arg;
            if (s_arg.equals("-n")) {
                testtorun = args[++argn];
            }
        }
        if (testtorun == null) {
            junit.textui.TestRunner.run(suite());
        } else {
            junit.textui.TestRunner.run(new F_Jdbc(testtorun));
        }
    }
}
