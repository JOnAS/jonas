/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.clients.entity;

import java.util.Collection;
import java.util.Iterator;

import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

import junit.framework.Test;
import junit.framework.TestSuite;

import org.objectweb.jonas.jtests.beans.ebasic.E4Query;
import org.objectweb.jonas.jtests.beans.ebasic.E4QueryHome;
import org.objectweb.jonas.jtests.util.JTestCase;


/**
 * For testing basic EJB QL queries.
 * @author Helene Joanin
 */

public class F_BasicEjbqlEC2 extends JTestCase {

    private static String BEAN_HOME_E4QUERY = "ebasicE4QueryEC2Home";
    protected static E4QueryHome home = null;


    public F_BasicEjbqlEC2(String name) {
        super(name);
    }

    protected void setUp() {
	super.setUp();
        if (home == null) {
            // load bean if not loaded yet
            useBeans("ebasic", true);
            try {
                home = (E4QueryHome) PortableRemoteObject.narrow(ictx.lookup(BEAN_HOME_E4QUERY),
                                                                 E4QueryHome.class);
            } catch (NamingException e) {
                fail("Cannot get bean home: " + e.getMessage());
            }
            // check if tables have been initialized
            try {
                home.findByPrimaryKey("id1");
            } catch (Exception e) {
                try {
                    home.create("idnull", null, 0, 0.0);
                    home.create("id1","helene", 1959, 1959.0);
                    home.create("id2","ahelene", -1959, 1959.0);
                    home.create("id3","helene-bis", 1959*1959, 1959.0*1959.0);
                    home.create("id4","eric", 1957, 1957.0);
                    home.create("id4e","ric", 1957, 1957.0);
                } catch (Exception i) {
                    fail("InitialState creation problem:: "+i);
                }
            }
        }
    }

    /**
     * Test the EJB-QL: SELECT OBJECT(o) FROM jt2_e4query o WHERE LENGTH(o.fstring) = ?1
     */
    public void testLength() throws Exception {
        int l = "helene".length();
        Collection cBeans = home.findByLengthString(l);
        Iterator iBeans = cBeans.iterator();
        int nb=0;
        while(iBeans.hasNext()) {
            E4Query bean = (E4Query) javax.rmi.PortableRemoteObject.narrow(iBeans.next(),
                                                                           E4Query.class);
            assertEquals("Id="+bean.getId(), l, bean.getFstring().length());
            nb++;
        }
        assertEquals("Beans number: ", 1, nb);

    }

    /**
     * Test the EJB-QL: SELECT OBJECT(o) FROM jt2_e4query o WHERE LOCATE(?1, o.fstring) > 0
     */
    public void testLocate() throws Exception {
        String l = "helene";
        Collection cBeans = home.findByLocateString(l);
        Iterator iBeans = cBeans.iterator();
        int nb=0;
        while(iBeans.hasNext()) {
            E4Query bean = (E4Query) javax.rmi.PortableRemoteObject.narrow(iBeans.next(),
                                                                           E4Query.class);
            String f = bean.getFstring();
            assertTrue("Id="+bean.getId(), f.indexOf(l)>-1);
            nb++;
        }
        assertEquals("Beans number: ", 3, nb);

    }

    /**
     * Test the EJB-QL: SELECT OBJECT(o) FROM jt2_e4query o WHERE LOCATE(?1, o.fstring, ?2) > 0
     * FAIL in postgresql. Spec 2.1 says that this implementation is optional.
     */
    public void testLocateAt() throws Exception {
        String l = "helene";
        Collection cBeans = home.findByLocateStringAt(l, 2);
        Iterator iBeans = cBeans.iterator();
        int nb=0;
        while(iBeans.hasNext()) {
            E4Query bean = (E4Query) javax.rmi.PortableRemoteObject.narrow(iBeans.next(),
                                                                           E4Query.class);
            String f = bean.getFstring();
            assertTrue("Id=" + bean.getId() + ",f=" + f, f.indexOf(l, 1)>-1);
            nb++;
        }
        assertEquals("Beans number: ", 1, nb);

    }

    /**
     * Test the EJB-QL: SELECT OBJECT(o) FROM jt2_e4query o WHERE SUBSTRING(o.fstring, ?2, ?3) = ?1
     */
    public void testSubstring() throws Exception {
        String s = "el";
        int is = 2;
        int il = "el".length();
        Collection cBeans = home.findBySubstring(s, is, il);
        Iterator iBeans = cBeans.iterator();
        int nb=0;
        int ib = is - 1;
        int ie = is - 1 + il;
        while(iBeans.hasNext()) {
            E4Query bean = (E4Query) javax.rmi.PortableRemoteObject.narrow(iBeans.next(),
                                                                           E4Query.class);
            String f = bean.getFstring();
            assertTrue("id="+bean.getId(), s.equals(f.substring(ib, ie)));
            nb++;
        }
        assertEquals("Beans number: ", 2, nb);

    }

    /**
     * Test the EJB-QL: SELECT OBJECT(o) FROM jt2_e4query o WHERE CONCAT(o.id, o.fstring) = ?1
     */
    public void testConcat() throws Exception {
        String s = "id4"+"eric";
        Collection cBeans = home.findByConcatString(s);
        Iterator iBeans = cBeans.iterator();
        int nb=0;
        while(iBeans.hasNext()) {
            E4Query bean = (E4Query) javax.rmi.PortableRemoteObject.narrow(iBeans.next(),
                                                                           E4Query.class);
            String f = bean.getFstring();
            assertTrue("Id="+bean.getId(), s.equals(bean.getId().concat(bean.getFstring())));
            nb++;
        }
        assertEquals("Beans number: ", 2, nb);

    }

    /**
     * Test the EJB-QL: SELECT OBJECT(o) FROM jt2_e4query o WHERE ABS(o.fint) = ?1
     */
    public void testAbsInt() throws Exception {
        int i = 1959;
        Collection cBeans = home.findByAbsInt(i);
        Iterator iBeans = cBeans.iterator();
        int nb=0;
        while(iBeans.hasNext()) {
            E4Query bean = (E4Query) javax.rmi.PortableRemoteObject.narrow(iBeans.next(),
                                                                           E4Query.class);
            int f = bean.getFint();
            assertTrue("Id="+bean.getId(), (f==-i) || (f==i));
            nb++;
        }
        assertEquals("Beans number: ", 2, nb);

    }

    /**
     * Test the EJB-QL: SELECT OBJECT(o) FROM jt2_e4query o WHERE SQRT(o.fdouble) BETWEEN  ?1 - 0.1 AND ?1 + 0.1
     */
    public void testSqrt() throws Exception {
        int i = 1959;
        Collection cBeans = home.findBySqrtDouble(1959.0);
        Iterator iBeans = cBeans.iterator();
        int nb=0;
        while(iBeans.hasNext()) {
            E4Query bean = (E4Query) javax.rmi.PortableRemoteObject.narrow(iBeans.next(),
                                                                           E4Query.class);
            int f = bean.getFint();
            assertTrue("Id="+bean.getId(), f==i*i);
            nb++;
        }
        assertEquals("Beans number: ", 1, nb);

    }

    /**
     * Test the EJB-QL: SELECT OBJECT(o) FROM jt2_e4query o WHERE o.fstring IS NULL
     */
    public void testIsNull() throws Exception {
        Collection cBeans = home.findByIsNull();
        Iterator iBeans = cBeans.iterator();
        int nb=0;
        while(iBeans.hasNext()) {
            E4Query bean = (E4Query) javax.rmi.PortableRemoteObject.narrow(iBeans.next(),
                                                                           E4Query.class);
            assertNull("Id="+bean.getId(), bean.getFstring());
            nb++;
        }
        assertEquals("Beans number: ", 1, nb);

    }

    /**
     * Test the EJB-QL: SELECT OBJECT(o) FROM jt2_e4query o WHERE ?1 IS NULL
     */
    public void testIsNull1Param() throws Exception {
        Collection cBeans = home.findByIsNullParam(null);
        Iterator iBeans = cBeans.iterator();
        int nb=0;
        while(iBeans.hasNext()) {
            E4Query bean = (E4Query) javax.rmi.PortableRemoteObject.narrow(iBeans.next(),
                                                                           E4Query.class);
            nb++;
        }
        assertEquals("Beans number: ", 6, nb);
    }

    /**
     * Test the EJB-QL: SELECT OBJECT(o) FROM jt2_e4query o WHERE ?1 IS NULL
     */
    public void testIsNull2Param() throws Exception {
        Collection cBeans = home.findByIsNullParam("hello");
        Iterator iBeans = cBeans.iterator();
        int nb=0;
        while(iBeans.hasNext()) {
            E4Query bean = (E4Query) javax.rmi.PortableRemoteObject.narrow(iBeans.next(),
                                                                           E4Query.class);
            nb++;
        }
        assertEquals("Beans number: ", 0, nb);
    }

    /**
     * Test the EJB-QL: SELECT OBJECT(o) FROM jt2_e4query o WHERE o.fstring IN ('helene', 'eric')
     */
    public void testInStrings() throws Exception {
        Collection cBeans = home.findByInStrings();
        Iterator iBeans = cBeans.iterator();
        int nb=0;
        while(iBeans.hasNext()) {
            E4Query bean = (E4Query) javax.rmi.PortableRemoteObject.narrow(iBeans.next(),
                                                                           E4Query.class);
            boolean ok = "helene".equals(bean.getFstring()) || "eric".equals(bean.getFstring());
            assertTrue("Id="+bean.getId(), ok);
            nb++;
        }
        assertEquals("Beans number: ", 2, nb);

    }

    /**
     * Test the EJB-QL: SELECT OBJECT(o) FROM jt2_e4query o WHERE ABS(o.fint) = -100
     */
    public void testLessThanMinus100() throws Exception {
        Collection cBeans = home.findByLessThanMinus100();
        Iterator iBeans = cBeans.iterator();
        int nb=0;
        while(iBeans.hasNext()) {
            E4Query bean = (E4Query) javax.rmi.PortableRemoteObject.narrow(iBeans.next(),
                                                                           E4Query.class);
            int f = bean.getFint();
            assertTrue("Id="+bean.getId(), f < -100);
            nb++;
        }
        assertEquals("Beans number: ", 1, nb);

    }

    /**
     * Test the EJB-QL: SELECT OBJECT(o) FROM jt2_e4query o WHERE o.fint = ?1 - ?2 - ?3
     * to reproduce bug 303587
     * This bug is pending: Remove this test from the list for now.
     */
    public void _testIntEqualExpr2() throws Exception {
        Collection cBeans = home.findByIntEqualExpr2(1962, 2, 1);
        Iterator iBeans = cBeans.iterator();
        int nb=0;
        while(iBeans.hasNext()) {
            E4Query bean = (E4Query) javax.rmi.PortableRemoteObject.narrow(iBeans.next(),
                                                                           E4Query.class);
            int f = bean.getFint();
            assertEquals("Id="+bean.getId(), 1959, f);
            nb++;
        }
        assertEquals("Beans number: ", 1, nb);

    }


    /**
     * Test the EJB-QL: SELECT OBJECT(o) FROM jt2_e4query o WHERE o.fstring > ?1
     */
    public void testStringGreaterThenExpr() throws Exception {
        Collection cBeans = home.findByStringGreaterThenExpr("helene");
        Iterator iBeans = cBeans.iterator();
        int nb=0;
        while(iBeans.hasNext()) {
            E4Query bean = (E4Query) javax.rmi.PortableRemoteObject.narrow(iBeans.next(),
                                                                           E4Query.class);
            String f = bean.getFstring();
            assertTrue("Id="+bean.getId()+",fString="+f, f.compareTo("helene") > 0);
            nb++;
        }
        assertEquals("Beans number: ", 2, nb);
    }

    /**
     * Test the EJB-QL: SELECT OBJECT(o) FROM jt2_e4query o WHERE o.fstring >= ?1
     */
    public void testStringGreaterOrEqualThenExpr() throws Exception {
        Collection cBeans = home.findByStringGreaterOrEqualThenExpr("helene");
        Iterator iBeans = cBeans.iterator();
        int nb=0;
        while(iBeans.hasNext()) {
            E4Query bean = (E4Query) javax.rmi.PortableRemoteObject.narrow(iBeans.next(),
                                                                           E4Query.class);
            String f = bean.getFstring();
            assertTrue("Id="+bean.getId()+",fString="+f, f.compareTo("helene") >= 0);
            nb++;
        }
        assertEquals("Beans number: ", 3, nb);
    }

    /**
     * Test the EJB-QL: SELECT OBJECT(o) FROM jt2_e4query o WHERE MOD(o.fint, ?1) = 0
     */
    public void testMod() throws Exception {
        int op2 = 1959;
        Collection cBeans = home.findByIntModXIsZero(op2);
        Iterator iBeans = cBeans.iterator();
        int nb=0;
        while(iBeans.hasNext()) {
            E4Query bean = (E4Query) javax.rmi.PortableRemoteObject.narrow(iBeans.next(),
                                                                           E4Query.class);
            int f = bean.getFint();
            //System.out.println("Id="+bean.getId()+",fInt="+f);
            assertTrue("Id="+bean.getId()+",fInt="+f, (f % op2) == 0);
            nb++;
        }
        assertEquals("Beans number: ", 4, nb);
    }


    public static Test suite() {
        return new TestSuite(F_BasicEjbqlEC2.class);
    }

    public static void main(String args[]) {
        String testtorun = null;
        // Get args
        for (int argn = 0; argn < args.length; argn++) {
            String s_arg = args[argn];
            Integer i_arg;
            if (s_arg.equals("-n")) {
                testtorun = args[++argn];
            }
        }
        if (testtorun == null) {
            junit.textui.TestRunner.run(suite());
        } else {
            junit.textui.TestRunner.run(new F_BasicEjbqlEC2(testtorun));
        }
    }

}
