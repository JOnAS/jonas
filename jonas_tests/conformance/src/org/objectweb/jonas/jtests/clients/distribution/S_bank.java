/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.objectweb.jonas.jtests.clients.distribution;

import javax.rmi.PortableRemoteObject;
import org.objectweb.jonas.jtests.beans.bank.Manager;
import org.objectweb.jonas.jtests.beans.bank.ManagerHome;
import org.objectweb.jonas.jtests.util.JTestCase;

public abstract class S_bank extends A_bankRead {

    public S_bank(String name) {
        super(name);
    }

 /**
  * Here is kept testcases that fails from time to times
  * do to the multithreading?
  * It comes from A_bank.java
  */

    /**
     * Operation= Move
     * min range for account nb = 6
     * max range for account nb = 6
     * nb of loop in each thread = 1
     * nb of threads running the same operations = 4
     * amount = 12
     * no verification
     * @throws Exception
     */
    public void testMultiSameAccount() throws Exception {
        createOpe(OP_MOVETO, 6, 6, 1, 4, 12);
    }

    /**
     * Operation= read
     * min range for account nb = 0
     * max range for account nb = 9
     * nb of loop in each thread = 10
     * nb of threads running the same operations = 3
     * verification of base's state
     * @throws Exception
     */
    public void test3Readers() throws Exception {
        createOpe(OP_READ, 0, 9, 10, 3);
    }

    /**
     * Operation= read
     * min range for account nb = 0
     * max range for account nb = 9
     * nb of loop in each thread = 10
     * nb of threads running the same operations = 4
     * verification of base's state
     * @throws Exception
     */
    public void test4Readers() throws Exception {
        createOpe(OP_READ, 0, 9, 10, 4);
    }
    /**
     * Operation= read
     * min range for account nb = 0
     * max range for account nb = 9
     * nb of loop in each thread = 10
     * nb of threads running the same operations = 5
     * verification of base's state
     * @throws Exception
     */
    public void test5Readers() throws Exception {
        createOpe(OP_READ, 0, 9, 10, 5);
    }
    /**
     * Operation= read
     * min range for account nb = 0
     * max range for account nb = 9
     * nb of loop in each thread = 10
     * nb of threads running the same operations = 8
     * verification of base's state
     * @throws Exception
     */
    public void test8Readers() throws Exception {
        createOpe(OP_READ, 0, 9, 10, 8);
    }

    /**
     * Operation= read in transaction
     * min range for account nb = 0
     * max range for account nb = 9
     * nb of loop in each thread = 10
     * nb of threads running the same operations = 3
     * verification of base's state
     * @throws Exception
     */
    public void test3ReadersTx() throws Exception {
        createOpe(OP_READTX, 0, 9, 10, 3);
    }
    /**
     * Operation= read in transaction
     * min range for account nb = 0
     * max range for account nb = 9
     * nb of loop in each thread = 10
     * nb of threads running the same operations = 4
     * verification of base's state
     * @throws Exception
     */
    public void test4ReadersTx() throws Exception {
        createOpe(OP_READTX, 0, 9, 10, 4);
    }

    /**
     * Operation= read in transaction
     * min range for account nb = 0
     * max range for account nb = 9
     * nb of loop in each thread = 10
     * nb of threads running the same operations = 5
     * verification of base's state
     * @throws Exception
     */
    public void test5ReadersTx() throws Exception {
        createOpe(OP_READTX, 0, 9, 10, 5);
    }

    /**
     * Operation= read in transaction
     * min range for account nb = 0
     * max range for account nb = 9
     * nb of loop in each thread = 10
     * nb of threads running the same operations = 8
     * verification of base's state
     * @throws Exception
     */
    public void test8ReadersTx() throws Exception {
        createOpe(OP_READTX, 0, 9, 100, 8);
    }

    /**
     * Operation= read outside transaction
     * min range for account nb = 0
     * max range for account nb = 9
     * nb of loop in each thread = 1
     * nb of threads running the same operations = 10
     * verification of base's state
     * @throws Exception
     */

    public void testMultiRead() throws Exception {
        ope(OP_READ, 0, 9, 1, 10);
    }
    /**
     * Operation= read inside transaction
     * min range for account nb = 0
     * max range for account nb = 9
     * nb of loop in each thread = 1
     * nb of threads running the same operations = 10
     * verification of base's state
     * @throws Exception
     */

    public void testMultiReadTx() throws Exception {
        ope(OP_READTX, 0, 9, 1, 10);
    }

    /**
     * Operation= read inside transaction
     * min range for account nb = 0
     * max range for account nb = 9
     * nb of loop in each thread = 10
     * nb of threads running the same operations = 10
     * verification of base's state
     * @throws Exception
     */

    /*  these tests come from A_bankWrite */
    
    public void testMultiShortReadTx() throws Exception {
        createOpe(OP_READTX, 0, 9, 10, 10);
    }
    
    public void testMultiRemove() throws Exception {
        createOpe(OP_REMOVE, 3010, 3040, 1, 4);
    }

    public void testManyRemove() throws Exception {
        createOpe(OP_REMOVE, 3000, 3200, 200, 1);
    }
    public void testMultiRB() throws Exception {
        createOpe(OP_MOVE, 0, 4, 4, 5, 600);
    }
    public void testMultiCreate() throws Exception {
        ope(OP_CREATE, 1000, 1900, 1, 12);
    }

    public void testMultiMove() throws Exception {
        ope(OP_MOVE, 0, 5, 1, 5);
    }

}
