/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.clients.distribution;

import junit.framework.Test;
import junit.framework.TestSuite;
import org.objectweb.jonas.jtests.util.JTestCase;

/**
 * This test suites test distribution pbs and involve generally
 * several beans, with different types of interfaces.
 * Beans used: folder
 * @author Philippe Coq, Philippe Durieux (jonas team)
 */
public class C_distribution extends JTestCase {

    public C_distribution(String name) {
        super(name);
    }

    public static Test suite() {
        TestSuite suite = new TestSuite();
        suite.addTest(F_FrontalCMP2.suite());
        suite.addTest(F_Frontal.suite());
        suite.addTest(F_Jdbc.suite());
        suite.addTest(F_Cluster.suite());
        suite.addTest(F_bankCRC.suite());
        suite.addTest(F_bankDB.suite());
        suite.addTest(F_bankRO.suite());
        suite.addTest(F_bankCST.suite());
        suite.addTest(F_bankCRW.suite());
 
        return suite;
    }

    public static void main (String args[]) {
        junit.textui.TestRunner.run(suite());
    }
}
