/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.clients.entity;

import javax.ejb.ObjectNotFoundException;

import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

import org.objectweb.jonas.jtests.beans.relation.oob.AHomeRemote;
import org.objectweb.jonas.jtests.beans.relation.oob.ARemote;
import org.objectweb.jonas.jtests.beans.relation.oob.BHomeRemote;
import org.objectweb.jonas.jtests.beans.relation.oob.BRemote;
import org.objectweb.jonas.jtests.beans.relation.oob.Front;
import org.objectweb.jonas.jtests.beans.relation.oob.FrontHome;

/**
 * This is an advanced test suite for home interface on entity bean CMP2.
 * Beans used: oob
 * @author Ph. Durieux
 */
public abstract class A_oob extends A_Cmp2Util {

    public abstract AHomeRemote getAHome();
    public abstract BHomeRemote getBHome();
    protected static String BEAN_HOME_FRONT = "relation_oob_FrontHome";
    protected static FrontHome fhome = null;


    public A_oob(String name) {
        super(name);
    }

    protected boolean isInit = false;

    protected void setUp() {
        super.setUp();
        boolean ok = false;
        int nbtry = 0;
        while (!ok && nbtry < 3) {
            if (!isInit) {
                // load bean if not loaded yet
                useBeans("oob", false);
                try {
                    fhome = (FrontHome) PortableRemoteObject.narrow(ictx.lookup(BEAN_HOME_FRONT),
                                                                              FrontHome.class);
                } catch (NamingException e) {
                    fail("Cannot get bean home: " + e.getMessage());
                }
                // check if tables have been initialized
                try {
                    debug("find a2");
                    getAHome().findByPrimaryKey("a2");
                } catch (Exception e) {
                    // Make the initialization needed for the tests
                    try {
                        utx.begin();
                        debug("Make initialization");
                        ARemote a1 = getAHome().create("a1");
                        ARemote a2 = getAHome().create("a2");
                        getAHome().create("a3");
                        getBHome().create("b0");
                        getBHome().create("b1");
                        getBHome().create("b2");
                        getBHome().create("b3");
                        a1.assignB("b1");
                        a2.assignB("b2");
                    } catch (Exception i) {
                        fail("InitialState creation problem: " + i);
                    } finally {
                        try {
                            utx.commit();
                        } catch (Exception ii) {
                        }
                    }
                }
                isInit = true;
            }
            // Check that all is OK. Sometimes, a test has failed and has corrupted
            // the bean state in the database. We must unload and reload the bean then.
            nbtry++;
            try {
                if (initStateOK()) {
                    ok = true;
                }
            } catch (Exception e) {
            }
            if (!ok) {
                debug("Unload oob");
                isInit = false;
                unloadBeans("oob");
            }
        }
    }

    /*
     * Check that we are in the same state as after the tables creation for thoses beans A and B
     * (ie if it is the initial state)
     */
    boolean initStateOK() throws Exception {
        boolean isOk = true;

        msgerror = new StringBuffer();

        debug("Checking init state");
        ARemote a1 = getAHome().findByPrimaryKey("a1");
        BRemote b1 = getBHome().findByPrimaryKey("b1");
        ARemote a2 = getAHome().findByPrimaryKey("a2");
        BRemote b2 = getBHome().findByPrimaryKey("b2");
        ARemote a3 = getAHome().findByPrimaryKey("a3");
        BRemote b3 = getBHome().findByPrimaryKey("b3");
        String idB1 = a1.retrieveB();
        String idB2 = a2.retrieveB();
        String idB3 = a3.retrieveB();
        String idA1 = b1.retrieveA();
        String idA2 = b2.retrieveA();
        String idA3 = b3.retrieveA();

        if (idB1 != null && !idB1.equals("b1")) {
            isOk = false;
            msgerror.append("\nWrong relation for  a1->b1"
                            + "(expected: A1.retrieveB()='b1'"
                            + ", found:" + idB1 + ")");
        }
        if (idB2 != null && !idB2.equals("b2")) {
            isOk = false;
            msgerror.append("\nWrong relation for  a2->b2"
                            + "(expected: A2.retrieveB()='b2'"
                            + ", found:" + idB2 + ")");
        }
        if (idA1 != null && !idA1.equals("a1")) {
            isOk = false;
            msgerror.append("\nWrong relation for  b1->a1"
                            + "(expected: B1.retrieveA()='a1'"
                            + ", found:" + idA1 + ")");
        }
        if (idA2 != null && !idA2.equals("a2")) {
            isOk = false;
            msgerror.append("\nWrong relation for  b2->a2"
                            + "(expected: B2.retrieveA()='a2'"
                            + ", found:" + idA2 + ")");
        }
        if (idA3 != null) {
            isOk = false;
            msgerror.append("\nWrong relation for  a3->b3"
                            + "(expected: A3.retrieveB()=null"
                            + ", found:" + idA3 + ")");
        }
        if (idB3 != null) {
            isOk = false;
            msgerror.append("\nWrong relation for  b3->a3"
                            + "(expected: B3.retrieveA()=null"
                            + ", found:" + idB3 + ")");
        }
        if (!isOk) {
            debug(msgerror.toString());
        }
        return isOk;
    }


    /**
     * Check that the bean 'a3' has no relation.
     */
    public void _testBasicGetEmpty(int tx) throws Exception {
        String  idB = null;
        if ((tx == TX_CALL) || (tx == TX_RB)) {
            utx.begin();
        }
        ARemote a = getAHome().findByPrimaryKey("a3");
        if (tx == TX_CONT) {
            idB = a.retrieveBInNewTx();
        } else {
            idB = a.retrieveB();
        }
        if (tx == TX_CALL) {
            utx.commit();
        } else if (tx == TX_RB) {
            utx.rollback();
        }
        // checking
        if (idB != null) {
            fail("a3->b not empty");
        }
    }

    public void testBasicGetEmptyTxNo() throws Exception {
        _testBasicGetEmpty(TX_NO);
    }
    public void testBasicGetEmptyTxCall() throws Exception {
        _testBasicGetEmpty(TX_CALL);
    }
    public void testBasicGetEmptyTxCont() throws Exception {
        _testBasicGetEmpty(TX_CONT);
    }

    /**
     * Set a relation : a3.assignB("b3")
     */
    public void _testBasicSetEmpty(int tx) throws Exception {
        if ((tx == TX_CALL) || (tx == TX_RB)) {
            utx.begin();
        }
        ARemote a = getAHome().findByPrimaryKey("a3");
        if (tx == TX_CONT) {
            a.assignBInNewTx("b3");
        } else {
            a.assignB("b3");
        }
        if (tx == TX_CALL) {
            utx.commit();
        } else if (tx == TX_RB) {
            utx.rollback();
        }
        // checking
        String idB = a.retrieveBInNewTx();
        if (tx != TX_RB) {
            assertEquals("Wrong assign for relation a3->b3 : ", "b3", idB);
            // undo
            a.assignBInNewTx(null);
        } else {
            assertNull("Rollback did not occur", idB);
        }
        checkIsInitialState();
    }

    public void testBasicSetEmptyTxNo() throws Exception {
        _testBasicSetEmpty(TX_NO);
    }
    public void testBasicSetEmptyTxCall() throws Exception {
        _testBasicSetEmpty(TX_CALL);
    }
    public void testBasicSetEmptyTxCont() throws Exception {
        _testBasicSetEmpty(TX_CONT);
    }
    public void testBasicSetEmptyTxRb() throws Exception {
        _testBasicSetEmpty(TX_RB);
    }

    /**
     * Set a relation to empty : a3.assignB(null)
     */
    public void _testBasicSetEmptyNull(int tx) throws Exception {
        if ((tx == TX_CALL) || (tx == TX_RB)) {
            utx.begin();
        }
        ARemote a = getAHome().findByPrimaryKey("a1");
        if (tx == TX_CONT) {
            a.assignBInNewTx(null);
        } else {
            a.assignB(null);
        }
        if (tx == TX_CALL) {
            utx.commit();
        } else if (tx == TX_RB) {
            utx.rollback();
        }
        // checking
        String idB = a.retrieveBInNewTx();
        if (tx == TX_RB) {
            assertEquals("Wrong assign null for relation a1->b1: ", "b1", idB);
        } else {
            assertNull("Wrong assign null for relation a1->b1: " + idB, idB);
            // undo
            a.assignBInNewTx("b1");
        }
        checkIsInitialState();
    }

    public void testBasicSetEmptyNullTxNo() throws Exception {
        _testBasicSetEmptyNull(TX_NO);
    }
    public void testBasicSetEmptyNullTxCall() throws Exception {
        _testBasicSetEmptyNull(TX_CALL);
    }
    public void testBasicSetEmptyNullTxCont() throws Exception {
        _testBasicSetEmptyNull(TX_CONT);
    }
    public void testBasicSetEmptyNullTxRb() throws Exception {
        _testBasicSetEmptyNull(TX_RB);
    }



    /**
     * test coherence relation one to one bidirectionnel,
     * A1.assignB(B2) => A1.retreiveB()=B2 && B2.retreiveA()=A1 && B1.retreiveA()=null && A2.retreiveB()=null
     */
    public void _testCohSetOne(int tx) throws Exception {
        if ((tx == TX_CALL) || (tx == TX_RB)) {
            utx.begin();
        }
        ARemote a1 = getAHome().findByPrimaryKey("a1");
        BRemote b1 = getBHome().findByPrimaryKey("b1");
        ARemote a2 = getAHome().findByPrimaryKey("a2");
        BRemote b2 = getBHome().findByPrimaryKey("b2");
        ARemote a3 = getAHome().findByPrimaryKey("a3");
        BRemote b3 = getBHome().findByPrimaryKey("b3");
        // change the relation
        if (tx == TX_CONT) {
            a1.assignBInNewTx("b2");
        } else {
            a1.assignB("b2");
        }
        if (tx == TX_CALL) {
            utx.commit();
        } else if (tx == TX_RB) {
            utx.rollback();
        }
        if (tx != TX_RB) {
            // Verify expected result
            utx.begin();
            try {
                assertNull("Bad coherence of relation null : expected for a2.retreiveB() found :" + a2.retrieveB(), a2.retrieveB());
                assertNull("Bad coherence of relation null : expected for b1.retreiveA() found :" + b1.retrieveA(), b1.retrieveA());
                assertEquals("Bad coherence of relation a1 : expected for b2.retreiveA() found :" + b2.retrieveA(), "a1", b2.retrieveA());
                assertEquals("Bad coherence of relation b2 : expected for a1.retreiveB() found :" + a1.retrieveB(), "b2", a1.retrieveB());

                // undo
                a1.assignB("b1");
                a2.assignB("b2");
            } finally {
                utx.commit();
            }
        }
        checkIsInitialState();
    }

    public void testCohSetOneTxNo() throws Exception {
        _testCohSetOne(TX_NO);
    }
    public void testCohSetOneTxCall() throws Exception {
        _testCohSetOne(TX_CALL);
    }

    /**
     * test coherence relation one to one bidirectionnel,
     * A3.assignB(B3)=>A3.retreiveB()==B3 && B3.retreiveB()==A3
     */
    public void _testCohWithoutRelation(int tx) throws Exception {
        if ((tx == TX_CALL) || (tx == TX_RB)) {
            utx.begin();
        }
        ARemote a3 = getAHome().findByPrimaryKey("a3");
        BRemote b3 = getBHome().findByPrimaryKey("b3");
        // change the relation
        if (tx == TX_CONT) {
            a3.assignBInNewTx("b3");
        } else {
            a3.assignB("b3");
        }
        if (tx == TX_CALL) {
            utx.commit();
        } else if (tx == TX_RB) {
            utx.rollback();
        }
        if (tx != TX_RB) {
            // Verify expected result
            utx.begin();
            try {
                assertEquals("Bad coherence of relation : b3 expected:" + a3.retrieveB(), "b3", a3.retrieveB());
                assertEquals("Bad coherence of relation : a3 expected:" + b3.retrieveA(), "a3", b3.retrieveA());

                // undo
                a3.assignB(null);
            } finally {
                utx.commit();
            }
        }
        checkIsInitialState();
    }

    public void testBasicCohWithoutRelation() throws Exception {
        _testCohWithoutRelation(TX_NO);
    }
    public void testBasicCohWithoutRelationTxCall() throws Exception {
        _testCohWithoutRelation(TX_CALL);
    }

    /**
     * test coherence relation one to one bidirectionnel,
     * A1.assignB(B3)=>A1.retreiveB()==B3 && B1.retreiveB()==null &&  B2.retreiveB()=null
     */
    public void _testCohAlreadyAssign(int tx) throws Exception {
        if ((tx == TX_CALL) || (tx == TX_RB)) {
            utx.begin();
        }
        ARemote a1 = getAHome().findByPrimaryKey("a1");
        BRemote b1 = getBHome().findByPrimaryKey("b1");
        BRemote b3 = getBHome().findByPrimaryKey("b3");
        // change the relation
        if (tx == TX_CONT) {
            a1.assignBInNewTx("b3");
        } else {
            a1.assignB("b3");
        }
        if (tx == TX_CALL) {
            utx.commit();
        } else if (tx == TX_RB) {
            utx.rollback();
        }
        if (tx != TX_RB) {
            // Verify expected result
            utx.begin();
            try {
                assertEquals("Bad coherence of relation : b3 expected:" + a1.retrieveB(), "b3", a1.retrieveB());
                assertNull("Bad coherence of relation : null expected:" + b1.retrieveA(), b1.retrieveA());
                assertEquals("Bad coherence of relation : a1 expected:" + b3.retrieveA(), "a1", b3.retrieveA());

                // undo
                a1.assignB("b1");
            } finally {
                utx.commit();
            }
        }
        checkIsInitialState();
    }

    public void testCohAlreadyAssign() throws Exception {
        _testCohAlreadyAssign(TX_NO);
    }
    public void testCohAlreadyAssignTxCall() throws Exception {
        _testCohAlreadyAssign(TX_CALL);
    }

    /**
     * test coherence relation one to one bidirectionnel,
     * A1.assignB(null)=>A1.retreiveB()==null && B1.retreiveB()==null
     */
    public void _testCohSetNull(int tx) throws Exception {
        if ((tx == TX_CALL) || (tx == TX_RB)) {
            utx.begin();
        }
        ARemote a1 = getAHome().findByPrimaryKey("a1");
        BRemote b1 = getBHome().findByPrimaryKey("b1");
        ARemote a2 = getAHome().findByPrimaryKey("a2");
        BRemote b2 = getBHome().findByPrimaryKey("b2");
        ARemote a3 = getAHome().findByPrimaryKey("a3");
        BRemote b3 = getBHome().findByPrimaryKey("b3");
        // change the relation
        if (tx == TX_CONT) {
            a1.assignBInNewTx(null);
        } else {
            a1.assignB(null);
        }
        if (tx == TX_CALL) {
            utx.commit();
        } else if (tx == TX_RB) {
            utx.rollback();
        }
        if (tx != TX_RB) {
            try {
                utx.begin();
                // Verify expected result
                assertNull("Bad coherence of relation : null expected for a1.retreiveB() found :" + a1.retrieveB(), a1.retrieveB());
                assertNull("Bad coherence of relation : null expected for b1.retreiveA() found :" + b1.retrieveA(), b1.retrieveA());

                // undo
                a1.assignB("b1");
            } finally {
                utx.commit();
            }
        }
        checkIsInitialState();
    }

    public void testCohSetNull() throws Exception {
        _testCohSetNull(TX_NO);
    }
    public void testCohSetNullTxCall() throws Exception {
        _testCohSetNull(TX_CALL);
    }

    /**
     * test coherence relation one to one bidirectionnel,
     * A1.remove=>A1removed && B1.retreiveA()==null
     */
    public void _testCohRemoveA(int tx) throws Exception {
        if (tx == TX_CONT) {
            // The transaction attribute of the remove method is TX_SUPPORT,
            // so the transaction cannot be initiate by the container
            fail("Transaction cannot be initiate by the container for this test");
        }
        if ((tx == TX_CALL) || (tx == TX_RB)) {
            utx.begin();
        }
        BRemote b1 = getBHome().findByPrimaryKey("b1");
        // remove the bean
        getAHome().remove("a1");
        if (tx == TX_CALL) {
            utx.commit();
        } else if (tx == TX_RB) {
            utx.rollback();
        }
        if (tx != TX_RB) {
            // Verify expected result
            assertNull("Bad coherence of relation : null expected for b1.retreiveB() found :" + b1.retrieveAInNewTx(), b1.retrieveAInNewTx());
            boolean not_found = false;
            try {
                ARemote a1 = getAHome().findByPrimaryKey("a1");
            } catch (ObjectNotFoundException e) {
                not_found = true;
            }
            assertTrue("a1 is not removed", not_found);
            // undo
            getAHome().create("a1");
            ARemote a1 = getAHome().findByPrimaryKey("a1");
            a1.assignBInNewTx("b1");
        }
        checkIsInitialState();
    }

    public void testCohRemoveATxNo() throws Exception {
        _testCohRemoveA(TX_NO);
    }
    public void testCohRemoveATxCall() throws Exception {
        _testCohRemoveA(TX_CALL);
    }
    public void testCohRemoveATxRb() throws Exception {
        _testCohRemoveA(TX_RB);
    }

    /**
     * test coherence relation one to one bidirectionnel,
     * getAHome().remove(a1)=>A1removed && B1.retreiveA()==null
     * Same as _testCohRemoveA except that the called remove method is on the bean
     * instead of the home.
     */
    public void _testCohBeanRemoveA(int tx) throws Exception {
        if (tx == TX_CONT) {
            // The transaction attribute of the remove method is TX_SUPPORT,
            // so the transaction cannot be initiate by the container
            fail("Transaction cannot be initiate by the container for this test");
        }

        if ((tx == TX_CALL) || (tx == TX_RB)) {
            utx.begin();
        }
        ARemote a1 = getAHome().findByPrimaryKey("a1");
        BRemote b1 = getBHome().findByPrimaryKey("b1");
        a1.remove();
        if (tx == TX_CALL) {
            utx.commit();
        } else if (tx == TX_RB) {
            utx.rollback();
        }
        if (tx != TX_RB) {
            // Verify expected result
            assertNull("Bad coherence of relation : null expected for b1.retreiveB() found :" + b1.retrieveAInNewTx(), b1.retrieveAInNewTx());
            boolean not_found=false;
            try {
                a1 = getAHome().findByPrimaryKey("a1");
            } catch (ObjectNotFoundException e) {
                not_found=true;
            }
            assertTrue("a1 is not removed",not_found);
            // undo
            getAHome().create("a1");
            a1 = getAHome().findByPrimaryKey("a1");
            a1.assignBInNewTx("b1");
        }
        checkIsInitialState();
    }

    public void testCohBeanRemoveATxNo() throws Exception {
        _testCohBeanRemoveA(TX_NO);
    }
    public void testCohBeanRemoveATxCall() throws Exception {
        _testCohBeanRemoveA(TX_CALL);
    }
    public void testCohBeanRemoveATxRb() throws Exception {
        _testCohBeanRemoveA(TX_RB);
    }

    /**
     * test coherence relation one to one bidirectionnel,
     * getBHome().remove(b1)=>B1 removed && A1.retreiveB()==null
     */
    public void _testCohRemoveB(int tx) throws Exception {
        if (tx == TX_CONT) {
            // The transaction attribute of the remove method is TX_SUPPORT,
            // so the transaction cannot be initiate by the container
            fail("Transaction cannot be initiate by the container for this test");
        }

        if ((tx == TX_CALL) || (tx == TX_RB)) {
            utx.begin();
        }
        ARemote a1 = getAHome().findByPrimaryKey("a1");
        BRemote b1 = getBHome().findByPrimaryKey("b1");
        ARemote a2 = getAHome().findByPrimaryKey("a2");
        BRemote b2 = getBHome().findByPrimaryKey("b2");
        ARemote a3 = getAHome().findByPrimaryKey("a3");
        BRemote b3 = getBHome().findByPrimaryKey("b3");
        // remove the bean
        getBHome().remove("b1");
        if (tx == TX_CALL) {
            utx.commit();
        } else if (tx == TX_RB) {
            utx.rollback();
        }
        if (tx != TX_RB) {
            // Verify expected result
            assertNull("Bad coherence of relation : null expected for a1.retreiveB() found :"+a1.retrieveBInNewTx(), a1.retrieveBInNewTx());
            boolean not_found=false;
            try {
                b1 = getBHome().findByPrimaryKey("b1");
            } catch (ObjectNotFoundException e) {
                not_found=true;
            }
            assertTrue("B is not removed",not_found);
            // undo
            getBHome().create("b1");
            a1.assignBInNewTx("b1");
        }
        checkIsInitialState();
    }

    public void testCohRemoveBTxNo() throws Exception {
        _testCohRemoveB(TX_NO);
    }
    public void testCohRemoveBTxCall() throws Exception {
        _testCohRemoveB(TX_CALL);
    }
    public void testCohRemoveBTxRb() throws Exception {
        _testCohRemoveB(TX_RB);
    }

    /**
     * test coherence relation one to one bidirectionnel,
     * B1.remove=>B1 removed && A1.retreiveB()==null
     * Same as  _testCohRemoveB except that the called remove method is on the bean
     * instead of the home.
     */
    public void _testCohBeanRemoveB(int tx) throws Exception {
        if (tx == TX_CONT) {
            // The transaction attribute of the remove method is TX_SUPPORT,
            // so the transaction cannot be initiate by the container
            fail("Transaction cannot be initiate by the container for this test");
        }

        if ((tx == TX_CALL) || (tx == TX_RB)) {
            utx.begin();
        }
        ARemote a1 = getAHome().findByPrimaryKey("a1");
        BRemote b1 = getBHome().findByPrimaryKey("b1");
        ARemote a2 = getAHome().findByPrimaryKey("a2");
        BRemote b2 = getBHome().findByPrimaryKey("b2");
        ARemote a3 = getAHome().findByPrimaryKey("a3");
        BRemote b3 = getBHome().findByPrimaryKey("b3");
        // change the relation
        b1.remove();
        if (tx == TX_CALL) {
            utx.commit();
        } else if (tx == TX_RB) {
            utx.rollback();
        }
        if (tx != TX_RB) {
            // Verify expected result
            assertNull("Bad coherence of relation : null expected for a1.retreiveB() found :"+a1.retrieveBInNewTx(), a1.retrieveBInNewTx());
            boolean not_found=false;
            try {
                b1 = getBHome().findByPrimaryKey("b1");
            } catch (ObjectNotFoundException e) {
                not_found=true;
            }
            assertTrue("B is not removed",not_found);
            // undo
            getBHome().create("b1");
            a1.assignBInNewTx("b1");
        }
        checkIsInitialState();
    }

    public void testCohBeanRemoveBTxNo() throws Exception {
        _testCohBeanRemoveB(TX_NO);
    }
    public void testCohBeanRemoveBTxCall() throws Exception {
        _testCohBeanRemoveB(TX_CALL);
    }
    public void testCohBeanRemoveBTxRb() throws Exception {
        _testCohBeanRemoveB(TX_RB);
    }

    /**
     * Combination of 2 tests that fails :
     * _testCohAlreadyAssign(TX_CALL) + _testCohRemoveA(TX_RB)
     */
    public void testMultiA1() throws Exception {
        ARemote a1 = getAHome().findByPrimaryKey("a1");
        BRemote b1 = getBHome().findByPrimaryKey("b1");
        BRemote b3 = getBHome().findByPrimaryKey("b3");
        utx.begin();
        a1.assignB("b3");
        utx.commit();
        a1.assignB("b1");
        utx.begin();
        getAHome().remove("a1");
        utx.rollback();
        String idB1 = a1.retrieveB();
        assertEquals("a1.b", "b1", idB1);
        checkIsInitialState();
    }

    public void testMultiA2() throws Exception {
        ARemote a1 = getAHome().findByPrimaryKey("a1");
        BRemote b1 = getBHome().findByPrimaryKey("b1");
        BRemote b3 = getBHome().findByPrimaryKey("b3");
        utx.begin();
        a1.assignB("b3");
        utx.commit();
        a1.assignB("b1");
        utx.begin();
        String idB1 = a1.retrieveB();
        utx.commit();
        assertEquals("a1.b", "b1", idB1);
        checkIsInitialState();
    }

    public void testMultiA3() throws Exception {
        ARemote a1 = getAHome().findByPrimaryKey("a1");
        BRemote b1 = getBHome().findByPrimaryKey("b1");
        BRemote b3 = getBHome().findByPrimaryKey("b3");
        utx.begin();
        a1.assignB("b3");
        utx.commit();
        a1.assignB("b1");
        utx.begin();
        String idA1 = b1.retrieveA();
        utx.commit();
        assertEquals("b1.a", "a1", idA1);
        checkIsInitialState();
    }

    public void testMultiA4() throws Exception {
        ARemote a1 = getAHome().findByPrimaryKey("a1");
        BRemote b1 = getBHome().findByPrimaryKey("b1");
        BRemote b3 = getBHome().findByPrimaryKey("b3");
        utx.begin();
        a1.assignB("b3");
        utx.commit();
        a1.assignB("b1");
        utx.begin();
        getBHome().remove("b1");
        utx.rollback();
        String idB1 = a1.retrieveB();
        assertEquals("a1.b", "b1", idB1);
        checkIsInitialState();
    }

    public void testMultiA5() throws Exception {
        ARemote a1 = getAHome().findByPrimaryKey("a1");
        BRemote b1 = getBHome().findByPrimaryKey("b1");
        BRemote b3 = getBHome().findByPrimaryKey("b3");
        utx.begin();
        a1.assignB("b3");
        utx.commit();
        a1.assignB("b1");
        utx.begin();
        getBHome().remove("b3");
        utx.rollback();
        String idB1 = a1.retrieveB();
        assertEquals("a1.b", "b1", idB1);
        checkIsInitialState();
    }

    public void testRollback() throws Exception {
        ARemote a = getAHome().findByPrimaryKey("a1");
        BRemote b = getBHome().findByPrimaryKey("b1");
        String orig = a.retrieveB();

        // disassociate and commit
        utx.begin();
        a.assignB(null);
        utx.commit();
        assertEquals(null, a.retrieveB());

        // revert to original state outside of a user transaction
        a.assignB(orig);

        // retrieveB inside a transaction
        utx.begin();
        assertEquals(orig, a.retrieveB());
        utx.rollback();

        // verify that retrieveB returns correct value after rollback
        try {
            assertEquals("a1", b.retrieveA());
            assertEquals(orig, a.retrieveB());
        } finally {
            // force sync to cleanup
            sync(true);
        }
        checkIsInitialState();
    }

    /**
     * Ensure the javax.ejb.EJBException is thrown when trying
     * to invoke an accessor method on a deleted entitybean object
     */
    public void testRemove1() throws Exception {
        Front fb = fhome.create();
        fb.testRemove1();

    }

}
