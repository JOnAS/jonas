/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.clients.distribution;

import java.rmi.NoSuchObjectException;
import java.rmi.ServerException;

public abstract class A_bankWrite extends A_bank {

    public A_bankWrite(String name) {
        super(name);
    }

    public void testBasicMove() throws Exception {
        ope(OP_MOVE, 0, 9, 1, 1);
    }

    public void testBasicMoveNC() throws Exception {
        ope(OP_MOVE, 0, 9, 1, 1, 5, false);
    }

    public void testBasicRemove() throws Exception {
        createOpe(OP_REMOVE, 3000, 3001, 1, 1);
    }

    public void testMultiRemove() throws Exception {
        createOpe(OP_REMOVE, 3010, 3040, 1, 1);
    }

    public void testManyRemove() throws Exception {
        createOpe(OP_REMOVE, 3000, 3200, 200, 1);
    }

    /**
     * Test the rollback
     */
    public void testBasicRB() throws Exception {
        ope(OP_MOVE, 0, 2, 4, 1, 700);
    }

    public void testMultiRB() throws Exception {
        createOpe(OP_MOVE, 0, 4, 4, 1, 600);
    }


    public void testBasicCreate() throws Exception {
        ope(OP_CREATE, 100, 150, 1, 1);
    }

    public void testShortCreate() throws Exception {
        ope(OP_CREATE, 6000, 8000, 100, 1);
    }

    public void testMultiCreate() throws Exception {
        ope(OP_CREATE, 1000, 1900, 1, 1);
    }


    public void testMultiMove() throws Exception {
        ope(OP_MOVE, 0, 5, 1, 1);
    }

    /**
     * test access on removed account
     * spec EJB 2.1 page 406 (18.3.6/18.3.7) says that the container
     * must throw a NoSuchObjectException to the client.
     */
    public void testAccessRemoved() throws Exception {
        int num = 700;
        // create the account if it doesn't exist yet
        manager.readBalance(num);
        manager.delAccount(num);
        try {
            // read balance for last accessed account
            manager.readBalance();
            fail("must throw NoSuchObjectException");
        } catch (NoSuchObjectException e) {
            manager = null;     // avoids that all other tests fail
        } catch (ServerException e) {
            manager = null;     // avoids that all other tests fail
            if (! (e.getCause() instanceof NoSuchObjectException)) {
                fail("Should receive NoSuchObjectException instead of :" + e);
            }
        }
    }
    
    /**
     * Test that an entity created inside a transaction rollback only is not accessible
     * Emulate a problem raised by safir project
     */
    public void testCreateRollbackOnly() throws Exception {
        int num = 800;
        manager.createRollbackOnly(num);
        try {
            // read balance for last accessed account
            manager.readBalance();
            fail("must throw NoSuchObjectException");
        } catch (NoSuchObjectException e) {
            manager = null;     // avoids that all other tests fail
        } catch (ServerException e) {
            manager = null;     // avoids that all other tests fail
            if (! (e.getCause() instanceof NoSuchObjectException)) {
                fail("Should receive NoSuchObjectException instead of :" + e);
            }
        }
    }
}
