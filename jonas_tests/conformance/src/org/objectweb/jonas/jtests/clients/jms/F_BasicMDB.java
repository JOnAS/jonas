/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.clients.jms;

import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;
import junit.framework.Test;
import junit.framework.TestSuite;
import org.objectweb.jonas.jtests.util.JTestCase;
import org.objectweb.jonas.jtests.beans.message.Sender;
import org.objectweb.jonas.jtests.beans.message.SenderHome;
import org.objectweb.jonas.jtests.beans.message.Sender1_1;
import org.objectweb.jonas.jtests.beans.message.Sender1_1Home;

public class F_BasicMDB extends JTestCase {

    private static String BEAN_HOME = "messageSenderSFHome";
    private static String BEAN1_1_HOME = "messageSender1_1SFHome";
    protected static SenderHome home = null;
    protected static Sender1_1Home home1 = null;

    public F_BasicMDB(String name) {
        super(name);
    }

    public SenderHome getHome() {
        if (home == null) {
            try {
                home = (SenderHome) PortableRemoteObject.narrow(ictx.lookup(BEAN_HOME), SenderHome.class);
            } catch (NamingException e) {
                fail("Cannot get bean home");
            }
        }
        return home;
    }

    public Sender1_1Home getHome1() {
        if (home1 == null) {
            try {
                home1 = (Sender1_1Home) PortableRemoteObject.narrow(ictx.lookup(BEAN1_1_HOME), Sender1_1Home.class);
            } catch (NamingException e) {
                fail("Cannot get bean home1");
            }
        }
        return home1;
    }

    /**
     * init environment:
     * - load beans
     * - create/init database for entities.
     */
    protected void setUp() {
        super.setUp();
        useBeans("message", true);
    }

    // --------------------------------------------------------
    // Basic Tests on Topics
    // --------------------------------------------------------

    /**
     * Basic test: Send 1 message on a topic
     * 2 MDB are reading the topic.
     * No tx.
     */
    public void testBasicSendOnTopic1() throws Exception {
        Sender s = getHome().create();
        int val = 200;
        s.sendOnTopic("JunitTopic1", val, 1);
        assertEquals(2, s.check(val, 2, 4));
        s.remove();
    }

    /**
     * Basic test: Send 1 message on a topic Durable
     * 2 MDB are reading the topic.
     * No tx.
     */
    public void testBasicSendOnTopic2() throws Exception {
        Sender s = getHome().create();
        int val = 200;
        s.sendOnTopic("JunitTopic2", val, 1);
        assertEquals(2, s.check(val, 2, 4));
        s.remove();
    }

    /**
     * Basic test: Send nb messages on a topic
     * 2 MDB are reading the topic.
     * No tx.
     */
    public void testMultipleSendOnTopic1() throws Exception {
        Sender s = getHome().create();
        int val = 210;
        int nb = 10;
        s.sendOnTopic("JunitTopic1", val, nb);
        assertEquals(nb * 2, s.check(val, nb * 2, 4));
        s.remove();
    }

    /**
     * Basic test: Send 1 message on a topic
     * 2 MDB are reading the topic.
     * tx.
     */
    public void testBasicSendOnTopic1Tx() throws Exception {
        Sender s = getHome().create();
        int val = 201;
        s.sendOnTopicTx("JunitTopic1", val, 1);
        assertEquals(2, s.check(val, 2, 4));
        s.remove();
    }

    /**
     * Basic test: Send n messages on a topic
     * 2 MDB are reading the topic.
     * tx.
     */
    public void testMutipleSendOnTopic1Tx() throws Exception {
        Sender s = getHome().create();
        int val = 211;
        int nb = 10;
        s.sendOnTopicTx("JunitTopic1", val, nb);
        assertEquals(2 * nb, s.check(val, 2 * nb, 4));
        s.remove();
    }

    /**
     * Basic test: send a message on a topic in a transaction committed
     */
    public void testCommitSendOnTopic1() throws Exception {

        Sender s = getHome().create();
        int val = 206;
        utx.begin();
        s.sendOnTopic("JunitTopic1", val, 1);
        utx.commit();
        assertEquals(2, s.check(val, 2, 4));
        s.remove();
    }

    /**
     * Basic test: send n message on a topic in a transaction committed
     */
    public void testNCommitSendOnTopic1() throws Exception {

        Sender s = getHome().create();
        int val = 206;
        int nb = 12;
        utx.begin();
        s.sendOnTopic("JunitTopic1", val, nb);
        utx.commit();
        assertEquals(2*nb, s.check(val, 2*nb, 4));
        s.remove();
    }

    /**
     * Basic test: send more messages on a topic in a transaction committed
     */
    public void testManySendOnTopic() throws Exception {

        Sender s = getHome().create();
        int val = 2060;
        int nb = 120;
        s.sendOnTopic("JunitTopic1", val, nb);
        assertEquals(2*nb, s.check(val, 2*nb, 6));
        s.remove();
    }

    /**
     * Basic test: Send more messages on a topic
     * 2 MDB are reading the topic.
     * tx.
     */
    public void testManySendOnTopicTx() throws Exception {
        Sender s = getHome().create();
        int val = 2110;
        int nb = 100;
        s.sendOnTopicTx("JunitTopic1", val, nb);
        assertEquals(2 * nb, s.check(val, 2 * nb, 8));
        s.remove();
    }

    // --------------------------------------------------------
    // Basic Tests on Queues
    // --------------------------------------------------------

    /**
     * Basic test: Send 1 message on a queue
     * No tx, MDB transacted.
     */
    public void testBasicSendOnQueue1() throws Exception {

        Sender s = getHome().create();
        int val = 100;
        s.sendOnQueue("JunitQueue1", val, 1);
        assertEquals(1, s.check(val, 1, 4));
        s.remove();
    }

    /**
     * Basic test: Send n messages on a queue
     * No tx, MDB transacted.
     */
    public void testMultipleSendOnQueue1() throws Exception {
        Sender s = getHome().create();
        int val = 100;
        int nb = 5;
        s.sendOnQueue("JunitQueue1", val, nb);
        assertEquals(nb, s.check(val, nb, 8));
        s.remove();
    }

    /**
     * Basic test: Send 1 message on a queue
     * tx, MDB transacted.
     */
    public void testBasicSendOnQueue1Tx() throws Exception {
        Sender s = getHome().create();
        int val = 101;
        s.sendOnQueueTx("JunitQueue1", val, 1);
        assertEquals(1, s.check(val, 1, 4));
        s.remove();
    }

    /**
     * Basic test: Send nb messages on a queue
     * tx, MDB transacted.
     */
    public void testMultipleSendOnQueue1Tx() throws Exception {
        Sender s = getHome().create();
        int val = 121;
        int nb = 20;
        s.sendOnQueueTx("JunitQueue1", val, nb);
        assertEquals(nb, s.check(val, nb, 8));
        s.remove();
    }

    /**
     * Basic test: Send 1 message on a queue
     * No tx, MDB not transacted.
     */
    public void testBasicSendOnQueue2() throws Exception {
        Sender s = getHome().create();
        int val = 102;
        s.sendOnQueue("JunitQueue2", val, 1);
        assertEquals(1, s.check(val, 1, 4));
        s.remove();
    }

    /**
     * Basic test: Send 1 message on a queue
     * tx, MDB not transacted.
     */
    public void testBasicSendOnQueue2Tx() throws Exception {
        Sender s = getHome().create();
        int val = 103;
        s.sendOnQueueTx("JunitQueue2", val, 1);
        assertEquals(1, s.check(val, 1, 4));
        s.remove();
    }


    /**
     * Basic test: send a message on a queue in a transaction rolled back
     * MDB not transacted.
     */
    public void testRollbackSendOnQueue2() throws Exception {
        Sender s = getHome().create();
        int val = 105;
        utx.begin();
        s.sendOnQueue("JunitQueue2", val, 1);
        utx.rollback();
        assertEquals(0, s.check(val, 1, 4));
        s.remove();
    }


    /**
     * Basic test: send a message on a queue in a transaction committed
     * MDB transacted.
     */
    public void testCommitSendOnQueue1() throws Exception {
        Sender s = getHome().create();
        int val = 106;
        utx.begin();
        s.sendOnQueue("JunitQueue1", val, 1);
        utx.commit();
        assertEquals(1, s.check(val, 1, 4));
        s.remove();
    }


    /**
     * Basic test: send a message on a queue in a transaction committed
     * MDB not transacted.
     */
    public void testCommitSendOnQueue2() throws Exception {
        Sender s = getHome().create();
        int val = 107;
        utx.begin();
        s.sendOnQueue("JunitQueue2", val, 1);
        utx.commit();
        assertEquals(1, s.check(val, 1, 4));
        s.remove();
    }



    // --------------------------------------------------------
    // Basic Tests on Topics (JMS1.1)
    // --------------------------------------------------------

    /**
     * Basic test: Send 1 message on a topic
     * 2 MDB are reading the topic.
     * No tx.
     */
    public void testBasicSendOnDestTopic1() throws Exception {
        Sender1_1 s = getHome1().create();
        int val = 200;
        s.sendOnDestination("JunitTopic1", val, 1);
        assertEquals(2, s.check(val, 2, 4));
        s.remove();
    }

    /**
     * Basic test: Send 1 message on a topic Durable
     * 2 MDB are reading the topic.
     * No tx.
     */
    public void testBasicSendOnDestTopic2() throws Exception {
        Sender1_1 s = getHome1().create();
        int val = 200;
        s.sendOnDestination("JunitTopic2", val, 1);
        assertEquals(2, s.check(val, 2, 4));
        s.remove();
    }

    /**
     * Basic test: Send nb messages on a topic
     * 2 MDB are reading the topic.
     * No tx.
     */
    public void testMultipleSendOnDestTopic1() throws Exception {
        Sender1_1 s = getHome1().create();
        int val = 210;
        int nb = 10;
        s.sendOnDestination("JunitTopic1", val, nb);
        assertEquals(nb * 2, s.check(val, nb * 2, 8));
        s.remove();
    }

    /**
     * Basic test: Send 1 message on a topic
     * 2 MDB are reading the topic.
     * tx.
     */
    public void testBasicSendOnDestTopic1Tx() throws Exception {
        Sender1_1 s = getHome1().create();
        int val = 201;
        s.sendOnDestinationTx("JunitTopic1", val, 1);
        assertEquals(2, s.check(val, 2, 4));
        s.remove();
    }

    /**
     * Basic test: Send n messages on a topic
     * 2 MDB are reading the topic.
     * tx.
     */
    public void testMutipleSendOnDestTopic1Tx() throws Exception {
        Sender1_1 s = getHome1().create();
        int val = 211;
        int nb = 10;
        s.sendOnDestinationTx("JunitTopic1", val, nb);
        assertEquals(2*nb, s.check(val, 2*nb, 4));
        s.remove();
    }

    /**
     * Basic test: send a message on a topic in a transaction committed
     */
    public void testCommitSendOnDestTopic1() throws Exception {

        Sender1_1 s = getHome1().create();
        int val = 206;
        utx.begin();
        s.sendOnDestination("JunitTopic1", val, 1);
        utx.commit();
        assertEquals(2, s.check(val, 2, 4));
        s.remove();
    }

    /**
     * Basic test: send n message on a topic in a transaction committed
     */
    public void testNCommitSendOnDestTopic1() throws Exception {

        Sender1_1 s = getHome1().create();
        int val = 206;
        int nb = 12;
        utx.begin();
        s.sendOnDestination("JunitTopic1", val, nb);
        utx.commit();
        assertEquals(2 * nb, s.check(val, 2 * nb, 4));
        s.remove();
    }

    /**
     * Basic test: send more messages on a topic in a transaction committed
     */
    public void testManySendOnDestTopic() throws Exception {

        Sender1_1 s = getHome1().create();
        int val = 2061;
        int nb = 100;
        s.sendOnDestination("JunitTopic1", val, nb);
        assertEquals(2*nb, s.check(val, 2*nb, 8));
        s.remove();
    }

    /**
     * Basic test: Send more messages on a topic
     * 2 MDB are reading the topic.
     * tx.
     */
    public void testManySendOnDestTopicTx() throws Exception {
        Sender1_1 s = getHome1().create();
        int val = 2111;
        int nb = 100;
        s.sendOnDestinationTx("JunitTopic1", val, nb);
        assertEquals(2 * nb, s.check(val, 2 * nb, 8));
        s.remove();
    }

    // --------------------------------------------------------
    // Basic Tests on Queues (JMS1.1)
    // --------------------------------------------------------

    /**
     * Basic test: Send 1 message on a queue
     * No tx, MDB transacted.
     */
    public void testBasicSendOnDestQueue1() throws Exception {

        Sender1_1 s = getHome1().create();
        int val = 100;
        s.sendOnDestination("JunitQueue1", val, 1);
        assertEquals(1, s.check(val, 1, 4));
        s.remove();
    }

    /**
     * Basic test: Send n messages on a queue
     * No tx, MDB transacted.
     */
    public void testMultipleSendOnDestQueue1() throws Exception {
        Sender1_1 s = getHome1().create();
        int val = 100;
        int nb = 20;
        s.sendOnDestination("JunitQueue1", val, nb);
        assertEquals(nb, s.check(val, nb, 8));
        s.remove();
    }

    /**
     * Basic test: Send 1 message on a queue
     * tx, MDB transacted.
     */
    public void testBasicSendOnDestQueue1Tx() throws Exception {
        Sender1_1 s = getHome1().create();
        int val = 101;
        s.sendOnDestinationTx("JunitQueue1", val, 1);
        assertEquals(1, s.check(val, 1, 4));
        s.remove();
    }

    /**
     * Basic test: Send nb messages on a queue
     * tx, MDB transacted.
     */
    public void testMultipleSendOnDestQueue1Tx() throws Exception {
        Sender1_1 s = getHome1().create();
        int val = 121;
        int nb = 20;
        s.sendOnDestinationTx("JunitQueue1", val, nb);
        assertEquals(nb, s.check(val, nb, 8));
        s.remove();
    }

    /**
     * Basic test: Send 1 message on a queue
     * No tx, MDB not transacted.
     */
    public void testBasicSendOnDestQueue2() throws Exception {
        Sender1_1 s = getHome1().create();
        int val = 102;
        s.sendOnDestination("JunitQueue2", val, 1);
        assertEquals(1, s.check(val, 1, 4));
        s.remove();
    }

    /**
     * Basic test: Send 1 message on a queue
     * tx, MDB not transacted.
     */
    public void testBasicSendOnDestQueue2Tx() throws Exception {
        Sender1_1 s = getHome1().create();
        int val = 103;
        s.sendOnDestinationTx("JunitQueue2", val, 1);
        assertEquals(1, s.check(val, 1, 4));
        s.remove();
    }


    /**
     * Basic test: send a message on a queue in a transaction rolled back
     * MDB not transacted.
     */
    public void testRollbackSendOnDestQueue2() throws Exception {
        Sender1_1 s = getHome1().create();
        int val = 105;
        utx.begin();
        s.sendOnDestination("JunitQueue2", val, 1);
        utx.rollback();
        assertEquals(0, s.check(val, 1, 4));
        s.remove();
    }


    /**
     * Basic test: send a message on a queue in a transaction committed
     * MDB transacted.
     */
    public void testCommitSendOnDestQueue1() throws Exception {
        Sender1_1 s = getHome1().create();
        int val = 106;
        utx.begin();
        s.sendOnDestination("JunitQueue1", val, 1);
        utx.commit();
        assertEquals(1, s.check(val, 1, 4));
        s.remove();
    }


    /**
     * Basic test: send a message on a queue in a transaction committed
     * MDB not transacted.
     */
    public void testCommitSendOnDestQueue2() throws Exception {
        Sender1_1 s = getHome1().create();
        int val = 107;
        utx.begin();
        s.sendOnDestination("JunitQueue2", val, 1);
        utx.commit();
        assertEquals(1, s.check(val, 1, 4));
        s.remove();
    }



    /**
     * Run all the tests
     */
    public static Test suite() {
        return new TestSuite(F_BasicMDB.class);
    }

    public static void main (String args[]) {
        String testtorun = null;
        // Get args
        for (int argn = 0; argn < args.length; argn++) {
            String s_arg = args[argn];
            Integer i_arg;
            if (s_arg.equals("-n")) {
                testtorun = args[++argn];
            }
        }
        if (testtorun == null) {
            junit.textui.TestRunner.run(suite());
        } else {
            junit.textui.TestRunner.run(new F_BasicMDB(testtorun));
        }
    }
}
