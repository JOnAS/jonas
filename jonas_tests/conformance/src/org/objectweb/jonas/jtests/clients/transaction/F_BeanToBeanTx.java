/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.clients.transaction;

import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;
import junit.framework.Test;
import junit.framework.TestSuite;
import org.objectweb.jonas.jtests.beans.transacted.Simple;
import org.objectweb.jonas.jtests.beans.transacted.SimpleEHome;
import org.objectweb.jonas.jtests.beans.transacted.SimpleSHome;
import org.objectweb.jonas.jtests.util.JTestCase;

/**
 * Test for Bean to Bean transactions
 * @author Ph.Durieux
 */
public class F_BeanToBeanTx extends JTestCase {

    protected static SimpleSHome syhome = null;
    protected static SimpleSHome sfhome = null;
    protected static SimpleSHome slhome = null;
    protected static SimpleEHome echome = null;
    protected static SimpleEHome ec2home = null;
    protected static SimpleEHome ebhome = null;

    public F_BeanToBeanTx(String name) {
        super(name);
    }

    protected void setUp() {
        super.setUp();
        useBeans("transacted", true);
        if (syhome == null) {
            String BEAN_HOME = "transactedSimpleSYHome";
            try {
                syhome = (SimpleSHome) PortableRemoteObject.narrow(ictx.lookup(BEAN_HOME), SimpleSHome.class);
            } catch (NamingException e) {
                fail("Cannot get " + BEAN_HOME + ":" + e);
            }
        }
        if (sfhome == null) {
            String BEAN_HOME = "transactedSimpleSFHome";
            try {
                sfhome = (SimpleSHome) PortableRemoteObject.narrow(ictx.lookup(BEAN_HOME), SimpleSHome.class);
            } catch (NamingException e) {
                fail("Cannot get " + BEAN_HOME + ":" + e);
            }
        }
        if (slhome == null) {
            String BEAN_HOME = "transactedSimpleSLHome";
            try {
                slhome = (SimpleSHome) PortableRemoteObject.narrow(ictx.lookup(BEAN_HOME), SimpleSHome.class);
            } catch (NamingException e) {
                fail("Cannot get " + BEAN_HOME + ":" + e);
            }
        }
        if (echome == null) {
            String BEAN_HOME = "transactedSimpleECHome";
            try {
                echome = (SimpleEHome) PortableRemoteObject.narrow(ictx.lookup(BEAN_HOME), SimpleEHome.class);
            } catch (NamingException e) {
                fail("Cannot get " + BEAN_HOME + ":" + e);
            }
        }
        if (ec2home == null) {
            String BEAN_HOME = "transactedSimpleEC2Home";
            try {
                ec2home = (SimpleEHome) PortableRemoteObject.narrow(ictx.lookup(BEAN_HOME), SimpleEHome.class);
            } catch (NamingException e) {
                fail("Cannot get " + BEAN_HOME + ":" + e);
            }
        }
        if (ebhome == null) {
            String BEAN_HOME = "transactedSimpleEBHome";
            try {
                ebhome = (SimpleEHome) PortableRemoteObject.narrow(ictx.lookup(BEAN_HOME), SimpleEHome.class);
            } catch (NamingException e) {
                fail("Cannot get " + BEAN_HOME + ":" + e);
            }
        }
    }

    /**
	 * SL/Required -> EC/RequiresNew
	 */
	public void testSLToEC() throws Exception {
		Simple s1 = slhome.create();
		Simple s2 = echome.create(30);
		try {
			assertEquals(true, s1.call_requires_new_on(s2));
		} finally {
			s1.remove();
			s2.remove();
		}
	}

	/**
	 * SL/Required -> EC2/RequiresNew
	 */
	public void testSLToEC2() throws Exception {
		Simple s1 = slhome.create();
		Simple s2 = ec2home.create(310);
		try {
			assertEquals(true, s1.call_requires_new_on(s2));
		} finally {
			s1.remove();
			s2.remove();
		}
	}

	/**
	 * SF/Required -> EC/RequiresNew
	 */
	public void testSFToEC() throws Exception {
		Simple s1 = sfhome.create();
		Simple s2 = echome.create(32);
		try {
			assertEquals(true, s1.call_requires_new_on(s2));
		} finally {
			s1.remove();
			s2.remove();
		}
	}

	/**
	 * SF/Required -> EC2/RequiresNew
	 */
	public void testSFToEC2() throws Exception {
		Simple s1 = sfhome.create();
		Simple s2 = ec2home.create(33);
		try {
			assertEquals(true, s1.call_requires_new_on(s2));
		} finally {
			s1.remove();
			s2.remove();
		}
	}

	/**
	 * SY/Required -> EC/RequiresNew
	 */
	public void testSYToEC() throws Exception {
		Simple s1 = syhome.create();
		Simple s2 = echome.create(34);
		try {
			assertEquals(true, s1.call_requires_new_on(s2));
		} finally {
			s1.remove();
			s2.remove();
		}
	}

	/**
	 * SY/Required -> EC2/RequiresNew
	 */
	public void testSYToEC2() throws Exception {
		Simple s1 = syhome.create();
		Simple s2 = ec2home.create(35);
		try {
			assertEquals(true, s1.call_requires_new_on(s2));
		} finally {
			s1.remove();
			s2.remove();
		}
	}

	/**
	 * EC/Required -> EC/RequiresNew
	 */
	public void testECToEC() throws Exception {
		Simple s1 = echome.create(36);
		Simple s2 = echome.create(37);
		try {
			assertEquals(true, s1.call_requires_new_on(s2));
		} finally {
			s1.remove();
			s2.remove();
		}
	}

	/**
	 * EC2/Required -> EC2/RequiresNew
	 */
	public void testEC2ToEC2() throws Exception {
		Simple s1 = ec2home.create(38);
		Simple s2 = ec2home.create(39);
		try {
			assertEquals(true, s1.call_requires_new_on(s2));
		} finally {
			s1.remove();
			s2.remove();
		}
	}

	/**
	 * EB/Required -> EC/RequiresNew
	 */
	public void testEBToEC() throws Exception {
		Simple s1 = ebhome.create(40);
		Simple s2 = echome.create(41);
		try {
			assertEquals(true, s1.call_requires_new_on(s2));
		} finally {
			s1.remove();
			s2.remove();
		}
	}

	/**
	 * EB/Required -> EC2/RequiresNew
	 */
	public void testEBToEC2() throws Exception {
		Simple s1 = ebhome.create(42);
		Simple s2 = ec2home.create(43);
		try {
			assertEquals(true, s1.call_requires_new_on(s2));
		} finally {
			s1.remove();
			s2.remove();
		}
	}

	/**
	 * SL/Required -> SY/RequiresNew
	 */
	public void testSLToSY() throws Exception {
		Simple s1 = slhome.create();
		Simple s2 = syhome.create();
		try {
			assertEquals(true, s1.call_requires_new_on(s2));
		} finally {
			s1.remove();
			s2.remove();
		}
	}

	/**
	 * SF/Required -> SY/RequiresNew
	 */
	public void testSFToSY() throws Exception {
		Simple s1 = sfhome.create();
		Simple s2 = syhome.create();
		try {
			assertEquals(true, s1.call_requires_new_on(s2));
		} finally {
			s1.remove();
			s2.remove();
		}
	}

	/**
	 * SY/Required -> SY/RequiresNew
	 */
	public void testSYToSY() throws Exception {
		Simple s1 = syhome.create();
		Simple s2 = syhome.create();
		try {
			assertEquals(true, s1.call_requires_new_on(s2));
		} finally {
			s1.remove();
			s2.remove();
		}
	}

	/**
	 * EC/Required -> SY/RequiresNew
	 */
	public void testECToSY() throws Exception {
		Simple s1 = echome.create(44);
		Simple s2 = syhome.create();
		try {
			assertEquals(true, s1.call_requires_new_on(s2));
		} finally {
			s1.remove();
			s2.remove();
		}
	}

	/**
	 * EC2/Required -> SY/RequiresNew
	 */
	public void testEC2ToSY() throws Exception {
		Simple s1 = ec2home.create(45);
		Simple s2 = syhome.create();
		try {
			assertEquals(true, s1.call_requires_new_on(s2));
		} finally {
			s1.remove();
			s2.remove();
		}
	}

	public void testSupports2Required() throws Exception {
		Simple s2 = sfhome.create();
		try {
			assertEquals(true, s2.supports_call_required());
		} finally {
			s2.remove();
		}
	}

	/**
	 * EB/Required -> SY/RequiresNew
	 */
	public void testEBToSY() throws Exception {
		Simple s1 = ebhome.create(46);
		Simple s2 = syhome.create();
		try {
			assertEquals(true, s1.call_requires_new_on(s2));
		} finally {
			s1.remove();
			s2.remove();
		}
	}

	public static Test suite() {
		return new TestSuite(F_BeanToBeanTx.class);
	}

	public static void main(String args[]) {
		String testtorun = null;
		// Get args
		for (int argn = 0; argn < args.length; argn++) {
			String s_arg = args[argn];
			Integer i_arg;
			if (s_arg.equals("-n")) {
				testtorun = args[++argn];
			}
		}
		if (testtorun == null) {
			junit.textui.TestRunner.run(suite());
		} else {
			junit.textui.TestRunner.run(new F_BeanToBeanTx(testtorun));
		}
	}
}
