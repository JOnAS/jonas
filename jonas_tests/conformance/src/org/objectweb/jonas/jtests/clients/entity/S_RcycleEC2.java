/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.clients.entity;

import java.util.Collection;
import java.util.Iterator;

import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

import junit.framework.Test;
import junit.framework.TestSuite;

import org.objectweb.jonas.jtests.beans.relation.rcycle.PersonRemote;
import org.objectweb.jonas.jtests.beans.relation.rcycle.PersonHomeRemote;
import org.objectweb.jonas.jtests.util.JTestCase;

/**
 * This test suite  collects some test cases of F_RcycleEC2 that are failing
 * with HSQLDB
 *
 * @author Helene Joanin Philippe Coq
 */
public class S_RcycleEC2 extends JTestCase {

    private static final int ID_L_ERIC = 1;

    private static final int ID_JL_HELENE = 2;

    private static final int ID_L_GUILHEM = 3;

    private static final int ID_L_MALVA = 4;

    private static PersonHomeRemote personhome = null;

    public S_RcycleEC2(String name) {
        super(name);
    }

    protected boolean isInit = false;

    protected void setUp() {
        super.setUp();
        if (!isInit) {
            useBeans("rcycle", true);
            try {
                personhome = (PersonHomeRemote) PortableRemoteObject.narrow(ictx.lookup("RcyclePersonHome"),
                        PersonHomeRemote.class);
            } catch (NamingException e) {
                fail("Cannot get bean home: " + e.getMessage());
            }
            isInit = true;
        }
    }



    /**
     * Test the spouse relation.
     * @throws Exception
     */
    public void testSpouseRelation() throws Exception {
        PersonRemote ph = personhome.findByPrimaryKey(new Integer(ID_L_ERIC));
        Integer iw = ph.retrieveSpouse();
        assertEquals("Wife of Laurent Eric: ", ID_JL_HELENE, iw.intValue());
        PersonRemote pw = personhome.findByPrimaryKey(iw);
        Integer ih = pw.retrieveSpouse();
        assertEquals("Husband of Joanin-Laurent Helene: ", ID_L_ERIC, ih.intValue());
        PersonRemote pc = personhome.findByPrimaryKey(new Integer(ID_L_GUILHEM));
        Integer in = pc.retrieveSpouse();
        assertNull("Laurent Guilhem spouse: ", in);
    }

    /**
     * Test the guardian/is-guardian-of relation.
     * @throws Exception
     */
    public void testGuardianRelation() throws Exception {
        Integer iLEric = new Integer(ID_L_ERIC);
        PersonRemote ph = personhome.findByPrimaryKey(iLEric);
        Collection c = ph.retrieveGuardianOf();
        assertEquals("Laurent Eric guardian of: ", 2, c.size());
        for (Iterator i = c.iterator(); i.hasNext();) {
            Integer ip = (Integer) i.next();
            PersonRemote p = personhome.findByPrimaryKey(ip);
            assertEquals("Guardian of " + p.getName() + ": ", iLEric, p.retrieveGuardian());
        }
    }

    /**
     * Test the parents/children relation.
     * @throws Exception
     */
    public void testParentsChildrenRelation() throws Exception {
        Integer iLEric = new Integer(ID_L_ERIC);
        PersonRemote ph = personhome.findByPrimaryKey(iLEric);
        Collection cc = ph.retrieveChildren();
        assertEquals("Laurent Eric father of: ", 2, cc.size());
        for (Iterator i = cc.iterator(); i.hasNext();) {
            Integer ip = (Integer) i.next();
            PersonRemote p = personhome.findByPrimaryKey(ip);
            Collection cp = p.retrieveParents();
            assertTrue("Father of " + p.getName() + ": ", cp.contains(iLEric));
        }
    }

    /**
     * Test if a cmr null is 'null'
     * @throws Exception
     */
    public void testCmrNull() throws Exception {
        PersonRemote ph = personhome.findByPrimaryKey(new Integer(ID_L_ERIC));
        assertTrue("CMR null is NOT null", ph.testCmrNull());
    }

    /**
     * Verify the spy trace to check less database access are done
     * @throws Exception
     */
    public void testPrefetch() throws Exception {
        //sleep(15000);
        try {
            utx.begin();
            Integer iLEric = new Integer(ID_L_ERIC);
            PersonRemote ph = personhome.findByPrimaryKey(iLEric);
            // If there is CMR prefetching, this may imply less database access
            Collection cc = ph.retrieveChildrenNames();
            assertEquals("Laurent Eric father of: ", 2, cc.size());
        } finally {
            utx.commit();
        }
    }

    protected boolean initStateOK() throws Exception {
        return true;
    }

    public static Test suite() {
        return new TestSuite(S_RcycleEC2.class);
    }

    public static void main(String args[]) {
        String testtorun = null;
        // Get args
        for (int argn = 0; argn < args.length; argn++) {
            String sarg = args[argn];
            if (sarg.equals("-n")) {
                testtorun = args[++argn];
            }
        }
        if (testtorun == null) {
            junit.textui.TestRunner.run(suite());
        } else {
            junit.textui.TestRunner.run(new S_RcycleEC2(testtorun));
        }
    }
}
