/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.objectweb.jonas.jtests.clients.transaction;

import java.rmi.RemoteException;
import javax.transaction.TransactionRequiredException;
import org.objectweb.jonas.jtests.beans.transacted.Simple;
import org.objectweb.jonas.jtests.beans.transacted.SimpleEHome;


/**
 * Here are found transactional attribute tests
 * common but specific for all type of entity beans (BMP, CMP1.1, CMP2.0)
 * Tests on finder and create methods
 * @author Philippe Durieux (jonas team)
 */
public abstract class A_TxAttributeEntity extends A_TxAttribute {

    /**
     * constructor
     * @param name name of the test suite.
     */
    public A_TxAttributeEntity(String name) {
        super(name);
    }

    /**
     * return SimpleHome, that can be either BMP, CMP1, or CMP2 bean.
     */
    protected abstract SimpleEHome getHome();

    // -----------------------------------------------------------------
    // tests on Create methods
    // -----------------------------------------------------------------

    /**
     * Test Required transactional attributes on create method
     * the create method xith required is called outside TX 
     * 
     */
    public void testCreateRequired() throws Exception {
        long i = 100;
        Simple entity = getHome().createForRequired(i);
        entity.remove();
    }

    /**
     * Test Required transactional attributes on create method
     * the create method xith required is called inside TX 
     * 
     */
    public void testCreateRequiredTx() throws Exception {
        utx.begin();
        long i = 110;
        Simple entity = null;
        try {
            entity = getHome().createForRequired(i);
        } finally {
            utx.rollback();        
        }

    }
    /**
     * Test NotSupported transactional attributes on create method
     * the create method xith required is called outside TX 
     * 
     */
    public void testCreateNotSupported() throws Exception {
        int i = 120;
        Simple entity = getHome().create(i);
        entity.remove();
    }

    /**
     * Test NotSupported transactional attributes on create method
     * the create method xith required is called inside TX 
     * 
     */
    public void testCreateNotSupportedTx() throws Exception {
        utx.begin();
        int i = 130;
        Simple entity = null;
        try {
            entity = getHome().create(i);
        } finally {
            utx.rollback();      
            entity.remove();
        }
    }

    /**
     * Test Never transactional attributes on create method
     * the create method xith required is called outside TX 
     * 
     */
    public void testCreateNever() throws Exception {
        short i = 140;
        Simple entity = getHome().createForNever(i);
        entity.remove();
    }

    /**
     * Test Never transactional attributes on create method
     * the create method xith required is called inside TX 
     * 
     */
    public void testCreateNeverTx() throws Exception {
        utx.begin();
        short i = 150;
        Simple entity = null;
        try {
            entity = getHome().createForNever(i);
            entity.remove();
            fail("never: should raise exception");
        } catch (java.rmi.RemoteException e) {
        } finally {
            utx.rollback();

        }
        
    }

    /**
     * Test RequiresNew transactional attributes on create method
     * the create method xith required is called outside TX 
     * 
     */
    public void testCreateRequiresNew() throws Exception {
        String  s  = "160";
        Simple entity = getHome().createForRequiresNew(s);
        entity.remove();
    }

    /**
     * Test RequiresNew transactional attributes on create method
     * the create method xith required is called inside TX 
     * 
     */
    public void testCreateRequiresNewTx() throws Exception {
        utx.begin();
        String  s  = "170";
        Simple entity = null;
        try {
            entity = getHome().createForRequiresNew(s);
        } finally {
            utx.rollback();
            entity.remove();
        }
    }


    /**
     * Test Mandatory transactional attributes on create method
     * the create method xith required is called outside TX 
     * 
     */
    public void testCreateMandatory() throws Exception {
        char  c  = 'a';
        try {
            Simple entity = getHome().createForMandatory(c);
            entity.remove();
            fail("mandatory: should raise exception");
        } catch (javax.transaction.TransactionRequiredException e) {
        } catch (java.rmi.RemoteException e) {
            assertTrue(e.detail instanceof javax.transaction.TransactionRequiredException);
        }
    }

    /**
     * Test Mandatory transactional attributes on create method
     * the create method xith required is called inside TX 
     * 
     */
    public void testCreateMandatoryTx() throws Exception {
        utx.begin();
        char  c  = 'b';
        Simple entity = null;
        try {
            entity = getHome().createForMandatory(c);
        } finally {
            utx.rollback();
            
        }
    }


    /**
     * Test Supports transactional attributes on create method
     * the create method xith required is called outside TX 
     * 
     */
    public void testCreateSupports() throws Exception {

        Simple entity = getHome().createForSupports(false);
        entity.remove();
    }

    /**
     * Test Supports transactional attributes on create method
     * the create method xith required is called inside TX 
     * 
     */
    public void testCreateSupportsTx() throws Exception {
        utx.begin();
        Simple entity = null;
        try {
            entity = getHome().createForSupports(true);           
        } finally {
            utx.rollback();
 
        }
    }

    // -----------------------------------------------------------------
    // tests on Home methods
    // -----------------------------------------------------------------

    /**
     * Test of NotSupported attribute
     * A home method with NotSupported is called outside TX
     * this method returns if the thread is associated to a transaction
     * 
     * the expected value is  false
     */
    public void testHomeNotSupported() throws Exception {
        assertEquals(false, getHome().opwith_notsupported());
    }

    /**
     * Test of RequiresNew attribute
     * A home method with RequiresNew is called outside TX
     * this method returns if the thread is associated to a transaction
     *
     * the expected value is  true
     */
    public void testHomeRequiresNew() throws Exception {
        assertEquals(true, getHome().opwith_requires_new());
    }

    /**
     * Test of Required attribute
     * A home method with Required is called outside TX
     * this method returns if the thread is associated to a transaction
     *
     * the expected value is  true
     */
    public void testHomeRequired() throws Exception {
        assertEquals(true, getHome().opwith_required());
    }

    /**
     * Test of Mandatory attribute
     * A home method with Mandatory is called outside TX
     * this method returns if the thread is associated to a transaction
     *
     * A javax.transaction.TransactionRequiredException must be received
     */

    public void testHomeMandatory() throws Exception {
        try {
            getHome().opwith_mandatory();
            fail("mandatory: should raise exception");
        } catch (javax.transaction.TransactionRequiredException e) {
        } catch (RemoteException e) {
            assertTrue(e.detail instanceof TransactionRequiredException);
        }
    }

    /**
     * Test of Never  attribute
     * A home method with Never is called outside TX
     * this method returns if the thread is associated to a transaction
     * 
     * the expected value is  false
     */
    public void testHomeNever() throws Exception {
        assertEquals(false, getHome().opwith_never());
    }


    /**
     * Test of Supports  attribute
     * A home method with Supports is called outside TX
     * this method returns if the thread is associated to a transaction
     * 
     * the expected value is  false
     */
    public void testHomeSupports() throws Exception {
        assertEquals(false, getHome().opwith_supports());
    }

    /**
     * Test of NotSupported attribute
     * A home method with NotSupported is called inside  TX
     * this method returns if the thread is associated to a transaction
     * 
     * the expected value is  false
     */
    public void testHomeNotSupportedTx() throws Exception {
        utx.begin();
        try {
            assertEquals(false, getHome().opwith_notsupported());
        } finally {
            utx.rollback();
        }
    }

    /**
     * Test of RequiresNew attribute
     * A home method with RequiresNew is called inside TX
     * this method returns if the thread is associated to a transaction
     *
     * the expected value is  true
     */
    public void testHomeRequiresNewTx() throws Exception {
        utx.begin();
        try {
            assertEquals(true, getHome().opwith_requires_new());
        } finally {
            utx.rollback();
        }
    }

    /**
     * Test of Required attribute
     * A home method with Required is called inside  TX
     * this method returns if the thread is associated to a transaction
     *
     * the expected value is  true
     */
    public void testHomeRequiredTx() throws Exception {
        utx.begin();
        try {
            assertEquals(true, getHome().opwith_required());
        } finally {
            utx.rollback(); 
        }

    }

    /**
     * Test of Mandatory  attribute
     * A home method with Mandatory is called inside  TX
     * this method returns if the thread is associated to a transaction
     *
     * the expected value is  true
     */
    public void testHomeMandatoryTx() throws Exception {
        utx.begin();
        try {
            assertEquals(true, getHome().opwith_mandatory());
        } finally {
            utx.rollback();
        }
    }




    /**
     * Test of Never attribute
     * A home method with Mandatory is called inside TX
     * this method returns if the thread is associated to a transaction
     *
     * A java.rmi.RemoteException must be received
     */
    public void testHomeNeverTx() throws Exception {
        utx.begin();
        try {
            getHome().opwith_never();
            fail("never: should raise exception");
        } catch (RemoteException e) {
        } finally {
            utx.rollback();
        }
    }

    /**
     * Test of Supports  attribute
     * A home method with Supports is called inside TX
     * this method returns if the thread is associated to a transaction
     * 
     * the expected value is  true
     */
    public void testHomeSupportsTx() throws Exception {
        utx.begin();
        try {
            assertEquals(true, getHome().opwith_supports());
        } finally {
            utx.rollback();
        }
	
    }

    /**
     * Test the sequence of several calls to home methods
     * with different transactional contexts
     */
    public void testHomeNoTx() throws Exception {
        assertEquals(false, getHome().opwith_notsupported());
        assertEquals(true, getHome().opwith_requires_new());
        assertEquals(true, getHome().opwith_required());
        assertEquals(false, getHome().opwith_never());
        assertEquals(false, getHome().opwith_supports());
    }

 
}
