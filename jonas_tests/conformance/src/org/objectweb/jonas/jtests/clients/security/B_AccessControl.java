/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id:
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.clients.security;


import org.ow2.jonas.lib.security.context.SecurityContext;
import org.ow2.jonas.lib.security.context.SecurityCurrent;

import org.objectweb.jonas.jtests.beans.secured.BaseS;
import org.objectweb.jonas.jtests.util.JTestCase;


/**
 * Security Management common tests for all type of beans (Entity/Session)
 *
 * @author Ph.Coq, Ph.Durieux
 *
 */

public abstract class B_AccessControl  extends JTestCase {

    protected static String PRINCIPAL1_NAME = "principal1";
    protected static String PRINCIPAL3_NAME = "principal3";
    protected static String ROLE1_NAME = "baserole1";
    protected static String ROLE2_NAME = "baserole2";

    protected static SecurityCurrent current = null;
    protected static SecurityContext principal1 = null;
    protected static SecurityContext principal2 = null;
    protected static SecurityContext principal3 = null;
    protected static SecurityContext principal4 = null;


    public B_AccessControl(String name) {
        super(name);
    }

    /**
     * init environment:
     * - load beans
     */
    protected void setUp() {
        super.setUp();
        if (current == null) {
            current = SecurityCurrent.getCurrent();
            principal1 = new SecurityContext("principal1");
            principal2 = new SecurityContext("principal2");
            String[] roles3 = new String[]{"role1", "role3"};
            principal3 = new SecurityContext(PRINCIPAL3_NAME, roles3);
            String[] roles4 = new String[]{"role2"};
            principal4 = new SecurityContext("principal4", roles4);
        }
    }

    public abstract BaseS getBaseS(String name) throws Exception;

    /**
     * test getCallerPrincipal. 
     * The Principal must be propagated.
     */
    public void testGetCallerPrincipal() throws Exception {
        current.setSecurityContext(principal1);
        BaseS sl = getBaseS("un");
        assertEquals(PRINCIPAL1_NAME, sl.getPrincipalName());
        sl.remove();
    }

    /**
     * test isCallerInRole.
     * principal1 = role1
     * principal2 = role2
     */
    public void testIsCallerInRole() throws Exception {
        current.setSecurityContext(principal1);
        BaseS sl = getBaseS("deux");
        assertTrue(sl.isCallerInRole(ROLE1_NAME) == true);
        assertTrue(sl.isCallerInRole(ROLE2_NAME) == false);
        sl.remove();
    }


 

    /**
     * test basic method accept
     */
    public void testBasicMethodAccept() throws Exception {
        current.setSecurityContext(principal2);
        BaseS sl = getBaseS("quatre");
        sl.simpleMethod();
        sl.remove();
    }

  
   

    /**
     * test principal propagation from bean to bean
     */
    public void testBeanToBeanPropagation() throws Exception {
        current.setSecurityContext(principal1);
        BaseS sl = getBaseS("sept");
        assertEquals(PRINCIPAL1_NAME, sl.getPrincipalNameOfAnotherBean());
        sl.remove();
    }

 
  
    
}
