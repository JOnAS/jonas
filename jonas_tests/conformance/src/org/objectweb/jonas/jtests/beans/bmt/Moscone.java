// Moscone.java

package org.objectweb.jonas.jtests.beans.bmt;

import java.rmi.RemoteException;
import javax.ejb.EJBObject;

/**
 * Moscone remote interface
 */
public interface Moscone extends EJBObject {
    public void tx_start() throws RemoteException;
    public void tx_commit() throws RemoteException;
    public void tx_rollback() throws RemoteException;
    public void moscone1() throws RemoteException;
    public void moscone2() throws RemoteException;
}
