/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.beans.relation.cascade;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Vector;
import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.EntityBean;
import javax.ejb.EntityContext;
import javax.ejb.RemoveException;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.ow2.jonas.lib.util.Log;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * Implementation for bean Car
 * @author Ph Durieux
 */
public abstract class CarBean implements EntityBean {

    static protected Logger logger = null;
    protected EntityContext ejbContext = null;
    protected InsuranceHL insuranceHL = null;
    protected InvoiceHL invoiceHL = null;

    public Object ejbCreate(String number, byte type, String name) throws CreateException {
        logger.log(BasicLevel.DEBUG, "");
        setNumber(number);
        setType(type);
        setName(name);
        return null;
    }

    public void ejbPostCreate(String number, byte type, String name) throws CreateException {
        logger.log(BasicLevel.DEBUG, "");
        InsuranceL ins = insuranceHL.create("000" + number);
        setInsurance(ins);
    }

    // persistent fields
    public abstract Integer getId();
    public abstract void setId(Integer id);
    public abstract String getNumber();
    public abstract void setNumber(String number);
    public abstract byte getType();
    public abstract void setType(byte type);
    public abstract String getName();
    public abstract void setName(String name);

    // persistent relationships
    public abstract InsuranceL getInsurance();
    public abstract void setInsurance(InsuranceL i);
    public abstract CustomerL getCustomer();
    public abstract void setCustomer(CustomerL c);
    public abstract java.util.Collection getInvoices();
    public abstract void setInvoices(java.util.Collection invoices);


    public void addInvoice(String number) throws NamingException, CreateException {
        logger.log(BasicLevel.DEBUG, "");
        InvoiceL invoice = invoiceHL.create(number);
        Collection invoices = getInvoices();
        invoices.add(invoice);
    }

    public void setEntityContext(EntityContext ec) {
        if (logger == null)
            logger = Log.getLogger(Log.JONAS_TESTS_PREFIX);
        logger.log(BasicLevel.DEBUG, "");
        ejbContext = ec;
        try {
            InitialContext cntx = new InitialContext();
            insuranceHL = (InsuranceHL) cntx.lookup("java:comp/env/ejb/InsuranceHomeLocal");
            invoiceHL = (InvoiceHL) cntx.lookup("java:comp/env/ejb/InvoiceHomeLocal");
        } catch (Exception e) {
            throw new javax.ejb.EJBException(e);
        }
    }

    public void unsetEntityContext() { 
        logger.log(BasicLevel.DEBUG, "");
        ejbContext = null;
    }

    public void ejbLoad() {
        logger.log(BasicLevel.DEBUG, "");
        checkCustomerAccess(); 
    }

    public void ejbStore() { 
        logger.log(BasicLevel.DEBUG, "");
        checkCustomerAccess();
    }

    public void ejbActivate() { 
        logger.log(BasicLevel.DEBUG, "");
        // This cannot be done: See EJB specs
        // A bean must not attempt to access its state here.
        //checkCustomerAccess();
    }

    public void ejbPassivate() { 
        logger.log(BasicLevel.DEBUG, "");
        // This cannot be done: See EJB specs
        // A bean must not attempt to access its state here.
        //checkCustomerAccess();
    }

    /**
     * this instance is being removed.
     * we must be able to access bean fields here, including the CMRs
     */
    public void ejbRemove() throws javax.ejb.RemoveException {
        logger.log(BasicLevel.DEBUG, "");
        InsuranceL myins = getInsurance();
        if (myins == null) {
            logger.log(BasicLevel.ERROR, "CMR field Insurance is null");
            throw new RemoveException("Cannot access CMR field Insurance inside ejbRemove");
        }
        String insnumber = myins.getNumber();
        String expect = "000" + getNumber();
        if (! expect.equals(insnumber)) {
            throw new RemoveException("Bad insurance number:" + insnumber);
        }
        checkCustomerAccess();
     }

    protected void checkCustomerAccess() {
        logger.log(BasicLevel.DEBUG, "");
        CustomerL mycust = getCustomer();
        if (mycust == null) {
            logger.log(BasicLevel.ERROR, "CMR field Customer is null");
            throw new EJBException("CMR field Customer is null");
        }
        Name n = mycust.getName();
        if (n == null || n.getLastName().length() == 0) {
            logger.log(BasicLevel.ERROR, "Cannot get customer name");
            throw new EJBException("Cannot get customer name");
        }
    }
}
