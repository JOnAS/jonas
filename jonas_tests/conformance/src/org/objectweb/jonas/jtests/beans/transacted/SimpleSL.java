/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.beans.transacted;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.ejb.TimedObject;
import javax.ejb.Timer;
import javax.ejb.TimerHandle;
import javax.ejb.TimerService;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.objectweb.util.monolog.api.BasicLevel;

public class SimpleSL extends SimpleCommon implements SessionBean, TimedObject {

    protected SessionContext sessionContext;
    static protected int timercount = 0;

    public void setSessionContext(SessionContext sessionContext) {
    	setEjbContext(sessionContext);
        initLogger();
        logger.log(BasicLevel.DEBUG, "");
        this.sessionContext = sessionContext;
    }

    public void ejbActivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbPassivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbRemove() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void  ejbCreate() {
        logger.log(BasicLevel.DEBUG, "");
        try {
            InitialContext ictx = new InitialContext();
        } catch(NamingException e) {
            throw new EJBException(e);
        }
    }

    public int setTimer(int dur, int period) {
        logger.log(BasicLevel.DEBUG, "");
        TimerService timerservice = sessionContext.getTimerService();
        int ret = dur * 10 + period;
        if (period > 0) {
            timerservice.createTimer(dur * 1000, period * 1000, new Integer(ret));
        } else {
            timerservice.createTimer(dur * 1000, new Integer(ret));
        }
        return ret;
    }

    public int setTimer(java.util.Date date, int period) {
        logger.log(BasicLevel.DEBUG, "");
        TimerService timerservice = sessionContext.getTimerService();
        int ret = 200 + period;
        if (period > 0) {
            timerservice.createTimer(date, period * 1000, new Integer(ret));
        } else {
            timerservice.createTimer(date, new Integer(ret));
        }
        return ret;
    }

    public int setTimerGetHandle(int dur, int period) {
        logger.log(BasicLevel.DEBUG, "");
        TimerService timerservice = sessionContext.getTimerService();
        int ret = dur * 10 + period;
        Timer t = null;
        if (period > 0) {
            t = timerservice.createTimer(dur * 1000, period * 1000, new Integer(ret));
        } else {
            t = timerservice.createTimer(dur * 1000, new Integer(ret));
        }
        TimerHandle hdl = t.getHandle();
        TimerHandle hdl2 = getDeserializedHandle(hdl);
        if (! timersAreIdentical(hdl, hdl2)) {
            logger.log(BasicLevel.ERROR, "Bad timer handle");
            throw new EJBException("Bad timer handle");
        }
        return ret;
    }

    public TimerHandle getTimerHandle(int ident) {
        logger.log(BasicLevel.DEBUG, "");
        TimerHandle hdl = null;
        TimerService timerservice = sessionContext.getTimerService();
        Collection timerList = timerservice.getTimers();
        for (Iterator i = timerList.iterator(); i.hasNext(); ) {
            Timer t = (Timer) i.next();
            Integer id = (Integer) t.getInfo();
            if (id.intValue() == ident) {
                hdl = t.getHandle();
                TimerHandle hdl2 = getDeserializedHandle(hdl);
                if (! timersAreIdentical(hdl, hdl2)) {
                    logger.log(BasicLevel.ERROR, "Bad timer handle");
                    throw new EJBException("Bad timer handle");
                }
                break;
            }
        }
        return hdl;
    }

    public void cancelTimer(int ident) {
        logger.log(BasicLevel.DEBUG, "");
        TimerService timerservice = sessionContext.getTimerService();
        Collection timerList = timerservice.getTimers();
        for (Iterator i = timerList.iterator(); i.hasNext(); ) {
            Timer t = (Timer) i.next();
            Integer id = (Integer) t.getInfo();
            if (id.intValue() == ident) {
                t.cancel();
            }
        }
    }

    public void cancelTimers() {
        logger.log(BasicLevel.DEBUG, "");
        TimerService timerservice = sessionContext.getTimerService();
        Collection timerList = timerservice.getTimers();
        for (Iterator i = timerList.iterator(); i.hasNext(); ) {
            Timer t = (Timer) i.next();
            t.cancel();
        }
    }

    public long getTimeRemaining(int ident) {
        logger.log(BasicLevel.DEBUG, "");
        TimerService timerservice = sessionContext.getTimerService();
        Collection timerList = timerservice.getTimers();
        long ret = -1;
        for (Iterator i = timerList.iterator(); i.hasNext(); ) {
            Timer t = (Timer) i.next();
            Integer id = (Integer) t.getInfo();
            if (id.intValue() == ident) {
                ret = t.getTimeRemaining();
            }
        }
        return ret;
    }

    public int getTimerNumber() {
        logger.log(BasicLevel.DEBUG, "");
        TimerService timerservice = sessionContext.getTimerService();
        Collection timerList = timerservice.getTimers();
        return timerList.size();
    }

    public int getTimerCount() {
        logger.log(BasicLevel.DEBUG, "");
        return timercount;
    }

    /**
     * This support method calls a required method
     */
    public boolean supports_call_required() throws RemoteException {
        logger.log(BasicLevel.DEBUG, "");
        Simple mysession = (Simple) sessionContext.getEJBObject();
        return mysession.opwith_required();
    }

    public void startInfoTimer(int dur, String info) throws RemoteException {
        logger.log(BasicLevel.ERROR, "not implemented");
    }

    // -----------------------------------------------------------
    // TimedObject implementation
    // -----------------------------------------------------------

    /**
     * A timer is expired.
     */
    public void ejbTimeout(Timer timer) {
        logger.log(BasicLevel.DEBUG, "");
        timercount++;
        TimerService timerservice = sessionContext.getTimerService();
        Collection timerList = timerservice.getTimers();
        TimerHandle hdl = timer.getHandle();
        TimerHandle hdl2 = getDeserializedHandle(hdl);
        if (! timersAreIdentical(hdl, hdl2)) {
            logger.log(BasicLevel.ERROR, "Bad timer handle");
            throw new EJBException("Bad timer handle");
        }
    }
}
