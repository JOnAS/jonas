/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.beans.beanexc;

import javax.ejb.EntityBean;
import javax.ejb.EntityContext;
import javax.ejb.EJBContext;
import javax.ejb.RemoveException;
import javax.ejb.CreateException;

import org.ow2.jonas.lib.util.Log;
import org.objectweb.util.monolog.api.BasicLevel;

/**
 * This is an entity bean with "container managed persistence version 2".
 * The state of an instance is stored into a relational database.
 * @author Philippe Durieux, Philippe Coq, Helene Joanin
 */
public abstract class AccountEC2 extends AccountCommon implements EntityBean {   
 
    boolean forceToFailEjbStore;
    protected EntityContext entityContext;


    // Get and Set accessor methods of the bean's abstract schema
    public abstract int getNumber();
    public abstract void setNumber(int n);
    public abstract long getBalance();
    public abstract void setBalance(long d);
    public abstract String  getCustomer();
    public abstract void setCustomer(String c);



    public EJBContext getContext() {
        return entityContext;
    }

    public void ejbActivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbPassivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbLoad() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbStore() {
        logger.log(BasicLevel.DEBUG, "");
        if (forceToFailEjbStore) {
            forceToFailEjbStore = false;
            throw new RuntimeException("RunTimeExceptionInEjbStore");
        }
    }
  
    /**
     * This method is common for impl and expl bean
     * it is used to test exception raised in ejbRemove()
     * with unspecified transactional context (Required attribute).
     * This method throws a RemoveException when the value of the PK is between 999990 and 999999
     */
    public void ejbRemove() throws RemoveException {
        logger.log(BasicLevel.DEBUG, "");
        AccountPK pk = (AccountPK) entityContext.getPrimaryKey();
        if ((pk.number >= 999990) && (pk.number <= 999999)) {
            logger.log(BasicLevel.DEBUG,
                       "RemoveException throwned by bean provider in ejbRemove");
            throw new RemoveException("RemoveException throwned by bean provider in ejbRemove");
        }
    }

    public void setEntityContext(EntityContext ctx) { 
        if (logger == null) {
            logger = Log.getLogger(Log.JONAS_TESTS_PREFIX);
        }
        logger.log(BasicLevel.DEBUG, "");
        entityContext  = ctx;
    }

    public void unsetEntityContext() {
        logger.log(BasicLevel.DEBUG, "");
        entityContext  = null;
    }

    public AccountPK ejbCreate(int val_number, String val_customer, long val_balance) throws CreateException {
        logger.log(BasicLevel.DEBUG, "");
        setNumber(val_number);
        setCustomer(val_customer);
        setBalance(val_balance);
        return (null);
    }

    /**
     * this method is common for impl and expl bean
     * it is used to test exception raised in ejbCreate
     * with unspecified transactional context (Required attribute)
     * CAUTION: Do not call ejbCreate inside another ejbCreate (known bug)
     */
    public AccountPK ejbCreate(int flag) throws CreateException, AppException {
        logger.log(BasicLevel.DEBUG, "");
        setNumber(1951);
        setCustomer("Myself");
        setBalance(10000);
        if (flag == 0) {
            entityContext.setRollbackOnly();
            throw new AppException("AppException in ejbCreate(boolean)");
        } else {
            int zero = 0;
            float f = 10 / zero;
        }
        return (null);
    }

    /**
     * this method is common for impl and expl bean
     * NotSupported attr.
     * CAUTION: Do not call ejbCreate inside another ejbCreate (known bug)
     */
    public AccountPK ejbCreate(boolean flag) throws CreateException, AppException {
        logger.log(BasicLevel.DEBUG, "");
        setNumber(1951);
        setCustomer("Myself");
        setBalance(10000);
        if (flag) {
            throw new AppException("AppException in ejbCreate(boolean)");
        } else {
            int zero = 0;
            float f = 10 / zero;
        }
        return (null);
    }

    public void ejbPostCreate(int val_number, String val_customer, long val_balance) {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbPostCreate(int flag) {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbPostCreate(boolean flag) {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void doFailedEjbStore_1() {
        logger.log(BasicLevel.DEBUG, "");
        setBalance(1000);
        forceToFailEjbStore = true;
    }
    public void doFailedEjbStore_2() {
        logger.log(BasicLevel.DEBUG, "");
        setBalance(2000);
        forceToFailEjbStore = true;
    }
    public void doFailedEjbStore_3() {
        logger.log(BasicLevel.DEBUG, "");
        setBalance(3000);
        forceToFailEjbStore = true;
    }

} 

