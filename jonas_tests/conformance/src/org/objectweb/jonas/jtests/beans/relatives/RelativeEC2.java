/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

// PersonneEC2.java

package org.objectweb.jonas.jtests.beans.relatives;

import java.util.Date;
import java.util.Set;

import javax.ejb.CreateException;
import javax.ejb.DuplicateKeyException;
import javax.ejb.EntityBean;
import javax.ejb.EntityContext;
import javax.ejb.RemoveException;

import org.ow2.jonas.lib.util.Log;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * This is an entity bean with "container managed persistence version 2.x".
 * @author  Christophe Ney - cney@batisseurs.com
 */
public abstract class RelativeEC2 implements EntityBean {

    static protected Logger logger = null;
    EntityContext ejbContext;

    // ------------------------------------------------------------------
    // Get and Set accessor methods of the bean's abstract schema
    // ------------------------------------------------------------------
    public abstract String getFullName();

    public abstract void setFullName(String name);

    public abstract int getLuckyNumber();

    public abstract void setLuckyNumber(int age);

    public abstract Date getBirthdate();

    public abstract void setBirthdate(Date birthdate);

    public abstract RelativeLocal getSpouse();

    public abstract void setSpouse(RelativeLocal spouse);

    public abstract Set getSibblings();

    public abstract void setSibblings(Set sibblings);

    public abstract Set getVisitedRelatives();

    public abstract void setVisitedRelatives(Set sibblings);

    public abstract boolean getIsMale();

    public abstract void setIsMale(boolean isMale);

    public abstract double getAverageAnnualVisits();

    public abstract void setAverageAnnualVisits(double averageAnnualVisits);

    // ------------------------------------------------------------------
    // EntityBean implementation
    // ------------------------------------------------------------------

    /**
     * Set the associated entity context.
     */
    public void setEntityContext(EntityContext ctx) {
        if (logger == null)
            logger = Log.getLogger(Log.JONAS_TESTS_PREFIX);
        logger.log(BasicLevel.DEBUG, "");
        ejbContext = ctx;
    }

    /**
     * Unset the associated entity context.
     */
    public void unsetEntityContext() {
        logger.log(BasicLevel.DEBUG, "");
        ejbContext = null;
    }

    /**
     * A container invokes this method before it removes the EJB object
     * that is currently associated with the instance.
     */
    public void ejbRemove() throws RemoveException {
        logger.log(BasicLevel.DEBUG, "");
    }

    /**
     * A container invokes this method to instruct the instance to synchronize
     * its state by loading it state from the underlying database.
     */
    public void ejbLoad() {
        logger.log(BasicLevel.DEBUG, "");
    }

    /**
     * A container invokes this method to instruct the instance to synchronize
     * its state by storing it to the underlying database.
     */
    public void ejbStore() {
        logger.log(BasicLevel.DEBUG, "");
    }

    /**
     * A container invokes this instance before creating the EJBObject
     */
    public String ejbCreate(String fullName, Date birthdate, int luckyNumber) throws CreateException, DuplicateKeyException {
        logger.log(BasicLevel.DEBUG, "");

        setFullName(fullName);
        setBirthdate(birthdate);
        setLuckyNumber(luckyNumber);

        // In CMP, should return null.
        return null;
    }

    /**
     * There must be an ejbPostCreate par ejbCreate method
     *
     * @throws CreateException Failure to create an entity EJB object.
     */
    public void ejbPostCreate(String fullName, Date birthdate, int luckyNumber) throws CreateException {
        logger.log(BasicLevel.DEBUG, "");
    }


    /**
     * This method is called before the instance enters the "passive" state.
     * The instance should release any resources that it can re-acquire later in the
     * ejbActivate() method.
     * After the passivate method completes, the instance must be in a state that
     * allows the container to use the Java Serialization protocol to externalize
     * and store away the instance's state.
     * This method is called with no transaction context.
     *
     * @exception EJBException - Thrown if the instance could not perform the
     * function requested by the container
     */
    public void ejbPassivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    /**
     * This method is called when the instance is activated from its "passive" state.
     * The instance should acquire any resource that it has released earlier in the
     * ejbPassivate() method.
     * This method is called with no transaction context.
     *
     * @exception EJBException - Thrown if the instance could not perform the
     * function requested by the container
     */
    public void ejbActivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

}
