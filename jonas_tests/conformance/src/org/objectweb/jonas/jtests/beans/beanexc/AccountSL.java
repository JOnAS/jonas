/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.beans.beanexc;

import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.ejb.EJBContext;
import org.ow2.jonas.lib.util.Log;
import org.objectweb.util.monolog.api.BasicLevel;

/**
 * Stateless Session Bean
 * @author Philippe Coq
 */
public class AccountSL extends AccountCommon implements SessionBean {
    protected SessionContext sessionContext = null;

    public void ejbActivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbPassivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbRemove() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void setSessionContext(SessionContext t) {
        if (logger == null) {
            logger = Log.getLogger(Log.JONAS_TESTS_PREFIX);
        }
        logger.log(BasicLevel.DEBUG, "");
        sessionContext = t;
    }

    public void  ejbCreate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void setBalance(long val) {
        logger.log(BasicLevel.DEBUG, "");
    }

    public EJBContext getContext() {
        logger.log(BasicLevel.DEBUG, "");
        return sessionContext;
    }

    public void doFailedEjbStore_1() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void doFailedEjbStore_2() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void doFailedEjbStore_3() {
        logger.log(BasicLevel.DEBUG, "");
    }
}
