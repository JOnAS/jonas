/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

// PersonneEC2.java

package org.objectweb.jonas.jtests.beans.annuaire;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.DuplicateKeyException;
import javax.ejb.EntityBean;
import javax.ejb.EntityContext;
import javax.ejb.RemoveException;
import javax.ejb.TimedObject;
import javax.ejb.Timer;
import javax.ejb.TimerService;

import org.ow2.jonas.lib.util.Log;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * This is an entity bean with "container managed persistence version 2.x".
 * @author Philippe Durieux, Helene Joanin (jonas team)
 */
public abstract class PersonneEC2 implements EntityBean, TimedObject {

    static protected Logger logger = null;
    EntityContext ejbContext;

    // ------------------------------------------------------------------
    // Get and Set accessor methods of the bean's abstract schema
    // ------------------------------------------------------------------
    public abstract String getNom();
    public abstract void setNom(String n);

    public abstract String getNumero();        // Tx Attribute = Required
    public abstract void setNumero(String n);  // Tx Attribute = Required

    public abstract int getTimerIdent();
    public abstract void setTimerIdent(int id);

    public abstract int getTimerCount();
    public abstract void setTimerCount(int cnt);


    // ------------------------------------------------------------------
    // EntityBean implementation
    // ------------------------------------------------------------------

    /**
     * Set the associated entity context. The container invokes this method 
     * on an instance after the instance has been created.
     * This method is called in an unspecified transaction context. 
     * 
     * @param ctx - An EntityContext interface for the instance. The instance
     * should store the reference to the context in an instance variable.
     * @throws EJBException Thrown by the method to indicate a failure caused by a
     * system-level error.
     */
    public void setEntityContext(EntityContext ctx) {
        if (logger == null)
            logger = Log.getLogger(Log.JONAS_TESTS_PREFIX);
        logger.log(BasicLevel.DEBUG, "");
        ejbContext = ctx;
    }

    /**
     * Unset the associated entity context. The container calls this method
     * before removing the instance. 
     * This is the last method that the container invokes on the instance.
     * The Java garbage collector will eventually invoke the finalize() method
     * on the instance.
     * This method is called in an unspecified transaction context.
     *
     * @throws EJBException Thrown by the method to indicate a failure caused by a
     * system-level error.
     */
    public void unsetEntityContext() {
        logger.log(BasicLevel.DEBUG, "");
        ejbContext = null;
    }
    
    /**
     * A container invokes this method before it removes the EJB object
     * that is currently associated with the instance. This method is
     * invoked when a client invokes a remove operation on the enterprise Bean's
     * home interface or the EJB object's remote interface. This method
     * transitions the instance from the ready state to the pool of available
     * instances.
     *
     * This method is called in the transaction context of the remove operation.
     * @throws RemoveException  The enterprise Bean does not allow destruction of the object.
     * @throws EJBException - Thrown by the method to indicate a failure caused by a system-level
     * error.
     */
    public void ejbRemove() throws RemoveException {
        logger.log(BasicLevel.DEBUG, "");
    }

    /**
     * A container invokes this method to instruct the instance to synchronize
     * its state by loading it state from the underlying database. 
     * This method always executes in the proper transaction context.
     *
     * @throws EJBException Thrown by the method to indicate a failure caused by
     * a system-level error.
     */
    public void ejbLoad() {
        logger.log(BasicLevel.DEBUG, "");
    }

    /**
     * A container invokes this method to instruct the instance to synchronize
     * its state by storing it to the underlying database. 
     * This method always executes in the proper transaction context.
     *
     * @throws EJBException Thrown by the method to indicate a failure caused by
     * a system-level error.
     */
    public void ejbStore() {
        logger.log(BasicLevel.DEBUG, "");
    }
	
    /**
     * There must be an ejbPostCreate par ejbCreate method
     *
     * @throws CreateException Failure to create an entity EJB object.
     */
    public void ejbPostCreate(String nom, String numero) throws CreateException {
        logger.log(BasicLevel.DEBUG, "");
    }	
    public void ejbPostCreate(String nom, String numero, boolean t) throws CreateException {
        logger.log(BasicLevel.DEBUG, "");
    }
	

    public java.lang.String ejbCreate(String nom, String numero) throws CreateException, DuplicateKeyException {
        logger.log(BasicLevel.DEBUG, "ejbCreate(" + nom + ", " + numero + ")");

        // Init here the bean fields
        setNom(nom);
        setNumero(numero);
        setTimerIdent(0);
        setTimerCount(0);

        // In CMP, should return null.
        return null;
    }

    public java.lang.String ejbCreate(String nom, String numero, boolean t) throws CreateException, DuplicateKeyException {
        logger.log(BasicLevel.DEBUG, "ejbCreate nom numero boolean");

        // Init here the bean fields
        setNom(nom);
        setNumero(numero);
        setTimerIdent(0);
        setTimerCount(0);

        // In CMP, should return null.
        return null;
    }

    /**
     * This method is called before the instance enters the "passive" state. 
     * The instance should release any resources that it can re-acquire later in the 
     * ejbActivate() method.
     * After the passivate method completes, the instance must be in a state that 
     * allows the container to use the Java Serialization protocol to externalize 
     * and store away the instance's state. 
     * This method is called with no transaction context. 
     *
     * @exception EJBException - Thrown if the instance could not perform the 
     * function requested by the container 
     */
    public void ejbPassivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    /**
     * This method is called when the instance is activated from its "passive" state.
     * The instance should acquire any resource that it has released earlier in the 
     * ejbPassivate() method. 
     * This method is called with no transaction context.
     * 
     * @exception EJBException - Thrown if the instance could not perform the 
     * function requested by the container 
     */
    public void ejbActivate() {
        logger.log(BasicLevel.DEBUG, "");
    }
    
    // ------------------------------------------------------------------
    // Personne implementation
    // ------------------------------------------------------------------

    /**
     * getNumeroNTX / Tx Attribute = Supports
     */
    public String getNumeroNTX() {
        logger.log(BasicLevel.DEBUG, "");
        return getNumero();
    }

    /**
     * setNumeroNTX / Tx Attribute = Supports
     */
    public void setNumeroNTX(String s) {
        logger.log(BasicLevel.DEBUG, "");
        setNumero(s);
    }

    //
    // Methods only implemented in the entity bean with "CMP version 1.x"
    // used to test the isModified extension for CMP version 1 Entity Bean.
    // (defined here to have a common interface)
    //

    public boolean isModified() {
        throw new UnsupportedOperationException();
    }

    public void reset() {
        throw new UnsupportedOperationException();
    }

    public boolean isModifiedCalled() {
        throw new UnsupportedOperationException();
    }

    public boolean ejbStoreCalled() {
        throw new UnsupportedOperationException();
    }

    public boolean isDirty() {
        throw new UnsupportedOperationException();
    }

    public int setTimer(int dur, int period) {
        TimerService timerservice = ejbContext.getTimerService();
        Timer mt = null;
        int ret = getTimerIdent() + 1;
        setTimerIdent(ret);
        if (period > 0) {
            mt = timerservice.createTimer(1000 * dur, 1000 * period, new Integer(ret));
        } else {
            mt = timerservice.createTimer(1000 * dur, new Integer(ret));
        }
        return ret;
    }

    public void cancelTimer(int ident) throws RemoteException {
        TimerService timerservice = ejbContext.getTimerService();
        Collection timerList = timerservice.getTimers();
        for (Iterator i = timerList.iterator(); i.hasNext(); ) {
            Timer t = (Timer) i.next();
            Integer id = (Integer) t.getInfo();
            if (id.intValue() == ident) {
                t.cancel();
            }
        }
    }

    public long getTimeRemaining(int ident) throws RemoteException {
        TimerService timerservice = ejbContext.getTimerService();
        Collection timerList = timerservice.getTimers();
        long ret = -1;
        for (Iterator i = timerList.iterator(); i.hasNext(); ) {
            Timer t = (Timer) i.next();
            Integer id = (Integer) t.getInfo();
            if (id.intValue() == ident) {
                ret = t.getTimeRemaining();
            }
        }
        return ret;
    }


    // -----------------------------------------------------------
    // TimedObject implementation
    // -----------------------------------------------------------

    /**
     * A timer is expired.
     */
    public void ejbTimeout(Timer timer) {
        setTimerCount(getTimerCount() + 1);
    }
}
