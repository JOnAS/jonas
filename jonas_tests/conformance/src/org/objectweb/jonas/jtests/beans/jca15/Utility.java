/*
 * Created on January 20, 2004
 *
 * Utility.java is part of a JUnit test suit to test 
 * J2EE Connector 1.5 as implemented by JOnAS. 
 */

package org.objectweb.jonas.jtests.beans.jca15;

import java.util.*;
import java.text.SimpleDateFormat;

import javax.resource.ResourceException;
import javax.resource.spi.*;
import javax.resource.spi.SecurityException;
import javax.resource.spi.security.*;
import javax.security.auth.Subject;
import javax.transaction.xa.*;
import org.ow2.jonas.lib.util.Log;
import org.objectweb.util.monolog.api.Logger;
import org.objectweb.util.monolog.api.BasicLevel;


/** <p>This class contains the support methods for the 
 *  JOnAS JCA1.5 test suite
 *
**/
public class Utility
{

  private static Logger logger = null;
  public static final String USER_TRANSACTION = "javax.transaction.UserTransaction";
  
  /**
   * Write a JOnAS log record
   *
   * @param msg  display this message in log
   *
   */
  public static synchronized void log(String msg) {
    if (logger == null) {
        logger = Log.getLogger("ersatz.resourceadapter");
    }
    logger.log(BasicLevel.DEBUG, msg);
  }  
 

}