/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.beans.applimet;

import java.rmi.RemoteException;
import java.sql.SQLException;
import javax.ejb.CreateException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import org.ow2.jonas.lib.util.Log;
import org.objectweb.util.monolog.api.Logger;
import org.objectweb.util.monolog.api.BasicLevel;

/**
 */
public class MetSession implements SessionBean {

    SessionContext ejbContext;
    static protected Logger logger = null;
    private Dao dao;
    private Dao dao2;

    public void ejbRemove() throws RemoteException {
        logger.log(BasicLevel.DEBUG, "");
        try {
            dao.removeConnexion();
        } catch (SQLException e) {
            throw new RemoteException("cannot remove Connexion: " + e);
        }
    }

    public void ejbCreate() throws CreateException {
        logger.log(BasicLevel.DEBUG, "");
        try {
            dao = new Dao();
        } catch (Exception e) {
            throw new CreateException("Cannot create Dao: " + e);
        }
    }

    public void methode1() throws RemoteException {
        logger.log(BasicLevel.DEBUG, "");
        try {
            String liste = dao.rechercherTousLesMarches();
            logger.log(BasicLevel.DEBUG, liste);
        } catch (Exception e) {
            throw new RemoteException("Error calling Dao: " + e);
        }
    }
    
    /**
     * Method with RequiresNew Tx Attribute.
     * Uses the connection allocated in another transaction
     * @throws RemoteException
     */
    public void testTxNew() throws RemoteException {
        logger.log(BasicLevel.DEBUG, "");
        try {
            String liste = dao2.rechercherTousLesMarches();
            logger.log(BasicLevel.DEBUG, liste);
        } catch (Exception e) {
            throw new RemoteException("Error calling Dao: " + e);
        }
    }

    public void methode2() throws java.rmi.RemoteException {
        logger.log(BasicLevel.DEBUG, "");
        try {
            dao2 = new Dao();
            testTxNew();
            dao2.removeConnexion();
        } catch (Exception e) {
            throw new RemoteException("Cannot create Dao: " + e);
        }
    }
    
    public void moscone1() throws java.rmi.RemoteException {
        logger.log(BasicLevel.DEBUG, "");
        try {
            dao2 = new Dao();
            dao2.rechercherTousLesMarches();
            dao2.removeConnexion();
        } catch (Exception e) {
            throw new RemoteException("Error on Dao2: " + e);
        }
    }

    public void getconn() throws java.rmi.RemoteException {
        logger.log(BasicLevel.DEBUG, "");
        try {
            dao2 = new Dao();
        } catch (Exception e) {
            throw new RemoteException("Cannot create Dao: " + e);
        }
    }

    public void getconntx() throws java.rmi.RemoteException {
        logger.log(BasicLevel.DEBUG, "");
        try {
            dao2 = new Dao();
        } catch (Exception e) {
            throw new RemoteException("Cannot create Dao: " + e);
        }
    }

    public void useconn() throws java.rmi.RemoteException {
        logger.log(BasicLevel.DEBUG, "");
        try {
            String liste = dao2.rechercherTousLesMarches();
            logger.log(BasicLevel.DEBUG, liste);
        } catch (Exception e) {
            throw new RemoteException("Error calling Dao: " + e);
        }
    }

    public void closeconn() throws java.rmi.RemoteException {
        logger.log(BasicLevel.DEBUG, "");
        try {
            dao2.removeConnexion();
        } catch (SQLException e) {
            throw new RemoteException("cannot remove Connexion: " + e);
        }
    }

    public void ejbActivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbPassivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void setSessionContext(javax.ejb.SessionContext ctx) {
        if (logger == null) {
            logger = Log.getLogger(Log.JONAS_TESTS_PREFIX);
        }
        logger.log(BasicLevel.DEBUG, "");
        ejbContext = ctx;
    }

    public void unsetSessionContext() {
        logger.log(BasicLevel.DEBUG, "");
    }
}
