/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

// PersonEC.java

package org.objectweb.jonas.jtests.beans.inherit;

import javax.ejb.CreateException;
import javax.ejb.EntityBean;
import javax.ejb.EntityContext;
import javax.ejb.RemoveException;

import org.ow2.jonas.lib.util.Log;
import org.objectweb.util.monolog.api.Logger;
import org.objectweb.util.monolog.api.BasicLevel;


public class PersonEC extends PersonImpl implements EntityBean {

    static protected Logger logger = null;
    protected EntityContext entityContext;

    public IdPK ejbCreate(int val_id, String val_name) throws CreateException {

	logger.log(BasicLevel.DEBUG,
		   "PersonEC.ejbCreate(int " + val_id + " ,String " + val_name + ")");
	// what should be done in a container managed bean is only the following lines ...
	id   = val_id;
	name = val_name;
	age  = 1;

	return(null);
    }
    
    public void ejbPostCreate(int val_id, String val_name) {
    }

    public void ejbActivate() {
    }

    public void ejbPassivate() {
    }

    public void ejbLoad() {
    }

    public void ejbStore() {
    }
  
    // Test with no RemoveException exception
    public void ejbRemove() {
    }

    public void setEntityContext(EntityContext ctx) { 
        if (logger == null) {
	    logger = Log.getLogger("org.objectweb.jonas_tests");
	}
	logger.log(BasicLevel.DEBUG, "");
	entityContext  = ctx;
    }

    public void unsetEntityContext() {
    }

} 
