 /*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.beans.folder;

import java.rmi.RemoteException;
import javax.ejb.CreateException;
import javax.ejb.EntityBean;
import javax.ejb.EntityContext;
import javax.ejb.RemoveException;

import org.ow2.jonas.lib.util.Log;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;


/**
 * This is an entity bean with "container managed persistence version 2.x".
 * The state of an instance is stored into a relational database.
 * The following table should exist :
 * jt2_paper3
 * c_statut3  varchar     primarey key
 * c_valide   integer
 *
 * @author Philippe Coq
 */

public abstract class Paper3EC2 implements EntityBean {

    static protected Logger logger = null;

    protected EntityContext entityContext;


    // Get and Set accessor methods of the bean's abstract schema
    // cmp-field
    public abstract  String  getStatut3();
    public abstract void setStatut3(String  statut);

    public abstract String getValide();
    public abstract void setValide(String val);


    //  cmr-field
    public abstract java.util.Collection  getPaper2s();
    public abstract void setPaper2s(java.util.Collection  p);

    public void ejbActivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbPassivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbLoad() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbStore() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbRemove() throws RemoveException {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void setEntityContext(EntityContext ctx) {
        if (logger == null)
            logger = Log.getLogger(Log.JONAS_TESTS_PREFIX);
        logger.log(BasicLevel.DEBUG, "");
        entityContext  = ctx;

    }

    public void unsetEntityContext() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public String ejbCreate(String statut) throws CreateException {
        logger.log(BasicLevel.DEBUG, "");
        setStatut3(statut);
        setValide("0");
        return null;            // In CMP, should return null.
    }

    public void ejbPostCreate(String id) {
        logger.log(BasicLevel.DEBUG, "");
    }

}
