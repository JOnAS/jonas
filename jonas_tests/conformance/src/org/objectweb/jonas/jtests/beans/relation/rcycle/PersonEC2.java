// PersonEC2.java

package org.objectweb.jonas.jtests.beans.relation.rcycle;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.FinderException;

import org.ow2.jonas.lib.util.Log;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;


/**
 * Class implementation of bean Person which is a CMP2 entity. <br>
 * This bean Person have several bidirectional relation-ships with itself. <br>
 * The CMP fields are: <br>
 * - id <br>
 * - name <br>
 * - sex (male or female). <br>
 * The CMR fields are: <br>
 * - spouse, (One-One-bi relation), <br>
 * - parents and children, (Many-Many-bi relation), <br>
 * - guardian and is-guardian-of, (One-Many-bi relation). <br>
 *
 * @author Helene Joanin
 */
public abstract class PersonEC2 implements javax.ejb.EntityBean {

    private static Logger logger = null;
    private javax.ejb.EntityContext ejbContext;
    private PersonHomeLocal plHome = null;


    // ---------------------------------------------------------------------
    //  Get and Set accessor methods of cmp and cmr fields
    // ---------------------------------------------------------------------

    public abstract Integer getId();
    public abstract void setId(Integer id);

    public abstract String getName();
    public abstract void setName(String name);

    public abstract int getSex();
    public abstract void setSex(int sex);

    public abstract PersonLocal getSpouse();
    public abstract void setSpouse(PersonLocal spouse);

    public abstract Collection getParents();
    public abstract void setParents(Collection parents);

    public abstract Collection getChildren();
    public abstract void setChildren(Collection parents);

    public abstract PersonLocal getGuardian();
    public abstract void setGuardian(PersonLocal guardian);

    public abstract Collection getGuardianOf();
    public abstract void setGuardianOf(Collection guardianOf);


    // ---------------------------------------------------------------------
    //  Call back methods for those defined in the bean interfaces.
    // ---------------------------------------------------------------------

    public java.lang.String ejbCreate(Integer id, String name, int sex)
        throws javax.ejb.CreateException {
        logger.log(BasicLevel.DEBUG, "id = " + id + ", name = " + name + ", sex = " + sex);
        // Init here the bean fields
        setId(id);
        setName(name);
        setSex(sex);
        return null;
    }

    public void ejbPostCreate(Integer id, String name, int sex) {
        logger.log(BasicLevel.DEBUG, "");
    }

    public Integer retrieveSpouse() {
        PersonLocal spouse = getSpouse();
        if (spouse == null) {
            logger.log(BasicLevel.DEBUG, "return null");
            return null;
        } else {
            logger.log(BasicLevel.DEBUG, "return " + spouse.getId());
            return spouse.getId();
        }
    }

    public void assignSpouse(Integer id) throws FinderException {
        logger.log(BasicLevel.DEBUG, "param=" + id);
        if (id != null) {
            PersonLocal spouse = plHome.findByPrimaryKey(id);
            setSpouse(spouse);
        } else {
            setSpouse(null);
        }
    }

    public Collection retrieveParents() {
        Collection ps = getParents();
        ArrayList result;
        if (ps.size() <= 0) {
            result = new ArrayList();
        } else {
            result = new ArrayList(ps.size());
        }
        for (Iterator it = ps.iterator(); it.hasNext();) {
            result.add(((PersonLocal) it.next()).getPrimaryKey());
        }
        logger.log(BasicLevel.DEBUG, "return=" + result);
        return result;
    }

    public void assignParents(Collection c) throws FinderException {
        logger.log(BasicLevel.DEBUG, "param=" + c);
        ArrayList al;
        if (c == null) {
            al = new ArrayList();
        } else {
            if (c.size() <= 0) {
                al = new ArrayList();
            } else {
                al = new ArrayList(c.size());
                for (Iterator it = c.iterator(); it.hasNext();) {
                    al.add(plHome.findByPrimaryKey((Integer) it.next()));
                }
            }
        }
        setParents(al);
    }

    public void addInParents(Integer id) throws FinderException {
        logger.log(BasicLevel.DEBUG, "param=" + id);
        getParents().add(plHome.findByPrimaryKey(id));
    }

    public Collection retrieveChildren() {
        Collection ps = getChildren();
        ArrayList result;
        if (ps.size() <= 0) {
            result = new ArrayList();
        } else {
            result = new ArrayList(ps.size());
        }
        for (Iterator it = ps.iterator(); it.hasNext();) {
            result.add(((PersonLocal) it.next()).getPrimaryKey());
        }
        logger.log(BasicLevel.DEBUG, "return=" + result);
        return result;
    }

    public Collection retrieveChildrenNames() {
        Collection ps = getChildren();
        ArrayList result;
        if (ps.size() <= 0) {
            result = new ArrayList();
        } else {
            result = new ArrayList(ps.size());
        }
        for (Iterator it = ps.iterator(); it.hasNext();) {
            result.add(((PersonLocal) it.next()).getName());
        }
        logger.log(BasicLevel.DEBUG, "return=" + result);
        return result;
    }

    public void assignChildren(Collection c) throws FinderException {
        logger.log(BasicLevel.DEBUG, "param=" + c);
        ArrayList al;
        if (c == null) {
            al = new ArrayList();
        } else {
            if (c.size() <= 0) {
                al = new ArrayList();
            } else {
                al = new ArrayList(c.size());
                for (Iterator it = c.iterator(); it.hasNext();) {
                    al.add(plHome.findByPrimaryKey((Integer) it.next()));
                }
            }
        }
        setChildren(al);
    }

    public void addInChildren(Integer id) throws FinderException {
        logger.log(BasicLevel.DEBUG, "param=" + id);
        getChildren().add(plHome.findByPrimaryKey(id));
    }


    public Integer retrieveGuardian() {
        PersonLocal guardian = getGuardian();
        if (guardian == null) {
            logger.log(BasicLevel.DEBUG, "return null");
            return null;
        } else {
            logger.log(BasicLevel.DEBUG, "return " + guardian.getId());
            return guardian.getId();
        }
    }

    public void assignGuardian(Integer id) throws FinderException {
        logger.log(BasicLevel.DEBUG, "param=" + id);
        if (id != null) {
            PersonLocal guardian = plHome.findByPrimaryKey(id);
            setGuardian(guardian);
        } else {
            setGuardian(null);
        }
    }

    public Collection retrieveGuardianOf() {
        Collection ps = getGuardianOf();
        ArrayList result;
        if (ps.size() <= 0) {
            result = new ArrayList();
        } else {
            result = new ArrayList(ps.size());
        }
        for (Iterator it = ps.iterator(); it.hasNext();) {
            result.add(((PersonLocal) it.next()).getPrimaryKey());
        }
        logger.log(BasicLevel.DEBUG, "return=" + result);
        return result;
    }

    public void assignInGuardianOf(Collection c) throws FinderException {
        logger.log(BasicLevel.DEBUG, "param=" + c);
        ArrayList al;
        if (c == null) {
            al = new ArrayList();
        } else {
            if (c.size() <= 0) {
                al = new ArrayList();
            } else {
                al = new ArrayList(c.size());
                for (Iterator it = c.iterator(); it.hasNext();) {
                    al.add(plHome.findByPrimaryKey((Integer) it.next()));
                }
            }
        }
        setGuardianOf(al);
    }

    public void addInGuardianOf(Integer id) throws FinderException {
        logger.log(BasicLevel.DEBUG, "param=" + id);
        getGuardianOf().add(plHome.findByPrimaryKey(id));
    }

    public boolean testCmrNull() throws FinderException {
        PersonLocal g = plHome.findByPrimaryKey(new Integer(3));
        PersonLocal sg = g.getSpouse();
        if (sg == null) {
            return true;
        } else {
            return false;
        }
    }

    // ---------------------------------------------------------------------
    //  Standard call back methods of those defined in javax.ejb.EntityBean
    // ---------------------------------------------------------------------

    public void setEntityContext(javax.ejb.EntityContext ctx) {
        // init the logger
        if (logger == null) {
            logger = Log.getLogger("org.objectweb.jonas_tests");
        }
        logger.log(BasicLevel.DEBUG, "");
        // save the given EntityContext
        ejbContext = ctx;
        // get the PersonHomeLocal (local home of the current bean
        plHome = (PersonHomeLocal) ejbContext.getEJBLocalHome();
    }

    public void unsetEntityContext() {
        logger.log(BasicLevel.DEBUG, "");
        ejbContext = null;
    }

    public void ejbRemove() throws javax.ejb.RemoveException {
        logger.log(BasicLevel.DEBUG, "");
    }


    public void ejbLoad() {
        logger.log(BasicLevel.DEBUG, "");
    }


    public void ejbStore() {
        logger.log(BasicLevel.DEBUG, "");
    }


    public void ejbPassivate() {
        logger.log(BasicLevel.DEBUG, "");
    }


    public void ejbActivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

}



