/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.beans.local;

import javax.ejb.CreateException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.InitialContext;
import javax.transaction.UserTransaction;

import org.ow2.jonas.lib.util.Log;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;


/**
 * Stateless Session
 * @author Philippe Durieux, Philippe Coq
 */
public class TargetSL implements SessionBean {

    static protected Logger logger = null;
    SessionContext ejbContext;

    public String string;
    public int number;
    public  boolean createdViaCreateXX;
    public  boolean createdViaCreateYY;
    private int [] inttable = new int[30];

    // ------------------------------------------------------------------
    // SessionBean implementation
    // ------------------------------------------------------------------

    /** 
     * Set the associated session context. The container calls this method
     * after the instance creation. 
     * The enterprise Bean instance should store the reference to the context
     * object in an instance variable. 
     * This method is called with no transaction context.
     *
     * @param sessionContext A SessionContext interface for the instance.
     * @throws EJBException Thrown by the method to indicate a failure caused by
     * a system-level error.
     */
    public void setSessionContext(SessionContext ctx) { 
        if (logger == null) {
            logger = Log.getLogger(Log.JONAS_TESTS_PREFIX);
        }
        logger.log(BasicLevel.DEBUG, "");
        ejbContext = ctx;
    }
	
    /**
     * A container invokes this method before it ends the life of the session object. 
     * This happens as a result of a client's invoking a remove operation, or when a 
     *  container decides to terminate the session object after a timeout. 
     * This method is called with no transaction context. 
     *
     * @throws EJBException Thrown by the method to indicate a failure caused by
     * a system-level error.
     */
    public void ejbRemove() {
        logger.log(BasicLevel.DEBUG, "");
    }
	
    /**
     * Check allowed and disallowed methods (See spec EJB 2.1 p 100)
     * @throws CreateException Failure to create a session EJB object.
     */
    public void ejbCreate() throws CreateException {
        logger.log(BasicLevel.DEBUG, "");
        
        // These operations are allowed
        ejbContext.getEJBHome();
        ejbContext.getEJBLocalHome();
        ejbContext.getEJBObject();
        ejbContext.getEJBLocalObject();
        ejbContext.getTimerService();
        
        try {
            // Must access java:comp/env
            InitialContext ictx = new InitialContext();
            ictx.lookup("java:comp/env/myname");
        } catch (Exception e) {
            logger.log(BasicLevel.ERROR, "Cannot lookup java:comp/env/myname: " + e);
            throw new CreateException("Cannot lookup java:comp/env/myname: " + e);
        }
        
        // These operations are disallowed
        try {
            ejbContext.getCallerPrincipal();
            throw new CreateException("getCallerPrincipal disallowed");
        } catch (IllegalStateException e) {
            logger.log(BasicLevel.DEBUG, "getCallerPrincipal disallowed");
        }
        try {
            ejbContext.isCallerInRole("");
            throw new CreateException("isCallerInRole disallowed");
        } catch (IllegalStateException e) {
            logger.log(BasicLevel.DEBUG, "isCallerInRole disallowed");
        }
    }

    /**
     * A container invokes this method on an instance before the instance
     * becomes disassociated with a specific EJB object.
     */
    public void ejbPassivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    /**
     * A container invokes this method when the instance is taken out of 
     * the pool of available instances to become associated with a specific 
     * EJB object.
     */
    public void ejbActivate() {
        logger.log(BasicLevel.DEBUG, "");
    }
    
    // ------------------------------------------------------------------
    // LocalInterface implementation
    // ------------------------------------------------------------------

    /**
     * getTwenty
     */
    public int  getTwenty() {
        logger.log(BasicLevel.DEBUG, "");
        return 20;
    }

    /**
     * lmethod2
     */
    public void lmethod2(java.lang.String s) {
        logger.log(BasicLevel.DEBUG, "");
    }

    // ------------------------------------------------------------------
    // Target implementation
    // ------------------------------------------------------------------

    /**
     * getTen
     */
    public int  getTen() {
        logger.log(BasicLevel.DEBUG, "");
        return 10;
    }

    /**
     * method2
     */
    public void method2(java.lang.String s) {
        logger.log(BasicLevel.DEBUG, "");
    }

    /**
     * getNumber
     * Not called
     */
    public int  getNumber() {
        logger.log(BasicLevel.DEBUG, "");
        return 0;
    }

    /**
     * getString
     * Not called
     */
    public String  getString() {
        logger.log(BasicLevel.DEBUG, "");
        return null;
    }

    /**
     * isCreatedViaCreateXX
     * Not called
     */
    public boolean  isCreatedViaCreateXX() {
        logger.log(BasicLevel.DEBUG, "");
        return false;
    }


}
