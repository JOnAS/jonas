/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

// SimpleCommon.java

package org.objectweb.jonas.jtests.beans.transacted;

import java.rmi.RemoteException;

import javax.ejb.EJBContext;
import javax.transaction.Status;
import javax.transaction.UserTransaction;


import org.ow2.jonas.lib.util.Log;
import org.objectweb.jonas.jtests.util.JBean;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * Common part to different implementations of Simple
 */
public class SimpleCommon extends JBean {

    static protected Logger logger = null;
	private EJBContext ejbContext = null;

	protected void setEjbContext(EJBContext ctx) {
		ejbContext = ctx;
	}
	
    /**
     * Utility method that returns true if the thread is associated to a
     * transaction
     */
	protected boolean isAssociated() {
		int ret;
		try {
			UserTransaction   ut = getUserTransaction();
			ret = ut.getStatus();
		} catch (Exception e) {
			logger.log(BasicLevel.ERROR, "isAssociated: " + e);
			return false;
		}
		if (ret == Status.STATUS_UNKNOWN) {
            logger.log(BasicLevel.DEBUG, "STATUS_UNKNOWN");
			return false;
		} else if (ret == Status.STATUS_NO_TRANSACTION) {
            logger.log(BasicLevel.DEBUG, "STATUS_NO_TRANSACTION");
			return false;
		} else {
            logger.log(BasicLevel.DEBUG, "Status=" + ret);
			return true;
		}
	}

	/**
	 * Utility method that init the logger
     */
    protected void initLogger() {
        if (logger == null)
            logger = Log.getLogger(Log.JONAS_TESTS_PREFIX);
    }

    /**
     * This method return true if there is an association of a transaction with 
     * this thread
     */
    public boolean  opwith_notsupported() {
        logger.log(BasicLevel.DEBUG, "");
        return isAssociated();
    }

    /**
     * This method return true if there is an association of a transaction with 
     * this thread
     */
    public boolean opwith_supports() {
        logger.log(BasicLevel.DEBUG, "");
        return isAssociated();
    }

    /**
     * This method return true if there is an association of a transaction with 
     * this thread
     */
    public boolean  opwith_required() {
        logger.log(BasicLevel.DEBUG, "");
        return isAssociated();
    }

    /**
     * This method return true if there is an association of a transaction with 
     * this thread
     */
    public boolean opwith_requires_new() {
        logger.log(BasicLevel.DEBUG, "");
        return isAssociated();
    }

    /**
     * This method return true if there is an association of a transaction with 
     * this thread
     */
    public boolean opwith_mandatory() {
        logger.log(BasicLevel.DEBUG, "");
        return isAssociated();
    }
  
    /**
     * This method return true if there is an association of a transaction with 
     * this thread
     */
    public boolean opwith_never() {
        logger.log(BasicLevel.DEBUG, "");
        return isAssociated();
    }
 
    /**
     * REQUIRED -> should return true.
     */
    public boolean required_call_requires_new() {
        logger.log(BasicLevel.DEBUG, "");
        boolean tx = opwith_requires_new();
        if (!tx) {
            logger.log(BasicLevel.ERROR, "opwith_requires_new was outside tx");
            return false;	// error
        }
        return isAssociated();
    }

    /**
     * REQUIRED -> should return true.
     */
    public boolean call_requires_new_on(Simple other) throws RemoteException {
        logger.log(BasicLevel.DEBUG, "");
        boolean tx = other.opwith_requires_new();
        if (!tx) {
            logger.log(BasicLevel.ERROR, "opwith_requires_new was outside tx");
            return false;	// error
        }
        return isAssociated();
    }

}
