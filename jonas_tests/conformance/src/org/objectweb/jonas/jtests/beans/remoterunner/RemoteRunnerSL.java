/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

// RemoteRunnerSL.java
// Stateless Session bean

package org.objectweb.jonas.jtests.beans.remoterunner;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.rmi.RemoteException;

import javax.ejb.CreateException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

import junit.framework.Test;
import junit.textui.TestRunner;

import org.ow2.jonas.lib.util.Log;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 *
 */

public class RemoteRunnerSL  implements SessionBean {

    static private Logger logger = null;
    SessionContext ejbContext;


    // ------------------------------------------------------------------
    // SessionBean implementation
    // ------------------------------------------------------------------

    /**
     * Set the associated session context. The container calls this method
     * after the instance creation.
     * The enterprise Bean instance should store the reference to the context
     * object in an instance variable.
     * This method is called with no transaction context.
     *
     * @param sessionContext A SessionContext interface for the instance.
     * @throws EJBException Thrown by the method to indicate a failure caused by
     * a system-level error.
     */
    public void setSessionContext(SessionContext ctx) {
        if (logger == null)
            logger = Log.getLogger(Log.JONAS_TESTS_PREFIX);
	logger.log(BasicLevel.DEBUG, "RemoteRunnerSL setSessionContext");
	ejbContext = ctx;
    }

    /**
     * A container invokes this method before it ends the life of the session object.
     * This happens as a result of a client's invoking a remove operation, or when a
     *  container decides to terminate the session object after a timeout.
     * This method is called with no transaction context.
     *
     * @throws EJBException Thrown by the method to indicate a failure caused by
     * a system-level error.
     */
    public void ejbRemove() {
	logger.log(BasicLevel.DEBUG, "RemoteRunnerSL ejbRemove");
    }

    /**
     * The Session bean must define 1 or more ejbCreate methods.
     *
     * @throws CreateException Failure to create a session EJB object.
     */
    public void ejbCreate() throws CreateException {
	logger.log(BasicLevel.DEBUG, "RemoteRunnerSL ejbCreate");
    }

    /**
     * A container invokes this method on an instance before the instance
     * becomes disassociated with a specific EJB object.
     */
    public void ejbPassivate() {
	logger.log(BasicLevel.DEBUG, "RemoteRunnerSL ejbPassivate");
    }

    /**
     * A container invokes this method when the instance is taken out of
     * the pool of available instances to become associated with a specific
     * EJB object.
     */
    public void ejbActivate() {
	logger.log(BasicLevel.DEBUG, "RemoteRunnerSL ejbActivate");
    }

    // ------------------------------------------------------------------
    // RemoteRunner implementation
    // ------------------------------------------------------------------

    /**
     * Run a JUnit TestSuite
     *
     * @param jtcc the class of the JUnit TestSuite to be run
     *
     * The code run here is equivalent to :
     *     TestRunner.run( jtcc.suite())
     */

    public String run(Class  jtcc) throws RemoteException {
	logger.log(BasicLevel.DEBUG, "RemoteRunnerSL run");
	try{
            Test  ts = null;
            Method method = jtcc.getMethod("suite", (Class []) null);
            ts = (Test)method.invoke(null, (Object []) null);
            ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
            PrintStream ps = new java.io.PrintStream(baos);
            TestRunner tr = new TestRunner(ps);
	    tr.doRun(ts , false);
	    return baos.toString();
	}catch( Exception e){
	    throw new RemoteException(e.toString());
	}

    }

  /**
     * Run a TestCase in the JUnit TestSuite
     *
     * @param jtcc the class of the JUnit TestSuite to be run
     * @param name name of the testcase to run
     *
     * This code run here is equivalent to do
     *     TestRunner.run(new jtcc(testtorun))
     */
    public String run(Class jtcc, String testtorun) throws RemoteException {
	logger.log(BasicLevel.DEBUG, "RemoteRunnerSL run: "+testtorun);
	try{
            Object suiteInstance = null;
            int nbParams = 1;
            Class paramTypes[] = new Class[nbParams];  // constructor argument types
            Object paramObjects[] = new Object[nbParams]; // constructor argument values
            paramTypes[0] = java.lang.String.class;
            paramObjects[0] = (Object)testtorun;
            Constructor constructor = jtcc.getConstructor(paramTypes);
            suiteInstance = constructor.newInstance(paramObjects);
            ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
            PrintStream ps = new java.io.PrintStream(baos);
            TestRunner tr = new TestRunner(ps);
	    tr.doRun((Test)suiteInstance , false);
	    return baos.toString();
	}catch( Exception e){
	    throw new RemoteException(e.toString());
	}

    }


}
