/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.beans.relation.omb;

import org.ow2.jonas.lib.util.Log;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

import javax.ejb.CreateException;
import javax.ejb.DuplicateKeyException;
import javax.ejb.EntityContext;
import javax.ejb.RemoveException;
import javax.ejb.FinderException;
import javax.ejb.EJBException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

import java.util.Collection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author S.Chassande-Barrioz, Helene Joanin
 */
public abstract class AEC2 implements javax.ejb.EntityBean {

    private BHomeLocal bhl = null;

    public void m1(){
    }

    public void assignB(Collection c) throws FinderException {
        logger.log(BasicLevel.DEBUG, "");
        ArrayList al = new ArrayList(c.size());
        for(Iterator it = c.iterator(); it.hasNext();)
            al.add(bhl.findByPrimaryKey((String) it.next()));
        setB(al);
    }
    public void assignBInNewTx(Collection c) throws FinderException {
        logger.log(BasicLevel.DEBUG, "");
        assignB(c);
    }

    public Collection retrieveB() {
        logger.log(BasicLevel.DEBUG, "");
        Collection bs = getB();
        ArrayList result = new ArrayList(bs.size());
        for(Iterator it = bs.iterator(); it.hasNext();)
            result.add(((BLocal) it.next()).getPrimaryKey());
        return result;
    }
    public Collection retrieveBInNewTx() {
        logger.log(BasicLevel.DEBUG, "");
        return retrieveB();
    }
    public Collection retrieveBisB() {
        logger.log(BasicLevel.DEBUG, "");
        // To reproduce the bug #300156: Error on creating Array from cmr-collection
        // same as retrieveB(), except the bs initialization
        List bs = new ArrayList(getB());
        ArrayList result = new ArrayList(bs.size());
        for(Iterator it = bs.iterator(); it.hasNext();)
            result.add(((BLocal) it.next()).getPrimaryKey());
        return result;
    }

    public void addInB(String pkb) throws FinderException {
        logger.log(BasicLevel.DEBUG, "");
        getB().add(bhl.findByPrimaryKey(pkb));
    }
    public void addInBInNewTx(String pkb) throws FinderException {
        logger.log(BasicLevel.DEBUG, "");
        addInB(pkb);
    }

    public void addNewB(String pkb) throws CreateException, FinderException {
        logger.log(BasicLevel.DEBUG, "");
        BLocal bl = bhl.create(pkb);
        getB().add(bl);
        bhl.findByName(pkb, getId());
    }

    public void addAllInB(Collection pkbs) throws FinderException {
        logger.log(BasicLevel.DEBUG, "");
        ArrayList al = new ArrayList();
        for (Iterator it = pkbs.iterator(); it.hasNext();)
            al.add(bhl.findByPrimaryKey((String) it.next()));
        getB().addAll(al);
    }
    public void addAllInBInNewTx(Collection pkbs) throws FinderException {
        logger.log(BasicLevel.DEBUG, "");
        addAllInB(pkbs);
    }

    public void removeFromB(String pkb) throws FinderException {
        logger.log(BasicLevel.DEBUG, "");
        getB().remove(bhl.findByPrimaryKey(pkb));
    }
    public void removeFromBInNewTx(String pkb) throws FinderException {
        logger.log(BasicLevel.DEBUG, "");
        removeFromB(pkb);
    }

    public void clearB() {
        logger.log(BasicLevel.DEBUG, "");
        getB().clear();
    }

    public void clearBInNewTx() {
        logger.log(BasicLevel.DEBUG, "");
        clearB();
    }

    public boolean containAllInB(Collection pkbs) throws FinderException {
        logger.log(BasicLevel.DEBUG, "");
        ArrayList al = new ArrayList(pkbs.size());
        for(Iterator it = pkbs.iterator(); it.hasNext();)
            al.add(bhl.findByPrimaryKey((String) it.next()));
        return getB().containsAll(al);
    }

    /**
     * It returns true the multivalued relation contains the bean B defined
     * by the primary key specified by the parameter.
     * This method has the transactional attribut TX_SUPPORTS.
     * @throw a FinderException if the primary key does not match to a bean.
     */
    public boolean containInB(String pkb) throws FinderException {
        logger.log(BasicLevel.DEBUG, "");
        return (getB().contains(bhl.findByPrimaryKey(pkb)));
    }

    /**
     * This method check it isn't allowed to reset the pk
     * and that the container throw the java.lang.IllegalStateException.
     * It returns true if ok.
     * See spec 2.0, chapter 10.3.5, page 134
     */
    public boolean testResetPkForbidden(String pka) {
        logger.log(BasicLevel.DEBUG, "");
        boolean ret = false;
        try {
            setId(pka);
        } catch (IllegalStateException e) {
            ret = true;
        }
        return ret;
    }


    // ------------------------------------------------------------------
    // Get and Set accessor methods of the bean's abstract schema
    // ------------------------------------------------------------------
    public abstract String getId();
    public abstract void setId(String id);

    // This cmp field with an utility class type Product
    // to test that this Product class can be resolved in the JORM adapter
    public abstract Product getProduct();
    public abstract void setProduct(Product p);

    public abstract Collection getB();
    public abstract void setB(Collection bl);

    // ------------------------------------------------------------------
    // EntityBean implementation
    // ------------------------------------------------------------------

    static protected Logger logger = null;
    EntityContext ejbContext;

    /**
     * The Entity bean can define 0 or more ejbCreate methods.
     *
     * @throws CreateException Failure to create an entity EJB object.
     * @throws DuplicateKeyException An object with the same key already exists.
     */
    public String ejbCreate(String id) throws CreateException, DuplicateKeyException {
        logger.log(BasicLevel.DEBUG, "");

        // Init here the bean fields
        setId(id);
        setProduct(new Product());

        // In CMP, should return null.
        return null;
    }

    /**
     * Set the associated entity context. The container invokes this method
     * on an instance after the instance has been created.
     * This method is called in an unspecified transaction context.
     *
     * @param ctx - An EntityContext interface for the instance. The instance
     * should store the reference to the context in an instance variable.
     * @throws EJBException Thrown by the method to indicate a failure caused by a
     * system-level error.
     */
    public void setEntityContext(EntityContext ctx) {
        if (logger == null)
            logger = Log.getLogger(Log.JONAS_TESTS_PREFIX);
        logger.log(BasicLevel.DEBUG, "");
        ejbContext = ctx;
        try {
            Context ictx = new InitialContext();
            bhl = (BHomeLocal) ictx.lookup("java:comp/env/ejb/b");
        } catch (NamingException e) {
            throw new EJBException("Impossible to fetch the ", e);
        }
    }

    /**
     * Unset the associated entity context. The container calls this method
     * before removing the instance.
     * This is the last method that the container invokes on the instance.
     * The Java garbage collector will eventually invoke the finalize() method
     * on the instance.
     * This method is called in an unspecified transaction context.
     *
     * @throws EJBException Thrown by the method to indicate a failure caused by a
     * system-level error.
     */
    public void unsetEntityContext() {
        logger.log(BasicLevel.DEBUG, "");
        ejbContext = null;
    }

    /**
     * A container invokes this method before it removes the EJB object
     * that is currently associated with the instance. This method is
     * invoked when a client invokes a remove operation on the enterprise Bean's
     * home interface or the EJB object's remote interface. This method
     * transitions the instance from the ready state to the pool of available
     * instances.
     *
     * This method is called in the transaction context of the remove operation.
     * @throws RemoveException  The enterprise Bean does not allow destruction of the object.
     * @throws EJBException - Thrown by the method to indicate a failure caused by a system-level
     * error.
     */
    public void ejbRemove() throws RemoveException {
        logger.log(BasicLevel.DEBUG, "");
    }

    /**
     * A container invokes this method to instruct the instance to synchronize
     * its state by loading it state from the underlying database.
     * This method always executes in the proper transaction context.
     *
     * @throws EJBException Thrown by the method to indicate a failure caused by
     * a system-level error.
     */
    public void ejbLoad() {
        logger.log(BasicLevel.DEBUG, "");
    }

    /**
     * A container invokes this method to instruct the instance to synchronize
     * its state by storing it to the underlying database.
     * This method always executes in the proper transaction context.
     *
     * @throws EJBException Thrown by the method to indicate a failure caused by
     * a system-level error.
     */
    public void ejbStore() {
        logger.log(BasicLevel.DEBUG, "");
    }

    /**
     * There must be an ejbPostCreate par ejbCreate method
     *
     * @throws CreateException Failure to create an entity EJB object.
     */
    public void ejbPostCreate(String id) throws CreateException {
        logger.log(BasicLevel.DEBUG, "id=" + id);
    }

    /**
     * A container invokes this method on an instance before the instance
     * becomes disassociated with a specific EJB object.
     */
    public void ejbPassivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    /**
     * A container invokes this method when the instance is taken out of
     * the pool of available instances to become associated with a specific
     * EJB object.
     */
    public void ejbActivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

}
