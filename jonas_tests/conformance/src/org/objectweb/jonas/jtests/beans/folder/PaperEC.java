/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.beans.folder;

import javax.ejb.EJBException;
import javax.ejb.EJBLocalHome;
import javax.ejb.EntityBean;
import javax.ejb.EntityContext;
import javax.ejb.CreateException;
import javax.ejb.RemoveException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

import org.ow2.jonas.lib.util.Log;
import org.objectweb.util.monolog.api.Logger;
import org.objectweb.util.monolog.api.BasicLevel;

/**
 * Implementation for the bean PaperEC.
 * @author Philippe Durieux, Philippe Coq
 */
public class PaperEC implements EntityBean {

    static protected Logger logger = null;
    EntityContext ejbContext;
    InitialContext ictx;
    Context myEnv;

    // ------------------------------------------------------------------
    // State of the bean.
    // They must be public for Container Managed Persistance.
    // ------------------------------------------------------------------
    public String name;
    public int value;

    /**
     * Check environment variables
     */
    void checkEnv(String method) {

        // Check directly in my context
        logger.log(BasicLevel.DEBUG, "Check directly in my context");
        try {
            String value = (String) myEnv.lookup("myname");
            if (!value.equals("myentity")) {
                logger.log(BasicLevel.ERROR, ": myEnv.lookup failed: myname=" + value);
                throw new EJBException("FileEC 1: " + method);
            }
        } catch (NamingException e) {
            logger.log(BasicLevel.ERROR, ": myEnv.lookup raised exception:\n" + e);
            throw new EJBException("FileEC 2: " + method);
        }
        // Check from initial Context
        logger.log(BasicLevel.DEBUG, "Check from initial Context");
        try {
            String value = (String) ictx.lookup("java:comp/env/myname");
            if (!value.equals("myentity")) {
                logger.log(BasicLevel.ERROR, ": ictx.lookup failed: myname=" + value);
                throw new EJBException("FileEC 6: " + method);
            }
        } catch (NamingException e) {
            logger.log(BasicLevel.ERROR, ": ictx.lookup raised exception:\n" + e);
            throw new EJBException("FileEC 7: " + method);
        }
        logger.log(BasicLevel.DEBUG, ": checkEnv OK");
    }

    // ------------------------------------------------------------------
    // EntityBean implementation
    // ------------------------------------------------------------------

    public void setEntityContext(EntityContext ctx) {
        if (logger == null) {
            logger = Log.getLogger(Log.JONAS_TESTS_PREFIX);
        }
        logger.log(BasicLevel.DEBUG, "");
        ejbContext = ctx;
        try {
            // Get initial Context
            ictx = new InitialContext();
            myEnv = (Context) ictx.lookup("java:comp/env");
        } catch (NamingException e) {
            throw new EJBException("PaperEC: Cannot get filehome:" + e);
        }
        checkEnv("setEntityContext");

        // Check that we can do "getEJBLocalHome"
        EJBLocalHome homel = ctx.getEJBLocalHome();
        if (homel == null) {
            throw new EJBException("PaperEC: setEntityContext cannot get EJBLocalHome");
        }
    }

    public void unsetEntityContext() {
        logger.log(BasicLevel.DEBUG, "");
        ejbContext = null;
    }

    public void ejbActivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbPassivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbLoad() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbStore() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbRemove() throws RemoveException {
        logger.log(BasicLevel.DEBUG, "");
    }

    // ------------------------------------------------------------------
    // ejbCreate methods
    // ------------------------------------------------------------------

    public String ejbCreate(String name) throws CreateException {
        logger.log(BasicLevel.DEBUG, "");
        this.name = name;
        this.value = 0;
        return null;            // In CMP, should return null.
    }

    public String ejbPostCreate(String name) throws CreateException {
        logger.log(BasicLevel.DEBUG, "");
        return null;            // In CMP, should return null.
    }

    // Not used for PaperEC only for PaperEC2
    public int ejbHomeGetTotalValeur() throws javax.ejb.FinderException {
         return 0;
     }
    // ------------------------------------------------------------------
    // PaperLocal implementation
    // ------------------------------------------------------------------

    public String getName() {
        logger.log(BasicLevel.DEBUG, "");
        return this.name;
    }

    public int getValue() {
        logger.log(BasicLevel.DEBUG, "");
        return this.value;
    }

    public void setValue(int v) {
        logger.log(BasicLevel.DEBUG, "");
        this.value = v;
    }
}

