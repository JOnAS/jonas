/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.beans.relation.cascade;

import java.rmi.RemoteException;
import javax.ejb.CreateException;
import javax.ejb.EntityContext;
import javax.ejb.EntityBean;

import org.ow2.jonas.lib.util.Log;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * Implementation for bean Address
 * @author Ph Durieux
 */
public abstract class AddressBean implements EntityBean {
   
    static protected Logger logger = null;
    protected EntityContext ejbContext = null;

    public Integer ejbCreate(String street, String city, String state, String zip) throws CreateException {
        logger.log(BasicLevel.DEBUG, "");
        setStreet(street);
        setCity(city);
        setState(state);
        setZip(zip);
        return null;
    }

    public void ejbPostCreate(String street, String city, String state, String zip) {
        logger.log(BasicLevel.DEBUG, "");
    }

    public Integer ejbCreate(AddressDO addr, CustomerL customer) throws CreateException {
        setStreet(addr.getStreet());
        setCity(addr.getCity());
        setState(addr.getState());
        setZip(addr.getZip());
        return null;
    }

    public void ejbPostCreate(AddressDO addr, CustomerL customer) throws CreateException {
        // Sould we do this ? Actually, it doesn't work.
        // SInce this is a 1-1 relation, when setting the cmr in customer it should set
        // this also, by coherence ? (to be confirmed)
        //setCustomer(customer);
    }

    public Integer getCustomerId() throws RemoteException {
        logger.log(BasicLevel.DEBUG, "");
        CustomerL c = getCustomer();
        Integer ret = null;
        if (c != null) {
            ret = c.getId();
        }
        return ret;
    }
    

    // relationship fields (if defined in ejb-relationship-role only)
    public abstract CustomerL getCustomer();
    public abstract void setCustomer(CustomerL cust);

    // persistent fields
    public abstract Integer getId();
    public abstract void setId(Integer id);
    public abstract String getStreet();
    public abstract void setStreet(String street);
    public abstract String getCity();
    public abstract void setCity(String city);
    public abstract String getState();
    public abstract void setState(String state);
    public abstract String getZip();
    public abstract void setZip(String zip);

    // standard call back methods
    
    public void setEntityContext(EntityContext ec) { 
        if (logger == null)
            logger = Log.getLogger(Log.JONAS_TESTS_PREFIX);
        logger.log(BasicLevel.DEBUG, "");
        ejbContext = ec;
    }

    public void unsetEntityContext() {
        logger.log(BasicLevel.DEBUG, "");
        ejbContext = null; 
    }

    public void ejbLoad() { 
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbStore() { 
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbActivate() { 
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbPassivate() { 
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbRemove() throws javax.ejb.RemoveException {
        logger.log(BasicLevel.DEBUG, ""); 
    }
}
