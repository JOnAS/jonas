/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.beans.relation.cascade;

import java.rmi.RemoteException;
import javax.ejb.CreateException;
import javax.ejb.EntityContext;
import javax.ejb.EntityBean;
import javax.ejb.RemoveException;

import org.ow2.jonas.lib.util.Log;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * Implementation for bean Invoice
 * @author Ph Durieux
 */
public abstract class InvoiceBean implements EntityBean {
   
    static protected Logger logger = null;
    protected EntityContext ejbContext = null;

    public Integer ejbCreate(String number) throws CreateException {
        logger.log(BasicLevel.DEBUG, "");
        setNumber(number);
        return null;
    }

    public void ejbPostCreate(String number) {
        logger.log(BasicLevel.DEBUG, "");
    }

    // persistent fields
    public abstract Integer getId();
    public abstract void setId(Integer id);
    public abstract String getNumber();
    public abstract void setNumber(String number);

    // persistent relationships
    public abstract CarL getCar();
    public abstract void setCar(CarL car);

    
    public void setEntityContext(EntityContext ec) {
        if (logger == null)
            logger = Log.getLogger(Log.JONAS_TESTS_PREFIX);
        logger.log(BasicLevel.DEBUG, "");
        ejbContext = ec;
    }

    public void unsetEntityContext() {
        logger.log(BasicLevel.DEBUG, "");
        ejbContext = null;
    }

    public void ejbLoad() {
        logger.log(BasicLevel.DEBUG, ""); 
    }

    public void ejbStore() { 
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbActivate() { 
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbPassivate() { 
        logger.log(BasicLevel.DEBUG, "");
    }

    /**
     * this instance is being removed.
     * we must be able to access bean fields here, including the CMRs
     */
    public void ejbRemove() throws javax.ejb.RemoveException {
        logger.log(BasicLevel.DEBUG, "number=" + getNumber());
        CarL mycar = getCar();
        if (mycar == null) {
            logger.log(BasicLevel.ERROR, "CMR field is null");
            throw new RemoveException("Cannot access CMR field inside Invoice.ejbRemove");
        }
        String carnumber = mycar.getNumber();
        if (! getNumber().startsWith(carnumber)) {
            throw new RemoveException("Bad car number while removing Invoice:" + carnumber);
        }
        CustomerL cust = mycar.getCustomer();
        if (cust == null) {
            logger.log(BasicLevel.ERROR, "Cannot get customer from invoice");
            throw new RemoveException("Cannot get customer from invoice");
        }
    }
}
