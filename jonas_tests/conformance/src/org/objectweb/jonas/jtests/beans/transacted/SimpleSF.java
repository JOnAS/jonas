/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

// SimpleSF.java

package org.objectweb.jonas.jtests.beans.transacted;


import java.rmi.RemoteException;

import javax.ejb.SessionContext;

import org.objectweb.util.monolog.api.BasicLevel;

/*
 * Stateful session bean
 */

public class SimpleSF extends SimpleCommon {

    protected SessionContext sessionContext;
    public int i = 0;

    public void setSessionContext(SessionContext sessionContext) {
    	setEjbContext(sessionContext);
        initLogger();
        logger.log(BasicLevel.DEBUG, "");
        this.sessionContext = sessionContext;
    }

    public void ejbActivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbPassivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbRemove() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void  ejbCreate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void  ejbCreate(int i) {
        this.i = i;
    }

    /**
     * This support method calls a required method
     */
    public boolean supports_call_required() throws RemoteException {
        logger.log(BasicLevel.DEBUG, "");
        Simple mysession = (Simple) sessionContext.getEJBObject();
        return mysession.opwith_required();
    }

    public void startInfoTimer(int dur, String info) throws RemoteException {
        logger.log(BasicLevel.ERROR, "not implemented");
    }

}
