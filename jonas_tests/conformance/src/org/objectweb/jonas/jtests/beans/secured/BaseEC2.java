/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.beans.secured;

import javax.ejb.CreateException;
import javax.ejb.EJBContext;
import javax.ejb.EntityBean;
import javax.ejb.EntityContext;
import javax.ejb.RemoveException;

import org.ow2.jonas.lib.util.Log;
import org.objectweb.util.monolog.api.BasicLevel;

/**
 * This is an entity bean with "container managed persistence 2".
 * The state of an instance is stored into a relational database.
 * @author Helene Joanin (jonas team)
 */
public abstract class BaseEC2 extends BaseCommon implements EntityBean {

    protected EntityContext entityContext;
	
    // ------------------------------------------------------------------
    // Get and Set accessor methods of the bean's abstract schema
    // ------------------------------------------------------------------
    public abstract String getInfo();
    public abstract void setInfo(String n);
    public abstract String getName();
    public abstract void setName(String n);

    public EJBContext getEJBContext() {
        return entityContext;
    }

    public void ejbActivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbPassivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbLoad() {
        logger.log(BasicLevel.DEBUG, "");	
    }

    public void ejbStore() {
	logger.log(BasicLevel.DEBUG, "");	
    }
  
    public void ejbRemove() throws RemoveException {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void setEntityContext(EntityContext ctx) { 
        if( logger == null)
            logger = Log.getLogger(Log.JONAS_TESTS_PREFIX);
        logger.log(BasicLevel.DEBUG, "");
	entityContext  = ctx;
	
    }

    public void unsetEntityContext() {
	logger.log(BasicLevel.DEBUG, "");
    }

    public String ejbCreate(String name, String info) throws CreateException {
	logger.log(BasicLevel.DEBUG,"");
	setInfo(info);
	setName(name);
	return(null);
    }

    public void ejbPostCreate(String name, String info){
	logger.log(BasicLevel.DEBUG, "");
    }

}
