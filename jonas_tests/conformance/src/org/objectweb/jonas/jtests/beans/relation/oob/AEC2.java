/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.beans.relation.oob;

import javax.ejb.CreateException;
import javax.ejb.DuplicateKeyException;
import javax.ejb.EJBException;
import javax.ejb.EntityContext;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

import org.ow2.jonas.lib.util.Log;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * @author S.Chassande-Barrioz
 */
public abstract class AEC2 implements javax.ejb.EntityBean {

    private BHomeLocal bhl = null;

    public void m1(){
    }

    public void assignB(String c) throws FinderException {
        logger.log(BasicLevel.DEBUG, "param=" + c);
        if (c != null) {
            BLocal bl = null;
            bl = bhl.findByPrimaryKey(c);
            setB(bl);
        } else
            setB(null);
    }
    public void assignBInNewTx(String c) throws FinderException {
        assignB(c);
    }

    public String retrieveB() {
        BLocal lejbB = getB();
        if (lejbB == null) {
            logger.log(BasicLevel.DEBUG, "return null");
            return null;
        } else {
            logger.log(BasicLevel.DEBUG, "return " + lejbB.getId());
            return lejbB.getId();
        }
    }

    public String retrieveBInNewTx() {
        return retrieveB();
    }

    // ------------------------------------------------------------------
    // Get and Set accessor methods of the bean's abstract schema
    // ------------------------------------------------------------------
    public abstract String getId();
    public abstract void setId(String id);

    public abstract BLocal getB();
    public abstract void setB(BLocal bl);

    public abstract BLocal getBBis();
    public abstract void setBBis(BLocal bl);

    // ------------------------------------------------------------------
    // EntityBean implementation
    // ------------------------------------------------------------------

    static protected Logger logger = null;
    EntityContext ejbContext;

    /**
     * The Entity bean can define 0 or more ejbCreate methods.
     *
     * @throws CreateException Failure to create an entity EJB object.
     * @throws DuplicateKeyException An object with the same key already exists.
     */
    public String ejbCreate(String id) throws CreateException, DuplicateKeyException {
        logger.log(BasicLevel.DEBUG, "");

        // Init here the bean fields
        setId(id);

        // In CMP, should return null.
        return null;
    }

    /**
     * Set the associated entity context. The container invokes this method
     * on an instance after the instance has been created.
     * This method is called in an unspecified transaction context.
     *
     * @param ctx - An EntityContext interface for the instance. The instance
     * should store the reference to the context in an instance variable.
     * @throws EJBException Thrown by the method to indicate a failure caused by a
     * system-level error.
     */
    public void setEntityContext(EntityContext ctx) {
        if (logger == null)
            logger = Log.getLogger(Log.JONAS_TESTS_PREFIX);
        logger.log(BasicLevel.DEBUG, "");
        ejbContext = ctx;
        try {
            Context ictx = new InitialContext();
            bhl = (BHomeLocal) ictx.lookup("java:comp/env/ejb/b");
        } catch (NamingException e) {
            throw new EJBException("Impossible to fetch the ", e);
        }
    }

    /**
     * Unset the associated entity context. The container calls this method
     * before removing the instance.
     * This is the last method that the container invokes on the instance.
     * The Java garbage collector will eventually invoke the finalize() method
     * on the instance.
     * This method is called in an unspecified transaction context.
     *
     * @throws EJBException Thrown by the method to indicate a failure caused by a
     * system-level error.
     */
    public void unsetEntityContext() {
        logger.log(BasicLevel.DEBUG, "");
        ejbContext = null;
    }

    /**
     * A container invokes this method before it removes the EJB object
     * that is currently associated with the instance. This method is
     * invoked when a client invokes a remove operation on the enterprise Bean's
     * home interface or the EJB object's remote interface. This method
     * transitions the instance from the ready state to the pool of available
     * instances.
     *
     * This method is called in the transaction context of the remove operation.
     * @throws RemoveException  The enterprise Bean does not allow destruction of the object.
     * @throws EJBException - Thrown by the method to indicate a failure caused by a system-level
     * error.
     */
    public void ejbRemove() throws RemoveException {
        logger.log(BasicLevel.DEBUG, "");
    }

    /**
     * A container invokes this method to instruct the instance to synchronize
     * its state by loading it state from the underlying database.
     * This method always executes in the proper transaction context.
     *
     * @throws EJBException Thrown by the method to indicate a failure caused by
     * a system-level error.
     */
    public void ejbLoad() {
        logger.log(BasicLevel.DEBUG, "");
    }

    /**
     * A container invokes this method to instruct the instance to synchronize
     * its state by storing it to the underlying database.
     * This method always executes in the proper transaction context.
     *
     * @throws EJBException Thrown by the method to indicate a failure caused by
     * a system-level error.
     */
    public void ejbStore() {
        logger.log(BasicLevel.DEBUG, "");
    }

    /**
     * There must be an ejbPostCreate par ejbCreate method
     *
     * @throws CreateException Failure to create an entity EJB object.
     */
    public void ejbPostCreate(String id) throws CreateException {
        logger.log(BasicLevel.DEBUG, "id=" + id);
    }

    /**
     * A container invokes this method on an instance before the instance
     * becomes disassociated with a specific EJB object.
     */
    public void ejbPassivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    /**
     * A container invokes this method when the instance is taken out of
     * the pool of available instances to become associated with a specific
     * EJB object.
     */
    public void ejbActivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

}
