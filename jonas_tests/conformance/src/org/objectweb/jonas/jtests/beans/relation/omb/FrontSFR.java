/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.beans.relation.omb;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

import org.ow2.jonas.lib.util.Log;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;


/**
 * Frontal Session bean.
 */
public class FrontSFR implements SessionBean {

    static private Logger logger = null;
    SessionContext ejbContext;
    private AHomeLocal ahl = null;
    private BHomeLocal bhl = null;

    // ------------------------------------------------------------------
    // SessionBean implementation
    // ------------------------------------------------------------------


    public void setSessionContext(SessionContext ctx) {
        if (logger == null) {
            logger = Log.getLogger("org.objectweb.jonas_tests");
        }
        logger.log(BasicLevel.DEBUG, "");
        ejbContext = ctx;
        try {
            Context ictx = new InitialContext();
            ahl = (AHomeLocal) ictx.lookup("java:comp/env/ejb/a");
            bhl = (BHomeLocal) ictx.lookup("java:comp/env/ejb/b");
        } catch (NamingException e) {
            throw new EJBException("Impossible to fetch the ", e);
        }
    }
        

    public void ejbRemove() {
        logger.log(BasicLevel.DEBUG, "");
    }
        

    public void ejbCreate() throws CreateException {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbCreate(String pka, String pkb) throws CreateException {
        logger.log(BasicLevel.DEBUG, "");
        try {
            ALocal al = ahl.create(pka);
            BLocal bl = bhl.create(pkb);
            al.getB().add(bl);
            bhl.findByName(pkb, al.getId());
        } catch (Exception e) {
            throw new CreateException("ejbCreate failed " + e);
        }
    }

    public void ejbPassivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbActivate() {
        logger.log(BasicLevel.DEBUG, "");
    }
    
    // ------------------------------------------------------------------
    // Front implementation
    // ------------------------------------------------------------------

    /**
     * method1
     */
    public void method1() {
        logger.log(BasicLevel.DEBUG, "");
    }

    /**
     * method2
     */
    public void method2(java.lang.String s) {
        logger.log(BasicLevel.DEBUG, "");
    }
    
    /**
     * Ensure the javax.ejb.EJBException is thrown when trying
     * to invoke an accessor method on a deleted entitybean object
     */
    public void testRemove1() {
        logger.log(BasicLevel.DEBUG, "");
        ALocal ax9;
        try {
            ax9 = ahl.create("ax9");
            ax9.remove();    
        } catch (Exception e) {
            throw new EJBException("Initial state creation problem: " + e);
        }
        try {
            ax9.getId(); 
            throw new EJBException("entity was not deleted, expected EJBException");  
        } catch (EJBException e) {
            // Pass
        } catch (Exception e) {
            throw new EJBException("Caught unexpected exception: " + e);
        }
    }
 
    /**
     * Ensure the IllegalArgumentException is thrown when trying
     * to set a collection cmr-field to a wrong relationship type
     */
    public void testSetCmrWithDeleted() {
        logger.log(BasicLevel.DEBUG, "");
        BLocal b0;
        try {
            b0 = bhl.findByPrimaryKey("b0");
        } catch (Exception e) {
            throw new EJBException("Initial state creation problem: " + e);
        }
        try {
            b0.testSetCmrWithDeleted();
        } catch (Exception e) {
            throw new EJBException("Caugth unexpected exception: " + e);
        }
    }
    
    /**
     * Ensure the IllegalArgumentException is thrown when trying
     * to set a collection cmr-field to a wrong relationship type
     */
    public void testSetCmrWithWrongType() {
        logger.log(BasicLevel.DEBUG, "");
        ALocal a0;
        try {
            a0 = ahl.findByPrimaryKey("a0");
        } catch (FinderException e) {
            throw new EJBException("Initial state creation problem: " + e);
        }
        try {
            a0.getB().add(a0); 
        } catch (IllegalArgumentException e) {
            // Pass
            return;
        } catch (Exception e) {
            throw new EJBException("Caugth unexpected exception: " + e);
        }
        throw new EJBException("Expected IllegalArgumentException");  
    }
}

