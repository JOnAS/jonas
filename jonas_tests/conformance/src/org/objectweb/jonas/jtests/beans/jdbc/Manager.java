// Manager.java

package org.objectweb.jonas.jtests.beans.jdbc;

import java.rmi.RemoteException;
import javax.ejb.EJBObject;

/**
 * Manager remote interface
 */
public interface Manager extends EJBObject {
    public int openConnection() throws RemoteException;
    public boolean closeConnection(int nb) throws RemoteException;
    public boolean openCloseConnection() throws RemoteException;
    public boolean accessCloseConnection() throws RemoteException;

    /**
     * Store the value for the pk entry
     */
    public void storeValue(String pk, int val) throws RemoteException;

    /**
     * return the value stored for this pk entry
     */
    public int getValue(String pk) throws RemoteException;
}
