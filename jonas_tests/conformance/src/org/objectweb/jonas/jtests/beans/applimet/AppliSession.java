/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.beans.applimet;

import java.rmi.RemoteException;
import javax.ejb.CreateException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;
import org.ow2.jonas.lib.util.Log;
import org.objectweb.util.monolog.api.Logger;
import org.objectweb.util.monolog.api.BasicLevel;

/**
 */
public class AppliSession implements SessionBean {

    static protected Logger logger = null;
	private MetHome th;
	private Met tr;

    SessionContext ejbContext;

	public void ejbRemove() throws RemoteException {
        logger.log(BasicLevel.DEBUG, "");
		try {
			tr.remove();
		} catch (Exception e) {
            logger.log(BasicLevel.ERROR, "Cannot remove Met:" + e);
            throw new RemoteException("Cannot remove Met:" + e);
		}
	}

	public void ejbCreate() throws CreateException {
        logger.log(BasicLevel.DEBUG, "");
		try{
			Context ctx = new InitialContext();
			Object objref = ctx.lookup(MetHome.JNDI_NAME);
			th = (MetHome) PortableRemoteObject.narrow(objref, MetHome.class);
			tr = th.create();
		} catch(Exception e) {
            logger.log(BasicLevel.ERROR, "Cannot create Met:" + e);
            throw new CreateException("Cannot create Met:" + e);
		} 
	}

    public void methodeApplicative() throws RemoteException { 
        logger.log(BasicLevel.DEBUG, "");
        try {
            tr.methode1();
        } catch (Exception e) {
            logger.log(BasicLevel.ERROR, "Cannot call Met:" + e);
            throw new RemoteException("Cannot call Met:" + e);
        } 
    }

    public void noTxMethod() throws RemoteException { 
        logger.log(BasicLevel.DEBUG, "");
        try {
            ejbContext.getRollbackOnly();
            throw new RemoteException("Should get IllegalStateException");
        } catch (IllegalStateException e) {
        }
    }

    public void ejbActivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbPassivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void setSessionContext(javax.ejb.SessionContext ctx) {
        if (logger == null) {
            logger = Log.getLogger(Log.JONAS_TESTS_PREFIX);
        }
        logger.log(BasicLevel.DEBUG, "");
        ejbContext = ctx;
    }

    public void unsetSessionContext() {
        logger.log(BasicLevel.DEBUG, "");
    }
}
