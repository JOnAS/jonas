/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.beans.ejbql;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.InitialContext;

import org.ow2.jonas.lib.util.Log;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;


/**
 * Stateless Session useful to do some specifics tests which local interfaces are needed.
 * @author Helene Joanin
 */
public class SessionTestBean implements SessionBean {

    static protected Logger logger = null;
    protected SessionContext ctx = null;
    protected CustomerHomeLocal hCustomer = null;
    protected PhoneHomeLocal hPhone = null;

    // SessionBean methods implementation
    public void setSessionContext(SessionContext ctx) { 
        if (logger == null) {
            logger = Log.getLogger(Log.JONAS_TESTS_PREFIX);
        }
        logger.log(BasicLevel.DEBUG, "");
        this.ctx = ctx;
        try {
            InitialContext inctx = new InitialContext();
            hCustomer = (CustomerHomeLocal) inctx.lookup("java:comp/env/ejb/CustomerHomeLocal");
            hPhone = (PhoneHomeLocal) inctx.lookup("java:comp/env/ejb/PhoneHomeLocal");
        } catch (Exception e) {
            throw new javax.ejb.EJBException(e);
        }
    }

    public void ejbCreate() throws CreateException {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbRemove() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbPassivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbActivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    // SessionTestRemote implementation
    public Integer getCustomerWithPhone(String phoneNumber) throws EJBException {
        try {
            PhoneLocal phone = hPhone.findByNumber(phoneNumber);
            CustomerLocal customer = hCustomer.findCustomerWithPhone(phone);
            return customer.getId();
        } catch (Exception e) {
            logger.log(BasicLevel.ERROR, "Cannot get the customer with phone " + phoneNumber, e);
            throw new EJBException("Cannot get the customer with phone " + phoneNumber, e);
        }
    }
}

