/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

// BaseSL.java

package org.objectweb.jonas.jtests.beans.secured;

import javax.ejb.EJBContext;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

import org.ow2.jonas.lib.util.Log;
import org.objectweb.util.monolog.api.BasicLevel;

public class BaseSL extends BaseCommon implements SessionBean {

    protected SessionContext ejbContext;



    // ------------------------------------------------------------------
    // SessionBean implementation
    // ------------------------------------------------------------------

    public void setSessionContext(SessionContext ctx) {
        if( logger == null)
            logger = Log.getLogger(Log.JONAS_TESTS_PREFIX);
        logger.log(BasicLevel.DEBUG, "");
        ejbContext = ctx;
    }
	
    public void ejbRemove() {
        logger.log(BasicLevel.DEBUG, "");
    }
	
    public void ejbCreate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbPassivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbActivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    // ------------------------------------------------------------------
    // Base  implementation
    // ------------------------------------------------------------------

    public EJBContext   getEJBContext(){
        return ejbContext;
    }


}
