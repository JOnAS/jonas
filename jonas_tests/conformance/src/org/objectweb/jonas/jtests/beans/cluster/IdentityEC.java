package org.objectweb.jonas.jtests.beans.cluster;

import javax.ejb.CreateException;
import javax.ejb.DuplicateKeyException;
import javax.ejb.EntityBean;
import javax.ejb.EntityContext;
import javax.ejb.RemoveException;

import org.ow2.jonas.lib.util.Log;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

public class IdentityEC implements EntityBean {

    static private Logger logger = null;
    EntityContext ejbContext;

    // ------------------------------------------------------------------
    // State of the bean. 
    // They must be public for Container Managed Persistence.
    // ------------------------------------------------------------------
    public String name;
    public int number;

    // ------------------------------------------------------------------
    // EntityBean implementation
    // ------------------------------------------------------------------

    /**
     * Set the associated entity context. The container invokes this method 
     * on an instance after the instance has been created.
     * This method is called in an unspecified transaction context. 
     * 
     * @param ctx - An EntityContext interface for the instance. The instance
     * should store the reference to the context in an instance variable.
     * @throws EJBException Thrown by the method to indicate a failure caused by a
     * system-level error.
     */
    public void setEntityContext(EntityContext ctx) {
        if (logger == null) {
	    logger = Log.getLogger(Log.JONAS_TESTS_PREFIX);
	}
	logger.log(BasicLevel.DEBUG, "");
	ejbContext = ctx;
    }

    /**
     * Unset the associated entity context. The container calls this method
     * before removing the instance. 
     * This is the last method that the container invokes on the instance.
     * The Java garbage collector will eventually invoke the finalize() method
     * on the instance.
     * This method is called in an unspecified transaction context.
     * @throws EJBException Thrown by the method to indicate a failure caused by a
     * system-level error.
     */
    public void unsetEntityContext() {
	logger.log(BasicLevel.DEBUG, "");
	ejbContext = null;
    }

    /**
     * A container invokes this method before it removes the EJB object
     * that is currently associated with the instance. This method is
     * invoked when a client invokes a remove operation on the enterprise Bean's
     * home interface or the EJB object's remote interface. This method
     * transitions the instance from the ready state to the pool of available
     * instances. 
     *
     * This method is called in the transaction context of the remove operation.
     * @throws RemoveException  The enterprise Bean does not allow destruction of the object.
     * @throws EJBException - Thrown by the method to indicate a failure caused by a system-level
     * error.
     */
    public void ejbRemove() throws RemoveException {
	logger.log(BasicLevel.DEBUG, "="+number);
    }

    /**
     * A container invokes this method to instruct the instance to synchronize
     * its state by loading it state from the underlying database. 
     * This method always executes in the proper transaction context.
     *
     * @throws EJBException Thrown by the method to indicate a failure caused by
     * a system-level error.
     */
    public void ejbLoad() {
	logger.log(BasicLevel.DEBUG, "="+number);
    }

    /**
     * A container invokes this method to instruct the instance to synchronize
     * its state by storing it to the underlying database. 
     * This method always executes in the proper transaction context.
     *
     * @throws EJBException Thrown by the method to indicate a failure caused by
     * a system-level error.
     */
    public void ejbStore() {
	logger.log(BasicLevel.DEBUG, "="+number);
    }
	
    /**
     * The Entity bean can define 0 or more ejbCreate methods.
     *
     * @throws CreateException Failure to create an entity EJB object.
     * @throws DuplicateKeyException An object with the same key already exists. 
     */
    public String ejbCreate(String s, int i) throws CreateException, DuplicateKeyException {

	// Init here the bean fields
	name = s;
	number = i;
	logger.log(BasicLevel.DEBUG, "="+number);

	// In CMP, should return null.
	return null;
    }
	
    /**
     * There must be an ejbPostCreate par ejbCreate method
     *
     * @throws CreateException Failure to create an entity EJB object.
     */
    public void ejbPostCreate(String s, int i) throws CreateException {
	logger.log(BasicLevel.DEBUG, "="+number);
    }

    /**
     * A container invokes this method on an instance before the instance
     * becomes disassociated with a specific EJB object.
     */
    public void ejbPassivate() {
	logger.log(BasicLevel.DEBUG, "="+number);
    }

    /**
     * A container invokes this method when the instance is taken out of 
     * the pool of available instances to become associated with a specific 
     * EJB object.
     */
    public void ejbActivate() {
	logger.log(BasicLevel.DEBUG, "="+number);
    }
    
    // ------------------------------------------------------------------
    // Identity implementation
    // ------------------------------------------------------------------

    /**
     * getName
     */
    public String getName() {
	logger.log(BasicLevel.DEBUG, "="+number);
	return name;
    }

    /**
     * getNumber
     */
    public int getNumber() {
	logger.log(BasicLevel.DEBUG, "="+number);
	return number;
    }

    /**
     * setNumber
     */
    public void setNumber(int val) {
	number = val;
	logger.log(BasicLevel.DEBUG, "="+number);
    }

}
