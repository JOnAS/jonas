/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.beans.ebasic;

import org.ow2.jonas.lib.util.Log;
import org.objectweb.util.monolog.api.Logger;
import org.objectweb.util.monolog.api.BasicLevel;

/**
 * This is an entity bean with "container managed persistence version 2".
 * This bean is used to test an entity with auto generated primary key
 * @author Jerome Camilleri
 */
public  abstract class pkautoObjectEC2 implements javax.ejb.EntityBean {

    static protected Logger logger = null;
    javax.ejb.EntityContext ejbContext;

 
    public java.lang.Object ejbCreate(int f1, int f2) throws javax.ejb.CreateException {
        logger.log(BasicLevel.DEBUG, "");

        // Init here the bean fields
        setField1(f1);
        setField2(f2);
        return null;
    }

    public void ejbPostCreate(int f1, int f2) {
        logger.log(BasicLevel.DEBUG, "");
    }

    // ------------------------------------------------------------------
    // Abstract Accessor methods for Persistent fields
    // ------------------------------------------------------------------
    public abstract int getField1();
    public abstract void setField1(int f1);

    public abstract int getField2();
    public abstract void setField2(int f2);

    // ------------------------------------------------------------------
    // Standard call back methods
    // ------------------------------------------------------------------

    public void setEntityContext(javax.ejb.EntityContext ctx) {
        if (logger == null) {
            logger = Log.getLogger("org.objectweb.jonas_tests");
        }
        logger.log(BasicLevel.DEBUG, "");
        ejbContext = ctx;
    }

    public void unsetEntityContext() {
        logger.log(BasicLevel.DEBUG, "");
        ejbContext = null;
    }

    public void ejbRemove() throws javax.ejb.RemoveException {
        logger.log(BasicLevel.DEBUG, "");
    }
 
    public void ejbLoad() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbStore() {
        logger.log(BasicLevel.DEBUG, "");
    }
        
    public void ejbPassivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbActivate() {
        logger.log(BasicLevel.DEBUG, "");
    }    

}



