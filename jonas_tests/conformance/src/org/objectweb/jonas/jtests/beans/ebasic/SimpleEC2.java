/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.beans.ebasic;

import java.rmi.RemoteException;
import javax.ejb.CreateException;
import javax.ejb.EntityBean;
import javax.ejb.EntityContext;
import javax.ejb.RemoveException;

import org.ow2.jonas.lib.util.Log;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;


/**
 * This is an entity bean with "container managed persistence version 2.x".
 * The state of an instance is stored into a relational database.
 * The following table should exist :
 * ebasicSimpleEC2
 * c_testname	varchar(30) 	primarey key
 * c_info		integer
 * c_numtest	integer
 * @author Philippe Coq, Philippe Durieux, Helene Joanin
 */

public abstract class SimpleEC2 implements EntityBean {

    static protected Logger logger = null;

    protected EntityContext entityContext;


    // Get and Set accessor methods of the bean's abstract schema
    public abstract int getInfo();
    public abstract void setInfo(int info);

    public abstract int getNumTest();
    public abstract void setNumTest(int numtest);

    public abstract String getTestName();
    public abstract void setTestName(String testname);


    public void ejbActivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbPassivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbLoad() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbStore() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbRemove() throws RemoveException {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void setEntityContext(EntityContext ctx) {
        if (logger == null)
            logger = Log.getLogger(Log.JONAS_TESTS_PREFIX);
        logger.log(BasicLevel.DEBUG, "");
        entityContext  = ctx;

    }

    public void unsetEntityContext() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public String ejbCreate(String name, int info, int  num) throws CreateException {
        logger.log(BasicLevel.DEBUG, "create"+info+","+name+","+num);
        setInfo(info);
        setNumTest(num);
        setTestName(name);
        return(null);
    }

    public void ejbPostCreate(String name, int info, int  num) {
        logger.log(BasicLevel.DEBUG, "");
    }

    /**
     * loop back on the same bean instance.
     * This is forbidden when bean is non reentrant.
     * @return true if test passed.
     * @throws RemoteException
     */
    public boolean loopBack() throws RemoteException {
        logger.log(BasicLevel.DEBUG, "");
        boolean pass = false;
        Simple myref = (Simple) entityContext.getEJBObject();
        try {
            myref.getNumTest();
        } catch (RemoteException e) {
            logger.log(BasicLevel.DEBUG, "expected exception:" + e);
            pass = true;
        }
        return pass;
    }

    /**
     * loop back on the same bean instance.
     * This is forbidden when bean is non reentrant.
     * @return true if test passed.
     * @throws RemoteException
     */
    public boolean loopBackTx() throws RemoteException {
        logger.log(BasicLevel.DEBUG, "");
        boolean pass = false;
        Simple myref = (Simple) entityContext.getEJBObject();
        try {
            myref.getNumTest();
        } catch (RemoteException e) {
            logger.log(BasicLevel.DEBUG, "expected exception:" + e);
            pass = true;
        }
        return pass;
    }

    public void nullPointerException() throws RemoteException {
        Account acc = null;
        acc.hashCode();
    }

    /**
     * Check the set of allowed operations
     * See EJB 2.1 spec - 12.1.6
     */
    public void ejbHomeGlobalOpe() throws RemoteException {
        logger.log(BasicLevel.DEBUG, "");
        if (entityContext == null) {
            throw new RemoteException("null entityContext");
        }
        entityContext.getEJBHome();
        // This bean has no local interface
        try {
            entityContext.getEJBLocalHome();
        } catch (IllegalStateException e) {
        }
        entityContext.getCallerPrincipal();
        entityContext.getRollbackOnly();
        entityContext.isCallerInRole("role");
        entityContext.getTimerService();
    }
}
