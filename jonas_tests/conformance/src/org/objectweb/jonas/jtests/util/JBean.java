/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.ejb.EJBException;
import javax.ejb.Timer;
import javax.ejb.TimerHandle;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.UserTransaction;


/**
 * This class can be extended in every bean of the jonas test suite. It provide
 * some utilities to look transaction status, ... Most of them are not a clean
 * and standard way to do this, but it is OK for jonas tests.
 */
public class JBean {

	public UserTransaction getUserTransaction() {
		InitialContext ctx;
		UserTransaction ut = null;
		try {
			ctx = new InitialContext();
			ut = (UserTransaction) ctx.lookup("javax.transaction.UserTransaction");
		} catch (NamingException e) {
			throw new EJBException("Cannot get UserTransaction from the bean");
		}
	    return ut;
	}

    public TimerHandle getDeserializedHandle(TimerHandle handle) {
        TimerHandle newhandle = null;
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream os = new ObjectOutputStream(baos);
            os.writeObject(handle);
            byte[] b = baos.toByteArray();
            ByteArrayInputStream bais = new ByteArrayInputStream(b);
            ObjectInputStream is = new ObjectInputStream(bais);
            newhandle = (TimerHandle) is.readObject();
        } catch (Exception e) {
            throw new EJBException("Bad deserialized timer handle:" + e);
        }
        return newhandle;
    }

    public static boolean timersAreIdentical(TimerHandle th1, TimerHandle th2) {
        Timer timer1, timer2;

        try {
            timer1 = th1.getTimer();
            if (timer1 == null) {
                return false;
            }
            timer2 = th2.getTimer();
            if (timer2 == null) {
                return false;
            }
            return timer1.equals(timer2);
        } catch (Exception e) {
            throw new EJBException("Cannot compare 2 timers:" + e);
        }
    }

    /**
     * sleep n millisec.
     */
    public void sleep(int msec) {
        try {
            Thread.sleep(msec);
        } catch (InterruptedException e) {
            System.err.println("sleep interrupted");
        }
    }

}