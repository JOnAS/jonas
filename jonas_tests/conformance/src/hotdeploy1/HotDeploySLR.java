// HotDeploySLR.java
// Stateless Session bean

package org.objectweb.jonas.jtests.beans.hotdeploy;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.RemoveException;
import javax.ejb.EJBObject;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.ow2.jonas.lib.util.Log;
import org.objectweb.util.monolog.api.Logger;
import org.objectweb.util.monolog.api.BasicLevel;


/**
 *
 */
public class HotDeploySLR implements SessionBean {

    static private Logger logger = null;
    SessionContext ejbContext;
    private static final int VERSION_NUMBER = 1;

    // ------------------------------------------------------------------
    // SessionBean implementation
    // ------------------------------------------------------------------


    public void setSessionContext(SessionContext ctx) {
        if (logger == null) {
            logger = Log.getLogger("org.objectweb.jonas_tests");
        }
        logger.log(BasicLevel.DEBUG, "");
        ejbContext = ctx;
    }
        

    public void ejbRemove() {
        logger.log(BasicLevel.DEBUG, "");
    }
        

    public void ejbCreate() throws CreateException {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbPassivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbActivate() {
        logger.log(BasicLevel.DEBUG, "");
    }
    
    // ------------------------------------------------------------------
    // HotDeploy implementation
    // ------------------------------------------------------------------
    /**
     * method1
     */
    public String getEnvString() {
        logger.log(BasicLevel.DEBUG, "getEnvString");
        String value = "";
        try {
            InitialContext ictx = new InitialContext();
            value = (String) ictx.lookup("java:comp/env/name");
            // value should be the one defined in ejb-jar.xml
        } catch (NamingException e) {
            throw new EJBException(e);
        }
       
        return value;
    }

    public int getVersionNumber() {
        logger.log(BasicLevel.DEBUG, "getVersionNumber = " + VERSION_NUMBER);
        return VERSION_NUMBER;
    }

    public int getHelperClassVersionNumber() {
        logger.log(BasicLevel.DEBUG, "getHelperClassVersionNumber = " + Helper.VERSION_NUMBER);
        return Helper.VERSION_NUMBER;
    }

}

