// TimeBeanSLR.java
// Stateless Session bean

package org.objectweb.jonas.jtests.beans.time;

import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

import org.ow2.jonas.lib.util.Log;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 *
 */
public class TimeBeanSLR implements SessionBean {

    static private Logger logger = null;
    SessionContext ejbContext;

    // ------------------------------------------------------------------
    // SessionBean implementation
    // ------------------------------------------------------------------


    public void setSessionContext(SessionContext ctx) {
        if (logger == null) {
            logger = Log.getLogger("org.objectweb.jonas_tests");
        }
        logger.log(BasicLevel.DEBUG, "");
        ejbContext = ctx;
    }
        

    public void ejbRemove() {
        logger.log(BasicLevel.DEBUG, "");
    }
        

    public void ejbCreate() throws CreateException {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbPassivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbActivate() {
        logger.log(BasicLevel.DEBUG, "");
    }
    
    // ------------------------------------------------------------------
    // TimeBean implementation
    // ------------------------------------------------------------------

    public long getDateAsLong() {
        logger.log(BasicLevel.DEBUG, "");
        return new Date().getTime();
    }

    public Date getDate() {
        logger.log(BasicLevel.DEBUG, "");
        return new Date();
    }
    
    public boolean isHandlerInitRequestInvoked() {
        logger.log(BasicLevel.DEBUG, "");
        StaticPassValue spv = StaticPassValue.getInstance();
        String init = spv.getInit();
        String req = spv.getRequest();
        return (init != null && req != null);
    }
}

