/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.objectweb.jonas.jtests.beans.time;

import javax.xml.namespace.QName;
import javax.xml.rpc.handler.GenericHandler;
import javax.xml.rpc.handler.HandlerInfo;
import javax.xml.rpc.handler.MessageContext;

import org.ow2.jonas.lib.util.Log;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;


/**
 *
 *
 * @author Guillaume Sauthier
 */
public class TimeBeanHandler extends GenericHandler {

    private Logger logger = null;

    /**
     * The <code>init</code> method to enable the Handler instance to
     * initialize itself.
     * @param config handler configuration
     */
    public void init(HandlerInfo hinfo) {
        if (logger == null) {
            logger = Log.getLogger("org.objectweb.jonas_tests");
        }
        logger.log(BasicLevel.DEBUG, "");
        String val = (String) hinfo.getHandlerConfig().get("jonas.test.server.handler");
        StaticPassValue spv = StaticPassValue.getInstance();
        spv.setInitValue(val);
    }

    /**
     * The <code>destroy</code> method indicates the end of lifecycle
     * for a Handler instance.
     */
    public void destroy() {
        logger.log(BasicLevel.DEBUG, "");
    }

    /**
     * The <code>handleRequest</code> method processes the request
     * SOAP message. The default implementation of this method returns
     * <code>true</code>. This indicates that the handler chain
     * should continue processing of the request SOAP message.
     * @param context the message context
     * @return true/false
     */
    public boolean handleRequest(MessageContext mc) {
        logger.log(BasicLevel.DEBUG, "");
        StaticPassValue spv = StaticPassValue.getInstance();
        spv.setRequestValue("handlerRequest-Invoked");
        return true;
    }

    /**
     * The <code>handleResponse</code> method processes the response
     * message. The default implementation of this method returns
     * <code>true</code>. This indicates that the handler chain
     * should continue processing of the response SOAP message.
     * @param context the message context
     * @return true/false
     */
    public boolean handleResponse(MessageContext context) {
        logger.log(BasicLevel.DEBUG, "");
        return true;
    }

    /**
     * The <code>handleFault</code> method processes the SOAP faults
     * based on the SOAP message processing model. The default
     * implementation of this method returns <code>true</code>. This
     * indicates that the handler chain should continue processing
     * of the SOAP fault. 
     * @param context the message context
     * @return true/false
     */
    public boolean handleFault(MessageContext context) {
        logger.log(BasicLevel.DEBUG, "");
        return true;
    }

    /**
     * Gets the header blocks processed by this Handler instance.
     *
     * @return Array of QNames of header blocks processed by this handler instance.
     * <code>QName</code> is the qualified name of the outermost element of the Header block.
     */
    public QName[] getHeaders() {
        logger.log(BasicLevel.DEBUG, "");
        return new QName[0];
    }

}
