// WsClient1SLR.java
// Stateless Session bean

package org.objectweb.jonas.jtests.beans.wsclient;



import java.rmi.RemoteException;
import javax.ejb.CreateException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.xml.namespace.QName;
import javax.xml.rpc.Call;
import javax.xml.rpc.Service;
import javax.xml.rpc.ServiceException;
import org.ow2.jonas.lib.util.Log;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;


/**
 *
 */
public class WsClient1SLR implements SessionBean {

    static private Logger logger = null;
    SessionContext ejbContext;

    Service service;

    // ------------------------------------------------------------------
    // SessionBean implementation
    // ------------------------------------------------------------------


    public void setSessionContext(SessionContext ctx) {
        if (logger == null) {
            logger = Log.getLogger("org.objectweb.jonas_tests");
        }
        logger.log(BasicLevel.DEBUG, "");
        ejbContext = ctx;
    }
        

    public void ejbRemove() {
        logger.log(BasicLevel.DEBUG, "");
    }
        

    public void ejbCreate() throws CreateException {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbPassivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbActivate() {
        logger.log(BasicLevel.DEBUG, "");
    }
    
    // ------------------------------------------------------------------
    // WsClient1 implementation
    // ------------------------------------------------------------------

    public boolean lookupServiceJNDI() {
        logger.log(BasicLevel.DEBUG, "lookupServiceJNDI");
        service = null;
        try {
            InitialContext ic = new InitialContext();
            service = (Service) ic.lookup("ws/XMethodsService");
        } catch (NamingException ne) {
            logger.log(BasicLevel.ERROR, "looking ws/XMethodsService : " + ne.getMessage());
            return false;
        }
        return (service != null);
    }

    public boolean lookupServiceName() {
        logger.log(BasicLevel.DEBUG, "lookupServiceName");
        service = null;
        try {
            InitialContext ic = new InitialContext();
            service = (Service) ic.lookup("java:comp/env/service/XMethods");
        } catch (NamingException ne) {
            logger.log(BasicLevel.ERROR, "looking java:comp/env/service/XMethods : " + ne.getMessage());
            return false;
        }
        return (service != null);
    }

    public void getAllServiceNames() throws ServiceException, RemoteException {
        logger.log(BasicLevel.DEBUG, "getAllServiceNames");

        lookupServiceName();
        Call call = service.createCall(new QName("XMethodsQuerySoap"),
                                   new QName("getAllServiceNames"));
        call.invoke(new Object[] {});

        logger.log(BasicLevel.DEBUG, "Call is OK");
    }
}

