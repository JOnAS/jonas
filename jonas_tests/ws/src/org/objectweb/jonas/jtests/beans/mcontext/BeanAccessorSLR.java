// BeanAccessorSLR.java
// Stateless Session bean

package org.objectweb.jonas.jtests.beans.mcontext;

import javax.ejb.CreateException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.Context;
import javax.naming.InitialContext;

import org.ow2.jonas.lib.util.Log;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;


/**
 *
 */
public class BeanAccessorSLR implements SessionBean {

    static private Logger logger = null;
    SessionContext ejbContext;

    // ------------------------------------------------------------------
    // SessionBean implementation
    // ------------------------------------------------------------------


    public void setSessionContext(SessionContext ctx) {
        if (logger == null) {
            logger = Log.getLogger("org.objectweb.jonas_tests");
        }
        logger.log(BasicLevel.DEBUG, "");
        ejbContext = ctx;
    }
        

    public void ejbRemove() {
        logger.log(BasicLevel.DEBUG, "");
    }
        

    public void ejbCreate() throws CreateException {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbPassivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbActivate() {
        logger.log(BasicLevel.DEBUG, "");
    }
    
    // ------------------------------------------------------------------
    // BeanAccessor implementation
    // ------------------------------------------------------------------

    /**
     * method1
     */
    public boolean localBeanHasMessageContext() {
        logger.log(BasicLevel.DEBUG, "");
        try {
            Context ctx = new InitialContext();
            TestMC1LocalHome home = (TestMC1LocalHome) ctx.lookup("TestMC1Home_L");
            TestMC1Local mc1 = home.create();
            return mc1.localMethodHasMessageContext();
        } catch (Exception e) {
            logger.log(BasicLevel.WARN, "Something goes wrong");
        }
        return true;
    }

}

