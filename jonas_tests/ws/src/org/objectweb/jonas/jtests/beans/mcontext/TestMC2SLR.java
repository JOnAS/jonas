// TestMC2SLR.java
// Stateless Session bean

package org.objectweb.jonas.jtests.beans.mcontext;

import javax.ejb.CreateException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.xml.rpc.handler.MessageContext;

import org.ow2.jonas.lib.util.Log;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;


/**
 *
 */
public class TestMC2SLR implements SessionBean {

    static private Logger logger = null;
    SessionContext ejbContext;

    // ------------------------------------------------------------------
    // SessionBean implementation
    // ------------------------------------------------------------------


    public void setSessionContext(SessionContext ctx) {
        if (logger == null) {
            logger = Log.getLogger("org.objectweb.jonas_tests");
        }
        logger.log(BasicLevel.DEBUG, "");
        ejbContext = ctx;
    }
        

    public void ejbRemove() {
        logger.log(BasicLevel.DEBUG, "");
    }
        

    public void ejbCreate() throws CreateException {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbPassivate() {
        logger.log(BasicLevel.DEBUG, "");
    }

    public void ejbActivate() {
        logger.log(BasicLevel.DEBUG, "");
    }
    
    // ------------------------------------------------------------------
    // TestMC2 implementation
    // ------------------------------------------------------------------

    public boolean serviceEndpointMethodHasMessageContext() {
        logger.log(BasicLevel.DEBUG, "");
        return hasMessageContext();        
    }

    public boolean remoteMethodHasMessageContext() {
        logger.log(BasicLevel.DEBUG, "");
        return hasMessageContext();
    }

    private boolean hasMessageContext() {
        try {
            MessageContext mc = ejbContext.getMessageContext();
            if (mc != null) {
                return true;
            } else {
                return false;
            }
        } catch(IllegalStateException ise) {
            return false;
        }
    }
    
}

