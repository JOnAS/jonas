/**
 * HelloWsServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package org.objectweb.jonas.jtests.hello;

import javax.xml.namespace.QName;

public class HelloWsServiceLocator extends org.ow2.jonas.ws.axis.JService implements org.objectweb.jonas.jtests.hello.HelloWsService {

    public HelloWsServiceLocator(String name, QName qname) {

    }

    // Use to get a proxy class for HelloPortComponent
    private final java.lang.String HelloPortComponent_address = "http://dummy_location/HelloPortComponent";

    public java.lang.String getHelloPortComponentAddress() {
        return HelloPortComponent_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String HelloPortComponentWSDDServiceName = "HelloPortComponent";

    public java.lang.String getHelloPortComponentWSDDServiceName() {
        return HelloPortComponentWSDDServiceName;
    }

    public void setHelloPortComponentWSDDServiceName(java.lang.String name) {
        HelloPortComponentWSDDServiceName = name;
    }

    public org.objectweb.jonas.jtests.hello.HelloWs getHelloPortComponent() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(HelloPortComponent_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getHelloPortComponent(endpoint);
    }

    public org.objectweb.jonas.jtests.hello.HelloWs getHelloPortComponent(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            org.objectweb.jonas.jtests.hello.HelloPortComponentSoapBindingStub _stub = new org.objectweb.jonas.jtests.hello.HelloPortComponentSoapBindingStub(portAddress, this);
            _stub.setPortName(getHelloPortComponentWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (org.objectweb.jonas.jtests.hello.HelloWs.class.isAssignableFrom(serviceEndpointInterface)) {
                org.objectweb.jonas.jtests.hello.HelloPortComponentSoapBindingStub _stub = new org.objectweb.jonas.jtests.hello.HelloPortComponentSoapBindingStub(new java.net.URL(HelloPortComponent_address), this);
                _stub.setPortName(getHelloPortComponentWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        String inputPortName = portName.getLocalPart();
        if ("HelloPortComponent".equals(inputPortName)) {
            return getHelloPortComponent();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://hello.simple", "HelloWsService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("HelloPortComponent"));
        }
        return ports.iterator();
    }

}
