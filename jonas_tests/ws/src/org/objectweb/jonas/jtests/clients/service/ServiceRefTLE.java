package org.objectweb.jonas.jtests.clients.service;

import org.ow2.jonas.deployment.common.xml.TopLevelElement;
import org.ow2.jonas.deployment.common.xml.ServiceRef;
import org.ow2.jonas.deployment.common.xml.JonasServiceRef;

public class ServiceRefTLE implements TopLevelElement {

    private ServiceRef sr = null;
    private JonasServiceRef jsr = null;

    public void addServiceRef(ServiceRef sr) {
        this.sr = sr;
    }

    public void addJonasServiceRef(JonasServiceRef jsr) {
        this.jsr = jsr;
    }

    public ServiceRef getServiceRef() {
        return sr;
    }

    public JonasServiceRef getJonasServiceRef() {
        return jsr;
    }

}
