/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.clients.wsgen;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

import org.objectweb.jonas.jtests.util.JWebServicesTestCase;

public abstract class A_WsGen extends JWebServicesTestCase {
    private String jonasbase = null;
    private String resources = null;
    private String basedir   = null;

    public A_WsGen(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();

        jonasbase = System.getProperty("jonas.base.client");
        resources = System.getProperty("ws.resources");
        basedir   = System.getProperty("basedir");

    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    protected String getJonasBaseFile(String path) {
        return jonasbase + File.separator + path;
    }

    protected String getResourceFile(String path) {
        return resources + File.separator + path;
    }

    protected String getTestFile(String path) {
        return basedir + File.separator + path;
    }

    protected static boolean deleteDirectory(String dir) throws IOException {
        File file = new File(dir);
        boolean isDestroy = true;
        if (file.isDirectory()) {
            // destroy all childs
            File[] childs = file.listFiles();
            for (int i = 0; i < childs.length; i++) {
                isDestroy &= deleteDirectory(childs[i].getAbsolutePath());
            }
        }
        return isDestroy && file.delete();

    }

    protected static String unpackJar(JarFile file) throws IOException {
        String tmp = createTempDir();

        for (Enumeration e = file.entries(); e.hasMoreElements();) {
            ZipEntry ze = (ZipEntry) e.nextElement();
            
            // If is not a directory
            if (!ze.getName().endsWith("/")) {
                String newFilename = tmp + File.separator + ze.getName();
                File entryFile = new File(newFilename);

                entryFile.getParentFile().mkdirs();
                FileOutputStream fos = new FileOutputStream(entryFile);
                InputStream is = file.getInputStream(ze);

                int n = 0;
                byte[] buffer = new byte[1024];

                while ((n = is.read(buffer)) > 0) {
                    fos.write(buffer, 0, n);
                }

                fos.close();
                is.close();
            }
        }

        return tmp;
    }

    protected static String createTempDir() throws IOException {
        File tmpDir = File.createTempFile("wsgen-tests", null, null);
        tmpDir.delete();
        tmpDir.mkdir();
        return tmpDir.getAbsolutePath();
    }

}
