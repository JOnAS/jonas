/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.jonas.jtests.clients.wsgen;

import java.io.File;
import java.util.Enumeration;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

import junit.framework.Test;
import junit.framework.TestSuite;


/**
 * test case for WsGen
 */
public class F_WsGen extends A_WsGen {

    public F_WsGen(String name) {
        super(name);
    }

    public static Test suite() {
        return new TestSuite(F_WsGen.class);
    }

    public void setUp() throws Exception {
        super.setUp();
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    /*
     * Tests :
     * - webapps (endpoint, clients, both, none) (various Schemas, DTD)
     * - ejbjars (endpoint, clients, both, none) (various Schemas, DTD)
     * - apps (combine above examples) + ()
     */

    public void testWebappEndpointAlone() throws Exception {

        // Check entries in JarFile
        String [] entries = new String[] { "META-INF/MANIFEST.MF",
                                           "WEB-INF/wsdl/AddressBookPort.wsdl",
                                           "WEB-INF/wsdl/AddressBook.xsd",
                                           "WEB-INF/web.xml",
                                           "WEB-INF/webservices.xml",
                                           "WEB-INF/jonas-webservices.xml",
                                           "WEB-INF/mapping.xml",
                                           "WEB-INF/classes/org/objectweb/jonas/jtests/servlets/endpoint/Address.class",
                                           "WEB-INF/classes/org/objectweb/jonas/jtests/servlets/endpoint/AddressBook.class",
                                           "WEB-INF/classes/org/objectweb/jonas/jtests/servlets/endpoint/AddressBookImpl.class",
                                           "WEB-INF/classes/org/objectweb/jonas/jtests/servlets/endpoint/AddressBookException.class"};

        JarFile alone = new JarFile(getJonasBaseFile("webapps" + File.separator + "webendpoint.war"));

        checkEntries(entries, alone);

        assertEquals("entries number doesn't match", entries.length, countEntries(alone));

    }

    public void testWsClientEjb() throws Exception {

        // Check entries in JarFile
        String path = "org/objectweb/jonas/jtests/beans/wsclient/";
        String [] entries = new String[] { "META-INF/MANIFEST.MF",
                                           "META-INF/jonas-ejb-jar.xml",
                                           "META-INF/ejb-jar.xml",
                                           "META-INF/wsdl/query.wsdl"};

        JarFile wsclient = new JarFile(getJonasBaseFile("ejbjars" + File.separator + "wsclient.jar"));

        checkEntries(entries, wsclient);

    }

    public void testTimeEndpoint() throws Exception {
        // Check entries in JarFile (Wrapping Application)
        String [] entries = new String[] { "META-INF/application.xml",
                                           "META-INF/MANIFEST.MF",
                                           "time.war",
                                           "time.jar",
                                           "time-client.jar"};

        JarFile time = new JarFile(getJonasBaseFile("apps" + File.separator + "time-test.ear"));

        checkEntries(entries, time);

        assertEquals("entries number doesn't match", 5, countEntries(time));

        // Check entries in JarFile (Wrapped WebApp)
        String tmp = unpackJar(time);

        // Check war correctly created
        JarFile web = new JarFile(tmp + File.separator
                                  + "time.war");
        String [] webEntries = new String[] { "META-INF/MANIFEST.MF",
                                              "WEB-INF/web.xml",
                                              "WEB-INF/web-jetty.xml",
                                              "META-INF/context.xml",
                                              "WEB-INF/sources/deploy-server-0.wsdd",
                                              "WEB-INF/deploy-server-0.wsdd"};

        // TODO should improve entries check : deploy-server-0 may be deploy-server-25 !!
        //checkEntries(webEntries, web);
        assertEquals("web entries number doesn't match", webEntries.length, countEntries(web));

        deleteDirectory(tmp);

    }

    public void checkEntries(String [] entries, JarFile file) {

        for (int i = 0; i < entries.length; i++) {
            ZipEntry ze = file.getEntry(entries[i]);
            assertNotNull("missing entry '" + entries[i] + "' in '" + file.getName() + "'", ze);
        }
    }

    public int countEntries(JarFile file) {
        Enumeration e = file.entries();
        int count = 0;
        while (e.hasMoreElements()) {
            ZipEntry ze = (ZipEntry) e.nextElement();
            // not a directory
            if (!ze.getName().endsWith("/")) {
                count++;
            }

        }
        return count;
    }


    public static void main (String args[]) {
        String testtorun = null;
        // Get args
        for (int argn = 0; argn < args.length; argn++) {
            String s_arg = args[argn];
            if (s_arg.equals("-n")) {
                testtorun = args[++argn];
            }
        }
        if (testtorun == null) {
            junit.textui.TestRunner.run(suite());
        } else {
            junit.textui.TestRunner.run(new F_WsGen(testtorun));
        }
    }

}
