/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.objectweb.jonas.jtests.clients.endpoint;

import java.io.File;
import java.util.Calendar;

import javax.wsdl.Definition;
import javax.wsdl.Import;
import javax.wsdl.factory.WSDLFactory;
import javax.wsdl.xml.WSDLReader;
import javax.xml.namespace.QName;
import javax.xml.rpc.Call;
import javax.xml.rpc.Service;
import javax.xml.rpc.ServiceFactory;

import junit.framework.Test;
import junit.framework.TestSuite;

import org.ow2.jonas.lib.util.Cmd;

import org.objectweb.jonas.jtests.util.JWebServicesTestCase;

/**
 * @author Guillaume Sauthier
 */
public class F_TimeEndpoint extends JWebServicesTestCase {

    private static final String TIMEPORT_URL = "/time/TimePort/TimePort";
    
    /**
     * @param s
     */
    public F_TimeEndpoint(String s) {
        super(s);
    }

    public static Test suite() {
        return new TestSuite(F_TimeEndpoint.class);
    }

    public void setUp() throws Exception {
        super.setUp();

        useEar("time-test");
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    public void testTimeEndpoint() throws Exception {
        String port = System.getProperty("http.port");

        Service service = ServiceFactory.newInstance().createService(new QName("jonas:Time", "TimeBeanService"));
        Call call = service.createCall(new QName("TimePort"), new QName("getDate"));
        call.setTargetEndpointAddress("http://localhost:" + port + TIMEPORT_URL);
        Calendar cal = (Calendar) call.invoke(new Object[] {});

        assertNotNull("ServiceEndpoint performed succesfully", cal);
    }

    public void testTimeEndpointServerHandler() throws Exception {
        String port = System.getProperty("http.port");

        Service service = ServiceFactory.newInstance().createService(new QName("jonas:Time", "TimeBeanService"));
        Call call = service.createCall(new QName("TimePort"), new QName("isHandlerInitRequestInvoked"));
        call.setTargetEndpointAddress("http://localhost:" + port + TIMEPORT_URL);
        Boolean b = (Boolean) call.invoke(new Object[] {});

        assertTrue("Handler were not invoked successfully", b.booleanValue());
    }

    public void testTimeEndpointFromAppClient() throws Exception {

        String javaHomeBin = System.getProperty("java.home") + File.separator + "bin" + File.separator;
        String jonasRoot = System.getProperty("jonas.root");
        String jonasBase = System.getProperty("jonas.base.client");

        Cmd cmd = new Cmd(javaHomeBin + "java");
        
        cmd.addArgument("-Djava.endorsed.dirs=" + jonasRoot + File.separator + "lib" + File.separator + "endorsed");
        
        // classpath
        cmd.addArgument("-classpath");
        
        String jonasBaseConf = jonasBase + File.separator + "conf";
        String clientJarPath = jonasBase + File.separator + "conf" + File.pathSeparator + jonasRoot + File.separator + "lib"
                + File.separator + "client.jar";
        String jonasClientJarPath = jonasRoot + File.separator + "lib" + File.separator + "jonas-client.jar";
        cmd.addArgument(jonasBaseConf + File.pathSeparator + clientJarPath + File.pathSeparator + jonasClientJarPath);
        cmd.addArgument("org.ow2.jonas.client.ClientContainer");
        // ear
        cmd.addArgument(jonasBase + File.separator + "apps" + File.separator + "time-test.ear");

        if (!cmd.run()) {
            fail("Client fail see output for informations");
        }
    }


    public void testTimeEndpointURLPublication() throws Exception {
        String port = System.getProperty("http.port");

        String url = "http://localhost:" + port + TIMEPORT_URL + "?JWSDL";
        WSDLFactory factory = WSDLFactory.newInstance();
        WSDLReader reader = factory.newWSDLReader();
        reader.setFeature("javax.wsdl.importDocuments", true);
        Definition def = reader.readWSDL(url);
        
        Import imp = (Import) def.getImports("jonas:Time").get(0);
        assertEquals("wsdl:import[@location] not updated !", url + "&filename=Time.wsdl&context=.", imp.getLocationURI());
    }

}