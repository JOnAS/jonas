<?xml version="1.0"?>

<!--
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 - JOnAS: Java(TM) Open Application Server
 - Copyright (C) 1999 Bull S.A.
 - Contact: jonas-team@objectweb.org
 -
 - This library is free software; you can redistribute it and/or
 - modify it under the terms of the GNU Lesser General Public
 - License as published by the Free Software Foundation; either
 - version 2.1 of the License, or any later version.
 -
 - This library is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 - Lesser General Public License for more details.
 -
 - You should have received a copy of the GNU Lesser General Public
 - License along with this library; if not, write to the Free Software
 - Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 - USA
 -
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 - $Id$
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 -->

<!--
 - Initial developer(s): Bob Kruse
 -->

<!DOCTYPE ant SYSTEM "../build.dtd">

<project name="JOnAS JCA 1.0 and 1.5 resource adapter tests"
         default="install" basedir=".">

  <!-- Set the customizable properties -->
  <property file="../build.properties" />

  <!-- Get environment variables -->
  <property environment="myenv"/>
  <property name="jonas.root" value="${myenv.JONAS_ROOT}" />

  <!-- Set the uncustomizable properties -->
  <property name="classes.dir" value="classes" />
  <property name="lib.dir" value="${jonas.root}/lib" />

  <property name="RA_jar.dir" value="${basedir}/jar" />
  <property name="rar.dir" value="${basedir}/rars" />
  <!-- used for JCA 1.0 tests -->
  <property name="jtestRA"   value="JtestResourceAdapter" />
  <property name="noLogName" value="Nolog" />
  <property name="reName"    value="Reauth" />
  <property name="secName"   value="Security" />
  <property name="locName"   value="Transaction" />
  <property name="xaName"    value="XATransaction" />
  <!-- used for JCA 1.5 tests -->
  <property name="erName"      value="ErsatzEIS" />
  <property name="ernoName"    value="ErsatzNolog" />
  <property name="ersecName"   value="ErsatzSecurity" />
  <property name="erlocName"   value="ErsatzLoTransaction" />
  <property name="erxaName"    value="ErsatzXATransaction" />

  <!-- base.classpath -->
  <path id="base.classpath">
    <pathelement location="${classes.dir}" />
    <pathelement location="${RA_jar.dir}/JtestResourceAdapter.jar" />
    <pathelement location="${RA_jar.dir}/ersatz.jar" />
    <fileset dir="${lib.dir}">
      <include name="client.jar"/>
    </fileset>
  </path>


  <target name="init">
    <mkdir dir="${classes.dir}" />
    <mkdir dir="${RA_jar.dir}" />
    <condition property="jonas.base" value="${myenv.JONAS_BASE}">
    <available file="${myenv.JONAS_BASE}"/>
    </condition>
    <condition property="jonas.base" value="${jonas.root}">
    <available file="${jonas.root}"/>
    </condition>
    <property name="deploy.dir" value="${jonas.base}/deploy" />
    <property name="autoload.dir" value="${jonas.base}/rars/" />
    <mkdir dir="${autoload.dir}" />

  </target>


  <target name="dtds">
    <copy todir="src/fictional/resourceadapter">
      <fileset dir="${jonas.root}/xml" includes="*connector*.dtd" />
    </copy>
  </target>


  <target name="clean"
      description="Removes all the generated files or directories">
    <delete dir="${classes.dir}" />
    <delete dir="${RA_jar.dir}" />
  </target>


  <target name="compile"
      description="Compile Resource Adapters"
      depends="init" >
    <javac srcdir="src"
       destdir="${classes.dir}"
       debug="${opt.javac.debug}"
       optimize="${opt.javac.optimize}">
      <classpath refid="base.classpath" />
    </javac>
  </target>

  <target name="jar" depends="compile">

    <jar jarfile="${RA_jar.dir}/${jtestRA}.jar">
      <fileset dir="${classes.dir}"
               includes="**/fictional/**"/>
    </jar>
    <jar jarfile="${RA_jar.dir}/ersatz.jar">
      <fileset dir="${classes.dir}"
               includes="**/ersatz/**"/>
    </jar>

  </target>

  <!--
       ===================================================================
          build  all rars Target directory= %JONAS_BASE%\rars

          JtestResourceAdapter.rar         ErsatzEIS.rar
          Nolog.rar                        ErsatzNolog.rar
          Reauth.rar                       ErsatzSecurity.rar
          Security.rar                     ErsatzLoTransaction.rar
          Transaction.rar                  ErsatzXATransaction.rar
          XATransaction.rar
       ===================================================================
  -->
  <target name="rar" depends="jar">

    <antcall target="buildrar10">
      <param name="rarName" value="${jtestRA}"/>
    </antcall>
    <antcall target="buildrar10">
      <param name="rarName" value="${noLogName}"/>
    </antcall>
    <antcall target="buildrar10">
      <param name="rarName" value="${reName}"/>
    </antcall>
    <antcall target="buildrar10">
      <param name="rarName" value="${secName}"/>
    </antcall>
    <antcall target="buildrar10">
      <param name="rarName" value="${locName}"/>
    </antcall>
    <antcall target="buildrar10">
      <param name="rarName" value="${xaName}"/>
    </antcall>
    <antcall target="buildrar15">
      <param name="rarName" value="${erName}"/>
    </antcall>
    <antcall target="buildrar15">
      <param name="rarName" value="${ernoName}"/>
    </antcall>
    <antcall target="buildrar15">
      <param name="rarName" value="${ersecName}"/>
    </antcall>
    <antcall target="buildrar15">
      <param name="rarName" value="${erlocName}"/>
    </antcall>
    <antcall target="buildrar15">
      <param name="rarName" value="${erxaName}"/>
    </antcall>
  </target>

  <target name="buildrar10">
    <jar jarfile="${autoload.dir}/${rarName}.rar">
      <fileset dir      = "${rar.dir}/${rarName}"
               includes = "META-INF/*.xml"/>
      <fileset dir      = "${RA_jar.dir}"
               includes = "JtestResourceAdapter.jar"/>
    </jar>
    <copy file="${autoload.dir}/${rarName}.rar" tofile="${deploy.dir}/${rarName}.rar"/>
  </target>

  <target name="buildrar15">
    <jar jarfile="${autoload.dir}/${rarName}.rar">
      <fileset dir      = "${rar.dir}/${rarName}"
               includes = "META-INF/*.xml"/>
      <fileset dir      = "${RA_jar.dir}"
               includes = "ersatz.jar"/>
    </jar>
    <copy file="${autoload.dir}/${rarName}.rar" tofile="${deploy.dir}/${rarName}.rar"/>
  </target>

  <target name="install"
      description="Build the jars and install the rars"
      depends="rar" >
    <echo message="using jonas.base as  ${jonas.base}" />
  </target>

</project>
