/*
 * Created on Jul 10, 2003
 *
 * JtestInteraction.java is used to test the J2EE Connector
 * as implemented by JOnAS. This class implements the Interaction and InteractionSpec 
 * (cci) classes.
 *  
 */
package fictional.resourceadapter;

import javax.resource.ResourceException;
import javax.resource.NotSupportedException;
import javax.resource.cci.*;
import javax.resource.spi.*;
//  logger imports
import org.ow2.jonas.lib.util.Log;
import org.objectweb.util.monolog.api.Logger;
import org.objectweb.util.monolog.api.BasicLevel;

/**
 * @author Bob Kruse
 *
 * Jtest Resource Adapter
 *
 * used to test the J2EE Connector as implemented by JOnAS.
 * 
 */
public class JtestInteraction 
        implements Interaction,
        InteractionSpec,
        java.io.Serializable
{

    private Record output;
    private boolean closed = false;
    private Connection con;
    private ManagedConnection mcon;
    private Logger logger = null;
    String cName = "";
    
    public JtestInteraction(Connection c) {
        cName = "Interaction";
        if (logger == null) {
            logger = Log.getLogger("fictional.resourceadapter");
        }
        logger.log(BasicLevel.DEBUG, cName+".constructor");
        con=c;
    }
    public JtestInteraction(Connection c, ManagedConnection mc) {
        cName = "Interaction";
        if (logger == null) {
            logger = Log.getLogger("fictional.resourceadapter");
        }
        logger.log(BasicLevel.DEBUG, cName+".constructor");
        con=c;
        mcon=mc;
    }
    private String impl(Object obj) {
        if (obj instanceof Interaction) {
            return "Interaction";
        } else if (obj instanceof InteractionSpec) {
            return "InteractionSpec";
        } else
            return "InteractionSpec. Is this an error";
    }       
    /****************************
     *    Interaction  methods
     ****************************/
    public boolean execute(InteractionSpec ispec,
        Record input, 
        Record output) throws ResourceException
    {
        cName = "Interaction";
        logger.log(BasicLevel.DEBUG, cName+".execute");
        this.output = input;
        return(true);        
    }
    public Record execute(InteractionSpec ispec,
        Record input) throws ResourceException
    {
        cName = "Interaction";
        logger.log(BasicLevel.DEBUG, cName+".execute");
        return (output);
    }
    public void close()
               throws ResourceException
    {
        cName = "Interaction";
        logger.log(BasicLevel.DEBUG, cName+".close");
        closed=true;
    }
    public Connection getConnection()
    {
        cName = "Interaction";
        logger.log(BasicLevel.DEBUG, cName+".getConnection");
        return con;
    }
    public ResourceWarning getWarnings()
                               throws ResourceException           
    {
        cName = "Interaction";
        logger.log(BasicLevel.DEBUG, cName+".getWarnings");
        NotSupportedException nse = new NotSupportedException(
                  "Interaction.getWarnings is not currently supported");
        throw nse;
    }
    public void clearWarnings()
                       throws ResourceException
    {
        cName = "Interaction";
        logger.log(BasicLevel.DEBUG, cName+".clearWarnings");
        NotSupportedException nse = new NotSupportedException(
                       "Interaction.clearWarnings is not currently supported");
        throw nse;
    }


    /****************************
     *  InteractionSpec  methods
     ****************************/
    // standard properties
    private int interactionVerb;
        //   SYNC_SEND          =0      These interactionVerb values are from 
        //   SYNC_SEND_RECEIVE  =1      Constant Field Values of J2EE API
        //   SYNC_RECEIVE       =2

    private String FunctionName;
    private int ExecutionTimeout;
    private int FetchSize;
    private int FetchDirection; 
    private int MaxFieldSize; 
    private int ResultSetType; 
    private int ResultSetConcurrency;
    
    public JtestInteraction() {
        cName = "InteractionSpec";
        if (logger == null) {
            logger = Log.getLogger("fictional.resourceadapter");
        }
        logger.log(BasicLevel.DEBUG, cName+".constructor");
        interactionVerb = SYNC_SEND_RECEIVE;
        FunctionName = "";
        FetchDirection = 0; 
        ExecutionTimeout = 2000;
        FetchSize = 30;
        MaxFieldSize = 444; 
        ResultSetType = 5; 
        ResultSetConcurrency = 6;
    }
    public void setFunctionName(String n) {
       FunctionName = n;
    }
    public String getFunctionName() {
       return (FunctionName);
    }
    public void setInteractionVerb(int verb) {
       interactionVerb = verb;
    }
    public int getInteractionVerb() {
       return (interactionVerb);
    }
    public void setExecutionTimeout(int verb) {
       ExecutionTimeout = verb;
    }
    public int getExecutionTimeout() {
       return (ExecutionTimeout);
    }

    public void setFetchSize (int x) {
        FetchSize = x;;
    }
    public int getFetchSize () {
        return (FetchSize);
    }
     
    public void setFetchDirection(int x) {
        FetchDirection = x;;
    }
    public int getFetchDirection() {
        return (FetchDirection);
    }
     
    public void setMaxFieldSize(int x) {
        MaxFieldSize = x;;
    }
    public int getMaxFieldSize() {
        return (MaxFieldSize);
    }
     
    public void setResultSetType(int x) {
        ResultSetType = x;
    }
    public int getResultSetType() {
        return (ResultSetType);
    } 
    
    public void setResultSetConcurrency(int x) {
        ResultSetConcurrency = x;;
    }
    public int getResultSetConcurrency() {
        return (ResultSetConcurrency);
    } 
}
