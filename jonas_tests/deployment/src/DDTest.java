/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.ow2.jonas.deployment.client.tests.ClientDeploymentTest;
import org.ow2.jonas.deployment.client.tests.JonasClientDeploymentTest;
import org.ow2.jonas.deployment.client.xml.ApplicationClient;
import org.ow2.jonas.deployment.client.xml.JonasClient;
import org.ow2.jonas.deployment.common.xml.Element;
import org.ow2.jonas.deployment.ear.tests.EarDeploymentTest;
import org.ow2.jonas.deployment.ear.xml.Application;
import org.ow2.jonas.deployment.ejb.tests.EJBDeploymentTest;
import org.ow2.jonas.deployment.ejb.tests.JonasEJBDeploymentTest;
import org.ow2.jonas.deployment.ejb.xml.EjbJar;
import org.ow2.jonas.deployment.ejb.xml.JonasEjbJar;
import org.ow2.jonas.deployment.rar.tests.JonasRarDeploymentTest;
import org.ow2.jonas.deployment.rar.tests.RarDeploymentTest;
import org.ow2.jonas.deployment.rar.xml.Connector;
import org.ow2.jonas.deployment.rar.xml.JonasConnector;
import org.ow2.jonas.deployment.tests.AbsDeploymentTest;
import org.ow2.jonas.deployment.web.tests.JonasWebDeploymentTest;
import org.ow2.jonas.deployment.web.tests.WebDeploymentTest;
import org.ow2.jonas.deployment.web.xml.JonasWebApp;
import org.ow2.jonas.deployment.web.xml.WebApp;
import org.ow2.jonas.deployment.ws.tests.JonasWsDeploymentTest;
import org.ow2.jonas.deployment.ws.tests.WsDeploymentTest;
import org.ow2.jonas.deployment.ws.xml.JavaWsdlMapping;
import org.ow2.jonas.deployment.ws.xml.JonasWebservices;
import org.ow2.jonas.deployment.ws.xml.Webservices;


/**
 * Defines a class for testing
 * @author Florent Benoit
 * @author Philippe Coq
 * @author Helene Joanin
 */

public class DDTest {

    /**
     * WebApp tests
     */
    private static WebDeploymentTest webDeploymentTest = null;
    private static JonasWebDeploymentTest jonasWebDeploymentTest = null;

    /**
     * Connector tests
     */
    private static RarDeploymentTest rarDeploymentTest = null;
    private static JonasRarDeploymentTest jonasRarDeploymentTest = null;
    /**
     * Application Client tests
     */
    private static ClientDeploymentTest clientDeploymentTest = null;
    private static JonasClientDeploymentTest jonasClientDeploymentTest = null;
    /**
     * EJB tests
     */
    private static EJBDeploymentTest ejbDeploymentTest = null;
    private static JonasEJBDeploymentTest jonasEjbDeploymentTest = null;
    /**
     * Application tests
     */
    private static EarDeploymentTest earDeploymentTest = null;

    /**
     * Web Services tests
     */
    private static WsDeploymentTest wsDeploymentTest = null;
    private static JonasWsDeploymentTest jonasWsDeploymentTest = null;

    private DDTest() {
        webDeploymentTest = new WebDeploymentTest();
        jonasWebDeploymentTest = new JonasWebDeploymentTest();
        rarDeploymentTest = new RarDeploymentTest();
        jonasRarDeploymentTest = new JonasRarDeploymentTest();
        clientDeploymentTest = new ClientDeploymentTest();
        jonasClientDeploymentTest = new JonasClientDeploymentTest();
        ejbDeploymentTest = new EJBDeploymentTest();
        jonasEjbDeploymentTest = new JonasEJBDeploymentTest();
        earDeploymentTest = new EarDeploymentTest();
        wsDeploymentTest = new WsDeploymentTest();
        jonasWsDeploymentTest = new JonasWsDeploymentTest();

    }

    public static void main(String[] args) throws Exception {
        DDTest ddTest = new DDTest();
        try {
            ddTest.interactUser();
        }catch (Exception e) {
            System.out.println("Error : '" + e.getMessage() + "'");
            e.printStackTrace();
            System.exit(2);
        }
    }

    private void choice() {
        System.out.println("d for Displaying an XML element");
        System.out.println("t for Testing an XML element");
        System.out.println("s for Stressing an XML element");
        System.out.println("p for Parsing check");
        System.out.println("f for Parsing from an XML file");
        System.out.println("q for Quit");
    }

    private void help() {
        System.out.println("+--------------------------------+");
        System.out.println("|                                |");
        System.out.println("+--------------------------------+");

    }

    private void interactUser() throws Exception {
        // User interface
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            choice();
            System.out.print("> ");
            String readLine = in.readLine();
            if (readLine.length() == 0) {
                continue;
            }
            char c = readLine.charAt(0);

            switch (c) {
            case 'd':
                System.out.println("1 - WebApp element");
                System.out.println("2 - JonasWebApp element");
                System.out.println("3 - Connector element");
                System.out.println("4 - JonasConnector element");
                System.out.println("5 - ClientApp element");
                System.out.println("6 - JonasClient element");
                System.out.println("7 - Application element");
                System.out.println("8 - EjbJar element");
                System.out.println("9 - JonasEjbJar element");
                System.out.println("a - WebServices element");
                System.out.println("b - JonasWebServices element");
                System.out.println("c - JavaWsdlMapping element");

                System.out.println("0 - Customize element");

                System.out.print("> ");
                readLine = in.readLine();
                if (readLine.length() == 0) {
                    continue;
                }
                c = readLine.charAt(0);
                Element element = null;
                switch (c) {
                case '1' :
                    element = new WebApp();
                    break;
                case '2' :
                    element = new JonasWebApp();
                    break;
                case '3' :
                    element = new Connector();
                    break;
                case '4' :
                    element = new JonasConnector();
                    break;
                case '5' :
                    element = new ApplicationClient();
                    break;
                case '6' :
                    element = new JonasClient();
                    break;
                case '7' :
                    element = new Application();
                    break;
                case '8' :
                    element = new EjbJar();
                    break;
                case '9' :
                    element = new JonasEjbJar();
                    break;
                case 'a' :
                    element = new Webservices();
                    break;
                case 'b' :
                    element = new JonasWebservices();
                    break;
                case 'c' :
                    element = new JavaWsdlMapping();
                    break;

                case '0' :
                    System.out.print("Enter the name of the Element > ");
                    String className = in.readLine();
                    try {
                        Class cl = Class.forName(className);
                        element = (Element) cl.newInstance();
                    } catch (Exception e) {
                        System.out.println("className is not a good element " + e);
                        continue;
                    }
                    break;

                default :
                    continue;
                }

                wsDeploymentTest.fill(element, false);
                System.out.println("");
                System.out.println(element);
                break;

            case 't':
                System.out.println("1 - WebApp element");
                System.out.println("2 - JonasWebApp element");
                System.out.println("3 - Connector element");
                System.out.println("4 - JonasConnector element");
                System.out.println("5 - ClientApp element");
                System.out.println("6 - JonasClient element");
                System.out.println("7 - Application element");
                System.out.println("8 - EjbJar element");
                System.out.println("9 - JonasEjbJar element");
                System.out.println("a - WebServices element");
                System.out.println("b - JonasWebServices element");
                System.out.println("c - JaxrcpMapping element");

                System.out.print("> ");
                readLine = in.readLine();
                if (readLine.length() == 0) {
                    continue;
                }
                c = readLine.charAt(0);
                AbsDeploymentTest testElememt = null;
                switch (c) {
                case '1' :
                    webDeploymentTest.startTest(true);
                    break;
                case '2' :
                    jonasWebDeploymentTest.startTest(true);
                    break;
                case '3' :
                    rarDeploymentTest.startTest(true);
                    break;
                case '4' :
                    jonasRarDeploymentTest.startTest(true);
                    break;
                case '5' :
                    clientDeploymentTest.startTest(true);
                    break;
                case '6' :
                    jonasClientDeploymentTest.startTest(true);
                    break;
                case '7' :
                    earDeploymentTest.startTest(true);
                    break;
                case '8' :
                    ejbDeploymentTest.startTest(true);
                    break;
                case '9' :
                    jonasEjbDeploymentTest.startTest(true);
                    break;
                case 'a' :
                    wsDeploymentTest.startTest(true);
                    break;
                case 'b' :
                    wsDeploymentTest.startTest(true);
                    break;
                case 'c' :
                    wsDeploymentTest.startJaxrpcMappingTest(true);
                    break;

                default :
                    continue;
                }
                break;

            case 's':
                System.out.println("1 - WebApp element");
                System.out.println("2 - JonasWebApp element");
                System.out.println("3 - Connector element");
                System.out.println("4 - JonasConnector element");
                System.out.println("5 - ClientApp element");
                System.out.println("6 - JonasClient element");
                System.out.println("7 - Application element");
                System.out.println("8 - EjbJar element");
                System.out.println("9 - JonasEjbJar element");
                System.out.println("a - WebServices element");
                System.out.println("b - JonasWebServices element");
                System.out.println("c - JaxrcpMapping element");

                System.out.print("> ");
                readLine = in.readLine();
                if (readLine.length() == 0) {
                    continue;
                }
                c = readLine.charAt(0);

                switch (c) {
                case '1' :
                    webDeploymentTest.stress();
                    break;
                case '2' :
                    jonasWebDeploymentTest.stress();
                    break;
                case '3' :
                    rarDeploymentTest.stress();
                    break;
                case '4' :
                    jonasRarDeploymentTest.stress();
                    break;
                case '5' :
                    clientDeploymentTest.stress();
                    break;
                case '6' :
                    jonasClientDeploymentTest.stress();
                    break;
                case '7' :
                    earDeploymentTest.stress();
                    break;
                case '8' :
                    ejbDeploymentTest.stress();
                    break;
                case '9' :
                    jonasEjbDeploymentTest.stress();
                    break;
                case 'a' :
                    wsDeploymentTest.stress();
                    break;
                case 'b' :
                    jonasWsDeploymentTest.stress();
                    break;
                case 'c' :
                    wsDeploymentTest.startJaxrpcMappingTest(true);
                    break;

                default :
                    continue;
                }
                break;

            case 'p':
                System.out.println("1 - WebApp element");
                System.out.println("2 - JonasWebApp element");
                System.out.println("3 - Connector element");
                System.out.println("4 - JonasConnector element");
                System.out.println("5 - ClientApp element");
                System.out.println("6 - JonasClient element");
                System.out.println("7 - Application element");
                System.out.println("8 - EjbJar element");
                System.out.println("9 - JonasEjbJar element");
                System.out.println("a - WebServices element");
                System.out.println("b - JonasWebServices element");
                System.out.println("c - JaxrcpMapping element");

                System.out.print("> ");
                readLine = in.readLine();
                if (readLine.length() == 0) {
                    continue;
                }
                c = readLine.charAt(0);
                switch (c) {
                case '1' :
                    webDeploymentTest.parseElement();
                    break;
                case '2' :
                    jonasWebDeploymentTest.parseElement();
                    break;
                case '3' :
                    rarDeploymentTest.parseElement();
                    break;
                case '4' :
                    jonasRarDeploymentTest.parseElement();
                    break;
                case '5' :
                    clientDeploymentTest.parseElement();
                    break;
                case '6' :
                    jonasClientDeploymentTest.parseElement();
                    break;
                case '7' :
                    earDeploymentTest.parseElement();
                    break;
                case '8' :
                    ejbDeploymentTest.parseElement();
                    break;
                case '9' :
                    jonasEjbDeploymentTest.parseElement();
                    break;

                }

                break;
            case 'f':
                System.out.println("1 - web.xml file");
                System.out.println("2 - jonas-web.xml file");
                System.out.println("3 - ra.xml file");
                System.out.println("4 - jonas-ra.xml file");
                System.out.println("5 - client.xml file");
                System.out.println("6 - jonas-client.xml file");
                System.out.println("7 - application.xml file");
                System.out.println("8 - ejb-jar.xml file");
                System.out.println("9 - jonas-ejbjar.xml file");

                System.out.print("> ");
                readLine = in.readLine();
                if (readLine.length() == 0) {
                    continue;
                }
                c = readLine.charAt(0);
                System.out.print("Enter the name of the xml file > ");
                String fileName = in.readLine();

                switch (c) {
                case '1' :
                    webDeploymentTest.parseXmlfromFile(fileName);
                    break;
                case '2' :
                    jonasWebDeploymentTest.parseXmlfromFile(fileName);
                    break;
                case '3' :
                    rarDeploymentTest.parseXmlfromFile(fileName);
                    break;
                case '4' :
                    jonasRarDeploymentTest.parseXmlfromFile(fileName);
                    break;
                case '5' :
                    clientDeploymentTest.parseXmlfromFile(fileName);
                    break;
                case '6' :
                    jonasClientDeploymentTest.parseXmlfromFile(fileName);
                    break;
                case '7' :
                    earDeploymentTest.parseXmlfromFile(fileName);
                    break;
                case '8' :
                    ejbDeploymentTest.parseXmlfromFile(fileName);
                    break;
                case '9' :
                    jonasEjbDeploymentTest.parseXmlfromFile(fileName);
                    break;
                case 'a' :
                    wsDeploymentTest.parseXmlfromFile(fileName);
                    break;
                case 'b' :
                    jonasWsDeploymentTest.parseXmlfromFile(fileName);
                    break;
                case 'c' :
                    wsDeploymentTest.parseJaxrcpMappingElement();
                    break;

                }
                break;

            case 'q':
                System.exit(0);
                break;

            default :
                break;
            }
        }
    }


}
