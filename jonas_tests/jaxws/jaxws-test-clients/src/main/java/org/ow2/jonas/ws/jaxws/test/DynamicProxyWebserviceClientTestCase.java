/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.jaxws.test;


import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import org.ow2.jonas.ws.jaxws.test.util.JWebServicesTestCase;
import org.ow2.jonas.ws.jaxws.test.util.SimpleServiceInterface;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class DynamicProxyWebserviceClientTestCase extends JWebServicesTestCase {

    public DynamicProxyWebserviceClientTestCase(String s) throws Exception {
        super(s);
    }
    private static final String PORT_URL = "/pojo/SimpleService";    


    @BeforeClass
    public void setUp() throws Exception {
        useWar("jaxws-pojo-simple-with-wsdl");
    }

    @AfterClass
    public void tearDown() throws Exception {
        unUseWar("jaxws-pojo-simple-with-wsdl");
    }

    
    @Test(groups = { "DynamicProxy" })
    public void testInvokeWithDispatchAPI() throws MalformedURLException {

        String port = System.getProperty("http.port");

        if(port == null) {
            port = "9000";
        }
        String UrlString = "http://localhost:"+ port + PORT_URL +"?wsdl";
        
        QName serviceName = new QName("http://pojo.test.jaxws.ws.jonas.ow2.org/","SimpleService");
        
        QName portName = new QName("http://pojo.test.jaxws.ws.jonas.ow2.org/","SimpleServicePort");
        
        System.out.println("UrlString = " + UrlString);
        URL helloWsdlUrl = new URL(UrlString);
        
        //ServiceFactory serviceFactory = ServiceFactory.newInstance();
        
        Service simpleService = Service.create(helloWsdlUrl, serviceName);
        SimpleServiceInterface ssi =(SimpleServiceInterface)simpleService.getPort( portName, SimpleServiceInterface.class);
        
        //javax.xml.ws.BindingProvider bp = (javax.xml.ws.BindingProvider)ssi;
        //Map<String,Object> context = bp.getRequestContext();
        //context.put("javax.xml.ws.session.maintain", Boolean.TRUE);
        
        Assert.assertEquals( ssi.greetMe("JOnAS"), "helloJOnAS");
    }    
}
