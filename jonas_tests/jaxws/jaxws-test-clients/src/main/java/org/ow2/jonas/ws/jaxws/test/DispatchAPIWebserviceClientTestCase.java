/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.jaxws.test;

import java.io.ByteArrayOutputStream;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.ow2.jonas.ws.jaxws.test.util.JWebServicesTestCase;


/**
 * This is a client of SimpleService web service and it tests the 
 * JAXWS Dispatch API by using Dispatch API to invoke the web service.
 *  
 * @author xiaoda
 *
 */
public class DispatchAPIWebserviceClientTestCase extends JWebServicesTestCase {

    public DispatchAPIWebserviceClientTestCase(String s) throws Exception {
        super(s);
    }
    private static final String PORT_URL = "/pojo/SimpleService";    


    @BeforeClass
    public void setUp() throws Exception {
        useWar("jaxws-pojo-simple-with-wsdl");
    }

    @AfterClass
    public void tearDown() throws Exception {
        unUseWar("jaxws-pojo-simple-with-wsdl");
    }

    @Test(groups = { "DispatchAPI" })
    public void testInvokeWithDispatchAPI() {

        try{

            String port = System.getProperty("http.port");

            if(port == null) {
                port = "9000";
            }
            //String endpointUrl = "http://localhost" + port + PORT_URL;

            URL wsdlLocation = new URL("http://localhost:" + port + PORT_URL + "?wsdl");

            QName serviceName = new QName("http://pojo.test.jaxws.ws.jonas.ow2.org/",
                    "SimpleService");


            QName portName = new QName("http://pojo.test.jaxws.ws.jonas.ow2.org/",
             "SimpleServicePort");
            Service service = Service.create(wsdlLocation, serviceName);

            //service.addPort(portName, SOAPBinding.SOAP11HTTP_BINDING, endpointUrl);
            Assert.assertNotNull(service);

            /** Create a Dispatch instance from a service.**/ 
            Dispatch<SOAPMessage> dispatch = service.createDispatch(portName, 
            SOAPMessage.class, Service.Mode.MESSAGE);

            Assert.assertNotNull(dispatch);

            /** Create SOAPMessage request. **/
            // compose a request message
            MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);

            // Create a message.  This example works with the SOAPPART.
            SOAPMessage request = mf.createMessage();
            SOAPPart part = request.getSOAPPart();

            // Obtain the SOAPEnvelope and header and body elements.
            SOAPEnvelope env = part.getEnvelope();
            SOAPBody body = env.getBody();

            // Construct the message payload.
            SOAPElement operation = body.addChildElement(env.createName("greetMe"));
            SOAPElement value = operation.addChildElement("arg0");
            value.addTextNode("JOnAS");
            request.saveChanges();

            /** Invoke the service endpoint. **/
            SOAPMessage response = dispatch.invoke(request);

            Assert.assertNotNull(response);

            ByteArrayOutputStream os = new ByteArrayOutputStream();
            response.writeTo(os);
            String str = new String(os.toByteArray());
            System.out.println(str);
            Assert.assertTrue(str.contains("<return>helloJOnAS</return>"));
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
