/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.jaxws.test.ejb;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPFault;
import javax.xml.ws.soap.SOAPFaultException;

/**
 * This is an EJB based JAXWS web service with @WebMethod annotation.
 * @author xiaoda
 *
 */

// Here we do not specify the SEI, web service container will generate a default SEI
// this SEI will contain all @WebMethod annotated methods and public methods
// see specs Web Services for Java EE, Version 1.2 # chapter 3.3
@WebService(name="JAXWSBeanWithoutWebMethod",
            serviceName="JAXWSBeanWithoutWebMethod",
            wsdlLocation="META-INF/wsdl/JAXWSBeanWithoutWebMethod.wsdl")

@Stateless(mappedName="JAXWSBeanWithoutWebMethod")
@Remote(JAXWSGreeter.class)
@SOAPBinding(style=SOAPBinding.Style.DOCUMENT,
             use=SOAPBinding.Use.LITERAL,
             parameterStyle=SOAPBinding.ParameterStyle.WRAPPED
)
public class JAXWSBeanWithoutWebMethod implements JAXWSGreeter {


    public String greetMe(final String me) {
        System.out.println("i'm a ejb ws : " + me);
        System.out.println("greet me");
        return "Hello " + me;
    }


    public void greetMeFault(final String me) {
        System.out.println("generate SOAP fault");
        SOAPFault fault = null;
        try {
            fault = SOAPFactory.newInstance().createFault();
            fault.setFaultCode(new QName("http://foo", "MyFaultCode"));
            fault.setFaultString("my error");
            fault.setFaultActor("my actor");
        } catch (SOAPException ex) {
            throw new RuntimeException(ex);
        }

        throw new SOAPFaultException(fault);
    }

    public String greetYou(final String you) {
        System.out.println("i'm a ejb ws: " + you);
        System.out.println("greet you");
        return "Hi " + you;
    }

    public String sayHello(String name) {
        return "Hello World!" + name;
    }

}
