package org.ow2.jonas.ws.jaxws.test.ejb;

import javax.jws.WebService;

@WebService
public interface JAXWSGreeterForWithWM {

    public String greetMe(final String me);
    public void greetMeFault(final String me);
}