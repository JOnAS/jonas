/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: xiaoda
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.ws.jaxws.test.pojo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.ws.WebServiceContext;
import javax.jws.WebService;
import javax.annotation.Resource;

@WebService(name = "Person",
            targetNamespace = "http://org.ow2.jonas.ws.jaxws.test.person",wsdlLocation="WEB-INF/wsdl/JAXWSBean3Service.wsdl")
public class JAXWSBean3 {

@Resource WebServiceContext wsContext;

    private static List<Person> PERSONS = new ArrayList<Person>();

    static {
        PERSONS.add(new Person("eric", "cartman"));
        PERSONS.add(new Person("homer", "simpson"));
    }

    public Person[] getPersons1() {  
        System.out.println(wsContext);
        return PERSONS.toArray(new Person [] {});
    }

    public List<Person> getPersons2() {
        return PERSONS;
    }

}
