package org.ow2.jonas.ejb3;

import javax.ejb.Remote;
import javax.ejb.Stateless;

// Referenced classes of package org.ow2.jonas.ws.axis2.test.ejb3:
//            Hello

@Remote(Hello.class)
@Stateless
public class HelloBean
    implements Hello
{

    public HelloBean()
    {
    }

    public String sayHello(String str)
    {
        System.out.println((new StringBuilder("HelloWorldEJB3: ")).append(str).toString());
        return "HelloWorld EJB3!";
    }
}