package org.ow2.jonas.ws.jaxws.test.pojo;

/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: xiaoda
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

import javax.annotation.Resource;
import javax.jws.WebService;

import org.ow2.jonas.ejb3.Hello;

/**
 * This is a basic JAXWS pojo web service.
 * @author xiaoda
 *
 */
@WebService(name="SimpleService",serviceName="SimpleService",wsdlLocation="WEB-INF/wsdl/SimpleService.wsdl")
public class SimpleService implements SimpleServiceInterface {  

    private static final long serialVersionUID = 1L;
    
    public String greetMe(String str) {
        return "hello" + str;
    }
}
