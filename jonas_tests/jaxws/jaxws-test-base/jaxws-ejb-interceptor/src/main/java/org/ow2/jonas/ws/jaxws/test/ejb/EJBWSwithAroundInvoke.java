/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.jaxws.test.ejb;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.soap.SOAPBinding;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPFault;
import javax.xml.ws.soap.SOAPFaultException;

/**
 * This is an EJB based JAXWS web service with @WebMethod annotation.
 * @author xiaoda
 *
 */
@WebService(name="EJBWSwithAroundInvoke",
            serviceName="EJBWSwithAroundInvoke",
            wsdlLocation="META-INF/wsdl/EJBWSwithAroundInvoke.wsdl",
            endpointInterface="org.ow2.jonas.ws.jaxws.test.ejb.JAXWSGreeterForWithWM")

@Stateless(mappedName="JAXWSBeanWithWebMethod")
@Remote(JAXWSGreeterForWithWM.class)
@SOAPBinding(style=SOAPBinding.Style.DOCUMENT,
             use=SOAPBinding.Use.LITERAL,
             parameterStyle=SOAPBinding.ParameterStyle.WRAPPED
)
public class EJBWSwithAroundInvoke implements JAXWSGreeter {

    private String response = "Hello";

    @WebMethod
    public String greetMe(final String me) {
        System.out.println("i'm a ejb ws: " + me);
        return response + me;
    }

    @WebMethod
    public void greetMeFault(final String me) {
        System.out.println("generate SOAP fault");
        SOAPFault fault = null;
        try {
            fault = SOAPFactory.newInstance().createFault();
            fault.setFaultCode(new QName("http://foo", "MyFaultCode"));
            fault.setFaultString("my error");
            fault.setFaultActor("my actor");
        } catch (SOAPException ex) {
            throw new RuntimeException(ex);
        }

        throw new SOAPFaultException(fault);
    }
    
    public String greetYou(final String you) {
        System.out.println("i'm a ejb ws: " + you);
        System.out.println("greet you");
        return "Hi " + you;
    }

    public String sayHello(String name) {
        return "Hello World! " + name;
    }
    
    
    @AroundInvoke
    public Object trace(InvocationContext ic) throws Exception {
        //ic.getParameters()[0] = "inceptor";
        response = response + " intercepted";
        System.out.println("Intercepted method : " + ic.getMethod().getName());
        return ic.proceed();
    }
}
