/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.jaxws.test.ejb;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;/**
 * This class intercepts Business interfaces methods
 */
public class GreeterInterceptor {  /**
   * Invocation counter
   */
   private int counter = 0;  /**
   * Count the invocations of a bean.
   * @param ic InvocationContext
   * @return proceeded method
   * @throws Exception if something wrong occurs.
   */
   @AroundInvoke
   public Object count(InvocationContext ic) throws Exception {      synchronized(this) {
         counter++;
         System.out.println("Method '" + ic.getMethod().getName()
                            + "' invoked " + counter + " times.");
         return ic.proceed();
      }
   }
}