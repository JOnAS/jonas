/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.jaxws.test.ejb;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.soap.SOAPBinding;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPFault;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.soap.SOAPFaultException;

import org.ow2.jonas.ejb3.Hello;

/**
 * This is an EJB based JAXWS web service with @WebMethod annotation.
 * 
 * @author xiaoda
 * 
 */
@WebService(name = "JAXWSBeanInjection", serviceName = "JAXWSBeanInjection", wsdlLocation = "META-INF/wsdl/JAXWSBeanInjection.wsdl")
@Stateless(mappedName = "JAXWSBeanInjection")
@Remote(JAXWSGreeter.class)
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public class JAXWSBeanInjection implements JAXWSGreeter {

    @Resource
    private SessionContext sessionContext;

    @Resource
    WebServiceContext wsContext;

    @EJB(name = "Hello", beanInterface=Hello.class,beanName="org.ow2.jonas.ejb3.HelloBean")
    Hello hello;

    @WebMethod(exclude = false)
    public String greetMe(final String me) {
        String res = new String();
        res = me;

        if (wsContext != null) {
            System.out.println("wsContext is not null");
            res = res + "wsContext";
        }
        if (hello != null) {
        System.out.println("hello is not null");
            res = res + "hello";
        }
        
        if (sessionContext != null) {
            System.out.println("sessionContext is not null");
                res = res + "sessionContext";
            }        
        return res;
    }

    public void greetMeFault(final String me) {
        System.out.println("generate SOAP fault");
        SOAPFault fault = null;
        try {
            fault = SOAPFactory.newInstance().createFault();
            fault.setFaultCode(new QName("http://foo", "MyFaultCode"));
            fault.setFaultString("my error");
            fault.setFaultActor("my actor");
        } catch (SOAPException ex) {
            throw new RuntimeException(ex);
        }
        throw new SOAPFaultException(fault);
    }

}
