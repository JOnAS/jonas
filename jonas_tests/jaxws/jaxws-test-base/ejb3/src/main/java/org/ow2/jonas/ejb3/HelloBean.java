/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ejb3;

import javax.ejb.Remote;
import javax.ejb.Stateless;

// Referenced classes of package org.ow2.jonas.ws.axis2.test.ejb3:
//            Hello

@Remote(Hello.class)
@Stateless
public class HelloBean
    implements Hello
{

    public HelloBean()
    {
    }

    public String sayHello(String str)
    {
        System.out.println((new StringBuilder("HelloWorldEJB3: ")).append(str).toString());
        return "HelloWorld EJB3!";
    }
}