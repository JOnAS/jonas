/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: xiaoda
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.jaxws.test.ejb;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.soap.SOAPBinding;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPFault;
import javax.xml.ws.soap.SOAPFaultException;

/**
 * This is an EJB based JAXWS web service with @WebMethod annotation.
 * @author xiaoda
 *
 */
@WebService(name="JAXWSBean_descriptor",
            serviceName="JAXWSBean_descriptor",
            wsdlLocation="META-INF/wsdl/JAXWSBean_descriptor.wsdl")

@Stateless(mappedName="JAXWSBean_With_WebMethod")
@Remote(JAXWSGreeter.class)
@SOAPBinding(style=SOAPBinding.Style.DOCUMENT,
             use=SOAPBinding.Use.LITERAL,
             parameterStyle=SOAPBinding.ParameterStyle.WRAPPED
)
public class JAXWSBean_descriptor implements JAXWSGreeter {

    @WebMethod(exclude=false)
    public String greetMe(final String me) {
        System.out.println("i'm a ejb ws: " + me);
        if (!"foo bar".equals(me)) {
            throw new RuntimeException("Wrong parameter");
        }
        return "Hello " + me;
    }

    
    public void greetMeFault(final String me) {
        System.out.println("generate SOAP fault");
        SOAPFault fault = null;
        try {
            fault = SOAPFactory.newInstance().createFault();
            fault.setFaultCode(new QName("http://foo", "MyFaultCode"));
            fault.setFaultString("my error");
            fault.setFaultActor("my actor");
        } catch (SOAPException ex) {
            throw new RuntimeException(ex);
        }

        throw new SOAPFaultException(fault);
    }

}
