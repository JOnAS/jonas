/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009-2010 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.itests.cargo.jrmp;

import java.lang.reflect.Method;
import java.net.URL;
import java.util.UUID;

import org.testng.Assert;
import org.testng.annotations.Test;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;

/**
 * Tests the JAX-WS services.
 */
public class JaxWSTest extends TestCommons {

    private String serviceURL;

    private URL wsdlURL;

    @Test
    public void waitForQuoteServiceInitialization() throws Exception {
        this.serviceURL = "http://localhost:" + this.webcontainerPort + "/quote/QuoteReporterService";
        this.wsdlURL = new URL(this.serviceURL + "?wsdl");

        final String expected = "http://jonas.ow2.org/tutorial/jaxws/quote";

        String content = null;
        final long stopTime = System.currentTimeMillis() + TestCommons.TIMEOUT * 1000;
        while (System.currentTimeMillis() < stopTime) {
            Thread.sleep(1000);
            content = this.getContentAsString(this.wsdlURL);
            if (content.contains(expected)) {
                break;
            }
        }

        Assert.assertTrue(content.contains(expected), content + " does not contain " + expected);
    }

    @Test(dependsOnMethods = "waitForQuoteServiceInitialization")
    public void checkBugJonas131() throws Exception {
        // JONAS-131: When exposing WebServices with OnDemand feature enabled,
        // the specified URL, where the webservice is available, is displayed
        // using the "onDemand" port and not the web application server port.

        final String content = this.getContentAsString(this.wsdlURL);
        Assert.assertTrue(content.contains(this.serviceURL), content + " does not contain " + this.wsdlURL);
    }

    @Test(dependsOnMethods = "waitForQuoteServiceInitialization")
    public void testQuoteService() throws Exception {
        final String quoteTicker = UUID.randomUUID().toString();

        JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
        Client client = dcf.createClient(this.wsdlURL);

        Object quoteResponse = client.invoke("getQuote", quoteTicker)[0];
        Method getTicker = quoteResponse.getClass().getMethod("getTicker");
        Method getValue = quoteResponse.getClass().getMethod("getValue");

        String ticker = getTicker.invoke(quoteResponse).toString();
        String value = getValue.invoke(quoteResponse).toString();

        Assert.assertEquals(ticker, quoteTicker);
        try {
            Double.parseDouble(value);
        } catch (NumberFormatException e) {
            Assert.fail("did not get a ticker value: " + value);
        }
    }

}
