/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009-2010 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.itests.cargo.jrmp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Tests that the JONAS-364 bug doesn't happen.
 * @author Florent
 */
public class ClassLoaderTest extends TestCommons {

    @Test
    public void testJonas364() throws Exception {
        String jBase = getJOnASBase();
        Assert.assertNotNull(jBase, "Unable to get JOnAS Base property");

        // Check the file with result is here
        File resultFile = new File(jBase + File.separator + "classloader-results.txt");

        Assert.assertTrue(resultFile.exists(), "No results for classloader");

        // Read the first line
        FileReader fReader = new FileReader(resultFile);
        BufferedReader bufferedReader = new BufferedReader(fReader);
        String firstLine = bufferedReader.readLine();
        Assert.assertEquals(firstLine, "OK");
        String secondLine = bufferedReader.readLine();
        Assert.assertEquals(secondLine, "OK");

    }

}
