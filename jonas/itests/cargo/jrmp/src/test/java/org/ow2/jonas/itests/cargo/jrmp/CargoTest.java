/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009-2010 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.itests.cargo.jrmp;

import java.net.URL;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Tests the JOnAS - CARGO plug-in. This test is expected to work with JOnAS 4
 * and JOnAS 5 without any issues.
 *
 * @author S. Ali Tokmen
 * @author Florent Benoit junit->TestNG
 */
public class CargoTest extends TestCommons {

    @Test
    public void testCargo() throws Exception {
        final URL url = new URL("http://localhost:" + this.webcontainerPort + "/cargocpc/index.html");
        final String expected = "Cargo Ping Component used to verify if the container is started.";

        String content = null;
        final long stopTime = System.currentTimeMillis() + TestCommons.TIMEOUT * 1000;
        while (System.currentTimeMillis() < stopTime) {
            Thread.sleep(1000);
            content = this.getContentAsString(url);
            if (content.contains(expected)) {
                break;
            }
        }

        Assert.assertTrue(content.contains(expected), content + " does not contain " + expected);
    }

}
