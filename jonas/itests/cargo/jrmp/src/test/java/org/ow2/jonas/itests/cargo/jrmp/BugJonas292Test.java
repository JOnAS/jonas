/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009-2010 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.itests.cargo.jrmp;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;

import javax.management.Attribute;
import javax.management.ObjectName;
import org.apache.commons.io.IOUtils;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Tests that the JONAS-292 bug doesn't happen.
 *
 * @author S. Ali Tokmen
 */
public class BugJonas292Test extends TestCommons {

    @Test
    public void deactivateDevelopmentMode() throws Exception {
        ObjectName depmonitor = new ObjectName("jonas:type=service,name=depmonitor");
        this.mBeanServerConnection.setAttribute(depmonitor, new Attribute("development", Boolean.FALSE));
    }

    @Test(dependsOnMethods = "deactivateDevelopmentMode")
    public void testJonas292() throws Exception {
        final ObjectName j2eeServer = new ObjectName("jonas:j2eeType=J2EEServer,name=jonas");

        final String jonasBase = (String) this.mBeanServerConnection.getAttribute(j2eeServer, "jonasBase");
        Assert.assertNotNull(jonasBase, "jonasBase is null");
        final String fileName = "jonas-292.jar";
        final File targetFile = new File(jonasBase, "deploy/" + fileName);
        final Object[] opParams = {targetFile.getAbsolutePath()};
        final String[] opSignature = {"java.lang.String"};

        this.checkDeployableStatus(j2eeServer, "deployableJars", fileName, false);
        this.checkDeployableStatus(j2eeServer, "deployableFiles", fileName, false);
        this.checkDeployableStatus(j2eeServer, "deployedJars", fileName, false);
        this.checkDeployableStatus(j2eeServer, "deployedFiles", fileName, false);

        try {
            final InputStream fileContents = this.getClass().getClassLoader().getResourceAsStream(fileName);
            Assert.assertNotNull(fileContents, "Cannot find " + fileName);

            FileOutputStream targetStream = new FileOutputStream(targetFile);
            IOUtils.copy(fileContents, targetStream);

            targetStream.close();
            targetStream = null;
        } finally {
            System.gc();
        }

        try {
            this.checkDeployableStatus(j2eeServer, "deployableJars", fileName, true);
            this.checkDeployableStatus(j2eeServer, "deployableFiles", fileName, true);
            this.checkDeployableStatus(j2eeServer, "deployedJars", fileName, false);
            this.checkDeployableStatus(j2eeServer, "deployedFiles", fileName, false);

            mBeanServerConnection.invoke(j2eeServer, "deploy", opParams, opSignature);

            this.checkDeployableStatus(j2eeServer, "deployableJars", fileName, false);
            this.checkDeployableStatus(j2eeServer, "deployableFiles", fileName, false);
            this.checkDeployableStatus(j2eeServer, "deployedJars", fileName, true);
            this.checkDeployableStatus(j2eeServer, "deployedFiles", fileName, true);

            mBeanServerConnection.invoke(j2eeServer, "undeploy", opParams, opSignature);

            this.checkDeployableStatus(j2eeServer, "deployableJars", fileName, true);
            this.checkDeployableStatus(j2eeServer, "deployableFiles", fileName, true);
            this.checkDeployableStatus(j2eeServer, "deployedJars", fileName, false);
            this.checkDeployableStatus(j2eeServer, "deployedFiles", fileName, false);
        } finally {
            Assert.assertTrue(targetFile.delete(), "File " + targetFile + " cannot be deleted");
        }
    }

    private void checkDeployableStatus(final ObjectName j2eeServer, final String attribute, final String fileName,
        final boolean status) throws Exception {

        boolean found = false;
        List<String> files = (List<String>) this.mBeanServerConnection.getAttribute(j2eeServer, attribute);
        for (String file : files) {
            if (file.contains(fileName)) {
                found = true;
                break;
            }
        }

        Assert.assertEquals(found, status, "File " + fileName + " is " + (status ? "not " : "") + "in the " + attribute
            + " list: " + files);
    }

}
