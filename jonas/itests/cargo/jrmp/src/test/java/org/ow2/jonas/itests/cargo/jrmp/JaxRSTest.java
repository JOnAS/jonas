/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2014 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 *  $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.itests.cargo.jrmp;

import java.io.ByteArrayInputStream;
import java.net.URL;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.ow2.util.xml.DocumentParser;
import org.ow2.util.xml.EmptyEntityResolver;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * A {@code JaxRSTest} tests that a JAX-RS application can be deployed on JOnAS either as a WebApplication or in an OSGi
 * bundle and that it support both XML and JSON as data representation.
 *
 * @author Loic Albertin
 */
public class JaxRSTest extends TestCommons {


    @Test
    public void testLibraryWarXML() throws Exception {
        String result = this.getContentAsString(new URL("http://localhost:" + webcontainerPort + "/jaxrs/web/library"),
                new HeaderPair("Accept", "application/xml"));
        checkXmlResult(result);
    }

    @Test
    public void testLibraryWarJSON() throws Exception {
        String result = this.getContentAsString(new URL("http://localhost:" + webcontainerPort + "/jaxrs/web/library"),
                new HeaderPair("Accept", "application/json"));
        checkJsonResult(result);
    }

    @Test
    public void testLibraryWarDefault() throws Exception {
        String result = this.getContentAsString(new URL("http://localhost:" + webcontainerPort + "/jaxrs/web/library"));
        checkXmlResult(result);
    }


    @Test
    public void testLibraryOSGiXML() throws Exception {
        String result = this.getContentAsString(new URL("http://localhost:" + webcontainerPort + "/jaxrs-osgi/osgi/library"),
                new HeaderPair("Accept", "application/xml"));
        checkXmlResult(result);
    }

    @Test
    public void testLibraryOSGiJSON() throws Exception {
        String result = this.getContentAsString(new URL("http://localhost:" + webcontainerPort + "/jaxrs-osgi/osgi/library"),
                new HeaderPair("Accept", "application/json"));
        checkJsonResult(result);
    }

    @Test
    public void testLibraryOSGiDefault() throws Exception {
        String result = this.getContentAsString(new URL("http://localhost:" + webcontainerPort + "/jaxrs-osgi/osgi/library"));
        checkXmlResult(result);
    }


    @Test
    public void testJsonConsumingWithPOJOMappingOSGi() throws Exception {
        getContentAsString(new URL("http://localhost:" + webcontainerPort + "/jaxrs-osgi/osgi/library"),
                "{\"test\": \"testValue\", \"test2\": \"testValue2\"}", "POST",
                new HeaderPair("Content-Type", "application/json"),
                new HeaderPair("Accept", "application/json"));
    }

    @Test
    public void testJsonConsumingWithPOJOMappingWar() throws Exception {
        getContentAsString(new URL("http://localhost:" + webcontainerPort + "/jaxrs/web/library"),
                "{\"test\": \"testValue\", \"test2\": \"testValue2\"}", "POST",
                new HeaderPair("Content-Type", "application/json"),
                new HeaderPair("Accept", "application/json"));
    }

    private void checkXmlResult(String result) throws Exception {
        Document document = null;
        try {
            document = DocumentParser.getDocument(new ByteArrayInputStream(result.getBytes("UTF-8")), false, new EmptyEntityResolver());
        } catch (Exception e) {
            Assert.fail("XML parsing of the response failed: " + result, e);
        }
        Element rootElement = document.getDocumentElement();
        Assert.assertEquals(rootElement.getTagName(), "library", "library root element missing");
        NodeList nodeList = rootElement.getElementsByTagName("books");
        Assert.assertEquals(nodeList.getLength(), 1, "No books collection");
        Element booksElement = (Element) nodeList.item(0);
        nodeList = booksElement.getElementsByTagName("book");
        Assert.assertEquals(nodeList.getLength(), 4, "Empty books collection");
    }

    private void checkJsonResult(String result) throws Exception {
        JSONParser parser = new JSONParser();
        Object obj = parser.parse(result);
        Assert.assertTrue(obj instanceof JSONObject, "Parsing of the root JSON object failed");
        obj = ((JSONObject) obj).get("books");
        Assert.assertTrue(obj instanceof JSONObject, "Parsing of the books failed");
        obj = ((JSONObject) obj).get("book");
        Assert.assertTrue(obj instanceof JSONArray, "Parsing of the books array failed");
        Assert.assertEquals(((JSONArray) obj).size(), 4, "Wrong number of elements in books array");
    }

}
