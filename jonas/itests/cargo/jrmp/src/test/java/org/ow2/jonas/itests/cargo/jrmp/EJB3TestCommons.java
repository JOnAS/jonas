/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2013 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 *  $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.itests.cargo.jrmp;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.ow2.easybeans.osgi.tests.AbsTesting;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;

/**
 * @author Loic Albertin
 */
public abstract class EJB3TestCommons extends AbsTesting {

    /**
     * Default InitialContextFactory to use.
     */
    private static final String INITIAL_CONTEXT_FACTORY = "org.ow2.easybeans.component.smartclient.spi.SmartContextFactory";

    /**
     * URL of the provider (without the port)
     */
    private static final String PROVIDER_URL = "smart://localhost:";

    /**
     * The system property for the port of the smartclient
     */
    public final static String SMART_CLIENT_PORT_SYSTEM_PROPERTY = "smartclient.port";
    /**
     * The context
     */
    private Context initialContext;

    /**
     * The old classloader
     */
    private ClassLoader oldClassLoader;


    /**
     * @return Returns the InitialContext.
     *
     * @throws javax.naming.NamingException If the Context cannot be created.
     */
    protected Context getInitialContext() throws NamingException {
        if (initialContext == null) {
            Hashtable<String, Object> env = new Hashtable<String, Object>();
            env.put(Context.INITIAL_CONTEXT_FACTORY, INITIAL_CONTEXT_FACTORY);
            env.put(Context.PROVIDER_URL, PROVIDER_URL + System.getProperty(SMART_CLIENT_PORT_SYSTEM_PROPERTY));

            initialContext = new InitialContext(env);
        }
        return initialContext;
    }

    /**
     * Initialize the context
     */
    @BeforeClass
    public void initInitialContext() throws Exception {
        super.init();

        //need to fix a classloader IRMI issue with the smartclient (some class wasn't downloaded...). Ugly but usefull
        //to fix a IRMI issue
        this.oldClassLoader = Thread.currentThread().getContextClassLoader();
        ClassLoader ejbClassLoader = getEjbClassLoader();
        Thread.currentThread().setContextClassLoader(ejbClassLoader);
    }

    public abstract ClassLoader getEjbClassLoader();

    /**
     * Reset the classloader after that all test methods in the current class have been run.
     */
    @AfterSuite
    public void resetClassLoader() {
        Thread.currentThread().setContextClassLoader(this.oldClassLoader);
    }
}
