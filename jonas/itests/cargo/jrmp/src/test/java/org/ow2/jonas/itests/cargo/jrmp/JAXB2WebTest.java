package org.ow2.jonas.itests.cargo.jrmp;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Scanner;

import javax.naming.NamingException;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;
import org.ow2.jonas.itests.jaxb2.JAXB2Marshaller;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * @author Loic Albertin
 */
public class JAXB2WebTest extends TestCommons {

    private String serviceURL;

    private URL wsdlURL;

    private String expectedResult;

    @BeforeClass
    public void loadExpectedResult() throws IOException, URISyntaxException, NamingException {
        URL url = JAXB2WebTest.class.getResource("/jaxb2-test-result.xml");
        Assert.assertNotNull(url);
        Scanner scanner = new Scanner(new File(url.toURI())).useDelimiter("\\Z");
        expectedResult = scanner.next();
        scanner.close();
        Assert.assertNotNull(expectedResult);
        expectedResult = expectedResult.trim();
        Assert.assertNotEquals(expectedResult, "");
    }

    @Test
    public void waitForWSInitialization() throws Exception {
        this.serviceURL = "http://localhost:" + this.webcontainerPort + "/jaxb2/JAXWSJAXB2MarshallerService";
        this.wsdlURL = new URL(this.serviceURL + "?wsdl");

        // Check TNS
        final String expected = "http://jonas.ow2.org/itests/jaxb2";

        String content = null;
        final long stopTime = System.currentTimeMillis() + TestCommons.TIMEOUT * 1000;
        while (System.currentTimeMillis() < stopTime) {
            Thread.sleep(1000);
            content = this.getContentAsString(this.wsdlURL);
            if (content.contains(expected)) {
                break;
            }
        }

        Assert.assertTrue(content.contains(expected), content + " does not contain " + expected);
    }

    @Test(dependsOnMethods = "waitForWSInitialization")
    public void testJAXB2MarshalingInJAXWS() throws Exception {
        JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
        Client client = dcf.createClient(this.wsdlURL);
        // For debugging purpose
        //        HTTPConduit http = (HTTPConduit) client.getConduit();
        //
        //        HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
        //
        //        httpClientPolicy.setConnectionTimeout(36000000);
        //        httpClientPolicy.setReceiveTimeout(32000000);
        //
        //        http.setClient(httpClientPolicy);

        String result = (String) client.invoke("generateXMLString")[0];
        result = result.trim();
        Assert.assertEquals(result, expectedResult, "JAXB2 marshaling in JAXWS POJO failed");
    }

    @Test
    public void checkServlet() throws Exception {
        String result = getContentAsString(new URL("http://localhost:" + webcontainerPort + "/jaxb2/servlet"));
        result = result.trim();
        Assert.assertEquals(result, expectedResult);
    }

}
