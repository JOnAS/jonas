/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009-2010 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.itests.cargo.jrmp;

import java.io.File;
import java.util.List;

import javax.management.Attribute;
import javax.management.ObjectName;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Tests that the JONAS-96 bug doesn't happen.
 *
 * @author S. Ali Tokmen
 */
public class BugJonas96Test extends TestCommons {

    @Test
    public void deactivateDevelopmentMode() throws Exception {
        ObjectName depmonitor = new ObjectName("jonas:type=service,name=depmonitor");
        this.mBeanServerConnection.setAttribute(depmonitor, new Attribute("development", Boolean.FALSE));
    }

    @Test(dependsOnMethods = "deactivateDevelopmentMode")
    public void testEar() throws Exception {
        checkIfFileAppearsAsDeployed("dummy.ear", "Ear");
    }

    @Test(dependsOnMethods = "deactivateDevelopmentMode")
    public void testJar() throws Exception {
        checkIfFileAppearsAsDeployed("dummy.jar", "Jar");
    }

    @Test(dependsOnMethods = "deactivateDevelopmentMode")
    public void testRar() throws Exception {
        checkIfFileAppearsAsDeployed("dummy.rar", "Rar");
    }

    @Test(dependsOnMethods = "deactivateDevelopmentMode")
    public void testWar() throws Exception {
        checkIfFileAppearsAsDeployed("dummy.war", "War");
    }

    @Test(dependsOnMethods = "deactivateDevelopmentMode")
    public void testFile() throws Exception {
        checkIfFileAppearsAsDeployed("dummy.xml", "File");
    }

    private void checkIfFileAppearsAsDeployed(final String fileName, final String type) throws Exception {
        // Connect to the JMX server
        List<String> files;
        ObjectName j2eeServer = new ObjectName("jonas:j2eeType=J2EEServer,name=jonas");

        files = (List<String>) this.mBeanServerConnection.getAttribute(j2eeServer, "deployable" + type + "s");
        for (String file : files) {
            Assert.assertFalse(file.contains(fileName), "File " + fileName + " already exists: " + file);
        }
        files = (List<String>) this.mBeanServerConnection.getAttribute(j2eeServer, "deployableFiles");
        for (String file : files) {
            Assert.assertFalse(file.contains(fileName), "File " + fileName + " already exists: " + file);
        }

        String jonasBase = (String) this.mBeanServerConnection.getAttribute(j2eeServer, "jonasBase");
        Assert.assertNotNull(jonasBase, "jonasBase is null");
        File testFile = new File(jonasBase, "deploy/" + fileName);
        testFile.createNewFile();

        try {
            boolean found = false;
            files = (List<String>) this.mBeanServerConnection.getAttribute(j2eeServer, "deployable" + type + "s");
            for (String file : files) {
                if (file.contains(file)) {
                    found = true;
                    break;
                }
            }
            Assert.assertTrue(found, "File " + fileName + " doesn't appear in the deployable" + type + "s list");
            files = (List<String>) this.mBeanServerConnection.getAttribute(j2eeServer, "deployableFiles");
            for (String file : files) {
                if (file.contains(file)) {
                    return;
                }
            }
            Assert.fail("File " + fileName + " doesn't appear in the deployableFiles list");
        } finally {
            Assert.assertTrue(testFile.delete(), "File " + testFile + " cannot be deleted");
        }
    }

}
