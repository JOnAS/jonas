/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2013 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 *  $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.itests.cargo.jrmp;

import java.lang.reflect.Method;
import java.net.URL;
import java.util.Random;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Tests the JAX-WS WSAddBean service.
 */
public class JaxWSAddBeanTest extends TestCommons {

    private String serviceURL;

    private URL wsdlURL;

    @Test
    public void waitForWSAddBeanServiceInitialization() throws Exception {
        this.serviceURL = "http://localhost:" + this.webcontainerPort + "/addbean/AddBeanService";
        this.wsdlURL = new URL(this.serviceURL + "?wsdl");

        final String expected = "http://wsaddbean.applis.tests.jonas.ow2.org/";

        String content = null;
        final long stopTime = System.currentTimeMillis() + TestCommons.TIMEOUT * 1000;
        while (System.currentTimeMillis() < stopTime) {
            Thread.sleep(1000);
            content = this.getContentAsString(this.wsdlURL);
            if (content.contains(expected)) {
                break;
            }
        }

        Assert.assertTrue(content.contains(expected), content + " does not contain " + expected);
    }


    @Test(dependsOnMethods = "waitForWSAddBeanServiceInitialization")
    public void testWSAddBeanService() throws Exception {
        final Random random = new Random(System.currentTimeMillis());
        final int op1 = random.nextInt();
        final int op2 = random.nextInt();

        JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
        Client client = dcf.createClient(this.wsdlURL);

        Object addRequest =
                Thread.currentThread().getContextClassLoader().loadClass("org.ow2.jonas.tests.applis.wsaddbean.AddRequest").newInstance();
        Method setOp1 = addRequest.getClass().getMethod("setOp1", Integer.TYPE);
        Method setOp2 = addRequest.getClass().getMethod("setOp2", Integer.TYPE);
        setOp1.invoke(addRequest, op1);
        setOp2.invoke(addRequest, op2);
        Object addResponse = client.invoke("addFromBean", addRequest)[0];

        Method getReturn = addResponse.getClass().getMethod("getReturn");
        String sresult = getReturn.invoke(addResponse).toString();

        int result = 0;
        try {
            result = Integer.parseInt(sresult);
        } catch (NumberFormatException e) {
            Assert.fail("did not get a add result value: " + sresult);
        }

        Assert.assertEquals(result, ((op1 + op2) * 10) + 1,
                "add result incorrect: " + result + " != ((" + op1 + " + " + op2 + ") * 10) + 1");

    }

}
