/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009-2010 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.itests.cargo.jrmp;

import java.net.URL;

import javax.management.Attribute;
import javax.management.ObjectName;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Tests the JDBC connection leak servlet.
 *
 * @author Florent Benoit
 */
public class JDBCConnectionLeakTest extends TestCommons {

    /**
     * Test that the connection leak closing mechanism is working
     * @throws Exception
     */
    @Test
    public void testConnectionLeak() throws Exception {
        int CONNECTION_LEAKS_REQUEST = 3;

        ObjectName datasourceON =
            new ObjectName("jonas:j2eeType=JCAConnectionFactory,name=jdbc_1,JCAResource=jdbc_1,J2EEServer=jonas");
        ObjectName jndiJDBCInterceptorON = new ObjectName("jonas:type=service,name=jndi-interceptors,interceptor="
            + "org.ow2.jonas.jndi.interceptors.impl.datasource.DataSourceLeakDetectorContextInterceptor");

        // Check there is no busy connections
        Integer currentBusy = (Integer) this.mBeanServerConnection.getAttribute(datasourceON, "currentBusy");
        Assert.assertNotNull(currentBusy);
        Assert.assertEquals(currentBusy.intValue(), 0);

        // Do some connection leaks
        for (int i = 0; i < CONNECTION_LEAKS_REQUEST; i++) {
            hitConnectionLeakServlet();
        }

        // Check busy are still to 0 (automatic close of connection leak is working)
        // Wait a little
        Thread.sleep(2000);
        currentBusy = (Integer) this.mBeanServerConnection.getAttribute(datasourceON, "currentBusy");
        Assert.assertEquals(currentBusy.intValue(), 0);

        // Disable automatic close of connection leaks
        Attribute attribute = new Attribute("forceClose", false);
        this.mBeanServerConnection.setAttribute(jndiJDBCInterceptorON, attribute);

        // Do some connection leaks
        for (int i = 0; i < CONNECTION_LEAKS_REQUEST; i++) {
            hitConnectionLeakServlet();
        }

        // Check that now we have busy connections
        // Wait a little
        Thread.sleep(2000);
        currentBusy = (Integer) this.mBeanServerConnection.getAttribute(datasourceON, "currentBusy");
        Assert.assertEquals(currentBusy.intValue(), CONNECTION_LEAKS_REQUEST);
    }

    /**
     * Make a request and check that the result is OK
     */
    protected void hitConnectionLeakServlet() throws Exception {
        // Connect on the servlet that is making connection leaks
        final URL url = new URL("http://localhost:" + this.webcontainerPort + "/connection-leaks/");
        final String expected = "Example OK";

        String content = null;
        final long stopTime = System.currentTimeMillis() + TestCommons.TIMEOUT * 1000;
        while (System.currentTimeMillis() < stopTime) {
            Thread.sleep(1000);
            content = this.getContentAsString(url);
            if (content.contains(expected)) {
                break;
            }
        }
        // Check expected value
        Assert.assertTrue(content.contains(expected), content + " does not contain " + expected);
    }

}
