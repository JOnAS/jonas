/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2014 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 *  $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.itests.cargo.jrmp;

import java.net.URL;

import junit.framework.Assert;
import org.testng.annotations.Test;

/**
 * Tests that both annotated types and inheriting types are detected by the ServletContainerInitializer
 * @author Loic Albertin
 */
public class OSGiSCITest extends TestCommons {


    /**
     * Tests that both annotated types and inheriting types are detected by the ServletContainerInitializer
     * @throws Exception If anything goes wrong
     */
    @Test
    public void testOSGiServletContextInitializer() throws Exception {
        String result = this.getContentAsString(new URL("http://localhost:" + webcontainerPort + "/osgi-sci/"));
        Assert.assertTrue("ServletContainerInitializer failed.", result.contains("InitializedBy: "));
        Assert.assertTrue("ServletContainerInitializer failed detect annotated type.", result.contains("InitializedBy: org.ow2.jonas.itests.osgi.sci.internal.AnnotationServletContainerInitializer for classes: org.ow2.jonas.itests.osgi.sci.war.AnnotatedModel"));
        Assert.assertTrue("ServletContainerInitializer failed detect inherited type.", result.contains("InitializedBy: org.ow2.jonas.itests.osgi.sci.internal.InheritedServletContainerInitializer for classes: org.ow2.jonas.itests.osgi.sci.war.SCIServlet"));
    }
}
