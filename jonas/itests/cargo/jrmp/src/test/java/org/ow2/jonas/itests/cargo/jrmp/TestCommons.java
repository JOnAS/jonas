/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009-2010 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.itests.cargo.jrmp;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

import javax.management.MBeanServerConnection;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;

/**
 * Common test files.
 *
 * @author S. Ali Tokmen
 */
public class TestCommons {
    /**
     * An {@code HeaderPair} is a basic holder for a connection request header.
     * @author Loic Albertin
     */
    protected class HeaderPair {
        private String key;
        private String value;

        public HeaderPair(String key, String value) {
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public String getValue() {
            return value;
        }
    }

    protected static final int TIMEOUT = 10;

    protected static MBeanServerConnection mBeanServerConnection;

    protected int webcontainerPort;

    @BeforeClass
    public void setUp() throws Exception {
        String webcontainerPort = System.getProperty("webcontainer.port");
        Assert.assertNotNull(webcontainerPort, "webcontainer.port not set!");
        this.webcontainerPort = Integer.parseInt(webcontainerPort);
        if (mBeanServerConnection == null) {
            mBeanServerConnection = getMBeanServerConnection("jonas", "jonas");
        }
    }

    /**
     * Get an MBeanServerConnection using the provided credentials.
     * @param username User name.
     * @param password Password.
     * @return An MBeanServerConnection.
     * @throws Exception If anything fails, for example invalid credentials.
     */
    protected MBeanServerConnection getMBeanServerConnection(final String username, final String password) throws Exception {
        String carolUrl = System.getProperty("carol.url");
        Assert.assertNotNull(carolUrl, "carol.url not set!");

        Map<String, Object> properties = new HashMap<String, Object>(1);
        if (username != null && password != null) {
            properties.put(JMXConnector.CREDENTIALS, new String[] { username, password } );
        }
        JMXServiceURL address = new JMXServiceURL(carolUrl);
        JMXConnector jmxConnector = JMXConnectorFactory.connect(address, properties);
        return jmxConnector.getMBeanServerConnection();
    }

    /**
     * Get the URL content as a String.
     * @param url the url
     * @return the URL content as a String
     * @throws IOException if an I/O exception occurs.
     */
    protected String getContentAsString(final URL url) throws IOException {
        return getContentAsString(url, new HeaderPair[]{});
    }

    /**
     * Get the URL content as a String.
     *
     * @param url the url
     * @param headers request headers pairs
     *
     * @return the URL content as a String
     *
     * @throws IOException if an I/O exception occurs.
     */
    protected String getContentAsString(final URL url, HeaderPair... headers) throws IOException {
        return getContentAsString(url, null, "GET", headers);
    }

    /**
     * Get the URL content as a String. Optionally sending a content string
     *
     * @param url the url
     * @param content Content to send or {@code null} for readonly urls
     * @param requestMethod HTTP Request method for http urls or {@code null} (defaults to {@code GET})
     * @param headers request headers pairs
     * @return the URL content as a String
     * @throws IOException if an I/O exception occurs.
     */
    protected String getContentAsString(final URL url, final String content, final String requestMethod, HeaderPair... headers) throws IOException {
        URLConnection urlConnection = url.openConnection();
        if (headers != null) {
            for (HeaderPair headerPair : headers) {
                urlConnection.addRequestProperty(headerPair.getKey(), headerPair.getValue());
            }
        }
        if (urlConnection instanceof HttpURLConnection && requestMethod != null) {
            ((HttpURLConnection) urlConnection).setRequestMethod(requestMethod);
        }
        if (content != null) {
            urlConnection.setDoOutput(true);
            OutputStreamWriter osw = new OutputStreamWriter(urlConnection.getOutputStream());
            osw.write(content);
            osw.flush();
        }

        final InputStream inStream = urlConnection.getInputStream();
        final ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            int c;
            while ((c = inStream.read()) != -1) {
                bos.write(c);
            }
        } finally {
            inStream.close();
            bos.flush();
        }
        return bos.toString();
    }

    /**
     * Check if needle appears in haystack.
     * @param haystack Haystack to look in.
     * @param needle Needle to look for.
     */
    protected void checkString(final String haystack, final String needle) {
        if (!haystack.contains(needle)) {
            Assert.fail("\"" + needle + "\" not found in \"" + haystack + "\"");
        }
    }

    /**
     * @return jonasBase
     */
    protected String getJOnASBase() {
        return System.getProperty("jonas.base");
    }

}
