/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2013 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 *  $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.itests.cargo.jrmp;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Scanner;

import javax.naming.NamingException;

import org.ow2.jonas.itests.jaxb2.JAXB2Marshaller;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * @author Loic Albertin
 */
public class JAXB2EJBTest extends EJB3TestCommons {


    private String expectedResult;

    private static final String EJB3_MAPPED_NAME = "ejb/test/JAXB2Marshaller";

    private JAXB2Marshaller ejb3Jaxb2Marshaller;


    @Override
    public ClassLoader getEjbClassLoader() {
        try {
            ejb3Jaxb2Marshaller = JAXB2Marshaller.class.cast(getInitialContext().lookup(EJB3_MAPPED_NAME));
        } catch (NamingException e) {
            Assert.fail("Couldn't lookup " + EJB3_MAPPED_NAME, e);
        }
        return ejb3Jaxb2Marshaller.getClass().getClassLoader();
    }

    @BeforeClass
    public void loadExpectedResult() throws IOException, URISyntaxException, NamingException {
        URL url = JAXB2EJBTest.class.getResource("/jaxb2-test-result.xml");
        Assert.assertNotNull(url);
        Scanner scanner = new Scanner(new File(url.toURI())).useDelimiter("\\Z");
        expectedResult = scanner.next();
        scanner.close();
        Assert.assertNotNull(expectedResult);
        expectedResult = expectedResult.trim();
        Assert.assertNotEquals(expectedResult, "");
    }

    @Test
    public void testEJB() {
        String result = ejb3Jaxb2Marshaller.generateXMLString();
        result = result.trim();
        Assert.assertEquals(result, expectedResult);
    }
}
