/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.itests.cargo.jrmp;

import javax.naming.NamingException;

import org.ow2.jonas.tests.applications.jndi.ejb3.Jndi;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Integration tests of the JNDI service
 *
 * @author Jeremy Cazaux
 */
public class JndiTest extends EJB3TestCommons {

    /**
     * The context name of the EJB to lookup
     */
    public final static String JNDI_EJB_KEY = "ejb/test/jndi";

    /**
     * Key of first entry of the sample example
     */
    public final static String SAMPLE_EXAMPLE_JNDI_KEY_1 = "jndi/developer";

    /**
     * Value of the first entry of the sample example
     */
    public final static String SAMPLE_EXAMPLE_JNDI_VALUE_1 = "The JOnAS Team";

    /**
     * Type of the first entry of the sample example
     */
    public final static String SAMPLE_EXAMPLE_JNDI_TYPE_1 = "java.lang.String";

    /**
     * Key of second entry of the sample example
     */
    public final static String SAMPLE_EXAMPLE_JNDI_KEY_2 = "jndi/arch";

    /**
     * Value of the second entry of the sample example
     */
    public final static String SAMPLE_EXAMPLE_JNDI_VALUE_2 = "64";

    /**
     * Type of the second entry of the sample example
     */
    public final static String SAMPLE_EXAMPLE_JNDI_TYPE_2 = "java.lang.Integer";

    /**
     * JNDI key of the empty value example
     */
    public final static String EMPTY_VALUE_EXAMPLE_JNDI_KEY = "test/value/empty";

    /*
     * The remote EJB
     */
    private Jndi ejb;

    @Override
    public ClassLoader getEjbClassLoader() {
        //lookup the EJB from the JNDI context
        this.ejb = null;
        try {
            this.ejb = Jndi.class.cast(getInitialContext().lookup(JNDI_EJB_KEY));
        } catch (NamingException e) {
            Assert.fail("Cannot lookup context " + JNDI_EJB_KEY, e);
        }
        return ejb.getClass().getClassLoader();
    }

    /**
     * JNDI injection test of a sample entry
     */
    @Test
    public void testThatSampleExampleIsOk() throws Exception {
        //test the first entry
        Object val1 = this.ejb.getValue(SAMPLE_EXAMPLE_JNDI_KEY_1);
        Assert.assertEquals(String.valueOf(val1), SAMPLE_EXAMPLE_JNDI_VALUE_1);
        Assert.assertEquals(val1.getClass().getName(), SAMPLE_EXAMPLE_JNDI_TYPE_1);

        //test the second entry
        Object val2 = this.ejb.getValue(SAMPLE_EXAMPLE_JNDI_KEY_2);
        Assert.assertEquals(String.valueOf(val2), SAMPLE_EXAMPLE_JNDI_VALUE_2);
        Assert.assertEquals(val2.getClass().getName(), SAMPLE_EXAMPLE_JNDI_TYPE_2);
    }

    /**
     * Tetst that empty value is Ok
     */
    @Test
    public void testThatEmptyValueIsOk() {
        Object value = this.ejb.getValue(EMPTY_VALUE_EXAMPLE_JNDI_KEY);
        Assert.assertTrue(value != null);
    }

}
