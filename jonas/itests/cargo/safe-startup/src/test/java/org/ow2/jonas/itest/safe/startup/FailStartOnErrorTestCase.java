/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.itest.safe.startup;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;

/**
 * Fail start on error test case
 * @author Jeremy Cazaux
 */
public class FailStartOnErrorTestCase {

    /**
     * One second delay.
     */
    private static final long ONE_SECOND = 1000L;

    /**
     * Max waiting time = 120 seconds.
     */
    private static final long MAX_WAITING_CARGO_RPC = 120 * ONE_SECOND;

    /**
     * JONAS_BASE
     */
    public static final String JONAS_BASE = System.getProperty("jonas.base");

    /**
     * WEB HTTP port 
     */
    public static final int WEB_HTTP_PORT = Integer.parseInt(System.getProperty("webcontainer.port"));

    /**
     * DB port
     */
    public static final int DB_PORT = Integer.parseInt(System.getProperty("db.port"));

    /**
     * JRMP port
     */
    public static final int JRMP_PORT = Integer.parseInt(System.getProperty("jrmp.port"));

    /**
     * JMS port
     */
    public static final int JMS_PORT = Integer.parseInt(System.getProperty("jms.port"));

    /**
     * Smartclient port
     */
    public static final int SMARTCLIENT_PORT = Integer.parseInt(System.getProperty("smartclient.port"));

    /**
     * Fail start on error test case
     */
    @Test
    public void testThatAJOnASInstanceWithAFailedDeloymentIsNotRunning() throws Exception {
        // Wait that CargoRPC.war is deployed
        waitCargoRPCIsDeployed();
        Thread.sleep(20 * ONE_SECOND);
        checkPortAvailability(WEB_HTTP_PORT);
        checkPortAvailability(DB_PORT);
        checkPortAvailability(JRMP_PORT);
        checkPortAvailability(JMS_PORT);
        checkPortAvailability(SMARTCLIENT_PORT);
    }


    /**
     * Wait CargoRPC is deployed.
     */
    private void waitCargoRPCIsDeployed() throws InterruptedException {
        final long maxWaitTime = System.currentTimeMillis() + MAX_WAITING_CARGO_RPC;
        File cargoRPCFile = new File(JONAS_BASE + File.separator + "work" + File.separator + "webapps" + File.separator + "jonas" + File.separator + "single" + File.separator + "cargocpc.war");
        while (!cargoRPCFile.exists() && System.currentTimeMillis() < maxWaitTime) {
            Thread.sleep(ONE_SECOND);
        }
        if (!cargoRPCFile.exists()) {
            throw new IllegalStateException("CargoRPC war has not been deployed. It means that JOnAS hasn't be able to be started");
        }
    }
 
    /**
     * Check port availability
     * @throws Exception
     */
    private void checkPortAvailability(int port) throws Exception  {
        ServerSocket socket = null;
        try {
            socket = new ServerSocket(port);
        } catch (IOException e) {
            throw new Exception("Port " + port + " is not available. The JOnAS instance with a failed " +
                    "deployment is still running", e);
        }
        Assert.assertTrue(socket != null);
    }
}
