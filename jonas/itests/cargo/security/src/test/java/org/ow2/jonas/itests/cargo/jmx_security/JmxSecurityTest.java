/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2010 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.itests.cargo.jmx_security;

import java.lang.reflect.UndeclaredThrowableException;

import javax.management.Attribute;
import javax.management.AttributeNotFoundException;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Tests JMX security.
 *
 * @author S. Ali Tokmen
 */
public class JmxSecurityTest extends TestCommons {

    @Test
    public void connectWithoutCredentials() throws Exception {
        try {
            this.getMBeanServerConnection(null, null);
            Assert.fail("No SecurityException raised");
        } catch (SecurityException expected) {
            // Expected
        }
    }

    @Test
    public void connectWithWrongCredentials() throws Exception {
        try {
            this.getMBeanServerConnection("invalid", "invalid");
            Assert.fail("No SecurityException raised");
        } catch (SecurityException expected) {
            // Expected
        }
    }

    @Test
    public void connectWithReadonlyCredentials() throws Exception {
        MBeanServerConnection mBeanServerConnection = this.getMBeanServerConnection("monitor", "jonas");

        ObjectName cargocpc = new ObjectName(
            "jonas:j2eeType=WebModule,name=//localhost/cargocpc,J2EEApplication=null,J2EEServer=jonas");

        mBeanServerConnection.getAttribute(cargocpc, "cookies");
        try {
            mBeanServerConnection.setAttribute(cargocpc, new Attribute("cookies", Boolean.FALSE));
            Assert.fail("No UndeclaredThrowableException raised");
        } catch (UndeclaredThrowableException e) {
            Assert.assertNotNull(e.getCause());
            Assert.assertEquals(e.getCause().getClass(), IllegalAccessException.class);
        }
        try {
            mBeanServerConnection.invoke(cargocpc, "reload", null, null);
            Assert.fail("No UndeclaredThrowableException raised");
        } catch (UndeclaredThrowableException e) {
            Assert.assertNotNull(e.getCause());
            Assert.assertEquals(e.getCause().getClass(), IllegalAccessException.class);
        }
    }

    @Test
    public void connectWithReadWriteCredentials() throws Exception {
        MBeanServerConnection mBeanServerConnection = this.getMBeanServerConnection("jonas", "jonas");

        ObjectName cargocpc = new ObjectName(
            "jonas:j2eeType=WebModule,name=//localhost/cargocpc,J2EEApplication=null,J2EEServer=jonas");

        mBeanServerConnection.getAttribute(cargocpc, "cookies");
        mBeanServerConnection.setAttribute(cargocpc, new Attribute("cookies", Boolean.FALSE));
        mBeanServerConnection.invoke(cargocpc, "reload", null, null);
    }

    @Test(dependsOnMethods = "connectWithReadonlyCredentials")
    public void testJonas385() throws Exception {
        MBeanServerConnection mBeanServerConnection = this.getMBeanServerConnection("monitor", "jonas");

        ObjectName cargocpc = new ObjectName(
            "jonas:j2eeType=WebModule,name=//localhost/cargocpc,J2EEApplication=null,J2EEServer=jonas");

        try {
            mBeanServerConnection.getAttribute(cargocpc, "attributeThatDoesntExist");
            Assert.fail("No AttributeNotFoundException raised");
        } catch (AttributeNotFoundException expected) {
            // That one was expected
        }
    }

}
