<?xml version="1.0" encoding="UTF-8"?>
<!--
  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  - JOnAS: Java(TM) Open Application Server
  - Copyright (C) 2007-2011 Bull S.A.S.
  - Contact: jonas-team@objectweb.org
  -
  - This library is free software; you can redistribute it and/or
  - modify it under the terms of the GNU Lesser General Public
  - License as published by the Free Software Foundation; either
  - version 2.1 of the License, or any later version.
  -
  - This library is distributed in the hope that it will be useful,
  - but WITHOUT ANY WARRANTY; without even the implied warranty of
  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  - Lesser General Public License for more details.
  -
  - You should have received a copy of the GNU Lesser General Public
  - License along with this library; if not, write to the Free Software
  - Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
  - USA
  -
  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  - $Id$
  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <parent>
    <groupId>org.ow2.jonas</groupId>
    <artifactId>jonas-itests-osgi</artifactId>
    <version>5.3.0-M6-SNAPSHOT</version>
    <relativePath>../pom.xml</relativePath>
  </parent>
  <modelVersion>4.0.0</modelVersion>
  <groupId>org.ow2.jonas</groupId>
  <artifactId>jonas-itests-knopflerfish</artifactId>
  <packaging>pom</packaging>
  <name>JOnAS :: iTests :: OSGi :: Knopflerfish</name>
  <description>Tests the OSGi Bundles of JOnAS with Knopflerfish</description>

  <repositories>
    <repository>
      <id>knopflerfish</id>
      <name>knopflerfish Repository for Maven2</name>
      <url>http://www.knopflerfish.org/maven2</url>
      <releases>
        <enabled>true</enabled>
      </releases>
      <snapshots>
        <enabled>false</enabled>
      </snapshots>
    </repository>
  </repositories>
  
  <build>
    <plugins>
      <!-- Make sure tests always use randomly assigned and available ports -->
      <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>build-helper-maven-plugin</artifactId>
        <version>${build-helper.version}</version>
        <executions>
          <execution>
            <id>generate-port-numbers</id>
            <phase>generate-test-sources</phase>
            <goals>
              <goal>reserve-network-port</goal>
            </goals>
            <configuration>
              <portNames>
                <portName>webcontainer.port</portName>
                <portName>carol.port</portName>
                <portName>jms.port</portName>
                <portName>db.port</portName>
              </portNames>
            </configuration>
          </execution>
        </executions>
      </plugin>

      <!-- Unpack the JOnAS assembly -->
      <plugin>
        <artifactId>maven-dependency-plugin</artifactId>
        <executions>
          <execution>
            <id>unpack-assembly</id>
            <phase>generate-test-resources</phase>
            <goals>
              <goal>unpack</goal>
            </goals>
            <configuration>
              <artifactItems>
                <artifactItem>
                  <groupId>org.ow2.jonas.assemblies.profiles.legacy</groupId>
                  <artifactId>jonas-full</artifactId>
                  <version>${project.version}</version>
                  <classifier>bin</classifier>
                  <type>zip</type>
                </artifactItem>
              </artifactItems>
              <outputDirectory>
                ${project.build.directory}
              </outputDirectory>
              <overWriteReleases>true</overWriteReleases>
              <overWriteSnapshots>true</overWriteSnapshots>
            </configuration>
          </execution>
          <execution>
            <id>unpack-properties-fiile</id>
            <phase>generate-test-resources</phase>
            <goals>
              <goal>unpack</goal>
            </goals>
            <configuration>
              <artifactItems>
                <artifactItem>
                  <groupId>org.ow2.jonas.launchers</groupId>
                  <artifactId>jonas-launcher</artifactId>
                  <version>${project.version}</version>
                </artifactItem>
              </artifactItems>
              <outputDirectory>
                ${project.build.directory}/launcher
              </outputDirectory>
              <overWriteReleases>true</overWriteReleases>
              <overWriteSnapshots>true</overWriteSnapshots>
            </configuration>
          </execution>
        </executions>
      </plugin>

      <!-- Configure JOnAS services and their port numbers -->
      <plugin>
        <artifactId>maven-antrun-plugin</artifactId>
        <executions>
          <execution>
            <id>create-jonas-base-with-all-options</id>
            <phase>generate-test-resources</phase>
            <configuration>
              <tasks>
                <delete dir="${jonas.base}" />
                <mkdir dir="${jonas.base}/deploy" />
                <typedef resource="org/ow2/jonas/antmodular/antlib.xml" classpath="${jonas.root}/lib/common/ow_jonas_ant.jar" />

                <newjonasbase update="off" />
                <jonasProperties services="${jonas.services}" />
                <tomcat7>
                  <connectors>
                    <http port="${webcontainer.port}" />
                  </connectors>
                </tomcat7>
                <jms port="${jms.port}" initialTopics="" initialQueues="" />
                <carol defaultPort="${carol.port}" protocols="jrmp" />
                <db-h2 port="${db.port}" />
                <jdbcRa name="jdbc_1" mapperName="rdb.h2" user="jonas" password="jonas" url="jdbc:h2:tcp://localhost:${db.port}/db_jonas" driverName="org.h2.Driver" jndiName="jdbc_1" autoload="on" />
                <ejb3 ejb3JpaProvider="eclipselink" />
                <replace file="${jonas.base}/conf/easybeans-jonas.xml"  >
                  <replacetoken><![CDATA[useLegacyNamingStrategy="false"]]></replacetoken>
                  <replacevalue><![CDATA[useLegacyNamingStrategy="true"]]></replacevalue>
                </replace>
              </tasks>
            </configuration>
            <goals>
              <goal>run</goal>
            </goals>
          </execution>
        </executions>
      </plugin>

      <!-- Launch OSGi knopflerfish Framework -->
      <plugin>
        <groupId>org.ow2.util.maven.osgi</groupId>
        <artifactId>osgiframework-maven-plugin</artifactId>
        <configuration>
          <frameworkDependencies>
            <dependency>
              <groupId>org.knopflerfish</groupId>
              <artifactId>framework</artifactId>
              <version>5.0.0</version>
            </dependency>
          </frameworkDependencies>     
          <bundleDirectories>
            <param>${jonas.root}/lib/bootstrap/bundles-jsf-2.0</param>
            <param>${jonas.root}/lib/bootstrap/bundles-jpa2.0</param>
            <param>${jonas.root}/lib/bootstrap/bundles-servlets-3.0</param>
          </bundleDirectories>
          <configurationsFiles>
            <file>${project.build.directory}/launcher/org/ow2/jonas/launcher/jonas/javase-profiles.properties</file>
            <file>${project.build.directory}/launcher/org/ow2/jonas/launcher/jonas/defaults.properties</file>
            <file>${project.build.directory}/launcher/org/ow2/jonas/launcher/jonas/gateway.properties</file>
          </configurationsFiles>
          <bundleConfigurationFile>
            ${project.build.directory}/launcher/org/ow2/jonas/launcher/jonas/auto-deploy.properties
          </bundleConfigurationFile>
          <jvmProperties>
            <property>
              <name>jonas.base</name>
              <value>${jonas.base}</value>
            </property>
            <property>
              <name>jonas.root</name>
              <value>${jonas.root}</value>
            </property>
            <property>
              <name>java.security.auth.login.config</name>
              <value>${jonas.base}/conf/jaas.config</value>
            </property>
            <property>
              <name>m2.repository</name>
              <value>${settings.localRepository}</value>
            </property>
            <property>
              <name>java.naming.factory.initial</name>
              <value>
                org.ow2.carol.jndi.spi.MultiOrbInitialContextFactory
              </value>
            </property>
            <property>
              <name>java.security.policy</name>
              <value>${jonas.base}/conf/java.policy</value>
            </property>
            <property>
              <name>java.security.auth.login.config</name>
              <value>${jonas.base}/conf/jaas.config</value>
            </property>
            <property>
              <name>
                javax.security.jacc.PolicyConfigurationFactory.provider
              </name>
              <value>
                org.ow2.jonas.lib.security.jacc.JPolicyConfigurationFactory
              </value>
            </property>
            <property>
              <name>monolog.wrappers</name>
              <value>
                mx4j.log.CommonsLogger,mx4j.log.Logger,java.util.logging.Logger,org.ow2.util.log.JDKLogger,org.ow2.util.log.jul.internal.JDKLogger,
                org.apache.juli.logging.DirectJDKLog,org.ow2.carol.util.configuration.TraceCarol
              </value>
            </property>
            <property>
              <name>java.awt.headless</name>
              <value>true</value>
            </property>
            <property>
              <name>ipojo.log.level</name>
              <value>ERROR</value>
            </property>

            <!-- Kill the JVM before this timeout whatever happens -->
            <property>
              <name>bootstrap.kill.timeout</name>
               <value>420</value>
            </property>
          </jvmProperties>

          <!-- Fork the JVM -->
          <fork>true</fork>

          <!-- JVM will have debug opts -->
          <debug>
            <enabled>${itestsdebug}</enabled>
            <port>8000</port>
          </debug>

          <!-- Wait 45 seconds after the start to ensure all is started -->
          <waitAfterStart>45</waitAfterStart>

          <!-- Wait 10 seconds after the stop to ensure all is stopped -->
          <waitAfterStop>10</waitAfterStop>

          <!-- Delayed the examples start -->
          <delayedBundles>
            <delayedBundlePattern>
              <secondsToWait>20</secondsToWait>
              <pattern>org.ow2.easybeans.examples.*</pattern>
            </delayedBundlePattern>
          </delayedBundles>

          <!-- Add JONAS_BASE/conf -->
          <userClasspath>
            <param>${jonas.base}/conf/</param>
          </userClasspath>

          <modules>
            <module>
              <groupId>org.ow2.easybeans.osgi</groupId>
              <artifactId>easybeans-examples-entitybean</artifactId>
              <version>${easybeans-examples.version}</version>
            </module>

            <module>
              <groupId>org.ow2.easybeans.osgi</groupId>
              <artifactId>easybeans-examples-mdb</artifactId>
              <version>${easybeans-examples.version}</version>
            </module>

            <module>
              <groupId>org.ow2.easybeans.osgi</groupId>
              <artifactId>easybeans-examples-migrationejb21</artifactId>
              <version>${easybeans-examples.version}</version>
            </module>

            <module>
              <groupId>org.ow2.easybeans.osgi</groupId>
              <artifactId>easybeans-example-osgi</artifactId>
              <version>${easybeans-examples.version}</version>
            </module>

            <module>
              <groupId>org.ow2.easybeans.osgi</groupId>
              <artifactId>easybeans-examples-security</artifactId>
              <version>${easybeans-examples.version}</version>
            </module>

            <module>
              <groupId>org.ow2.easybeans.osgi</groupId>
              <artifactId>easybeans-examples-statefulbean</artifactId>
              <version>${easybeans-examples.version}</version>
            </module>

            <module>
              <groupId>org.ow2.easybeans.osgi</groupId>
              <artifactId>easybeans-examples-statelessbean</artifactId>
              <version>${easybeans-examples.version}</version>
            </module>

            <module>
              <groupId>org.ow2.easybeans.osgi</groupId>
              <artifactId>easybeans-examples-timerservice</artifactId>
              <version>${easybeans-examples.version}</version>
            </module>
          </modules>
        </configuration>

        <executions>
          <!-- Start the OSGi framework before running the tests -->
          <execution>
            <id>start-framework</id>
            <phase>pre-integration-test</phase>
            <goals>
              <goal>start</goal>
            </goals>
          </execution>
          <!-- Stop the OSGi framework after the tests -->
          <execution>
            <id>stop-framework</id>
            <phase>post-integration-test</phase>
            <goals>
              <goal>stop</goal>
            </goals>
          </execution>
        </executions>
      </plugin>

      <!-- Launch the tests -->
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <configuration>
          <testClassesDirectory>${basedir}/target</testClassesDirectory>
          <suiteXmlFiles>
            <suiteXmlFile>${basedir}/../src/test/configuration/testng.xml</suiteXmlFile>
          </suiteXmlFiles>
          <systemProperties>
            <property>
              <name>rmi.port</name>
              <value>${carol.port}</value>
            </property>
          </systemProperties>
          <additionalClasspathElements>
            <additionalClasspathElement>${jonas.root}/lib/client.jar</additionalClasspathElement>
          </additionalClasspathElements>
        </configuration>
        <executions>
          <execution>
            <id>testing-jonas-full</id>
            <phase>integration-test</phase>
            <goals>
              <goal>test</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
    </plugins>
  </build>
</project>
