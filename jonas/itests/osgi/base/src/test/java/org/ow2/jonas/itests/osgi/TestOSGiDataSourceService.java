/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 *  $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.itests.osgi;

import org.ow2.jonas.tests.applications.osgi.datasources.IDataSourceService;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Test the OSGi DataSourceService.
 *
 * @author Loic Albertin
 */
public class TestOSGiDataSourceService extends OSGiTestCase {

    /**
     * Reference to the bean.
     */
    private IDataSourceService dataSourceServiceBean = null;

    /**
     * Gets the bean instance.
     *
     * @throws Exception if bean is not obtained
     */
    @BeforeClass
    protected void initBean() throws Exception {
        super.init();
        this.dataSourceServiceBean = (IDataSourceService) getInitialContext().lookup("DataSourceServiceBean");
        System.out.println("dataSourceBean = " + dataSourceServiceBean);
    }


    /**
     * Test default data source creation.
     *
     * @throws Exception if test fails
     */
    @Test
    public void testDefault() throws Exception {
        Assert.assertTrue(dataSourceServiceBean.testDefault(), "failure");
    }

    /**
     * Test data-source caching.
     *
     * @throws Exception if test fails
     */
    @Test
    public void testDSCache() throws Exception {
        Assert.assertTrue(dataSourceServiceBean.testDSCache(), "failure");
    }

    /**
     * Test data-source usage.
     *
     * @throws Exception if test fails
     */
    @Test
    public void testDSUsage() throws Exception {
        Assert.assertTrue(dataSourceServiceBean.testDSUsage(), "failure");
    }
}
