/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 *  $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.itests.osgi;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.ow2.easybeans.osgi.tests.AbsTesting;

/**
 * @author Loic Albertin
 */
public abstract class OSGiTestCase extends AbsTesting {

    /**
     * Default InitialContextFactory to use.
     */
    private static final String DEFAULT_INITIAL_CONTEXT_FACTORY = "org.ow2.carol.jndi.spi.MultiOrbInitialContextFactory";


    /**
     * @return Returns the InitialContext.
     *
     * @throws javax.naming.NamingException If the Context cannot be created.
     */
    protected static Context getInitialContext() throws NamingException {

        // if user don't use jclient/client container
        // we can specify the InitialContextFactory to use
        // But this is *not recommended*.
        Hashtable<String, Object> env = new Hashtable<String, Object>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, getInitialContextFactory());

        // Usually a simple new InitialContext() without any parameters is sufficent.
        // return new InitialContext();

        return new InitialContext(env);
    }

    /**
     * Returns a configurable InitialContextFactory classname.<br/>
     * Can be configured with the <code>easybeans.client.initial-context-factory</code> System property.
     *
     * @return Returns a configurable InitialContextFactory classname.
     */
    private static String getInitialContextFactory() {
        String prop = System.getProperty("easybeans.client.initial-context-factory");
        // If not found, use the default
        if (prop == null) {
            prop = DEFAULT_INITIAL_CONTEXT_FACTORY;
        }
        return prop;
    }
}
