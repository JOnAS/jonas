/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.itests.osgi;

import java.sql.SQLException;

import org.ow2.jonas.tests.applications.osgi.datasources.IDataSource;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Test the OSGi Datasource factories.
 *
 * @author Florent Benoit
 */
public class TestOSGiDataSourceFactory extends OSGiTestCase {

    /**
     * Reference to the bean.
     */
    private IDataSource dataSourceBean = null;

    /**
     * Gets the bean instance.
     *
     * @throws Exception if bean is not obtained
     */
    @BeforeClass
    protected void initBean() throws Exception {
        super.init();
        this.dataSourceBean = (IDataSource) getInitialContext().lookup("DatasourceBean");
        System.out.println("dataSourceBean = " + dataSourceBean);
    }


    /**
     * Test Driver.
     *
     * @throws SQLException if test fails
     */
    @Test
    public void testDriver() throws SQLException {
        Assert.assertTrue(dataSourceBean.testDriver(), "failure");
    }


    /**
     * Test execution of a datasource.
     *
     * @throws SQLException if test fails
     */
    @Test
    public void testDatasource() throws SQLException {
        Assert.assertTrue(dataSourceBean.testDatasource(), "failure");
    }


    /**
     * Test execution of a XA datasource.
     *
     * @throws SQLException if test fails
     */
    @Test
    public void testXADatasource() throws SQLException {
        Assert.assertTrue(dataSourceBean.testXADatasource(), "failure");
    }


    /**
     * Test execution of a Pooled datasource.
     *
     * @throws SQLException if test fails
     */
    @Test
    public void testConnectionPoolDataSourceDatasource() throws SQLException {
        Assert.assertTrue(dataSourceBean.testConnectionPoolDataSourceDatasource(), "failure");
    }

}
