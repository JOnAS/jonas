/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.tests.applications.connectionsleaks;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 * This class is getting a JDBC connection and forgot to close the SQL Connection
 * @author Florent Benoit
 */
public class ConnectionLeakServlet extends HttpServlet {

    /**
     * Serial version UID.
     */
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html><head><title>Connection Leak Servlet</title></head><body>");

        DataSource ds = null;
        try {
            ds = (DataSource) new InitialContext().lookup("jdbc_1");
            ds.getConnection();
            out.println("Example OK");
        } catch (NamingException e) {
            out.println("Cannot lookup get jdbc_1");
            e.printStackTrace();
        } catch (SQLException e) {
            out.println("Cannot get a Connection");
            e.printStackTrace();
        } finally {
            out.println("</body></html>");
            out.close();
        }

    }
}
