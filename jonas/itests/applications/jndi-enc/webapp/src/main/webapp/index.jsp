<%@ page import="javax.naming.InitialContext" %>
<%@ page import="javax.naming.NamingException" %>
<%@ page import="org.ow2.jonas.itests.jndi.enc.ejb.Session" %>
<html>
<body>
<h2>Hello World!</h2>
<%
    boolean failed = false;
    InitialContext ctx = null;
    try {
        ctx = new InitialContext();
    } catch (NamingException e) {
%>
<h3>Failed to create initial context</h3>
<%
        failed = true;
    }

    Session mySession = null;
    String result;
    if (!failed) {
        try {
            mySession = (Session) ctx.lookup("java:global/jndi-enc-application/jndi-enc-ejb/SessionEJB");
            result = mySession.helloWorld("global");
%>
<p>
    <%=result%>
</p>
<%
        } catch (NamingException e) {
%>
<h3>Failed to lookup bean through java:global namespace</h3>
<%
            e.printStackTrace();
            failed = true;
        }
    }
    mySession = null;
    if (!failed) {
        try {
            mySession = (Session) ctx.lookup("java:app/jndi-enc-ejb/SessionEJB");
            result = mySession.helloWorld("app");
%>
<p>
    <%=result%>
</p>
<%
        } catch (NamingException e) {
%>
<h3>Failed to lookup bean through java:app namespace</h3>
<%
            e.printStackTrace();
            failed = true;
        }
    }

    if (!failed) {
%>
<h3>Test succeeded</h3>
<%
    }
%>
</body>
</html>
