/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 *  $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.itests.jndi.enc.war;

import java.io.IOException;

import javax.annotation.Resource;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.itests.jndi.enc.ejb.Session;

/**
 * @author Loic Albertin
 */
public class JndiServlet extends HttpServlet {

    private static final long serialVersionUID = 3078638316636339847L;

    @Resource(lookup = "java:global/jndi-enc-application/jndi-enc-ejb/SessionEJB")
    private Session globalSession;

    @Resource(lookup = "java:app/jndi-enc-ejb/SessionEJB")
    private Session appSession;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.getOutputStream().println("<html><body>");
        boolean failed = false;
        InitialContext ctx = null;
        try {
            ctx = new InitialContext();
        } catch (NamingException e) {
            resp.getOutputStream().println("<h3>Failed to create initial context</h3>");
            failed = true;
        }
        try {
            resp.getOutputStream().println("<h3>App name: " + ctx.lookup("java:app/AppName") + "</h3>");
        } catch (NamingException e) {
            e.printStackTrace();
        }

        String result;
        if (!failed) {
            if (globalSession != null) {
                result = globalSession.helloWorld("global");
                resp.getOutputStream().println("<p>" + result + "</p>");
            } else {
                resp.getOutputStream().println("<h3 > Failed to lookup bean through java: global namespace</h3>");
                failed = true;
            }
        }
        if (!failed) {
            if (appSession != null) {
                result = appSession.helloWorld("app");
                resp.getOutputStream().println("<p>" + result + "</p>");
            } else {
                resp.getOutputStream().println("<h3 > Failed to lookup bean through java: app namespace</h3>");
                failed = true;
            }
        }

        if (!failed) {
            resp.getOutputStream().println("<h3>Test succeeded</h3>");
        }

        resp.getOutputStream().println("</body></html>");
    }
}
