package org.ow2.jonas.itests.jaxrs.impl.war;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Loic Albertin
 */
@ApplicationPath("/web")
public class LibraryApplication extends Application {

    private LibraryResourceImpl libraryResource = new LibraryResourceImpl();

    @Override
    public Set<Object> getSingletons() {
        Set<Object> singletons = new HashSet<Object>();
        singletons.add(libraryResource);
        return singletons;
    }
}
