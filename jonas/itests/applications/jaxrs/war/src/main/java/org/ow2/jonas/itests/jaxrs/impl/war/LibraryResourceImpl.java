package org.ow2.jonas.itests.jaxrs.impl.war;

import org.ow2.jonas.itests.jaxrs.api.LibraryResource;
import org.ow2.jonas.itests.jaxrs.api.beans.LibraryBean;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import java.util.HashMap;
import java.util.HashSet;

/**
 * @author Loic Albertin
 */
public class LibraryResourceImpl implements LibraryResource {

    private static Log logger = LogFactory.getLog(LibraryResourceImpl.class);

    @Context
    private UriInfo uriInfo;

    private HashSet<String> bookNames = new HashSet<String>();

    public LibraryResourceImpl() {
        bookNames.add("Book1");
        bookNames.add("Book2");
        bookNames.add("Book3");
        bookNames.add("Book4");
    }

    @Override
    public Response get() {
        LibraryBean libraryBean = new LibraryBean();
        libraryBean.getBooks().addAll(bookNames);
        return Response.ok(libraryBean).build();
    }

    @Override
    public Response readJSON(HashMap<String, String> input) {
        // Just test if we can map JSON to an HashMap
        logger.warn("retrieved JSON input: {0}", input);
        return Response.ok().build();
    }
}
