/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2014 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 *  $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.itests.jaxrs.api;

import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * A {@code LibraryResource} is a representation of a REST resource for a library
 *
 * @author Loic Albertin
 */
@Path("/library")
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public interface LibraryResource {

    /**
     * @return Returns a {@link javax.ws.rs.core.Response} containing the resource content
     */
    @GET
    Response get();

    /**
     *
     * @param input A JSON input string converted into an HashMap
     * @return an empty {@link javax.ws.rs.core.Response}
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    Response readJSON(HashMap<String, String> input);

}
