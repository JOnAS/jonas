/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2014 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 *  $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.itests.jaxrs.impl.osgi;

import org.ow2.jonas.itests.jaxrs.api.LibraryResource;
import org.ow2.jonas.itests.jaxrs.api.beans.LibraryBean;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import java.util.HashMap;
import java.util.HashSet;

/**
 *
 * @author Loic Albertin
 */
public class LibraryResourceImpl implements LibraryResource {

    private static Log logger = LogFactory.getLog(LibraryResourceImpl.class);

    @Context
    private UriInfo uriInfo;

    private HashSet<String> bookNames = new HashSet<String>();

    /**
     * Default constructor
     */
    public LibraryResourceImpl() {
        bookNames.add("Book1");
        bookNames.add("Book2");
        bookNames.add("Book3");
        bookNames.add("Book4");
    }

    @Override
    public Response get() {
        LibraryBean libraryBean = new LibraryBean();
        libraryBean.getBooks().addAll(bookNames);
        return Response.ok(libraryBean).build();
    }

    @Override
    public Response readJSON(HashMap<String, String> input) {
        // Just test if we can map JSON to an HashMap
        logger.warn("retrieved JSON input: {0}", input);
        return Response.ok().build();
    }
}
