/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.tests.applications.osgi.datasources;

import java.sql.SQLException;

import javax.ejb.Remote;

/**
 * Interface for the EJB3.
 * @author Florent Benoit
 *
 */
@Remote
public interface IDataSource {

    /**
     * Test if driver can be retrieved.
     * @return true if test is ok
     * @throws SQLException if test fails
     */
    boolean testDriver() throws SQLException;

    /**
     * Test execution of a datasource.
     * @return true if test is ok
     * @throws SQLException if test fails
     */
    boolean testDatasource() throws SQLException;

    /**
     * Test execution of a XA datasource.
     * @return true if test is ok
     * @throws SQLException if test fails
     */
    boolean testXADatasource() throws SQLException;

    /**
     * Test execution of a Pooled datasource.
     * @return true if test is ok
     * @throws SQLException if test fails
     */
    boolean testConnectionPoolDataSourceDatasource() throws SQLException;

}
