/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2014 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 *  $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.itests.osgi.sci.war;

import java.io.IOException;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.itests.osgi.sci.api.IHandler;

/**
 * A {@code SCIServlet} is an {@link javax.servlet.http.HttpServlet} that implements {@link org.ow2.jonas.itests.osgi.sci.api.IHandler}.
 * It should appear in the "InitializedBy" attribute of the {@link javax.servlet.ServletContext}.
 *
 * @author Loic Albertin
 */
public class SCIServlet extends HttpServlet implements IHandler {
    private static final long serialVersionUID = 4412234643128656791L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/plain");
        Set<String> initBy = (Set<String>)
                getServletContext().getAttribute("InitializedBy");
        ServletOutputStream sos = resp.getOutputStream();
        if (initBy == null || initBy.isEmpty()) {
            sos.println("Servlet Context not initialized by the OSGi ServletContainerInitializer.");
        } else {
            for (String i : initBy) {
                sos.println("InitializedBy: " + i);
            }
        }
    }
}
