/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2014 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 *  $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.itests.osgi.sci.internal;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletContainerInitializer;

import org.apache.felix.ipojo.annotations.Component;
import org.apache.felix.ipojo.annotations.Instantiate;
import org.apache.felix.ipojo.annotations.Invalidate;
import org.apache.felix.ipojo.annotations.Validate;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

/**
 * A {@code Startup} is a iPOJO component used to register the {@link javax.servlet.ServletContainerInitializer}s
 *
 * @author Loic Albertin
 */
@Component
@Instantiate
public class Startup {

    private BundleContext bundleContext;

    private Set<ServiceRegistration<ServletContainerInitializer>> registrations =
            new HashSet<ServiceRegistration<ServletContainerInitializer>>();

    /**
     * @param bundleContext The {@link BundleContext} used to register the
     *                      {@link org.ow2.jonas.itests.osgi.sci.internal.JServletContainerInitializer}
     */
    public Startup(BundleContext bundleContext) {
        this.bundleContext = bundleContext;
    }

    @Validate
    private void startup() {
        registrations.add(
                bundleContext.registerService(ServletContainerInitializer.class,
                        new AnnotationServletContainerInitializer(), null)
        );
        registrations.add(
                bundleContext.registerService(ServletContainerInitializer.class,
                        new InheritedServletContainerInitializer(), null)
        );
    }

    @Invalidate
    private void shutdown() {
        for (ServiceRegistration<ServletContainerInitializer> registration : registrations) {
            registration.unregister();
        }
        registrations.clear();
    }
}
