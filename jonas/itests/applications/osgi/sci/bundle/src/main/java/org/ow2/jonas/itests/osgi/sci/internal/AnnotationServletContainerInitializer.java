/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2014 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 *  $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.itests.osgi.sci.internal;

import javax.servlet.annotation.HandlesTypes;

import org.ow2.jonas.itests.osgi.sci.api.Handler;

/**
 * A {@code JServletContainerInitializer} is a {@link javax.servlet.ServletContainerInitializer} that handles types annotated with the
 * {@link org.ow2.jonas.itests.osgi.sci.api.Handler} annotation.
 *
 * @author Loic Albertin
 */
@HandlesTypes({Handler.class})
public class AnnotationServletContainerInitializer extends JServletContainerInitializer {

}
