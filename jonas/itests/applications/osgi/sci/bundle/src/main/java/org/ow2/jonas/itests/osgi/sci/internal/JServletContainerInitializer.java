/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2014 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 *  $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.itests.osgi.sci.internal;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

/**
 * A {@code JServletContainerInitializer} is a base class for our {@link javax.servlet.ServletContainerInitializer}s that tracks classes
 * handled by those initializers
 *
 * @author Loic Albertin
 */
public abstract class JServletContainerInitializer implements ServletContainerInitializer {
    @Override
    public void onStartup(Set<Class<?>> classes, ServletContext servletContext) throws ServletException {
        if (classes != null) {
            Set<String> initBy = (Set<String>) servletContext.getAttribute("InitializedBy");
            if (initBy == null) {
                initBy = new HashSet<String>();
                servletContext.setAttribute("InitializedBy", initBy);
            }

            StringBuilder builder = new StringBuilder(this.getClass().getName());
            builder.append(" for classes: ");
            for (Class<?> aClass : classes) {
                builder.append(aClass.getName()).append(" ");
            }
            initBy.add(builder.toString());
        }
    }
}
