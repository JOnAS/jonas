/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2013 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 *  $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.tests.osgi.di;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.url.URLStreamHandlerService;
import org.ow2.easybeans.osgi.annotation.OSGiResource;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

/**
 * @author Loic Albertin
 */
public class OSGiResourceServlet extends HttpServlet {
    private static final long serialVersionUID = -3565609350877196690L;

    private static Log logger = LogFactory.getLog(OSGiResourceServlet.class);

    @OSGiResource
    private BundleContext bundleContext;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (bundleContext == null) {
            logger.error("Bundle Context not injected");
            resp.getOutputStream().println("Bundle Context is null.");
            return;
        }

        ServiceReference<URLStreamHandlerService> urlStreamHandlerServiceReference =
                bundleContext.getServiceReference(URLStreamHandlerService.class);
        if (urlStreamHandlerServiceReference == null) {
            logger.error("Service reference lookup failed.");
            resp.getOutputStream().println("Service reference lookup failed.");
            return;
        }

        URLStreamHandlerService urlStreamHandlerService = bundleContext.getService(urlStreamHandlerServiceReference);
        if (urlStreamHandlerService == null) {
            logger.error("Service lookup failed, URLStreamHandlerService is null");
            resp.getOutputStream().println("Service lookup failed, URLStreamHandlerService is null");
            return;
        }
        logger.warn("URLStreamHandlerService default port: {0}", urlStreamHandlerService.getDefaultPort());
        resp.getOutputStream().println("Bundle Context injection succeeded!");
    }

}
