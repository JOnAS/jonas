/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.test.mutlitenant.writer;

import org.ow2.jonas.test.mutlitenant.Author;


/**
 * Remote interface for the bean Writer.
 * @author JOnAS team
 */
public interface Writer {

    /**
     * Persists a new {@link Author}.
     * @param author {@link Author} to add.
     */
    void addAuthor(final Author author);

    /**
     * Cascade remove an {@link Author}.
     * @param author {@link Author} to be removed.
     */
    void removeAuthor(final Author author);
}
