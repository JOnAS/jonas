/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.tests.applications.classloader;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Component used to check that the Thread context classloader doesn't contain
 * the client.jar
 * @author Florent Benoit
 */
public class ClassLoaderChecker {

    /**
     * Logger.
     */
    private Logger log = Logger.getLogger(ClassLoaderChecker.class.getName());

    /**
     * 
     */
    public void start() throws IOException {
        // Write result into a file in work directory

        String jBase = System.getProperty("jonas.base");
        log.log(Level.INFO, "JONAS_BASE set to '" + jBase + "'.");

        FileWriter writer = null;
        File jFile = new File(jBase + File.separator
                + "classloader-results.txt");
        writer = new FileWriter(jFile);

        try {

            // Get current CL
            ClassLoader currentCL = Thread.currentThread()
            .getContextClassLoader();

            // Expect to miss the client.jar classes but not the bootstrap
            // classes that should be available
            try {
                currentCL
                .loadClass("org.ow2.jonas.deployablemonitor.DeployableMonitorService");
                // problem, write it to the file
                writer.write("KO\n");
            } catch (ClassNotFoundException e) {
                // Not found as expected
                writer.write("OK\n");
            }

            // No test with a bootstrap class
            try {
                currentCL.loadClass("org.ow2.jonas.launcher.jonas.JOnAS");
                // ok, write it to the file
                writer.write("OK\n");
            } catch (ClassNotFoundException e) {
                // Not found as expected
                writer.write("KO\n");
            }
        } finally {
            writer.write("\n");
            writer.flush();
            writer.close();

        }

    }

}
