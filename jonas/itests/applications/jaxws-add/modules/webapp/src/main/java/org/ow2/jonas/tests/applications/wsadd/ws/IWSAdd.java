/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2010 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.tests.applications.wsadd.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;

@WebService(name = "WSAdd", targetNamespace = "http://wsadd.applis.tests.jonas.ow2.org/")
public interface IWSAdd {

    @WebMethod(operationName = "add")
    @RequestWrapper(localName = "addRequest")
    public int add(@WebParam(name = "op1") int op1, @WebParam(name = "op2") int op2);

    public boolean checkEnvEntry();
    public boolean checkEjb();
    public boolean checkResource();

    boolean checkPersistenceContext();

    boolean checkPersistenceUnit();
}
