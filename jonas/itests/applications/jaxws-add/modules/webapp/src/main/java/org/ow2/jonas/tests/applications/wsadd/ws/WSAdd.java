/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 20010 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.tests.applications.wsadd.ws;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.xml.ws.RequestWrapper;

import org.ow2.jonas.tests.applications.wsadd.api.IAddBean;


@WebService(name="WSAdd",targetNamespace="http://wsadd.applis.tests.jonas.ow2.org/") 
public class WSAdd implements IWSAdd {

    @WebMethod(operationName = "add")
    @RequestWrapper(localName = "addRequest")
    public int add(@WebParam(name = "op1") int op1, @WebParam(name = "op2") int op2) {
        return op1 + op2;
    }

    public boolean checkEnvEntry() {
        try {
            InitialContext ic = new InitialContext();
            String envValue = (String) ic.lookup("java:comp/env/location");
            return ("echirolles".equals(envValue));
        } catch (javax.naming.NamingException e) {
            e.printStackTrace();
            return false;
        }

    }

    @EJB
    private IAddBean addBean;
    public boolean checkEjb() {
        int op1 = 10;
        int op2 = 12;
        int res = addBean.addFromBean(op1, op2);
        return (res == (op1 + op2));
    }

    @Resource(mappedName="CF")
    private ConnectionFactory cf;
    public boolean checkResource() {
        try {
            Connection con = cf.createConnection();
            con.close();
            return true;
        } catch (javax.jms.JMSException e) {
            e.printStackTrace();
            return false;
        }
    }

    @PersistenceContext
    private EntityManager entityManager;
    public boolean checkPersistenceContext() {
        return entityManager != null && entityManager.isOpen();
    }

    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;
    public boolean checkPersistenceUnit() {
        return entityManagerFactory != null && entityManagerFactory.isOpen();
    }
    
}
