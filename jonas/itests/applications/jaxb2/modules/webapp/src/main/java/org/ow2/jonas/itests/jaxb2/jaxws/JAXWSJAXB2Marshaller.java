/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2013 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 *  $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.itests.jaxb2.jaxws;

import java.io.StringWriter;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.JAXB;

import org.ow2.jonas.itests.jaxb2.JAXB2Marshaller;
import org.ow2.jonas.itests.jaxb2.TestElementFactory;

/**
 * @author Loic Albertin
 */
@WebService(name = "WSMarshaller", targetNamespace = "http://jonas.ow2.org/itests/jaxb2")
public class JAXWSJAXB2Marshaller implements JAXB2Marshaller {
    @WebMethod(operationName = "generateXMLString")
    @WebResult(partName = "return", targetNamespace = "http://jonas.ow2.org/itests/jaxb2")
    @Override
    public String generateXMLString() {
        StringWriter result = new StringWriter();
        JAXB.marshal(TestElementFactory.createTestElement(), result);
        return result.toString();
    }
}
