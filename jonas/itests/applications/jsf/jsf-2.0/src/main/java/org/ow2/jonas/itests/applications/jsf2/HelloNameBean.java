/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.itests.applications.jsf2;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import java.io.Serializable;


/**
 * Bean with name attribute.
 * @author Florent Benoit
 */
@ManagedBean
@SessionScoped
public class HelloNameBean implements Serializable {

	private static final long serialVersionUID = 123456789L;
	
        //@ManagedProperty(value="Florent")
	private String name;

	public String getName() {
		return name;
	}

	public void setName(final String name) {
            this.name = name;
	}
	
}
