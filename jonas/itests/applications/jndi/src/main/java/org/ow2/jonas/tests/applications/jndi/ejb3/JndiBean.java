/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 20012 Bull S.A.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.tests.applications.jndi.ejb3;

import javax.ejb.Stateless;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Sample EJB to test the JNDI service
 * @author Jeremy Cazaux
 */
@Stateless(mappedName = "ejb/test/jndi")
public class JndiBean implements Jndi {

    /**
     * Logger.
     */
    public static Logger log = Logger.getLogger(JndiBean.class.getName());

    /**
     * Default constructor
     */
    public JndiBean() {
    }

    /**
     * {@inheritDoc}
     */
    public Object getValue(final String key) {

        //create a new InitialContex
        InitialContext initialContext = null;
        try {
            initialContext = new InitialContext();
        } catch (NamingException e) {
            log.log(Level.SEVERE,"Cannot create a new InitialContext", e);
        }

        //lookup of the JNDI entry
        Object value;
        try {
            value = initialContext.lookup(key);
        } catch (NamingException e) {
            log.log(Level.SEVERE, "Cannot lookup the context " + key, e);
            return null;
        }
        return value;
    }
}