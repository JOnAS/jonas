package org.ow2.jonas.test.versioning;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TestServlet extends HttpServlet {

    @EJB
    private TestBean bean;

    private static String version = "2.0.0";

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getSession(true);
        response.setContentType("text/html");
        response.getWriter().println("WAR version: " + version);
        response.getWriter().println("EJB version: " + bean.getVersion());
        response.getWriter().println("Context URL: \"" + this.getServletContext().getContextPath() + "\"");

        // Split output to test JONAS-221
        final int firstDivider = 3;
        final int secondDivider = 6;
        String firstPart = this.getServletContext().getContextPath().substring(0, firstDivider);
        String secondPart = this.getServletContext().getContextPath().substring(firstDivider, secondDivider);
        String thirdPart = this.getServletContext().getContextPath().substring(secondDivider);
        response.getWriter().print("Test image: <img src=\"" + firstPart);
        response.getWriter().write(secondPart.toCharArray());
        response.getWriter().println(thirdPart + "/image.jpg\" />");

        // Output that looks like split but that actually isn't (JONAS-221)
        response.getWriter().print("First part: " + firstPart);
        response.getWriter().println(" ... and no other parts !");
    }
}
