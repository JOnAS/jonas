package org.ow2.jonas.test.versioning;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TestServlet extends HttpServlet {

    @EJB
    private TestBean bean;

    private static String version = "1.0.0";

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getSession(true);
        response.setContentType("text/html");

        // Use response.getOutputStream() to test JONAS-166
        ServletOutputStream output = response.getOutputStream();
        output.println("WAR version: " + version);
        output.println("EJB version: " + bean.getVersion());

        // Write two context paths in order to test when offset is not 0
        StringBuilder sb = new StringBuilder();
        sb.append("Context URL: \"" + this.getServletContext().getContextPath() + "\", ");
        sb.append("Test image: <img src=\"" + this.getServletContext().getContextPath() + "/image.jpg\" />");
        output.println(sb.toString());
    }
}
