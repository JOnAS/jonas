package org.ow2.jonas.test.versioning;

public interface TestBean {

    /**
     * @return Version number.
     */
    String getVersion();

}
