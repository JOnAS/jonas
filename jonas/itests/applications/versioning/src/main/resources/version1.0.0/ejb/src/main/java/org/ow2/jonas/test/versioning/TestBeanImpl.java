package org.ow2.jonas.test.versioning;

import javax.ejb.Stateless;

@Stateless
public class TestBeanImpl implements TestBean {

    private static String version = "1.0.0";

    public String getVersion() {
        return version;
    }

}
