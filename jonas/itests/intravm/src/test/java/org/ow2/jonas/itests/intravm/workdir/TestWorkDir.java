/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2010 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.itests.intravm.workdir;

import java.io.File;
import java.util.Arrays;

import org.ow2.jonas.itests.intravm.JOnASLauncher;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


/**
 * Ensures that the default work dir.
 * @author Florent Benoit
 */
public class TestWorkDir extends JOnASLauncher {

    /**
     * JONAS_BASE.
     */
    private String jonasBase = null;

    /**
     * Init properties.
     */
    @BeforeClass
    public void init() throws Exception {
        this.jonasBase = System.getProperty("jonas.base");
    }

    /**
     * Test that the default JONAS_BASE/work directory is not present on the
     * filesystem.
     * @throws Exception on failure
     */
    @Test
    public void testNotAvailableDefaultWorkDir() throws Exception {
        waitStoppingSoon();
        File defaultWorkDir = new File(jonasBase, "work");
        if (defaultWorkDir.exists()) {
            throw new Exception("The directory '" + defaultWorkDir
                    + "' exists while it shouldn't as a new work directory has been set");
        }

    }

    /**
     * Test that the defined JOnAS work directory is present on the filesystem
     * and contains some files.
     * @throws Exception on failure
     */
    @Test
    public void testExistsNewWorkDir() throws Exception {
        waitStoppingSoon();
        File workDir = new File(jonasBase, "work-tests");
        if (!workDir.exists()) {
            throw new Exception("The directory '" + workDir
                    + "' doesn't exists while it should as a new work directory has been set");
        }
        File[] files = workDir.listFiles();
        if (files.length < 3) {
            throw new Exception("The directory '" + workDir + "' should contains some sub directories. Found '"
                    + Arrays.asList(files) + "'.");
        }

    }

}
