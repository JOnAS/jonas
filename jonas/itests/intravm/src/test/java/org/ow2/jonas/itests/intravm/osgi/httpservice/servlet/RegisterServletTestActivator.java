/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.itests.intravm.osgi.httpservice.servlet;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;
import org.osgi.service.http.HttpService;

/**
 * @author Guillaume Porcher
 */
public class RegisterServletTestActivator implements BundleActivator, ServiceListener {

    BundleContext context;

    String version;

    public void start(final BundleContext context) throws Exception {
        this.version = (String) context.getBundle().getHeaders().get(Constants.BUNDLE_VERSION);
        try {
            this.context = context;

            final ServiceReference ref = this.context.getServiceReference(HttpService.class.getName());
            if (ref != null) {
                this.register(ref);
            }
            this.context.addServiceListener(this, "(objectclass=" + HttpService.class.getName() + ")");
        } catch (final Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
     */
    public void stop(final BundleContext context) throws Exception {
        final ServiceReference ref = this.context.getServiceReference(HttpService.class.getName());
        if (ref != null) {
            this.unregister(ref);
        }
    }

    public void serviceChanged(final ServiceEvent event) {
        if (event.getType() == ServiceEvent.REGISTERED) {
            this.register(event.getServiceReference());
        } else if (event.getType() == ServiceEvent.UNREGISTERING) {
            this.unregister(event.getServiceReference());
        }
    }

    private void register(final ServiceReference serviceReference) {
        try {
            final HttpService httpService = (HttpService) this.context.getService(serviceReference);

            httpService.registerServlet("/servletTest", new SimpleTestServlet(this.version), null, null);
        } catch (final Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    /**
     * @param serviceReference
     */
    private void unregister(final ServiceReference serviceReference) {
        try {
            final HttpService httpService = (HttpService) this.context.getService(serviceReference);
            httpService.unregister("/servletTest");
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }

    }

}
