/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.itests.intravm.osgi.httpservice.servlet;

import java.io.IOException;
import java.io.PrintStream;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Dummy servlet that loads the servlet context
 * @author Florent Benoit
 */
public class MyServlet extends HttpServlet {

    
    /**
     * Serial version UID. 
     */
    private static final long serialVersionUID = 8483263607697552196L;

    /**
     * Servlet Context found in init().
     */
    private ServletContext initServletContext = null;
    
    @Override
    public void init(ServletConfig servletConfig) {
        this.initServletContext = servletConfig.getServletContext();
        System.out.println("init() servletContext = " + initServletContext);
    }

    public ServletContext getInitServletContext() {
        return initServletContext;
    }
    
}

