<?xml version="1.0"?>

<!--
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 - JOnAS: Java(TM) Open Application Server
 - Copyright (C) 1999-2007 Bull S.A.S.
 - Contact: jonas-team@ow2.org
 -
 - This library is free software; you can redistribute it and/or
 - modify it under the terms of the GNU Lesser General Public
 - License as published by the Free Software Foundation; either
 - version 2.1 of the License, or any later version.
 -
 - This library is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 - Lesser General Public License for more details.
 -
 - You should have received a copy of the GNU Lesser General Public
 - License along with this library; if not, write to the Free Software
 - Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 - USA
 -
 - Initial developer(s): Philippe Durieux
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 - $Id$
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 -->

<!--
 - build.xml file for JOnAS (for Ant Version 1.6.5)
 - environment variables used :
 -  JONAS_ROOT
 -  JONAS_BASE
 -->

<project name="jonas"
         default="install"
         basedir="."
         xmlns:jant="http://jonas.objectweb.org/ant">

  <!-- properties -->
  <property environment="myenv" />
  <!-- jonas.root is the directory where this file is located -->
  <dirname property="jonas.root" file="${basedir}/X" />
  <property name="server.name" value="jonas" />

  <!-- tasks -->
  <typedef resource="org/ow2/jonas/ant/antlib.xml"
           classpath="lib/common/ow_jonas_ant.jar"
           uri="http://jonas.objectweb.org/ant" />

  <!-- initialization tasks to be executed before any other target -->
  <target name="init" unless="init_env_already_done">

    <property name="init_env_already_done" value="true" />

    <!-- Set jonas.base and check if the property is defined -->
    <condition property="jonas.base.isset">
      <or>
        <isset property="jonas.base" />
        <isset property="myenv.JONAS_BASE" />
      </or>
    </condition>
    <condition property="jonas.base" value="${myenv.JONAS_BASE}">
      <and>
        <isset property="myenv.JONAS_BASE" />
      </and>
    </condition>
    <condition property="jonas.base" value="${jonas.root}">
      <available file="${jonas.root}" />
    </condition>

    <!-- Test if there is already a jonas.properties in JONAS_BASE/conf -->
    <available file="${jonas.base}/conf/jonas.properties"
               property="jonas.base.alreadyset" />

    <property name="fileName" value="${jonas.base}/ejbjars/sb.jar" />

  </target>

  <target name="removeall">
    <delete>
      <fileset dir="apps">
        <include name="*.ear" />
      </fileset>
      <fileset dir="webapps">
        <include name="*.war" />
      </fileset>
      <fileset dir="ejbjars">
        <include name="*.jar" />
      </fileset>
    </delete>
  </target>


  <target name="update_jonasbase"
          description="update jonas.base with .jar .war and .rar generated in jonas.root"
          depends="init, update_jonasbase_isset, update_jonasbase_notset">
  </target>


  <target name="update_jonasbase_isset" if="jonas.base.isset">

    <copy todir="${jonas.base}/webapps">
      <fileset dir="webapps">
        <include name="juddi.war" />
      </fileset>
    </copy>
    <copy todir="${jonas.base}/webapps/autoload">
      <fileset dir="webapps/autoload">
        <include name="jonasAdmin.war" />
      </fileset>
    </copy>
    <copy todir="${jonas.base}/apps/autoload">
      <fileset dir="apps/autoload">
        <include name="mejb.ear" />
      </fileset>
    </copy>
    <copy todir="${jonas.base}/rars/autoload">
      <fileset dir="rars/autoload">
        <include name="JOnAS_jdbcCP.rar" />
        <include name="JOnAS_jdbcDM.rar" />
        <include name="JOnAS_jdbcDS.rar" />
        <include name="JOnAS_jdbcXA.rar" />
        <include name="joram_ra_for_jonas.rar" />
      </fileset>
    </copy>
    <copy todir="${jonas.base}/rars">
      <fileset dir="rars">
        <include name="speedo_for_jonas_ra.rar" />
      </fileset>
    </copy>

  </target>

  <target name="update_jonasbase_notset" unless="jonas.base.isset">
    <fail message="You have not set your JONAS_BASE. Don't update this directory." />

  </target>



  <target name="create_jonasbase_alreadydefined" if="jonas.base.alreadyset">
    <fail message="You have defined a JONAS_BASE but the directory is already configured. Remove the files before or change your JONAS_BASE." />
  </target>

  <target name="create_jonasbase_notalreadydefined"
          unless="jonas.base.alreadyset">
    <echo message="Creating a JONAS_BASE directory to ${jonas.base} using jonas.root/templates/conf" />
    <mkdir dir="${jonas.base}" />
    <copy todir="${jonas.base}">
      <fileset dir="templates/conf" />
    </copy>
    <antcall target="update_jonasbase" />
  </target>


  <target name="create_jonasbase_isset"
          if="jonas.base.isset"
          depends="create_jonasbase_alreadydefined, create_jonasbase_notalreadydefined">
  </target>

  <target name="create_jonasbase_notset" unless="jonas.base.isset">
    <fail message="You have not set your JONAS_BASE. Don't init this directory." />
  </target>

  <target name="create_jonasbase"
          description="Create a new JONAS_BASE. You have to set JONAS_BASE environment variable."
          depends="init, create_jonasbase_notset, create_jonasbase_isset">
  </target>

  <target name="create_clusterdaemon"
          description="Create a new JONAS_BASE with the required files for the cluster daemon. You have to set JONAS_BASE environment variable."
          depends="create_jonasbase">
          <!-- clean the cluster daemon configuration -->
        <delete includeemptydirs="true">
          <fileset dir="${jonas.base}/apps" />
          <fileset dir="${jonas.base}/clients" />
          <fileset dir="${jonas.base}/ejbjars" />
          <fileset dir="${jonas.base}/lib" />
          <fileset dir="${jonas.base}/rars" />
          <fileset dir="${jonas.base}/webapps" />
          <fileset dir="${jonas.base}/conf" excludes="carol.properties trace.properties clusterd.xml" />
          </delete>
        <touch file="${jonas.base}/conf/jonas.properties"/>
  </target>

  <target name="create_win32service_init">
    <fail message="JONAS_BASE not set. Unable to create Win32 Service."
          unless="jonas.base.isset" />
    <fail unless="jonas.base.alreadyset">
      JONAS_BASE structure is not initialized properly.
      Run ANT create_jonasbase before attempting to create a Win32 Service.
    </fail>

    <!-- verify that JONAS_ROOT (required by jonas.bat) environment variable is set -->
    <condition property="jonas.root.isset">
      <and>
        <isset property="myenv.JONAS_ROOT" />
      </and>
    </condition>
    <fail unless="jonas.root.isset"
          message="JONAS_ROOT environment variable not set." />

    <property name="wrapper.home" value="${myenv.WRAPPER_HOME}" />
    <condition property="wrapper.home.isset">
      <and>
        <isset property="myenv.WRAPPER_HOME" />
      </and>
    </condition>
    <fail unless="wrapper.home.isset"
          message="WRAPPER_HOME environment variable not set." />

    <!-- Set jonas.name property to default in case it is not set by -Djonas.name -->
    <property name="jonas.name" value="jonas" />

    <!-- make sure Java Service Wrapper is installed -->
    <available file="${wrapper.home}/bin/Wrapper.exe"
               property="wrapper.isinstalled" />
    <fail unless="wrapper.isinstalled"
          message="Java Service Wrapper not installed." />

  </target>

  <target name="create_win32service"
          description="Create Win32 Service for the JOnAS server configured by JONAS_BASE."
          depends="init, create_win32service_init">

    <!-- copy Java Service Wrapper files to JONAS_BASE directory -->
    <copy file="${wrapper.home}/lib/wrapper.jar" todir="${jonas.base}/lib" />
    <copy file="${wrapper.home}/lib/Wrapper.DLL" todir="${jonas.base}/lib" />

    <!-- update wrapper_ext.conf for current JONAS_BASE -->
    <exec executable="bin/nt/jonas.bat">
      <arg value="-cfgsvc" />
      <arg file="${jonas.base}/conf/wrapper_ext.conf" />
      <arg value="-n" />
      <arg value="${jonas.name}" />
    </exec>

    <replace file="${jonas.base}/conf/wrapper.conf" token="@jonas.base@" value="${jonas.base}"/>

  </target>

  <target name="win32service"
          description="run the Win32 wrapper command"
          depends="init, create_win32service_init">

    <exec executable="bin/nt/jonasnt.bat" >
      <arg value="${win32param}" />
    </exec>
  </target>


  <target name="install_win32service"
          description="install the Win32 service associated with JONAS_BASE"
          depends="init, create_win32service_init">

    <exec executable="bin/nt/jonasnt.bat" >
      <arg value="install" />
    </exec>
  </target>

  <target name="uninstall_win32service"
          description="uninstall the Win32 service associated with JONAS_BASE"
          depends="init, create_win32service_init">

    <exec executable="bin/nt/jonasnt.bat" >
      <arg value="uninstall" />
    </exec>
  </target>



  <target name="install" description="build all" depends="">
  </target>


  <!-- Start -->
  <target name="start" depends="init" description="Start JOnAS">
    <!--  Some JVM options can be added with jvmOpts="-Dproperty=value" -->
    <jant:jonas jonasroot="${jonas.root}"
           jonasbase="${jonas.base}"
           serverName="${server.name}"
           domainName="defaultDomain"
           mode="start" />
  </target>

  <!-- Start -->
  <target name="startWithDomainName" depends="init" description="Start JOnAS">
    <!--  Some JVM options can be added with jvmOpts="-Dproperty=value" -->
    <jant:jonas jonasroot="${jonas.root}"
           jonasbase="${jonas.base}"
           serverName="${server.name}"
           domainName="${domain.name}"
           mode="start" />
  </target>

  <!-- Stop -->
  <target name="stop" depends="init" description="Stop JOnAS">
    <jant:jonas jonasroot="${jonas.root}"
           jonasbase="${jonas.base}"
           serverName="${server.name}"
           mode="stop">
    </jant:jonas>
  </target>

  <!-- Deploy a file -->
  <target name="deploy" depends="init" description="Deploy a file">
    <jant:serverdeploy action="deploy" source="${fileName}">
      <jonas jonasRoot="${jonas.root}"
             jonasBase="${jonas.base}"
             servername="${server.name}" />
    </jant:serverdeploy>
  </target>

    <!-- Deploy a file -->
  <target name="deployNoInit" description="Deploy a file">
    <jant:serverdeploy action="deploy" source="${fileName}">
      <jonas jonasRoot="${jonas.root}"
             jonasBase="${jonas.base}"
             servername="${server.name}" />
    </jant:serverdeploy>
  </target>

  <!-- List beans -->
  <target name="list" depends="init" description="List name of beans">
    <jant:serverdeploy action="list" source="${fileName}">
      <jonas jonasRoot="${jonas.root}"
             jonasBase="${jonas.base}"
             servername="${server.name}" />
    </jant:serverdeploy>
  </target>

  <!-- Undeploy a file -->
  <target name="undeploy" depends="init" description="Undeploy a file">
    <jant:serverdeploy action="undeploy" source="${fileName}">
      <jonas jonasRoot="${jonas.root}"
             jonasBase="${jonas.base}"
             servername="${server.name}" />
    </jant:serverdeploy>
  </target>

  <!-- Check Environment -->
  <target name="check" depends="init" description="Check JOnAS environment">
    <jant:jonas jonasroot="${jonas.root}"
           jonasbase="${jonas.base}"
           mode="check" />
  </target>

  <!-- Ping JOnAS server -->
  <target name="ping" depends="init" description="Ping JOnAS server">
    <jant:jonas jonasroot="${jonas.root}" jonasbase="${jonas.base}" mode="ping" />
  </target>

  <target name="pingName" depends="init" description="Ping JOnAS server">
    <jant:jonas jonasroot="${jonas.root}" jonasbase="${jonas.base}" servername="${server.name}" mode="ping" />
  </target>

  <!-- Print version -->
  <target name="version" depends="init" description="Display JOnAS version">
    <jant:jonas jonasroot="${jonas.root}" mode="version" />
  </target>

  <!-- JOnAS JNDI tree -->
  <target name="jndi" depends="init" description="JOnAS JNDI tree">
    <jant:jonas jonasroot="${jonas.root}" jonasbase="${jonas.base}" mode="jndi" />
  </target>

  <!-- deploy file, list beans, undeploy it -->
  <target name="deploy_list_undeploy"
          depends="init, deploy,list,undeploy"
          description="deploy/undeploy on JOnAS">
  </target>

  <!-- Start and deploy a file -->
  <target name="start_deploy"
          depends="init"
          description="Start JOnAS and deploy a file">
    <parallel>
      <antcall target="start" />
      <sequential>
        <!-- wait end of JOnAS start -->
        <antcall target="ping" />
        <!-- then deploy file -->
        <antcall target="deploy" />
      </sequential>
    </parallel>
  </target>


  <!-- Start JOnAS, deploy a file, list beans, undeploy it and stop JOnAS -->
  <target name="demo">
    <parallel>
      <antcall target="start" />
      <sequential>
        <!-- wait end of JOnAS start -->
        <antcall target="ping" />
        <!-- do some actions -->
        <antcall target="deploy_list_undeploy" />
        <!-- stop the server -->
        <antcall target="stop" />
      </sequential>
    </parallel>
  </target>

</project>
