/**
 * JOnAS : Java(TM) OpenSource Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.generators.wsgen.generator.ews.wsdltoj2ee.writer;

import java.io.IOException;
import java.util.Vector;

import javax.xml.namespace.QName;

import org.w3c.dom.Node;

import org.apache.axis.wsdl.gen.Generator;
import org.apache.axis.wsdl.symbolTable.SchemaUtils;
import org.apache.axis.wsdl.symbolTable.SymbolTable;
import org.apache.axis.wsdl.symbolTable.Type;
import org.apache.axis.wsdl.symbolTable.TypeEntry;
import org.apache.axis.wsdl.toJava.Emitter;
import org.apache.axis.wsdl.toJava.JavaGeneratorFactory;
import org.apache.axis.wsdl.toJava.JavaTypeWriter;
import org.apache.axis.wsdl.toJava.Utils;

/**
 * This is Wsdl2java's Type Writer.  It writes the following files, as appropriate:
 * <typeName>.java, <typeName>Holder.java.
 */
public class JOnASTypeWriter extends JavaTypeWriter implements Generator {

    /** Field typeWriter */
    private Generator jonasTypeWriter = null;

    /**
     * Constructor.
     *
     * @param emitter the Emitter
     * @param type Type to be generated
     * @param symbolTable SymbolTable containing mapping informations
     */
    public JOnASTypeWriter(Emitter emitter, TypeEntry type,
                          SymbolTable symbolTable) {
        super(emitter, type, symbolTable);
        if (type.isReferenced() && !type.isOnlyLiteralReferenced() && type instanceof Type) {

            // Determine what sort of type this is and instantiate
            // the appropriate Writer.
            Node node = type.getNode();

            boolean isSimpleList = SchemaUtils.isListWithItemType(node);
            // If it's an array, don't emit a class
            if (!type.getName().endsWith("[]") && !isSimpleList) {

                // Generate the proper class for either "complex" or "enumeration" types
                Vector v = Utils.getEnumerationBaseAndValues(node, symbolTable);

                if (v == null) {
                    // that's not an enumeration
                    TypeEntry base =
                            SchemaUtils.getComplexElementExtensionBase(node,
                                    symbolTable);

                    if (base == null) {
                        base = SchemaUtils.getComplexElementRestrictionBase(
                                node, symbolTable);
                    }

                    if (base == null) {
                        QName baseQName = SchemaUtils.getSimpleTypeBase(node);

                        if (baseQName != null) {
                            base = symbolTable.getType(baseQName);
                        }
                    }
                    Vector elements = type.getContainedElements();
                    Vector attributes = type.getContainedAttributes();

                    // If this complexType is referenced in a
                    // fault context, emit a bean-like exception
                    // class
                    Boolean isComplexFault = (Boolean) type.getDynamicVar(
                            JavaGeneratorFactory.COMPLEX_TYPE_FAULT);

                    if ((isComplexFault != null) && isComplexFault.booleanValue()) {

                        jonasTypeWriter = getBeanHelperWriter(emitter, type, elements, base, attributes, true);
                    } else {
                        jonasTypeWriter = getBeanHelperWriter(emitter, type, elements, base, attributes, false);
                    }
                }
            }
        }
    }    // ctor

    /**
     * Write all the service bindnigs:  service and testcase.
     *
     * @throws IOException if generation of Helper fails
     */
    public void generate() throws IOException {

        if (jonasTypeWriter != null) {
            jonasTypeWriter.generate();
        }
    }

}    // class JavaTypeWriter
