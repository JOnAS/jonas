/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Xavier Delplanque
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.generators.wsgen.generator.axis;

import javax.xml.namespace.QName;

/**
 * Member of a VelocityContext. Contains information used to create a
 * beanMapping WSDD tag.
 *
 * @author Guillaume SAUTHIER
 */
public class VcBeanMapping extends VcTypeMapping {

    /**
     * Axis bean serializer factory
     */
    private static final String BEAN_SERIALIZER_FACTORY = "org.apache.axis.encoding.ser.BeanSerializerFactory";

    /**
     * Axis bean deserializer factory
     */
    private static final String BEAN_DESERIALIZER_FACTORY = "org.apache.axis.encoding.ser.BeanDeserializerFactory";

    /**
     * Create a VcBeanMapping holding typeMapping information.
     *
     * @param xml XML Qname of the type
     * @param name Java name of the type
     */
    public VcBeanMapping(QName xml, String name) {
        super(xml, name);
    }

    /**
     * @return Returns the serializer factory for the mapping
     */
    public String getSerializerFactory() {
        return BEAN_SERIALIZER_FACTORY;
    }

    /**
     * @return Returns the deserializer factory for the mapping
     */
    public String getDeserializerFactory() {
        return BEAN_DESERIALIZER_FACTORY;
    }
}