/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.generators.wsgen.generator.ews.wsdltoj2ee;

import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.ws.ews.context.webservices.client.ServiceReferenceContext;
import org.apache.ws.ews.context.webservices.server.WSCFHandler;

/**
 * Implementation of EWS ServiceReferenceContext for JOnAS.
 *
 * @author Guillaume Sauthier
 */
public class ServiceReferenceContextImpl implements ServiceReferenceContext {

    /**
     * The service [to be used] QName
     */
    private QName serviceQName = null;

    /**
     * handlers to be applied for this service-ref
     */
    private List handlers = null;

    /**
     * Default constructor
     */
    public ServiceReferenceContextImpl() {
        super();
        handlers = new ArrayList();
    }

    /**
     * @deprecated not used by JOnAS
     * @see org.apache.ws.ews.context.webservices.client.ServiceReferenceContext#setServiceInterface(java.lang.String)
     */
    public void setServiceInterface(String arg0) {

    }

    /**
     * @deprecated not used by JOnAS
     * @see org.apache.ws.ews.context.webservices.client.ServiceReferenceContext#getServiceInterface()
     */
    public String getServiceInterface() {
        return null;
    }

    /**
     * @deprecated not used by JOnAS
     * @see org.apache.ws.ews.context.webservices.client.ServiceReferenceContext#getWsdlFile()
     */
    public String getWsdlFile() {
        return null;
    }

    /**
     * @deprecated not used by JOnAS
     * @see org.apache.ws.ews.context.webservices.client.ServiceReferenceContext#setWsdlFile(java.lang.String)
     */
    public void setWsdlFile(String arg0) {

    }

    /**
     * Set the QName of the service to be used
     * @see org.apache.ws.ews.context.webservices.client.ServiceReferenceContext#setServiceQName(javax.xml.namespace.QName)
     */
    public void setServiceQName(QName serviceQName) {
        this.serviceQName = serviceQName;
    }

    /**
     * Returns the QName of the service to be used
     * @see org.apache.ws.ews.context.webservices.client.ServiceReferenceContext#getServiceQName()
     */
    public QName getServiceQName() {
        return this.serviceQName;
    }

    /**
     * @deprecated not used by JOnAS
     * @see org.apache.ws.ews.context.webservices.client.ServiceReferenceContext#getJaxrpcMappingFile()
     */
    public String getJaxrpcMappingFile() {
        return null;
    }

    /**
     * @deprecated not used by JOnAS
     * @see org.apache.ws.ews.context.webservices.client.ServiceReferenceContext#setJaxrpcMappingFile(java.lang.String)
     */
    public void setJaxrpcMappingFile(String arg0) {

    }

    /**
     * @deprecated not used by JOnAS
     * @see org.apache.ws.ews.context.webservices.client.ServiceReferenceContext#getServiceRefName()
     */
    public String getServiceRefName() {
        return null;
    }

    /**
     * @deprecated not used by JOnAS
     * @see org.apache.ws.ews.context.webservices.client.ServiceReferenceContext#setServiceRefName(java.lang.String)
     */
    public void setServiceRefName(String arg0) {

    }

    /**
     * Return the array of Handlers to be pplied for this service-ref
     * @see org.apache.ws.ews.context.webservices.client.ServiceReferenceContext#getHandlers()
     */
    public WSCFHandler[] getHandlers() {
        WSCFHandler[] handlersArray = new WSCFHandler[handlers.size()];
        return (WSCFHandler[]) handlers.toArray(handlersArray);
    }

    /**
     * Add a given handler to the list of applied handlers
     * @param handler the added handler
     * @see org.apache.ws.ews.context.webservices.client.ServiceReferenceContext#addHandler(org.apache.ws.ews.context.webservices.server.WSCFHandler)
     */
    public void addHandler(WSCFHandler handler) {
        this.handlers.add(handler);
    }

}
