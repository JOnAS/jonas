/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2003-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.generators.wsgen.modifier;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.jar.Attributes;

import org.objectweb.util.monolog.api.BasicLevel;

import org.ow2.jonas.Version;
import org.ow2.jonas.deployment.ws.ServiceDesc;
import org.ow2.jonas.deployment.ws.ServiceRefDesc;
import org.ow2.jonas.generators.genbase.GenBaseException;
import org.ow2.jonas.generators.genbase.archive.Archive;
import org.ow2.jonas.generators.genbase.archive.WebApp;
import org.ow2.jonas.generators.genbase.modifier.ArchiveModifier;
import org.ow2.jonas.generators.wsgen.ddmodifier.ContextDDModifier;
import org.ow2.jonas.generators.wsgen.ddmodifier.WebJettyDDModifier;
import org.ow2.jonas.generators.wsgen.ddmodifier.WebServicesDDModifier;
import org.ow2.jonas.generators.wsgen.ddmodifier.WsClientDDModifier;
import org.ow2.jonas.generators.wsgen.ddmodifier.WsEndpointDDModifier;
import org.ow2.jonas.generators.wsgen.generator.Generator;
import org.ow2.jonas.generators.wsgen.generator.GeneratorFactory;
import org.ow2.jonas.generators.wsgen.generator.SecurityGenerator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Modify a given WebApp.
 *
 * @author Guillaume Sauthier
 */
public class WebAppModifier extends ArchiveModifier {

    /** webapp */
    private WebApp web;

    /**
     * Creates a new WebAppModifier
     *
     * @param webapp
     *            the WebApp J2EE archive
     */
    public WebAppModifier(WebApp webapp) {
        super(webapp);
        web = webapp;
    }

    /**
     * Modify the current archive and return a modified archive.
     *
     * @return a modified archive.
     *
     * @throws GenBaseException
     *             When modification fails
     */
    public Archive modify() throws GenBaseException {

        getLogger().log(BasicLevel.INFO, "Processing WebApp " + web.getName());

        // Update MANIFEST with Version Number
        // used to say if WsGen has already been applied to this Module
        Attributes main = this.web.getManifest().getMainAttributes();
        main.put(new Attributes.Name(WsGenModifierConstants.WSGEN_JONAS_VERSION_ATTR), Version.getNumber());

        GeneratorFactory gf = GeneratorFactory.getInstance();
        Document jwebapp = web.getJonasWebAppDoc();
        Document webapp = web.getWebAppDoc();
        Document webservices = web.getWebservicesDoc();
        Document jonasWebservices = web.getJonasWebservicesDoc();

        List refs = web.getServiceRefDescs();

        for (Iterator i = refs.iterator(); i.hasNext();) {
            ServiceRefDesc ref = (ServiceRefDesc) i.next();

            // create dd modifier
            Element base = null;
            if (jwebapp != null) {
                base = jwebapp.getDocumentElement();
            }
            WsClientDDModifier ddm = new WsClientDDModifier(ref.getServiceRefName(), jwebapp, base);

            // launch generation
            Generator g = gf.newGenerator(ref, ddm, web);
            g.generate();
            g.compile();

            // add files in web archive
            g.addFiles(web);

            // update the Document if it was created
            // in the meantime
            jwebapp = ddm.getDocument();

        }

        // create dd modifier
        WsEndpointDDModifier ddm = new WsEndpointDDModifier(webapp);
        WebServicesDDModifier wsddm = null;
        if (webservices != null) {
            wsddm = new WebServicesDDModifier(webservices);
        }

        List sds = web.getServiceDescs();

        for (Iterator i = sds.iterator(); i.hasNext();) {
            ServiceDesc sd = (ServiceDesc) i.next();

            // launch generation
            Generator g = gf.newGenerator(sd, ddm, wsddm, web);
            g.generate();
            g.compile();

            // add files in web archive
            g.addFiles(web);

            //Now generate the security for the web app
            Document context = web.getContextDoc();
            if (context == null) {
                context = web.newContextDoc();
            }
            ContextDDModifier cddm = new ContextDDModifier(context);
            Document webJetty = web.getWebJettyDoc();
            if (webJetty == null) {
                webJetty = web.newWebJettyDoc();
            }
            WebJettyDDModifier wjddm = new WebJettyDDModifier(webJetty);
            SecurityGenerator  sm = new SecurityGenerator(jonasWebservices);
            sm.generate(ddm, cddm, wjddm);

        }

        return save(gf.getConfiguration(), "webapps" + File.separator
                + web.getName());
    }
}