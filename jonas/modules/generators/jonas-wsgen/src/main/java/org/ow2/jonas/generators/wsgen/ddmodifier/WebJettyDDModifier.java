/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial Developer: Matt Wringe
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.generators.wsgen.ddmodifier;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Modify the web-jetty.xml file. Wrapper around a web.xml DOM
 *
 * @author Matt Wringe
 */
public class WebJettyDDModifier extends DeploymentDescModifier {

    /** the name of the class that sets the realm for Jetty */
    private static final String REALM_CLASS = "org.ow2.jonas.web.jetty6.security.Realm";

    /** the name of the class that sets the context for Jetty */
    private static final String CONTEXT_CLASS = "org.mortbay.jetty.webapp.WebAppContext";

    /** Default Realm name is no name is specified */
    private static final String DEFAULT_REALM_NAME = "Endpoint Authentication Area";

    /**
     * Create a new WebJettyDDModifier
     *
     * @param doc the context.xml Document
     */
    public WebJettyDDModifier(Document doc) {
        super(doc.getDocumentElement(), doc);
    }

    /**
     * Sets up the realm for Jetty to use
     *
     * @param realm the realm to use
     */
    public void configRealm (String realm) {
        configRealm (DEFAULT_REALM_NAME, realm);
    }

    /**
     * Sets up the realm for Jetty to use
     *
     * @param realmName the realm name for the realm
     * @param realm the realm to use
     */
    public void configRealm(String realmName, String realm) {
        Element configureElement = getElement();
        configureElement.appendChild(setRealmName(realmName));
        configureElement.appendChild(setRealm(realm, realmName));
    }


    /**
     * Returns an element that defines the realm name to use
     *
     * @param realmName the name of the realm
     * @return an element that defines the realm name
     */
    private Element setRealmName (String realmName) {
        Element callElement = newElement("Call");
        callElement.setAttribute("name", "setRealmName");

        Element argElement = newElement("Arg", realmName);
        callElement.appendChild(argElement);

        return callElement;
    }

    /**
     * Returns an element that defines which realm to use
     *
     * @param realm the realm
     * @param realmName the name of the realm
     * @return an element that defines which realm to use
     */
    private Element setRealm (String realm, String realmName) {
        Element callElement = newElement ("Call");
        callElement.setAttribute("name", "setRealm");

        Element argElement = newElement ("Arg");

        Element newElement = newElement ("New");
        newElement.setAttribute("class", REALM_CLASS);

        Element argRealmName = newElement ("Arg", realmName);
        Element argRealm = newElement ("Arg", realm);

        newElement.appendChild(argRealmName);
        newElement.appendChild(argRealm);

        argElement.appendChild(newElement);
        callElement.appendChild(argElement);

        return callElement;
    }
}
