/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2003-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.generators.wsgen.modifier;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.jar.Attributes;

import org.ow2.jonas.Version;
import org.ow2.jonas.deployment.ws.ServiceRefDesc;
import org.ow2.jonas.generators.genbase.GenBaseException;
import org.ow2.jonas.generators.genbase.archive.Archive;
import org.ow2.jonas.generators.genbase.archive.Client;
import org.ow2.jonas.generators.genbase.modifier.ArchiveModifier;
import org.ow2.jonas.generators.wsgen.ddmodifier.WsClientDDModifier;
import org.ow2.jonas.generators.wsgen.generator.Generator;
import org.ow2.jonas.generators.wsgen.generator.GeneratorFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;



import org.objectweb.util.monolog.api.BasicLevel;

/**
 * Modify a given Client.
 *
 * @author Guillaume Sauthier
 */
public class ClientModifier extends ArchiveModifier {

    /** client */
    private Client client;

    /**
     * Creates a new ClientModifier object.
     *
     * @param client Client Archive
     */
    public ClientModifier(Client client) {
        super(client);
        this.client = client;
    }

    /**
     * Modify the current archive and return a modified archive.
     *
     * @return a modified archive.
     *
     * @throws GenBaseException When Client Generation or storing phase fails.
     */
    public Archive modify() throws GenBaseException {

        getLogger().log(BasicLevel.INFO, "Processing Client " + client.getName());

        // Update MANIFEST with Version Number
        // used to say if WsGen has already been applied to this Module
        Attributes main = this.client.getManifest().getMainAttributes();
        main.put(new Attributes.Name(WsGenModifierConstants.WSGEN_JONAS_VERSION_ATTR), Version.getNumber());

        GeneratorFactory gf = GeneratorFactory.getInstance();
        Document jclient = client.getJonasClientDoc();

        List refs = client.getServiceRefDescs();
        for (Iterator i = refs.iterator(); i.hasNext();) {
            ServiceRefDesc ref = (ServiceRefDesc) i.next();

            // create dd modifier
            Element base = null;
            if (jclient != null) {
                base = jclient.getDocumentElement();
            }
            WsClientDDModifier ddm = new WsClientDDModifier(ref.getServiceRefName(), jclient, base);

            // launch generation
            Generator g = gf.newGenerator(ref, ddm, client);
            g.generate();
            g.compile();
            // add files in web archive
            g.addFiles(client);

            // update the Document if it was created
            // in the meantime
            jclient = ddm.getDocument();
        }

        return save(gf.getConfiguration(), "clients" + File.separator + client.getRootFile().getName());
    }
}