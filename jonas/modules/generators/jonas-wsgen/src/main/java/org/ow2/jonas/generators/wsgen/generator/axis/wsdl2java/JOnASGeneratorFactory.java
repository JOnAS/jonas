/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.generators.wsgen.generator.axis.wsdl2java;

import javax.wsdl.Definition;
import javax.wsdl.Service;

import org.apache.axis.wsdl.gen.Generator;
import org.apache.axis.wsdl.gen.NoopGenerator;
import org.apache.axis.wsdl.symbolTable.ServiceEntry;
import org.apache.axis.wsdl.symbolTable.SymbolTable;
import org.apache.axis.wsdl.toJava.Emitter;
import org.apache.axis.wsdl.toJava.JavaDefinitionWriter;
import org.apache.axis.wsdl.toJava.JavaGeneratorFactory;

/**
 * <code>JOnASGeneratorFactory</code> returns our own JOnASServiceWriter in WSDL2Java process.
 *
 * @author Guillaume Sauthier
 */
public class JOnASGeneratorFactory extends JavaGeneratorFactory {

    /**
     * Constructs a new JOnASGeneratorFactory associated with a given
     * JOnASEmitter
     * @param emitter the JOnASEmitter
     */
    public JOnASGeneratorFactory(Emitter emitter) {
        super(emitter);
    }

    /**
     * Method addDefinitionGenerators
     */
    protected void addDefinitionGenerators() {
        addGenerator(Definition.class, JavaDefinitionWriter.class); // for
        // faults
    } // addDefinitionGenerators

    /**
     * @param service wsdl:service instance
     * @param symbolTable SymbolTable
     * @return Returns the JOnASServiceWriter when encoutering Service in
     *         SymbolTable
     */
    public Generator getGenerator(Service service, SymbolTable symbolTable) {
        if (include(service.getQName())) {
            Generator writer = new JOnASServiceWriter(emitter, service, symbolTable);
            ServiceEntry sEntry = symbolTable.getServiceEntry(service.getQName());
            serviceWriters.addStuff(writer, sEntry, symbolTable);
            return serviceWriters;
        } else {
            return new NoopGenerator();
        }
    } // getGenerator

    /**
     * Method getGenerator
     *
     * @param type TypeEntry
     * @param symbolTable SymbolTable
     * @return a NoopGenerator (we don't need Types at this time, should be already generated)
     */
    /*
    public Generator getGenerator(TypeEntry type, SymbolTable symbolTable) {
        return new NoopGenerator();
    }    // getGenerator
*/
}