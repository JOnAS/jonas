/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.generators.wsgen.finder;

import java.util.Iterator;

import org.ow2.jonas.generators.genbase.archive.Application;
import org.ow2.jonas.generators.genbase.archive.Client;
import org.ow2.jonas.generators.genbase.archive.EjbJar;
import org.ow2.jonas.generators.genbase.archive.J2EEArchive;
import org.ow2.jonas.generators.genbase.archive.WebApp;


/**
 * <code>GeneralWSFinder</code> is a wrapper for ear and other J2EE module types.
 *
 * @author Guillaume Sauthier
 */
public class GeneralWSFinder implements J2EEWebServicesFinder {

    /**
     * Explored archive.
     */
    private J2EEArchive archive = null;

    /**
     * Creates a WSFinder suitable for all types of modules.
     * @param archive Explored archive.
     */
    public GeneralWSFinder(J2EEArchive archive) {
        super();
        this.archive = archive;
    }

    /**
     * @see org.ow2.jonas.generators.wsgen.finder.J2EEWebServicesFinder#find()
     */
    public boolean find() {

        if (archive instanceof Application) {
            // explore sub modules
            Application app = (Application) archive;

            // If any ejbjar has a webservice, return directly
            for (Iterator i = app.getEjbJars(); i.hasNext();) {
                EjbJar ejb = (EjbJar) i.next();
                if (findWebServicesInEjbJar(ejb)) {
                    return true;
                }
            }

            // If any webapp has a webservice, return directly
            for (Iterator i = app.getWebApps(); i.hasNext();) {
                WebApp web = (WebApp) i.next();
                if (findWebServicesInWebApp(web)) {
                    return true;
                }
            }

            //If any client has a webservice, return directly
            for (Iterator i = app.getClients(); i.hasNext();) {
                Client client = (Client) i.next();
                if (findWebServicesInClient(client)) {
                    return true;
                }
            }

        } else if (archive instanceof EjbJar) {
            return findWebServicesInEjbJar((EjbJar) archive);
        } else if (archive instanceof WebApp) {
            return findWebServicesInWebApp((WebApp) archive);
        } else if (archive instanceof Client) {
            return findWebServicesInClient((Client) archive);
        }
        return false;
    }

    /**
     * @param archive The archive to explore.
     * @return Returns <code>true</code> if the Client has WebServices.
     */
    private static boolean findWebServicesInClient(Client archive) {
        J2EEWebServicesFinder finder = new GenericWSFinder(archive, "META-INF/application-client.xml");
        return finder.find();
    }

    /**
     * @param archive The archive to explore.
     * @return Returns <code>true</code> if the WebApp has WebServices.
     */
    private static boolean findWebServicesInWebApp(WebApp archive) {
        J2EEWebServicesFinder finder = new GenericWSFinder(archive, "WEB-INF/web.xml");
        return finder.find();
    }

    /**
     * @param archive The archive to explore.
     * @return Returns <code>true</code> if the EjbJar has WebServices.
     */
    private static boolean findWebServicesInEjbJar(EjbJar archive) {
        J2EEWebServicesFinder finder = new GenericWSFinder(archive, "META-INF/ejb-jar.xml");
        return finder.find();
    }


}
