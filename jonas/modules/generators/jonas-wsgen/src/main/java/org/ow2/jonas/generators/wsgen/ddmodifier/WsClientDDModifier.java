/**
 * JOnAS : Java(TM) OpenSource Application Server
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial Developer : Guillaume Sauthier
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.generators.wsgen.ddmodifier;

import org.ow2.jonas.generators.genbase.utils.XMLUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;



/**
 * Modify a Web Services Client Deployment Desc Element. Wrapper around a
 * <code>jonas-service-ref</code> element
 * @author Guillaume Sauthier
 */
public class WsClientDDModifier extends DeploymentDescModifier {

    /**
     * jonas-init-param Element name
     */
    private static final String JONAS_INIT_PARAM = "jonas-init-param";

    /**
     * param-name Element name
     */
    private static final String PARAM_NAME = "param-name";

    /**
     * param-value Element name
     */
    private static final String PARAM_VALUE = "param-value";

    /**
     * Creates a new WsClientDDModifier where element is a
     * <code>jonas-service-ref</code> XML Node.
     * @param name service-ref-name value
     * @param doc document base for Element creation
     * @param base base element for searching jonas-service-ref
     */
    public WsClientDDModifier(String name, Document doc, Element base) {
        super(XMLUtils.getJonasServiceRef(base, name), doc, base);
    }

    /**
     * Add a jonas-init-param in jonas-service-ref.
     * @param name param name
     * @param value param value
     */
    public void addJonasInitParam(String name, String value) {
        Element jip = newJOnASElement(JONAS_INIT_PARAM);
        Element pn = newJOnASElement(PARAM_NAME, name);
        Element pv = newJOnASElement(PARAM_VALUE, value);

        jip.appendChild(pn);
        jip.appendChild(pv);

        getElement().appendChild(jip);
    }

    /**
     * @return Returns true if this web service client has a JOnAS specific
     *         descriptor (jonas-service-ref).
     */
    public boolean hasJonasServiceRef() {
        return (getElement() != null);
    }

    /**
     * @param serviceRefName the value of service-ref-name Element
     * @return Returns a default jonas-service-ref Element (initialized with the given service-ref-name).
     */
    public Element createJonasServiceRef(String serviceRefName) {
        Element jsr = newJOnASElement("jonas-service-ref");
        Element srn = newJOnASElement("service-ref-name", serviceRefName);

        jsr.appendChild(srn);
        return jsr;
    }

}