/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Sauthier Guillaume
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.generators.wsgen.generator.axis;

import javax.xml.namespace.QName;

/**
 * Member of a VelocityContext. Contains information used to create a
 * typeMapping WSDD tag.
 *
 * @author Guillaume SAUTHIER
 */
public abstract class VcTypeMapping {

    /** classname */
    private String classname;

    /** xml type QName */
    private QName xmlType;

    /**
     * SOAP Encoding Style
     */
    private static final String ENCODING_STYLE = "http://schemas.xmlsoap.org/soap/encoding/";

    /**
     * Create a VcTypeMapping holding typeMapping information.
     *
     * @param xml XML Qname of the type
     * @param name Java name of the type
     */
    public VcTypeMapping(QName xml, String name) {
        classname = name;
        xmlType = xml;
    }

    /**
     * @return Returns the Java classname
     */
    public String getClassname() {
        return classname;
    }

    /**
     * @return Returns the namespace of the QName
     */
    public String getNamespaceURI() {
        return xmlType.getNamespaceURI();
    }

    /**
     * @return Returns the local-part of the QName
     */
    public String getLocalPart() {
        return xmlType.getLocalPart();
    }

    /**
     * @return Returns the serializer factory for the mapping
     */
    protected abstract String getSerializerFactory();

    /**
     * @return Returns the deserializer factory for the mapping
     */
    protected abstract String getDeserializerFactory();

    /**
     * @return Returns the encoding style for the mapping
     */
    protected String getEncodingStyle() {
        return ENCODING_STYLE;
    }

    /**
     * @return Returns a String representation of the typeMapping
     */
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("<typeMapping xmlns:ns=\"" + getNamespaceURI() + "\"\n");
        sb.append("             qname=\"ns:" + getLocalPart() + "\"\n");
        sb.append("             languageSpecificType=\"java:" + getClassname() + "\"\n");
        sb.append("             serializer=\"" + getSerializerFactory() + "\"\n");
        sb.append("             deserializer=\"" + getDeserializerFactory() + "\"\n");
        sb.append("             encodingStyle=\"" + getEncodingStyle() + "\" />");
        return sb.toString();
    }
}