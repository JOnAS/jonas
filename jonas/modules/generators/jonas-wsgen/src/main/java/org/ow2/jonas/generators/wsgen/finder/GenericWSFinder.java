/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.generators.wsgen.finder;

import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;
import org.ow2.jonas.generators.genbase.archive.J2EEArchive;
import org.ow2.jonas.lib.util.Log;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.InputStream;

/**
 * Multi-module (EjbJar, WebApp, Client) WebServices Finder.
 *
 * @author Guillaume Sauthier
 */
public class GenericWSFinder implements J2EEWebServicesFinder {

    /**
     * logger.
     */
    private static Logger logger = Log.getLogger(Log.JONAS_WSGEN_PREFIX);

    /**
     * The archive to be explored.
     */
    private J2EEArchive archive = null;

    /**
     * Standard XML descriptor name.
     */
    private String descriptorName = null;

    /**
     * Create a multi-module type WSFinder.
     * @param archive The archive to be explored.
     * @param descriptorName Standard XML descriptor name.
     */
    public GenericWSFinder(J2EEArchive archive, String descriptorName) {
        super();
        this.archive = archive;
        this.descriptorName = descriptorName;
    }

    /**
     * @see org.ow2.jonas.generators.wsgen.finder.J2EEWebServicesFinder#find()
     */
    public boolean find() {

        // Look for webservices.xml
        WebServicesXmlFinder wsFinder = new WebServicesXmlFinder(archive);
        try {
            wsFinder.init(getSAXParser());
        } catch (Exception e) {
            logger.log(BasicLevel.DEBUG, "Cannot return a SAXParser", e);
            // by default, the WebServicesXmlFinder will return false...

        }

        // Look for service-ref
        ServiceRefFinder srFinder = new ServiceRefFinder();
        try {
            InputStream is = archive.getInputStream(descriptorName);
            SAXParser parser = getSAXParser();
            parser.parse(is, srFinder);
        } catch (Exception e) {
            logger.log(BasicLevel.DEBUG, "Cannot return a SAXParser", e);
            // by default, the ServiceRefFinder will return false...
        }

        // returns true if there is a webservices.xml (JAX-RPC only)
        // and/or some service-ref (J2EE namespace only)
        return wsFinder.find() || srFinder.find();
    }
    /**
     * @return Returns a SAXParser
     * @throws ParserConfigurationException when Parser cannot be created.
     * @throws SAXException when Parser cannot be created.
     */
    protected SAXParser getSAXParser() throws ParserConfigurationException, SAXException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        factory.setNamespaceAware(true);
        factory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
        return factory.newSAXParser();
    }

}

