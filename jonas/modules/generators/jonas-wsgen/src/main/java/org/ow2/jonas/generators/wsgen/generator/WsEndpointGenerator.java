/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2003-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.generators.wsgen.generator;

import org.ow2.jonas.deployment.ws.ServiceDesc;
import org.ow2.jonas.generators.genbase.GenBaseException;
import org.ow2.jonas.generators.genbase.archive.Archive;
import org.ow2.jonas.generators.genbase.generator.Config;
import org.ow2.jonas.generators.wsgen.ddmodifier.WebServicesDDModifier;
import org.ow2.jonas.generators.wsgen.ddmodifier.WsEndpointDDModifier;



/**
 * Generate sources and/or config files for WebServices Endpoint.
 * @author Guillaume Sauthier
 */
public abstract class WsEndpointGenerator extends Generator {

    /** WebService Endpoint description */
    private ServiceDesc service;

    /** archive */
    private Archive archive;

    /** Web DD Modifier */
    private WsEndpointDDModifier modifier;

    /** Webservices DD Modifier */
    private WebServicesDDModifier wsModifier;

    /**
     * Creates a new WsEndpointGenerator
     * @param config Generator Configuration
     * @param serviceDesc WebService Endpoint description
     * @param ddm Web DD Modifier
     * @param wsddm webservices.xml DD modifier
     * @param arch the Archive to modify
     * @throws GenBaseException When instanciation fails
     */
    public WsEndpointGenerator(Config config, ServiceDesc serviceDesc, WsEndpointDDModifier ddm,
            WebServicesDDModifier wsddm, Archive arch) throws GenBaseException {
        super(config);
        service = serviceDesc;
        modifier = ddm;
        wsModifier = wsddm;
        archive = arch;
    }

    /**
     * @return the service.
     */
    public ServiceDesc getService() {
        return service;
    }

    /**
     * @return the archive.
     */
    public Archive getArchive() {
        return archive;
    }

    /**
     * @return the modifier.
     */
    public WsEndpointDDModifier getModifier() {
        return modifier;
    }

    /**
     * @return the wsModifier.
     */
    public WebServicesDDModifier getWsModifier() {
        return wsModifier;
    }
}