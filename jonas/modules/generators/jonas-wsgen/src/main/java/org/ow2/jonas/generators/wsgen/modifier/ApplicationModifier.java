/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2003-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.generators.wsgen.modifier;

import java.util.Iterator;
import java.util.jar.Attributes;

import org.ow2.jonas.Version;
import org.ow2.jonas.generators.genbase.GenBaseException;
import org.ow2.jonas.generators.genbase.archive.Application;
import org.ow2.jonas.generators.genbase.archive.Archive;
import org.ow2.jonas.generators.genbase.archive.Client;
import org.ow2.jonas.generators.genbase.archive.EjbJar;
import org.ow2.jonas.generators.genbase.archive.WebApp;
import org.ow2.jonas.generators.genbase.modifier.AbsApplicationModifier;
import org.ow2.jonas.generators.wsgen.WsGenException;
import org.ow2.jonas.generators.wsgen.generator.GeneratorFactory;



/**
 * Modify a given Application.
 * @author Guillaume Sauthier
 */
public class ApplicationModifier extends AbsApplicationModifier {

    /**
     * Creates a new ApplicationModifier.
     * @param archive the Application J2EE archive
     * @throws WsGenException if the application modifier cannot be built
     */
    public ApplicationModifier(Application archive) throws WsGenException {
        super(archive, GeneratorFactory.getInstance().getConfiguration());
    }

    /**
     * initialize modifier
     */
    protected void init() {

        // fill ejbjar list
        for (Iterator i = getApplication().getEjbJars(); i.hasNext();) {
            EjbJar ejbjar = (EjbJar) i.next();
            getEjbModifiers().add(new EjbJarModifier(ejbjar));
        }

        // fill webapp list
        for (Iterator i = getApplication().getWebApps(); i.hasNext();) {
            WebApp webapp = (WebApp) i.next();
            getWebModifiers().add(new WebAppModifier(webapp));
        }

        // fill client list
        for (Iterator i = getApplication().getClients(); i.hasNext();) {
            Client client = (Client) i.next();
            getCltModifiers().add(new ClientModifier(client));
        }
    }

    /**
     * Update the Manifest of the Application before modifying it.
     * @return Returns the modified Archive.
     * @see org.ow2.jonas.generators.genbase.modifier.AbsApplicationModifier#modify()
     */
    public Archive modify() throws GenBaseException {
        // Update MANIFEST with Version Number
        // used to say if WsGen has already been applied to this Module
        Attributes main = this.getApplication().getManifest().getMainAttributes();
        main.put(new Attributes.Name(WsGenModifierConstants.WSGEN_JONAS_VERSION_ATTR), Version.getNumber());

        return super.modify();
    }


}