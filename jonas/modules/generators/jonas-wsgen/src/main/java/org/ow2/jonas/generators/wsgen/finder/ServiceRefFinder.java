/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.generators.wsgen.finder;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Specialized Finder for XML files.
 * If one contains some service-ref elements, it will be marked has WS enabled.
 *
 * @author Guillaume Sauthier
 */
public class ServiceRefFinder extends DefaultHandler implements J2EEWebServicesFinder {

    /**
     * J2EE Namespace URI.
     */
    private static final String J2EE_NS = "http://java.sun.com/xml/ns/j2ee";

    /**
     * true if a service-ref element was found.
     */
    private boolean found = false;

    /**
     * @see org.xml.sax.helpers.DefaultHandler#startElement(java.lang.String, java.lang.String, java.lang.String, org.xml.sax.Attributes)
     */
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        super.startElement(uri, localName, qName, attributes);
        if ("service-ref".equals(localName) && J2EE_NS.equals(uri)) {
            // found a j2ee:service-ref element
            // react in some way
            found = true;
        }
    }

    /**
     * @see org.ow2.jonas.generators.wsgen.finder.J2EEWebServicesFinder#find()
     */
    public boolean find() {
        return found;
    }

}
