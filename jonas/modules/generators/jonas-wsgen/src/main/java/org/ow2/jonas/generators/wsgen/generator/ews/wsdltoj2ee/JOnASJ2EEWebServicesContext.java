/**
 * JOnAS : Java(TM) OpenSource Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.generators.wsgen.generator.ews.wsdltoj2ee;

import java.util.List;
import java.util.Vector;

import javax.ejb.EJBContext;

import org.ow2.jonas.deployment.ws.ServiceDesc;

import org.apache.ws.ews.context.webservices.client.ServiceReferenceContext;
import org.apache.ws.ews.context.webservices.server.WSCFContext;
import org.apache.ws.ews.mapper.context.JAXRPCMapperContext;



/**
 * JOnAS implementation of the J2EEWebServiceContext from EWS
 * @author Guillaume Sauthier
 */
public class JOnASJ2EEWebServicesContext implements JAXRPCMapperContext {

    /**
     * list of service-ref (for client)
     */
    private List srCtx;

    /**
     * service description (for endpoint)
     */
    private ServiceDesc serviceDesc = null;

    /**
     * Default constructor
     */
    public JOnASJ2EEWebServicesContext() {
        super();
        this.srCtx = new Vector();
    }

    /**
     * @deprecated not used by JOnAS Implementation
     * @inheritDoc
     * @see org.apache.geronimo.ews.ws4j2ee.context.J2EEWebServiceContext#getWSCFContext()
     */
    public WSCFContext getWSCFContext() {
        return null;
    }

    /**
     * @deprecated not used by JOnAS Implementation
     * @inheritDoc
     * @see org.apache.geronimo.ews.ws4j2ee.context.J2EEWebServiceContext#setWSCFContext(org.apache.geronimo.ews.ws4j2ee.context.webservices.server.interfaces.WSCFContext)
     */
    public void setWSCFContext(WSCFContext wscfcontext) {
    }

    /**
     * @deprecated not used by JOnAS Implementation
     * @inheritDoc
     * @see org.apache.geronimo.ews.ws4j2ee.context.J2EEWebServiceContext#getJAXRPCMappingContext()
     */
    public JAXRPCMapperContext getJAXRPCMappingContext() {
        return null;
    }

    /**
     * @deprecated not used by JOnAS Implementation
     * @inheritDoc
     * @see org.apache.geronimo.ews.ws4j2ee.context.J2EEWebServiceContext#setJAXRPCMappingContext(org.apache.geronimo.ews.ws4j2ee.context.JaxRpcMapperContext)
     */
    public void setJAXRPCMappingContext(JAXRPCMapperContext context) {
    }

    /**
     * @deprecated not used by JOnAS Implementation
     * @inheritDoc
     * @see org.apache.geronimo.ews.ws4j2ee.context.J2EEWebServiceContext#validate()
     */
    public void validate() {
    }

    /**
     * @return Returns the serviceDesc.
     */
    public ServiceDesc getServiceDesc() {
        return serviceDesc;
    }

    /**
     * @param serviceDesc The serviceDesc to set.
     */
    public void setServiceDesc(ServiceDesc serviceDesc) {
        this.serviceDesc = serviceDesc;
    }

    /**
     * @deprecated not used by JOnAS Implementation
     * @inheritDoc
     * @see org.apache.geronimo.ews.ws4j2ee.context.J2EEWebServiceContext#getEJBDDContext()
     */
    public EJBContext getEJBDDContext() {
        return null;
    }

    /**
     * @deprecated not used by JOnAS Implementation
     * @inheritDoc
     * @see org.apache.geronimo.ews.ws4j2ee.context.J2EEWebServiceContext#setEJBDDContext(org.apache.geronimo.ews.ws4j2ee.context.j2eeDD.EJBContext)
     */
    public void setEJBDDContext(EJBContext context) {
    }

    /**
     * @see org.apache.geronimo.ews.ws4j2ee.context.J2EEWebServiceContext#getServiceReferanceContext(int)
     */
    public ServiceReferenceContext getServiceReferenceContext(int index) {
        return (ServiceReferenceContext) srCtx.get(index);
    }

    /**
     * @see org.apache.geronimo.ews.ws4j2ee.context.J2EEWebServiceContext#addServiceReferanceContext(org.apache.geronimo.ews.ws4j2ee.context.webservices.client.interfaces.ServiceReferanceContext)
     */
    public void addServiceReferenceContext(ServiceReferenceContext context) {
        srCtx.add(context);
    }

    /**
     * @see org.apache.geronimo.ews.ws4j2ee.context.J2EEWebServiceContext#getServiceReferanceContextCount()
     */
    public int getServiceReferenceContextCount() {
        return srCtx.size();
    }

}
