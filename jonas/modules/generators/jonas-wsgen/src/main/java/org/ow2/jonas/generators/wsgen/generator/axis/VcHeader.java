/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Xavier Delplanque
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.generators.wsgen.generator.axis;

import javax.xml.namespace.QName;

/**
 * Member of a VelocityContext. Contains information about a SOAP Header
 * (basically a qName).
 *
 * @author Xavier Delplanque
 */
public class VcHeader {

    /** LocalPart of the QName */
    private String qname;

    /** NamespaceURI of the QName */
    private String namespace;

    /**
     * Construct a VcHeader from a QName.
     *
     * @param qn the Header QName
     */
    public VcHeader(QName qn) {

        // set qname
        qname = qn.getLocalPart();

        // set namespace
        namespace = qn.getNamespaceURI();
    }

    /**
     * @return Returns the Header localpart
     */
    public String getQName() {
        return qname;
    }

    /**
     * @return Returns the Header namespace URI
     */
    public String getNamespace() {
        return namespace;
    }
}