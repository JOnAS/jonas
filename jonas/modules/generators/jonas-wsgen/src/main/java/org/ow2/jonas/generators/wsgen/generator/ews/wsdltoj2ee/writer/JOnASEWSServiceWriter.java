/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.generators.wsgen.generator.ews.wsdltoj2ee.writer;

import javax.wsdl.Service;

import org.ow2.jonas.generators.wsgen.generator.ews.wsdltoj2ee.emitter.JOnASWSEmitter;

import org.apache.axis.wsdl.symbolTable.ServiceEntry;
import org.apache.axis.wsdl.symbolTable.SymbolTable;
import org.apache.axis.wsdl.toJava.Emitter;
import org.apache.axis.wsdl.toJava.JavaServiceWriter;

/**
 * This is JOnASWsdl2java's Service Writer. It writes the following files, as
 * appropriate: <serviceName>.java, <serviceName>Locator.java.
 */
public class JOnASEWSServiceWriter extends JavaServiceWriter {

    /**
     * Constructor.
     * @param emitter JOnASEmitter
     * @param service Service
     * @param symbolTable SymbolTable
     */
    public JOnASEWSServiceWriter(Emitter emitter, Service service, SymbolTable symbolTable) {
        super(emitter, service, symbolTable);
    } // ctor

    /**
     * setGenerators
     * Logic to set the generators that are based on the Service.
     * This logic was moved from the constructor so extended interfaces
     * can more effectively use the hooks.
     */
    protected void setGenerators() {
        ServiceEntry sEntry = symbolTable.getServiceEntry(service.getQName());

        if (sEntry.isReferenced()) {
            serviceIfaceWriter = null;
            testCaseWriter = null;
            // Write service interface if :
            //    - class is not available
            //    - emitter is not a J2eeEmitter (for backward compliance)
            if (emitter instanceof JOnASWSEmitter) {
                ClassLoader cl = ((JOnASWSEmitter) emitter).getClassLoader();
                String className = sEntry.getName();
                try {
                    cl.loadClass(className);
                } catch (ClassNotFoundException cnfe) {
                    // class not available, generate it
                    serviceIfaceWriter = new JOnASEWSServiceIntfWriter(emitter, sEntry, symbolTable);
                }
            }

            // overwrite Axis Writer
            serviceImplWriter = new JOnASEWSServiceImplWriter(emitter, sEntry, symbolTable);
        }
    }
} // class JOnASServiceWriter
