/**
 * JOnAS : Java(TM) OpenSource Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.generators.wsgen.generator.ews.wsdltoj2ee.writer;

import javax.wsdl.Binding;

import org.apache.axis.wsdl.gen.Generator;
import org.apache.axis.wsdl.symbolTable.BindingEntry;
import org.apache.axis.wsdl.symbolTable.PortTypeEntry;
import org.apache.axis.wsdl.symbolTable.SymbolTable;
import org.apache.ws.ews.mapper.J2eeBindingWriter;
import org.apache.ws.ews.mapper.J2eeEmitter;


/**
 * Extends EWS J2eeBindingWriter to generate port interface only when needed.
 * @author Guillaume Sauthier
 */
public class JOnASEWSBindingWriter extends J2eeBindingWriter implements Generator {

    /**
     * @param emitter J2eeEmitter
     * @param binding wsdl:binding containing port-interface infos
     * @param st Entry table
     */
    public JOnASEWSBindingWriter(J2eeEmitter emitter, Binding binding, SymbolTable st) {
        super(emitter, binding, st);
    }

    /**
     * @see org.apache.geronimo.ews.jaxrpcmapping.J2eeBindingWriter#getJavaInterfaceWriter(org.apache.geronimo.ews.jaxrpcmapping.J2eeEmitter, org.apache.axis.wsdl.symbolTable.PortTypeEntry, org.apache.axis.wsdl.symbolTable.BindingEntry, org.apache.axis.wsdl.symbolTable.SymbolTable)
     */
    protected Generator getJavaInterfaceWriter(J2eeEmitter emitter, PortTypeEntry ptEntry, BindingEntry bEntry, SymbolTable st) {
        ClassLoader cl = ((org.ow2.jonas.generators.wsgen.generator.ews.wsdltoj2ee.emitter.JOnASWSEmitter) emitter).getClassLoader();
        try {
            cl.loadClass(ptEntry.getName());
        } catch (ClassNotFoundException cnfe) {
            // class not available, generate it
            return super.getJavaInterfaceWriter(emitter, ptEntry, bEntry, symbolTable);
        }
        return null;
    }

    /**
     * @see org.apache.geronimo.ews.jaxrpcmapping.J2eeBindingWriter#getJavaImplWriter(org.apache.geronimo.ews.jaxrpcmapping.J2eeEmitter, org.apache.axis.wsdl.symbolTable.BindingEntry, org.apache.axis.wsdl.symbolTable.SymbolTable)
     */
    protected Generator getJavaImplWriter(J2eeEmitter arg0, BindingEntry arg1,
            SymbolTable arg2) {
        return null;
    }

    /**
     * @see org.apache.geronimo.ews.jaxrpcmapping.J2eeBindingWriter#getJavaSkelWriter(org.apache.geronimo.ews.jaxrpcmapping.J2eeEmitter, org.apache.axis.wsdl.symbolTable.BindingEntry, org.apache.axis.wsdl.symbolTable.SymbolTable)
     */
    protected Generator getJavaSkelWriter(J2eeEmitter arg0, BindingEntry arg1,
            SymbolTable arg2) {
        return null;
    }
}
