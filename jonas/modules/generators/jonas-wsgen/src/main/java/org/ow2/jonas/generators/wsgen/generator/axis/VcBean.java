/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Xavier Delplanque
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.generators.wsgen.generator.axis;

import org.ow2.jonas.deployment.ejb.SessionStatelessDesc;

/**
 * Member of a VelocityContext. Contains information about a bean.
 *
 * @author Xavier Delplanque
 */
public class VcBean {

    /**
     * the bean jndi name
     */
    private String jndiName;

    /**
     * ServiceEndpoint interface name
     */
    private String serviceEndpointInterface;

    /**
     * Construct a Holder for Bean informations
     *
     * @param ssd SessionStatelessDesc descripting the given SessionBean
     */
    public VcBean(SessionStatelessDesc ssd) {

        // set jndiName
        jndiName = ssd.getJndiServiceEndpointName();

        // set service endpoint
        serviceEndpointInterface = ssd.getServiceEndpointClass().getName();

    }

    /**
     * The bean jndi name
     *
     * @return The bean jndi name
     */
    public String getServiceEndpointJndiName() {
        return jndiName;
    }

    /**
     * the bean service-endpoint interface name
     *
     * @return the bean service-endpoint interface name
     */
    public String getServiceEndpointInterfaceName() {
        return serviceEndpointInterface;
    }
}