/**
 * JOnAS : Java(TM) OpenSource Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.generators.wsgen.generator.ews.wsdltoj2ee.wscf;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.xml.namespace.QName;

import org.ow2.jonas.deployment.ee.HandlerDesc;

import org.apache.ws.ews.context.webservices.server.impl.AbstractWSCFHandler;



/**
 * JOnAS WSCFHandler implementation.
 * That's used to fit into the EWS model.
 * @author Guillaume Sauthier
 */
public class JOnASWSCFHandler extends AbstractWSCFHandler {

    /**
     * port-name list
     */
    private List portNames;

    /**
     * @param handler JOnAS Handler implementation
     */
    public JOnASWSCFHandler(HandlerDesc handler) {
        // handler-class
        this.handlerClass = handler.getHandlerClassName();
        // handler-name
        this.handlerName = handler.getName();

        // handler/init-param
        this.initParam = new HashMap();
        for (Iterator i = handler.getInitParams().keySet().iterator(); i.hasNext();) {
            String paramName = (String) i.next();
            this.initParam.put(paramName, new JOnASWSCFInitParam(paramName, handler.getInitParam(paramName)));
        }

        // port-name list
        this.portNames = new Vector();
        for (Iterator i = handler.getPortNames().iterator(); i.hasNext();) {
            String sr = (String) i.next();
            this.portNames.add(sr);
        }

        // soap-header*
        this.soapHeader = new Vector();
        for (Iterator i = handler.getSOAPHeaders().iterator(); i.hasNext();) {
            QName sh = (QName) i.next();
            this.soapHeader.add(new JOnASWSCFSOAPHeader(sh));
        }

        // soap-roles*
        this.soapRole = new Vector();
        for (Iterator i = handler.getSOAPRoles().iterator(); i.hasNext();) {
            String sr = (String) i.next();
            this.soapRole.add(sr);
        }
    }

    /**
     * Gets the port name of the handler element
     *
     * @return port-name
     */
    public List getPortNames() {
        return portNames;
    }
}
