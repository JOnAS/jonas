/**
 * JOnAS : Java(TM) OpenSource Application Server
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial Developer : Guillaume Sauthier
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.generators.wsgen.ddmodifier;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * @author Guillaume Sauthier
 */
public class WebServicesDDModifier extends DeploymentDescModifier {

    /**
     * @param doc webservices.xml Document
     */
    public WebServicesDDModifier(Document doc) {
        super(doc.getDocumentElement(), doc);
    }

    /**
     * Update the servlet-link element in webservices.xml to the Generated servlet element.
     *
     * @param wsdName WebService Description name
     * @param link old servlet-link value
     * @param replace new servlet-link value
     */
    public void changeServletLink(String wsdName, String link, String replace) {

        Element wsd = findWebserviceDesc(wsdName);

        // if element found
        if (wsd != null) {
            NodeList nl = wsd.getElementsByTagNameNS(J2EE_NS, "port-component");

            for (int i = 0; i < nl.getLength(); i++) {
                Element e = (Element) nl.item(i);
                NodeList sib = e.getElementsByTagNameNS(J2EE_NS, "service-impl-bean");

                Node servletLink = ((Element) sib.item(0)).getElementsByTagNameNS(J2EE_NS, "servlet-link").item(0);

                // test
                // port-component/sib-link/servlet-link/#text-node.value
                if (servletLink.getFirstChild().getNodeValue().equals(link)) {
                    servletLink.getFirstChild().setNodeValue(replace);
                }
            }
        }
    }

    /**
     * Search element for webservices-description named with the given name.
     *
     * @param name the searched webservices-description name
     *
     * @return the found element or null if element is not found (should'nt
     *         occurs).
     */
    private Element findWebserviceDesc(String name) {
        NodeList nl = getElement().getElementsByTagNameNS(J2EE_NS, "webservice-description");
        Element wsd = null;

        for (int i = 0; (i < nl.getLength()) && (wsd == null); i++) {
            Element e = (Element) nl.item(i);

            NodeList names = e.getElementsByTagNameNS(J2EE_NS, "webservice-description-name");

            // test
            // webservices-description/webservices-description--name/#text-node.value
            if (names.item(0).getFirstChild().getNodeValue().equals(name)) {
                wsd = e;
            }
        }

        return wsd;
    }

}