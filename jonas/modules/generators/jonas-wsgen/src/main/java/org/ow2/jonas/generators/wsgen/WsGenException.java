/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Guillaume Sauthier
 * Contributor(s): ______________________________________.
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.generators.wsgen;

import org.ow2.jonas.generators.genbase.GenBaseException;

/**
 * This class represents the exception that can be throwned by the WsGen tool.
 */
public class WsGenException extends GenBaseException {

    /**
     * Constructs a WsGenException with no specified detail message.
     */
    public WsGenException() {
        super();
    }

    /**
     * Constructs an WsGenException with the specified detail message.
     * @param msg Error message
     */
    public WsGenException(String msg) {
        super(msg);
    }

    /**
     * Constructs an WsGenException with the specified detail message.
     * @param inner Cause Exception to wrap
     */
    public WsGenException(Exception inner) {
        super(inner);
    }

    /**
     * Constructs an WsGenException with the specified detail message.
     * @param msg Error message
     * @param inner Cause Exception to wrap
     */
    public WsGenException(String msg, Exception inner) {
        super(msg, inner);
    }
}