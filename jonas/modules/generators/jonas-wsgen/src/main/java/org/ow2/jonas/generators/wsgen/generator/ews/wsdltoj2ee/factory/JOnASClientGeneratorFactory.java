/**
 * JOnAS : Java(TM) OpenSource Application Server
 * Copyright (C) 2004-2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.generators.wsgen.generator.ews.wsdltoj2ee.factory;

import javax.wsdl.Binding;
import javax.wsdl.Definition;
import javax.wsdl.PortType;
import javax.wsdl.Service;

import org.ow2.jonas.generators.wsgen.generator.ews.wsdltoj2ee.writer.J2EEClientDeployWriter;
import org.ow2.jonas.generators.wsgen.generator.ews.wsdltoj2ee.writer.JOnASEWSBindingWriter;
import org.ow2.jonas.generators.wsgen.generator.ews.wsdltoj2ee.writer.JOnASEWSServiceWriter;
import org.ow2.jonas.generators.wsgen.generator.ews.wsdltoj2ee.writer.JOnASTypeWriter;

import org.apache.axis.wsdl.gen.Generator;
import org.apache.axis.wsdl.gen.NoopGenerator;
import org.apache.axis.wsdl.symbolTable.BindingEntry;
import org.apache.axis.wsdl.symbolTable.ServiceEntry;
import org.apache.axis.wsdl.symbolTable.SymbolTable;
import org.apache.axis.wsdl.symbolTable.TypeEntry;
import org.apache.ws.ews.mapper.J2eeGeneratorFactory;


/**
 * JOnAS implementation of J2EEGeneratorFactory for the client side.
 * @author Guillaume Sauthier
 */
public class JOnASClientGeneratorFactory extends J2eeGeneratorFactory {

    /**
     * @see org.apache.geronimo.ews.jaxrpcmapping.J2eeGeneratorFactory#addDefinitionGenerators()
     */
    protected void addDefinitionGenerators() {
        // for faults
        // Do not emit Faults
        //addGenerator(Definition.class, JavaDefinitionWriter.class);
        // for deploy.wsdd
        addGenerator(Definition.class, J2EEClientDeployWriter.class);
    }

    /**
     * Use the JOnASServiceWriter instead of default JavaServiceWriter
     * @param service wsdl:service
     * @param symbolTable symbol Table
     * @return Returns the JOnASServiceWriter
     */
    public Generator getGenerator(Service service, SymbolTable symbolTable) {
            Generator writer = new JOnASEWSServiceWriter(emitter, service, symbolTable);
            ServiceEntry sEntry = symbolTable.getServiceEntry(service.getQName());
            serviceWriters.addStuff(writer, sEntry, symbolTable);
            return serviceWriters;
    }

    /**
     * Use the JOnASServiceWriter instead of default JavaServiceWriter
     * @param binding wsdl:binding
     * @param symbolTable symbol Table
     * @return Returns the JOnASJ2eeBindingWriter
     */
    public Generator getGenerator(Binding binding, SymbolTable symbolTable) {
        Generator writer = new JOnASEWSBindingWriter(emitter, binding, symbolTable);
        BindingEntry bEntry = symbolTable.getBindingEntry(binding.getQName());
        bindingWriters.addStuff(writer, bEntry, symbolTable);
        return bindingWriters;
    }

    /**
     * Since this Generator doesn't output anything else than
     * deploy-server.wsdd, other generator are NoopGenerator.
     * @see org.apache.axis.wsdl.gen.GeneratorFactory#getGenerator(javax.wsdl.PortType,
     *      org.apache.axis.wsdl.symbolTable.SymbolTable)
     */
    public Generator getGenerator(PortType portType, SymbolTable symbolTable) {
        return new NoopGenerator();
    }

    /**
     * Use the JOnASTypeWriter instead of default JavaTypeWriter
     * @see org.apache.axis.wsdl.gen.GeneratorFactory#getGenerator(org.apache.axis.wsdl.symbolTable.TypeEntry,
     *      org.apache.axis.wsdl.symbolTable.SymbolTable)
     */
    public Generator getGenerator(TypeEntry type, SymbolTable symbolTable) {
        Generator writer = new JOnASTypeWriter(emitter, type, symbolTable);
        typeWriters.addStuff(writer, type, symbolTable);
        return typeWriters;
    }
}