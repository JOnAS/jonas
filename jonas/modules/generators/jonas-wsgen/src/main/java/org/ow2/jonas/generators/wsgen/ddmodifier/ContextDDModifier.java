/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial Developer : Matt Wringe
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.generators.wsgen.ddmodifier;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Modify a the context.xml file. Wrapper around a web.xml DOM
 *
 * @author Matt Wringe
 */
public class ContextDDModifier extends DeploymentDescModifier {


    /**
     * The class used by Tomcat to setup the realm
     */
    private static final String REALM_CLASS_NAME = "org.ow2.jonas.web.tomcat6.security.Realm";


    /**
     * Create a new ContextDDModifier
     *
     * @param doc the context.xml Document
     */
    public ContextDDModifier(Document doc) {
        super(doc.getDocumentElement(), doc);
    }

    /**
     * Setups the realm for Tomcat to use
     *
     * @param realm the realm to use
     */
    public void addContextRealm(String realm) {
        Element contextElement = getElement();

        contextElement.appendChild (setRealm(realm));
    }

    /**
     * Returns an element that defines the realm to use
     *
     * @param realm the realm to use
     * @return an element the defines the realm to use
     */
    private Element setRealm (String realm) {
        Element realmElement = newElement("Realm");
        realmElement.setAttribute("className", REALM_CLASS_NAME);
        realmElement.setAttribute("resourceName", realm);

        return realmElement;
    }

}