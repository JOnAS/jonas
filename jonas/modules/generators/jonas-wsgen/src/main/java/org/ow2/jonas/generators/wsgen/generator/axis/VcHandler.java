/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Xavier Delplanque
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.generators.wsgen.generator.axis;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.xml.namespace.QName;

import org.ow2.jonas.deployment.ee.HandlerDesc;


/**
 * Member of a VelocityContext. Contains information about a Handler (Ref/Desc).
 *
 * @author Xavier Delplanque
 */
public class VcHandler {

    /** handler name (unique) */
    private String name;

    /** handler class name */
    private String className;

    /** handler init parameters */
    private Vector initParameters;

    /** handler SOAP headers */
    private Vector headers;

    /** handler SOAP roles */
    private Vector soapRoles;

    /**
     * Create a new VcHandler from a HandlerDesc
     *
     * @param hr HandlerDesc
     */
    public VcHandler(HandlerDesc hr) {

        // set name
        name = hr.getName();

        //set className
        className = hr.getHandlerClass().getName();

        // set initParameters
        Map ips = hr.getInitParams();
        initParameters = new Vector();
        for (Iterator itKey = ips.keySet().iterator(); itKey.hasNext();) {
            String key = (String) itKey.next();
            String val = (String) ips.get(key);
            initParameters.add(new VcInitParam(key, val));
        }

        // set headers
        List hs = hr.getSOAPHeaders();
        headers = new Vector();
        for (Iterator itH = hs.iterator(); itH.hasNext();) {
            QName qn = (QName) itH.next();
            headers.add(new VcHeader(qn));
        }

        // set soapRoles
        soapRoles = new Vector();
        List srs = hr.getSOAPRoles();
        for (Iterator itSr = srs.iterator(); itSr.hasNext();) {
            String sr = (String) itSr.next();
            soapRoles.add(sr);
        }
    }

    /**
     * @return Returns the Handler name
     */
    public String getName() {
        return name;
    }

    /**
     * @return Returns the handler classname
     */
    public String getClassName() {
        return className;
    }

    /**
     * @return Returns the list of handler init param
     */
    public Vector getInitParameters() {
        return initParameters;
    }

    /**
     * @return Returns the list of header the given Handler is able to access.
     */
    public Vector getHeaders() {
        return headers;
    }

    /**
     * @return Returns the list of SOAP roles the Handler use
     */
    public Vector getSoapRoles() {
        return soapRoles;
    }

}