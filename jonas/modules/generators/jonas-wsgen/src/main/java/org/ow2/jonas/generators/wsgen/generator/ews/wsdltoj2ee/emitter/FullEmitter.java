/**
 * JOnAS : Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.generators.wsgen.generator.ews.wsdltoj2ee.emitter;

import org.ow2.jonas.generators.wsgen.generator.ews.wsdltoj2ee.JOnASJ2EEWebServicesContext;

import org.apache.ws.ews.mapper.J2eeEmitter;


/**
 * Allows to define more options like classloader.
 * @author Florent Benoit
 */
public class FullEmitter extends J2eeEmitter implements JOnASWSEmitter {

    /**
     * Classloader to use
     */
    private ClassLoader classLoader = null;

    /**
     * Generate bindings ?
     */
    private boolean bindingGeneration = true;

    /**
     * Generate Services ?
     */
    private boolean serviceGeneration = true;

    /**
     * Generate WSDDs ?
     */
    private boolean deployGeneration = true;

    /**
     * @return the classLoader.
     */
    public ClassLoader getClassLoader() {
        return classLoader;
    }

    /**
     * Set the classloader to use
     * @param classLoader The classLoader to set.
     */
    public void setClassLoader(ClassLoader classLoader) {
        this.classLoader = classLoader;
    }

    /**
     * @see org.ow2.jonas.generators.wsgen.generator.ews.wsdltoj2ee.emitter.JOnASWSEmitter#getJOnASWsContext()
     */
    public JOnASJ2EEWebServicesContext getJOnASWsContext() {
        return (JOnASJ2EEWebServicesContext) getWscontext();
    }

    /**
     * @see org.ow2.jonas.generators.wsgen.generator.ews.wsdltoj2ee.emitter.JOnASWSEmitter#setJOnASWsContext(org.ow2.jonas.generators.wsgen.generator.ews.wsdltoj2ee.JOnASJ2EEWebServicesContext)
     */
    public void setJOnASWsContext(JOnASJ2EEWebServicesContext context) {
        setWscontext(context);
    }

    /**
     * @see org.ow2.jonas.generators.wsgen.generator.ews.wsdltoj2ee.emitter.JOnASWSEmitter#hasBindingGeneration()
     */
    public boolean hasBindingGeneration() {
        return bindingGeneration;
    }

    /**
     * @see org.ow2.jonas.generators.wsgen.generator.ews.wsdltoj2ee.emitter.JOnASWSEmitter#hasServiceGeneration()
     */
    public boolean hasServiceGeneration() {
        return serviceGeneration;
    }

    /**
     * @see org.ow2.jonas.generators.wsgen.generator.ews.wsdltoj2ee.emitter.JOnASWSEmitter#hasDeployGeneration()
     */
    public boolean hasDeployGeneration() {
        return deployGeneration;
    }

    /**
     * @param bg generate bindings on/off
     */
    public void setBindingGeneration(boolean bg) {
        bindingGeneration = bg;
    }

    /**
     * @param sg generate services on/off
     */
    public void setServiceGeneration(boolean sg) {
        serviceGeneration = sg;
    }

    /**
     * @param dg generate deploy files on/off
     */
    public void setDeployGeneration(boolean dg) {
        deployGeneration = dg;
    }
}