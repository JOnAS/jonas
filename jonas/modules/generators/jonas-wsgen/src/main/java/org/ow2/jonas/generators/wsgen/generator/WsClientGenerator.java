/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2003-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.generators.wsgen.generator;

import org.ow2.jonas.deployment.ws.ServiceRefDesc;
import org.ow2.jonas.generators.genbase.GenBaseException;
import org.ow2.jonas.generators.genbase.archive.Archive;
import org.ow2.jonas.generators.genbase.generator.Config;
import org.ow2.jonas.generators.wsgen.ddmodifier.WsClientDDModifier;



/**
 * Generate sources and/or config for WebServices clients.
 *
 * @author Guillaume Sauthier
 */
public abstract class WsClientGenerator extends Generator {

    /** service-ref describing client dependency on a webservice */
    private ServiceRefDesc ref;

    /** jonas-service-ref modifier */
    private WsClientDDModifier modifier;

    /** archive */
    private Archive archive;

    /**
     * Creates a new WsClientGenerator.
     *
     * @param config Generator Configuration
     * @param serviceRef client dependency on a webservice
     * @param ddm jonas-service-ref modifier
     * @param arch modified archive
     *
     * @throws GenBaseException When instanciation fails
     */
    public WsClientGenerator(Config config, ServiceRefDesc serviceRef, WsClientDDModifier ddm, Archive arch)
            throws GenBaseException {
        super(config);
        ref = serviceRef;
        modifier = ddm;
        archive = arch;
    }
    /**
     * @return the archive.
     */
    public Archive getArchive() {
        return archive;
    }
    /**
     * @return the modifier.
     */
    public WsClientDDModifier getModifier() {
        return modifier;
    }
    /**
     * @return the ref.
     */
    public ServiceRefDesc getRef() {
        return ref;
    }
}