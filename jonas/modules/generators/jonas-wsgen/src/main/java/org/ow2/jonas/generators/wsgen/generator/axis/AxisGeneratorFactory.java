/**
 * JOnAS : Java(TM) OpenSource Application Server
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial Developer : Guillaume Sauthier
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
*/

package org.ow2.jonas.generators.wsgen.generator.axis;

import org.ow2.jonas.deployment.ws.ServiceDesc;
import org.ow2.jonas.deployment.ws.ServiceRefDesc;
import org.ow2.jonas.generators.genbase.GenBaseException;
import org.ow2.jonas.generators.genbase.archive.Archive;
import org.ow2.jonas.generators.wsgen.WsGenException;
import org.ow2.jonas.generators.wsgen.ddmodifier.WebServicesDDModifier;
import org.ow2.jonas.generators.wsgen.ddmodifier.WsClientDDModifier;
import org.ow2.jonas.generators.wsgen.ddmodifier.WsEndpointDDModifier;
import org.ow2.jonas.generators.wsgen.generator.GeneratorFactory;
import org.ow2.jonas.generators.wsgen.generator.WsClientGenerator;
import org.ow2.jonas.generators.wsgen.generator.WsEndpointGenerator;




/**
 * GeneratorFactory impl for Axis.
 *
 * @deprecated
 * @author Guillaume Sauthier
 */
public class AxisGeneratorFactory extends GeneratorFactory {
    /**
     * Return a new WsClientGenerator for the specific generation mecanism.
     *
     * @param serviceRef the service-ref containing information for client side
     *        generation process.
     * @param ddm the XML modifier.
     * @param archive the Archive to be modified
     *
     * @return a new WsClientGenerator.
     *
     * @throws GenBaseException When creation fails.
     * @throws WsGenException When creation fails.
     */
    public WsClientGenerator newGenerator(ServiceRefDesc serviceRef,
        WsClientDDModifier ddm, Archive archive) throws GenBaseException, WsGenException {
        return new AxisWsClientGenerator(getConfiguration(), serviceRef, ddm, archive);
    }

    /**
     * Return a new WsEndpointGenerator for the specific generation mecanism.
     *
     * @param serviceDesc the webservice-description containing information for
     *        server side generation process.
     * @param ddm the XML modifier.
     * @param wsddm webservices.xml DD modifier
     *
     * @return a new WsEndpointGenerator.
     *
     * @throws GenBaseException When creation fails.
     * @throws WsGenException When creation fails.
     */
    public WsEndpointGenerator newGenerator(ServiceDesc serviceDesc,
        WsEndpointDDModifier ddm, WebServicesDDModifier wsddm, Archive arch) throws GenBaseException, WsGenException {
        return new AxisWsEndpointGenerator(getConfiguration(), serviceDesc, ddm, wsddm, arch);
    }
}
