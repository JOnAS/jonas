/**
 * JOnAS : Java(TM) OpenSource Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.generators.wsgen.generator.ews.wsdltoj2ee.emitter;

import org.ow2.jonas.generators.wsgen.generator.ews.wsdltoj2ee.JOnASJ2EEWebServicesContext;


/**
 * JOnAS interface for code Emitter
 * @author Guillaume Sauthier
 */
public interface JOnASWSEmitter {

    /**
     * @return Returns the Ws Context
     */
    JOnASJ2EEWebServicesContext getJOnASWsContext();

    /**
     * @param context set the WS Context
     */
    void setJOnASWsContext(JOnASJ2EEWebServicesContext context);

    /**
     * @return true if needs to generate bindings
     */
    boolean hasBindingGeneration();

    /**
     * @return true if needs to generate services
     */
    boolean hasServiceGeneration();

    /**
     * @return true if needs to generate deploy files
     */
    boolean hasDeployGeneration();

    /**
     * @return the classLoader.
     */
    ClassLoader getClassLoader();

    /**
     * Set the classloader to use
     * @param classLoader The classLoader to set.
     */
    void setClassLoader(ClassLoader classLoader);
}
