/**
 * JOnAS : Java(TM) OpenSource Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.generators.wsgen.generator.ews.wsdltoj2ee.wscf;

import org.apache.ws.ews.context.webservices.server.impl.AbstractWSCFInitParam;


/**
 * JOnAS WSCFInitParam implementation.
 * That's used to fit into the EWS model.
 * @author Guillaume Sauthier
 */
public class JOnASWSCFInitParam extends AbstractWSCFInitParam {

    /**
     * @param paramName parameter name
     * @param initParam parameter value
     */
    public JOnASWSCFInitParam(String paramName, String initParam) {
        this.paramName = paramName;
        this.paramValue = initParam;
    }

}
