/**
 * JOnAS : Java(TM) OpenSource Application Server
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial Developer : Guillaume Sauthier
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.generators.wsgen.ddmodifier;

import org.ow2.jonas.lib.util.Log;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;


import org.objectweb.util.monolog.api.Logger;

/**
 * Modify an Element from a Deployment Descriptor. Contains XML commons
 * utilities.
 *
 * @author Guillaume Sauthier
 */
public class DeploymentDescModifier {

    /** J2EE Default XML namespace */
    protected static final String J2EE_NS = "http://java.sun.com/xml/ns/j2ee";

    /** JOnAS Default XML namespace */
    protected static final String JONAS_NS = "http://www.objectweb.org/jonas/ns";

    /** logger */
    private static Logger logger = Log.getLogger(Log.JONAS_WSGEN_PREFIX);

    /** base element */
    private Element element;

    /** parent element */
    private Element parent;

    /** base document */
    private Document doc;

    /**
     * Create a new DeploymentDescModifier to update the given Element.
     *
     * @param element XML element to be modified.
     * @param doc base document for Element creation.
     */
    public DeploymentDescModifier(Element element, Document doc) {
        this(element, doc, null);
    }

    /**
     * Create a new DeploymentDescModifier to update the given Element.
     *
     * @param element XML element to be modified.
     * @param doc base document for Element creation.
     * @param parent parent Element, used if element is null
     */
    public DeploymentDescModifier(Element element, Document doc, Element parent) {
        this.element = element;
        this.doc = doc;
        this.parent = parent;
    }

    /**
     * Create a new Element with given name in J2EE XML namespace.
     *
     * @param name Element name
     *
     * @return the created Element
     */
    protected Element newJ2EEElement(String name) {
        return doc.createElementNS(J2EE_NS, name);
    }

    /**
     * Create a new Element with given name in J2EE XML namespace with an inner
     * Text Node.
     *
     * @param name Element name
     * @param text Element content
     *
     * @return the created Element
     */
    protected Element newJ2EEElement(String name, String text) {
        Element e = doc.createElementNS(J2EE_NS, name);
        Text txt = doc.createTextNode(text);
        e.appendChild(txt);

        return e;
    }

    /**
     * Create a new Element with given name in JOnAS XML namespace.
     *
     * @param name Element name
     *
     * @return the created Element
     */
    protected Element newJOnASElement(String name) {
        return doc.createElementNS(JONAS_NS, name);
    }

    /**
     * Create a new Element with given name in JOnAS XML namespace, with an
     * inner Text Node.
     *
     * @param name Element name
     * @param text node's text
     *
     * @return the created Element
     */
    protected Element newJOnASElement(String name, String text) {
        Element e = doc.createElementNS(JONAS_NS, name);
        Text txt = doc.createTextNode(text);
        e.appendChild(txt);

        return e;
    }


    /**
     * Create a new Element with given name
     *
     * @param name Element name
     * @return the created Element
     */
    protected Element newElement (String name) {
         return doc.createElement(name);
    }

    /**
     * Create a new Element with an inner Text Node.
     *
     * @param name Element name
     * @param text node's text
     *
     * @return the created Element
     */
    protected Element newElement (String name, String text) {
        Element e = doc.createElement(name);
        Text txt = doc.createTextNode(text);
        e.appendChild(txt);

        return e;
    }

    /**
     * @return Returns the logger.
     */
    public static Logger getLogger() {
        return logger;
    }

    /**
     * @return Returns the parent (can be null).
     */
    public Element getParent() {
        return parent;
    }

    /**
     * @return Returns the element.
     */
    public Element getElement() {
        return element;
    }

    /**
     * @return Returns the document.
     */
    public Document getDocument() {
        return doc;
    }

    /**
     * Set the new document to use
     * @param doc the new document to use
     */
    public void setDocument(Document doc) {
        this.doc = doc;
        this.parent = doc.getDocumentElement();
    }

    /**
     * Set the new base Element to use
     * @param e the new base Element to use
     */
    public void setElement(Element e) {
        element = e;
        getParent().appendChild(e);
    }

}