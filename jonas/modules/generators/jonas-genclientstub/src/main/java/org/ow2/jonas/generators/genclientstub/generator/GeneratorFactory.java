/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.generators.genclientstub.generator;

import org.ow2.jonas.generators.genbase.generator.Config;

/**
 * A <code>GeneratorFactory</code> has to be extended by specific generation
 * mecanism.
 * @author Florent Benoit
 */
public class GeneratorFactory implements org.ow2.jonas.generators.genbase.generator.GeneratorFactory {

    /** <code>GeneratorFactory</code> unique instance */
    private static GeneratorFactory instance = null;

    /** Configuration to set on instanciated Generator */
    private Config configuration;

    /**
     * Utility class, no constructor
     */
    private GeneratorFactory() {

    }

    /**
     * Returns the unique GeneratorFactory instance.
     * @return the unique GeneratorFactory instance.
     */
    public static GeneratorFactory getInstance() {
        if (instance == null) {
            instance = newInstance();
        }

        return instance;
    }

    /**
     * Create a new generatorFactory instance
     * @return a new generatorFactory instance.

     */
    private static GeneratorFactory newInstance() {
        return new GeneratorFactory();
    }

    /**
     * Set the Configuration to use with newly created Generator.
     * @param config the Configuration to use with newly created Generator.
     */
    public void setConfiguration(Config config) {
        this.configuration = config;
    }

    /**
     * Get the Configuration to use with newly created Generator.
     * @return the Configuration to use with newly created Generator
     */
    public Config getConfiguration() {
        return configuration;
    }
}