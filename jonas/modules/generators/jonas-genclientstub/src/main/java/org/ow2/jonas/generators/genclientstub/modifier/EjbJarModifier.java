/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.generators.genclientstub.modifier;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import org.ow2.jonas.deployment.api.IEJBRefDesc;
import org.ow2.jonas.generators.genbase.GenBaseException;
import org.ow2.jonas.generators.genbase.archive.Archive;
import org.ow2.jonas.generators.genbase.archive.Ejb;
import org.ow2.jonas.generators.genbase.archive.EjbJar;
import org.ow2.jonas.generators.genclientstub.ClientStubGenException;
import org.ow2.jonas.generators.genclientstub.generator.Generator;
import org.ow2.jonas.generators.genclientstub.generator.GeneratorFactory;


import org.objectweb.util.monolog.api.BasicLevel;

/**
 * Modify a given EjbJar.
 * @author Florent Benoit
 */
public class EjbJarModifier extends AbsArchiveModifier {

    /**
     * Modified ejbjar
     */
    private EjbJar ejbjar = null;

    /**
     * Creates a new EjbJarModifier object.
     * @param ejbjar EjbJar Archive
     */
    public EjbJarModifier(EjbJar ejbjar) {
        super(ejbjar);
        this.ejbjar = ejbjar;
    }

    /**
     * modify the current EjbJar. If EjbJar is contained in not an application
     * and have webservices endpoints, A DummyApplication is created and
     * modification process launched against the newly created application. If
     * EjbJar is contained in an application + webservices endpoints, a
     * DummyWebApp is created to hold "facade" servlet managing SOAP processing.
     * @return an EjbJar or an Application Archive
     * @throws GenBaseException When generation or storing fails
     * @throws ClientStubGenException When generation or storing fails
     */
    public Archive modify() throws GenBaseException, ClientStubGenException {

        getLogger().log(BasicLevel.INFO, "Processing EjbJar " + ejbjar.getName());

        GeneratorFactory gf = GeneratorFactory.getInstance();

        // Found automatically the stubs
        generateFoundStubs(gf.getConfiguration(), ejbjar);

        // Ejb-Ref
        List ejbs = ejbjar.getEjbs();
        for (Iterator i = ejbs.iterator(); i.hasNext();) {
            Ejb ejb = (Ejb) i.next();
            List ejbRefs = ejb.getEjbRefDescs();
            for (Iterator j = ejbRefs.iterator(); j.hasNext();) {
                IEJBRefDesc ejbRef = (IEJBRefDesc) j.next();

                // launch generation
                Generator g = new Generator(gf.getConfiguration(), ejbRef, null, ejbjar);
                g.generate();
                g.compile();
                // add files in web archive
                g.addFiles(ejbjar);
            }
        }

        return save(gf.getConfiguration(), "ejbjars" + File.separator + ejbjar.getRootFile().getName());

    }
}