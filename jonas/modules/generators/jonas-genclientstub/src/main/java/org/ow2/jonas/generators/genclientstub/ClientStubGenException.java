/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.generators.genclientstub;

import org.ow2.jonas.generators.genbase.GenBaseException;

/**
 * This class represents the exception that can be throwned by the ClientStubGen tool.
 * @author Florent Benoit
 */
public class ClientStubGenException extends GenBaseException {

    /**
     * Constructs a ClientStubGenException with no specified detail message.
     */
    public ClientStubGenException() {
        super();
    }

    /**
     * Constructs an ClientStubGenException with the specified detail message.
     * @param msg Error message
     */
    public ClientStubGenException(String msg) {
        super(msg);
    }

    /**
     * Constructs an ClientStubGenException with the specified detail message.
     * @param inner Cause Exception to wrap
     */
    public ClientStubGenException(Exception inner) {
        super(inner);
    }

    /**
     * Constructs an ClientStubGenException with the specified detail message.
     * @param msg Error message
     * @param inner Cause Exception to wrap
     */
    public ClientStubGenException(String msg, Exception inner) {
        super(msg, inner);
    }
}