/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.generators.genic;

import org.ow2.jonas.lib.ejb21.JavaType;

/**
 * This class is the "Velocity context" for a parameter of a where clause of finder method for CMP1 only,
 * used in the Velocity Templates.
 * @author Helene Joanin : Initial developer
 */
public class VcParamWhere {

    /**
     * name of the parameter (ie "p1")
     */
    private String  mName;

    /**
     * type's name of the parameter
     */
    private String  mTypeName;

    /**
     * SQL type's name of the parameter
     */
    private String  mSqlTypeName;

    /**
     * SQL setter method name for this parameter
     */
    private String  mSqlSetMethod;

    /**
     * true if the parameter hasn't a java primitive type
     */
    private boolean hasNotPrimitiveType;

    /**
     * true if the parameter has the java.math.BigInteger type
     */
    private boolean hasBigIntegerType;

    /**
     * true if the parameter has a Serializable type
     */
    private boolean hasSerializableType;

    /**
     * true if the parameter has a java.lang.* type except java.lang.string
     */
    private boolean hasJavaLangTypeExceptString;

    /**
     * VcParamWhere Constructor
     * @param type parameter's type
     * @param position parameter's position in the where clause
     */
    VcParamWhere(Class type, int position) {

        mName = new String("p" + position);
        mTypeName = JavaType.getName(type);
        mSqlTypeName = JavaType.getSQLType(type);
        mSqlSetMethod = JavaType.getSQLSetMethod(type);
        if (mSqlSetMethod == null) {
            throw new Error("Cannot container persistence manage the type '"
                            + type.getName() + "'");
        }
        hasNotPrimitiveType = !type.isPrimitive();
        hasBigIntegerType = type.equals(java.math.BigInteger.class);
        hasSerializableType = "setSerializable".equals(mSqlSetMethod);
        hasJavaLangTypeExceptString = false;
        if (type.getPackage() != null) {
            if ("java.lang".equals(type.getPackage().getName())
                && !java.lang.String.class.equals(type)) {
                hasJavaLangTypeExceptString = true;
            }
        }

        // System.out.print("VcParamWhere: "+type.toString());
        // System.out.println(toString());
    }

    /**
     * @return Returns the name of the parameter (ie "p1")
     */
    public String getName() {
        return mName;
    }

    /**
     * @return Returns the type's name of the parameter
     */
    public String getTypeName() {
        return mTypeName;
    }

    /**
     * @return Returns the SQL type's name of the parameter
     */
    public String getSqlTypeName() {
        return mSqlTypeName;
    }

    /**
     * @return Returns the SQL setter method associated to the parameter
     */
    public String getSqlSetMethod() {
        return mSqlSetMethod;
    }

    /**
     * @return Returns true if the parameter's type is not a java primitive type
     */
    public boolean hasNotPrimitiveType() {
        return hasNotPrimitiveType;
    }

    /**
     * @return Returns true if the parameter's type is java.math.BigInteger
     */
    public boolean hasBigIntegerType() {
        return hasBigIntegerType;
    }

    /**
     * @return Returns true if the parameter's type is Serializable
     */
    public boolean hasSerializableType() {
        return hasSerializableType;
    }

    /**
     * @return true if the parameter's type is a java.lang.* type except java.lang.string
     */
    public boolean hasJavaLangTypeExceptString() {
        return hasJavaLangTypeExceptString;
    }

    /**
     * @return Returns a string representation of the VcParamWhere object to debug use
     */
    public String toString() {
        StringBuffer ret = new StringBuffer();
        ret.append("\n    Name                 = " + getName());
        ret.append("\n    TypeName             = " + getTypeName());
        ret.append("\n    SqlTypeName          = " + getSqlTypeName());
        ret.append("\n    SqlSetMethod         = " + getSqlSetMethod());
        ret.append("\n    hasNotPrimitiveType  = " + hasNotPrimitiveType());
        ret.append("\n    hasBigIntegerType    = " + hasBigIntegerType());
        ret.append("\n    hasSerializableType  = " + hasSerializableType());
        ret.append("\n    hasJavaLangTypeExceptString = " + hasJavaLangTypeExceptString());
        return (ret.toString());
    }

}
