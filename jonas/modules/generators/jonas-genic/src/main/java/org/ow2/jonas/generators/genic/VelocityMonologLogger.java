/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.generators.genic;

import org.apache.velocity.runtime.RuntimeServices;
import org.apache.velocity.runtime.log.LogSystem;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * Wrap the velocity logger into a Monolog logger.
 * @author Florent Benoit
 */
public class VelocityMonologLogger implements LogSystem {

    /**
     * Monolog logger.
     */
    private Logger monologLogger = null;

    /**
     * Constructor.
     * @param monologLogger the given monolog logger.
     */
    public VelocityMonologLogger(final Logger monologLogger) {
        this.monologLogger = monologLogger;
    }

    /**
     * Do nothing.
     * @param services velocity services
     * @throws Exception if there are errors.
     */
    public void init(final RuntimeServices services) throws Exception {
    }

    /**
     * Logs the given message at the specified level.
     * @param level the given level of log
     * @param message the given message
     */
    public void logVelocityMessage(final int level, final String message) {
        switch (level) {
        case LogSystem.WARN_ID:
            monologLogger.log(BasicLevel.WARN, message);
            break;
        case LogSystem.ERROR_ID:
            monologLogger.log(BasicLevel.ERROR, message);
            break;
        case LogSystem.INFO_ID:
            monologLogger.log(BasicLevel.INFO, message);
            break;
        case LogSystem.DEBUG_ID:
            monologLogger.log(BasicLevel.DEBUG, message);
            break;
        default:
            // log in debug by default
            monologLogger.log(BasicLevel.DEBUG, message);
        }

    }

}
