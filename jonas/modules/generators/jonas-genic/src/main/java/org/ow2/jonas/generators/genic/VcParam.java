/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.generators.genic;

import org.ow2.jonas.deployment.ejb.BeanDesc;
import org.ow2.jonas.lib.ejb21.JavaType;
import org.ow2.jonas.lib.ejb21.jorm.JormType;


/**
 * This class is the "Velocity context" for a parameter of a finder/select method for CMP2 only,
 * used in the Velocity Templates.
 * @author Helene Joanin : Initial developer
 */
public class VcParam {

    /**
     * Type's name of the parameter
     */
    private String mTypeName = null;

    /**
     * JORM type's name of the parameter
     */
    private String mJormTypeName = null;

    /**
     * true if the parameter is a local bean
     */
    private boolean mIsEjbLocal = false;
    /**
     * In case the parameter is a local bean, name of this local bean
     */
    private String mEjbName = null;

    /**
     * VcParam Constructor
     * @param type parameter class
     * @param dd bean descriptor
     */
    VcParam(Class type, BeanDesc dd) {
        mTypeName = JavaType.getName(type);
        mJormTypeName = JormType.getPTypeDef(type, false);
        mIsEjbLocal = javax.ejb.EJBLocalObject.class.isAssignableFrom(type);
        if (mIsEjbLocal) {
            BeanDesc bdp = dd.getDeploymentDesc().getBeanDescWithLocalInterface(mTypeName);
            if (bdp == null) {
                throw new Error("VcParam: Cannot get the BeanDesc associated to the interface local '" + mTypeName + "'");
            }
            mEjbName = bdp.getEjbName();
        }
    }

    /**
     * @return Returns the type's name of the parameter
     */
    public String getTypeName() {
        return mTypeName;
    }

    /**
     * @return Returns the JORM type's name of the parameter
     */
    public String getJormType() {
        return mJormTypeName;
    }

    /**
     * @return Returns true if the parameter is a local bean
     */
    public boolean isEjbLocal() {
        return mIsEjbLocal;
    }

    /**
     * @return If the parameter is a local bean, returns the name of this local bean
     */
    public String getEjbName() {
        if (!isEjbLocal()) {
            throw new Error("VcParam.getEjbName(): No BeanDesc associated to the param '"
                            + mTypeName + "'");
        }
        return mEjbName;
    }

}

