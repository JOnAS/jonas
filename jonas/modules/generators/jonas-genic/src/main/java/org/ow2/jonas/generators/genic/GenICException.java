/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.generators.genic;

/**
 * This class represents the exception that can be throwned by the GenIC tool.
 * 
 * @author Helene Joanin : Initial developer
 */
public class GenICException extends Exception {

    protected Exception inner = null;

    /**
     * Constructs an GenICException with no specified detail message.
     */
    public GenICException() {
        super();
    }

    /**
     * Constructs an GenICException with the specified detail message.
     */
    public GenICException(String msg) {
        super(msg);
    }

    public GenICException(String msg, Exception inner) {
        super(msg);
        this.inner = inner;

    }

    public String toString() {
        String s = GenICException.class.getName() + ": " + super.getMessage();
        if (inner == null) {
            return (s);
        } else {
            return (s + " (" + inner.toString() + ")");
        }
    }

    public String getMessage() {
        String s = super.getMessage();
        if (inner == null) {
            return (s);
        } else {
            return (s + " (" + inner.getMessage() + ")");
        }
    }

    public void printStackTrace() {
        System.out.println(getMessage());
        if (inner != null) {
            inner.printStackTrace();
        }
    }
    public void printStackTrace(java.io.PrintStream s) {
        s.println(getMessage());
        if (inner != null) {
            inner.printStackTrace(s);
        }
    }
    public void printStackTrace(java.io.PrintWriter s) {
        s.println(getMessage());
        if (inner != null) {
            inner.printStackTrace(s);
        }
    }
}
