/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.generators.genic;

import java.io.File;
import java.util.ArrayList;

import org.ow2.jonas.lib.ejb21.Protocols;


/**
 * GenIC Parameters holder.
 *
 * @author Guillaume Sauthier
 */
public class GenICParameters {

    /**
     * Supported protocol list.
     */
    private Protocols protocols = null;

    /**
     * directory where files will be generated.
     */
    private String outputDirectory = null;

    /**
     * is verbose mode ?
     */
    private boolean verbose = false;

    /**
     * do not delete intermediate generated source files.
     */
    private boolean keepGenerated = false;

    /**
     * compile the generated source files via the java and
     * rmi compilers.
     */
    private boolean compil = true;

    /**
     * display the help message.
     */
    private boolean help = false;

    /**
     * add the generated classes in the given ejb-jar
     * file.
     */
    private boolean addInJar = true;

    /**
     * parse the XML deployment descriptors with
     * validation.
     */
    private boolean parseWithValidation = true;

    /**
     * invoke, in some case, directly the method of the java
     * class corresponding to the command.
     */
    private boolean invokeCmd = false;

    /**
     * use fastRMIC compiler.
     */
    private boolean fastRmicEnabled = true;

    /**
     * file name of the standard deployment descriptor (.xml ended),
     * or file name of the EJB-jar (.jar ended).
     */
    private String inputFilename = null;

    /**
     * name of the java compiler to use.
     */
    private String javacName = null;

    /**
     * options to pass to the java compiler.
     */
    private ArrayList javacOptions = new ArrayList();

    /**
     * options to pass to the rmi compiler.
     */
    private ArrayList rmicOptions = new ArrayList();

    /**
     * define the classpath to be used to compile classes.
     */
    private String classpathParam = null;

    /**
     * Generated working copy file.
     */
    private String workingFilename = null;

    /**
     * @return Returns the outputDirectory.
     */
    public String getOutputDirectory() {
        return outputDirectory;
    }

    /**
     * @param outputDirectory The outputDirectory to set.
     */
    public void setOutputDirectory(final String outputDirectory) {
        this.outputDirectory = outputDirectory;
    }

    /**
     * @return Returns the protocols.
     */
    public Protocols getProtocols() {
        return protocols;
    }

    /**
     * @param protocols The protocols to set.
     */
    public void setProtocols(final Protocols protocols) {
        this.protocols = protocols;
    }

    /**
     * @return Returns the verbose.
     */
    public boolean isVerbose() {
        return verbose;
    }

    /**
     * @param verbose The verbose to set.
     */
    public void setVerbose(final boolean verbose) {
        this.verbose = verbose;
    }

    /**
     * @return Returns the addInJar.
     */
    public boolean isAddInJar() {
        return addInJar;
    }

    /**
     * @param addInJar The addInJar to set.
     */
    public void setAddInJar(final boolean addInJar) {
        this.addInJar = addInJar;
    }

    /**
     * @return Returns the classpathParam.
     */
    public String getClasspathParam() {
        return classpathParam;
    }

    /**
     * @param classpathParam The classpathParam to set.
     */
    public void setClasspathParam(final String classpathParam) {
        this.classpathParam = classpathParam;
    }

    /**
     * @return Returns the compil.
     */
    public boolean isCompil() {
        return compil;
    }

    /**
     * @param compil The compil to set.
     */
    public void setCompil(final boolean compil) {
        this.compil = compil;
    }

    /**
     * @return Returns the fastRmicEnabled.
     */
    public boolean isFastRmicEnabled() {
        return fastRmicEnabled;
    }

    /**
     * @param fastRmicEnabled The fastRmicEnabled to set.
     */
    public void setFastRmicEnabled(final boolean fastRmicEnabled) {
        this.fastRmicEnabled = fastRmicEnabled;
    }

    /**
     * @return Returns the help.
     */
    public boolean isHelp() {
        return help;
    }

    /**
     * @param help The help to set.
     */
    public void setHelp(final boolean help) {
        this.help = help;
    }

    /**
     * @return Returns the inputFilename.
     */
    public String getInputFilename() {
        return inputFilename;
    }

    /**
     * @param inputFilename The inputFilename to set.
     */
    public void setInputFilename(final String inputFilename) {
        this.inputFilename = inputFilename;
    }

    /**
     * @return Returns the original (unchanged) inputFilename.
     */
    public String getOriginalFilename() {
        return inputFilename;
    }

    /**
     * @return Returns the working copy inputFilename.
     */
    public String getWorkingFilename() {

        // Only do that once
        if (workingFilename == null) {

            // prepend an almost unique prefix to the original filename
            File input = new File(inputFilename);
            String filename = input.getName();
            File parent = input.getParentFile();

            File out = new File(parent, "GenICWorkingCopyJar-" + filename);

            workingFilename = out.getPath();
        }
        return workingFilename;
    }

    /**
     * @return Returns the invokeCmd.
     */
    public boolean isInvokeCmd() {
        return invokeCmd;
    }

    /**
     * @param invokeCmd The invokeCmd to set.
     */
    public void setInvokeCmd(final boolean invokeCmd) {
        this.invokeCmd = invokeCmd;
    }

    /**
     * @return Returns the javacName.
     */
    public String getJavacName() {
        return javacName;
    }

    /**
     * @param javacName The javacName to set.
     */
    public void setJavacName(final String javacName) {
        this.javacName = javacName;
    }

    /**
     * @return Returns the keepGenerated.
     */
    public boolean isKeepGenerated() {
        return keepGenerated;
    }

    /**
     * @param keepGenerated The keepGenerated to set.
     */
    public void setKeepGenerated(final boolean keepGenerated) {
        this.keepGenerated = keepGenerated;
    }

    /**
     * @return Returns the parseWithValidation.
     */
    public boolean isParseWithValidation() {
        return parseWithValidation;
    }

    /**
     * @param parseWithValidation The parseWithValidation to set.
     */
    public void setParseWithValidation(final boolean parseWithValidation) {
        this.parseWithValidation = parseWithValidation;
    }

    /**
     * @return Returns the javacOptions.
     */
    public ArrayList getJavacOptions() {
        return javacOptions;
    }

    /**
     * @return Returns the rmicOptions.
     */
    public ArrayList getRmicOptions() {
        return rmicOptions;
    }


}
