/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2003-2007 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.generators.genbase.modifier;



/**
 * Used to create the right ArchiveModifier from a given filename (ear, jar,
 * war, ...).
 *
 * @author Guillaume Sauthier
 * @author Florent Benoit (make common class for client stubs and WsGen)
 */
public abstract class AbsModifierFactory {

     /**
     * Application Archive mode.
     */
    public static final int APPLICATION = 0;

    /**
     * EjbJar Archive mode.
     */
    public static final int EJBJAR = 1;

    /**
     * WebApp Archive mode.
     */
    public static final int WEBAPP = 2;

    /**
     * ApplicationClient Archive mode.
     */
    public static final int CLIENT = 3;

    /**
     * Utility class (no default public constructor).
     */
    protected AbsModifierFactory() {

    }

}
