/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2003-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.generators.genbase.modifier;

import java.io.File;

import org.ow2.jonas.generators.genbase.GenBaseException;
import org.ow2.jonas.generators.genbase.archive.Archive;
import org.ow2.jonas.generators.genbase.archive.FileArchive;
import org.ow2.jonas.generators.genbase.archive.J2EEArchive;
import org.ow2.jonas.generators.genbase.archive.JarArchive;
import org.ow2.jonas.generators.genbase.generator.Config;
import org.ow2.jonas.generators.genbase.utils.ArchiveStorer;
import org.ow2.jonas.generators.genbase.utils.DirStorer;
import org.ow2.jonas.generators.genbase.utils.JarStorer;
import org.ow2.jonas.lib.util.Log;



import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * Modify a J2EEArchive
 * @author Guillaume Sauthier
 */
public abstract class ArchiveModifier {

    /**
     * J2EEArchive to be modified
     */
    private J2EEArchive archive;

    /** logger */
    private static Logger logger = Log.getLogger(Log.JONAS_GENBASE_PREFIX);

    /**
     * Creates a new ArchiveModifier object.
     * @param archive rchive to be modified
     */
    public ArchiveModifier(J2EEArchive archive) {
        this.archive = archive;
    }

    /**
     * Modify the current archive and return a modified archive.
     * @return a modified archive.
     * @throws GenBaseException When Archive modification fails
     */
    public abstract Archive modify() throws GenBaseException;

    /**
     * Save the curernt archive usin given configuration and given out filename.
     * @param config configuration to use
     * @param outname filename
     * @param archive archive to be saved
     * @return the save Archive
     * @throws GenBaseException is save fails.
     */
    protected static Archive save(Config config, String outname, J2EEArchive archive) throws GenBaseException {
        ArchiveStorer storer = null;
        File endfile = new File(config.getOut(), outname);
        Archive out = null;

        if (config.getSaveMode() == Config.PACKED) {
            if (logger.isLoggable(BasicLevel.DEBUG)) {
                logger.log(BasicLevel.DEBUG, "Saving '" + endfile + "' in packed form...");
            }
            storer = new JarStorer(archive, endfile);
            storer.store();
            out = new JarArchive(endfile);
        } else {
            if (logger.isLoggable(BasicLevel.DEBUG)) {
                logger.log(BasicLevel.DEBUG, "Saving '" + endfile + "' in unpacked form...");
            }
            storer = new DirStorer(archive, endfile);
            storer.store();
            out = new FileArchive(endfile);
        }

        return out;

    }

    /**
     * Save the current archive using given configuration and given out filename.
     * @param config configuration to use
     * @param outname filename
     * @return the save Archive
     * @throws GenBaseException is save fails.
     */
    protected Archive save(Config config, String outname) throws GenBaseException {
        return save(config, outname, archive);
    }

    /**
     * @return the logger.
     */
    public static Logger getLogger() {
        return logger;
    }

    /**
     * @return returns the J2EE Archive which wil be modified.
     */
    public J2EEArchive getArchive() {
        return this.archive;
    }
}