/**
 * JOnAS : Java(TM) OpenSource Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.generators.genbase.archive;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.jar.Manifest;

/**
 * An <code>Archive</code> is a Wrapper around a Jar file (ear, war, jar) or
 * around a directory structured lik a Jar file (unpacked jar file for example).
 *
 * @author Guillaume Sauthier
 */
public interface Archive {

    /**
     * add the content of the given directory into the root of the archive.
     *
     * @param directory directory to add
     */
    void addDirectory(File directory);

    /**
     * add the content of the given directory into the given directory of the
     * archive.
     *
     * @param dirName archive directory name.
     * @param directory directory to add.
     */
    void addDirectoryIn(String dirName, File directory);

    /**
     * add a lonely file into the root directory of the archive.
     *
     * @param file the file to be added.
     */
    void addFile(File file);

    /**
     * add a file into the root directory of the archive with a specified name.
     *
     * @param file the file to be added.
     * @param name filename
     */
    void addFile(File file, String name);

    /**
     * add a lonely file into the given directory of the archive.
     *
     * @param dirName archive directory name.
     * @param file the file to be added.
     */
    void addFileIn(String dirName, File file);

    /**
     * Returns the File corresponding to the root of the archive.
     *
     * @return the File corresponding to the root of the archive.
     */
    File getRootFile();

    /**
     * Returns the Manifest of the Archive.
     *
     * @return the Manifest of the Archive.
     */
    Manifest getManifest();

    /**
     * Returns an InputStream corresponding to the given filename.
     *
     * @param filename file name source of the InputStream
     *
     * @return the InputStream corresponding to the given filename.
     *
     * @throws IOException When InputStream cannot be retrieved for filename.
     */
    InputStream getInputStream(String filename) throws IOException;

    /**
     * Returns a List of all files contained in this archive. Original files in
     * jar, added Files are all included as String in this Enumeration.
     *
     * @return a List of all files contained in this archive.
     */
    List getContainedFiles();

    /**
     * Returns true if archive is packed or false if archive is unpacked.
     *
     * @return true if archive is packed or false if archive is unpacked.
     */
    boolean isPacked();

    /**
     * Returns the name of the Archive.
     *
     * @return the name of the Archive.
     */
    String getName();

    /**
     * Close this archive
     * @throws IOException if close fail
     */
    void close() throws IOException;
}