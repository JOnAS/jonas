/*
 * JOnAS : Java(TM) OpenSource Application Server
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Initial Developer : Guillaume Sauthier
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.generators.genbase.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.jar.JarOutputStream;
import java.util.zip.ZipEntry;

import org.ow2.jonas.generators.genbase.GenBaseException;
import org.ow2.jonas.generators.genbase.archive.J2EEArchive;


/**
 * Store a J2EEArchive in a Jar file.
 *
 * @author Guillaume Sauthier
 */
public class JarStorer extends ArchiveStorer {

    /**
     * Jar file filled as OutputStream
     */
    private JarOutputStream jos;

    /**
     * Creates a new JarStorer object.
     *
     * @param archive archive to be stored
     * @param jar outout jar file
     *
     * @throws GenBaseException When cannot create output jar
     */
    public JarStorer(J2EEArchive archive, File jar) throws GenBaseException {
        super(archive);

        setOut(jar.getAbsolutePath());

        try {
            if (!jar.getParentFile().exists()) {
                if (!jar.getParentFile().mkdirs()) {
                    String err = getI18n().getMessage("JarStorer.constr.create", jar.getParentFile());
                    throw new GenBaseException(err);
                }
            }

            jos = new JarOutputStream(new FileOutputStream(jar), archive.getManifest());
        } catch (IOException ioe) {
            String err = getI18n().getMessage("JarStorer.constr.ioe", jar);
            throw new GenBaseException(err, ioe);
        }
    }

    /**
     * Convert a name from any format in Jar filename format
     *
     * @param name filename to be converted
     *
     * @return converted filename
     */
    protected String convertName(String name) {
        return name.replace('\\', '/');
    }

    /**
     * Add a file in Jar
     *
     * @param name file name to be added
     *
     * @throws IOException when filling fails.
     */
    protected void addFile(String name) throws IOException {
        ZipEntry ze = new ZipEntry(convertName(name));
        jos.putNextEntry(ze);

        InputStream is = getArchive().getInputStream(name);
        fill(is, jos);
        is.close();
    }

    /**
     * Store the archive and close the streams.
     *
     * @throws GenBaseException When Output Jar cannot be closed
     */
    public void store() throws GenBaseException {
        super.store();

        try {
            jos.flush();
            jos.close();
        } catch (IOException ioe) {
            String err = getI18n().getMessage("JarStorer.store.close");
            throw new GenBaseException(err, ioe);
        }
    }

    /**
     * returns the OuputStream corresponding to the given filename.
     *
     * @param name filename to create
     *
     * @return An OutputStream
     *
     * @throws IOException When file creation fails.
     */
    protected OutputStream getOutputStream(String name) throws IOException {
        ZipEntry ze = new ZipEntry(convertName(name));
        jos.putNextEntry(ze);

        return jos;
    }
}