/**
 * JOnAS : Java(TM) OpenSource Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.generators.genbase.archive;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Vector;
import java.util.jar.Manifest;

import org.ow2.jonas.generators.genbase.GenBaseException;



/**
 * A <code>FileArchive</code> is a wrapper for directory structured as a jar.
 *
 * @author Guillaume Sauthier
 */
public class FileArchive extends AbsArchive {

    /**
     * Creates a new FileArchive object.
     *
     * @param archive directory structured as a jar
     *
     * @throws GenBaseException When manifest cannot be loaded
     */
    public FileArchive(File archive) throws GenBaseException {
        super(archive);

        try {
            File mf = new File(archive, "META-INF" + File.separator + "MANIFEST.MF");

            if (mf.exists()) {
                InputStream is = new FileInputStream(mf);
                setManifest(new Manifest(is));
                is.close();
            } else {
                setManifest(new Manifest());
            }
        } catch (Exception e) {
            String err = getI18n().getMessage("FileArchive.constr.manifest", getRootFile());
            throw new GenBaseException(err, e);
        }
    }

    /**
     * Returns an InputStream corresponding to the given filename.
     *
     * @param filename file name source of the InputStream
     *
     * @return the InputStream corresponding to the given filename.
     *
     * @throws IOException When InputStream corersponding to the given filename
     *         cannot be found.
     */
    public InputStream getInputStream(String filename) throws IOException {
        File file = (File) getFiles().get(filename);

        if (file == null) {
            // filename not found in added files
            // try root search
            file = new File(getRootFile(), filename);

            if (!file.exists()) {
                return null;
            }
        }

        // file exists (in added files or in original archive)
        return new FileInputStream(file);
    }

    /**
     * Returns a List of all files contained in this archive. Original files in
     * jar, added Files are all included as String in this Enumeration.
     *
     * @return a List of all files contained in this archive.
     */
    public List getContainedFiles() {
        List list = new Vector(getFiles().keySet());

        // add files of the original archive
        traverse("", getRootFile(), list);

        return list;
    }

    /**
     * Add all files contained in the given directory into a list.
     *
     * @param dirName current directory name
     * @param base directory where file are listed
     * @param map list of filename
     */
    private static void traverse(String dirName, File base, List map) {
        File[] childs = base.listFiles();

        // directory exists ?
        if (childs != null) {
            for (int i = 0; i < childs.length; i++) {
                if (childs[i].isFile()) {
                    // File
                    map.add(dirName + childs[i].getName());
                } else {
                    // Directory
                    traverse(dirName + childs[i].getName() + File.separator, childs[i], map);
                }
            }
        }
    }

    /**
     * Returns true if archive is packed or false if archive is unpacked.
     *
     * @return true if archive is packed or false if archive is unpacked.
     */
    public boolean isPacked() {
        return false;
    }
}