/**
 * JOnAS : Java(TM) OpenSource Application Server
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial Developer : Guillaume Sauthier
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.generators.genbase.utils;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

/**
 * A <code>TempRepository</code> object is tye main container for temporary
 * directories. When an object of WsGen or ClientStubGen wants to use a temporary directory, it
 * ask TempRepository instance to create a new one. It is usefull when multiples
 * tempo directories are created. Deletion is much more simpler. The
 * TempRepository is a Singleton.
 *
 * @author Guillaume Sauthier
 */
public class TempRepository {

    /** the TempRepository instance */
    private static TempRepository instance = null;

    /** list of temporary directories */
    private List directories;

    /**
     * Create a new empty TempRepository.
     */
    private TempRepository() {
        directories = new Vector();
    }

    /**
     * Return the only instance of TempRepository.
     *
     * @return the only instance of TempRepository.
     */
    public static TempRepository getInstance() {
        if (instance == null) {
            instance = new TempRepository();
        }

        return instance;
    }

    /**
     * Add a temporary File in the repository (file or directory).
     *
     * @param file the temporary file or directory to add.
     */
    public void addDir(File file) {
        directories.add(file);
    }

    /**
     * Delete all the files contained in the repository. Returns true when
     * deletion is successfull, false otherwise.
     *
     * @return true when deletion is successfull, false otherwise.
     */
    public boolean deleteAll() {
        boolean result = true;

        for (Iterator i = directories.iterator(); i.hasNext();) {
            File dir = (File) i.next();
            i.remove();

            // delete only if file exists
            if (dir.exists()) {
                result &= delete(dir);
            }
        }

        return result;
    }

    /**
     * Delete a file or directory recursively
     *
     * @param f file or directory to be deleted
     *
     * @return true if deletion ok, false otherweise.
     */
    private boolean delete(File f) {
        if (f.isFile()) {
            return f.delete();
        } else {
            File[] childs = f.listFiles();
            if (childs == null) {
                // no childs
                return f.delete();
            } else {
                // childs
                boolean result = true;
                for (int i = 0; i < childs.length; i++) {
                    result &= delete(childs[i]);
                }
                return result && f.delete();
            }
        }
    }

    /**
     * Return an empty temporary directory and automatically add it into the
     * repository.
     *
     * @return an empty temporary directory.
     *
     * @throws IOException when creation fails.
     */
    public File createDir() throws IOException {
        return createDir(null);
    }

    /**
     * Return an empty temporary directory and automatically add it into the
     * repository.
     *
     * @return an empty temporary directory.
     *
     * @throws IOException when creation fails.
     */
    public File createDir(final String extension) throws IOException {
        // create file
        File tmp = File.createTempFile("genbase", extension);

        if (!tmp.delete()) {
            throw new IOException("Cannot delete temporary file");
        }

        // create directory
        if (!tmp.mkdir()) {
            throw new IOException("Cannot create temporary directory");
        }

        // add in repo
        addDir(tmp);

        return tmp;
    }
}