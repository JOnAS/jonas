/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2003-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.generators.genbase.generator;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

import org.ow2.jonas.lib.bootstrap.loader.JClassLoader;
import org.ow2.jonas.lib.util.Env;

/**
 * Configuration object storing generation params.
 */
public class Config {

    /**
     * packed mode.
     */
    public static final int PACKED = 0;

    /**
     * unpacked mode.
     */
    public static final int UNPACKED = 1;

    /**
     * Java executable name.
     */
    private String nameJava = "java";

    /**
     * Javac executable name.
     */
    private String nameJavac = "javac";

    /**
     * Rmic executable name.
     */
    private String nameRmic = "rmic";

    /**
     * Javac options.
     */
    private List<String> javacOpts = new ArrayList<String>();

    /**
     * Bin directory for JDK.
     */
    private String javaHomeBin = null;

    /**
     * no config needed .
     */
    private boolean noConfig = false;

    /**
     * verbose mode.
     */
    private boolean verbose = false;

    /**
     * debug mode .
     */
    private boolean debug = false;

    /**
     * classpath.
     */
    private String classpath = ".";

    /**
     * output directory.
     */
    private File out = new File(".");

    /**
     * keep alrady generated files.
     */
    private boolean keepGenerated = false;

    /**
     * validating parser ?
     */
    private boolean parseWithValidation = true;

    /**
     * configuration error.
     */
    private boolean error = false;

    /**
     * file input name.
     */
    private String inputname;

    /** help requested ? */
    private boolean help = false;

    /**
     * Save mode (PACKED/UNPACKED).
     */
    private int saveMode = PACKED;

    /**
     * DTDs use is allowed.
     */
    private boolean dtdsAllowed = false;

    /**
     * Creates a new Config. Automatically setup javaHomeBin property. And
     * create classpath from classloader.
     */
    public Config() {
        // Setup java_home/bin directory
        setJavaHomeBin(System.getProperty("java.home", ""));

        if (!("".equals(getJavaHomeBin()))) {
            if (Env.isOsMacOsX()) {
                setJavaHomeBin(getJavaHomeBin() + File.separator + "bin" + File.separator);
            } else {
                // JRE Directory !
                setJavaHomeBin(getJavaHomeBin() + File.separator + ".." + File.separator + "bin" + File.separator);
            }
        }

        // classpath
        URLClassLoader ucl = (URLClassLoader) (Thread.currentThread().getContextClassLoader());

        // Try to detect the mode
        if (ucl instanceof JClassLoader) {
            setClasspath(((JClassLoader) ucl).getClassPath());
        } else {
            setClasspath(extractURLs(ucl));
        }
    }

    /**
     * @param ucl the ClassLoader to search in
     * @return Returns the classpath representation of that ClassLoader
     */
    private String extractURLs(final URLClassLoader ucl) {
        URL[] urls = ucl.getURLs();
        // for each URL
        StringBuffer sb = new StringBuffer();
        boolean first = true;
        for (int i = 0; i < urls.length; i++) {
            String file = urls[i].getFile();
            if (!"".equals(file)) {
                if (first) {
                    sb.append(file);
                    first = false;
                } else {
                    sb.append(File.pathSeparator + file);
                }
            }
        }
        return sb.toString();
    }

    /**
     * @param nameJavac Set javac command name to use
     */
    public void setNameJavac(final String nameJavac) {
        this.nameJavac = nameJavac;
    }

    /**
     * @return Returns the javac command name to use
     */
    public String getNameJavac() {
        return nameJavac;
    }

    /**
     * @param javacOpts Set javac Opts
     */
    public void setJavacOpts(final List<String> javacOpts) {
        this.javacOpts = javacOpts;
    }

    /**
     * @return Returns the Javac Opts to use
     */
    public List<String> getJavacOpts() {
        return javacOpts;
    }

    /**
     * @param javaHomeBin JAVA_HOME/bin directory
     */
    public void setJavaHomeBin(final String javaHomeBin) {
        this.javaHomeBin = javaHomeBin;
    }

    /**
     * @return Returns the JAVA_HOME/bin directory
     */
    public String getJavaHomeBin() {
        return javaHomeBin;
    }

    /**
     * @param noConfig Generate Configuration ?
     */
    public void setNoConfig(final boolean noConfig) {
        this.noConfig = noConfig;
    }

    /**
     * @return Returns noConfig option
     */
    public boolean isNoConfig() {
        return noConfig;
    }

    /**
     * @param verbose Verbose ?
     */
    public void setVerbose(final boolean verbose) {
        this.verbose = verbose;
    }

    /**
     * @return Returns verbose option
     */
    public boolean isVerbose() {
        return verbose;
    }

    /**
     * @param debug Debug ?
     */
    public void setDebug(final boolean debug) {
        this.debug = debug;
    }

    /**
     * @return Returns debug option
     */
    public boolean isDebug() {
        return debug;
    }

    /**
     * @param classpath Classpath to use with java commands
     */
    public void setClasspath(final String classpath) {
        this.classpath = classpath;
    }

    /**
     * @return Retruns Classpath
     */
    public String getClasspath() {
        return classpath;
    }

    /**
     * @param out Output directory
     */
    public void setOut(final File out) {
        this.out = out;
    }

    /**
     * @return Returns Ouput directory
     */
    public File getOut() {
        return out;
    }

    /**
     * @param keepGenerated Kepp Generated files ?
     */
    public void setKeepGenerated(final boolean keepGenerated) {
        this.keepGenerated = keepGenerated;
    }

    /**
     * @return Returns keepGenerated option
     */
    public boolean isKeepGenerated() {
        return keepGenerated;
    }

    /**
     * @param parseWithValidation Parse XML desc with validation ?
     */
    public void setParseWithValidation(final boolean parseWithValidation) {
        this.parseWithValidation = parseWithValidation;
    }

    /**
     * @return Returns validation
     */
    public boolean isParseWithValidation() {
        return parseWithValidation;
    }

    /**
     * @param error Error Mode ?
     */
    public void setError(final boolean error) {
        this.error = error;
    }

    /**
     * @return Returns true if there is Configuration errors
     */
    public boolean isError() {
        return error;
    }

    /**
     * @param inputname File inputname
     */
    public void setInputname(final String inputname) {
        this.inputname = inputname;
    }

    /**
     * @return Returns file input name
     */
    public String getInputname() {
        return inputname;
    }

    /**
     * @param help Help Mode ?
     */
    public void setHelp(final boolean help) {
        this.help = help;
    }

    /**
     * @return Returns Help option.
     */
    public boolean isHelp() {
        return help;
    }

    /**
     * Set Packed Mode for storing.
     */
    public void setSavePacked() {
        this.saveMode = PACKED;
    }

    /**
     * Set UnPacked Mode for storing.
     */
    public void setSaveUnpacked() {
        this.saveMode = UNPACKED;
    }

    /**
     * @return Returns Save mode
     */
    public int getSaveMode() {
        return saveMode;
    }

    /**
     * @return Returns the nameRmic.
     */
    public String getNameRmic() {
        return nameRmic;
    }

    /**
     * @param nameRmic The nameRmic to set.
     */
    public void setNameRmic(final String nameRmic) {
        this.nameRmic = nameRmic;
    }

    /**
     * @return the java command
     */
    public String getNameJava() {
        return nameJava;
    }

    /**
     * @return true if the use of DTDs is allowed.
     */
    public boolean isDTDsAllowed() {
        return dtdsAllowed;
    }

    /**
     * Use of DTDs.
     * @param dTDsAllowed The dtdsAllowed to set.
     */
    public void setDTDsAllowed(final boolean dTDsAllowed) {
        this.dtdsAllowed = dTDsAllowed;
    }
}
