/**
 * JOnAS : Java(TM) OpenSource Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.generators.genbase.archive;

import java.util.List;
import java.util.Vector;

import org.ow2.jonas.deployment.api.IEJBRefDesc;
import org.ow2.jonas.deployment.api.IServiceRefDesc;
import org.ow2.jonas.deployment.ejb.BeanDesc;
import org.w3c.dom.Element;




/**
 * Ejb represent a Bean in an EjbJar.
 * @author Guillaume Sauthier
 */
public class Ejb implements EjbRefModule, WsClient {

    /**
     * service-ref list
     */
    private List sRefs;

    /**
     * ejb-ref list
     */
    private List ejbRefs;

    /**
     * bean name
     */
    private String name;

    /**
     * jonas- element (session, entity, message)
     */
    private Element jbean;

    /**
     * Creates a new Ejb from a BeanDesc
     * @param bd the BeanDesc
     * @param bean jonas specific bean Element
     *        (jonas-session/jonas-entity/jonas-message-driven)
     */
    public Ejb(BeanDesc bd, Element bean) {
        // get name
        name = bd.getEjbName();

        // store jonas informations
        jbean = bean;

        // we want a List of service-ref
        sRefs = new Vector();

        IServiceRefDesc[] refs = bd.getServiceRefDesc();

        for (int i = 0; i < refs.length; i++) {
            sRefs.add(refs[i]);
        }

        // List of ejb-refs
        ejbRefs = new Vector();
        IEJBRefDesc[] refDesc = bd.getEjbRefDesc();

        for (int i = 0; i < refDesc.length; i++) {
            ejbRefs.add(refDesc[i]);
        }
    }

    /**
     * Returns the list of service-ref elements contained by a module.
     * @return the list of service-ref elements contained by a module.
     */
    public List getServiceRefDescs() {
        return sRefs;
    }


    /**
     * Returns the list of ejb-ref elements contained by a module.
     * @return the list of ejb-ref elements contained by a module.
     */
    public List getEjbRefDescs() {
        return ejbRefs;
    }

    /**
     * Returns the bean name.
     * @return the bean name.
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the bean element.
     * @return the bean element.
     */
    public Element getJonasBeanElement() {
        return jbean;
    }

    /**
     * Close this archive
     */
    public void close() {
        sRefs = null;
        ejbRefs = null;
        name = null;
        jbean = null;
    }

}