/**
 * JOnAS : Java(TM) OpenSource Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.generators.genbase.archive;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import org.ow2.jonas.generators.genbase.GenBaseException;
import org.ow2.jonas.generators.genbase.utils.TempRepository;
import org.ow2.util.file.FileUtils;
import org.ow2.util.file.FileUtilsException;

/**
 * DummyWebApp is a wrapper for auto generated webapp archive.
 *
 * @author Guillaume Sauthier
 */
public class DummyWebApp extends WebApp {

    /** webapp name */
    private String name;

    /**
     * Creates a new DummyWebApp archive.
     *
     * @param app the file containing the webapp archive.
     * @param name webapp filename
     *
     * @throws GenBaseException when archive creation fails.
     */
    public DummyWebApp(Application app, String name) throws GenBaseException {
        super(createFileArchive(), app);
        this.name = name;
    }

    /**
     * Create a new FileArchive
     *
     * @return a new Archive
     *
     * @throws GenBaseException When Archive creation fails.
     */
    private static Archive createFileArchive() throws GenBaseException {
        TempRepository tr = TempRepository.getInstance();

        try {
            File tmp = tr.createDir();
            File meta = new File(tmp, "META-INF");
            meta.mkdirs();

            File web = new File(tmp, "WEB-INF");
            web.mkdirs();

            File manifest = new File(meta, "MANIFEST.MF");

            Manifest mf = new Manifest();
            mf.getMainAttributes().putValue(Attributes.Name.MANIFEST_VERSION.toString(), "1.0");

            OutputStream os = new FileOutputStream(manifest);
            mf.write(os);
            os.flush();
            os.close();

            // Provide the basis for web.xml
            File webXml = new File(web, "web.xml");
            InputStream wis = DummyWebApp.class.getResourceAsStream("web.xml");
            if (wis == null) {
                throw new GenBaseException("Cannot load 'web.xml' template.");
            }
            FileUtils.dump(wis, webXml);

            // Close stream
            wis.close();

            //Load context.xml, used by Tomcat to determine security realm to use (if any)
            // Provide the basis for context.xml
            File context = new File(meta, "context.xml");
            InputStream cis = DummyWebApp.class.getResourceAsStream("context.xml");
            if (cis == null) {
                throw new GenBaseException("Cannot load 'context.xml' template.");
            }
            FileUtils.dump(cis, context);
            // Close stream
            cis.close();

            //Load web-jetty.xml, used by Jetty to determine security realm to use (if any)
            // Provide the basis for web-jetty.xml
            File jetty = new File(web, "web-jetty.xml");
            InputStream jis = DummyWebApp.class.getResourceAsStream("web-jetty.xml");
            if (jis == null) {
                throw new GenBaseException("Cannot load 'web-jetty.xml' template.");
            }
            FileUtils.dump(jis, jetty);
            // Close stream
            jis.close();


            return new FileArchive(tmp);
        } catch (IOException ioe) {
            throw new GenBaseException(ioe);
        } catch (FileUtilsException fue) {
            throw new GenBaseException(fue);
        }
    }

    /**
     * @return Returns the DummyApplication filename.
     */
    public String getName() {
        return name;
    }

}