/**
 * JOnAS : Java(TM) OpenSource Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.generators.genbase.archive;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import org.ow2.jonas.generators.genbase.GenBaseException;
import org.ow2.jonas.generators.genbase.utils.TempRepository;
import org.ow2.jonas.generators.genbase.utils.XMLUtils;
import org.ow2.util.file.FileUtils;
import org.ow2.util.file.FileUtilsException;

/**
 * DummyApplication is a wrapper for auto generated application archive.
 *
 * @author Guillaume Sauthier
 */
public class DummyApplication extends Application {

    /** application filename */
    private String name;

    /**
     * Creates a new Application archive.
     *
     * @param name the file containing the application archive.
     *
     * @throws GenBaseException When Init fails
     */
    public DummyApplication(final String name) throws GenBaseException {
        super(createFileArchive(), null);
        this.name = name;
        init();
    }

    /**
     * Create a new FileArchive
     *
     * @return a new Archive
     *
     * @throws GenBaseException When Archive creation fails.
     */
    private static Archive createFileArchive() throws GenBaseException {
        TempRepository tr = TempRepository.getInstance();

        try {
            File tmp = tr.createDir();
            File meta = new File(tmp, "META-INF");
            meta.mkdirs();

            File manifest = new File(meta, "MANIFEST.MF");

            Manifest mf = new Manifest();
            mf.getMainAttributes().putValue(Attributes.Name.MANIFEST_VERSION.toString(), "1.0");

            OutputStream os = new FileOutputStream(manifest);
            mf.write(os);
            os.flush();
            os.close();

            // Provide the basis for application.xml
            File application = new File(meta, "application.xml");
            InputStream is = DummyApplication.class.getResourceAsStream("application.xml");
            if (is == null) {
                throw new GenBaseException("Cannot load 'application.xml' template.");
            }
            FileUtils.dump(is, application);

            // Close stream
            is.close();

            return new FileArchive(tmp);
        } catch (IOException ioe) {
            throw new GenBaseException(ioe);
        } catch (FileUtilsException fue) {
            throw new GenBaseException(fue);
        }
    }

    /**
     * Initialize the DummyApplication. Creates modules lists. Overriddes
     * Application.init() behavior
     *
     * @throws GenBaseException When Descriptor cannot be parsed.
     */
    protected void init() throws GenBaseException {
        // remove dummy modules
        setEjbjars(new Vector());
        setWebapps(new Vector());
        setClients(new Vector());

        loadDescriptors();

        XMLUtils.cleanDummyApplication(getApp());

    }

    /**
     * @return Returns the DummyApplication filename.
     */
    public String getName() {
        return name;
    }

}