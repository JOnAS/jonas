/**
 * JOnAS : Java(TM) OpenSource Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.generators.genbase.archive;

import java.io.File;
import java.util.Hashtable;
import java.util.Map;
import java.util.jar.Manifest;

import org.ow2.jonas.lib.util.I18n;


/**
 * An <code>AbsArchive</code> centralized commonly used methods for Jar and
 * File support.
 *
 * @author Guillaume Sauthier
 */
public abstract class AbsArchive implements Archive {

    /** i18n */
    private static I18n i18n = I18n.getInstance(AbsArchive.class);

    /** root File of the Archive */
    private File root;

    /** the archive Manifest */
    private Manifest manifest = null;

    /** map between name and files added in the archive */
    private Map files = null;

    /**
     * Create a FileArchive where the root if the given file.
     *
     * @param file the directory base of the archive
     */
    public AbsArchive(File file) {
        root = file;
        files = new Hashtable();
    }

    /**
     * add the content of the given directory into the root of the archive.
     *
     * @param directory directory to add
     */
    public void addDirectory(File directory) {
        addDirectoryIn("", directory);
    }

    /**
     * add the content of the given directory into the given directory of the
     * archive.
     *
     * @param dirName archive directory name.
     * @param directory directory to add.
     */
    public void addDirectoryIn(String dirName, File directory) {
        File[] childs = directory.listFiles();

        // directory exists ?
        if (childs != null) {
            for (int i = 0; i < childs.length; i++) {
                if (childs[i].isFile()) {
                    // File
                    addFileIn(dirName, childs[i]);
                } else {
                    // Directory
                    addDirectoryIn(dirName + childs[i].getName() + File.separator, childs[i]);
                }
            }
        }
    }

    /**
     * add a lonely file into the root directory of the archive.
     *
     * @param file the file to be added.
     */
    public void addFile(File file) {
        addFileIn("", file);
    }

    /**
     * add a file into the root directory of the archive with a specified name.
     *
     * @param file the file to be added.
     * @param name filename
     */
    public void addFile(File file, String name) {
        files.put(name, file);
    }

    /**
     * add a lonely file into the given directory of the archive.
     *
     * @param dirName archive directory name.
     * @param file the file to be added.
     */
    public void addFileIn(String dirName, File file) {
        files.put(dirName + file.getName(), file);
    }

    /**
     * Returns the File corresponding to the root of the archive.
     *
     * @return the File corresponding to the root of the archive.
     */
    public File getRootFile() {
        return root;
    }

    /**
     * Returns the name of the Archive.
     *
     * @return the name of the Archive.
     */
    public String getName() {
        return root.getName();
    }

    /**
     * @return Returns the manifest.
     */
    public Manifest getManifest() {
        if (manifest == null) {
            manifest = new Manifest();
        }
        return manifest;
    }
    /**
     * @param manifest The manifest to set.
     */
    public void setManifest(Manifest manifest) {
        this.manifest = manifest;
    }
    /**
     * @return Returns the i18n.
     */
    public static I18n getI18n() {
        return i18n;
    }
    /**
     * @return Returns the files.
     */
    public Map getFiles() {
        return files;
    }

    /**
     * close this archive
     */
    public void close() {
        this.files = null;
        this.manifest = null;
    }
}