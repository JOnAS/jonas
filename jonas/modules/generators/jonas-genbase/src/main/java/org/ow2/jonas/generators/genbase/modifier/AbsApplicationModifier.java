/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2003-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.generators.genbase.modifier;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.ow2.jonas.generators.genbase.GenBaseException;
import org.ow2.jonas.generators.genbase.archive.Application;
import org.ow2.jonas.generators.genbase.archive.Archive;
import org.ow2.jonas.generators.genbase.generator.Config;


import org.objectweb.util.monolog.api.BasicLevel;

/**
 * Modify a given Application.
 * @author Guillaume Sauthier
 * @author Florent Benoit
 */
public abstract class AbsApplicationModifier extends ArchiveModifier {

    /**
     * ejbjar modifiers
     */
    private List ejbModifiers = null;

    /**
     * web modifiers
     */
    private List webModifiers = null;

    /**
     * client modifiers
     */
    private List cltModifiers = null;

    /**
     * Wrapped application
     */
    private Application app = null;

    /**
     * Configuration used to save
     */
    private Config config = null;

    /**
     * Creates a new ApplicationModifier.
     * @param archive the Application J2EE archive
     * @param config the configuration object
     */
    public AbsApplicationModifier(Application archive, Config config) {
        super(archive);
        this.config = config;
        app = archive;
        initVectors();
        init();
    }

    /**
     * Init the vectors of modifiers
     */
    private void initVectors() {
        // create lists
        ejbModifiers = new Vector();
        webModifiers = new Vector();
        cltModifiers = new Vector();
    }

    /**
     * initialize modifier
     */
    protected abstract void init();

    /**
     * Modify the current archive and return a modified archive.
     * @return a modified archive.
     * @throws GenBaseException When modifications fails
     */
    public Archive modify() throws GenBaseException {

        getLogger().log(BasicLevel.INFO, "Processing Application " + app.getName());

        /**
         * Modify inner modules
         */
        for (Iterator i = webModifiers.iterator(); i.hasNext();) {
            ArchiveModifier wm = (ArchiveModifier) i.next();
            Archive a = wm.modify();
            if (getLogger().isLoggable(BasicLevel.DEBUG)) {
                getLogger().log(BasicLevel.DEBUG, "Modifying WebApp '" + a.getName() + "'");
            }
            app.addFile(a.getRootFile());
        }

        for (Iterator i = cltModifiers.iterator(); i.hasNext();) {
            ArchiveModifier cm = (ArchiveModifier) i.next();
            Archive a = cm.modify();
            if (getLogger().isLoggable(BasicLevel.DEBUG)) {
                getLogger().log(BasicLevel.DEBUG, "Modifying Client '" + a.getName() + "'");
            }
            app.addFile(a.getRootFile());
        }

        for (Iterator i = ejbModifiers.iterator(); i.hasNext();) {
            ArchiveModifier em = (ArchiveModifier) i.next();
            Archive a = em.modify();
            if (getLogger().isLoggable(BasicLevel.DEBUG)) {
                getLogger().log(BasicLevel.DEBUG, "Modifying EjbJar '" + a.getName() + "'");
            }
            app.addFile(a.getRootFile());
        }

        return save(config, "apps" + File.separator + app.getName());

    }

    /**
     * @return the cltModifiers.
     */
    protected List getCltModifiers() {
        return cltModifiers;
    }

    /**
     * @return the ejbModifiers.
     */
    protected List getEjbModifiers() {
        return ejbModifiers;
    }

    /**
     * @return the webModifiers.
     */
    protected List getWebModifiers() {
        return webModifiers;
    }
    /**
     * @return the application element.
     */
    protected Application getApplication() {
        return app;
    }
}