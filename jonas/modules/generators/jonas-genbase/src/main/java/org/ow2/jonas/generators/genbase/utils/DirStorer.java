/*
 * JOnAS : Java(TM) OpenSource Application Server
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Initial Developer : Guillaume Sauthier
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.generators.genbase.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.ow2.jonas.generators.genbase.GenBaseException;
import org.ow2.jonas.generators.genbase.archive.J2EEArchive;


/**
 * Store a J2EEArchive in an unpacked form.
 *
 * @author Guillaume Sauthier
 */
public class DirStorer extends ArchiveStorer {

    /**
     * directory name containing unpacked archive.
     */
    private File base;

    /**
     * Creates a new DirStorer object.
     *
     * @param archive archive to be stored
     * @param dir output directory
     *
     * @throws GenBaseException if cannot create output directory
     */
    public DirStorer(J2EEArchive archive, File dir) throws GenBaseException {
        super(archive);

        setOut(dir.getAbsolutePath());

        // assure base exists
        if (!dir.exists()) {
            if (!dir.mkdirs()) {
                String err = getI18n().getMessage("DirStorer.constr.create", dir);
                throw new GenBaseException(err);
            }
        }

        base = dir;
    }

    /**
     * Convert a filename to a local filesystem filename.
     *
     * @param name name to be converted
     *
     * @return converted filename
     */
    protected String convertName(String name) {
        return name.replace('/', File.separatorChar);
    }

    /**
     * add a given file in the filesystem.
     *
     * @param name filename
     *
     * @throws IOException When fill fails.
     */
    protected void addFile(String name) throws IOException {
        OutputStream fos = getOutputStream(name);
        InputStream is = getArchive().getInputStream(name);
        fill(is, fos);
        is.close();
        fos.close();
    }

    /**
     * returns the OuputStream corresponding to the given filename.
     *
     * @param name filename to create
     *
     * @return An OutputStream
     *
     * @throws IOException When file creation fails.
     */
    protected OutputStream getOutputStream(String name) throws IOException {
        File out = new File(base, convertName(name));
        File parent = out.getParentFile();

        if (!parent.exists()) {
            if (!parent.mkdirs()) {
                String err = getI18n().getMessage("DirStorer.getOutputStream.create", out);
                throw new IOException(err);
            }
        }

        OutputStream os = new FileOutputStream(out);

        return os;
    }
}