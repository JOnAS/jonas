/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

/**
 *
 */
package org.ow2.jonas.eclipse.compiler;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

/**
 * Eclipse Compiler Utilities.
 * @author Guillaume Sauthier
 */
public class Util {

    /**
     * Maximum buffer size.
     */
    private static final int MAX_BUFFER_SIZE = 8192;

    /**
     * Transform a given {@link InputStream} into a char array. May return null.
     * @param is the {@link InputStream} to convert
     * @param charset Encoding to use.
     * @return the content of the {@link InputStream} as a char array
     */
    public static char[] toCharArray(final InputStream is, final String charset) {
        try {
            char[] result = null;
            Reader reader = new BufferedReader(new InputStreamReader(is, charset));
            if (reader != null) {
                char[] chars = new char[MAX_BUFFER_SIZE];
                StringBuffer buf = new StringBuffer();
                int count;
                while ((count = reader.read(chars, 0, chars.length)) > 0) {
                    buf.append(chars, 0, count);
                }
                result = new char[buf.length()];
                buf.getChars(0, result.length, result, 0);
                return result;
            }
        } catch (IOException e) {
            // do nothing
            return null;
        }

        // return null if something failed
        return (char[]) null;
    }

    /**
     * Convert a given {@link InputStream} in a byte array.
     * @param is Class file input
     * @return the byte[] content of the given {@link InputStream}
     * @throws IOException if InputStream manipulation fails.
     */
    public static byte[] toByteArray(final InputStream is) throws IOException {

        byte[] buf = new byte[MAX_BUFFER_SIZE];
        ByteArrayOutputStream baos = new ByteArrayOutputStream(buf.length);
        int count;
        while ((count = is.read(buf, 0, buf.length)) > 0) {
            baos.write(buf, 0, count);
        }
        baos.flush();
        return baos.toByteArray();
    }

    /**
     * Get the fully qualified Java class name from a source
     * file (system path + .java).
     * @param source Java Source file name
     * @return the fully qualified classname
     */
    public static String getClassName(final String source) {
        String classname = source.replace(File.separatorChar, '.');
        // remove trailing .java
        return classname.substring(0, classname.lastIndexOf('.'));
    }
}
