/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.eclipse.compiler;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jdt.internal.compiler.impl.CompilerOptions;

/**
 * The {@link CompilationContext} defines all the options needed by the eclipse compiler.
 * @author Guillaume Sauthier
 */
public class CompilationContext {

    /**
     * The directory where sources files are located.
     */
    private File sourceDirectory;

    /**
     * The directory where all generated class files will be placed.
     */
    private File outputDirectory;

    /**
     * List of source files relatives to the base source directory.
     */
    private List<String> sources;

    /**
     * Source file encoding.
     */
    private String encoding;

    /**
     * Contextual ClassLoader used for compilation.
     */
    private ClassLoader contextualClassLoader;

    /**
     * Eclipse Compiler options.
     */
    private CompilerOptions compilerOptions;

    /**
     * @return the sourceDirectory
     */
    public File getSourceDirectory() {
        return sourceDirectory;
    }

    /**
     * @param sourceDirectory the sourceDirectory to set
     */
    public void setSourceDirectory(final File sourceDirectory) {
        this.sourceDirectory = sourceDirectory;
    }

    /**
     * @return the outputDirectory
     */
    public File getOutputDirectory() {
        return outputDirectory;
    }

    /**
     * @param outputDirectory the outputDirectory to set
     */
    public void setOutputDirectory(final File outputDirectory) {
        this.outputDirectory = outputDirectory;
    }

    /**
     * @return the sources
     */
    public List<String> getSources() {
        // return an empty list if no source files have been provided.
        if (sources == null) {
            sources = new ArrayList<String>();
        }
        return sources;
    }

    /**
     * @param sources the sources to set
     */
    public void setSources(final List<String> sources) {
        this.sources = sources;
    }

    /**
     * @return the encoding
     */
    public String getEncoding() {
        //default to UTF-8
        if (encoding == null) {
            encoding = "UTF-8";
        }
        return encoding;
    }

    /**
     * @param encoding the encoding to set
     */
    public void setEncoding(final String encoding) {
        this.encoding = encoding;
    }

    /**
     * @return the contextualClassLoader
     */
    public ClassLoader getContextualClassLoader() {
        // if not set, use the TCCL
        if (contextualClassLoader == null) {
            contextualClassLoader = Thread.currentThread().getContextClassLoader();
        }
        return contextualClassLoader;
    }

    /**
     * @param contextualClassLoader the contextualClassLoader to set
     */
    public void setContextualClassLoader(final ClassLoader contextualClassLoader) {
        this.contextualClassLoader = contextualClassLoader;
    }

    /**
     * @return the compilerOptions
     */
    public CompilerOptions getCompilerOptions() {
        if (compilerOptions == null) {
            compilerOptions = getDefaultOptions();
        }
        return compilerOptions;
    }

    /**
     * @param compilerOptions the compilerOptions to set
     */
    public void setCompilerOptions(final CompilerOptions compilerOptions) {
        this.compilerOptions = compilerOptions;
    }

    /**
     * @param filename relative path.
     * @return the absolute path to the file.
     */
    public String getAbsolutePath(final String filename) {
        File path = new File(sourceDirectory, filename);
        return path.getAbsolutePath();
    }

    /**
     * @return the default set of compiler options.
     */
    private CompilerOptions getDefaultOptions() {

        // Configure compiler options
        Map<String, String> settings = new HashMap<String, String>();
        settings.put(CompilerOptions.OPTION_LocalVariableAttribute, CompilerOptions.GENERATE);
        settings.put(CompilerOptions.OPTION_Source, CompilerOptions.VERSION_1_5);
        settings.put(CompilerOptions.OPTION_TargetPlatform, CompilerOptions.VERSION_1_5);
        settings.put(CompilerOptions.OPTION_ReportDeprecation, CompilerOptions.IGNORE);
        settings.put(CompilerOptions.OPTION_LineNumberAttribute, CompilerOptions.GENERATE);
        settings.put(CompilerOptions.OPTION_SourceFileAttribute, CompilerOptions.GENERATE);

        CompilerOptions options = new CompilerOptions();
        options.set(settings);

        return options;
    }
}
