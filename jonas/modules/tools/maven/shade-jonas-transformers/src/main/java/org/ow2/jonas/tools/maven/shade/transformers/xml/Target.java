/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id:
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.tools.maven.shade.transformers.xml;

import org.ow2.util.maven.plugin.mergeproperties.core.Resource;
import org.w3c.dom.Element;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a XML target tag
 * @author Jeremy Cazaux
 */
public class Target implements Comparable {

    /**
     * Template file associate to the target
     */
    private Resource resource;

    /**
     * List of child target. A children target is a XML file which is called by an antcall of this current target
     */
    private List<Target> children;

    /**
     * Parent target
     */
    private Target parent;

    /**
     * Dom Element of a Target
     */
    private Element element;

    /**
     * Is true if this object is a master targe.
     */
    private boolean isMasterTarget;

    /**
     * Default constructor
     */
    public Target() {
        this.children = new ArrayList<Target>();
    }

    /**
     *
     * @return the name of the target
     */
    public String getName() {
        return element.getAttribute("name");
    }

    /**
     *
     * @return the template file associate to the target
     */
    public Resource getResource() {
        return resource;
    }

    /**
     *
     * Add a child to the list of child target
     * @param child
     */
    public void addChild(Target child) {
        this.children.add(child);
    }

    /**
     *
     * @return  children of this target
     */
    public List<Target> getChildren() {
        return children;
    }

    /**
     *
     * @return  the parent target
     */
    public Target getParent() {
        return parent;
    }

    /**
     *
     * @return true if this object is a master target. Otherwise, it returns false
     */
    public boolean isMasterTarget() {
        return isMasterTarget;
    }

    /**
     *
     * @param resource  The file associated to this target is set
     */
    public void setResource(Resource resource) {
        this.resource = resource;
    }

    /**
     *
     * @param parent Parent target is set
     */
    public void setParent(Target parent) {
        this.parent = parent;
    }

    /**
     *
     * @param masterTarget Set if this target is the master
     */
    public void setMasterTarget(boolean masterTarget) {
        isMasterTarget = masterTarget;
    }

    /**
     *
     * @param element Set the target Element
     */
    public void setElement(Element element) {
        this.element = element;
    }

    /**
     *
     * @return the target Element
     */
    public Element getElement() {
        return element;
    }

    /**
     * Compare two target object
     *
     * @param o Object
     * @return a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater
     * than the second.
     */
    public int compareTo(Object o) {
        Target target = (Target) o;

        //Targets of the master template file have the highest priority
        //Other targets are sort by target name in ascending order
        if (this.isMasterTarget && !target.isMasterTarget) {
            return -1;
        } else if (!this.isMasterTarget && target.isMasterTarget) {
            return 1;
        } else if (this.isMasterTarget && target.isMasterTarget){
            return 0;
        } else {
            return element.getAttribute("name").compareTo(target.element.getAttribute("name"));
        }
    }
}
