/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.tool.maven.packaging;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.ow2.jonas.packaging.internal.PackagingManager;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * A {@code ApplicationPackagingMojo} is responsible to generate an addon
 * of the application
 *
 * @author Mohammed Boukada
 * @goal generate-application-addon
 * @phase generate-resources
 * @requiresProject false
 */
public class ApplicationPackagingMojo extends AbstractMojo {

    /**
     * @parameter expression="${urlCloudApplication}"
     * @readonly
     * @required
     */
    private String urlCloudApplication;

    /**
     * @parameter expression="${urlEnvironmentTemplate}"
     * @readonly
     * @optional
     */
    private String urlEnvironmentTemplate;

    /**
     * @parameter expression="${urlMappingTopology}"
     * @readonly
     * @optional
     */
    private String urlMappingTopology;

    /**
     * @parameter expression="${tenant-id}"
     *            default-value=""
     * @readonly
     * @optional
     */
    private String tenantId;

    /**
     * @parameter expression="${outPutDirectory}"
     * @readonly
     * @required
     */
    private String outPutDirectory;

    public void execute() throws MojoExecutionException, MojoFailureException {

        // Quick exit
        if (urlCloudApplication == null && outPutDirectory == null) {
            return;
        }
        try {
            generateAddon();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void generateAddon() throws Exception {
        PackagingManager packagingManager = new PackagingManager();
        if (urlEnvironmentTemplate != null && urlMappingTopology != null &&
                !"".equals(urlEnvironmentTemplate) && !"".equals(urlMappingTopology)) {
            packagingManager.generateAddon(getUrl(urlCloudApplication), tenantId, getUrl(urlEnvironmentTemplate),
                    getUrl(urlMappingTopology), outPutDirectory);
        } else {
            packagingManager.generateAddon(getUrl(urlCloudApplication), tenantId, outPutDirectory);
        }
    }

    private URL getUrl(String url) throws MalformedURLException {
        if (url.indexOf(':') == -1) {
            url = "file:" + url;
        }
        return new URL(url);
    }
}
