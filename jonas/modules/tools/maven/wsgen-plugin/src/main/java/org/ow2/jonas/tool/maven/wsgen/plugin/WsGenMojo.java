/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.tool.maven.wsgen.plugin;

import java.io.File;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.MavenProjectHelper;
import org.ow2.jonas.generators.wsgen.WsGen;

/**
 * Maven plugin to launch WsGen.
 * @goal wsgen
 * @phase package
 * @author Florent BENOIT
 */
public class WsGenMojo extends AbstractMojo {

    /**
     * Name of the file on which WSGen needs to be launched.
     * @parameter expression="${project.build.finalName}"
     */
    private String fileName;

    /**
     * The maven project.
     * @parameter expression="${project}"
     * @required
     * @readonly
     */
    private MavenProject project;

    /**
     * Helper used to attach artifacts.
     * @component
     * @required
     */
    private MavenProjectHelper mavenProjectHelper;

    /**
     * Directory of the fileName.
     * @parameter expression="${project.build.directory}"
     */
    private File fileDirectory;

    /**
     * Work directory.
     * @parameter expression="${project.build.directory}"
     */
    private File workDirectory;

    /**
     * Execute the Maven plugin.
     * @throws MojoExecutionException if the file is not generated.
     */
    public void execute() throws MojoExecutionException {

        // Get artifact
        Artifact artifact = project.getArtifact();

        // Get extension
        String extension = artifact.getArtifactHandler().getExtension();

        // Filename
        String inputFilePath = fileDirectory + File.separator + fileName + "." + extension;

        // Create args
        String[] args = new String[] {"-d", workDirectory.getPath(), inputFilePath};

        // Create WsGen
        WsGen wsGen = new WsGen();

        // Call it
        String resultFilePath = null;
        try {
            resultFilePath = wsGen.execute(args);
        } catch (Exception e) {
            throw new MojoExecutionException("Cannot execute WSGen", e);
        }

        // Filename differs ? needs to attach the file.
        if (!inputFilePath.equals(resultFilePath)) {
            // Get new File extension produced or null if there is no extension
            String newExtension = null;
            int indexDot = resultFilePath.lastIndexOf('.');
            if (indexDot > 0) {
                newExtension = resultFilePath.substring(indexDot + 1);
            }
            if (extension.equals(newExtension)) {
                // Replace the input file
                new File(resultFilePath).renameTo(new File(inputFilePath));
            } else {
                // attach the new artifact to the build
                mavenProjectHelper.attachArtifact(project, newExtension, null, new File(resultFilePath));
            }

        } else {
            // Nothing to do
            getLog().info("No WsGen generation");
        }
    }

}
