/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.tool.maven.genic.plugin;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.DependencyResolutionRequiredException;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.MavenProject;
import org.ow2.jonas.generators.genic.GenIC;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Maven plugin to launch GenIC.
 * @goal genic
 * @phase package
 */
public class GenICMojo extends AbstractMojo {

    /**
     * Name of the file on which GenIC needs to be launched.
     * @parameter expression="${project.build.finalName}"
     */
    private String fileName;

    /**
     * The maven project.
     * @parameter expression="${project}"
     * @required
     * @readonly
     */
    private MavenProject project;

    /**
     * Directory of the fileName.
     * @parameter expression="${project.build.directory}"
     */
    private File fileDirectory;

    /**
     * validation of XML files ?
     */
    private boolean validation = true;

    /**
     * list of javac options.
     */
    private String javacOpts = null;

    /**
     * will GenIC keep already generated files ?
     */
    private boolean keepGen = false;

    /**
     * specifies which RMIC compiler to user: the built-in one or the external
     * one.
     */
    private boolean noFastRMIC = false;

    /**
     * protocol list.
     */
    private String protocols = "jrmp,iiop";

    /**
     * nocompil.
     */
    private boolean nocompil = false;

    /**
     * Invoke Javac with tools.jar.
     */
    private boolean invokeCmd = false;

    /**
     * Options for rmic compiler.
     */
    private String rmicOpts = null;

    /**
     * extra arguments to be passed to GenIC.
     */
    private String additionalArgs = null;

    /**
     * verbose mode.
     */
    private boolean verbose = false;

    /**
     * List of arguments that will be used to call GenIC.
     */
    private List<String> argumentList = null;

    /**
     * Maven runtime  classpath
     */
    private List<String> runtimeclasspath = null;


    /**
     * Default constructor.
     */
    public GenICMojo() {
        this.argumentList = new ArrayList<String>();
    }

    /**
     * Execute the Maven plugin.
     * @throws MojoExecutionException if the file is not generated.
     */
    @SuppressWarnings("unchecked")
    public void execute() throws MojoExecutionException {

        // keepgenerated
        if (keepGen) {
            argumentList.add("-keepgenerated");
        }

        if (noFastRMIC) {
            argumentList.add("-nofastrmic");
        }

        // novalidation
        if (!validation) {
            argumentList.add("-novalidation");
        }

        // nocompil
        if (nocompil) {
            argumentList.add("-nocompil");
        }

        // invokecmd
        if (invokeCmd) {
            argumentList.add("-invokecmd");
        }

        // javacopts
        if (javacOpts != null && !javacOpts.equals("")) {
            argumentList.add("-javacopts");
            argumentList.add(javacOpts);
        }

        // rmicopts
        if (rmicOpts != null && !rmicOpts.equals("")) {
            argumentList.add("-rmicopts");
            argumentList.add(rmicOpts);
        }

        // verbose
        if (verbose) {
            argumentList.add("-verbose");
        }

        // additionalargs
        if (additionalArgs != null) {
            argumentList.add(additionalArgs);
        }

        // protocols
        if (protocols != null) {
            argumentList.add("-protocols");
            argumentList.add(protocols);
        }

        try {
            runtimeclasspath = project.getRuntimeClasspathElements();
            String cp = new String();
            for (int i = 0; i < runtimeclasspath.size(); i++) {
                cp = cp + runtimeclasspath.get(i) + File.pathSeparator;

            }
            argumentList.add("-classpath");
            argumentList.add(cp);

        } catch (DependencyResolutionRequiredException e) {
            e.printStackTrace();
        }

        // Get artifact
        Artifact artifact = project.getArtifact();

        // Get extension
        String extension = artifact.getArtifactHandler().getExtension();

        // Filename
        String inputFilePath = fileDirectory + File.separator + fileName + "." + extension;

        // Add file
        argumentList.add(inputFilePath);

        // Call GenIC
        GenIC.main(argumentList.toArray(new String[argumentList.size()]));

    }
}
