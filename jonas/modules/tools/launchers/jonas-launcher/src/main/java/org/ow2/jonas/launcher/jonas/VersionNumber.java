/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2010 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.launcher.jonas;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Simple version number holder.
 * @author Guillaume Sauthier
 */
public class VersionNumber {

    /**
     * Components of the version number.
     */
    private List<String> values = new ArrayList<String>();

    /**
     * Construct a version number
     * @param version value to be parsed
     */
    public VersionNumber(final String version) {

        // Cut the string on '.'
        String[] elements = version.split("\\.");
        values.addAll(Arrays.asList(elements));

        // Fix value if the element list is not long enough
        // (we requires at least 2 elements: <major> and <minor>)
        if (values.size() == 1) {
            // Append a trailing '0'
            values.add("0");
        }
    }

    /**
     * @return the 2 first indices of the version number (representing the major and minor numbers).
     */
    public String getMajorMinor() {
        return values.get(0) + "." + values.get(1);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (String element : values) {
            if (sb.length() != 0) {
                sb.append('.');
            }
            sb.append(element);
        }
        return sb.toString();
    }
}
