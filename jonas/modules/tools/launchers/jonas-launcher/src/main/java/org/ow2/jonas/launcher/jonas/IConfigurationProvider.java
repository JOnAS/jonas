/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.launcher.jonas;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

/**
 * Provides a gateway configuration.
 * @author Guillaume Sauthier
 */
public interface IConfigurationProvider {

    /**
     * @return an OSGi configuration.
     * @throws IOException If the configuration couldn't be provided.
     */
    Map<String, String> getConfiguration() throws IOException;

    /**
     * @return list of the bundles to install and add it to the start level
     */
    SortedMap<Integer, List<String>> getAutoInstallBundles();

    /**
     * @return map of the bundles to "automatically start for a given level"
     */
    SortedMap<Integer, List<String>> getAutoStartBundles();

    ClassLoader getFrameworkClassLoader(final ClassLoader parentClassLoader);

}
