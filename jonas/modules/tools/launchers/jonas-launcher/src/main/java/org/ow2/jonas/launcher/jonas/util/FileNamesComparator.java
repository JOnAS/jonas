/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2013 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 *  $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.launcher.jonas.util;

import java.io.File;
import java.io.Serializable;
import java.util.Comparator;

/**
 * A {@link FileNamesComparator} is a {@link java.util.Comparator} based on {@link java.io.File}s names. Comparison relays on {@link String#compareTo(String)}
 * method.
 * Optionally (not enabled by default) this {@link java.util.Comparator} could be configured to be case insensitive and to sort directories first
 *
 * @author Loic Albertin
 * @see String#compareTo(String)
 */
public class FileNamesComparator implements Comparator<File>, Serializable {
    private static final long serialVersionUID = 5127974197302050785L;

    private boolean directoriesFirst;

    private boolean ignoreCase;

    /**
     * Default constructor.
     * This comparator will be case sensitive and will not sort directories first.
     */
    public FileNamesComparator() {
        this(false, false);
    }

    /**
     * Constructor allowing to specify if it should sort directories first and be case insensitive
     *
     * @param directoriesFirst if true this {@link java.util.Comparator} sorts directories first
     * @param ignoreCase       if true this {@link java.util.Comparator} is case insensitive
     */
    public FileNamesComparator(boolean directoriesFirst, boolean ignoreCase) {
        this.directoriesFirst = directoriesFirst;
        this.ignoreCase = ignoreCase;
    }

    @Override
    public int compare(File file1, File file2) {
        if (file1 == file2) {
            return 0;
        } else if (file1 == null) {
            return -1;
        } else if (file2 == null) {
            return 1;
        }
        if (directoriesFirst) {
            if (file1.isDirectory() && !file2.isDirectory()) {
                return -1;
            } else if (!file1.isDirectory() && file2.isDirectory()) {
                return 1;
            }
        }
        if (ignoreCase) {
            return file1.getName().toLowerCase().compareTo(file2.getName().toLowerCase());
        } else {
            return file1.getName().compareTo(file2.getName());
        }
    }

    public boolean isDirectoriesFirst() {
        return directoriesFirst;
    }

    public void setDirectoriesFirst(boolean directoriesFirst) {
        this.directoriesFirst = directoriesFirst;
    }

    public boolean isIgnoreCase() {
        return ignoreCase;
    }

    public void setIgnoreCase(boolean ignoreCase) {
        this.ignoreCase = ignoreCase;
    }
}
