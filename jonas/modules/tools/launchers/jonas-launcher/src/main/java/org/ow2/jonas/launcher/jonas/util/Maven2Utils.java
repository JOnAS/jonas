/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.launcher.jonas.util;

import java.io.File;
import java.net.MalformedURLException;


/**
 * Maven2 Utilities.
 * @author Guillaume Sauthier
 */
public final class Maven2Utils {

    /**
     * User specified user repository property name.
     */
    private static final String PROP_M2_REPOSITORY = "m2.repository";

    /**
     * Path to default M2 repository.
     */
    private static final String PATH_DEFAULT_REPOSITORY = ".m2/repository";

    /**
     * Private empty constructor.
     */
    private Maven2Utils() {
    }

    /**
     * @param repository Artifact repository
     * @param groupId Artifact groupId
     * @param artifactId Artifact ID
     * @param version Artifact version
     * @param classifier Artifact's classifier (may be null)
     * @return a string-ified URL
     * @throws MalformedURLException If jar file is not in the repository.
     */
    public static String getBundleMaven2Location(final String repository, final String groupId, final String artifactId, final String version,
            final String classifier) throws MalformedURLException {

        // Creates a File from artifact info
        String appendedClassifier = "";
        if (classifier != null) {
            // Handle classifier if any
            appendedClassifier = appendedClassifier.concat("-").concat(classifier);
        }
        String path = constructDirectoryPath(groupId, artifactId, version);
        File bundle = new File(repository, path.concat(File.separator).concat(artifactId).concat("-").concat(version).concat(
                appendedClassifier).concat(".jar"));

        // Transform to URI first, because File.toURL() is buggy
        return bundle.toURI().toURL().toExternalForm();
    }

    /**
     * @return a {@link File} to a usable Maven2 repository.
     */
    public static File getMaven2Repository() {
        File myRepository = null;
        String m2Repository = System.getProperty(PROP_M2_REPOSITORY);

        // Priority to a user defined repository
        if (m2Repository == null) {
            // nothing, so try a reasonable default
            // Assume that local Maven2 repository is in ~/.m2/repository
            String userHome = System.getProperty("user.home");
            myRepository = IOUtils.getSystemFile(new File(userHome), PATH_DEFAULT_REPOSITORY);
        } else {
            // Just use the provided value
            myRepository = new File(m2Repository);
        }

        // Return the detected repository
        return myRepository;
    }

    /**
     * @param groupId Artifact groupId
     * @param artifactId Artifact ID
     * @param version Artifact version
     * @return transform the artifact coordinates into a m2 repository path
     */
    private static String constructDirectoryPath(final String groupId, final String artifactId, final String version) {
        // Transform groupId '.' into '/'
        String transformed = groupId.replace('.', File.separatorChar);
        return transformed.concat(File.separator).concat(artifactId).concat(File.separator).concat(version);
    }

    /**
     * @return a File pointing to $JONAS_ROOT/repositories/maven2-internal
     */
    public static File getMaven2InternalRepository() {
        File root = JOnASUtils.getJOnASRoot();
        return IOUtils.getSystemFile(root, "repositories/maven2-internal");
    }
}
