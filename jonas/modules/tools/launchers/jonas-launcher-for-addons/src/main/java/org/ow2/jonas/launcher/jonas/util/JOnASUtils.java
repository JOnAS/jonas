/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007-2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.launcher.jonas.util;

import java.io.File;
import java.util.Properties;

import org.ow2.jonas.lib.util.ConfigurationConstants;

/**
 * JOnAS utilities.
 * @author Guillaume Sauthier
 */
public final class JOnASUtils {

    /**
     * JOnAS Base directory.
     */
    private static File base;

    /**
     * JOnAS Root directory.
     */
    private static File root;

    /**
     * Are we in developer mode ?
     * TODO Will be initialized by System property.
     */
    private static Boolean developerMode = null;


    /**
     * Empty private constructor.
     */
    private JOnASUtils() {
    }

    /**
     * @return the JOnAS Base to be used
     */
    public static File getJOnASBase() {
        if (base == null) {
            // Init JOnAS Base value (using jonas.root if unset)
            String jb = System.getProperty(ConfigurationConstants.JONAS_BASE_PROP,
                    System.getProperty(ConfigurationConstants.JONAS_ROOT_PROP));
            base = new File(jb);
        }
        return base;
    }


    /**
     * Returns the value associated to the given property in jonas.properties file.
     * @param property the property name
     * @param defaultValue the default value
     * @return The value associated to the given property.
     * @throws Exception If the property value cannot be retrieved
     */
    public static String getServerProperty(final String property, final String defaultValue) throws Exception {
        File jonasPropertiesFile = IOUtils.getSystemFile(JOnASUtils.getJOnASBase(),
                                                         "conf/" + ConfigurationConstants.JONAS_PROPERTIES_PROP);
        Properties props = IOUtils.getPropertiesFromFile(jonasPropertiesFile);
        return props.getProperty(property, defaultValue);
    }

    /**
     * @return the JOnAS Root to be used. May throw an {@link IllegalStateException}
     * if <code>jonas.root</code> System property is not set.
     */
    public static File getJOnASRoot() {
        if (root == null) {
            // Init JOnAS Root value
            String jr = System.getProperty(ConfigurationConstants.JONAS_ROOT_PROP);
            if (jr == null) {
                throw new IllegalStateException("Property 'jonas.root' is not set but is required");
            }
            root = new File(jr);
        }
        return root;
    }

    /**
     * @return the work directory
     */
    public static File getWorkDirectory() {
        String workDirectoryStr = null;
        try {
            workDirectoryStr = getServerProperty(ConfigurationConstants.WORK_DIRECTORY_PROP,
                    ConfigurationConstants.DEFAULT_WORK_DIRECTORY);
        } catch (Exception e) {
            workDirectoryStr = ConfigurationConstants.DEFAULT_WORK_DIRECTORY;
        }
        File workDirectory = new File(workDirectoryStr);
        if (workDirectory.isAbsolute()) {
            return workDirectory;
        }
        return new File(getJOnASBase(), workDirectoryStr);
    }

    /**
     * @return the JOnAS version from the Implementation-Version Manifest entry.
     */
    public static String getVersion() {
        return JOnASUtils.class.getPackage().getImplementationVersion();
    }

    /**
     * @return true if the <code>jonas.developer</code> system property has been set.
     */
    public static boolean isDeveloperMode() {
        if (developerMode == null) {
            boolean mode = Boolean.getBoolean(ConfigurationConstants.JONAS_DEVELOPER_PROP);
            developerMode = Boolean.valueOf(mode);
        }
        return developerMode.booleanValue();
    }

}
