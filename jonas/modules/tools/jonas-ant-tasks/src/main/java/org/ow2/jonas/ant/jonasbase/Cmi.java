/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ant.jonasbase;

import org.ow2.jonas.ant.JOnASBaseTask;

/**
 * Defines properties for cmi service.
 * @author Loris Bouzonnet
 */
public class Cmi extends Tasks {

    /**
     * Info for the logger.
     */
    private static final String INFO = "[Cmi] ";

    /**
     * CMI mcast address attribute.
     */
    private static final String CMI_MCASTADDR_ATTR = "mcast_addr";

    /**
     * CMI mcast port attribute.
     */
    private static final String CMI_MCASTPORT_ATTR = "mcast_port";

    /**
     * Default cmi mcast address.
     */
    private static final String DEFAULT_CMI_MCASTADDR = "224.0.0.35";

    /**
     * Default cmi mcast port.
     */
    private static final String DEFAULT_CMI_MCASTPORT = "35467";

    /**
     * CMI replication enabled attribute.
     */
    private static final String REPLICATION_ENABLED_ATTR = "cmi.server.start.replication";

    /**
     * Default constructor.
     */
    public Cmi() {
        super();
    }

    /**
     * Set mcastAddr for CMI.
     * @param mcastAddr multicast address
     */
    public void setMcastAddr(final String mcastAddr) {

        // Token to replace the multicast addr
        String token = CMI_MCASTADDR_ATTR + "=" + "\"" + DEFAULT_CMI_MCASTADDR
                + "\"";
        String value = CMI_MCASTADDR_ATTR + "=" + "\"" + mcastAddr + "\"";
        JReplace mcastAddrReplace = new JReplace();
        mcastAddrReplace
                .setLogInfo(INFO + "Setting mcastaddr for cmi");
        mcastAddrReplace
                .setConfigurationFile(JOnASBaseTask.JGROUPS_CMI_CONF_FILE);
        mcastAddrReplace.setToken(token);
        mcastAddrReplace.setValue(value);
        addTask(mcastAddrReplace);
    }

    /**
     * Set mcastPort for CMI.
     * @param mcastPort multicast port
     */
    public void setMcastPort(final String mcastPort) {

        // Token to replace the multicast port
        String token = CMI_MCASTPORT_ATTR + "=" + "\"" + DEFAULT_CMI_MCASTPORT
                + "\"";
        String value = CMI_MCASTPORT_ATTR + "=" + "\"" + mcastPort + "\"";
        JReplace mcastPortReplace = new JReplace();
        mcastPortReplace
                .setLogInfo(INFO + "Setting mcastport for cmi");
        mcastPortReplace
                .setConfigurationFile(JOnASBaseTask.JGROUPS_CMI_CONF_FILE);
        mcastPortReplace.setToken(token);
        mcastPortReplace.setValue(value);
        addTask(mcastPortReplace);

    }

    /**
     * Set if the replication is enabled.
     * @param replicationEnabled true if the replication is enabled
     */
    public void setReplicationEnabled(final boolean replicationEnabled) {
        // Token to replace the MBean name
        String token = REPLICATION_ENABLED_ATTR + "=false";
        String value = REPLICATION_ENABLED_ATTR + "=true";
        JReplace mcastPortReplace = new JReplace();
        mcastPortReplace
                .setLogInfo(INFO + "Setting if replication is enabled for cmi");
        mcastPortReplace
                .setConfigurationFile(JOnASBaseTask.CAROL_CONF_FILE);
        mcastPortReplace.setToken(token);
        mcastPortReplace.setValue(value);
        addTask(mcastPortReplace);
    }

}
