/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006-2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ant.jonasbase.web;

import org.ow2.jonas.ant.JOnASBaseTask;
import org.ow2.jonas.ant.jonasbase.JReplace;
import org.ow2.jonas.ant.jonasbase.Tasks;


/**
 * Support Jetty Connector Configuration.
 *
 * @author Guillaume Sauthier
 */
public class Jetty extends Tasks {

    /**
     * Info for the logger.
     */
    private static final String INFO = "[Jetty] ";

    /**
     * HTTPS TOKEN.
     */
    private static final String HTTPS_TOKEN = "<!-- Add a HTTPS SSL listener on port 9043";

    /**
     * AJP TOKEN.
     */
    private static final String AJP_TOKEN = "<!-- Add a AJP13 listener on port 8009";

    /**
     * Default Constructor.
     */
    public Jetty() {
        super();
    }

    /**
     * Configure a HTTP Connector.
     * @param http HTTP Configuration.
     */
    public void addConfiguredHttp(final Http http) {
        JReplace propertyReplace = new JReplace();
        propertyReplace.setConfigurationFile(JOnASBaseTask.JETTY_CONF_FILE);
        propertyReplace.setToken(Http.DEFAULT_PORT);
        propertyReplace.setValue(http.getPort());
        propertyReplace.setLogInfo(INFO + "Setting HTTP port number to : " + http.getPort());
        addTask(propertyReplace);
    }

    /**
     * Configure a HTTPS Connector.
     * @param https HTTPS Configuration.
     */
    public void addConfiguredHttps(final Https https) {
        JReplace propertyReplace = new JReplace();
        propertyReplace.setConfigurationFile(JOnASBaseTask.JETTY_CONF_FILE);
        propertyReplace.setToken(HTTPS_TOKEN);
        StringBuffer value = new StringBuffer();
        value.append("<!-- Add a HTTPS SSL listener on port " + https.getPort() + "                           -->\n");
        value.append("  <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->\n");
        value.append("  <Call name=\"addListener\">\n");
        value.append("    <Arg>\n");
        if (Https.IBM_VM.equalsIgnoreCase(https.getVm())) {
            value.append("      <New class=\"org.mortbay.http.IbmJsseListener\">\n");
        } else {
                // default to Sun
            value.append("      <New class=\"org.mortbay.http.SunJsseListener\">\n");
        }
        value.append("        <Set name=\"Port\">" + https.getPort() + "</Set>\n");
        value.append("        <Set name=\"MinThreads\">5</Set>\n");
        value.append("        <Set name=\"MaxThreads\">100</Set>\n");
        value.append("        <Set name=\"MaxIdleTimeMs\">30000</Set>\n");
        value.append("        <Set name=\"LowResourcePersistTimeMs\">2000</Set>\n");
        if (https.getKeystoreFile() != null) {
            value.append("        <Set name=\"Keystore\">" + https.getKeystoreFile() + "</Set>\n");
        }
        if (https.getKeystorePass() != null) {
            value.append("        <Set name=\"Password\">" + https.getKeystorePass() + "</Set>\n");
        }
        if (https.getKeyPassword() != null) {
            value.append("  <Set name=\"KeyPassword\">" + https.getKeyPassword() + "</Set>\n");
        }
        value.append("      </New>\n");
        value.append("    </Arg>\n");
        value.append("  </Call>\n\n");
        value.append("  <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->\n");
        value.append("  " + HTTPS_TOKEN);
        propertyReplace.setValue(value.toString());
        propertyReplace.setLogInfo(INFO + "Setting HTTPS port number to : " + https.getPort());
        addTask(propertyReplace);

        propertyReplace = new JReplace();
        propertyReplace.setConfigurationFile(JOnASBaseTask.JETTY_CONF_FILE);
        propertyReplace.setToken(Https.DEFAULT_PORT);
        propertyReplace.setValue(https.getPort());
        propertyReplace.setLogInfo(INFO + "Fix HTTP redirect port number to : " + https.getPort());
        addTask(propertyReplace);


    }

    /**
     * Configure an AJP Connector.
     * @param ajp AJP Configuration.
     */
    public void addConfiguredAjp(final Ajp ajp) {
        JReplace propertyReplace = new JReplace();
        propertyReplace.setConfigurationFile(JOnASBaseTask.JETTY_CONF_FILE);
        propertyReplace.setToken(AJP_TOKEN);
        StringBuffer value = new StringBuffer();
        value.append("<!-- Add a AJP13 listener on port " + ajp.getPort() + "                               -->\n");
        value.append("  <!-- This protocol can be used with mod_jk in apache, IIS etc.       -->\n");
        value.append("  <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->\n");
        value.append("  <Call name=\"addListener\">\n");
        value.append("    <Arg>\n");
        value.append("      <New class=\"org.mortbay.http.ajp.AJP13Listener\">\n");
        value.append("        <Set name=\"Port\">" + ajp.getPort() + "</Set>\n");
        value.append("        <Set name=\"MinThreads\">5</Set>\n");
        value.append("        <Set name=\"MaxThreads\">20</Set>\n");
        value.append("        <Set name=\"MaxIdleTimeMs\">0</Set>\n");
        value.append("        <Set name=\"confidentialPort\">9043</Set>\n");
        value.append("      </New>\n");
        value.append("    </Arg>\n");
        value.append("  </Call>\n\n");
        value.append("  <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->\n");
        value.append("  " + AJP_TOKEN);
        propertyReplace.setValue(value.toString());
        propertyReplace.setLogInfo(INFO + "Setting AJP Connector to : " + ajp.getPort());
        addTask(propertyReplace);
    }

}
