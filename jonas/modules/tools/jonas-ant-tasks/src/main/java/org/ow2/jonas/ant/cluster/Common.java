/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Benoit Pelletier
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ant.cluster;

/**
 * Common task
 * @author Benoit Pelletier
 */
public class Common extends ClusterTasks {

    /**
     * Info for the logger
     */
    private static final String INFO = "[Common] ";

    /**
     * Default constructor
     */
    public Common() {
        super();
    }

    /**
     * Add tasks for Carol configuration
     * @param carolCluster added task
     */
    public void addConfiguredCarolCluster(final CarolCluster carolCluster) {
        carolCluster.setRootTask(getRootTask());
        log(INFO + "CarolCluster added");
        carolCluster.setLogInfo("CarolCluster");
        addClusterTask(carolCluster);
    }

    /**
     * Add tasks for Db configuration
     * @param dbCluster added task
     */
    public void addConfiguredDbCluster(final DbCluster dbCluster) {
        dbCluster.setRootTask(getRootTask());
        log(INFO + "DbCluster added");
        dbCluster.setLogInfo("DbCluster");
        addClusterTask(dbCluster);
    }

    /**
     * Add tasks for Mail configuration
     * @param mailCluster added task
     */
    public void addConfiguredMailCluster(final MailCluster mailCluster) {
        mailCluster.setRootTask(getRootTask());
        log(INFO + "MailCluster added");
        mailCluster.setLogInfo("MailCluster");
        addClusterTask(mailCluster);
    }

    /**
     * Add tasks for Jms configuration
     * @param jmsCluster added task
     */
    public void addConfiguredJmsCluster(final JmsCluster jmsCluster) {
        jmsCluster.setRootTask(getRootTask());
        log(INFO + "JmsCluster added");
        jmsCluster.setLogInfo("JmsCluster");
        addClusterTask(jmsCluster);
    }

    /**
     * Add tasks for WsdlPublish configuration
     * @param wsdlPublishCluster added task
     */
    public void addConfiguredWsdlPublishCluster(final WsdlPublishCluster wsdlPublishCluster) {
        wsdlPublishCluster.setRootTask(getRootTask());
        log(INFO + "WsdlPublishCluster added");
        wsdlPublishCluster.setLogInfo("WsdlPublishCluster");
        addClusterTask(wsdlPublishCluster);
    }

    /**
     * Add tasks for Discovery configuration
     * @param discoveryCluster added task
     */
    public void addConfiguredDiscoveryCluster(final DiscoveryCluster discoveryCluster) {
        discoveryCluster.setRootTask(getRootTask());
        log(INFO + "DiscoveryCluster added");
        discoveryCluster.setLogInfo("DiscoveryCluster");
        addClusterTask(discoveryCluster);
    }

    /**
     * Add deployable task.
     * @param DeployableCluster added task
     */
    public void addConfiguredDeployableCluster(final DeployableCluster deployableCluster) {
        deployableCluster.setRootTask(getRootTask());
        log(INFO + "DeployableCluster added");
        deployableCluster.setLogInfo("DeployableCluster");
        addClusterTask(deployableCluster);
    }

    /**
     *  Generates tasks for common.
     */
    @Override
    public void generatesTasks() {

        for (ClusterTasks ct : getClusterTasks()) {
            log(INFO + "tasks generation for " + ct.getLogInfo());
            ct.setArch(getArch());
            ct.setDestDirPrefix(getDestDirPrefix());
            ct.setDestDirSuffixIndFirst(getDestDirSuffixIndFirst());
            ct.setDestDirSuffixIndLast(getDestDirSuffixIndLast());
            ct.setWebInstNb(getWebInstNb());
            ct.setEjbInstNb(getEjbInstNb());

            ct.generatesTasks();
            addTasks(ct);
        }
    }

}
