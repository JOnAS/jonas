/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Benoit Pelletier
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ant.cluster;

import java.io.File;

import org.ow2.jonas.ant.jonasbase.JdbcXml;

/**
 *  Define JdbcXmlCluster task
 * @author Benoit Pelletier
 */
public class JdbcXmlCluster extends ClusterTasks {

    /**
     * Info for the logger
     */
    private static final String INFO = "[JdbcXmlCluster] ";

    /**
     * New file or add into existing one
     */
    private boolean newFile = false;

    /**
     * Name of this JDBC Resource Adaptor.
     */
    private String name = null;

    /**
     * Mapper Name of this JDBC Resource Adaptor.
     */
    private String mapper = null;

    /**
     * username of this JDBC Resource Adaptor.
     */
    private String userName = null;

    /**
     * Password of this JDBC Resource Adaptor.
     */
    private String password = null;

    /**
     * URL of this JDBC Resource Adaptor.
     */
    private String url = null;

    /**
     * Driver Name of this JDBC Resource Adaptor (may be set to the P6Spy driver name).
     */
    private String className = null;

    /**
     * Init Pool Size.
     */
    private String initConPool = "1";

    /**
     * Min Pool Size.
     */
    private String minConPool = "10";

    /**
     * Max Pool Size.
     */
    private String maxConPool = "100";

    /**
     * Max Prepared Statements Size
     */
    private String pstmtMax = "10";

    /**
     * Pstmt cache policy
     */
    private String pstmtCachePolicy = "List";

    /**
     * Max Wait Time jdbc
     */
    private String maxWaitTime = "10";

    /**
     * Max Waiters jdbc
     */
    private String maxWaiters = "100";

    /**
     * connexion max age jdbc
     */
    private String connMaxAge = "1440";

    /**
     * Max open time jdbc
     */
    private String maxOpenTime = "60";

    /**
     * Connection check level
     */
    private String conCheckLevel = "0";

    /**
     * Connection check level
     */
    private String conTestStmt = "";

    /**
     * Sampling period
     */
    private String samplingPeriod = "30";
    /**
     * Default constructor
     */
    public JdbcXmlCluster() {
        super();
    }

    /**
     * Set the name of the driver of this JDBC Resource Adaptor.
     * @param className the name of the driver of this JDBC Resource Adaptor
     */
    public void setClassName(final String className) {
        this.className = className;
    }

    /**
     * Set the init pool size of this JDBC Resource Adaptor Connection Pool.
     * @param initConPool init pool size of connection
     */
    public void setInitConPool(final String initConPool) {
        this.initConPool = initConPool;
    }

    /**
     * Set the min pool size of this JDBC Resource Adaptor Connection Pool.
     * @param minConPool min pool size of connection
     */
    public void setMinConPool(final String minConPool) {
        this.minConPool = minConPool;
    }

    /**
     * Set the max pool size of this JDBC Resource Adaptor Connection Pool.
     * @param maxConPool max pool size of connection
     */
    public void setMaxConPool(final String maxConPool) {
        this.maxConPool = maxConPool;
    }

    /**
     * Set the prepared statement pool size of this JDBC Resource Adaptor Connection Pool.
     * @param pstmtMax pool size of connection
     */
   public void setPstmtMax(final String pstmtMax) {
        this.pstmtMax = pstmtMax;
    }

   /**
    * Set the prepared statement cache policy of this JDBC Resource Adaptor Connection Pool.
    * @param pstmtCachePolicy cache policy
    */
   public void setPstmtCachePolicy(final String pstmtCachePolicy) {
       this.pstmtCachePolicy = pstmtCachePolicy;
   }


   /**
    * Set the connection check level.
    * @param conCheckLevel level
    */
   public void setConCheckLevel(final String conCheckLevel) {
        this.conCheckLevel = conCheckLevel;
    }

   /**
    * Set the statement for the connection check level.
    * @param conTestStmt request
    */
    public void setConTestStmt(final String conTestStmt) {
        this.conTestStmt = conTestStmt;
    }

    /**
     * Set the name of this JDBC Resource Adaptor.
     * @param name the name of this JDBC Resource Adaptor
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Set the mapper name of this JDBC Resource Adaptor.
     * @param mapper the mappername of this JDBC Resource Adaptor
     */
    public void setMapper(final String mapperName) {
        this.mapper = mapperName;
    }

    /**
     * Set the user of this JDBC Resource Adaptor.
     * @param user the user of this JDBC Resource Adaptor
     */
    public void setUserName(final String user) {
        this.userName = user;
    }

    /**
     * Set the password of this JDBC Resource Adaptor.
     * @param password the name of this JDBC Resource Adaptor
     */
    public void setPassword(final String password) {
        this.password = password;
    }

    /**
     * Set the samplingPeriod of the JDBC Resource Adaptor.
     * @param samplingPeriod the sampling period
     */
    public void setSamplingPeriod(final String samplingPeriod) {
        this.samplingPeriod = samplingPeriod;
    }

    /**
     * Set the url of this JDBC Resource Adaptor.
     * @param url the name of this JDBC Resource Adaptor
     */
    public void setUrl(final String url) {
        this.url = url;
    }

    /**
     * Set the max wait time of this JDBC Resource Adaptor Connection Pool.
     * @param maxWaitTime of the connection
     */
    public void setMaxWaitTime(final String maxWaitTime) {
        this.maxWaitTime = maxWaitTime;
    }

    /**
     * Set the max waiters of this JDBC Resource Adaptor Connection Pool.
     * @param maxWaiters of connection
     */
    public void setMaxWaiters(final String maxWaiters) {
        this.maxWaiters = maxWaiters;
    }

    /**
     * Set the connexion max age of this JDBC Resource Adaptor Connection Pool.
     * @param maxConnMAxAge of connection
     */
    public void setConnMaxAge(final String connMaxAge) {
        this.connMaxAge = connMaxAge;
    }

    /**
     * Set the open time max of this JDBC Resource Adaptor Connection Pool.
     * @param maxWaiters of connection
     */
    public void setMaxOpenTime(final String maxOpenTime) {
        this.maxOpenTime = maxOpenTime;
    }

    /**
    *
    * @return true if a new file is requested
    */
    public boolean isNewFile() {
       return newFile;
    }

   /**
    * Set is a new file has to be created
    * @param newFile
    */
    public void setNewFile(final String newFile) {
       this.newFile = Boolean.valueOf(newFile);
    }

    /**
     * Generates the JdbcRa tasks for each JOnAS's instances
     */
    @Override
    public void generatesTasks() {

        for (int i = getDestDirSuffixIndFirst(); i <= getDestDirSuffixIndLast(); i++) {

            String destDir = getDestDir(getDestDirPrefix(), i);
            log(INFO + "tasks generation for " + destDir);
            // creation of the JdbcXml task
            JdbcXml jdbcXml = new JdbcXml();

            jdbcXml.setNewFile(String.valueOf(newFile));
            jdbcXml.setName(name);
            jdbcXml.setMapper(mapper);
            jdbcXml.setUserName(userName);
            jdbcXml.setPassword(password);
            jdbcXml.setUrl(url);
            jdbcXml.setClassName(className);


            jdbcXml.setInitConPool(initConPool);
            jdbcXml.setMinConPool(minConPool);
            jdbcXml.setMaxConPool(maxConPool);
            jdbcXml.setPstmtMax(pstmtMax);
            jdbcXml.setPstmtCachePolicy(pstmtCachePolicy);
            jdbcXml.setMaxWaitTime(maxWaitTime);
            jdbcXml.setMaxWaiters(maxWaiters);
            jdbcXml.setConnMaxAge(connMaxAge);
            jdbcXml.setMaxOpenTime(maxOpenTime);
            jdbcXml.setConCheckLevel(conCheckLevel);
            jdbcXml.setConTestStmt(conTestStmt);
            jdbcXml.setSamplingPeriod(samplingPeriod);

            jdbcXml.setDestDir(new File(destDir));

            addTask(jdbcXml);

        }
    }
}
