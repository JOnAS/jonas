/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004-2008 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ant.jonasbase.wsdl;

/**
 * Defines a UDDI configuration file for WSDL publication.
 * @author Florent Benoit
 */
public class Uddi {

    /**
     * Class for RegistryWsdl publisher.
     */
    public static final String PUBLISHER_CLASS = "org.ow2.jonas.ws.publish.internal.registry.RegistryWSDLPublisher";

    /**
     * Name of the file.
     */
    private String name = null;

    /**
     * Username.
     */
    private String userName = null;

    /**
     * Password.
     */
    private String password = null;

    /**
     * Organization name.
     */
    private String orgName = null;

    /**
     * Organization Description.
     */
    private String orgDesc = null;

    /**
     * Organization Person name.
     */
    private String orgPersonName = null;

    /**
     * url : lifecyclemanagerURL.
     */
    private String lifecyclemanagerURL = null;

    /**
     * url : queryManagerURL.
     */
    private String queryManagerURL = null;

    /**
     * Gets the name.
     * @return the name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     * @param name name to set.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return Returns the lifecyclemanagerURL.
     */
    public String getLifecyclemanagerURL() {
        return lifecyclemanagerURL;
    }

    /**
     * @param lifecyclemanagerURL The lifecyclemanagerURL to set.
     */
    public void setLifecyclemanagerURL(final String lifecyclemanagerURL) {
        this.lifecyclemanagerURL = lifecyclemanagerURL;
    }

    /**
     * @return Returns the orgDesc.
     */
    public String getOrgDesc() {
        return orgDesc;
    }

    /**
     * @param orgDesc The orgDesc to set.
     */
    public void setOrgDesc(final String orgDesc) {
        this.orgDesc = orgDesc;
    }

    /**
     * @return Returns the orgName.
     */
    public String getOrgName() {
        return orgName;
    }

    /**
     * @param orgName The orgName to set.
     */
    public void setOrgName(final String orgName) {
        this.orgName = orgName;
    }

    /**
     * @return Returns the orgPersonName.
     */
    public String getOrgPersonName() {
        return orgPersonName;
    }

    /**
     * @param orgPersonName The orgPersonName to set.
     */
    public void setOrgPersonName(final String orgPersonName) {
        this.orgPersonName = orgPersonName;
    }

    /**
     * @return Returns the password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password The password to set.
     */
    public void setPassword(final String password) {
        this.password = password;
    }

    /**
     * @return Returns the queryManagerURL.
     */
    public String getQueryManagerURL() {
        return queryManagerURL;
    }

    /**
     * @param queryManagerURL The queryManagerURL to set.
     */
    public void setQueryManagerURL(final String queryManagerURL) {
        this.queryManagerURL = queryManagerURL;
    }

    /**
     * @return Returns the userName.
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName The userName to set.
     */
    public void setUserName(final String userName) {
        this.userName = userName;
    }
}
