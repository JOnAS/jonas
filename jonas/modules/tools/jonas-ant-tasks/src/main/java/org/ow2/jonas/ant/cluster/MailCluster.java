/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Benoit Pelletier
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ant.cluster;

import java.io.File;

import org.ow2.jonas.ant.jonasbase.Mail;


/**
 *  Define MailCluster task
 * @author Benoit Pelletier
 */
public class MailCluster extends ClusterTasks {

    /**
     * Info for the logger
     */
    private static final String INFO = "[MailCluster] ";


    /**
     * Type of factory (Session or MimePartDataSource)
     */
    private String type = null;

    /**
     * Name of the factory
     */
    private String name = null;

    /**
     * Recipient (TO) of MimePartDataSource factory
     */
    private String mailTo = null;

    /**
     * Subject of MimePartDataSource factory
     */
    private String subject = null;

    /**
     * Sets the recipient (MimePartDataSource)
     * @param mailTo recipient.
     */
    public void setMailTo(String mailTo) {
        this.mailTo = mailTo;
    }

    /**
     * Sets the name
     * @param name name of the factory
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Sets the subject (MimePartDataSource)
     * @param subject of the mail
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * Sets the type of factory
     * @param type of factory
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Default constructor
     */
    public MailCluster() {
        super();
    }

    /**
     * Generates the mail tasks for each JOnAS's instances
     */
    public void generatesTasks() {

        for (int i = getDestDirSuffixIndFirst(); i <= getDestDirSuffixIndLast(); i++) {

            String destDir = getDestDir(getDestDirPrefix(), i);

            // creation of the Mail task
            Mail mail = new Mail();

            log(INFO + "tasks generation for " + destDir);

            mail.setMailTo(mailTo);
            mail.setName(name);
            mail.setSubject(subject);
            mail.setType(type);

            mail.setDestDir(new File(destDir));

            addTask(mail);

        }
    }
}