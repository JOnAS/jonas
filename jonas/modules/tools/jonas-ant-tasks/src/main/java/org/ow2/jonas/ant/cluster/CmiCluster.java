/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ant.cluster;

import java.io.File;
import java.util.Iterator;

import org.apache.tools.ant.Task;
import org.ow2.jonas.ant.jonasbase.BaseTaskItf;
import org.ow2.jonas.ant.jonasbase.Cmi;

/**
 * Define CmiCluster task.
 * @author Loris Bouzonnet
 */
public class CmiCluster extends ClusterTasks {

    /**
     * Info for the logger
     */
    private static final String INFO = "[CmiCluster] ";

    /**
     * multicast addr
     */
    private String mcastAddr = null;

    /**
     * multicast port
     */
    private String mcastPort = null;

    /**
     * Default constructor
     */
    public CmiCluster() {
        super();
    }

    /**
     * Set mcastAddr
     * @param mcastAddr multicast address to set
     */
    public void setMcastAddr(final String mcastAddr) {
        this.mcastAddr = mcastAddr;
    }

    /**
     * Set mcastPort
     * @param mcastPort multicast port to set
     */
    public void setMcastPort(final String mcastPort) {
        this.mcastPort = mcastPort;
    }

    @Override
    public void generatesTasks() {
        for (int i = getDestDirSuffixIndFirst(); i <= getDestDirSuffixIndLast(); i++) {

            String destDir = getDestDir(getDestDirPrefix(), i);

            // creation of the Cmi tasks
            Cmi cmi = new Cmi();
            log(INFO + "tasks generation for " + destDir);
            cmi.setMcastAddr(mcastAddr);
            cmi.setMcastPort(mcastPort);
            cmi.setReplicationEnabled(true);

            // set destDir for each carol task
            for (Iterator<Task> it = cmi.getTasks().iterator(); it.hasNext();) {
                BaseTaskItf task = (BaseTaskItf) it.next();
                task.setDestDir(new File(destDir));
            }

            addTasks(cmi);

        }
    }

}
