/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004-2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ant.jonasbase;

import org.ow2.jonas.ant.JOnASBaseTask;

import org.apache.tools.ant.Task;

import org.ow2.jonas.ant.jonasbase.web.Jetty;
import org.ow2.jonas.ant.jonasbase.web.Tomcat;

/**
 * Allow to configure the WebContainer service.
 *
 * @author Florent Benoit
 */
public class WebContainer extends Tasks {

    /**
     * Info for the logger.
     */
    private static final String INFO = "[WebContainer] ";

    /**
     * Name of the implementation class for Jetty.
     */
    private static final String JETTY_SERVICE = "org.ow2.jonas.web.jetty6.Jetty6Service";

    /**
     * Name of the implementation class for Tomcat.
     */
    private static final String TOMCAT_SERVICE = "org.ow2.jonas.web.tomcat6.Tomcat6Service";

    /**
     * Name of the implementation class for the default service (tomcat).
     */
    private static final String DEFAULT_SERVICE = TOMCAT_SERVICE;

    /**
     * Default port number.
     */
    private static final String DEFAULT_PORT = "9000";

    /**
     * ondemand.enabled property.
     */
    private static final String ONDEMANDENABLED = "jonas.service.web.ondemand.enabled";
    /**
     * ondemand.enabled property token default value.
     */
    private static final String ONDEMANDENABLEDTOKEN = "jonas.service.web.ondemand.enabled    true";
    
     /**
     * ondemand.redirectPort property.
     */
    private static final String ONDEMANDREDIRECTPORT = "jonas.service.web.ondemand.redirectPort";

    /**
     * ondemand.redirectPort property token default value
     */
    private static final String ONDEMANDREDIRECTPORTTOKEN = "jonas.service.web.ondemand.redirectPort    0";

    /**
     * is serviceName set ?
     */
    private boolean serviceNameSet = false;

    /**
     * Default constructor.
     */
    public WebContainer() {
        super();
    }

    /**
     * Set the port number for the WebContainer.
     * Optionnal, if missing, we look at the inner element &lt;jetty>/&lt;http> or &lt;tomcat>/&lt;http>.
     * It overrides both &lt;jetty>/&lt;http> or &lt;tomcat>/&lt;http> values if set.
     *
     * @param portNumber the port for the HTTP web Container
     */
    public void setPort(final String portNumber) {

        // For tomcat
        JReplace propertyReplace = new JReplace();
        propertyReplace.setConfigurationFile(JOnASBaseTask.TOMCAT_CONF_FILE);
        propertyReplace.setToken(DEFAULT_PORT);
        propertyReplace.setValue(portNumber);
        propertyReplace.setLogInfo(INFO + "Setting Tomcat port number to : " + portNumber);
        addTask(propertyReplace);

        // For Jetty
        propertyReplace = new JReplace();
        propertyReplace.setConfigurationFile(JOnASBaseTask.JETTY_CONF_FILE);
        propertyReplace.setToken(DEFAULT_PORT);
        propertyReplace.setValue(portNumber);
        propertyReplace.setLogInfo(INFO + "Setting Jetty port number to : " + portNumber);
        addTask(propertyReplace);
    }


    /**
     * Set the name of the web container : jetty or tomcat.
     * Optionnal, if missing, we look at the inner element &lt;jetty> or &lt;tomcat>.
     * This setting is priority 1 if both &lt;jetty> or &lt;tomcat> are set.
     *
     * @param containerName <code>jetty</code> or <code>tomcat</code>.
     */
    public void setName(final String containerName) {

        if ("jetty".equalsIgnoreCase(containerName)) {
            addTask(createServiceNameReplace(JETTY_SERVICE));
            serviceNameSet = true;
        } else if ("tomcat".equalsIgnoreCase(containerName)) {
            addTask(createServiceNameReplace(TOMCAT_SERVICE));
            serviceNameSet = true;
        }
    }

    /**
     * Set the value of the jonas.service.web.ondemand.enabled property in jonas.properties.
     *
     * @param ondemandenabled
     */
    public void setOndemandenabled(final String ondemandenabled) {
        // Ondemandenabled Token to replace
        String token = ONDEMANDENABLEDTOKEN;
        String value = ONDEMANDENABLED + "    "  + ondemandenabled;
        JReplace propertyReplace = new JReplace();
        propertyReplace.setConfigurationFile(JOnASBaseTask.JONAS_CONF_FILE);
        propertyReplace.setToken(token);
        propertyReplace.setValue(value);
        propertyReplace.setLogInfo(INFO + "Setting "+ ONDEMANDENABLED+" to "+ondemandenabled);
        addTask(propertyReplace);
    }

    /**
     * Set the value of the jonas.service.web.ondemand.redirectPort property in jonas.properties.
     *
     * @param portNumber
     */
    public void setOndemandredirectPort(final String portNumber) {
        // OndemanderedirectPort Token to replace
        String token = ONDEMANDREDIRECTPORTTOKEN;
        String value =  ONDEMANDREDIRECTPORT+ "    "  + portNumber;
        JReplace propertyReplace = new JReplace();
        propertyReplace.setConfigurationFile(JOnASBaseTask.JONAS_CONF_FILE);
        propertyReplace.setToken(token);
        propertyReplace.setValue(value);
        propertyReplace.setLogInfo(INFO + "Setting "+ ONDEMANDREDIRECTPORT +" to "+portNumber);
        addTask(propertyReplace);
    }


    /**
     * Create a JReplace Task for changing service classname in jonas.properties.
     *
     * @param serviceName service classname to use.
     * @return Returns a JReplace Task.
     */
    private Task createServiceNameReplace(final String serviceName) {
        // Replace the service in jonas.properties file
        JReplace propertyReplace = new JReplace();
        propertyReplace.setConfigurationFile(JOnASBaseTask.JONAS_CONF_FILE);
        propertyReplace.setToken(DEFAULT_SERVICE);
        propertyReplace.setValue(serviceName);
        propertyReplace.setLogInfo(INFO + "Setting service to : " + serviceName);
        return propertyReplace;
    }

    /**
     * Configure the Jetty WebContainer.
     *
     * @param jetty Jetty Configuration.
     */
    public void addConfiguredJetty(final Jetty jetty) {
        if (!serviceNameSet) {
            addTask(createServiceNameReplace(JETTY_SERVICE));
            serviceNameSet = true;
        }
        addTasks(jetty);
    }

    /**
     * Configure the Tomcat WebContainer.
     *
     * @param tomcat Tomcat Configuration.
     */
    public void addConfiguredTomcat(final Tomcat tomcat) {
        if (!serviceNameSet) {
            addTask(createServiceNameReplace(TOMCAT_SERVICE));
            serviceNameSet = true;
        }
        addTasks(tomcat);
    }
}
