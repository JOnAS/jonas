/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Benoit Pelletier
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ant.cluster;

import java.io.File;

import org.ow2.jonas.ant.jonasbase.DeployableTask;


/**
 *  Define MailCluster task
 * @author eyindanga.
 */
public class DeployableCluster extends ClusterTasks {

    /**
     * Info for the logger
     */
    private static final String INFO = "[DeployableCluster] ";


    /**
     * <code>this.extension</code> is extension of deployables.
     */
    private String extension = null;

    /**
     * <code>this.values</code> list of deployables, separated with comma.
     */
    private String values = null;

    /**
     * <code>this.sourceDir</code> is stringified url of a filesystem directory.
     */
    private String sourceDir = null;

    /**
     * <code>this.destdir</code> is a directory within <code>JONAS_BASE</code>
     */
    private String destDir = null;

    /**
     * Possible values of <code>this.target</code> are <code>copy, exclude</code>.
     */
    private String target = null;

    /**
     * Get the source directory.
     * @return the source directory.
     */
    public String getSourceDir() {
        return sourceDir;
    }


    /**
     * Set the source directory.
     * @param sourceDir the source directory.
     */
    public void setSourceDir(final String sourceDir) {
        this.sourceDir = sourceDir;
    }


    /**
     * Get the destination directory.
     * @return the destination directory.
     */
    public String getDestDir() {
        return destDir;
    }

    /**
     * Set the destination directory.
     * @param destDir the destination directory.
     */
    public void setDestDir(final String destDir) {
        this.destDir = destDir;
    }


    /**
     * Get extension of deployables.
     * @return extension of deployables.
     */
    public String getExtension() {
        return extension;
    }


    /**
     * Get deployables.
     * @return comma separated list of deployables.
     */
    public String getValues() {
        return values;
    }

    /**
     * Sets comma separated list of deployables.
     * @param values the comma separated list of deployables to set.
     */
    public void setValues(final String values) {
        this.values = values;
    }



    /**
     * Sets deployables extension
     * @param extension deployables extension
     */
    public void setExtension(final String extension) {
        this.extension = extension;
    }

    /**
     * Default constructor.
     */
    public DeployableCluster() {
        super();
    }
    /**
     * Get the target to be executed.
     * @return the target to be executed.
     */
    public String getTarget() {
        return target;
    }

    /**
     * set the target to be executed.
     * @param target to be executed
     */
    public void setTarget(final String target) {
        this.target = target;
    }

    /**
     * Generates <code>DeployableTask</code> tasks for each JOnAS's instances
     */
    @Override
    public void generatesTasks() {

        for (int i = getDestDirSuffixIndFirst(); i <= getDestDirSuffixIndLast(); i++) {

            String jonasBase = getDestDir(getDestDirPrefix(), i);

            log(INFO + "tasks generation for " + destDir);
            DeployableTask deptask = new DeployableTask();
            deptask.setDestDir(new File(destDir));
            /**
             * <code>this.destdir</code> is a directory within <code>JONAS_BASE</code>
             */
            deptask.setDstDir(this.destDir);
            /**
             * <code>this.sourceDir</code> is stringified url of a filesystem directory.
             */
            deptask.setSrcDir(this.sourceDir);
            /**
             * Possible values of <code>this.target</code> are <code>copy, exclude</code>.
             */
            deptask.setTarget(this.target);
            /**
             * <code>this.extension</code> is extension of deployables.
             */
            deptask.setExtension(this.extension);
            /**
             * <code>this.values</code> list of deployables, separated with comma.
             */
            deptask.setValues(this.values);

            deptask.setDestDir(new File(jonasBase));

            addTask(deptask);

        }
    }
}