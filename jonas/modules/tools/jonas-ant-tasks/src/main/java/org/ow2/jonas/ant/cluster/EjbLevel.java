/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Benoit Pelletier
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ant.cluster;


/**
 * Defines Ejblevel task
 * @author Benoit Pelletier
 */
public class EjbLevel extends ClusterTasks {

    /**
     * Info for the logger
     */
    private static final String INFO = "[EjbLevel] ";

    /**
     * Default constructor
     */
    public EjbLevel() {
        super();
    }

    /**
     * Add tasks for servicesCluster configuration
     * @param servicesCluster added task
     */
    public void addConfiguredServicesCluster(final ServicesCluster servicesCluster) {
        servicesCluster.setRootTask(getRootTask());
        log(INFO + "ServicesCluster added");
        servicesCluster.setLogInfo("ServicesCluster");
        addClusterTask(servicesCluster);
    }

    /**
     * Add tasks for jdbcRaCluster configuration
     * @param jdbcRaCluster added task
     */
    public void addConfiguredJdbcRaCluster(final JdbcRaCluster jdbcRaCluster) {
        jdbcRaCluster.setRootTask(getRootTask());
        log(INFO + "JdbcRaCluster added");
        jdbcRaCluster.setLogInfo("JdbcRaCluster");
        addClusterTask(jdbcRaCluster);
    }

    /**
     * Add tasks for jdbcXmlCluster configuration
     * @param jdbcXmlCluster added task
     */
    public void addConfiguredJdbcXmlCluster(final JdbcXmlCluster jdbcXmlCluster) {
        jdbcXmlCluster.setRootTask(getRootTask());
        log(INFO + "JdbcXmlCluster added");
        jdbcXmlCluster.setLogInfo("JdbcXmlCluster");
        addClusterTask(jdbcXmlCluster);
    }


    /**
     * Add tasks for DbmCluster configuration
     * @param dbmCluster added task
     */
    public void addConfiguredDbmCluster(final DbmCluster dbmCluster) {
        dbmCluster.setRootTask(getRootTask());
        log(INFO + "DbmCluster added");
        dbmCluster.setLogInfo("DbmCluster");
        addClusterTask(dbmCluster);
    }

    /**
     * Add tasks for CmiCluster configuration
     * @param cmiCluster added task
     */
    public void addConfiguredCmiCluster(final CmiCluster cmiCluster) {
        cmiCluster.setRootTask(getRootTask());
        log(INFO + "CmiCluster added");
        cmiCluster.setLogInfo("CmiCluster");
        addClusterTask(cmiCluster);
    }

    /**
     * Add tasks for HaCluster configuration
     * @param haCluster added task
     */
    public void addConfiguredHaCluster(final HaCluster haCluster) {
        haCluster.setRootTask(getRootTask());
        log(INFO + "HaCluster added");
        haCluster.setLogInfo("HaCluster");
        addClusterTask(haCluster);
    }

    /**
     * Add tasks for LibCluster configuration
     * @param libCluster added task
     */
    public void addConfiguredLibCluster(final LibCluster libCluster) {
        libCluster.setRootTask(getRootTask());
        log(INFO + "LibCluster added");
        libCluster.setLogInfo("LibCluster");
        addClusterTask(libCluster);
    }

    /**
     * Add tasks for DeploymentCluster configuration
     * @param deploymentCluster added task
     */
    public void addConfiguredDeploymentCluster(final DeploymentCluster deploymentCluster) {
        deploymentCluster.setRootTask(getRootTask());
        log(INFO + "DeploymentCluster added");
        deploymentCluster.setLogInfo("DeploymentCluster");
        addClusterTask(deploymentCluster);
    }

    /**
     *  Generates tasks for common
     */
    @Override
    public void generatesTasks() {
        for (ClusterTasks ct : getClusterTasks()) {
            log(INFO + "tasks generation for " + ct.getLogInfo());
            ct.setArch(getArch());
            ct.setDestDirPrefix(getDestDirPrefix());
            ct.setDestDirSuffixIndFirst(getDestDirSuffixIndFirst());
            ct.setDestDirSuffixIndLast(getDestDirSuffixIndLast());
            ct.generatesTasks();
            addTasks(ct);
        }
    }

}
