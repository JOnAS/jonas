/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Benoit Pelletier
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ant.cluster;

import java.io.File;

import org.ow2.jonas.ant.jonasbase.Services;

/**
 *  Define ServicesCluster task
 * @author Benoit Pelletier
 */
public class ServicesCluster extends ClusterTasks {

    /**
     * Info for the logger
     */
    private static final String INFO = "[ServicesCluster] ";
    /**
     * Partial list of the JOnAS's services to be used whether the chosen
     * architecture is separated web/ejb
     */
    private String partialList = null;

    /**
     * Partial list of the JOnAS's services to be used whether the chosen
     * architecture is both web/ejb
     */
    private String fullList = null;

    /**
     * Default constructor
     */
    public ServicesCluster() {
        super();
    }

    /**
     * Set the partial list of the JOnAS's services
     * @param partialList partial list of JOnAS services
     */
    public void setPartialList(String partialList) {
        this.partialList = partialList;

    }

    /**
     * Set the full list of the JOnAS's services
     * @param fullList full list of JOnAS services
     */
    public void setFullList(String fullList) {
        this.fullList = fullList;

    }

    /**
     * Generates the Services tasks for each JOnAS's instances
     */
    public void generatesTasks() {

        for (int i = getDestDirSuffixIndFirst(); i <= getDestDirSuffixIndLast(); i++) {

            String destDir = getDestDir(getDestDirPrefix(), i);

            log(INFO + "tasks generation for " + destDir);

            // creation of the Services task
            Services services = new Services();

            if (this.getArch().compareTo(ARCH_BOTH_WEB_EJB) == 0) {
                services.setNames(fullList);
            } else {
                services.setNames(partialList);
            }
            services.setDestDir(new File(destDir));

            addTask(services);
        }
    }
}