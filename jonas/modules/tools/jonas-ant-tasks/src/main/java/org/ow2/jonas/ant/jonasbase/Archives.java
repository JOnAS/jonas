/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004-2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ant.jonasbase;

import java.io.File;

/**
 * Allow to copy files to JONAS_BASE/xxxx/(autoload).
 * @author Florent Benoit
 */
public class Archives extends JCopy implements BaseTaskItf {

   /**
     * Folder to copy files (apps|webapps|rars|...).
     */
    private String folderName = null;

    /**
     * Default constructor.
     * @param header text for the header displayed for each line
     * @param folderName text to display
     */
    public Archives(final String header, final String folderName) {
        super();
        this.folderName = folderName;
        setLogInfo(header + "Copying " + folderName + "...");
    }

    /**
     * Override method to copy files to
     * JONAS_BASE/deploy.
     * @param destDir The destDir to set.
     */
    public void setDestDir(final File destDir) {
        File jBaseLibExtDir = new File(destDir.getPath() + File.separator + "deploy");
        super.setDestDir(jBaseLibExtDir);
    }

}
