/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004-2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ant.jonasbase;

import java.io.File;

/**
 * Allow to copy files to lib/ext.
 * @author Florent Benoit
 */
public class Lib extends JCopy implements BaseTaskItf {

    /**
     * Info for the logger.
     */
    private static final String INFO = "[Lib] ";

    /**
     * Directory for copying.
     */
    private String dir = "ext";

    /**
     * Default constructor.
     */
    public Lib() {
        super();
        // Don't fail if the source directory doesn't exist
        setFailOnError(false);
        setLogInfo(INFO + "Copying files to lib folder");
    }

    /**
     * Override method to copy files to lib/ext folder.
     * @param destDir The destDir to set.
     */
    @Override
    public void setDestDir(final File destDir) {
        File jBaseLibExtDir = new File(destDir.getPath() + File.separator + "lib" + File.separator + dir);
        super.setDestDir(jBaseLibExtDir);
    }

    /**
     * Gets the directory.
     * @return the directory
     */
    public String getDir() {
        return dir;
    }

    /**
     * Sets the directory.
     * @param dir the directory to set
     */
    public void setDir(final String dir) {
        this.dir = dir;
    }

}
