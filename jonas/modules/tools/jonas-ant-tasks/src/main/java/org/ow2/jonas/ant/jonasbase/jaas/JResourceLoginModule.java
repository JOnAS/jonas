/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ant.jonasbase.jaas;

/**
 * @author Julien Legrand
 */
public class JResourceLoginModule extends LoginModule {

    /**
     * Class name of the JResourceLoginModule
     */
    public static final String DEFAULT_LOGIN_MODULE_NAME = "org.ow2.jonas.security.auth.spi.JResourceLoginModule";

    /**
     * Default Resource name
     */
    private static final String DEFAULT_RESOURCENAME = "memrlm_1";

    /**
     *  Resource name
     */
    private String resourceName = DEFAULT_RESOURCENAME;

    /**
     * Server (JOnAS instance) name
     */
    private String serverName = "";

    /**
     * Certificate callback needed
     */
    private boolean certCallback = false;

    /**
     * Case conversion needed
     */
    private boolean useUpperCaseUsername = false;

    /**
     * Constructor
     */
    public JResourceLoginModule(){
        super();
    }

    /**
     * @return Returns the resource name.
     */
    public String getResourceName() {
        return resourceName;
    }

    /**
     * @param resourceName the resource name to set.
     */
    public void setResourceName(final String resourceName){
        this.resourceName = resourceName;
    }

    /**
     * @return Returns the server name.
     */
    public String getServerName() {
        return serverName;
    }

    /**
     * @param serverName the server name to set.
     */
    public void setServerName(final String serverName) {
        this.serverName = serverName;
    }

    /**
     * @return Returns true if a callback certificate is needed. False otherwise.
     */
    public boolean isCertCallback() {
        return certCallback;
    }

    /**
     * @param certCallback
     */
    public void setCertCallback(final boolean certCallback) {
        this.certCallback = certCallback;
    }

    /**
     * @return Returns true if the username needs to be converted in upper case. False otherwise.
     */
    public boolean isUseUpperCaseUsername() {
        return useUpperCaseUsername;
    }

    /**
     * @param useUpperCaseUsername
     */
    public void setUseUpperCaseUsername(final boolean useUpperCaseUsername) {
        this.useUpperCaseUsername = useUpperCaseUsername;
    }
}
