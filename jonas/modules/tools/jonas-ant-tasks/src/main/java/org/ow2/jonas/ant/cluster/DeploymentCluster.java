/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ant.cluster;

import java.io.File;

import org.ow2.jonas.ant.jonasbase.PropertyTask;

/**
 * @author eyindanga
 * Set deployment properties to cluster instances.
 */
public class DeploymentCluster extends ClusterTasks {

    /**
     * Info for the logger.
     */
    private static final String INFO = "[DeploymentCluster] ";

    /**
     * JOnAS prefix.
     */
    private static final String JOnAS_PREFIX = "jonas.";

    /**
     * Service prefix.
     */
    private static final String SERVICE_PREFIX = JOnAS_PREFIX + "service.";


    /**
     * JOnAS properties file.
     */
    private static final String PROPERTIES_FILE = JOnAS_PREFIX + "properties";

    /**
     * JOnAS development mode.
     */
    private static final String JOnAS_DEVMODE = JOnAS_PREFIX + "development";

    /**
     * Depmonitor development mode.
     */
    private static final String DEPMONITOR_DEVMODE = SERVICE_PREFIX + "depmonitor.development";


    /**
     * SecurityManager property.
     */
    private static final String ONDEMAND_ENABLED_PROPERTY = SERVICE_PREFIX + "web.ondemand.enabled";

    /**
     * Services property.
     */
    private static final String ONDEMAND_REDIRECTPORT_PROPERTY =  SERVICE_PREFIX + "web.ondemand.redirectPort";



    /**
     * Ondemand enabled ? <code>true</code> if so.
     */
    private boolean ondemandEnabled = false;
    /**
     * Ondemand redirect port.
     */
    private String ondemandRedirectPort = "0";
    /**
     * Depmonitor development mode enabled ? <code>true</code> if so.
     */
    private boolean depmonitorDevModeEnabled = true;

    /**
     * JOnAS Development mode enabled ? <code>true</code> if so.
     */
    private boolean devModeEnabled = false;

    /**
     * @return the ondemandEnabled
     */
    public boolean isOndemandEnabled() {
        return ondemandEnabled;
    }

    /**
     * @param ondemandEnabled the ondemandEnabled to set
     */
    public void setOndemandEnabled(final boolean ondemandEnabled) {
        this.ondemandEnabled = ondemandEnabled;
    }

    /**
     * @return the ondemandRedirectPort
     */
    public String getOndemandRedirectPort() {
        return ondemandRedirectPort;
    }

    /**
     * @param ondemandRedirectPort the ondemandRedirectPort to set
     */
    public void setOndemandRedirectPort(final String ondemandRedirectPort) {
        this.ondemandRedirectPort = ondemandRedirectPort;
    }

    /**
     * @return the depmonitorDevModeEnabled
     */
    public boolean isDepmonitorDevModeEnabled() {
        return depmonitorDevModeEnabled;
    }

    /**
     * @param depmonitorDevModeEnabled the depmonitorDevModeEnabled to set
     */
    public void setDepmonitorDevModeEnabled(final boolean depmonitorDevModeEnabled) {
        this.depmonitorDevModeEnabled = depmonitorDevModeEnabled;
    }

    /**
     * @return the devModeEnabled
     */
    public boolean isDevModeEnabled() {
        return devModeEnabled;
    }

    /**
     * @param devModeEnabled the devModeEnabled to set
     */
    public void setDevModeEnabled(final boolean devModeEnabled) {
        this.devModeEnabled = devModeEnabled;
    }

    @Override
    public void generatesTasks() {

        for (int i = getDestDirSuffixIndFirst(); i <= getDestDirSuffixIndLast(); i++) {

            String destDir = getDestDir(getDestDirPrefix(), i);
            log(INFO + "tasks generation for " + destDir);
            PropertyTask propertyTask = new PropertyTask();
            propertyTask.addPropertyStatement(PROPERTIES_FILE, DEPMONITOR_DEVMODE, Boolean.toString(depmonitorDevModeEnabled), false);
            propertyTask.addPropertyStatement(PROPERTIES_FILE, JOnAS_DEVMODE, Boolean.toString(devModeEnabled), false);
            propertyTask.addPropertyStatement(PROPERTIES_FILE, ONDEMAND_REDIRECTPORT_PROPERTY , ondemandRedirectPort, false);
            propertyTask.addPropertyStatement(PROPERTIES_FILE, ONDEMAND_ENABLED_PROPERTY , Boolean.toString(ondemandEnabled), false);
            propertyTask.setDestDir(new File(destDir));
            addTask(propertyTask);
        }
    }

}
