/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004-2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ant.jonasbase;

import java.io.File;

import org.apache.tools.ant.taskdefs.Mkdir;;

/**
 * Defines creation directory task.
 * @author Florent Benoit
 * @author Benoit Pelletier
 */
public class JMkdir extends Mkdir implements BaseTaskItf {

    /**
     * configuration file used.
     */
    private String configurationFile = null;

    /**
     * Information for the logger.
     */
    private String logInfo = null;

    /**
     * JONAS_ROOT directory.
     */
    private File jonasRoot = null;

    /**
     * Sets the configuration file.
     * @param configurationFile The configurationFile to set.
     */
    public void setConfigurationFile(final String configurationFile) {
        this.configurationFile = configurationFile;
    }

    /**
     * @param destDir The destDir to set.
     */
    public void setDestDir(final File destDir) {
        setDir(destDir);
    }

    /**
     * Gets logger info (to be displayed)?
     * @return logger info
     * @see org.ow2.jonas.ant.jonasbase.BaseTaskItf#getLogInfo()
     */
    public String getLogInfo() {
        return logInfo;
    }

    /**
     * Set the info to be displayed by the logger.
     * @param logInfo information to be displayed
     * @see org.ow2.jonas.ant.jonasbase.BaseTaskItf#setLogInfo(String)
     */
    public void setLogInfo(final String logInfo) {
        this.logInfo = logInfo;
    }

    /**
     * @param jonasRoot The jonasRoot directory
     */
    public void setJonasRoot(final File jonasRoot) {
        this.jonasRoot = jonasRoot;
    }

    /**
     * @return the jonasRoot.
     */
    protected File getJonasRoot() {
        return jonasRoot;
    }
}
