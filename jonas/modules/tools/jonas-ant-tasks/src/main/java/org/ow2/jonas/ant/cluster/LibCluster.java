/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Benoit Pelletier
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ant.cluster;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.ow2.jonas.ant.jonasbase.Lib;

import org.apache.tools.ant.types.FileSet;


/**
 *  Define LibCluster task
 * @author Benoit Pelletier
 */
public class LibCluster extends ClusterTasks {

    /**
     * Info for the logger
     */
    private static final String INFO = "[LibCluster] ";

    /**
     * List of files (WSDL publish)
     */
    private List files = new ArrayList();

    /**
     * destination Directory
     */
    private File destDir = null;

    /**
     * Default constructor
     */
    public LibCluster() {
        super();
    }

    /**
     * Add file (wsdl publish)
     * @param file properties file
     */
    public void addConfiguredFileSet(FileSet file) {
        files.add(file);
    }

    /**
     * Override method to copy files to custom folder and not for several JONAS_BASE
     * @param destDir The destDir to set.
     */
    public void setDestDir(File destDir) {
        this.destDir = destDir;
    }

    /**
     * Generates the LibCluster tasks for each JOnAS's instances
     */
    public void generatesTasks() {

        for (int i = getDestDirSuffixIndFirst(); i <= getDestDirSuffixIndLast(); i++) {

            String destDir = getDestDir(getDestDirPrefix(), i);
            log(INFO + "tasks generation for " + destDir);

            // creation of the Lib task
            Lib lib = new Lib();

            for (Iterator it = files.iterator(); it.hasNext();) {
                FileSet fileSet = (FileSet) it.next();
                lib.addFileset(fileSet);
                if (this.destDir == null) {
                    lib.setDestDir(new File(destDir));
                } else {
                    // bypass the adding of '/lib/ext' suffix
                    lib.setTodir(this.destDir);
                }

                addTask(lib);
            }
        }
    }
}