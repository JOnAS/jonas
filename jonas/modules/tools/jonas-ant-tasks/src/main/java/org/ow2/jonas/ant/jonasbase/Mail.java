/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004-2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ant.jonasbase;

import java.io.File;
import java.util.Properties;

import org.ow2.jonas.ant.JOnASBaseTask;

import org.apache.tools.ant.BuildException;

/**
 * Allow to create mail factory.
 * @author Florent Benoit
 */
public class Mail extends JTask implements BaseTaskItf {

    /**
     * Info for the logger.
     */
    private static final String INFO = "[Mail] ";

    /**
     * Property for Mail factories.
     */
    private static final String MAILFACTORY_PROPERTY = "jonas.service.mail.factories";

    /**
     * Session factory.
     */
    private static final String SESSION_FACTORY = "Session";

    /**
     * MimePartDataSource factory.
     */
    private static final String MIMEPARTDATASOURCE_FACTORY = "MimePartDataSource";

    /**
     * Session factory class.
     */
    private static final String SESSION_FACTORY_CLASS = "javax.mail.Session";

    /**
     * MimePartDataSource factory class.
     */
    private static final String MIMEPARTDATASOURCE_FACTORY_CLASS = "javax.mail.internet.MimePartDataSource";

    /**
     * Type of factory (Session or MimePartDataSource).
     */
    private String type = null;

    /**
     * Name of the factory.
     */
    private String name = null;

    /**
     * Recipient (TO) of MimePartDataSource factory.
     */
    private String mailTo = null;

    /**
     * Subject of MimePartDataSource factory.
     */
    private String subject = null;

    /**
     * Host of the mail server.
     */
    private String host = null;

    /**
     * Sets the recipient (MimePartDataSource).
     * @param mailTo recipient.
     */
    public void setMailTo(final String mailTo) {
        this.mailTo = mailTo;
    }

    /**
     * Sets the name.
     * @param name name of the factory
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Sets the subject (MimePartDataSource).
     * @param subject of the mail
     */
    public void setSubject(final String subject) {
        this.subject = subject;
    }

    /**
     * Sets the type of factory.
     * @param type of factory
     */
    public void setType(final String type) {
        this.type = type;
    }

    /**
     * Sets the host of mail server.
     * @param host  host of mail server
     */
    public void setHost(final String host) {
        this.host = host;
    }
    /**
     * Check the properties.
     */
    private void checkProperties() {
        if (name == null) {
            throw new BuildException(INFO + "Property 'name' is missing.");
        } else if (type == null) {
            throw new BuildException(INFO + "Property 'type' is missing.");
        }
    }

    /**
     * Execute this task.
     */
    public void execute() {
        checkProperties();

        Properties props = new Properties();
        props.put("mail.factory.name", name);
        String className = null;

        String infoTxt = "Generating a MailFactory with type '" + type + "' and name '" + name + "'";
        if(host != null) {
            props.put("mail.host", host);
        }
        if (type.equalsIgnoreCase(SESSION_FACTORY)) {
            className = SESSION_FACTORY_CLASS;
        } else if (type.equalsIgnoreCase(MIMEPARTDATASOURCE_FACTORY)) {
            className = MIMEPARTDATASOURCE_FACTORY_CLASS;
            // Set mailTo
            if (mailTo != null) {
                props.put("mail.to", mailTo);
                infoTxt += ", mailTo field '" + mailTo + "'";
            }
            // Subject
            if (subject != null) {
                props.put("mail.subject", subject);
                infoTxt += ", subject '" + subject + "'";
            }

        } else {
            throw new BuildException(INFO + "Invalid type '" + type + "'.");
        }

        // Display info
        log(INFO + infoTxt + "...");

        // Set type
        props.put("mail.factory.type", className);

        // Build new mail factory
        String jBaseConf = getDestDir().getPath() + File.separator + "conf";

        String propsFileName = jBaseConf + File.separator + name + ".properties";
        File tmpFile = new File(propsFileName);

        writePropsToFile(INFO, props, tmpFile);

        // Now add name to the existing list for the property
        changeValueForKey(INFO, jBaseConf, JOnASBaseTask.JONAS_CONF_FILE, MAILFACTORY_PROPERTY, name, true);
    }

}
