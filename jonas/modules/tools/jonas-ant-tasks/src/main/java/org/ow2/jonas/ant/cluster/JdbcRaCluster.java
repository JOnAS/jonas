/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Benoit Pelletier
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ant.cluster;

import java.io.File;

import org.ow2.jonas.ant.jonasbase.JdbcRa;

/**
 *  Define JdbcRaCluster task
 * @author Benoit Pelletier
 */
public class JdbcRaCluster extends ClusterTasks {

    /**
     * Info for the logger
     */
    private static final String INFO = "[JdbcRaCluster] ";

    /**
     * New file or add into existing one
     */
    private boolean newFile = false;

    /**
     * Name of this JDBC Resource Adaptor
     */
    private String name = null;

    /**
     * Mapper Name of this JDBC Resource Adaptor
     */
    private String mapperName = null;

    /**
     * username of this JDBC Resource Adaptor
     */
    private String user = null;

    /**
     * Password of this JDBC Resource Adaptor
     */
    private String password = null;

    /**
     * URL of this JDBC Resource Adaptor
     */
    private String url = null;

    /**
     * Driver Name of this JDBC Resource Adaptor (may be set to the P6Spy driver name)
     */
    private String driverName = null;

    /**
     * Driver Name of this JDBC Resource Adaptor
     */
    private String realDriverName = null;

    /**
     * Max Pool Size
     */
    private String maxPoolSize = "100";

    /**
     * JNDI Name of this JDBC Resource Adaptor
     */
    private String jndiName = null;

    /**
     * Copy to autoload dir or not
     */
    private boolean autoload = true;

    /**
     * Using of the P6Spy tool or not
     */
    private boolean p6spy = false;
    /**
     * Default constructor
     */
    public JdbcRaCluster() {
        super();
    }

    /**
     * Set the name of this JDBC Resource Adaptor
     * @param name the name of this JDBC Resource Adaptor
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Set the mapper name of this JDBC Resource Adaptor
     * @param mapperName the mappername of this JDBC Resource Adaptor
     */
    public void setMapperName(final String mapperName) {
        this.mapperName = mapperName;
    }

    /**
     * Set the user of this JDBC Resource Adaptor
     * @param user the user of this JDBC Resource Adaptor
     */
    public void setUser(final String user) {
        this.user = user;
    }

    /**
     * Set the password of this JDBC Resource Adaptor
     * @param password the name of this JDBC Resource Adaptor
     */
    public void setPassword(final String password) {
        this.password = password;
    }

    /**
     * Set the url of this JDBC Resource Adaptor
     * @param url the name of this JDBC Resource Adaptor
     */
    public void setUrl(final String url) {
        this.url = url;
    }

    /**
     * Set the max pool size of this JDBC Resource Adaptor Connection Pool
     * @param maxPoolSize max pool size of connection
     */
    public void setMaxPoolSize(final String maxPoolSize) {
        this.maxPoolSize = maxPoolSize;
    }

    /**
     * Set the name of the driver of this JDBC Resource Adaptor
     * @param driverName the name of the driver of this JDBC Resource Adaptor
     */
    public void setDriverName(final String driverName) {
        this.driverName = driverName;
        this.realDriverName = driverName;
    }

    /**
     * Set the jndiName of this JDBC Resource Adaptor
     * @param jndiName the jndiName of this JDBC Resource Adaptor
     */
    public void setJndiName(final String jndiName) {
        this.jndiName = jndiName;
    }

    /**
     * opy rar to autoload or only in rars/ ?
     * @param autoload true of false
     */
    public void setAutoload(final boolean autoload) {
        this.autoload = autoload;
    }

    /**
     * Configure the using of the P6Spy tool or not ?
     * @param p6spy true or false
     */
    public void setP6spy(final boolean p6spy) {
        this.p6spy = p6spy;
    }

    /**
     * Generates the JdbcRa tasks for each JOnAS's instances
     */
    @Override
    public void generatesTasks() {

        int portInd = 0;

        for (int i = getDestDirSuffixIndFirst(); i <= getDestDirSuffixIndLast(); i++) {

            String destDir = getDestDir(getDestDirPrefix(), i);
            log(INFO + "tasks generation for " + destDir);
            // creation of the JdbcRa task
            JdbcRa jdbcRa = new JdbcRa();

            jdbcRa.setAutoload(autoload);
            jdbcRa.setDriverName(driverName);
            jdbcRa.setJndiName(jndiName);
            jdbcRa.setMapperName(mapperName);
            jdbcRa.setMaxPoolSize(maxPoolSize);
            jdbcRa.setName(name);
            jdbcRa.setP6spy(p6spy);
            jdbcRa.setPassword(password);
            jdbcRa.setUrl(url);
            jdbcRa.setUser(user);

            jdbcRa.setDestDir(new File(destDir));

            addTask(jdbcRa);

            portInd++;

        }
    }
}
