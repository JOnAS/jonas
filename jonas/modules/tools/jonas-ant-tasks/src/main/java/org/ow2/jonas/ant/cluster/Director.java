/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005-2006 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Benoit Pelletier
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ant.cluster;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.ow2.jonas.ant.jonasbase.JEcho;
import org.ow2.jonas.ant.jonasbase.JMkdir;
import org.ow2.jonas.ant.jonasbase.JTouch;
import org.ow2.jonas.ant.jonasbase.Tasks;



/**
 * Allow to configure the director Load Balancer for Apache
 * @author Benoit Pelletier
 */
public class Director extends Tasks {

    /**
     * Info for the logger
     */
    private static final String INFO = "[Director] ";

    /**
     * Name of the director conf file
     */
    private static final String DIRECTOR_FILE = "enhydra_director.conf";

    /**
     * Configuration file
     */
    private File configurationFile = null;

    /**
     * Application Server list
     */
    private List appServerList = null;

    /**
     * sticky session
     */
    private boolean stickySession = false;

    /**
     * Default constructor
     */
    public Director() {
        super();
        appServerList = new ArrayList();
    }

    /**
     * Creation of the Director file
     * @param destDir destination directory
     */
    public void createFile(String destDir) {
        JMkdir mkdir = new JMkdir();
        mkdir.setDestDir(new File(destDir));
        addTask(mkdir);

        JTouch touchWorker = new JTouch();
        configurationFile = new File(destDir + "/" + DIRECTOR_FILE);
        touchWorker.setDestDir(configurationFile);
        addTask(touchWorker);

   }

    /**
     * Add an application server
     * @param portNumber port number
     * @param lbFactor load balancing factor
     */
    public void addAppServer(String portNumber, String lbFactor) {
        AppServer appServer = new AppServer();
        appServer.setPortNumber(portNumber);
        appServer.setLbFactor(lbFactor);
        appServerList.add(appServer);
    }

    /**
     * get app server definition
     * @param appServer application server to define
     * @return definition of the application server
     */
    private String getAppServerDef(AppServer appServer) {
        String appServerDef = "\n"
            + "      <AppServer host= \"localhost\" port=\""
            + appServer.getPortNumber()
            + "\" weight=\""
            + appServer.getLbFactor()
            + "\" />";
        return appServerDef;
    }

    /**
     * creation of the AppServer file
     */
    private void flushAppServerFile() {
        JEcho echo = new JEcho();
        echo.setDestDir(configurationFile);
        String appServersDefs = "";
        int ind = 1;
        for (Iterator it = this.appServerList.iterator(); it.hasNext();) {
            AppServer appServer = (AppServer) it.next();
            appServer.setName("appServer" + ind);
            appServersDefs = appServersDefs + getAppServerDef(appServer);
            ind++;
        }

        String contentFile = "\n"
            + "<?xml version=\"1.0\"?>" + "\n"
            + "<!DOCTYPE EnhydraDirectorConfig SYSTEM \"EnhydraDirectorConfig.dtd\">" + "\n"
            + "<EnhydraDirectorConfig>" + "\n"
            + "   <Application prefix=\"/sampleCluster2/\">"
            + appServersDefs + "\n"
            + "   </Application>" + "\n"
            + "   <Status prefix=\"/status\">" + "\n"
            + "      <Restrict server=\"127.0.0.1\" />" + "\n"
            + "      <Restrict client=\"127.0.0.1\" />" + "\n"
            + "   </Status>" + "\n"
            + "</EnhydraDirectorConfig>";

        echo.setMessage(contentFile);
        echo.setLogInfo(INFO + "Flushing AppServer Configuration in '" + configurationFile + "'");
        addTask(echo);
    }

    /**
     * Generation of the config files
     */
    public void flushFile() {
        flushAppServerFile();
    }

    /**
     * Set sticky Session
     * @param stickySession to set
     **/
    public void setStickySession(boolean stickySession) {
        this.stickySession = stickySession;
    }

    /**
     * Define an inner class for application servers
     * @author Benoit Pelletier
     */
    public class AppServer {

        /**
         * port number
         */
        private String portNumber = null;

        /**
         * load balancing factor
         */
        private String lbFactor = null;

        /**
         * name
         */
        private String name = null;

        /**
         * get port number
         * @return port number
         */
        public String getPortNumber() {
            return portNumber;
        }

        /**
         * set port number
         * @param portNumber port number
         */
        public void setPortNumber(String portNumber) {
            this.portNumber = portNumber;
        }

        /**
         * get load balancing factor
         * @return load balancing factor
         */
        public String getLbFactor() {
            return lbFactor;
        }

        /**
         * set load balancing factor
         * @param lbFactor load balancing factor
         */
        public void setLbFactor(String lbFactor) {
            this.lbFactor = lbFactor;
        }

        /**
         * get name
         * @return name
         */
        public String getName() {
            return name;
        }

        /**
         * set name
         * @param name name to set
         */
        public void setName(String name) {
            this.name = name;
        }
    }
}