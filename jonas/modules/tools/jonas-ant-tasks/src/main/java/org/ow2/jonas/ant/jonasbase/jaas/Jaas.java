/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ant.jonasbase.jaas;

import org.ow2.jonas.ant.jonasbase.Tasks;
import org.ow2.jonas.ant.jonasbase.JReplace;
import org.ow2.jonas.ant.JOnASBaseTask;
import org.apache.tools.ant.Task;

/**
 * Allow to configure JAAS
 * @author Julien Legrand
 */
public class Jaas extends Tasks {

    /**
     * Info for the logger.
     */
    private static final String INFO = "[JAAS] ";

    /**
     * Token for the begin of the jaas.config configuration file.
     */
    private static final String TOKEN_BEGIN_CONF_FILE = "jaasclient {";

    /**
     * Constructor
     */
    public Jaas(){
        super();
    }

    /**
     * Configure a JAAS entry
     * @param entry the entry to configure.
     */
    public void addConfiguredJaasEntry(final JaasEntry entry){

        JReplace propertyReplace = new JReplace();
        propertyReplace.setConfigurationFile(JOnASBaseTask.JAAS_CONF_FILE);
        propertyReplace.setToken(TOKEN_BEGIN_CONF_FILE);
        StringBuffer value = new StringBuffer();

        value.append(entry.getName() + "{" + "\n");
        for(LoginModule lm : entry.getLoginModules()){

            if(lm instanceof JResourceLoginModule){

                JResourceLoginModule rlm = (JResourceLoginModule) lm;

                value.append("    " + JResourceLoginModule.DEFAULT_LOGIN_MODULE_NAME + " " + lm.getFlag() + "\n");
                value.append("    " + "resourceName=\"" + rlm.getResourceName() + "\"" + "\n");

                if(rlm.getServerName() != null && !"".equals(rlm.getServerName())){
                    value.append("    " + "serverName=\"" + rlm.getServerName() + "\"" + "\n");
                }
                if(rlm.isUseUpperCaseUsername()){
                    value.append("    " + "useUpperCaseUsername=\"" + rlm.isUseUpperCaseUsername() + "\"" + "\n");
                }
                if(rlm.isCertCallback()){
                    value.append("    " + "certCallback=\"" + rlm.isCertCallback() + "\"" + "\n");
                }
            }
            
            value.append("\n");
        }

        value.append("    " + ";" + "\n");
        value.append("};" + "\n");
        value.append("\n");
        value.append(TOKEN_BEGIN_CONF_FILE);

        propertyReplace.setValue(value.toString());
        propertyReplace.setLogInfo(INFO + "Add the JAAS entry : " + entry.getName());
        addTask(propertyReplace);
    }
}
