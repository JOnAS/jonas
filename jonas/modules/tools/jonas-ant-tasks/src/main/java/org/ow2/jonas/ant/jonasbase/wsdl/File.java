/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ant.jonasbase.wsdl;


/**
 * Defines a file for WSDL publication.
 * @author Florent Benoit
 */
public class File {

    /**
     * Directory for publish.
     */
    private String dir = null;

    /**
     * Class for FileWSDL publisher.
     */
    public static final String PUBLISHER_CLASS = "org.ow2.jonas.ws.publish.internal.file.FileWSDLPublisher";

    /**
     * Default encoding.
     */
    private static final String DEFAULT_ENCODING = "UTF-8";

    /**
     * User defined encoding.
     */

    private String encoding = DEFAULT_ENCODING;

    /**
     * Name of the file.
     */
    private String name = null;

    /**
     * Gets the directory.
     * @return the directory
     */
    public String getDir() {
        return dir;
    }

    /**
     * Sets the directory.
     * @param dir the directory to set
     */
    public void setDir(final String dir) {
        this.dir = dir;
    }

    /**
     * Gets the encoding.
     * @return the encoding.
     */
    public String getEncoding() {
        return encoding;
    }

    /**
     * Sets the encoding.
     * @param encoding the encoding to set.
     */
    public void setEncoding(final String encoding) {
        this.encoding = encoding;
    }

    /**
     * Gets the name.
     * @return the name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     * @param name name to set.
     */
    public void setName(final String name) {
        this.name = name;
    }
}
