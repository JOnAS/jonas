/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Benoit Pelletier
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ant.cluster;

import java.io.File;

import org.ow2.jonas.ant.jonasbase.Db;

/**
 *  Define DbCluster task
 * @author Benoit Pelletier
 */
public class DbCluster extends ClusterTasks {

    /**
     * Info for the logger
     */
    private static final String INFO = "[DbCluster] ";

    /**
     * ports range
     */
    private String[] portRange = null;

    /**
     * Default constructor
     */
    public DbCluster() {
        super();
    }

    /**
     * Set ports range
     * @param portRange ports range
     */
    public void setPortRange(String portRange) {
        this.portRange = portRange.split(",");

    }

    /**
     * Generates the db tasks for each JOnAS's instances
     */
    public void generatesTasks() {

        int portInd = 0;

        for (int i = getDestDirSuffixIndFirst(); i <= getDestDirSuffixIndLast(); i++) {

            String destDir = getDestDir(getDestDirPrefix(), i);
            log(INFO + "tasks generation for " + destDir);
            // creation of the Db task
            Db db = new Db();

            db.setPort(portRange[portInd]);
            db.setDestDir(new File(destDir));

            addTask(db);

            portInd++;

        }
    }
}