/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Benoit Pelletier
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ant.cluster;

import java.util.Iterator;

/**
 * Defines Weblevel task
 * @author Benoit Pelletier
 */
public class WebLevel extends ClusterTasks {

    /**
     * Info for the logger
     */
    private static final String INFO = "[WebLevel] ";

    /**
     * Default constructor
     */
    public WebLevel() {
        super();
    }

    /**
     * Add tasks for Services configuration
     * @param servicesCluster added task
     */
    public void addConfiguredServicesCluster(final ServicesCluster servicesCluster) {
        servicesCluster.setRootTask(getRootTask());
        log(INFO + "ServicesCluster added");
        servicesCluster.setLogInfo("ServicesCluster");
        addClusterTask(servicesCluster);
    }

    /**
     * Add tasks for WebContainer configuration
     * @param webContainerCluster added task
     */
    public void addConfiguredWebContainerCluster(final WebContainerCluster webContainerCluster) {
        webContainerCluster.setRootTask(getRootTask());
        log(INFO + "WebContainerCluster added");
        webContainerCluster.setLogInfo("WebContainerCluster");
        addClusterTask(webContainerCluster);
    }

    /**
     * Add tasks for LibCluster configuration
     * @param libCluster added task
     */
    public void addConfiguredLibCluster(final LibCluster libCluster) {
        libCluster.setRootTask(getRootTask());
        log(INFO + "LibCluster added");
        libCluster.setLogInfo("LibCluster");
        addClusterTask(libCluster);
    }
    /**
     * Add tasks for DeploymentCluster configuration
     * @param deploymentCluster added task
     */
    public void addConfiguredDeploymentCluster(final DeploymentCluster deploymentCluster) {
        deploymentCluster.setRootTask(getRootTask());
        log(INFO + "DeploymentCluster added");
        deploymentCluster.setLogInfo("DeploymentCluster");
        addClusterTask(deploymentCluster);
    }

    /**
     *  Generates tasks for common
     */
    @Override
    public void generatesTasks() {

        for (Iterator it = this.getClusterTasks().iterator(); it.hasNext();) {
            ClusterTasks ct = (ClusterTasks) it.next();

            log(INFO + "tasks generation for " + ct.getLogInfo());

            ct.setArch(getArch());
            ct.setDestDirPrefix(getDestDirPrefix());
            ct.setDestDirSuffixIndFirst(getDestDirSuffixIndFirst());
            ct.setDestDirSuffixIndLast(getDestDirSuffixIndLast());
            ct.generatesTasks();
            addTasks(ct);
        }
    }

}