/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Benoit Pelletier
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ant.cluster;


import java.io.File;
import java.util.Iterator;

import org.ow2.jonas.ant.jonasbase.BaseTaskItf;
import org.ow2.jonas.ant.jonasbase.Jms;


/**
 * Define JmsCluster task
 * @author Benoit Pelletier
 */
public class JmsCluster extends ClusterTasks {

    /**
     * Info for the logger
     */
    private static final String INFO = "[JmsCluster] ";

    /**
     * ports range
     */
    private String[] portRange = null;

    /**
     * initialTopics
     */
    private String initialTopics = null;

    /**
     * initialQueues
     */
    private String initialQueues = null;

    /**
     * Default constructor
     */
    public JmsCluster() {
        super();
    }

    /**
     * Set ports range
     * @param portRange ports range
     */
    public void setPortRange(String portRange) {
        this.portRange = portRange.split(",");

    }

    /**
     * Set the initial topics
     * @param initialTopics comma separated list of topics
     */
    public void setInitialTopics(String initialTopics) {
        this.initialTopics = initialTopics;
    }

    /**
     * Set the initial queues when JOnAS start
     * @param initialQueues comma separated list of topics
     */
    public void setInitialQueues(String initialQueues) {
        this.initialQueues = initialQueues;
    }

    /**
     * Generates the Jms tasks for each JOnAS's instances
     */
    public void generatesTasks() {

        int portInd = 0;

        for (int i = getDestDirSuffixIndFirst(); i <= getDestDirSuffixIndLast(); i++) {

            String destDir = getDestDir(getDestDirPrefix(), i);
            log(INFO + "tasks generation for " + destDir);
            // creation of the Db task
            Jms jms = new Jms();

            jms.setPort(portRange[portInd]);
            jms.setInitialQueues(initialQueues);
            jms.setInitialTopics(initialTopics);

            // set destDir for each carol task
            for (Iterator it = jms.getTasks().iterator(); it.hasNext();) {
                BaseTaskItf task = (BaseTaskItf) it.next();
                task.setDestDir(new File(destDir));
            }

            addTasks(jms);

            portInd++;

        }
    }
}