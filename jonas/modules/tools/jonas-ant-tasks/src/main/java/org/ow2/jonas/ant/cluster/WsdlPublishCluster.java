/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005-2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ant.cluster;


import java.util.ArrayList;
import java.util.List;

import org.ow2.jonas.ant.jonasbase.wsdl.File;
import org.ow2.jonas.ant.jonasbase.wsdl.Uddi;
import org.ow2.jonas.ant.jonasbase.wsdl.WsdlPublish;


/**
 * Define WsdlPublishCluster task.
 * @author Benoit Pelletier
 */
public class WsdlPublishCluster extends ClusterTasks {

    /**
     * Info for the logger.
     */
    private static final String INFO = "[WsdlPublishCluster] ";

    /**
     * List of files (WSDL publish).
     */
    private List<File> files = new ArrayList<File>();

    /**
     * List of uddi (WSDL publish).
     */
    private List<Uddi> uddis = new ArrayList<Uddi>();

    /**
     * Add file (wsdl publish).
     * @param file properties file
     */
    public void addConfiguredFile(final File file) {
        files.add(file);
    }

    /**
     * Add UDDI (wsdl publish).
     * @param uddi properties file
     */
    public void addConfiguredUddi(final Uddi uddi) {
        uddis.add(uddi);
    }
    /**
     * Generates the WsdlPublish tasks for each JOnAS's instances.
     */
    public void generatesTasks() {

        for (int i = getDestDirSuffixIndFirst(); i <= getDestDirSuffixIndLast(); i++) {

            String destDir = getDestDir(getDestDirPrefix(), i);

            log(INFO + "tasks generation for " + destDir);

            // creation of the Mail task
            WsdlPublish wsdl = new WsdlPublish();

            wsdl.setFiles(files);
            wsdl.setUddis(uddis);

            wsdl.setDestDir(new java.io.File(destDir));

            addTask(wsdl);

        }
    }
}
