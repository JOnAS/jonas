/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004-2008 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ant.jonasbase;

import java.io.File;

import org.ow2.jonas.ant.JOnASBaseTask;

import org.apache.tools.ant.BuildException;


/**
 * Allow to configure the DB service.
 * @author Florent Benoit
 */
public class Db extends JTask implements BaseTaskItf {

    /**
     * Info for the logger.
     */
    private static final String INFO = "[DB] ";

    /**
     * Name of the property for changing the port.
     */
    private static final String PORT_PROPERTY = "jonas.service.db.port";

    /**
     * Port number.
     */
    private String portNumber = null;

    /**
     * Default constructor.
     */
    public Db() {
        super();
    }

    /**
     * Set the port number for the Db service.
     * @param portNumber the port for the Db service
     */
    public void setPort(final String portNumber) {
            this.portNumber = portNumber;
    }


    /**
     * Check the properties.
     */
    private void checkProperties() {
        if (portNumber == null) {
            throw new BuildException(INFO + "Property 'portNumber' is missing.");
        }
    }

    /**
     * Execute this task.
     */
    public void execute() {
        checkProperties();

        // Path to JONAS_BASE
        String jBaseConf = getDestDir().getPath() + File.separator + "conf";

        changeValueForKey(INFO, jBaseConf, JOnASBaseTask.JONAS_CONF_FILE,
                PORT_PROPERTY, portNumber, false);


    }
}
