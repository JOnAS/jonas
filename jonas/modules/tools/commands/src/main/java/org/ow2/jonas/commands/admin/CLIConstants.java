/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.commands.admin;

public interface CLIConstants {

    /**
     * Static states
     */
    public final static int ADDFILE = 101;

    /**
     * Remove file state.
     */
    public final static int REMOVEFILE = 102;

    /**
     * List bean state.
     */
    public final static int LISTBEAN = 103;

    /**
     * List Jndi state.
     */
    public final static int LISTJNDI = 104;

    /**
     * List env state.
     */
    public final static int LISTENV = 105;

    /**
     * List topics state.
     */
    public final static int LISTTOPICS = 106;

    /**
     * Debug option state.
     */
    public final static int DEBUGOPTION = 107;

    /**
     * .
     */
    public final static int TTOPTION = 108;

    /**
     * Sync option state.
     */
    public final static int SYNCOPTION = 109;

    /**
     * Custom option state.
     */
    public final static int CUSTOMOPTION = 110;

    /**
     * Passivate option state.
     */
    public final static int PASSIVATEOPTION = 111;

    /**
     * GC option state.
     */
    public final static int GCOPTION = 112;

    /**
     * Is deploy option state.
     */
    public final static int ISDEPLOYOPTION = 113;

    /**
     * Is start option state.
     */
    public final static int STARTOPTION = 114;

    /**
     * Stop option state.
     */
    public final static int STOPOPTION = 115;

    /**
     * List module state.
     */
    public final static int LISTMODULE = 116;

    /**
     * List applications state.
     */
    public final static int LISTAPP = 117;

}