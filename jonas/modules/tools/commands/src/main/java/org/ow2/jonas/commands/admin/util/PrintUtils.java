/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.commands.admin.util;

import java.io.PrintStream;

/**
 * This utility class is responsible of keeping logs coherents.
 * Should be used to display informative messages to the user.
 */
public class PrintUtils {

    /**
     * Success prefix.
     */
    private static final String SUCCESS = "[SUCCESS] ";

    /**
     * Failure prefix.
     */
    private static final String FAILURE = "[FAILURE] ";

    /**
     * Error prefix.
     */
    private static final String ERROR = "[ERROR] ";

    /**
     * Warning prefix.
     */
    private static final String WARNING = "[WARNING] ";

    /**
     * Print a message on the given PrintStream.
     * @param message displayed message
     * @param stream PrintStream used to print the message
     */
    public static void println(final String message,
                               final PrintStream stream) {
        stream.println(message);
    }

    /**
     * Print a message on the System.out PrintStream.
     * @param message displayed message
     */
    public static void println(final String message) {
        println(message, System.out);
    }

    /**
     * Print the given Throwable on the given PrintStream.
     * @param exception displayed exception
     * @param stream PrintStream used to print the message
     */
    public static void println(final Throwable exception,
                               final PrintStream stream) {
        // TODO Maybe we could format the Throwable appropriately ? only root cause ?
        exception.printStackTrace(stream);
    }

    /**
     * Print the given Throwable on the System.err PrintStream.
     * @param exception displayed exception
     */
    public static void println(final Throwable exception) {
        println(exception, System.err);
    }

    /**
     * Print a success type message (prefixed with [SUCCESS])
     * on the System.out PrintStream.
     * @param message displayed message
     */
    public static void warning(final String message) {
        println(WARNING + message, System.err);
    }

    /**
     * Print a success type message (prefixed with [SUCCESS])
     * on the System.out PrintStream.
     * @param message displayed message
     */
    public static void success(final String message) {
        println(SUCCESS + message, System.out);
    }

    /**
     * Print a failure type message (prefixed with [FAILURE])
     * on the System.err PrintStream.
     * @param message displayed message
     */
    public static void failure(final String message) {
        println(FAILURE + message, System.err);
    }

    /**
     * Print a failure type message (prefixed with [FAILURE])
     * on the System.err PrintStream and then print the
     * cause' message of the given exception.
     * @param message displayed message
     * @param exception Displayed exception
     */
    public static void failure(final String message, final Throwable exception) {
        failure(message);
        String type = exception.getClass().getSimpleName();
        failure("Cause: [" + type + "] " + exception.getMessage());
    }

    /**
     * Print an error type message (prefixed with [ERROR])
     * on the System.err PrintStream.
     * @param message displayed message
     */
    public static void error(final String message) {
        println(ERROR + message, System.err);
    }

    /**
     * Print an error type message (prefixed with [ERROR])
     * on the System.err PrintStream and then print the
     * cause' message of the given exception.
     * @param message displayed message
     * @param exception Throwable
     */
    public static void error(final String message, final Throwable exception) {
        error(message);
        String type = exception.getClass().getSimpleName();
        error("Cause: [" + type + "] " + exception.getMessage());
    }
}
