/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.cluster.daemon.api;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Defines the MBean interface. Don't use Models MBeans as it will imply some
 * external libraries like commons-modeler (easier).
 * @author Benoit Pelletier
 */
/**
 * @author eyindanga
 */
public interface IClusterDaemon {

    /**
     * @return Object Name
     */
    String getObjectName() throws ClusterDaemonException;

    /**
     * Sets the object name of this mbean
     * @param name the Object Name
     * @throws ClusterDaemonException any.
     */
    void setObjectName(String name) throws ClusterDaemonException;

    /**
     * @return true if it is an event provider
     * @throws ClusterDaemonException any.
     */
    boolean iseventProvider() throws ClusterDaemonException;

    /**
     * @return true if this managed object implements J2EE State Management
     *         Model
     * @throws ClusterDaemonException any.
     */
    boolean isstateManageable() throws ClusterDaemonException;

    /**
     * @return true if this managed object implements the J2EE StatisticProvider
     *         Model
     * @throws ClusterDaemonException any.
     */
    boolean isstatisticsProvider() throws ClusterDaemonException;

    /**
     * @return list of controlled servers.
     */
    public ArrayList serversNames() throws ClusterDaemonException;

    /**
     * Controlled servers.
     * @return cluster daemon controlled server names.
     */
    public ArrayList getControlledServersNames() throws ClusterDaemonException;

    /**
     * @param serverName The name of the server
     * @return true if the given server is managed by the cluster daemon. Otherwise, return false.
     * @throws ClusterDaemonException
     */
    boolean isServerManaged(final String serverName) throws ClusterDaemonException;

    /**
     * @return the domain name for a specified server name.
     * @param name JOnAS instance name
     * @throws ClusterDaemonException any.
     */
    String getDomain4Server(String name) throws ClusterDaemonException;

    /**
     * @return the JAVA_HOME for a specified server name.
     * @param name JOnAS instance name
     * @throws ClusterDaemonException any.
     */
    String getJavaHome4Server(String name) throws ClusterDaemonException;

    /**
     * get <code>JONAS_ROOT</code> for the given server.
     * @return the JONAS_ROOT for a specified server name
     * @param name JOnAS instance name
     * @throws ClusterDaemonException any.
     */
    String getJonasRoot4Server(String name) throws ClusterDaemonException;

    /**
     * Get <code>JONAS_BASE</code> for the given server.
     * @return the JONAS_BASE for a specified server name.
     * @param name JOnAS instance name
     * @throws ClusterDaemonException any.
     */
    String getJonasBase4Server(String name) throws ClusterDaemonException;

    /**
     * Get command for the given server.
     * @return the user command for a specified server name
     * @param name JOnAS instance name
     * @throws ClusterDaemonException any.
     */
    String getJonasCmd4Server(String name) throws ClusterDaemonException;

    /**
     * Get extra parameters for the given server.
     * @return xprm for a specified server name
     * @param name JOnAS instance name
     * @throws ClusterDaemonException any.
     */
    String getXprm4Server(String name) throws ClusterDaemonException;

    /**
     * True if <code>autoboot</code> is enabled for the given server.
     * @return autoBoot value for a specified server name
     * @param name JOnAS instance name
     */
    String getAutoBoot4Server(String name) throws ClusterDaemonException;

    /**
     * Get the whole JMX URL for the given server.
     * @return The JMX URL for a specified server name
     * @param name JOnAS instance name
     */
    String getJmxUrl4Server(String name) throws ClusterDaemonException;

    /**
     * The JMX port for the given server.
     * @return JMX port value for a specified server name
     * @param name JOnAS instance name
     */
    String getJmxPort4Server(String name) throws ClusterDaemonException;

    /**
     * Reload the configuration.
     * @throws ClusterDaemonException if an error occurs
     */
    void reloadConfiguration() throws ClusterDaemonException;

    /**
     * Add a server configuration.
     * @param name server name
     * @param domain domain name
     * @param description server description
     * @param javaHome JAVA_HOME dir
     * @param jonasBase JONAS_BASE dir
     * @param jonasRoot JONAS_ROOT dir
     * @param xprm extra JVM parameters
     * @param autoBoot automatic start
     * @param jonasCmd user command
     * @throws ClusterDaemonException if an error occurs
     */
    void addServer(String name, String domain, String description, String jonasRoot, String jonasBase, String javaHome, String xprm,
            String autoBoot, String jonasCmd, String saveIt) throws ClusterDaemonException;

    /**
     * Modify a server configuration.
     * @param name server name
     * @param domain domain name
     * @param description server description
     * @param javaHome JAVA_HOME dir
     * @param jonasBase JONAS_BASE dir
     * @param jonasRoot JONAS_ROOT dir
     * @param xprm extra JVM parameters
     * @param autoBoot automatic start
     * @param jonasCmd user command
     * @throws ClusterDaemonException if an error occurs
     */
    void modifyServer(String name, String domain, String description, String javaHome, String jonasRoot, String jonasBase, String xprm,
            String autoBoot, String jonasCmd) throws ClusterDaemonException;

    /**
     * Remove this server from cluster daemon control.
     * @param serverName the server to remove
     * @param saveIt if true then flush the configuration
     */
    public void removeServer(String serverName, String saveIt) throws ClusterDaemonException;

    /**
     * Save the configuration.
     * @throws ClusterDaemonException any.
     */
    public void saveConfiguration() throws ClusterDaemonException;

    /**
     * Remove this server from cluster daemon control.
     * @param serverName the server to remove
     */
    public void removeServer(String serverName) throws ClusterDaemonException;

    /**
     * Start a JOnAS instance.
     * @param name instance name
     * @param prm extra parameters
     * @throws ClusterDaemonException if an error occurs
     */
    void startJOnAS(String name, String prm) throws ClusterDaemonException;

    /**
     * Stop a JOnAS instance.
     * @param name instance name
     * @throws ClusterDaemonException if an error occurs
     */
    void stopJOnAS(String name) throws ClusterDaemonException;

    /**
     * Halt a JOnAS instance.
     * @param name instance name
     * @throws ClusterDaemonException if an error occurs
     */
    void haltJOnAS(String name) throws ClusterDaemonException;

    /**
     * Ping a JOnAS instance (MBean interface).
     * @param name instance name
     * @return exit code of the ping (0 ok, 1 ko)
     * @throws ClusterDaemonException if an error occurs
     */
    int pingJOnAS(String name) throws ClusterDaemonException;

    /**
     * Start all the JOnAS instances configured with auto-reboot.
     * @param domainName domain name
     * @param prm extra parameters
     * @return the nodes list with an indicator started/starting failed
     */
    String startAllJOnAS(String domainName, String prm) throws ClusterDaemonException;

    /**
     * Stop all the JOnAS instances.
     * @return the nodes list with an indicator stopped/stopping failed
     * @throws ClusterDaemonException any.
     */
    String stopAllJOnAS() throws ClusterDaemonException;

    /**
     * Stop the cluster daemon instance.
     * @throws ClusterDaemonException any.
     */
    void stopClusterDaemon() throws ClusterDaemonException;

    /**
     * Get jmx url of the cluster daemon.
     * @return clusterd jmx url
     */
    String getJmxUrl() throws ClusterDaemonException;

    /**
     * Get available processors of the OS.
     * @return Operating system processors number
     */
    public String getOperatingSystemAvailableProcessors() throws ClusterDaemonException;

    /**
     * Get OS name.
     * @return OS name
     * @throws ClusterDaemonException any.
     */
    public String getOperatingSystemName() throws ClusterDaemonException;

    /**
     * get OS architecture.
     * @return Os architecture
     * @throws ClusterDaemonException any.
     */
    public String getOperatingSystemArch() throws ClusterDaemonException;

    /**
     * Get OS version.
     * @return OS version
     * @throws ClusterDaemonException any.
     */
    public String getOperatingSystemVersion() throws ClusterDaemonException;

    /**
     * Get spec. vendor.
     * @return Spec vendor
     * @throws ClusterDaemonException any.
     */
    public String getRunTimeSpecVendor() throws ClusterDaemonException;

    /**
     * Get runtime spec. version.
     * @return Spec version
     * @throws ClusterDaemonException any.
     */
    public String getRunTimeSpecVersion() throws ClusterDaemonException;

    /**
     * Get runtime vm name.
     * @return the Vm name
     * @throws ClusterDaemonException any.
     */
    public String getRunTimeVmName() throws ClusterDaemonException;

    /**
     * Get runtime vendor.
     * @return Vm vendor
     * @throws ClusterDaemonException any.
     */
    public String getRunTimeVmVendor() throws ClusterDaemonException;

    /**
     * Get runtime vm version.
     * @return Vm version
     * @throws ClusterDaemonException any.
     */
    public String getRunTimeVmVersion() throws ClusterDaemonException;

    /**
     * Get dynamic host attributes.
     * @return hashtable with dynamic attributes keys and values.
     * @throws ClusterDaemonException any.
     */
    public Hashtable<String, String> getDynamicHostAttributes() throws ClusterDaemonException;

    /**
     * Retrieve the state of the given controlled server.
     * @param srvName server's name.
     * @return true if the server is running
     * @throws ClusterDaemonException any.
     */
    public boolean checkServerState(String srvName) throws ClusterDaemonException;

    /**
     * (this method is not a duplicate of checkServerState -> checkServerState can be run only in isLooselyCoupled mode
     * and only if a process associated to a server has been registered before)
     * @param serverName The name of a server
     * @return true if the server is running. Otherwise, false
     */
    public boolean isServerRunning(final String serverName);

    /**
     * Get remote Vm used Memory.
     * @return the value of current used memory
     * @throws ClusterDaemonException any.
     */

    public String getVmCurrentUsedMemory() throws ClusterDaemonException;

    /**
     * Getting remote Vm Total Memory.
     * @return the value of Vm Total memory
     * @throws ClusterDaemonException any.
     */
    public String getVmTotalMemory() throws ClusterDaemonException;

    /**
     * Getting remote Vm's Current used Heap memory.
     * @return the value of Vm's Current used Heap memory
     * @throws ClusterDaemonException any.
     */
    public String getVmCurrentUsedHeapMemory() throws ClusterDaemonException;

    /**
     * Getting remote Vm's Current used non Heap memory.
     * @return the value of Vm's Current used non Heap memory
     * @throws ClusterDaemonException any.
     */
    public String getVmCurrentUsedNonHeapMemory() throws ClusterDaemonException;

    /**
     * Getting Operating system Current used space.
     * @return the value of Operating system Current used space
     * @throws ClusterDaemonException any.
     */
    public String getOsCurrentUsedSpace() throws ClusterDaemonException;

    /**
     * Getting Operating system Current used space.
     * @return the value of Operating system Total space
     * @throws ClusterDaemonException any.
     */
    public String getOsTotalSpace() throws ClusterDaemonException;

    /**
     * Generate the JOnAS report of the given server
     * @param name The server name
     * @return the path to the report
     */
    String generateReport(String name) throws ClusterDaemonException;

}
