package org.ow2.jonas.cluster.daemon;

import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

import java.io.BufferedWriter;
import java.io.FileWriter;
/**
 * Monitor is a tool that collects node's performance information regularly.
 * if needed (only supporting CPU and memory information now).
 *
 * @author zhengzz
 */
public class MonitorThread extends Thread {

    private static final int FACTOR = 10;

    /**
     * Logger to use.
     */
    private Logger logger = null;

    private int cpurate;

    private long totalM, availM, percentM;

    private BufferedWriter out;

    private int count = 0;

    /**
     * Initializes log monitor.
     *
     * @param logger
     */
    public MonitorThread(final Logger logger) {
        this.logger = logger;
        try {
            out = new BufferedWriter(new FileWriter("monitor.txt", true));
        } catch (Exception e) {
            this.logger.log(BasicLevel.ERROR, "Fail to new FileWrite from monitor.txt" + e);
        }
    }

    /*
    * (non-Javadoc)
    * @see java.lang.Thread#run()
    */
    @Override
    @SuppressWarnings("static-access")
    public void run() {
        this.logger.log(BasicLevel.INFO, "Start monitoring thread....");
        while (true) {
            try {
                Thread.currentThread().sleep(FACTOR * ClusterDaemon.SLEEP_TIME / 2);
                count = count + FACTOR;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}