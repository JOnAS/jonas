/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.cluster.daemon;

import javax.management.MBeanServerConnection;
import javax.management.ObjectInstance;

import org.ow2.jonas.cluster.daemon.mbean.JMXRemoteHelper;

/**
 * This class implements a JMX client to administer the cluster daemon.
 *
 * @author Benoit Pelletier
 * @author S. Ali Tokmen
 */
public class ClusterDaemonAdmin {
    /**
     * Connection to the MBean server
     */
    private static MBeanServerConnection cnx = null;

    /**
     * Object Instance
     */
    private static ObjectInstance oi = null;

    /**
     * STOP command
     */
    private static final int STOP_CMD = 0;

    /**
     * clusterd.xml filename (by default $JONAS_ROOT/conf/clusterd.xml)
     */
    private static String confFile = null;

    /**
     * User name to use for JMX connections.
     */
    private static String username = null;

    /**
     * Password to use for JMX connections.
     */
    private static String password = null;

    /**
     * Private constructor
     */
    private ClusterDaemonAdmin() {

    }

    /**
     * Init MBean server connection
     */
    private static void initJmxCnt() {

        try {
            cnx = JMXRemoteHelper.connect(
                    ClusterDaemonTools.getJmxUrl(ClusterDaemonTools.getCurrentConfiguration().getClusterDaemon().getName()),
                    username, password);
            oi = JMXRemoteHelper.getInstance(cnx, "*:type=ClusterDaemon");
        } catch (Exception e) {
            System.err.println("Cannot init MBean server connection : " + e);
            System.exit(2);
        }

    }

    /**
     * Usage
     */
    private static void usage() {
        System.err.println("usage : jclusterd <options>");
        System.err.println("list of available options:");
        System.err.println("    start : start the cluster daemon.");
        System.err.println("    stop  : stop the cluster daemon.");
    }


    /**
     * Main method
     * @param args arguments
     */
    public static void main(String[] args)  {

        int cmd = -1;


        // Get command args
        for (int argn = 0; argn < args.length; argn++) {
            String arg = args[argn];

            if (arg.equals("-stop")) {
                cmd = STOP_CMD;
            }

            if (arg.equals("-confFile")) {
                confFile = args[++argn];
                continue;
            }

            if (arg.equals("-username")) {
                username = args[++argn];
                continue;
            }

            if (arg.equals("-password")) {
                password = args[++argn];
                continue;
            }

            // load the clusterd.xml file
            try {
                ClusterDaemonTools.loadClusterDaemonConfiguration(confFile);
            } catch (Exception e) {
                System.err.println("Error during the loading of the configuration file : " + e);
                System.exit(2);
            }

            // init the jmx connection with the cluster daemon
            initJmxCnt();

            // execute the command
            switch (cmd) {
                case STOP_CMD:
                    try {
                        // call the stop() operation
                        Object[] params = new Object[] {};
                        String[] sig = new String[] {};
                        cnx.invoke(oi.getObjectName(), "stopClusterDaemon", params, sig);
                        System.out.println("Cluster daemon stopped");
                    } catch (Exception e) {
                        System.err.println("MBean invocation exception : " + e);
                        System.exit(2);
                    }
                    break;
                default:
                    System.err.println("Bad cmd: " + cmd);
                    usage();
                    System.exit(2);
                    break;
            }
        }
    }
}