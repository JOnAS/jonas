@Echo Off
Rem ---------------------------------------------------------------------------
Rem JOnAS: Java(TM) Open Application Server
Rem Copyright (C) 1999-2009 Bull S.A.S.
Rem Contact: jonas-team@ow2.org
Rem
Rem This library is free software; you can redistribute it and/or
Rem modify it under the terms of the GNU Lesser General Public
Rem License as published by the Free Software Foundation; either
Rem version 2.1 of the License, or any later version.
Rem
Rem This library is distributed in the hope that it will be useful,
Rem but WITHOUT ANY WARRANTY; without even the implied warranty of
Rem MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Rem Lesser General Public License for more details.
Rem
Rem You should have received a copy of the GNU Lesser General Public
Rem License along with this library; if not, write to the Free Software
Rem Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
Rem USA
Rem
Rem Initial developer(s): Benoit Pelletier
Rem ---------------------------------------------------------------------------
Rem $Id$
Rem ---------------------------------------------------------------------------

Rem Keep variables local to this script
setlocal ENABLEDELAYEDEXPANSION

Rem  Check for JONAS_ROOT, if not set set it to the default
if ["%JONAS_ROOT%"]==[""] Goto SetJONAS_ROOT
echo JONAS_ROOT is set to %JONAS_ROOT%
Goto ExecuteBatch

Rem %~dp0 is the directory in which this batch file is,
Rem therefore JONAS_ROOT\bin in our case.
:SetJONAS_ROOT
pushd "%~dp0"
cd ..
@set JONAS_ROOT=%CD%
@echo Setting JONAS_ROOT to %JONAS_ROOT%
popd

:ExecuteBatch
Rem ---------------------------------------------
Rem set environment
Rem ---------------------------------------------
if ["%JONAS_ROOT%"]==[""] goto setroot
call "%JONAS_ROOT%\bin\setenv.bat"
set JONAS_LIB=%JONAS_ROOT%\lib


Rem ---------------------------------------------
Rem set JAVA_OPTS
Rem ---------------------------------------------
if ["%JAVA_HOME%"]==[""] goto setjava

Rem JONAS_OPTS may be already partially initialized
set JONAS_OPTS=%JONAS_OPTS% -Djonas.root="%JONAS_ROOT%"
set JONAS_OPTS=%JONAS_OPTS% -Djonas.root="%JONAS_ROOT%"
set JONAS_OPTS=%JONAS_OPTS% -Djonas.base="%JONAS_BASE%"
set JONAS_OPTS=%JONAS_OPTS% -Djava.security.policy="%JONAS_ROOT%\conf\java.policy"
set JONAS_OPTS=%JONAS_OPTS% -Djonas.classpath="%XTRA_CLASSPATH%"
set JONAS_OPTS=%JONAS_OPTS% -Djonas.default.classloader=true
set JONAS_OPTS=%JONAS_OPTS% -Dorg.omg.CORBA.ORBClass=org.jacorb.orb.ORB
set JONAS_OPTS=%JONAS_OPTS% -Dorg.omg.CORBA.ORBSingletonClass=org.jacorb.orb.ORBSingleton
set JONAS_OPTS=%JONAS_OPTS% -Dorg.omg.PortableInterceptor.ORBInitializerClass.standard_init=org.jacorb.orb.standardInterceptors.IORInterceptorInitializer
set JONAS_OPTS=%JONAS_OPTS% -Djavax.rmi.CORBA.PortableRemoteObjectClass=org.ow2.carol.rmi.multi.MultiPRODelegate
set JONAS_OPTS=%JONAS_OPTS% -Djava.naming.factory.initial=org.ow2.carol.jndi.spi.MultiOrbInitialContextFactory
set JONAS_OPTS=%JONAS_OPTS% -Djavax.rmi.CORBA.UtilClass=org.ow2.carol.util.delegate.UtilDelegateImpl
set JONAS_OPTS=%JONAS_OPTS% -Djava.security.auth.login.config="%JONAS_BASE%\conf\jaas.config"
set JONAS_OPTS=%JONAS_OPTS% -Djava.endorsed.dirs="%JONAS_ROOT%\lib\endorsed"

Rem ---------------------------------------------
Rem Get args
Rem ---------------------------------------------
set ARGS=
set MODE=
set USERNAME=
set PASSWORD=

:loop_on_args
Set VALUE=%~1
if [%1]==[]    goto usage
if [%1]==[start]    goto start_arg
if [%1]==[stop]     goto stop_arg
if [%1]==[-cp]      goto cp_arg
if [%1]==[-debug]   goto debug_arg
if [%1]==[-username]    goto username_arg
if [%1]==[-password]    goto password_arg
if %VALUE:~0,2%==-D goto java_opts
set ARGS=%ARGS% %1
goto next_arg

Rem Add -D System Properties
:java_opts
Set PROP=%~1
shift
Set PROP=%PROP%=%1
Set JONAS_OPTS=%JONAS_OPTS% %PROP%
goto next_arg


:start_arg
set MODE=start
set CLASS_TO_RUN=org.ow2.jonas.cluster.daemon.ClusterDaemon
echo JONAS_BASE is set to %JONAS_BASE%
goto next_arg

:stop_arg
set MODE=stop
set CLASS_TO_RUN=org.ow2.jonas.cluster.daemon.ClusterDaemonAdmin -stop
goto next_arg

:cp_arg
shift
set JONAS_CLASSPATH=%JONAS_CLASSPATH%;%~1
goto next_arg

:debug_arg
shift
if not [%1]==[-p] goto debug_usage
shift
set JONAS_DEBUG_PORT=%1
set JONAS_DEBUG_SUSPEND=n
if not [%2]==[-s] goto set_debug_opts
shift
shift
set JONAS_DEBUG_SUSPEND=%1

:set_debug_opts
echo JOnAS Debug Info :
echo  listening on port : %JONAS_DEBUG_PORT%
echo  suspend mode : %JONAS_DEBUG_SUSPEND%
set JONAS_DEBUG_OPTS=-Xdebug -Xnoagent -Xrunjdwp:transport=dt_socket,server=y,address=%JONAS_DEBUG_PORT%,suspend=%JONAS_DEBUG_SUSPEND%
goto next_arg

:username_arg
shift
set USERNAME=-username %1
goto next_arg

:password_arg
shift
set PASSWORD=-password %1
goto next_arg

:next_arg
shift
if not [%1]==[] goto loop_on_args

if [%MODE%]==[] goto no_mode


Rem ---------------------------------------------
Rem Set tomcat/jetty base directory
Rem ---------------------------------------------
set TOMCAT_OPTS=
set JETTY_OPTS=
if not ["%TOMCAT_BASE%"]==[""] set TOMCAT_OPTS=%TOMCAT_OPTS% -Dtomcat.base="%TOMCAT_BASE%"
if not ["%CATALINA_HOME%"]==[""] set TOMCAT_OPTS=%TOMCAT_OPTS% -Dcatalina.home="%CATALINA_HOME%"
if not ["%CATALINA_BASE%"]==[""] set TOMCAT_OPTS=%TOMCAT_OPTS% -Dcatalina.base="%CATALINA_BASE%"
if not ["%JETTY_HOME%"]==[""] set JETTY_OPTS=-Djetty.home="%JETTY_HOME%"

Rem ---------------------------------------------
Rem include jonas classes
Rem ---------------------------------------------
Set JONAS_CLASSPATH=%JONAS_ROOT%\lib\bootstrap\client-bootstrap.jar;%JONAS_ROOT%\lib\jonas-cluster-daemon.jar;%JONAS_ROOT%\lib\client.jar;%JONAS_CLASSPATH%

Rem ---------------------------------------------
Rem Run java command
Rem ---------------------------------------------
set BOOT=org.ow2.jonas.client.boot.Bootstrap
start /B "jclusterd" %JAVA% -cp "%JONAS_CLASSPATH%" %JAVA_OPTS% %JONAS_DEBUG_OPTS% %JONAS_OPTS% %TOMCAT_OPTS% %JETTY_OPTS% %BOOT% %CLASS_TO_RUN% %USERNAME% %PASSWORD%

goto :EOF

:setjava
echo JAVA_HOME not set.
goto :EOF

:setroot
echo JONAS_ROOT not set.
goto :EOF

:debug_usage
echo -debug option parameters are : "-debug -p <debug-port> [-s <suspend:y/n>]"
goto :EOF

:no_mode
echo "No startup mode specified, specify start | stop | admin | version | check"
goto usage

:usage
echo "jclusterd start | stop [options]"
echo "Debug mode : jclusterd start -debug -p <debug-port> [-s <suspend:y/n>]"
goto :EOF

