/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.service.manager;

import java.util.ArrayList;
import java.util.List;

import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;

/**
 * This class provides facilities for managing the JOnAS services implemented as OSGI services.
 * @author Adriana Danes
 */
public final class ServiceUtil {

    /**
     * Private constructor.
     */
    private ServiceUtil() {
        // Do nothing
    }

    /**
     * OSGI service property defined for all the JOnAS services, giving the service name for JOnAS management support.
     */
    public static final String SERVICE_NAME_PROP = "jonas.service";

    /**
     * If the given ServiceReference has the "jonas.service" property set, returns its value which represents the JOnAS service
     * name.
     * @param serviceReference the ServiceReference
     * @return "jonas.service" service property value if set, null otherwise.
     */
    public static String getJOnASService(final ServiceReference serviceReference) {
        return (String) serviceReference.getProperty(SERVICE_NAME_PROP);
    }

    /**
     * Returns the names of the JOnAS services that are running on the OSGi platform.
     * @param bc The BundleContext
     * @return The running service names
     * @throws org.osgi.framework.InvalidSyntaxException Cannot get the OSGi services
     */
    public static List<String> getRunningServices(final BundleContext bc) throws InvalidSyntaxException {
        ServiceReference[] serviceReferences = bc.getAllServiceReferences(null, "(" + SERVICE_NAME_PROP + "=*)");
        List<String> jonasServicesList = new ArrayList<String>();
        for (ServiceReference serviceReference : serviceReferences) {
            String name = getJOnASService(serviceReference);
            if (name != null) {
                jonasServicesList.add(name);
            }
        }
        return jonasServicesList;
    }
}
