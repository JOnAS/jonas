/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.service.manager;

import org.ow2.jonas.lib.management.javaee.J2EEServiceState;

/**
 * Service item class.
 * @author Adriana Danes
 */
public class ServiceItem  {
    public enum ServiceLevel {
        MANDATORY (0), REQUIRED (1), OPTIONAL (2);

        private int value;

        ServiceLevel(final int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    private String name;
    private J2EEServiceState state;
    private String description;
    private ServiceLevel serviceLevel;

    public ServiceItem() {
        serviceLevel = ServiceLevel.OPTIONAL;
    }
    public String getName() {
        return name;
    }
    public void setName(final String name) {
        this.name = name;
    }
    public J2EEServiceState getState() {
        return state;
    }
    public void setState(final J2EEServiceState state) {
        this.state = state;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(final String description) {
        this.description = description;
    }
    public ServiceLevel getServiceLevel() {
        return serviceLevel;
    }
    public void setServiceLevel(final ServiceLevel serviceLevel) {
        this.serviceLevel = serviceLevel;
    }

    @Override
    public String toString() {
        return getName() + "|" + getServiceLevel();
    }

    @Override
    public boolean equals(final Object object) {
        if (!(object instanceof ServiceItem)) {
            return false;
        }
        return getName().equals(((ServiceItem) object).getName());
    }
}
