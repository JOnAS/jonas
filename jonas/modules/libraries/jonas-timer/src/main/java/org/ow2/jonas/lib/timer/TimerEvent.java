/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.lib.timer;

import org.objectweb.util.monolog.api.BasicLevel;

public class TimerEvent {

    private TimerEventListener listener = null;
    private Object arg = null;
    private long remaining;     // millisec
    private long startvalue;    // millisec
    private long createtime;
    private boolean permanent = false;
    private boolean stopped = false;

    /**
     * Constructor
     * @param l Object that will be notified when the timer expire.
     * @param timeout nb of milliseconds before the timer expires.
     * @param a info passed with the timer
     * @param p true if the timer is permanent.
     */
    public TimerEvent(TimerEventListener l, long timeout, Object a, boolean p) {
        if (TraceTimer.isDebug())
            TraceTimer.logger.log(BasicLevel.DEBUG,"listener = "+l+",timeout = "+timeout+" ,object = "+a+" ,permanent = "+p+")");
        listener = l;
        remaining = timeout;
        startvalue = timeout;
        createtime = System.currentTimeMillis();
        arg = a;
        permanent = p;
    }


    /**
     * Update timer every second. Used by clock.
     * - this must be called with the timerList monitor.
     * @param ms clock period
     */
    public long update() {
        remaining = startvalue + createtime - System.currentTimeMillis();
        if (TraceTimer.isDebug() && remaining <= 10) {
            TraceTimer.logger.log(BasicLevel.DEBUG,"listener = "+listener+" remaining = "+remaining);
        }
        return remaining;
    }

    /**
     * Restart timer to its initial value
     */
    public long restart() {
        stopped = false;
        // remaining should be < 0 here.
        createtime = System.currentTimeMillis() + remaining;
        remaining += startvalue;
        if (TraceTimer.isDebug())
            TraceTimer.logger.log(BasicLevel.DEBUG,"listener = "+listener+" remaining = "+remaining);
        return remaining;
    }

    /**
     * Process the Timer
     */
    public void process() {
        if (listener != null) {
            if (TraceTimer.isDebug())
                TraceTimer.logger.log(BasicLevel.DEBUG,"listener = "+listener+" remaining = "+remaining);
            listener.timeoutExpired(arg);
        }
    }

    /**
     * Change the Timer value
     * @param timeout in milliseconds
     */
    public void change(long timeout, Object a) {
        if (TraceTimer.isDebug())
            TraceTimer.logger.log(BasicLevel.DEBUG,"listener = "+listener+" timeout = "+timeout+",object = "+a);
        stopped = false;
        startvalue = timeout;
        remaining = startvalue;
        arg = a;
    }

    /**
     * Unvalidate the timer. It will be removed by the timer manager.
     */
    public void unset() {
        if (TraceTimer.isDebug())
            TraceTimer.logger.log(BasicLevel.DEBUG,"listener = "+listener);
        // the timerlist is not locked.
        remaining = 100;
        arg = null;
        listener = null;
        permanent = false;
        stopped = false;
    }

    /**
     * stop the timer, but keep it for further reuse (See change())
     */
    public void stop() {
        if (TraceTimer.isDebug())
            TraceTimer.logger.log(BasicLevel.DEBUG,"listener = "+listener);
        // the timerlist is not locked.
        remaining = 1000000;
        stopped = true;
    }

    /**
     * Is this timer valid ?
     */
    public boolean valid() {
        return (listener != null);
    }

    /**
     * Is this timer permanent ?
     */
    public boolean ispermanent() {
        return permanent;
    }

    /**
     * Is this timer stopped ?
     */
    public boolean isStopped() {
        return stopped;
    }

    /**
     * @return remaining time in millisec
     */
    public long getRemaining() {
        return remaining;
    }
}

