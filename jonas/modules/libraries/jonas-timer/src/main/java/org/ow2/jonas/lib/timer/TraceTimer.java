/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.timer;

import org.objectweb.util.monolog.api.LoggerFactory;
import org.objectweb.util.monolog.api.Logger;
import org.objectweb.util.monolog.api.BasicLevel;

/**
 * Traces for module timer
 * @author Sebastien Chassande-Barrioz sebastien.chassande@inrialpes.fr
 */
public class TraceTimer {

    /**
     * Default private Constructor for Utility classes.
     */
    private TraceTimer() { }

    /**
     * Timer Logger.
     */
    public static Logger logger = null;

    /**
     * Configure the Timer logger.
     * @param lf {@link org.objectweb.util.monolog.api.LoggerFactory} used for configuration
     */
    public static void configure(LoggerFactory lf) {
        logger = lf.getLogger("org.ow2.jonas.lib.timer");
    }

    /**
     * @return Returns <code>true</code> if debug enabled on Timer logger.
     */
    public static boolean isDebug() {
        return (logger != null) && logger.isLoggable(BasicLevel.DEBUG);
    }
}
