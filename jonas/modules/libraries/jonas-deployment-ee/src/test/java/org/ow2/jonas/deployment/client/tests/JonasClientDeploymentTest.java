/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Philippe Coq
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.client.tests;

import java.io.Reader;

import org.ow2.jonas.deployment.client.lib.ClientDeploymentDescManager;
import org.ow2.jonas.deployment.client.xml.JonasClient;
import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.tests.AbsDeploymentTest;




/**
 * Defines a class for Client deployment tests
 * @author Florent Benoit
 * @author Philippe Coq
 */
public class JonasClientDeploymentTest extends AbsDeploymentTest {


    /**
     * 
     */
    public AbsElement getTopLevelElement() throws Exception {
        JonasClient jonasclient = new JonasClient();
        return jonasclient;
    }

    /**
     * 
     */
    public String parse(Reader reader, String name, boolean validation) throws Exception {
        ClientDeploymentDescManager.setParsingWithValidation(validation);
        String xmlParsed = ClientDeploymentDescManager.loadJonasClient(reader, "test").toXML();
        return xmlParsed;
    }


}
