/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.ejb.tests;

import java.io.Reader;

import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.ejb.lib.EjbDeploymentDescManager;
import org.ow2.jonas.deployment.ejb.wrapper.EjbManagerWrapper;
import org.ow2.jonas.deployment.ejb.xml.EjbJar;
import org.ow2.jonas.deployment.tests.AbsDeploymentTest;


/**
 * Defines a class for Ejb deployment tests
 * @author Philippe Coq
 */
public class EJBDeploymentTest extends AbsDeploymentTest {

    /**
     * 
     */

    public AbsElement getTopLevelElement() throws Exception {
        EjbJar ejbApp = new EjbJar();
        return ejbApp;
    }

    /**
     * 
     */

    public String parse(Reader reader, String name, boolean validation) throws Exception {
        EjbDeploymentDescManager.getInstance().setParsingWithValidation(validation);
        String xmlParsed = EjbDeploymentDescManager.getInstance().loadEjbJar(reader, "test").toXML();
        return xmlParsed;
    }



}
