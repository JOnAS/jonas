/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Helene Joanin
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.ws.tests;

import java.io.StringReader;
import java.io.Reader;

import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.tests.AbsDeploymentTest;
import org.ow2.jonas.deployment.ws.MappingFile;
import org.ow2.jonas.deployment.ws.lib.MappingFileManager;
import org.ow2.jonas.deployment.ws.lib.WSDeploymentDescManager;
import org.ow2.jonas.deployment.ws.xml.JavaWsdlMapping;
import org.ow2.jonas.deployment.ws.xml.Webservices;


/**
 * Defines a class for Web Services deployment tests
 * @author Helene Joanin
 * @author Philipe Coq
 */

public class WsDeploymentTest extends AbsDeploymentTest {

   /**
     * 
     */

    public AbsElement getTopLevelElement() throws Exception {
        Webservices ws = new Webservices();
        return ws;
    }

    /**
     * 
     */

    public String parse(Reader reader, String name, boolean validation) throws Exception {
        WSDeploymentDescManager.setParsingWithValidation(validation);
        String xmlParsed = WSDeploymentDescManager.loadWebservices(reader, "test").toXML();
        return xmlParsed;
    }


 

    /**
     * Defines the function for the jaxrpc-mapping test
     * @param random use random or not to fill elements
     * @throws Exception if the test failed
     */
    public void startJaxrpcMappingTest(boolean random) throws Exception {
        JavaWsdlMapping jmp = new JavaWsdlMapping();
        fill(jmp, random);
        String xmlOriginal = jmp.toXML();
        MappingFile mapFile = MappingFileManager.getInstance(new StringReader(xmlOriginal), "test", false);
        String xmlParsed = mapFile.getXmlJavaWsdlMapping().toXML();
        checkDiff(xmlOriginal, xmlParsed);
    }


    /**
     * Gets the xml after digester parsing
     * @throws Exception if the parsing fail
     */
    public void parseJaxrcpMappingElement() throws Exception {
        JavaWsdlMapping jmp = new JavaWsdlMapping();
        fill(jmp, false);
        System.out.println("Parsing xml :");
        System.out.println(jmp);
        String xmlOriginal = jmp.toXML();
        MappingFile mapFile = MappingFileManager.getInstance(new StringReader(xmlOriginal), "test", false);
        String xmlParsed = mapFile.getXmlJavaWsdlMapping().toXML();
        System.out.println("Result = ");
        System.out.println(xmlParsed);
    }

}
