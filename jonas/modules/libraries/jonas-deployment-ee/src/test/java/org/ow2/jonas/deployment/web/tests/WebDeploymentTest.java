/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.web.tests;

import java.io.Reader;

import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.tests.AbsDeploymentTest;
import org.ow2.jonas.deployment.web.lib.WebDeploymentDescManager;
import org.ow2.jonas.deployment.web.xml.WebApp;


/**
 * Defines a class for Web deployment tests
 * @author Florent Benoit
 * @author Philippe Coq
 */
public class WebDeploymentTest extends AbsDeploymentTest {

    /**
     * @return the top level element
     * @throws Exception if the top level element can't be returned
     */
    public AbsElement getTopLevelElement() throws Exception {
        WebApp webApp = new WebApp();
        return webApp;
    }

    /**
     * Parse the specific reader
     * @param reader contains the stream of the file
     * @param name the name
     * @param validation true or false for enable or disable validation
     * @return the parsed string
     * @throws Exception if the parsing can't be done
     * @see org.ow2.jonas.deployment.tests.AbsDeploymentTest#parse(java.io.Reader,
     *      java.lang.String, boolean)
     */
    public String parse(Reader reader, String name, boolean validation) throws Exception {
        WebDeploymentDescManager.setParsingWithValidation(validation);
        String xmlParsed = WebDeploymentDescManager.loadWebApp(reader, "test").toXML();
        return xmlParsed;
    }

}