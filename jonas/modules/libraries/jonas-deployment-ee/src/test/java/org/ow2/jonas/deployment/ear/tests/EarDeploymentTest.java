/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Helene Joanin
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.ear.tests;


import java.io.Reader;

import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.ear.lib.EarDeploymentDescManager;
import org.ow2.jonas.deployment.ear.xml.Application;
import org.ow2.jonas.deployment.tests.AbsDeploymentTest;


/**
 * Defines a class for Ear deployment tests (application.xml)
 * @author Helene Joanin
 * @author Philippe Coq
 */
public class EarDeploymentTest extends AbsDeploymentTest {

    /**
     * @return Returns the Application XML Element
     *
     * @throws Exception never thrown
     */
    public AbsElement getTopLevelElement() throws Exception {
        Application app = new Application();
        return app;
    }

    /**
     * Returns the XML document as a String.
     *
     * @param reader Reader of an application.xml file
     * @param name not used
     * @param validation true/false
     *
     * @return Returns the XML document as a String.
     *
     * @throws Exception when parsing fails
     */
    public String parse(Reader reader, String name, boolean validation) throws Exception {
        EarDeploymentDescManager.setParsingWithValidation(validation);
        String xmlParsed = EarDeploymentDescManager.loadApplication(reader, "test").toXML();
        return xmlParsed;
    }


}
