<?xml version="1.0" encoding="UTF-8"?>
<xsd:schema targetNamespace="http://www.objectweb.org/jonas/ns"
            xmlns="http://www.w3.org/2001/XMLSchema"
            xmlns:j2ee="http://java.sun.com/xml/ns/j2ee"
            xmlns:xsd="http://www.w3.org/2001/XMLSchema"
            xmlns:jonas="http://www.objectweb.org/jonas/ns"
            elementFormDefault="qualified"
            attributeFormDefault="unqualified"
            version="4.2">
  <xsd:annotation>
    <xsd:documentation>
      @(#)$Name$ $Date: 2007-08-08 11:56:58 +0200 (mer 08 aoû 2007) $
    </xsd:documentation>
  </xsd:annotation>
  <xsd:annotation>
    <xsd:documentation>
      <![CDATA[
      JOnAS: Java(TM) Open Application Server
      Copyright (C) 2004-2005 Bull S.A.
      Contact: jonas-team@objectweb.org

      This library is free software; you can redistribute it and/or
      modify it under the terms of the GNU Lesser General Public
      License as published by the Free Software Foundation; either
      version 2.1 of the License, or any later version.

      This library is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
      Lesser General Public License for more details.

      You should have received a copy of the GNU Lesser General Public
      License along with this library; if not, write to the Free Software
      Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
      USA

      Initial Author : Philippe Coq
      ]]>
    </xsd:documentation>
  </xsd:annotation>

  <xsd:annotation>
    <xsd:documentation>
      <![CDATA[
     This is XML Schema for jonas specific web services 1.1 deployment descriptor information.
     All JOnAS web-services deployment descriptors must indicate
     the jonas-webservices schema by using the Jonas namespace:
         http://www.objectweb.org/jonas/ns
     and by indicating the version of the schema by
     using the version element as shown below:
         <jonas-webservices xmlns="http://www.objectweb.org/jonas/ns"
                            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                            xsi:schemaLocation="http://www.objectweb.org/jonas/ns
                         http://www.objectweb.org/jonas/ns/jonas_j2ee_web_services_4_2.xsd">
         ...
         </jonas-webservices>
     The instance documents may indicate the published version of
     the schema using the xsi:schemaLocation attribute for the
     JOnAS namespace with the following location:
         http://www.objectweb.org/jonas/ns/jonas_j2ee_web_services_4_2.xsd
      ]]>
    </xsd:documentation>
  </xsd:annotation>

  <xsd:include schemaLocation="http://www.objectweb.org/jonas/ns/jonas_j2ee_4_2.xsd" />
  <xsd:import namespace="http://java.sun.com/xml/ns/j2ee"
          schemaLocation="http://www.ibm.com/webservices/xsd/j2ee_web_services_1_1.xsd" />

  <!-- **************************************************** -->

  <xsd:element name="jonas-webservices" type="jonas:jonas-webservicesType">
    <xsd:annotation>
      <xsd:documentation>
  This is the root element of the JOnAS specific web-services deployment descriptor.
      </xsd:documentation>
    </xsd:annotation>
  </xsd:element>


  <!-- **************************************************** -->

  <xsd:complexType name="jonas-webservicesType">
    <xsd:annotation>
      <xsd:documentation>
     The jonas-webservicesType defines the root element of the JOnAS specific
     web services deployment descriptor.
     It contains the JOnAS specific information about all included web services.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element name="war" type="j2ee:pathType" minOccurs="0" maxOccurs="1">
        <xsd:annotation>
          <xsd:documentation> The war element specifies the path whithin the 
            ear of the web-app which dispatch the SOAP requests to the session 
            bean web services. Works only for EJB exposed as web services. It 
            is not used for Webapps alone.</xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="context-root" type="j2ee:string" minOccurs="0" maxOccurs="1">
        <xsd:annotation>
          <xsd:documentation> The context-root element represents the 
            context-root to be used when generating web app with WsGen. Notice 
            that this is not read during installation phase on JOnAS. Works 
            only for EJB exposed as web services. It is not used for Webapps 
            alone.</xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="jonas-webservice-description" 
        type="jonas:jonas-webservice-descriptionType" minOccurs="0" 
        maxOccurs="unbounded">
      </xsd:element>
    </xsd:sequence>
    <xsd:attribute name="id" type="xsd:ID" />
  </xsd:complexType>

  <!-- **************************************************** -->

  <xsd:complexType name="jonas-webservice-descriptionType">
    <xsd:annotation>
      <xsd:documentation>
     The jonas-webservice-descriptionType defines a group of jonas-port-component
     It contains the JOnAS specific information about port-component of a given webservice-description
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element name="webservice-description-name" type="j2ee:string" />
      <xsd:element name="default-endpoint-uri" type="j2ee:url-patternType" 
        minOccurs="0" maxOccurs="1">
        <xsd:annotation>
          <xsd:documentation> The default-endpoint-uri element represents the 
            last part of the URL where port-components of the service can be 
            accessed. Used if jonas-port-component/endpoint-uri is not set 
            </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="jonas-port-component" 
        type="jonas:jonas-port-componentType" minOccurs="0" 
        maxOccurs="unbounded">
        <xsd:annotation>
          <xsd:documentation> The jonas-port-component element is used to 
            specify the endpoint URI used to access the designed 
            port-component. </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="wsdl-publish-directory" type="j2ee:string"
        minOccurs="0" maxOccurs="1">
        <xsd:annotation>
          <xsd:documentation> The wsdl-publish-directory element is used to 
            tell the container where to publish the WSDL(s) of this webservice. 
            Must be a local directory and be specified as an URL : 
            "file:///pub/wsdl" for example. </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
    </xsd:sequence>
    <xsd:attribute name="id" type="xsd:ID" />
  </xsd:complexType>

  <!-- **************************************************** -->

  <xsd:complexType name="jonas-port-componentType">
    <xsd:annotation>
      <xsd:documentation>
     The jonas-port-componentType defines 
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element name="port-component-name" type="j2ee:string" />
      <xsd:element name="endpoint-uri" type="j2ee:url-patternType" minOccurs="0" maxOccurs="1">
        <xsd:annotation>
          <xsd:documentation> The endpoint-uri element represents the last part 
            of the URL where the given port can be accessed. 
            </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
    </xsd:sequence>
    <xsd:attribute name="id" type="xsd:ID" />
  </xsd:complexType>

</xsd:schema>
