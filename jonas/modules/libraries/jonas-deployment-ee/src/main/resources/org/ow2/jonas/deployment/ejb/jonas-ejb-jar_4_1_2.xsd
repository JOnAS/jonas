<?xml version="1.0" encoding="UTF-8"?>
<xsd:schema targetNamespace="http://www.objectweb.org/jonas/ns"
            xmlns="http://www.w3.org/2001/XMLSchema"
            xmlns:j2ee="http://java.sun.com/xml/ns/j2ee"
            xmlns:xsd="http://www.w3.org/2001/XMLSchema"
            xmlns:jonas="http://www.objectweb.org/jonas/ns"
            elementFormDefault="qualified"
            attributeFormDefault="unqualified"
            version="4.1.2">
  <xsd:annotation>
    <xsd:documentation>
      @(#)jonas-ejb-jar_4_1_2.xsd  14/11/03
    </xsd:documentation>
  </xsd:annotation>
  <xsd:annotation>
    <xsd:documentation>
      <![CDATA[
      JOnAS: Java(TM) Open Application Server
      Copyright (C) 2004-2005 Bull S.A.
      Contact: jonas-team@objectweb.org

      This library is free software; you can redistribute it and/or
      modify it under the terms of the GNU Lesser General Public
      License as published by the Free Software Foundation; either
      version 2.1 of the License, or any later version.

      This library is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
      Lesser General Public License for more details.

      You should have received a copy of the GNU Lesser General Public
      License along with this library; if not, write to the Free Software
      Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
      USA

      Initial Author : Philippe Coq
      ]]>
   </xsd:documentation>
  </xsd:annotation>

  <xsd:annotation>
    <xsd:documentation>
      <![CDATA[
    This is XML Schema for jonas specific EJB 2.1 deployment descriptor information.
    The deployment descriptor must be named "META-INF/jonas-ejb-jar.xml" in
    the EJB's jar file
    All JOnAS EJB deployment descriptors must indicate
    the jonas-ejb-jar schema by using the Jonas namespace:

    http://www.objectweb.org/jonas/ns

    and by indicating the version of the schema by
    using the version element as shown below:

    <jonas-ejb-jar xmlns="http://www.objectweb.org/jonas/ns"
                   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                   xsi:schemaLocation="http://www.objectweb.org/jonas/ns
          http://www.objectweb.org/jonas/ns/jonas-ejb-jar_4_1_2.xsd">
    ...
    </jonas-ejb-jar>

    The instance documents may indicate the published version of
    the schema using the xsi:schemaLocation attribute for the
    Jonas namespace with the following location:

    http://www.objectweb.org/jonas/ns/jonas-ejb-jar_4_1_2.xsd

      ]]>
    </xsd:documentation>
  </xsd:annotation>

  <xsd:annotation>
    <xsd:documentation>
      The import must be done before the include, else
      ejb-jar_2.1.xsd is not resolved
    </xsd:documentation>
  </xsd:annotation>
  <import namespace="http://java.sun.com/xml/ns/j2ee"
          schemaLocation="http://java.sun.com/xml/ns/j2ee/ejb-jar_2_1.xsd"/>

  <xsd:include schemaLocation="jonas_j2ee_4_1_2.xsd" />


  <!-- **************************************************** -->

  <xsd:element name="jonas-ejb-jar" type="jonas:jonas-ejb-jarType">
    <xsd:annotation>
      <xsd:documentation>
        This is the root element of the JOnAS specific EJB deployment descriptor.
      </xsd:documentation>
    </xsd:annotation>
  </xsd:element>

  <!-- **************************************************** -->
  <xsd:complexType name="activation-configType">
    <xsd:annotation>
      <xsd:documentation> The activation-configType defines information about 
        the expected configuration properties of the message-driven bean in its 
        operational environment. This may include information about message 
        acknowledgement, message selector, expected destination type, etc. The 
        configuration information is expressed in terms of name/value 
        configuration properties. The properties that are recognized for a 
        particular message-driven bean are determined by the messaging type. 
        </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element name="description" type="j2ee:descriptionType" minOccurs="0" 
        maxOccurs="unbounded"/>
      <xsd:element name="activation-config-property" 
        type="jonas:activation-config-propertyType" maxOccurs="unbounded"/>
    </xsd:sequence>
    <xsd:attribute name="id" type="xsd:ID"/>
  </xsd:complexType>

  <!-- **************************************************** -->
  <xsd:complexType name="activation-config-propertyType">
    <xsd:annotation>
      <xsd:documentation> The activation-config-propertyType contains a 
        name/value configuration property pair for a message-driven bean. The 
        properties that are recognized for a particular message-driven bean are 
        determined by the messaging type. </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element name="activation-config-property-name" 
        type="j2ee:xsdStringType">
        <xsd:annotation>
          <xsd:documentation> The activation-config-property-name element 
            contains the name for an activation configuration property of a 
            message-driven bean. For JMS message-driven beans, the following 
            property names are recognized: acknowledgeMode, messageSelector, 
            destinationType, subscriptionDurability </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="activation-config-property-value" 
        type="j2ee:xsdStringType">
        <xsd:annotation>
          <xsd:documentation> The activation-config-property-value element 
            contains the value for an activation configuration property of a 
            message-driven bean. </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
    </xsd:sequence>
    <xsd:attribute name="id" type="xsd:ID"/>
  </xsd:complexType>

  <!-- **************************************************** -->
  <xsd:simpleType name="cleanupType">
    <xsd:annotation>
      <xsd:documentation>
        Policy about cleanup at bean loading.
        Possible values are : none, create, removeall, removedata.
        'none' is not implemented, the default value is 'create'.
     </xsd:documentation>
    </xsd:annotation>
    <xsd:restriction base="xsd:string">
      <xsd:enumeration value="none" />
      <xsd:enumeration value="create" />
      <xsd:enumeration value="removeall" />
      <xsd:enumeration value="removedata" />
    </xsd:restriction>
  </xsd:simpleType>

  <!-- **************************************************** -->
  <xsd:complexType name="cmp-field-jdbc-mappingType">
    <xsd:annotation>
      <xsd:documentation>
        The cmp-field-jdbc-mapping element declares
        the mapping of a container-managed field
        of an entity to a column of a relational table.
        It consists of
          - the field's name,
          - the column name of the relational table,
          - and optionaly the associated sql-type (CMP2 only).
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element name="field-name" type="j2ee:string">
        <xsd:annotation>
          <xsd:documentation>
            The field-name element specifies the name
            of a container managed field.
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="jdbc-field-name" type="j2ee:string">
        <xsd:annotation>
          <xsd:documentation>
            The jdbc-field-name element specifies the name
            of a column name in a relational table
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="sql-type" type="j2ee:string" minOccurs="0">
        <xsd:annotation>
          <xsd:documentation>
            The sql-type element specifies the SQL type
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
    </xsd:sequence>
    <xsd:attribute name="id" type="xsd:ID" />
  </xsd:complexType>
  <!-- **************************************************** -->
  <xsd:complexType name="finder-method-jdbc-mappingType">
    <xsd:annotation>
      <xsd:documentation>
           The finder-method-jdbc-mapping element declares the SQL WHERE clause associated to a
        finder method of a container-managed persistence entity.
        It consists of
              - the description of the finder method,
              - the SQL WHERE clause.
        This is used only for entity bean CMP1.
     </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>

      <xsd:element name="jonas-method" type="jonas:jonas-methodType" />
      <xsd:element name="jdbc-where-clause" type="j2ee:string">
        <xsd:annotation>
          <xsd:documentation>
                The jdbc-where-clause element specifies a SQL WHERE clause.
                for CMP1.x only
               </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
    </xsd:sequence>
    <xsd:attribute name="id" type="xsd:ID" />
  </xsd:complexType>

  <!-- **************************************************** -->
  <xsd:complexType name="foreign-key-jdbc-mappingType">
    <xsd:annotation>
      <xsd:documentation>
      The foreign-key-jdbc-mapping element specifies a foreign key column name
      and its associated primary key column name.
     </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element name="foreign-key-jdbc-name" type="j2ee:string">
        <xsd:annotation>
          <xsd:documentation>
      The foreign-key-jdbc-name element specifies a foreign key column name
      of a relational table.
            </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="key-jdbc-name" type="j2ee:string" minOccurs="0">
        <xsd:annotation>
          <xsd:documentation>
      The key-jdbc-name element specifies an optional primary key column name
      of a relational table.
            </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
    </xsd:sequence>
    <xsd:attribute name="id" type="xsd:ID" />
  </xsd:complexType>
  <!-- **************************************************** -->
  <xsd:complexType name="jdbc-mappingType">
    <xsd:annotation>
      <xsd:documentation>
    The jdbc-mapping element declares the mapping of an entity with container-managed
    persistence to the underlying database.
    It consists of
      - the JNDI name of the datasource (CMP only),
      - the name of the relational table (CMP only),
      - an optional tag automatic-pk-field-name to specify the field name of the automatic primary key (CMP only),
      - for each container-managed field, the associated column name in the table (CMP only),
      - for each finder method, the associated SQL WHERE clause (CMP1 only).
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element name="jndi-name" type="j2ee:jndi-nameType">
        <xsd:annotation>
          <xsd:documentation>
            The jndi-name element contains the JNDI name
            of the DataSource.
            </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="jdbc-table-name" type="j2ee:string" minOccurs="0">
        <xsd:annotation>
          <xsd:documentation>
                the name of the relational table
            </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="automatic-pk" type="j2ee:true-falseType" minOccurs="0">
        <xsd:annotation>
          <xsd:documentation>
                    optional flag to use automatic generation of primary key
                </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="automatic-pk-field-name"
                   type="j2ee:string"
                   minOccurs="0">
        <xsd:annotation>
          <xsd:documentation>
                           optional flag to specify the name of automatic cmp field generate for pk auto-generated
                 </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="cmp-field-jdbc-mapping"
                   type="jonas:cmp-field-jdbc-mappingType"
                   minOccurs="0"
                   maxOccurs="unbounded" />
      <xsd:element name="finder-method-jdbc-mapping"
                   type="jonas:finder-method-jdbc-mappingType"
                   minOccurs="0"
                   maxOccurs="unbounded" />
    </xsd:sequence>
    <xsd:attribute name="id" type="xsd:ID" />
  </xsd:complexType>
  <!-- **************************************************** -->

  <xsd:complexType name="jonas-ejb-jarType">
    <xsd:annotation>
      <xsd:documentation>
    The jonas-ejb-jarType  defines the root element of the JOnAS specific
    EJB deployment descriptor.
    It contains the JOnAS specific information about all included
    enterprise beans and optional information for EJB relations
      </xsd:documentation>
    </xsd:annotation>
    <xsd:choice maxOccurs="unbounded">
      <xsd:element name="jonas-session" type="jonas:jonas-session-beanType">
      </xsd:element>
      <xsd:element name="jonas-entity" type="jonas:jonas-entity-beanType">
      </xsd:element>
      <xsd:element name="jonas-message-driven"
                   type="jonas:jonas-message-driven-beanType">
      </xsd:element>
      <xsd:element name="jonas-ejb-relation"
                   type="jonas:jonas-ejb-relation-beanType">
      </xsd:element>
      <xsd:element name="jonas-message-destination"
                   minOccurs="0"
                   maxOccurs="unbounded"
                   type="jonas:jonas-message-destinationType" />

      <xsd:element name="jonas-run-as-mapping" type="jonas:principal-to-role-mappingType" 
                   minOccurs="0" maxOccurs="1"/>
    </xsd:choice>
    <xsd:attribute name="id" type="xsd:ID" />
  </xsd:complexType>

  <!-- **************************************************** -->

  <xsd:complexType name="jonas-ejb-relation-beanType">
    <xsd:annotation>
      <xsd:documentation>
        The jonas-ejb-relation element declares the mapping of a relation
        to the underlying database.
        It consist of
              - the name of the relation specified in the standard
              EJB deployment descriptor,
              - the optional name of the relational joint table,
              - and for the two relationship roles of the relation,
              the mapping of those relationship roles.
     </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element name="ejb-relation-name" type="j2ee:string">
        <xsd:annotation>
          <xsd:documentation>
             the name of the relation specified in
            the standard EJB deployment descriptor,
            </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="jdbc-table-name" type="j2ee:string" minOccurs="0">
        <xsd:annotation>
          <xsd:documentation>
             optional name of the relational joint table
            </xsd:documentation>
        </xsd:annotation>
      </xsd:element>

      <xsd:element name="jonas-ejb-relationship-role"
                   type="jonas:jonas-ejb-relationship-roleType"
                   minOccurs="0"
                   maxOccurs="2" />
    </xsd:sequence>
    <xsd:attribute name="id" type="xsd:ID" />
  </xsd:complexType>

  <!-- **************************************************** -->
  <xsd:complexType name="jonas-ejb-relationship-roleType">
    <xsd:annotation>
      <xsd:documentation>
    The jonas-ejb-relationship-role element declares the mapping of
    a relationship-role of a relation.
    It consist of
        - the name of the relationship-role specified
          in the standard EJB deployment descriptor
        - and the names of the foreign key columns.
    </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element name="ejb-relationship-role-name" type="j2ee:string">
        <xsd:annotation>
          <xsd:documentation>
      The ejb-relationship-role-name element specifies the name
      of an relationship-role (CMP2 only).
            </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:sequence>
        <xsd:element name="foreign-key-jdbc-mapping"
                     type="jonas:foreign-key-jdbc-mappingType"
                     minOccurs="1"
                     maxOccurs="unbounded" />
      </xsd:sequence>
    </xsd:sequence>
    <xsd:attribute name="id" type="xsd:ID" />
  </xsd:complexType>
  <!-- **************************************************** -->

  <xsd:complexType name="jonas-entity-beanType">
    <xsd:annotation>
      <xsd:documentation>
    The jonas-entity-beanType defines the JOnAS specific information for a entity.
    It consists of
          - the enterprise bean's name specified in the standard EJB deployment descriptor,
          - the JNDI name of the enterprise bean's home,
        - the JNDI name of the local enterprise bean's home,
          - JOnAS specific information for bean's EJB references,
          - JOnAS specific information for bean's resource
          manager connection factory references
        - JOnAS specific information for bean's resource
          environment references.
        - JOnAS specific information for bean's web
          service references
          - an optionnal isModified method name
        - an optionnal passivation timeout
        - an optional shared flag
        - an optional max size of instances cache
        - an optional min size for the pool of ready instances
        - an optional policy about cleanup at bean loading
        - an optional lock policy
        - an optional mapping of an entity with container-managed
        persistence to the underlying database

     </xsd:documentation>
    </xsd:annotation>

    <xsd:choice maxOccurs="unbounded">
      <xsd:element name="ejb-name" type="j2ee:ejb-nameType" 
                   minOccurs="1" maxOccurs="1"/>
      <xsd:element name="jndi-name" type="j2ee:jndi-nameType" minOccurs="0" maxOccurs="1"/>
      <xsd:element name="jndi-local-name"
                   type="j2ee:jndi-nameType"
                   minOccurs="0" maxOccurs="1" />
      <xsd:group ref="jonas:referenceGroup" />
      <xsd:element name="run-as" type="jonas:run-asType" 
                   minOccurs="0" maxOccurs="1"/>
      <xsd:element name="is-modified-method-name"
                   type="j2ee:string"
                   minOccurs="0">
        <xsd:annotation>
          <xsd:documentation>
        The is-modified-method-name element specifies the name
        of the is-modified method of a entity.
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="passivation-timeout"
                   type="j2ee:xsdIntegerType"
                   minOccurs="0" maxOccurs="1">
        <xsd:annotation>
          <xsd:documentation>
        specifies the value of timeout in seconds for
        expiration of session instances.
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="shared" type="j2ee:true-falseType" minOccurs="0" maxOccurs="1">
        <xsd:annotation>
          <xsd:documentation>
        The shared element specifies if the bean state can be accessed
        outside JOnAS.
        Possible values are true or false (default is false).
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="max-cache-size"
                   type="j2ee:xsdIntegerType"
                   minOccurs="0" maxOccurs="1">
        <xsd:annotation>
          <xsd:documentation>
        defines the max number of instances that can
        be hold in memory.The default value is infinite.
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="min-pool-size"
                   type="j2ee:xsdIntegerType"
                   minOccurs="0" maxOccurs="1" >
        <xsd:annotation>
          <xsd:documentation>
        Defines the number of instances that will be
        created to populate the pool when the bean
        is loaded for the first time.
        The default value is 0.
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="cleanup" type="jonas:cleanupType" minOccurs="0" maxOccurs="1">
        <xsd:annotation>
          <xsd:documentation>
        The cleanup element specifies the policy about
        cleanup at bean loading.
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="lock-policy"
                   type="jonas:lock-policyType"
                   minOccurs="0" maxOccurs="1">
        <xsd:annotation>
          <xsd:documentation>
        The lock-policy element specifies the lock policy for this entity bean.
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="jdbc-mapping"
                   type="jonas:jdbc-mappingType"
                   minOccurs="0" />
    </xsd:choice>
    <xsd:attribute name="id" type="xsd:ID" />
  </xsd:complexType>


  <!-- **************************************************** -->

  <xsd:complexType name="jonas-message-driven-beanType">
    <xsd:annotation>
      <xsd:documentation>
    The jonas-message-drivenType defines the JOnAS specific information for a message
    driven bean.
    It consists of
          - the enterprise bean's name specified in the standard EJB deployment descriptor,
          - JOnAS specific information about the message-driven destination,
          - JOnAS specific information for bean's EJB references,
          - JOnAS specific information for bean's resource
          manager connection factory references
        - JOnAS specific information for bean's resource
          environment references.
        - JOnAS specific information for bean's web
          service references
        - an optional max size of instances cache
        - an optional min size for the pool of ready instances

     </xsd:documentation>
    </xsd:annotation>

    <xsd:choice maxOccurs="unbounded">
      <xsd:element name="ejb-name" type="j2ee:ejb-nameType" maxOccurs="1" minOccurs="1" />
      <xsd:element name="jonas-message-driven-destination"
                   type="jonas:jonas-message-driven-destinationType"
                   minOccurs="0" maxOccurs="1" />
      <xsd:element name="activation-config"
		   type="jonas:activation-configType"
		   minOccurs="0" maxOccurs="1"/>
      <xsd:group ref="jonas:referenceGroup" />
      <xsd:element name="run-as" type="jonas:run-asType" 
                   minOccurs="0" maxOccurs="1"/>
      <xsd:element name="max-cache-size"
                   type="j2ee:xsdIntegerType"
                   minOccurs="0" maxOccurs="1" >
        <xsd:annotation>
          <xsd:documentation>
        defines the max number of instances that can
        be hold in memory.The default value is infinite.
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="min-pool-size"
                   type="j2ee:xsdIntegerType"
                   minOccurs="0" maxOccurs="1" >
        <xsd:annotation>
          <xsd:documentation>
        Defines the number of instances that will be
        created to populate the pool when the bean
        is loaded for the first time.
        The default value is 0.
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
    </xsd:choice>
    <xsd:attribute name="id" type="xsd:ID" />
  </xsd:complexType>
  <!-- **************************************************** -->

  <xsd:complexType name="jonas-message-driven-destinationType">
    <xsd:annotation>
      <xsd:documentation>
        The jonas-message-driven-destination element declares
        the JOnAS specific information for
        a the message driven bean destination.
        It consists of:
          the JNDI name of the message driven destination.
     </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element name="jndi-name" type="j2ee:jndi-nameType" />
    </xsd:sequence>
    <xsd:attribute name="id" type="xsd:ID" />
  </xsd:complexType>

  <!-- **************************************************** -->
  <xsd:complexType name="jonas-methodType">
    <xsd:annotation>
      <xsd:documentation>
        The jonas-method element is used to denote a method
        of the enterprise bean's home.
        It consists of
          - the method's name,
          - optional method-params elements identify a single method
        among multiple methods with an overloaded method name.
     </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element name="method-name" type="j2ee:string">
        <xsd:annotation>
          <xsd:documentation>
        The method-name element contains a name of an enterprise bean method
        </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="method-params"
                   type="jonas:method-paramsType"
                   minOccurs="0" />
    </xsd:sequence>
    <xsd:attribute name="id" type="xsd:ID" />
  </xsd:complexType>

  <!-- **************************************************** -->

  <xsd:complexType name="jonas-session-beanType">
    <xsd:annotation>
      <xsd:documentation>
    The jonas-session-beanType defines the JOnAS specific information for a session.
    It consists of
          - the enterprise bean's name specified in the standard EJB deployment descriptor,
          - the JNDI name of the enterprise bean's home,
          - the JNDI name of the local enterprise bean's home,
          - JOnAS specific information for bean's EJB references,
          - JOnAS specific information for bean's resource
          manager connection factory references
        - JOnAS specific information for bean's resource
          environment references.
        - JOnAS specific information for bean's web
          service references
          - an optionnal session-timeout.
        - an optional max size of instances cache
        - an optional min size for the pool of ready instances
     </xsd:documentation>
    </xsd:annotation>
    <xsd:choice maxOccurs="unbounded">
      <xsd:element name="ejb-name" type="j2ee:ejb-nameType" minOccurs="1" maxOccurs="1"/>
      <xsd:element name="jndi-name"
                   type="j2ee:jndi-nameType"
                   minOccurs="0" maxOccurs="1"/>
      <xsd:element name="jndi-local-name"
                   type="j2ee:jndi-nameType"
                   minOccurs="0" maxOccurs="1" />
      <xsd:element name="jndi-endpoint-name"
                   type="j2ee:jndi-nameType"
                   minOccurs="0" maxOccurs="1"/>
      <xsd:group ref="jonas:referenceGroup" />
      <xsd:element name="run-as" type="jonas:run-asType" 
                   minOccurs="0" maxOccurs="1"/>
      <xsd:element name="session-timeout"
                   type="j2ee:xsdIntegerType"
                   minOccurs="0" maxOccurs="1">
        <xsd:annotation>
          <xsd:documentation>
        specifies the value of timeout in seconds for
        expiration of session instances.
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="max-cache-size"
                   type="j2ee:xsdIntegerType"
                   minOccurs="0" maxOccurs="1">
        <xsd:annotation>
          <xsd:documentation>
        defines the max number of instances that can
        be hold in memory.The default value is infinite.
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="min-pool-size"
                   type="j2ee:xsdIntegerType"
                   minOccurs="0" maxOccurs="1">
        <xsd:annotation>
          <xsd:documentation>
        Defines the number of instances that will be
        created to populate the pool when the bean
        is loaded for the first time.
        The default value is 0.
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
    </xsd:choice>
  </xsd:complexType>
  <xsd:attribute name="id" type="xsd:ID" />

  <!-- **************************************************** -->
  <xsd:simpleType name="lock-policyType">
    <xsd:annotation>
      <xsd:documentation>
        Policy about isolation locking
        Possible values are : container-serialized, container-read-committed, container-read-uncommitted, database
        the default value is 'container-serialized'.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:restriction base="xsd:string">
      <xsd:enumeration value="container-serialized" />
      <xsd:enumeration value="container-read-committed" />
      <xsd:enumeration value="container-read-uncommitted" />
      <xsd:enumeration value="database" />
    </xsd:restriction>
  </xsd:simpleType>

  <!-- **************************************************** -->
  <xsd:complexType name="method-paramsType">
    <xsd:annotation>
      <xsd:documentation>
  	    The method-paramsType defines a list of the
	    fully-qualified Java type names of the method parameters.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element name="method-param"
		   type="j2ee:java-typeType"
		   minOccurs="0"
		   maxOccurs="unbounded">
	    <xsd:annotation>
	      <xsd:documentation>
	        The method-param element contains a primitive
	        or a fully-qualified Java type name of a method
	        parameter.
	      </xsd:documentation>
	    </xsd:annotation>
      </xsd:element>
    </xsd:sequence>
    <xsd:attribute name="id" type="xsd:ID"/>
  </xsd:complexType>

  <!-- **************************************************** -->
  <xsd:complexType name="run-asType">
    <xsd:annotation>
      <xsd:documentation>

	The run-asType specifies the run-as principal to be
	used for the execution of a component. It is an optional
    element.

      </xsd:documentation>
    </xsd:annotation>

    <xsd:sequence>
      <xsd:element name="description"
		   type="j2ee:descriptionType"
		   minOccurs="0"
		   maxOccurs="unbounded"/>
      <xsd:element name="principal-name"
		   type="j2ee:string"/>
    </xsd:sequence>
    <xsd:attribute name="id" type="xsd:ID"/>
  </xsd:complexType>

  <!-- **************************************************** -->
  <xsd:complexType name="principal-to-role-mappingType">
    <xsd:annotation>
      <xsd:documentation>

	The principal-to-role-mappingType specifies the mapping between
    principal-name and the list of roles for this principal. It is used
    for the mapping of roles for run-as identity.

      </xsd:documentation>
    </xsd:annotation>

    <xsd:sequence>
      <xsd:element name="description"
		   type="j2ee:descriptionType"
		   minOccurs="0"
		   maxOccurs="unbounded"/>
      <xsd:element name="principal-name"
		   type="j2ee:string"/>
      <xsd:element name="role-name"
		   type="j2ee:role-nameType"
		   minOccurs="0"
		   maxOccurs="unbounded"/>
    </xsd:sequence>
    <xsd:attribute name="id" type="xsd:ID"/>
  </xsd:complexType>
</xsd:schema>
