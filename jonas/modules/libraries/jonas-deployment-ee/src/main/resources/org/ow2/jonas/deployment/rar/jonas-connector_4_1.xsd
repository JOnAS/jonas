<?xml version="1.0" encoding="UTF-8"?>
<xsd:schema targetNamespace="http://www.objectweb.org/jonas/ns"
            xmlns="http://www.w3.org/2001/XMLSchema"
            xmlns:j2ee="http://java.sun.com/xml/ns/j2ee"
            xmlns:xsd="http://www.w3.org/2001/XMLSchema"
            xmlns:jonas="http://www.objectweb.org/jonas/ns"
            elementFormDefault="qualified"
            attributeFormDefault="unqualified"
            version="4.1">
  <xsd:annotation>
    <xsd:documentation>
      @(#)jonas-connector_4_1.xsd  14/11/03
    </xsd:documentation>
  </xsd:annotation>
  <xsd:annotation>
    <xsd:documentation>
      <![CDATA[
      JOnAS: Java(TM) Open Application Server
      Copyright (C) 2004-2005 Bull S.A.
      Contact: jonas-team@objectweb.org

      This library is free software; you can redistribute it and/or
      modify it under the terms of the GNU Lesser General Public
      License as published by the Free Software Foundation; either
      version 2.1 of the License, or any later version.

      This library is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
      Lesser General Public License for more details.

      You should have received a copy of the GNU Lesser General Public
      License along with this library; if not, write to the Free Software
      Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
      USA

      Initial Author : Philippe Coq
      ]]>
    </xsd:documentation>
  </xsd:annotation>

  <xsd:annotation>
    <xsd:documentation>
      <![CDATA[
      This is XML Schema for jonas specific connector deployment descriptor information.
      The deployment descriptor must be named "META-INF/jonas-ra.xml" in
      the connector's rar file
      All JOnAS connector deployment descriptors must indicate
      the jonas-connector schema by using the Jonas namespace:

      http://www.objectweb.org/jonas/ns

      and by indicating the version of the schema by:

      <jonas-connector xmlns="http://www.objectweb.org/jonas/ns"
                       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                       xsi:schemaLocation="http://www.objectweb.org/jonas/ns
            http://www.objectweb.org/jonas/ns/jonas-connector_4_1.xsd">
      ...
      </jonas-connector>

      The instance documents may indicate the published version of
      the schema using the xsi:schemaLocation attribute for the
      Jonas namespace with the following location:

      http://www.objectweb.org/jonas/ns/jonas-connector_4_1.xsd
      ]]>
    </xsd:documentation>
  </xsd:annotation>

  <xsd:include schemaLocation="jonas_j2ee_4_1.xsd" />

  <import namespace="http://java.sun.com/xml/ns/j2ee"
          schemaLocation="http://java.sun.com/xml/ns/j2ee/j2ee_1_4.xsd" />

  <!-- **************************************************** -->

  <xsd:element name="jonas-connector" type="jonas:jonas-connectorType">
    <xsd:annotation>
      <xsd:documentation>
        This is the root element of the JOnAS specific Connector deployment descriptor.
      </xsd:documentation>
    </xsd:annotation>
  </xsd:element>

  <!-- **************************************************** -->
  <xsd:group name="configurationGroup">
    <xsd:annotation>
      <xsd:documentation>
        This group defines:
         -  a jonas idGroup
         -  a sequence of name/value pairs
       </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:group ref="jonas:idGroup" />
      <xsd:element name="jonas-config-property"
                   type="jonas:jonas-config-propertyType"
                   minOccurs="0"
                   maxOccurs="unbounded" />
    </xsd:sequence>
  </xsd:group>

  <!-- **************************************************** -->
  <xsd:group name="idGroup">
    <xsd:annotation>
      <xsd:documentation>
        This group defines:
         -  a optional element id
         -  a optional element description
         -  a mandatory jndi-name
       </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element name="id" type="j2ee:xsdIntegerType" minOccurs="0">
        <xsd:annotation>
          <xsd:documentation>
           The id element defines an identifier for this element that matches one in the ra.xml
         </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="description"
                   type="j2ee:descriptionType"
                   minOccurs="0"
                   maxOccurs="unbounded" />
      <xsd:element name="jndi-name" type="j2ee:jndi-nameType">
        <xsd:annotation>
          <xsd:documentation>
           The jndi-name element defines the name that will map to the ConnectionFactory
           object stored in JNDI.
         </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
    </xsd:sequence>
  </xsd:group>

  <!-- **************************************************** -->

  <xsd:complexType name="jdbc-conn-paramsType">
    <xsd:annotation>
      <xsd:documentation>
        The jdbc-conn-params element is the root element for JDBC Connection specific
        values for this Resource Adapter.
       </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element name="jdbc-check-level"
                   minOccurs="0"
                   type="j2ee:xsdIntegerType">
        <xsd:annotation>
          <xsd:documentation>
           The jdbc-check-level element identifies the level of checking
           that will be done for a connection.
           The values are 0 for no checking, 1 to validate that the connection is
           not closed before returning it and
           greater than 1 to send the jdbc-test-statement.
         </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="jdbc-test-statement" minOccurs="0" type="j2ee:string">
        <xsd:annotation>
          <xsd:documentation>
           The jdbc-test-statement element identifies the test statement that will be sent on
           the connection if the jdbc-check-level is set accordingly.
         </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>


  <!-- **************************************************** -->

  <xsd:complexType name="jonas-activationspecType">
    <xsd:annotation>
      <xsd:documentation>
       Each jonas-activationspec element specifies
       a different inbound activationspec configuration
       for the deployed resource adapter.
       This element contains the jndi-name and the required config
       property values.
     </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:group ref="jonas:idGroup" />
      <xsd:element name="defaultAS" type="j2ee:true-falseType" minOccurs="0">
        <xsd:annotation>
          <xsd:documentation>
           The defaultAS element defines if this activationSpec can be used as a default
           for the container to attach MDBs.  A value of true will add this to a list of defaults
           wherein the first deployed will be used if the destination is not configured correctly.
           (Default value false)
         </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>

  <!-- **************************************************** -->

  <xsd:complexType name="jonas-adminobjectType">
    <xsd:annotation>
      <xsd:documentation>
        Each jonas-adminobject element specifies a different administered object configuration
        for the deployed resource adapter.
        This element contains the jndi-name and the required config
        property values.
       </xsd:documentation>
    </xsd:annotation>
    <xsd:group ref="jonas:configurationGroup" />
  </xsd:complexType>

  <!-- **************************************************** -->

  <xsd:complexType name="jonas-config-propertyType">
    <xsd:sequence>
      <xsd:annotation>
        <xsd:documentation>
         Each jonas-config-property element specifies a configuration property name and
         value that maps to an ra.xml config-entry element with the corresponding name.
       </xsd:documentation>
      </xsd:annotation>
      <xsd:element name="jonas-config-property-name" type="j2ee:string" />
      <xsd:element name="jonas-config-property-value" type="j2ee:string" />
    </xsd:sequence>
  </xsd:complexType>

  <!-- **************************************************** -->

  <xsd:complexType name="jonas-connection-definitionType">
    <xsd:annotation>
      <xsd:documentation>
        Each jonas-connection-definition element specifies a different
        outbound connection factory configuration
        for the deployed connection definition of the resource adapter.
        This element contains the jndi-name and different
        configuration information (logging parameters, pooling parameters, config property values)
        that will override the global defaults specified
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:group ref="jonas:configurationGroup" />
      <xsd:element name="log-enabled" type="j2ee:true-falseType" minOccurs="0">
        <xsd:annotation>
          <xsd:documentation>
           The log-enabled element defines if a log topic should be set for the
           ManagedConnectionFactory.  A value true will enable the logging mechanism
           and a value false will disable it.
           (Default value false)
         </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="log-topic" type="j2ee:string" minOccurs="0">
        <xsd:annotation>
          <xsd:documentation>
          The log-topic element defines the log topic that will be used to write
          log messages for this rar file.
          This will work in conjunction with log configuration facility that can
          define where the log will be written.
          (Default value: org.objectweb.jonas.jca)
        </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="pool-params"
                   type="jonas:pool-paramsType"
                   minOccurs="0">
        <xsd:annotation>
          <xsd:documentation>
           The pool-params element is the root element for Managed Connection Pool
           specific values for this Resource Adapter.
         </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="jdbc-conn-params"
                   type="jonas:jdbc-conn-paramsType"
                   minOccurs="0">
        <xsd:annotation>
          <xsd:documentation>
           The jdbc-conn-params is for JDBC Connection
           specific values for this Resource Adapter.
         </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>


  <!-- **************************************************** -->

  <xsd:complexType name="jonas-connectorType">
    <xsd:annotation>
      <xsd:documentation>
       The jonas-connectorType  defines the root element of the JOnAS specific
       deployment descriptor for the deployed resource adapter.
       This element includes different configuration
       information - associated rar link, pooling parameters, config property values.
     </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element name="jndi-name" type="j2ee:jndi-nameType" minOccurs="0">
        <xsd:annotation>
          <xsd:documentation>
           The jndi-name element defines the name that will map to the ConnectionFactory
           object stored in JNDI.
         </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="rarlink" type="j2ee:string" minOccurs="0">
        <xsd:annotation>
          <xsd:documentation>
           The rarlink element defines an association to a deployed Resource Adapter
           via the jndi-name.
           This will result in the config values from the name specified
           to be used as default values and then overridden by the pool and
           configuration values for the Resource Adapter being deployed.
         </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="native-lib" type="j2ee:string" minOccurs="0">
        <xsd:annotation>
          <xsd:documentation>
           The native-lib element defines the path that will be used
           to write the native libraries included in the rar file.
         </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="log-enabled" type="j2ee:true-falseType" minOccurs="0">
        <xsd:annotation>
          <xsd:documentation>
           The log-enabled element defines if a log topic should be set for the
           ManagedConnectionFactory.  A value true will enable the logging mechanism
           and a value false will disable it.
           (Default value false)
         </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="log-topic" type="j2ee:string" minOccurs="0">
        <xsd:annotation>
          <xsd:documentation>
           The log-topic element defines the log topic that will be used to write
           log messages for this rar file.
           This will work in conjunction with log configuration facility that can
           define where the log will be written.
           (Default value: org.objectweb.jonas.jca)
         </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="pool-params"
                   type="jonas:pool-paramsType"
                   minOccurs="0">
        <xsd:annotation>
          <xsd:documentation>
           The pool-params element is the root element for Managed Connection Pool
           specific values for this Resource Adapter.
         </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="jdbc-conn-params"
                   type="jonas:jdbc-conn-paramsType"
                   minOccurs="0">
        <xsd:annotation>
          <xsd:documentation>
           The jdbc-conn-params is for JDBC Connection
           specific values for this Resource Adapter.
         </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="jonas-config-property"
                   type="jonas:jonas-config-propertyType"
                   minOccurs="0"
                   maxOccurs="unbounded" />

      <xsd:element name="jonas-connection-definition"
                   type="jonas:jonas-connection-definitionType"
                   minOccurs="0"
                   maxOccurs="unbounded" />

      <xsd:element name="jonas-activationspec"
                   type="jonas:jonas-activationspecType"
                   minOccurs="0"
                   maxOccurs="unbounded" />

      <xsd:element name="jonas-adminobject"
                   type="jonas:jonas-adminobjectType"
                   minOccurs="0"
                   maxOccurs="unbounded" />
      <xsd:element name="jonas-security-mapping"
                   type="jonas:jonas-security-mappingType"
                   minOccurs="0" />
    </xsd:sequence>
  </xsd:complexType>

  <!-- **************************************************** -->

  <xsd:complexType name="jonas-security-mappingType">
    <xsd:annotation>
      <xsd:documentation>
       The jonas-securitymappingType defines the JOnAS specific mapping from a JOnAS
       principal name to an EIS user and password.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element name="security-entry"
                   type="jonas:security-entryType"
                   minOccurs="0"
                   maxOccurs="unbounded" />
    </xsd:sequence>
  </xsd:complexType>

  <!-- **************************************************** -->

  <xsd:complexType name="pool-paramsType">
    <xsd:annotation>
      <xsd:documentation>
       The pool-params element is the root element for Managed Connection Pool specific
       values for this Resource Adapter.
     </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element name="pool-init" type="j2ee:xsdIntegerType" minOccurs="0">
        <xsd:annotation>
          <xsd:documentation>
           The pool-init element identifies the initial number of managed connections
           to create during deployment.
           (Default value: 0)
         </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="pool-min" type="j2ee:xsdIntegerType" minOccurs="0">
        <xsd:annotation>
          <xsd:documentation>
           The pool-min element identifies the minimum number of managed connections
           to keep in the pool.
           (Default value: 0)
         </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="pool-max" type="j2ee:xsdIntegerType" minOccurs="0">
        <xsd:annotation>
          <xsd:documentation>
           The pool-max element identifies the maximun number of managed connections
           to keep in the pool.
           An Exception will be thrown to the caller if an attempt
           is made to create a managed connection above this limit.
         </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="pool-max-age" type="j2ee:xsdIntegerType" minOccurs="0">
        <xsd:annotation>
          <xsd:documentation>
           The pool-max-age element identifies the maximum number of milliseconds that the
           managed connection will be kept in the pool.
         </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="pstmt-max" type="j2ee:xsdIntegerType" minOccurs="0">
        <xsd:annotation>
          <xsd:documentation>
           The pstmt-max element identifies the maximum number of
           preparedStatement objects to keep in the pool.
           A PreparedStatment will not be cached if an attempt
           is made to get a PrepardedStatement above this limit.
           Value of 0 is unlimited,-1 disables the cache.
         </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>

  <!-- **************************************************** -->

  <xsd:complexType name="security-entryType">
    <xsd:annotation>
      <xsd:documentation>
       The security-entryType specifies a principalName that is
       mapped to the specified user and password.
     </xsd:documentation>
    </xsd:annotation>

    <xsd:attribute name="principalName" type="xsd:string" />
    <xsd:attribute name="user" type="xsd:string" />
    <xsd:attribute name="password" type="xsd:string" />
    <xsd:attribute name="encrypted" type="xsd:boolean" />
  </xsd:complexType>
</xsd:schema>
