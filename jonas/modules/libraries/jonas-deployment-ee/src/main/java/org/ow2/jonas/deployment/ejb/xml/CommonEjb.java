/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.xml;

import org.ow2.jonas.deployment.common.xml.AbsEnvironmentElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;
import org.ow2.jonas.deployment.common.xml.SecurityRoleRef;

/**
 * This class defines the implementation of the element session
 *
 * @author JOnAS team
 */

public abstract class CommonEjb extends AbsEnvironmentElement implements CommonEjbXml {



    /**
     * ejb-name
     */
    private String ejbName = null;

    /**
     * home
     */
    private String home = null;

    /**
     * remote
     */
    private String remote = null;

    /**
     * local-home
     */
    private String localHome = null;

    /**
     * local
     */
    private String local = null;

    /**
     * ejb-class
     */
    private String ejbClass = null;


    /**
     * messaging-type
     */
    private String messagingType = null;

    /**
     * transaction-type
     */
    private String transactionType = null;


    /**
     * security-role-ref
     */
    private JLinkedList securityRoleRefList = null;

    /**
     * security-identity
     */
    private SecurityIdentity securityIdentity = null;

    /**
     * mapped-name
     */
    private String mappedName;

    /**
     * Constructor
     */
    public CommonEjb() {
        super();
        securityRoleRefList = new  JLinkedList("security-role-ref");
        // Default transaction type is container
        this.transactionType = "Container";
    }

    /**
     * Gets the ejb-name
     * @return the ejb-name
     */
    public String getEjbName() {
        return ejbName;
    }

    /**
     * Set the ejb-name
     * @param ejbName ejbName
     */
    public void setEjbName(final String ejbName) {
        this.ejbName = ejbName;
    }


    /**
     * Set the mapped-name
     * @param name jndiName
     * @return
     */
    public String getMappedName() {
        return mappedName;
    }

    /**
     * Set the mapped-name
     * @param name jndiName
     */
    public void setMappedName(final String name) {
        this.mappedName = name;
    }
    /**
     * Gets the home
     * @return the home
     */
    public String getHome() {
        return home;
    }

    /**
     * Set the home
     * @param home home
     */
    public void setHome(final String home) {
        this.home = home;
    }

    /**
     * Gets the remote
     * @return the remote
     */
    public String getRemote() {
        return remote;
    }

    /**
     * Set the remote
     * @param remote remote
     */
    public void setRemote(final String remote) {
        this.remote = remote;
    }

    /**
     * Gets the local-home
     * @return the local-home
     */
    public String getLocalHome() {
        return localHome;
    }

    /**
     * Set the local-home
     * @param localHome localHome
     */
    public void setLocalHome(final String localHome) {
        this.localHome = localHome;
    }

    /**
     * Gets the local
     * @return the local
     */
    public String getLocal() {
        return local;
    }

    /**
     * Set the local
     * @param local local
     */
    public void setLocal(final String local) {
        this.local = local;
    }

    /**
     * Gets the ejb-class
     * @return the ejb-class
     */
    public String getEjbClass() {
        return ejbClass;
    }

    /**
     * Set the ejb-class
     * @param ejbClass ejbClass
     */
    public void setEjbClass(final String ejbClass) {
        this.ejbClass = ejbClass;
    }

    /**
     * Gets the messaging-type
     * @return the messaging-type
     */
    public String getMessagingType() {
        return messagingType;
    }

    /**
     * Set the messaging-type
     * @param messagingType messagingType
     */
    public void setMessagingType(final String messagingType) {
        this.messagingType = messagingType;
    }

    /**
     * Gets the transaction-type
     * @return the transaction-type
     */
    public String getTransactionType() {
        return transactionType;
    }

    /**
     * Set the transaction-type
     * @param transactionType transactionType
     */
    public void setTransactionType(final String transactionType) {
        this.transactionType = transactionType;
    }

    /**
     * Gets the security-role-ref
     * @return the security-role-ref
     */
    public JLinkedList getSecurityRoleRefList() {
        return securityRoleRefList;
    }

    /**
     * Set the security-role-ref
     * @param securityRoleRefList securityRoleRef
     */
    public void setSecurityRoleRefList(final JLinkedList securityRoleRefList) {
        this.securityRoleRefList = securityRoleRefList;
    }

    /**
     * Add a new  security-role-ref element to this object
     * @param securityRoleRef the securityRoleRefobject
     */
    public void addSecurityRoleRef(final SecurityRoleRef securityRoleRef) {
        securityRoleRefList.add(securityRoleRef);
    }

    /**
     * Gets the security-identity
     * @return the security-identity
     */
    public SecurityIdentity getSecurityIdentity() {
        return securityIdentity;
    }

    /**
     * Set the security-identity
     * @param securityIdentity securityIdentity
     */
    public void setSecurityIdentity(final SecurityIdentity securityIdentity) {
        this.securityIdentity = securityIdentity;
    }


}
