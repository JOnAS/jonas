/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.lib.util;

import org.apache.commons.digester.Digester;
import org.ow2.jonas.deployment.ejb.rules.ClusterRuleSet;

/**
 * Util class for cluster rules.
 * @author eyindanga
 *
 */
public class ClusterUtil {

    /**
     * default constructor.
     */
    private ClusterUtil() {

    }
    /**
     * Add cluster rule.
     * @param prefix the prefix to be used.
     * @param digester the digester.
     */
    public static void addClusterRule(final String prefix, final Digester digester) {
        try {
            ClusterRuleSet.class.getClassLoader().loadClass("org.ow2.cmi.info.mapping.Cluster");
        } catch (Exception e) {
            return;
        }
        /**
         * Cluster Rule set
         */
        digester.addRuleSet(new ClusterRuleSet(prefix));

    }
}
