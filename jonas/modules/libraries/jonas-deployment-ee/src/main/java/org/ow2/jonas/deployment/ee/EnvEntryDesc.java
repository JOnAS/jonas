/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Christophe Ney
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.ee;

import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;
import org.ow2.jonas.deployment.api.IEnvEntryDesc;
import org.ow2.jonas.deployment.common.DeploymentDescException;
import org.ow2.jonas.deployment.common.xml.EnvEntry;
import org.ow2.jonas.lib.util.Log;


/**
 * This class represents the description of an EnvEntry object
 * @author Christophe Ney
 * @author Florent Benoit
 */
public class EnvEntryDesc implements IEnvEntryDesc {

    /**
     * logger
     */
    protected static Logger logger = Log.getLogger(Log.JONAS_DEPLOY_PREFIX);
    
    /**
     * The env entry name.
     */
    private String name;

    /**
     * The env entry type.
     */
    private Class type;

    /**
     * The lookup name element.
     */
    private String lookupName;

    /**
     * The env entry value.
     */
    private Object value;

    /**
     * Construct a descriptor for an env-entry tag.
     * @param env the env-entry resulting of the xml parsing.
     * @throws DeploymentDescException when missing information for
     * creating the EnvEntryDesc.
     */
    public EnvEntryDesc(EnvEntry env) throws DeploymentDescException {
        name = env.getEnvEntryName();
        String t = env.getEnvEntryType();
        String v = null;
        if (env.getEnvEntryValue() != null) {
            v = env.getEnvEntryValue();
        }
        
        this.lookupName = env.getEnvEntryLookupName();
        
        
        if (t == null) {
            return;
        }
        
        
        try {
            if (t.equals(Boolean.class.getName())) {
                type = Boolean.class;
                if (v != null) {
                    if (v.equalsIgnoreCase("true")) {
                        value = Boolean.TRUE;
                    } else if (v.equalsIgnoreCase("false")) {
                        value = Boolean.FALSE;
                    } else {
                        value = Boolean.FALSE;
                        logger.log(BasicLevel.WARN, v + " is not a valid boolean value for env-entry " + name + "', defaulting to false");
                    }
                } else {
                    value = Boolean.FALSE;
                }

            } else if (t.equals(String.class.getName())) {
                type = String.class;
                if (v != null) {
                    value = v;
                } else {
                    value = new String();
                }
            } else if (t.equals(Integer.class.getName())) {
                type = Integer.class;
                if (v != null) {
                    value = new Integer(v);
                } else {
                    value = new Integer(0);
                }
            } else if (t.equals(Character.class.getName())) {
                type = Character.class;
                if (v != null) {
                    if (v.length() != 1) {
                        throw new DeploymentDescException("The value '" + v + "' is not a valid value for env-entry of type java.lang.Character.");
                    }
                    value = new Character(v.charAt(0));
                } else {
                    value = new Character("".charAt(0));
                }
            } else if (t.equals(Double.class.getName())) {
                type = Double.class;
                if (v != null) {
                    value = new Double(v);
                } else {
                    value = new Double(0);
                }
            } else if (t.equals(Byte.class.getName())) {
                type = Byte.class;
                if (v != null) {
                    value = new Byte(v);
                } else {
                    value = new Byte("");
                }
            } else if (t.equals(Short.class.getName())) {
                type = Short.class;
                if (v != null) {
                    value = new Short(v);
                } else {
                    value = new Short("");
                }
            } else if (t.equals(Long.class.getName())) {
                type = Long.class;
                if (v != null) {
                    value = new Long(v);
                } else {
                    value = new Long(0);
                }
            } else if (t.equals(Float.class.getName())) {
                type = Float.class;
                if (v != null) {
                    value = new Float(v);
                } else {
                    value = new Float(0);
                }
            } else {
                throw new DeploymentDescException(t + " is not a valid type for env-entry " + name);
            }
        } catch (NumberFormatException e) {
            throw new DeploymentDescException(v + " is not a valid value for env-entry " + name, e);
        }

    }

    public String getName() {
            return name;
    }

    public Class getType() {
            return type;
    }

    public boolean hasValue() {
            return value != null;
    }

    public Object getValue() {
        // An env-entry value is optional, so no error should be thrown
        /*
         *  if (value == null) {
         *      throw new Error("Value not set for env-entry " + name);
         *  }
         */
        return value;
    }

    /**
     * @return Env-Entry lookup name.
     */
    public String getLookupName() {
        return lookupName;
    }
    
    
    /**
     * String representation of the object for test purpose
     * @return String representation of this object
     */
    public String toString() {
        StringBuffer ret = new StringBuffer();
        ret.append("\ngetName()=" + getName());
        ret.append("\ngetType()=" + getType());
        if (hasValue()) {
            ret.append("\ngetValue()=" + getValue().toString());
        }
        return ret.toString();
    }

}
