/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.web.xml;

import org.ow2.jonas.deployment.common.CommonsSchemas;
import org.ow2.jonas.deployment.common.xml.AbsJonasEnvironmentElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;
import org.ow2.jonas.deployment.common.xml.JonasMessageDestination;
import org.ow2.jonas.deployment.common.xml.JonasSecurity;
import org.ow2.jonas.deployment.common.xml.TopLevelElement;
import org.ow2.jonas.deployment.web.JonasWebAppSchemas;


/**
 * This class defines the implementation of the element jonas-web-app.
 * @author Florent Benoit
 */
public class JonasWebApp extends AbsJonasEnvironmentElement implements TopLevelElement {

    /**
     * Header (with right XSD version) for XML
     */
    private String header = null;

    /**
     * Host element
     */
    private String host = null;

    /**
     * Context-root element
     */
    private String contextRoot = null;


    /**
     * Port to use (used by web services)
     */
    private String port = null;

    /**
     * Follow the java 2 delegation model or not
     */
    private String java2DelegationModel = null;

    /**
     * jonas-message-destination
     */
    private JLinkedList jonasMessageDestinationList = null;

    /**
     * servlet
     */
    private JLinkedList servletList = null;

    /**
     * security mapping
     */
    private JonasSecurity jonasSecurity = null;

    /**
     * jonas-web-app element XML header
     */
    public static final String JONAS_WEBAPP_ELEMENT = CommonsSchemas.getHeaderForElement("jonas-web-app",
                                                                                         JonasWebAppSchemas.getLastSchema());

    /**
     * Constructor : build a new WebApp object
     */
    public JonasWebApp() {
        super();
        jonasMessageDestinationList = new JLinkedList("jonas-message-destination");
        servletList = new JLinkedList("servlet");

        header = JONAS_WEBAPP_ELEMENT;
    }


    // Setters



    /**
     * Set the host element of this object
     * @param host host element of this object
     */
    public void setHost(final String host) {
        this.host = host;
    }


    /**
     * Set the context-root element of this object
     * @param contextRoot context-root element of this object
     */
    public void setContextRoot(final String contextRoot) {
        this.contextRoot = contextRoot;
    }


    /**
     * Set the port element of this object
     * @param port port element of this object
     */
    public void setPort(final String port) {
        this.port = port;
    }


    /**
     * Set the java 2 delegation model element of this object
     * @param java2DelegationModel java2-delegation-model element of this object
     */
    public void setJava2DelegationModel(final String java2DelegationModel) {
        this.java2DelegationModel = java2DelegationModel;
    }


    /**
     * Set the jonas-message-destination
     * @param jonasMessageDestinationList jonasMessageDestination
     */
    public void setJonasMessageDestinationList(final JLinkedList jonasMessageDestinationList) {
        this.jonasMessageDestinationList = jonasMessageDestinationList;
    }

    /**
     * Set the servlet
     * @param servletList servletList
     */
    public void setServletList(final JLinkedList servletList) {
        this.servletList = servletList;
    }

    /**
     * Add a new jonas-message-destination element to this object
     * @param jonasMessageDestination the jonas-message-destination object
     */
    public void addJonasMessageDestination(final JonasMessageDestination jonasMessageDestination) {
        jonasMessageDestinationList.add(jonasMessageDestination);
    }

    /**
     * Add a new servlet element to this object
     * @param jonasServlet the servlet object
     */
    public void addServlet(final JonasServlet jonasServlet) {
        servletList.add(jonasServlet);
    }

    // Getters


    /**
     * @return the host element
     */
    public String getHost() {
        return host;
    }

    /**
     * @return the context-root element
     */
    public String getContextRoot() {
        return contextRoot;
    }

    /**
     * @return the port element
     */
    public String getPort() {
        return port;
    }

    /**
     * @return the java2-delegation-model element
     */
    public String getJava2DelegationModel() {
        return java2DelegationModel;
    }

    /**
     * @return the list of all jonas-message-destination elements
     */
    public JLinkedList getJonasMessageDestinationList() {
        return jonasMessageDestinationList;
    }

    /**
     * @return the list of all servlet elements
     */
    public JLinkedList getServletList() {
        return servletList;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    @Override
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        if (header != null) {
            sb.append(header);
        } else {
            sb.append("<jonas-web-app>\n");
        }

        indent += 2;

        // jonas-resource-env
        sb.append(getJonasResourceEnvList().toXML(indent));

        // jonas-resource
        sb.append(getJonasResourceList().toXML(indent));

        // jonas-ejb-ref
        sb.append(getJonasEjbRefList().toXML(indent));

        // jonas-message-destination-ref
        sb.append(getJonasMessageDestinationRefList().toXML(indent));

        // jonas-message-destination
        sb.append(getJonasMessageDestinationList().toXML(indent));

        // jonas-service-ref
        sb.append(getJonasServiceRefList().toXML(indent));

        // servlet
        sb.append(getServletList().toXML(indent));

        // tenantId
        sb.append(xmlElement(getTenantId(),"tenant-id",indent));

        // host
        if (host != null) {
            sb.append(xmlElement(host, "host", indent));
        }

        // context-root
        if (contextRoot != null) {
            sb.append(xmlElement(contextRoot, "context-root", indent));
        }

        // port
        if (port != null) {
            sb.append(xmlElement(port, "port", indent));
        }

        // java2-delegation-model
        if (java2DelegationModel != null) {
            sb.append(xmlElement(java2DelegationModel, "java2-delegation-model", indent));
        }

        // jonas-security
        if (jonasSecurity != null) {
            sb.append(jonasSecurity.toXML(indent));
        }

        indent -= 2;
        sb.append(indent(indent));
        sb.append("</jonas-web-app>");

        return sb.toString();
    }

    /**
     * @return the header.
     */
    public String getHeader() {
        return header;
    }

    /**
     * @param header The header to set.
     */
    public void setHeader(final String header) {
        this.header = header;
    }

    public JonasSecurity getJonasSecurity() {
        return jonasSecurity;
    }

    public void setJonasSecurity(JonasSecurity jonasSecurity) {
        this.jonasSecurity = jonasSecurity;
    }
}
