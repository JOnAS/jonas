/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A. 
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar;

import java.io.Serializable;
import java.util.List;

import org.ow2.jonas.deployment.rar.xml.SecurityPermission;


/** 
 * This class defines the implementation of the element security-permission
 * 
 * @author Eric Hardesty
 */

public class SecurityPermissionDesc implements Serializable {

    /**
     * description
     */ 
    private List descriptionList = null;

    /**
     * security-permission-spec
     */ 
    private String securityPermissionSpec = null;


    /**
     * Constructor
     */
    public SecurityPermissionDesc(SecurityPermission sp) {
        if (sp != null) {
            descriptionList = sp.getDescriptionList();
            securityPermissionSpec = sp.getSecurityPermissionSpec();
        }
    }

    /** 
     * Gets the description
     * @return the description
     */
    public List getDescriptionList() {
        return descriptionList;
    }

    /** 
     * Gets the security-permission-spec
     * @return the security-permission-spec
     */
    public String getSecurityPermissionSpec() {
        return securityPermissionSpec;
    }

}
