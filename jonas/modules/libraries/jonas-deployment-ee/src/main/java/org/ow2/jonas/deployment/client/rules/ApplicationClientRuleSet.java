/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: jonas-team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.client.rules;

import org.ow2.jonas.deployment.common.rules.EnvironmentRuleSet;
import org.ow2.jonas.deployment.common.rules.IconRuleSet;
import org.ow2.jonas.deployment.common.rules.JRuleSetBase;
import org.ow2.jonas.deployment.common.rules.MessageDestinationRuleSet;

import org.apache.commons.digester.Digester;

/**
 * This class defines the rules  to analyze the element application-client
 *
 * @author jonas-team
 */
public class ApplicationClientRuleSet extends JRuleSetBase {

    /**
     * <p><strong>RuleSet</strong> for processing the contents of a
     *application-client definition element.
     *
     * @author jonas-team
     */

    /**
     * Construct an object with a specific prefix
     */
    public ApplicationClientRuleSet() {
        super("application-client/");
   }
     /**
     * Add a set of rules to the digester object
     * @param digester Digester instance
     */

    public void addRuleInstances(Digester digester) {
        digester.addRuleSet(new IconRuleSet(prefix));
        digester.addCallMethod(prefix + "display-name",
                           "setDisplayName", 0);
        digester.addCallMethod(prefix + "description",
                           "setDescription", 0);
        digester.addRuleSet(new EnvironmentRuleSet(prefix));
        digester.addCallMethod(prefix + "callback-handler",
                           "setCallbackHandler", 0);
        digester.addRuleSet(new MessageDestinationRuleSet(prefix));
   }
}
