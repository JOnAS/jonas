/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.deployment.ejb;

import java.lang.reflect.Method;

import org.ow2.jonas.deployment.common.DeploymentDescException;
import org.ow2.jonas.deployment.common.xml.JLinkedList;
import org.ow2.jonas.deployment.ejb.xml.AssemblyDescriptor;
import org.ow2.jonas.deployment.ejb.xml.JonasSession;
import org.ow2.jonas.deployment.ejb.xml.Session;


/**
 * class to hold meta-information related to a stateful session bean.
 * At this stage, this is only a tagger class.
 * @author Christophe Ney [cney@batisseurs.com] : Initial developer
 * @author Helene Joanin
 */
public class SessionStatefulDesc extends SessionDesc {

    protected Method isModifiedMethod = null;

    /**
     * constructor: called when the DeploymentDescriptor is read.
     * Currently, called by both GenIC and createContainer.
     */
    public SessionStatefulDesc(ClassLoader classLoader, Session ses,
			       AssemblyDescriptor asd, JonasSession jSes,
			       JLinkedList jMDRList, String filename)
            throws DeploymentDescException {
        super(classLoader, ses, asd, jSes, jMDRList, filename);

        // isModifiedMethod
        if (jSes.getIsModifiedMethodName() != null) {
            String mName = jSes.getIsModifiedMethodName();
            try {
                isModifiedMethod = this.ejbClass.getMethod(mName, new Class[0]);
            } catch (NoSuchMethodException e) {
                isModifiedMethod = null;
                throw new DeploymentDescException(mName + " is not a method of " + this.ejbClass.getName());
            } catch (SecurityException e) {
                throw new DeploymentDescException("Cannot use java reflexion on " + this.ejbClass.getName());
            }
        }
    }

    /**
     * Get the 'isModified' method name implemented in the bean class.
     * (This information is JOnAS specific).
     * @return Name of the isModified method
     */
    public Method getIsModifiedMethod(){
        if (isModifiedMethod == null)
            throw new Error("No isModified method defined for bean " + this.ejbName);
        return isModifiedMethod;
    }

    /**
     * Assessor for existence of a isModified methoe
     * @return true of isModified method exist for the bean
     */
    public boolean hasIsModifiedMethod(){
        return isModifiedMethod != null;
    }

    /**
     * String representation of the object for test purpose
     * @return String representation of this object
     */
    public String toString() {
        StringBuffer ret = new StringBuffer();
        ret.append(super.toString());
        if (hasIsModifiedMethod())
            ret.append("getIsModifiedMethod()=" + getIsModifiedMethod().toString());
        return ret.toString();
    }

}
