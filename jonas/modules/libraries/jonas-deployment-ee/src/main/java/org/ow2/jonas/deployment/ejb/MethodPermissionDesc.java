/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.ejb;

import java.util.List;

import org.ow2.jonas.deployment.ejb.xml.MethodPermission;

/**
 * Defines a MethodPermissionDesc class for the management of
 * EJBMethodPermissions in JACC
 * @author Florent Benoit : Initial developer
 */
public class MethodPermissionDesc extends CommonMethodDesc {

    /**
     * List of roles if not unchecked
     */
    private List roleNameList = null;

    /**
     * Unchecked method-permission or not ?
     */
    private boolean unchecked = false;


    /**
     * Constructor for MethodPermissionDesc
     * @param methodPermission method-permission which contains xml content
     */
    public MethodPermissionDesc(MethodPermission methodPermission) {
        super(methodPermission.getMethodList());
        this.unchecked = methodPermission.isUnchecked();
        this.roleNameList = methodPermission.getRoleNameList();
    }


    /**
     * Gets the role-name
     * @return the role-name
     */
    public List getRoleNameList() {
        return roleNameList;
    }


    /**
     * Unchecked method permission ?
     * @return true if the method-permission is unchecked, false otherwise
     */
    public boolean isUnchecked() {
        return unchecked;
    }

}
