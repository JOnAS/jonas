/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ear.wrapper;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;
import org.ow2.jonas.deployment.ear.EarDeploymentDesc;
import org.ow2.jonas.deployment.ear.EarDeploymentDescException;
import org.ow2.jonas.lib.bootstrap.LoaderManager;
import org.ow2.jonas.lib.util.Log;
import org.ow2.util.ee.deploy.api.deployable.EARDeployable;


/**
 * Wrapper that allow us to use Commons Digester.
 * @author Guillaume Sauthier
 */
public final class EarManagerWrapper {

    /**
     * EarDeploymentDescManager fully qualified classname.
     */
    private static final String EAR_MANAGER_CLASSNAME = "org.ow2.jonas.deployment.ear.lib.EarDeploymentDescManager";

    /**
     * logger.
     */
    private static Logger logger = Log.getLogger(Log.JONAS_EAR_PREFIX);

    /**
     * Empty private constructor for utility classes.
     */
    private EarManagerWrapper() { }

    /**
     * Wrap EarDeploymentDescManager.getDeploymentDesc(ear, cl).
     *
     * @param earDeployable the given EAR deployable
     * @param cl Application ClassLoader
     *
     * @return the EarDeploymentDesc of the ear filename
     *
     * @throws EarDeploymentDescException When Descriptor cannot be instanciated
     */
    public static EarDeploymentDesc getDeploymentDesc(final EARDeployable earDeployable, final ClassLoader cl)
    throws EarDeploymentDescException {
        LoaderManager lm = LoaderManager.getInstance();
        EarDeploymentDesc earDD = null;
        try {
            ClassLoader ext = lm.getExternalLoader();
            Class<?> manager = ext.loadClass(EAR_MANAGER_CLASSNAME);
            Method m = manager.getDeclaredMethod("getDeploymentDesc", new Class[] {EARDeployable.class, ClassLoader.class});
            earDD = (EarDeploymentDesc) m.invoke(null, new Object[] {earDeployable, cl});
        } catch (InvocationTargetException ite) {
            Throwable t = ite.getTargetException();
            if (EarDeploymentDescException.class.isInstance(t)) {
                throw (EarDeploymentDescException) ite.getTargetException();
            } else {
                throw new EarDeploymentDescException("EarDeploymentDescManager.getDeploymentDesc fails", t);
            }
        } catch (Exception e) {
            // TODO add i18n here
            throw new EarDeploymentDescException("Problems when using reflection on EarDeploymentDescManager", e);
        }

        return earDD;
    }

    /**
     * Wrap EarDeploymentDescManager.setParsingWithValidation(b).
     *
     * @param b true/false
     */
    public static void setParsingWithValidation(final boolean b) {
        LoaderManager lm = LoaderManager.getInstance();

        try {
            ClassLoader ext = lm.getExternalLoader();
            Class<?> manager = ext.loadClass(EAR_MANAGER_CLASSNAME);
            Method m = manager.getDeclaredMethod("setParsingWithValidation", new Class[] {boolean.class});
            m.invoke(null, new Object[] {new Boolean(b)});
        } catch (Exception e) {
            // Should never occurs
            logger.log(BasicLevel.ERROR, e);
        }
    }

}
