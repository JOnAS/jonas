/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.client;


import org.ow2.jonas.deployment.common.CommonsDTDs;
import org.ow2.jonas.deployment.common.util.ResourceHelper;

/**
 * This class defines the declarations of DTDs for application-client.xml and jonas-client.xml.
 * @author Florent Benoit
 */
public class AppClientDTDs extends CommonsDTDs {

    /**
     * Package name.
     */
    private static final String PACKAGE = ResourceHelper.getResourcePackage(AppClientDTDs.class);

    /**
     * List of application-client dtds.
     */
    private static final String[] APPCLIENT_DTDS = new String[] {
        PACKAGE + "application-client_1_2.dtd",
        PACKAGE + "application-client_1_3.dtd"
    };

    /**
     * List of application-client publicId.
     */
    private static final String[] APPCLIENT_DTDS_PUBLIC_ID = new String[] {
        "-//Sun Microsystems, Inc.//DTD J2EE Application Client 1.2//EN",
        "-//Sun Microsystems, Inc.//DTD J2EE Application Client 1.3//EN"
    };




    /**
     * Build a new object for web.xml DTDs handling.
     */
    public AppClientDTDs() {
        super();
        addMapping(APPCLIENT_DTDS, APPCLIENT_DTDS_PUBLIC_ID, AppClientDTDs.class.getClassLoader());
    }

}
