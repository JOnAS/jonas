/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.web.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;
import org.ow2.jonas.deployment.common.xml.RunAs;
import org.ow2.jonas.deployment.common.xml.SecurityRoleRef;

/**
 * This class defines the implementation of the element servlet
 * <b> Don't take into account nested element :
 *   icon, display-name, description, init-param, load-on-startup, run-as and security-role-ref
 * </b> Which are optional (web container implentation will parse it)
 * @author Florent Benoit
 */
public class Servlet extends AbsElement {

    /**
     * Name of the servlet
     */
    private String servletName = null;

    /**
     * Servlet-class
     */
    private String servletClass = null;

    /**
     * Jsp-file
     */
    private String jspFile = null;

    /**
     * security-role-ref
     */
    private JLinkedList securityRoleRefList = null;

    /**
     * run-as
     */
    private RunAs runAs = null;


    /**
     * Constructor
     */
    public Servlet() {
        super();
        securityRoleRefList = new JLinkedList("security-role-ref");
    }

    // Setters


    /**
     * Sets the name of the servlet
     * @param servletName name of the servlet
     */
    public void setServletName(String servletName) {
        this.servletName = servletName;
    }


    /**
     * Sets the class of the servlet
     * @param servletClass class of the servlet
     */
    public void setServletClass(String servletClass) {
        this.servletClass = servletClass;
    }

    /**
     * Sets the jsp-file of the servlet
     * @param jspFile jsp-file of the servlet
     */
    public void setJspFile(String jspFile) {
        this.jspFile = jspFile;
    }

    /**
     * Add a new security-role-ref element to this object
     * @param securityRoleRef security-role-ref
     */
    public void addSecurityRoleRef(SecurityRoleRef securityRoleRef) {
        securityRoleRefList.add(securityRoleRef);
    }

    /**
     * Set the security-role-ref
     * @param securityRoleRefList securityRoleRef
     */
    public void setSecurityRoleRefList(JLinkedList securityRoleRefList) {
        this.securityRoleRefList = securityRoleRefList;
    }

    /**
     * Set the run-as
     * @param runAs runAs
     */
    public void setRunAs(RunAs runAs) {
        this.runAs = runAs;
    }

    // Getters

    /**
     * @return the name of the servlet
     */
    public String getServletName() {
        return servletName;
    }

    /**
     * @return the class of the servlet
     */
    public String getServletClass() {
        return servletClass;
    }

    /**
     * @return the jsp-file of the servlet
     */
    public String getJspFile() {
        return jspFile;
    }

    /**
     * Gets the security-role-ref
     * @return the security-role-ref
     */
    public JLinkedList getSecurityRoleRefList() {
        return securityRoleRefList;
    }


    /**
     * Gets the run-as
     * @return the run-as
     */
    public RunAs getRunAs() {
        return runAs;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<servlet>\n");

        indent += 2;

        // servlet-name
        sb.append(xmlElement(servletName, "servlet-name", indent));

        // servlet-class or jsp-file
        sb.append(xmlElement(servletClass, "servlet-class", indent));
        sb.append(xmlElement(jspFile, "jsp-file", indent));

        // run-as
        if (runAs != null) {
            sb.append(runAs.toXML(indent));
        }

        // security-role-ref
        if (securityRoleRefList != null) {
            sb.append(securityRoleRefList.toXML(indent));
        }

        indent -= 2;
        sb.append(indent(indent));
        sb.append("</servlet>\n");

        return sb.toString();
    }
}
