/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;
/**
 * This class defines the implementation of the element method-permission
 *
 * @author JOnAS team
 */

public class MethodPermission extends AbsElement  {

    /**
     * description
     */
    private String description = null;

    /**
     * role-name
     */
    private JLinkedList roleNameList = null;

    /**
     * unchecked
     */
    private boolean unchecked = false;

    /**
     * method
     */
    private JLinkedList methodList = null;


    /**
     * Constructor
     */
    public MethodPermission() {
        super();
        roleNameList = new  JLinkedList("role-name");
        methodList = new  JLinkedList("method");
    }

    /**
     * Gets the description
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the description
     * @param description description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the role-name
     * @return the role-name
     */
    public JLinkedList getRoleNameList() {
        return roleNameList;
    }

    /**
     * Set the role-name
     * @param roleNameList roleName
     */
    public void setRoleNameList(JLinkedList roleNameList) {
        this.roleNameList = roleNameList;
    }

    /**
     * Add a new  role-name element to this object
     * @param roleName the roleNameobject
     */
    public void addRoleName(String roleName) {
        roleNameList.add(roleName);
    }

    /**
     * Gets the unchecked status
     * @return true if it is unchecked
     */
    public boolean isUnchecked() {
        return unchecked;
    }

    /**
     * Set the unchecked
     */
    public void setUnchecked() {
        this.unchecked = true;
    }

    /**
     * Gets the method
     * @return the method
     */
    public JLinkedList getMethodList() {
        return methodList;
    }

    /**
     * Set the method
     * @param methodList method
     */
    public void setMethodList(JLinkedList methodList) {
        this.methodList = methodList;
    }

    /**
     * Add a new  method element to this object
     * @param method the methodobject
     */
    public void addMethod(Method method) {
        methodList.add(method);
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<method-permission>\n");

        indent += 2;

        // description
        sb.append(xmlElement(description, "description", indent));
        // role-name
        sb.append(roleNameList.toXML(indent));
        // unchecked
        if (unchecked) {
            sb.append(indent(indent));
            sb.append("<unchecked></unchecked>\n");
        }
        // method
        sb.append(methodList.toXML(indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</method-permission>\n");

        return sb.toString();
    }
}
