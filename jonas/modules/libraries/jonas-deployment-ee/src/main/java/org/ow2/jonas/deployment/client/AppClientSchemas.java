/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.client;

import org.ow2.jonas.deployment.common.CommonsSchemas;
import org.ow2.jonas.deployment.common.util.ResourceHelper;

/**
 * This class defines the declarations of Schemas for appliation-client.xml and jonas-client.xml.
 * @author Philippe Coq
 */
public class AppClientSchemas extends CommonsSchemas {

    /**
     * Package name.
     */
    private static final String PACKAGE = ResourceHelper.getResourcePackage(AppClientSchemas.class);

    /**
     * List of schemas used for application-client.xml.
     */
    private static final String[] APPCLIENT_SCHEMAS = new String[] {
        PACKAGE + "application-client_1_4.xsd",
        PACKAGE + "application-client_5.xsd"
    };


    /**
     * Build a new object for Schemas handling.
     */
    public AppClientSchemas() {
        super();
        addSchemas(APPCLIENT_SCHEMAS, AppClientSchemas.class.getClassLoader());
    }

}
