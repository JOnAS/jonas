/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.clusterd.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;

/**
 *
 * Server element
 * @author pelletib
 */
public class Server extends AbsElement  {

    /**
     * Version UID
     */
    private static final long serialVersionUID = -7044912860997997046L;

    /**
     * JOnAS instance name
     */
    private String name = null;

    /**
     * JOnAS instance domain name
     */
    private String domain = null;

    /**
     * description
     */
    private String description = null;

    /**
     * JAVA_HOME  directory
     */
    private String javaHome = null;

    /**
     * JONAS_ROOT directory
     */
    private String jonasRoot = null;

    /**
     * JONAS_BASE directory
     */
    private String jonasBase = null;

    /**
     * Extra parameters for starting the JVM
     */
    private String xprm = null;

    /**
     * Automatic boot
     */
    private String autoBoot = null;

    /**
     * User command for controlling JOnAS (optional)
     * If not set, use the JOnAS one
     */
    private String jonasCmd = null;

    /**
     * Constructor
     */
    public Server() {
        super();
    }

    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name to set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return Returns the domain.
     */
    public String getDomain() {
        return domain;
    }

    /**
     * @param domain The domain to set.
     */
    public void setDomain(String domain) {
        this.domain= domain;
    }

    /**
     * @return Returns the JAVA_HOME dir.
     */
    public String getJavaHome() {
        return javaHome;
    }

    /**
     * @param javaHome The JAVA_HOME to set.
     */
    public void setJavaHome(String javaHome) {
        this.javaHome = javaHome;
    }

    /**
     * @return Returns the JONAS_ROOT.
     */
    public String getJonasRoot() {
        return jonasRoot;
    }

    /**
     * @param jonasRoot The JONAS_ROOT to set.
     */
    public void setJonasRoot(String jonasRoot) {
        this.jonasRoot = jonasRoot;
    }

    /**
     * @return Returns the JONAS_BASE.
     */
    public String getJonasBase() {
        return jonasBase;
    }

    /**
     * @param jonasBase The JONAS_BASE to set.
     */
    public void setJonasBase(String jonasBase) {
        this.jonasBase = jonasBase;
    }

    /**
     * @return Returns the description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description to set.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<server>\n");

        indent += 2;

        // name
        if (name != null) {
            sb.append(xmlElement(name, "name", indent));
        }
        // domain
        if (domain != null) {
            sb.append(xmlElement(domain, "domain", indent));
        }
        // description
        if (getDescription() != null) {
            sb.append(xmlElement(getDescription(), "description", indent));
        }
        // javaHome
        if (getJavaHome() != null) {
            sb.append(xmlElement(getJavaHome(), "java-home", indent));
        }
        // jonasRoot
        if (getJonasRoot() != null) {
            sb.append(xmlElement(getJonasRoot(), "jonas-root", indent));
        }
        // jonasBase
        if (getJonasBase() != null) {
            sb.append(xmlElement(getJonasBase(), "jonas-base", indent));
        }

        // xprm
        if (getXprm() != null) {
            sb.append(xmlElement(getXprm(), "xprm", indent));
        }

        // autoBoot
        sb.append(xmlElement(new Boolean(isAutoBoot()).toString(), "auto-boot", indent));

        indent -= 2;
        sb.append(indent(indent));
        sb.append("</server>\n");

        return sb.toString();
    }

    /**
     * Set the auto boot for the server
     * @param autoBoot true if automatic boot is set
     */
    public void setAutoBoot(String autoBoot) {
        this.autoBoot = autoBoot;
    }

    /**
     * @return true/false
     */
    public String getAutoBoot() {
        return this.autoBoot;
    }

    /**
    *
    * @return true if the auto boot is set
    */
   public boolean isAutoBoot() {
       return new Boolean(getAutoBoot()).booleanValue();
   }


   /**
    *
    * @return the extra parameters
    */
    public String getXprm() {
        return xprm;
    }

    /**
     * Set the extra parameters
     * @param xprm extra parameters
     */
    public void setXprm(String xprm) {
        this.xprm = xprm;
    }

    /**
     *
     * @return JOnAS command
     */
    public String getJonasCmd() {
        return jonasCmd;
    }

    /**
     * Set the JOnAS command
     * @param jonasCmd user command
     */
    public void setJonasCmd(String jonasCmd) {
        this.jonasCmd = jonasCmd;
    }
}