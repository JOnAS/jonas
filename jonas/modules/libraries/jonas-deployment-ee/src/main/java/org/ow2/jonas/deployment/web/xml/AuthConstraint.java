/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.web.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;

/**
 * This class defines the implementation of the element auth-constraint
 * @author Florent Benoit
 */
public class AuthConstraint extends AbsElement {

    /**
     * description
     */
    private JLinkedList descriptionList = null;

    /**
     * role-name
     */
    private JLinkedList roleNameList = null;


    /**
     * Constructor
     */
    public AuthConstraint() {
        super();
        descriptionList = new  JLinkedList("description");
        roleNameList = new  JLinkedList("role-name");
    }


    // Setters

    /**
     * Add a new description element to this object
     * @param description description
     */
    public void addDescription(String description) {
        descriptionList.add(description);
    }

    /**
     * Add a new role-name element to this object
     * @param roleName role-name
     */
    public void addRoleName(String roleName) {
        roleNameList.add(roleName);
    }

    // Getters

    /**
     * Gets the description list
     * @return the description list
     */
    public JLinkedList getDescriptionList() {
        return descriptionList;
    }

    /**
     * Gets the role-name list
     * @return the role-name list
     */
    public JLinkedList getRoleNameList() {
        return roleNameList;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<auth-constraint>\n");

        indent += 2;
        // description
        sb.append(descriptionList.toXML(indent));

        // role-name
        sb.append(roleNameList.toXML(indent));

        indent -= 2;
        sb.append(indent(indent));
        sb.append("</auth-constraint>\n");

        return sb.toString();
    }

}
