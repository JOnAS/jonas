/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ws.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;

/**
 * This class defines the implementation of the element package-mapping
 *
 * @author JOnAS team
 */

public class PackageMapping extends AbsElement  {

    /**
     * package-type
     */
    private String packageType = null;

    /**
     * namespaceURI
     */
    private String namespaceURI = null;


    /**
     * Constructor
     */
    public PackageMapping() {
        super();
    }

    /**
     * Gets the package-type
     * @return the package-type
     */
    public String getPackageType() {
        return packageType;
    }

    /**
     * Set the package-type
     * @param packageType packageType
     */
    public void setPackageType(String packageType) {
        this.packageType = packageType;
    }

    /**
     * Gets the namespaceURI
     * @return the namespaceURI
     */
    public String getNamespaceURI() {
        return namespaceURI;
    }

    /**
     * Set the namespaceURI
     * @param namespaceURI namespaceURI
     */
    public void setNamespaceURI(String namespaceURI) {
        this.namespaceURI = namespaceURI;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<package-mapping>\n");

        indent += 2;

        // package-type
        sb.append(xmlElement(packageType, "package-type", indent));
        // namespaceURI
        sb.append(xmlElement(namespaceURI, "namespaceURI", indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</package-mapping>\n");

        return sb.toString();
    }
}
