/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.deployment.ejb;

import java.io.CharArrayReader;
import java.lang.reflect.Method;

import org.ow2.jonas.deployment.common.DeploymentDescException;
import org.ow2.jonas.deployment.ejb.ejbql.ASTEJBQL;
import org.ow2.jonas.deployment.ejb.ejbql.EJBQL;
import org.ow2.jonas.deployment.ejb.ejbql.ParseException;



/**
 * Class to hold meta-information related to CMP2 find/select methods
 * @author Christophe Ney [cney@batisseurs.com] : Initial developer
 * @author Helene Joanin
 */
public class MethodCmp2Desc extends MethodDesc  {

    protected String query = null;
    protected ASTEJBQL queryNode = null;
    protected boolean resultTypeMappingRemote = false;
    protected EntityDesc entityDesc;


    /**
     * construtor form XML data binding structures
     */
    MethodCmp2Desc(BeanDesc beanDesc, Method meth, Class classDef, int index) {
        super(beanDesc, meth, classDef, index);
        entityDesc = (EntityDesc) beanDesc;
    }

    /**
     * get EJB-QL query when defined
     * @return possibly null String containing the EJB-QL query for the method
     */
    public String getQuery() {
        return query;
    }

    /**
     * set EJB-QL query. Because of the deployment descriptor structure, the query is
     * set after the object is created.
     */
    public void setQuery(String query) throws ParseException {
        // we want a one line query for error reporting
        this.query = query.replace('\r',' ').replace('\n',' ').replace('\t',' ');
        EJBQL parser = new EJBQL(new CharArrayReader(query.toCharArray()));
        queryNode = (ASTEJBQL)parser.EJBQL();
    }


    public ASTEJBQL getQueryNode() {
        return queryNode;
    }

    /**
     * get result type mapping state (remote/local)
     * @return true when remote
     */
    public boolean isResultTypeMappingRemote() {
        return resultTypeMappingRemote;
    }

    /**
     * set the state of resultTypeMappingRemote
     */
    public void setResultTypeMapping(String resultTypeMapping)
        throws DeploymentDescException {
            if (resultTypeMapping.equals("Remote")) {
                resultTypeMappingRemote=true;
            } else if (resultTypeMapping.equals("Local")) {
                resultTypeMappingRemote=false;
            } else {
                throw new DeploymentDescException(resultTypeMapping+" is not a valid result-type-mapping value");
            }
    }

    /**
     * Get the prefetch tag value for this method.
     * May be true only for finder methods (not for ejbSelect methods)
     */
    public boolean getPrefetch() {
        if (this.isFinder()) {
            return entityDesc.isPrefetch();
        } else {
            return false;
        }
    }


    /**
     * String representation of the given element <method>
     * @param  m an element <method>
     * @return String representation of the given element method
     */
    public static String queryMethodElementToString(org.ow2.jonas.deployment.ejb.xml.QueryMethod m) {
        return methodElementToString(null, m.getMethodName(),m.getMethodParams());
    }

    /**
     * String representation of the object for test purpose
     * @return String representation of this object
     */
    public String toString() {
        StringBuffer ret = new StringBuffer();
        ret.append(super.toString());
        ret.append("\nquery = " + query);
        ret.append("\nqueryNode = " + queryNode);
        ret.append("\nresultTypeMappingRemote = " + resultTypeMappingRemote);
        return ret.toString();
    }

}
