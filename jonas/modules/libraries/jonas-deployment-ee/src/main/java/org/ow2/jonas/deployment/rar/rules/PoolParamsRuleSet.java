/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar.rules;

import org.apache.commons.digester.Digester;
import org.ow2.jonas.deployment.common.rules.JRuleSetBase;

/**
 * This class defines the rules to analyze the element pool-params
 *
 * @author Florent Benoit
 */

public class PoolParamsRuleSet extends JRuleSetBase {

    /**
     * Construct an object with a specific prefix
     * @param prefix prefix to use during the parsing
     */
    public PoolParamsRuleSet(final String prefix) {
        super(prefix);
   }
     /**
     * Add a set of rules to the digester object
     * @param digester Digester instance
     */

    @Override
    public void addRuleInstances(final Digester digester) {
        digester.addObjectCreate(prefix + "pool-params",
                                 "org.ow2.jonas.deployment.rar.xml.PoolParams");
        digester.addSetNext(prefix + "pool-params",
                            "setPoolParams",
                            "org.ow2.jonas.deployment.rar.xml.PoolParams");
        digester.addCallMethod(prefix + "pool-params/pool-init",
                               "setPoolInit", 0);
        digester.addCallMethod(prefix + "pool-params/pool-min",
                               "setPoolMin", 0);
        digester.addCallMethod(prefix + "pool-params/pool-max",
                               "setPoolMax", 0);
        digester.addCallMethod(prefix + "pool-params/pool-max-age",
                               "setPoolMaxAge", 0);
        digester.addCallMethod(prefix + "pool-params/pool-max-age-minutes",
                               "setPoolMaxAgeMinutes", 0);
        digester.addCallMethod(prefix + "pool-params/pstmt-max",
                               "setPstmtMax", 0);
        digester.addCallMethod(prefix + "pool-params/pstmt-cache-policy",
                "setPstmtCachePolicy", 0);
        digester.addCallMethod(prefix + "pool-params/pool-max-opentime",
                               "setPoolMaxOpentime", 0);
        digester.addCallMethod(prefix + "pool-params/pool-max-waiters",
                               "setPoolMaxWaiters", 0);
        digester.addCallMethod(prefix + "pool-params/pool-max-waittime",
                               "setPoolMaxWaittime", 0);
        digester.addCallMethod(prefix + "pool-params/pool-sampling-period",
                               "setPoolSamplingPeriod", 0);
   }
}
