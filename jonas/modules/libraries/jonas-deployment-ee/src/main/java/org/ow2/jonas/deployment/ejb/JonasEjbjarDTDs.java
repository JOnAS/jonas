/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Philippe Coq
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.ejb;


import org.ow2.jonas.deployment.common.CommonsDTDs;
import org.ow2.jonas.deployment.common.util.ResourceHelper;

/**
 * This class defines the declarations of DTDs for jonas-ejb-jar.xml.
 * @author Philippe Coq
 */
public class JonasEjbjarDTDs extends CommonsDTDs {

    /**
     * Package name.
     */
    private static final String PACKAGE = ResourceHelper.getResourcePackage(JonasEjbjarDTDs.class);

    /**
     * List of jonas-ejb-jar dtds.
     */
    private static final String[] JONAS_EJBJAR_DTDS = new String[] {
        PACKAGE + "jonas-ejb-jar_2_4.dtd",
        PACKAGE + "jonas-ejb-jar_2_5.dtd",
        PACKAGE + "jonas-ejb-jar_3_0.dtd",
        PACKAGE + "jonas-ejb-jar_3_2.dtd",
        PACKAGE + "jonas-ejb-jar_3_3.dtd",
        PACKAGE + "jonas-ejb-jar_3_3_1.dtd",
        PACKAGE + "jonas-ejb-jar_3_3_2.dtd",
    };

    /**
     * List of jonas-ejb-jar publicId.
     */
    private static final String[] JONAS_EJBJAR_DTDS_PUBLIC_ID = new String[] {
        "-//ObjectWeb//DTD JOnAS 2.4//EN",
        "-//ObjectWeb//DTD JOnAS 2.5//EN",
        "-//ObjectWeb//DTD JOnAS 3.0//EN",
        "-//ObjectWeb//DTD JOnAS 3.2//EN",
        "-//ObjectWeb//DTD JOnAS 3.3//EN",
        "-//ObjectWeb//DTD JOnAS 3.3.1//EN",
        "-//ObjectWeb//DTD JOnAS 3.3.2//EN",
    };




    /**
     * Build a new object for web.xml DTDs handling.
     */
    public JonasEjbjarDTDs() {
        super();
        addMapping(JONAS_EJBJAR_DTDS, JONAS_EJBJAR_DTDS_PUBLIC_ID, JonasEjbjarDTDs.class.getClassLoader());
    }

}
