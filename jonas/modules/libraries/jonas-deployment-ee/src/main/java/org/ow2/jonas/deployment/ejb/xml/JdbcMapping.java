/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;
/**
 * This class defines the implementation of the element jdbc-mapping
 *
 * @author JOnAS team
 */

public class JdbcMapping extends AbsElement  {

    /**
     * jndi-name
     */
    private String jndiName = null;

    /**
     * jdbc-table-name
     */
    private String jdbcTableName = null;

    /**
     * automatic-pk
     */
    private String automaticPk = null;

    /**
     * jdbcAutomaticPkFieldName
     */
    private String jdbcAutomaticPkFieldName = null;

    /**
     * cmp-field-jdbc-mapping
     */
    private JLinkedList cmpFieldJdbcMappingList = null;

    /**
     * finder-method-jdbc-mapping
     */
    private JLinkedList finderMethodJdbcMappingList = null;


    /**
     * Constructor
     */
    public JdbcMapping() {
        super();
        cmpFieldJdbcMappingList = new  JLinkedList("cmp-field-jdbc-mapping");
        finderMethodJdbcMappingList = new  JLinkedList("finder-method-jdbc-mapping");
    }

    /**
     * Gets the jndi-name
     * @return the jndi-name
     */
    public String getJndiName() {
        return jndiName;
    }

    /**
     * Set the jndi-name
     * @param jndiName jndiName
     */
    public void setJndiName(String jndiName) {
        this.jndiName = jndiName;
    }

    /**
     * Gets the jdbc-table-name
     * @return the jdbc-table-name
     */
    public String getJdbcTableName() {
        return jdbcTableName;
    }

    /**
     * Set the jdbc-table-name
     * @param jdbcTableName jdbcTableName
     */
    public void setJdbcTableName(String jdbcTableName) {
        this.jdbcTableName = jdbcTableName;
    }

    /**
     * Set the jdbcAutomaticPkFieldName
     * @param jdbcAutomaticPkFieldName jdbcAutomaticPkFieldName
     */
    public void setJdbcAutomaticPkFieldName(String jdbcAutomaticPkFieldName) {
        this.jdbcAutomaticPkFieldName = jdbcAutomaticPkFieldName;
    }

    /**
     * Gets the jdbcAutomaticPkFieldName
     * @return the jdbcAutomaticPkFieldName
     */
    public String getJdbcAutomaticPkFieldName() {
        return jdbcAutomaticPkFieldName;
    }

    /**
     * Gets the cmp-field-jdbc-mapping
     * @return the cmp-field-jdbc-mapping
     */
    public JLinkedList getCmpFieldJdbcMappingList() {
        return cmpFieldJdbcMappingList;
    }

    /**
     * Get the automatic-pk
     * @return automaticPk String Value
     */
    public String getAutomaticPk() {
        return automaticPk;
    }

    /**
     * Set the automatic-pk
     * @param automaticPk automaticPk
     */
    public void setAutomaticPk(String automaticPk) {
        this.automaticPk = automaticPk;
    }

    /**
     * Set the cmp-field-jdbc-mapping
     * @param cmpFieldJdbcMappingList cmpFieldJdbcMapping
     */
    public void setCmpFieldJdbcMappingList(JLinkedList cmpFieldJdbcMappingList) {
        this.cmpFieldJdbcMappingList = cmpFieldJdbcMappingList;
    }

    /**
     * Add a new  cmp-field-jdbc-mapping element to this object
     * @param cmpFieldJdbcMapping the cmpFieldJdbcMappingobject
     */
    public void addCmpFieldJdbcMapping(CmpFieldJdbcMapping cmpFieldJdbcMapping) {
        cmpFieldJdbcMappingList.add(cmpFieldJdbcMapping);
    }

    /**
     * Gets the finder-method-jdbc-mapping
     * @return the finder-method-jdbc-mapping
     */
    public JLinkedList getFinderMethodJdbcMappingList() {
        return finderMethodJdbcMappingList;
    }

    /**
     * Set the finder-method-jdbc-mapping
     * @param finderMethodJdbcMappingList finderMethodJdbcMapping
     */
    public void setFinderMethodJdbcMappingList(JLinkedList finderMethodJdbcMappingList) {
        this.finderMethodJdbcMappingList = finderMethodJdbcMappingList;
    }

    /**
     * Add a new  finder-method-jdbc-mapping element to this object
     * @param finderMethodJdbcMapping the finderMethodJdbcMappingobject
     */
    public void addFinderMethodJdbcMapping(FinderMethodJdbcMapping finderMethodJdbcMapping) {
        finderMethodJdbcMappingList.add(finderMethodJdbcMapping);
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<jdbc-mapping>\n");

        indent += 2;

        // jndi-name
        sb.append(xmlElement(jndiName, "jndi-name", indent));
        // jdbc-table-name
        sb.append(xmlElement(jdbcTableName, "jdbc-table-name", indent));
        // automatic-pk
        sb.append(xmlElement(automaticPk, "automatic-pk", indent));
        // jdbcAutomaticPkFieldName
        sb.append(xmlElement(jdbcAutomaticPkFieldName, "automatic-pk-field-name", indent));
        // cmp-field-jdbc-mapping
        sb.append(cmpFieldJdbcMappingList.toXML(indent));
        // finder-method-jdbc-mapping
        sb.append(finderMethodJdbcMappingList.toXML(indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</jdbc-mapping>\n");

        return sb.toString();
    }
}
