/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A. 
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;

/** 
 * This class defines the implementation of the element config-property
 * 
 * @author Florent Benoit
 */

public class ConfigProperty extends AbsElement  {

    /**
     * description
     */ 
    private JLinkedList descriptionList = null;

    /**
     * config-property-name
     */ 
    private String configPropertyName = null;

    /**
     * config-property-type
     */ 
    private String configPropertyType = null;

    /**
     * config-property-value
     */ 
    private String configPropertyValue = null;


    /**
     * Constructor
     */
    public ConfigProperty() {
        super();
        descriptionList = new  JLinkedList("description");
    }

    /** 
     * Gets the description
     * @return the description
     */
    public JLinkedList getDescriptionList() {
        return descriptionList;
    }

    /** 
     * Set the description
     * @param descriptionList description
     */
    public void setDescriptionList(JLinkedList descriptionList) {
        this.descriptionList = descriptionList;
    }

    /** 
     * Add a new  description element to this object
     * @param description the description String
     */
    public void addDescription(String description) {
        descriptionList.add(description);
    }


    /** 
     * Gets the config-property-name
     * @return the config-property-name
     */
    public String getConfigPropertyName() {
        return configPropertyName;
    }

    /** 
     * Set the config-property-name
     * @param configPropertyName configPropertyName
     */
    public void setConfigPropertyName(String configPropertyName) {
        this.configPropertyName = configPropertyName;
    }

    /** 
     * Gets the config-property-type
     * @return the config-property-type
     */
    public String getConfigPropertyType() {
        return configPropertyType;
    }

    /** 
     * Set the config-property-type
     * @param configPropertyType configPropertyType
     */
    public void setConfigPropertyType(String configPropertyType) {
        this.configPropertyType = configPropertyType;
    }

    /** 
     * Gets the config-property-value
     * @return the config-property-value
     */
    public String getConfigPropertyValue() {
        return configPropertyValue;
    }

    /** 
     * Set the config-property-value
     * @param configPropertyValue configPropertyValue
     */
    public void setConfigPropertyValue(String configPropertyValue) {
        this.configPropertyValue = configPropertyValue;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prefixing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<config-property>\n");

        indent += 2;

        // description
        sb.append(descriptionList.toXML(indent));
        // config-property-name
        sb.append(xmlElement(configPropertyName, "config-property-name", indent));
        // config-property-type
        sb.append(xmlElement(configPropertyType, "config-property-type", indent));
        // config-property-value
        sb.append(xmlElement(configPropertyValue, "config-property-value", indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</config-property>\n");

        return sb.toString();
    }
}
