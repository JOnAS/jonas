/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.web;

import org.ow2.jonas.deployment.common.CommonsDTDs;
import org.ow2.jonas.deployment.common.util.ResourceHelper;

/**
 * This class defines the declarations of DTDs for jonas-web.xml.
 * @author Florent Benoit
 */
public class JonasWebAppDTDs extends CommonsDTDs {

    /**
     * Package name.
     */
    private static final String PACKAGE = ResourceHelper.getResourcePackage(JonasWebAppDTDs.class);

    /**
     * List of jonas-web-app dtds.
     */
    private static final String[] JONAS_WEBAPP_DTDS = new String[] {
        PACKAGE + "jonas-web-app_2_6.dtd",
        PACKAGE + "jonas-web-app_3_1.dtd",
        PACKAGE + "jonas-web-app_3_3.dtd"
    };

    /**
     * List of jonas-web-app publicId.
     */
    private static final String[] JONAS_WEBAPP_DTDS_PUBLIC_ID = new String[] {
        "-//ObjectWeb//DTD JOnAS Web App 2.6//EN",
        "-//ObjectWeb//DTD JOnAS Web App 3.1//EN",
        "-//ObjectWeb//DTD JOnAS Web App 3.3//EN"
    };

    /**
     * DTDs of jetty (used by WS for security).
     */
    private static final String[] JETTY_DTDS = new String[] {
        PACKAGE + "configure_1_1.dtd",
        PACKAGE + "configure_1_2.dtd"
    };

    /**
     * List of Public ID of jetty (used by WS for security).
     */
    private static final String[] JETTY_DTDS_PUBLIC_ID = new String[] {
        "-//Mort Bay Consulting//DTD Configure 1.1//EN",
        "-//Mort Bay Consulting//DTD Configure 1.2//EN"
    };

    /**
     * Build a new object for jonas-web.xml DTDs handling.
     */
    public JonasWebAppDTDs() {
        super();
        addMapping(JONAS_WEBAPP_DTDS, JONAS_WEBAPP_DTDS_PUBLIC_ID, JonasWebAppDTDs.class.getClassLoader());
        addMapping(JETTY_DTDS, JETTY_DTDS_PUBLIC_ID, JonasWebAppDTDs.class.getClassLoader());
    }


}
