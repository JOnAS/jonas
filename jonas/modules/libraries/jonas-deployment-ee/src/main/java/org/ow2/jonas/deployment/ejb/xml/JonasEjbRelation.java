/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;
/**
 * This class defines the implementation of the element jonas-ejb-relation
 *
 * @author JOnAS team
 */

public class JonasEjbRelation extends AbsElement  {

    /**
     * ejb-relation-name
     */
    private String ejbRelationName = null;

    /**
     * jdbc-table-name
     */
    private String jdbcTableName = null;

    /**
     * jonas-ejb-relationship-role
     */
    private JLinkedList jonasEjbRelationshipRoleList = null;


    /**
     * Constructor
     */
    public JonasEjbRelation() {
        super();
        jonasEjbRelationshipRoleList = new  JLinkedList("jonas-ejb-relationship-role");
    }

    /**
     * Gets the ejb-relation-name
     * @return the ejb-relation-name
     */
    public String getEjbRelationName() {
        return ejbRelationName;
    }

    /**
     * Set the ejb-relation-name
     * @param ejbRelationName ejbRelationName
     */
    public void setEjbRelationName(String ejbRelationName) {
        this.ejbRelationName = ejbRelationName;
    }

    /**
     * Gets the jdbc-table-name
     * @return the jdbc-table-name
     */
    public String getJdbcTableName() {
        return jdbcTableName;
    }

    /**
     * Set the jdbc-table-name
     * @param jdbcTableName jdbcTableName
     */
    public void setJdbcTableName(String jdbcTableName) {
        this.jdbcTableName = jdbcTableName;
    }

    /**
     * Gets the jonas-ejb-relationship-role
     * @return the jonas-ejb-relationship-role
     */
    public JLinkedList getJonasEjbRelationshipRoleList() {
        return jonasEjbRelationshipRoleList;
    }

    /**
     * Set the jonas-ejb-relationship-role
     * @param jonasEjbRelationshipRoleList jonasEjbRelationshipRole
     */
    public void setJonasEjbRelationshipRoleList(JLinkedList jonasEjbRelationshipRoleList) {
        this.jonasEjbRelationshipRoleList = jonasEjbRelationshipRoleList;
    }

    /**
     * Add a new  jonas-ejb-relationship-role element to this object
     * @param jonasEjbRelationshipRole the jonasEjbRelationshipRoleobject
     */
    public void addJonasEjbRelationshipRole(JonasEjbRelationshipRole jonasEjbRelationshipRole) {
        jonasEjbRelationshipRoleList.add(jonasEjbRelationshipRole);
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<jonas-ejb-relation>\n");

        indent += 2;

        // ejb-relation-name
        sb.append(xmlElement(ejbRelationName, "ejb-relation-name", indent));
        // jdbc-table-name
        sb.append(xmlElement(jdbcTableName, "jdbc-table-name", indent));
        // jonas-ejb-relationship-role
        sb.append(jonasEjbRelationshipRoleList.toXML(indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</jonas-ejb-relation>\n");

        return sb.toString();
    }
}
