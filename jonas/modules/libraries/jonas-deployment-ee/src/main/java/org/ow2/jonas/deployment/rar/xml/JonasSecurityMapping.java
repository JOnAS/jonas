/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A. 
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;
/** 
 * This class defines the implementation of the element jonas-security-mapping
 * 
 * @author Eric Hardesty
 */

public class JonasSecurityMapping extends AbsElement  {

    /**
     * security-entry
     */ 
    private JLinkedList securityEntryList = null;

    /**
     * Constructor
     */
    public JonasSecurityMapping() {
        super();
        securityEntryList = new  JLinkedList("security-entry");
    }

    /** 
     * Gets the security-entry
     * @return the security-entry
     */
    public JLinkedList getSecurityEntryList() {
        return securityEntryList;
    }

    /** 
     * Set the security-entry
     * @param securityEntryList securityEntryList
     */
    public void setSecurityEntryList(JLinkedList securityEntryList) {
        this.securityEntryList = securityEntryList;
    }

    /** 
     * Add a new security-entry element to this object
     * @param securityEntry the securityEntry object
     */
    public void addSecurityEntry(SecurityEntry securityEntry) {
        securityEntryList.add(securityEntry);
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prefixing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<jonas-security-mapping>\n");

        indent += 2;

        // config-property
        sb.append(securityEntryList.toXML(indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</jonas-security-mapping>\n");

        return sb.toString();
    }
}
