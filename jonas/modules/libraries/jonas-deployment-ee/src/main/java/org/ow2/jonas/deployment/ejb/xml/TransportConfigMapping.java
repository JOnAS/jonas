/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
/**
 * This class defines the implementation of the element transport-config
 *
 * @author JOnAS team
 */

public class TransportConfigMapping extends AbsElement  {

    /**
     * integrity
     */
    private String integrity = null;

    /**
     * confidentiality
     */
    private String confidentiality = null;

    /**
     * establish-trust-in-target
     */
    private String establishTrustInTarget = null;

    /**
     * establish-trust-in-client
     */
    private String establishTrustInClient = null;


    /**
     * @return Returns the confidentiality.
     */
    public String getConfidentiality() {
        return confidentiality;
    }
    /**
     * @param confidentiality The confidentiality to set.
     */
    public void setConfidentiality(String confidentiality) {
        this.confidentiality = confidentiality;
    }
    /**
     * @return Returns the establishTrustInClient.
     */
    public String getEstablishTrustInClient() {
        return establishTrustInClient;
    }
    /**
     * @param establishTrustInClient The establishTrustInClient to set.
     */
    public void setEstablishTrustInClient(String establishTrustInClient) {
        this.establishTrustInClient = establishTrustInClient;
    }
    /**
     * @return Returns the establishTrustInTarget.
     */
    public String getEstablishTrustInTarget() {
        return establishTrustInTarget;
    }
    /**
     * @param establishTrustInTarget The establishTrustInTarget to set.
     */
    public void setEstablishTrustInTarget(String establishTrustInTarget) {
        this.establishTrustInTarget = establishTrustInTarget;
    }
    /**
     * @return Returns the integrity.
     */
    public String getIntegrity() {
        return integrity;
    }
    /**
     * @param integrity The integrity to set.
     */
    public void setIntegrity(String integrity) {
        this.integrity = integrity;
    }

    /**
     * Constructor
     */
    public TransportConfigMapping() {
        super();
   }



    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<transport-config>\n");

        indent += 2;

        // integrity
        sb.append(xmlElement(integrity, "integrity", indent));
        // confidentiality
        sb.append(xmlElement(confidentiality, "confidentiality", indent));
        // establish-trust-in-target
        sb.append(xmlElement(establishTrustInTarget, "establish-trust-in-target", indent));
        // establish-trust-in-client
        sb.append(xmlElement(establishTrustInClient, "establish-trust-in-client", indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</transport-config>\n");

        return sb.toString();
    }
}
