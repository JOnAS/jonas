/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar;

import java.io.Serializable;
import java.util.List;

import org.ow2.jonas.deployment.rar.xml.JonasConnectionDefinition;


/**
 * This class defines the implementation of the element jonas-connection-definition
 *
 * @author Eric Hardesty
 */

public class JonasConnectionDefinitionDesc implements Serializable {

    /**
     * id
     */
    private String id = null;

    /**
     * description
     */ 
    private List descriptionList = null;

    /**
     * jndi-name
     */
    private String jndiName = null;

    /**
     * log-enabled
     */
    private String logEnabled = null;

    /**
     * log-topic
     */
    private String logTopic = null;

    /**
     * pool-params
     */
    private PoolParamsDesc poolParamsDesc = null;

    /**
     * jdbc-conn-params
     */
    private JdbcConnParamsDesc jdbcConnParamsDesc = null;

    /**
     * jonas-config-property
     */
    private List jonasConfigPropertyList = null;


    /**
     * Constructor
     */
    public JonasConnectionDefinitionDesc(JonasConnectionDefinition jcd) {
        if (jcd != null) {
            id = jcd.getId();
            descriptionList = jcd.getDescriptionList();
            jndiName = jcd.getJndiName();
            logEnabled = jcd.getLogEnabled();
            logTopic = jcd.getLogTopic();
            poolParamsDesc = new PoolParamsDesc(jcd.getPoolParams());
            jdbcConnParamsDesc = new JdbcConnParamsDesc(jcd.getJdbcConnParams());
            jonasConfigPropertyList = Utility.jonasConfigProperty(jcd.getJonasConfigPropertyList());
        }
    }

    /**
     * Gets the id
     * @return the id
     */
    public String getId() {
        return id;
    }

    /** 
     * Gets the description
     * @return the description
     */
    public List getDescriptionList() {
        return descriptionList;
    }

    /**
     * Gets the jndi-name
     * @return the jndi-name
     */
    public String getJndiName() {
        return jndiName;
    }

    /**
     * Gets the log-enabled
     * @return the log-enabled
     */
    public String getLogEnabled() {
        return logEnabled;
    }

    /**
     * Gets the log-topic
     * @return the log-topic
     */
    public String getLogTopic() {
        return logTopic;
    }

    /**
     * Gets the pool-params
     * @return the pool-params
     */
    public PoolParamsDesc getPoolParamsDesc() {
        return poolParamsDesc;
    }

    /**
     * Gets the jdbc-conn-params
     * @return the jdbc-conn-params
     */
    public JdbcConnParamsDesc getJdbcConnParamsDesc() {
        return jdbcConnParamsDesc;
    }

    /**
     * Gets the jonas-config-property
     * @return the jonas-config-property
     */
    public List getJonasConfigPropertyList() {
        return jonasConfigPropertyList;
    }

}
