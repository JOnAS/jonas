/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.ws.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;

/**
 * This class defines the implementation of the element webservice-description
 *
 * @author JOnAS team
 */

public class WebserviceDescription extends AbsElement  {

    /**
     * description
     */
    private String description = null;

    /**
     * display-name
     */
    private String displayName = null;

    /**
     * small-icon
     */
    private String smallIcon = null;

    /**
     * large-icon
     */
    private String largeIcon = null;

    /**
     * webservice-description-name
     */
    private String webserviceDescriptionName = null;

    /**
     * wsdl-file
     */
    private String wsdlFile = null;

    /**
     * jaxrpc-mapping-file
     */
    private String jaxrpcMappingFile = null;

    /**
     * port-component
     */
    private JLinkedList portComponentList = null;


    /**
     * Constructor
     */
    public WebserviceDescription() {
        super();
        portComponentList = new  JLinkedList("port-component");
    }

    /**
     * Gets the description
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the description
     * @param description description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the display-name
     * @return the display-name
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Set the display-name
     * @param displayName displayName
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * Gets the small-icon
     * @return the small-icon
     */
    public String getSmallIcon() {
        return smallIcon;
    }

    /**
     * Set the small-icon
     * @param smallIcon smallIcon
     */
    public void setSmallIcon(String smallIcon) {
        this.smallIcon = smallIcon;
    }

    /**
     * Gets the large-icon
     * @return the large-icon
     */
    public String getLargeIcon() {
        return largeIcon;
    }

    /**
     * Set the large-icon
     * @param largeIcon largeIcon
     */
    public void setLargeIcon(String largeIcon) {
        this.largeIcon = largeIcon;
    }

    /**
     * Gets the webservice-description-name
     * @return the webservice-description-name
     */
    public String getWebserviceDescriptionName() {
        return webserviceDescriptionName;
    }

    /**
     * Set the webservice-description-name
     * @param webserviceDescriptionName webserviceDescriptionName
     */
    public void setWebserviceDescriptionName(String webserviceDescriptionName) {
        this.webserviceDescriptionName = webserviceDescriptionName;
    }

    /**
     * Gets the wsdl-file
     * @return the wsdl-file
     */
    public String getWsdlFile() {
        return wsdlFile;
    }

    /**
     * Set the wsdl-file
     * @param wsdlFile wsdlFile
     */
    public void setWsdlFile(String wsdlFile) {
        this.wsdlFile = wsdlFile;
    }

    /**
     * Gets the jaxrpc-mapping-file
     * @return the jaxrpc-mapping-file
     */
    public String getJaxrpcMappingFile() {
        return jaxrpcMappingFile;
    }

    /**
     * Set the jaxrpc-mapping-file
     * @param jaxrpcMappingFile jaxrpcMappingFile
     */
    public void setJaxrpcMappingFile(String jaxrpcMappingFile) {
        this.jaxrpcMappingFile = jaxrpcMappingFile;
    }

    /**
     * Gets the port-component
     * @return the port-component
     */
    public JLinkedList getPortComponentList() {
        return portComponentList;
    }

    /**
     * Set the port-component
     * @param portComponentList portComponent
     */
    public void setPortComponentList(JLinkedList portComponentList) {
        this.portComponentList = portComponentList;
    }

    /**
     * Add a new  port-component element to this object
     * @param portComponent the portComponentobject
     */
    public void addPortComponent(PortComponent portComponent) {
        portComponentList.add(portComponent);
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<webservice-description>\n");

        indent += 2;

        // description
        sb.append(xmlElement(description, "description", indent));
        // display-name
        sb.append(xmlElement(displayName, "display-name", indent));
        // small-icon
        sb.append(xmlElement(smallIcon, "small-icon", indent));
        // large-icon
        sb.append(xmlElement(largeIcon, "large-icon", indent));
        // webservice-description-name
        sb.append(xmlElement(webserviceDescriptionName, "webservice-description-name", indent));
        // wsdl-file
        sb.append(xmlElement(wsdlFile, "wsdl-file", indent));
        // jaxrpc-mapping-file
        sb.append(xmlElement(jaxrpcMappingFile, "jaxrpc-mapping-file", indent));
        // port-component
        sb.append(portComponentList.toXML(indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</webservice-description>\n");

        return sb.toString();
    }
}
