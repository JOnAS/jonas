/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;

/**
 * This class defines the implementation of the element inboundResourceadapter
 *
 * @author Eric Hardesty
 */

public class InboundResourceadapter extends AbsElement  {

    /**
     * authentication-mechanism
     */
    private Messageadapter messageadapter = null;

    /**
     * Constructor
     */
    public InboundResourceadapter() {
        super();
    }

    /**
     * Gets the messageadapter
     * @return the messageadapter
     */
    public Messageadapter getMessageadapter() {
        return messageadapter;
    }

    /**
     * Set the messageadapter
     * @param messageadapter messageadapter
     */
    public void setMessageadapter(Messageadapter messageadapter) {
        this.messageadapter = messageadapter;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prefixing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<inbound-resourceadapter>\n");

        indent += 2;

        // messageadapter
        if (messageadapter != null) {
            sb.append(messageadapter.toXML(indent));
        }
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</inbound-resourceadapter>\n");

        return sb.toString();
    }
}
