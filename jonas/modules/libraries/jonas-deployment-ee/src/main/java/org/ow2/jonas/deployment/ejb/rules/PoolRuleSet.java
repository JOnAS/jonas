/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.rules;

import org.apache.commons.digester.Digester;
import org.ow2.jonas.deployment.common.rules.JRuleSetBase;

/**
 * Rule set for the pool configuration of a clustered object.
 * @author eyindanga
 *
 */
public class PoolRuleSet extends JRuleSetBase {

    /**Constructor.
     * @param prefix Rule set prefix
     */
    public PoolRuleSet(final String prefix) {
        super(prefix);
    }

    @Override
    public void addRuleInstances(final Digester digester) {
        digester.addObjectCreate(prefix + "pool",
        "org.ow2.cmi.info.mapping.PoolMappingForJonas");
        digester.addSetNext(prefix + "pool",
                "setPoolConfiguration",
        "org.ow2.cmi.info.mapping.PoolMappingForJonas");
        digester.addCallMethod(prefix + "pool/max-size", "setMax", 0);
        digester.addCallMethod(prefix + "pool/max-waiters", "setMaxWaiters", 0);
        digester.addCallMethod(prefix + "pool/timeout", "setTimeout", 0);
    }

}
