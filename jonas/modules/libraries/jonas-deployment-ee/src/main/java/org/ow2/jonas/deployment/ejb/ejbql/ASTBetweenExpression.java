/* Generated By:JJTree: Do not edit this line. ASTBetweenExpression.java */

package org.ow2.jonas.deployment.ejb.ejbql;

public class ASTBetweenExpression extends SimpleNode {
  public ASTBetweenExpression(int id) {
    super(id);
  }

  public ASTBetweenExpression(EJBQL p, int id) {
    super(p, id);
  }


  /** Accept the visitor. **/
  public Object jjtAccept(EJBQLVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
}
