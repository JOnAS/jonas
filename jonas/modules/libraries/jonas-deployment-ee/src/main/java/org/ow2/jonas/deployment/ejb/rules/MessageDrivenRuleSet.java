/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.rules;

import org.ow2.jonas.deployment.common.rules.EnvironmentRuleSet;
import org.ow2.jonas.deployment.common.rules.JRuleSetBase;

import org.apache.commons.digester.Digester;


/**
 * This class defines the rules to analyze the element message-driven
 *
 * @author JOnAS team
 */

public class MessageDrivenRuleSet extends JRuleSetBase {

    /**
     * Construct an object with a specific prefix
     * @param prefix prefix to use during the parsing
     */
    public MessageDrivenRuleSet(String prefix) {
        super(prefix);
   }
     /**
     * Add a set of rules to the digester object
     * @param digester Digester instance
     */

    public void addRuleInstances(Digester digester) {
        digester.addObjectCreate(prefix + "message-driven",
                                 "org.ow2.jonas.deployment.ejb.xml.MessageDriven");
        digester.addSetNext(prefix + "message-driven",
                            "addMessageDriven",
                            "org.ow2.jonas.deployment.ejb.xml.MessageDriven");
        digester.addCallMethod(prefix + "message-driven/description",
                               "setDescription", 0);
        digester.addCallMethod(prefix + "message-driven/display-name",
                               "setDisplayName", 0);
        digester.addCallMethod(prefix + "message-driven/small-icon",
                               "setSmallIcon", 0);
        digester.addCallMethod(prefix + "message-driven/large-icon",
                               "setLargeIcon", 0);
        digester.addCallMethod(prefix + "message-driven/ejb-name",
                               "setEjbName", 0);
        digester.addCallMethod(prefix + "message-driven/ejb-class",
                               "setEjbClass", 0);
        digester.addCallMethod(prefix + "message-driven/transaction-type",
                               "setTransactionType", 0);
        /* EJB 2.0 Specific Part */
        digester.addCallMethod(prefix + "message-driven/message-selector",
                               "setMessageSelector", 0);
        digester.addCallMethod(prefix + "message-driven/acknowledge-mode",
                               "setAcknowledgeMode", 0);
        digester.addRuleSet(new MessageDrivenDestinationRuleSet(prefix + "message-driven/"));
        /* EJB 2.1 Specific Part */
        digester.addCallMethod(prefix + "message-driven/messaging-type",
                               "setMessagingType", 0);
        digester.addCallMethod(prefix + "message-driven/message-destination-type",
                               "setMessageDestinationType", 0);
        digester.addCallMethod(prefix + "message-driven/message-destination-link",
                               "setMessageDestinationLink", 0);
        digester.addRuleSet(new ActivationConfigRuleSet(prefix + "message-driven/"));

        /* End of EJB 2.1 Specific Part */
        digester.addRuleSet(new EnvironmentRuleSet(prefix + "message-driven/"));
        digester.addRuleSet(new SecurityIdentityRuleSet(prefix + "message-driven/"));

   }
}
