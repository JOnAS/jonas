/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.rules;

import java.lang.reflect.Method;

import org.apache.commons.digester.Digester;
import org.apache.commons.digester.Rule;

import org.ow2.jonas.deployment.common.rules.JRuleSetBase;
import org.xml.sax.Attributes;

/**
 * This class defines the rules to analyze the element ejb-jar
 *
 * @author JOnAS team
 */

public class EjbJarRuleSet extends JRuleSetBase {

    /**
     * Construct an object with a specific prefix
     */
    public EjbJarRuleSet() {
        super("ejb-jar/");
    }

    /**
     * Add a set of rules to the digester object
     *
     * @param digester Digester instance
     */

    public void addRuleInstances(Digester digester) {
        digester.addRule("ejb-jar", new SetPublicIdRule("setPublicId"));
        digester.addSetProperties("ejb-jar");
        digester.addCallMethod(prefix + "description", "setDescription", 0);
        digester.addCallMethod(prefix + "display-name", "setDisplayName", 0);
        digester.addCallMethod(prefix + "small-icon", "setSmallIcon", 0);
        digester.addCallMethod(prefix + "large-icon", "setLargeIcon", 0);
        digester.addRuleSet(new EnterpriseBeansRuleSet(prefix));
        digester.addRuleSet(new RelationshipsRuleSet(prefix));
        digester.addRuleSet(new AssemblyDescriptorRuleSet(prefix));
        digester.addCallMethod(prefix + "ejb-client-jar", "setEjbClientJar", 0);
    }

    /**
     * Class that calls a property setter for the top object on the stack,
     * passing the public ID of the entity we are currently processing.
     */

    private final class SetPublicIdRule extends Rule {

        /**
         * Construct a PublicId Setter object
         *
         * @param method method name
         */
        public SetPublicIdRule(String method) {
            this.method = method;
        }

        /**
         * method name
         */
        private String method = null;

        /**
         * Invoke the given method on the TopLevelElement
         *
         * @param attributes contains publicId value
         *
         * @throws Exception When invokation through reflection fail.
         */
        public void begin(Attributes attributes) throws Exception {
            Object top = digester.peek();
            Class[] paramClasses = new Class[1];
            paramClasses[0] = "String".getClass();
            String[] paramValues = new String[1];
            paramValues[0] = digester.getPublicId();

            Method m = null;
            try {
                m = top.getClass().getMethod(method, paramClasses);
            } catch (NoSuchMethodException e) {
                System.out.println("Can't find method " + method + " in " + top + " CLASS " + top.getClass());
                return;
            }

            m.invoke(top, (Object[]) paramValues);

        }

    }
}