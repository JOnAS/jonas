/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ws.wrapper;

import java.io.File;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.ow2.jonas.deployment.ws.MappingFile;
import org.ow2.jonas.deployment.ws.WSDeploymentDescException;
import org.ow2.jonas.lib.bootstrap.LoaderManager;




/**
 * Wrap the MappingFileManager to solve ClassLoader problems linked to
 * Digester.
 *
 * @author Guillaume Sauthier
 */
public class MappingFileManagerWrapper {

    /**
     * MappingFileManager fully qualified classname
     */
    private static final String MAPPINGFILEMANAGER_CLASSNAME = "org.ow2.jonas.deployment.ws.lib.MappingFileManager";

    /**
     * Private Empty Constructor for utility class
     */
    private MappingFileManagerWrapper() { }

    /**
     * Wrap MappingFileManager.getInstance(module, filename) call.
     *
     * @param module directory where mapping file can be found
     * @param filename mapping file filename
     *
     * @return the MappingFile
     *
     * @throws WSDeploymentDescException When MappingFile cannot be instanciated
     */
    public static MappingFile getMappingFile(final File module, final String filename)
    throws WSDeploymentDescException {
        LoaderManager lm = LoaderManager.getInstance();
        MappingFile mf = null;

        try {
            ClassLoader ext = lm.getExternalLoader();
            Class manager = ext.loadClass(MAPPINGFILEMANAGER_CLASSNAME);
            Method m = manager.getDeclaredMethod("getInstance", new Class[] {File.class, String.class});
            mf = (MappingFile) m.invoke(null, new Object[] {module, filename});
        } catch (InvocationTargetException ite) {
            Throwable t = ite.getTargetException();
            if (WSDeploymentDescException.class.isInstance(t)) {
                throw (WSDeploymentDescException) ite.getTargetException();
            } else {
                throw new WSDeploymentDescException("MappingFileManager.getInstance fails", t);
            }
        } catch (Exception e) {
            // TODO add i18n here
            throw new WSDeploymentDescException("Problems when using reflection on MappingFileManager", e);
        }

        return mf;
    }

    /**
     * Wrap MappingFileManager.getInstance(module, filename) call.
     *
     * @param is InputStream of the MappingFile
     * @param filename mapping file filename
     *
     * @return the MappingFile
     *
     * @throws WSDeploymentDescException When MappingFile cannot be instanciated
     */
    public static MappingFile getMappingFile(final InputStream is, final String filename)
    throws WSDeploymentDescException {
        LoaderManager lm = LoaderManager.getInstance();
        MappingFile mf = null;

        try {
            ClassLoader ext = lm.getExternalLoader();
            Class manager = ext.loadClass(MAPPINGFILEMANAGER_CLASSNAME);
            Method m = manager.getDeclaredMethod("getInstance", new Class[] {InputStream.class, String.class});
            mf = (MappingFile) m.invoke(null, new Object[] {is, filename});
        } catch (InvocationTargetException ite) {
            Throwable t = ite.getTargetException();
            if (WSDeploymentDescException.class.isInstance(t)) {
                throw (WSDeploymentDescException) ite.getTargetException();
            } else {
                throw new WSDeploymentDescException("MappingFileManager.getInstance fails", t);
            }
        } catch (Exception e) {
            // TODO add i18n here
            throw new WSDeploymentDescException("Problems when using reflection on MappingFileManager", e);
        }

        return mf;
    }
}
