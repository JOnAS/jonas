/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A. 
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar;

import java.io.Serializable;

import org.ow2.jonas.deployment.rar.xml.SecurityEntry;

/** 
 * This class defines the implementation of the element security-entry
 * 
 * @author Eric Hardesty
 */

public class SecurityEntryDesc implements Serializable {

    /**
     * principalName
     */ 
    private String principalName = null;

    /**
     * user
     */ 
    private String user = null;

    /**
     * password
     */ 
    private String password = null;

    /**
     * encrypted
     */ 
    private String encrypted = null;

    /**
     * Constructor
     */
    public SecurityEntryDesc(SecurityEntry se) {
        if (se != null) {
            principalName = se.getPrincipalName();
            user = se.getUser();
            password = se.getPassword();
            encrypted = se.getEncrypted();
        }
    }

    /** 
     * Gets the principalName
     * @return the principalName
     */
    public String getPrincipalName() {
        return principalName;
    }

    /** 
     * Gets the user
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /** 
     * Gets the password
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /** 
     * Gets the encrypted value
     * @return the encrypted value
     */
    public String getEncypted() {
        return encrypted;
    }

}
