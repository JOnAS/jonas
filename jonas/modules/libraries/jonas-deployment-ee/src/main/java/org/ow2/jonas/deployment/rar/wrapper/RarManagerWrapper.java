/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar.wrapper;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.naming.Context;

import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;
import org.ow2.jonas.deployment.common.DeploymentDescException;
import org.ow2.jonas.deployment.rar.RarDeploymentDesc;
import org.ow2.jonas.deployment.rar.RarDeploymentDescException;
import org.ow2.jonas.lib.bootstrap.LoaderManager;
import org.ow2.jonas.lib.util.Log;


/**
 * Wrap the RarDeploymentDescManager in this class to solve the ClassLoader problem of Digester.
 *
 * @author Guillaume Sauthier
 */
public class RarManagerWrapper {

    /**
     * RarDeploymentDescManager fully qualified classname
     */
    private static final String RAR_MANAGER_CLASSNAME = "org.ow2.jonas.deployment.rar.lib.RarDeploymentDescManager";

    /**
     * logger
     */
    private static Logger logger = Log.getLogger(Log.JONAS_EAR_PREFIX);

    /**
     * Empty private constructor for utility class
     */
    private RarManagerWrapper() {
    }

    /**
     * Returns the RarDeploymentDesc corresponding to the Rar filename.
     *
     * @param rar Rar filename
     * @param cl ClassLoader to use
     *
     * @return the RarDeploymentDesc corresponding to the Rar filename
     *
     * @throws DeploymentDescException when Rar deployment fails
     */
    public static RarDeploymentDesc getRarDeploymentDesc(final String rar, final ClassLoader cl)
    throws DeploymentDescException {
        LoaderManager lm = LoaderManager.getInstance();
        RarDeploymentDesc rarDD = null;
        try {
            ClassLoader ext = lm.getExternalLoader();
            Class manager = ext.loadClass(RAR_MANAGER_CLASSNAME);
            Method m = manager.getDeclaredMethod("getInstance", new Class[] {java.lang.String.class, java.lang.ClassLoader.class});
            rarDD = (RarDeploymentDesc) m.invoke(null, new Object[] {rar, cl});
        } catch (InvocationTargetException ite) {
            Throwable t = ite.getTargetException();
            if (RarDeploymentDescException.class.isInstance(t)) {
                throw (RarDeploymentDescException) ite.getTargetException();
            } else {
                throw new RarDeploymentDescException("RarDeploymentDescManager.getInstance fails", t);
            }
        } catch (Exception e) {
            // TODO add i18n here
            throw new RarDeploymentDescException("Problems when using reflection on RarDeploymentDescManager", e);
        }
        return rarDD;
    }

    /**
     * Wrap the RarDeploymentDescManager.setParsingWithValidation(boolean)
     *
     * @param b boolean
     */
    public static void setParsingWithValidation(final boolean b) {
        LoaderManager lm = LoaderManager.getInstance();

        try {
            ClassLoader ext = lm.getExternalLoader();
            Class manager = ext.loadClass(RAR_MANAGER_CLASSNAME);
            Method m = manager.getDeclaredMethod("setParsingWithValidation", new Class[] {boolean.class});
            m.invoke(null, new Object[] {new Boolean(b)});
        } catch (Exception e) {
            // Should never occurs
            logger.log(BasicLevel.ERROR, e);
        }
    }

    /**
     * Wrap the RarDeploymentDescManager.getInstance(Context).
     *
     * @param ctx Context
     *
     * @return RarDeploymentDesc
     *
     * @throws DeploymentDescException When init fails
     */
    public static RarDeploymentDesc getInstance(final Context ctx) throws DeploymentDescException {
        LoaderManager lm = LoaderManager.getInstance();
        RarDeploymentDesc rarDD = null;

        try {
            ClassLoader ext = lm.getExternalLoader();
            Class manager = ext.loadClass(RAR_MANAGER_CLASSNAME);
            Method m = manager.getDeclaredMethod("getInstance", new Class[] {javax.naming.Context.class});
            rarDD = (RarDeploymentDesc) m.invoke(null, new Object[] {ctx});
        } catch (InvocationTargetException ite) {
            Throwable t = ite.getTargetException();
            if (RarDeploymentDescException.class.isInstance(t)) {
                throw (RarDeploymentDescException) ite.getTargetException();
            } else {
                throw new RarDeploymentDescException("RarDeploymentDescManager.getInstance fails", t);
            }
        } catch (Exception e) {
            // TODO add i18n here
            throw new RarDeploymentDescException("Problems when using reflection on RarDeploymentDescManager", e);
        }

        return rarDD;
    }

}
