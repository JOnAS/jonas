/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.ejb;

import org.ow2.jonas.deployment.ejb.xml.ExcludeList;

/**
 * Defines a ExcludeListDesc class for the management of
 * EJBMEthodPermissions in JACC
 * @author Florent Benoit : Initial developer
 */
public class ExcludeListDesc extends CommonMethodDesc {

    /**
     * Constructor for ExcludeListDesc
     * @param excludeList excludelist which contains xml content
     */
    public ExcludeListDesc(ExcludeList excludeList) {
        super(excludeList.getMethodList());
    }

}
