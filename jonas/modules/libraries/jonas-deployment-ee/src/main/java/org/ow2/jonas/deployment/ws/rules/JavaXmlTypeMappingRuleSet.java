/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ws.rules;

import org.ow2.jonas.deployment.common.rules.JRuleSetBase;

import org.apache.commons.digester.Digester;

/**
 * This class defines the rules to analyze the element java-xml-type-mapping
 *
 * @author JOnAS team
 */

public class JavaXmlTypeMappingRuleSet extends JRuleSetBase {

    /**
     * Construct an object with a specific prefix
     * @param prefix prefix to use during the parsing
     */
    public JavaXmlTypeMappingRuleSet(String prefix) {
        super(prefix);
    }
    /**
     * Add a set of rules to the digester object
     * @param digester Digester instance
     */

    public void addRuleInstances(Digester digester) {
        digester.addObjectCreate(prefix + "java-xml-type-mapping",
                                 "org.ow2.jonas.deployment.ws.xml.JavaXmlTypeMapping");
        digester.addSetNext(prefix + "java-xml-type-mapping",
                            "addJavaXmlTypeMapping",
                            "org.ow2.jonas.deployment.ws.xml.JavaXmlTypeMapping");
        digester.addCallMethod(prefix + "java-xml-type-mapping/java-type",
                               "setJavaType", 0);
        digester.addRuleSet(new RootTypeQnameRuleSet(prefix + "java-xml-type-mapping/"));
        digester.addRuleSet(new AnonymousTypeQnameRuleSet(prefix + "java-xml-type-mapping/"));
        digester.addCallMethod(prefix + "java-xml-type-mapping/qname-scope",
                               "setQnameScope", 0);
        digester.addRuleSet(new VariableMappingRuleSet(prefix + "java-xml-type-mapping/"));
    }
}
