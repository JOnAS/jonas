/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ws.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;

/**
 * This class defines the implementation of the element jonas-webservice-description
 *
 * @author JOnAS team
 */

public class JonasPortComponent extends AbsElement  {

    /**
     * port-component-name
     */
    private String portComponentName = null;

    /**
     * endpoint-uri
     */
    private String endpointURI = null;

    /**
     * Constructor
     */
    public JonasPortComponent() {
        super();
    }

    /**
     * @return Returns the endpointURI.
     */
    public String getEndpointURI() {
        return endpointURI;
    }

    /**
     * @param endpointURI The endpointURI to set.
     */
    public void setEndpointURI(String endpointURI) {
        this.endpointURI = endpointURI;
    }

    /**
     * @return Returns the portComponentName.
     */
    public String getPortComponentName() {
        return portComponentName;
    }

    /**
     * @param portComponentName The portComponentName to set.
     */
    public void setPortComponentName(String portComponentName) {
        this.portComponentName = portComponentName;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<jonas-port-component>\n");

        indent += 2;
        // port-component-name
        sb.append(xmlElement(portComponentName, "port-component-name", indent));
        // endpoint-uri
        sb.append(xmlElement(endpointURI, "endpoint-uri", indent));
        indent -= 2;

        sb.append(indent(indent));
        sb.append("</jonas-port-component>\n");

        return sb.toString();
    }
}
