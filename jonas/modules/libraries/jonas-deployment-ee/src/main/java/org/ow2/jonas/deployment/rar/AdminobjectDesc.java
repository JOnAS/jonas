/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A. 
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar;

import java.io.Serializable;
import java.util.List;

import org.ow2.jonas.deployment.rar.xml.Adminobject;


/** 
 * This class defines the implementation of the element adminobject
 * 
 * @author Eric Hardesty
 */

public class AdminobjectDesc implements Serializable {

    /**
     * id
     */ 
    private String id = null;

    /**
     * adminobject-interface
     */ 
    private String adminobjectInterface = null;

    /**
     * adminobject-class
     */ 
    private String adminobjectClass = null;

    /**
     * config-property
     */ 
    private List configPropertyList = null;


    /**
     * Constructor
     */
    public AdminobjectDesc(Adminobject ao) {
        if (ao != null) {
            id = ao.getId();
            adminobjectInterface = ao.getAdminobjectInterface();
            adminobjectClass = ao.getAdminobjectClass();
            configPropertyList = Utility.configProperty(ao.getConfigPropertyList());
        }
    }

    /** 
     * Gets the id
     * @return the id
     */
    public String getId() {
        return id;
    }

    /** 
     * Gets the adminobject-interface
     * @return the adminobject-interface
     */
    public String getAdminobjectInterface() {
        return adminobjectInterface;
    }

    /** 
     * Gets the adminobject-class
     * @return the adminobject-class
     */
    public String getAdminobjectClass() {
        return adminobjectClass;
    }

    /** 
     * Gets the config-property
     * @return the config-property
     */
    public List getConfigPropertyList() {
        return configPropertyList;
    }
}
