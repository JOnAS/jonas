/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
/**
 * This class defines the implementation of the element foreign-key-jdbc-mapping
 *
 * @author JOnAS team
 */

public class ForeignKeyJdbcMapping extends AbsElement  {

    /**
     * foreign-key-jdbc-name
     */
    private String foreignKeyJdbcName = null;

    /**
     * key-jdbc-name
     */
    private String keyJdbcName = null;


    /**
     * Constructor
     */
    public ForeignKeyJdbcMapping() {
        super();
    }

    /**
     * Gets the foreign-key-jdbc-name
     * @return the foreign-key-jdbc-name
     */
    public String getForeignKeyJdbcName() {
        return foreignKeyJdbcName;
    }

    /**
     * Set the foreign-key-jdbc-name
     * @param foreignKeyJdbcName foreignKeyJdbcName
     */
    public void setForeignKeyJdbcName(String foreignKeyJdbcName) {
        this.foreignKeyJdbcName = foreignKeyJdbcName;
    }

    /**
     * Gets the key-jdbc-name
     * @return the key-jdbc-name
     */
    public String getKeyJdbcName() {
        return keyJdbcName;
    }

    /**
     * Set the key-jdbc-name
     * @param keyJdbcName keyJdbcName
     */
    public void setKeyJdbcName(String keyJdbcName) {
        this.keyJdbcName = keyJdbcName;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<foreign-key-jdbc-mapping>\n");

        indent += 2;

        // foreign-key-jdbc-name
        sb.append(xmlElement(foreignKeyJdbcName, "foreign-key-jdbc-name", indent));
        // key-jdbc-name
        sb.append(xmlElement(keyJdbcName, "key-jdbc-name", indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</foreign-key-jdbc-mapping>\n");

        return sb.toString();
    }
}
