/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.web;

import org.ow2.jonas.deployment.common.xml.SecurityRole;

/**
 * Class is used for the security role
 * @author Florent Benoit
 */
public class SecurityRoleDesc {

    /**
     * Name of te role
     */
    private String name = null;


    /**
     * Constructor
     * @param securityRole security role for this Description object
     */
    public SecurityRoleDesc(SecurityRole securityRole) {
        this.name = securityRole.getRoleName();
    }

    /**
     * Gets the name of the role
     * @return name of the role
     */
    public String getRoleName() {
        return name;
    }

}
