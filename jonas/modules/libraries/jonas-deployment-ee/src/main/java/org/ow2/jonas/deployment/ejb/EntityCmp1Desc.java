/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.ejb;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Iterator;

import org.ow2.jonas.deployment.common.DeploymentDescException;
import org.ow2.jonas.deployment.common.xml.JLinkedList;
import org.ow2.jonas.deployment.ejb.xml.AssemblyDescriptor;
import org.ow2.jonas.deployment.ejb.xml.Entity;
import org.ow2.jonas.deployment.ejb.xml.JonasEntity;


/**
 * Class to hold meta-information related to an entity of type CMP version 1.x.
 * Created on Jun 24, 2002
 * @author Christophe Ney [cney@batisseurs.com] : Initial developer
 * @author Helene Joanin
 */
public class EntityCmp1Desc extends EntityCmpDesc {

    protected Method isModifiedMethod = null;

    /**
     * constructor to be used by parent node
     */
    public EntityCmp1Desc(ClassLoader classLoader, Entity ent,
			  AssemblyDescriptor asd, JonasEntity jEnt,
			  JLinkedList jMDRList, String fileName)
	throws DeploymentDescException {

        super(classLoader, ent, asd, jEnt, jMDRList, fileName);

        // check if persistent fields map to fields
        for (Iterator i = fieldDesc.keySet().iterator();i.hasNext();) {
            String fn = (String)i.next();
            // check for valid field name
            try {
                Field f = ejbClass.getField(fn);
                ((FieldDesc)(fieldDesc.get(fn))).setFieldType(f.getType());
            } catch (NoSuchFieldException e) {
                throw new DeploymentDescException("Invalid field name "+fn+" in field-name in bean "+this.ejbName,e);
            } catch (SecurityException e) {
                throw new DeploymentDescException("Cannot use java reflexion on "+this.ejbClass.getName());
            }
        }

        // isModifiedMethod
        if (jEnt.getIsModifiedMethodName() != null) {
            String mName = jEnt.getIsModifiedMethodName();
            try {
                isModifiedMethod = this.ejbClass.getMethod(mName, new Class[0]);
            } catch (NoSuchMethodException e) {
                isModifiedMethod = null;
                throw new DeploymentDescException(mName + " is not a method of " + this.ejbClass.getName());
            } catch (SecurityException e) {
                throw new DeploymentDescException("Cannot use java reflexion on " + this.ejbClass.getName());
            }
        }
        if (isUndefinedPK()) {
           FieldDesc fd = this.newFieldDescInstance();
           fd.setName("JONASAUTOPKFIELD");
           fd.setPrimaryKey(true);
           fieldDesc.put("JONASAUTOPKFIELD", fd);
           ((FieldDesc) (fieldDesc.get("JONASAUTOPKFIELD"))).setFieldType(java.lang.Integer.class);
           ((FieldJdbcDesc) (fieldDesc.get("JONASAUTOPKFIELD"))).setJdbcFieldName(this.getJdbcAutomaticPkFieldName());
        }
    }

    /**
     * Get descriptor for a given field
     * @param field of the bean class
     * @return Descriptor for the given field
     */
    public FieldDesc getCmpFieldDesc(Field field) {
        FieldDesc ret = (FieldDesc) fieldDesc.get(field.getName());
        if (ret == null)
            throw new Error("Field " + field.getName() + " is not a cmp field of class " + this.ejbClass.getName());
        return ret;
    }


    /**
     * Get the 'isModified' method name implemented in the bean class.
     * (This information is JOnAS specific).
     * @return Name of the isModified method
     */
    public Method getIsModifiedMethod(){
        if (isModifiedMethod == null)
            throw new Error("No isModified method defined for bean " + this.ejbName);
        return isModifiedMethod;
    }

    /**
     * Assessor for existence of a isModified method
     * @return true of isModified method exist for the bean
     */
    public boolean hasIsModifiedMethod() {
        return isModifiedMethod != null;
    }

    /**
     * Assessor for a CMP field
     * @param field for which a descriptor is to be returned
     * @return Descriptor for the given field
     */
    public boolean hasCmpFieldDesc(Field field) {
        return fieldDesc.containsKey(field.getName());
    }

    /**
     * String representation of the object for test purpose
     * @return String representation of this object
     */
    public String toString() {
        StringBuffer ret = new StringBuffer();
        ret.append(super.toString());
        if (hasIsModifiedMethod())
            ret.append("getIsModifiedMethod()=" + getIsModifiedMethod().toString());
        return ret.toString();
    }

}
