/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.rules;

import org.apache.commons.digester.Digester;
import org.ow2.jonas.deployment.common.rules.JRuleSetBase;
import org.ow2.jonas.deployment.common.rules.JonasEnvironmentRuleSet;
import org.ow2.jonas.deployment.ejb.lib.util.ClusterUtil;

/**
 * This class defines the rules to analyze the element jonas-entity
 *
 * @author JOnAS team
 */

public class JonasEntityRuleSet extends JRuleSetBase {

    /**
     * Construct an object with a specific prefix
     * @param prefix prefix to use during the parsing
     */
    public JonasEntityRuleSet(final String prefix) {
        super(prefix);
   }
     /**
     * Add a set of rules to the digester object
     * @param digester Digester instance
     */

    @Override
    public void addRuleInstances(final Digester digester) {
        digester.addObjectCreate(prefix + "jonas-entity",
        "org.ow2.jonas.deployment.ejb.xml.JonasEntity");
        digester.addSetNext(prefix + "jonas-entity",
                "addJonasEntity",
        "org.ow2.jonas.deployment.ejb.xml.JonasEntity");
        digester.addCallMethod(prefix + "jonas-entity/ejb-name",
                "setEjbName", 0);
        digester.addCallMethod(prefix + "jonas-entity/jndi-name",
                "setJndiName", 0);
        digester.addCallMethod(prefix + "jonas-entity/jndi-local-name",
                "setJndiLocalName", 0);
        digester.addRuleSet(new JonasEnvironmentRuleSet(prefix + "jonas-entity/"));

        digester.addCallMethod(prefix + "jonas-entity/is-modified-method-name",
                "setIsModifiedMethodName", 0);
        digester.addCallMethod(prefix + "jonas-entity/passivation-timeout",
                "setPassivationTimeout", 0);
        digester.addCallMethod(prefix + "jonas-entity/inactivity-timeout",
                "setInactivityTimeout", 0);
        digester.addCallMethod(prefix + "jonas-entity/deadlock-timeout",
                "setDeadlockTimeout", 0);
        digester.addCallMethod(prefix + "jonas-entity/read-timeout",
                "setReadTimeout", 0);
        digester.addCallMethod(prefix + "jonas-entity/max-wait-time",
                "setMaxWaitTime", 0);
        digester.addCallMethod(prefix + "jonas-entity/shared",
                "setShared", 0);
        digester.addCallMethod(prefix + "jonas-entity/prefetch",
                "setPrefetch", 0);
        digester.addCallMethod(prefix + "jonas-entity/hard-limit",
                "setHardLimit", 0);
        digester.addCallMethod(prefix + "jonas-entity/max-cache-size",
                "setMaxCacheSize", 0);
        digester.addCallMethod(prefix + "jonas-entity/min-pool-size",
                "setMinPoolSize", 0);
        digester.addCallMethod(prefix + "jonas-entity/cleanup",
                "setCleanup", 0);
        digester.addCallMethod(prefix + "jonas-entity/lock-policy",
                "setLockPolicy", 0);
        digester.addCallMethod(prefix + "jonas-entity/cluster-replicated",
                "setClusterReplicated", 0);
        digester.addRuleSet(new JdbcMappingRuleSet(prefix + "jonas-entity/"));
        digester.addRuleSet(new IorSecurityConfigRuleSet(prefix + "jonas-entity/"));

        ClusterUtil.addClusterRule(prefix + "jonas-entity/", digester);
   }
}
