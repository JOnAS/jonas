/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;
/**
 * This class defines the implementation of the element method-params
 *
 * @author JOnAS team
 */

public class MethodParams extends AbsElement  {

    /**
     * method-param
     */
    private JLinkedList methodParamList = null;


    /**
     * Constructor
     */
    public MethodParams() {
        super();
        methodParamList = new  JLinkedList("method-param");
    }

    /**
     * Gets the method-param
     * @return the method-param
     */
    public JLinkedList getMethodParamList() {
        return methodParamList;
    }

    /**
     * Set the method-param
     * @param methodParamList methodParam
     */
    public void setMethodParamList(JLinkedList methodParamList) {
        this.methodParamList = methodParamList;
    }

    /**
     * Add a new  method-param element to this object
     * @param methodParam the methodParamobject
     */
    public void addMethodParam(String methodParam) {
        methodParamList.add(methodParam);
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<method-params>\n");

        indent += 2;

        // method-param
        sb.append(methodParamList.toXML(indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</method-params>\n");

        return sb.toString();
    }
}
