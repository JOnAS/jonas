/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;
import org.ow2.jonas.deployment.common.xml.MessageDestination;
import org.ow2.jonas.deployment.common.xml.SecurityRole;

/**
 * This class defines the implementation of the element assembly-descriptor
 *
 * @author JOnAS team
 */

public class AssemblyDescriptor extends AbsElement  {

    /**
     * security-role
     */
    private JLinkedList securityRoleList = null;

    /**
     * method-permission
     */
    private JLinkedList methodPermissionList = null;

    /**
     * container-transaction
     */
    private JLinkedList containerTransactionList = null;

    /**
     * message-destination
     */
    private JLinkedList messageDestinationList = null;

    /**
     * exclude-list
     */
    private ExcludeList excludeList = null;


    /**
     * Constructor
     */
    public AssemblyDescriptor() {
        super();
        securityRoleList = new  JLinkedList("security-role");
        methodPermissionList = new  JLinkedList("method-permission");
        containerTransactionList = new  JLinkedList("container-transaction");
        messageDestinationList = new  JLinkedList("message-destination");
    }

    /**
     * Gets the security-role
     * @return the security-role
     */
    public JLinkedList getSecurityRoleList() {
        return securityRoleList;
    }

    /**
     * Set the security-role
     * @param securityRoleList securityRole
     */
    public void setSecurityRoleList(JLinkedList securityRoleList) {
        this.securityRoleList = securityRoleList;
    }

    /**
     * Add a new  security-role element to this object
     * @param securityRole the securityRoleobject
     */
    public void addSecurityRole(SecurityRole securityRole) {
        securityRoleList.add(securityRole);
    }

    /**
     * Gets the method-permission
     * @return the method-permission
     */
    public JLinkedList getMethodPermissionList() {
        return methodPermissionList;
    }

    /**
     * Set the method-permission
     * @param methodPermissionList methodPermission
     */
    public void setMethodPermissionList(JLinkedList methodPermissionList) {
        this.methodPermissionList = methodPermissionList;
    }

    /**
     * Add a new  method-permission element to this object
     * @param methodPermission the methodPermissionobject
     */
    public void addMethodPermission(MethodPermission methodPermission) {
        methodPermissionList.add(methodPermission);
    }

    /**
     * Gets the container-transaction
     * @return the container-transaction
     */
    public JLinkedList getContainerTransactionList() {
        return containerTransactionList;
    }

    /**
     * Set the container-transaction
     * @param containerTransactionList containerTransaction
     */
    public void setContainerTransactionList(JLinkedList containerTransactionList) {
        this.containerTransactionList = containerTransactionList;
    }

    /**
     * Add a new  container-transaction element to this object
     * @param containerTransaction the containerTransactionobject
     */
    public void addContainerTransaction(ContainerTransaction containerTransaction) {
        containerTransactionList.add(containerTransaction);
    }

    /**
     * Gets the message-destination
     * @return the message-destination
     */
    public JLinkedList getMessageDestinationList() {
        return messageDestinationList;
    }

    /**
     * Set the message-destination
     * @param messageDestinationList messageDestination
     */
    public void setMessageDestinationList(JLinkedList messageDestinationList) {
        this.messageDestinationList = messageDestinationList;
    }

    /**
     * Add a new  message-destination element to this object
     * @param messageDestination the messageDestinationobject
     */
    public void addMessageDestination(MessageDestination messageDestination) {
        messageDestinationList.add(messageDestination);
    }

    /**
     * Gets the exclude-list
     * @return the exclude-list
     */
    public ExcludeList getExcludeList() {
        return excludeList;
    }

    /**
     * Set the exclude-list
     * @param excludeList excludeList
     */
    public void setExcludeList(ExcludeList excludeList) {
        this.excludeList = excludeList;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<assembly-descriptor>\n");

        indent += 2;

        // security-role
        sb.append(securityRoleList.toXML(indent));
        // method-permission
        sb.append(methodPermissionList.toXML(indent));
        // container-transaction
        sb.append(containerTransactionList.toXML(indent));
        // message-destination
        sb.append(messageDestinationList.toXML(indent));
        // exclude-list
        if (excludeList != null) {
            sb.append(excludeList.toXML(indent));
        }
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</assembly-descriptor>\n");

        return sb.toString();
    }
}
