/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A. 
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar;

import java.io.Serializable;
import java.util.List;

import org.ow2.jonas.deployment.rar.xml.InboundResourceadapter;
import org.ow2.jonas.deployment.rar.xml.OutboundResourceadapter;
import org.ow2.jonas.deployment.rar.xml.Resourceadapter;

/** 
 * This class defines the implementation of the element resourceadapter
 * 
 * @author Florent Benoit
 */

public class ResourceadapterDesc  implements Serializable {

    /**
     * resourceadapter-class
     */ 
    private String resourceadapterClass = null;

    /**
     * managedconnectionfactory-class
     */ 
    private String managedconnectionfactoryClass = null;

    /**
     * connectionfactory-interface
     */ 
    private String connectionfactoryInterface = null;

    /**
     * connectionfactory-impl-class
     */ 
    private String connectionfactoryImplClass = null;

    /**
     * connection-interface
     */ 
    private String connectionInterface = null;

    /**
     * connection-impl-class
     */ 
    private String connectionImplClass = null;

    /**
     * transaction-support
     */ 
    private String transactionSupport = null;

    /**
     * config-property
     */ 
    private List configPropertyList = null;

    /**
     * authentication-mechanism
     */ 
    private List authenticationMechanismList = null;

    /**
     * outbound-resourceadapter
     */ 
    private OutboundResourceadapterDesc outboundResourceadapterDesc = null;

    /**
     * inbound-resourceadapter
     */ 
    private InboundResourceadapterDesc inboundResourceadapterDesc = null;

    /**
     * adminobject
     */ 
    private List adminobjectList = null;

    /**
     * reauthentication-support
     */ 
    private String reauthenticationSupport = null;

    /**
     * security-permission
     */ 
    private List securityPermissionList = null;


    /**
     * Constructor
     */
    public ResourceadapterDesc(Resourceadapter ra) {
        if (ra != null) {
            resourceadapterClass = ra.getResourceadapterClass();
            managedconnectionfactoryClass = ra.getManagedconnectionfactoryClass();
            connectionfactoryInterface = ra.getConnectionfactoryInterface();
            connectionfactoryImplClass = ra.getConnectionfactoryImplClass();
            connectionInterface = ra.getConnectionInterface();
            connectionImplClass = ra.getConnectionImplClass();
            transactionSupport = ra.getTransactionSupport();
            configPropertyList = Utility.configProperty(ra.getConfigPropertyList());
            authenticationMechanismList = Utility.authenticationMechanism(ra.getAuthenticationMechanismList());
            outboundResourceadapterDesc = new OutboundResourceadapterDesc(ra.getOutboundResourceadapter());
            inboundResourceadapterDesc = new InboundResourceadapterDesc(ra.getInboundResourceadapter());
            adminobjectList = Utility.adminobject(ra.getAdminobjectList());
            reauthenticationSupport = ra.getReauthenticationSupport();
            securityPermissionList = Utility.securityPermission(ra.getSecurityPermissionList());
        }
    }

    /** 
     * Gets the resourceadapter-class
     * @return the resourceadapter-class
     */
    public String getResourceadapterClass() {
        return resourceadapterClass;
    }

    /** 
     * Gets the managedconnectionfactory-class
     * @return the managedconnectionfactory-class
     */
    public String getManagedconnectionfactoryClass() {
        return managedconnectionfactoryClass;
    }

    /** 
     * Gets the connectionfactory-interface
     * @return the connectionfactory-interface
     */
    public String getConnectionfactoryInterface() {
        return connectionfactoryInterface;
    }

    /** 
     * Gets the connectionfactory-impl-class
     * @return the connectionfactory-impl-class
     */
    public String getConnectionfactoryImplClass() {
        return connectionfactoryImplClass;
    }

    /** 
     * Gets the connection-interface
     * @return the connection-interface
     */
    public String getConnectionInterface() {
        return connectionInterface;
    }

    /** 
     * Gets the connection-impl-class
     * @return the connection-impl-class
     */
    public String getConnectionImplClass() {
        return connectionImplClass;
    }

    /** 
     * Gets the transaction-support
     * @return the transaction-support
     */
    public String getTransactionSupport() {
        return transactionSupport;
    }

    /** 
     * Gets the config-property
     * @return the config-property
     */
    public List getConfigPropertyList() {
        return configPropertyList;
    }

    /** 
     * Gets the authentication-mechanism
     * @return the authentication-mechanism
     */
    public List getAuthenticationMechanismList() {
        return authenticationMechanismList;
    }

    /** 
     * Gets the outbound-resourceadapter
     * @return the outbound-resourceadapter
     */
    public OutboundResourceadapterDesc getOutboundResourceadapterDesc() {
        return outboundResourceadapterDesc;
    }

    /** 
     * Gets the inbound-resourceadapter
     * @return the inbound-resourceadapter
     */
    public InboundResourceadapterDesc getInboundResourceadapterDesc() {
        return inboundResourceadapterDesc;
    }

    /** 
     * Gets the adminobject
     * @return the adminobject
     */
    public List getAdminobjectList() {
        return adminobjectList;
    }

    /** 
     * Gets the reauthentication-support
     * @return the reauthentication-support
     */
    public String getReauthenticationSupport() {
        return reauthenticationSupport;
    }

    /** 
     * Gets the security-permission
     * @return the security-permission
     */
    public List getSecurityPermissionList() {
        return securityPermissionList;
    }

}
