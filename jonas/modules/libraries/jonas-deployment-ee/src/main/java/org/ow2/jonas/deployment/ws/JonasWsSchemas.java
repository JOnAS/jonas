/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.ws;

import org.ow2.jonas.deployment.common.util.ResourceHelper;
import org.ow2.jonas.deployment.web.WebAppSchemas;

/**
 * This class defines the declarations of Schemas for jonas-webservices.xml.
 * @author Helene Joanin
 */
public class JonasWsSchemas extends WsSchemas {

    /**
     * Package name.
     */
    private static final String PACKAGE = ResourceHelper.getResourcePackage(JonasWsSchemas.class);

    /**
     * List of schemas used for jonas-webservices.xml.
     */
    private static final String[] JONAS_WS_SCHEMAS = new String[] {
        PACKAGE + "jonas_j2ee_web_services_4_0.xsd",
        PACKAGE + "jonas_j2ee_web_services_4_1_2.xsd",
        PACKAGE + "jonas_j2ee_web_services_4_1_4.xsd",
        PACKAGE + "jonas_j2ee_web_services_4_2.xsd",
        PACKAGE + "jonas_j2ee_web_services_4_5.xsd",
        PACKAGE + "jonas_j2ee_web_services_5_0.xsd"
    };


    /**
     * Build a new object for Schemas handling.
     */
    public JonasWsSchemas() {
        super();
        addSchemas(JONAS_WS_SCHEMAS, JonasWsSchemas.class.getClassLoader());
        // Add webapp schemas as WS schemas include Web schemas (for WS security)
        addSchemas(WebAppSchemas.WEBAPP_SCHEMAS, WebAppSchemas.class.getClassLoader());
    }

    /**
     * @return Returns the last Schema
     */
    public static String getLastSchema() {
        return getLastSchema(JONAS_WS_SCHEMAS, PACKAGE);
    }
}

