/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.clusterd.rules;

import org.ow2.jonas.deployment.common.rules.JRuleSetBase;

import org.apache.commons.digester.Digester;

/**
 * This class defines the rules to analyze the cluster-daemon element.
 *
 * @author Benoit Pelletier
 * @author S. Ali Tokmen
 */

public class ClusterDaemonRuleSet extends JRuleSetBase {

    /**
     * Constructs an object with the prefix "cluster-daemon".
     */
    public ClusterDaemonRuleSet() {
        super("cluster-daemon/");
    }

    /** Add a set of rules to the digester object.
     * @param digester Digester instance.
     */
    public void addRuleInstances(final Digester digester) {
        digester.addCallMethod(prefix + "name", "setName", 0);
        digester.addCallMethod(prefix + "domain-name", "setDomainName", 0);
        digester.addCallMethod(prefix + "jonas-interaction-mode", "setJonasInteractionMode", 0);
        digester.addCallMethod(prefix + "jmx.secured", "setJmxSecured", 0);
        digester.addCallMethod(prefix + "jmx.authentication.method", "setJmxAuthenticationMethod", 0);
        digester.addCallMethod(prefix + "jmx.authentication.parameter", "setJmxAuthenticationParameter", 0);
        digester.addCallMethod(prefix + "jmx.authorization.method", "setJmxAuthorizationMethod", 0);
        digester.addCallMethod(prefix + "jmx.authorization.parameter", "setJmxAuthorizationParameter", 0);
        digester.addRuleSet(new DiscoveryRuleSet(prefix));
        digester.addRuleSet(new ServerRuleSet(prefix));

    }

}
