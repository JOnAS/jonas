/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.rules;

import org.ow2.jonas.deployment.common.rules.JRuleSetBase;
import org.ow2.jonas.deployment.common.rules.JonasEnvironmentRuleSet;

import org.apache.commons.digester.Digester;


/**
 * This class defines the rules to analyze the element jonas-message-driven
 *
 * @author JOnAS team
 */

public class JonasMessageDrivenRuleSet extends JRuleSetBase {

    /**
     * Construct an object with a specific prefix
     * @param prefix prefix to use during the parsing
     */
    public JonasMessageDrivenRuleSet(String prefix) {
        super(prefix);
   }
     /**
     * Add a set of rules to the digester object
     * @param digester Digester instance
     */

    public void addRuleInstances(Digester digester) {
        digester.addObjectCreate(prefix + "jonas-message-driven",
                                 "org.ow2.jonas.deployment.ejb.xml.JonasMessageDriven");
        digester.addSetNext(prefix + "jonas-message-driven",
                            "addJonasMessageDriven",
                            "org.ow2.jonas.deployment.ejb.xml.JonasMessageDriven");
        digester.addCallMethod(prefix + "jonas-message-driven/ejb-name",
                               "setEjbName", 0);
        digester.addRuleSet(new JonasMessageDrivenDestinationRuleSet(prefix
                                                                     + "jonas-message-driven/"));
        digester.addRuleSet(new ActivationConfigRuleSet(prefix + "jonas-message-driven/"));
        digester.addRuleSet(new JonasEnvironmentRuleSet(prefix + "jonas-message-driven/"));
        digester.addCallMethod(prefix + "jonas-message-driven/max-cache-size",
                               "setMaxCacheSize", 0);
        digester.addCallMethod(prefix + "jonas-message-driven/min-pool-size",
                               "setMinPoolSize", 0);
        digester.addRuleSet(new IorSecurityConfigRuleSet(prefix + "jonas-message-driven/"));
   }
}
