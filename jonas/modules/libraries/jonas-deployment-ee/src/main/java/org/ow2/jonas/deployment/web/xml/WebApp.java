/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.web.xml;

import org.ow2.jonas.deployment.common.xml.AbsEnvironmentElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;
import org.ow2.jonas.deployment.common.xml.JndiEnvRefsGroupXml;
import org.ow2.jonas.deployment.common.xml.SecurityRole;
import org.ow2.jonas.deployment.common.xml.TopLevelElement;

/**
 * This class defines the implementation of the element web-app.
 * @author Florent Benoit
 */
public class WebApp extends AbsEnvironmentElement implements TopLevelElement, JndiEnvRefsGroupXml {

    /**
     * List of servlet
     */
    private JLinkedList servletList = null;

    /**
     * List of servlet-mapping
     */
    private JLinkedList servletMappingList = null;

    /**
     * security-constraint
     */
    private JLinkedList securityConstraintList = null;

    /**
     * security-role
     */
    private JLinkedList securityRoleList = null;

    /**
     * Number of jsp-config
     */
    private int jspConfigNumber = 0;

    /**
     * Number of login-config
     */
    private int loginConfigNumber = 0;

    /**
     * Number of jsp-config
     */
    private int sessionConfigNumber = 0;

    /**
     * Constructor : build a new WebApp object
     */
    public WebApp() {
        super();
        servletList = new JLinkedList("servlet");
        servletMappingList = new JLinkedList("servlet-mapping");
        securityConstraintList = new JLinkedList("security-constraint");
        securityRoleList = new JLinkedList("security-role");
    }

    /**
     * Add a new servlet element to this object
     * @param servlet the servlet object
     */
    public void addServlet(Servlet servlet) {
        servletList.add(servlet);
    }

    /**
     * Add a new servlet-mapping element to this object
     * @param servletMapping the servlet-mapping object
     */
    public void addServletMapping(ServletMapping servletMapping) {
        servletMappingList.add(servletMapping);
    }

    /**
     * Set the security-role
     * @param securityRoleList securityRole
     */
    public void setSecurityRoleList(JLinkedList securityRoleList) {
        this.securityRoleList = securityRoleList;
    }

    /**
     * Add a new security role element to this object
     * @param securityRole security role object
     */
    public void addSecurityRole(SecurityRole securityRole) {
        securityRoleList.add(securityRole);
    }

    /**
     * Set the security-constraint
     * @param securityConstraintList securityConstraint
     */
    public void setSecurityConstraintList(JLinkedList securityConstraintList) {
        this.securityConstraintList = securityConstraintList;
    }

    /**
     * Add a new security constraint element to this object
     * @param securityConstraint security constraint object
     */
    public void addSecurityConstraint(SecurityConstraint securityConstraint) {
        securityConstraintList.add(securityConstraint);
    }

    // Getters

    /**
     * Gets the security-constraint
     * @return the security-constraint
     */
    public JLinkedList getSecurityConstraintList() {
        return securityConstraintList;
    }

    /**
     * Gets the security-role
     * @return the security-role
     */
    public JLinkedList getSecurityRoleList() {
        return securityRoleList;
    }

    /**
     * @return the list of all servlet elements
     */
    public JLinkedList getServletList() {
        return servletList;
    }

    /**
     * @return the list of all servlet-mapping elements
     */
    public JLinkedList getServletMappingList() {
        return servletMappingList;
    }

    /**
     * Count a new jsp config
     */
    public void newJspConfig() {
        jspConfigNumber++;
    }

    /**
     * Count a new login-config
     */
    public void newLoginConfig() {
        loginConfigNumber++;
    }

    /**
     * Count a new jsp config
     */
    public void newSessionConfig() {
        sessionConfigNumber++;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<web-app>\n");

        indent += 2;

        // display-name
        sb.append(xmlElement(getDisplayName(), "display-name", indent));

        // servlet
        sb.append(servletList.toXML(indent));

        // servlet-mapping
        sb.append(servletMappingList.toXML(indent));

        // security-constraint
        sb.append(securityConstraintList.toXML(indent));

        // security-role
        sb.append(securityRoleList.toXML(indent));

        // resource-env-ref
        sb.append(getResourceEnvRefList().toXML(indent));

        // resource-ref
        sb.append(getResourceRefList().toXML(indent));

        // env-entry
        sb.append(getEnvEntryList().toXML(indent));

        // ejb-ref
        sb.append(getEjbRefList().toXML(indent));

        // ejb-local-ref
        sb.append(getEjbLocalRefList().toXML(indent));

        // service-ref
        sb.append(getServiceRefList().toXML(indent));

        // message-destination-ref
        sb.append(getMessageDestinationRefList().toXML(indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</web-app>");

        return sb.toString();
    }

    /**
     * @return the jspConfigNumber.
     */
    public int getJspConfigNumber() {
        return jspConfigNumber;
    }

    /**
     * @return the loginConfigNumber.
     */
    public int getLoginConfigNumber() {
        return loginConfigNumber;
    }

    /**
     * @return the sessionConfigNumber.
     */
    public int getSessionConfigNumber() {
        return sessionConfigNumber;
    }
}