/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
/**
 * This class defines the implementation of the element jonas-message-driven-destination
 *
 * @author JOnAS team
 */

public class JonasMessageDrivenDestination extends AbsElement  {

    /**
     * jndi-name
     */
    private String jndiName = null;


    /**
     * Constructor
     */
    public JonasMessageDrivenDestination() {
        super();
    }

    /**
     * Gets the jndi-name
     * @return the jndi-name
     */
    public String getJndiName() {
        return jndiName;
    }

    /**
     * Set the jndi-name
     * @param jndiName jndiName
     */
    public void setJndiName(String jndiName) {
        this.jndiName = jndiName;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<jonas-message-driven-destination>\n");

        indent += 2;

        // jndi-name
        sb.append(xmlElement(jndiName, "jndi-name", indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</jonas-message-driven-destination>\n");

        return sb.toString();
    }
}
