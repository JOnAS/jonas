/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * Icon as published by the Free Software Foundation; either
 * version 2.1 of the Icon, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public Icon for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * Icon along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar;

import java.io.Serializable;

import org.ow2.jonas.deployment.rar.xml.Icon;


/**
 * This class defines the implementation of the element icon
 *
 * @author Eric Hardesty
 */

public class IconDesc  implements Serializable {

    /**
     * small-icon
     */
    private String smallIcon = null;

    /**
     * large-icon
     */
    private String largeIcon = null;


    /**
     * Constructor
     */
    public IconDesc(Icon icon) {
        if (icon != null) {
            smallIcon = icon.getSmallIcon();
            largeIcon = icon.getLargeIcon();
        }
    }

    /**
     * Gets the small-icon
     * @return the small-icon
     */
    public String getSmallIcon() {
        return smallIcon;
    }

    /**
     * Gets the large-icon
     * @return the large-icon
     */
    public String getLargeIcon() {
        return largeIcon;
    }
}
