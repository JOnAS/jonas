/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A. 
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;
/** 
 * This class defines the implementation of the element activationspec
 * 
 * @author Eric Hardesty
 */

public class Activationspec extends AbsElement  {

    /**
     * activationspec-class
     */ 
    private String activationspecClass = null;

    /**
     * required-config-property
     */ 
    private JLinkedList requiredConfigPropertyList = null;


    /**
     * Constructor
     */
    public Activationspec() {
        super();
        requiredConfigPropertyList = new  JLinkedList("required-config-property");
    }

    /** 
     * Gets the activationspec-class
     * @return the activationspec-class
     */
    public String getActivationspecClass() {
        return activationspecClass;
    }

    /** 
     * Set the activationspec-class
     * @param activationspecClass activationspecClass
     */
    public void setActivationspecClass(String activationspecClass) {
        this.activationspecClass = activationspecClass;
    }

    /** 
     * Gets the required-config-property
     * @return the required-config-property
     */
    public JLinkedList getRequiredConfigPropertyList() {
        return requiredConfigPropertyList;
    }

    /** 
     * Set the required-config-property
     * @param requiredConfigPropertyList requiredConfigProperty
     */
    public void setRequiredConfigPropertyList(JLinkedList requiredConfigPropertyList) {
        this.requiredConfigPropertyList = requiredConfigPropertyList;
    }

    /** 
     * Add a new  required-config-property element to this object
     * @param requiredConfigProperty the requiredConfigPropertyobject
     */
    public void addRequiredConfigProperty(RequiredConfigProperty requiredConfigProperty) {
        requiredConfigPropertyList.add(requiredConfigProperty);
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prefixing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<activationspec>\n");

        indent += 2;

        // activationspec-class
        sb.append(xmlElement(activationspecClass, "activationspec-class", indent));
        // required-config-property
        sb.append(requiredConfigPropertyList.toXML(indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</activationspec>\n");

        return sb.toString();
    }
}
