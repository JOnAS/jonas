/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A. 
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;

/** 
 * This class defines the implementation of the element tm-config-property
 * 
 * @author Florent Benoit
 */

public class TmConfigProperty extends AbsElement  {

    /**
     * tm-config-property-name
     */ 
    private String tmConfigPropertyName = null;

    /**
     * tm-config-property-value
     */ 
    private String tmConfigPropertyValue = null;


    /**
     * Constructor
     */
    public TmConfigProperty() {
        super();
    }

    /** 
     * Gets the tm-config-property-name
     * @return the tm-config-property-name
     */
    public String getTmConfigPropertyName() {
        return tmConfigPropertyName;
    }

    /** 
     * Set the tm-config-property-name
     * @param tmConfigPropertyName tmConfigPropertyName
     */
    public void setTmConfigPropertyName(String tmConfigPropertyName) {
        this.tmConfigPropertyName = tmConfigPropertyName;
    }

    /** 
     * Gets the tm-config-property-value
     * @return the tm-config-property-value
     */
    public String getTmConfigPropertyValue() {
        return tmConfigPropertyValue;
    }

    /** 
     * Set the tm-config-property-value
     * @param tmConfigPropertyValue tmConfigPropertyValue
     */
    public void setTmConfigPropertyValue(String tmConfigPropertyValue) {
        this.tmConfigPropertyValue = tmConfigPropertyValue;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prefixing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<tm-config-property>\n");

        indent += 2;

        // tm-config-property-name
        sb.append(xmlElement(tmConfigPropertyName, "tm-config-property-name", indent));
        // tm-config-property-value
        sb.append(xmlElement(tmConfigPropertyValue, "tm-config-property-value", indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</tm-config-property>\n");

        return sb.toString();
    }
}
