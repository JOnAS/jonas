/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;

/**
 * This class defines the implementation of the element messagelistener
 *
 * @author Eric Hardesty
 */

public class Messagelistener extends AbsElement  {

    /**
     * id
     */
    private String id = null;

    /**
     * messagelistener-type
     */
    private String messagelistenerType = null;

    /**
     * activationspec
     */
    private Activationspec activationspec = null;


    /**
     * Constructor
     */
    public Messagelistener() {
        super();
    }

    /**
     * Gets the id
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Set the id
     * @param id id object
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets the messagelistener-type
     * @return the messagelistener-type
     */
    public String getMessagelistenerType() {
        return messagelistenerType;
    }

    /**
     * Set the messagelistener-type
     * @param messagelistenerType messagelistenerType
     */
    public void setMessagelistenerType(String messagelistenerType) {
        this.messagelistenerType = messagelistenerType;
    }

    /**
     * Gets the activationspec
     * @return the activationspec
     */
    public Activationspec getActivationspec() {
        return activationspec;
    }

    /**
     * Set the activationspec
     * @param activationspec activationspec
     */
    public void setActivationspec(Activationspec activationspec) {
        this.activationspec = activationspec;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prefixing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<messagelistener");
        sb.append(xmlAttribute(id, "id"));
        sb.append(">\n");

        indent += 2;

        // messagelistener-type
        sb.append(xmlElement(messagelistenerType, "messagelistener-type", indent));
        // activationspec
        if (activationspec != null) {
          sb.append(activationspec.toXML(indent));
        }
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</messagelistener>\n");

        return sb.toString();
    }
}
