/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.client;


// import java
import org.ow2.jonas.deployment.api.IEJBRefDesc;
import org.ow2.jonas.deployment.api.IEnvEntryDesc;
import org.ow2.jonas.deployment.api.IMessageDestinationRefDesc;
import org.ow2.jonas.deployment.api.IPersistenceUnitRefDesc;
import org.ow2.jonas.deployment.api.IResourceEnvRefDesc;
import org.ow2.jonas.deployment.api.IResourceRefDesc;
import org.ow2.jonas.deployment.client.xml.ApplicationClient;
import org.ow2.jonas.deployment.client.xml.JonasClient;
import org.ow2.jonas.deployment.client.xml.JonasSecurity;
import org.ow2.jonas.deployment.common.DeploymentDescException;
import org.ow2.jonas.deployment.ee.JndiEnvRefsGroupDesc;
import org.ow2.util.ee.metadata.car.api.ICarDeployableMetadata;


/**
 * This class do the parsing of the application-client.xml file and jonas-client.xml files and
 * contruct a data structure associated to these two files.
 * @author Florent Benoit
 * @author Philippe Coq
 */
public class ClientContainerDeploymentDesc extends JndiEnvRefsGroupDesc {

    /**
     * The name of the callback handler
     */
    private String callbackHandler = null;

    /**
     * The name of the jaas config file
     */
    private String jaasFile = null;

    /**
     * The name of the jaas entry
     */
    private String jaasEntry = null;

    /**
     * The username to use for the callback handler
     */
    private String username = null;

    /**
     * The password to use for the callback handler
     */
    private String password = null;

    /**
     * Xml content of the standard deployement descriptor file
     */
    private String xmlContent = "";

    /**
     * Xml content of the JOnAS deployement descriptor file
     */
    private String jonasXmlContent = "";

    /**
     * Application-Client archive metadatas.
     */
    private ICarDeployableMetadata carDeployableMetadata;

    /**
     * Construct an instance of a ClientContainerDeploymentDesc.<BR>
     * Constructor is private, call one of the static getInstance
     * method instead.
     * @param classLoader the classloader for the classes.
     * @param applicationClient the data structure of the application-client (application-client.xml)
     * @param jonasClient the data structure of the jonas-client (jonas-client.xml)
     * @throws DeploymentDescException if the deployment
     * descriptors are corrupted.
     */
    public ClientContainerDeploymentDesc(final ClassLoader classLoader,
                                          final ApplicationClient applicationClient,
                                          final JonasClient jonasClient)
        throws DeploymentDescException {
        super(classLoader, applicationClient, jonasClient, null);

        // callbackHandler
        callbackHandler = null;
        if (applicationClient.getCallbackHandler() != null) {
            callbackHandler = applicationClient.getCallbackHandler();
        }

        JonasSecurity jonasSecurity = jonasClient.getJonasSecurity();
        if (jonasSecurity != null) {
            // jaas config file
            jaasFile = null;
            if (jonasSecurity.getJaasfile() != null) {
                jaasFile = jonasSecurity.getJaasfile();
            }

            // jaas entry
            jaasEntry = null;
            if (jonasSecurity.getJaasentry() != null) {
                jaasEntry = jonasSecurity.getJaasentry();
            }

            // username
            username = null;
            if (jonasSecurity.getUsername() != null) {
                username = jonasSecurity.getUsername();
            }

            // password
            password = null;
            if (jonasSecurity.getPassword() != null) {
                password = jonasSecurity.getPassword();
            }
        }


    }

    /**
     * Get the name of the jaas configuration file
     * @return the name of the jaas configuration file
     */
    public String getJaasFile() {
        return jaasFile;
    }

    /**
     * Get the entry in the jaas configuration file
     * @return the entry in the jaas configuration file
     */
    public String getJaasEntry() {
        return jaasEntry;
    }

    /**
     * Get the username used for a callback handler
     * @return the username used for a callback handler
     */
    public String getUsername() {
        return username;
    }

    /**
     * Get the password used for a callback handler
     * @return the password used for a callback handler
     */
    public String getPassword() {
        return password;
    }

    /**
     * Get the callback handler of this client application.
     * @return the callback handler of this client application.
     */
    public String getCallbackHandler() {
        return callbackHandler;
    }

    /**
     * Return the content of the web.xml file
     * @return the content of the web.xml file
     */
    public String getXmlContent() {
        return xmlContent;
    }

    /**
     * Return the content of the jonas-web.xml file
     * @return the content of the jonas-web.xml file
     */
    public String getJOnASXmlContent() {
        return jonasXmlContent;
    }

    /**
     * @param xmlContent XML Content
     */
    public void setXmlContent(final String xmlContent) {
        this.xmlContent = xmlContent;
    }

    /**
     * @param jonasXmlContent XML Content
     */
    public void setJOnASXmlContent(final String jonasXmlContent) {
        this.jonasXmlContent = jonasXmlContent;
    }

    /**
     * Return a String representation of the ClientContainerDeploymentDesc.
     * @return a String representation of the ClientContainerDeploymentDesc.
     */
    @Override
    public String toString() {
        StringBuffer ret = new StringBuffer();

        // Return the displayName
        ret.append("\ngetDisplayName()=" + getDisplayName());

        // Return the resource-env-ref
        IResourceEnvRefDesc[] rer = getResourceEnvRefDesc();
        for (int i = 0; i < rer.length; i++) {
            ret.append("\ngetResourceEnvRefDesc(" + i + ")=" + rer[i].getClass().getName());
            ret.append(rer[i].toString());
        }

        // Return the resource-ref
        IResourceRefDesc[] resourceRefDesc = getResourceRefDesc();
        for (int i = 0; i < resourceRefDesc.length; i++) {
            ret.append("\ngetResourceRefDesc(" + i + ")=" + resourceRefDesc[i].getClass().getName());
            ret.append(resourceRefDesc[i].toString());
        }

        // Return the env-entry
        IEnvEntryDesc[] envEntries = getEnvEntryDesc();
        for (int i = 0; i < envEntries.length; i++) {
            ret.append("\ngetEnvEntryDesc(" + i + ")=" + envEntries[i].getClass().getName());
            ret.append(envEntries[i].toString());
        }

        // Return the ejb-ref
        IEJBRefDesc[] ejbRefDesc = getEjbRefDesc();
        for (int i = 0; i < ejbRefDesc.length; i++) {
            ret.append("\ngetEjbRefDesc(" + i + ")=" + ejbRefDesc[i].getClass().getName());
            ret.append(ejbRefDesc[i].toString());
        }

        // Return the message-destination-ref
        IMessageDestinationRefDesc[] mdRefDesc = getMessageDestinationRefDesc();
        for (int i = 0; i < mdRefDesc.length; i++) {
            ret.append("\ngetMessageDestinationRefDesc(" + i + ")=" + mdRefDesc[i].getClass().getName());
            ret.append(mdRefDesc[i].toString());
        }

        // Return the persistence-unit-ref
        IPersistenceUnitRefDesc[] persistUnitRefDesc = getPersistenceUnitRefs();
        for (int i = 0; i < persistUnitRefDesc.length; i++) {
            ret.append("\ngetPersistenceUnitRefs(" + i + ")=" + persistUnitRefDesc[i].getClass().getName());
            ret.append(persistUnitRefDesc[i].toString());
        }

        // Return the callbackHandler
        ret.append("\ngetCallbackHandler()=" + getCallbackHandler());

        return ret.toString();
    }

    /**
     * Store a reference to this application-client metadatas
     * @param carDeployableMetadata application-client metadatas
     */
    public void setCarMetadata(ICarDeployableMetadata carDeployableMetadata) {
        this.carDeployableMetadata = carDeployableMetadata;
    }

    /**
     * @return this application-client metadatas.
     */
    public ICarDeployableMetadata getCarDeployableMetadata() {
        return this.carDeployableMetadata;
    }
}
