/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004-2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.web;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.ow2.jonas.deployment.ee.SecurityRoleRefDesc;
import org.ow2.jonas.deployment.common.xml.RunAs;
import org.ow2.jonas.deployment.common.xml.SecurityRoleRef;
import org.ow2.jonas.deployment.web.xml.Servlet;


/**
 * Defines a Servlet object.
 * It manages classname, security-role-ref and runas
 * @author Florent Benoit
 */

public class ServletDesc {

    /**
     * Internal servlet object
     */
    private Servlet servlet = null;

    /**
     * List of securityRoleRef
     */
    private List<SecurityRoleRefDesc> securityRoleRefDescList = null;



    /**
     * Constructor : Build a new Servlet object
     * @param servlet Servlet object
     */
    public ServletDesc(final Servlet servlet) {
        this.servlet = servlet;
        securityRoleRefDescList = new ArrayList<SecurityRoleRefDesc>();
        for (Iterator it = servlet.getSecurityRoleRefList().iterator(); it.hasNext();) {
            SecurityRoleRef securityRoleRef = (SecurityRoleRef) it.next();
            SecurityRoleRefDesc securityRoleRefDesc =
                new SecurityRoleRefDesc(getServletName(), securityRoleRef, false);
            securityRoleRefDescList.add(securityRoleRefDesc);
        }

    }

    /**
     * @return the runas of the servlet
     */
    public RunAs getServletRunAS() {
        return servlet.getRunAs();
    }

    /**
     * @return the name of the servlet
     */
    public String getServletName() {
        return servlet.getServletName();
    }

    /**
     * @return the class of the servlet
     */
    public String getServletClass() {
        return servlet.getServletClass();
    }

    /**
     * Gets the security-role-ref
     * @return the security-role-ref
     */
    public List<SecurityRoleRefDesc> getSecurityRoleRefList() {
        return securityRoleRefDescList;
    }

}
