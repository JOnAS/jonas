/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.rar;

import org.ow2.jonas.deployment.common.DeploymentDescException;

/**
 * The class RarDeploymentDescException indicates conditions
 * that a reasonable application might want to catch.
 * @author Florent Benoit
 * @author Ludovic Bert
 */
public class RarDeploymentDescException extends DeploymentDescException {

    /**
     * Constructs a new RarDeploymentDescException with no detail
     * message.
     */
    public RarDeploymentDescException() {
        super();
    }

    /**
     * Constructs a new RarDeploymentDescException with the specified
     * message.
     * @param message the detail message.
     */
    public RarDeploymentDescException(String message) {
        super(message);
    }

    /**
     * Constructs a new RarDeploymentDescException with the specified
     * error cause.
     * @param cause the cause of the error.
     */
    public RarDeploymentDescException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a new RarDeploymentDescException with the specified
     * error cause.
     * @param message the detail message.
     * @param cause the cause of the error.
     */
    public RarDeploymentDescException(String message, Throwable cause) {
        super(message, cause);
    }

}
