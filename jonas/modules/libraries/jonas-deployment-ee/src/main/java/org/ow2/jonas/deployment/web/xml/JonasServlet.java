/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.web.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;

/**
 * This class defines the implementation of the servlet element.
 * @author Francois Fornaciari
 */
public class JonasServlet extends AbsElement {

    /**
     * Name of this servlet
     */
    private String servletName = null;

    /**
     * Principal name of this servlet
     */
    private String principalName = null;


    // Setters

    /**
     * Sets the servlet name
     * @param servletName the servlet-name to use
     */
    public void setServletName(final String servletName) {
        this.servletName = servletName;
    }


    /**
     * Sets the principal name
     * @param principalName the principal name to use
     */
    public void setPrincipalName(final String principalName) {
        this.principalName = principalName;
    }



    // Getters


    /**
     * @return the name of the servlet
     */
    public String getServletName() {
        return servletName;
    }


    /**
     * @return the princiapl name of the servlet
     */
    public String getPrincipalName() {
        return principalName;
    }



    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prefixing XML representation.
     * @return the XML description of this object.
     */
    @Override
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<servlet>\n");

        indent += 2;

        // servlet-name
        sb.append(xmlElement(servletName, "servlet-name", indent));

        // principal-name
        sb.append(xmlElement(principalName, "principal-name", indent));

        indent -= 2;
        sb.append(indent(indent));
        sb.append("</servlet>\n");

        return sb.toString();
    }



}
