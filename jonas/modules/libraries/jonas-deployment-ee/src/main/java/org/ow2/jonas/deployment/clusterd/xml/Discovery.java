/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.clusterd.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;

public class Discovery extends AbsElement {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * File name for JGroups discovery stack
     */

    private String discoveryStackFile = null;

    /**
     * JGroups discovery will be connected to this group.
     */
    private String discoveryGroupName = null;

    /**
     * Set to true to start JGroups discovery.
     */
    private boolean startDiscovery = false;

    /**
     * @return the discoveryStackFile
     */
    public String getDiscoveryStackFile() {
        return discoveryStackFile;
    }

    /**
     * @param discoveryStackFile the discoveryStackFile to set
     */
    public void setDiscoveryStackFile(final String discoveryStackFile) {
        this.discoveryStackFile = discoveryStackFile;
    }

    /**
     * @return the discoveryGroupName
     */
    public String getDiscoveryGroupName() {
        return discoveryGroupName;
    }

    /**
     * @param discoveryGroupName the discoveryGroupName to set
     */
    public void setDiscoveryGroupName(final String discoveryGroupName) {
        this.discoveryGroupName = discoveryGroupName;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    @Override
    public String toXML(final int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<discovery>\n");
        sb.append(xmlElement(this.getDiscoveryGroupName(), "group-name", indent + 2));
        sb.append(xmlElement(this.getDiscoveryStackFile(), "stack-file", indent + 2));
        sb.append(xmlElement(String.valueOf(this.getStartDiscovery()), "start-up", indent + 2));
        sb.append(indent(indent));
        sb.append("</discovery>\n");
        return sb.toString();
    }

    /**
     * @return the startDiscovery
     */
    public boolean getStartDiscovery() {
        return startDiscovery;
    }

    /**
     * @param startDiscovery the startDiscovery to set
     */
    public void setStartDiscovery(final String startDiscovery) {
        this.startDiscovery = new Boolean(startDiscovery);
    }

}
