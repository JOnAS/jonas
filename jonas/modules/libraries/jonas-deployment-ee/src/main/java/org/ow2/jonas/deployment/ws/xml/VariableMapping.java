/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ws.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
/**
 * This class defines the implementation of the element variable-mapping
 *
 * @author JOnAS team
 */

public class VariableMapping extends AbsElement  {

    /**
     * java-variable-name
     */
    private String javaVariableName = null;

    /**
     * data-member
     */
    private boolean dataMember = false;

    /**
     * xml-element-name
     */
    private String xmlElementName = null;


    /**
     * Constructor
     */
    public VariableMapping() {
        super();
    }

    /**
     * Gets the java-variable-name
     * @return the java-variable-name
     */
    public String getJavaVariableName() {
        return javaVariableName;
    }

    /**
     * Set the java-variable-name
     * @param javaVariableName javaVariableName
     */
    public void setJavaVariableName(String javaVariableName) {
        this.javaVariableName = javaVariableName;
    }

    /**
     * Gets the data-member
     * @return true if data-member
     */
    public boolean isDataMember() {
        return dataMember;
    }

    /**
     * Set the data-member
     */
    public void setDataMember() {
        this.dataMember = true;
    }

    /**
     * Gets the xml-element-name
     * @return the xml-element-name
     */
    public String getXmlElementName() {
        return xmlElementName;
    }

    /**
     * Set the xml-element-name
     * @param xmlElementName xmlElementName
     */
    public void setXmlElementName(String xmlElementName) {
        this.xmlElementName = xmlElementName;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<variable-mapping>\n");

        indent += 2;

        // java-variable-name
        sb.append(xmlElement(javaVariableName, "java-variable-name", indent));
        // data-member
        if (dataMember) {
            sb.append(indent(indent));
            sb.append("<data-member/>");
        }
        // xml-element-name
        sb.append(xmlElement(xmlElementName, "xml-element-name", indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</variable-mapping>\n");

        return sb.toString();
    }
}
