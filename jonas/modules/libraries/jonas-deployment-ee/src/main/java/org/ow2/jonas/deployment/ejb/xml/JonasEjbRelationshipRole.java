/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;
/**
 * This class defines the implementation of the element jonas-ejb-relationship-role
 *
 * @author JOnAS team
 */

public class JonasEjbRelationshipRole extends AbsElement  {

    /**
     * ejb-relationship-role-name
     */
    private String ejbRelationshipRoleName = null;

    /**
     * foreign-key-jdbc-mapping
     */
    private JLinkedList foreignKeyJdbcMappingList = null;


    /**
     * Constructor
     */
    public JonasEjbRelationshipRole() {
        super();
        foreignKeyJdbcMappingList = new  JLinkedList("foreign-key-jdbc-mapping");
    }

    /**
     * Gets the ejb-relationship-role-name
     * @return the ejb-relationship-role-name
     */
    public String getEjbRelationshipRoleName() {
        return ejbRelationshipRoleName;
    }

    /**
     * Set the ejb-relationship-role-name
     * @param ejbRelationshipRoleName ejbRelationshipRoleName
     */
    public void setEjbRelationshipRoleName(String ejbRelationshipRoleName) {
        this.ejbRelationshipRoleName = ejbRelationshipRoleName;
    }

    /**
     * Gets the foreign-key-jdbc-mapping
     * @return the foreign-key-jdbc-mapping
     */
    public JLinkedList getForeignKeyJdbcMappingList() {
        return foreignKeyJdbcMappingList;
    }

    /**
     * Set the foreign-key-jdbc-mapping
     * @param foreignKeyJdbcMappingList foreignKeyJdbcMapping
     */
    public void setForeignKeyJdbcMappingList(JLinkedList foreignKeyJdbcMappingList) {
        this.foreignKeyJdbcMappingList = foreignKeyJdbcMappingList;
    }

    /**
     * Add a new  foreign-key-jdbc-mapping element to this object
     * @param foreignKeyJdbcMapping the foreignKeyJdbcMappingobject
     */
    public void addForeignKeyJdbcMapping(ForeignKeyJdbcMapping foreignKeyJdbcMapping) {
        foreignKeyJdbcMappingList.add(foreignKeyJdbcMapping);
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<jonas-ejb-relationship-role>\n");

        indent += 2;

        // ejb-relationship-role-name
        sb.append(xmlElement(ejbRelationshipRoleName, "ejb-relationship-role-name", indent));
        // foreign-key-jdbc-mapping
        sb.append(foreignKeyJdbcMappingList.toXML(indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</jonas-ejb-relationship-role>\n");

        return sb.toString();
    }
}
