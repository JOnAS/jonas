/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: jonas-team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.client.xml;

import org.ow2.jonas.deployment.common.xml.AbsEnvironmentElement;
import org.ow2.jonas.deployment.common.xml.JndiEnvRefsGroupXml;
import org.ow2.jonas.deployment.common.xml.TopLevelElement;

/**
 * This class defines the implementation of the element application-client
 * @author jonas-team
 */

public class ApplicationClient extends AbsEnvironmentElement implements TopLevelElement, JndiEnvRefsGroupXml {

    /**
     * callback-handler
     */
    private String callbackHandler = null;

    /**
     * Constructor
     */
    public ApplicationClient() {
        super();
    }

    /**
     * @return the callback-handler
     */
    public String getCallbackHandler() {
        return callbackHandler;
    }

    /**
     * Set the callback-handler
     * @param callbackHandler callbackHandler
     */
    public void setCallbackHandler(final String callbackHandler) {
        this.callbackHandler = callbackHandler;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    @Override
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<application-client>\n");

        indent += 2;

        // icon
        sb.append(getIcon().toXML(indent));
        // display-name
        sb.append(xmlElement(getDisplayName(), "display-name", indent));
        // description
        sb.append(xmlElement(getDescription(), "description", indent));
        // env-entry
        sb.append(getEnvEntryList().toXML(indent));
        // ejb-ref
        sb.append(getEjbRefList().toXML(indent));
        // service-ref
        sb.append(getServiceRefList().toXML(indent));
        // resource-ref
        sb.append(getResourceRefList().toXML(indent));
        // resource-env-ref
        sb.append(getResourceEnvRefList().toXML(indent));
        // message-destination-ref
        sb.append(getMessageDestinationRefList().toXML(indent));
        // persistence-unit-ref
        sb.append(getPersistenceUnitRefList().toXML(indent));
        // callback-handler
        sb.append(xmlElement(callbackHandler, "callback-handler", indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</application-client>\n");

        return sb.toString();
    }

}