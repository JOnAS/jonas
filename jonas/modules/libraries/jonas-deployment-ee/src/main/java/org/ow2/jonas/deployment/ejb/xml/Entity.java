/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.xml;

import org.ow2.jonas.deployment.common.xml.JLinkedList;
/**
 * This class defines the implementation of the element entity
 *
 * @author JOnAS team
 */

public class Entity extends  CommonEjb {



    /**
     * persistence-type
     */
    private String persistenceType = null;

    /**
     * prim-key-class
     */
    private String primKeyClass = null;

    /**
     * reentrant
     */
    private String reentrant = null;

    /**
     * cmp-version
     */
    private String cmpVersion = null;

    /**
     * abstract-schema-name
     */
    private String abstractSchemaName = null;

    /**
     * cmp-field
     */
    private JLinkedList cmpFieldList = null;

    /**
     * primkey-field
     */
    private String primkeyField = null;

    /**
     * query
     */
    private JLinkedList queryList = null;


    /**
     * Constructor
     */
    public Entity() {
        super();
        cmpFieldList = new  JLinkedList("cmp-field");
        queryList = new  JLinkedList("query");
    }

    /**
     * Gets the persistence-type
     * @return the persistence-type
     */
    public String getPersistenceType() {
        return persistenceType;
    }

    /**
     * Set the persistence-type
     * @param persistenceType persistenceType
     */
    public void setPersistenceType(final String persistenceType) {
        this.persistenceType = persistenceType;
    }

    /**
     * Gets the prim-key-class
     * @return the prim-key-class
     */
    public String getPrimKeyClass() {
        return primKeyClass;
    }

    /**
     * Set the prim-key-class
     * @param primKeyClass primKeyClass
     */
    public void setPrimKeyClass(final String primKeyClass) {
        this.primKeyClass = primKeyClass;
    }

    /**
     * Gets the reentrant
     * @return the reentrant
     */
    public String getReentrant() {
        return reentrant;
    }

    /**
     * Set the reentrant
     * @param reentrant reentrant
     */
    public void setReentrant(final String reentrant) {
        this.reentrant = reentrant;
    }

    /**
     * Gets the cmp-version
     * @return the cmp-version
     */
    public String getCmpVersion() {
        return cmpVersion;
    }

    /**
     * Set the cmp-version
     * @param cmpVersion cmpVersion
     */
    public void setCmpVersion(final String cmpVersion) {
        this.cmpVersion = cmpVersion;
    }

    /**
     * Gets the abstract-schema-name
     * @return the abstract-schema-name
     */
    public String getAbstractSchemaName() {
        return abstractSchemaName;
    }

    /**
     * Set the abstract-schema-name
     * @param abstractSchemaName abstractSchemaName
     */
    public void setAbstractSchemaName(final String abstractSchemaName) {
        this.abstractSchemaName = abstractSchemaName;
    }

    /**
     * Gets the cmp-field
     * @return the cmp-field
     */
    public JLinkedList getCmpFieldList() {
        return cmpFieldList;
    }

    /**
     * Set the cmp-field
     * @param cmpFieldList cmpField
     */
    public void setCmpFieldList(final JLinkedList cmpFieldList) {
        this.cmpFieldList = cmpFieldList;
    }

    /**
     * Add a new  cmp-field element to this object
     * @param cmpField the cmpFieldobject
     */
    public void addCmpField(final CmpField cmpField) {
        cmpFieldList.add(cmpField);
    }

    /**
     * Gets the primkey-field
     * @return the primkey-field
     */
    public String getPrimkeyField() {
        return primkeyField;
    }

    /**
     * Set the primkey-field
     * @param primkeyField primkeyField
     */
    public void setPrimkeyField(final String primkeyField) {
        this.primkeyField = primkeyField;
    }


    /**
     * Gets the query
     * @return the query
     */
    public JLinkedList getQueryList() {
        return queryList;
    }

    /**
     * Set the query
     * @param queryList query
     */
    public void setQueryList(final JLinkedList queryList) {
        this.queryList = queryList;
    }

    /**
     * Add a new  query element to this object
     * @param query the queryobject
     */
    public void addQuery(final Query query) {
        queryList.add(query);
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    @Override
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<entity>\n");

        indent += 2;

        // description
        sb.append(xmlElement(getDescription(), "description", indent));
        // display-name
        sb.append(xmlElement(getDisplayName(), "display-name", indent));
        // small-icon
        sb.append(xmlElement(getIcon().getSmallIcon(), "small-icon", indent));
        // large-icon
        sb.append(xmlElement(getIcon().getLargeIcon(), "large-icon", indent));
        // ejb-name
        sb.append(xmlElement(getEjbName(), "ejb-name", indent));
        // mapped-name
        sb.append(xmlElement(getMappedName(), "mapped-name", indent));
        // home
        sb.append(xmlElement(getHome(), "home", indent));
        // remote
        sb.append(xmlElement(getRemote(), "remote", indent));
        // local-home
        sb.append(xmlElement(getLocalHome(), "local-home", indent));
        // local
        sb.append(xmlElement(getLocal(), "local", indent));
        // ejb-class
        sb.append(xmlElement(getEjbClass(), "ejb-class", indent));
        // persistence-type
        sb.append(xmlElement(persistenceType, "persistence-type", indent));
        // prim-key-class
        sb.append(xmlElement(primKeyClass, "prim-key-class", indent));
        // reentrant
        sb.append(xmlElement(reentrant, "reentrant", indent));
        // cmp-version
        sb.append(xmlElement(cmpVersion, "cmp-version", indent));
        // abstract-schema-name
        sb.append(xmlElement(abstractSchemaName, "abstract-schema-name", indent));
        // cmp-field
        sb.append(cmpFieldList.toXML(indent));
        // primkey-field
        sb.append(xmlElement(primkeyField, "primkey-field", indent));
        // env-entry
        sb.append(getEnvEntryList().toXML(indent));
        // ejb-ref
        sb.append(getEjbRefList().toXML(indent));
        // ejb-local-ref
        sb.append(getEjbLocalRefList().toXML(indent));
        // service-ref
        sb.append(getServiceRefList().toXML(indent));
        // resource-ref
        sb.append(getResourceRefList().toXML(indent));
        // resource-env-ref
        sb.append(getResourceEnvRefList().toXML(indent));
        // message-destination-ref
        sb.append(getMessageDestinationRefList().toXML(indent));
        // security-role-ref
        sb.append(getSecurityRoleRefList().toXML(indent));
        // security-identity
        if (getSecurityIdentity() != null) {
            sb.append(getSecurityIdentity().toXML(indent));
        }
        // query
        sb.append(queryList.toXML(indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</entity>\n");

        return sb.toString();
    }
}
