/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.ws.rules;

import org.ow2.jonas.deployment.common.rules.JRuleSetBase;

import org.apache.commons.digester.Digester;

/**
 * This class defines the rules to analyze the element webservices
 *
 * @author JOnAS team
 */
public class WebservicesRuleSet extends JRuleSetBase {

    /**
     * Construct an object with a specific prefix
     */
    public WebservicesRuleSet() {
        super("webservices/");
    }

    /**
     * Add a set of rules to the digester object
     * @param digester Digester instance
     */
    public void addRuleInstances(Digester digester) {
        //digester.addCallMethod(prefix + "description", "setDescription", 0);
        // Parse the 'version' attribute
        digester.addSetProperties(prefix);
        digester.addCallMethod(prefix + "display-name", "setDisplayName", 0);
        //digester.addRuleSet(new IconRuleSet(prefix));
        digester.addRuleSet(new WebserviceDescriptionRuleSet(prefix));
    }
}
