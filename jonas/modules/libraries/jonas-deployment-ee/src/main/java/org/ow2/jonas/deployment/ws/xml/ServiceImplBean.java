/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.ws.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;

/**
 * This class defines the implementation of the element service-impl-bean
 *
 * @author JOnAS team
 */

public class ServiceImplBean extends AbsElement  {

    /**
     * ejb-link
     */
    private String ejbLink = null;

    /**
     * servlet-link
     */
    private String servletLink = null;


    /**
     * Constructor
     */
    public ServiceImplBean() {
        super();
    }

    /**
     * Gets the ejb-link
     * @return the ejb-link
     */
    public String getEjbLink() {
        return ejbLink;
    }

    /**
     * Set the ejb-link
     * @param ejbLink ejbLink
     */
    public void setEjbLink(String ejbLink) {
        this.ejbLink = ejbLink;
    }

    /**
     * Gets the servlet-link
     * @return the servlet-link
     */
    public String getServletLink() {
        return servletLink;
    }

    /**
     * Set the servlet-link
     * @param servletLink servletLink
     */
    public void setServletLink(String servletLink) {
        this.servletLink = servletLink;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<service-impl-bean>\n");

        indent += 2;

        // ejb-link
        sb.append(xmlElement(ejbLink, "ejb-link", indent));
        // servlet-link
        sb.append(xmlElement(servletLink, "servlet-link", indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</service-impl-bean>\n");

        return sb.toString();
    }
}
