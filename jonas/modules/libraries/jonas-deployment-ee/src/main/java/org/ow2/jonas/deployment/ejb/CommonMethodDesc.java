/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.ejb;

import java.security.PermissionCollection;
import java.security.Permissions;
import java.util.List;
import java.util.Iterator;
import javax.security.jacc.EJBMethodPermission;

import org.ow2.jonas.deployment.ejb.xml.Method;
import org.ow2.jonas.deployment.ejb.xml.MethodParams;


/**
 * Defines a ExcludeListDesc class for the management of
 * EJBMEthodPermissions in JACC
 * @author Florent Benoit : Initial developer
 */
public class CommonMethodDesc {

    /**
     * List of EJBMethodPermission
     */
    private PermissionCollection ejbMethodPermissions = null;


    /**
     * Constructor for CommonMethodDesc
     * @param list given methods
     */
    public CommonMethodDesc(List list) {
        this.ejbMethodPermissions = new Permissions();
        generateEJBMethodPermissions(list);
    }


    /**
     * Build all ejbMethodPermissions for the given methods
     * @param methodList given methods
     */
    protected void generateEJBMethodPermissions(List methodList) {
        Iterator it = methodList.iterator();

        // Manage permissions
        while (it.hasNext()) {
            Method method = (Method) it.next();
            MethodParams methodParams = method.getMethodParams();
            String[] methodParamsArray = null;
            if (methodParams != null) {

                List methodParamsList = method.getMethodParams().getMethodParamList();
                String[] methodParamSize = new String[methodParamsList.size()];
                methodParamsArray = (String[]) methodParamsList.toArray(methodParamSize);
            }

            // * or null value means that permission pertains to all methods
            String methodName = method.getMethodName();
            if (methodName.equals("*")) {
                methodName = null;
            }


            EJBMethodPermission ejbMethodPermission =
                new EJBMethodPermission(method.getEjbName(),
                                        methodName,
                                        method.getMethodIntf(),
                                        methodParamsArray);
            // Add the permission
            ejbMethodPermissions.add(ejbMethodPermission);
        }
    }

    /**
     * Gets EJBMethod permissions
     * @return the EJBMethodPermissions
     */
    public PermissionCollection getEJBMethodPermissions() {
        return ejbMethodPermissions;
    }
}
