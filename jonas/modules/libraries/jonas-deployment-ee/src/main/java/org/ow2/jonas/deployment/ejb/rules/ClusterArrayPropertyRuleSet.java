/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.rules;

import org.apache.commons.digester.Digester;
import org.ow2.jonas.deployment.common.rules.JRuleSetBase;

/**
 * Rule set for cluster array property.
 * @author eyindanga
 */
public class ClusterArrayPropertyRuleSet extends JRuleSetBase {

    /**
     * Constructor.
     * @param prefix Rule set prefix
     */
    public ClusterArrayPropertyRuleSet(final String prefix) {
        super(prefix);
    }

    @Override
    public void addRuleInstances(final Digester digester) {
        digester.addObjectCreate(prefix + "array-property", "org.ow2.cmi.info.mapping.ArrayPropertyInfo");
        digester.addSetNext(prefix + "array-property", "addArrayPropertyInfo", "org.ow2.cmi.info.mapping.ArrayPropertyInfo");
        digester.addSetProperties(prefix + "array-property");
        digester.addCallMethod(prefix + "array-property/value", "addValue", 0);

    }

}
