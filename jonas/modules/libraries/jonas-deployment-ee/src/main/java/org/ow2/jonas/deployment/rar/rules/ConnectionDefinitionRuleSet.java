/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar.rules;

import org.ow2.jonas.deployment.common.rules.JRuleSetBase;

import org.apache.commons.digester.Digester;

/**
 * This class defines the rules to analyze the element connection-definition
 *
 * @author Eric Hardesty
 */

public class ConnectionDefinitionRuleSet extends JRuleSetBase {

    /**
     * Construct an object with a specific prefix
     * @param prefix prefix to use during the parsing
     */
    public ConnectionDefinitionRuleSet(String prefix) {
        super(prefix);
   }
     /**
     * Add a set of rules to the digester object
     * @param digester Digester instance
     */

    public void addRuleInstances(Digester digester) {
        digester.addObjectCreate(prefix + "connection-definition",
                                 "org.ow2.jonas.deployment.rar.xml.ConnectionDefinition");
        digester.addSetProperties(prefix + "connection-definition", "id", "id");
        digester.addSetNext(prefix + "connection-definition",
                            "addConnectionDefinition",
                            "org.ow2.jonas.deployment.rar.xml.ConnectionDefinition");
        digester.addCallMethod(prefix + "connection-definition/managedconnectionfactory-class",
                               "setManagedconnectionfactoryClass", 0);
        digester.addRuleSet(new ConfigPropertyRuleSet(prefix + "connection-definition/"));
        digester.addCallMethod(prefix + "connection-definition/connectionfactory-interface",
                               "setConnectionfactoryInterface", 0);
        digester.addCallMethod(prefix + "connection-definition/connectionfactory-impl-class",
                               "setConnectionfactoryImplClass", 0);
        digester.addCallMethod(prefix + "connection-definition/connection-interface",
                               "setConnectionInterface", 0);
        digester.addCallMethod(prefix + "connection-definition/connection-impl-class",
                               "setConnectionImplClass", 0);
   }
}
