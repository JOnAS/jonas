/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.deployment.ejb;

/**
 * Class to hold meta-information related to a cmp-field
 * @author Christophe Ney [cney@batisseurs.com] : Initial developer
 * @author Helene Joanin on May 2003: code cleanup
 */
public class FieldDesc {

    protected String fieldName = null;
    protected boolean pkField = false;
    protected Class fieldType = null;


    /**
     * Assessor method for primary key field
     * @return true if field is a primary key
     */
    public boolean isPrimaryKey() {
        return pkField;
    }

    /**
     * Field name getter
     * @return field name
     */
    public String getName() {
        return fieldName;
    }

    /**
     * field name setter
     */
    protected void setName(String fieldName) {
        this.fieldName = fieldName;
    }

    /**
     * primary key flag setter
     */
    protected void setPrimaryKey(boolean pkField) {
        this.pkField = pkField;
    }

    /**
     * field type getter
     */
    public Class getFieldType() {
        return fieldType;
    }

    /**
     * field type setter
     */
    protected void setFieldType(Class fieldType) {
        this.fieldType = fieldType;
    }

    /**
     * get the setter method name for a given field
     */
    public static String getSetterName(String fieldName) {
        return "set" + Character.toUpperCase(fieldName.charAt(0)) + fieldName.substring(1);
    }

    /**
     * get the getter method name for a given field
     */
    public static String getGetterName(String fieldName) {
        return "get" + Character.toUpperCase(fieldName.charAt(0)) + fieldName.substring(1);
    }

    /**
     * String representation of the object for test purpose
     * @return String representation of this object
     */
    public String toString() {
        StringBuffer ret = new StringBuffer();
        ret.append("\ngetName() = " + getName());
        ret.append("\ngetFieldType() = " + getFieldType());
        ret.append("\nisPrimaryKey() = " + isPrimaryKey());
        return ret.toString();
    }

}
