/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.web;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Defines a transport guarantee object.
 * Several kind of tranport can be assigned.
 * It could be : INTEGRAL, CONFIDENTIAL, NONE
 * @author Florent Benoit
 */
public class TransportGuaranteeDesc {

    /**
     * Constant for Confidential transport guarantee
     */
    public static final String CONFIDENTIAL_TRANSPORT = "CONFIDENTIAL";

    /**
     * Constant for Integral transport guarantee
     */
    public static final String INTEGRAL_TRANSPORT = "INTEGRAL";

    /**
     * Constant for None transport guarantee
     */
    public static final String NONE_TRANSPORT = "NONE";

    /**
     * Internal list of transports
     */
    private List transports = null;

    /**
     * Constructor : Build a new Guarantee object
     */
    public TransportGuaranteeDesc() {
        transports = new ArrayList();
    }


    /**
     * Add transport name
     * No check is performed here on the name
     * validation has to be done at XML parsing time
     * @param name protocol value
     */
    public void addTransportValue(String name) {
        if (name == null) {
            name = NONE_TRANSPORT;
        }
        String upperCasename = name.toUpperCase();
        if (!transports.contains(upperCasename)) {
            transports.add(upperCasename);
        }
    }

    /**
     * Test if this transport Guarantee contains the INTEGRAL value
     * @return true if it contains INTEGRAL
     */
    public boolean isIntegral() {
        return transports.contains(INTEGRAL_TRANSPORT);
    }

    /**
     * Test if this transport Guarantee contains the CONFIDENTIAL value
     * @return true if it contains CONFIDENTIAL
     */
    public boolean isConfidential() {
        return transports.contains(CONFIDENTIAL_TRANSPORT);
    }

    /**
     * Test if this transport Guarantee contains the NONE value
     * @return true if it contains NONE
     */
    public boolean hasNone() {
        return transports.contains(NONE_TRANSPORT);
    }


    /**
     * Gets the number of different transports.
     * @return count of transports.
     */
    public int getNumber() {
        return transports.size();
    }


    /**
     * String representation
     * @return string representation of the pattern
     */
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("TransportGuarantee[values=");
        for (Iterator it = transports.iterator(); it.hasNext();) {
            String value = (String) it.next();
            sb.append("(");
            sb.append(value);
            sb.append(")");
        }
        sb.append("]");
        return sb.toString();
    }

}
