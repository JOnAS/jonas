/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar.xml;


import org.ow2.jonas.deployment.common.CommonsSchemas;
import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;
import org.ow2.jonas.deployment.common.xml.TopLevelElement;
import org.ow2.jonas.deployment.rar.JonasConnectorSchemas;


/**
 * This class defines the implementation of the element jonas-connector
 *
 * @author Florent Benoit
 */

public class JonasConnector extends AbsElement implements TopLevelElement {

    /**
     * jndi-name
     */
    private String jndiName = null;

    /**
     * rarlink
     */
    private String rarlink = null;

    /**
     * native-lib
     */
    private String nativeLib = null;

    /**
     * log-enabled
     */
    private String logEnabled = null;

    /**
     * log-topic
     */
    private String logTopic = null;

    /**
     * pool-params
     */
    private PoolParams poolParams = null;

    /**
     * jdbc-conn-params
     */
    private JdbcConnParams jdbcConnParams = null;

    /**
     * tm-params
     */
    private TmParams tmParams = null;

    /**
     * jonas-config-property
     */
    private JLinkedList jonasConfigPropertyList = null;

    /**
     * jonas-connection-definition
     */
    private JLinkedList jonasConnectionDefinitionList = null;

    /**
     * jonas-activationspec
     */
    private JLinkedList jonasActivationspecList = null;

    /**
     * jonas-adminobject
     */
    private JLinkedList jonasAdminobjectList = null;

    /**
     * jonas-security-mapping
     */
    private JonasSecurityMapping jonasSecurityMapping = null;

    /**
     * jonas-connector element XML header
     */
    public static final String JONAS_RA_ROOT_ELEMENT = CommonsSchemas.getHeaderForElement("jonas-connector",
                                                                                          JonasConnectorSchemas.getLastSchema());


    /**
     * Constructor
     */
    public JonasConnector() {
        super();
        jonasConfigPropertyList = new  JLinkedList("jonas-config-property");
        jonasConnectionDefinitionList = new  JLinkedList("jonas-connection-definition");
        jonasActivationspecList = new  JLinkedList("jonas-activationspec");
        jonasAdminobjectList = new  JLinkedList("jonas-adminobject");
    }

    /**
     * Gets the jndiname
     * @return the jndiname
     */
    public String getJndiName() {
        return jndiName;
    }

    /**
     * Set the jndi-name
     * @param jndiName jndi-name
     */
    public void setJndiName(final String jndiName) {
        this.jndiName = jndiName;
    }

    /**
     * Gets the rarlink
     * @return the rarlink
     */
    public String getRarlink() {
        return rarlink;
    }

    /**
     * Set the rarlink
     * @param rarlink rarlink
     */
    public void setRarlink(final String rarlink) {
        this.rarlink = rarlink;
    }

    /**
     * Gets the native-lib
     * @return the native-lib
     */
    public String getNativeLib() {
        return nativeLib;
    }

    /**
     * Set the native-lib
     * @param nativeLib nativeLib
     */
    public void setNativeLib(final String nativeLib) {
        this.nativeLib = nativeLib;
    }

    /**
     * Gets the log-enabled
     * @return the log-enabled
     */
    public String getLogEnabled() {
        return logEnabled;
    }

    /**
     * Set the log-enabled
     * @param logEnabled logEnabled
     */
    public void setLogEnabled(final String logEnabled) {
        this.logEnabled = logEnabled;
    }

    /**
     * Gets the log-topic
     * @return the log-topic
     */
    public String getLogTopic() {
        return logTopic;
    }

    /**
     * Set the log-topic
     * @param logTopic logTopic
     */
    public void setLogTopic(final String logTopic) {
        this.logTopic = logTopic;
    }

    /**
     * Gets the pool-params
     * @return the pool-params
     */
    public PoolParams getPoolParams() {
        return poolParams;
    }

    /**
     * Set the pool-params
     * @param poolParams poolParams
     */
    public void setPoolParams(final PoolParams poolParams) {
        this.poolParams = poolParams;
    }

    /**
     * Gets the jdbc-conn-params
     * @return the jdbc-conn-params
     */
    public JdbcConnParams getJdbcConnParams() {
        return jdbcConnParams;
    }

    /**
     * Set the jdbc-conn-params
     * @param jdbcConnParams jdbcConnParams
     */
    public void setJdbcConnParams(final JdbcConnParams jdbcConnParams) {
        this.jdbcConnParams = jdbcConnParams;
    }

    /**
     * Gets the jonas-config-property
     * @return the jonas-config-property
     */
    public JLinkedList getJonasConfigPropertyList() {
        return jonasConfigPropertyList;
    }

    /**
     * Set the jonas-config-property
     * @param jonasConfigPropertyList jonasConfigProperty
     */
    public void setJonasConfigPropertyList(final JLinkedList jonasConfigPropertyList) {
        this.jonasConfigPropertyList = jonasConfigPropertyList;
    }

    /**
     * Add a new  jonas-config-property element to this object
     * @param jonasConfigProperty the jonasConfigPropertyobject
     */
    public void addJonasConfigProperty(final JonasConfigProperty jonasConfigProperty) {
        jonasConfigPropertyList.add(jonasConfigProperty);
    }

    /**
     * Gets the jonas-connection-definition
     * @return the jonas-connection-definition
     */
    public JLinkedList getJonasConnectionDefinitionList() {
        return jonasConnectionDefinitionList;
    }

    /**
     * Set the jonas-connection-definition-property
     * @param jonasConnectionDefinitionList jonasConnectionDefinition list
     */
    public void setJonasConnectionDefinitionList(final JLinkedList jonasConnectionDefinitionList) {
        this.jonasConnectionDefinitionList = jonasConnectionDefinitionList;
    }

    /**
     * Add a new  jonas-connection-definition-property element to this object
     * @param jonasConnectionDefinition the jonasConnectionDefinition object
     */
    public void addJonasConnectionDefinition(final JonasConnectionDefinition jonasConnectionDefinition) {
        jonasConnectionDefinitionList.add(jonasConnectionDefinition);
    }

    /**
     * Gets the jonas-activationspec
     * @return the jonas-activationspec
     */
    public JLinkedList getJonasActivationspecList() {
        return jonasActivationspecList;
    }

    /**
     * Set the jonas-activationspec-property
     * @param jonasActivationspecList jonas-activationspec-property list
     */
    public void setJonasActivationspecList(final JLinkedList jonasActivationspecList) {
        this.jonasActivationspecList = jonasActivationspecList;
    }

    /**
     * Add a new  jonas-activationspec-property element to this object
     * @param jonasActivationspec the jonasActivationspec object
     */
    public void addJonasActivationspec(final JonasActivationspec jonasActivationspec) {
        jonasActivationspecList.add(jonasActivationspec);
    }

    /**
     * Gets the jonas-adminobject
     * @return the jonas-adminobject
     */
    public JLinkedList getJonasAdminobjectList() {
        return jonasAdminobjectList;
    }

    /**
     * Set the jonas-adminobject-property
     * @param jonasAdminobjectList jonas-adminobject-property list
     */
    public void setJonasAdminobjectList(final JLinkedList jonasAdminobjectList) {
        this.jonasAdminobjectList = jonasAdminobjectList;
    }

    /**
     * Add a new  jonas-adminobject-property element to this object
     * @param jonasAdminobject the jonasAdminobject object
     */
    public void addJonasAdminobject(final JonasAdminobject jonasAdminobject) {
        jonasAdminobjectList.add(jonasAdminobject);
    }

    /**
     * Gets the jonas-security-mapping
     * @return the jonasSecurityMapping
     */
    public JonasSecurityMapping getJonasSecurityMapping() {
        return jonasSecurityMapping;
    }

    /**
     * Set the jonas-security-mapping
     * @param jonasSecurityMapping jonasSecurityMapping
     */
    public void setJonasSecurityMapping(final JonasSecurityMapping jonasSecurityMapping) {
        this.jonasSecurityMapping = jonasSecurityMapping;
    }

    /**
     * Gets the tm-params
     * @return the tm-params
     */
    public TmParams getTmParams() {
        return tmParams;
    }

    /**
     * Set the tm-params
     * @param tmParams tmParams
     */
    public void setTmParams(final TmParams tmParams) {
        this.tmParams = tmParams;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prefixing XML representation.
     * @return the XML description of this object.
     */
    @Override
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append(JONAS_RA_ROOT_ELEMENT);

        indent += 2;

        // jndiname
        sb.append(xmlElement(jndiName, "jndi-name", indent));
        // rarlink
        sb.append(xmlElement(rarlink, "rarlink", indent));
        // native-lib
        sb.append(xmlElement(nativeLib, "native-lib", indent));
        // log-enabled
        sb.append(xmlElement(logEnabled, "log-enabled", indent));
        // log-topic
        sb.append(xmlElement(logTopic, "log-topic", indent));
        // pool-params
        if (poolParams != null) {
            sb.append(poolParams.toXML(indent));
        }
        // jdbc-conn-params
        if (jdbcConnParams != null) {
            sb.append(jdbcConnParams.toXML(indent));
        }
        // tm-params
        if (tmParams != null) {
            sb.append(tmParams.toXML(indent));
        }
        // jonas-config-property
        if (jonasConfigPropertyList != null) {
            sb.append(jonasConfigPropertyList.toXML(indent));
        }
        // jonas-connection-definition
        if (jonasConnectionDefinitionList != null) {
            sb.append(jonasConnectionDefinitionList.toXML(indent));
        }
        // jonas-activationspec
        if (jonasActivationspecList != null) {
            sb.append(jonasActivationspecList.toXML(indent));
        }
        // jonas-adminobject
        if (jonasAdminobjectList != null) {
            sb.append(jonasAdminobjectList.toXML(indent));
        }
        // jonas-security-mapping
        if (jonasSecurityMapping != null) {
            sb.append(jonasSecurityMapping.toXML(indent));
        }
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</jonas-connector>\n");

        return sb.toString();
    }
}
