/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar.rules;

import org.ow2.jonas.deployment.common.rules.JRuleSetBase;

import org.apache.commons.digester.Digester;

/**
 * This class defines the rules to analyze the element jonas-resource
 *
 * @author Florent Benoit
 */

public class JonasConnectorRuleSet extends JRuleSetBase {

    /**
     * Prefix to use with JOnAS 4.0
     */
    private static final String CONNECTOR_PREFIX = "jonas-connector/";

    /**
     * Prefix for backward compliance. This prefix is deprecated
     */
    private static final String OLDCONNECTOR_PREFIX = "jonas-resource/";

    /**
     * Construct an object
     */
    public JonasConnectorRuleSet() {
        super(CONNECTOR_PREFIX);
    }

    /**
     * Construct an object with a specific prefix
     * @param prefix prefix to use
     */
    private JonasConnectorRuleSet(String prefix) {
        super(prefix);
    }

     /**
     * Add a set of rules to the digester object
     * @param digester Digester instance
     */

    public void addRuleInstances(Digester digester) {
        digester.addCallMethod(prefix + "jndi-name",
                               "setJndiName", 0);
        // The rule below is for compliance with jonas-connector_3_0.dtd
        // Now the right way is to use jndi-name
        digester.addCallMethod(prefix + "jndiname",
                               "setJndiName", 0);
        digester.addCallMethod(prefix + "rarlink",
                               "setRarlink", 0);
        digester.addCallMethod(prefix + "native-lib",
                               "setNativeLib", 0);
        digester.addCallMethod(prefix + "log-enabled",
                               "setLogEnabled", 0);
        digester.addCallMethod(prefix + "log-topic",
                               "setLogTopic", 0);
        digester.addRuleSet(new PoolParamsRuleSet(prefix));
        digester.addRuleSet(new JdbcConnParamsRuleSet(prefix));
        digester.addRuleSet(new TmParamsRuleSet(prefix));
        digester.addRuleSet(new JonasConfigPropertyRuleSet(prefix));
        digester.addRuleSet(new JonasConnectionDefinitionRuleSet(prefix));
        digester.addRuleSet(new JonasActivationspecRuleSet(prefix));
        digester.addRuleSet(new JonasAdminobjectRuleSet(prefix));
        digester.addRuleSet(new JonasSecurityMappingRuleSet(prefix));
        // Add same rules but with another prefix
        // This is for backward compliance with 3.0 DTD
        if (prefix.equals(CONNECTOR_PREFIX)) {
            digester.addRuleSet(new JonasConnectorRuleSet(OLDCONNECTOR_PREFIX));
        }

    }
}
