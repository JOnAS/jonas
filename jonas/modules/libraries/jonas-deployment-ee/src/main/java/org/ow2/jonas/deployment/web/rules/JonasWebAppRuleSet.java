/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.web.rules;

import org.apache.commons.digester.Digester;
import org.ow2.jonas.deployment.common.rules.JRuleSetBase;
import org.ow2.jonas.deployment.common.rules.JonasEnvironmentRuleSet;
import org.ow2.jonas.deployment.common.rules.JonasMessageDestinationRuleSet;
import org.ow2.jonas.deployment.common.rules.JonasSecurityRuleSet;

/**
 * This class defines rules to analyze jonas-web.xml file
 * @author Florent Benoit
 */
public class JonasWebAppRuleSet extends JRuleSetBase {

    /**
     * Construct an object
     */
    public JonasWebAppRuleSet() {
        super("jonas-web-app/");
    }

    /**
     * Add a set of rules to the digester object
     * @param digester Digester instance
     */
    @Override
    public void addRuleInstances(final Digester digester) {

        digester.addCallMethod(prefix + "host",
                               "setHost", 0);

        digester.addCallMethod(prefix + "port",
                               "setPort", 0);

        digester.addCallMethod(prefix + "context-root",
                               "setContextRoot", 0);

        digester.addCallMethod(prefix + "tenant-id",
                "setTenantId", 0);

        digester.addCallMethod(prefix + "java2-delegation-model",
                               "setJava2DelegationModel", 0);

        digester.addRuleSet(new JonasEnvironmentRuleSet(prefix));

        digester.addRuleSet(new JonasMessageDestinationRuleSet(prefix));

        digester.addRuleSet(new JonasServletRuleSet(prefix));

        digester.addRuleSet(new JonasSecurityRuleSet(prefix));

    }


}
