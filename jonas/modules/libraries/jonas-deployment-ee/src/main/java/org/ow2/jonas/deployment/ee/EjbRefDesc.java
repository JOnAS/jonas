/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.ee;

import org.ow2.jonas.deployment.api.IEJBRefDesc;
import org.ow2.jonas.deployment.common.xml.EjbRef;
import org.ow2.jonas.deployment.common.xml.JonasEjbRef;


/**
 * This class represents the description of an EjbRef object
 * @author Christophe Ney
 * @author Florent Benoit
 */
public class EjbRefDesc implements IEJBRefDesc {

    /**
     * The ejb ref name.
     */
    private String ejbRefName = null;

    /**
     * The type of the ejb ref
     */
    private String ejbRefType = null;

    /**
     * Fully qualified name of the enterprise bean's home interface
     */
    private String home = null;

    /**
     * Fully qualified name of the enterprise bean's remote interface
     */
    private String remote = null;

    /**
     * The ejb link of the ejb local ref.
     */
    private String ejbLink = null;

    /**
     * The jndi name of the ejb local ref.
     */
    private String jndiName = null;


    /**
     * Construct a descriptor for an ejb-ref tag.
     * @param ejbRef the ejb ref resulting of the xml parsing.
     * @param jonasEjbRef the jonas ejb ref resulting of the xml parsing.
     */
    public EjbRefDesc(EjbRef ejbRef, JonasEjbRef jonasEjbRef) {
        ejbRefName = ejbRef.getEjbRefName();
        ejbRefType = ejbRef.getEjbRefType();
        ejbLink = null;
        if (ejbRef.getEjbLink() != null) {
            ejbLink = ejbRef.getEjbLink();
        }
        jndiName = null;
        if (jonasEjbRef != null) {
            jndiName = jonasEjbRef.getJndiName();
        }
        this.home = ejbRef.getHome();
        this.remote = ejbRef.getRemote();

    }

    public String getEjbRefName() {
        return ejbRefName;
    }

    public String getEjbRefType() {
        return ejbRefType;
    }

    public String getEjbLink() {
        return ejbLink;
    }


    public String getJndiName() {
        return jndiName;
    }

    /**
     * Set the jndi name of the ejb-ref.
     * @param jndiName representation of the JNDI name
     */
    public void setJndiName(String jndiName) {
        this.jndiName = jndiName;
    }


    /**
     * String representation of the object for test purpose
     * @return String representation of this object
     */
    public String toString() {
        StringBuffer ret = new StringBuffer();
        ret.append("\ngetEjbRefName()=" + getEjbRefName());
        ret.append("\ngetEjbRefType()=" + getEjbRefType());
        ret.append("\ngetEjbLink()=" + getEjbLink());
        ret.append("\ngetJndiName()=" + getJndiName());
        return ret.toString();
    }

    public String getHome() {
        return home;
    }
    
    public String getRemote() {
        return remote;
    }
}
