/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar;

import java.io.Serializable;

import org.ow2.jonas.deployment.rar.xml.PoolParams;


/**
 * This class defines the implementation of the element pool-params
 *
 * @author Eric Hardesty
 */

public class PoolParamsDesc  implements Serializable {

    /**
     * pool-init
     */
    private String poolInit = null;

    /**
     * pool-min
     */
    private String poolMin = null;

    /**
     * pool-max
     */
    private String poolMax = null;

    /**
     * pool-max-age
     */
    private String poolMaxAge = null;

    /**
     * pstmt-max
     */
    private String pstmtMax = null;

    /**
     * pstmt-cache-policy
     */
    private String pstmtCachePolicy = null;

    /**
     * pool-max-age-minutes
     */
    private String poolMaxAgeMinutes = null;

    /**
     * pool-max-opentime
     */
    private String poolMaxOpentime = null;

    /**
     * pool-max-waiters
     */
    private String poolMaxWaiters = null;

    /**
     * pool-max-waittime
     */
    private String poolMaxWaittime = null;

    /**
     * pool-sampling-period
     */
    private String poolSamplingPeriod = null;

    /**
     * Constructor
     * @param pp PoolParams to set
     */
    public PoolParamsDesc(final PoolParams pp) {
        if (pp != null) {
            poolInit = pp.getPoolInit();
            poolMin = pp.getPoolMin();
            poolMax = pp.getPoolMax();
            poolMaxAge = pp.getPoolMaxAge();
            pstmtMax = pp.getPstmtMax();
            pstmtCachePolicy = pp.getPstmtCachePolicy();
            poolMaxAgeMinutes = pp.getPoolMaxAgeMinutes();
            poolMaxOpentime = pp.getPoolMaxOpentime();
            poolMaxWaiters = pp.getPoolMaxWaiters();
            poolMaxWaittime = pp.getPoolMaxWaittime();
            poolSamplingPeriod = pp.getPoolSamplingPeriod();
        }
    }

    /**
     * Gets the pool-init
     * @return the pool-init
     */
    public String getPoolInit() {
        return poolInit;
    }

    /**
     * Gets the pool-min
     * @return the pool-min
     */
    public String getPoolMin() {
        return poolMin;
    }

    /**
     * Gets the pool-max
     * @return the pool-max
     */
    public String getPoolMax() {
        return poolMax;
    }

    /**
     * Gets the pool-max-age
     * @return the pool-max-age
     */
    public String getPoolMaxAge() {
        return poolMaxAge;
    }

    /**
     * Gets the pstmt-max
     * @return the pstmt-max
     */
    public String getPstmtMax() {
        return pstmtMax;
    }

    /**
     * Gets the pstmt-cache-policy
     * @return the pstmt-cache-policy
     */
    public String getPstmtCachePolicy() {
        return pstmtCachePolicy;
    }

    /**
     * Gets the pool-max-age-minutes
     * @return the pool-max-age-minutes
     */
    public String getPoolMaxAgeMinutes() {
        return poolMaxAgeMinutes;
    }

    /**
     * Gets the pool-max-opentime
     * @return the pool-max-opentime
     */
    public String getPoolMaxOpentime() {
        return poolMaxOpentime;
    }

    /**
     * Gets the pool-max-waiters
     * @return the pool-max-waiters
     */
    public String getPoolMaxWaiters() {
        return poolMaxWaiters;
    }

    /**
     * Gets the pool-max-waittime
     * @return pool-max-waittime
     */
    public String getPoolMaxWaittime() {
        return poolMaxWaittime;
    }

    /**
     * Gets the pool-sampling-period
     * @return pool-sampling-period
     */
    public String getPoolSamplingPeriod() {
        return poolSamplingPeriod;
    }
}
