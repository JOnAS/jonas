/* Generated By:JJTree: Do not edit this line. ASTArithmeticLiteral.java */

package org.ow2.jonas.deployment.ejb.ejbql;

public class ASTArithmeticLiteral extends SimpleNode {
  public ASTArithmeticLiteral(int id) {
    super(id);
  }

  public ASTArithmeticLiteral(EJBQL p, int id) {
    super(p, id);
  }


  /** Accept the visitor. **/
  public Object jjtAccept(EJBQLVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
}
