/* Generated By:JJTree: Do not edit this line. ASTSingleValuedPathExpression.java */

package org.ow2.jonas.deployment.ejb.ejbql;

public class ASTSingleValuedPathExpression extends SimpleNode {
  public ASTSingleValuedPathExpression(int id) {
    super(id);
  }

  public ASTSingleValuedPathExpression(EJBQL p, int id) {
    super(p, id);
  }


  /** Accept the visitor. **/
  public Object jjtAccept(EJBQLVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
}
