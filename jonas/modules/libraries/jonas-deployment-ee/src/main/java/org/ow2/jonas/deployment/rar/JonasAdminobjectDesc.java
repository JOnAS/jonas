/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar;

import java.io.Serializable;
import java.util.List;

import org.ow2.jonas.deployment.rar.xml.JonasAdminobject;


/**
 * This class defines the implementation of the element jonas-adminobject
 *
 * @author Eric Hardesty
 */

public class JonasAdminobjectDesc implements Serializable {

    /**
     * jonas-adminiobjectinterface
     */
    private String jonasAdminobjectinterface = null;

    /**
     * jonas-adminiobjectclass
     */
    private String jonasAdminobjectclass = null;

    /**
     * id
     */
    private String id = null;

    /**
     * description
     */ 
    private List descriptionList = null;

    /**
     * jndiname
     */
    private String jndiName = null;

    /**
     * jonas-config-property
     */
    private List jonasConfigPropertyList = null;


    /**
     * Constructor
     */
    public JonasAdminobjectDesc(JonasAdminobject ja) {
        if (ja != null) {
            jonasAdminobjectinterface = ja.getJonasAdminobjectinterface();
            jonasAdminobjectclass = ja.getJonasAdminobjectclass();
            id = ja.getId();
            descriptionList = ja.getDescriptionList();
            jndiName = ja.getJndiName();
            jonasConfigPropertyList = Utility.jonasConfigProperty(ja.getJonasConfigPropertyList());
        }
    }

    /**
     * Gets the jonas-adminobjectinterface
     * @return the jonas-adminobjectinterface
     */
    public String getJonasAdminobjectinterface() {
        return jonasAdminobjectinterface;
    }

    /**
     * Gets the jonas-adminobjectclass
     * @return the jonas-adminobjectclass
     */
    public String getJonasAdminobjectclass() {
        return jonasAdminobjectclass;
    }

    /**
     * Gets the id
     * @return the id
     */
    public String getId() {
        return id;
    }

    /** 
     * Gets the description
     * @return the description
     */
    public List getDescriptionList() {
        return descriptionList;
    }

    /**
     * Gets the jndiName
     * @return the jndiname
     */
    public String getJndiName() {
        return jndiName;
    }

    /**
     * Gets the jonas-config-property
     * @return the jonas-config-property
     */
    public List getJonasConfigPropertyList() {
        return jonasConfigPropertyList;
    }

}
