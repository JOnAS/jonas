/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Christophe Ney
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.ee;

import org.ow2.jonas.deployment.api.IEJBLocalRefDesc;
import org.ow2.jonas.deployment.common.DeploymentDescException;
import org.ow2.jonas.deployment.common.xml.EjbLocalRef;


/**
 * This class represents the description of an EjblocalRef object
 * @author Christophe Ney
 * @author Florent Benoit
 */
public class EjbLocalRefDesc implements IEJBLocalRefDesc {

    /**
     * The ejb local ref name.
     */
    private String ejbRefName = null;

    /**
     * The type of the ejb ref
     */
    private String ejbRefType = null;

    /**
     * The local home of the ejb local ref.
     */
    private String localHome = null;

    /**
     * The local interface name of the ejb local ref.
     */
    private String local = null;

    /**
     * The ejb link of the ejb local ref.
     */
    private String ejbLink = null;

    /**
     * The jndi name of the ejb local ref.
     */
    private String jndiName = null;


    /**
     * Construct a descriptor for an ejb-local-ref tag.
     * @param ejbLocalRef the ejb local ref resulting of the xml parsing.
     * @throws DeploymentDescException when missing information for
     * creating the EjbLocalRefDesc.
     */
    public EjbLocalRefDesc(EjbLocalRef ejbLocalRef) throws DeploymentDescException {
        ejbRefName = ejbLocalRef.getEjbRefName();
        ejbRefType = ejbLocalRef.getEjbRefType();
        localHome = ejbLocalRef.getLocalHome();
        local = ejbLocalRef.getLocal();
        if (ejbLocalRef.getEjbLink() == null) {
            String err = "ejb-link missing for ejb-local-ref '" + ejbRefName + "'.";
            throw new DeploymentDescException(err);
        }
        ejbLink = ejbLocalRef.getEjbLink();
    }

    /**
     * Get the name of the ejb-ref
     * @return String representation of the ejb-ref-name.
     */
    public String getEjbRefName() {
        return ejbRefName;
    }

    /**
     * Get the ejb-ref-type.
     * @return String representation of the ejb-ref-type.
     */
    public String getEjbRefType() {
        return ejbRefType;
    }

    /**
     * Get the LocalHome class of the target bean
     * @return String representation of the class.
     */
    public String getLocalHome() {
        return localHome;
    }

    /**
     * Get the Local class of the target bean
     * @return String representation of the class.
     */
    public String getLocal() {
        return local;
    }

    /**
     * Get the ejb-link
     * @return String representation of the ejb-link
     */
    public String getEjbLink() {
        return ejbLink;
    }

    /**
     * Get the jndi name of the ejb-local-ref.
     * @return the string representation of the JNDI name
     */
    public String getJndiLocalName() {
        return jndiName;
    }

    /**
     * Set the jndi name of the ejb-local-ref.
     * @param jndiName the string representation of the jndi name.
     */
    public void setJndiLocalName(String jndiName) {
        this.jndiName = jndiName;
    }

    /**
     * String representation of the object for test purpose
     * @return String representation of this object
     */
    public String toString() {
        StringBuffer ret = new StringBuffer();
        ret.append("\ngetEjbRefName()=" + getEjbRefName());
        ret.append("\ngetEjbRefType()=" + getEjbRefType());
        ret.append("\ngetLocalHome()=" + getLocalHome());
        ret.append("\ngetLocal()=" + getLocal());
        ret.append("\ngetEjbLink()=" + getEjbLink());
        ret.append("\ngetJndiLocalName()=" + getJndiLocalName());
        return ret.toString();
    }

}
