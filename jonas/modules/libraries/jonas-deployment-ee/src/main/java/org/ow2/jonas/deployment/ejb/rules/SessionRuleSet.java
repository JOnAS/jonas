/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.rules;

import org.apache.commons.digester.Digester;
import org.ow2.jonas.deployment.common.rules.EnvironmentRuleSet;
import org.ow2.jonas.deployment.common.rules.JRuleSetBase;
import org.ow2.jonas.deployment.common.rules.SecurityRoleRefRuleSet;


/**
 * This class defines the rules to analyze the element session
 *
 * @author JOnAS team
 */

public class SessionRuleSet extends JRuleSetBase {

    /**
     * Construct an object with a specific prefix
     * @param prefix prefix to use during the parsing
     */
    public SessionRuleSet(final String prefix) {
        super(prefix);
   }
     /**
     * Add a set of rules to the digester object
     * @param digester Digester instance
     */

    @Override
    public void addRuleInstances(final Digester digester) {
        digester.addObjectCreate(prefix + "session",
                                 "org.ow2.jonas.deployment.ejb.xml.Session");
        digester.addSetNext(prefix + "session",
                            "addSession",
                            "org.ow2.jonas.deployment.ejb.xml.Session");
        digester.addCallMethod(prefix + "session/description",
                               "setDescription", 0);
        digester.addCallMethod(prefix + "session/display-name",
                               "setDisplayName", 0);
        digester.addCallMethod(prefix + "session/small-icon",
                               "setSmallIcon", 0);
        digester.addCallMethod(prefix + "session/large-icon",
                               "setLargeIcon", 0);
        digester.addCallMethod(prefix + "session/ejb-name",
                               "setEjbName", 0);
        digester.addCallMethod(prefix + "session/mapped-name",
                               "setMappedName", 0);
        digester.addCallMethod(prefix + "session/home",
                               "setHome", 0);
        digester.addCallMethod(prefix + "session/remote",
                               "setRemote", 0);
        digester.addCallMethod(prefix + "session/local-home",
                               "setLocalHome", 0);
        digester.addCallMethod(prefix + "session/local",
                               "setLocal", 0);
        digester.addCallMethod(prefix + "session/service-endpoint",
                "setServiceEndpoint", 0);
        digester.addCallMethod(prefix + "session/ejb-class",
                               "setEjbClass", 0);
        digester.addCallMethod(prefix + "session/session-type",
                               "setSessionType", 0);
        digester.addCallMethod(prefix + "session/transaction-type",
                               "setTransactionType", 0);
        digester.addRuleSet(new EnvironmentRuleSet(prefix + "session/"));
        digester.addRuleSet(new SecurityRoleRefRuleSet(prefix + "session/"));
        digester.addRuleSet(new SecurityIdentityRuleSet(prefix + "session/"));
        digester.addRuleSet(new IorSecurityConfigRuleSet(prefix + "session/"));
   }
}
