/* Generated By:JJTree: Do not edit this line. ASTIdentifier.java */

package org.ow2.jonas.deployment.ejb.ejbql;

public class ASTIdentifier extends SimpleNode {
  public ASTIdentifier(int id) {
    super(id);
  }

  public ASTIdentifier(EJBQL p, int id) {
    super(p, id);
  }


  /** Accept the visitor. **/
  public Object jjtAccept(EJBQLVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
}
