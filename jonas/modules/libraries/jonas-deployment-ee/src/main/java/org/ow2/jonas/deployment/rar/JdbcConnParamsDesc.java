/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar;

import java.io.Serializable;

import org.ow2.jonas.deployment.rar.xml.JdbcConnParams;


/**
 * This class defines the implementation of the element jdbc-conn-params
 *
 * @author Eric Hardesty
 */

public class JdbcConnParamsDesc implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * jdbc-check-level
     */
    private String jdbcCheckLevel = null;

    /**
     * jdbc-test-statement
     */
    private String jdbcTestStatement = null;

    /**
     * Becomes true only if the jdbc-conn-params are present.
     */
    private boolean jdbcConnSetUp = false;


    /**
     * Constructor
     */
    public JdbcConnParamsDesc(final JdbcConnParams jcp) {
        if (jcp != null) {
            jdbcCheckLevel = jcp.getJdbcCheckLevel();
            jdbcTestStatement = jcp.getJdbcTestStatement();
            jdbcConnSetUp = true;
        }
    }

    /**
     * Gets the jdbc-check-level
     * @return the jdbc-check-level
     */
    public String getJdbcCheckLevel() {
        return jdbcCheckLevel;
    }

    /**
     * Gets the jdbc-test-statement
     * @return the jdbc-test-statement
     */
    public String getJdbcTestStatement() {
        return jdbcTestStatement;
    }

    /**
     *
     * @return true only if the jdbc-conn-params are present.
     */
    public boolean isJdbcConnSetUp() {
        return jdbcConnSetUp;
    }

}
