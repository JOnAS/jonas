/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.deployment.ejb;

/**
 * Class to hold meta-information related to a cmp-field of a bean with jdbc based store
 * @author Christophe Ney [cney@batisseurs.com] : Initial developer
 * @author Helene Joanin on May 2003: code cleanup
 * @author Helene Joanin on May 2003: add sqlType.
 */
// TODO : Remove this class and merge it with the FieldDesc class.

public class FieldJdbcDesc extends FieldDesc {


    protected String jdbcFieldName = null;
    protected String sqlType = null;

    /**
     * Get database field name in the table where bean is stored
     * @return Name of the field in the database
     */
    public String getJdbcFieldName() {
        return jdbcFieldName;
    }

    /**
     * Jdbc field name setter.
     */
    protected void setJdbcFieldName(String jdbcFieldName) {
        this.jdbcFieldName = jdbcFieldName;
    }

    /**
     * Is the field sql-type is defined.
     * @return true if the sql-type of the field is defined.
     */
    public boolean hasSqlType() {
        return (sqlType != null);
    }

    /**
     * Get field sql-type
     * @return sql-type of the field in the database
     */
    public String getSqlType() {
        return sqlType;
    }

    /**
     * Field sql-type setter.
     */
    protected void setSqlType(String st) {
        sqlType = st;
    }

    /**
     * String representation of the object for test purpose
     * @return String representation of this object
     */
    public String toString() {
        StringBuffer ret = new StringBuffer();
        ret.append(super.toString());
        ret.append("\ngetJdbcFieldName()=" + getJdbcFieldName());
        if (hasSqlType()) {
            ret.append("\ngetSqlType()=" + getSqlType());
        }
        return ret.toString();
    }

}
