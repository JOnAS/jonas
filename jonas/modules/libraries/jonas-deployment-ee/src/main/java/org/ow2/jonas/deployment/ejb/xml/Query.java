/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
/**
 * This class defines the implementation of the element query
 *
 * @author JOnAS team
 */

public class Query extends AbsElement  {

    /**
     * description
     */
    private String description = null;

    /**
     * query-method
     */
    private QueryMethod queryMethod = null;

    /**
     * result-type-mapping
     */
    private String resultTypeMapping = null;

    /**
     * ejb-ql
     */
    private String ejbQl = null;


    /**
     * Constructor
     */
    public Query() {
        super();
    }

    /**
     * Gets the description
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the description
     * @param description description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the query-method
     * @return the query-method
     */
    public QueryMethod getQueryMethod() {
        return queryMethod;
    }

    /**
     * Set the query-method
     * @param queryMethod queryMethod
     */
    public void setQueryMethod(QueryMethod queryMethod) {
        this.queryMethod = queryMethod;
    }

    /**
     * Gets the result-type-mapping
     * @return the result-type-mapping
     */
    public String getResultTypeMapping() {
        return resultTypeMapping;
    }

    /**
     * Set the result-type-mapping
     * @param resultTypeMapping resultTypeMapping
     */
    public void setResultTypeMapping(String resultTypeMapping) {
        this.resultTypeMapping = resultTypeMapping;
    }

    /**
     * Gets the ejb-ql
     * @return the ejb-ql
     */
    public String getEjbQl() {
        return ejbQl;
    }

    /**
     * Set the ejb-ql
     * @param ejbQl ejbQl
     */
    public void setEjbQl(String ejbQl) {
        this.ejbQl = ejbQl;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<query>\n");

        indent += 2;

        // description
        sb.append(xmlElement(description, "description", indent));
        // query-method
        if (queryMethod != null) {
            sb.append(queryMethod.toXML(indent));
        }
        // result-type-mapping
        sb.append(xmlElement(resultTypeMapping, "result-type-mapping", indent));
        // ejb-ql
        sb.append(xmlElement(ejbQl, "ejb-ql", indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</query>\n");

        return sb.toString();
    }
}
