/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.deployment.ejb;

import java.lang.reflect.Method;

import org.ow2.jonas.deployment.common.DeploymentDescException;

/**
 * Class to hold meta-information related to CMP v1 attributes of a method when a jdbc
 * data store is used.
 * @author Christophe Ney [cney@batisseurs.com] : Initial developer
 * @author Helene Joanin
 */
public class MethodJdbcCmp1Desc extends MethodDesc  {

    protected String whereClause = null;
    protected int whereClauseStatus = APPLY_TO_BEAN;

    /**
     * constructor to be used by parent node
     */
    public MethodJdbcCmp1Desc(BeanDesc beanDesc,Method meth, Class classDef, int index) {
        super(beanDesc,meth, classDef,index);
    }

    /**
     * Overwrite JdbcWhereClause
     * @param jdbcWhereClause new value for jdbcWhereClause
     * @param status applicability of given transAttribute parameter
     */
    void overwriteJdbcWhereClause(String jdbcWhereClause,int status)
            throws DeploymentDescException {
        if (status<this.whereClauseStatus)
            return;
        // Replace special characters (as line-feed, carriage-return) by a white space
        char[] iwc = jdbcWhereClause.toCharArray();
        char[] owc = new char[iwc.length];
        for (int i = 0; i < iwc.length; i++) {
            if (Character.isWhitespace(iwc[i])) {
                owc[i] = ' ';
            } else {
                owc[i] = iwc[i];
            }
        }
        whereClause = new String(owc);
        whereClauseStatus = status;
    }

    /**
     * Assessor for 'WHERE' clause set
     * @return true if where clause has been set
     */
    public boolean hasWhereClause(){
        return whereClause!=null;
    }

    /**
     * Get 'WHERE' clause for the method
     * @return String containing 'WHERE' clause
     */
    public String getWhereClause(){
        if (whereClause==null) {
            throw new Error("No whereClause defined for this method");
        }
        return whereClause;
    }

    /**
     * Get 'WHERE' clause status for the method
     */
    public int getWhereClauseStatus(){
        return whereClauseStatus;
    }

    /**
     * String representation of the object for test purpose
     * @return String representation of this object
     */
    public String toString() {
        StringBuffer ret = new StringBuffer();
        ret.append(super.toString());
        if (hasWhereClause()) {
            ret.append("\ngetWhereClause()="+getWhereClause());
            ret.append("\ngetWhereClauseStatus()="+APPLY_TO[getWhereClauseStatus()]);
        }
        return ret.toString();
    }

}
