/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Christophe Ney
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.ee;

import org.ow2.jonas.deployment.api.IResourceRefDesc;
import org.ow2.jonas.deployment.common.DeploymentDescException;
import org.ow2.jonas.deployment.common.xml.JonasResource;
import org.ow2.jonas.deployment.common.xml.ResourceRef;



/**
 * This class represents the description of a ResourceRef object
 * @author Christophe Ney
 * @author Florent Benoit
 */
public class ResourceRefDesc implements IResourceRefDesc {

    /**
     * List of all possible authentication.
     */
    private static final String[] AUTH = {"APPLICATION_AUTH", "CONTAINER_AUTH"};

    /**
     * The resource ref name.
     */
    private String name;

    /**
     * The resource ref type name.
     */
    private String typeName;

    /**
     * The resource ref authentication.
     */
    private int authentication;

    /**
     * The resource ref jndi name.
     */
    private String jndiName;

    /**
     * Construct a descriptor for the resource-ref and jonas-resource tags.
     * @param classLoader the classloader for the classes.
     * @param res the resource-ref resulting of the web.xml parsing.
     * @param jRes the jonas-resource resulting of the jonas-web.xml parsing.
     * @throws DeploymentDescException when missing information for
     * creating the ResourceRefDesc.
     */
    public ResourceRefDesc(ClassLoader classLoader,
                           ResourceRef res, JonasResource jRes)
        throws DeploymentDescException {
        name = res.getResRefName();
        typeName = new String(res.getResType());
        String auth = res.getResAuth();

        if (auth.equals("Application")) {
            authentication = IResourceRefDesc.APPLICATION_AUTH;
        } else if (auth.equals("Container")) {
            authentication = IResourceRefDesc.CONTAINER_AUTH;
        } else {
            throw new DeploymentDescException("res-auth not valid for resource-ref " + name);
        }
        jndiName = jRes.getJndiName();
    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.deployment.api.IResourceRefDesc#getName()
     */
    public String getName() {
        return name;
    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.deployment.api.IResourceRefDesc#getTypeName()
     */
    public String getTypeName() {
        return typeName;
    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.deployment.api.IResourceRefDesc#getAuthentication()
     */
    public int getAuthentication() {
        return authentication;
    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.deployment.api.IResourceRefDesc#isJdbc()
     */
    public boolean isJdbc() {
        return "javax.sql.DataSource".equals(typeName);
    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.deployment.api.IResourceRefDesc#getJndiName()
     */
    public String getJndiName() {
        return jndiName;
    }

    /**
     * String representation of the object for test purpose
     * @return String representation of this object
     */
    public String toString() {
        StringBuffer ret = new StringBuffer();
        ret.append("\ngetName()=" + getName());
        ret.append("\ngetTypeName()=" + getTypeName());
        ret.append("\ngetAuthentication()=" + AUTH[getAuthentication()]);
        ret.append("\nisJdbc()=" + new Boolean(isJdbc()).toString());
        ret.append("\ngetJndiName()=" + getJndiName());
        return ret.toString();
    }
}
