/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.rules;

import org.ow2.jonas.deployment.common.rules.JRuleSetBase;
import org.ow2.jonas.deployment.common.rules.JonasEnvironmentRuleSet;
import org.ow2.jonas.deployment.ejb.lib.util.ClusterUtil;
import org.apache.commons.digester.Digester;

/**
 * This class defines the rules to analyze the element jonas-session
 *
 * @author JOnAS team
 */

public class JonasSessionRuleSet extends JRuleSetBase {

    /**
     * Construct an object with a specific prefix
     * @param prefix prefix to use during the parsing
     */
    public JonasSessionRuleSet(final String prefix) {
        super(prefix);
   }
     /**
     * Add a set of rules to the digester object
     * @param digester Digester instance
     */

    @Override
    public void addRuleInstances(final Digester digester) {
        digester.addObjectCreate(prefix + "jonas-session",
        "org.ow2.jonas.deployment.ejb.xml.JonasSession");
        digester.addSetNext(prefix + "jonas-session",
                "addJonasSession",
        "org.ow2.jonas.deployment.ejb.xml.JonasSession");
        digester.addCallMethod(prefix + "jonas-session/ejb-name",
                "setEjbName", 0);
        digester.addCallMethod(prefix + "jonas-session/jndi-name",
                "setJndiName", 0);
        digester.addCallMethod(prefix + "jonas-session/jndi-local-name",
                "setJndiLocalName", 0);
        digester.addCallMethod(prefix + "jonas-session/jndi-endpoint-name",
                "setJndiEndpointName", 0);
        digester.addRuleSet(new JonasEnvironmentRuleSet(prefix + "jonas-session/"));

        digester.addCallMethod(prefix + "jonas-session/is-modified-method-name",
                "setIsModifiedMethodName", 0);
        digester.addCallMethod(prefix + "jonas-session/session-timeout",
                "setSessionTimeout", 0);
        digester.addCallMethod(prefix + "jonas-session/max-cache-size",
                "setMaxCacheSize", 0);
        digester.addCallMethod(prefix + "jonas-session/min-pool-size",
                "setMinPoolSize", 0);
        digester.addCallMethod(prefix + "jonas-session/monitoring-enabled",
                "setMonitoringEnabled", 0);
        digester.addCallMethod(prefix + "jonas-session/warning-threshold",
                "setWarningThreshold", 0);
        digester.addCallMethod(prefix + "jonas-session/singleton",
                "setSingleton", 0);
        digester.addCallMethod(prefix + "jonas-session/cluster-replicated",
                "setClusterReplicated", 0);

        digester.addRuleSet(new IorSecurityConfigRuleSet(prefix + "jonas-session/"));

        ClusterUtil.addClusterRule(prefix + "jonas-session/", digester);

    }
}
