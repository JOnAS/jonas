#!/bin/sh

#
# This script is only an example to show how the java sources of this directory
# may be generated from the grammar file
#

JAVACC_HOME=/usr/java/javacc/current
TMP_DIR=./NEW
CUR_PWD=`pwd`

set -x

# Initialization
mkdir $TMP_DIR
cp EJBQL.jjt $TMP_DIR
# Java sources generation
cd $TMP_DIR
$JAVACC_HOME/bin/jjtree EJBQL.jjt
$JAVACC_HOME/bin/javacc EJBQL.jj
# Diff between the new sources and the old sources
set +x
for f in `ls *.java`
do
  if [ -f $CUR_PWD/$f ]
  then
    echo "====== $f differences: "
    diff $f $CUR_PWD/$f
  else
    echo "====== $f ADDED "
  fi
done
cd $CUR_PWD
for f in `ls *.java`
do
  if [ ! -f $TMP_DIR/$f ]
  then
    echo "====== $f REMOVED "
  fi
done



