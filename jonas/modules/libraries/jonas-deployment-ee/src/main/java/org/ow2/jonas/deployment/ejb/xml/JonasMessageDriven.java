/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.xml;


/**
 * This class defines the implementation of the element jonas-message-driven
 *
 * @author JOnAS team
 */

public class JonasMessageDriven extends JonasCommonEjb {


    /**
     * jonas-message-driven-destination
     */
    private JonasMessageDrivenDestination jonasMessageDrivenDestination = null;

    /**
     * activation-config (EJB2.1 only)
     */
    private ActivationConfig  activationConfig = null;

    /**
     * Constructor
     */
    public JonasMessageDriven() {
        super();
    }



    /**
     * Gets the jonas-message-driven-destination
     * @return the jonas-message-driven-destination
     */
    public JonasMessageDrivenDestination getJonasMessageDrivenDestination() {
        return jonasMessageDrivenDestination;
    }

    /**
     * Set the jonas-message-driven-destination
     * @param jonasMessageDrivenDestination jonasMessageDrivenDestination
     */
    public void setJonasMessageDrivenDestination(JonasMessageDrivenDestination jonasMessageDrivenDestination) {
        this.jonasMessageDrivenDestination = jonasMessageDrivenDestination;
    }

    /**
     * Gets the activation-config (EJB2.1 only)
     * @return the activation-config
     */
    public ActivationConfig getActivationConfig() {
        return activationConfig;
    }

    /**
     * Set the activation-config (EJB2.1 only)
     * @param activationConfig activation-config
     */
    public void setActivationConfig(ActivationConfig activationConfig) {
        this.activationConfig = activationConfig;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<jonas-message-driven>\n");

        indent += 2;

        // ejb-name
        sb.append(xmlElement(getEjbName(), "ejb-name", indent));
        // jonas-message-driven-destination
        if (jonasMessageDrivenDestination != null) {
            sb.append(jonasMessageDrivenDestination.toXML(indent));
        }
        // activation-config (EJB2.1 only)
        if (activationConfig != null) {
            sb.append(activationConfig.toXML(indent));
        }

        // jonas-ejb-ref
        sb.append(getJonasEjbRefList().toXML(indent));
        // jonas-resource
        sb.append(getJonasResourceList().toXML(indent));
        // jonas-resource-env
        sb.append(getJonasResourceEnvList().toXML(indent));
        // jonas-service-ref
        sb.append(getJonasServiceRefList().toXML(indent));
        // jonas-message-destination-ref
        sb.append(getJonasMessageDestinationRefList().toXML(indent));
        // max-cache-size
        sb.append(xmlElement(getMaxCacheSize(), "max-cache-size", indent));
        // min-pool-size
        sb.append(xmlElement(getMinPoolSize(), "min-pool-size", indent));

        // run-as
        if (getRunAsPrincipalName() != null) {
            sb.append(indent(indent));
            sb.append("<run-as>\n");
            indent += 2;
            sb.append(xmlElement(getRunAsPrincipalName(), "principal-name", indent));
            indent -= 2;
            sb.append(indent(indent));
            sb.append("</run-as>\n");
        }
        // ior-security-config
        if (getIorSecurityConfig() != null) {
            sb.append(getIorSecurityConfig().toXML(indent));
        }
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</jonas-message-driven>\n");

        return sb.toString();
    }
}
