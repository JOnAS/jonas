/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: jonas-team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.client.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;

/**
 * This class defines the implementation of the element jonas-security
 * @author jonas-team
 */

public class JonasSecurity extends AbsElement {

    /**
     * jaasfile
     */
    private String jaasfile = null;

    /**
     * jaasentry
     */
    private String jaasentry = null;

    /**
     * username
     */
    private String username = null;

    /**
     * password
     */
    private String password = null;

    /**
     * Default constructor
     */
    public JonasSecurity() {
        super();
    }

    /**
     * @return the jaasfile
     */
    public String getJaasfile() {
        return jaasfile;
    }

    /**
     * Set the jaasfile
     * @param jaasfile jaasfile
     */
    public void setJaasfile(String jaasfile) {
        this.jaasfile = jaasfile;
    }

    /**
     * @return the jaasentry
     */
    public String getJaasentry() {
        return jaasentry;
    }

    /**
     * Set the jaasentry
     * @param jaasentry jaasentry
     */
    public void setJaasentry(String jaasentry) {
        this.jaasentry = jaasentry;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Set the username
     * @param username username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set the password
     * @param password password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<jonas-security>\n");

        indent += 2;

        // jaasfile
        sb.append(xmlElement(jaasfile, "jaasfile", indent));
        // jaasentry
        sb.append(xmlElement(jaasentry, "jaasentry", indent));
        // username
        sb.append(xmlElement(username, "username", indent));
        // password
        sb.append(xmlElement(password, "password", indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</jonas-security>\n");

        return sb.toString();
    }
}