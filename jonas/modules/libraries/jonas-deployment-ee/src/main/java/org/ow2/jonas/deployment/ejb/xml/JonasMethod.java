/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
/**
 * This class defines the implementation of the element jonas-method
 *
 * @author JOnAS team
 */

public class JonasMethod extends AbsElement  {

    /**
     * method-name
     */
    private String methodName = null;

    /**
     * method-params
     */
    private MethodParams methodParams = null;


    /**
     * Constructor
     */
    public JonasMethod() {
        super();
    }

    /**
     * Gets the method-name
     * @return the method-name
     */
    public String getMethodName() {
        return methodName;
    }

    /**
     * Set the method-name
     * @param methodName methodName
     */
    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    /**
     * Gets the method-params
     * @return the method-params
     */
    public MethodParams getMethodParams() {
        return methodParams;
    }

    /**
     * Set the method-params
     * @param methodParams methodParams
     */
    public void setMethodParams(MethodParams methodParams) {
        this.methodParams = methodParams;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<jonas-method>\n");

        indent += 2;

        // method-name
        sb.append(xmlElement(methodName, "method-name", indent));
        // method-params
        if (methodParams != null) {
            sb.append(methodParams.toXML(indent));
        }
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</jonas-method>\n");

        return sb.toString();
    }
}
