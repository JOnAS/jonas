/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;
/**
 * This class defines the implementation of the element enterprise-beans
 *
 * @author JOnAS team
 */

public class EnterpriseBeans extends AbsElement  {

    /**
     * session
     */
    private JLinkedList sessionList = null;

    /**
     * entity
     */
    private JLinkedList entityList = null;

    /**
     * message-driven
     */
    private JLinkedList messageDrivenList = null;


    /**
     * Constructor
     */
    public EnterpriseBeans() {
        super();
        sessionList = new  JLinkedList("session");
        entityList = new  JLinkedList("entity");
        messageDrivenList = new  JLinkedList("message-driven");
    }

    /**
     * Gets the session
     * @return the session
     */
    public JLinkedList getSessionList() {
        return sessionList;
    }

    /**
     * Set the session
     * @param sessionList session
     */
    public void setSessionList(JLinkedList sessionList) {
        this.sessionList = sessionList;
    }

    /**
     * Add a new  session element to this object
     * @param session the sessionobject
     */
    public void addSession(Session session) {
        sessionList.add(session);
    }

    /**
     * Gets the entity
     * @return the entity
     */
    public JLinkedList getEntityList() {
        return entityList;
    }

    /**
     * Set the entity
     * @param entityList entity
     */
    public void setEntityList(JLinkedList entityList) {
        this.entityList = entityList;
    }

    /**
     * Add a new  entity element to this object
     * @param entity the entityobject
     */
    public void addEntity(Entity entity) {
        entityList.add(entity);
    }

    /**
     * Gets the message-driven
     * @return the message-driven
     */
    public JLinkedList getMessageDrivenList() {
        return messageDrivenList;
    }

    /**
     * Set the message-driven
     * @param messageDrivenList messageDriven
     */
    public void setMessageDrivenList(JLinkedList messageDrivenList) {
        this.messageDrivenList = messageDrivenList;
    }

    /**
     * Add a new  message-driven element to this object
     * @param messageDriven the messageDrivenobject
     */
    public void addMessageDriven(MessageDriven messageDriven) {
        messageDrivenList.add(messageDriven);
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<enterprise-beans>\n");

        indent += 2;

        // session
        sb.append(sessionList.toXML(indent));
        // entity
        sb.append(entityList.toXML(indent));
        // message-driven
        sb.append(messageDrivenList.toXML(indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</enterprise-beans>\n");

        return sb.toString();
    }
}
