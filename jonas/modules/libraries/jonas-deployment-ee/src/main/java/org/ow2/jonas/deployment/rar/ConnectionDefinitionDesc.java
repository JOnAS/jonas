/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A. 
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar;

import java.io.Serializable;
import java.util.List;

import org.ow2.jonas.deployment.rar.xml.ConnectionDefinition;


/** 
 * This class defines the implementation of the element connection-definition
 * 
 * @author Eric Hardesty
 */

public class ConnectionDefinitionDesc implements Serializable {

    /**
     * id
     */ 
    private String id = null;

    /**
     * managedconnectionfactory-class
     */ 
    private String managedconnectionfactoryClass = null;

    /**
     * config-property
     */ 
    private List configPropertyList = null;

    /**
     * connectionfactory-interface
     */ 
    private String connectionfactoryInterface = null;

    /**
     * connectionfactory-impl-class
     */ 
    private String connectionfactoryImplClass = null;

    /**
     * connection-interface
     */ 
    private String connectionInterface = null;

    /**
     * connection-impl-class
     */ 
    private String connectionImplClass = null;


    /**
     * Constructor
     */
    public ConnectionDefinitionDesc(ConnectionDefinition cd) {
        if (cd != null) {
            id = cd.getId();
            managedconnectionfactoryClass = cd.getManagedconnectionfactoryClass();
            configPropertyList = Utility.configProperty(cd.getConfigPropertyList());
            connectionfactoryInterface = cd.getConnectionfactoryInterface();
            connectionfactoryImplClass = cd.getConnectionfactoryImplClass();
            connectionInterface = cd.getConnectionInterface();
            connectionImplClass = cd.getConnectionImplClass();
        }
    }

    /** 
     * Gets the id
     * @return the id
     */
    public String getId() {
        return id;
    }

    /** 
     * Gets the managedconnectionfactory-class
     * @return the managedconnectionfactory-class
     */
    public String getManagedconnectionfactoryClass() {
        return managedconnectionfactoryClass;
    }

    /** 
     * Gets the config-property
     * @return the config-property
     */
    public List getConfigPropertyList() {
        return configPropertyList;
    }

    /** 
     * Gets the connectionfactory-interface
     * @return the connectionfactory-interface
     */
    public String getConnectionfactoryInterface() {
        return connectionfactoryInterface;
    }

    /** 
     * Gets the connectionfactory-impl-class
     * @return the connectionfactory-impl-class
     */
    public String getConnectionfactoryImplClass() {
        return connectionfactoryImplClass;
    }

    /** 
     * Gets the connection-interface
     * @return the connection-interface
     */
    public String getConnectionInterface() {
        return connectionInterface;
    }

    /** 
     * Gets the connection-impl-class
     * @return the connection-impl-class
     */
    public String getConnectionImplClass() {
        return connectionImplClass;
    }

}
