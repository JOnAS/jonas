/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Helene Joanin
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.ear;


import org.ow2.jonas.deployment.common.CommonsDTDs;
import org.ow2.jonas.deployment.common.util.ResourceHelper;

/**
 * This class defines the declarations of DTDs for application.xml.
 * @author Helene Joanin
 */
public class EarDTDs extends CommonsDTDs {

    /**
     * Package name.
     */
    private static final String PACKAGE = ResourceHelper.getResourcePackage(EarDTDs.class);

    /**
     * List of application dtds.
     */
    private static final String[] EAR_DTDS = new String[] {
        PACKAGE + "application_1_2.dtd",
        PACKAGE + "application_1_3.dtd"
    };

    /**
     * List of application publicId.
     */
    private static final String[] EAR_DTDS_PUBLIC_ID = new String[] {
        "-//Sun Microsystems, Inc.//DTD J2EE Application 1.2//EN",
        "-//Sun Microsystems, Inc.//DTD J2EE Application 1.3//EN"
    };

    /**
     * Build a new object for appliaction.xml DTDs handling.
     */
    public EarDTDs() {
        super();
        addMapping(EAR_DTDS, EAR_DTDS_PUBLIC_ID, EarDTDs.class.getClassLoader());
    }

}
