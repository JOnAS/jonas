/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ws.rules;

import org.ow2.jonas.deployment.common.rules.JRuleSetBase;

import org.apache.commons.digester.Digester;


/**
 * @author Guillaume Sauthier
 */
public class JonasWebserviceDescriptionRuleSet extends JRuleSetBase {

    /**
     * Construct an object with a specific prefix
     * @param prefix prefix to use during the parsing
     */
    public JonasWebserviceDescriptionRuleSet(String prefix) {
        super(prefix);
    }

    /**
     * Add a set of rules to the digester object
     * @param digester Digester instance
     */
    public void addRuleInstances(Digester digester) {
        digester.addObjectCreate(prefix + "jonas-webservice-description", "org.ow2.jonas.deployment.ws.xml.JonasWebserviceDescription");
        digester.addSetNext(prefix + "jonas-webservice-description", "addJonasWebserviceDescription",
                "org.ow2.jonas.deployment.ws.xml.JonasWebserviceDescription");
        digester.addCallMethod(prefix + "jonas-webservice-description/webservice-description-name", "setWebserviceDescriptionName", 0);
        digester.addCallMethod(prefix + "jonas-webservice-description/default-endpoint-uri", "setDefaultEndpointURI", 0);
        digester.addCallMethod(prefix + "jonas-webservice-description/wsdl-publish-directory", "setWsdlPublishDirectory", 0);

        // jonas-port-component
        digester.addRuleSet(new JonasPortComponentRuleSet(prefix + "jonas-webservice-description/"));
    }

}