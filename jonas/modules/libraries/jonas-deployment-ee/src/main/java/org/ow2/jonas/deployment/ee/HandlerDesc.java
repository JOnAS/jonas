/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial Developer : Delplanque Xavier & Sauthier Guillaume
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
*/

package org.ow2.jonas.deployment.ee;

import java.util.List;
import java.util.Properties;
import java.util.Vector;

import javax.xml.namespace.QName;

import org.ow2.jonas.deployment.common.DeploymentDescException;
import org.ow2.jonas.deployment.common.xml.Handler;
import org.ow2.jonas.deployment.common.xml.InitParam;



/**
 * The Handler class describe both
 * - a Handler reference to use on the client side of a Web Service,
 * - and a Handler description.
 * The difference is that the port-names attribute is present for a handler reference,
 * and the port-names attribute does not exist for a handler description.
 *
 * @author Guillaume Sauthier
 * @author Xavier Delplanque
 * @author Helene Joanin
 */
public class HandlerDesc {

    /** The name of the handler (must be unique) */
    private String name;

    /** The classname of the Handler */
    private String className;

    /** The handler class */
    private Class clazz;

    /** Params needed to initialize the handler */
    private Properties params = new Properties();

    /** The list of SOAP Headers the handler will access */
    private List headers = new Vector();

    /** The list of SOAP actor the handler will play as a role */
    private List roles = new Vector();

    /** List of port names the handler is associated with */
    private List portNames = new Vector();

    /**
     * Creates a new HandlerRef object.
     *
     * @param classLoader ejbjar classLoader
     * @param handler contains informations defined in web deployment
     *        descriptor (service-ref)
     *
     * @throws DeploymentDescException When Construction fails.
     */
    public HandlerDesc(ClassLoader classLoader, Handler handler)
        throws DeploymentDescException {

        name = handler.getHandlerName();

        className = handler.getHandlerClass();

        try {
            clazz = classLoader.loadClass(className);
        } catch (ClassNotFoundException e) {
            throw new DeploymentDescException("handler class not found", e);
        }

        // fill init params table
        List iparams = handler.getInitParamList();
        try {
            for (int i = 0; i < iparams.size(); i++) {
                // add in params table each init parameter name and associated value
                InitParam p = (InitParam) iparams.get(i);

                if (p != null) {
                    params.put(p.getParamName(), p.getParamValue());
                }
            }
        } catch (NullPointerException e) {
            throw new DeploymentDescException("parameter name missing", e);
        }

        // fill headers a list containing soap header QNames
        List shl = handler.getSoapHeaderList();
        for (int i = 0; i < shl.size(); i++) {
            // build qnames and add it in the table
            org.ow2.jonas.deployment.common.xml.Qname sh = (org.ow2.jonas.deployment.common.xml.Qname) shl.get(i);

            if (sh != null) {
                QName qn = sh.getQName();
                headers.add(qn);
            }
        }

        // fill roles a list containing soap role names
        List srl = handler.getSoapRoleList();
        for (int i = 0; i < srl.size(); i++) {
            String role = (String) srl.get(i);
            if (role != null) {
                roles.add(role);
            }
        }

        // fill portNames a list containing ports names
        List pnl = handler.getPortNameList();
        for (int i = 0; i < pnl.size(); i++) {
            String pn = (String) pnl.get(i);
            if (pn != null) {
                portNames.add(pn);
            }
        }
    }

    /**
     * Return the name of the Handler.
     *
     * @return the name of the Handler.
     */
    public String getName() {
        return name;
    }


    /**
     * Return the name of class of the Handler.
     *
     * @return the name of class of the Handler.
     */
    public String getHandlerClassName() {
        return className;
    }

    /**
     * Return the Handler implementation class.
     *
     * @return the Handler class
     */
    public Class getHandlerClass() {
        return clazz;
    }

    /**
     * Return all the init-params of the Handler.
     *
     * @return the init-params of the Handler
     */
    public Properties getInitParams() {
        return params;
    }

    /**
     * Return the value of an init-param.
     *
     * @param pname The key of init-param map.
     *
     * @return the value of an init-param
     */
    public String getInitParam(String pname) {
        return params.getProperty(pname);
    }

    /**
     * Return the list of Headers the Handlers will access.
     *
     * @return the list of Headers the Handlers will access.
     */
    public List getSOAPHeaders() {
        return headers;
    }

    /**
     * Return the list of SOAP Actor Definitions the Handler will play as a
     * role.
     *
     * @return the list of Role the Handler will play
     */
    public List getSOAPRoles() {
        return roles;
    }

    /**
     * Return the list of port name the Handler is associated with. The names
     * match the localPart of the Port QName.
     *
     * @return the list of port name the Handler is associated with.
     */
    public List getPortNames() {
        return portNames;
    }

    /**
     * Test Equality between 2 Objects.
     *
     * @param other The object to compare.
     *
     * @return true if the objects are equals in value, else false.
     */
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (!(other instanceof HandlerDesc)) {
            return false;
        }
        HandlerDesc ref = (HandlerDesc) other;
        if (!name.equals(ref.getName())) {
            return false;
        }
        if (!clazz.getName().equals(ref.getHandlerClass().getName())) {
            return false;
        }
        if (!params.equals(ref.getInitParams())) {
            return false;
        }
        if (!headers.equals(ref.getSOAPHeaders())) {
            return false;
        }
        if (!roles.equals(ref.getSOAPRoles())) {
            return false;
        }
        if (!portNames.equals(ref.getPortNames())) {
            return false;
        }
        // After all theses tests, the 2 objects are equals in value
        return true;
    }

    /**
     * @return Returns a String representation of a HandlerDesc
     */
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("\n" + getClass().getName());
        sb.append("\ngetName()=" + getName());
        sb.append("\ngetClassname()=" + getHandlerClassName());
        sb.append("\ngetSOAPRoles()=" + getSOAPRoles());
        sb.append("\ngetSOAPHeaders()=" + getSOAPHeaders());
        sb.append("\ngetInitParams()=" + getInitParams());
        sb.append("\ngetPortNames()=" + getPortNames());
        return sb.toString();
    }

}
