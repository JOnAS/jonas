/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.ear;

import org.ow2.jonas.deployment.common.CommonsSchemas;
import org.ow2.jonas.deployment.common.util.ResourceHelper;

/**
 * This class defines the declarations of Schemas for application.xml.
 * @author Helene Joanin
 */
public class EarSchemas extends CommonsSchemas {

    /**
     * Package name.
     */
    private static final String PACKAGE = ResourceHelper.getResourcePackage(EarSchemas.class);

    /**
     * List of schemas used for application.xml.
     */
    private static final String[] APP_SCHEMAS = new String[] {
        PACKAGE + "application_1_4.xsd",
        PACKAGE + "application_5.xsd"
    };


    /**
     * Build a new object for Schemas handling.
     */
    public EarSchemas() {
        super();
        addSchemas(APP_SCHEMAS, EarSchemas.class.getClassLoader());
    }

    /**
     * @return Returns the last Schema.
     */
    public static String getLastSchema() {
        return getLastSchema(APP_SCHEMAS, PACKAGE);
    }

}
