/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.clusterd;

import org.ow2.jonas.deployment.clusterd.xml.ClusterDaemon;
import org.ow2.jonas.deployment.common.AbsDeploymentDesc;

/**
 * This class provides a cluster daemon configuration structure as described by
 * the description file. It extends the AbsDeploymentDesc which requires to
 * implement toString() and provides getSAXMsg() method (displayName is not used
 * by ClusterDaemonConfiguration).
 * @author Benoit Pelletier
 */

public class ClusterDaemonConfiguration extends AbsDeploymentDesc {

    /**
     * The cluster daemon configuraton
     */
    private ClusterDaemon clusterDaemon = null;

    /**
     * Constructor
     * @param clusterDaemon Cluster daemon
     */
    public ClusterDaemonConfiguration(final ClusterDaemon clusterDaemon) {

        this.clusterDaemon = clusterDaemon;

    }

    /**
     * Return a String representation of the ClusterDaemonConfiguration
     * @return a String representation of the ClusterDaemonConfiguration
     */
    @Override
    public String toString() {

        return toXML();
    }

    /**
     * @return Return a XML representation of the clusterDaemonConfiguration
     */
    public String toXML() {

        StringBuffer sb = new StringBuffer();
        sb.append(clusterDaemon.toXML());
        return sb.toString();
    }

    /**
     * @return the cluster daemon bean
     */
    public ClusterDaemon getClusterDaemon() {
        return clusterDaemon;
    }

    /**
     * Set the cluster daemon bean
     * @param clusterDaemon cluster daemon
     */
    public void setClusterDaemon(final ClusterDaemon clusterDaemon) {
        this.clusterDaemon = clusterDaemon;
    }
}
