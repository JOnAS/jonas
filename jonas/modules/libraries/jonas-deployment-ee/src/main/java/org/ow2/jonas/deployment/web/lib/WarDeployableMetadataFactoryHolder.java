/**
 * EasyBeans
 * Copyright (C) 2008 Bull S.A.S.
 * Contact: easybeans@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.web.lib;

import org.ow2.util.ee.metadata.war.api.IWarDeployableMetadataFactory;
import org.ow2.util.ee.metadata.war.impl.WarDeployableMetadataFactory;

/**
 * This class is a temporary hack.
 * @author Gael Lalire
 */
public final class WarDeployableMetadataFactoryHolder {

    /**
     * Utility class.
     */
    private WarDeployableMetadataFactoryHolder() {
    }

    /**
     * War deployable metadata factory service.
     */
    private static IWarDeployableMetadataFactory warDeployableMetadataFactory = null;

    /**
     * @return the deployable metadata factory service
     */
    public static IWarDeployableMetadataFactory getWarDeployableMetadataFactory() {
        if (warDeployableMetadataFactory == null) {
            try {
                warDeployableMetadataFactory = new WarDeployableMetadataFactory();
            } catch (Exception e) {
                throw new IllegalStateException("Unable to get a factory", e);
            }
        }
        return warDeployableMetadataFactory;
    }

    /**
     * @param warDeployableMetadataFactory the deployable metadata factory service
     */
    public static void setWarDeployableMetadataFactory(final IWarDeployableMetadataFactory warDeployableMetadataFactory) {
        WarDeployableMetadataFactoryHolder.warDeployableMetadataFactory = warDeployableMetadataFactory;
    }

}
