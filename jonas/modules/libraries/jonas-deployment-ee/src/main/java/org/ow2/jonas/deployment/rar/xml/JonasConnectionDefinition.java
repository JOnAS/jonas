/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;
import org.ow2.jonas.deployment.common.xml.TopLevelElement;

/**
 * This class defines the implementation of the element jonas-connection-definition
 *
 * @author Eric Hardesty
 */

public class JonasConnectionDefinition extends AbsElement implements TopLevelElement {

    /**
     * id
     */
    private String id = null;

    /**
     * description
     */ 
    private JLinkedList descriptionList = null;

    /**
     * jndi-name
     */
    private String jndiName = null;

    /**
     * log-enabled
     */
    private String logEnabled = null;

    /**
     * log-topic
     */
    private String logTopic = null;

    /**
     * pool-params
     */
    private PoolParams poolParams = null;

    /**
     * jdbc-conn-params
     */
    private JdbcConnParams jdbcConnParams = null;

    /**
     * jonas-config-property
     */
    private JLinkedList jonasConfigPropertyList = null;


    /**
     * Constructor
     */
    public JonasConnectionDefinition() {
        super();
        descriptionList = new  JLinkedList("description");
        jonasConfigPropertyList = new  JLinkedList("jonas-config-property");
    }

    /**
     * Gets the id
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Set the id
     * @param id id
     */
    public void setId(String id) {
        this.id = id;
    }

    /** 
     * Gets the description
     * @return the description
     */
    public JLinkedList getDescriptionList() {
        return descriptionList;
    }

    /** 
     * Set the description
     * @param descriptionList description
     */
    public void setDescriptionList(JLinkedList descriptionList) {
        this.descriptionList = descriptionList;
    }

    /** 
     * Add a new  description element to this object
     * @param description the description String
     */
    public void addDescription(String description) {
        descriptionList.add(description);
    }

    /**
     * Gets the jndi-name
     * @return the jndi-name
     */
    public String getJndiName() {
        return jndiName;
    }

    /**
     * Set the jndiname
     * @param jndiName jndiname
     */
    public void setJndiName(String jndiName) {
        this.jndiName = jndiName;
    }

    /**
     * Gets the log-enabled
     * @return the log-enabled
     */
    public String getLogEnabled() {
        return logEnabled;
    }

    /**
     * Set the log-enabled
     * @param logEnabled logEnabled
     */
    public void setLogEnabled(String logEnabled) {
        this.logEnabled = logEnabled;
    }

    /**
     * Gets the log-topic
     * @return the log-topic
     */
    public String getLogTopic() {
        return logTopic;
    }

    /**
     * Set the log-topic
     * @param logTopic logTopic
     */
    public void setLogTopic(String logTopic) {
        this.logTopic = logTopic;
    }

    /**
     * Gets the pool-params
     * @return the pool-params
     */
    public PoolParams getPoolParams() {
        return poolParams;
    }

    /**
     * Set the pool-params
     * @param poolParams poolParams
     */
    public void setPoolParams(PoolParams poolParams) {
        this.poolParams = poolParams;
    }

    /**
     * Gets the jdbc-conn-params
     * @return the jdbc-conn-params
     */
    public JdbcConnParams getJdbcConnParams() {
        return jdbcConnParams;
    }

    /**
     * Set the jdbc-conn-params
     * @param jdbcConnParams jdbcConnParams
     */
    public void setJdbcConnParams(JdbcConnParams jdbcConnParams) {
        this.jdbcConnParams = jdbcConnParams;
    }

    /**
     * Gets the jonas-config-property
     * @return the jonas-config-property
     */
    public JLinkedList getJonasConfigPropertyList() {
        return jonasConfigPropertyList;
    }

    /**
     * Set the jonas-config-property
     * @param jonasConfigPropertyList jonasConfigProperty
     */
    public void setJonasConfigPropertyList(JLinkedList jonasConfigPropertyList) {
        this.jonasConfigPropertyList = jonasConfigPropertyList;
    }

    /**
     * Add a new  jonas-config-property element to this object
     * @param jonasConfigProperty the jonasConfigPropertyobject
     */
    public void addJonasConfigProperty(JonasConfigProperty jonasConfigProperty) {
        jonasConfigPropertyList.add(jonasConfigProperty);
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prefixing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<jonas-connection-definition>\n");

        indent += 2;

        // id
        sb.append(xmlElement(id, "id", indent));
        // description
        sb.append(descriptionList.toXML(indent));
        // jndiname
        sb.append(xmlElement(jndiName, "jndi-name", indent));
        // log-enabled
        sb.append(xmlElement(logEnabled, "log-enabled", indent));
        // log-topic
        sb.append(xmlElement(logTopic, "log-topic", indent));
        // pool-params
        if (poolParams != null) {
            sb.append(poolParams.toXML(indent));
        }
        // jdbc-conn-params
        if (jdbcConnParams != null) {
            sb.append(jdbcConnParams.toXML(indent));
        }
        // jonas-config-property
        if (jonasConfigPropertyList != null) {
            sb.append(jonasConfigPropertyList.toXML(indent));
        }
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</jonas-connection-definition>\n");

        return sb.toString();
    }
}
