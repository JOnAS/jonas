/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A. 
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;

/** 
 * This class defines the implementation of the element jdbc-conn-params
 * 
 * @author Florent Benoit
 */

public class JdbcConnParams extends AbsElement  {

    /**
     * jdbc-check-level
     */ 
    private String jdbcCheckLevel = null;

    /**
     * jdbc-test-statement
     */ 
    private String jdbcTestStatement = null;


    /**
     * Constructor
     */
    public JdbcConnParams() {
        super();
    }

    /** 
     * Gets the jdbc-check-level
     * @return the jdbc-check-level
     */
    public String getJdbcCheckLevel() {
        return jdbcCheckLevel;
    }

    /** 
     * Set the jdbc-check-level
     * @param jdbcCheckLevel jdbcCheckLevel
     */
    public void setJdbcCheckLevel(String jdbcCheckLevel) {
        this.jdbcCheckLevel = jdbcCheckLevel;
    }

    /** 
     * Gets the jdbc-test-statement
     * @return the jdbc-test-statement
     */
    public String getJdbcTestStatement() {
        return jdbcTestStatement;
    }

    /** 
     * Set the jdbc-test-statement
     * @param jdbcTestStatement jdbcTestStatement
     */
    public void setJdbcTestStatement(String jdbcTestStatement) {
        this.jdbcTestStatement = jdbcTestStatement;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prefixing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<jdbc-conn-params>\n");

        indent += 2;

        // jdbc-check-level
        sb.append(xmlElement(jdbcCheckLevel, "jdbc-check-level", indent));
        // jdbc-test-statement
        sb.append(xmlElement(jdbcTestStatement, "jdbc-test-statement", indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</jdbc-conn-params>\n");

        return sb.toString();
    }
}
