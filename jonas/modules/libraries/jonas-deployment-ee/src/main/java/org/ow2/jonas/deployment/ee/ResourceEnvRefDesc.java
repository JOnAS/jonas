/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Christophe Ney
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.ee;

import org.ow2.jonas.deployment.api.IResourceEnvRefDesc;
import org.ow2.jonas.deployment.common.DeploymentDescException;
import org.ow2.jonas.deployment.common.xml.JonasResourceEnv;
import org.ow2.jonas.deployment.common.xml.ResourceEnvRef;


/**
 * This class represents the description of a ResourceEnvRef object
 * @author Christophe Ney
 * @author Florent Benoit
 */
public class ResourceEnvRefDesc implements IResourceEnvRefDesc {

    /**
     * The resource env ref name.
     */
    private String name;

    /**
     * The resource env ref type.
     */
    private Class type;

    /**
     * The resource env ref jndi name.
     */
    private String jndiName;

    /**
     * Construct a descriptor for the resource-env-ref and jonas-resource-env
     * tags.
     * @param classLoader the classloader for the classes.
     * @param res the resource-env-ref resulting of the web.xml parsing.
     * @param jRes the jonas-resource-env resulting of the jonas-web.xml
     * parsing.
     * @throws DeploymentDescException when missing information for
     * creating the ResourceEnvRefDesc.
     */
    public ResourceEnvRefDesc(ClassLoader classLoader, ResourceEnvRef res,
                              JonasResourceEnv jRes) throws DeploymentDescException {

        name = res.getResourceEnvRefName();
        try {
            type = classLoader.loadClass(res.getResourceEnvRefType());
        } catch (ClassNotFoundException e) {
            throw new DeploymentDescException("resource-env-ref-type class not found for resource-env-ref " + name, e);
        }
        jndiName = jRes.getJndiName();
    }

    public String getName() {
        return name;
    }

    public Class getType() {
        return type;
    }

    public String getJndiName() {
        return jndiName;
    }

    /**
     * String representation of the object for test purpose.
     * @return String representation of this object.
     */
    public String toString() {
        StringBuffer ret = new StringBuffer();
        ret.append("\ngetName()=" + getName());
        ret.append("\ngetType()=" + getType());
        ret.append("\ngetJndiName()=" + getJndiName());
        return ret.toString();
    }

}
