/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.ws.xml;

import org.ow2.jonas.deployment.common.xml.AbsDescriptionElement;
import org.ow2.jonas.deployment.common.xml.DescriptionGroupXml;
import org.ow2.jonas.deployment.common.xml.JLinkedList;
import org.ow2.jonas.deployment.common.xml.TopLevelElement;

/**
 * This class defines the implementation of the element webservices
 *
 * @author JOnAS team
 */

public class Webservices extends AbsDescriptionElement
    implements TopLevelElement, DescriptionGroupXml {

    /**
     * webservice-description
     */
    private JLinkedList webserviceDescriptionList = null;

    /**
     * Webservices.xml version (1.1/1.2/...).
     */
    private String version = null;

    /**
     * Constructor
     */
    public Webservices() {
        super();
        webserviceDescriptionList = new  JLinkedList("webservice-description");
    }

    /**
     * Gets the webservice-description
     * @return the webservice-description
     */
    public JLinkedList getWebserviceDescriptionList() {
        return webserviceDescriptionList;
    }

    /**
     * Set the webservice-description
     * @param webserviceDescriptionList webserviceDescription
     */
    public void setWebserviceDescriptionList(JLinkedList webserviceDescriptionList) {
        this.webserviceDescriptionList = webserviceDescriptionList;
    }

    /**
     * Add a new  webservice-description element to this object
     * @param webserviceDescription the webserviceDescriptionobject
     */
    public void addWebserviceDescription(WebserviceDescription webserviceDescription) {
        webserviceDescriptionList.add(webserviceDescription);
    }

    /**
     * @return the webservices.xml file version.
     */
    public String getVersion() {
        return version;
    }

    /**
     * Set the version.
     * @param version version String value
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<webservices version=\"" + version + "\">\n");
        indent += 2;

        // description
        sb.append(xmlElement(getDescription(), "description", indent));
        // display-name
        sb.append(xmlElement(getDisplayName(), "display-name", indent));
        // icon
        sb.append(getIcon().toXML(indent));
        // webservice-description
        sb.append(webserviceDescriptionList.toXML(indent));

        indent -= 2;
        sb.append(indent(indent));
        sb.append("</webservices>\n");

        return sb.toString();
    }
}
