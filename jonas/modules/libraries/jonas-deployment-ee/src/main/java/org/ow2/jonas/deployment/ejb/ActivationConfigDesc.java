/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.ejb;

import java.util.LinkedList;
import java.util.List;

import org.ow2.jonas.deployment.common.xml.JLinkedList;
import org.ow2.jonas.deployment.ejb.xml.ActivationConfig;
import org.ow2.jonas.deployment.ejb.xml.ActivationConfigProperty;




/**
 * This class defines the implementation of the element activation-config
 *
 * @author JOnAS team
 */

public class ActivationConfigDesc {

    /**
     * description
     */
    private String description = null;


    /**
     * activation-config-property
     */
    private List activationConfigPropertyList = null;

    /**
     * Constructor
     */
    public ActivationConfigDesc(ActivationConfig ac) {
        if (ac != null) {
            description = ac.getDescription();
            activationConfigPropertyList = convert(ac.getActivationConfigPropertyList());
        } else {
            activationConfigPropertyList = new LinkedList();
        }
    }

    /**
     * Gets the description
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets the list of activation-config-property
     * @return the list of activation-config-property
     */
    public List getActivationConfigPropertyList() {
        return activationConfigPropertyList;
    }

    private List convert(JLinkedList acpl) {
        ActivationConfigProperty acp = null;
        LinkedList ll = new LinkedList();
        for (int i = 0; i < acpl.size(); i++) {
            acp = (ActivationConfigProperty) acpl.get(i);
            ll.add(new ActivationConfigPropertyDesc(acp));
        }
        return ll;
    }
}
