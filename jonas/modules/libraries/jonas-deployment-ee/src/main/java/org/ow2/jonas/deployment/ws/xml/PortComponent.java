/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.ws.xml;

import org.ow2.jonas.deployment.common.xml.AbsDescriptionElement;
import org.ow2.jonas.deployment.common.xml.DescriptionGroupXml;
import org.ow2.jonas.deployment.common.xml.Handler;
import org.ow2.jonas.deployment.common.xml.JLinkedList;
import org.ow2.jonas.deployment.common.xml.Qname;

/**
 * This class defines the implementation of the element port-component
 * (use here the common handler object defined in jonas-lib,
 *  even if there is no "port-name" in our case)
 *
 * @author JOnAS team
 */

public class PortComponent extends AbsDescriptionElement implements DescriptionGroupXml {

    /**
     * port-component-name
     */
    private String portComponentName = null;

    /**
     * wsdl-port
     */
    private Qname wsdlPort = null;

    /**
     * service-endpoint-interface
     */
    private String serviceEndpointInterface = null;

    /**
     * service-impl-bean
     */
    private ServiceImplBean serviceImplBean = null;

    /**
     * handler
     * (use here the common handler object defined in jonas-lib,
     *  even if there is no "port-name" in our case)
     */
    private JLinkedList handlerList = null;


    /**
     * Constructor
     */
    public PortComponent() {
        super();
        handlerList = new JLinkedList("handler");
    }

    /**
     * Gets the port-component-name
     * @return the port-component-name
     */
    public String getPortComponentName() {
        return portComponentName;
    }

    /**
     * Set the port-component-name
     * @param portComponentName portComponentName
     */
    public void setPortComponentName(String portComponentName) {
        this.portComponentName = portComponentName;
    }

    /**
     * Gets the wsdl-port
     * @return the wsdl-port
     */
    public Qname getWsdlPort() {
        return wsdlPort;
    }

    /**
     * Set the wsdl-port
     * @param wsdlPort wsdlPort
     */
    public void setWsdlPort(Qname wsdlPort) {
        this.wsdlPort = wsdlPort;
    }

    /**
     * Gets the service-endpoint-interface
     * @return the service-endpoint-interface
     */
    public String getServiceEndpointInterface() {
        return serviceEndpointInterface;
    }

    /**
     * Set the service-endpoint-interface
     * @param serviceEndpointInterface serviceEndpointInterface
     */
    public void setServiceEndpointInterface(String serviceEndpointInterface) {
        this.serviceEndpointInterface = serviceEndpointInterface;
    }

    /**
     * Gets the service-impl-bean
     * @return the service-impl-bean
     */
    public ServiceImplBean getServiceImplBean() {
        return serviceImplBean;
    }

    /**
     * Set the service-impl-bean
     * @param serviceImplBean serviceImplBean
     */
    public void setServiceImplBean(ServiceImplBean serviceImplBean) {
        this.serviceImplBean = serviceImplBean;
    }

    /**
     * Gets the handler
     * @return the handler
     */
    public JLinkedList getHandlerList() {
        return handlerList;
    }

    /**
     * Set the handler
     * @param handlerList handler
     */
    public void setHandlerList(JLinkedList handlerList) {
        this.handlerList = handlerList;
    }

    /**
     * Add a new  handler element to this object
     * @param handler the handlerobject
     */
    public void addHandler(Handler handler) {
        handlerList.add(handler);
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<port-component>\n");

        indent += 2;

        // description
        sb.append(xmlElement(getDescription(), "description", indent));
        // display-name
        sb.append(xmlElement(getDisplayName(), "display-name", indent));
        // icon
        sb.append(getIcon().toXML(indent));
        // port-component-name
        sb.append(xmlElement(portComponentName, "port-component-name", indent));
        // wsdl-port
        if (wsdlPort != null) {
            sb.append(wsdlPort.toXML(indent));
        }
        // service-endpoint-interface
        sb.append(xmlElement(serviceEndpointInterface, "service-endpoint-interface", indent));
        // service-impl-bean
        if (serviceImplBean != null) {
            sb.append(serviceImplBean.toXML(indent));
        }
        // handler
        sb.append(handlerList.toXML(indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</port-component>\n");

        return sb.toString();
    }
}
