/* Generated By:JJTree: Do not edit this line. ASTFromClause.java */

package org.ow2.jonas.deployment.ejb.ejbql;

public class ASTFromClause extends SimpleNode {
  public ASTFromClause(int id) {
    super(id);
  }

  public ASTFromClause(EJBQL p, int id) {
    super(p, id);
  }


  /** Accept the visitor. **/
  public Object jjtAccept(EJBQLVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
}
