/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.rules;

import org.ow2.jonas.deployment.common.rules.JRuleSetBase;

import org.apache.commons.digester.Digester;

/**
 * This class defines the rules to analyze the element jdbc-mapping
 *
 * @author JOnAS team
 */

public class JdbcMappingRuleSet extends JRuleSetBase {

    /**
     * Construct an object with a specific prefix
     * @param prefix prefix to use during the parsing
     */
    public JdbcMappingRuleSet(String prefix) {
        super(prefix);
   }
     /**
     * Add a set of rules to the digester object
     * @param digester Digester instance
     */

    public void addRuleInstances(Digester digester) {
        digester.addObjectCreate(prefix + "jdbc-mapping",
                                 "org.ow2.jonas.deployment.ejb.xml.JdbcMapping");
        digester.addSetNext(prefix + "jdbc-mapping",
                            "setJdbcMapping",
                            "org.ow2.jonas.deployment.ejb.xml.JdbcMapping");
        digester.addCallMethod(prefix + "jdbc-mapping/jndi-name",
                               "setJndiName", 0);
        digester.addCallMethod(prefix + "jdbc-mapping/jdbc-table-name",
                               "setJdbcTableName", 0);
        digester.addCallMethod(prefix + "jdbc-mapping/automatic-pk",
                               "setAutomaticPk", 0);
        digester.addCallMethod(prefix + "jdbc-mapping/automatic-pk-field-name",
                               "setJdbcAutomaticPkFieldName", 0);
        digester.addRuleSet(new CmpFieldJdbcMappingRuleSet(prefix + "jdbc-mapping/"));
        digester.addRuleSet(new FinderMethodJdbcMappingRuleSet(prefix + "jdbc-mapping/"));
   }
}
