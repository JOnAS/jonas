/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.rules;

import org.apache.commons.digester.Digester;
import org.ow2.jonas.deployment.common.rules.JRuleSetBase;

/**
 * Rule set for cluster configuration
 * @author eyindanga
 *
 */
public class ClusterRuleSet extends JRuleSetBase {

    /**Constructor.
     * @param prefix Rule set prefix
     */
    public ClusterRuleSet(final String prefix) {
        super(prefix);
    }

    @Override
    public void addRuleInstances(final Digester digester) {
        digester.addObjectCreate(prefix + "cluster-config",
        "org.ow2.cmi.info.mapping.Cluster");
        digester.addSetNext(prefix + "cluster-config",
                "setCluster",
        "org.ow2.cmi.info.mapping.Cluster");
        digester.addCallMethod(prefix + "cluster-config/name", "setClusterName", 0);
        digester.addCallMethod(prefix + "cluster-config/policy", "setPolicyType", 0);
        digester.addCallMethod(prefix + "cluster-config/strategy", "setStrategyType", 0);
        /**
         * Properties rule set.
         */
        digester.addRuleSet(new ClusterPropertiesRuleSet(prefix + "cluster-config/"));

        /**
         * Pool rule set.
         */
        digester.addRuleSet(new PoolRuleSet(prefix + "cluster-config/"));

    }

}
