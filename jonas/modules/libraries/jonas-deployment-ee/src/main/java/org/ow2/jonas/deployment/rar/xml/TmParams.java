/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A. 
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;

/** 
 * This class defines the implementation of the element tm-params
 * 
 * @author Eric Hardesty
 */

public class TmParams extends AbsElement  {

    /**
     * tm-config-property
     */
    private JLinkedList tmConfigPropertyList = null;



    /**
     * Constructor
     */
    public TmParams() {
        super();
        tmConfigPropertyList = new  JLinkedList("tm-config-property");
    }

    /**
     * Gets the tm-config-property
     * @return the tm-config-property
     */
    public JLinkedList getTmConfigPropertyList() {
        return tmConfigPropertyList;
    }

    /**
     * Set the tm-config-property
     * @param tmConfigPropertyList tmConfigProperty
     */
    public void setTmConfigPropertyList(JLinkedList tmConfigPropertyList) {
        this.tmConfigPropertyList = tmConfigPropertyList;
    }

    /**
     * Add a new  tm-config-property element to this object
     * @param tmConfigProperty the tmConfigPropertyobject
     */
    public void addTmConfigProperty(TmConfigProperty tmConfigProperty) {
        tmConfigPropertyList.add(tmConfigProperty);
    }


    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prefixing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<tm-params>\n");

        indent += 2;

        // tm-config-property
        if (tmConfigPropertyList != null) {
            sb.append(tmConfigPropertyList.toXML(indent));
        }
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</tm-params>\n");

        return sb.toString();
    }
}
