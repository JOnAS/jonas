/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Philippe Coq
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.ejb.xml;


import org.ow2.jonas.deployment.common.xml.AbsJonasEnvironmentElement;

/**
 * This interface provides to the value of Xml elements common
 * for jonas-session/jonas-entity/jonas-mdb
 * @author Philippe Coq
 */

public abstract class JonasCommonEjb extends AbsJonasEnvironmentElement implements JonasCommonEjbXml {

    /**
     * ejb-name
     */
    private String ejbName = null;

    /**
     * jndi-name
     */
    private String jndiName = null;

    /**
     * jndi-local-name
     */
    private String jndiLocalName = null;

    /**
     * max-cache-size
     */
    private String maxCacheSize = null;

    /**
     * min-pool-size
     */
    private String minPoolSize = null;

    /**
     * principal name to use in case of run-as
     */
    private String runAsPrincipalName = null;

    /**
     * ior security config
     */
    private IorSecurityConfigMapping iorSecurityConfig = null;


    /**
     * cluster-replicated
     */
    private String clusterReplicated = null;


    /**
     * Cluster object.
     */
    private Object cluster;

    /**
     * @return ejb-name element
     */
    public String getEjbName() {
        return ejbName;
    }

    /**
     * Set the ejb-name
     * @param ejbName ejbName
     */
    public void setEjbName(final String ejbName) {
        this.ejbName = ejbName;
    }

    /**
     * Gets the jndi-name
     * @return the jndi-name
     */
    public String getJndiName() {
        return jndiName;
    }

    /**
     * Set the jndi-name
     * @param jndiName jndiName
     */
    public void setJndiName(final String jndiName) {
        this.jndiName = jndiName;
    }

    /**
     * Gets the jndi-local-name
     * @return the jndi-local-name
     */
    public String getJndiLocalName() {
        return jndiLocalName;
    }

    /**
     * Set the jndi-local-name
     * @param jndiLocalName jndi-local-name
     */
    public void setJndiLocalName(final String jndiLocalName) {
        this.jndiLocalName = jndiLocalName;
    }

    /**
     * Gets the max-cache-size
     * @return the max-cache-size
     */
    public String getMaxCacheSize() {
        return maxCacheSize;
    }

    /**
     * Set the max-cache-size
     * @param maxCacheSize maxCacheSize
     */
    public void setMaxCacheSize(final String maxCacheSize) {
        this.maxCacheSize = maxCacheSize;
    }

    /**
     * Gets the min-pool-size
     * @return the min-pool-size
     */
    public String getMinPoolSize() {
        return minPoolSize;
    }

    /**
     * Set the min-pool-size
     * @param minPoolSize minPoolSize
     */
    public void setMinPoolSize(final String minPoolSize) {
        this.minPoolSize = minPoolSize;
    }

    /**
     * @return the runAs Principal name.
     */
    public String getRunAsPrincipalName() {
        return runAsPrincipalName;
    }

    /**
     * @param runAsPrincipalName the principal-name to set for run-as
     */
    public void setRunAsPrincipalName(final String runAsPrincipalName) {
        this.runAsPrincipalName = runAsPrincipalName;
    }

    /**
     * @return Returns the iorSecurityConfig.
     */
    public IorSecurityConfigMapping getIorSecurityConfig() {
        return iorSecurityConfig;
    }
    /**
     * @param iorSecurityConfig The iorSecurityConfig to set.
     */
    public void setIorSecurityConfig(final IorSecurityConfigMapping iorSecurityConfig) {
        this.iorSecurityConfig = iorSecurityConfig;
    }


    /**
     * Gets the cluster-replicated
     * @return the cluster-replicated
     */
    public String getClusterReplicated() {
        return clusterReplicated;
    }

    /**
     * Set the cluster-replicated
     * @param clusterReplicated clusterReplicated
     */
    public void setClusterReplicated(final String clusterReplicated) {
        this.clusterReplicated = clusterReplicated;
    }

    /**
     * @return the cluster
     */
    public Object getCluster() {
        return this.cluster;
    }

    /**
     * @param cluster the cluster to set
     */
    public void setCluster(final Object cluster) {
        this.cluster = cluster;
    }

}
