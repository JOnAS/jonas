/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial Developer : Sauthier Guillaume
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ws;

import org.ow2.jonas.deployment.ws.xml.JonasPortComponent;
import org.ow2.jonas.deployment.ws.xml.PortComponent;


/**
 * Factory used to create appropriate PortComponentDesc.<br>
 * Choice is made in function of the port-component given as input parameter.<br>
 * If service-impl-bean has a child called servlet-link,
 * then a JaxRpcPortComponentDesc is created.<br>
 * If service-impl-bean has a child called ejb-link,
 * then a SSBPortComponentDesc is created.<br>
 */
public class PortComponentDescFactory {

    /**
     * Private empty constructor for utility class.
     */
    private PortComponentDescFactory() { }

    /**
     * Create a new PortComponentDesc instance.
     *
     * @param cl the ClassLoader used to load files (*.wsdl, mapping files, ...).
     * @param pc the PortComponent object representing port-component XML element.
     * @param jpc the JonasPortComponent object representing jonas-port-component XML element.
     * @param parent container ServiceDesc
     *
     * @return the created PortComponentDesc
     *
     * @throws WSDeploymentDescException when instanciation fails.
     */
    public static PortComponentDesc newInstance(ClassLoader cl,
                                                PortComponent pc,
                                                JonasPortComponent jpc,
                                                ServiceDesc parent)
        throws WSDeploymentDescException {

        // EJB Case
        if (pc.getServiceImplBean().getEjbLink() != null) {
            return new SSBPortComponentDesc(cl, pc, jpc, parent);
        } else { //WebApp case
            return new JaxRpcPortComponentDesc(cl, pc, jpc, parent);
        }
    }
}
