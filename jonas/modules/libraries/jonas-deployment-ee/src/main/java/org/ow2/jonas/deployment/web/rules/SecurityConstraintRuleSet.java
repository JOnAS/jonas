/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.web.rules;

import org.ow2.jonas.deployment.common.rules.JRuleSetBase;

import org.apache.commons.digester.Digester;


/**
 * This class defines a rule to analyze security-constraint in <webapp> element
 * @author Florent Benoit
 */
public class SecurityConstraintRuleSet extends JRuleSetBase {


    /**
     * Construct an object with a specific prefix
     * @param prefix prefix to use during the parsing
     */
    public SecurityConstraintRuleSet(String prefix) {
        super(prefix);
    }


    /**
     * Add a set of rules to the digester object
     * @param digester Digester instance
     */
    public void addRuleInstances(Digester digester) {
        digester.addObjectCreate(prefix + "security-constraint",
                                 "org.ow2.jonas.deployment.web.xml.SecurityConstraint");
        digester.addSetNext(prefix + "security-constraint",
                            "addSecurityConstraint",
                            "org.ow2.jonas.deployment.web.xml.SecurityConstraint");

        digester.addRuleSet(new WebResourceCollectionRuleSet(prefix + "security-constraint/"));
        digester.addRuleSet(new AuthConstraintRuleSet(prefix + "security-constraint/"));
        digester.addRuleSet(new UserDataConstraintRuleSet(prefix + "security-constraint/"));
    }
}

