/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A. 
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar;

import java.io.Serializable;
import java.util.List;

import org.ow2.jonas.deployment.rar.xml.AuthenticationMechanism;


/** 
 * This class defines the implementation of the element authentication-mechanism
 * 
 * @author Eric Hardesty
 */

public class AuthenticationMechanismDesc implements Serializable {

    /**
     * description
     */ 
    private List descriptionList = null;

    /**
     * authentication-mechanism-type
     */ 
    private String authenticationMechanismType = null;

    /**
     * credential-interface
     */ 
    private String credentialInterface = null;


    /**
     * Constructor
     */
    public AuthenticationMechanismDesc(AuthenticationMechanism am) {
        if (am != null) {
            descriptionList =  am.getDescriptionList();
            authenticationMechanismType = am.getAuthenticationMechanismType();
            credentialInterface = am.getCredentialInterface();
        }
    }

    /** 
     * Gets the description
     * @return the description
     */
    public List getDescriptionList() {
        return descriptionList;
    }

    /** 
     * Gets the authentication-mechanism-type
     * @return the authentication-mechanism-type
     */
    public String getAuthenticationMechanismType() {
        return authenticationMechanismType;
    }

    /** 
     * Gets the credential-interface
     * @return the credential-interface
     */
    public String getCredentialInterface() {
        return credentialInterface;
    }

}
