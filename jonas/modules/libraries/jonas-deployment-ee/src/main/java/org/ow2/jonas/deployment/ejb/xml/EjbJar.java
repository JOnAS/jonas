/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.xml;

import java.util.ArrayList;
import java.util.StringTokenizer;

import org.ow2.jonas.deployment.common.xml.AbsDescriptionElement;
import org.ow2.jonas.deployment.common.xml.DescriptionGroupXml;
import org.ow2.jonas.deployment.common.xml.TopLevelElement;


/**
 * This class defines the implementation of the element ejb-jar
 *
 * @author JOnAS team
 */

public class EjbJar extends AbsDescriptionElement
    implements TopLevelElement, DescriptionGroupXml {


    /**
     * Position of Version Number in Public ID Spec
     */
    private static final int VERSION_INDEX = 3;

    /**
     * enterprise-beans
     */
    private EnterpriseBeans enterpriseBeans = null;

    /**
     * relationships
     */
    private Relationships relationships = null;

    /**
     * assembly-descriptor
     */
    private AssemblyDescriptor assemblyDescriptor = null;

    /**
     * ejb-client-jar
     */
    private String ejbClientJar = null;

    /**
     * PublicId of the DTD we are processing
     */
    private String publicId = null;

    /**
     * Version of the EJB specification
     * is built from the publicId if DOCTYPE
     * or via the attribute version ins case of schema
     */
    private String version = null;

    /**
     * Constructor
     */
    public EjbJar() {
        super();
    }

    /**
     * Get the PublicId of the DTD used
     * @return the PublicId
     */
    public String getPublicId() {
        return publicId;
    }

    /**
     * Set the PublicId of the DTD used
     * @param pid the publicId
     */
    public void  setPublicId(String pid) {
        publicId = pid;
    }


    /**
     * Get the Version of the EJB specification
     * @return the Version
     */
    public String getVersion() {
        if (version != null) {
            return version;
        }
        if (publicId == null) {
            // No DOCTYPE  and No version attribute in ejb-jar element
            version = "2.1";
        } else {
            ArrayList al = new ArrayList();
            // Version must be set via the PublicId of the DOCTYPE
            StringTokenizer st = new StringTokenizer(publicId , "//");
            while (st.hasMoreTokens()) {
                al.add(st.nextToken().trim());
            }
            String spec = (String) al.get(2);
            al.clear();
            st = new StringTokenizer(spec , " ");
            while (st.hasMoreTokens()) {
                al.add(st.nextToken().trim());
            }
            version = (String) al.get(VERSION_INDEX);
        }
        return version;

    }

    /**
     * Set the Version  of the EJB specification
     *
     * @param ver the version
     */
    public void  setVersion(String ver) {
        version = ver;
    }

    /**
     * Gets the enterprise-beans
     * @return the enterprise-beans
     */
    public EnterpriseBeans getEnterpriseBeans() {
        return enterpriseBeans;
    }

    /**
     * Set the enterprise-beans
     * @param enterpriseBeans enterpriseBeans
     */
    public void setEnterpriseBeans(EnterpriseBeans enterpriseBeans) {
        this.enterpriseBeans = enterpriseBeans;
    }

    /**
     * Gets the relationships
     * @return the relationships
     */
    public Relationships getRelationships() {
        return relationships;
    }

    /**
     * Set the relationships
     * @param relationships relationships
     */
    public void setRelationships(Relationships relationships) {
        this.relationships = relationships;
    }

    /**
     * Gets the assembly-descriptor
     * @return the assembly-descriptor
     */
    public AssemblyDescriptor getAssemblyDescriptor() {
        return assemblyDescriptor;
    }

    /**
     * Set the assembly-descriptor
     * @param assemblyDescriptor assemblyDescriptor
     */
    public void setAssemblyDescriptor(AssemblyDescriptor assemblyDescriptor) {
        this.assemblyDescriptor = assemblyDescriptor;
    }

    /**
     * Gets the ejb-client-jar
     * @return the ejb-client-jar
     */
    public String getEjbClientJar() {
        return ejbClientJar;
    }

    /**
     * Set the ejb-client-jar
     * @param ejbClientJar ejbClientJar
     */
    public void setEjbClientJar(String ejbClientJar) {
        this.ejbClientJar = ejbClientJar;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<ejb-jar>\n");

        indent += 2;

        // description
        sb.append(xmlElement(getDescription(), "description", indent));
        // display-name
        sb.append(xmlElement(getDisplayName(), "display-name", indent));
        // small-icon
        sb.append(xmlElement(getIcon().getSmallIcon(), "small-icon", indent));
        // large-icon
        sb.append(xmlElement(getIcon().getLargeIcon(), "large-icon", indent));
        // enterprise-beans
        if (enterpriseBeans != null) {
            sb.append(enterpriseBeans.toXML(indent));
        }
        // relationships
        if (relationships != null) {
            sb.append(relationships.toXML(indent));
        }
        // assembly-descriptor
        if (assemblyDescriptor != null) {
            sb.append(assemblyDescriptor.toXML(indent));
        }
        // ejb-client-jar
        sb.append(xmlElement(ejbClientJar, "ejb-client-jar", indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</ejb-jar>\n");

        return sb.toString();
    }
}
