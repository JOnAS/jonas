/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Philippe Coq
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.rar;

import org.ow2.jonas.deployment.common.CommonsSchemas;
import org.ow2.jonas.deployment.common.util.ResourceHelper;

/**
 * This class defines the declarations of Schemas for and jonas-ra.xml.
 *
 * @author Philippe Coq
 */
public class JonasConnectorSchemas extends CommonsSchemas {

    /**
     * Package name.
     */
    private static final String PACKAGE = ResourceHelper.getResourcePackage(JonasConnectorSchemas.class);

    /**
     * List of schemas used for jonas-ra.xml.
     */
    private static final String[] JONAS_CONNECTOR_SCHEMAS = new String[] {
        PACKAGE + "jonas-connector_4_0.xsd",
        PACKAGE + "jonas-connector_4_1.xsd",
        PACKAGE + "jonas-connector_4_2.xsd",
        PACKAGE + "jonas-connector_4_4.xsd",
        PACKAGE + "jonas-connector_5_0.xsd",
        PACKAGE + "jonas-connector_5_3.xsd",
        PACKAGE + "jonas-connector_5_4.xsd"};


    /**
     * Build a new object for Schemas handling.
     */
    public JonasConnectorSchemas() {
        super();
        addSchemas(JONAS_CONNECTOR_SCHEMAS, JonasConnectorSchemas.class.getClassLoader());
    }

    /**
     * @return Returns the last Schema.
     */
    public static String getLastSchema() {
        return getLastSchema(JONAS_CONNECTOR_SCHEMAS, PACKAGE);
    }

}
