/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.web;

import org.ow2.jonas.deployment.common.CommonsDTDs;
import org.ow2.jonas.deployment.common.util.ResourceHelper;

/**
 * This class defines the declarations of DTDs for web.xml.
 * @author Florent Benoit
 */
public class WebAppDTDs extends CommonsDTDs {

    /**
     * Package name.
     */
    private static final String PACKAGE = ResourceHelper.getResourcePackage(WebAppDTDs.class);

    /**
     * List of web-app dtds.
     */
    private static final String[] WEBAPP_DTDS = new String[] {
        PACKAGE + "web-app_2_2.dtd",
        PACKAGE + "web-app_2_3.dtd"
    };

    /**
     * List of web-app publicId.
     */
    private static final String[] WEBAPP_DTDS_PUBLIC_ID = new String[] {
        "-//Sun Microsystems, Inc.//DTD Web Application 2.2//EN",
        "-//Sun Microsystems, Inc.//DTD Web Application 2.3//EN",
    };



    /**
     * Build a new object for web.xml DTDs handling.
     */
    public WebAppDTDs() {
        super();
        addMapping(WEBAPP_DTDS, WEBAPP_DTDS_PUBLIC_ID, WebAppDTDs.class.getClassLoader());
    }

}
