/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.ejb.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;

/**
 * This class defines the implementation of the element activation-config-property
 *
 * @author JOnAS team
 */

public class ActivationConfigProperty extends AbsElement  {

    /**
     * activation-config-property-name
     */
    private String activationConfigPropertyName = null;

    /**
     * activation-config-property-value
     */
    private String activationConfigPropertyValue = null;

    /**
     * Constructor
     */
    public ActivationConfigProperty() {
        super();
    }

    /**
     * Gets the activation-config-property-name
     * @return the activation-config-property-name
     */
    public String getActivationConfigPropertyName() {
        return activationConfigPropertyName;
    }

    /**
     * Set the activation-config-property-name
     * @param activationConfigPropertyName activation-config-property-name
     */
    public void setActivationConfigPropertyName(String activationConfigPropertyName) {
        this.activationConfigPropertyName = activationConfigPropertyName;
    }


    /**
     * Gets the activation-config-property-value
     * @return the activation-config-property-value
     */
    public String getActivationConfigPropertyValue() {
        return activationConfigPropertyValue;
    }

    /**
     * Set the activation-config-property-value
     * @param activationConfigPropertyValue activation-config-property-value
     */
    public void setActivationConfigPropertyValue(String activationConfigPropertyValue) {
        this.activationConfigPropertyValue = activationConfigPropertyValue;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<activation-config-property>\n");

        indent += 2;

        // activation-config-property-name
        sb.append(xmlElement(activationConfigPropertyName, "activation-config-property-name", indent));
        // activation-config-property-value
        sb.append(xmlElement(activationConfigPropertyValue, "activation-config-property-value", indent));

        indent -= 2;
        sb.append(indent(indent));
        sb.append("</activation-config-property>\n");

        return sb.toString();
    }
}
