/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
/**
 * This class defines the implementation of the element finder-method-jdbc-mapping
 *
 * @author JOnAS team
 */

public class FinderMethodJdbcMapping extends AbsElement  {

    /**
     * jonas-method
     */
    private JonasMethod jonasMethod = null;

    /**
     * jdbc-where-clause
     */
    private String jdbcWhereClause = null;


    /**
     * Constructor
     */
    public FinderMethodJdbcMapping() {
        super();
    }

    /**
     * Gets the jonas-method
     * @return the jonas-method
     */
    public JonasMethod getJonasMethod() {
        return jonasMethod;
    }

    /**
     * Set the jonas-method
     * @param jonasMethod jonasMethod
     */
    public void setJonasMethod(JonasMethod jonasMethod) {
        this.jonasMethod = jonasMethod;
    }

    /**
     * Gets the jdbc-where-clause
     * @return the jdbc-where-clause
     */
    public String getJdbcWhereClause() {
        return jdbcWhereClause;
    }

    /**
     * Set the jdbc-where-clause
     * @param jdbcWhereClause jdbcWhereClause
     */
    public void setJdbcWhereClause(String jdbcWhereClause) {
        this.jdbcWhereClause = jdbcWhereClause;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<finder-method-jdbc-mapping>\n");

        indent += 2;

        // jonas-method
        if (jonasMethod != null) {
            sb.append(jonasMethod.toXML(indent));
        }
        // jdbc-where-clause
        sb.append(xmlElement(jdbcWhereClause, "jdbc-where-clause", indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</finder-method-jdbc-mapping>\n");

        return sb.toString();
    }
}
