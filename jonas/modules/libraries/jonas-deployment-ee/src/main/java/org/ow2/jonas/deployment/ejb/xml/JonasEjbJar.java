/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.xml;

import org.ow2.jonas.deployment.common.CommonsSchemas;
import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;
import org.ow2.jonas.deployment.common.xml.JonasMessageDestination;
import org.ow2.jonas.deployment.common.xml.TopLevelElement;
import org.ow2.jonas.deployment.ejb.JonasEjbjarSchemas;



/**
 * This class defines the implementation of the element jonas-ejb-jar
 *
 * @author JOnAS team
 */

public class JonasEjbJar extends AbsElement implements TopLevelElement {

    /**
     * Header (with right XSD version) for XML
     */
    private String header = null;

    /**
     * jonas-session
     */
    private JLinkedList jonasSessionList = null;

    /**
     * jonas-entity
     */
    private JLinkedList jonasEntityList = null;

    /**
     * jonas-message-driven
     */
    private JLinkedList jonasMessageDrivenList = null;

    /**
     * jonas-ejb-relation
     */
    private JLinkedList jonasEjbRelationList = null;

    /**
     * jonas-message-destination
     */
    private JLinkedList jonasMessageDestinationList = null;

    /**
     * jonas-run-as-mapping
     */
    private JLinkedList jonasRunAsMappingList = null;

    /**
     * jonas-ejb-jar element XML header
     */
    public static final String JONAS_EJBJAR_ELEMENT = CommonsSchemas.getHeaderForElement("jonas-ejb-jar",
                                                                                         JonasEjbjarSchemas.getLastSchema());

    /**
     * Constructor
     */
    public JonasEjbJar() {
        super();
        jonasSessionList = new  JLinkedList("jonas-session");
        jonasEntityList = new  JLinkedList("jonas-entity");
        jonasMessageDrivenList = new  JLinkedList("jonas-message-driven");
        jonasEjbRelationList = new  JLinkedList("jonas-ejb-relation");
        jonasMessageDestinationList = new JLinkedList("jonas-message-destination");
        jonasRunAsMappingList = new JLinkedList("jonas-run-as-mapping");

        header = JONAS_EJBJAR_ELEMENT;
    }

    /**
     * Gets the jonas-run-as-mapping
     * @return the jonas-run-as-mapping
     */
    public JLinkedList getJonasRunAsMappingList() {
        return jonasRunAsMappingList;
    }

    /**
     * Gets the jonas-session
     * @return the jonas-session
     */
    public JLinkedList getJonasSessionList() {
        return jonasSessionList;
    }

    /**
     * Set the jonas-session
     * @param jonasSessionList jonasSession
     */
    public void setJonasSessionList(JLinkedList jonasSessionList) {
        this.jonasSessionList = jonasSessionList;
    }

    /**
     * Add a new jonas-run-as-mapping element to this object
     * @param jonasRunAsMapping the jonasRunAsMapping object
     */
    public void addJonasRunAsMapping(JonasRunAsMapping jonasRunAsMapping) {
        jonasRunAsMappingList.add(jonasRunAsMapping);
    }

    /**
     * Add a new  jonas-session element to this object
     * @param jonasSession the jonasSessionobject
     */
    public void addJonasSession(JonasSession jonasSession) {
        jonasSessionList.add(jonasSession);
    }

    /**
     * Gets the jonas-entity
     * @return the jonas-entity
     */
    public JLinkedList getJonasEntityList() {
        return jonasEntityList;
    }

    /**
     * Set the jonas-entity
     * @param jonasEntityList jonasEntity
     */
    public void setJonasEntityList(JLinkedList jonasEntityList) {
        this.jonasEntityList = jonasEntityList;
    }

    /**
     * Add a new  jonas-entity element to this object
     * @param jonasEntity the jonasEntityobject
     */
    public void addJonasEntity(JonasEntity jonasEntity) {
        jonasEntityList.add(jonasEntity);
    }

    /**
     * Gets the jonas-message-driven
     * @return the jonas-message-driven
     */
    public JLinkedList getJonasMessageDrivenList() {
        return jonasMessageDrivenList;
    }

    /**
     * Set the jonas-message-driven
     * @param jonasMessageDrivenList jonasMessageDriven
     */
    public void setJonasMessageDrivenList(JLinkedList jonasMessageDrivenList) {
        this.jonasMessageDrivenList = jonasMessageDrivenList;
    }

    /**
     * Add a new  jonas-message-driven element to this object
     * @param jonasMessageDriven the jonasMessageDrivenobject
     */
    public void addJonasMessageDriven(JonasMessageDriven jonasMessageDriven) {
        jonasMessageDrivenList.add(jonasMessageDriven);
    }

    /**
     * Gets the jonas-ejb-relation
     * @return the jonas-ejb-relation
     */
    public JLinkedList getJonasEjbRelationList() {
        return jonasEjbRelationList;
    }

    /**
     * Set the jonas-ejb-relation
     * @param jonasEjbRelationList jonasEjbRelation
     */
    public void setJonasEjbRelationList(JLinkedList jonasEjbRelationList) {
        this.jonasEjbRelationList = jonasEjbRelationList;
    }

    /**
     * Add a new  jonas-ejb-relation element to this object
     * @param jonasEjbRelation the jonasEjbRelationobject
     */
    public void addJonasEjbRelation(JonasEjbRelation jonasEjbRelation) {
        jonasEjbRelationList.add(jonasEjbRelation);
    }

    /**
     * @return the list of all jonas-message-destination elements
     */
    public JLinkedList getJonasMessageDestinationList() {
        return jonasMessageDestinationList;
    }

    /**
     * Set the jonas-message-destination
     * @param jonasMessageDestinationList jonasMessageDestination
     */
    public void setJonasMessageDestinationList(JLinkedList jonasMessageDestinationList) {
        this.jonasMessageDestinationList = jonasMessageDestinationList;
    }

    /**
     * Add a new jonas-message-destination element to this object
     * @param jonasMessageDestination the jonas-message-destination object
     */
    public void addJonasMessageDestination(JonasMessageDestination jonasMessageDestination) {
        jonasMessageDestinationList.add(jonasMessageDestination);
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        if (header != null) {
            sb.append(header);
        } else {
            sb.append("<jonas-ejb-jar>");
        }

        indent += 2;

        // jonas-session
        sb.append(jonasSessionList.toXML(indent));
        // jonas-entity
        sb.append(jonasEntityList.toXML(indent));
        // jonas-message-driven
        sb.append(jonasMessageDrivenList.toXML(indent));
        // jonas-ejb-relation
        sb.append(jonasEjbRelationList.toXML(indent));
        // jonas-message-destination
        sb.append(jonasMessageDestinationList.toXML(indent));
        // jonas-run-as-mapping
        sb.append(jonasRunAsMappingList.toXML(indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</jonas-ejb-jar>\n");

        return sb.toString();
    }

    /**
     * @return the header.
     */
    public String getHeader() {
        return header;
    }

    /**
     * @param header The header to set.
     */
    public void setHeader(String header) {
        this.header = header;
    }
}
