/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;

/**
 * This class defines the implementation of the element ior-security-config-mapping
 * @author JOnAS team
 */
public class IorSecurityConfigMapping extends AbsElement  {

    /**
     * transport-config
     */
    private TransportConfigMapping transportConfig = null;

    /**
     * as-context
     */
    private AsContextMapping asContext = null;

    /**
     * sas-context
     */
    private SasContextMapping sasContext = null;


    /**
     * Constructor
     */
    public IorSecurityConfigMapping() {
        super();
    }

    /**
     * @return Returns the asContext.
     */
    public AsContextMapping getAsContext() {
        return asContext;
    }
    /**
     * @param asContext The asContext to set.
     */
    public void setAsContext(AsContextMapping asContext) {
        this.asContext = asContext;
    }
    /**
     * @return Returns the sasContext.
     */
    public SasContextMapping getSasContext() {
        return sasContext;
    }
    /**
     * @param sasContext The sasContext to set.
     */
    public void setSasContext(SasContextMapping sasContext) {
        this.sasContext = sasContext;
    }
    /**
     * @return Returns the transportConfig.
     */
    public TransportConfigMapping getTransportConfig() {
        return transportConfig;
    }
    /**
     * @param transportConfig The transportConfig to set.
     */
    public void setTransportConfig(TransportConfigMapping transportConfig) {
        this.transportConfig = transportConfig;
    }
    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<ior-security-config>\n");

        indent += 2;

        // transport-config
        sb.append(transportConfig.toXML(indent));
        // as-context
        sb.append(asContext.toXML(indent));
        // sas-context
        sb.append(sasContext.toXML(indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</ior-security-config>\n");

        return sb.toString();
    }
}
