/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.web;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Defines a Method object. It manages exclued, unchecked methods with
 * the right transportGuarantee. It also manages the roles which were
 * defined for this method
 * @author Florent Benoit
 */
public class MethodDesc {

    /**
     * Name of the http-method
     */
    private String name = null;


    /**
     * Method excluded ?
     */
    private boolean excluded = false;


    /**
     * Method unchecked (by default = yes) ?
     */
    private boolean unchecked = true;

    /**
     * Transport Guarantee value(s)
     */
    private TransportGuaranteeDesc transportGuarantee;


    /**
     * List of roles and transportGuarantee
     * key = role name
     * value = transport guarantee value
     */
    private List roles = null;


    /**
     * Constructor
     * Build a Method with the right type
     * @param name the name of the method
     */
    public MethodDesc(final String name) {
        this.name = name.toUpperCase();
        this.roles = new ArrayList();
        this.transportGuarantee = new TransportGuaranteeDesc();
    }


    /**
     * Is this method excluded ?
     * @return true is this method is excluded, false otherwise
     */
    public boolean isExcluded() {
        return excluded;
    }

    /**
     * Is this method unchecked ?
     * @return true is this method is unchecked, false otherwise
     */
    public boolean isUnchecked() {
        return unchecked;
    }

    /**
     * Set this method excluded
     */
    public void setExcluded() {
        this.excluded = true;
        this.unchecked = false;
    }

    /**
     * Set this method unchecked
     */
    public void setUnchecked(final boolean unchecked) {
        this.unchecked = unchecked;
        this.excluded = false;
    }


    /**
     * Defines the transport guarantee
     * @param transportGuaranteeValue the value
     */
    public void addTransportGuarantee(final String transportGuaranteeValue) {
        transportGuarantee.addTransportValue(transportGuaranteeValue);
    }


    /**
     * Add role with its transportGuarantee to this method
     * @param role role to add
     * @param transportGuaranteeRoleValue transport guarantee for this role
     */
    public void addRole(final String role, final String transportGuaranteeRoleValue) {
        if (!roles.contains(role)) {
            roles.add(role);
        }
        addTransportGuarantee(transportGuaranteeRoleValue);
    }


    /**
     * Test if there are roles for this method
     * @return true if there are roles defined for this method
     */
    public boolean hasRole() {
        return (roles.size() > 0);
    }


    /**
     * Gets the name of the Method
     * @return name of the Method
     */
    public String getName() {
        return name;
    }


    /**
     * Gets iterator on roles
     * @return iterator on roles
     */
    public Iterator getRolesIterator() {
        return roles.iterator();
    }


    /**
     * Gets the transport guarantee
     * @return transport guarantee
     */
    public TransportGuaranteeDesc getTransportGuarantee() {
        return transportGuarantee;
    }



    /**
     * Defined the Equals method
     * true if it is the same name
     * @param other the object to test if it is equals or not
     * @return true if it is the same object
     */
    @Override
    public boolean equals(final Object other) {
        if (other instanceof String) {
            return name.equals(other);
        } else if (other instanceof MethodDesc) {
            return name.equals(((MethodDesc) other).getName());
        } else {
            return false;
        }
    }

    /**
     * Gets the hashcode for this object
     * @return hashcode of this object
     */
    @Override
    public int hashCode() {
        return name.hashCode();
    }


    /**
     * String representation
     * @return string representation of the pattern
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("Method[name=");
        sb.append(name);
        // excluded
        if (excluded) {
            sb.append(";excluded");
        }
        // unchecked
        if (unchecked) {
            sb.append(";unchecked");
        }
        // transportGuarantee
        sb.append(transportGuarantee);


        // roles
        sb.append(";roles=");
        for (Iterator it = getRolesIterator(); it.hasNext();) {
            String role = (String) it.next();
            sb.append(role);
        }
        sb.append("]");
        return sb.toString();

    }
}
