/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A. 
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;
/** 
 * This class defines the implementation of the element adminobject
 * 
 * @author Eric Hardesty
 */

public class Adminobject extends AbsElement  {

    /**
     * id
     */ 
    private String id = null;

    /**
     * adminobject-interface
     */ 
    private String adminobjectInterface = null;

    /**
     * adminobject-class
     */ 
    private String adminobjectClass = null;

    /**
     * config-property
     */ 
    private JLinkedList configPropertyList = null;


    /**
     * Constructor
     */
    public Adminobject() {
        super();
        configPropertyList = new  JLinkedList("config-property");
    }

    /** 
     * Gets the id
     * @return the id
     */
    public String getId() {
        return id;
    }

    /** 
     * Set the id
     * @param id id object
     */
    public void setId(String id) {
        this.id = id;
    }

    /** 
     * Gets the adminobject-interface
     * @return the adminobject-interface
     */
    public String getAdminobjectInterface() {
        return adminobjectInterface;
    }

    /** 
     * Set the adminobject-interface
     * @param adminobjectInterface adminobjectInterface
     */
    public void setAdminobjectInterface(String adminobjectInterface) {
        this.adminobjectInterface = adminobjectInterface;
    }

    /** 
     * Gets the adminobject-class
     * @return the adminobject-class
     */
    public String getAdminobjectClass() {
        return adminobjectClass;
    }

    /** 
     * Set the adminobject-class
     * @param adminobjectClass adminobjectClass
     */
    public void setAdminobjectClass(String adminobjectClass) {
        this.adminobjectClass = adminobjectClass;
    }

    /** 
     * Gets the config-property
     * @return the config-property
     */
    public JLinkedList getConfigPropertyList() {
        return configPropertyList;
    }

    /** 
     * Set the config-property
     * @param configPropertyList configProperty
     */
    public void setConfigPropertyList(JLinkedList configPropertyList) {
        this.configPropertyList = configPropertyList;
    }

    /** 
     * Add a new  config-property element to this object
     * @param configProperty the configPropertyobject
     */
    public void addConfigProperty(ConfigProperty configProperty) {
        configPropertyList.add(configProperty);
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prefixing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<adminobject");
        sb.append(xmlAttribute(id, "id"));
        sb.append(">\n");

        indent += 2;

        // adminobject-interface
        sb.append(xmlElement(adminobjectInterface, "adminobject-interface", indent));
        // adminobject-class
        sb.append(xmlElement(adminobjectClass, "adminobject-class", indent));
        // config-property
        sb.append(configPropertyList.toXML(indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</adminobject>\n");

        return sb.toString();
    }
}
