/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ear.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
/**
 * This class defines the implementation of the element module
 *
 * @author JOnAS team
 */

public class Module extends AbsElement  {

    /**
     * connector
     */
    private String connector = null;

    /**
     * ejb
     */
    private String ejb = null;

    /**
     * java
     */
    private String java = null;

    /**
     * web
     */
    private Web web = null;

    /**
     * alt-dd
     */
    private String altDd = null;


    /**
     * Constructor
     */
    public Module() {
        super();
    }

    /**
     * Gets the connector
     * @return the connector
     */
    public String getConnector() {
        return connector;
    }

    /**
     * Set the connector
     * @param connector connector
     */
    public void setConnector(String connector) {
        this.connector = connector;
    }

    /**
     * Gets the ejb
     * @return the ejb
     */
    public String getEjb() {
        return ejb;
    }

    /**
     * Set the ejb
     * @param ejb ejb
     */
    public void setEjb(String ejb) {
        this.ejb = ejb;
    }

    /**
     * Gets the java
     * @return the java
     */
    public String getJava() {
        return java;
    }

    /**
     * Set the java
     * @param java java
     */
    public void setJava(String java) {
        this.java = java;
    }

    /**
     * Gets the web
     * @return the web
     */
    public Web getWeb() {
        return web;
    }

    /**
     * Set the web
     * @param web web
     */
    public void setWeb(Web web) {
        this.web = web;
    }

    /**
     * Gets the alt-dd
     * @return the alt-dd
     */
    public String getAltDd() {
        return altDd;
    }

    /**
     * Set the alt-dd
     * @param altDd altDd
     */
    public void setAltDd(String altDd) {
        this.altDd = altDd;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<module>\n");

        indent += 2;

        // connector
        if (connector != null) {
            sb.append(xmlElement(connector, "connector", indent));
        }
        // ejb
        if (ejb != null) {
            sb.append(xmlElement(ejb, "ejb", indent));
        }
        // java
        if (java != null) {
            sb.append(xmlElement(java, "java", indent));
        }
        // web
        if (web != null) {
            sb.append(web.toXML(indent));
        }
        // alt-dd
        if (altDd != null) {
            sb.append(xmlElement(altDd, "alt-dd", indent));
        }
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</module>\n");

        return sb.toString();
    }
}
