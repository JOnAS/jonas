/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
/**
 * This class defines the implementation of the element cmp-field-jdbc-mapping
 *
 * @author JOnAS team
 */

public class CmpFieldJdbcMapping extends AbsElement  {

    /**
     * field-name
     */
    private String fieldName = null;

    /**
     * jdbc-field-name
     */
    private String jdbcFieldName = null;

    /**
     * sql-type
     */
    private String sqlType = null;


    /**
     * Constructor
     */
    public CmpFieldJdbcMapping() {
        super();
    }

    /**
     * Gets the field-name
     * @return the field-name
     */
    public String getFieldName() {
        return fieldName;
    }

    /**
     * Set the field-name
     * @param fieldName fieldName
     */
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    /**
     * Gets the jdbc-field-name
     * @return the jdbc-field-name
     */
    public String getJdbcFieldName() {
        return jdbcFieldName;
    }

    /**
     * Set the jdbc-field-name
     * @param jdbcFieldName jdbcFieldName
     */
    public void setJdbcFieldName(String jdbcFieldName) {
        this.jdbcFieldName = jdbcFieldName;
    }

    /**
     * Gets the sql-type
     * @return the sql-type
     */
    public String getSqlType() {
        return sqlType;
    }

    /**
     * Set the sql-type
     * @param sqlType sqlType
     */
    public void setSqlType(String sqlType) {
        this.sqlType = sqlType;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<cmp-field-jdbc-mapping>\n");

        indent += 2;

        // field-name
        sb.append(xmlElement(fieldName, "field-name", indent));
        // jdbc-field-name
        sb.append(xmlElement(jdbcFieldName, "jdbc-field-name", indent));
        // sql-type
        sb.append(xmlElement(sqlType, "sql-type", indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</cmp-field-jdbc-mapping>\n");

        return sb.toString();
    }
}
