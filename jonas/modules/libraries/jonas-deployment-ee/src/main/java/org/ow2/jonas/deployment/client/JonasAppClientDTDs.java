/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.client;

import org.ow2.jonas.deployment.common.CommonsDTDs;
import org.ow2.jonas.deployment.common.util.ResourceHelper;

/**
 * This class defines the declarations of DTDs for jonas-client.xml.
 * @author Florent Benoit
 */
public class JonasAppClientDTDs extends CommonsDTDs {

    /**
     * Package name.
     */
    private static final String PACKAGE = ResourceHelper.getResourcePackage(JonasAppClientDTDs.class);

    /**
     * List of jonas-client-app dtds.
     */
    private static final String[] JONAS_APPCLIENT_DTDS = new String[] {
        PACKAGE + "jonas-client_3_2.dtd"

    };

    /**
     * List of jonas-client-app publicId.
     */
    private static final String[] JONAS_APPCLIENT_DTDS_PUBLIC_ID = new String[] {
        "-//ObjectWeb//DTD JOnAS Client 3.2//EN"
    };


    /**
     * Build a new object for jonas-client.xml DTDs handling.
     */
    public JonasAppClientDTDs() {
        super();
        addMapping(JONAS_APPCLIENT_DTDS, JONAS_APPCLIENT_DTDS_PUBLIC_ID, JonasAppClientDTDs.class.getClassLoader());
    }


}
