/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.web.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;

/**
 * This class defines the implementation of the element web-resource-collection
 * @author Florent Benoit
 */
public class WebResourceCollection extends AbsElement {

    /**
     * web-resource-name
     */
    private String webResourceName = null;

    /**
     * description
     */
    private JLinkedList descriptionList = null;

    /**
     * url-pattern
     */
    private JLinkedList urlPatternList = null;

    /**
     * http-method
     */
    private JLinkedList httpMethodList = null;

    /**
     * Constructor
     */
    public WebResourceCollection() {
        super();
        descriptionList = new  JLinkedList("description");
        urlPatternList = new  JLinkedList("url-pattern");
        httpMethodList = new  JLinkedList("http-method");
    }


    // Setters

    /**
     * Set the web-resource-name of this object
     * @param webResourceName name of this object
     */
    public void setWebResourceName(String webResourceName) {
        this.webResourceName = webResourceName;
    }


    /**
     * Add a new description element to this object
     * @param description description
     */
    public void addDescription(String description) {
        descriptionList.add(description);
    }

    /**
     * Add a new url-pattern element to this object
     * @param urlPattern url-pattern
     */
    public void addUrlPattern(String urlPattern) {
        urlPatternList.add(urlPattern);
    }

    /**
     * Add a new http-method element to this object
     * @param httpMethod http-method
     */
    public void addHttpMethod(String httpMethod) {
        httpMethodList.add(httpMethod);
    }


    // Getters

    /**
     * Gets the web-resource-name
     * @return the web resource name
     */
    public String getWebResourceName() {
        return webResourceName;
    }

    /**
     * Gets the description list
     * @return the description list
     */
    public JLinkedList getDescriptionList() {
        return descriptionList;
    }

    /**
     * Gets the url-pattern list
     * @return the url-pattern list
     */
    public JLinkedList getUrlPatternList() {
        return urlPatternList;
    }

    /**
     * Gets the http-method list
     * @return the http-method list
     */
    public JLinkedList getHttpMethodList() {
        return httpMethodList;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<web-resource-collection>\n");

        indent += 2;
        // web-resource-name
        if (webResourceName != null) {
            sb.append(xmlElement(webResourceName, "web-resource-name", indent));
        }

        // description
        sb.append(descriptionList.toXML(indent));

        // url-pattern
        sb.append(urlPatternList.toXML(indent));

        // http-method
        sb.append(httpMethodList.toXML(indent));


        indent -= 2;
        sb.append(indent(indent));
        sb.append("</web-resource-collection>\n");

        return sb.toString();
    }

}
