/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.web.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;

/**
 * This class defines the implementation of the element security-constraint
 * @author Florent Benoit
 */
public class SecurityConstraint extends AbsElement {

    /**
     * web-resource-collection
     */
    private JLinkedList webResourceCollectionList = null;

    /**
     * auth-constraint
     */
    private AuthConstraint authConstraint = null;

    /**
     * user-data-constraint
     */
    private UserDataConstraint userDataConstraint = null;


    /**
     * Constructor
     */
    public SecurityConstraint() {
        super();
        webResourceCollectionList = new  JLinkedList("web-resource-collection");
    }


    // Setters

    /**
     * Set the web-resource-collection
     * @param webResourceCollectionList web-resource-collection
     */
    public void setWebResourceCollectionList(JLinkedList webResourceCollectionList) {
        this.webResourceCollectionList = webResourceCollectionList;
    }

    /**
     * Add a new web-resource-collection element to this object
     * @param webResourceCollection web-resource-collection
     */
    public void addWebResourceCollection(WebResourceCollection webResourceCollection) {
        webResourceCollectionList.add(webResourceCollection);
    }

    /**
     * Set the auth-constraint
     * @param authConstraint auth-constraint
     */
    public void setAuthConstraint(AuthConstraint authConstraint) {
        this.authConstraint = authConstraint;
    }

    /**
     * Set the user-data-constraint
     * @param userDataConstraint user-data-constraint
     */
    public void setUserDataConstraint(UserDataConstraint userDataConstraint) {
        this.userDataConstraint = userDataConstraint;
    }




    // Getters

    /**
     * Gets the webResourceCollection list
     * @return the webResourceCollection list
     */
    public JLinkedList getWebResourceCollectionList() {
        return webResourceCollectionList;
    }

    /**
     * Gets the authConstraint
     * @return the authConstraint
     */
    public AuthConstraint getAuthConstraint() {
        return authConstraint;
    }

    /**
     * Gets the userDataConstraint
     * @return the userDataConstraint
     */
    public UserDataConstraint getUserDataConstraint() {
        return userDataConstraint;
    }


    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<security-constraint>\n");

        indent += 2;

        // web-resource-collection
        sb.append(webResourceCollectionList.toXML(indent));

        // auth-constraint
        if (authConstraint != null) {
            sb.append(authConstraint.toXML(indent));
        }

        // user-data-constraint
        if (userDataConstraint != null) {
            sb.append(userDataConstraint.toXML(indent));
        }


        indent -= 2;
        sb.append(indent(indent));
        sb.append("</security-constraint>\n");

        return sb.toString();
    }

}
