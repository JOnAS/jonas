/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.deployment.clusterd;

import org.ow2.jonas.deployment.common.DeploymentDescException;

/**
 * Cluster daemon exception when loading its configuration
 * @author Benoit Pelletier
 */
public class ClusterDaemonConfigurationException extends DeploymentDescException {

    /**
     * version UID
     */
    private static final long serialVersionUID = 2734060670758309859L;

    /**
     * Constructs a new ClusterDaemonConfigurationException with no detail
     * message.
     */
    public ClusterDaemonConfigurationException() {
        super();
    }

    /**
     * Constructs a new ClusterDaemonConfigurationException with the specified
     * message.
     * @param message the detail message.
     */
    public ClusterDaemonConfigurationException(String message) {
        super(message);
    }

    /**
     * Constructs a new ClusterDaemonConfigurationException with the specified
     * error cause.
     * @param cause the cause of the error.
     */
    public ClusterDaemonConfigurationException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a new ClusterDaemonConfigurationException with the specified
     * error cause.
     * @param message the detail message.
     * @param cause the cause of the error.
     */
    public ClusterDaemonConfigurationException(String message, Throwable cause) {
        super(message, cause);
    }

}
