/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Eric HARDESTY
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.rar;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.ow2.jonas.deployment.common.AbsDeploymentDesc;
import org.ow2.jonas.deployment.rar.ConfigPropertyDesc;
import org.ow2.jonas.deployment.rar.ConnectorDesc;
import org.ow2.jonas.deployment.rar.JonasConfigPropertyDesc;
import org.ow2.jonas.deployment.rar.JonasConnectorDesc;
import org.ow2.jonas.deployment.rar.ResourceadapterDesc;
import org.ow2.jonas.deployment.rar.xml.ConfigProperty;
import org.ow2.jonas.deployment.rar.xml.Connector;
import org.ow2.jonas.deployment.rar.xml.JonasConfigProperty;
import org.ow2.jonas.deployment.rar.xml.JonasConnector;




/**
 * This class extends the AbsDeploymentDescriptor class of JOnAS
 * It provides a description of the specific RAR desployment descriptor
 *
 * @author Eric Hardesty
 */
public class RarDeploymentDesc extends AbsDeploymentDesc {


    /**
     * Vector of raConfigPropTags (config-property objects).
     */
    private Vector raConfigPropTags = null;

    /**
     * Vector of jonasConfigPropTags (config-property objects).
     */
    private Vector jonasConfigPropTags = null;

    /**
     * Connector
     */
    private Connector connector = null;

    /**
     * Connector
     */
    private ConnectorDesc connectorDesc = null;

    /**
     * Jonas Connector
     */
    private JonasConnectorDesc jonasConnectorDesc = null;

    /**
     * Xml content of the ra.xml file
     */
    private String xmlContent = "";

    /**
     * Xml content of the jonas-ra.xml file
     */
    private String jonasXmlContent = "";


    /**
     * Construct an instance of a RarDeploymentDesc.
     * Called by the RarDeploymentDescManager.getInstance() method.
     *
     * @param classLoader ClassLoader of the classes .
     * @param connector ra.xml parsed file.
     * @param jonasConnector jonas-ra.xml parsed file.
     *
     * @throws RarDeploymentDescException if we can't build an instance.
     */
    public RarDeploymentDesc(
        ClassLoader classLoader,
        Connector connector,
        JonasConnector jonasConnector)
        throws RarDeploymentDescException {

        // test classloader
        if (classLoader == null) {
            throw new RarDeploymentDescException("DeploymentDesc: Classloader is null");
        }

        this.connector = connector;
        this.connectorDesc = new ConnectorDesc(connector);
        this.jonasConnectorDesc = new JonasConnectorDesc(jonasConnector);

        if (connectorDesc != null) {
            // display name
            displayName = null;
            if (connectorDesc.getDisplayName() != null) {
                displayName = connectorDesc.getDisplayName();
            }

            raConfigPropTags = new Vector();
            //Tags
            ResourceadapterDesc rd = connectorDesc.getResourceadapterDesc();
            if (rd != null) {
                for (Iterator i = rd.getConfigPropertyList().iterator(); i.hasNext(); ) {
                    raConfigPropTags.add((ConfigPropertyDesc) i.next());
                }
            }
        }

        if (jonasConnectorDesc != null && 
            jonasConnectorDesc.getJonasConfigPropertyList() != null) {
            jonasConfigPropTags = new Vector();
            //Tags
            for (Iterator i =
                jonasConnectorDesc.getJonasConfigPropertyList().iterator();
                i.hasNext();
                ) {
                jonasConfigPropTags.add((JonasConfigPropertyDesc) i.next());
            }
        }

    }

    /**
     * Set the config-property tags of the ra.xml file.
     * @param conn the Connector objeect with the config-property tags of the ra.xml file.
     */
    public void setRaConfigPropTags(Connector conn) {
        //Tags
        for (Iterator i =
            conn.getResourceadapter().getConfigPropertyList().iterator();
            i.hasNext();
            ) {
            if (raConfigPropTags == null) {
                raConfigPropTags = new Vector();
            }
            raConfigPropTags.add(new ConfigPropertyDesc((ConfigProperty) i.next()));
        }
        if (raConfigPropTags == null) {
            return;
        }
        ConfigPropertyDesc[] tmp = new ConfigPropertyDesc[raConfigPropTags.size()];
        raConfigPropTags.copyInto(tmp);
    }

    /**
     * Get the config-property tags of the ra.xml file.
     * @return the config-property tags of the ra.xml file.
     */
    public ConfigPropertyDesc[] getRaConfigPropTags() {
        if (raConfigPropTags == null) {
            return null;
        }
        ConfigPropertyDesc[] tmp = new ConfigPropertyDesc[raConfigPropTags.size()];
        raConfigPropTags.copyInto(tmp);
        return tmp;
    }

    /**
     * Get the config-property tags of the jonas-ra.xml file.
     * @return the config-property tags of the jonas-ra.xml file.
     */
    public JonasConfigPropertyDesc[] getJonasConfigPropTags() {
        if (jonasConfigPropTags == null) {
            return null;
        }
        if (jonasConfigPropTags != null) {
            JonasConfigPropertyDesc[] tmp =
                new JonasConfigPropertyDesc[jonasConfigPropTags.size()];
            jonasConfigPropTags.copyInto(tmp);
            return tmp;
        }
        return null;
    }

    /**
     * Return a String representation of the RarDeploymentDesc.
     * @return a String representation of the RarDeploymentDesc.
     */
    public String toString() {
        String ret = "Connector :";

        return ret;
    }

    /**
     * Get the current Connector
     * @return the current Connector.
     */
    public Connector getConnector() {
        return connector;
    }

    /**
     * Get the current ConnectorDesc.
     * @return the current ConnectorDesc.
     */
    public ConnectorDesc getConnectorDesc() {
        return connectorDesc;
    }

    /**
     * Get the current JonasConnectorDesc.
     * @return the current JonasConnectorDesc.
     */
    public JonasConnectorDesc getJonasConnectorDesc() {
        return jonasConnectorDesc;
    }
    /**
     * Return the content of the ra.xml file
     * @return the content of the ra.xml file
     */
    public String getXmlContent() {
        return xmlContent;
    }

    /**
     * Return the content of the jonas-ra.xml file
     * @return the content of the jonas-ra.xml file
     */
    public String getJOnASXmlContent() {
        return jonasXmlContent;
    }

    /**
     * Set the content of the ra.xml file
     * @param xml the content of the file
     */
    public void setXmlContent(String xml) {
        xmlContent = xml;
    }

    /**
     * Set the content of the jonas-ra.xml file
     * @param jXml the content of the file
     */
    public void setJOnASXmlContent(String jXml) {
        jonasXmlContent = jXml;
    }
}
