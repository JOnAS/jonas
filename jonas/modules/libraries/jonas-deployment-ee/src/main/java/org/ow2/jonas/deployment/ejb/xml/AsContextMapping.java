/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
/**
 * This class defines the implementation of the element as-context
 *
 * @author JOnAS team
 */

public class AsContextMapping extends AbsElement  {

    /**
     * auth-method
     */
    private String authMethod = null;

    /**
     * realm
     */
    private String realm = null;

    /**
     * required
     */
    private String required = null;


    /**
     * @return Returns the authMethod.
     */
    public String getAuthMethod() {
        return authMethod;
    }
    /**
     * @param authMethod The authMethod to set.
     */
    public void setAuthMethod(String authMethod) {
        this.authMethod = authMethod;
    }
    /**
     * @return Returns the realm.
     */
    public String getRealm() {
        return realm;
    }
    /**
     * @param realm The realm to set.
     */
    public void setRealm(String realm) {
        this.realm = realm;
    }
    /**
     * @return Returns the required.
     */
    public String getRequired() {
        return required;
    }
    /**
     * @param required The required to set.
     */
    public void setRequired(String required) {
        this.required = required;
    }
    /**
     * Constructor
     */
    public AsContextMapping() {
        super();
   }



    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<as-context>\n");

        indent += 2;

        // auth-method
        sb.append(xmlElement(authMethod, "auth-method", indent));
        // realm
        sb.append(xmlElement(realm, "realm", indent));
        // required
        sb.append(xmlElement(required, "required", indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</as-context>\n");

        return sb.toString();
    }
}
