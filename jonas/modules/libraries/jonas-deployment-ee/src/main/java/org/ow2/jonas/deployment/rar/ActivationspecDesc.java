/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A. 
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar;

import java.io.Serializable;
import java.util.List;

import org.ow2.jonas.deployment.rar.xml.Activationspec;

/** 
 * This class defines the implementation of the element activationspec
 * 
 * @author Eric Hardesty
 */

public class ActivationspecDesc implements Serializable  {

    /**
     * activationspec-class
     */ 
    private String activationspecClass = null;

    /**
     * required-config-property
     */ 
    private List requiredConfigPropertyList = null;


    /**
     * Constructor
     */
    public ActivationspecDesc(Activationspec as) {
        if (as != null) {
            activationspecClass = as.getActivationspecClass();
            requiredConfigPropertyList = Utility.requiredConfigProperty(as.getRequiredConfigPropertyList());
        }
    }

    /** 
     * Gets the activationspec-class
     * @return the activationspec-class
     */
    public String getActivationspecClass() {
        return activationspecClass;
    }

    /** 
     * Gets the required-config-property
     * @return the required-config-property
     */
    public List getRequiredConfigPropertyList() {
        return requiredConfigPropertyList;
    }

}
