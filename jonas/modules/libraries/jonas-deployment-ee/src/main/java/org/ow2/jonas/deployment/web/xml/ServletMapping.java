/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.web.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;

/**
 * This class defines the implementation of the element servlet-mapping
 * @author Florent Benoit
 */
public class ServletMapping extends AbsElement {

    /**
     * Name of the servlet
     */
    private String servletName = null;

    /**
     * URL pattern of the servlet
     */
    private String urlPattern = null;


    // Setters


    /**
     * Sets the name of the servlet
     * @param servletName name of the servlet
     */
    public void setServletName(String servletName) {
        this.servletName = servletName;
    }


    /**
     * Sets the url-pattern of the servlet
     * @param urlPattern url-pattern of the servlet
     */
    public void setUrlPattern(String urlPattern) {
        this.urlPattern = urlPattern;
    }



    // Getters

    /**
     * @return the name of the servlet of the servlet-mapping
     */
    public String getServletName() {
        return servletName;
    }

    /**
     * @return the url of the servlet of the servlet-mapping
     */
    public String getUrlPattern() {
        return urlPattern;
    }


    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<servlet-mapping>\n");

        indent += 2;

        // servlet-name
        sb.append(xmlElement(servletName, "servlet-name", indent));

        // url-pattern
        sb.append(xmlElement(urlPattern, "url-pattern", indent));


        indent -= 2;
        sb.append(indent(indent));
        sb.append("</servlet-mapping>\n");

        return sb.toString();
    }

}
