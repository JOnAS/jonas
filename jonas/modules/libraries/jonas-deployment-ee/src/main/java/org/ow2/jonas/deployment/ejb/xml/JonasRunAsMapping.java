/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent Benoit
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;

/**
 * This class defines the implementation of the element jonas-run-as-mapping It
 * defines mapping between principal and list of roles
 * @author Florent Benoit
 */

public class JonasRunAsMapping extends AbsElement {

    /**
     * principal name
     */
    private String principalName = null;

    /**
     * role names
     */
    private JLinkedList roleNamesList = null;

    /**
     * Constructor
     */
    public JonasRunAsMapping() {
        super();
        roleNamesList = new JLinkedList("role-name");
    }

    /**
     * Gets the role-name
     * @return the role-name
     */
    public JLinkedList getRoleNamesList() {
        return roleNamesList;
    }

    /**
     * Add a new role-name element to this object
     * @param roleName the name of the role
     */
    public void addRoleName(String roleName) {
        roleNamesList.add(roleName);
    }

    /**
     * @return the principal Name.
     */
    public String getPrincipalName() {
        return principalName;
    }

    /**
     * Sets the principal name
     * @param principalName name of the principal to set.
     */
    public void setPrincipalName(String principalName) {
        this.principalName = principalName;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<jonas-run-as-mapping>\n");

        indent += 2;

        // principal-name
        sb.append(xmlElement(principalName, "principal-name", indent));

        // role-name
        sb.append(roleNamesList.toXML(indent));

        indent -= 2;
        sb.append(indent(indent));
        sb.append("</jonas-run-as-mapping>\n");

        return sb.toString();
    }
}