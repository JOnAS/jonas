/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ee;

import org.ow2.jonas.deployment.api.IPersistenceUnitRefDesc;
import org.ow2.jonas.deployment.common.DeploymentDescException;
import org.ow2.jonas.deployment.common.xml.PersistenceUnitRef;

/**
 * Class used to represent persistence-unit-ref elements in J2EE deployment descriptors.
 * @author Adriana Danes
 */
public class PersistenceUnitRefDesc implements IPersistenceUnitRefDesc {

    /**
     * optional description.
     */
    private String description;

    /**
     * persistence unit reference name.
     */
    private String persistenceUnitRefName;

    /**
     * persistence unit name.
     */
    private String persistenceUnitName;


    /**
     * Construct a descriptor for an persistence-unit-ref tag.
     * @param persistenceUnitRef the persistence-unit-ref resulting of the xml parsing.
     * @throws DeploymentDescException when missing information for
     * creating the PersistenceUnitRefDesc
     */
    public PersistenceUnitRefDesc(final PersistenceUnitRef persistenceUnitRef) throws DeploymentDescException {
        description = persistenceUnitRef.getDescription();
        persistenceUnitRefName = persistenceUnitRef.getPersistenceUnitRefName();
        persistenceUnitName = persistenceUnitRef.getPersistenceUnitName();
        if (persistenceUnitName == null) {
            // set default value to ""
            persistenceUnitName = "";
        }
    }

    /**
     * @return persistence unit reference description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return persistence unit name
     */
    public String getName() {
        return persistenceUnitName;
    }

    /**
     * @return persistence unit reference name
     */
    public String getRefName() {
        return persistenceUnitRefName;
    }

}
