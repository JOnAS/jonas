/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A. 
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
/** 
 * This class defines the implementation of the element security-entry
 * 
 * @author Eric Hardesty
 */

public class SecurityEntry extends AbsElement  {

    /**
     * principalName
     */ 
    private String principalName = null;

    /**
     * user
     */ 
    private String user = null;

    /**
     * password
     */ 
    private String password = null;

    /**
     * encrypted
     */ 
    private String encrypted = null;

    /**
     * Constructor
     */
    public SecurityEntry() {
        super();
    }

    /** 
     * Gets the principalName
     * @return the principalName
     */
    public String getPrincipalName() {
        return principalName;
    }

    /** 
     * Set the principalName
     * @param principalName id object
     */
    public void setPrincipalName(String principalName) {
        this.principalName = principalName;
    }

    /** 
     * Gets the user
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /** 
     * Set the user
     * @param user id object
     */
    public void setUser(String user) {
        this.user = user;
    }

    /** 
     * Gets the password
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /** 
     * Set the password
     * @param password id object
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /** 
     * Gets the encrypted value
     * @return the encrypted value
     */
    public String getEncrypted() {
        return encrypted;
    }

    /** 
     * Set the encrypted
     * @param encrypted id object
     */
    public void setEncrypted(String encrypted) {
        this.encrypted = encrypted;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prefixing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<security-entry");
        sb.append(xmlAttribute(principalName, "principalName"));
        sb.append(xmlAttribute(user, "user"));
        sb.append(xmlAttribute(password, "password"));
        sb.append(xmlAttribute(encrypted, "encrypted"));
        sb.append("/>\n");

        return sb.toString();
    }
}
