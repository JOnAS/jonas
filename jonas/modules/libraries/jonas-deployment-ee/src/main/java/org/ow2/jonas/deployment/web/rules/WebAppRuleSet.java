/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.web.rules;

import org.ow2.jonas.deployment.common.rules.EnvironmentRuleSet;
import org.ow2.jonas.deployment.common.rules.JRuleSetBase;
import org.ow2.jonas.deployment.common.rules.MessageDestinationRuleSet;
import org.ow2.jonas.deployment.common.rules.SecurityRoleRuleSet;

import org.apache.commons.digester.Digester;

/**
 * This class defines rules to analyze web.xml file
 * @author Florent Benoit
 */
public class WebAppRuleSet extends JRuleSetBase {

    /**
     * Construct an object
     */
    public WebAppRuleSet() {
        super("web-app/");
    }

    /**
     * Add a set of rules to the digester object
     * @param digester Digester instance
     */
    public void addRuleInstances(Digester digester) {
        digester.addCallMethod(prefix + "display-name",
                               "setDisplayName", 0);
        digester.addRuleSet(new ServletRuleSet(prefix));
        digester.addRuleSet(new ServletMappingRuleSet(prefix));
        digester.addRuleSet(new SecurityConstraintRuleSet(prefix));
        digester.addRuleSet(new SecurityRoleRuleSet(prefix));
        digester.addRuleSet(new EnvironmentRuleSet(prefix));
        digester.addRuleSet(new MessageDestinationRuleSet(prefix));
        digester.addRuleSet(new DistributableRuleSet(prefix));

    }


}
