/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.web;


/**
 * Defines a Pattern object for JACC
 * Allow to check if this pattern is a path prefix pattern, and can return the
 * type of the pattern
 * Implements Comparable interface to sort items
 * @see JACC 3.1.3.1 and 3.1.3.3 for more details
 * @author Florent Benoit
 */
public class Pattern implements Comparable {

    /**
     * Exact pattern
     * @see JACC 3.1.3.3 table 3.2
     */
    private static final int EXACT = 0;

    /**
     * Path prefix pattern
     * @see JACC 3.1.3.3 table 3.2
     */
    private static final int PATH_PREFIX = 1;

    /**
     * Extension pattern
     * @see JACC 3.1.3.3 table 3.2
     */
    private static final int EXTENSION = 2;

    /**
     * Default pattern
     * @see JACC 3.1.3.3 table 3.2
     */
    private static final int DEFAULT = 3;

    /**
     * String representation of the pattern
     */
    private String pattern = null;

    /**
     * Type of the pattern
     */
    private int type;



    /**
     * Constructor
     * @param pattern string representation of the pattern
     */
    public Pattern(String pattern) {
        this.pattern = pattern;
        defineTypePattern();
    }

    /**
     * Defines the type of the pattern
     * Avoid to compute the type every time it is asked
     */
    private void defineTypePattern() {
        if (pattern.startsWith("/") && pattern.endsWith("/*")) {
            // Path prefix
            type = PATH_PREFIX;
        } else if (pattern.startsWith("*.")) {
            // Extension
            type = EXTENSION;
        } else if (pattern.equals("/")) {
            // Default
            type = DEFAULT;
        } else {
            // else it is EXACT
            type = EXACT;
        }
    }


    /**
     * Test if this pattern is a path-prefix pattern or not
     * (Starts with "/" and ends with "/*"
     * @return true if this pattern is a path-prefix
     */
    public boolean isPathPrefix() {
        return (type == PATH_PREFIX);
    }


    /**
     * Test if this pattern is an extension pattern
     * (Starts with ".*")
     * @return true if this pattern is an extension pattern
     */
    public boolean isExtensionPattern() {
        return (type == EXTENSION);
    }


    /**
     * Test if this pattern is the default pattern
     * (equals to "/")
     * @return true if this pattern is the default pattern
     */
    public boolean isDefaultPattern() {
        return (type == DEFAULT);
    }


    /**
     * Test if this pattern is an exact pattern
     * (not in the other case)
     * @return true if this pattern is an exact pattern
     */
    public boolean isExactPattern() {
        return (type == EXACT);
    }


    /**
     * Test if the pattern starts with the given pattern
     * It's the inverse of this definition :
     * The other pattern starts with the substring of this pattern,
     * minus its last 2 characters, and the next character
     * of the other pattern, if there is one, is "/"
     * @param substring string to test
     * @return true if the pattern starts with the given pattern
     */
    public boolean isSubstringPattern(String substring) {
        int size = substring.length();
        if (size == 0) {
            return true;
        } else {
            // true if next character = '/' if any or true if equals (no last char)
            return (pattern.startsWith(substring)
                    && (pattern.length() == size || pattern.substring(size).charAt(0) == '/'));
        }
    }


    /**
     * Test if this pattern matches another pattern
     * This URL pattern matches another pattern if they
     * are related, by case sensitive comparison, as follows:
     *  - their pattern values are String equivalent, or
     *  - this pattern is the path-prefix pattern "/*", or
     *  - this pattern is a path-prefix pattern (that is, it
     *    starts with "/" and ends with "/*") and the other
     *    pattern starts with the substring of this pattern,
     *    minus its last 2 characters, and the next character
     *    of the other pattern, if there is one, is "/", or
     * - this pattern is an extension pattern (that is, it
     *   starts with "*.") and the other pattern ends with this
     *   pattern, or this pattern is the special default pattern,
     *   "/", which matches all other patterns.
     * @param otherPattern pattern to check for matching
     * @see JACC 3.1.3.3 for the definition of these rules
     * @return true if the patterns match
     */
    public boolean isMatching(Pattern otherPattern) {
        if (pattern.equals(otherPattern)) {
            // case 1
            return true;
        } else if ((pattern.length() == 2) && isPathPrefix()) {
            // case 2
            return true;
        } else if (isPathPrefix() && otherPattern.isSubstringPattern(pattern.substring(0, pattern.length() - 2))) {
            // case 3
            return true;
        } else if (isExtensionPattern() && otherPattern.getValue().endsWith(pattern.substring(1))) {
            // case 4
            return true;
         } else {
             // case 5 or no match
             return isDefaultPattern();
         }
    }

    /**
     * Gets the string representation of this object
     * @return the string representation of this pattern
     */
    public String getValue() {
        return pattern;
    }


    /**
     * String representation
     * @return the string representation of this pattern
     */
    public String toString() {
        return getValue();
    }

    /**
     * Tests if this object is equal to another object
     * @param o given object to test
     * @return true if the other object is a pattern object with the same value
     */
    public boolean equals(Object o) {
        if (!(o instanceof Pattern)) {
            return false;
        }
        return pattern.equals(((Pattern) o).getValue());
    }

    /**
     * Gets the hashcode of this object
     * @return hashcode of this object
     */
    public int hashCode() {
        return pattern.hashCode();
    }

    /**
     * Compares this object with the specified object for order.
     * @param o object to compare
     * @return a negative integer, zero, or a positive integer
     * as this object is less than, equal to, or greater than
     * the specified object.
     */
    public int compareTo(Object o) {
        if (!(o instanceof Pattern)) {
            return -1;
        }
        return pattern.compareTo(((Pattern) o).getValue());
    }

}
