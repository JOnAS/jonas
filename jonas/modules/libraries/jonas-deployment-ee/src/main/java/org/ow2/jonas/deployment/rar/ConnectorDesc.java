/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A. 
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar;

import java.io.Serializable;
import java.util.List;

import org.ow2.jonas.deployment.rar.xml.Connector;
import org.ow2.jonas.deployment.rar.xml.Resourceadapter;


/** 
 * This class defines the implementation of the element connector
 * 
 * @author Eric Hardesty
 */

public class ConnectorDesc implements Serializable {

    /**
     * display-name
     */ 
    private String displayName = null;

    /**
     * description
     */ 
    private List descriptionList = null;

    /**
     * icon
     */ 
    private IconDesc icon = null;

    /**
     * vendor-name
     */ 
    private String vendorName = null;

    /**
     * spec-version
     */ 
    private String specVersion = null;

    /**
     * eis-type
     */ 
    private String eisType = null;

    /**
     * version
     */ 
    private String version = null;

    /**
     * resourceadapter-version
     */ 
    private String resourceadapterVersion = null;

    /**
     * license
     */ 
    private LicenseDesc license = null;

    /**
     * resourceadapter
     */ 
    private Resourceadapter resourceadapter = null;           //Used by RAConfig
    private ResourceadapterDesc resourceadapterDesc = null;


    /**
     * Constructor
     */
    public ConnectorDesc(Connector conn) {
        if (conn != null) {
            displayName = conn.getDisplayName();
            icon = new IconDesc(conn.getIcon());
            vendorName = conn.getVendorName();
            specVersion = conn.getSpecVersion();
            eisType = conn.getEisType();
            version = conn.getVersion();
            resourceadapterVersion = conn.getResourceadapterVersion();
            descriptionList = conn.getDescriptionList();
            license = new LicenseDesc(conn.getLicense());
            resourceadapter = conn.getResourceadapter();
            resourceadapterDesc = new ResourceadapterDesc(conn.getResourceadapter());
        }
    }

    /** 
     * Gets the display-name
     * @return the display-name
     */
    public String getDisplayName() {
        return displayName;
    }

    /** 
     * Gets the description
     * @return the description
     */
    public List getDescriptionList() {
        return descriptionList;
    }

    /** 
     * Gets the icon
     * @return the icon
     */
    public IconDesc getIcon() {
        return icon;
    }

    /** 
     * Gets the vendor-name
     * @return the vendor-name
     */
    public String getVendorName() {
        return vendorName;
    }

    /** 
     * Gets the spec-version
     * @return the spec-version
     */
    public String getSpecVersion() {
        return specVersion;
    }

    /** 
     * Gets the eis-type
     * @return the eis-type
     */
    public String getEisType() {
        return eisType;
    }

    /** 
     * Gets the version
     * @return the version
     */
    public String getVersion() {
        return version;
    }

    /** 
     * Gets the resourceadapter-version
     * @return the resourceadapter-version
     */
    public String getResourceadapterVersion() {
        return resourceadapterVersion;
    }

    /** 
     * Gets the license
     * @return the license
     */
    public LicenseDesc getLicense() {
        return license;
    }

    /** 
     * Gets the resourceadapter
     * @return the resourceadapter
     */
    public Resourceadapter getResourceadapter() {
        return resourceadapter;
    }

    /** 
     * Gets the resourceadapter
     * @return the resourceadapter
     */
    public ResourceadapterDesc getResourceadapterDesc() {
        return resourceadapterDesc;
    }
    
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("<connector>\n");
        sb.append("  specVersion="+specVersion+"\n");

        // resourceadapter
        if (resourceadapter != null) {
            sb.append(""+resourceadapter+"\n");
        } else {
            sb.append("  ra=null\n");
        }

        sb.append("</connector>\n");

        return sb.toString();
    }

}
