/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Xavier Delplanque
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.ws;


import org.ow2.jonas.deployment.common.DeploymentDescException;

/**
 * The class WSDeploymentDescException that indicates conditions
 * that a reasonable application might want to catch.
 *
 * @author Xavier Delplanque
 */

public class WSDeploymentDescException extends DeploymentDescException {

    /**
     * Constructs a new WebContainerDeploymentDescException with no detail
     * message.
     */
    public WSDeploymentDescException() {
        super();
    }

    /**
     * Constructs a new WSDeploymentDescException with the specified
     * message.
     *
     * @param message the detail message.
     */
    public WSDeploymentDescException(String message) {
        super(message);
    }

    /**
     * Constructs a new WSDeploymentDescException with the specified
     * message and cause.
     *
     * @param message the detail message.
     * @param cause The error or exception that cause this exception.
     */
    public WSDeploymentDescException(String message,
                                     Throwable cause) {
        super(message, cause);
    }

    /**
     * Construct an exception from a causing exception.
     *
     * @param cause The error or exception that cause this exception.
     * The message will be take be this object's message.
     */
    public WSDeploymentDescException(Throwable cause) {
        super(cause);
    }

}
