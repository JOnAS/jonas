/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ear.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
/**
 * This class defines the implementation of the element web
 *
 * @author JOnAS team
 */

public class Web extends AbsElement  {

    /**
     * web-uri
     */
    private String webUri = null;

    /**
     * context-root
     */
    private String contextRoot = null;


    /**
     * Constructor
     */
    public Web() {
        super();
    }

    /**
     * Gets the web-uri
     * @return the web-uri
     */
    public String getWebUri() {
        return webUri;
    }

    /**
     * Set the web-uri
     * @param webUri webUri
     */
    public void setWebUri(String webUri) {
        this.webUri = webUri;
    }

    /**
     * Gets the context-root
     * @return the context-root
     */
    public String getContextRoot() {
        return contextRoot;
    }

    /**
     * Set the context-root
     * @param contextRoot contextRoot
     */
    public void setContextRoot(String contextRoot) {
        this.contextRoot = contextRoot;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<web>\n");

        indent += 2;

        // web-uri
        sb.append(xmlElement(webUri, "web-uri", indent));
        // context-root
        sb.append(xmlElement(contextRoot, "context-root", indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</web>\n");

        return sb.toString();
    }
}
