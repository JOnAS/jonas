/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.clusterd.xml;

import org.ow2.jonas.deployment.clusterd.ClusterDaemonSchemas;
import org.ow2.jonas.deployment.common.CommonsSchemas;
import org.ow2.jonas.deployment.common.xml.AbsDescriptionElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;
import org.ow2.jonas.deployment.common.xml.TopLevelElement;

/**
 * This class defines the configuration of the cluster daemon
 *
 * @author Benoit Pelletier
 * @author eyindanga (Added discovery configuration)
 * @author S. Ali Tokmen (Added JMX security)
 */
public class ClusterDaemon extends AbsDescriptionElement implements TopLevelElement {

    /**
     * Version UID.
     */
    private static final long serialVersionUID = 3212755715938568244L;

    /**
     * name.
     */
    private String name = null;

    /**
     * Domain name
     */
    private String domainName = null;

    /**
     * JOnAS interaction mode : loosely/tighly coupled.
     */
    private String jonasInteractionMode = null;

    /**
     * servers
     */
    private JLinkedList serverList = null;

    /**
     * Discovery configuration : stack file and group name.
     */
    private Discovery discovery =  null;

    /**
     * Whether JMX is secured.
     */
    private boolean jmxSecured = false;

    /**
     * JMX authentication method.
     */
    private String jmxAuthenticationMethod = null;

    /**
     * JMX authentication method's parameter.
     */
    private String jmxAuthenticationParameter = null;

    /**
     * JMX authorization method.
     */
    private String jmxAuthorizationMethod = null;

    /**
     * JMX authorization method's parameter.
     */
    private String jmxAuthorizationParameter = null;

    /**
     * XML export header.
     */
    private static final String header = CommonsSchemas.getHeaderForElement("cluster-daemon",
            ClusterDaemonSchemas.getLastSchema());

    /**
     * Constructor
     */
    public ClusterDaemon() {
        super();
        serverList = new  JLinkedList("server");
    }

    /**
     * Add a new server element to this object
     * @param server the Server object
     */
    @SuppressWarnings("unchecked")
    public void addServer(final Server server) {
        serverList.add(server);
    }

    /**
     * @return Returns the serverList.
     */
    public JLinkedList getServerList() {
        return serverList;
    }

    /**
     * @param serverList The serverList to set.
     */
    public void setServerList(final JLinkedList serverList) {
        this.serverList = serverList;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prefixing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append(header);
        indent += 2;

        // name
        if (getName() != null) {
            sb.append(xmlElement(getName(), "name", indent));
        }

        // domain-name
        if (getDomainName() != null) {
            sb.append(xmlElement(getDomainName(), "domain-name", indent));
        }

        // jonas-interaction-mode
        if (getJonasInteractionMode() != null) {
            sb.append(xmlElement(getJonasInteractionMode(), "jonas-interaction-mode", indent));
        }

        // JMX security
        sb.append(xmlElement(getJmxSecured(), "jmx.secured", indent));
        sb.append(xmlElement(getJmxAuthenticationMethod(), "jmx.authentication.method", indent));
        sb.append(xmlElement(getJmxAuthenticationParameter(), "jmx.authentication.parameter", indent));
        sb.append(xmlElement(getJmxAuthorizationMethod(), "jmx.authorization.method", indent));
        sb.append(xmlElement(getJmxAuthorizationParameter(), "jmx.authorization.parameter", indent));

        //discovery config
        if (discovery != null) {
            sb.append(discovery.toXML(indent));
        }
        // servers
        sb.append(getServerList().toXML(indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</cluster-daemon>\n");

        return sb.toString();
    }

    /**
     *
     * @return the domain name
     */
    public String getDomainName() {
        return domainName;
    }

    /**
     * Sets the domain name
     * @param domainName domain name
     */
    public void setDomainName(final String domainName) {
        this.domainName = domainName;
    }

    /**
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * set the name
     * @param name name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     *
     * @return the interaction mode with JOnAS
     */
    public String getJonasInteractionMode() {
        return jonasInteractionMode;
    }


    /**
     * set the interaction mode with JOnAS.
     * @param jonasInteraction interaction mode
     */
    public void setJonasInteractionMode(final String jonasInteractionMode) {
        this.jonasInteractionMode = jonasInteractionMode;
    }

    /**
     * @return the discoveryStackFile
     */
    public String getDiscoveryStackFile() {
        return discovery.getDiscoveryStackFile();
    }

    /**
     * @param discoveryStackFile the discoveryStackFile to set
     */
    public void setDiscoveryStackFile(final String discoveryStackFile) {
        this.discovery.setDiscoveryStackFile(discoveryStackFile);
    }

    /**
     * @return the discoveryGroupName
     */
    public String getDiscoveryGroupName() {
        return this.discovery.getDiscoveryGroupName();
    }

    /**
     * @param discoveryGroupName the discoveryGroupName to set
     */
    public void setDiscoveryGroupName(final String discoveryGroupName) {
        this.discovery.setDiscoveryGroupName(discoveryGroupName);
    }

    /**
     * @return the discovery
     */
    public Discovery getDiscovery() {
        return discovery;
    }

    /**
     * @param discovery the discovery to set
     */
    public void setDiscovery(Discovery discovery) {
        this.discovery = discovery;
    }

    /**
     * @return Whether JMX is secured.
     */
    public boolean isJmxSecured() {
        return jmxSecured;
    }

    /**
     * @return Whether JMX is secured.
     */
    public String getJmxSecured() {
        return Boolean.toString(jmxSecured);
    }

    /**
     * @param jmxSecured Whether JMX is secured.
     */
    public void setJmxSecured(boolean jmxSecured) {
        this.jmxSecured = jmxSecured;
    }

    /**
     * @param jmxSecured Whether JMX is secured.
     */
    public void setJmxSecured(String jmxSecured) {
        this.jmxSecured = "true".equalsIgnoreCase(jmxSecured);
    }

    /**
     * @return JMX authentication method.
     */
    public String getJmxAuthenticationMethod() {
        return jmxAuthenticationMethod;
    }

    /**
     * @param jmxAuthenticationMethod JMX authentication method.
     */
    public void setJmxAuthenticationMethod(String jmxAuthenticationMethod) {
        this.jmxAuthenticationMethod = jmxAuthenticationMethod;
    }

    /**
     * @return JMX authentication method's parameter.
     */
    public String getJmxAuthenticationParameter() {
        return jmxAuthenticationParameter;
    }

    /**
     * @param jmxAuthenticationParameter JMX authentication method's parameter.
     */
    public void setJmxAuthenticationParameter(String jmxAuthenticationParameter) {
        this.jmxAuthenticationParameter = jmxAuthenticationParameter;
    }

    /**
     * @return JMX authorization method.
     */
    public String getJmxAuthorizationMethod() {
        return jmxAuthorizationMethod;
    }

    /**
     * @param jmxAuthorizationMethod JMX authorization method.
     */
    public void setJmxAuthorizationMethod(String jmxAuthorizationMethod) {
        this.jmxAuthorizationMethod = jmxAuthorizationMethod;
    }

    /**
     * @return JMX authentication method's parameter.
     */
    public String getJmxAuthorizationParameter() {
        return jmxAuthorizationParameter;
    }

    /**
     * @param jmxAuthorizationParameter JMX authentication method's parameter.
     */
    public void setJmxAuthorizationParameter(String jmxAuthorizationParameter) {
        this.jmxAuthorizationParameter = jmxAuthorizationParameter;
    }

}
