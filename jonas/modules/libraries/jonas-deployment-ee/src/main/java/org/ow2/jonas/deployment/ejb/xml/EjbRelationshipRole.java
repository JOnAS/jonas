/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
/**
 * This class defines the implementation of the element ejb-relationship-role
 *
 * @author JOnAS team
 */

public class EjbRelationshipRole extends AbsElement  {

    /**
     * description
     */
    private String description = null;

    /**
     * ejb-relationship-role-name
     */
    private String ejbRelationshipRoleName = null;

    /**
     * multiplicity
     */
    private String multiplicity = null;

    /**
     * cascade-delete
     */
    private boolean cascadeDelete = false;

    /**
     * relationship-role-source
     */
    private RelationshipRoleSource relationshipRoleSource = null;

    /**
     * cmr-field
     */
    private CmrField cmrField = null;


    /**
     * Constructor
     */
    public EjbRelationshipRole() {
        super();
    }

    /**
     * Gets the description
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the description
     * @param description description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the ejb-relationship-role-name
     * @return the ejb-relationship-role-name
     */
    public String getEjbRelationshipRoleName() {
        return ejbRelationshipRoleName;
    }

    /**
     * Set the ejb-relationship-role-name
     * @param ejbRelationshipRoleName ejbRelationshipRoleName
     */
    public void setEjbRelationshipRoleName(String ejbRelationshipRoleName) {
        this.ejbRelationshipRoleName = ejbRelationshipRoleName;
    }

    /**
     * Gets the multiplicity
     * @return the multiplicity
     */
    public String getMultiplicity() {
        return multiplicity;
    }

    /**
     * Set the multiplicity
     * @param multiplicity multiplicity
     */
    public void setMultiplicity(String multiplicity) {
        this.multiplicity = multiplicity;
    }

    /**
     * Gets the cascade-delete
     * @return true if cascade-delete
     */
    public boolean isCascadeDelete() {
        return cascadeDelete;
    }

    /**
     * Set the cascade-delete
     */
    public void setCascadeDelete() {
        this.cascadeDelete = true;
    }

    /**
     * Gets the relationship-role-source
     * @return the relationship-role-source
     */
    public RelationshipRoleSource getRelationshipRoleSource() {
        return relationshipRoleSource;
    }

    /**
     * Set the relationship-role-source
     * @param relationshipRoleSource relationshipRoleSource
     */
    public void setRelationshipRoleSource(RelationshipRoleSource relationshipRoleSource) {
        this.relationshipRoleSource = relationshipRoleSource;
    }

    /**
     * Gets the cmr-field
     * @return the cmr-field
     */
    public CmrField getCmrField() {
        return cmrField;
    }

    /**
     * Set the cmr-field
     * @param cmrField cmrField
     */
    public void setCmrField(CmrField cmrField) {
        this.cmrField = cmrField;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<ejb-relationship-role>\n");

        indent += 2;

        // description
        sb.append(xmlElement(description, "description", indent));
        // ejb-relationship-role-name
        sb.append(xmlElement(ejbRelationshipRoleName, "ejb-relationship-role-name", indent));
        // multiplicity
        sb.append(xmlElement(multiplicity, "multiplicity", indent));
        // cascade-delete
        if (cascadeDelete) {
            sb.append(indent(indent));
            sb.append("<cascade-delete>\n");
            sb.append(indent(indent));
            sb.append("</cascade-delete>\n");
        }
        // relationship-role-source
        if (relationshipRoleSource != null) {
            sb.append(relationshipRoleSource.toXML(indent));
        }
        // cmr-field
        if (cmrField != null) {
            sb.append(cmrField.toXML(indent));
        }
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</ejb-relationship-role>\n");

        return sb.toString();
    }
}
