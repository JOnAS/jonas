/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ws.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;
import org.ow2.jonas.deployment.common.xml.Qname;


/**
 * This class defines the implementation of the element java-xml-type-mapping
 *
 * @author JOnAS team
 */

public class JavaXmlTypeMapping extends AbsElement  {

    /**
     * java-type
     */
    private String javaType = null;

    /**
     * root-type-qname
     */
    private Qname rootTypeQname = null;

    /**
     * anonymous-type-qname
     */
    private Qname anonymousTypeQname = null;

    /**
     * qname-scope
     */
    private String qnameScope = null;

    /**
     * variable-mapping
     */
    private JLinkedList variableMappingList = null;


    /**
     * Constructor
     */
    public JavaXmlTypeMapping() {
        super();
        variableMappingList = new  JLinkedList("variable-mapping");
    }

    /**
     * Gets the class-type
     * @return the class-type
     */
    public String getJavaType() {
        return javaType;
    }

    /**
     * Set the java-type
     * @param javaType javaType
     */
    public void setJavaType(String javaType) {
        this.javaType = javaType;
    }

    /**
     * Gets the root-type-qname
     * @return the root-type-qname
     */
    public Qname getRootTypeQname() {
        return rootTypeQname;
    }

    /**
     * Set the root-type-qname
     * @param rootTypeQname rootTypeQname
     */
    public void setRootTypeQname(Qname rootTypeQname) {
        this.rootTypeQname = rootTypeQname;
    }

    /**
     * Gets the qname-scope
     * @return the qname-scope
     */
    public String getQnameScope() {
        return qnameScope;
    }

    /**
     * Set the qname-scope
     * @param qnameScope qnameScope
     */
    public void setQnameScope(String qnameScope) {
        this.qnameScope = qnameScope;
    }

    /**
     * Gets the variable-mapping
     * @return the variable-mapping
     */
    public JLinkedList getVariableMappingList() {
        return variableMappingList;
    }

    /**
     * Set the variable-mapping
     * @param variableMappingList variableMapping
     */
    public void setVariableMappingList(JLinkedList variableMappingList) {
        this.variableMappingList = variableMappingList;
    }

    /**
     * Add a new  variable-mapping element to this object
     * @param variableMapping the variableMappingobject
     */
    public void addVariableMapping(VariableMapping variableMapping) {
        variableMappingList.add(variableMapping);
    }

    /**
     * @return Returns the anonymousTypeQname.
     */
    public Qname getAnonymousTypeQname() {
        return anonymousTypeQname;
    }

    /**
     * @param anonymousTypeQname The anonymousTypeQname to set.
     */
    public void setAnonymousTypeQname(Qname anonymousTypeQname) {
        this.anonymousTypeQname = anonymousTypeQname;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<java-xml-type-mapping>\n");

        indent += 2;

        // class-type
        sb.append(xmlElement(javaType, "java-type", indent));
        // root-type-qname
        if (rootTypeQname != null) {
            sb.append(rootTypeQname.toXML(indent));
        }
        // anonymous-type-qname
        if (anonymousTypeQname != null) {
            sb.append(anonymousTypeQname.toXML(indent));
        }
        // qname-scope
        sb.append(xmlElement(qnameScope, "qname-scope", indent));
        // variable-mapping
        sb.append(variableMappingList.toXML(indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</java-xml-type-mapping>\n");

        return sb.toString();
    }
}
