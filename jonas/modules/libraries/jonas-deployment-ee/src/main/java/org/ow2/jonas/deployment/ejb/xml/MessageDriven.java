/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.xml;

/**
 * This class defines the implementation of the element message-driven
 *
 * @author JOnAS team
 */

public class MessageDriven extends CommonEjb {

    /**
     * message-selector (EJB2.0 only)
     */
    private String messageSelector = null;

    /**
     * acknowledge-mode (EJB2.0 only)
     */
    private String acknowledgeMode = null;

    /**
     * message-driven-destination (EJB2.0 only)
     */
    private MessageDrivenDestination messageDrivenDestination = null;

    /**
     * message-destination-type (EJB2.1 only)
     */
    private String messageDestinationType = null;

    /**
     * message-destination-link (EJB2.1 only)
     */
    private String messageDestinationLink = null;

    /**
     * activation-config (EJB2.1 only)
     */
    private ActivationConfig  activationConfig = null;

    /**
     * Constructor
     */
    public MessageDriven() {
        super();

        // specifies the messaging type interface of the message driven bean
        // if not specified it is assumed to be javax.jms.MessageListener
        setMessagingType("javax.jms.MessageListener");
    }

    /**
     * Gets the message-selector (EJB2.0 only)
     * @return the message-selector
     */
    public String getMessageSelector() {
        return messageSelector;
    }

    /**
     * Set the message-selector (EJB2.0 only)
     * @param messageSelector messageSelector
     */
    public void setMessageSelector(String messageSelector) {
        this.messageSelector = messageSelector;
    }

    /**
     * Gets the acknowledge-mode (EJB2.0 only)
     * @return the acknowledge-mode
     */
    public String getAcknowledgeMode() {
        return acknowledgeMode;
    }

    /**
     * Set the acknowledge-mode (EJB2.0 only)
     * @param acknowledgeMode acknowledgeMode
     */
    public void setAcknowledgeMode(String acknowledgeMode) {
        this.acknowledgeMode = acknowledgeMode;
    }

    /**
     * Gets the message-driven-destination (EJB2.0 only)
     * @return the message-driven-destination
     */
    public MessageDrivenDestination getMessageDrivenDestination() {
        return messageDrivenDestination;
    }

    /**
     * Set the message-driven-destination  (EJB2.0 only)
     * @param messageDrivenDestination messageDrivenDestination
     */
    public void setMessageDrivenDestination(MessageDrivenDestination messageDrivenDestination) {
        this.messageDrivenDestination = messageDrivenDestination;
    }

    /**
     * Gets the message-destination-type (EJB2.1 only)
     * @return the message-destination-type
     */
    public String getMessageDestinationType() {
        return messageDestinationType;
    }

    /**
     * Set the message-destination-type (EJB2.1 only)
     * @param messageDestinationType message-destination-type
     */
    public void setMessageDestinationType(String messageDestinationType) {
        this.messageDestinationType = messageDestinationType;
    }


    /**
     * Gets the message-destination-link (EJB2.1 only)
     * @return the message-destination-link
     */
    public String getMessageDestinationLink() {
        return messageDestinationLink;
    }

    /**
     * Set the message-destination-link (EJB2.1 only)
     * @param messageDestinationLink message-destination-link
     */
    public void setMessageDestinationLink(String messageDestinationLink) {
        this.messageDestinationLink = messageDestinationLink;
    }

    /**
     * Gets the activation-config (EJB2.1 only)
     * @return the activation-config
     */
    public ActivationConfig getActivationConfig() {
        return activationConfig;
    }

    /**
     * Set the activation-config (EJB2.1 only)
     * @param activationConfig activation-config
     */
    public void setActivationConfig(ActivationConfig activationConfig) {
        this.activationConfig = activationConfig;
    }


    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<message-driven>\n");

        indent += 2;

         // description
        sb.append(xmlElement(getDescription(), "description", indent));
        // display-name
        sb.append(xmlElement(getDisplayName(), "display-name", indent));
        // small-icon
        sb.append(xmlElement(getIcon().getSmallIcon(), "small-icon", indent));
        // large-icon
        sb.append(xmlElement(getIcon().getLargeIcon(), "large-icon", indent));
        // ejb-name
        sb.append(xmlElement(getEjbName(), "ejb-name", indent));
        // ejb-class
        sb.append(xmlElement(getEjbClass(), "ejb-class", indent));
        // messaging-type
        sb.append(xmlElement(getMessagingType(), "messaging-type", indent));
        // transaction-type
        sb.append(xmlElement(getTransactionType(), "transaction-type", indent));

        /* the old EJB2.0
        // message-selector
        sb.append(xmlElement(messageSelector, "message-selector", indent));
        // acknowledge-mode
        sb.append(xmlElement(acknowledgeMode, "acknowledge-mode", indent));
        // message-driven-destination
        if(messageDrivenDestination != null) {
            sb.append(messageDrivenDestination.toXML(indent));
        }
        */

        // message-destination-type (EJB2.1 only)
        sb.append(xmlElement(messageDestinationType, "message-destination-type", indent));
        // message-destination-link  (EJB2.1 only)
        sb.append(xmlElement(messageDestinationLink, "message-destination-link", indent));
        // activation-config (EJB2.1 only)
        if (activationConfig != null) {
            sb.append(activationConfig.toXML(indent));
        }

        // env-entry
        sb.append(getEnvEntryList().toXML(indent));
        // ejb-ref
        sb.append(getEjbRefList().toXML(indent));
        // ejb-local-ref
        sb.append(getEjbLocalRefList().toXML(indent));
        // resource-ref
        sb.append(getResourceRefList().toXML(indent));
        // resource-env-ref
        sb.append(getResourceEnvRefList().toXML(indent));
        // service-ref
        sb.append(getServiceRefList().toXML(indent));
        // message-destination-ref
        sb.append(getMessageDestinationRefList().toXML(indent));
        // security-identity
        if (getSecurityIdentity() != null) {
            sb.append(getSecurityIdentity().toXML(indent));
        }
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</message-driven>\n");

        return sb.toString();
    }
}
