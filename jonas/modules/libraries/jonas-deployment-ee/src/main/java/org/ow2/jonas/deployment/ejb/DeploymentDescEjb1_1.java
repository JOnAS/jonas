/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.deployment.ejb;

import org.ow2.jonas.deployment.common.DeploymentDescException;
import org.ow2.jonas.deployment.common.xml.JLinkedList;
import org.ow2.jonas.deployment.ejb.xml.AssemblyDescriptor;
import org.ow2.jonas.deployment.ejb.xml.EjbJar;
import org.ow2.jonas.deployment.ejb.xml.Entity;
import org.ow2.jonas.deployment.ejb.xml.JonasEjbJar;
import org.ow2.jonas.deployment.ejb.xml.JonasEntity;

import org.objectweb.util.monolog.api.Logger;
import org.objectweb.util.monolog.api.BasicLevel;

/**
 * Class to hold meta-information related to the deployment of an ejb-jar 1.1
 * @author Philippe Durieux (Bull)
 */
public class DeploymentDescEjb1_1 extends DeploymentDesc {

    /**
     * constructor
     */
    public DeploymentDescEjb1_1(ClassLoader cl, EjbJar ej,
                                JonasEjbJar jej, Logger l,
                                String fileName)
        throws DeploymentDescException {
        super(cl, ej, jej, l, fileName);
        // Trace
        if (logger.getCurrentIntLevel() == BasicLevel.DEBUG) {
            logger.log(BasicLevel.DEBUG, "DEPLOYMENT DESCRIPTOR = \n(" + this.toString() + "\n)");
        }
    }

    protected BeanDesc newEntityBeanDesc(ClassLoader classLoader,
                                         Entity ent,
                                         AssemblyDescriptor asd,
                                         JonasEntity jEnt,
                                         JLinkedList jMDRList) throws DeploymentDescException {
        return new EntityJdbcCmp1Desc(classLoader, ent, asd, jEnt, jMDRList, fileName);
    }

}
