/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial Developer : Sauthier Guillaume
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ws;

import org.ow2.jonas.deployment.web.WebContainerDeploymentDesc;
import org.ow2.jonas.deployment.ws.xml.JonasPortComponent;
import org.ow2.jonas.deployment.ws.xml.PortComponent;


/**
 * PortComponent using JaxRpc class.
 * @author Guillaume Sauthier
 */
public class JaxRpcPortComponentDesc extends PortComponentDesc {

    /** the PortComponent is linked to a servlet */
    private WebContainerDeploymentDesc webDesc = null;

    /**
     * Constructs a JaxRpcPortComponentDesc
     * @param cl ClassLoader of the module containing PortComponent
     * @param pc XML Element of the PortComponent ( <port-component>)
     * @param jpc XML Element jonas-port-component
     * @param parent ServiceDesc containing the PortComponent
     * @throws WSDeploymentDescException When call to PortComponentDesc
     *         constructor fails.
     */
    JaxRpcPortComponentDesc(ClassLoader cl, PortComponent pc, JonasPortComponent jpc, ServiceDesc parent)
            throws WSDeploymentDescException {

        super(cl, pc, jpc, parent);

        // set ServiceImplBean link from servlet-link element
        setSibLink(pc.getServiceImplBean().getServletLink());
    }

    /**
     * Return true if the Service Impl Bean is an EJB.
     * @return true if the Service Impl Bean is an EJB.
     */
    public boolean hasBeanImpl() {
        return false;
    }

    /**
     * Returns true if the Service Impl Bean is a JaxRpc component.
     * @return true if the Service Impl Bean is a JaxRpc component.
     */
    public boolean hasJaxRpcImpl() {
        return true;
    }

    /**
     * Returns the WebContainerDeploymentDesc object linked with this
     * portComponentDesc
     * @return the WebContainerDeploymentDesc object linked with this
     *         portComponentDesc
     */
    public WebContainerDeploymentDesc getWebDesc() {
        return webDesc;
    }

    /**
     * Set the webDesc for this endpoint.
     * @param web The web DD declaring the JaxRpc Endpoint.
     */
    public void setWebDesc(WebContainerDeploymentDesc web) {
        webDesc = web;
        setSib(web.getServletClassname(getSibLink()));
    }

    /**
     * Setter method for J2EE component linking.
     * @param desc the descriptor of the component implementing the endpoint.
     * @throws WSDeploymentDescException when desc is an unknown type.
     */
    public void setDesc(Object desc) throws WSDeploymentDescException {
        if (desc instanceof WebContainerDeploymentDesc) {
            setWebDesc((WebContainerDeploymentDesc) desc);
        } else {
            throw new IllegalStateException(getI18n().getMessage("JaxRpcPortComponentDesc.illegalState", //$NON-NLS-1$
                    WebContainerDeploymentDesc.class.getName()));
        }
    }

    /**
     * @return Returns a String representation of the JaxRpcPortComponentDesc
     */
    public String toString() {
        StringBuffer sb = new StringBuffer();

        sb.append(super.toString());
        sb.append("\ngetWebDesc()=" + getWebDesc()); //$NON-NLS-1$

        return sb.toString();
    }
}