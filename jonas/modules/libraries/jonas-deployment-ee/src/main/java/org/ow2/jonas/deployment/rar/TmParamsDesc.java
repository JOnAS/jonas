/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A. 
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar;

import java.io.Serializable;
import java.util.List;

import org.ow2.jonas.deployment.rar.xml.TmParams;


/** 
 * This class defines the implementation of the element tm-params
 * 
 * @author Eric Hardesty
 */

public class TmParamsDesc implements Serializable {

    /**
     * tm-config-property
     */
    private List tmConfigPropertyList = null;


    /**
     * Constructor
     */
    public TmParamsDesc(TmParams tp) {
        if (tp != null) {
            tmConfigPropertyList = Utility.tmConfigProperty(tp.getTmConfigPropertyList());
        }
    }

    /**
     * Gets the tm-config-property
     * @return the tm-config-property
     */
    public List getTmConfigPropertyList() {
        return tmConfigPropertyList;
    }

}
