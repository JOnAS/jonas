/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A. 
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar;

import java.io.Serializable;

import org.ow2.jonas.deployment.rar.xml.TmConfigProperty;


/** 
 * This class defines the implementation of the element tm-config-property
 * 
 * @author Eric Hardesty
 */

public class TmConfigPropertyDesc  implements Serializable {

    /**
     * tm-config-property-name
     */ 
    private String tmConfigPropertyName = null;

    /**
     * tm-config-property-value
     */ 
    private String tmConfigPropertyValue = null;


    /**
     * Constructor
     */
    public TmConfigPropertyDesc(TmConfigProperty tcp) {
        if (tcp != null) {
            tmConfigPropertyName = tcp.getTmConfigPropertyName();
            tmConfigPropertyValue = tcp.getTmConfigPropertyValue();
        }
    }

    /** 
     * Gets the tm-config-property-name
     * @return the tm-config-property-name
     */
    public String getTmConfigPropertyName() {
        return tmConfigPropertyName;
    }

    /** 
     * Gets the tm-config-property-value
     * @return the tm-config-property-value
     */
    public String getTmConfigPropertyValue() {
        return tmConfigPropertyValue;
    }

}
