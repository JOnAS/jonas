/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.ejb.xml;

import java.util.Iterator;

import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;

/**
 * This class defines the implementation of the element ejb-relation
 * @author JOnAS team
 */

public class EjbRelation extends AbsElement {

    /**
     * description
     */
    private String description = null;

    /**
     * ejb-relation-name
     */
    private String ejbRelationName = null;


    /**
     * ejb-relationship-role list
     */
    private JLinkedList ejbRelationshipRoleList = null;


    /**
     * Constructor
     */
    public EjbRelation() {
        super();
        ejbRelationshipRoleList = new JLinkedList("ejb-relationship-role");
    }

    /**
     * Gets the description
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the description
     * @param description description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the ejb-relation-name
     * @return the ejb-relation-name
     */
    public String getEjbRelationName() {
        return ejbRelationName;
    }

    /**
     * Set the ejb-relation-name
     * @param ejbRelationName ejbRelationName
     */
    public void setEjbRelationName(String ejbRelationName) {
        this.ejbRelationName = ejbRelationName;
    }

    /**
     * Add a new  ejb-relationship-role element to this object
     * @param ejbRelationshipRole the ejb-relationship-role object
     */
    public void addEjbRelationshipRole(EjbRelationshipRole ejbRelationshipRole) {
        ejbRelationshipRoleList.add(ejbRelationshipRole);
    }

    /**
     * Gets the ejb-relationship-role list
     * @return the ejb-relationship-role list
     */
    public JLinkedList getEjbRelationshipRoleList() {
        return ejbRelationshipRoleList;
    }

    /**
     * Gets the first ejb-relationship-role
     * @return the ejb-relationship-role
     */
    public EjbRelationshipRole getEjbRelationshipRole() {
        Iterator i = ejbRelationshipRoleList.iterator();
        EjbRelationshipRole ejbRelationshipRole1 = (EjbRelationshipRole) i.next();
        return ejbRelationshipRole1;
    }


    /**
     * Gets the second ejb-relationship-role
     * @return the ejb-relationship-role
     */
    public EjbRelationshipRole getEjbRelationshipRole2() {
        Iterator i = ejbRelationshipRoleList.iterator();
        EjbRelationshipRole ejbRelationshipRole2 = (EjbRelationshipRole) i.next();
        ejbRelationshipRole2 = (EjbRelationshipRole) i.next();
        return ejbRelationshipRole2;
    }



    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<ejb-relation>\n");

        indent += 2;

        // description
        sb.append(xmlElement(description, "description", indent));
        // ejb-relation-name
        sb.append(xmlElement(ejbRelationName, "ejb-relation-name", indent));
        // ejb-relationship-role
        if (ejbRelationshipRoleList != null) {
            for (Iterator i = ejbRelationshipRoleList.iterator(); i.hasNext();) {
                sb.append(((EjbRelationshipRole) i.next()).toXML(indent));
            }
        }

        indent -= 2;
        sb.append(indent(indent));
        sb.append("</ejb-relation>\n");

        return sb.toString();
    }
}
