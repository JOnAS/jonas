/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.web.rules;

import org.apache.commons.digester.Digester;
import org.ow2.jonas.deployment.common.rules.JRuleSetBase;

/**
 * This class defines a rule to analyze jonas servlet
 * @author Francois Fornaciari
 */
public class JonasServletRuleSet extends JRuleSetBase {


    /**
     * Construct an object with a specific prefix
     * @param prefix prefix to use during the parsing
     */
    public JonasServletRuleSet(final String prefix) {
        super(prefix);
    }


    /**
     * Add a set of rules to the digester object
     * @param digester Digester instance
     */
    @Override
    public void addRuleInstances(final Digester digester) {
        digester.addObjectCreate(prefix + "servlet",
                                 "org.ow2.jonas.deployment.web.xml.JonasServlet");
        digester.addSetNext(prefix + "servlet",
                            "addServlet",
                            "org.ow2.jonas.deployment.web.xml.JonasServlet");

        digester.addCallMethod(prefix + "servlet/servlet-name",
                               "setServletName", 0);

        digester.addCallMethod(prefix + "servlet/principal-name",
                               "setPrincipalName", 0);

    }


}


