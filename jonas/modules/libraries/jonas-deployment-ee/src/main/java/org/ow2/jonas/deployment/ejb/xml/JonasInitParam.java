/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
/**
 * This class defines the implementation of the element jonas-init-param
 *
 * @author JOnAS team
 */

public class JonasInitParam extends AbsElement  {

    /**
     * param-name
     */
    private String paramName = null;

    /**
     * param-value
     */
    private String paramValue = null;


    /**
     * Constructor
     */
    public JonasInitParam() {
        super();
    }

    /**
     * Gets the param-name
     * @return the param-name
     */
    public String getParamName() {
        return paramName;
    }

    /**
     * Set the param-name
     * @param paramName paramName
     */
    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    /**
     * Gets the param-value
     * @return the param-value
     */
    public String getParamValue() {
        return paramValue;
    }

    /**
     * Set the param-value
     * @param paramValue paramValue
     */
    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<jonas-init-param>\n");

        indent += 2;

        // param-name
        sb.append(xmlElement(paramName, "param-name", indent));
        // param-value
        sb.append(xmlElement(paramValue, "param-value", indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</jonas-init-param>\n");

        return sb.toString();
    }
}
