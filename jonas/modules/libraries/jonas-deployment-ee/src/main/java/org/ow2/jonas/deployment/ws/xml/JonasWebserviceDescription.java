/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ws.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;

/**
 * This class defines the implementation of the element jonas-webservice-description
 *
 * @author JOnAS team
 */

public class JonasWebserviceDescription extends AbsElement  {

    /**
     * webservice-description-name
     */
    private String webserviceDescriptionName = null;

    /**
     * default-endpoint-uri
     */
    private String defaultEndpointURI = null;

    /**
     * wsdl-publish-directory
     */
    private String wsdlPublishDirectory = null;

    /**
     * jonas-port-component
     */
    private JLinkedList jonasPortComponentList = null;

    /**
     * Constructor
     */
    public JonasWebserviceDescription() {
        super();
        jonasPortComponentList = new JLinkedList("jonas-port-component");
    }

    /**
     * @return Returns the jonasPortComponentList.
     */
    public JLinkedList getJonasPortComponentList() {
        return jonasPortComponentList;
    }

    /**
     * Add a new  jonas-port-component element to this object
     * @param jonasPortComponent the jonasPortComponent object
     */
    public void addJonasPortComponent(JonasPortComponent jonasPortComponent) {
        jonasPortComponentList.add(jonasPortComponent);
    }


    /**
     * @return Returns the defaultEndpointURI.
     */
    public String getDefaultEndpointURI() {
        return defaultEndpointURI;
    }

    /**
     * @param defaultEndpointURI The defaultEndpointURI to set.
     */
    public void setDefaultEndpointURI(String defaultEndpointURI) {
        this.defaultEndpointURI = defaultEndpointURI;
    }

    /**
     * @return Returns the webserviceDescriptionName.
     */
    public String getWebserviceDescriptionName() {
        return webserviceDescriptionName;
    }

    /**
     * @param webserviceDescriptionName The webserviceDescriptionName to set.
     */
    public void setWebserviceDescriptionName(String webserviceDescriptionName) {
        this.webserviceDescriptionName = webserviceDescriptionName;
    }

    /**
     * @return Returns the wsdlPublishDirectory.
     */
    public String getWsdlPublishDirectory() {
        return wsdlPublishDirectory;
    }

    /**
     * @param wsdlPublishDirectory The wsdlPublishDirectory to set.
     */
    public void setWsdlPublishDirectory(String wsdlPublishDirectory) {
        this.wsdlPublishDirectory = wsdlPublishDirectory;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<jonas-webservice-description>\n");

        indent += 2;
        // webservice-description-name
        sb.append(xmlElement(webserviceDescriptionName, "webservice-description-name", indent));
        // default-endpoint-uri
        sb.append(xmlElement(defaultEndpointURI, "default-endpoint-uri", indent));
        // jonas-port-component
        sb.append(jonasPortComponentList.toXML(indent));
        // wsdl-publish-directory
        sb.append(xmlElement(wsdlPublishDirectory, "wsdl-publish-directory", indent));
        indent -= 2;

        sb.append(indent(indent));
        sb.append("</jonas-webservice-description>\n");

        return sb.toString();
    }
}
