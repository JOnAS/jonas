/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A. 
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar;

import java.io.Serializable;
import java.util.List;

import org.ow2.jonas.deployment.rar.xml.RequiredConfigProperty;


/** 
 * This class defines the implementation of the element required-config-property
 * 
 * @author Eric Hardesty
 */

public class RequiredConfigPropertyDesc implements Serializable {

    /**
     * description
     */ 
    private List descriptionList = null;

    /**
     * config-property-name
     */ 
    private String configPropertyName = null;

    /**
     * Constructor
     */
    public RequiredConfigPropertyDesc(RequiredConfigProperty rcp) {
        if (rcp != null) {
            descriptionList = rcp.getDescriptionList();
            configPropertyName = rcp.getConfigPropertyName();
        }
    }

    /** 
     * Gets the description
     * @return the description
     */
    public List getDescriptionList() {
        return descriptionList;
    }

    /** 
     * Gets the config-property-name
     * @return the config-property-name
     */
    public String getConfigPropertyName() {
        return configPropertyName;
    }

}
