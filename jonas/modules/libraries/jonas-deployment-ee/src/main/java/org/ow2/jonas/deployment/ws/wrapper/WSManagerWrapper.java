/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ws.wrapper;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;

import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;
import org.ow2.jonas.deployment.ws.WSDeploymentDesc;
import org.ow2.jonas.deployment.ws.WSDeploymentDescException;
import org.ow2.jonas.lib.bootstrap.LoaderManager;
import org.ow2.jonas.lib.util.Log;


/**
 * Wrap the WSDeploymentDescManager to solve ClassLoader problems linked to
 * Digester.
 *
 * @author Guillaume Sauthier
 */
public class WSManagerWrapper {

    /**
     * WSDeploymentDescManager fully qualified classname
     */
    private static final String WS_MANAGER_CLASSNAME = "org.ow2.jonas.deployment.ws.lib.WSDeploymentDescManager";

    /**
     * logger
     */
    private static Logger logger = Log.getLogger(Log.JONAS_EAR_PREFIX);

    /**
     * Private Empty Constructor for utility class
     */
    private WSManagerWrapper() { }

    /**
     * Wrap the WSDeploymentDescManager.getInstance().getDeploymentDesc(url, moduleCL, earCL) call.
     *
     * @param url module URL
     * @param moduleCL module ClassLoader
     * @param earCL Application ClassLoader
     *
     * @return the WSDeploymentDesc of the given module
     *
     * @throws WSDeploymentDescException When WSDeploymentDesc cannot be instanciated.
     */
    public static WSDeploymentDesc getDeploymentDesc(final URL url, final ClassLoader moduleCL, final ClassLoader earCL)
    throws WSDeploymentDescException {
        LoaderManager lm = LoaderManager.getInstance();
        WSDeploymentDesc wsDD = null;

        try {
            ClassLoader ext = lm.getExternalLoader();
            Class manager = ext.loadClass(WS_MANAGER_CLASSNAME);
            Method m = manager.getDeclaredMethod("getInstance", new Class[] {});
            Object instance = m.invoke(null, new Object[] {});
            m = manager.getDeclaredMethod("getDeploymentDesc", new Class[] {URL.class, ClassLoader.class,
                    ClassLoader.class});
            wsDD = (WSDeploymentDesc) m.invoke(instance, new Object[] {url, moduleCL, earCL});
        } catch (InvocationTargetException ite) {
            Throwable t = ite.getTargetException();
            if (WSDeploymentDescException.class.isInstance(t)) {
                throw (WSDeploymentDescException) ite.getTargetException();
            } else {
                throw new WSDeploymentDescException("WSDeploymentDescManager.getDeploymentDesc fails", t);
            }
        } catch (Exception e) {
            // TODO add i18n here
            throw new WSDeploymentDescException("Problems when using reflection on WSDeploymentDescManager", e);
        }

        return wsDD;
    }

    /**
     * Wrap the WSDeploymentDescManager.getInstance().getDeploymentDesc(url, url, moduleCL, earCL) call.
     *
     * @param url module URL
     * @param unpackedURL unpacked URL of the module (may be null)
     * @param moduleCL module ClassLoader
     * @param earCL Application ClassLoader
     *
     * @return the WSDeploymentDesc of the given module
     *
     * @throws WSDeploymentDescException When WSDeploymentDesc cannot be instanciated.
     */
    public static WSDeploymentDesc getDeploymentDesc(final URL url, final URL unpackedURL, final ClassLoader moduleCL, final ClassLoader earCL)
    throws WSDeploymentDescException {
        LoaderManager lm = LoaderManager.getInstance();
        WSDeploymentDesc wsDD = null;

        try {
            ClassLoader ext = lm.getExternalLoader();
            Class manager = ext.loadClass(WS_MANAGER_CLASSNAME);
            Method m = manager.getDeclaredMethod("getInstance", new Class[] {});
            Object instance = m.invoke(null, new Object[] {});
            m = manager.getDeclaredMethod("getDeploymentDesc", new Class[] {URL.class, URL.class, ClassLoader.class,
                    ClassLoader.class});
            wsDD = (WSDeploymentDesc) m.invoke(instance, new Object[] {url, unpackedURL, moduleCL, earCL});
        } catch (InvocationTargetException ite) {
            Throwable t = ite.getTargetException();
            if (WSDeploymentDescException.class.isInstance(t)) {
                throw (WSDeploymentDescException) ite.getTargetException();
            } else {
                throw new WSDeploymentDescException("WSDeploymentDescManager.getDeploymentDesc fails", t);
            }
        } catch (Exception e) {
            // TODO add i18n here
            throw new WSDeploymentDescException("Problems when using reflection on WSDeploymentDescManager", e);
        }

        return wsDD;
    }

    /**
     * Wrap the WSDeploymentDescManager.setParsingWithValidation() call.
     *
     * @param b true/false
     */
    public static void setParsingWithValidation(final boolean b) {
        LoaderManager lm = LoaderManager.getInstance();

        try {
            ClassLoader ext = lm.getExternalLoader();
            Class manager = ext.loadClass(WS_MANAGER_CLASSNAME);
            Method m = manager.getDeclaredMethod("setParsingWithValidation", new Class[] {boolean.class});
            m.invoke(null, new Object[] {new Boolean(b)});
        } catch (Exception e) {
            // Should never occurs
            logger.log(BasicLevel.ERROR, e);
        }
    }

    /**
     * Wrap the WSDeploymentDescManager.getInstance().removeCache() call.
     *
     * @param cl WebApp ClassLoader
     */
    public static void removeCache(final ClassLoader cl) {
        LoaderManager lm = LoaderManager.getInstance();

        try {
            ClassLoader ext = lm.getExternalLoader();
            Class manager = ext.loadClass(WS_MANAGER_CLASSNAME);
            Method m = manager.getDeclaredMethod("getInstance", new Class[] {});
            Object instance = m.invoke(null, new Object[] {});
            m = manager.getDeclaredMethod("removeCache", new Class[] {ClassLoader.class});
            m.invoke(instance, new Object[] {cl});
        } catch (Exception e) {
            // Should never occurs
            logger.log(BasicLevel.ERROR, e);
        }
    }

}
