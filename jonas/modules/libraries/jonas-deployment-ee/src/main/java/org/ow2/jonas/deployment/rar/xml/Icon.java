/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * Icon as published by the Free Software Foundation; either
 * version 2.1 of the Icon, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public Icon for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * Icon along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;

/**
 * This class defines the implementation of the element icon
 *
 * @author Florent Benoit
 */

public class Icon extends AbsElement  {

    /**
     * small-icon
     */
    private String smallIcon = null;

    /**
     * large-icon
     */
    private String largeIcon = null;


    /**
     * Constructor
     */
    public Icon() {
        super();
    }

    /**
     * Gets the small-icon
     * @return the small-icon
     */
    public String getSmallIcon() {
        return smallIcon;
    }

    /**
     * Set the small-icon
     * @param smallIcon smallIcon
     */
    public void setSmallIcon(String smallIcon) {
        this.smallIcon = smallIcon;
    }

    /**
     * Gets the large-icon
     * @return the large-icon
     */
    public String getLargeIcon() {
        return largeIcon;
    }

    /**
     * Set the large-icon
     * @param largeIcon largeIcon
     */
    public void setLargeIcon(String largeIcon) {
        this.largeIcon = largeIcon;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prefixing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<icon>\n");

        indent += 2;

        // small-icon
        sb.append(xmlElement(smallIcon, "small-icon", indent));
        // large-icon
        sb.append(xmlElement(largeIcon, "large-icon", indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</icon>\n");

        return sb.toString();
    }
}
