/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A. 
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;

/** 
 * This class defines the implementation of the element jonas-config-property
 * 
 * @author Florent Benoit
 */

public class JonasConfigProperty extends AbsElement  {

    /**
     * jonas-config-property-name
     */ 
    private String jonasConfigPropertyName = null;

    /**
     * jonas-config-property-value
     */ 
    private String jonasConfigPropertyValue = null;


    /**
     * Constructor
     */
    public JonasConfigProperty() {
        super();
    }

    /** 
     * Gets the jonas-config-property-name
     * @return the jonas-config-property-name
     */
    public String getJonasConfigPropertyName() {
        return jonasConfigPropertyName;
    }

    /** 
     * Set the jonas-config-property-name
     * @param jonasConfigPropertyName jonasConfigPropertyName
     */
    public void setJonasConfigPropertyName(String jonasConfigPropertyName) {
        this.jonasConfigPropertyName = jonasConfigPropertyName;
    }

    /** 
     * Gets the jonas-config-property-value
     * @return the jonas-config-property-value
     */
    public String getJonasConfigPropertyValue() {
        return jonasConfigPropertyValue;
    }

    /** 
     * Set the jonas-config-property-value
     * @param jonasConfigPropertyValue jonasConfigPropertyValue
     */
    public void setJonasConfigPropertyValue(String jonasConfigPropertyValue) {
        this.jonasConfigPropertyValue = jonasConfigPropertyValue;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prefixing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<jonas-config-property>\n");

        indent += 2;

        // jonas-config-property-name
        sb.append(xmlElement(jonasConfigPropertyName, "jonas-config-property-name", indent));
        // jonas-config-property-value
        sb.append(xmlElement(jonasConfigPropertyValue, "jonas-config-property-value", indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</jonas-config-property>\n");

        return sb.toString();
    }
}
