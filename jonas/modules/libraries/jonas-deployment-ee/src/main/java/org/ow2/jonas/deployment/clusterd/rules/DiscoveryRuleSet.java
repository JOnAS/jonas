/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.clusterd.rules;

import org.apache.commons.digester.Digester;
import org.ow2.jonas.deployment.common.rules.JRuleSetBase;

public class DiscoveryRuleSet extends JRuleSetBase {
    /**
    * Construct an object with the prefix.
    * @param prefix prefix for the rule set
    */
    public DiscoveryRuleSet(String prefix) {
        super(prefix);
    }

    @Override
    public void addRuleInstances(Digester digester) {
        digester.addObjectCreate(prefix + "discovery",
                "org.ow2.jonas.deployment.clusterd.xml.Discovery", "discovery");
        digester.addSetNext(prefix + "discovery",
                "setDiscovery",
        "org.ow2.jonas.deployment.clusterd.xml.Discovery");
        digester.addCallMethod(prefix + "discovery/group-name", "setDiscoveryGroupName", 0);
        digester.addCallMethod(prefix + "discovery/stack-file", "setDiscoveryStackFile", 0);
        digester.addCallMethod(prefix + "discovery/start-up", "setStartDiscovery", 0);
    }

}
