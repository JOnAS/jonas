/* Generated By:JJTree: Do not edit this line. ASTEntityBeanExpression.java */

package org.ow2.jonas.deployment.ejb.ejbql;

public class ASTEntityBeanExpression extends SimpleNode {
  public ASTEntityBeanExpression(int id) {
    super(id);
  }

  public ASTEntityBeanExpression(EJBQL p, int id) {
    super(p, id);
  }


  /** Accept the visitor. **/
  public Object jjtAccept(EJBQLVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
}
