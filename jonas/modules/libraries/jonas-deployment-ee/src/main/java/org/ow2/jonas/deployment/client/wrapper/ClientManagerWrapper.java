/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial Developer : Guillaume Sauthier
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.client.wrapper;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;

import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;
import org.ow2.jonas.deployment.client.ClientContainerDeploymentDesc;
import org.ow2.jonas.deployment.client.ClientContainerDeploymentDescException;
import org.ow2.jonas.lib.bootstrap.LoaderManager;
import org.ow2.jonas.lib.util.Log;

/**
 * Wrap the call to ClientDeploymentDescManager during JOnAS runtime to solve
 * classloader problem with Digester.
 *
 * @author Guillaume Sauthier
 */
public class ClientManagerWrapper {

    /**
     * logger
     */
    private static Logger logger = Log.getLogger("org.ow2.jonas.deployment.client");

    /**
     * Private Empty constructor for utility class.
     */
    private ClientManagerWrapper() { }

    /**
     * Wrap the ClientDeploymentDescManager.getDeploymentDesc(URL, ClassLoader,
     * ClassLoader).
     *
     * @param url URL of the client to deploy
     * @param moduleCL ClassLoader of the Client
     * @param earCL ClassLoader of the application wrapping the client.
     *
     * @return the ClientContainerDeploymentDesc found at the given URL.
     * @throws ClientContainerDeploymentDescException When deployment fails.
     */
    public static ClientContainerDeploymentDesc getDeploymentDesc(final URL url, final ClassLoader moduleCL, final ClassLoader earCL)
            throws ClientContainerDeploymentDescException {
        LoaderManager lm = LoaderManager.getInstance();
        ClientContainerDeploymentDesc ccDD = null;

        try {
            ClassLoader ext = lm.getExternalLoader();
            Class manager = ext.loadClass("org.ow2.jonas.deployment.client.lib.ClientDeploymentDescManager");
            Method m = manager.getDeclaredMethod("getInstance", new Class[] {});
            Object instance = m.invoke(null, new Object[] {});
            m = manager.getDeclaredMethod("getDeploymentDesc", new Class[] {URL.class, ClassLoader.class,
                    ClassLoader.class});
            ccDD = (ClientContainerDeploymentDesc) m.invoke(instance, new Object[] {url, moduleCL, earCL});
        } catch (InvocationTargetException ite) {
            Throwable t = ite.getTargetException();
            if (ClientContainerDeploymentDescException.class.isInstance(t)) {
                throw (ClientContainerDeploymentDescException) ite.getTargetException();
            } else {
                throw new ClientContainerDeploymentDescException("ClientDeploymentDescManager.getDeploymentDesc fails", t);
            }
        } catch (Exception e) {
            e.printStackTrace();
            // TODO add i18n here
            throw new ClientContainerDeploymentDescException("Problems when using reflection on ClientDeploymentDescManager", e);
        }

        return ccDD;
    }

    /**
     * Wrap the ClientDeploymentDescManager.getInstance().setAltDD(ClassLoader, URL[], URL[])
     *
     * @param earClassLoader ClassLoader of the Application wrapping the Client
     * @param clientUrls Array of ClientApplication URL
     * @param clientsAltDDs Array of Client Alternative Deployment Desc URL
     */
    public static void setAltDD(final URLClassLoader earClassLoader, final URL[] clientUrls, final URL[] clientsAltDDs) {
        LoaderManager lm = LoaderManager.getInstance();
        try {
            ClassLoader ext = lm.getExternalLoader();
            Class manager = ext.loadClass("org.ow2.jonas.deployment.client.lib.ClientDeploymentDescManager");
            Method m = manager.getDeclaredMethod("getInstance", new Class[] {});
            Object instance = m.invoke(null, new Object[] {});
            m = manager.getDeclaredMethod("setAltDD", new Class[] {ClassLoader.class, URL[].class, URL[].class});
            m.invoke(instance, new Object[] {earClassLoader, clientUrls, clientsAltDDs});
        } catch (Exception e) {
            // Should never occurs
            logger.log(BasicLevel.ERROR, e);
        }
    }

}