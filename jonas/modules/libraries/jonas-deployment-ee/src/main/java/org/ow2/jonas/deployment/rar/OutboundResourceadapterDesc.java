/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A. 
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar;

import java.io.Serializable;
import java.util.List;

import org.ow2.jonas.deployment.rar.xml.OutboundResourceadapter;

/** 
 * This class defines the implementation of the element outbound-resourceadapter
 * 
 * @author Eric Hardesty
 */

public class OutboundResourceadapterDesc implements Serializable {

    /**
     * connection-definition
     */ 
    private List connectionDefinitionList = null;

    /**
     * transaction-support
     */ 
    private String transactionSupport = null;

    /**
     * authentication-mechanism
     */ 
    private List authenticationMechanismList = null;

    /**
     * reauthentication-support
     */ 
    private String reauthenticationSupport = null;


    /**
     * Constructor
     */
    public OutboundResourceadapterDesc(OutboundResourceadapter or) {
        if (or != null) {
            connectionDefinitionList = Utility.connectionDefinition(or.getConnectionDefinitionList());
            transactionSupport = or.getTransactionSupport();
            authenticationMechanismList = or.getAuthenticationMechanismList();
            reauthenticationSupport = or.getReauthenticationSupport();
        }
    }

    /** 
     * Gets the connection-definition
     * @return the connection-definition
     */
    public List getConnectionDefinitionList() {
        return connectionDefinitionList;
    }

    /** 
     * Gets the transaction-support
     * @return the transaction-support
     */
    public String getTransactionSupport() {
        return transactionSupport;
    }

    /** 
     * Gets the authentication-mechanism
     * @return the authentication-mechanism
     */
    public List getAuthenticationMechanismList() {
        return authenticationMechanismList;
    }

    /** 
     * Gets the reauthentication-support
     * @return the reauthentication-support
     */
    public String getReauthenticationSupport() {
        return reauthenticationSupport;
    }

}
