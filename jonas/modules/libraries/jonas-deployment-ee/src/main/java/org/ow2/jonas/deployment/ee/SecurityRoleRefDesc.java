/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.ee;

import java.security.Permission;

import javax.security.jacc.EJBRoleRefPermission;
import javax.security.jacc.WebRoleRefPermission;

import org.ow2.jonas.deployment.common.xml.SecurityRoleRef;


/**
 * Defines a SecurityRoleRefDesc class for the management of
 * EJBRoleRefPermissions and WebRoleRefPermissions in JACC
 * @author Florent Benoit
 */
public class SecurityRoleRefDesc {

    /**
     * EJBRoleRefPermission
     */
    private EJBRoleRefPermission ejbRoleRefPermission = null;


    /**
     * WebRoleRefPermission
     */
    private WebRoleRefPermission webRoleRefPermission = null;


    /**
     * role-link used for addToRole method on PolicyConfiguration
     */
    private String roleLink = null;


    /**
     * role-name
     */
    private String roleName = null;


    /**
     * Constructor for SecurityRoleRefDesc
     * @param componentName name of the EJB or Servlet
     * @param securityRoleRef the security role of the assembly descriptor
     * @param isEjb indicate if the is a SecurityRoleRef for an EJB or a Web Component
     */
    public SecurityRoleRefDesc(String componentName, SecurityRoleRef securityRoleRef, boolean isEjb) {
        this.roleLink = securityRoleRef.getRoleLink();
        this.roleName = securityRoleRef.getRoleName();
        if (isEjb) {
            this.ejbRoleRefPermission = new EJBRoleRefPermission(componentName, roleName);
        } else {
            this.webRoleRefPermission = new WebRoleRefPermission(componentName, roleName);
        }
    }



    /**
     * Gets the role-name value
     * @return role-name
     */
    public String getRoleName() {
        return roleName;
    }


    /**
     * Gets the role-link value
     * @return role-link
     */
    public String getRoleLink() {
        return roleLink;
    }

    /**
     * @return Returns the EJBRoleRefPermission
     */
    public Permission getEJBRoleRefPermission() {
        return ejbRoleRefPermission;
    }

    /**
     * @return Returns the WebRoleRefPermission
     */
    public Permission getWebRoleRefPermission() {
        return webRoleRefPermission;
    }
}
