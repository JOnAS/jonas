/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Florent BENOIT & Ludovic BERT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.ear;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.ow2.jonas.deployment.common.AbsDeploymentDesc;
import org.ow2.jonas.deployment.common.xml.JonasSecurity;
import org.ow2.jonas.deployment.common.xml.SecurityRole;
import org.ow2.jonas.deployment.common.xml.SecurityRoleMapping;
import org.ow2.jonas.deployment.ear.xml.Application;
import org.ow2.jonas.deployment.ear.xml.JonasApplication;
import org.ow2.jonas.deployment.ear.xml.Module;
import org.ow2.jonas.deployment.ear.xml.Web;



/**
 * This class extends the AbsDeploymentDescriptor class of JOnAS It provides a
 * description of the specific EAR desployment descriptor
 * @author Florent Benoit
 * @author Ludovic Bert
 * @author Helene Joanin
 */

public class EarDeploymentDesc extends AbsDeploymentDesc {

    /**
     * Vector of connectorTags (Connector objects).
     */
    private Vector connectorTags = null;

    /**
     * Vector of altDD for connectors
     */
    private Vector altDDConnectors = null;

    /**
     * Vector of ejbTags (Ejb objects).
     */
    private Vector ejbTags = null;

    /**
     * Vector of altDD for EJBs
     */
    private Vector altDDEjbs = null;

    /**
     * Vector of webTags (Web objects).
     */
    private Vector webTags = null;

    /**
     * Vector of clientTags (Java objects).
     */
    private Vector clientTags = null;

    /**
     * Vector of altDD for wars
     */
    private Vector altDDWebs = null;

    /**
     * Vector of altDD for clients
     */
    private Vector altDDClients = null;

    /**
     * Vector of security roles
     */
    private Vector securityRolesNames = null;

    /**
     * Xml content of application.xml file
     */
    private String xmlContent = null;

    /**
     * Xml content of jonas-application.xml file
     */
    private String jonasXmlContent = null;

    /**
     * User to role mapping
     */
    private Map userToRoleMapping = null;

    /**
     * The tenant identifier of the application.
     */
    private String tenantId = null;

    /**
     * Construct an instance of a EarDeploymentDesc. Called by the getInstance()
     * method.
     * @param classLoaderForCls ClassLoader of the classes .
     * @param application application.xml parsed file.
     * @param jonasApplication jonas-application.xml parsed file.
     * @throws EarDeploymentDescException if we can't build an instance.
     */
    public EarDeploymentDesc(ClassLoader classLoaderForCls, Application application, JonasApplication jonasApplication)
            throws EarDeploymentDescException {

        // test classloader
        if (classLoaderForCls == null) {
            throw new EarDeploymentDescException("DeploymentDesc: Classloader is null");
        }

        ejbTags = new Vector();
        connectorTags = new Vector();
        webTags = new Vector();
        clientTags = new Vector();
        altDDEjbs = new Vector();
        altDDClients = new Vector();
        altDDConnectors = new Vector();
        altDDWebs = new Vector();
        securityRolesNames = new Vector();

        // display name
        displayName = application.getDisplayName();

        // Tags
        for (Iterator i = application.getModuleList().iterator(); i.hasNext();) {
            Module module = (Module) i.next();
            String ejb = module.getEjb();
            String connector = module.getConnector();
            String java = module.getJava();
            Web web = module.getWeb();
            String altDD = module.getAltDd();
            if (ejb != null) {
                ejbTags.add(ejb);
                altDDEjbs.add(altDD);
            } else if (connector != null) {
                connectorTags.add(connector);
                altDDConnectors.add(altDD);
            } else if (java != null) {
                clientTags.add(java);
                altDDClients.add(altDD);
            } else if (web != null) {
                webTags.add(web);
                altDDWebs.add(altDD);
            }
        }

        // application security role
        for (Iterator i = application.getSecurityRoleList().iterator(); i.hasNext();) {
            SecurityRole securityRole = (SecurityRole) i.next();
            if (securityRole != null) {
                if (securityRole.getRoleName() != null) {
                    securityRolesNames.add(securityRole.getRoleName());
                }
            }
        }

        //tenantId
        tenantId = jonasApplication.getTenantId();

        // user to role mapping
        JonasSecurity jonasSecurity = jonasApplication.getJonasSecurity();
        if (jonasSecurity != null) {
            userToRoleMapping = new HashMap();
            for (Iterator it = jonasSecurity.getSecurityRoleMappingList().iterator(); it.hasNext();) {
                SecurityRoleMapping securityRoleMapping = (SecurityRoleMapping) it.next();
                if (securityRoleMapping != null) {
                    // get role name
                    String roleName = securityRoleMapping.getRoleName();
                    // get principals
                    List principals = securityRoleMapping.getPrincipalNamesList();

                    // Iterator on principals
                    for (Iterator itPrincipals = principals.iterator(); itPrincipals.hasNext();) {
                        // get principal name
                        String principalName = (String) itPrincipals.next();
                        // Existing mapping ?
                        List currentMapping = (List) userToRoleMapping.get(principalName);
                        if (currentMapping == null) {
                            currentMapping = new ArrayList();
                            userToRoleMapping.put(principalName, currentMapping);
                        }
                        // add mapping
                        currentMapping.add(roleName);
                    }
                }
            }
        }

    }

    /**
     * Get the ejb tags of the application.xml file.
     * @return the ejb tags of the application.xml file.
     */
    public String[] getEjbTags() {
        String[] tmp = new String[ejbTags.size()];
        ejbTags.copyInto(tmp);
        return tmp;
    }

    /**
     * Get the alt-dd of the ejbs of the application.xml file.
     * @return the alt-dd of the ejbs of the application.xml file.
     */
    public String[] getAltDDEjbs() {
        String[] tmp = new String[altDDEjbs.size()];
        altDDEjbs.copyInto(tmp);
        return tmp;
    }

    /**
     * Get the client tags of the application.xml file.
     * @return the client tags of the application.xml file.
     */
    public String[] getClientTags() {
        String[] tmp = new String[clientTags.size()];
        clientTags.copyInto(tmp);
        return tmp;
    }

    /**
     * Get the alt-dd of the clients of the application.xml file.
     * @return the alt-dd of the clients of the application.xml file.
     */
    public String[] getAltDDClients() {
        String[] tmp = new String[altDDClients.size()];
        altDDClients.copyInto(tmp);
        return tmp;
    }

    /**
     * Get the web tags of the application.xml file.
     * @return the web tags of the application.xml file.
     */
    public Web[] getWebTags() {
        Web[] tmp = new Web[webTags.size()];
        webTags.copyInto(tmp);
        return tmp;
    }

    /**
     * Get the alt-dd of the wars of the application.xml file.
     * @return the alt-dd of the wars of the application.xml file.
     */
    public String[] getAltDDWebs() {
        String[] tmp = new String[altDDWebs.size()];
        altDDWebs.copyInto(tmp);
        return tmp;
    }

    /**
     * Get the connector tags of the application.xml file.
     * @return the connector tags of the application.xml file.
     */
    public String[] getConnectorTags() {
        String[] tmp = new String[connectorTags.size()];
        connectorTags.copyInto(tmp);
        return tmp;
    }

    /**
     * Get the alt-dd of the connectors of the application.xml file.
     * @return the alt-dd of the connectors of the application.xml file.
     */
    public String[] getAltDDConnectors() {
        String[] tmp = new String[altDDConnectors.size()];
        altDDConnectors.copyInto(tmp);
        return tmp;
    }

    /**
     * Get the security-role names tags
     * @return the security roles names.
     */
    public String[] getSecurityRolesNames() {
        String[] tmp = new String[securityRolesNames.size()];
        securityRolesNames.copyInto(tmp);
        return tmp;
    }

    /**
     * Get the tenant identifier
     * @return the identifier of the tenant
     */
    public String getTenantId() {
        return tenantId;
    }

    /**
     * Get the content of the xml file
     * @return the content of the application xml file
     */
    public String getXmlContent() {
        return xmlContent;
    }

    /**
     * Get the content of the jonas-application xml file
     * @return the content of the jonas-application xml file
     */
    public String getJonasXmlContent() {
        return jonasXmlContent;
    }

    /**
     * Set the content of the xml file
     * @param xml the content of the application xml file
     */
    public void setXmlContent(String xml) {
        xmlContent = xml;
    }

    /**
     * Set the content of the xml file
     * @param xml the content of the jonas-application xml file
     */
    public void setJonasXmlContent(String xml) {
        jonasXmlContent = xml;
    }

    /**
     * Return a String representation of the EarDeploymentDesc.
     * @return a String representation of the EarDeploymentDesc.
     */
    public String toString() {

        StringBuffer ret = new StringBuffer();
        ret.append("\ndisplay-name=" + displayName);
        ret.append("\nconnectors=");
        for (Enumeration e = connectorTags.elements(); e.hasMoreElements();) {
            ret.append(e.nextElement() + ",");
        }
        ret.append("\nejbs=");
        for (Enumeration e = ejbTags.elements(); e.hasMoreElements();) {
            ret.append(e.nextElement() + ",");
        }
        ret.append("\nwebs=");
        for (Enumeration e = webTags.elements(); e.hasMoreElements();) {
            ret.append(((Web) e.nextElement()).getWebUri() + ",");
        }
        ret.append("\njavas=");
        for (Enumeration e = clientTags.elements(); e.hasMoreElements();) {
            ret.append(e.nextElement() + ",");
        }
        ret.append("\nsecurity-roles-names=");
        for (Enumeration e = securityRolesNames.elements(); e.hasMoreElements();) {
            ret.append(e.nextElement() + ",");
        }

        return ret.toString();
    }

    /**
     * @return the userToRoleMapping.
     */
    public Map getUserToRoleMapping() {
        return userToRoleMapping;
    }
}