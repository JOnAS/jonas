/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial Developer : Sauthier Guillaume
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ws;

import org.ow2.jonas.deployment.ejb.SessionStatelessDesc;
import org.ow2.jonas.deployment.ws.xml.JonasPortComponent;
import org.ow2.jonas.deployment.ws.xml.PortComponent;



/**
 * PortComponentDesc specialization for Session Bean exposed as WebServices.
 * @author Guillaume Sauthier
 */
public class SSBPortComponentDesc extends PortComponentDesc {

    /** linked to the StatelessSessionBean */
    private SessionStatelessDesc ssDesc = null;

    /**
     * Constructs a new SSBPortComponentDesc.
     * @param cl module ClassLoader
     * @param pc XML Element port-component
     * @param jpc XML Element jonas-port-component
     * @param parent ServiceDesc containing the PortComponent
     * @throws WSDeploymentDescException When call to PortComponentDesc
     *         constructor fails.
     */
    SSBPortComponentDesc(ClassLoader cl, PortComponent pc, JonasPortComponent jpc, ServiceDesc parent) throws WSDeploymentDescException {

        super(cl, pc, jpc, parent);

        // set ServiceImplBean link from ejb-link element
        setSibLink(pc.getServiceImplBean().getEjbLink());
    }

    /**
     * Return true if the Service Impl Bean is an EJB.
     * @return true if the Service Impl Bean is an EJB.
     */
    public boolean hasBeanImpl() {
        return true;
    }

    /**
     * Return true if the Service Impl Bean is a JaxRpc component.
     * @return true if the Service Impl Bean is a JaxRpc component.
     */
    public boolean hasJaxRpcImpl() {
        return false;
    }

    /**
     * Return the SessionStatelessDesc object linked with this
     * portComponentDesc.
     * @return the SessionStatelessDesc object linked with this
     *         portComponentDesc.
     */
    public SessionStatelessDesc getSessionStatelessDesc() {
        return ssDesc;
    }

    /**
     * Set the beanDesc for this endpoint.
     * @param bean The SSB Object declaring the endpoint.
     */
    public void setSessionStatelessDesc(SessionStatelessDesc bean) {
        ssDesc = bean;
        setSib(bean.getEjbClass().getName());
    }

    /**
     * Setter method for J2EE component linking.
     * @param desc the descriptor of the component implementing the endpoint.
     * @throws WSDeploymentDescException when desc is an unknown type.
     */
    public void setDesc(Object desc) throws WSDeploymentDescException {
        if (desc instanceof SessionStatelessDesc) {
            setSessionStatelessDesc((SessionStatelessDesc) desc);
        } else {
            throw new IllegalStateException(getI18n().getMessage(
                    "SSBPortComponentDesc.illegalState", SessionStatelessDesc.class.getName())); //$NON-NLS-1$
        }
    }

    /**
     * @return Returns a String representation of this SSBPortComponentDesc
     */
    public String toString() {
        StringBuffer sb = new StringBuffer();

        sb.append(super.toString());
        sb.append("\ngetSessionStatelessDesc().displayName=" + getSessionStatelessDesc());

        return sb.toString();
    }
}