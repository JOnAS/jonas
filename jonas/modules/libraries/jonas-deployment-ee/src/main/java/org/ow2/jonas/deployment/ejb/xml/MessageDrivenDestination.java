/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
/**
 * This class defines the implementation of the element message-driven-destination
 *
 * @author JOnAS team
 */

public class MessageDrivenDestination extends AbsElement  {

    /**
     * destination-type
     */
    private String destinationType = null;

    /**
     * subscription-durability
     */
    private String subscriptionDurability = null;


    /**
     * Constructor
     */
    public MessageDrivenDestination() {
        super();
    }

    /**
     * Gets the destination-type
     * @return the destination-type
     */
    public String getDestinationType() {
        return destinationType;
    }

    /**
     * Set the destination-type
     * @param destinationType destinationType
     */
    public void setDestinationType(String destinationType) {
        this.destinationType = destinationType;
    }

    /**
     * Gets the subscription-durability
     * @return the subscription-durability
     */
    public String getSubscriptionDurability() {
        return subscriptionDurability;
    }

    /**
     * Set the subscription-durability
     * @param subscriptionDurability subscriptionDurability
     */
    public void setSubscriptionDurability(String subscriptionDurability) {
        this.subscriptionDurability = subscriptionDurability;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<message-driven-destination>\n");

        indent += 2;

        // destination-type
        sb.append(xmlElement(destinationType, "destination-type", indent));
        // subscription-durability
        sb.append(xmlElement(subscriptionDurability, "subscription-durability", indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</message-driven-destination>\n");

        return sb.toString();
    }
}
