/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Helene Joanin
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.ws;

import org.ow2.jonas.deployment.common.CommonsSchemas;
import org.ow2.jonas.deployment.common.util.ResourceHelper;

/**
 * This class defines the declarations of Schemas for jaxrpc_mapping.xml.
 * @author Helene Joanin
 */
public class JaxrpcMappingSchemas extends CommonsSchemas {

    /**
     * Package name.
     */
    private static final String PACKAGE = ResourceHelper.getResourcePackage(JaxrpcMappingSchemas.class);

    /**
     * List of schemas used for jaxrpc_mapping.xml.
     */
    private static final String[] JAXRPC_MAPPING_SCHEMAS = new String[] {
        PACKAGE + "j2ee_jaxrpc_mapping_1_1.xsd"
    };


    /**
     * Build a new object for Schemas handling.
     */
    public JaxrpcMappingSchemas() {
        super();
        addSchemas(JAXRPC_MAPPING_SCHEMAS, JaxrpcMappingSchemas.class.getClassLoader());
    }

}

