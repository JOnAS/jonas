/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar;

import java.io.Serializable;
import java.util.List;

import org.ow2.jonas.deployment.rar.xml.ConfigProperty;


/**
 * This class defines the implementation of the element config-property
 *
 * @author Eric Hardesty
 */

public class ConfigPropertyDesc  implements Serializable {

    /**
     * description
     */
    private List descriptionList = null;

    /**
     * config-property-name
     */
    private String configPropertyName = null;

    /**
     * config-property-type
     */
    private String configPropertyType = null;

    /**
     * config-property-value
     */
    private String configPropertyValue = null;


    /**
     * Constructor
     */
    public ConfigPropertyDesc() {
    }

    public ConfigPropertyDesc(ConfigProperty cp) {
        if (cp != null) {
            descriptionList = cp.getDescriptionList();
            configPropertyName = cp.getConfigPropertyName();
            configPropertyType = cp.getConfigPropertyType();
            configPropertyValue = cp.getConfigPropertyValue();
        }
    }

    public ConfigPropertyDesc(ConfigPropertyDesc cpd) {
        descriptionList = cpd.getDescriptionList();
        configPropertyName = cpd.getConfigPropertyName();
        configPropertyType = cpd.getConfigPropertyType();
        configPropertyValue = cpd.getConfigPropertyValue();
    }

    /**
     * Gets the description
     * @return the description
     */
    public List getDescriptionList() {
        return descriptionList;
    }

    /**
     * Gets the config-property-name
     * @return the config-property-name
     */
    public String getConfigPropertyName() {
        return configPropertyName;
    }

    /**
     * Set the config-property-name
     * @param configPropertyName configPropertyName
     */
    public void setConfigPropertyName(String configPropertyName) {
        this.configPropertyName = configPropertyName;
    }

    /**
     * Gets the config-property-type
     * @return the config-property-type
     */
    public String getConfigPropertyType() {
        return configPropertyType;
    }

    /**
     * Set the config-property-type
     * @param configPropertyType configPropertyType
     */
    public void setConfigPropertyType(String configPropertyType) {
        this.configPropertyType = configPropertyType;
    }

    /**
     * Gets the config-property-value
     * @return the config-property-value
     */
    public String getConfigPropertyValue() {
        return configPropertyValue;
    }

    /**
     * Set the config-property-value
     * @param configPropertyValue configPropertyValue
     */
    public void setConfigPropertyValue(String configPropertyValue) {
        this.configPropertyValue = configPropertyValue;
    }
}
