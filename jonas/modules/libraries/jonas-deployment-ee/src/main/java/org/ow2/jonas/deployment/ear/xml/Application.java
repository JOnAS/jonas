/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ear.xml;

import org.ow2.jonas.deployment.common.CommonsSchemas;
import org.ow2.jonas.deployment.common.xml.AbsDescriptionElement;
import org.ow2.jonas.deployment.common.xml.DescriptionGroupXml;
import org.ow2.jonas.deployment.common.xml.JLinkedList;
import org.ow2.jonas.deployment.common.xml.SecurityRole;
import org.ow2.jonas.deployment.common.xml.TopLevelElement;
import org.ow2.jonas.deployment.ear.EarSchemas;

/**
 * This class defines the implementation of the element application
 *
 * @author JOnAS team
 */

public class Application
    extends AbsDescriptionElement
    implements TopLevelElement, DescriptionGroupXml {

    /**
     * Header (with right XSD version) for XML
     */
    private String header = null;

    /**
     * module
     */
    private JLinkedList moduleList = null;

    /**
     * security-role
     */
    private JLinkedList securityRoleList = null;

    /**
     * application element XML header
     */
    public static final String APPLICATION_ELEMENT = CommonsSchemas.getHeaderForStandardElement("application", EarSchemas.getLastSchema());

    /**
     * Constructor
     */
    public Application() {
        super();
        moduleList = new  JLinkedList("module");
        securityRoleList = new  JLinkedList("security-role");

        header = APPLICATION_ELEMENT;
    }

    /**
     * Gets the module
     * @return the module
     */
    public JLinkedList getModuleList() {
        return moduleList;
    }

    /**
     * Set the module
     * @param moduleList module
     */
    public void setModuleList(JLinkedList moduleList) {
        this.moduleList = moduleList;
    }

    /**
     * Add a new  module element to this object
     * @param module the moduleobject
     */
    public void addModule(Module module) {
        moduleList.add(module);
    }

    /**
     * Gets the security-role
     * @return the security-role
     */
    public JLinkedList getSecurityRoleList() {
        return securityRoleList;
    }

    /**
     * Set the security-role
     * @param securityRoleList securityRole
     */
    public void setSecurityRoleList(JLinkedList securityRoleList) {
        this.securityRoleList = securityRoleList;
    }

    /**
     * Add a new  security-role element to this object
     * @param securityRole the securityRoleobject
     */
    public void addSecurityRole(SecurityRole securityRole) {
        securityRoleList.add(securityRole);
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        if (header != null) {
            sb.append(header);
        } else {
            sb.append("<application>\n");
        }

        indent += 2;

        // icon
        if (getIcon() != null) {
            sb.append(getIcon().toXML(indent));
        }
        // display-name
        if (getDisplayName() != null) {
            sb.append(xmlElement(getDisplayName(), "display-name", indent));
        }
        // description
        if (getDescription() != null) {
            sb.append(xmlElement(getDescription(), "description", indent));
        }
        // module
        sb.append(getModuleList().toXML(indent));
        // security-role
        sb.append(getSecurityRoleList().toXML(indent));

        indent -= 2;
        sb.append(indent(indent));
        sb.append("</application>\n");

        return sb.toString();
    }
}
