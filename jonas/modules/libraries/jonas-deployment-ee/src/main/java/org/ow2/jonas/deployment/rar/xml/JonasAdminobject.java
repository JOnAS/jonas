/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;
import org.ow2.jonas.deployment.common.xml.TopLevelElement;

/**
 * This class defines the implementation of the element jonas-adminobject
 *
 * @author Eric Hardesty
 */

public class JonasAdminobject extends AbsElement implements TopLevelElement {

    /**
     * jonas-adminiobjectinterface
     */
    private String jonasAdminobjectinterface = null;

    /**
     * jonas-adminiobjectclass
     */
    private String jonasAdminobjectclass = null;

    /**
     * id
     */
    private String id = null;

    /**
     * description
     */ 
    private JLinkedList descriptionList = null;

    /**
     * jndiname
     */
    private String jndiName = null;

    /**
     * jonas-config-property
     */
    private JLinkedList jonasConfigPropertyList = null;


    /**
     * Constructor
     */
    public JonasAdminobject() {
        super();
        descriptionList = new  JLinkedList("description");
        jonasConfigPropertyList = new  JLinkedList("jonas-config-property");
    }

    /**
     * Gets the jonas-adminobjectinterface
     * @return the jonas-adminobjectinterface
     */
    public String getJonasAdminobjectinterface() {
        return jonasAdminobjectinterface;
    }

    /**
     * Set the jonas-adminobjectinterface
     * @param jonasAdminobjectinterface jonasAdminobjectinterface
     */
    public void setJonasAdminobjectinterface(String jonasAdminobjectinterface) {
        this.jonasAdminobjectinterface = jonasAdminobjectinterface;
    }

    /**
     * Gets the jonas-adminobjectclass
     * @return the jonas-adminobjectclass
     */
    public String getJonasAdminobjectclass() {
        return jonasAdminobjectclass;
    }

    /**
     * Set the jonas-adminobjectclass
     * @param jonasAdminobjectclass jonasAdminobjectclass
     */
    public void setJonasAdminobjectclass(final String jonasAdminobjectclass) {
        this.jonasAdminobjectclass = jonasAdminobjectclass;
    }

    /**
     * Gets the id
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Set the id
     * @param id id
     */
    public void setId(String id) {
        this.id = id;
    }

    /** 
     * Gets the description
     * @return the description
     */
    public JLinkedList getDescriptionList() {
        return descriptionList;
    }

    /** 
     * Set the description
     * @param descriptionList description
     */
    public void setDescriptionList(JLinkedList descriptionList) {
        this.descriptionList = descriptionList;
    }

    /** 
     * Add a new  description element to this object
     * @param description the description String
     */
    public void addDescription(String description) {
        descriptionList.add(description);
    }

    /**
     * Gets the jndiName
     * @return the jndiname
     */
    public String getJndiName() {
        return jndiName;
    }

    /**
     * Set the jndiname
     * @param jndiName jndiname
     */
    public void setJndiName(String jndiName) {
        this.jndiName = jndiName;
    }

    /**
     * Gets the jonas-config-property
     * @return the jonas-config-property
     */
    public JLinkedList getJonasConfigPropertyList() {
        return jonasConfigPropertyList;
    }

    /**
     * Set the jonas-config-property
     * @param jonasConfigPropertyList jonasConfigProperty
     */
    public void setJonasConfigPropertyList(JLinkedList jonasConfigPropertyList) {
        this.jonasConfigPropertyList = jonasConfigPropertyList;
    }

    /**
     * Add a new  jonas-config-property element to this object
     * @param jonasConfigProperty the jonasConfigPropertyobject
     */
    public void addJonasConfigProperty(JonasConfigProperty jonasConfigProperty) {
        jonasConfigPropertyList.add(jonasConfigProperty);
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prefixing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<jonas-adminobject>\n");

        indent += 2;

        // jonas-adminobjectinterface
        sb.append(xmlElement(jonasAdminobjectinterface, "jonas-adminobjectinterface", indent));
        // jonas-adminobjectclass
        sb.append(xmlElement(jonasAdminobjectclass, "jonas-adminobjectclass", indent));
        // id
        sb.append(xmlElement(id, "id", indent));
        // description
        sb.append(descriptionList.toXML(indent));
        // jndiname
        sb.append(xmlElement(jndiName, "jndi-name", indent));
        // jonas-config-property
        if (jonasConfigPropertyList != null) {
            sb.append(jonasConfigPropertyList.toXML(indent));
        }
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</jonas-adminobject>\n");

        return sb.toString();
    }
}
