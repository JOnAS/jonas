/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.ejb;

import org.ow2.jonas.deployment.common.CommonsSchemas;
import org.ow2.jonas.deployment.common.util.ResourceHelper;

/**
 * This class defines the declarations of Schemas for ejb-jar.xml.
 * @author Philippe Coq
 */
public class EjbjarSchemas extends CommonsSchemas {

    /**
     * Package name.
     */
    private static final String PACKAGE = ResourceHelper.getResourcePackage(EjbjarSchemas.class);

    /**
     * List of schemas used for ejb-jar.xml.
     */
    protected static final String[] EJBJAR_SCHEMAS = new String[] {
        PACKAGE + "ejb-jar_2_1.xsd",
        PACKAGE + "ejb-jar_3_0.xsd",
        PACKAGE + "ejb-jar_3_1.xsd"
    };


    /**
     * Build a new object for Schemas handling.
     */
    public EjbjarSchemas() {
        super();
        addSchemas(EJBJAR_SCHEMAS, EjbjarSchemas.class.getClassLoader());
    }

}
