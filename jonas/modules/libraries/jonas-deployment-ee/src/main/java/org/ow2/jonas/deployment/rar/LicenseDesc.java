/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A. 
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar;

import java.io.Serializable;
import java.util.List;

import org.ow2.jonas.deployment.rar.xml.License;


/** 
 * This class defines the implementation of the element license
 * 
 * @author Eric Hardesty
 */

public class LicenseDesc implements Serializable {

    /**
     * description
     */ 
    private List descriptionList = null;

    /**
     * license-required
     */ 
    private String licenseRequired = null;


    /**
     * Constructor
     */
    public LicenseDesc(License lic) {
        if (lic != null) {
            descriptionList = lic.getDescriptionList();
            licenseRequired = lic.getLicenseRequired();
        }
    }

    /** 
     * Gets the description
     * @return the description
     */
    public List getDescriptionList() {
        return descriptionList;
    }

    /** 
     * Add a new  description element to this object
     * @param description the description String
     */
    public void addDescription(String description) {
        descriptionList.add(description);
    }
}
