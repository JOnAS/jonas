/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.rules;

import org.apache.commons.digester.Digester;
import org.ow2.jonas.deployment.common.rules.EnvironmentRuleSet;
import org.ow2.jonas.deployment.common.rules.JRuleSetBase;
import org.ow2.jonas.deployment.common.rules.SecurityRoleRefRuleSet;


/**
 * This class defines the rules to analyze the element entity
 *
 * @author JOnAS team
 */

public class EntityRuleSet extends JRuleSetBase {

    /**
     * Construct an object with a specific prefix
     * @param prefix prefix to use during the parsing
     */
    public EntityRuleSet(final String prefix) {
        super(prefix);
   }
     /**
     * Add a set of rules to the digester object
     * @param digester Digester instance
     */

    @Override
    public void addRuleInstances(final Digester digester) {
        digester.addObjectCreate(prefix + "entity",
                                 "org.ow2.jonas.deployment.ejb.xml.Entity");
        digester.addSetNext(prefix + "entity",
                            "addEntity",
                            "org.ow2.jonas.deployment.ejb.xml.Entity");
        digester.addCallMethod(prefix + "entity/description",
                               "setDescription", 0);
        digester.addCallMethod(prefix + "entity/display-name",
                               "setDisplayName", 0);
        digester.addCallMethod(prefix + "entity/small-icon",
                               "setSmallIcon", 0);
        digester.addCallMethod(prefix + "entity/large-icon",
                               "setLargeIcon", 0);
        digester.addCallMethod(prefix + "entity/ejb-name",
                               "setEjbName", 0);
        digester.addCallMethod(prefix + "session/mapped-name",
                "setMappedName", 0);
        digester.addCallMethod(prefix + "entity/home",
                               "setHome", 0);
        digester.addCallMethod(prefix + "entity/remote",
                               "setRemote", 0);
        digester.addCallMethod(prefix + "entity/local-home",
                               "setLocalHome", 0);
        digester.addCallMethod(prefix + "entity/local",
                               "setLocal", 0);
        digester.addCallMethod(prefix + "entity/ejb-class",
                               "setEjbClass", 0);
        digester.addCallMethod(prefix + "entity/persistence-type",
                               "setPersistenceType", 0);
        digester.addCallMethod(prefix + "entity/prim-key-class",
                               "setPrimKeyClass", 0);
        digester.addCallMethod(prefix + "entity/reentrant",
                               "setReentrant", 0);
        digester.addCallMethod(prefix + "entity/cmp-version",
                               "setCmpVersion", 0);
        digester.addCallMethod(prefix + "entity/abstract-schema-name",
                               "setAbstractSchemaName", 0);
        digester.addRuleSet(new CmpFieldRuleSet(prefix + "entity/"));
        digester.addCallMethod(prefix + "entity/primkey-field",
                               "setPrimkeyField", 0);
        digester.addRuleSet(new EnvironmentRuleSet(prefix + "entity/"));
        digester.addRuleSet(new SecurityRoleRefRuleSet(prefix + "entity/"));
        digester.addRuleSet(new SecurityIdentityRuleSet(prefix + "entity/"));
        digester.addRuleSet(new QueryRuleSet(prefix + "entity/"));
   }
}
