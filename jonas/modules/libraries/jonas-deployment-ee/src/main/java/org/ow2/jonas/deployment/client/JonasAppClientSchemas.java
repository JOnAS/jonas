/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Philippe Coq
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.client;

import org.ow2.jonas.deployment.common.CommonsSchemas;
import org.ow2.jonas.deployment.common.util.ResourceHelper;

/**
 * This class defines the declarations of Schemas for and jonas-web.xml.
 * @author Philippe Coq
 */
public class JonasAppClientSchemas extends CommonsSchemas {

    /**
     * Package name.
     */
    private static final String PACKAGE = ResourceHelper.getResourcePackage(JonasAppClientSchemas.class);

    /**
     * List of schemas used for jonas-web.xml.
     */
    private static final String[] JONAS_APPCLIENT_SCHEMAS = new String[] {
        PACKAGE + "jonas-client_4_0.xsd",
        PACKAGE + "jonas-client_4_1.xsd",
        PACKAGE + "jonas-client_4_1_2.xsd",
        PACKAGE + "jonas-client_4_1_4.xsd",
        PACKAGE + "jonas-client_4_2.xsd",
        PACKAGE + "jonas-client_5_0.xsd"
    };


    /**
     * Build a new object for Schemas handling.
     */
    public JonasAppClientSchemas() {
        super();
        addSchemas(JONAS_APPCLIENT_SCHEMAS, JonasAppClientSchemas.class.getClassLoader());
    }

    /**
     * @return Returns the last Schema.
     */
    public static String getLastSchema() {
        return getLastSchema(JONAS_APPCLIENT_SCHEMAS, PACKAGE);
    }

}
