/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.xml;


/**
 * This class defines the implementation of the element jonas-session
 *
 * @author JOnAS team
 */

public class JonasSession extends  JonasCommonEjb {


    /**
     * is-modified-method-name
     */
    private String isModifiedMethodName = null;

    /**
     * session-timeout
     */
    private String sessionTimeout = null;

    /**
     * jndi-endpoint-name
     */
    private String jndiEndpointName = null;

    /**
     * singleton
     */
    private String singleton = null;

    /**
     * monitoring-enabled
     */
    private String monitoringEnabled = null;

    /**
     * warning-threshold
     */
    private String warningThreshold = null;

    /**
     * Constructor
     */
    public JonasSession() {
        super();
    }

    /**
     * Gets the is-modified-method-name
     * @return the is-modified-method-name
     */
    public String getIsModifiedMethodName() {
        return isModifiedMethodName;
    }

    /**
     * Set the is-modified-method-name
     * @param isModifiedMethodName isModifiedMethodName
     */
    public void setIsModifiedMethodName(final String isModifiedMethodName) {
        this.isModifiedMethodName = isModifiedMethodName;
    }

    /**
     * Gets the session-timeout
     * @return the session-timeout
     */
    public String getSessionTimeout() {
        return sessionTimeout;
    }

    /**
     * Set the session-timeout
     * @param sessionTimeout sessionTimeout
     */
    public void setSessionTimeout(final String sessionTimeout) {
        this.sessionTimeout = sessionTimeout;
    }

    /**
     * Gets the jndi-endpoint-name
     * @return the jndi-endpoint-name
     */
    public String getJndiEndpointName() {
        return jndiEndpointName;
    }

    /**
     * Gets the singleton flag
     * @return the singleton flag
     */
    public String getSingleton() {
        return singleton;
    }

    /**
     * Set the singleton flag
     * @param singleton the singleton flag
     */
    public void setSingleton(final String singleton) {
        this.singleton = singleton;
    }

    /**
     * Set the jndi-endpoint-name
     * @param jndiEndpointName jndi-endpoint-name
     */
    public void setJndiEndpointName(final String jndiEndpointName) {
        this.jndiEndpointName = jndiEndpointName;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    @Override
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<jonas-session>\n");

        indent += 2;

        // ejb-name
        sb.append(xmlElement(getEjbName(), "ejb-name", indent));
        // jndi-name
        sb.append(xmlElement(getJndiName(), "jndi-name", indent));
        // jndi-local-name
        sb.append(xmlElement(getJndiLocalName(), "jndi-local-name", indent));
        // jndi-endpoint-name
        sb.append(xmlElement(getJndiEndpointName(), "jndi-endpoint-name", indent));
         // jonas-ejb-ref
        sb.append(getJonasEjbRefList().toXML(indent));
        // jonas-resource
        sb.append(getJonasResourceList().toXML(indent));
        // jonas-resource-env
        sb.append(getJonasResourceEnvList().toXML(indent));
        // jonas-service-ref
        sb.append(getJonasServiceRefList().toXML(indent));
        // jonas-message-destination-ref
        sb.append(getJonasMessageDestinationRefList().toXML(indent));
        // session-timeout
        sb.append(xmlElement(sessionTimeout, "session-timeout", indent));
        // monitoring-enabled
        sb.append(xmlElement(monitoringEnabled, "monitoring-enabled", indent));
        //warning-threshold
        sb.append(xmlElement(warningThreshold, "warning-threshold", indent));
        //max-cache-size
        sb.append(xmlElement(getMaxCacheSize(), "max-cache-size", indent));
        // min-pool-size
        sb.append(xmlElement(getMinPoolSize(), "min-pool-size", indent));
        // singleton
        sb.append(xmlElement(singleton, "singleton", indent));
        // is-modified-method-name
        if (isModifiedMethodName != null && !isModifiedMethodName.equals("")) {
            sb.append(xmlElement(isModifiedMethodName, "is-modified-method-name", indent));
        }

        // run-as
        if (getRunAsPrincipalName() != null) {
            sb.append(indent(indent));
            sb.append("<run-as>\n");
            indent += 2;
            sb.append(xmlElement(getRunAsPrincipalName(), "principal-name", indent));
            indent -= 2;
            sb.append(indent(indent));
            sb.append("</run-as>\n");
        }
        // ior-security-config
        if (getIorSecurityConfig() != null) {
            sb.append(getIorSecurityConfig().toXML(indent));
        }

        indent -= 2;
        sb.append(indent(indent));
        sb.append("</jonas-session>\n");

        return sb.toString();
    }

    public String getMonitoringEnabled() {
        return monitoringEnabled;
    }

    public void setMonitoringEnabled(final String monitoringEnabled) {
        this.monitoringEnabled = monitoringEnabled;
    }

    public String getWarningThreshold() {
        return warningThreshold;
    }

    public void setWarningThreshold(final String warningThreshold) {
        this.warningThreshold = warningThreshold;
    }
}
