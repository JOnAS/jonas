/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ejb.xml;

import java.lang.reflect.Method;

/**
 * This class defines the implementation of the element session
 * @author JOnAS team
 */

public class Session extends CommonEjb {

    /**
     * session-type
     */
    private String sessionType = null;

    /**
     * service-endpoint
     */
    private String serviceEndpoint = null;

    /**
     * ior-security-config
     */
    private IorSecurityConfigMapping iorSecurityConfigMapping = null;

    /**
     * Cluster object
     */
    private Object Cluster;

    /**
     * @return the cluster
     */
    public Object getCluster() {
        return Cluster;
    }

    /**
     * @param cluster the cluster to set
     */
    public void setCluster(final Object cluster) {
        Cluster = cluster;
    }

    /**
     * Constructor
     */
    public Session() {
        super();

    }

    /**
     * Gets the session-type
     * @return the session-type
     */
    public String getSessionType() {
        return sessionType;
    }

    /**
     * Set the session-type
     * @param sessionType sessionType
     */
    public void setSessionType(final String sessionType) {
        this.sessionType = sessionType;
    }

    /**
     * Gets the service-endpoint
     * @return the service-endpoint
     */
    public String getServiceEndpoint() {
        return serviceEndpoint;
    }

    /**
     * Set the service-endpoint
     * @param serviceEndpoint serviceEndpoint
     */
    public void setServiceEndpoint(final String serviceEndpoint) {
        this.serviceEndpoint = serviceEndpoint;
    }

    /**
     * @return Returns the iorSecurityConfigMapping.
     */
    public IorSecurityConfigMapping getIorSecurityConfigMapping() {
        return iorSecurityConfigMapping;
    }

    /**
     * @param iorSecurityConfigMapping The iorSecurityConfigMapping to set.
     */
    public void setIorSecurityConfigMapping(final IorSecurityConfigMapping iorSecurityConfigMapping) {
        this.iorSecurityConfigMapping = iorSecurityConfigMapping;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    @Override
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<session>\n");

        indent += 2;

        // description
        sb.append(xmlElement(getDescription(), "description", indent));
        // display-name
        sb.append(xmlElement(getDisplayName(), "display-name", indent));
        // small-icon
        sb.append(xmlElement(getIcon().getSmallIcon(), "small-icon", indent));
        // large-icon
        sb.append(xmlElement(getIcon().getLargeIcon(), "large-icon", indent));
        // ejb-name
        sb.append(xmlElement(getEjbName(), "ejb-name", indent));
        // mapped-name
        sb.append(xmlElement(getMappedName(), "mapped-name", indent));
        // home
        sb.append(xmlElement(getHome(), "home", indent));
        // remote
        sb.append(xmlElement(getRemote(), "remote", indent));
        // local-home
        sb.append(xmlElement(getLocalHome(), "local-home", indent));
        // local
        sb.append(xmlElement(getLocal(), "local", indent));
        // service-endpoint
        sb.append(xmlElement(getLocal(), "service-endpoint", indent));
        // ejb-class
        sb.append(xmlElement(getEjbClass(), "ejb-class", indent));
        // session-type
        sb.append(xmlElement(sessionType, "session-type", indent));
        // transaction-type
        sb.append(xmlElement(getTransactionType(), "transaction-type", indent));
        // env-entry
        sb.append(getEnvEntryList().toXML(indent));
        // ejb-ref
        sb.append(getEjbRefList().toXML(indent));
        // ejb-local-ref
        sb.append(getEjbLocalRefList().toXML(indent));
        // resource-ref
        sb.append(getResourceRefList().toXML(indent));
        // resource-env-ref
        sb.append(getResourceEnvRefList().toXML(indent));
        // service-ref
        sb.append(getServiceRefList().toXML(indent));
        // message-destination-ref
        sb.append(getMessageDestinationRefList().toXML(indent));
        // security-role-ref
        sb.append(getSecurityRoleRefList().toXML(indent));
        // security-identity
        if (getSecurityIdentity() != null) {
            sb.append(getSecurityIdentity().toXML(indent));
        }
        // ior-security-config
        if (getIorSecurityConfigMapping() != null) {
            sb.append(indent(indent));
            sb.append("<ior-security-config>\n");
            indent += 2;
            sb.append(iorSecurityConfigMapping.toXML());
            indent -= 2;
            sb.append(indent(indent));
            sb.append("</ior-security-config>\n");
        }
        clusterToxml(sb, indent);

        indent -= 2;
        sb.append(indent(indent));
        sb.append("</session>\n");

        return sb.toString();
    }

    /**
     * Xml structure of the cluster element.
     * @param sb the strig buffer to fill.
     * @param indent the indent.
     */
    private void clusterToxml(final StringBuffer sb, final int indent) {
        // TODO Auto-generated method stub
        // ior-security-config
        int ind = indent;
        if (getCluster() != null) {
            try {
                Method method = Cluster.getClass().getDeclaredMethod("jonasClusterToXml", new Class[]{});
                if (method != null) {
                    String xml = (String) method.invoke(Cluster, new Object[]{});
                    sb.append(indent(indent));
                    sb.append("<cluster-config>\n");
                    ind += 2;
                    sb.append(xml);
                    ind -= 2;
                    sb.append(indent(indent));
                    sb.append("</cluster-config>\n");
                }
            } catch (Exception e) {
                // TODO: handle exception
                return;
            }

        }
    }

}
