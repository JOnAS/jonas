/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.web;

import org.ow2.jonas.deployment.common.CommonsSchemas;
import org.ow2.jonas.deployment.common.util.ResourceHelper;

/**
 * This class defines the declarations of Schemas for web.xml.
 * @author Florent Benoit
 */
public class WebAppSchemas extends CommonsSchemas {

    /**
     * Package name.
     */
    private static final String PACKAGE = ResourceHelper.getResourcePackage(WebAppSchemas.class);

    /**
     * List of schemas used for web.xml.
     */
    public static final String[] WEBAPP_SCHEMAS = new String[] {
        PACKAGE + "web-app_2_4.xsd",
        PACKAGE + "jsp_2_0.xsd",
        PACKAGE + "jsp_2_1.xsd",
        PACKAGE + "jsp_2_2.xsd",
        PACKAGE + "web-facesconfig_1_2.xsd",
        PACKAGE + "web-jsptaglibrary_2_1.xsd",
        PACKAGE + "web-app_2_5.xsd",
        PACKAGE + "web-common_3_0.xsd",
        PACKAGE + "web-app_3_0.xsd"
    };


    /**
     * Build a new object for Schemas handling.
     */
    public WebAppSchemas() {
        super();
        addSchemas(WEBAPP_SCHEMAS, WebAppSchemas.class.getClassLoader());
    }

}
