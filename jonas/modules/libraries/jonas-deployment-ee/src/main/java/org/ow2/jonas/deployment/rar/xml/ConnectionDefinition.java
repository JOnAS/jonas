/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A. 
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;
/** 
 * This class defines the implementation of the element connection-definition
 * 
 * @author Eric Hardesty
 */

public class ConnectionDefinition extends AbsElement  {

    /**
     * id
     */ 
    private String id = null;

    /**
     * managedconnectionfactory-class
     */ 
    private String managedconnectionfactoryClass = null;

    /**
     * config-property
     */ 
    private JLinkedList configPropertyList = null;

    /**
     * connectionfactory-interface
     */ 
    private String connectionfactoryInterface = null;

    /**
     * connectionfactory-impl-class
     */ 
    private String connectionfactoryImplClass = null;

    /**
     * connection-interface
     */ 
    private String connectionInterface = null;

    /**
     * connection-impl-class
     */ 
    private String connectionImplClass = null;


    /**
     * Constructor
     */
    public ConnectionDefinition() {
        super();
        configPropertyList = new  JLinkedList("config-property");
    }

    /** 
     * Gets the id
     * @return the id
     */
    public String getId() {
        return id;
    }

    /** 
     * Set the id
     * @param id id object
     */
    public void setId(String id) {
        this.id = id;
    }

    /** 
     * Gets the managedconnectionfactory-class
     * @return the managedconnectionfactory-class
     */
    public String getManagedconnectionfactoryClass() {
        return managedconnectionfactoryClass;
    }

    /** 
     * Set the managedconnectionfactory-class
     * @param managedconnectionfactoryClass managedconnectionfactoryClass
     */
    public void setManagedconnectionfactoryClass(String managedconnectionfactoryClass) {
        this.managedconnectionfactoryClass = managedconnectionfactoryClass;
    }

    /** 
     * Gets the config-property
     * @return the config-property
     */
    public JLinkedList getConfigPropertyList() {
        return configPropertyList;
    }

    /** 
     * Set the config-property
     * @param configPropertyList configProperty
     */
    public void setConfigPropertyList(JLinkedList configPropertyList) {
        this.configPropertyList = configPropertyList;
    }

    /** 
     * Add a new  config-property element to this object
     * @param configProperty the configPropertyobject
     */
    public void addConfigProperty(ConfigProperty configProperty) {
        configPropertyList.add(configProperty);
    }

    /** 
     * Gets the connectionfactory-interface
     * @return the connectionfactory-interface
     */
    public String getConnectionfactoryInterface() {
        return connectionfactoryInterface;
    }

    /** 
     * Set the connectionfactory-interface
     * @param connectionfactoryInterface connectionfactoryInterface
     */
    public void setConnectionfactoryInterface(String connectionfactoryInterface) {
        this.connectionfactoryInterface = connectionfactoryInterface;
    }

    /** 
     * Gets the connectionfactory-impl-class
     * @return the connectionfactory-impl-class
     */
    public String getConnectionfactoryImplClass() {
        return connectionfactoryImplClass;
    }

    /** 
     * Set the connectionfactory-impl-class
     * @param connectionfactoryImplClass connectionfactoryImplClass
     */
    public void setConnectionfactoryImplClass(String connectionfactoryImplClass) {
        this.connectionfactoryImplClass = connectionfactoryImplClass;
    }

    /** 
     * Gets the connection-interface
     * @return the connection-interface
     */
    public String getConnectionInterface() {
        return connectionInterface;
    }

    /** 
     * Set the connection-interface
     * @param connectionInterface connectionInterface
     */
    public void setConnectionInterface(String connectionInterface) {
        this.connectionInterface = connectionInterface;
    }

    /** 
     * Gets the connection-impl-class
     * @return the connection-impl-class
     */
    public String getConnectionImplClass() {
        return connectionImplClass;
    }

    /** 
     * Set the connection-impl-class
     * @param connectionImplClass connectionImplClass
     */
    public void setConnectionImplClass(String connectionImplClass) {
        this.connectionImplClass = connectionImplClass;
    }


    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prefixing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<connection-definition");
        sb.append(xmlAttribute(id, "id"));
        sb.append(">\n");

        indent += 2;

        // managedconnectionfactory-class
        sb.append(xmlElement(managedconnectionfactoryClass, "managedconnectionfactory-class", indent));
        // config-property
        sb.append(configPropertyList.toXML(indent));
        // connectionfactory-interface
        sb.append(xmlElement(connectionfactoryInterface, "connectionfactory-interface", indent));
        // connectionfactory-impl-class
        sb.append(xmlElement(connectionfactoryImplClass, "connectionfactory-impl-class", indent));
        // connection-interface
        sb.append(xmlElement(connectionInterface, "connection-interface", indent));
        // connection-impl-class
        sb.append(xmlElement(connectionImplClass, "connection-impl-class", indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</connection-definition>\n");

        return sb.toString();
    }
}
