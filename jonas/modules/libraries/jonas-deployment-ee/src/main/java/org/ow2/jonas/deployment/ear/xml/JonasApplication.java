/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ear.xml;

import org.ow2.jonas.deployment.common.CommonsSchemas;
import org.ow2.jonas.deployment.common.xml.AbsDescriptionElement;
import org.ow2.jonas.deployment.common.xml.DescriptionGroupXml;
import org.ow2.jonas.deployment.common.xml.JonasSecurity;
import org.ow2.jonas.deployment.common.xml.TopLevelElement;
import org.ow2.jonas.deployment.ear.JonasEarSchemas;



/**
 * This class defines the implementation of the element jonas-application
 * @author Florent Benoit
 */
public class JonasApplication extends AbsDescriptionElement implements TopLevelElement, DescriptionGroupXml {

    /**
     * Header (with right XSD version) for XML
     */
    private String header = null;

    /**
     * Element jonas-security
     */
    private JonasSecurity jonasSecurity = null;

    /**
     * jonas-ejb-jar element XML header
     */
    public static final String JONAS_APPLICATION_ELEMENT = CommonsSchemas.getHeaderForElement("jonas-application",
                                                                                              JonasEarSchemas.getLastSchema());

    /**
     * Constructor
     */
    public JonasApplication() {
        super();

        header = JONAS_APPLICATION_ELEMENT;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        if (header != null) {
            sb.append(header);
        } else {
            sb.append("<jonas-application>\n");
        }
        indent += 2;

        // tenantId
        sb.append(xmlElement(getTenantId(),"tenant-id",indent));

        // jonas-security
        if (jonasSecurity != null) {
            sb.append(jonasSecurity.toXML(indent));
        }

        indent -= 2;
        sb.append(indent(indent));
        sb.append("</jonas-application>\n");

        return sb.toString();
    }

    /**
     * @return the jonasSecurity.
     */
    public JonasSecurity getJonasSecurity() {
        return jonasSecurity;
    }

    /**
     * @param jonasSecurity The jonasSecurity to set.
     */
    public void setJonasSecurity(JonasSecurity jonasSecurity) {
        this.jonasSecurity = jonasSecurity;
    }

    /**
     * @return the header.
     */
    public String getHeader() {
        return header;
    }

    /**
     * @param header The header to set.
     */
    public void setHeader(String header) {
        this.header = header;
    }
}