/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A. 
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;
/** 
 * This class defines the implementation of the element messageadapter
 * 
 * @author Eric Hardesty
 */

public class Messageadapter extends AbsElement  {

    /**
     * authentication-mechanism
     */ 
    private JLinkedList messagelistenerList = null;

    /**
     * Constructor
     */
    public Messageadapter() {
        super();
        messagelistenerList = new  JLinkedList("messagelistener");
    }

    /** 
     * Gets the messagelistener
     * @return the messagelistener
     */
    public JLinkedList getMessagelistenerList() {
        return messagelistenerList;
    }

    /** 
     * Set the messagelistener
     * @param messagelistenerList messagelistener
     */
    public void setMessagelistenerList(JLinkedList messagelistenerList) {
        this.messagelistenerList = messagelistenerList;
    }

    /** 
     * Add a new  messagelistener element to this object
     * @param messagelistener the messagelistenerobject
     */
    public void addMessagelistener(Messagelistener messagelistener) {
        messagelistenerList.add(messagelistener);
    }


    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prefixing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<messageadapter>\n");

        indent += 2;

        // messagelistener
        sb.append(messagelistenerList.toXML(indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</messageadapter>\n");

        return sb.toString();
    }
}
