/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.ee;

import org.ow2.jonas.deployment.api.IMessageDestinationRefDesc;
import org.ow2.jonas.deployment.common.DeploymentDescException;
import org.ow2.jonas.deployment.common.xml.JonasMessageDestinationRef;
import org.ow2.jonas.deployment.common.xml.MessageDestinationRef;


/**
 * This class represents the description of a MessageDestinationRef object
 * @author Eric Hardesty
 */
public class MessageDestinationRefDesc implements IMessageDestinationRefDesc {

    /**
     * The name of the message destination ref
     */
    private String messageDestinationRefName = null;

    /**
     * The type of the message destination ref
     */
    private String messageDestinationType = null;

    /**
     * The usage of the message destination ref.
     */
    private String messageDestinationUsage = null;

    /**
     * The link of the message destination ref.
     */
    private String messageDestinationLink = null;

    /**
     * The jndi name of the message destination ref.
     */
    private String jndiName = null;


    /**
     * Construct a descriptor for an message-destination-ref tag.
     * @param messageDestinationRef the messageDestination ref result of the xml parsing.
     * @param jonasMessageDestinationRef the jonas messageDestinationRef result of the xml parsing.
     * @throws DeploymentDescException when missing information for
     * creating the MessageDestinationRefDesc.
     */
    public MessageDestinationRefDesc(MessageDestinationRef messageDestinationRef,
                                      JonasMessageDestinationRef jonasMessageDestinationRef)
                       throws DeploymentDescException {
        messageDestinationRefName = messageDestinationRef.getMessageDestinationRefName();
        messageDestinationType = messageDestinationRef.getMessageDestinationType();
        messageDestinationUsage = messageDestinationRef.getMessageDestinationUsage();
        messageDestinationLink = messageDestinationRef.getMessageDestinationLink();
        jndiName = null;
        if (jonasMessageDestinationRef != null) {
            jndiName = jonasMessageDestinationRef.getJndiName();
        }
    }

    public String getMessageDestinationRefName() {
        return messageDestinationRefName;
    }

    public String getMessageDestinationType() {
        return messageDestinationType;
    }

    public String getMessageDestinationUsage() {
        return messageDestinationUsage;
    }

    public String getMessageDestinationLink() {
        return messageDestinationLink;
    }


    public String getJndiName() {
        return jndiName;
    }

    /**
     * Set the jndi name of the messageDestination-ref.
     * @param jndiName representation of the JNDI name
     */
    public void setJndiName(String jndiName) {
        this.jndiName = jndiName;
    }


    /**
     * String representation of the object for test purpose
     * @return String representation of this object
     */
    public String toString() {
        StringBuffer ret = new StringBuffer();
        ret.append("\ngetMessageDestinationRefName()=" + getMessageDestinationRefName());
        ret.append("\ngetMessageDestinationType()=" + getMessageDestinationType());
        ret.append("\ngetMessageDestinationUsage()=" + getMessageDestinationUsage());
        ret.append("\ngetMessageDestinationLink()=" + getMessageDestinationLink());
        ret.append("\ngetJndiName()=" + getJndiName());
        return ret.toString();
    }

}
