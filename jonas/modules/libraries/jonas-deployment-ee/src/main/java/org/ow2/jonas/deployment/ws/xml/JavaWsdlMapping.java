/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.ws.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;
import org.ow2.jonas.deployment.common.xml.TopLevelElement;

/**
 * This class defines the implementation of the element java-wsdl-mapping
 *
 * @author JOnAS team
 */

public class JavaWsdlMapping extends AbsElement  implements TopLevelElement {

    /**
     * package-mapping
     */
    private JLinkedList packageMappingList = null;

    /**
     * java-xml-type-mapping
     */
    private JLinkedList javaXmlTypeMappingList = null;

    /**
     * Constructor
     */
    public JavaWsdlMapping() {
        super();
        packageMappingList = new  JLinkedList("package-mapping");
        javaXmlTypeMappingList = new  JLinkedList("java-xml-type-mapping");
    }

    /**
     * Gets the package-mapping
     * @return the package-mapping
     */
    public JLinkedList getPackageMappingList() {
        return packageMappingList;
    }

    /**
     * Set the package-mapping
     * @param packageMappingList packageMapping
     */
    public void setPackageMappingList(JLinkedList packageMappingList) {
        this.packageMappingList = packageMappingList;
    }

    /**
     * Add a new  package-mapping element to this object
     * @param packageMapping the packageMappingobject
     */
    public void addPackageMapping(PackageMapping packageMapping) {
        packageMappingList.add(packageMapping);
    }

    /**
     * Gets the java-xml-type-mapping
     * @return the java-xml-type-mapping
     */
    public JLinkedList getJavaXmlTypeMappingList() {
        return javaXmlTypeMappingList;
    }

    /**
     * Set the java-xml-type-mapping
     * @param javaXmlTypeMappingList javaXmlTypeMapping
     */
    public void setJavaXmlTypeMappingList(JLinkedList javaXmlTypeMappingList) {
        this.javaXmlTypeMappingList = javaXmlTypeMappingList;
    }

    /**
     * Add a new  java-xml-type-mapping element to this object
     * @param javaXmlTypeMapping the javaXmlTypeMappingobject
     */
    public void addJavaXmlTypeMapping(JavaXmlTypeMapping javaXmlTypeMapping) {
        javaXmlTypeMappingList.add(javaXmlTypeMapping);
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<java-wsdl-mapping>\n");
        indent += 2;

        // package-mapping
        sb.append(packageMappingList.toXML(indent));
        // java-xml-type-mapping
        sb.append(javaXmlTypeMappingList.toXML(indent));

        indent -= 2;
        sb.append(indent(indent));
        sb.append("</java-wsdl-mapping>\n");

        return sb.toString();
    }
}
