/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;
import org.ow2.jonas.deployment.common.xml.TopLevelElement;

/**
 * This class defines the implementation of the element jonas-activationspec
 *
 * @author Eric Hardesty
 */

public class JonasActivationspec extends AbsElement  implements TopLevelElement {

    /**
     * id
     */
    private String id = null;

    /**
     * description
     */ 
    private JLinkedList descriptionList = null;

    /**
     * jndi-name
     */
    private String jndiName = null;

    /**
     * defaultAS
     */
    private String defaultAS = null;

    /**
     * Constructor
     */
    public JonasActivationspec() {
        super();
        descriptionList = new  JLinkedList("description");
    }

    /**
     * Gets the id
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Set the id
     * @param id id
     */
    public void setId(String id) {
        this.id = id;
    }

    /** 
     * Gets the description
     * @return the description
     */
    public JLinkedList getDescriptionList() {
        return descriptionList;
    }

    /** 
     * Set the description
     * @param descriptionList description
     */
    public void setDescriptionList(JLinkedList descriptionList) {
        this.descriptionList = descriptionList;
    }

    /** 
     * Add a new  description element to this object
     * @param description the description String
     */
    public void addDescription(String description) {
        descriptionList.add(description);
    }

    /**
     * Gets the jndiname
     * @return the jndiname
     */
    public String getJndiName() {
        return jndiName;
    }

    /**
     * Set the jndiname
     * @param jndiName jndiname
     */
    public void setJndiName(String jndiName) {
        this.jndiName = jndiName;
    }

    /**
     * Gets the defaultAS
     * @return the defaultAS
     */
    public String getDefaultAS() {
        return defaultAS;
    }

    /**
     * Set the defaultAS
     * @param defaultAS defaultAS
     */
    public void setDefaultAS(String defaultAS) {
        this.defaultAS = defaultAS;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prefixing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<jonas-activationspec>\n");

        indent += 2;

        // id
        sb.append(xmlElement(id, "id", indent));
        // description
        sb.append(descriptionList.toXML(indent));
        // jndiname
        sb.append(xmlElement(jndiName, "jndi-name", indent));
        // defaultAS
        sb.append(xmlElement(defaultAS, "defaultAS", indent));

        indent -= 2;
        sb.append(indent(indent));
        sb.append("</jonas-activationspec>\n");

        return sb.toString();
    }
}
