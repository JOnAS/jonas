/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar;

import java.io.Serializable;
import java.util.List;

import org.ow2.jonas.deployment.rar.xml.JonasConnector;



/**
 * This class defines the implementation of the element jonas-connector
 *
 * @author Eric Hardesty
 */

public class JonasConnectorDesc implements Serializable {

    private boolean isSetup = false;

	/**
     * jndi-name
     */
    private String jndiName = null;

    /**
     * rarlink
     */
    private String rarlink = null;

    /**
     * native-lib
     */
    private String nativeLib = null;

    /**
     * log-enabled
     */
    private String logEnabled = null;

    /**
     * log-topic
     */
    private String logTopic = null;

    /**
     * pool-params
     */
    private PoolParamsDesc poolParamsDesc = null;

    /**
     * jdbc-conn-params
     */
    private JdbcConnParamsDesc jdbcConnParamsDesc = null;

    /**
     * tm-params
     */
    private TmParamsDesc tmParamsDesc = null;

    /**
     * jonas-config-property
     */
    private List jonasConfigPropertyList = null;

    /**
     * jonas-connection-definition
     */
    private List jonasConnectionDefinitionList = null;

    /**
     * jonas-activationspec
     */
    private List jonasActivationspecList = null;

    /**
     * jonas-adminobject
     */
    private List jonasAdminobjectList = null;

    /**
     * jonas-security-mapping
     */
    private JonasSecurityMappingDesc jonasSecurityMappingDesc = null;

    /**
     * Constructor
     */
    public JonasConnectorDesc(final JonasConnector jc) {
        if (jc != null) {
            jndiName = jc.getJndiName();
            rarlink = jc.getRarlink();
            nativeLib = jc.getNativeLib();
            logEnabled = jc.getLogEnabled();
            logTopic = jc.getLogTopic();
            poolParamsDesc = new PoolParamsDesc(jc.getPoolParams());
            jdbcConnParamsDesc = new JdbcConnParamsDesc(jc.getJdbcConnParams());
            tmParamsDesc = new TmParamsDesc(jc.getTmParams());

            jonasConfigPropertyList = Utility.jonasConfigProperty(jc.getJonasConfigPropertyList());
            jonasConnectionDefinitionList = Utility.jonasConnectionDefinition(jc.getJonasConnectionDefinitionList());
            jonasActivationspecList = Utility.jonasActivationspec(jc.getJonasActivationspecList());
            jonasAdminobjectList = Utility.jonasAdminobject(jc.getJonasAdminobjectList());
            jonasSecurityMappingDesc = new JonasSecurityMappingDesc(jc.getJonasSecurityMapping());
            isSetup = true;
        }
    }

    /**
     * Gets the jndiname
     * @return the jndiname
     */
    public String getJndiName() {
        return jndiName;
    }

    /**
     * Gets the rarlink
     * @return the rarlink
     */
    public String getRarlink() {
        return rarlink;
    }

    /**
     * Gets the native-lib
     * @return the native-lib
     */
    public String getNativeLib() {
        return nativeLib;
    }

    /**
     * Gets the log-enabled
     * @return the log-enabled
     */
    public String getLogEnabled() {
        return logEnabled;
    }

    /**
     * Gets the log-topic
     * @return the log-topic
     */
    public String getLogTopic() {
        return logTopic;
    }

    /**
     * Gets the pool-params
     * @return the pool-params
     */
    public PoolParamsDesc getPoolParamsDesc() {
        return poolParamsDesc;
    }

    /**
     * Gets the jdbc-conn-params
     * @return the jdbc-conn-params
     */
    public JdbcConnParamsDesc getJdbcConnParamsDesc() {
        return jdbcConnParamsDesc;
    }

    /**
     * Gets the jonas-config-property
     * @return the jonas-config-property
     */
    public List getJonasConfigPropertyList() {
        return jonasConfigPropertyList;
    }

    /**
     * Gets the jonas-connection-definition
     * @return the jonas-connection-definition
     */
    public List getJonasConnectionDefinitionList() {
        return jonasConnectionDefinitionList;
    }

    /**
     * Gets the jonas-activationspec
     * @return the jonas-activationspec
     */
    public List getJonasActivationspecList() {
        return jonasActivationspecList;
    }

    /**
     * Gets the jonas-adminobject
     * @return the jonas-adminobject
     */
    public List getJonasAdminobjectList() {
        return jonasAdminobjectList;
    }

    /**
     * Gets the jonas-security-mapping
     * @return the jonasSecurityMapping
     */
    public JonasSecurityMappingDesc getJonasSecurityMappingDesc() {
        return jonasSecurityMappingDesc;
    }

    /**
     * Gets the tm-params
     * @return the tm-params
     */
    public TmParamsDesc getTmParamsDesc() {
        return tmParamsDesc;
    }

    public boolean isSetup() {
    	return isSetup;
    }
}
