/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A. 
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar;

import java.util.Iterator;
import java.util.List;

import org.ow2.jonas.deployment.common.xml.JLinkedList;
import org.ow2.jonas.deployment.rar.xml.Adminobject;
import org.ow2.jonas.deployment.rar.xml.AuthenticationMechanism;
import org.ow2.jonas.deployment.rar.xml.ConfigProperty;
import org.ow2.jonas.deployment.rar.xml.ConnectionDefinition;
import org.ow2.jonas.deployment.rar.xml.JonasActivationspec;
import org.ow2.jonas.deployment.rar.xml.JonasAdminobject;
import org.ow2.jonas.deployment.rar.xml.JonasConfigProperty;
import org.ow2.jonas.deployment.rar.xml.JonasConnectionDefinition;
import org.ow2.jonas.deployment.rar.xml.Messagelistener;
import org.ow2.jonas.deployment.rar.xml.RequiredConfigProperty;
import org.ow2.jonas.deployment.rar.xml.SecurityPermission;
import org.ow2.jonas.deployment.rar.xml.TmConfigProperty;



/** 
 * This class is a utility class for the rar api deployment classes
 * 
 * @author Eric Hardesty
 */

public class Utility {

    /** 
     * Convert the list
     * @return the updated list
     */
    public static List adminobject(List cp) {
        
        List conv = new JLinkedList("AdminobjectDesc");
        for (Iterator i = cp.iterator(); i.hasNext(); ) {
            conv.add(new AdminobjectDesc((Adminobject) i.next()));
        }
        return conv;
    }

    /** 
     * Convert the list
     * @return the updated list
     */
    public static List authenticationMechanism(List am) {
        List conv = new JLinkedList("AuthenticationMechanismDesc");
        for (Iterator i = am.iterator(); i.hasNext(); ) {
            conv.add(new AuthenticationMechanismDesc((AuthenticationMechanism) i.next()));
        }
        return conv;
    }

    /** 
     * Convert the list
     * @return the updated list
     */
    public static List configProperty(List cp) {
        
        List conv = new JLinkedList("ConfigPropertyDesc");
        for (Iterator i = cp.iterator(); i.hasNext(); ) {
            conv.add(new ConfigPropertyDesc((ConfigProperty) i.next()));
        }
        return conv;
    }

    /** 
     * Convert the list
     * @return the updated list
     */
    public static List connectionDefinition(List cd) {
        List conv = new JLinkedList("ConnectionDefinitionDesc");
        for (Iterator i = cd.iterator(); i.hasNext(); ) {
            conv.add(new ConnectionDefinitionDesc((ConnectionDefinition) i.next()));
        }
        return conv;
    }

    /** 
     * Convert the list
     * @return the updated list
     */
    public static List jonasActivationspec(List jas) {
        List conv = new JLinkedList("JonasActivationspecDesc");
        for (Iterator i = jas.iterator(); i.hasNext(); ) {
            conv.add(new JonasActivationspecDesc((JonasActivationspec) i.next()));
        }
        return conv;
    }

    /** 
     * Convert the list
     * @return the updated list
     */
    public static List jonasAdminobject(List jao) {
        List conv = new JLinkedList("JonasAdminobjectDesc");
        for (Iterator i = jao.iterator(); i.hasNext(); ) {
            conv.add(new JonasAdminobjectDesc((JonasAdminobject) i.next()));
        }
        return conv;
    }

    /** 
     * Convert the list
     * @return the updated list
     */
    public static List jonasConfigProperty(List jcp) {
        List conv = new JLinkedList("JonasConfigPropertyDesc");
        for (Iterator i = jcp.iterator(); i.hasNext(); ) {
            conv.add(new JonasConfigPropertyDesc((JonasConfigProperty) i.next()));
        }
        return conv;
    }

    /** 
     * Convert the list
     * @return the updated list
     */
    public static List jonasConnectionDefinition(List jcd) {
        List conv = new JLinkedList("JonasConnectionDefinitionDesc");
        for (Iterator i = jcd.iterator(); i.hasNext(); ) {
            conv.add(new JonasConnectionDefinitionDesc((JonasConnectionDefinition) i.next()));
        }
        return conv;
    }

    /** 
     * Convert the list
     * @return the updated list
     */
    public static List messagelistener(List ml) {
        List conv = new JLinkedList("MessagelistenerDesc");
        for (Iterator i = ml.iterator(); i.hasNext(); ) {
            conv.add(new MessagelistenerDesc((Messagelistener) i.next()));
        }
        return conv;
    }
    /** 
     * Convert the list
     * @return the updated list
     */
    public static List requiredConfigProperty(List rcp) {
        
        List conv = new JLinkedList("RequiredConfigPropertyDesc");
        for (Iterator i = rcp.iterator(); i.hasNext(); ) {
            conv.add(new RequiredConfigPropertyDesc((RequiredConfigProperty) i.next()));
        }
        return conv;
    }

    /** 
     * Convert the list
     * @return the updated list
     */
    public static List securityPermission(List cp) {
        
        List conv = new JLinkedList("SecurityPermission");
        for (Iterator i = cp.iterator(); i.hasNext(); ) {
            conv.add(new SecurityPermissionDesc((SecurityPermission) i.next()));
        }
        return conv;
    }

    /** 
     * Convert the list
     * @return the updated list
     */
    public static List tmConfigProperty(List cp) {
        
        List conv = new JLinkedList("TmConfigPropertyDesc");
        for (Iterator i = cp.iterator(); i.hasNext(); ) {
            conv.add(new TmConfigPropertyDesc((TmConfigProperty) i.next()));
        }
        return conv;
    }

}
