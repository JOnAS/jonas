/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A. 
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;
/** 
 * This class defines the implementation of the element outbound-resourceadapter
 * 
 * @author Eric Hardesty
 */

public class OutboundResourceadapter extends AbsElement  {

    /**
     * connection-definition
     */ 
    private JLinkedList connectionDefinitionList = null;

    /**
     * transaction-support
     */ 
    private String transactionSupport = null;

    /**
     * authentication-mechanism
     */ 
    private JLinkedList authenticationMechanismList = null;

    /**
     * reauthentication-support
     */ 
    private String reauthenticationSupport = null;


    /**
     * Constructor
     */
    public OutboundResourceadapter() {
        super();
        connectionDefinitionList = new  JLinkedList("connection-definition");
        authenticationMechanismList = new  JLinkedList("authentication-mechanism");
    }

    /** 
     * Gets the connection-definition
     * @return the connection-definition
     */
    public JLinkedList getConnectionDefinitionList() {
        return connectionDefinitionList;
    }

    /** 
     * Set the connection-definition
     * @param connectionDefinitionList connectionDefinition
     */
    public void setConnectionDefinitionList(JLinkedList connectionDefinitionList) {
        this.connectionDefinitionList = connectionDefinitionList;
    }

    /** 
     * Add a new  connection-definition element to this object
     * @param connectionDefinition the connectionDefinitionobject
     */
    public void addConnectionDefinition(ConnectionDefinition connectionDefinition) {
        connectionDefinitionList.add(connectionDefinition);
    }

    /** 
     * Gets the transaction-support
     * @return the transaction-support
     */
    public String getTransactionSupport() {
        return transactionSupport;
    }

    /** 
     * Set the transaction-support
     * @param transactionSupport transactionSupport
     */
    public void setTransactionSupport(String transactionSupport) {
        this.transactionSupport = transactionSupport;
    }

    /** 
     * Gets the authentication-mechanism
     * @return the authentication-mechanism
     */
    public JLinkedList getAuthenticationMechanismList() {
        return authenticationMechanismList;
    }

    /** 
     * Set the authentication-mechanism
     * @param authenticationMechanismList authenticationMechanism
     */
    public void setAuthenticationMechanismList(JLinkedList authenticationMechanismList) {
        this.authenticationMechanismList = authenticationMechanismList;
    }

    /** 
     * Add a new  authentication-mechanism element to this object
     * @param authenticationMechanism the authenticationMechanismobject
     */
    public void addAuthenticationMechanism(AuthenticationMechanism authenticationMechanism) {
        authenticationMechanismList.add(authenticationMechanism);
    }

    /** 
     * Gets the reauthentication-support
     * @return the reauthentication-support
     */
    public String getReauthenticationSupport() {
        return reauthenticationSupport;
    }

    /** 
     * Set the reauthentication-support
     * @param reauthenticationSupport reauthenticationSupport
     */
    public void setReauthenticationSupport(String reauthenticationSupport) {
        this.reauthenticationSupport = reauthenticationSupport;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prefixing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<outbound-resourceadapter>\n");

        indent += 2;

        // connection-definition
        sb.append(connectionDefinitionList.toXML(indent));
        // transaction-support
        sb.append(xmlElement(transactionSupport, "transaction-support", indent));
        // authentication-mechanism
        sb.append(authenticationMechanismList.toXML(indent));
        // reauthentication-support
        sb.append(xmlElement(reauthenticationSupport, "reauthentication-support", indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</outbound-resourceadapter>\n");

        return sb.toString();
    }
}
