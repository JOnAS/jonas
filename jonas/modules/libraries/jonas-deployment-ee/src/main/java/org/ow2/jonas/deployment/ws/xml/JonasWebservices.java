/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.ws.xml;

import org.ow2.jonas.deployment.common.CommonsSchemas;
import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;
import org.ow2.jonas.deployment.common.xml.TopLevelElement;
import org.ow2.jonas.deployment.rar.JonasConnectorSchemas;
import org.ow2.jonas.deployment.ws.JonasWsSchemas;


/**
 * This class defines the implementation of the element jonas-webservices
 *
 * @author JOnAS team
 */

public class JonasWebservices extends AbsElement  implements TopLevelElement {

    /**
     * Header (with right XSD version) for XML
     */
    private String header = null;

    /**
     * war
     */
    private String war = null;

    /**
     * context-root
     */
    private String contextRoot = null;

    /**
     * jonas-webservice-description
     */
    private JLinkedList jwsDescList = null;

    /**
     * jonas-webservices element XML header
     */
    public static final String JONAS_WEBSERVICES_ELEMENT = CommonsSchemas.getHeaderForElement("jonas-webservices",
                                                                                              JonasWsSchemas.getLastSchema());

    /**
     * Constructor
     */
    public JonasWebservices() {
        super();
        jwsDescList = new JLinkedList("jonas-webservice-description");
    }

    /**
     * Gets the war
     * @return the war
     */
    public String getWar() {
        return war;
    }

    /**
     * Set the war
     * @param war war
     */
    public void setWar(String war) {
        this.war = war;
    }

    /**
     * @return Returns the contextRoot.
     */
    public String getContextRoot() {
        return contextRoot;
    }

    /**
     * @param contextRoot The contextRoot to set.
     */
    public void setContextRoot(String contextRoot) {
        this.contextRoot = contextRoot;
    }

    /**
     * Gets the jonas-webservice-description
     * @return the jonas-webservice-description
     */
    public JLinkedList getJonasWebserviceDescriptionList() {
        return jwsDescList;
    }


    /**
     * Add a new  jonas-webservice-description element to this object
     * @param jwsd the jonas-webservice-description
     */
    public void addJonasWebserviceDescription(JonasWebserviceDescription jwsd) {
        jwsDescList.add(jwsd);
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append(JONAS_WEBSERVICES_ELEMENT);

        indent += 2;

        // war
        if (war != null) {
            sb.append(xmlElement(war, "war", indent));
        }
        // context-root
        if (contextRoot != null) {
            sb.append(xmlElement(contextRoot, "context-root", indent));
        }
        // jonas-webservice-description
        sb.append(getJonasWebserviceDescriptionList().toXML(indent));

        indent -= 2;
        sb.append(indent(indent));
        sb.append("</jonas-webservices>\n");

        return sb.toString();
    }
    /**
     * @return the header.
     */
    public String getHeader() {
        return header;
    }

    /**
     * @param header The header to set.
     */
    public void setHeader(String header) {
        this.header = header;
    }

}
