/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A. 
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.rar.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;
import org.ow2.jonas.deployment.common.xml.TopLevelElement;

/** 
 * This class defines the implementation of the element connector
 * 
 * @author Florent Benoit
 */

public class Connector extends AbsElement implements TopLevelElement {

    /**
     * display-name
     */ 
    private String displayName = null;

    /**
     * description
     */ 
    private JLinkedList descriptionList = null;

    /**
     * icon
     */ 
    private Icon icon = null;

    /**
     * vendor-name
     */ 
    private String vendorName = null;

    /**
     * spec-version
     */ 
    private String specVersion = null;

    /**
     * eis-type
     */ 
    private String eisType = null;

    /**
     * version
     */ 
    private String version = null;

    /**
     * resourceadapter-version
     */ 
    private String resourceadapterVersion = null;

    /**
     * license
     */ 
    private License license = null;

    /**
     * resourceadapter
     */ 
    private Resourceadapter resourceadapter = null;


    /**
     * Constructor
     */
    public Connector() {
        super();
        descriptionList = new  JLinkedList("description");
    }

    /** 
     * Gets the display-name
     * @return the display-name
     */
    public String getDisplayName() {
        return displayName;
    }

    /** 
     * Set the display-name
     * @param displayName displayName
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /** 
     * Gets the description
     * @return the description
     */
    public JLinkedList getDescriptionList() {
        return descriptionList;
    }

    /** 
     * Set the description
     * @param descriptionList description
     */
    public void setDescriptionList(JLinkedList descriptionList) {
        this.descriptionList = descriptionList;
    }

    /** 
     * Add a new  description element to this object
     * @param description the description String
     */
    public void addDescription(String description) {
        descriptionList.add(description);
    }


    /** 
     * Gets the icon
     * @return the icon
     */
    public Icon getIcon() {
        return icon;
    }

    /** 
     * Set the icon
     * @param icon icon
     */
    public void setIcon(Icon icon) {
        this.icon = icon;
    }


    /** 
     * Gets the vendor-name
     * @return the vendor-name
     */
    public String getVendorName() {
        return vendorName;
    }

    /** 
     * Set the vendor-name
     * @param vendorName vendorName
     */
    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    /** 
     * Gets the spec-version
     * @return the spec-version
     */
    public String getSpecVersion() {
        return specVersion;
    }

    /** 
     * Set the spec-version
     * @param specVersion specVersion
     */
    public void setSpecVersion(String specVersion) {
        this.specVersion = specVersion;
    }

    /** 
     * Gets the eis-type
     * @return the eis-type
     */
    public String getEisType() {
        return eisType;
    }

    /** 
     * Set the eis-type
     * @param eisType eisType
     */
    public void setEisType(String eisType) {
        this.eisType = eisType;
    }

    /** 
     * Gets the version
     * @return the version
     */
    public String getVersion() {
        return version;
    }

    /** 
     * Set the version
     * @param version version
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /** 
     * Gets the resourceadapter-version
     * @return the resourceadapter-version
     */
    public String getResourceadapterVersion() {
        return resourceadapterVersion;
    }

    /** 
     * Set the resourceadapter-version
     * @param resourceadapterVersion resourceadapterVersion
     */
    public void setResourceadapterVersion(String resourceadapterVersion) {
        this.resourceadapterVersion = resourceadapterVersion;
    }

    /** 
     * Gets the license
     * @return the license
     */
    public License getLicense() {
        return license;
    }

    /** 
     * Set the license
     * @param license license
     */
    public void setLicense(License license) {
        this.license = license;
    }

    /** 
     * Gets the resourceadapter
     * @return the resourceadapter
     */
    public Resourceadapter getResourceadapter() {
        return resourceadapter;
    }

    /** 
     * Set the resourceadapter
     * @param resourceadapter resourceadapter
     */
    public void setResourceadapter(Resourceadapter resourceadapter) {
        this.resourceadapter = resourceadapter;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prefixing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        sb.append("<connector");
        sb.append(xmlAttribute(specVersion, "version"));
        sb.append(">\n");

        indent += 2;

        // description
        sb.append(descriptionList.toXML(indent));
        // display-name
        sb.append(xmlElement(displayName, "display-name", indent));
        // icon
        if (icon != null) {
            sb.append(icon.toXML(indent));
        }
        // vendor-name
        sb.append(xmlElement(vendorName, "vendor-name", indent));
        // eis-type
        sb.append(xmlElement(eisType, "eis-type", indent));
        // version
        sb.append(xmlElement(resourceadapterVersion, "resourceadapter-version", indent));
        // license
        if (license != null) {
            sb.append(license.toXML(indent));
        }
        // resourceadapter
        if (resourceadapter != null) {
            sb.append(resourceadapter.toXML(indent));
        }
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</connector>\n");

        return sb.toString();
    }
}
