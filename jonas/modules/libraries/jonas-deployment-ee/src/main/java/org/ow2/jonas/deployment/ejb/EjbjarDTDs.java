/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Philippe Coq
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.ejb;


import org.ow2.jonas.deployment.common.CommonsDTDs;
import org.ow2.jonas.deployment.common.util.ResourceHelper;

/**
 * This class defines the declarations of DTDs for ejbjar.xml.
 * @author Philippe Coq
 */
public class EjbjarDTDs extends CommonsDTDs {

    /**
     * Package name.
     */
    private static final String PACKAGE = ResourceHelper.getResourcePackage(EjbjarDTDs.class);

    /**
     * List of ejb-jar dtds.
     */
    private static final String[] EJBJAR_DTDS = new String[] {
        PACKAGE + "ejb-jar_1_1.dtd",
        PACKAGE + "ejb-jar_2_0.dtd"
    };

    /**
     * List of jonas-ejb-jar  publicId.
     */
    private static final String[] EJBJAR_DTDS_PUBLIC_ID = new String[] {
        "-//Sun Microsystems, Inc.//DTD Enterprise JavaBeans 1.1//EN",
        "-//Sun Microsystems, Inc.//DTD Enterprise JavaBeans 2.0//EN",
    };




    /**
     * Build a new object for web.xml DTDs handling.
     */
    public EjbjarDTDs() {
        super();
        addMapping(EJBJAR_DTDS, EJBJAR_DTDS_PUBLIC_ID, EjbjarDTDs.class.getClassLoader());
    }

}
