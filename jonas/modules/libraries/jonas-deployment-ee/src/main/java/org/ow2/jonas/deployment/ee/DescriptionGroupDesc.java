/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s):  Philippe Coq
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.deployment.ee;

// SAX imports
import org.ow2.jonas.deployment.api.IDescriptionGroupDesc;
import org.xml.sax.SAXParseException;


/**
 * This class is used to keep the usage of the contained description related
 * elements consistent acoross J2EE deployment descriptors.
 * Elements are "description", "display-name", "icon"
 * used by application, connector, ejb-jar, webservices.
 *
 * @author Philippe Coq
 *
 */

public abstract class DescriptionGroupDesc implements IDescriptionGroupDesc {

    /**
     * The description field
     */
    protected String description = null;

    /**
     * The displayName field
     */
    protected String displayName = null;

    /**
     * The small-icon field
     */
    protected String smallIcon = null;

    /**
     * The large-icon field
     */
    protected String largeIcon = null;

    /**
     * Get the display name of the deployment descriptor.
     * @return the display name of the deployment descriptor.
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Get the description of the deployment descriptor.
     * @return the description of the deployment descriptor.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Get the small-icon of the deployment descriptor.
     * @return the small-icon of the deployment descriptor.
     */
    public String getSmallIcon() {
        return smallIcon;
    }

    /**
     * Get the large-icon of the deployment descriptor.
     * @return the large-icon of the deployment descriptor.
     */
    public String getLargeIcon() {
        return largeIcon;
    }

    /**
     * Return a String representation of the DeploymentDesc.
     * @return a String representation of the DeploymentDesc.
     */
    public abstract String toString();

    /**
     * build a message from SAX Exception in a consistent style, consistent
     * with emacs compile mode (same as grep, cc, javac, etc).
     * @param fileName name of the file
     * @param exception the SAX exception
     * @param msg the string message
     * @return a message which is in a consistent style
     */
    protected  static String getSAXMsg(String fileName,
                                       SAXParseException exception, String msg) {
        String ret = fileName + ":" + exception.getLineNumber() + ":"
            + exception.getColumnNumber() + ": ";
        ret += msg;
        return ret;
    }

}
