/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.web.rules;

import org.ow2.jonas.deployment.common.rules.InitParamRuleSet;
import org.ow2.jonas.deployment.common.rules.JRuleSetBase;
import org.ow2.jonas.deployment.common.rules.RunAsRuleSet;
import org.ow2.jonas.deployment.common.rules.SecurityRoleRefRuleSet;

import org.apache.commons.digester.Digester;

/**
 * This class defines a rule to analyze servlet
 * @author Florent Benoit
 */
public class ServletRuleSet extends JRuleSetBase {


    /**
     * Construct an object with a specific prefix
     * @param prefix prefix to use during the parsing
     */
    public ServletRuleSet(String prefix) {
        super(prefix);
    }


    /**
     * Add a set of rules to the digester object
     * @param digester Digester instance
     */
    public void addRuleInstances(Digester digester) {
        digester.addObjectCreate(prefix + "servlet",
                                 "org.ow2.jonas.deployment.web.xml.Servlet");
        digester.addSetNext(prefix + "servlet",
                            "addServlet",
                            "org.ow2.jonas.deployment.web.xml.Servlet");

        digester.addCallMethod(prefix + "servlet/servlet-name",
                               "setServletName", 0);

        digester.addCallMethod(prefix + "servlet/servlet-class",
                               "setServletClass", 0);

        digester.addCallMethod(prefix + "servlet/jsp-file",
                               "setJspFile", 0);

        digester.addRuleSet(new InitParamRuleSet(prefix + "servlet"));
        digester.addRuleSet(new RunAsRuleSet(prefix + "servlet/"));
        digester.addRuleSet(new SecurityRoleRefRuleSet(prefix + "servlet/"));


    }


}


