/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.web.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;

/**
 * This class defines the implementation of the element user-data-constraint
 * @author Florent Benoit
 */
public class UserDataConstraint extends AbsElement {

    /**
     * description
     */
    private JLinkedList descriptionList = null;

    /**
     * transport-guarantee
     */
    private String transportGuarantee = null;


    /**
     * Constructor
     */
    public UserDataConstraint() {
        super();
        descriptionList = new  JLinkedList("description");
    }


    // Setters

    /**
     * Add a new description element to this object
     * @param description description
     */
    public void addDescription(String description) {
        descriptionList.add(description);
    }

    /**
     * Add a new transport-guarantee element to this object
     * @param transportGuarantee transport-guarantee
     */
    public void setTransportGuarantee(String transportGuarantee) {
        this.transportGuarantee = transportGuarantee;
    }

    // Getters

    /**
     * Gets the description list
     * @return the description list
     */
    public JLinkedList getDescriptionList() {
        return descriptionList;
    }

    /**
     * Gets the transport-guarantee
     * @return the transport-guarantee
     */
    public String getTransportGuarantee() {
        return transportGuarantee;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<user-data-constraint>\n");

        indent += 2;
        // description
        sb.append(descriptionList.toXML(indent));

        // transport-guarantee
        if (transportGuarantee != null) {
            sb.append(xmlElement(transportGuarantee, "transport-guarantee", indent));
        }

        indent -= 2;
        sb.append(indent(indent));
        sb.append("</user-data-constraint>\n");

        return sb.toString();
    }

}
