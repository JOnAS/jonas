/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id:JMetaData.java 11039 2007-07-26 13:35:52Z durieuxp $
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.svc;

import java.io.Serializable;

import javax.ejb.EJBException;
import javax.ejb.EJBHome;
import javax.ejb.EJBMetaData;

import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * This class is a Serializable class that allows a client to obtain the
 * enterprise Bean's meta-data information.
 * @author Philippe Coq
 */
public class JMetaData implements EJBMetaData, Serializable {

    /**
     * JDK logger to be portable
     */
    private static Logger logger = Logger.getLogger("org.ow2.jonas.lib.svc");

    /**
     * EJB Home
     */
    private EJBHome home;

    /**
     * EJB Home class
     */
    private Class homeClass;

    /**
     * EJB Remote class
     */
    private Class remoteClass;

    /**
     * Primary Key Home class
     */
    private Class primaryKeyClass;

    /**
     * True if is a Session Bean
     */
    private boolean isSession;

    /**
     * True if is a Stateless Session Bean
     */
    private boolean isStatelessSession;

    /**
     * Constructor
     * @param home The EJBHome
     * @param homeClass EJB Home class
     * @param remoteClass EJB Remote class
     * @param isSession true if is a Session Bean
     * @param isStatelessSession true if is a StatelessSessionBean
     * @param primaryKeyClass primary key class
     */
    public JMetaData(EJBHome home, Class homeClass, Class remoteClass, boolean isSession, boolean isStatelessSession, Class primaryKeyClass) {
        logger.log(Level.FINE, "");
        this.home = home;
        this.homeClass = homeClass;
        this.remoteClass = remoteClass;
        this.isSession = isSession;
        this.isStatelessSession = isStatelessSession;
        this.primaryKeyClass = primaryKeyClass;
    }

    // -----------------------------------------------------------------------
    // EJBMetaData implementation
    // -----------------------------------------------------------------------

    /**
     * @return the home interface of the enterprise Bean.
     */
    public EJBHome getEJBHome() {
        return home;
    }

    /**
     * @return the Class object for the enterprise Bean's home interface.
     */
    public Class getHomeInterfaceClass() {
        return homeClass;
    }

    /**
     * @return the Class object for the enterprise Bean's primary key class.
     */
    public Class getPrimaryKeyClass() {
        if (isSession) {
            throw new EJBException("getPrimaryKeyClass() not allowed for session");
        }
        return primaryKeyClass;
    }

    /**
     * @return the Class object for the enterprise Bean's remote interface.
     */
    public Class getRemoteInterfaceClass() {
        return remoteClass;
    }

    /**
     * @return True if the enterprise Bean's type is "session".
     */
    public boolean isSession() {
        return isSession;
    }

    /**
     * @return True if the type of the enterprise Bean is stateless session.
     */
    public boolean isStatelessSession() {
        return isStatelessSession;
    }

}