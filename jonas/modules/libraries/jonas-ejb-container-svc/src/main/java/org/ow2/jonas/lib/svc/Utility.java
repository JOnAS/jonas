/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.svc;

import javax.ejb.spi.HandleDelegate;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

import org.omg.CORBA.ORB;


/**
 * Utility class for System Value Classes.
 * @author pelletib
 */
public final class Utility  {

    /**
     * orb instance.
     */
    private static ORB orb = null;

    /**
     * Handledelegate.
     */
    private static HandleDelegate hdlDel = null;

    /**
     * private constructor.
     */
    private Utility() {
    }

    /**
     * Get the Handle Delegate implementation, registered in java:comp.
     * @return The HandleDelegate object.
     * @throws NamingException Cannot get the HandleDelegate
     */
    public static HandleDelegate getHandleDelegate() throws NamingException {
        if (hdlDel == null) {
            InitialContext ictx = new InitialContext();
            hdlDel = (HandleDelegate) ictx.lookup("java:comp/HandleDelegate");
        }
        return hdlDel;
     }
    /**
     * Get the Handle Delegate implementation, registered in java:comp.
     * @return The HandleDelegate object.
     * @throws NamingException Cannot get the HandleDelegate
     */
    public static ORB getORB() throws NamingException {
        InitialContext ictx = new InitialContext();
        orb = (ORB) PortableRemoteObject.narrow(ictx.lookup("java:comp/ORB"), ORB.class);
        return orb;
    }
}
