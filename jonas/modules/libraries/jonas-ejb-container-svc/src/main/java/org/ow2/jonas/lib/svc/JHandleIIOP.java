/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id:JHandleIIOP.java 11039 2007-07-26 13:35:52Z durieuxp $
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.svc;

import java.io.IOException;
import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJBObject;
import javax.ejb.Handle;
import javax.ejb.spi.HandleDelegate;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;
import javax.rmi.CORBA.Util;

import org.omg.PortableServer.Servant;
/**
 * This class implements javax.ejb.Handle interface.
 * @author Philippe Coq
 */
public class JHandleIIOP implements Handle, Serializable {

    /**
     * JDK logger to be portable.
     */
    private static Logger logger = Logger.getLogger("org.ow2.jonas.lib.svc");

    /**
     * CORBA ref.
     * @serial
     */
    private String ior = null;

   /**
     * constructor.
     * @param r remote ref
     */
    public JHandleIIOP(final Remote r) {
        try {
            if (logger.isLoggable(Level.FINE)) {
                logger.log(Level.FINE, "r=" + r);
            }
            Servant servant = (Servant) Util.getTie(r);
            org.omg.CORBA.Object o = servant._this_object();
            this.ior = Utility.getORB().object_to_string(o);
            if (logger.isLoggable(Level.FINE)) {
                logger.log(Level.FINE, "ior=" + ior);
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, "cannot get Handle: ", e);
        }
    }

    /**
     * Obtains the EJB object represented by this handle.
     * @return The EJB object
     * @throws RemoteException The EJB object could not be obtained because of a
     *         system-level failure.
     */
    public EJBObject getEJBObject() throws RemoteException {
        logger.log(Level.FINE, "");
        try {
            return (EJBObject) PortableRemoteObject.narrow(Utility.getORB().string_to_object(ior), EJBObject.class);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Cannot narrow ior '" + ior + "'.", e);
            throw new RemoteException("Cannot narrow ior '" + ior + "'.", e);
        }
    }

    /**
     * Specific implementation of serialization.
     * Must call HandleDelegate.writeEJBObject, as specified in 19.5.5.1 of spec EJB 2.1
     * @param out The output stream used to write object
     * @throws IOException error when writing object.
     */
    private void writeObject(final java.io.ObjectOutputStream out)
    throws IOException {
        HandleDelegate hdld;
        logger.log(Level.FINE, "");
        try {
            hdld = Utility.getHandleDelegate();
        } catch (NamingException e) {
            logger.log(Level.SEVERE, "Cannot get HandleDelegate", e);
            throw new IOException("Cannot get HandleDelegate");
        }
        hdld.writeEJBObject(getEJBObject(), out);
    }

    /**
     * Specific implementation of deserialization.
     * Must call HandleDelegate.readEJBObject, as specified in 19.5.5.1 of spec EJB 2.1
     * @param in The input Stream from where is read the object.
     * @throws IOException error when reading object.
     * @throws ClassNotFoundException -
     */
    private void readObject(final java.io.ObjectInputStream in)
    throws IOException, ClassNotFoundException {
        HandleDelegate hdld;
        logger.log(Level.FINE, "");
        try {
            hdld = Utility.getHandleDelegate();
        } catch (NamingException e) {
            logger.log(Level.SEVERE, "Cannot get HandleDelegate", e);
            throw new IOException("Cannot get HandleDelegate");
        }
        EJBObject obj = hdld.readEJBObject(in);
        try {
            this.ior = Utility.getORB().object_to_string((org.omg.CORBA.Object) obj);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "JHandle.readObject()", e);
            throw new RemoteException("JHandle.readObject(): " + e, e);
        }
    }

}

