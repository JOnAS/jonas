/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Guillaume SAUTHIER
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.loader.factory;

import java.net.URL;
import java.io.IOException;

/**
 * A <code>JarURLFactory</code> is used to create URLs
 * from a JarURL (jar:XXX) and paths.
 * Example : <br/>
 * base : <code>file:lib.jar</code><br/>
 * path : <code>META-INF/directory/</code><br/>
 * results : <code>jar:file:lib.jar!/META-INF/directory/</code><br/>
 *
 * @author Guillaume Sauthier
 */
public class JarURLFactory extends URLFactory {

    /** the base URL */
    private URL base;

    /** Jar URl prefix */
    private static final String JAR = "jar:";

    /**
     * Jar URL separator
     */
    private static final String SEP = "!/";

    /**
     * Create a new JarURLFactory using the specified base URL.
     *
     * @param url the base url. (must be a Jar archive)
     */
    public JarURLFactory(URL url) {
        base = url;
    }

    /**
     * Returns a new URL basically adding path to the base URL.
     * returns the base URL when "" is used as parameter.
     *
     * @param path the path to add to the URL. (must not start with "/")
     *
     * @return a new URL of the form jar:<base>!/<path>.
     *
     * @throws IOException when created URL is invalid.
     */
    public URL getURL(String path) throws IOException {
        if (path.equals("")) {
            return base;
        } else {
            return new URL(JAR + base + SEP + path);
        }
    }

}
