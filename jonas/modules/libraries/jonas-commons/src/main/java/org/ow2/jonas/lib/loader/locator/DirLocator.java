/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Guillaume SAUTHIER
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.lib.loader.locator;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Vector;

import org.ow2.util.url.URLUtils;

/**
 * A <code>DirLocator</code> is used to look up for a file
 * inside a directory.
 *
 * @author Guillaume Sauthier
 */
public class DirLocator extends Locator {

    /**
     * Wrapped File where search will be performed.
     */
    private File file = null;

    /**
     * Construct a new DirLocator from an URL pointing to a directory.
     *
     * @param jar URL pointing to a directory.
     *
     * @throws IOException When
     */
    public DirLocator(final URL jar) throws IOException {
        this.file = URLUtils.urlToFile(jar);

        if (!file.exists()) {
            throw new IOException("File " + file + " does not exists.");
        }
        if (!file.isDirectory()) {
            throw new IOException("File " + file + " is not a directory.");
        }
    }

    /**
     * Returns true when file was found.
     *
     * @param path the path to the file to look up
     *
     * @return true when file was found, otherwise false.
     */
    @Override
    public boolean hasFile(final String path) {

        File child = new File(file, path);
        return (child.exists() && child.isFile());
    }

    /**
     * Returns true when directory was found.
     *
     * @param path the path to the directory to look up
     *
     * @return true when directory was found, otherwise false.
     */
    @Override
    public boolean hasDirectory(final String path) {

        File child = new File(file, path);
        return (child.exists() && child.isDirectory());
    }

    /**
     * Returns a list of filename stored in path.
     *
     * @param path the path to the directory where looking for files
     *
     * @return a list of filename stored in path.
     */
    @Override
    public List listContent(final String path) {

        File child = new File(file, path);
        List libs = new Vector();
        // List directory content
        if (child.isDirectory()) {
            addContent(child, libs);
        }

        return libs;
    }

    /**
     * Add only files in the given List.
     * Recursive!
     *
     * @param f base directory to explore
     * @param l file list to be filled
     */
    private void addContent(final File f, final List l) {
        File[] childs = f.listFiles();
        if (childs != null) {
            for (int i = 0; i < childs.length; i++) {
                if (childs[i].isDirectory()) {
                    addContent(childs[i], l);
                } else if (childs[i].isFile()) {
                    l.add(childs[i].getPath().substring(file.getPath().length()));
                }
            }
        }
    }

}
