/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Guillaume SAUTHIER
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.loader;

import java.io.IOException;
import java.net.URL;

/**
 * ClassLoader used for loading EJB classes and resources
 *
 * @author Guillaume Sauthier
 */
public class EjbJarClassLoader extends AbsModuleClassLoader {

    /** classes are located in the module root */
    private static final String CLASSES_DIRECTORY = "";

    /** WSDL files are located in META-INF/wsdl directory */
    private static final String WSDL_DIRECTORY = "META-INF/wsdl/";

    /**
     * Create a new EjbJarClassLoader with default parent ClassLoader
     *
     * @param modules an URL[] of EjbJar files
     *
     * @throws IOException if creation fails
     */
    public EjbJarClassLoader(URL[] modules) throws IOException {
        super(modules);
    }

    /**
     * Create a new EjbJarClassLoader with specified parent ClassLoader
     *
     * @param modules an URL[] of EjbJar files
     * @param parent the parent ClassLoader to use
     *
     * @throws IOException if creation fails
     */
    public EjbJarClassLoader(URL[] modules, ClassLoader parent) throws IOException {
        super(modules, parent);
    }

    /**
     * Add the directory and the content of META-INF/wsdl/ in the ClassLoader
     *
     * @throws IOException When WEB-INF/classes and WEB-INF/wsdl directories cannot be added
     *         in loader
     */
    protected void init() throws IOException {
        super.init();
        addInRepository(CLASSES_DIRECTORY);
        addInRepository(WSDL_DIRECTORY);
    }
}