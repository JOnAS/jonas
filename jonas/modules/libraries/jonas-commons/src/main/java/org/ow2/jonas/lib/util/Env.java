/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): ____________________________________.
 * Contributor(s): ______________________________________.
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.util;

/**
 * This class manages global variables and properties used by EJB Server. A
 * static design pattern is used.
 */
public class Env {

    /**
     * No public constructor : utility class
     */
    private Env() {

    }

    // Variables related to Java Version
    public static final int JAVA_1_1_6 = 116;

    public static final int JAVA_1_1_7 = 117;

    public static final int JAVA_1_1_8 = 118;

    public static final int JAVA_1_2 = 120;

    public static final int JAVA_1_3 = 130;

    public static final int JAVA_1_4 = 140;

    private static int javaVersion = -1;

    /**
     * @return true if the os.name starts with "Windows"
     */
    public static boolean isOsWindows() {
        String osName = System.getProperty("os.name", "");
        return (osName.startsWith("Windows"));
    }

    /**
     * @return true if the os.name starts with "Mac OS X"
     */
    public static boolean isOsMacOsX() {
        String osName = System.getProperty("os.name", "");
        return (osName.startsWith("Mac OS X"));
    }

    /**
     * Gets Java Version.
     * @return javaVersion or -1 if error
     */
    public static int getJavaVersion() {

        if (javaVersion == -1) {
            // Sets Java Version
            String strjv = System.getProperty("java.version", "");
            if (strjv.indexOf("1.1.6") == 0) {
                javaVersion = JAVA_1_1_6;
            }
            if (strjv.indexOf("1.1.7") == 0) {
                javaVersion = JAVA_1_1_7;
            }
            if (strjv.indexOf("1.1.8") == 0) {
                javaVersion = JAVA_1_1_8;
            }
            if (strjv.indexOf("1.2") == 0) {
                javaVersion = JAVA_1_2;
            }
            if (strjv.indexOf("1.3") == 0) {
                javaVersion = JAVA_1_3;
            }
            if (strjv.indexOf("1.4") == 0) {
                javaVersion = JAVA_1_4;
            }
        }
        return javaVersion;
    }

    /**
     * @return true if JDK 1.2 or later
     */
    public static boolean isJAVA2() {
        return (getJavaVersion() >= JAVA_1_2);
    }

    /**
     * @return true if JDK 1.4 or later
     */
    public static boolean isJAVA4() {
        return (getJavaVersion() >= JAVA_1_4);
    }
}

