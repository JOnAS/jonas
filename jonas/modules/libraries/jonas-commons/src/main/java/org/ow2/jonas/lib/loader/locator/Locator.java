/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Guillaume SAUTHIER
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.loader.locator;

import java.io.IOException;
import java.net.URL;
import java.util.List;

/**
 * A <code>Locator</code> is used to hide System specific
 * when looking for a file.
 *
 * @author Guillaume Sauthier
 */
public abstract class Locator {

    /**
     * Returns true when file was found.
     *
     * @param path the path to the file to look up
     *
     * @return true when file was found, otherwise false.
     */
    public abstract boolean hasFile(String path);

    /**
     * Returns true when directory was found.
     *
     * @param path the path to the directory to look up
     *
     * @return true when directory was found, otherwise false.
     */
    public abstract boolean hasDirectory(String path);

    /**
     * Returns a list of filename stored in path.
     *
     * @param path the path to the directory where looking for files
     *
     * @return a list of filename stored in path.
     */
    public abstract List listContent(String path);

    /**
     * Return a new Locator in function of the URL type.
     * an URL pointing to a jar file will return a <code>JarFileLocator</code>
     * and an URL pointing to a directory file will return a <code>DirFileLocator</code>.
     *
     * @param url the base URL
     *
     * @return a new Locator in function of the URL type.
     *
     * @throws IOException when cannot find a specialized locator for the given URL.
     */
    public static Locator getLocator(URL url) throws IOException {

        String path = url.getPath();
        if (path.matches(".*\\..ar")) {
            // jar detected if path ends with .?ar (*.jar, *.war, *.ear)
            return new JarFileLocator(url);
        } else if (path.endsWith("/")) {
            // directory detected if url ends with a separator /
            return new DirLocator(url);
        } else {
            String err = "Unsupported URL '" + url + "' support "
                + "only jar archive and directory";
            throw new IOException(err);
        }
    }
}
