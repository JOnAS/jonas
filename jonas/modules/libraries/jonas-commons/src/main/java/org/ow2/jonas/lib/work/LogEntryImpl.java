/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Florent BENOIT & Ludovic BERT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.work;

// import java
import java.io.File;

import org.ow2.jonas.workcleaner.LogEntry;

/**
 * Class wich represent an entry in a log file : a original associate with its work copy.
 * @author Florent Benoit
 * @author Ludovic Bert
 * @author Benoit PELLETIER
 */
public class LogEntryImpl implements LogEntry {

    /**
     * The original file.
     */
    private File original = null;

    /**
     * The copy file / directory.
     */
    private File copy = null;

    /**
     * Constructor of a log entry.
     * @param original the file to copy
     * @param copy name of the copy
     */
    public LogEntryImpl(final File original, final File copy) {
        super();
        this.original = original;
        this.copy = copy;
    }

    /**
     * Return the ear file of this ear log entry.
     * @return the ear file of this ear log entry.
     */
    public File getOriginal() {
        return original;
    }

    /**
     * Return the unpacked directory.
     * @return the unpacked directory.
     */
    public File getCopy() {
        return copy;
    }

    //TODO add an equals method

}