/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.lib.util;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;

/**
 * {@link NetUtils} is meant to provide routine network related information to
 * components of JONAS. One of the main purposes is to allow the server to
 * extract it's own IP address correctly. This addresses a bug in java when
 * dealing with Linux boxes on DHCP trying to extract their own IP addresses and
 * always getting the loop back address instead of the external one.
 *
 * @link http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4060691
 *
 * @author Vivek Lakshmanan
 */
public class NetUtils {

    /**
     * IPv4 Loop back address.
     */
    public static final String LOOP_BACK_ADDR = "127.0.0.1";

    /**
     * Get the string form of the local IP address and not naively assume
     * <code>InetAddress.getLocalHost().getHostAddress()</code> is the
     * correct value. See the class description above for details.
     *
     * @return A string representing the IPv4 localhost IP address.
     * @throws UnknownHostException
     */
    public static String getLocalAddress() throws UnknownHostException {

        InetAddress localhost = InetAddress.getLocalHost();
        // Check if the value of the localhost address is a loop back address.
        if (localhost.getHostAddress().equals(LOOP_BACK_ADDR)) {
            // If so, try to find a non-loop back address (IPv4 only)
            try {
                NetworkInterface inter = null;
                InetAddress addr = null;
                // Get all network interfaces on this machine.
                Enumeration networkEnum = NetworkInterface.getNetworkInterfaces();
                while (networkEnum.hasMoreElements()) {
                    inter = (NetworkInterface) networkEnum.nextElement();
                    Enumeration enumAddr = inter.getInetAddresses();
                    // Check all addresses for this interface.
                    while (enumAddr.hasMoreElements()) {
                        addr = (InetAddress) enumAddr.nextElement();
                        // Check only IP v4 addresses.
                        if (addr instanceof Inet4Address) {
                            if (!addr.isLoopbackAddress()) {
                                // Found a non-loop back address so return it.
                                return addr.getHostAddress();
                            }
                        }

                    }
                }

                // No non-loop back IP v4 addresses found so return loop back
                // address.
                return localhost.getHostAddress();

            } catch (SocketException e) {
                return localhost.getHostAddress();
            }
        } else {
            // Got a non-loop back address so return it.
            return localhost.getHostAddress();
        }
    }

}