/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Florent BENOIT & Ludovic BERT
 * --------------------------------------------------------------------------
 * $Id:JFileFilter.java 10822 2007-07-04 08:26:06Z durieuxp $
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.bootstrap;

//import java
import java.util.StringTokenizer;
import java.io.File;
import java.io.FileFilter;

/**
 * This class implements a FileFilter, used to list files of a directory.
 * @author Ludovic Bert
 * @author Florent Benoit
 */
public class JFileFilter implements FileFilter {

    /**
     * The extensions that match in the file filter.
     */
    private String[] extensions = null;

    /**
     * Contruct a new JFileFilter with the specified filter.
     * @param filter the filter to use. A comma-separated list of the allowed
     *        extensions. For instance ".jar,.dtd", note that ".*" is allowed.
     */
    public JFileFilter(String filter) {
        StringTokenizer st = new StringTokenizer(filter, ",");
        int tokenCount = st.countTokens();
        extensions = new String[tokenCount];
        for (int i = 0; i < tokenCount; i++) {
            extensions[i] = st.nextToken();
        }
    }

    /**
     * Tests whether or not the specified abstract pathname should be included
     * in a pathname list.
     * @param pathname the abstract pathname to be tested.
     * @return true if and only if pathname should be included.
     */
    public boolean accept(File pathname) {
        if (pathname.isDirectory()) {
            return true;
        } else {
            for (int i = 0; i < extensions.length; i++) {
                if (extensions[i].equals(".*")) {
                    return true;
                } else {
                    if (pathname.getName().endsWith(extensions[i])) {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}