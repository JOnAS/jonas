/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Guillaume SAUTHIER
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.lib.loader.locator;

import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

/**
 * A <code>JarfileLocator</code> is used to look up for a file inside a Jar
 * archive. Works only for local jars.
 *
 * @author Guillaume Sauthier
 */
public class JarFileLocator extends Locator {

    /**
     * Wrapped JarFile where searches will be performed.
     */
    private JarFile file = null;

    /**
     * Construct a new JarFileLocator from an URL representing a Jar.
     *
     * @param jar URL pointing to a Jar file
     *
     * @throws IOException When URL is not a JarFile.
     */
    public JarFileLocator(final URL jar) throws IOException {
        String filename = jar.getFile();
        file = new JarFile(filename);
    }

    /**
     * Returns true when file was found.
     *
     * @param path the path to the file to look up
     *
     * @return true when file was found, otherwise false.
     */
    @Override
    public boolean hasFile(final String path) {

        ZipEntry entry = file.getEntry(path);
        return (entry != null);
    }

    /**
     * Returns true when directory was found.
     *
     * @param path the path to the directory to look up
     *
     * @return true when directory was found, otherwise false.
     */
    @Override
    public boolean hasDirectory(final String path) {

        boolean found = false;
        for (Enumeration e = file.entries(); e.hasMoreElements() && !found;) {
            ZipEntry entry = (ZipEntry) e.nextElement();
            if (entry.getName().startsWith(path)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns a list of filename stored in path.
     *
     * @param path the path to the directory where looking for files
     *
     * @return a list of filename stored in path.
     */
    @Override
    public List listContent(final String path) {

        List libs = new Vector();
        for (Enumeration e = file.entries(); e.hasMoreElements();) {
            ZipEntry entry = (ZipEntry) e.nextElement();
            if (entry.getName().startsWith(path)) {
                libs.add(entry.getName());
            }
        }

        return libs;
    }

}