/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.util;

import java.io.File;
import java.net.URL;

import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * Utility class to build J2eeApplicationNames.
 * @Deprecated Use Deployable.getModuleName() instead
 */
@Deprecated
public class ModuleNamingUtils {

    private static Logger logger = Log.getLogger(Log.JONAS_EAR_PREFIX);

    /**
     * Build the J2EEApplication name.
     * @param pUrl The URL of Ear file
     * @return The J2EEApplication name
     */
    public static String fromURL(final URL pUrl) {
        String sName = null;
        try {
            sName = new File(pUrl.getFile()).getName();
            if ("file".equals(pUrl.getProtocol())) {
                sName = fromFileName(sName);
            }
        } catch (NullPointerException e) {
            logger.log(BasicLevel.DEBUG, "Can't build j2ee application", e);
        }
        return sName;
    }

    /**
     * Build the J2EEApplication name.
     * @param pFilename The name of Ear file
     * @return The J2EEApplication name
     */
    public static String fromFileName(final String pFilename) {
        String sName = null;
        try {
            sName = new File(pFilename).getName();
            int iPos = sName.lastIndexOf('.');
            if (iPos > -1) {
                sName = sName.substring(0, iPos);
            }
        } catch (NullPointerException e) {
            logger.log(BasicLevel.DEBUG, "Can't build j2ee application : " + e);
        }
        return sName;
    }
}
