/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id:JarList.java 10672 2007-06-19 07:18:50Z sauthieg $
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.cpmanager;

//java import
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import org.ow2.util.url.URLUtils;

/**
 * JOnAS Jar list This class provides a way for managing the class-path
 * dependency libraries.
 * @author Florent Benoit
 * @author Ludovic Bert
 */
public class JarList extends Vector {

    /**
     * Construct an instance of a JarList
     */
    public JarList() {
        super();
    }

    /**
     * Construct an instance of a JarList
     * @param st a String tokenizer
     */
    public JarList(StringTokenizer st) {
        super();

        //add each entries
        while (st.hasMoreTokens()) {
            String fileName = st.nextToken();
            add(fileName);
        }
    }

    /**
     * Construct an instance of a JarList
     * @param strs an array of files
     */
    public JarList(String[] strs) {
        super();

        for (int i = 0; i < strs.length; i++) {
            add(strs[i]);
        }
    }

    /**
     * Add the specified file to the current list.
     * @param fileName the name of the file to add.
     */
    public void add(String fileName) {
        if (!contains(fileName) && !fileName.equals("")) {
            super.add(fileName);
        }
    }

    /**
     * Get the URLs of all the JAR file list append to dirName.
     * @param dirName the name of the directory where we append the files (URL format).
     * @return the absolute URLs of the JAR files.
     * @throws JarListException if url are malformed
     */
    public URL[] getURLs(String dirName) throws JarListException {

        List<URL> verifiedUrls = new ArrayList<URL>();
        for (int i = 0; i < elementCount; i++) {
            String s = (String) elementData[i];
            try {
                URL potentialUrl = new URL(dirName + File.separator + s);
                File potentialFile = URLUtils.urlToFile(potentialUrl);
                if (potentialFile.exists()) {
                    // Avoid incorporating an URL pointing to a non-existent file
                    URL verifiedUrl = URLUtils.fileToURL(potentialFile.getCanonicalFile());
                    verifiedUrls.add(verifiedUrl);
                }
            } catch (IOException e) {
                throw new JarListException("Error when trying to get the canonical form for the file " + elementData[i]);
            }
        }
        return verifiedUrls.toArray(new URL[verifiedUrls.size()]);
    }

    /**
     * Merge the specified JarList to the current JarList without adding
     * duplicate entry.
     * @param jarList the list to merge.
     */
    public void merge(JarList jarList) {
        for (Enumeration e = jarList.elements(); e.hasMoreElements();) {
            String s = (String) e.nextElement();
            //call add which call super.add
            add(s);
        }
    }

    /**
     * Remove all the entries contained in the jarList to the current jarList.
     * @param jarList the jarList to remove.
     */
    public void remove(JarList jarList) {

        for (Enumeration e = jarList.elements(); e.hasMoreElements();) {
            String s = (String) e.nextElement();
            if (contains(s)) {
                remove(s);
            }
        }
    }

    /**
     * Set the relative path of the current list. This is useful in the case of
     * an EAR file because the class-path is relative to the JAR file and not
     * the EAR file.
     * @param path the path for set the relative path
     */
    public void setRelativePath(String path) {
        if (path.equals("")) {
            return;
        }

        for (int i = 0; i < elementCount; i++) {
            elementData[i] = path + File.separator + elementData[i];
        }
    }
}
