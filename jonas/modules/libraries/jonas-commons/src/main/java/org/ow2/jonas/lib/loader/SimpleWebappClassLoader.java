/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Guillaume SAUTHIER
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.loader;

import java.io.IOException;
import java.net.URL;

/**
 * ClassLoader specialized for WebApps. It add only the WEB-INF/wsdl/ directory
 * of the war in the URL repository. Used because Catalina doesn't automatically
 * add this resources in its own ClassLoader
 *
 * @author Guillaume Sauthier
 */
public class SimpleWebappClassLoader extends AbsModuleClassLoader {

    /** WSDL directory in a Web module */
    private static final String WSDL_DIRECTORY = "WEB-INF/wsdl/";

    /**
     * Create a new SimpleWebappClassLoader with default parent ClassLoader
     *
     * @param module an URL of Web file
     *
     * @throws IOException if creation fails
     */
    public SimpleWebappClassLoader(URL module) throws IOException {
        super(new URL[] {module});
    }

    /**
     * Create a new SimpleWebappClassLoader with specified parent ClassLoader
     *
     * @param module an URL of Web file
     * @param parent the parent ClassLoader
     *
     * @throws IOException if creation fails
     */
    public SimpleWebappClassLoader(URL module, ClassLoader parent) throws IOException {
        super(new URL[] {module}, parent);
    }

    /**
     * Add the WEB-INF/wsdl/ directory in the ClassLoader.
     *
     * @throws IOException if META-INF/wsdl and ejb-jar root directories cannot
     *         be inserted in loader
     */
    protected void init() throws IOException {
        super.init();
        addInRepository(WSDL_DIRECTORY);
        addInRepository("");
    }
}