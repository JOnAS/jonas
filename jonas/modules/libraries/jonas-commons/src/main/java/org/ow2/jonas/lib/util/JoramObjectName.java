/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.util;

import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;

/**
 * A set of static methods used to build the names of proprietary MBeans used in Joram.
 * @author Adriana Danes
 */
public class JoramObjectName {

    /**
     * JORAM MOM Domain.
     */
    static final String JORAM_DOMAIN = "Joram";

    /**
     * JORAM Client Domain.
     */
    static final String JORAM_CLIENT_DOMAIN = "joramClient";

    /**
     * JORAM Adapter domain
     */
    public static final String JORAM_ADAPTER_DOMAIN = "JoramAdapter";

    /**
     * @return Returns an ObjectName pattern for JORAM Client MBeans.
     * @throws MalformedObjectNameException Could not create {@link ObjectName}
     */
    public static ObjectName joramClientMBeans() throws MalformedObjectNameException {
        return ObjectName.getInstance(JORAM_CLIENT_DOMAIN + ":*");
    }

    /**
     * @return ObjectName for the current JoramAdapter MBean
     * @exception MalformedObjectNameException Could not create ObjectName with the given String
     */
    public static ObjectName joramAdapter() throws MalformedObjectNameException {
        return ObjectName.getInstance(JORAM_ADAPTER_DOMAIN + "#ra:type=JoramAdapter");
        //return ObjectName.getInstance(JORAM_CLIENT_DOMAIN + ":type=JoramAdapter");
    }

    /**
     * Create ObjectName for a Joram managed queue.
     * @param name queue name
     * @return ObjectName for a Joram managed queue
     * @exception MalformedObjectNameException Could not create ObjectName with the given String
     */
    public static ObjectName joramQueue(final String name) throws MalformedObjectNameException {
        return ObjectName.getInstance(JORAM_CLIENT_DOMAIN + ":type=Queue,name=" + name);
    }

    /**
     * Create ObjectName for all Joram managed queues
     * @return ObjectName for a Joram managed queue
     * @exception MalformedObjectNameException Could not create ObjectName with the given String
     */
    public static ObjectName joramQueues() throws MalformedObjectNameException {
        return ObjectName.getInstance(JORAM_CLIENT_DOMAIN + ":type=Queue,*");
    }

    /**
     * Create ObjectName for a Joram managed dead message queue.
     * @param name queue name
     * @return ObjectName for a Joram managed dead message queue
     * @exception MalformedObjectNameException Could not create ObjectName with the given String
     */
    public static ObjectName joramDmQueue(final String name) throws MalformedObjectNameException {
        return ObjectName.getInstance(JORAM_CLIENT_DOMAIN + ":type=Queue.dmq,name=" + name);
    }
    /**
     * Create ObjectName for all Joram managed dead message queues
     * @return ObjectName for a Joram managed dead message queue
     * @exception MalformedObjectNameException Could not create ObjectName with the given String
     */
    public static ObjectName joramDmQueues() throws MalformedObjectNameException {
        return ObjectName.getInstance(JORAM_CLIENT_DOMAIN + ":type=Queue.dmq,*");
    }

    /**
     * Create ObjectName for a Joram managed topic
     * @param name topic name
     * @return ObjectName for a Joram managed topic
     * @exception MalformedObjectNameException Could not create ObjectName with the given String
     */
    public static ObjectName joramTopic(final String name) throws MalformedObjectNameException {
        return ObjectName.getInstance(JORAM_CLIENT_DOMAIN + ":type=Topic,name=" + name);
    }

    /**
     * Create ObjectName for all Joram managed topics
     * @return ObjectName for a Joram managed topic
     * @exception MalformedObjectNameException Could not create ObjectName with the given String
     */
    public static ObjectName joramTopics() throws MalformedObjectNameException {
        return ObjectName.getInstance(JORAM_CLIENT_DOMAIN + ":type=Topic,*");
    }

    /**
     * Create ObjectName for a Joram managed user
     * @param name user name
     * @return ObjectName for a Joram managed user
     * @exception MalformedObjectNameException Could not create ObjectName with the given String
     */
    public static ObjectName joramUser(final String name) throws MalformedObjectNameException {
        return ObjectName.getInstance(JORAM_CLIENT_DOMAIN + ":type=User,name=" + name);
    }

    /**
     * Create ObjectName for all Joram managed users
     * @return ObjectName for a Joram managed user
     * @exception MalformedObjectNameException Could not create ObjectName with the given String
     */
    public static ObjectName joramUsers() throws MalformedObjectNameException {
        return ObjectName.getInstance(JORAM_CLIENT_DOMAIN + ":type=User,*");
    }

}
