/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.execution;

/**
 * Store the result of an {@link IExecution}.
 * @param <T> result object type.
 * @author Guillaume Sauthier
 */
public class ExecutionResult<T> {

    /**
     * Object result of the execution.
     */
    private T result;

    /**
     * Exception (if any).
     */
    private Exception exception;

    /**
     * @return the result
     */
    public T getResult() {
        return result;
    }
    /**
     * @param result the result to set
     */
    public void setResult(final T result) {
        this.result = result;
    }
    /**
     * @return the exception
     */
    public Exception getException() {
        return exception;
    }
    /**
     * @param exception the Exception to set
     */
    public void setException(final Exception exception) {
        this.exception = exception;
    }

    /**
     * @return true if errors were thrown during the execution.
     */
    public boolean hasException() {
        return exception != null;
    }
}
