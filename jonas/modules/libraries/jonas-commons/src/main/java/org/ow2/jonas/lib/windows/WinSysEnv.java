/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Francois Waeselynck
 * Contributor(s): Benoit Pelletier
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.windows;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
//import java.util.Iterator;
import java.util.Properties;

import org.ow2.jonas.lib.util.Log;
//import java.util.Map.Entry;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * This class provides some utilities for the windows environment
 */
public class WinSysEnv {

    /**
     * Maintains an image of the environment
     */
    private static Properties env = null;

    /**
     * Logger
     */
    private static Logger logger = Log.getLogger(WinSysEnv.class.getName());


    /**
     * Get a variable from the windows env
     * @param var variable name
     * @return the variable value
     */
	public static String get(String var) {
		if (env == null) load();
		String value = null;
		if (env != null) value = (String) env.getProperty(var);
		return value;
	}

    /**
     * Get the windows env
     * @return properties
     */
	public static Properties getEnv() {
		if (env == null) load();
		return env;
	}

    /**
     * Get the windows environment
     *
     */
	private static void load() {

		if (env == null) {
			Properties props = new Properties();
			Runtime rt = Runtime.getRuntime();
			Process proc = null;
			try {
				proc = rt.exec("cmd /c SET");
				BufferedReader r = new BufferedReader(new InputStreamReader(
						proc.getInputStream()));
				String ln = null;
				while ((ln = r.readLine()) != null) {
					//System.out.println(ln);
					int ix = ln.indexOf("=");
					props.setProperty(ln.substring(0, ix), ln.substring(ix + 1));
				}
				if (proc != null)
					proc.waitFor();
			} catch (IOException e1) {
				logger.log(BasicLevel.ERROR, "Cannot get System environment : " + e1);
			} catch (InterruptedException e) {
				// Do nothing
			}
			env = props;
		}
	}

    /**
     * main
     * For test purposes
     * @param args
     */
    /*
    public static void main(String[] args) {

        Properties props = System.getProperties();

        // props.list(System.out);

        Iterator it = props.entrySet().iterator();
        while (it.hasNext()) {
            Entry p = (Entry) it.next();
            System.out.println(p.getKey() + "=" + p.getValue());

        }
        System.out.println("\n=========\nWinSysEnv\n=========");
        System.out.println("SystemRoot="+WinSysEnv.get("SystemRoot"));
        props = WinSysEnv.getEnv();
        it = props.entrySet().iterator();
        while (it.hasNext()) {
            Entry p = (Entry) it.next();
            System.out.println(p.getKey() + "=" + p.getValue());
        }
    }
    */

}
