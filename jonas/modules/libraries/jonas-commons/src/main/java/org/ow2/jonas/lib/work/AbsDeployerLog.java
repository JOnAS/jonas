/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.lib.work;

import org.ow2.jonas.workcleaner.DeployerLogException;
import org.ow2.jonas.workcleaner.IDeployerLog;
import org.ow2.jonas.workcleaner.LogEntry;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;
import java.io.File;
import java.util.Vector;

/**
 * Abstract class for deployer log
 * @author Jeremy Cazaux
 */
public abstract class AbsDeployerLog<T extends LogEntry> implements IDeployerLog<T> {

    /**
     * The logger.
     */
    protected Log logger = LogFactory.getLog(AbsDeployerLog.class);

    /**
     * Separator char of a entry in the log file.
     */
    protected static final String SEPARATOR_ENTRY = ";";

    /**
     * File for logging.
     */
    protected File logFile;

    /**
     * The current entries of the logFile.
     */
    protected Vector<T> logEntries = null;

    /**
     * Constructor for the deployerLog.
     * @param logFile the file which is used for read/write entries
     * @throws DeployerLogException if the loadentries failed.
     */
    public AbsDeployerLog(final File logFile) throws DeployerLogException {
        logger.debug("logfile=" + logFile.getName());

        this.logFile = logFile;
        logEntries = new Vector<T>();
        loadEntries();
    }

    /**
     * @return the logger
     */
    protected Log getLogger() {
        return logger;
    }

    /**
     * load the entries of the log file.
     * @throws DeployerLogException if the load failed.
     */
    protected abstract void loadEntries() throws DeployerLogException;

    /**
     * Dump(save) the entries to the log file.
     * @throws DeployerLogException if the save failed.
     */
    protected abstract void saveEntries() throws DeployerLogException;

    /**
     * Return the entries of the file.
     * @return a vector of LogEntry item.
     */
    public synchronized Vector<T> getEntries() {
        return logEntries;
    }

    /**
     * Remove the given entry and return the entries of the file.
     * @param entry the LogEntry which must be remove.
     * @return the new vector of LogEntry item.
     * @throws DeployerLogException if the remove can't be done
     */
    public synchronized Vector<T> removeEntry(final T entry) throws DeployerLogException {
        if (logEntries == null) {
            throw new DeployerLogException("Can not remove a entry, the vector is null");
        }

        if (!logEntries.contains(entry)) {
            throw new DeployerLogException("Can not remove entry " + entry + ". There is no such entry");
        }

        //remove can be done
        logEntries.remove(entry);

        //write to the file.
        saveEntries();

        //return the new vector
        return logEntries;
    }

    /**
     * Add the entry and return the new entries.
     * @param logEntry the entry to add
     * @throws DeployerLogException if the add can't be done
     */
    public abstract Vector<T> addEntry(T logEntry) throws DeployerLogException;

    /**
     * @param original Original File
     * @return the entry which match with the orginal file
     */
    public abstract T getEntry(File original);

}
