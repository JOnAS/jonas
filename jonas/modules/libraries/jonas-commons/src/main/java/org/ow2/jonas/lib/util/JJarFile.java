/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.lib.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.IOException;
import java.util.jar.JarFile;
import java.util.jar.JarEntry;

/**
 * This class implements a JarFile that adds a extract/write method.
 * @author Eric Hardesty
 */
public class JJarFile extends JarFile {

    /**
     * Size of the buffer.
     */
    private static final int BUFFER_SIZE = 2048;

    /**
     * Constructs a new JarFile with the specified File.
     * @param file the jar file to be opened
     */
    public JJarFile(File file) throws IOException {
        super(file);
    }

    /**
     * Constructs a new JarFile with the specified File.
     * @param file the jar file to be opened
     * @param verity boolean to verify if the JarFile is signed
     */
    public JJarFile(File file, boolean verify) throws IOException {
        super(file, verify);
    }

    /**
     * Constructs a new JarFile with the specified File.
     * @param file the jar file to be opened
     * @param verity boolean to verify if the JarFile is signed
     * @param mode int of the mode to open the jar
     */
    public JJarFile(File file, boolean verify, int mode) throws IOException {
        super(file, verify, mode);
    }

    /**
     * Constructs a new JarFile with the specified File.
     * @param name the name of the jar file to be opened
     */
    public JJarFile(String name) throws IOException {
        super(name);
    }

    /**
     * Constructs a new JarFile with the specified File.
     * @param name the name of the jar file to be opened
     * @param verity boolean to verify if the JarFile is signed
     */
    public JJarFile(String name, boolean verify) throws IOException {
        super(name, verify);
    }

    /**
     * Extract the specified Jar Entry to the path given
     * @param jEnt the JarEntry to extract
     * @param filename the filename to write the extracted file
     */
    public void extract(JarEntry jEnt, String filename) throws IOException {

        try {
            //File output
            FileOutputStream out = new FileOutputStream(filename);
            InputStream jis = this.getInputStream(jEnt);
            int n = 0;
            try {
                //buffer
                byte buffer[] = new byte[BUFFER_SIZE];

                while ((n = jis.read(buffer)) > 0) {
                    out.write(buffer, 0, n);
                }
            } finally {
                out.close();
                jis.close();
            }
        } catch (IOException e) {
            throw new IOException("Error while uncompressing the file " +  filename + ": " + e.getMessage());
        }

    }

}
