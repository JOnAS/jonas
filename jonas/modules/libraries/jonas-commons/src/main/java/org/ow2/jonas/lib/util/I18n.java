/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.util;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;


/**
 * This class defines the way for getting message with
 * ResourceBundle for different language.
 * Inspired from http://java.sun.com/docs/books/tutorial/i18n/
 * @author Florent Benoit
 */
public final class I18n {

    /**
     * Name of the resource bundle to use.
     */
    private static final String RESOURCE_BUNDLE_NAME = "I18n";

    /**
     * List of our I18n objects.
     */
    private static Map<String, I18n> bundles = null;

    /**
     * Internal Resource Bundle.
     */
    private ResourceBundle resourceBundle = null;

    /**
     * Constructor (private access, use getInstance instead).
     * @param packageName name of the package for the bundle
     * @param cl the classloader used to load bundle
     */
    private I18n(final String packageName, final ClassLoader cl) {

        String bundleName = packageName + "." + RESOURCE_BUNDLE_NAME;

        // Resource bundle is on the form packageName.I18n.properties
        try {
            Locale locale = Locale.getDefault();
            resourceBundle = ResourceBundle.getBundle(bundleName, locale, cl);
        } catch (MissingResourceException mre) {
                String err = "Error when trying to get a ResourceBundle for package '" + packageName + "' : " + mre.getMessage();
                throw new IllegalStateException(err);
        }
    }

    /**
     * Gets the instance for a given class.
     * @param c the class for which we want a bundle
     * @return I18n object
     */
    public static I18n getInstance(final Class<?> c) {
        return getInstance(c.getPackage().getName(), c.getClassLoader());
    }

    /**
     * Gets the instance for a given class.
     * @param c the class for which we want a bundle
     * @param cl the classloader used to load bundle
     * @return I18n object
     */
    public static I18n getInstance(final Class<?> c, final ClassLoader cl) {
        return getInstance(c.getPackage().getName(), cl);
    }

    /**
     * Gets the instance for a given package.
     * @param packageName the package for which we want a bundle
     * @return I18n object
     */
    public static I18n getInstance(final String packageName) {
        return getInstance(packageName, Thread.currentThread().getContextClassLoader());
    }

    /**
     * Gets the instance for a given package.
     * @param packageName the package for which we want a bundle
     * @param cl the classloader used to load bundle
     * @return I18n object
     */
    public static I18n getInstance(final String packageName, final ClassLoader cl) {
        if (bundles == null) {
            bundles = new HashMap<String, I18n>();
        }

        I18n i18n = (I18n) bundles.get(packageName);
        if (i18n != null) {
            return i18n;
        }


        i18n = new I18n(packageName, cl);
        bundles.put(packageName, i18n);
        return i18n;
    }

    /**
     * Gets the formatted string with the given arguments.
     * @param key the keystring on which to apply arguments
     * @param args the object arguments for the formatter
     * @return the formatted string
     */
    public String getMessage(final String key, final Object[] args) {
        String value = getMessage(key);
        return MessageFormat.format(value, args);
    }


    /**
     * Gets the value of the given key.
     * @param key the keystring to retrieve
     * @return the value for the given key or the key if the value can not be found
     */
    public String getMessage(final String key) {
        String ret = null;

        // No bundle, return key
        if (resourceBundle == null) {
            return key;
        }

        try {
            ret = resourceBundle.getString(key);
        } catch (MissingResourceException mre) {
            // key not found, return the key
            ret = key;
        }

        return ret;
    }

    /**
     * Gets the formatted string with the given arguments.
     * @param key the keystring on which to apply arguments
     * @param obj the object argument for the formatter
     * @return the formatted string
     */
    public String getMessage(final String key, final Object obj) {
        return getMessage(key, new Object[] {obj});
    }


    /**
     * Gets the formatted string with the given arguments.
     * @param key the keystring on which to apply arguments
     * @param obj1 the first object argument for the formatter
     * @param obj2 the second object argument for the formatter
     * @return the formatted string
     */
    public String getMessage(final String key, final Object obj1, final Object obj2) {
        return getMessage(key, new Object[] {obj1, obj2 });
    }

    /**
     * Gets the formatted string with the given arguments.
     * @param key the keystring on which to apply arguments
     * @param obj1 the first object argument for the formatter
     * @param obj2 the second object argument for the formatter
     * @param obj3 the third object argument for the formatter
     * @return the formatted string
     */
    public String getMessage(final String key, final Object obj1, final Object obj2, final Object obj3) {
        return getMessage(key, new Object[] {obj1, obj2, obj3 });
    }


    /**
     * Gets the formatted string with the given arguments.
     * @param key the keystring on which to apply arguments
     * @param obj1 argument for the formatter
     * @param obj2 argument for the formatter
     * @param obj3 argument for the formatter
     * @param obj4 argument for the formatter
     * @return the formatted string
     */
    public String getMessage(final String key, final Object obj1, final Object obj2, final Object obj3, final Object obj4) {
        return getMessage(key, new Object[] {obj1, obj2, obj3, obj4 });
    }


    /**
     * Gets the formatted string with the given arguments.
     * @param key the keystring on which to apply arguments
     * @param obj1 argument for the formatter
     * @param obj2 argument for the formatter
     * @param obj3 argument for the formatter
     * @param obj4 argument for the formatter
     * @param obj5 argument for the formatter
     * @return the formatted string
     */
    public String getMessage(final String key,
                             final Object obj1,
                             final Object obj2,
                             final Object obj3,
                             final Object obj4,
                             final Object obj5) {
        return getMessage(key, new Object[] {obj1, obj2, obj3, obj4, obj5 });
    }


    /**
     * Gets the formatted string with the given arguments.
     * @param key the keystring on which to apply arguments
     * @param obj1 argument for the formatter
     * @param obj2 argument for the formatter
     * @param obj3 argument for the formatter
     * @param obj4 argument for the formatter
     * @param obj5 argument for the formatter
     * @param obj6 argument for the formatter
     * @return the formatted string
     */
    public String getMessage(final String key,
                             final Object obj1,
                             final Object obj2,
                             final Object obj3,
                             final Object obj4,
                             final Object obj5,
                             final Object obj6) {
        return getMessage(key, new Object[] {obj1, obj2, obj3, obj4, obj5, obj6 });
    }

}
