/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.lib.util;

/**
 * @author eyindanga
 *
 */
public interface ConfigurationConstants {

    /**
     * JOnAS Development mode.
     */
    String JONAS_PROPERTIES_PROP = "jonas.properties";


    /**
     * System property name for JONAS_ROOT.
     */
    String JONAS_ROOT_PROP = "jonas.root";

    /**
     * System property name for JONAS_BASE.
     */
    String JONAS_BASE_PROP = "jonas.base";

    /**
     * JOnAS Development mode.
     */
    String JONAS_DEVELOPER_PROP = "jonas.developer";

    /**
     * File name where JOnAS components versions are stored.
     */
    String DEFAULT_JONAS_VERSIONS = "VERSIONS";

    /**
     * Prefix for jonas.properties file
     */
    String DEFAULT_JONAS_PREFIX = "jonas";

    /**
     * JOnAS server name
     */
    String JONAS_NAME_PROP = "jonas.name";

    /**
     * jonas.master property
     */
    String JONAS_MASTER_PROP = "jonas.master";

    /**
     * Work directory property.
     */
    String WORK_DIRECTORY_PROP = "jonas.workdirectory";

    /**
     * Default work directory name.
     */
    String DEFAULT_WORK_DIRECTORY = "work";

    /**
     * configuration directory name (changed from 'config' to 'conf' !!)
     */
    String DEFAULT_CONFIG_DIR = "conf";

    /**
     * Default server name
     */
    String DEFAULT_JONAS_NAME = "jonas";

    /**
     * Default server name
     */
    String DEFAULT_DOMAIN_NAME = "jonas";

    /**
     * domain.name property
     */
    String DOMAIN_NAME_PROP = "domain.name";

    /**
     * Directory in the work directory where the jonas.properties is generated
     */
    String CONF_GENERATED_DIRECTORY = "conf-generated";

}
