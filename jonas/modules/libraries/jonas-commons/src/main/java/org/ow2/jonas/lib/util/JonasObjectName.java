/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.util;

import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;

/**
 * A set of static classes used to build the names of proprietary MBeans used in JOnAS and in Joram.
 * @author Bruno Michel
 * @author Guillaume Riviere
 * @author Florent Benoit
 * @author Ludovic Bert
 * @author Miroslav Halas
 * @author Adriana Danes
 */
public class JonasObjectName {

    /**
     * Create ObjectName for the JMX Connector Server which is an MBean inside the target MBeanServer.
     * @param protocol used protocol
     * @param connectorName name used to distinguish connector servers using the same protocol
     * @return ObjectName for the JMX Connector Server MBean
     */
    public static ObjectName jmxConnectorServer(final String protocol, final String connectorName) {
        try {
            return ObjectName.getInstance("connectors:protocol=" + protocol + ",name=" + connectorName);
        } catch (MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * @param domainName domain name
     * @return ObjectName for reconfiguration manager MBean
     */
    public static ObjectName serverConfig(final String domainName) {
        try {
            return ObjectName.getInstance(domainName + ":type=management,name=reconfigManager");
        } catch (MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * @param domainName the domain name
     * @return ObjectName for an web services
     */
    public static ObjectName wsService(final String domainName) {
        try {
            return ObjectName.getInstance(domainName + ":type=WebService,*");
        } catch (MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * @param domainName the domain name
     * @return ObjectName for the ejb service MBean
     */
    public static ObjectName ejbService(final String domainName) {
        try {
            return ObjectName.getInstance(domainName + ":type=service,name=ejbContainers");
        } catch (MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * @param domainName the domain name
     * @return ObjectName for the ejb3 service MBean
     */
    public static ObjectName ejb3Service(final String domainName) {
        try {
            return ObjectName.getInstance(domainName + ":type=service,name=ejb3");
        } catch (MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * @param domainName the domain name
     * @return ObjectName for the validation service MBean
     */
    public static ObjectName jsfService(final String domainName) {
        try {
            return ObjectName.getInstance(domainName + ":type=service,name=jsf");
        } catch (MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * @param domainName the domain name
     * @return ObjectName for the validation service MBean
     */
    public static ObjectName validationService(final String domainName) {
        try {
            return ObjectName.getInstance(domainName + ":type=service,name=validation");
        } catch (MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * Return the earService (Jmx).
     * @param domainName the domain name
     * @return the Ear service.
     */
    public static ObjectName earService(final String domainName) {
        try {
            return ObjectName.getInstance(domainName + ":type=service,name=ear");
        } catch (MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * Return the deployableMonitorService (Jmx).
     * @param domainName the domain name
     * @return the DeployableMonitor service.
     */
    public static ObjectName deployableMonitorService(final String domainName) {
        try {
            return ObjectName.getInstance(domainName + ":type=service,name=depmonitor");
        } catch (MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * Return the mailService (Jmx).
     * @param domainName the domain name
     * @return the Mail service.
     */
    public static ObjectName mailService(final String domainName) {
        try {
            return ObjectName.getInstance(domainName + ":type=service,name=mail");
        } catch (MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * Return the JNDI Interceptors Object Name (Jmx).
     * @param domainName the domain name
     * @return the ObjectName of the JNDI Interceptor component.
     */
    public static ObjectName jndiInterceptors(final String domainName) {
        try {
            return ObjectName.getInstance(domainName + ":type=service,name=jndi-interceptors");
        } catch (MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * ObjectName for a given context interceptor.
     * @param domainName the JMX domain
     * @param interceptorName the name of the interceptor
     * @return the built object name
     */
    public static ObjectName jndiContextInterceptor(final String domainName, final String interceptorName) {
        try {
            StringBuffer sb = new StringBuffer(domainName);
            sb.append(":type=service");
            sb.append(",name=jndi-interceptors");
            sb.append(",interceptor=");
            sb.append(interceptorName);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }



    /**
     * Return the webContainerService MBean ObjectName.
     * @param domainName the domain name
     * @return the web container service.
     */
    public static ObjectName webContainerService(final String domainName) {
        try {
            return ObjectName.getInstance(domainName + ":type=service,name=webContainers");
        } catch (MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * Return the jaxrpcService MBean ObjectName
     * @param domainName the domain name
     * @return the JAX-RPC service.
     */
    public static ObjectName jaxrpcService(final String domainName) {
        try {
            return ObjectName.getInstance(domainName + ":type=service,name=jaxrpc");
        } catch (MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * Return the jaxwsService MBean ObjectName
     * @param domainName the domain name
     * @return the JAX-WS service.
     */
    public static ObjectName jaxwsService(final String domainName) {
        try {
            return ObjectName.getInstance(domainName + ":type=service,name=jaxws");
        } catch (MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * Return the wsdlPublisher MBean ObjectName
     * @param domainName the domain name
     * @return the WSDL Publisher service.
     */
    public static ObjectName wsdlPublisherService(final String domainName) {
        try {
            return ObjectName.getInstance(domainName + ":type=service,name=wsdl-publisher");
        } catch (MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * @param domainName the domain name
     * @return ObjectName for dbm service MBean
     */
    public static ObjectName databaseService(final String domainName) {
        try {
            return ObjectName.getInstance(domainName + ":type=service,name=database");
        } catch (MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * @param domainName the domain name
     * @return ObjectName for resource service MBean
     */
    public static ObjectName resourceService(final String domainName) {
        try {
            return ObjectName.getInstance(domainName + ":type=service,name=resource");
        } catch (MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * @param domainName the domain name
     * @return ObjectName for jtm service MBean
     */
    public static ObjectName transactionService(final String domainName) {
        try {
            return ObjectName.getInstance(domainName + ":type=service,name=jtm");
        } catch (MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * @param domainName domain name
     * @return ObjectName for JOnAS logging MBeans
     */
    public static ObjectName logService(final String domainName) {
        try {
            return ObjectName.getInstance(domainName + ":type=service,name=log");
        } catch (MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }
    /**
     * @return ObjectName for JOnAS logging MBeans
     */
    public static ObjectName logService() {
        try {
            return ObjectName.getInstance("*:type=service,name=log");
        } catch (MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * Create ObjectName for the LogBuffer MBean
     * @param domainName domain name
     * @param name LogBuffer name
     * @return ObjectName for the LogBuffer MBean
     */
    public static ObjectName logBuffer(final String domainName, final String name) {
        try {
            return ObjectName.getInstance(domainName + ":type=LogBuffer,name=" + name);
        } catch (MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * Create ObjectName for the LogBuffer MBean
     * @param domainName domain name
     * @param name LogBuffer name
     * @return ObjectName for the LogBuffer MBean
     */
    public static ObjectName logBuffers(final String domainName) {
        try {
            return ObjectName.getInstance(domainName + ":type=LogBuffer,*");
        } catch (MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }
    /**
     * Construct ObjectName for the discovery service MBean
     * @param domainName the domain name
     * @return ObjectName for the discovery service MBean
     */
    public static ObjectName discoveryService(final String domainName) {
        try {
            return ObjectName.getInstance(domainName + ":type=service,name=discovery");
        } catch (MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * @param domainName the domain name
     * @return ObjectName for jms service MBean
     */
    public static ObjectName jmsService(final String domainName) {
        try {
            return ObjectName.getInstance(domainName + ":type=service,name=jms");
        } catch (MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * @param domainName the domain name
     * @return ObjectName for jtm service MBean
     */
    public static ObjectName securityService(final String domainName) {
        try {
            return ObjectName.getInstance(domainName + ":type=service,name=security");
        } catch (MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * @return ObjectName for jmx service MBean
     */
    public static ObjectName jmxService() {
        try {
            return ObjectName.getInstance("JMImplementation:type=MBeanServerDelegate");
        } catch (MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * War MBean ObjectName
     * @param domainName domain name
     * @param filename name of the file containing the war
     * @return War MBean ObjectName
     * @throws MalformedObjectNameException Could not construct ObjectName
     */
    public static ObjectName war(final String domainName, final String filename)
        throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=war,fname=" + fileNameForObjectName(filename));
    }

    /**
     * Return an objectName for the WorkCleaner.
     * @param domainName domain name
     * @return an objectName for the WorkCleaner
     */
    public static ObjectName workCleaner(final String domainName) {
        try {
            return ObjectName.getInstance(domainName + ":type=service,name=workcleaner");
        } catch (MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * Return an objectName for the versioning service.
     * @param domainName domain name
     * @return an objectName for the versioning service.
     */
    public static ObjectName versioning(final String domainName) {
        try {
            return ObjectName.getInstance(domainName + ":type=service,name=versioning");
        } catch (MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * @param domainName the domain name
     * @return ObjectName for the ejb3 service MBean
     */
    public static ObjectName dataSourceService(final String domainName) {
        try {
            return ObjectName.getInstance(domainName + ":type=service,name=datasource");
        } catch (MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * Return an objectName for the multitenant service.
     * @param domainName domain name
     * @return an objectName for the multitenant service.
     */
    public static ObjectName multitenant(final String domainName) {
        try {
            return ObjectName.getInstance(domainName + ":type=service,name=multitenant");
        } catch (MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * Return an objectName for the packaging service.
     * @param domainName domain name
     * @return an objectName for the packaging service.
     */
    public static ObjectName packaging(final String domainName) {
        try {
            return ObjectName.getInstance(domainName + ":type=service,name=packaging");
        } catch (MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * Return an objectName for the smartclient service.
     * @param domainName domain name
     * @return an objectName for the smartclient service.
     */
    public static ObjectName smartclient(final String domainName) {
        try {
            return ObjectName.getInstance(domainName + ":type=service,name=smartclient");
        } catch (MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * Return an objectName for the WorkManager
     * @param domainName domain name
     * @return an objectName for the WorkManager
     */
    public static ObjectName workManager(final String domainName) {
        try {
            return ObjectName.getInstance(domainName + ":type=service,name=workmanager");
        } catch (MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * Return an objectName for a Security memory factory.
     * @param domainName the domain name
     * @param name the name of the security memory factory
     * @return an objectName for the security memory factory.
     * @throws MalformedObjectNameException if the objectname can't be build
     */
    public static ObjectName securityMemoryFactory(final String domainName, final String name)
        throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=securityfactory,subtype=memory,name=" + name);
    }

    /**
     * Return an objectName for a Security datasource factory.
     * @param domainName the domain name
     * @param name the name of the security datasource factory
     * @return an objectName for the security datasource factory.
     * @throws MalformedObjectNameException if the objectname can't be build
     */
    public static ObjectName securityDatasourceFactory(final String domainName, final String name)
        throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=securityfactory,subtype=datasource,name=" + name);
    }

    /**
     * Return an objectName for a Security ldap factory.
     * @param domainName the domain name
     * @param name the name of the security ldap factory
     * @return an objectName for the security ldap factory.
     * @throws MalformedObjectNameException if the objectname can't be build
     */
    public static ObjectName securityLdapFactory(final String domainName, final String name)
        throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=securityfactory,subtype=ldap,name=" + name);
    }

    /**
     * Return an objectName for a user
     * @param resource the name of the resource on which the user depends
     * @param username the name of the user
     * @param domainName the domain name
     * @return an objectName for the user MBean
     * @throws MalformedObjectNameException if the objectname can't be build
     */
    public static ObjectName user(final String domainName, final String resource, final String username)
        throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=security,subtype=users,resource=" + resource
            + ",name=" + username);
    }

    /**
     * Return an objectName for a group
     * @param resource the name of the resource on which the group depends
     * @param groupname the name of the group
     * @param domainName the domain name
     * @return an objectName for the group MBean
     * @throws MalformedObjectNameException if the objectname can't be build
     */
    public static ObjectName group(final String domainName, final String resource, final String groupname)
        throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=security,subtype=groups,resource=" + resource
            + ",name=" + groupname);
    }

    /**
     * Return an objectName for a role
     * @param resource the name of the resource on which the role depends
     * @param rolename the name of the role
     * @param domainName the domain name
     * @return an objectName for the user MBean
     * @throws MalformedObjectNameException if the objectname can't be build
     */
    public static ObjectName role(final String domainName, final String resource, final String rolename)
        throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=security,subtype=roles,resource=" + resource
            + ",name=" + rolename);
    }

    /**
     * ObjectName for all war MBeans in the domain
     * @param domainName domain name
     * @return ObjectName for all war MBeans in the domain
     */
    public static ObjectName allWars(final String domainName) {
        try {
            return ObjectName.getInstance(domainName + ":type=war,*");
        } catch (MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    public static String getType(final ObjectName obj) {
        return obj.getKeyProperty("type");
    }

    public static String fileNameForObjectName(final String fileName) {
        return fileName.replace(':', '|');
    }

    /**
     * Return an objectName for the Security factory name.
     * @param domainName the domain name
     * @return an objectName for the security factory name.
     * @throws MalformedObjectNameException if the objectname can't be build
     */
    public static ObjectName allSecurityFactories(final String domainName)
        throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=securityfactory,*");
    }

    /**
     * Return an objectName for all the Security memory factories.
     * @param domainName the domain name
     * @return an objectName for all the security memory factories
     * @throws MalformedObjectNameException if the objectname can't be build
     */
    public static ObjectName allSecurityMemoryFactories(final String domainName)
        throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=securityfactory,subtype=memory,*");
    }

    /**
     * Return an objectName for all the Security datasource factories.
     * @param domainName the domain name
     * @return an objectName for all the security datasource factories
     * @throws MalformedObjectNameException if the objectname can't be build
     */
    public static ObjectName allSecurityDatasourceFactories(final String domainName)
        throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=securityfactory,subtype=datasource,*");
    }

    /**
     * Return an objectName for all the Security ldap factories.
     * @param domainName the domain name
     * @return an objectName for all the security ldap factories
     * @throws MalformedObjectNameException if the objectname can't be build
     */
    public static ObjectName allSecurityLdapFactories(final String domainName)
        throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=securityfactory,subtype=ldap,*");
    }

    /**
     * Return an objectName for all users in a resource.
     * @param domainName the management domain name
     * @param pResource the name of the resource on which the user depends
     * @return an objectName for the user MBean
     * @throws MalformedObjectNameException if the objectname can't be build
     */
    public static ObjectName allUsers(final String domainName, final String pResource)
        throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=security,subtype=users,resource="
            + pResource + ",*");
    }

    /**
     * Return an objectName for all roles in a resource.
     * @param domainName the management domain name
     * @param pResource the name of the resource on which the role depends
     * @return an objectName for the user MBean
     * @throws MalformedObjectNameException if the objectname can't be build
     */
    public static ObjectName allRoles(final String domainName, final String pResource)
        throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=security,subtype=roles,resource="
            + pResource + ",*");
    }

    /**
     * Return an objectName for all groups in a resource.
     * @param domainName the management domain name
     * @param pResource the name of the resource on which the group depends
     * @return an objectName for the user MBean
     * @throws MalformedObjectNameException if the objectname can't be build
     */
    public static ObjectName allGroups(final String domainName, final String pResource)
        throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=security,subtype=groups,resource="
            + pResource + ",*");
    }

    /**
     * A different implementation should allow returning a logical name.
         * This is done currently in the invoke method of ManagementReprImpl/Mx4jManagementReprImpl classes.
     * @return String representation of the ObjectName
     */
    @Override
    public String toString() {
        return super.toString();
    }

    /**
     * @return ObjectName for discovery manager MBean
     * @param domainName the management domain name
     * @exception MalformedObjectNameException Could not create ObjectName with the given String
     */
    public static ObjectName discoveryManager(final String domainName) throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=management,name=discoveryManager,server=JOnAS");
    }

    /**
     * @return ObjectName for Jgroups discovery manager MBean
     * @param domainName the management domain name
     * @exception MalformedObjectNameException Could not create ObjectName with the given String
     */
    public static ObjectName jgroupsDiscoveryManager(final String domainName, final String discoveryType, final String srvName) throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=management,name=discoveryManager"+discoveryType+",server="+srvName);
    }

    /**
     * @return ObjectName for discovery client MBean
     * @param domainName the management domain name
     * @exception MalformedObjectNameException Could not create ObjectName with the given String
     */
    public static ObjectName discoveryClient(final String domainName) throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=management,name=discoveryClient,server=JOnAS");
    }
    /**
     * @return ObjectName for discovery enroller MBean
     * @param domainName the management domain name
     * @exception MalformedObjectNameException Could not create ObjectName with the given String
     */
    public static ObjectName discoveryEnroller(final String domainName) throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=management,name=discoveryEnroller,server=JOnAS");
    }

    /**
     * @return ObjectName for ServerProxy MBean
     * @exception MalformedObjectNameException Could not create ObjectName with the given String
     */
    public static ObjectName serverProxy(final String domainName, final String serverName) throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=ServerProxy,name=" + serverName);
    }

    /**
     * @return ObjectName for ServerProxy MBean
     * @exception MalformedObjectNameException Could not create ObjectName with the given String
     */
    public static ObjectName serverProxys(final String domainName) throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=ServerProxy,*");
    }

    /**
     * Build an ObjectName for a Cluster defined by its type and its name.
     * @param name Name of the Cluster
     * @param type Type: JkCluster, TomcatCluster, JoramCluster, EjbHaCluster, CmiCluster, ...
     * @return ObjectName for a Cluster
     * @throws MalformedObjectNameException
     */
    public static ObjectName cluster(final String domainName, final String name, final String type) throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=" + type + ",name=" + name);
    }

    /**
     * Build an ObjectName for a Cluster defined by its type only.
     * @param type Type: JkCluster, TomcatCluster, JoramCluster, EjbHaCluster, CmiCluster, ...
     * @return ObjectName for a Cluster
     * @throws MalformedObjectNameException
     */
    public static ObjectName clusters(final String domainName, final String type) throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=" + type + ",*");
    }

    /**
     * Build an ObjectName for a ClusterMember.
     * All ClusterMember have the same type, regardless the cluster type.
     * @param name Name of the ClusterMember
     * @param type type of the cluster (ex: JkCluster or TomcatCluster)
     * @param cluster Name of the Cluster
     * @return ObjectName for a ClusterMember
     * @throws MalformedObjectNameException
     */
    public static ObjectName clusterMember(final String domainName, final String name, final String type, final String cluster) throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=" + type + "Member,name=" + name + "," + type + "=" + cluster);
    }

    public static ObjectName clusterDaemon(final String domainName) throws Exception {
        return ObjectName.getInstance(domainName + ":type=ClusterDaemon");
    }

    /**
     * @param name name of the ClusterDaemon
     * @return
     * @exception MalformedObjectNameException Could not create ObjectName with the given String
     */
    public static ObjectName clusterDaemonProxy(final String domainName, final String name) throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=ClusterDaemonProxy,name=" + name);
    }

    /**
     * @param name name of the ClusterDaemon
     * @return
     * @exception MalformedObjectNameException Could not create ObjectName with the given String
     */
    public static ObjectName clusterDaemonProxys(final String domainName) throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=ClusterDaemonProxy,*");
    }

    /**
     * @param domainName the management domain name
     * @return ObjectName for the generic archive configurationMBean
     */
    public static ObjectName ArchiveConfig(final String domainName) {
        try {
            return new ObjectName(":type=archiveConfig,name=ArchiveConfigMBean");
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * @param domainName the management domain name
     * @return ObjectName for RAR specific archive configuration MBean
     */
    public static ObjectName RarConfig(final String domainName) {
        try {
            return new ObjectName(":type=archiveConfig,name=RarConfigMBean");
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * @param domainName
     * @return
     * @throws MalformedObjectNameException
     */
    public static ObjectName jgroupsDiscoveryCommManager(final String domainName, final String discoveryType, final String srvName) throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=management,name=discoveryCommManager"+discoveryType+",server="+srvName);
    }

    /**
     * Build an ObjectName for the CMIClient MBean.
     * @param domainName the management domain name
     * @param serverName the managed server
     * @return ObjectName for a CMIClient
     * @throws MalformedObjectNameException if an ObjectName cannot be created with the given String
     */
    public static ObjectName cmiClient(final String domainName, final String serverName) throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=cmi,name=CMIClient,J2EEServer=" + serverName);
    }

    /**
     * Build an ObjectName for the CMIServer MBean.
     * @param domainName the management domain name
     * @param serverName the managed server
     * @return ObjectName for a CMIServer
     * @throws MalformedObjectNameException if an ObjectName cannot be created with the given String
     */
    public static ObjectName cmiServer(final String domainName, final String serverName) throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=cmi,name=CMIServer,J2EEServer=" + serverName);
    }

    /**
     * Build an ObjectName for the Repository MBean.
     * @param domainName the management domain name
     * @return ObjectName for a Repository Service
     * @throws MalformedObjectNameException if an ObjectName cannot be created with the given String
     */
    public static ObjectName repository(final String domainName) throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName +":type=deployment,name=repository");
    }

    /**
     * Build an ObjectName for the DeploymentPlan MBean.
     * @param domainName the management domain name
     * @return ObjectName for a deploymentPlan Service
     * @throws MalformedObjectNameException if an ObjectName cannot be created with the given String
     */
    public static ObjectName deploymentPlan(final String domainName) throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName +":type=deployment,name=deploymentPlan");
    }


    /**
     * Build an ObjectName for the ResourceMonitor MBean.
     * @param domainName the management domain name
     * @return ObjectName for a resource monitor Service
     * @throws MalformedObjectNameException if an ObjectName cannot be created with the given String
     */
    public static ObjectName resourceMonitor(final String domainName) throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName +":type=deployment,name=resourceMonitor");
    }
}
