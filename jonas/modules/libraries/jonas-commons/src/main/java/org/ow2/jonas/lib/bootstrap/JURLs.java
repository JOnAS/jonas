/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Florent BENOIT & Ludovic BERT
 * --------------------------------------------------------------------------
 * $Id:JURLs.java 10822 2007-07-04 08:26:06Z durieuxp $
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.bootstrap;

import java.util.Enumeration;
import java.util.Vector;
import java.io.File;
import java.net.URL;
import java.net.MalformedURLException;

/**
 * This implements an utility class that list the URLs of the jars to load at
 * the launch time of the JOnAS server.
 * @author Ludovic Bert
 * @author Florent Benoit
 */
public class JURLs extends Vector {

    /**
     * Construct an empty JURLs.
     */
    public JURLs() {
        super();
    }

    /**
     * Add the specified file or directory to this JURLs. Note that in the case
     * of a directory this method add only the content of this one.
     * @param file the specified file or directory to add to this JURLs.
     * @throws MalformedURLException to indicate that a malformed URL has
     *         occured.
     */
    public void add(File file) throws MalformedURLException {
        add(file, null, null, null);
    }

    /**
     * Add the specified directory to this JURLs. Note that this method add the
     * URL of the specified directory and not the content of this directory.
     * @param file the directory to add to this JURLs.
     * @throws MalformedURLException to indicate that a malformed URL has
     *         occured.
     */
    public void addDir(File file) throws MalformedURLException {
        if (!file.exists() || !file.isDirectory()) {
            String err = "Warning: Resource " + file.getName();
            err += " cannot be loaded : The directory does not exist";
            System.out.println(err);
        } else {
            if (!contains(file.toURL())) {
                add(file.toURL());
            }
        }
    }

    /**
     * Add the content of the specified directory to this JURLs, by using the
     * specified filter.
     * @param file the directory to add to this JURLs.
     * @param filter the filter to use to add the content of this directory,
     *        null if not use the filter.
     * @throws MalformedURLException to indicate that a malformed URL has
     *         occured.
     */
    public void add(File file, String filter) throws MalformedURLException {
        if (file.isDirectory()) {
            add(file, filter, null, null);
        } else {
            String err = "Warning: Resource " + file.getName();
            err += " cannot be loaded : It is not a directory";
            System.out.println(err);
        }
    }

    /**
     * Add the content of the specified directory to this JURLs if the contained
     * file not starts with the specified prefix.
     * @param file the directory to add to this JURLs.
     * @param prefix the prefix to ignore, null if not use the prefix.
     * @throws MalformedURLException to indicate that a malformed URL has
     *         occured.
     */
    public void addNotStartWith(File file, String prefix) throws MalformedURLException {

        if (file.isDirectory()) {
            add(file, null, prefix, null);
        } else {
            String err = "Warning: Resource " + file.getName();
            err += " cannot be loaded : It is not a directory";
            System.out.println(err);
        }
    }

    /**
     * Add the content of the specified directory to this JURLs if the contained
     * file not ends with the specified suffix.
     * @param file the directory to add to this JURLs.
     * @param suffix the suffix to ignore, null if not use the suffix.
     * @throws MalformedURLException to indicate that a malformed URL has
     *         occured.
     */
    public void addNotEndWith(File file, String suffix) throws MalformedURLException {

        if (file.isDirectory()) {
            add(file, null, null, suffix);
        } else {
            String err = "Warning: Resource " + file.getName();
            err += " cannot be loaded : It is not a directory";
            System.out.println(err);
        }
    }

    /**
     * Add the content of the specified directory to this JURLs by using a file
     * filter and if the contained file not starts with the prefix and not ends
     * with the suffix.
     * @param file the directory to add to this JURLs.
     * @param filter the filter to use to add the content of this directory,
     *        null if not use the filter.
     * @param prefix the prefix to ignore, null if not use the prefix.
     * @param suffix the suffix to ignore, null if not use the suffix.
     * @throws MalformedURLException to indicate that a malformed URL has
     *         occured.
     */
    public void add(File file, String filter, String prefix, String suffix) throws MalformedURLException {
        if (!file.exists()) {
            String err = "Warning: Resource " + file.getPath();
            err += " cannot be loaded : The file or directory does not exist";
            err += "(Check your environment variable)";
            System.out.println(err);
        } else {
            if (file.isFile()) {
                if (!isMatching(file, prefix, suffix) && !contains(file.toURL())) {
                    // System.out.println("Adding URL "+file.toURL()); // DEBUG
                    add(file.toURL());
                }
            } else {
                File[] childrenFiles = null;
                if (filter != null) {
                    childrenFiles = file.listFiles(new JFileFilter(filter));
                } else {
                    childrenFiles = file.listFiles();
                }
                for (int i = 0; i < childrenFiles.length; i++) {
                    add(childrenFiles[i], filter, prefix, suffix);
                }
            }
        }
    }

    /**
     * Return true if only if the file starts with the specified prefix or ends
     * with the specified suffix.
     * @param file the file to match.
     * @param prefix the prefix to use for the matching, null if do not use the
     *        prefix.
     * @param suffix the suffix to use for the matching, null if do not use the
     *        suffix.
     * @return true if only if the file starts with the specified prefix or ends
     *         with the specified suffix.
     */
    private boolean isMatching(File file, String prefix, String suffix) {
        String fileName = file.getName();
        if (prefix == null) {
            if (suffix == null) {
                return false;
            } else {
                return fileName.endsWith(suffix);
            }
        } else {
            if (suffix == null) {
                return fileName.startsWith(prefix);
            } else {
                return fileName.startsWith(prefix) || fileName.endsWith(suffix);
            }
        }
    }

    /**
     * Merge the specified JURls to this JURLs.
     * @param jurl the JURls to merge with this JURLs.
     */
    public void merge(JURLs jurl) {
        for (Enumeration e = jurl.elements(); e.hasMoreElements();) {
            URL url = (URL) e.nextElement();
            if (!contains(url)) {
                add(url);
            }
        }
    }

    /**
     * Remove the specified file of this JURLs
     * @param file the file to be removed
     * @throws MalformedURLException to indicate that a malformed URL has
     *         occured.
     */
    public void remove(File file) throws MalformedURLException {
        if (file.exists()) {
            remove(file.toURL());
        } else {
            String err = "Warning: Resource " + file.getName();
            err += " cannot be removed : It doesn't exist";
            System.out.println(err);
        }
    }

    /**
     * Return an array containing all the URLs of this JURLs.
     * @return an array containing all the URLs of this JURLs.
     */
    public URL[] toURLs() {
        return (URL[]) super.toArray(new URL[elementCount]);
    }
}