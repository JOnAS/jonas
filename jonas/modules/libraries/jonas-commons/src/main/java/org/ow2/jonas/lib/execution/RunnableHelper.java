/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.execution;

import java.security.AccessController;
import java.security.PrivilegedAction;

/**
 * Helps to run blocks of code under a controlled environment.
 * @author Guillaume Sauthier
 */
public final class RunnableHelper {

    /**
     * Private empty constructor for utility class.
     */
    private RunnableHelper() { }

    /**
     * Run a code block in the context of a given ClassLoader.
     * @param loader The {@link ClassLoader} to be used as TCCL.
     * @param exec code block.
     * @param <T> execution result type.
     * @return the result of this execution.
     */
    public static <T> ExecutionResult<T> execute(final ClassLoader loader,
                                                 final IExecution<T> exec) {

        ExecutionResult<T> result = new ExecutionResult<T>();

        // Store the old loader
        ClassLoader old = getThreadContextClassLader();
        try {
            // Set the new one
            Thread.currentThread().setContextClassLoader(loader);

            // Run the code and store the result
            result.setResult(exec.execute());

        } catch (Exception e) {
            result.setException(e);
        } finally {
            // Revert to the previous loader
            Thread.currentThread().setContextClassLoader(old);
        }

        // return the result holder
        return result;
    }

    /**
     * @return Return the current {@link Thread} context ClassLoader
     *         (using a priviledged block).
     */
    public static ClassLoader getThreadContextClassLader() {
        return AccessController.doPrivileged(new PrivilegedAction<ClassLoader>() {
            public ClassLoader run() {
                return Thread.currentThread().getContextClassLoader();
            }
        });
    }
}
