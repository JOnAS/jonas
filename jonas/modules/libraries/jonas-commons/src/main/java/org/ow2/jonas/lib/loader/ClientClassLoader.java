/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.loader;

import java.io.IOException;
import java.net.URL;

/**
 * ClassLoader used for loading Client classes and resources
 *
 * @author Guillaume Sauthier
 */
public class ClientClassLoader extends AbsModuleClassLoader {

    /** classes are located in the module root */
    private static final String CLASSES_DIRECTORY = "";

    /** WSDL files are located in META-INF/wsdl directory */
    private static final String WSDL_DIRECTORY = "META-INF/wsdl/";

    /**
     * Create a new ClientClassLoader with default parent ClassLoader
     *
     * @param module an URL of Client files
     *
     * @throws IOException if creation fails
     */
    public ClientClassLoader(final URL module) throws IOException {
        super(new URL[]{module});
    }

    /**
     * Create a new ClientClassLoader with specified parent ClassLoader
     *
     * @param module an URL of Client files
     * @param parent the parent ClassLoader to use
     *
     * @throws IOException if creation fails
     */
    public ClientClassLoader(final URL module, final ClassLoader parent) throws IOException {
        super(new URL[]{module}, parent);
    }

    /**
     * Create a new ClientClassLoader with specified parent ClassLoader
     *
     * @param modules an URL[] of Client files + dependencies
     * @param parent the parent ClassLoader to use
     *
     * @throws IOException if creation fails
     */
    public ClientClassLoader(final URL[] modules, final ClassLoader parent) throws IOException {
        super(modules, parent);
    }

    /**
     * Add the  directory and the content of
     * META-INF/wsdl/ in the ClassLoader.
     *
     * @throws IOException When initialisation fails.
     */
    @Override
    protected void init() throws IOException {
        super.init();
        addInRepository(CLASSES_DIRECTORY);
        addInRepository(WSDL_DIRECTORY);
    }
}
