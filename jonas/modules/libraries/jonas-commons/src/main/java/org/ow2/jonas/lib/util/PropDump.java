/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.util;

import java.util.Enumeration;
import java.util.Properties;
import org.objectweb.util.monolog.api.Logger;

/**
 * Used to print out properties
 */
public class PropDump {

    /**
     * Prints out the {@link Properties}.
     * @param header Display header
     * @param props Properties to be printed
     * @param logger {@link Logger} to use
     * @param logLevel Log level
     */
    public static void print(final String header,
                             final Properties props,
                             final Logger logger,
                             final int logLevel) {

        logger.log(logLevel, header);
        logger.log(logLevel, "=================================");
        String aName = null;
        String aValue = null;
        for (Enumeration pNames = props.propertyNames(); pNames.hasMoreElements();) {
            aName = (String) pNames.nextElement();
            aValue = (String) props.getProperty(aName);
            if (aValue != null) {
                logger.log(logLevel, aName + " = " + aValue);
            }
        }
        logger.log(logLevel, "=================================");
    }
}
