/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.lib.loader;

import java.io.InputStream;
import java.net.URL;

/**
 * Delegate all call to the Thread context ClassLoader.
 * @author Guillaume Sauthier
 */
public class ThreadContextClassLoader extends ClassLoader {

    /**
     *
     */
    public ThreadContextClassLoader() {
        super();
    }

    /**
     * @see java.lang.ClassLoader#clearAssertionStatus()
     */
    public synchronized void clearAssertionStatus() {
        getContextClassLoader().clearAssertionStatus();
    }

    /**
     * @see java.lang.ClassLoader#getResource(java.lang.String)
     */
    public URL getResource(String name) {
        return getContextClassLoader().getResource(name);
    }

    /**
     * @see java.lang.ClassLoader#getResourceAsStream(java.lang.String)
     */
    public InputStream getResourceAsStream(String name) {
        return getContextClassLoader().getResourceAsStream(name);
    }

    /**
     * @see java.lang.ClassLoader#loadClass(java.lang.String)
     */
    public Class loadClass(String name) throws ClassNotFoundException {
        return getContextClassLoader().loadClass(name);
    }

    /**
     * @see java.lang.ClassLoader#setClassAssertionStatus(java.lang.String,
     *      boolean)
     */
    public synchronized void setClassAssertionStatus(String className, boolean enabled) {
        getContextClassLoader().setClassAssertionStatus(className, enabled);
    }

    /**
     * @see java.lang.ClassLoader#setDefaultAssertionStatus(boolean)
     */
    public synchronized void setDefaultAssertionStatus(boolean enabled) {
        getContextClassLoader().setDefaultAssertionStatus(enabled);
    }

    /**
     * @see java.lang.ClassLoader#setPackageAssertionStatus(java.lang.String,
     *      boolean)
     */
    public synchronized void setPackageAssertionStatus(String packageName, boolean enabled) {
        getContextClassLoader().setPackageAssertionStatus(packageName, enabled);
    }

    /**
     * @return Thread context ClassLoader
     */
    private ClassLoader getContextClassLoader() {
        return Thread.currentThread().getContextClassLoader();
    }
}