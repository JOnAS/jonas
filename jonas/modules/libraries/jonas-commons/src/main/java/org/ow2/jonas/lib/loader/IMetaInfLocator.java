/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.loader;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;

/**
 * Interface used to search entries located in META-INF/ folder of some bundles.
 * @author Florent Benoit
 */
public interface IMetaInfLocator {

    /**
     * Gets an URL for a given resource name.
     * @param name the name to search
     * @return value else null if not found
     */
    URL getResource(String name);


    /**
     * Gets list of URL for a given resource name.
     * @param name the name to search
     * @return value else null if not found
     * @throws IOException if there is an error while searching
     */
    Enumeration<URL> getResources(String name) throws IOException;

    /**
     * Gets an input stream for a given resource name.
     * @param name the name to search
     * @return value else null if not found
     */
    InputStream getResourceAsStream(String name);

}
