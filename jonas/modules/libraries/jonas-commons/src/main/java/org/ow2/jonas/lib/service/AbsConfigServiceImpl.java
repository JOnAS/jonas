/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.lib.service;

import org.ow2.jonas.addon.deploy.api.config.IAddonConfig;
import org.ow2.jonas.service.ConfigService;

/**
 * Abstract implementation of a {@link org.ow2.jonas.service.ConfigService}.
 * @author Jeremy Cazaux
 */
public abstract class AbsConfigServiceImpl extends AbsServiceImpl implements ConfigService {

    /**
     * Addon configuration component
     */
    protected IAddonConfig addonConfig;

    /**
     * {@inheritDoc}
     */
    public void registerAddonConfig(final IAddonConfig addonConfig) {
        this.addonConfig = addonConfig;
    }

    /**
     * {@inheritDoc}
     */
    public void unregisterAddonConfig(final IAddonConfig addonConfig) {
        this.addonConfig = null;
    }
}
