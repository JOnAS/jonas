/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.loader.xml;

import java.util.ArrayList;
import java.util.List;

/**
 * A {@code FiltersDefinition} is the XML representation of the {@code &lt;filters>} (or
 * {@code &lt;default-filters>}) elements.<br />
 * It defines a set of filter name.
 * @author Florent Benoit
 *
 */
public class FiltersDefinition {

    /**
     * List of filters.
     */
    private List<String> filters = new ArrayList<String>();

    /**
     * @return list of filters.
     */
    public List<String> getFilters() {
        return this.filters;
    }

    /**
     * Sets the list of filters.
     * @param filters the given value
     */
    public void setFilters(final List<String> filters) {
        this.filters = filters;
    }

}
