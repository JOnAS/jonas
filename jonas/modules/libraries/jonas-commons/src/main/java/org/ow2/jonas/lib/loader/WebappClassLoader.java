/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2003-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.loader;

import java.io.IOException;
import java.net.URL;

/**
 * ClassLoader specialized for WebApps. It add the WEB-INF/wsdl/ directory
 * of the war in the URL repository,  the base URL of the jar file and the
 * WEB-INF/classes/ directory.
 * Used in WsGen.
 *
 * @author Guillaume Sauthier
 */
public class WebappClassLoader extends SimpleWebappClassLoader {

    /** Classes directory entry name in Web module */
    private static final String CLASSES_DIRECTORY = "WEB-INF/classes/";

    /** Libraries directory entry name in Web module */
    private static final String LIB_DIRECTORY = "WEB-INF/lib/";

    /**
     * Create a new WebappClassLoader with default parent ClassLoader
     *
     * @param module an URL of Web file
     *
     * @throws IOException if creation fails
     */
    public WebappClassLoader(URL module) throws IOException {
        super(module);
    }

    /**
     * Create a new WebappClassLoader with specified parent ClassLoader
     *
     * @param module an URL of Web file
     * @param parent the parent ClasLoader
     *
     * @throws IOException if creation fails
     */
    public WebappClassLoader(URL module, ClassLoader parent) throws IOException {
        super(module, parent);
    }

    /**
     * Add the WEB-INF/classes/ directory and the content of
     * WEB-INF/lib/ in the ClassLoader
     *
     * @throws IOException if cannot add in repository specified paths
     */
    protected void init() throws IOException {
        super.init();
        addInRepository(CLASSES_DIRECTORY);
        addContentInRepository(LIB_DIRECTORY);
    }

    /**
     * @return Returns the Base URL for this ClassLoader
     */
    public URL getBaseURL() {
        return getBases()[0];
    }

}
