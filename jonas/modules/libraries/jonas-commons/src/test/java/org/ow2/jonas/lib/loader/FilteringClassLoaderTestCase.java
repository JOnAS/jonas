/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2010-2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.lib.loader;

import java.net.URL;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * A {@code FilteringClassLoaderTestCase} is ...
 *
 * @author Guillaume Sauthier
 */
public class FilteringClassLoaderTestCase {

    private ClassLoader getParent() {
        return FilteringClassLoaderTestCase.class.getClassLoader();
    }

    private URL getResource(final String name) {
        return FilteringClassLoaderTestCase.class.getResource(name);
    }

    @Test
    public void testAdminCanForceDisabledFiltering() throws Exception {
        FilteringClassLoader loader = new FilteringClassLoader(getParent());
        loader.setTransparent(false);
        loader.setDefinitionUrl(getResource("/test-default-filters.xml"));
        System.setProperty(FilteringClassLoader.DISABLE_FILTERING_PROPERTY_NAME, "true");

        loader.start();

        Assert.assertTrue(loader.isTransparent(), "ClassLoader is not transparent");

        System.getProperties().remove(FilteringClassLoader.DISABLE_FILTERING_PROPERTY_NAME);

    }

    @Test
    public void testXmlParsingWithDefaultFiltersDefinition() throws Exception {
        FilteringClassLoader loader = new FilteringClassLoader(getParent());
        loader.setDefinitionUrl(getResource("/test-default-filters.xml"));

        loader.start();
        Assert.assertTrue(loader.getFilters().contains("the.system.filter.*"),
                          "ClassLoader has not the expected filtering content");
    }

    @Test
    public void testXmlParsingWithFiltersDefinition() throws Exception {
        FilteringClassLoader loader = new FilteringClassLoader(getParent());
        loader.setDefinitionUrl(getResource("/test-app-filters.xml"));

        loader.start();
        Assert.assertTrue(loader.getFilters().contains("my.app.filter.*"),
                          "ClassLoader has not the expected filtering content");
    }


    @Test
    public void testXmlParsingWithFiltersDefinitionAndWildcardForbidden() throws Exception {
        FilteringClassLoader loader = new FilteringClassLoader(getParent());
        loader.setDefinitionUrl(getResource("/test-app-filters-wildcard-forbidden.xml"));

        loader.start();
        Assert.assertEquals(loader.getFilters().size(), 1, "* wildcard should have been removed");
        Assert.assertTrue(loader.getFilters().contains("library.*"),
                          "ClassLoader has not the expected filtering content");
    }
}
