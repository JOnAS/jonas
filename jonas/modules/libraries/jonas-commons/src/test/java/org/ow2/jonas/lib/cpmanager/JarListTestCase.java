/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.lib.cpmanager;

import java.io.File;
import java.net.URL;

import org.hamcrest.Matchers;
import org.ow2.util.url.URLUtils;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class JarListTestCase {

    @Test
    public void testReferenceToMissingArchiveIsOmitted() throws Exception {
        JarList list = new JarList();
        list.add("$$$$non-existent.jar");

        String userDir = System.getProperty("user.dir");
        File userDirFile = new File(userDir);
        URL userDirUrl = URLUtils.fileToURL(userDirFile);
        URL[] urls = list.getURLs(userDirUrl.toExternalForm());

        assertThat(urls, is(Matchers.<Object>notNullValue()));
        assertThat(urls.length, is(equalTo(0)));
    }

}
