package org.ow2.jonas.lib.management.javaee;


/**
 * Enumeration listing all possible states for a JOnAS Service.
 * @author Adriana Danes
 */
public enum J2EEServiceState {

    /**
     * STARTING state.
     */
    STARTING ("service.state.starting"),

    /**
     * RUNNING state.
     */
    RUNNING ("service.state.running"),

    /**
     * STOPPING state.
     */
    STOPPING ("service.state.stopping"),

    /**
     * STOPPED state.
     */
    STOPPED ("service.state.stopped"),

    /**
    * FAILED state.
    */
    FAILED ("service.state.failed");


    /**
     * State name.
     */
    private String name = null;

    /**
     * Private constructor.
     * @param name the given state name
     */
    private J2EEServiceState(final String name) {
        this.name = name;
    }

    /**
     * Returns the state name.
     * @return the state name
     */
    public String getName(){
        return name;
    }
}
