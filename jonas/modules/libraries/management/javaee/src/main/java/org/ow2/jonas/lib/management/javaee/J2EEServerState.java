/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.management.javaee;

/**
 * Enumeration listing all possible J2EE Server states.
 * @author Francois Fornaciari
 */
public enum J2EEServerState {
    /**
     * INITIAL state.
     */
    INITIAL ("j2ee.state.initial"),

    /**
     * STARTING state.
     */
    STARTING ("j2ee.state.starting"),

    /**
     * RUNNING state.
     */
    RUNNING ("j2ee.state.running"),

    /**
     * STOPPING state.
     */
    STOPPING ("j2ee.state.stopping"),

    /**
     * STOPPED state.
     */
    STOPPED ("j2ee.state.stopped"),

    /**
    * FAILED state.
    */
    FAILED ("j2ee.state.failed"),

    /**
     * UNREACHABLE state.
     */
    UNREACHABLE ("j2ee.state.unreachable"),

    /**
     * UNKNOWN state.
     */
    UNKNOWN ("j2ee.state.unknown"),
    /**
     * STANDBY state.
     */
    STANDBY ("j2ee.state.standby");

    /**
     * State name.
     */
    private String name = null;

    /**
     * Private constructor.
     * @param name the given state name
     */
    private J2EEServerState(final String name) {
        this.name = name;
    }

    /**
     * Returns the state name.
     * @return the state name
     */
    public String getName(){
        return name;
    }
}
