/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.management.javaee;

import javax.management.MBeanException;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;

/**
 * J2EEDeployedObject JSR77 MBean.
 * @param <T> ManagedObject type
 * @author Guillaume Sauthier
 * @author Florent BENOIT
 */
public class J2EEDeployedObjectMBean<T> extends J2EEManagedObjectMBean<T> {

    /**
     * Creates a J2EEDeployedObject.
     * @throws MBeanException if creation fails.
     */
    public J2EEDeployedObjectMBean() throws MBeanException {
        super();
    }

    /**
     * @return Returns the XML Deployment Descriptors of the Module.
     */
    public String getDeploymentDescriptor() {
        //TODO: implement it
       return null;
    }

    /**
     * @return Returns the J2EEServer ObjectName.
     */
    public String getServer() {
        String serverName = null;
        try {
            ObjectName on = new ObjectName(getObjectName());
            serverName = J2eeObjectName.J2EEServer(on.getDomain(), on.getKeyProperty(J2EESERVER_KEY)).toString();
        } catch (MalformedObjectNameException e) {
            throw new IllegalStateException("Cannot build Server object name", e);
        }
        return serverName;
    }
}
