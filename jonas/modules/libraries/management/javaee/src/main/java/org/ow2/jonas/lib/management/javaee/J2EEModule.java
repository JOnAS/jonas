/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.management.javaee;


/**
 * A J2EE MBean module used on server (EJB container).
 * @author Michel-Ange Anton
 */
public class J2EEModule extends J2EEDeployedObject {

    private String [] vms = new String[0];

    /**
     * MBean constructor.
     * @param objectName The object name of the deployed object
     */
    protected J2EEModule(String objectName) {
        super(objectName);
    }

    /**
     * MBean constructor.
     * @param p_ObjectName The object name of the module object
     * @param p_StateManageable If true, this object implements J2EE State Management Model
     * @param p_StatisticsProvider If true, this object implements the J2EE StatisticProvide Model
     * @param p_EventProvider If true, this object implements the J2EE EventProvider Model
     */
    protected J2EEModule(String p_ObjectName, boolean p_StateManageable
        , boolean p_StatisticsProvider, boolean p_EventProvider) {
        super(p_ObjectName, p_StateManageable, p_StatisticsProvider, p_EventProvider);
    }

    public void setJavaVMs(String [] vms) {
        // TODO
    }

    public String [] getJavaVMs() {
        return vms;
    }
}
