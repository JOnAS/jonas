/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.management.javaee;

import javax.management.MBeanException;
import javax.management.Notification;
import javax.management.RuntimeOperationsException;
import javax.management.modelmbean.ModelMBean;

import org.ow2.jonas.lib.management.services.ReconfigNotifications;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;
/**
 *
 * @author Adriana Danes
 *
 */
public class ManagedObject {
    /**
     * Reference on the model MBean.
     */
    private ModelMBean modelMBean = null;

    protected static Log logger = LogFactory.getLog(ManagedObject.class);
    /**
     * Set the reference on my ModelMBean.
     * @param modelMBean the modelMBean
     */
    public void setModelMBean(final ModelMBean modelMBean) {
        this.modelMBean = modelMBean;
    }

    public ModelMBean getModelMBean() {
        return modelMBean;
    }

    /**
     * Send a <i>save configuration</i> notification to the registerd
     * listeners.
     * @param sequenceNumber notification attribute
     * @param resourceName the name of a reconfigurable resource or of a JOnAS
     *        service
     */
    public void sendSaveNotification(final long sequenceNumber, final String resourceName) {
        // create save notification
        Notification saveNotif = new Notification(ReconfigNotifications.SAVE_RECONFIG_TYPE, modelMBean, sequenceNumber, resourceName);
        sendNotification(saveNotif);
    }

    /**
     * Send a <i>reconfiguration</i> notification to the registerd listener.
     * @param sequenceNumber notification attribute
     * @param resourceName the name of a reconfigurable resource or of a JOnAS
     *        service
     * @param userData data containing the name and the value of the
     *        reconfigured property(or properties)
     */
    public void sendReconfigNotification(final long sequenceNumber, final String resourceName, final Object userData) {
        // create notification and set the userData
        Notification configNotif = new Notification(ReconfigNotifications.RECONFIG_TYPE, modelMBean, sequenceNumber, resourceName);
        configNotif.setUserData(userData);
        sendNotification(configNotif);
    }
    /**
     *
     * @param notif
     */
    protected void sendNotification(final Notification notif) {
        try {
            if (modelMBean != null) {
                modelMBean.sendNotification(notif);
            } else {
                logger.info("Can't send notification " + notif.toString());
            }
        } catch (RuntimeOperationsException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (MBeanException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
