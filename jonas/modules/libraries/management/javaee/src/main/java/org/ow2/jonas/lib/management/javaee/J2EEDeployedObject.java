/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007-2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.management.javaee;

/**
 * Implements JSR77 J2EEDeployedObject corresponding to a J2EEApplication
 * or to a J2EEModule.
 * @author Adriana.Danes@bull.net
 */
public class J2EEDeployedObject extends J2EEManagedObject {

    /**
     * J2EEDeployedObject deployment descriptor.
     */
    private String deploymentDescriptor = null;

    /**
     * Specific JOnAS Deployment descriptor.
     */
    private String jonasDeploymentDescriptor = null;


    /**
     * OBJECT_NAME of the server the application or module is deployed on.
     */
    private String server = null;
    /**
     * MBean constructor.
     * @param objectName The complete name of the managed object
     */
    protected J2EEDeployedObject(final String objectName) {
        super(objectName);
        this.deploymentDescriptor = "";
    }
    /**
     * MBean constructor.
     * @param objectName object name of the managed object
     * @param stateManageable if true, this managed object implements J2EE State
     *        Management Model
     * @param statisticsProvider if true, this managed object implements the
     *        J2EE StatisticProvide Model
     * @param eventProvider if true, this managed object implements the J2EE
     *        EventProvider Model
     */
    protected J2EEDeployedObject(final String objectName, final boolean stateManageable, final boolean statisticsProvider,
            final boolean eventProvider) {
        super(objectName, stateManageable, statisticsProvider, eventProvider);
    }
    public String getDeploymentDescriptor() {
        return deploymentDescriptor;
    }
    public void setDeploymentDescriptor(final String deploymentDescriptor) {
        // Only set if it is not null, else keep an empty string
        if (deploymentDescriptor != null) {
            this.deploymentDescriptor = deploymentDescriptor;
        }
    }
    public String getJonasDeploymentDescriptor() {
        return jonasDeploymentDescriptor;
    }
    public void setJonasDeploymentDescriptor(final String jonasDeploymentDescriptor) {
        this.jonasDeploymentDescriptor = jonasDeploymentDescriptor;
    }
    public String getServer() {
        return server;
    }
    public void setServer(final String server) {
        this.server = server;
    }
    /**
     * Allow detecting a J2EEDeployedObject MBean.
     * @param type MBean type
     * @return true if the type corresponds to a J2EEDeployedObject type
     */
    public static boolean isJ2EEDeployedObjectType(final String type) {
        if (type == null) {
            return false;
        }
        if (type.equals("J2EEApplication")
                || type.equals("ApplClientModule")
                || type.equals("EJBModule")
                || type.equals("WebModule")
                || type.equals("ResourceAdapterModule")
                ) {
            return true;
        }
        return false;

    }
}
