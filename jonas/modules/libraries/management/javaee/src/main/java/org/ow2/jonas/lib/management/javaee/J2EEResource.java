/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.management.javaee;


public class J2EEResource extends J2EEManagedObject {
    /**
     * MBean constructor.
     * @param objectName object name of the managed object
     * @param stateManageable if true, this managed object implements J2EE State Management Model
     * @param statisticsProvider if true, this managed object implements the J2EE StatisticProvide Model
     * @param eventProvider  if true, this managed object implements the J2EE EventProvider Model
     */
    public J2EEResource(final String objectName, final boolean stateManageable, final boolean statisticsProvider, final boolean eventProvider) {
        super(objectName, stateManageable, statisticsProvider, eventProvider);
    }

    /**
     * MBean constructor.
     * @param objectName object name of the managed object
     */
    public J2EEResource(final String objectName) {
        super(objectName);
    }

    /**
     * Allow detecting a J2EEResource MBean.
     * @param type MBean type
     * @return true if the type corresponds to a J2EEResource type
     */
    public static boolean isJ2eeResourceType(final String type) {
        if (type == null) {
            return false;
        }
        if (type.equals("JavaMailResource")
                || type.equals("JNDIResource")
                || type.equals("JDBCResource")
                || type.equals("JMSResource")
                || type.equals("JTAResource")
                || type.equals("JCAResource")
                || type.equals("RMIIIOPResource")
                || type.equals("URLResource")
                ) {
            return true;
        } else {
            return false;
        }
    }
}
