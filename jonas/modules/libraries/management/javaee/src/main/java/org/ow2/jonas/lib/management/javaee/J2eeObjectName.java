/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2009 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.management.javaee;

import javax.management.ObjectName;

/**
 * A set of static methods used to create object names for J2EE managed object.
 * This implementation is conformant to the JSR77.
 * @author Adriana Danes
 * @author Michel-Ange Anton
 */
public class J2eeObjectName {

    /**
     * Private constructor.
     */
    private J2eeObjectName() {
    }

    // --------------------------------------------------------- Constants
    /**
     * Constant used in pattern ObjectNames.
     */
    public static final String ALL = "*";

    /**
     * Constant used in ObjectNames of stand-alone modules.
     */
    public static final String NONE = "null";

    //  --------------------------------------------------------- Public Methods
    /**
     * @param pObjectName Stringified ObjectName
     * @return ObjectName instance corresponding to the received argument
     */
    public static ObjectName getObjectName(final String pObjectName) {
        try {
            return ObjectName.getInstance(pObjectName);
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * @param pDomain The Domain name
     * @return ObjectName for a J2EEDomain MBean
     */
    public static ObjectName J2EEDomain(final String pDomain) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=J2EEDomain");
            sb.append(",name=");
            sb.append(pDomain);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * @param pDomain The Domain name
     * @return OBJECT_NAME String for a J2EEDomain MBean
     */
    public static String J2EEDomainName(final String pDomain) {
        StringBuffer sb = new StringBuffer(pDomain);
        sb.append(":j2eeType=J2EEDomain");
        sb.append(",name=");
        sb.append(pDomain);
        return sb.toString();
    }
    /**
     *
     * @return Pattern ObjectName for J2EEDomain MBeans.
     */
    public static ObjectName J2EEDomains() {
        try {
            StringBuffer sb = new StringBuffer(ALL);
            sb.append(":j2eeType=J2EEDomain");
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * @return Pattern ObjectName for J2EEServer MBeans.
     */
    public static ObjectName J2EEServers() {
        try {
            StringBuffer sb = new StringBuffer(ALL);
            sb.append(":j2eeType=J2EEServer");
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * @return Pattern ObjectName for J2EEServer MBeans in a given domain.
     */
    public static ObjectName J2EEServers(final String pDomain) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=J2EEServer");
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * Create ObjectName for a J2EEServer MBean.
     * @param pDomain domain name
     * @param pServer server name
     * @return the created ObjectName
     */
    public static ObjectName J2EEServer(final String pDomain, final String pServer) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=J2EEServer");
            sb.append(",name=");
            sb.append(pServer);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * Create OBJECT_NAME for a J2EEServer MBean.
     * @param pDomain domain name
     * @param pServer server name
     * @return the created OBJECT_NAME
     */
    public static String J2EEServerName(final String pDomain, final String pServer) {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=J2EEServer");
            sb.append(",name=");
            sb.append(pServer);
            return sb.toString();
    }

    /**
     * Create ObjectName for a JVM MBean.
     * @param pDomain domain name
     * @param pServer server name
     * @param pName JVM name
     * @return the created ObjectName
     */
    public static ObjectName JVM(final String pDomain, final String pServer, final String pName) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=JVM");
            sb.append(",name=");
            sb.append(pName);
            sb.append(",J2EEServer=");
            sb.append(pServer);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * Create OBJECT_NAME for a JVM MBean.
     * @param pDomain domain name
     * @param pServer server name
     * @param pName JVM name
     * @return the created OBJECT_NAME
     */
    public static String JVMName(final String pDomain, final String pServer, final String pName) {
        StringBuffer sb = new StringBuffer(pDomain);
        sb.append(":j2eeType=JVM");
        sb.append(",name=");
        sb.append(pName);
        sb.append(",J2EEServer=");
        sb.append(pServer);
        return sb.toString();
    }
    /**
     * @return Pattern ObjectName for JVM MBeans.
     */
    public static ObjectName JVMs(final String pDomain, final String pServer) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=JVM");
            sb.append(",J2EEServer=");
            sb.append(pServer);
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    public static ObjectName J2EEApplications() {
        try {
            StringBuffer sb = new StringBuffer(ALL);
            sb.append(":j2eeType=J2EEApplication");
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    public static ObjectName J2EEApplications(final String pDomain) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=J2EEApplication");
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    public static ObjectName J2EEApplications(final String pDomain, final String pServer) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=J2EEApplication");
            sb.append(",J2EEServer=");
            sb.append(pServer);
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    public static ObjectName J2EEApplication(final String pDomain, final String pServer, final String pName) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=J2EEApplication");
            sb.append(",name=");
            sb.append(pName);
            sb.append(",J2EEServer=");
            sb.append(pServer);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }
    public static String J2EEApplicationName(final String pDomain, final String pServer, final String pName) {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=J2EEApplication");
            sb.append(",name=");
            sb.append(pName);
            sb.append(",J2EEServer=");
            sb.append(pServer);
            return sb.toString();
    }
    /**
     * Create generic name for all AppClientModules deployed in a given server, possibly belonging
     * to a given J2EE application
     * @param pDomain domain name
     * @param pServer server name
     * @param pApplication application name
     * @return generic name for the AppClientModules in a given server and application
     */
    public static ObjectName getAppClientModules(final String pDomain, final String pServer, final String pApplication) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=AppClientModule");
            sb.append(",J2EEApplication=");
            if ((pApplication != null) && (pApplication.length() > 0)) {
                sb.append(pApplication);
            } else {
                sb.append(NONE);
            }
            sb.append(",J2EEServer=");
            sb.append(pServer);
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }
    /**
     * Create ObjectName for an AppClientModule
     * @param pDomain domain name
     * @param pServer server name
     * @param pApplication J2ee application name
     * @param pName module name
     * @return ObjectName for an AppClientModule
     */
    public static ObjectName getAppClientModule(final String pDomain, final String pServer, final String pApplication, final String pName) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=AppClientModule");
            sb.append(",name=");
            sb.append(pName);
            sb.append(",J2EEApplication=");
            if ((pApplication != null) && (pApplication.length() > 0)) {
                sb.append(pApplication);
            } else {
                sb.append(NONE);
            }
            sb.append(",J2EEServer=");
            sb.append(pServer);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }
    /**
     * Create OBJECT_NAME for an AppClientModule
     * @param pDomain domain name
     * @param pServer server name
     * @param pApplication J2ee application name
     * @param pName module name
     * @return OBJECT_NAME for an AppClientModule
     */
    public static String getAppClientModuleName(final String pDomain, final String pServer, final String pApplication, final String pName) {
        StringBuffer sb = new StringBuffer(pDomain);
        sb.append(":j2eeType=AppClientModule");
        sb.append(",name=");
        sb.append(pName);
        sb.append(",J2EEApplication=");
        if ((pApplication != null) && (pApplication.length() > 0)) {
            sb.append(pApplication);
        } else {
            sb.append(NONE);
        }
        sb.append(",J2EEServer=");
        sb.append(pServer);
        return sb.toString();
    }

    public static ObjectName getEJBModules() {
        try {
            StringBuffer sb = new StringBuffer(ALL);
            sb.append(":j2eeType=EJBModule");
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    public static ObjectName getEJBModules(final String pDomain) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=EJBModule");
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    public static ObjectName getEJBModules(final String pDomain, final String pServer) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=EJBModule");
            sb.append(",J2EEServer=");
            sb.append(pServer);
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    public static ObjectName getEJBModules(final String pDomain, final String pServer, final String pApplication) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=EJBModule");
            sb.append(",J2EEApplication=");
            if ((pApplication != null) && (pApplication.length() > 0)) {
                sb.append(pApplication);
            } else {
                sb.append(NONE);
            }
            sb.append(",J2EEServer=");
            sb.append(pServer);
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }
    /**
     * Create OBJECT_NAME for an EJB Module
     * @param pDomain domain name
     * @param pServer server name
     * @param pApplication j2eeapplication name
     * @param pName module name - currently the container name
     * @return created OBJECT_NAME
     */
    public static String getEJBModuleName(final String pDomain, final String pServer, final String pApplication, final String pName) {
        StringBuffer sb = new StringBuffer(pDomain);
        sb.append(":j2eeType=EJBModule");
        sb.append(",name=");
        sb.append(pName);
        sb.append(",J2EEApplication=");
        if ((pApplication != null) && (pApplication.length() > 0)) {
            sb.append(pApplication);
        } else {
            sb.append(NONE);
        }
        sb.append(",J2EEServer=");
        sb.append(pServer);
        return sb.toString();
    }

    /**
     * Create ObjectName for an EJB Module
     * @param pDomain domain name
     * @param pServer server name
     * @param pApplication j2eeapplication name
     * @param pName module name - currently the container name
     * @return created ObjectName
     */
    public static ObjectName getEJBModule(final String pDomain, final String pServer, final String pApplication, final String pName) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=EJBModule");
            sb.append(",name=");
            sb.append(pName);
            sb.append(",J2EEApplication=");
            if ((pApplication != null) && (pApplication.length() > 0)) {
                sb.append(pApplication);
            } else {
                sb.append(NONE);
            }
            sb.append(",J2EEServer=");
            sb.append(pServer);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }
    /**
     * Create ObjectName for EntityMBean
     * @param pDomain domain name
     * @param pModule name of the module containing the Entity
     * @param pServer server name
     * @param pApplication application name if any
     * @param pName MBean name
     * @return created ObjectName
     */
    public static ObjectName getEntityBean(final String pDomain, final String pModule, final String pServer, final String pApplication,
            final String pName) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=EntityBean");
            sb.append(",name=");
            sb.append(pName);
            sb.append(",EJBModule=");
            sb.append(pModule);
            sb.append(",J2EEApplication=");
            if ((pApplication != null) && (pApplication.length() > 0)) {
                sb.append(pApplication);
            } else {
                sb.append(NONE);
            }
            sb.append(",J2EEServer=");
            sb.append(pServer);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * Create OBJECT_NAME for EntityMBean
     * @param pDomain domain name
     * @param pModule name of the module containing the Entity
     * @param pServer server name
     * @param pApplication application name if any
     * @param pName MBean name
     * @return created OBJECT_NAME
     */
    public static String getEntityBeanName(final String pDomain, final String pModule, final String pServer, final String pApplication,
            final String pName) {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=EntityBean");
            sb.append(",name=");
            sb.append(pName);
            sb.append(",EJBModule=");
            sb.append(pModule);
            sb.append(",J2EEApplication=");
            if ((pApplication != null) && (pApplication.length() > 0)) {
                sb.append(pApplication);
            } else {
                sb.append(NONE);
            }
            sb.append(",J2EEServer=");
            sb.append(pServer);
            return sb.toString();
    }
    /**
     * Create ObjectName for all the EntityBean MBeans in a domain
     * @param pDomain domain name
     * @return created ObjectName
     */
    public static ObjectName getEntityBeans(final String pDomain) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=EntityBean");
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * Create ObjectName for all the EntityBean MBeans in a module
     * @param pDomain domain name
     * @param pModule name of the module containing the Entity
     * @return created ObjectName
     */
    public static ObjectName getEntityBeans(final String pDomain, final String pModule) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=EntityBean");
            sb.append(",EJBModule=");
            sb.append(pModule);
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * Create ObjectName for all the EntityBean MBeans in a module which is deployed in a given server
     * @param pDomain domain name
     * @param pModule name of the module containing the Entity
     * @param pServer the server name
     * @return created ObjectName
     */
    public static ObjectName getEntityBeans(final String pDomain, final String pModule, final String pServer) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=EntityBean");
            sb.append(",EJBModule=");
            sb.append(pModule);
            sb.append(",J2EEServer=");
            sb.append(pServer);
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * Create ObjectName for all the EntityBean MBeans in a module which is deployed in a given server
     * @param pDomain domain name
     * @param pModule name of the module containing the Entity
     * @param pServer the server name
     * @return created ObjectName
     */
    public static ObjectName getEntityBeans(final String pDomain, final String pModule, final String pServer, final String appName) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=EntityBean");
            sb.append(",EJBModule=");
            sb.append(pModule);
            sb.append(",J2EEApplication=");
            sb.append(appName);
            sb.append(",J2EEServer=");
            sb.append(pServer);
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }
    /**
     * Create ObjectName for StatefulSessionBean MBean
     * @param pDomain domain name
     * @param pModule name of the module containing the Entity
     * @param pServer server name
     * @param pApplication application name if any
     * @param pName MBean name
     * @return created OBJECT_NAME
     */
    public static ObjectName getStatefulSessionBean(final String pDomain, final String pModule, final String pServer,
            final String pApplication, final String pName) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=StatefulSessionBean");
            sb.append(",name=");
            sb.append(pName);
            sb.append(",EJBModule=");
            sb.append(pModule);
            sb.append(",J2EEApplication=");
            if ((pApplication != null) && (pApplication.length() > 0)) {
                sb.append(pApplication);
            } else {
                sb.append(NONE);
            }
            sb.append(",J2EEServer=");
            sb.append(pServer);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * Create OBJECT_NAME for StatefulSessionBean MBean
     * @param pDomain domain name
     * @param pModule name of the module containing the Entity
     * @param pServer server name
     * @param pApplication application name if any
     * @param pName MBean name
     * @return created OBJECT_NAME
     */
    public static String getStatefulSessionBeanName(final String pDomain, final String pModule, final String pServer,
            final String pApplication, final String pName) {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=StatefulSessionBean");
            sb.append(",name=");
            sb.append(pName);
            sb.append(",EJBModule=");
            sb.append(pModule);
            sb.append(",J2EEApplication=");
            if ((pApplication != null) && (pApplication.length() > 0)) {
                sb.append(pApplication);
            } else {
                sb.append(NONE);
            }
            sb.append(",J2EEServer=");
            sb.append(pServer);
            return sb.toString();
    }

    public static ObjectName getStatefulSessionBeans(final String pDomain) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=StatefulSessionBean");
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    public static ObjectName getStatefulSessionBeans(final String pDomain, final String pModule) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=StatefulSessionBean");
            sb.append(",EJBModule=");
            sb.append(pModule);
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    public static ObjectName getStatefulSessionBeans(final String pDomain, final String pModule, final String pServer) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=StatefulSessionBean");
            sb.append(",EJBModule=");
            sb.append(pModule);
            sb.append(",J2EEServer=");
            sb.append(pServer);
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    public static ObjectName getStatefulSessionBeans(final String pDomain, final String pModule, final String pServer, final String appName) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=StatefulSessionBean");
            sb.append(",EJBModule=");
            sb.append(pModule);
            sb.append(",J2EEApplication=");
            sb.append(appName);
            sb.append(",J2EEServer=");
            sb.append(pServer);
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * Create ObjectName for StatelessSessionBean MBean
     * @param pDomain domain name
     * @param pModule name of the module containing the Entity
     * @param pServer server name
     * @param pApplication application name if any
     * @param pName MBean name
     * @return created ObjectName
     */
    public static ObjectName getStatelessSessionBean(final String pDomain, final String pModule, final String pServer,
            final String pApplication, final String pName) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=StatelessSessionBean");
            sb.append(",name=");
            sb.append(pName);
            sb.append(",EJBModule=");
            sb.append(pModule);
            sb.append(",J2EEApplication=");
            if ((pApplication != null) && (pApplication.length() > 0)) {
                sb.append(pApplication);
            } else {
                sb.append(NONE);
            }
            sb.append(",J2EEServer=");
            sb.append(pServer);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * Create OBJECT_NAME for StatelessSessionBean MBean
     * @param pDomain domain name
     * @param pModule name of the module containing the Entity
     * @param pServer server name
     * @param pApplication application name if any
     * @param pName MBean name
     * @return created OBJECT_NAME
     */
    public static String getStatelessSessionBeanName(final String pDomain, final String pModule, final String pServer,
            final String pApplication, final String pName) {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=StatelessSessionBean");
            sb.append(",name=");
            sb.append(pName);
            sb.append(",EJBModule=");
            sb.append(pModule);
            sb.append(",J2EEApplication=");
            if ((pApplication != null) && (pApplication.length() > 0)) {
                sb.append(pApplication);
            } else {
                sb.append(NONE);
            }
            sb.append(",J2EEServer=");
            sb.append(pServer);
            return sb.toString();
    }

    public static ObjectName getStatelessSessionBeans(final String pDomain, final String pModule) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=StatelessSessionBean");
            sb.append(",EJBModule=");
            sb.append(pModule);
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    public static ObjectName getStatelessSessionBeans(final String pDomain) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=StatelessSessionBean");
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
             // this should never occur
            return null;
        }
    }


    public static ObjectName getStatelessSessionBeans(final String pDomain, final String pModule, final String pServer) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=StatelessSessionBean");
            sb.append(",EJBModule=");
            sb.append(pModule);
            sb.append(",J2EEServer=");
            sb.append(pServer);
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    public static ObjectName getStatelessSessionBeans(final String pDomain, final String pModule, final String pServer, final String pApp) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=StatelessSessionBean");
            sb.append(",EJBModule=");
            sb.append(pModule);
            sb.append(",J2EEApplication=");
            sb.append(pApp);
            sb.append(",J2EEServer=");
            sb.append(pServer);
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }
    /**
     * Create ObjectName for MessageDrivenBean MBean
     * @param pDomain domain name
     * @param pModule name of the module containing the Entity
     * @param pServer server name
     * @param pApplication application name if any
     * @param pName MBean name
     * @return created ObjectName
     */
    public static ObjectName getMessageDrivenBean(final String pDomain, final String pModule, final String pServer,
            final String pApplication, final String pName) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=MessageDrivenBean");
            sb.append(",name=");
            sb.append(pName);
            sb.append(",EJBModule=");
            sb.append(pModule);
            sb.append(",J2EEApplication=");
            if ((pApplication != null) && (pApplication.length() > 0)) {
                sb.append(pApplication);
            } else {
                sb.append(NONE);
            }
            sb.append(",J2EEServer=");
            sb.append(pServer);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }
    /**
     * Create OBJECT_NAME for MessageDrivenBean MBean
     * @param pDomain domain name
     * @param pModule name of the module containing the Entity
     * @param pServer server name
     * @param pApplication application name if any
     * @param pName MBean name
     * @return created OBJECT_NAME
     */
    public static String getMessageDrivenBeanName(final String pDomain, final String pModule, final String pServer,
            final String pApplication, final String pName) {
        StringBuffer sb = new StringBuffer(pDomain);
        sb.append(":j2eeType=MessageDrivenBean");
        sb.append(",name=");
        sb.append(pName);
        sb.append(",EJBModule=");
        sb.append(pModule);
        sb.append(",J2EEApplication=");
        if ((pApplication != null) && (pApplication.length() > 0)) {
            sb.append(pApplication);
        } else {
            sb.append(NONE);
        }
        sb.append(",J2EEServer=");
        sb.append(pServer);
        return sb.toString();
    }

    public static ObjectName getMessageDrivenBeans(final String pDomain) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=MessageDrivenBean");
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    public static ObjectName getMessageDrivenBeans(final String pDomain, final String pModule, final String pServer) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=MessageDrivenBean");
            sb.append(",EJBModule=");
            sb.append(pModule);
            sb.append(",J2EEServer=");
            sb.append(pServer);
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }


    public static ObjectName getMessageDrivenBeans(final String pDomain, final String pModule, final String pServer, final String appName) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=MessageDrivenBean");
            sb.append(",EJBModule=");
            sb.append(pModule);
            sb.append(",J2EEApplication=");
            sb.append(appName);
            sb.append(",J2EEServer=");
            sb.append(pServer);
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }
    public static ObjectName getMessageDrivenBeans(final String pDomain, final String pModule) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=MessageDrivenBean");
            sb.append(",EJBModule=");
            sb.append(pModule);
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    public static ObjectName getWebServices(final String pDomain, final String pServer) {
        //TODO: Add a proper web service mbean instead.
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":type=WebService");
            sb.append(",");
            sb.append(ALL);
            sb.append(",J2EEServer=");
            sb.append(pServer);
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    public static ObjectName getWebServices() {
        try {
            StringBuffer sb = new StringBuffer(ALL);
            sb.append(":type=WebService");
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }


    public static ObjectName JavaMailResource(final String pDomain, final String pName, final String pServer, final String p_Type) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=JavaMailResource");
            sb.append(",name=");
            sb.append(pName);
            sb.append(",J2EEServer=");
            sb.append(pServer);
            sb.append(",type=");
            sb.append(p_Type);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    public static ObjectName JavaMailResources(final String pDomain, final String pServer, final String p_Type) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=JavaMailResource");
            sb.append(",");
            sb.append(ALL);
            if (isValid(pServer)) {
                sb.append(",J2EEServer=");
                sb.append(pServer);
            }
            if (isValid(p_Type)) {
                sb.append(",type=");
                sb.append(p_Type);
            }
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * @param value String value to test.
     * @return Returns <code>true</code> if the value is not null and its length is > 0.
     */
    private static boolean isValid(final String value) {
        return ((value != null) && (value.length() > 0));
    }

    public static ObjectName JNDIResources(final String pDomain, final String pServer) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=JNDIResource");
            sb.append(",J2EEServer=");
            sb.append(pServer);
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    public static ObjectName getWebModules() {
        try {
            return new ObjectName("*:j2eeType=WebModule,*");
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    public static ObjectName getWebModules(final String pDomain) {
        try {
            return new ObjectName(pDomain + ":j2eeType=WebModule,*");
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    public static ObjectName getWebModules(final String pDomain, final String pServer) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=WebModule");
            if ((pServer != null) && (pServer.length() > 0)) {
                sb.append(",J2EEServer=");
                sb.append(pServer);
            }
            sb.append(",*");
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    public static ObjectName getWebModules(final String pDomain, final String pServer, final String pApplication) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=WebModule");
            if ((pServer != null) && (pServer.length() > 0)) {
                sb.append(",J2EEServer=");
                sb.append(pServer);
            }
            if ((pApplication != null) && (pApplication.length() > 0)) {
                sb.append(",J2EEApplication=");
                sb.append(pApplication);
            }
            sb.append(",*");
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    public static ObjectName getWebModule(final String pDomain, final String pServer, final String pApplication, final String pName) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=WebModule");
            sb.append(",name=");
            sb.append(pName);
            sb.append(",J2EEServer=");
            sb.append(pServer);
            sb.append(",J2EEApplication=");
            if ((pApplication != null) && (pApplication.length() > 0)) {
                sb.append(pApplication);
            } else {
                sb.append(NONE);
            }

            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    public static ObjectName getResourceAdapterModule(final String pDomain, final String pServer, final String pApplication,
            final String pName) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=ResourceAdapterModule");
            sb.append(",name=");
            sb.append(pName);
            sb.append(",J2EEServer=");
            sb.append(pServer);
            sb.append(",J2EEApplication=");
            if ((pApplication == null) || (pApplication.length() == 0)) {
                sb.append(NONE);
            } else {
                sb.append(pApplication);
            }
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    public static ObjectName getResourceAdapterModules(final String pDomain, final String pServer) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=ResourceAdapterModule");
            if ((pServer != null) && (pServer.length() > 0)) {
                sb.append(",J2EEServer=");
                sb.append(pServer);
            }
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    public static ObjectName getResourceAdapterModules(final String pDomain, final String pServer, final String pApplication) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=ResourceAdapterModule");
            if ((pServer != null) && (pServer.length() > 0)) {
                sb.append(",J2EEServer=");
                sb.append(pServer);
            }
            if ((pApplication != null) && (pApplication.length() > 0)) {
                sb.append(",J2EEApplication=");
                sb.append(pApplication);
            }
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    public static ObjectName getResourceAdapter(final String pDomain, final String pResourceAdapterModule, final String pApplication,
            final String pServer, final String pName) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=ResourceAdapter");
            sb.append(",name=");
            sb.append(pName);
            sb.append(",ResourceAdapterModule=");
            sb.append(pResourceAdapterModule);
            sb.append(",J2EEApplication=");
            if ((pApplication == null) || (pApplication.length() == 0)) {
                sb.append(NONE);
            } else {
                sb.append(pApplication);
            }
            sb.append(",J2EEServer=");
            sb.append(pServer);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    public static ObjectName getResourceAdapters(final String pDomain, final String pServer) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=ResourceAdapter");
            if ((pServer != null) && (pServer.length() > 0)) {
                sb.append(",J2EEServer=");
                sb.append(pServer);
            }
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    public static ObjectName getResourceAdapters(final String pDomain, final String pApplication, final String pServer) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=ResourceAdapter");
            sb.append(",J2EEApplication=");
            if ((pApplication == null) || (pApplication.length() == 0)) {
                sb.append(NONE);
            } else {
                sb.append(pApplication);
            }
            if ((pServer != null) && (pServer.length() > 0)) {
                sb.append(",J2EEServer=");
                sb.append(pServer);
            }
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }
    public static ObjectName getJCAResource(final String pDomain, final String pServer, final String pResourceAdapter, final String pName) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=JCAResource");
            sb.append(",name=");
            sb.append(pName);
            sb.append(",J2EEServer=");
            sb.append(pServer);
            sb.append(",ResourceAdapter=");
            sb.append(pResourceAdapter);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    public static ObjectName getJCAActivationSpec(final String pDomain, final String pJCAResource, final String pServer, final String pName) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=JCAActivationSpec");
            sb.append(",name=");
            sb.append(pName);
            sb.append(",JCAResource=");
            sb.append(pJCAResource);
            sb.append(",J2EEServer=");
            sb.append(pServer);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    public static ObjectName getJCAAdminObject(final String pDomain, final String pJCAResource, final String pServer, final String pName) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=JCAAdminObject");
            sb.append(",name=");
            sb.append(pName);
            sb.append(",JCAResource=");
            sb.append(pJCAResource);
            sb.append(",J2EEServer=");
            sb.append(pServer);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    public static ObjectName getJCAConnectionFactory(final String pDomain, final String pJCAResource, final String pServer,
            final String pName) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=JCAConnectionFactory");
            sb.append(",name=");
            sb.append(pName);
            sb.append(",JCAResource=");
            sb.append(pJCAResource);
            sb.append(",J2EEServer=");
            sb.append(pServer);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }
    public static ObjectName getJCAConnectionFactories(final String pDomain, final String pServer) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=JCAConnectionFactory");
            sb.append(",J2EEServer=");
            sb.append(pServer);
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }
    public static ObjectName getJCAManagedConnectionFactory(final String pDomain, final String pServer, final String pName) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=JCAManagedConnectionFactory");
            sb.append(",name=");
            sb.append(pName);
            sb.append(",J2EEServer=");
            sb.append(pServer);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    public static ObjectName JTAResource(final String pDomain, final String pServer, final String pName) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=JTAResource,name=JTAResource,J2EEServer=");
            sb.append(pServer);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * Create ObjectName for a JDBCResource MBean
     * @param pDomain domain name
     * @param pServer server name
     * @param pName MBean name
     * @return ObjectName for a JDBCResource MBean
     */
    public static ObjectName JDBCResource(final String pDomain, final String pServer, final String pName) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=JDBCResource");
            sb.append(",name=");
            sb.append(pName);
            sb.append(",J2EEServer=");
            sb.append(pServer);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }
    /**
     * Create OBJECT_NAME for a JDBCResource MBean
     * @param pDomain domain name
     * @param pServer server name
     * @param pName MBean name
     * @return OBJECT_NAME for a JDBCResource MBean
     */
    public static String JDBCResourceName(final String pDomain, final String pServer, final String pName) {
        StringBuffer sb = new StringBuffer(pDomain);
        sb.append(":j2eeType=JDBCResource");
        sb.append(",name=");
        sb.append(pName);
        sb.append(",J2EEServer=");
        sb.append(pServer);
        return sb.toString();
    }
    /**
     * Create ObjectName for a JDBCResource MBean using name="JDBCResource"
     * @param pDomain
     * @param pServer server name
     * @return ObjectName for a JDBCResource MBean
     */
    public static ObjectName JDBCResource(final String pDomain, final String pServer) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=JDBCResource");
            sb.append(",name=");
            sb.append("JDBCResource");
            sb.append(",J2EEServer=");
            sb.append(pServer);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * Create ObjectName for a JDBCDataSource MBeans in a JDBCResource named "JDBCResource"
     * @param pDomain domain name
     * @param pServer server name
     * @param pName JDBCDataSource name
     * @return ObjectName for a JDBCDataSource MBean
     */
    public static ObjectName getJDBCDataSource(final String pDomain, final String pServer, final String pName) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=JDBCDataSource");
            sb.append(",name=");
            sb.append(pName);
            sb.append(",JDBCResource=");
            sb.append("JDBCResource");
            sb.append(",J2EEServer=");
            sb.append(pServer);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * Create OBJECT_NAME for a JDBCDataSource MBeans in a JDBCResource named "JDBCResource"
     * @param pDomain domain name
     * @param pServer server name
     * @param pName JDBCDataSource name
     * @return OBJECT_NAME for a JDBCDataSource MBean
     */
    public static String getJDBCDataSourceName(final String pDomain, final String pServer, final String pName) {
        StringBuffer sb = new StringBuffer(pDomain);
        sb.append(":j2eeType=JDBCDataSource");
        sb.append(",name=");
        sb.append(pName);
        sb.append(",JDBCResource=");
        sb.append("JDBCResource");
        sb.append(",J2EEServer=");
        sb.append(pServer);
        return sb.toString();
    }
    /**
     * @param pDomain domain name
     * @param pServer server name
     * @return ObjectName for all JDBCDataSource MBeans in the JDBCResource of a given server
     */
    public static ObjectName getJDBCDataSources(final String pDomain, final String pServer) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=JDBCDataSource");
            sb.append(",JDBCResource=");
            sb.append("JDBCResource");
            if ((pServer != null) && (pServer.length() > 0)) {
                sb.append(",J2EEServer=");
                sb.append(pServer);
            }
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * This method is equivalent with the previous as in a server we currently have a
     * sole JDBCResource MBean.
     * @param pDomain domain name
     * @param pServer server name
     * @return ObjectName for all JDBCDataSource MBeans in a given server
     */
    public static ObjectName JDBCDataSources(final String pDomain, final String pServer) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=JDBCDataSource");
            sb.append(",J2EEServer=");
            sb.append(pServer);
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    public static ObjectName JDBCDataSources(final String pDomain, final String pJDBCResource, final String pServer) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=JDBCDataSource");
            sb.append(",JDBCResource=");
            sb.append(pJDBCResource);
            sb.append(",J2EEServer=");
            sb.append(pServer);
            sb.append(",");
            sb.append(ALL);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    public static String getJDBCDriverName(final String pDomain, final String pServer, final String pName) {
        StringBuffer sb = new StringBuffer(pDomain);
        sb.append(":j2eeType=JDBCDriver");
        sb.append(",name=");
        sb.append(pName);
        sb.append(",J2EEServer=");
        sb.append(pServer);
        return sb.toString();
    }

    public static ObjectName getJDBCDriver(final String pDomain, final String pServer, final String pName) {
        try {
            StringBuffer sb = new StringBuffer(pDomain);
            sb.append(":j2eeType=JDBCDriver");
            sb.append(",name=");
            sb.append(pName);
            sb.append(",J2EEServer=");
            sb.append(pServer);
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

}
