/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006-2007 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.lib.management.domain.proxy.clusterd;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Implements MBean interface for cluster daemon proxy MBeans. A cluster daemon proxy MBean represents
 * cluster daemon in the domain on the master server's MBean server.
 * @author Adriana Danes
 * @author eyindanga
 * @author THOMAS KOUASSI
 */
public interface ClusterDaemonProxyMBean {
    /**
     * @return the cluster daemon name
     */
    public String getName();

    /**
     * @param name
     */
    public void setName(String name);

    // this method will be used to save Cd config in the xml file
    public void save(String name);

    /**
     * @param serverName name of the server to be started
     * @param otherParams i.e jvm parameters
     * @return
     */
    public boolean startServer(String serverName, String otherParams);

    /**
     * @param serverName
     * @param otherParams
     * @return
     */
    public boolean stopServer(String serverName, String otherParams);

    /**
     * @param otherParams
     */
    public void startAllServers(String otherParams);

    /**
     * @param otherParams
     */
    public void stopAllServers(String otherParams);

    /**
     * @param name
     * @return
     */
    public int pingJOnAS(String name);

    /**
     * Reload clusterd configuration
     */
    public void reloadConfiguration();

    /**
     * @return
     */
    public String getHostName();

    /**
     * @return the server's state as known by the proxy
     */
    public String getState();

    /**
     * @return the URL of the current connection
     */
    String getConnectionUrl();

    /**
     * @return list of controlled servers.
     */
    @SuppressWarnings("unchecked")
    public ArrayList getControlledServersNames();

    /**
     * @return
     */
    public Hashtable<String, String> dynamicRemoteHostInfos();

    /**
     * @return the operatingSystemAvailableProcessors
     */
    public String getOperatingSystemAvailableProcessors();

    /**
     * @return the operatingSystemName
     */
    public String getOperatingSystemName();

    /**
     * @return the operatingSystemVersion
     */
    public String getOperatingSystemVersion();

    /**
     * @return the runTimeSpecVendor
     */
    public String getRunTimeSpecVendor();

    /**
     * @return the runTimeSpecVersion
     */
    public String getRunTimeSpecVersion();

    /**
     * @return the runTimeVmName
     */
    public String getRunTimeVmName();

    /**
     * @return the runTimeVmVendor
     */
    public String getRunTimeVmVendor();

    /**
     * @return the runTimeVmVersion
     */
    public String getRunTimeVmVersion();

    /**
     * @return the OperatingSystem Architecture
     */
    public String getOperatingSystemArch();

    /**
     * Getting remote Vm used Memory
     * @return the value of current used memory
     */
    public String getVmCurrentUsedMemory();

    /**
     * Getting remote Vm Total Memory
     * @return the value of Vm Total memory
     */
    public String getVmTotalMemory();

    /**
     * Getting remote Vm's Current used Heap memory
     * @return the value of Vm's Current used Heap memory
     */
    public String getVmCurrentUsedHeapMemory();

    /**
     * Getting remote Vm's Current used non Heap memory
     * @return the value of Vm's Current used non Heap memory
     */
    public String getVmCurrentUsedNonHeapMemory();

    /**
     * Getting Operating system Current used space
     * @return the value of Operating system Current used space
     */
    public String getOsCurrentUsedSpace();

    /**
     * Getting Operating system Current used space
     * @return the value of Operating system Total space
     */
    public String getOsTotalSpace();

    /**
     * Add a server to cluster daemon control.
     * @param name the server name
     * @param description server description
     * @param javaHome path to JRE
     * @param jonasRoot path to bin repository
     * @param JonasBase path to lib repository
     * @param xprem extra parameter e.g: -Djava.net.preferIPv4Stack=true
     * @param autoBoot true if the server is launched when cluster daemon starts
     * @param jonasCmd user command
     * @param saveIt True to flush clusterd configuration
     */
    @SuppressWarnings("unchecked")
    public void addServer(String name, String description, String jonasRoot, String jonasBase, String javaHome, String xprem,
            String autoBoot, String jonasCmd, String saveIt);

    /**
     * Remove This server from cluster daemon control.
     * @param serverName The server to remove
     * @param saveIt True to flush clusterd configuration
     */
    public void removeServer(String serverName, String saveIt);

}
