/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.management.domain;

import java.io.UnsupportedEncodingException;

import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;
import org.ow2.jonas.lib.util.Base64;
import org.ow2.jonas.lib.util.Log;

/**
 * Stores the authentication information for a server.
 * @author S. Ali Tokmen
 */
public final class AuthenticationInformation {
    /**
     * Logger for warnings.
     */
    private static Logger logger = Log.getLogger("org.ow2.jonas.management.domain");

    /**
     * User name.
     */
    private String username;

    /**
     * Password.
     */
    private String password;

    /**
     * Saves the authentication information.
     * @param username  user name
     * @param password  password, may be encoded in the following formats:
     *                      - None, don't prefix at all
     *                      - Encoded using base64, prefix with {base64}
     * @throws UnsupportedEncodingException  If password is not base64 encoded UTF-8
     */
    public AuthenticationInformation(String username, String password) throws UnsupportedEncodingException {
        if (password.startsWith("{base64}")) {
            try {
                new String(Base64.decode(password.substring("{base64}".length()).toCharArray()), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                throw new UnsupportedEncodingException("The given password is not a UTF-8 string encoded in base64");
            }
        } else {
            logger.log(BasicLevel.WARN, "The password for the user " + username + " is in clear text!");
        }
        this.password = password;
        this.username = username;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @return the password, original format
     */
    public String getPassword() {
        return password;
    }

    /**
     * @return the password, decoded format
     * @throws UnsupportedEncodingException  Shouldn't be thrown since
     *                                       constructor already checks for it.
     */
    public String decodePassword() throws UnsupportedEncodingException {
        if (password.startsWith("{base64}")) {
            return new String(Base64.decode(password.substring("{base64}".length()).toCharArray()), "UTF-8");
        } else {
            return password;
        }
    }

    /**
     * Encode a password.
     *
     * @param password  Password to encode.
     *
     * @return Encoded password.
     *
     * @throws UnsupportedEncodingException  If password is not UTF-8 encoded.
     */
    public static String encodePassword(String password) throws UnsupportedEncodingException {
        return "{base64}" + new String(Base64.encode(password.getBytes("UTF-8")));
    }
}
