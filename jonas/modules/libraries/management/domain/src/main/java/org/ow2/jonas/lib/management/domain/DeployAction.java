/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.management.domain;

import org.ow2.jonas.lib.management.domain.proxy.server.ServerProxy;

/**
 * Action of deployment on a remote server
 * Used by jonasAdmin to follow the progression of operations
 * @author durieuxp
 */
public class DeployAction {

    /**
     * Current state of the operation
     */
    private int state = FAILED;
    public static final int FAILED = -1;
    public static final int SUCCESS = 0;
    public static final int UPLOADING = 1;
    public static final int DEPLOYING = 2;
    public static final int UNDEPLOYING = 3;

    /**
     * Error message when failed
     */
    private String errorMessage;

    private ServerProxy proxy;
    private String filename;

    /**
     * Deployment Action
     */
    private int action;
    public static final int DEPLOY = 1;
    public static final int UNDEPLOY = 2;
    public static final int UPLOAD = 3;
    public static final int UPLOADDEPLOY = 4;
    public static final int START = 5;
    public static final int STOP = 6;

    /**
     * Constructor
     */
    public DeployAction(ServerProxy proxy, String filename, int action) {
        this.proxy = proxy;
        this.filename = filename;
        this.action = action;
        switch (action) {
            case DEPLOY:
                state = DEPLOYING;
                break;
            case UNDEPLOY:
                state = UNDEPLOYING;
                break;
            case UPLOAD:
            case UPLOADDEPLOY:
                state = UPLOADING;
                break;
        }
    }

    /**
     * Action is successful
     */
    public void setOK() {
        state = SUCCESS;
    }

    /**
     * Set state to Deploying
     */
    public void setDeploying() {
        state = DEPLOYING;
    }

    /**
     * Action has failed
     */
    public void setError(String mess) {
        state = FAILED;
        errorMessage = mess;
    }

    /**
     * @return Current state of the action
     */
    public int getState() {
        return state;
    }

    /**
     * @return Current state of the action (String form)
     */
    public String getStateAsString() {
        switch (state) {
            case FAILED:
                return "FAILED :";
            case SUCCESS:
                return "SUCCESS";
            case DEPLOYING:
                return "IN PROGRESS : DEPLOY";
            case UNDEPLOYING:
                return "IN PROGRESS : UNDEPLOY";
            case UPLOADING:
                return "IN PROGRESS : UPLOAD";
        }
        return "error";
    }

    /**
     * @return The error message when failed
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * @return The deployment action
     */
    public int getAction() {
        return action;
    }

    /**
     * @return Filename used for deployment action
     */
    public String getFileName() {
        return filename;
    }

    /**
     * @return servername
     */
    public String getServerName() {
        return proxy.getName();
    }

}