/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.management.domain.cluster.tomcat;


import javax.management.JMException;
import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.ow2.jonas.lib.management.domain.cluster.BaseCluster;
import org.ow2.jonas.lib.management.domain.cluster.ClusterMember;
import org.ow2.jonas.lib.management.domain.proxy.server.ServerProxy;
import org.ow2.jonas.lib.util.JonasObjectName;


import org.objectweb.util.monolog.api.BasicLevel;

/**
 * Implements Tomcat Cluster MBean
 * @author Adriana Danes, Philippe Durieux
 */
public class TomcatCluster extends BaseCluster implements TomcatClusterMBean {

    private String host = null;
    /**
     * The type of Cluster, that is part of the MBean ObjectName
     */
    protected String type = "TomcatCluster";

    /**
     *
     */
    private String mcastAddr = null;

    /**
     *
     */
    private long mcastDropTime;

    /**
     *
     */
    private long mcastFrequency;

    /**
     *
     */
    private int mcastPort;

    /**
     *
     */
    private int mcastSocketTimeout;

    /**
     * Tomcat cluster constructor
     * @param cf ClusterFactory
     * @throws JMException could not create MBean instance
     */
    public TomcatCluster(final TomcatClusterFactory cf) throws JMException {
        super(cf);
    }

    /**
     * Should never be called, as by definition, we can't create a 'physical' cluster
     * member via an administration program or a tool.
     * (only automatic creation is possible via addTomcatServer method)
     */
    @Override
    public ClusterMember createClusterMember(final String svname, final ServerProxy proxy) {
        logger.log(BasicLevel.WARN, "Cannot correctly create tomcat cluster member " + svname);
        return new TomcatClusterMember(svname, host, proxy);
    }

    // --------------------------------------------------------------------------
    // Other public methods
    // --------------------------------------------------------------------------

    /**
     * @return The String type to be put in the ObjectName
     */
    @Override
    public String getType() {
        return type;
    }

    /**
     * Add a Tomcat Server to the list of the TomcatCluster.
     * Make link between the member and the ServerProxy.
     * @param serverName name of the managed server which corresponds to a Tomcat session replication cluster memeber
     * @param proxy The ServerProxy related object.
     * @return True if correctly added in the List.
     */
    public boolean addTomcatServer(final String serverName, final ServerProxy proxy) {
        // Create the TomcatClusterMember instance ( = a tomcat server)
        String domainName = proxy.getDomain();
        TomcatClusterMember tomcat = new TomcatClusterMember(serverName, host, proxy);
        // Set configuration parameters
        tomcat.setInfo();
        // Add this member if not already there
        boolean added = addMember(tomcat);
        if (added) {
            // Build the ObjectName and register MBean
            try {
                ObjectName on = JonasObjectName.clusterMember(domainName, serverName, getType(), name);
                tomcat.setObjectName(on);
                MBeanServer mbeanServer = jmx.getJmxServer();
                if (mbeanServer.isRegistered(on)) {
                    mbeanServer.unregisterMBean(on);
                }
                mbeanServer.registerMBean(tomcat, on);
            } catch (JMException e) {
                logger.log(BasicLevel.WARN, "Cannot register tomcat " + serverName + ": " + e);
            }
        }
        return added;
    }

    public String getMcastAddr() {
        return mcastAddr;
    }
    public long getMcastDropTime() {
        return mcastDropTime;
    }
    public long getMcastFrequency() {
        return mcastFrequency;
    }
    public int getMcastPort() {
        return mcastPort;
    }
    public int getMcastSocketTimeout() {
        return mcastSocketTimeout;
    }
    public void setMcastAddr(final String mcastAddr) {
        this.mcastAddr = mcastAddr;
    }

    public void setMcastDropTime(final long mcastDropTime) {
        this.mcastDropTime = mcastDropTime;
    }

    public void setMcastFrequency(final long mcastFrequency) {
        this.mcastFrequency = mcastFrequency;
    }

    public void setMcastPort(final int mcastPort) {
        this.mcastPort = mcastPort;
    }

    public void setMcastSocketTimeout(final int mcastSocketTimeout) {
        this.mcastSocketTimeout = mcastSocketTimeout;
    }

    public String getHost() {
        return host;
    }

    public void setHost(final String host) {
        this.host = host;
    }

}
