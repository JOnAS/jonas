/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.management.domain.cluster.ha;


import javax.management.JMException;
import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.objectweb.util.monolog.api.BasicLevel;
import org.ow2.jonas.lib.management.domain.cluster.BaseCluster;
import org.ow2.jonas.lib.management.domain.cluster.ClusterMember;
import org.ow2.jonas.lib.management.domain.proxy.server.ServerProxy;
import org.ow2.jonas.lib.util.JonasObjectName;

/**
 * Implements Tomcat Cluster MBean
 * @author Philippe Durieux
 */
public class EjbHaCluster extends BaseCluster implements EjbHaClusterMBean {

    /**
     * The type of Cluster, that is part of the MBean ObjectName
     */
    protected String type = "EjbHaCluster";

    /**
     * multicast addr
     */
    private String mcastAddr = null;

    /**
     * multicast port
     */
    private int mcastPort;

    /**
     * HA cluster constructor
     * @param cf ClusterFactory
     * @throws JMException could not create MBean instance
     */
    public EjbHaCluster(final EjbHaClusterFactory cf) throws JMException {
        super(cf);
    }

    @Override
    public ClusterMember createClusterMember(final String svname, final ServerProxy proxy) {
        return new EjbHaClusterMember(svname, proxy);
    }

    // --------------------------------------------------------------------------
    // Other public methods
    // --------------------------------------------------------------------------

    /**
     * @return The String type to be put in the ObjectName
     */
    @Override
    public String getType() {
        return type;
    }

    public String getMcastAddr() {
        return mcastAddr;
    }

    public void setMcastAddr(final String mcastAddr) {
        this.mcastAddr = mcastAddr;
    }

    public int getMcastPort() {
        return mcastPort;
    }

    public void setMcastPort(final int mcastPort) {
        this.mcastPort = mcastPort;
    }

    /**
     * Add a CMI Server to the list of the Cluster
     * Make link between the member and the ServerProxy.
     * @param serverName name of the managed server which corresponds to a Tomcat session replication cluster memeber
     * @param proxy The ServerProxy related object.
     * @return True if correctly added in the List.
     */
    public boolean addHaServer(final String serverName, final ServerProxy proxy) {

        // create the EjbHaClusterMember instance ( = a jonas server)
        EjbHaClusterMember ha = new EjbHaClusterMember(serverName, proxy);

        // Add this member if not already there
        boolean added = addMember(ha);
        if (added) {
            // Build the ObjectName and register MBean
            try {
                ObjectName on = JonasObjectName.clusterMember(domainName, serverName, getType(), name);
                ha.setObjectName(on);
                MBeanServer mbeanServer = jmx.getJmxServer();
                if (mbeanServer.isRegistered(on)) {
                    mbeanServer.unregisterMBean(on);
                }
                mbeanServer.registerMBean(ha, on);
            } catch (JMException e) {
                logger.log(BasicLevel.WARN, "Cannot register tomcat " + serverName + ": " + e);
            }
        }
        return added;
    }

}
