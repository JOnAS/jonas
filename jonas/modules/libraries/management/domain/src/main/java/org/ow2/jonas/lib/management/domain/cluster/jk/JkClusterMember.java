/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id:Worker.java 8660 2006-06-20 08:15:28Z danesa $
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.management.domain.cluster.jk;

import org.ow2.jonas.lib.management.domain.cluster.ClusterMember;
import org.ow2.jonas.lib.management.domain.proxy.server.ServerProxy;



/**
 * JkCluster members are actually workers.
 * A worker represents a JOnAS server which plays the role of worker in a
 * Mod_jk loadbalancing cluster.
 * @author Adriana Danes
 */
public class JkClusterMember extends ClusterMember implements JkClusterMemberMBean {

    /**
     * The worker's host
     */
    private String host = null;

    /**
     * The worker's port
     */
    private int port;

    /**
     * The worker's load balance factor
     */
    private int lbFactor;

    /**
     * worker type
     */
    private String type;

    /**
     * load balancing factor, as defined in workers.properties
     */
    private int lbfactor;


    /**
     * Constructor
     * @param name the worker name as defined in worker.properties
     * @param proxy related Server Proxy
     */
    public JkClusterMember(String name, ServerProxy proxy) {
        super(name, proxy);
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getLbfactor() {
        return lbfactor;
    }

    public void setLbfactor(int lbfactor) {
        this.lbfactor = lbfactor;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getLbFactor() {
        return lbFactor;
    }

    public void setLbFactor(int lbFactor) {
        this.lbFactor = lbFactor;
    }
}