/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.lib.management.domain.cluster.tomcat;

import org.ow2.jonas.lib.management.domain.cluster.ClusterMemberMBean;


/**
 * Define here only specific operations and attributes.
 * Generic one are in ClusterMemberMBean
 * @author Adriana Danes, Philippe Durieux
 */
public interface TomcatClusterMemberMBean  extends ClusterMemberMBean {

    /**
     * @return tcp listener address
     */
    String getTcpListenAddress();

    /**
     * @return tcp listener port
     */
    int getTcpListenPort();

    /**
     * @param tcpListenAddress the tcp ListenAddress
     */
    void setTcpListenAddress(String tcpListenAddress);

    /**
     * @param tcpListenPort the tcp ListenPort
     */
    void setTcpListenPort(int tcpListenPort);

    boolean isCompress();
    boolean isDoReceivedProcessingStats();
    String getReceiverInfo();
    boolean isSendAck();
    long getTcpSelectorTimeout();
    String getHostName();
    double getAvgReceivedProcessingTime();
    long getMaxReceivedProcessingTime();
    long getMinReceivedProcessingTime();
    long getNrOfMsgsReceived();
    long getReceivedProcessingTime();
    long getReceivedTime();
    boolean isDoListen();
    int getTcpThreadCount();
    long getTotalReceivedBytes();


    String getSenderInfo();
    long getAckTimeout();
    boolean isAutoConnect();
    boolean isDoTransmitterProcessingStats();
    String getReplicationMode();
    boolean isWaitForAck();
}
