/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.management.domain.cluster;

import javax.management.JMException;
import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.ow2.jonas.lib.management.domain.proxy.server.ServerProxy;
import org.ow2.jonas.lib.util.Log;

import org.objectweb.util.monolog.api.Logger;


/**
 * A ClusterMember object refers to the ServerProxy corresponding
 * to the managed server which is being member of a given cluster.
 * There may be several ClusterMember instances for the same ServerProxy object.
 * @author Philippe Durieux
 */
public abstract class ClusterMember {

    /**
     * Name of this Member. Depend on the derived class.
     */
    protected String name;

    /**
     * MBean ObjectName
     */
    protected ObjectName objectName = null;

    /**
     * The ServerProxy representing the managed server.
     */
    protected ServerProxy proxy = null;

    /**
     * MBeanServer where the MBean is registered.
     */
    protected MBeanServer mbeanServer = null;

    /**
     * logger for traces
     */
    protected static Logger logger = Log.getLogger("org.ow2.jonas.management.domain");
    /**
     * Constructor in case of Remote Server
     * @param name serverName
     * @param proxy The ServerProxy representing the managed server.
     */
    public ClusterMember(final String name, final ServerProxy proxy) {
        this.name = name;
        this.proxy = proxy;
    }

    /**
     * @return the MBeanServer reference
     */
    public MBeanServer getMbeanServer() {
        return mbeanServer;
    }

    /**
     * Set the MBeanServer reference
     * @param mbeanServer MBeanServer reference
     */
    public void setMbeanServer(final MBeanServer mbeanServer) {
        this.mbeanServer = mbeanServer;
    }

    /**
     * @return the member state if it is connected to a ServerProxy as a String
     * @throws JMException could not get ServerProxy state because of a JMX exception
     */
    public String getState() {
        String state = "NOT_CONNECTED";
        if (proxy != null) {
            state = proxy.getState();
        }
        return state;
    }

    /**
     * @return name of this member
     */
    public String getName() {
        return name;
    }

    /**
     * Get the Mbean ObjectName (for jonasAdmin)
     * @return ObjectName of this member
     */
    public ObjectName getObjectName() {
        return objectName;
    }

    /**
     * Set the ObjectName. Should be called when the MBean is registered.
     */
    public void setObjectName(final ObjectName on) {
        objectName = on;
    }

    /**
     * @return The ServerProxy OBJECT_NAME referenced by this cluster member
     */
    public String getServerProxy() {
        return proxy.getObjectName();
    }

    /**
     * @return The proxy pointed by this Member
     */
    public ServerProxy getProxy() {
        return proxy;
    }

    /**
     * The JOnAS server's name which is represented by the proxy, null if the memeber
     * is not connected to a proxy.
     * @return the JOnAS server's name which is represented by the proxy
     * @throws JMException could not get serverName from the ServerProxy because of a JMX exception
     */
    public String getServerName() throws JMException {
        String serverName = null;
        if (proxy != null) {
            serverName = proxy.getServerName();
        }
        return serverName;
    }

}
