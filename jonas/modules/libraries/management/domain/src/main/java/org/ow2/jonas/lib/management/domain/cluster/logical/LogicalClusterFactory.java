/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.management.domain.cluster.logical;

import java.util.Collection;
import java.util.HashMap;

import javax.management.JMException;
import javax.management.ObjectName;

import org.ow2.jonas.lib.management.domain.DomainMonitor;
import org.ow2.jonas.lib.management.domain.cluster.BaseCluster;
import org.ow2.jonas.lib.management.domain.cluster.ClusterFactory;
import org.ow2.jonas.lib.management.domain.proxy.server.ServerProxy;


import org.objectweb.util.monolog.api.BasicLevel;

/**
 * Factory for logical clusters.
 */
public class LogicalClusterFactory extends ClusterFactory {

    /**
     * List of the logical clusters in this domain.
     * key = clusterName, value = LogicalCluster
     */
    private HashMap<String, BaseCluster>  myclusters = new HashMap<String, BaseCluster>();

    /**
     * Constructor for a LogicalClusterFactory.
     * @param dm Reference to the DomainMonitor
     */
    public LogicalClusterFactory(final DomainMonitor dm) {
        super(dm);
    }

    /**
     * @return The corresponding cluster object.
     */
    @Override
    public BaseCluster getCluster(final String name) {
        return myclusters.get(name);
    }

    @Override
    /**
     * @return The all the cluster objects
     */
    public Collection<BaseCluster> getClusterList() {
        return myclusters.values();
    }
    /**
     * Treat event corresponding to a new ServerProxyy (new server arriving in the domain).
     * Add the ServerProxy to the domain's LogicalCluster.
     * @param proxy ServerProxy that has been detected
     * @return true if
     */
    @Override
    public boolean notifyServer(final ServerProxy proxy) {
        String serverName = proxy.getServerName();
        logger.log(BasicLevel.DEBUG, serverName);

        LogicalCluster domaincluster = (LogicalCluster) myclusters.get(domainName);
        if (domaincluster == null) {
            domaincluster = createLogicalCluster(domainName);
            if (domaincluster == null) {
                return false;
            }
        }

        // add a server to the default cluster
        return domaincluster.addServer(serverName, proxy);
    }

    /**
     * Create a logical cluster.
     * @param name the cluster name.
     * @return LogicalCluster reference.
     */
    public LogicalCluster createLogicalCluster(final String name) {
        LogicalCluster cluster = null;
        ObjectName clOn = null;
        try {
            cluster = new LogicalCluster(this);
            clOn = cluster.setName(name);
        } catch (JMException e) {
            logger.log(BasicLevel.ERROR, "Cannot create LogicalCluster:" + e);
            return null;
        }
        // Register the MBean if not done
        if (!mbeanServer.isRegistered(clOn)) {
            try {
                // Register a MBean is for each this cluster
                logger.log(BasicLevel.DEBUG, "Resister Cluster MBean : "  + clOn);
                mbeanServer.registerMBean(cluster, clOn);
            } catch (Exception e) {
                logger.log(BasicLevel.ERROR, "Cannot register cluster:" + e);
                return null;
            }
        }
        if (logger.isLoggable(BasicLevel.DEBUG)) {
            logger.log(BasicLevel.DEBUG, "Adding cluster : "  + name);
        }
        myclusters.put(name, cluster);
        return cluster;
    }

    /**
     * Update dynamic info for all the ha clusters.
     */
    @Override
	public void getMonitoringInfo() {
        // Not implemented.
    }
}
