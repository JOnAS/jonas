/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.lib.management.domain.cluster;

import javax.management.JMException;

/**
 * Implements MBean interface for different Cluster MBean types
 * @author Adriana Danes
 * @author S. Ali Tokmen
 */
public interface BaseClusterMBean {

    /**
     * @return the cluster's current state
     */
    String getState();

    /**
     * @return the cluster name
     */
    String getName();

    /**
     * @return the members number
     */
    int getNbMembers();

    /**
     * @return the member OBJECT_NAME's
     */
    String[] getMembers();

    /**
     * Calls {@link BaseClusterMBean#addServer(String, String[], String, String, String)}(name, urls, clusterd, null, null).
     * @param name logical name of the server
     * @param urls array of urls for connection
     * @param clusterd possible clusterdaemon managing the server
     * @deprecated {@link BaseClusterMBean#addServer(String, String[], String, String, String)}
     * @throws JMException problem when trying to a server to a cluster
     */
    @Deprecated
    public void addServer(String name, String [] urls, String clusterd) throws JMException;

    /**
     * Add a new server in the cluster (jonasAdmin).
     * @param name logical name of the server
     * @param urls array of urls for connection
     * @param clusterd possible clusterdaemon managing the server
     * @param username user name to use when connecting if any. Null otherwise.
     * @param password password to use when connecting if any. Null otherwise.
     * @throws JMException problem when trying to a server to a cluster
     */
    void addServer(String name, String [] urls, String clusterd, String username, String password) throws JMException;

    /**
     * remove a server from the cluster (jonasAdmin).
     * @param name logical name of the server
     * @throws JMException problem when trying to a server to a cluster
     */
    void removeServer(String name) throws JMException;

    void startit() throws JMException;
    void stopit() throws JMException;
    void deployModule(String filename) throws JMException;
    void undeployModule(String filename) throws JMException;
    void uploadFile(String filename, boolean replaceExisting) throws JMException;
    void uploadDeployModule(String filename, boolean replaceExisting) throws JMException;
}
