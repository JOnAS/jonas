/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.lib.management.domain.proxy.server;

public enum ServerProxyProperties {
    /**
     * Standby option.
     */
    STANDBY_OPTION("-standby"),
    /**
     * Clean option.
     */
    CLEAN_OPTION("-clean"),

    /**
     * Key for start operation.
     */
    START_OPERATION_KEY("start"),
    /**
     * Key for stop operation.
     */
    STOP_OPERATION_KEY("stop"),

    /**
     * Key for undeploy operation.
     */
    UNDEPLOY_OPERATION_KEY("undeploy"),

    /**
     * Key for deploy operation.
     */
    DEPLOY_OPERATION_KEY("deploy"),

    /**
     * Key for sendFile operation.
     */
    SENDFILE_OPERATION_KEY("sendFile"),

    /**
     * Key for state attribute.
     */
    STATE_ATTRIBUTE_KEY("state"),

    /**
     * Key for halt operation.
     */
    HALT_OPERATION_KEY("halt");

    /**
     * A name of property.
     */
    private final String propertyName;

    /**
     * Default constructor.
     * @param propertyName name of property
     */
    ServerProxyProperties(final String propertyName) {
        this.propertyName = propertyName;
    }

    /**
     * @return the name of property.
     */
    @Override
    public String toString() {
        return propertyName;
    }

    /**
     * @return the name of property.
     */
    public String getPropertyName() {
        return propertyName;
    }


}
