/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.lib.management.domain.proxy.server;

import org.ow2.jonas.lib.management.domain.proxy.clusterd.ClusterDaemonProxy;



/**
 * Implements MBean interface for proxy MBeans. A proxy MBean represents a server in
 * the domain on the master server's MBean server.
 * @author Adriana Danes
 */
public interface ServerProxyMBean {

    /**
     * Start server.
     * @param standby true to enter standby mode.
     */
    void start(boolean standby);
    /**
     * Stop server.
     * @param standby true to enter standby mode.
     */
    void stop(boolean standby);

    void stop();

    public void deployModule(String filename);
    public void undeployModule(String filename);
    public void uploadFile(String filename, boolean replaceExisting);
    public void uploadDeployModule(String filename, boolean replaceExisting);

    public String getClusterDaemonName();

    public int getAllThreadsCount();

    public int getConnectionFailuresJCAConnection();

    public int getConnectionLeaksJCAConnection();

    public int getCurrentBusyJCAConnection();

    public int getCurrentNumberOfEntityBean();

    public int getCurrentNumberOfEJB();

    public int getCurrentNumberOfMDB();

    public int getCurrentNumberOfSBF();

    public int getCurrentNumberOfSBL();

    public int getCurrentOpenedJCAConnection();

    boolean getTomcat();

    public int getCurrentThreadBusyByConnectorTomcat();

    public int getCurrentThreadCountByConnectorTomcat();

    public int getRequestCountByConnectorTomcat();

    public int getErrorCountByConnectorTomcat();

    public int getMaxThreadsByConnectorTomcat();

    public long getProcessingTimeByConnectorTomcat();

    public long getBytesReceivedByConnectorTomcat();

    public long getBytesSentByConnectorTomcat();

    public Long getCurrentTotalMemory();

    public Long getCurrentUsedMemory();

    public String getHostName();

    public String getJavaVendor();

    public String getJavaVersion();

    public boolean getJmsJoram();

    public int getJmsQueuesNbMsgsDeliverSinceCreation();

    public int getJmsQueuesNbMsgsReceiveSinceCreation();

    public int getJmsQueuesNbMsgsSendToDMQSinceCreation();

    public int getJmsTopicsNbMsgsDeliverSinceCreation();

    public int getJmsTopicsNbMsgsReceiveSinceCreation();

    public int getJmsTopicsNbMsgsSendToDMQSinceCreation();

    public String getJOnASVersion();

    public String getLoadCPU();

    boolean getWorkers();

    public int getMinWorkerPoolSize();

    public int getMaxWorkerPoolSize();

    public int getCurrentWorkerPoolSize();

    public String getProtocols();

    public int getRejectedOpenJCAConnection();

    boolean getJcaConnection();

    boolean getJdbcDatasource();
    /**
     * @return the server's state as known by the proxy
     */
    String getState();

    boolean getTransaction();

    public int getTotalBegunTransactions();

    public int getTotalCommittedTransactions();

    public int getTotalCurrentTransactions();

    public int getTotalExpiredTransactions();

    public int getTotalRolledbackTransactions();

    public int getWaiterCountJCAConnection();

    public long getWaitingTimeJCAConnection();

    public int getServedOpenJCAConnection();

    int getConnectionFailuresJDBCResource();
    int getConnectionLeaksJDBCResource();
    int getCurrentBusyJDBCResource();
    int getCurrentOpenedJDBCResource();
    int getRejectedOpenJDBCResource();
    int getServedOpenJDBCResource();
    int getWaiterCountJDBCResource();
    long getWaitingTimeJDBCResource();

    public String getObjectName();

    public String getConnectionUrl();

    /**
     * @return the description
     */
    public String getDescription();


    /**
     * @param description the description to set
     */
    public void setDescription(String description);


    /**
     * @return the javaHome
     */
    public String getJavaHome();


    /**
     * @param javaHome the javaHome to set
     */
    public void setJavaHome(String javaHome);


    /**
     * @return the jonasRoot
     */
    public String getJonasRoot();


    /**
     * @param jonasRoot the jonasRoot to set
     */
    public void setJonasRoot(String jonasRoot);


    /**
     * @return the jonasBase
     */
    public String getJonasBase();


    /**
     * @param jonasBase the jonasBase to set
     */
    public void setJonasBase(String jonasBase);


    /**
     * @return the xprem
     */
    public String getXprem();


    /**
     * @param xprem the xprem to set
     */
    public void setXprem(String xprem);


    /**
     * @return the autoBoot
     */
    public String getAutoBoot();


    /**
     * @param autoBoot the autoBoot to set
     */
    public void setAutoBoot(String autoBoot);

    /**
     *set server cluster daemon
     */
    public void setClusterdaemon(ClusterDaemonProxy clusterdaemon);


}
