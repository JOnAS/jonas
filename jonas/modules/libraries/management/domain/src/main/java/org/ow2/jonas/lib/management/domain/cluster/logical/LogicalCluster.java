/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.management.domain.cluster.logical;


import javax.management.JMException;
import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.objectweb.util.monolog.api.BasicLevel;
import org.ow2.jonas.lib.management.domain.cluster.BaseCluster;
import org.ow2.jonas.lib.management.domain.cluster.BaseClusterMBean;
import org.ow2.jonas.lib.management.domain.cluster.ClusterFactory;
import org.ow2.jonas.lib.management.domain.cluster.ClusterMember;
import org.ow2.jonas.lib.management.domain.proxy.server.ServerProxy;
import org.ow2.jonas.lib.util.JonasObjectName;

/**
 * Logical Cluster: may be created by jonasAdmin, or declared in domain.xml
 * A default logical cluster is created for each domain, it holds all the
 * servers in the domain.
 * @author Adriana Danes
 * @author Philippe Durieux
 */
public class LogicalCluster extends BaseCluster implements BaseClusterMBean {

    /**
     * The type of Cluster, that is part of the MBean ObjectName
     */
    protected String type = "LogicalCluster";


    /**
     * logical cluster constructor
     * @param cf ClusterFactory
     * @throws JMException could not create MBean instance
     */
    public LogicalCluster(final ClusterFactory cf) throws JMException {
        super(cf);
    }

    @Override
    public ClusterMember createClusterMember(final String svname, final ServerProxy proxy) {
        return new LogicalClusterMember(svname, proxy);
    }

    // --------------------------------------------------------------------------
    // Other public methods
    // --------------------------------------------------------------------------

    /**
     * @return The String type to be put in the ObjectName
     */
    @Override
    public String getType() {
        return type;
    }

    /**
     * Add a Server to the list.
     * Make link between the member and the ServerProxy.
     * @param serverName name of the managed server
     * @param proxy The ServerProxy related object.
     * @return True if correctly added in the List.
     */
    public boolean addServer(final String serverName, final ServerProxy proxy) {

        // Create the LogicalClusterMember object
        LogicalClusterMember server = new LogicalClusterMember(serverName, proxy);

        // Add this member to the 'members' list
        boolean added = addMember(server);
        if (added) {
            // Build the ObjectName and register the ClusterMember MBean
            ObjectName on = null;
            try {
                on = JonasObjectName.clusterMember(domainName, serverName, getType(), name);
                server.setObjectName(on);
                MBeanServer mbeanServer = jmx.getJmxServer();
                if (mbeanServer.isRegistered(on)) {
                    mbeanServer.unregisterMBean(on);
                }
                mbeanServer.registerMBean(server, on);
            } catch (JMException e) {
                logger.log(BasicLevel.WARN, "Cannot register " + on + ": " + e);
            }
        }
        return added;
    }

    /**
     * MBean operation which overides the BaseCluster's one
     * It removes the corresponding member object from the <code>members</code>
     * list and unregisters the corresponding ServerProxy MBean if this
     * is the domain cluster.
     * Note that the ClusterMember MBean is not unregistered (it will
     * be done in case of the re-register of a new member in case the server
     * is re-added to the domain).
     * Remove a server from the cluster (jonasAdmin)
     * @param svname logical name of the server
     */
    @Override
    public synchronized void removeServer(final String svname) {
        // Remove the corresponding member from the 'members' list
        ClusterMember member = (ClusterMember) members.remove(svname);
        if (member != null) {
            // found a member corresponding to this server in this cluster
            String domainName = member.getProxy().getDomain();
            // Only treat ServerProxy if this is the domain cluster
            if (domainName.equals(name)) {
                // Unregister the server's ServerProxy MBean
                ServerProxy sp = member.getProxy();
                ObjectName spOn = null;
                try {
                    spOn = ObjectName.getInstance(sp.getObjectName());
                    MBeanServer mbeanServer = jmx.getJmxServer();
                    if (mbeanServer.isRegistered(spOn)) {
                        mbeanServer.unregisterMBean(spOn);
                    }
                } catch (JMException e) {
                    logger.log(BasicLevel.WARN, "Cannot unregister ServerProxy of " + svname + ": " + e);
                }
            }
        }
    }

}
