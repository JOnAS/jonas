/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.management.domain.cluster;

import java.util.Collection;

import javax.management.MBeanServer;

import org.objectweb.util.monolog.api.Logger;
import org.ow2.jonas.lib.management.domain.DomainMonitor;
import org.ow2.jonas.lib.management.domain.proxy.server.ServerProxy;
import org.ow2.jonas.lib.util.Log;



/**
 * Factory for any Cluster.
 * @author durieuxp
 */
public abstract class ClusterFactory {

    /**
     * logger for traces
     */
    protected static Logger logger = Log.getLogger("org.ow2.jonas.management.domain");

    /**
     * The MBeanServer used for registering new MBeans.
     */
    protected MBeanServer mbeanServer;

    /**
     * Reference to the DomainMonitor.
     */
    protected DomainMonitor dm;

    /**
     * Domain name.
     */
    protected String domainName;

    public ClusterFactory(final DomainMonitor dm) {
        this.dm = dm;
        mbeanServer = dm.getJmxService().getJmxServer();
        domainName = dm.getDomainName();
    }

    /**
     * A new Server has been detected by the DomainManager.
     * Look if it can be added in a cluster. This cluster could
     * be created if necessary.
     * @param proxy The new ServerProxy object just created.
     * @return True if server was added in a Cluster.
     */
    public abstract boolean notifyServer(ServerProxy proxy);

    /**
     * @return a reference to the DomainMonitor
     */
    public DomainMonitor getDomainMonitor() {
        return dm;
    }

    /**
     * @return The MBeanServer used to register MBeans
     */
    public MBeanServer getMBeanServer() {
        return mbeanServer;
    }

    /**
     * Look for a cluster by its name.
     * @param name fo the cluster
     * @return The cluster or null if not found
     */
    public abstract BaseCluster getCluster(String name);

    /**
     * @return A list of all Clusters managed by this ClusterFactory
     */
    public abstract Collection getClusterList();

    /**
     * Update dynamic info for all the clusters.
     */
    public abstract void getMonitoringInfo();

}
