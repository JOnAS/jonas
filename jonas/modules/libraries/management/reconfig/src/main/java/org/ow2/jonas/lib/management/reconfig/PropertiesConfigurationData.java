/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.lib.management.reconfig;


import java.util.Properties;

/**
 * @author Adriana Danes
 */
public class PropertiesConfigurationData implements IConfigurationData {
    String propName;
    String propValue;
    Properties props;

    /**
     * true = the normal case : the provided propVale replaces the old one
     * false = special case when a property value is a String composed of a list of ',' separated sub-Strings
     * (for example : the jonas.service.jms.topics propery)
     */
    boolean replace;

    /**
     * If true, add a new sub-String . If false, remove a sub-String
     */
    boolean add;

    public PropertiesConfigurationData(final String propName, final String propValue, final boolean replace) {
        this.propName = propName;
        this.propValue = propValue;
        this.replace = replace;
    }

    /**
     * Replace a given property with the given value.
     * @param propName Property name
     * @param propValue New property value
     */
    public PropertiesConfigurationData(final String propName, final String propValue) {
        this(propName, propValue, true);
    }

    public String getPropName() {
        return propName;
    }

    public String getPropValue() {
        return propValue;
    }

    public void setProps(final Properties props) {
        this.props = props;
    }

    public Properties getProps() {
        return props;
    }

    public boolean replaceProp() {
        return replace;
    }

    public void setAdd(final boolean add) {
        this.add = add;
    }

    public boolean addProp() {
        return add;
    }
}
