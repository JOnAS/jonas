/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id:XMLConfigurationActuator.java 10753 2007-06-26 13:39:43Z durieuxp $
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.management.reconfig.actuator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

import org.ow2.jonas.lib.management.reconfig.IConfigurationActuator;
import org.ow2.jonas.lib.management.reconfig.ReconfigException;

import org.objectweb.util.monolog.api.BasicLevel;

/**
 * This class allows for persistent reconfiguration of a JOnAS service or of a JOnAS resource
 * being configured by a .xml file.
 */
public class XMLConfigurationActuator extends BaseConfigurationActuator {


    /**
     * Content of the file
     */
    private String xml = null;

    /**
     * Construct an {@link IConfigurationActuator} for a JOnAS service or a JOnAS resource
     * @param name Name of the JOnAS service or JOnAS resource to which this object is associated
     * @param configFileName name of the config file
     * @param xml XML content of the file
     */
    public XMLConfigurationActuator(String name, String configFileName, String xml) {
        super(name, configFileName);
        this.xml = xml;
    }

    /**
     * Updates the configuration file
     * @param xml The new configuration
     * @param sequence the sequence number of management notification producing the update
     */
    public void updateConfig(String xml, long sequence) {
        if (sequence > lastSequence) {
            this.xml = xml;
            lastSequence = sequence;
        } else {
            logger.log(BasicLevel.WARN, "Received out of order reconfiguration message !");
        }
    }


    /**
     * Saves the updated configuration
     * @param sequence the sequence number of management notification producing the save (in fact store) operation
     * @throws ReconfigException if the saveConfig could not be done
     */
    public void saveConfig(long sequence) throws ReconfigException {
        if (sequence > lastSequence) {
            try {

                BufferedWriter out = new BufferedWriter(new FileWriter(new File(configFileName)));
                out.write(xml);
                out.flush();
                out.close();
                lastSequence = sequence;
                if (logger.isLoggable(BasicLevel.DEBUG)) {
                    logger.log(BasicLevel.DEBUG, "Configuration file " + configFileName + " updated");
                }
            } catch (FileNotFoundException e) {
                throw new ReconfigException("Cant' save configuration file: " + e.toString());
            } catch(IOException ioe) {
                throw new ReconfigException("Cant' save configuration file: " + ioe.toString());
            }
        } else {
            logger.log(BasicLevel.WARN, "Received out of order save reconfiguration message for " + name + " !");
            logger.log(BasicLevel.WARN, "Can not save !!");
            logger.log(BasicLevel.WARN, "Please reconfigure and than save !!");
        }

    }


}
