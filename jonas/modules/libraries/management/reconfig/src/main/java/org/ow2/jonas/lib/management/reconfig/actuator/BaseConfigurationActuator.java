/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id:BaseConfigurationActuator.java 10753 2007-06-26 13:39:43Z durieuxp $
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.management.reconfig.actuator;

// general Java imports
import org.ow2.jonas.lib.management.reconfig.IConfigurationActuator;
import org.ow2.jonas.lib.util.Log;

import org.objectweb.util.monolog.api.Logger;
import org.objectweb.util.monolog.api.BasicLevel;

/**
 * This class allows for persistent reconfiguration of a JOnAS service or of a JOnAS resource
 * This is an abstract class. It must be extended to handle properties or xml file
 * @author Adriana Danes
 * @author Florent Benoit : Add an abstract class to define XMLConfigurationActuator in addition to PropertiesConfigurationActuator
 */
public abstract class BaseConfigurationActuator implements IConfigurationActuator {

    /**
     * Name of the JOnAS service or JOnAS resource to which this object is associated.
     */
    protected String name;

    /**
     * Name of the file updated by the saveConfig method.
     */
    protected String configFileName;

    /**
     * The sequence number of the last treated management notification.
     */
    protected long lastSequence;

    /**
     * Logger used.
     */
    protected static Logger logger = null;

    /**
     * Construct a reconfigurator for a JOnAS service or a JOnAS resource.
     * @param name Name of the JOnAS service or JOnAS resource to which this object is associated
     * @param configFileName name of the config file
     */
    public BaseConfigurationActuator(final String name, final String configFileName) {
        // Get a logger for server traces
        logger = Log.getLogger(Log.JONAS_MANAGEMENT_PREFIX);
        this.name = name;
        this.configFileName = configFileName;
        this.lastSequence = 0;
        if (logger.isLoggable(BasicLevel.DEBUG)) {
            logger.log(BasicLevel.DEBUG, "Creating IConfigurationActuator for "
                    + name + " - configuration file is "
                    + configFileName);
        }
    }

}
