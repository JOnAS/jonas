/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.lib.management.services;

public class ReconfigNotifications {

    /**
     * Management notification type for <i>reconfiguration</i> events (notify that the management application made a reconfiguration operation)
     */
    public static final String RECONFIG_TYPE = "jonas.management.reconfiguration";

    /**
     * Management notification type for <i>save configuration</i> events (notify that the management application asks for save changes)
     */
    public static final String SAVE_RECONFIG_TYPE = "jonas.management.reconfiguration.save";

}
