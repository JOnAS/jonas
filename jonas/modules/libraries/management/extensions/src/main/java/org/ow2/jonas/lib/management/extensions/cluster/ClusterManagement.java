/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 BULL S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.management.extensions.cluster;

import javax.management.ObjectName;

import org.ow2.jonas.lib.management.extensions.base.BaseManagement;
import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.management.extensions.domain.DomainManagement;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.management.extensions.cluster.api.ICluster;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

/**
 * Implements cluster management functions.
 * @author Adriana Danes
 * @author Oualaa Hani
 * @author THOMAS KOUASSI
 */

/**
 * Cluster management base.
 */

public class ClusterManagement extends BaseManagement implements ICluster {

    /**
     * create logger.
     */
    protected static Log logger = LogFactory.getLog(ClusterManagement.class);

    /**
     * ClusterManagement.
     */
    private static ClusterManagement unique = null;

    /**
     * Default constructor.
     */
    public ClusterManagement() {
        super();
    }

    /**
     * Get singleton instance.
     * @return
     */
    public static ICluster getInstance() {
        if (unique == null) {
            unique = new ClusterManagement();
        }
        return unique;
    }

    /**
     * Add server to cluster.
     * @param clusterName cluster name
     * @param serverName server name
     * @param clusterDaemon cluster daemon name
     * @param serverURL server url
     */

    public void addServerToCluster(final String clusterName, final String serverName, final String clusterDaemon,
            final String serverURL) {
        String clusterType = "LogicalCluster";
        try {
            ObjectName on = JonasObjectName.cluster(getDomainName(), clusterName, clusterType);
            String[] signature = {"java.lang.String", "[Ljava.lang.String;", "java.lang.String", "java.lang.String",
                    "java.lang.String"};
            Object[] params = new Object[5];
            String[] urls = {serverURL};
            // the current Jonas server name.
            String currentServerName = null;
            String username = null;
            if (serverName != null) {
                params[0] = serverName;
                params[1] = urls;
                params[2] = clusterDaemon;
                params[3] = username;
                params[4] = null;
                JonasManagementRepr.invoke(on, "addServer", params, signature, currentServerName);
            }
        } catch (Exception e) {
            logger.error("Error in adding server to cluster   ", e);
            return;
        }
    }

    /**
     * start all servers in the cluster.
     * @param clusterName
     */
    public void startAllServers(final String clusterName) {
        DomainManagement dm = new DomainManagement();
        String domainName = getDomainName();
        String clusterType = dm.getClusterType(clusterName);
        String serverName = getServerName();
        ObjectName on = null;
        try {
            on = JonasObjectName.cluster(domainName, clusterName, clusterType);
        } catch (Exception e) {
            logger.error("Error in startAllServers", e);
            return;
        }
        String opName = "start";
        Object[] standby = {false};
        String[] signature = {Boolean.class.toString()};
        JonasManagementRepr.invoke(on, opName, standby, signature, serverName);
    }

    /**
     * stop all servers in the cluster.
     * @param clusterName
     */
    public void stopAllServers(final String clusterName) {
        DomainManagement dm = new DomainManagement();
        String domainName = getDomainName();
        String clusterType = dm.getClusterType(clusterName);
        String serverName = getServerName();
        ObjectName on = null;
        try {
            on = JonasObjectName.cluster(domainName, clusterName, clusterType);
        } catch (Exception e) {
            logger.error("Error in stopAllServers", e);
            return;
        }
        // Do not halt.
        String opName = "stopit";
        JonasManagementRepr.invoke(on, opName, null, null, serverName);
    }

    /**
     * @param clusterName
     * @return the multicast address
     */
    public String getMcastAddr(final String clusterName) {
        DomainManagement dm = new DomainManagement();
        String domainName = getDomainName();
        String clusterType = dm.getClusterType(clusterName);
        ObjectName on = null;
        try {
            on = JonasObjectName.cluster(domainName, clusterName, clusterType);
        } catch (Exception e) {
            logger.error("Error in getMcastAddr", e);
            return null;
        }
        return getStringAttribute(on, "McastAddr");
    }

    /**
     * @param clusterName
     * @return the multicast port
     */
    public int getMcastPort(final String clusterName) {
        String domainName = getDomainName();
        DomainManagement dm = new DomainManagement();
        String clusterType = dm.getClusterType(clusterName);
        ObjectName on = null;
        try {
            on = JonasObjectName.cluster(domainName, clusterName, clusterType);
        } catch (Exception e) {
            logger.error("Error in getMcastPort", e);
            return -1;
        }

        return getIntegerAttribute(on, "McastPort");
    }

    /**
     * @param clusterName
     * @return the protocol used
     */
    public String getProtocol(final String clusterName) {
        String domainName = getDomainName();
        ObjectName on = null;
        try {
            on = JonasObjectName.cluster(domainName, clusterName, "CmiCluster");
        } catch (Exception e) {
            logger.error("Error in getProtocol", e);
            return null;
        }
        return getStringAttribute(on, "Protocol");
    }

    /**
     * @param clusterName
     * @return the delay used by clients to update their cluster view
     */
    public int getDelayToRefresh(final String clusterName) {
        String domainName = getDomainName();
        ObjectName on = null;
        try {
            on = JonasObjectName.cluster(domainName, clusterName, "CmiCluster");
        } catch (Exception e) {
            logger.error("Error in getDelayToRefresh", e);
            return -1;
        }
        return getIntegerAttribute(on, "DelayToRefresh");
    }
}
