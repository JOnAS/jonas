/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.lib.management.extensions.util;
/**
 * File management util.
 * @author eyindanga
 *
 */
public final class FileManagementUtils {

    /**
     * Autoload dir.
     */
    private static String DIR_AUTOLOAD = "autoload";
    /**
     * private Constructor
     */
    private FileManagementUtils(){

    }

    /**
     * Extract the dirname of complete path.
     * @param p_Path Complete path (directory and filename)
     * @return The dirname or null
     */
    public static String extractDirname(final String p_Path) {
        String sDirname = null;
        try {
            int iPosSeparator = p_Path.lastIndexOf("/");
            if (iPosSeparator < 0) {

            } else {
                sDirname = p_Path.substring(0, iPosSeparator);
            }

        } catch (NullPointerException e) {
                // none action
        }
        return sDirname;
    }

    /**
     * Extract the filename of complete path.
     * @param p_Path Complete path (directory and filename)
     * @return The filename or null
     */
    public static String extractFilename(String p_Path) {
        String sFilename = null;
        try {
            int lastCharIndex = p_Path.length() - 1;
            if (p_Path.lastIndexOf("/") == lastCharIndex) {
                // the path ends with "/"
                p_Path = p_Path.substring(0, lastCharIndex);
            }
            int iPosSeparator = p_Path.lastIndexOf("/");
            if (iPosSeparator < 0) {
                iPosSeparator = p_Path.lastIndexOf("\\");
                if (iPosSeparator < 0) {
                    sFilename = new String(p_Path);
                }
                else {
                    sFilename = p_Path.substring(iPosSeparator + 1);
                }
            }
            else {
                sFilename = p_Path.substring(iPosSeparator + 1);
            }
            if (sFilename.length() > 0) {
                int iPos_1 = p_Path.indexOf(DIR_AUTOLOAD + "/" + sFilename);
                int iPos_2 = p_Path.indexOf(DIR_AUTOLOAD + "\\" + sFilename);
                if (iPos_1 > -1) {
                    sFilename = DIR_AUTOLOAD + "/" + sFilename;
                }
                else if (iPos_2 > -1) {
                    sFilename = DIR_AUTOLOAD + "\\" + sFilename;
                }
            }
            else {
                sFilename = null;
            }
        }
        catch (NullPointerException e) {
            // none action
            sFilename = null;
        }
        return sFilename;
    }

}
