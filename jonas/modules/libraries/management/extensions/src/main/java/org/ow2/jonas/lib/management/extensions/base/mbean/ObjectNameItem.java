/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.management.extensions.base.mbean;

import javax.management.ObjectName;

import org.ow2.jonas.lib.management.extensions.base.NameItem;

public class ObjectNameItem implements NameItem {

// --------------------------------------------------------- Properties Variables

    private String objectName = null;
    private String name = null;

// --------------------------------------------------------- Constructors

    public ObjectNameItem() {
    }

    public ObjectNameItem(ObjectName p_ObjectName) {
        setObjectName(p_ObjectName.toString());
        setName(p_ObjectName.getKeyProperty("name"));
    }

// --------------------------------------------------------- Properties Methods

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

}