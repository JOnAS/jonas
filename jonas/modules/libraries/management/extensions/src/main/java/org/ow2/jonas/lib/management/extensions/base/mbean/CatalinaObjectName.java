/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2003-2007 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.lib.management.extensions.base.mbean;

import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;

/**
 * A set of static methods used to build the names of Catalina MBeans used in JOnAS.
 *
 * @author Michel-Ange ANTON
 * @author Adriana Danes
 */
public class CatalinaObjectName {

    private CatalinaObjectName() {
    }

    public static ObjectName catalinaServer() throws MalformedObjectNameException {
        return ObjectName.getInstance("Catalina:type=Server");
    }

    public static ObjectName catalinaFactory() throws MalformedObjectNameException {
        return ObjectName.getInstance("Catalina:type=MBeanFactory");
    }

    public static ObjectName catalinaService(String domainName, String p_ServiceName) throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=Service,name=" + p_ServiceName);
    }

    public static ObjectName catalinaEngine(String domainName) throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=Engine");
    }


    public static ObjectName catalinaClusters(String domainName) throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=Cluster,*");
    }

    public static ObjectName catalinaClusterMembership(String domainName, String hostName) throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=ClusterMembership,host=" + hostName);
    }

    public static ObjectName catalinaClusterReceiver(String domainName, String hostName) throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=ClusterReceiver,host=" + hostName);
    }

    public static ObjectName catalinaClusterSender(String domainName, String hostName) throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=ClusterSender,host=" + hostName);
    }

    public static ObjectName catalinaConnectors(String domainName) throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=Connector,*");
    }

    public static ObjectName catalinaConnector(String domainName, String p_Port) throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=Connector,port=" + p_Port);
    }

    public static ObjectName catalinaHosts(String domainName) throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=Host,*");
    }

    public static ObjectName catalinaHost(String domainName, String p_HostName) throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=Host,host=" + p_HostName);
    }

    public static ObjectName catalinaRealms() throws MalformedObjectNameException {
        return ObjectName.getInstance("*:type=Realm,*");
    }

    public static ObjectName catalinaRealm(String domainName) throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=Realm");
    }

    public static ObjectName catalinaValves(String domainName) throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=Valve,*");
    }

    public static ObjectName catalinaAccessLogValves(String domainName) throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=Valve,name=AccessLogValve,*");
    }

    public static ObjectName catalinaValves(String domainName, String p_Host) throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=Valve,host=" + p_Host + ",*");
    }

    public static ObjectName catalinaValve(String domainName, String p_Name) throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=Valve,name=" + p_Name);
    }

    public static ObjectName catalinaValve(String domainName, String p_Name, String p_Host) throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=Valve,name=" + p_Name + ",host=" + p_Host);
    }

    public static ObjectName catalinaThreadPools(String domainName) throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=ThreadPool,*");
    }

    public static ObjectName catalinaGlobalRequestProcessors(String domainName) throws MalformedObjectNameException {
        return ObjectName.getInstance(domainName + ":type=GlobalRequestProcessor,*");
    }
}
