/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2003-2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.management.extensions.server;

import javax.management.ObjectName;

import org.ow2.jonas.lib.management.extensions.base.mbean.ObjectNameItem;


@SuppressWarnings("serial")
public class ServerItem extends ObjectNameItem {
    /**
     * server state
     */
    private String state = null;
    /**
     * url used to connect on JMX remote server
     */
    private String url = null;

    private String clusterDaemonName = null;
    /**
     * true if this server is in clusterd.xml
     */
    private boolean isInCdConfig;
    /**
     * The following informations are needed for cluster daemon configuration file (clusterd.xml)
     * the server description
     */
    String description = null;
    String javaHome = null;
    String jonasRoot = null;
    String jonasBase = null;
    /**
     * extra parameter
     */
    String xprem = null;

    String autoBoot = null;
    /**
     * true if the server is master
     */
    boolean isMaster = false;

    /**
     * true if the server has already properties that are needed for affecting it to a cluster daemon.
     */
    private boolean isConfiguredForClusterd = false;




    public ServerItem(ObjectName pObjectName, String pState, String pClusterDaemonName) {
        super(pObjectName);
        this.state = pState;
        this.clusterDaemonName = pClusterDaemonName;
        this.isConfiguredForClusterd = false;
    }
    /**
     *
     * @param pObjectName
     * @param pState
     * @param pClusterDaemonName
     * @param isInCdConfig
     */
    public ServerItem(ObjectName pObjectName, String pState, String pClusterDaemonName,boolean isInCdConfig) {
        super(pObjectName);
        this.state = pState;
        this.clusterDaemonName = pClusterDaemonName;
        this.isInCdConfig=isInCdConfig;
        this.isConfiguredForClusterd = false;
    }
    /**
     * @return Returns the url.
     */
    public String getUrl() {
        return url;
    }
    /**
     * @param url The url to set.
     */
    public void setUrl(String url) {
        this.url = url;
    }
    /**
     * @return Returns the state.
     */
    public String getState() {
        return state;
    }

    /**
     * @param state The state to set.
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return the cluster daemon's name or null if no cluster daemon
     */
    public String getClusterDaemonName() {
        return clusterDaemonName;
    }
    /**
     * @return true if the server is in the clusterd.xml
     */
    public boolean getIsInCdConfig() {
        return isInCdConfig;
    }
    /**
     *
     * @param isInCdConfig
     */
    public void setIsInCdConfig(boolean isInCdConfig) {
        this.isInCdConfig = isInCdConfig;
    }
    public void setClusterDaemonName(String clusterDaemonName) {
        this.clusterDaemonName = clusterDaemonName;
    }
    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }
    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    /**
     * @return the javaHome
     */
    public String getJavaHome() {
        return javaHome;
    }
    /**
     * @param javaHome the javaHome to set
     */
    public void setJavaHome(String javaHome) {
        this.javaHome = javaHome;
    }
    /**
     * @return the jonasBase
     */
    public String getJonasBase() {
        return jonasBase;
    }
    /**
     * @param jonasBase the jonasBase to set
     */
    public void setJonasBase(String jonasBase) {
        this.jonasBase = jonasBase;
    }
    /**
     * @return the jonasRoot
     */
    public String getJonasRoot() {
        return jonasRoot;
    }
    /**
     * @param jonasRoot the jonasRoot to set
     */
    public void setJonasRoot(String jonasRoot) {
        this.jonasRoot = jonasRoot;
    }
    /**
     * @param isInCdConfig the isInCdConfig to set
     */
    public void setInCdConfig(boolean isInCdConfig) {
        this.isInCdConfig = isInCdConfig;
    }
    public boolean getIsConfiguredForClusterd() {
        return isConfiguredForClusterd;
    }
    public void setIsConfiguredForClusterd(boolean isConfiguredForClusterd) {
        this.isConfiguredForClusterd = isConfiguredForClusterd;
    }
    /**
     * @return the autoBoot
     */
    public String getAutoBoot() {
        return autoBoot;
    }
    /**
     * @param autoBoot the autoBoot to set
     */
    public void setAutoBoot(String autoBoot) {
        this.autoBoot = autoBoot;
    }
    /**
     * @return the xprem
     */
    public String getXprem() {
        return xprem;
    }
    /**
     * @param xprem the xprem to set
     */
    public void setXprem(String xprem) {
        this.xprem = xprem;
    }
    /**
     * @return the isMaster
     */
    public boolean isMaster() {
        return isMaster;
    }
    /**
     * @param isMaster the isMaster to set
     */
    public void setMaster(boolean isMaster) {
        this.isMaster = isMaster;
    }
}