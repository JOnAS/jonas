/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.lib.management.extensions.base;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.management.MBeanServer;
import javax.management.MBeanServerFactory;
import javax.management.ObjectName;

import org.ow2.jonas.lib.management.extensions.base.mbean.J2EEObjectNames;
import org.ow2.jonas.management.extensions.base.api.ManagementException;

/**
 * This class gives the local context information allowing to initialize the management applications:
 * the domain name, the server name and local MBeanServer reference.
 * @author Adriana Danes
 */
public class LocalManagementContext {

    private static LocalManagementContext unique = null;

    private String domainName = null;
    private String serverName = null;
    private MBeanServer mbeanServer = null;

    /**
     * @return LocalManagementContext singleton.
     */
    public static synchronized LocalManagementContext getInstance() {
        if(unique == null) {
            unique = new LocalManagementContext();
        }
        return unique;
    }

    /**
     * Initialize local management context by looking for an MBeanServer, in the current JVM,
     * with a J2EEServer MBean registered within it.
     * @throws ManagementException The local management context could not be initialized.
     */
    private LocalManagementContext() throws ManagementException {
        try {
            // Return a list of registered MBeanServer objects
            List<MBeanServer> mbeanServersList = MBeanServerFactory.findMBeanServer(null);
            if (mbeanServersList.isEmpty()) {
                // No MBean server found, can't provide management functions
                throw new ManagementException("No MBean server found in JVM.");
            }
            // Check for the J2EEServer MBean
            for (Iterator<MBeanServer> it = mbeanServersList.iterator(); it.hasNext();) {
                MBeanServer mbeanServer = it.next();
                ObjectName j2eeServerOns = J2EEObjectNames.getJ2EEServerObjectNames();
                Set<ObjectName> ons = mbeanServer.queryNames(j2eeServerOns, null);
                for (Iterator<ObjectName> onit = ons.iterator(); onit.hasNext(); ) {
                   ObjectName on = onit.next();
                   domainName = on.getDomain();
                   serverName = on.getKeyProperty(J2EEObjectNames.NAME_KEY);
                   this.mbeanServer = mbeanServer;
                   break;
                }
            }
        } catch (SecurityException se) {
            // treat SecurityException
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (domainName == null || serverName == null) {
            throw new ManagementException("Couldn't initialize server or domain name");
        }
    }

    /**
     * @return the current domain name
     */
    public String getDomainName() {
        return domainName;
    }

    /**
     * @return the current server name
     */
    public String getServerName() {
        return serverName;
    }

    /**
     * @return the current MBean server's reference
     */
    public MBeanServer getMbeanServer() {
        return mbeanServer;
    }

}
