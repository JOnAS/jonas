/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 BULL S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.lib.management.extensions.server;

import org.ow2.jonas.lib.management.domain.DomainMonitor;
import org.ow2.jonas.lib.management.domain.J2EEDomain;
import org.ow2.jonas.lib.management.domain.proxy.server.ServerProxy;
import org.ow2.jonas.lib.management.extensions.base.BaseManagement;
import org.ow2.jonas.management.extensions.server.api.IServer;

/**
 * Provides informations about a server in the domain. General information or
 * server statistics are based on the corresponding ServerProxy object.
 * @author Adriana Danes
 * @author THOMAS KOUASSI
 */
public class ServerMonitoring extends BaseManagement implements IServer {

    /**
     * Ref. to DomainMonitor.
     */
    private DomainMonitor dm = null;

    /**
     * Constructor.
     */
    public ServerMonitoring() {
        super();
        dm = J2EEDomain.getInstance().getDomainMonitor();
    }

    /**
     * Return the version of a server in the domain.
     * @param serverName the name of the server.
     * @return the server's version
     */
    public String getServerVersion(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getJOnASVersion();
        }
        return null;
    }

    /**
     * Get the JMX connection URL of a server in the domain.
     * @param serverName the server's name.
     * @return the JMX connection URL of the server
     */
    public String getConnectionUrl(final String serverName) {
        if (!dm.isMaster()) {
            //Returns its own connection URL.
            return dm.getJmxServiceURL().toString();
        }
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getConnectionUrl();
        }
        return null;
    }

    /**
     * Get the memory used by a server in the domain.
     * @param serverName the server's name.
     * @return the used memory
     */
    public long getCurrentUsedMemory(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getCurrentUsedMemory();
        }
        return 0;
    }

    /**
     * Get the number of threads used by a server in the domain.
     * @param serverName the server's name.
     * @return the number of threads
     */
    public int getAllThreadsCount(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getAllThreadsCount();
        }
        return 0;
    }

    /**
     * @return bytes received by connector tomcat.
     * @param serverName Server name
     */
    public long getBytesReceivedByConnectorTomcat(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getBytesReceivedByConnectorTomcat();
        }
        return 0;
    }

    /**
     * @return bytes sent by connector tomcat.
     * @param serverName Server name
     */
    public long getBytesSentByConnectorTomcat(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getBytesSentByConnectorTomcat();
        }
        return 0;
    }

    /**
     * @return current number of Entity bean.
     * @param serverName Server name
     */
    public int getCurrentNumberOfEntityBean(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getCurrentNumberOfEntityBean();
        }
        return 0;
    }

    /**
     * @return current number of EJB.
     * @param serverName Server name
     */
    public int getCurrentNumberOfEJB(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getCurrentNumberOfEJB();
        }
        return 0;
    }

    /**
     * @return current number of message driven bean.
     * @param serverName Server name
     */
    public int getCurrentNumberOfMDB(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getCurrentNumberOfMDB();
        }
        return 0;
    }

    /**
     * @return current number of state full bean.
     * @param serverName Server name
     */
    public int getCurrentNumberOfSBF(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getCurrentNumberOfSBF();
        }
        return 0;
    }

    /**
     * @return current number of state less bean.
     * @param serverName Server name
     */
    public int getCurrentNumberOfSBL(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getCurrentNumberOfSBL();
        }
        return 0;
    }

    /**
     * @return current threads busy by connector tomcat.
     * @param serverName Server name
     */
    public int getCurrentThreadBusyByConnectorTomcat(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getCurrentThreadBusyByConnectorTomcat();
        }
        return 0;
    }

    /**
     * @return current threads count by tomcat connector
     * @param serverName Server name
     */
    public int getCurrentThreadCountByConnectorTomcat(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getCurrentThreadCountByConnectorTomcat();
        }
        return 0;
    }

    /**
     * Get the total memory of the server in the domain.
     * @param serverName the server's name.
     * @return the total memory
     */
    public Long getCurrentTotalMemory(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getCurrentTotalMemory();
        }
        return null;
    }

    /**
     * @return error count by connector tomcat.
     * @param serverName Server name
     */
    public int getErrorCountByConnectorTomcat(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getErrorCountByConnectorTomcat();
        }
        return 0;
    }

    /**
     * Get the JVM vendor.
     * @param serverName the server's name.
     * @return the JVM vendor
     */
    public String getJavaVendor(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getJavaVendor();
        }
        return null;
    }

    /**
     * Get the java version.
     * @param serverName the server's name.
     * @return the java version
     */
    public String getJavaVersion(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getJavaVersion();
        }
        return null;
    }

    /**
     * @return the max threads by connector tomcat.
     * @param serverName Server name
     */
    public int getMaxThreadsByConnectorTomcat(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getMaxThreadsByConnectorTomcat();
        }
        return 0;
    }

    /**
     * @return processing time by connector tomcat.
     * @param serverName Server name
     */

    public long getProcessingTimeByConnectorTomcat(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getProcessingTimeByConnectorTomcat();
        }
        return 0;
    }

    /**
     * Get the the protocols used bye the server.
     * @param serverName the server's name.
     * @return the protocols used bye the server
     */
    public String getProtocols(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getProtocols();
        }
        return null;
    }

    /**
     * @return request count by connector tomcat.
     * @param serverName Server name
     */
    public int getRequestCountByConnectorTomcat(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getRequestCountByConnectorTomcat();
        }
        return 0;

    }

    /**
     * @return total begun transactions.
     * @param serverName Server name
     */
    public int getTotalBegunTransactions(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getTotalBegunTransactions();
        }
        return 0;
    }

    /**
     * @return total commited transactions.
     * @param serverName Server name
     */
    public int getTotalCommittedTransactions(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getTotalCommittedTransactions();
        }
        return 0;
    }

    /**
     * @return total current transactions.
     * @param serverName Server name
     */
    public int getTotalCurrentTransactions(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getTotalCurrentTransactions();
        }
        return 0;
    }

    /**
     * @return total expired transactions.
     * @param serverName Server name
     */
    public int getTotalExpiredTransactions(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getTotalExpiredTransactions();
        }
        return 0;
    }

    /**
     * @return total global transactions.
     * @param serverName Server name
     */
    public int getTotalRolledbackTransactions(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getTotalRolledbackTransactions();
        }
        return 0;
    }

    /**
     * @return the description
     */
    public String getDescription(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getDescription();
        }
        return null;
    }

    /**
     * @return the javaHome
     */
    public String getJavaHome(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getJavaHome();
        }
        return null;
    }

    /**
     * @return the jonasRoot
     */
    public String getJonasRoot(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getJonasRoot();
        }
        return null;
    }

    /**
     * @return the jonasBase
     */
    public String getJonasBase(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getJonasRoot();
        }
        return null;
    }

    /**
     * @return the xprem
     */
    public String getXprem(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getXprem();
        }
        return null;
    }

    /**
     * @return the autoBoot
     */
    public String getAutoBoot(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getAutoBoot();
        }
        return null;
    }

    public String getJ2eeObjectName(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getJ2eeObjectName();
        }
        return null;
    }

    public int getConnectionFailuresJCAConnection(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getConnectionFailuresJCAConnection();
        }
        return 0;
    }

    public int getConnectionLeaksJCAConnection(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getConnectionLeaksJCAConnection();
        }
        return 0;
    }

    public int getCurrentBusyJCAConnection(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getCurrentBusyJCAConnection();
        }
        return 0;
    }

    public int getCurrentWorkerPoolSize(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getCurrentWorkerPoolSize();
        }
        return 0;
    }

    public int getCurrentOpenedJCAConnection(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getCurrentOpenedJCAConnection();
        }
        return 0;
    }

    public int getWaiterCountJCAConnection(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getWaiterCountJCAConnection();
        }
        return 0;
    }

    public long getWaitingTimeJCAConnection(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getWaitingTimeJCAConnection();
        }
        return 0;
    }

    public boolean getTomcat(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getTomcat();
        }
        return false;
    }

    public boolean getTransaction(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getTransaction();
        }
        return false;
    }

    public boolean getWorkers(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getWorkers();
        }
        return false;
    }

    public int getMaxWorkerPoolSize(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getMaxWorkerPoolSize();
        }
        return 0;
    }

    public int getMinWorkerPoolSize(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getMinWorkerPoolSize();
        }
        return 0;
    }

    public int getConnectionFailuresJDBCResource(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getConnectionFailuresJDBCResource();
        }
        return 0;
    }

    public int getConnectionLeaksJDBCResource(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getConnectionLeaksJDBCResource();
        }
        return 0;
    }

    public int getCurrentBusyJDBCResource(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getCurrentBusyJDBCResource();
        }
        return 0;
    }

    public int getCurrentOpenedJDBCResource(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getCurrentOpenedJDBCResource();
        }
        return 0;
    }

    public int getRejectedOpenJDBCResource(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getRejectedOpenJDBCResource();
        }
        return 0;
    }

    public int getServedOpenJDBCResource(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getServedOpenJDBCResource();
        }
        return 0;
    }

    public int getWaiterCountJDBCResource(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getWaiterCountJDBCResource();
        }
        return 0;
    }

    public long getWaitingTimeJDBCResource(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getWaitingTimeJDBCResource();
        }
        return 0;
    }

    public int getServedOpenJCAConnection(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getServedOpenJCAConnection();
        }
        return 0;
    }

    public int getRejectedOpenJCAConnection(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getRejectedOpenJCAConnection();
        }
        return 0;
    }

    public boolean getJcaConnection(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getJcaConnection();
        }
        return false;
    }

    public boolean getJdbcDatasource(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getJdbcDatasource();
        }
        return false;
    }

    public boolean getJmsJoram(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getJmsJoram();
        }
        return false;
    }

    public int getJmsQueuesNbMsgsDeliverSinceCreation(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getJmsQueuesNbMsgsDeliverSinceCreation();
        }
        return 0;
    }

    public int getJmsQueuesNbMsgsReceiveSinceCreation(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getJmsQueuesNbMsgsReceiveSinceCreation();
        }
        return 0;
    }

    public int getJmsQueuesNbMsgsSendToDMQSinceCreation(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getJmsQueuesNbMsgsSendToDMQSinceCreation();
        }
        return 0;
    }

    public int getJmsTopicsNbMsgsDeliverSinceCreation(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getJmsTopicsNbMsgsDeliverSinceCreation();
        }
        return 0;
    }

    public int getJmsTopicsNbMsgsReceiveSinceCreation(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getJmsTopicsNbMsgsReceiveSinceCreation();
        }
        return 0;
    }

    public int getJmsTopicsNbMsgsSendToDMQSinceCreation(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getJmsTopicsNbMsgsSendToDMQSinceCreation();
        }
        return 0;
    }

    public String getHostName(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getHostName();
        }
        return null;
    }

    public String getJOnASVersion(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getJOnASVersion();
        }
        return null;
    }

    public String getLoadCPU(final String serverName) {
        ServerProxy srvProxy = dm.findServerProxy(serverName);
        if (srvProxy != null) {
            return srvProxy.getLoadCPU();
        }
        return null;
    }

}
