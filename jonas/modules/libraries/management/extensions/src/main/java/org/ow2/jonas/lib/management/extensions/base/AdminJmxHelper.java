/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.lib.management.extensions.base;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;

import org.ow2.jonas.lib.management.extensions.base.mbean.CatalinaObjectName;
import org.ow2.jonas.lib.management.extensions.base.mbean.MbeanItem;
import org.ow2.jonas.lib.management.extensions.container.ContainerManagement;
import org.ow2.jonas.lib.management.extensions.util.FileManagementUtils;
import org.ow2.jonas.lib.management.javaee.J2eeObjectName;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.mail.MailService;
import org.ow2.jonas.management.extensions.base.api.ManagementException;
/**
 * Jmx helper for administration.
 * @author eyindanga
 *
 */
public class AdminJmxHelper {

    // ----------------------------------------------------------- Constants
    /**
     * Object used for synchronization.
     */
    protected static Object s_Synchro = new Object();
    /**
     * J2ee application constant.
     */
    protected static final String J2EEApplication = "J2EEApplication";
    /**
     * Constant for client module.
     */
    protected static final String AppClientModule = "AppClientModule";
    /**
     * Constant for ejb module.
     */
    protected static final String EJBModule = "EJBModule";
    /**
     * Constant for web module.
     */
    protected static final String WebModule = "WebModule";
    /**
     * Constant for rar module.
     */
    protected static final String ResourceAdapterModule = "ResourceAdapterModule";

    /**
     * Rar dir.
     */
    private static String DIR_RARS = "rars";
    /**
     * Autoload dir.
     */
    private static String DIR_AUTOLOAD = "autoload";
    /**
     * Conf dir.
     */
    private static String DIR_CONF = "conf";

    /**
     *
     */
    private static String DIR_DEPLAN_JONAS = "repositories/url-internal";

    private static String REPO_MAVAEN2_INTERNAL = "repositories/maven2-internal";
    private static String REPO_MAVAEN2_DEV = ".m2/repository";

    /**
     * Default constructor.
     */
    protected AdminJmxHelper() {
    }

    // --------------------------------------------------------- Public Methods

    /**
     * Replace any occurrence of the specified placeholder in the specified
     * template string with the specified replacement value.
     *
     * @param template Pattern string possibly containing the placeholder
     * @param placeholder Placeholder expression to be replaced
     * @param value Replacement value for the placeholder
     * @return A complete string
     */
    public static String replace(final String template, final String placeholder, final String value) {
        if (template == null) {
            return (null);
        }
        if ((placeholder == null) || (value == null)) {
            return (template);
        }
        String sRet = new String(template);
        while (true) {
            int index = sRet.indexOf(placeholder);
            if (index < 0) {
                break;
            }
            StringBuffer temp = new StringBuffer(sRet.substring(0, index));
            temp.append(value);
            temp.append(sRet.substring(index + placeholder.length()));
            sRet = temp.toString();
        }
        return (sRet);

    }
    /**
     * Object name of archive configuration.
     * @param domainName The domain name
     * @param serverName The server name
     * @return ObjectName corresponding to the archive configuration
     */
    public static ObjectName getArchiveConfigObjectName(final String domainName, final String serverName) {
        ObjectName onUtils = JonasObjectName.ArchiveConfig(domainName);
        return getFirstMbean(onUtils, serverName);
    }
    /**
     * Object name of rar configuration.
     * @param domainName The domain name
     * @param serverName The server name
     * @return Corresponding ObjectName
     */
    public static ObjectName getRarConfigObjectName(final String domainName, final String serverName) {
        ObjectName onUtils = JonasObjectName.RarConfig(domainName);
        return getFirstMbean(onUtils, serverName);
    }

    /**
     * Return the ObjectName corresponding to the J2EEServer managed object registered in the
     * current MBeanServer and belonging to a domain.
     * We should have one ObjectName corresponding to the current JOnAS server instance.
     * @param p_DomainName The name of the management domain.
     * @return an ObjectName which corresponds to a J2EEServer ObjectName pattern or null if
     * no J2EEServer found for the given management domain
     * (having <code>j2eeType</code> key property equal to <code>J2EEServer</code>)
     * @throws ManagementException if could not connect to the MBean server
     */
    public static ObjectName getJ2eeServerObjectName(final String p_DomainName, final String serverName) throws ManagementException {
        // Lookup for a J2EEServer managed object in the current MBeanServer
        ObjectName pattern_server_on = J2eeObjectName.J2EEServers(p_DomainName);
        ObjectName server_on = null;
        Iterator it = JonasManagementRepr.queryNames(pattern_server_on, serverName).iterator();
        if (it.hasNext()) {
            // Got one J2EEServer managed object ; normally should not be more than one
            server_on = (ObjectName) it.next();
        }
        return server_on;
    }

    /**
     * Return the ObjectName corresponding to the J2EEServer managed object registered in the
     * current MBeanServer. This method is used by EditTopAction to determine the list of
     * JOnAS servers registered in the current registry (this code is particular to the current
     * domain concept implementation).
     * We should have one ObjectName corresponding to the current JOnAS server instance.
     * @return an ObjectName which corresponds to a J2EEServer ObjectName pattern
     * (having <code>j2eeType</code> key property equal to <code>J2EEServer</code>)
     */
    public static ObjectName getJ2eeServerObjectName(final String serverName) {
        // Lookup for a J2EEServer managed object in the current MBeanServer
        ObjectName pattern_server_on = J2eeObjectName.J2EEServers();
        ObjectName server_on = null;
        Iterator it = JonasManagementRepr.queryNames(pattern_server_on, serverName).iterator();
        if (it.hasNext()) {
            // Got one J2EEServer managed object ; normally should not be more than one
            server_on = (ObjectName) it.next();
        }
        return server_on;
    }
    /**
     * @param pObjectName an ObjectName
     * @return the j2eeType key property value
     */
    private static String getJ2eeType(final ObjectName pObjectName) {
        return pObjectName.getKeyProperty("j2eeType");
    }
    /**
     *
     * @param pObjectName The objectName
     * @return true if J2ee application
     */
    public static boolean isJ2EEApplication(final ObjectName pObjectName) {
        if (J2EEApplication.equals(getJ2eeType(pObjectName))) {
            return true;
        }
        return false;
    }
    /**
     *
     * @param pObjectName The objectName
     * @return true if client module.
     */
    public static boolean isAppClientModule(final ObjectName pObjectName) {
        if (AppClientModule.equals(getJ2eeType(pObjectName))) {
            return true;
        }
        return false;
    }

    /**
     * @param pObjectName The objectName.
     * @return true if ejb module.
     */
    public static boolean isEJBModule(final ObjectName pObjectName) {
        if (EJBModule.equals(getJ2eeType(pObjectName))) {
            return true;
        }
        return false;
    }

    /**
     * @param pObjectName The objectName.
     * @return true if web module.
     */
    public static boolean isWebModule(final ObjectName pObjectName) {
        if (WebModule.equals(getJ2eeType(pObjectName))) {
            return true;
        }
        return false;
    }
    /**
     * @param pObjectName true if ejb module.
     * @return true if rar module
     */
    public static boolean isResourceAdapterModule(final ObjectName pObjectName) {
        if (ResourceAdapterModule.equals(getJ2eeType(pObjectName))) {
            return true;
        }
        return false;
    }
    /**
     * Verify if the Mbean gotten by the query in the current MbeanServer exists.
     *
     * @param p_On Query Mbean name to search
     * @return true if MBean exists
     * @throws ManagementException
     */
    public static boolean hasMBeanName(final ObjectName p_On, final String serverName)
    throws ManagementException {
        synchronized (s_Synchro) {
            ArrayList al = new ArrayList();
            Iterator itNames = JonasManagementRepr.queryNames(p_On, serverName).iterator();
            if (itNames.hasNext()) {
                return true;
            }
            return false;
        }
    }

    /**
     * Return the first MBean name found by the query in the current MBeanServer.
     *
     * @param p_On Query MBean name to search
     * @return The first MBean name or null if not found
     * @throws ManagementException
     */
    public static String getFirstMBeanName(final ObjectName p_On, final String serverName)
    throws ManagementException {
        synchronized (s_Synchro) {
            ArrayList al = new ArrayList();
            Iterator itNames = JonasManagementRepr.queryNames(p_On, serverName).iterator();
            if (itNames.hasNext()) {
                return itNames.next().toString();
            }
            return null;
        }
    }

    /**
     * Return the list of Mbean name gotten by the query in the current MbeanServer.
     *
     * @param p_On Query Mbean name to search
     * @return A list of string Mbean name
     */
    public static List getListMBeanName(final ObjectName p_On, final String serverName) {
        return BaseManagement.getInstance().getListMbean(p_On, serverName);
    }

    /**
     * Return the first <code>ObjectName</code> Mbean gotten by the query
     * in the current MbeanServer.
     *
     * @param p_On Query Mbean name to search
     * @return The first <code>ObjectName</code> or null if not found
     * @throws ManagementException
     */
    public static ObjectName getFirstMbean(final ObjectName p_On, final String serverName)
    throws ManagementException {
        synchronized (s_Synchro) {
            Iterator itNames = JonasManagementRepr.queryNames(p_On, serverName).iterator();
            if (itNames.hasNext()) {
                return (ObjectName) itNames.next();
            }
            return null;
        }
    }

    /**
     * Return the list of <code>ObjectName</code> Mbean gotten by the query
     * in the current MbeanServer.
     *
     * @param p_On Query Mbeans to search
     * @return The list of <code>ObjectName</code>
     * @throws ManagementException
     */
    public static List getListMbean(final ObjectName p_On, final String serverName)
    throws ManagementException {
        synchronized (s_Synchro) {
            ArrayList<ObjectName> al = new ArrayList<ObjectName>();
            Iterator itNames = JonasManagementRepr.queryNames(p_On, serverName).iterator();
            while (itNames.hasNext()) {
                al.add((ObjectName) itNames.next());
            }
            return al;
        }
    }

    /**
     * Extract the value of a key property from the MBean name.
     * This method is usefull when we have the String form and not the ObjectName
     * (avoid creating an ObjectName instance).
     * @param pName Name of the key property
     * @param pMBeanName Stringified ObjectName
     * @return The value or null if not found
     */
    public static String extractValueMbeanName(final String pName, final String pMBeanName) {
        String sValue = null;
        try {
            String sSearch = pName.trim() + "=";
            int iPos = pMBeanName.indexOf(sSearch);
            if (iPos > -1) {
                sValue = pMBeanName.substring(iPos + sSearch.length());
                iPos = sValue.indexOf(",");
                if (iPos > -1) {
                    sValue = sValue.substring(0, iPos);
                }
            }
        } catch (NullPointerException e) {
            // none
            sValue = null;
        }
        return sValue;
    }

    /**
     * Extract the filename of complete path.
     * @param p_Path Complete path (directory and filename)
     * @return The filename or null
     */
    public static String extractFilename(final String p_Path) {
        return FileManagementUtils.extractFilename(p_Path);
    }
    /**
     * @param p_Directory The directory to look into
     * @param p_Extension the file extension
     * @return  The files.
     */
    private static ArrayList getFilenames(final String p_Directory, final String p_Extension) {
        ArrayList al = new ArrayList();
        String sExt = "." + p_Extension.toLowerCase();
        String sFile;

        File oFile = new File(p_Directory);
        String[] asFiles = oFile.list();
        int iPos;
        if (asFiles != null) {
            for (int i = 0; i < asFiles.length; i++) {
                oFile = new File(p_Directory, asFiles[i]);
                if (oFile.isFile() == true) {
                    sFile = oFile.getName().toLowerCase();
                    iPos = sFile.lastIndexOf(sExt);
                    if (iPos > -1) {
                        if (iPos == (sFile.length() - sExt.length())) {
                            al.add(oFile.getName());
                        }
                    }
                }
            }
        }
        Collections.sort(al);
        return al;
    }
    /**
     * @param p_Directory The directory.
     * @return the directories
     */
    private static ArrayList getDirectories(final String p_Directory) {
        ArrayList al = new ArrayList();
        String sFile;

        File oFile = new File(p_Directory);
        String[] asFiles = oFile.list();

        if (asFiles != null) {
            for (int i = 0; i < asFiles.length; i++) {
                oFile = new File(p_Directory, asFiles[i]);
                if (oFile.isDirectory() == true) {
                    al.add(oFile.getName());
                }
            }
        }
        Collections.sort(al);
        return al;
    }
    /**
     * @param p_List the list.
     * @param p_Dir the directory.
     */
    private static void appendDirectory(final ArrayList p_List, final String p_Dir) {
        String sDir = p_Dir + "/";
        for (int i = 0; i < p_List.size(); i++) {
            p_List.set(i, sDir + p_List.get(i));
        }
    }

    /**
     * Check with 'depmonitor' MBean if a given directory is a depmonitor directory
     * @param dir the name of the given directory
     * @param domainName current domain name
     * @param serverName current server name
     * @return true if the given directory is a depmonitor directory
     * @throws ManagementException
     */
    public static boolean isDepmonitorDir(final String dir, final String domainName, final String serverName)
    throws ManagementException {
        ObjectName on = JonasObjectName.deployableMonitorService(domainName);
        Object[] params = {dir};
        String[] sign = {"java.lang.String"};
        return (Boolean) JonasManagementRepr.invoke(on, "isDepmonitorDir", params, sign, serverName);
    }

    /**
     * Get the deployment repositories from 'depmonitor' MBean.
     * @param domainName
     * @param serverName
     * @return the current URL repositories.
     */
    public static String[] getDepmonitorDirs(final String domainName, final String serverName)
    throws ManagementException {
        ArrayList<String> result = new ArrayList<String> ();
        ObjectName on = JonasObjectName.deployableMonitorService(domainName);
        if (on != null) {
            String[] dirs = (String[]) JonasManagementRepr.getAttribute(on, "directoryNames", serverName);
            for (String dir : dirs) {
                result.add(dir);
            }
            Collections.sort(result);
        }
        return result.toArray(new String[result.size()]);
    }

    /**
     * Get the deployment repositories from 'repository' MBean.
     * @param domainName
     * @param serverName
     * @return the current URL repositories.
     */
    public static String[] getRepositoryDirs(final String domainName, final String serverName)
    throws ManagementException {
        ArrayList<String> result = new ArrayList<String> ();
        ObjectName on = null;
        try {
            on = JonasObjectName.repository(domainName);
        } catch (MalformedObjectNameException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        ArrayList<String> al = (ArrayList<String>) JonasManagementRepr.getAttribute(on, "URLRepositories", serverName);
        String filePrefix = "file:";
        for (String url : al) {
            File tmpFile = new File(url);
            String path = tmpFile.getPath();
            if (path.startsWith(filePrefix)) {
                result.add(path.substring(filePrefix.length()));
            } else {
                // TODO
            }
        }
        // Sort
        Collections.sort(result);
        return result.toArray(new String[result.size()]);
    }

    /**
     * Add additional deployment repositories for the J2EEServer.
     * @param dirs the repository directories paths
     * @param domainName current domain name
     * @param serverName current server name
     * @throws ManagementException
     */
    public static void setRepositoryDirs(final String[] dirs, final String domainName, final String serverName)
    throws ManagementException {
        ObjectName on = J2eeObjectName.J2EEServer(domainName, serverName);
        Object[] params = {dirs};
        String[] sign = {"[Ljava.lang.String;"};
        JonasManagementRepr.invoke(on, "setRepositories", params, sign, serverName);
    }

    /**
     * @param domainName the domain
     * @param serverName the server
     * @return the deployables
     * @throws ManagementException any.
     */
    public static ArrayList getFilesDeployable(final String domainName, final String serverName)
    throws ManagementException {
        ObjectName on = J2eeObjectName.J2EEServer(domainName, serverName);
        ArrayList<String> al = (ArrayList<String>) JonasManagementRepr.getAttribute(on, "deployableFiles", serverName);
        // Sort
        Collections.sort(al);
        return al;
    }

    /**
     * Return the list of JAR filename ready to deploy in the current server.
     * @return The list of JAR filename.
     * @throws ManagementException any.
     */
    public static ArrayList getJarFilesDeployable(final String domainName, final String serverName)
    throws ManagementException {
        ObjectName on = J2eeObjectName.J2EEServer(domainName, serverName);
        ArrayList<String> al = (ArrayList<String>) JonasManagementRepr.getAttribute(on, "deployableJars", serverName);
        // Sort
        Collections.sort(al);
        return al;
    }

    /**
     * Return the list of deployed JAR filename in the current server.
     * @return The list of JAR filename.
     * @throws ManagementException any.
     */
    public static ArrayList getJarFilesDeployed(final String domainName, final String serverName)
    throws ManagementException {
        ObjectName on = J2eeObjectName.J2EEServer(domainName, serverName);
        ArrayList<String> al = (ArrayList<String>) JonasManagementRepr.getAttribute(on, "deployedJars", serverName);
        // Sort
        Collections.sort(al);
        return al;
    }

    /**
     * Return the list of EAR filename ready to deploy in the current server.
     * @return The list of EAR filename.
     * @throws ManagementException any.
     */
    public static ArrayList getEarFilesDeployable(final String domainName, final String serverName)
    throws ManagementException {
        ObjectName on = J2eeObjectName.J2EEServer(domainName, serverName);
        ArrayList<String> al = (ArrayList<String>) JonasManagementRepr.getAttribute(on, "deployableEars", serverName);
        // Sort
        Collections.sort(al);
        return al;
    }

    /**
     * Return the list of WAR filename ready to deploy in the current server.
     * @return The list of WAR filename.
     * @throws ManagementException any.
     */
    public static ArrayList getWarFilesDeployable(final String domainName, final String serverName)
    throws ManagementException {
        ObjectName on = J2eeObjectName.J2EEServer(domainName, serverName);
        ArrayList<String> al = (ArrayList<String>) JonasManagementRepr.getAttribute(on, "deployableWars", serverName);
        // Sort
        Collections.sort(al);
        return al;
    }

    /**
     * Return the list of RAR filename ready to deploy in the current server.
     * @return The list of RAR filename.
     * @throws ManagementException any.
     */
    public static ArrayList getRarFilesDeployable(final String domainName, final String serverName)
    throws ManagementException {
        ObjectName on = J2eeObjectName.J2EEServer(domainName, serverName);
        ArrayList al = (ArrayList) JonasManagementRepr.getAttribute(on, "deployableRars", serverName);
        Collections.sort(al);
        return al;
    }

    /**
     * Return the list of deployed WAR filename in the current server.
     * @return The list of WAR filename.
     * @throws ManagementException any.
     */
    public static ArrayList getWarFilesDeployed(final String domainName, final String serverName)
    throws ManagementException {
        ObjectName on = J2eeObjectName.J2EEServer(domainName, serverName);
        ArrayList al = (ArrayList) JonasManagementRepr.getAttribute(on, "deployedWars", serverName);
        Collections.sort(al);
        return al;
    }

    /**
     * Return the list of deployed EAR filename in the current server.
     * @return The list of EAR filename.
     * @throws ManagementException any.
     */
    public static ArrayList getEarFilesDeployed(final String domainName, final String serverName)
    throws ManagementException {
        ObjectName on = J2eeObjectName.J2EEServer(domainName, serverName);
        ArrayList al = (ArrayList) JonasManagementRepr.getAttribute(on, "deployedEars", serverName);
        Collections.sort(al);
        return al;
    }

    /**
     * Return the list of deployed RAR filename in the current server.
     * @return The list of RAR filename.
     * @throws ManagementException any.
     */
    public static ArrayList getRarFilesDeployed(final String domainName, final String serverName)
    throws ManagementException {
        ObjectName on = J2eeObjectName.J2EEServer(domainName, serverName);
        ArrayList al = (ArrayList) JonasManagementRepr.getAttribute(on, "deployedRars", serverName);
        Collections.sort(al);
        return al;
    }

    /**
     * Return the list of deployed filename in the current server.
     * @return The list of filenames deployed.
     * @throws ManagementException any.
     */
    public static ArrayList getFilesDeployed(final String domainName, final String serverName)
    throws ManagementException {
        ObjectName on = J2eeObjectName.J2EEServer(domainName, serverName);
        ArrayList al = (ArrayList) JonasManagementRepr.getAttribute(on, "deployedFiles", serverName);
        try {
            on = JonasObjectName.deploymentPlan(domainName);
            String[] dplans = (String[]) JonasManagementRepr.getAttribute(on, "DeploymentPlans", serverName);
            for (String dplan : dplans) {
                // Filter out JOnAS internal deployment plans
                boolean filtered = false;
                if (dplan.indexOf(DIR_DEPLAN_JONAS) > 0) {
                    // TODO more intelligent
                    // check that dplan matches JONAS_BASE/conf/deploymenet/toto.xml
                   filtered = true;
                }
                if (!filtered) {
                    al.add(dplan);
                }
            }
        } catch (MalformedObjectNameException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Collections.sort(al);
        return al;
    }
    /**
     * Prepare a list of containers to diplay.
     * Deleting prefix root container directory, sorting the list.
     * @param p_Containers The list of containers
     * @param p_ContainerDir The root container directory
     * @param p_EarDir The root ear directory or null
     * @return The list of containers ready to display
     */
    public static ArrayList prepareContainersToDisplay(final ArrayList p_Containers
            , final String p_ContainerDir, final String p_EarDir) {
        int iPos;
        String sPath;
        boolean bAdd;
        ArrayList al = new ArrayList();
        for (int i = 0; i < p_Containers.size(); i++) {
            // Delete prefix root container directory
            sPath = p_Containers.get(i).toString();
            iPos = sPath.indexOf(p_ContainerDir);
            if (iPos > -1) {
                sPath = sPath.substring(p_ContainerDir.length());
            }
            // Delete suffix slash
            if (sPath.endsWith("/") == true) {
                sPath = sPath.substring(0, sPath.length() - 1);
            }
            // Flag to filter
            bAdd = true;
            // Filter Jar or War include in Ear
            if (p_EarDir != null) {
                iPos = sPath.indexOf(p_EarDir);
                if (iPos > -1) {
                    bAdd = false;
                }

            }
            if (bAdd == true) {
                al.add(sPath);
            }
        }
        // Sort
        Collections.sort(al);
        return al;
    }
    /**
     * Get files deployed in a server
     * @param on The objectName
     * @param serverName the server name.
     * @return the deployed files.
     * @throws ManagementException any.
     */
    private static ArrayList getFilesDeployed(final ObjectName on, final String serverName)
    throws ManagementException {
        ArrayList alDeployed = new ArrayList();
        String sPath;
        String sFile;
        Iterator itNames = getListMBeanName(on, serverName).iterator();
        while (itNames.hasNext()) {
            sPath = extractValueMbeanName("fname", itNames.next().toString());
            sFile = extractFilename(sPath);
            if (sFile != null) {
                alDeployed.add(sFile);
            }
        }
        Collections.sort(alDeployed);
        return alDeployed;

    }

    /**
     * Return the list of Mail factory files ready to deploy in the current server.
     * @return The list of Mail factory properties filenames.
     * @throws ManagementException any.
     */
    public static ArrayList getMailFilesDeployable(final String domainName, final String serverName)
    throws ManagementException {
        ObjectName on = JonasObjectName.mailService(domainName);
        return (ArrayList) JonasManagementRepr.invoke(on, "getMailFactoryPropertiesFiles", new Object[0], new String[0], serverName);
    }

    /**
     * Return the list of MimePartDataSource Mail factory files ready to deploy in the current server.
     * @return The list of MimePartDataSource Mail factory properties filenames.
     * @throws ManagementException any.
     */
    public static ArrayList getMimePartMailFilesDeployable(final String domainName, final String serverName)
    throws ManagementException {
        ObjectName on = JonasObjectName.mailService(domainName);
        return (ArrayList) JonasManagementRepr.invoke(on
                , "getMimePartMailFactoryPropertiesFiles", new Object[0], new String[0], serverName);
    }

    /**
     * Return the list of Session Mail factory files ready to deploy in the current server.
     * @return The list of Session Mail factory properties filenames.
     * @throws ManagementException any.
     */
    public static ArrayList getSessionMailFilesDeployable(final String domainName, final String serverName)
    throws ManagementException {
        ObjectName on = JonasObjectName.mailService(domainName);
        return (ArrayList) JonasManagementRepr.invoke(on, "getSessionMailFactoryPropertiesFiles", new Object[0], new String[0], serverName);
    }

    /**
     * Return the list of deployed MimePartDataSource Mail factories in the current server.
     * @param p_AdminHelper The container AdminHelper
     * @return The list of MimePartDataSource Mail factory filename.
     * @throws ManagementException any.
     */
    public static ArrayList getMimePartMailFilesDeployed(final AdminHelper p_AdminHelper)
    throws ManagementException {
        ArrayList alDeployed = new ArrayList();
        String sName;
        String domainName = p_AdminHelper.getCurrentDomainName();
        String serverName = p_AdminHelper.getCurrentJonasServerName();
        ObjectName on = J2eeObjectName.JavaMailResources(domainName, serverName
                , MailService.MIMEPART_PROPERTY_TYPE);
        Iterator itNames = JonasManagementRepr.queryNames(on, serverName).iterator();
        while (itNames.hasNext()) {
            ObjectName it_on = (ObjectName) itNames.next();
            sName = it_on.getKeyProperty("name");
            if (sName != null) {
                alDeployed.add(sName);
            }
        }
        Collections.sort(alDeployed);
        return alDeployed;
    }


    /**
     * Transform file names with absolute path on mave2 repositories to module (resource) names.
     * @param filesDeployed list of file names
     * @return list of file names having mave2 absolute paths converted
     */
    public static ArrayList<String> getDeployed(final ArrayList<String> filesDeployed) {
        ArrayList<String> result = new ArrayList<String>();
        for (String fileDeployed : filesDeployed) {
            // convert file names to resource names
            boolean convert = false;
            if (fileDeployed.indexOf(REPO_MAVAEN2_INTERNAL) > 0) {
               convert = true;
            } else if (fileDeployed.indexOf(REPO_MAVAEN2_DEV) > 0) {
                convert = true;
            }
            if (convert) {
                result.add(convertDeployed(fileDeployed));
            } else {
                result.add(fileDeployed);
            }
        }
        return result;
    }

    public static String findDeployedPath(final ArrayList<String> filesDeployed, final String resourceName) {
        for (String fileDeployed : filesDeployed) {
            // can convert file names to resource names ?
            boolean convertible = false;
            if (fileDeployed.indexOf(REPO_MAVAEN2_INTERNAL) > 0) {
               convertible = true;
            } else if (fileDeployed.indexOf(REPO_MAVAEN2_DEV) > 0) {
                convertible = true;
            }
            if (convertible) {
                // check that the resource name is a converted name
                if (resourceName.equals(convertDeployed(fileDeployed))) {
                    return fileDeployed;
                }
            }
        }
        return resourceName;
    }

    /**
     * Convert absolute path of resource in maven repository to resource name.
     * @param fileDeployedPath absolute path
     * @return cnverted name
     */
    private static String convertDeployed(final String fileDeployedPath) {
        String convertedName = null;
        int iPosSeparator = fileDeployedPath.lastIndexOf("/");
        if (iPosSeparator < 0) {
            iPosSeparator = fileDeployedPath.lastIndexOf("\\");
            if (iPosSeparator < 0) {
                convertedName = new String(fileDeployedPath);
            } else {
                convertedName = fileDeployedPath.substring(iPosSeparator + 1);
            }
        } else {
            convertedName = fileDeployedPath.substring(iPosSeparator + 1);
        }
        return convertedName;
    }

    /**
     * Return the list of deployed Session Mail factories in the current server.
     * @param p_AdminHelper The container AdminHelper
     * @return The list of Session Mail factory filename.
     * @throws ManagementException any.
     */
    public static ArrayList getSessionMailFilesDeployed(final AdminHelper p_AdminHelper)
    throws ManagementException {
        ArrayList alDeployed = new ArrayList();
        String sName;
        String domainName = p_AdminHelper.getCurrentDomainName();
        String serverName = p_AdminHelper.getCurrentJonasServerName();
        ObjectName on = J2eeObjectName.JavaMailResources(domainName, serverName
                , MailService.SESSION_PROPERTY_TYPE);
        Iterator itNames = JonasManagementRepr.queryNames(on, serverName).iterator();
        while (itNames.hasNext()) {
            ObjectName it_on = (ObjectName) itNames.next();
            sName = it_on.getKeyProperty("name");
            if (sName != null) {
                alDeployed.add(sName);
            }
        }
        Collections.sort(alDeployed);
        return alDeployed;
    }

    /**
     * Return the list of deployed Mail factories in the current server.
     * @param p_AdminHelper The container AdminHelper
     * @return The list of Mail factory filename.
     * @throws ManagementException any.
     */
    public static ArrayList getMailFilesDeployed(final AdminHelper p_AdminHelper)
    throws ManagementException {
        ArrayList alDeployed = getSessionMailFilesDeployed(p_AdminHelper);
        alDeployed.addAll(getMimePartMailFilesDeployed(p_AdminHelper));
        Collections.sort(alDeployed);
        return alDeployed;
    }

    /**
     * Return the list of Datasource properties files ready to deploy in the current server.
     * @return The list of Datasource properties filenames.
     * @throws ManagementException Could not get management info from the MBeanServer
     */
    public static ArrayList getDatasourceFilesDeployable(final String domainName, final String serverName)
    throws ManagementException {
        ObjectName on = JonasObjectName.databaseService(domainName);
        return (ArrayList) JonasManagementRepr.getAttribute(on, "DataSourcePropertiesFiles", serverName);
    }

    /**
     * Return the list of deployed Datasources in the current server.
     * @param domainName Current domain name
     * @param serverName Current server name
     * @return The list of Datasource filename.
     * @throws ManagementException Could not get management info from the MBeanServer
     */
    public static ArrayList getDatasourceFilesDeployed(final String domainName, final String serverName)
    throws ManagementException {

        ArrayList<String> alDeployed = new ArrayList<String>();
        ObjectName ons = J2eeObjectName.JDBCDataSources(domainName, serverName);
        // iterate ovent the JDBCDataSources within the current server
        Iterator itNames = JonasManagementRepr.queryNames(ons, serverName).iterator();
        String sName = null;
        while (itNames.hasNext()) {
            ObjectName on = (ObjectName) itNames.next();
            sName = on.getKeyProperty("name");
            if (sName != null) {
                alDeployed.add(sName);
            }
        }
        Collections.sort(alDeployed);
        return alDeployed;
    }

    /**
     * Return the list of Datasource dependences for a given datasource name in the current server.
     * @param pDatasourceName The name of the datasource
     * @param domainName Current domain name
     * @param serverName Current server name
     * @return The list of Datasource dependence (a list of names corresponding to EJBs using this datasource).
     * @throws ManagementException Could not get management info from the MBeanServer
     * @throws MalformedObjectNameException any.
     */
    public static ArrayList getDatasourceDependences(final String pDatasourceName, final String domainName, final String serverName)
    throws ManagementException {
        ObjectName jdbcDatasource = J2eeObjectName.getJDBCDataSource(domainName, serverName, pDatasourceName);
        String[] asParam = new String[1];
        String[] asSignature = new String[1];
        asSignature[0] = "java.lang.String";
        ArrayList<String> al = new ArrayList<String>();
        asParam[0] = (String) JonasManagementRepr.getAttribute(jdbcDatasource, "jndiName", serverName);
        if (JonasManagementRepr.isRegistered(JonasObjectName.ejbService(domainName), serverName)) {
            String sName;
            Iterator it = ((java.util.Set) JonasManagementRepr.invoke(JonasObjectName.ejbService(domainName)
                    , "getDataSourceDependence", asParam, asSignature, serverName)).iterator();
            while (it.hasNext()) {
                sName = extractValueMbeanName(",name", it.next().toString());
                if (sName != null) {
                    al.add(sName);
                }
            }
            Collections.sort(al);
        }
        return al;
    }

    /**
     * Return the list of Mail factory dependences in the current server for a given factory name.
     * @param p_MailFactoryName The name of the mail factory
     * @param p_AdminHelper The container AdminHelper
     * @return The list of Mail factory dependences (a list of names corresponding to EJBs using this mail factory).
     * @throws ManagementException any.
     */
    public static ArrayList getMailFactoryDependences(final String p_MailFactoryName
            , final AdminHelper p_AdminHelper)
    throws ManagementException {
        String jndiName = null;
        String domainName = p_AdminHelper.getCurrentDomainName();
        String serverName = p_AdminHelper.getCurrentJonasServerName();
        String type = MailService.SESSION_PROPERTY_TYPE;
        // create an MBean name with the given MailFactory name
        // - try with a session Mail Factory type
        ObjectName on = J2eeObjectName.JavaMailResource(domainName, p_MailFactoryName, serverName
                , type);
        //ObjectName on = JonasObjectName.sessionMailFactory(p_MailFactoryName);
        if (JonasManagementRepr.isRegistered(on, serverName) != true) {
            // in this case, this is a MimePartDataSource mail factory
            //on = JonasObjectName.mimeMailFactory(p_MailFactoryName);
            type = MailService.MIMEPART_PROPERTY_TYPE;
            on = J2eeObjectName.JavaMailResource(domainName, p_MailFactoryName, serverName, type);
        }
        jndiName = (String) JonasManagementRepr.getAttribute(on, "Name", serverName);

        String[] asParam = new String[1];
        String[] asSignature = new String[1];
        asSignature[0] = "java.lang.String";
        ArrayList<String> al = new ArrayList<String>();
        asParam[0] = jndiName;
        if (JonasManagementRepr.isRegistered(JonasObjectName.ejbService(domainName), serverName) == true) {
            String sName;
            Iterator it = ((java.util.Set) JonasManagementRepr.invoke(JonasObjectName.ejbService(domainName)
                    , "getMailFactoryDependence", asParam, asSignature, serverName)).iterator();
            while (it.hasNext()) {
                sName = extractValueMbeanName(",name", it.next().toString());
                if (sName != null) {
                    al.add(sName);
                }
            }
            Collections.sort(al);
        }
        return al;
    }

    /**
     * Return the list of deployed Session Mail Factories in the current server.
     * @param p_AdminHelper The container AdminHelper
     * @return The list
     * @throws ManagementException any.
     */
    public static ArrayList getSessionMailFactoriesDeployed(final AdminHelper p_AdminHelper)
    throws ManagementException {

        ArrayList<String> alDeployed = new ArrayList<String>();
        String sName;
        String domainName = p_AdminHelper.getCurrentDomainName();
        String serverName = p_AdminHelper.getCurrentJonasServerName();
        String type = MailService.SESSION_PROPERTY_TYPE;

        ObjectName any_on = J2eeObjectName.JavaMailResources(domainName, serverName, type);
        Iterator itNames = JonasManagementRepr.queryNames(any_on, serverName).iterator();
        while (itNames.hasNext()) {
            ObjectName on = (ObjectName) itNames.next();
            sName = on.getKeyProperty("name");
            if (sName != null) {
                alDeployed.add(sName);
            }
        }

        Collections.sort(alDeployed);
        return alDeployed;
    }

    /**
     * Return the list of deployed MimePartDatasource Mail Factories in the current server.
     * @param p_AdminHelper The container AdminHelper
     * @return The list
     * @throws ManagementException any.
     */
    public static ArrayList getMimeMailPartFactoriesDeployed(final AdminHelper p_AdminHelper)
    throws ManagementException {
        ArrayList<String> alDeployed = new ArrayList<String>();
        String sName;
        String domainName = p_AdminHelper.getCurrentDomainName();
        String serverName = p_AdminHelper.getCurrentJonasServerName();
        String type = MailService.MIMEPART_PROPERTY_TYPE;

        ObjectName any_on = J2eeObjectName.JavaMailResources(domainName, serverName, type);
        Iterator itNames = JonasManagementRepr.queryNames(any_on, serverName).iterator();
        while (itNames.hasNext()) {
            ObjectName on = (ObjectName) itNames.next();
            sName = on.getKeyProperty("name");
            if (sName != null) {
                alDeployed.add(sName);
            }
        }
        Collections.sort(alDeployed);
        return alDeployed;
    }

    /**
     * Return the ObjectName corresponding to the J2EEServer managed object registered in the
     * current MBeanServer. This method is used by EditTopAction to determine the list of
     * JOnAS servers registered in the current registry (this code is particular to the current
     * domain concept implementation).
     * We should have one ObjectName corresponding to the current JOnAS server instance.
     * @return an ObjectName which corresponds to a J2EEServer ObjectName pattern
     * (having <code>j2eeType</code> key property equal to <code>J2EEServer</code>)
     */
    /*
    public static ObjectName getJ2eeServerObjectName(final String serverName) {
        // Lookup for a J2EEServer managed object in the current MBeanServer
        ObjectName pattern_server_on = J2eeObjectName.J2EEServers();
        ObjectName server_on = null;
        Iterator it = JonasManagementRepr.queryNames(pattern_server_on, serverName).iterator();
        if (it.hasNext()) {
            // Got one J2EEServer managed object ; normally should not be more than one
            server_on = (ObjectName) it.next();
        }
        return server_on;
    }*/


    public static ObjectName getTomcatSecurityRealm(final String domainName, final String serverName)
    throws MalformedObjectNameException {
        ObjectName pattern_realms = CatalinaObjectName.catalinaRealms();
        ObjectName realm_on = null;
        Iterator it = JonasManagementRepr.queryNames(pattern_realms, serverName).iterator();
        while (it.hasNext()) {
            ObjectName on = (ObjectName) it.next();
            // Looking for MBeans without path key
            String path = on.getKeyProperty("path");
            if (path == null) {
                realm_on = on;
                break;
            }
        }
        return realm_on;
    }

    public static ArrayList<RealmItem> getTomcatSecurityRealms(final String usedRealmName, final String domainName, final String serverName)
    throws MalformedObjectNameException {

        ArrayList<RealmItem> al = new ArrayList<RealmItem>();

        ObjectName pattern_realms = CatalinaObjectName.catalinaRealms();
        Iterator it = JonasManagementRepr.queryNames(pattern_realms, serverName).iterator();
        while (it.hasNext()) {
            ObjectName on = (ObjectName) it.next();
            // Looking for MBeans with path key
            String path = on.getKeyProperty("path");
            if (path != null) {
                String resourceName = (String) JonasManagementRepr.getAttribute(on, "resourceName", serverName);
                String resourceType = findSecurityFactorySubType(resourceName, domainName, serverName);
                al.add(new RealmItem(resourceName, resourceType, path, resourceName.equals(usedRealmName)));
            }
        }
        Collections.sort(al, new RealmItemByNameComparator());
        return al;
    }

    /**
     * Return the list of Security Memory Factories in the current server.
     * @return The list of Security Memory Factories
     * @throws MalformedObjectNameException any.
     */
    public static ArrayList getSecurityMemoryFactories(final String domainName, final String serverName)
    throws MalformedObjectNameException {
        String sName;
        ArrayList<String> al = new ArrayList<String>();
        Iterator itNames = getListMBeanName(JonasObjectName.allSecurityMemoryFactories(domainName), serverName).iterator();
        while (itNames.hasNext()) {
            sName = extractValueMbeanName(",name", itNames.next().toString());
            if (sName != null) {
                al.add(sName);
            }
        }
        Collections.sort(al);
        return al;
    }

    /**
     * Return the list of Security Datasource Factories in the current server.
     * @return The list of Security Datasource Factories
     * @throws MalformedObjectNameException any.
     */
    public static ArrayList getSecurityDatasourceFactories(final String domainName, final String serverName)
    throws MalformedObjectNameException {
        String sName;
        ArrayList<String> al = new ArrayList<String>();
        Iterator itNames = getListMBeanName(JonasObjectName.allSecurityDatasourceFactories(domainName), serverName).
        iterator();
        while (itNames.hasNext()) {
            sName = extractValueMbeanName(",name", itNames.next().toString());
            if (sName != null) {
                al.add(sName);
            }
        }
        Collections.sort(al);
        return al;
    }

    /**
     * Return the list of Security Ldap Factories in the current server.
     * @return The list of Security Ldap Factories
     * @throws MalformedObjectNameException any.
     */
    public static ArrayList getSecurityLdapFactories(final String domainName, final String serverName)
    throws MalformedObjectNameException {
        String sName;
        ArrayList<String> al = new ArrayList<String>();
        Iterator itNames = getListMBeanName(JonasObjectName.allSecurityLdapFactories(domainName), serverName).iterator();
        while (itNames.hasNext()) {
            sName = extractValueMbeanName(",name", itNames.next().toString());
            if (sName != null) {
                al.add(sName);
            }
        }
        Collections.sort(al);
        return al;
    }

    /**
     * Return the SubType of a Security Factory in the current server.
     * @param p_NameFactory The factory name to find
     * @return The SubType or null if not found
     * @throws ManagementException any.
     * @throws MalformedObjectNameException if the objectName is malformed
     */
    public static String findSecurityFactorySubType(final String p_NameFactory, final String domainName, final String serverName)
    throws MalformedObjectNameException {
        String sName;
        String sSubType = null;
        ObjectName on = null;

        Iterator itNames = getListMBeanName(JonasObjectName.allSecurityFactories(domainName), serverName).iterator();
        while (itNames.hasNext()) {
            on = new ObjectName(itNames.next().toString());
            sName = on.getKeyProperty("name");
            if (p_NameFactory.equals(sName) == true) {
                sSubType = on.getKeyProperty("subtype");
                break;
            }
        }
        return sSubType;
    }

    /**
     * Return the list of all users in a resource.
     * @param domainName the domain name
     * @param p_Resource The resource
     * @return The list of users
     * @throws ManagementException any.
     * @throws MalformedObjectNameException if the objectName is malformed
     */
    public static ArrayList getUsers(final String domainName, final String p_Resource, final String serverName)
    throws ManagementException, MalformedObjectNameException {
        String sName;
        ArrayList<String> al = new ArrayList<String>();
        Iterator itNames = getListMBeanName(JonasObjectName.allUsers(domainName, p_Resource), serverName).iterator();
        while (itNames.hasNext()) {
            sName = extractValueMbeanName(",name", itNames.next().toString());
            if (sName != null) {
                al.add(sName);
            }
        }
        Collections.sort(al);
        return al;
    }

    /**
     * Return the list of all roles in a resource.
     * @param domainName the domain name
     * @param p_Resource The resource
     * @return The list of roles
     * @throws ManagementException any.
     * @throws MalformedObjectNameException if the objectName is malformed
     */
    public static ArrayList getRoles(final String domainName, final String p_Resource, final String serverName)
    throws ManagementException, MalformedObjectNameException {
        String sName;
        ArrayList<String> al = new ArrayList<String>();
        Iterator itNames = getListMBeanName(JonasObjectName.allRoles(domainName, p_Resource), serverName).iterator();
        while (itNames.hasNext()) {
            sName = extractValueMbeanName(",name", itNames.next().toString());
            if (sName != null) {
                al.add(sName);
            }
        }
        Collections.sort(al);
        return al;
    }

    /**
     * Return the list of all groups in a resource.
     * @param domainName the domain name
     * @param p_Resource The resource
     * @return The list of groups
     * @throws ManagementException any.
     * @throws MalformedObjectNameException if the objectName is malformed
     */
    public static ArrayList getGroups(final String domainName, final String p_Resource, final String serverName)
    throws ManagementException, MalformedObjectNameException {
        String sName;
        ArrayList<String> al = new ArrayList<String>();
        Iterator itNames = getListMBeanName(JonasObjectName.allGroups(domainName, p_Resource), serverName).iterator();
        while (itNames.hasNext()) {
            sName = extractValueMbeanName(",name", itNames.next().toString());
            if (sName != null) {
                al.add(sName);
            }
        }
        Collections.sort(al);
        return al;
    }

    /**
     * Return the Queue destinations list.
     * @return The queue list.
     */
    public static ArrayList getQueuesList(final String domainName, final String serverName) {
        synchronized (s_Synchro) {
            ObjectName jmsServMB = JonasObjectName.jmsService(domainName);
            Set queues = (Set) JonasManagementRepr.getAttribute(jmsServMB
                    , "AllJmsQueueDestinationNames", serverName);
            ArrayList<Object> al = new ArrayList<Object>();
            Iterator itNames = queues.iterator();
            while (itNames.hasNext()) {
                al.add(itNames.next());
            }
            return al;
        }
    }

    /**
     * Return the Topic destinations list.
     * @return The topics list
     */
    public static ArrayList getTopicsList(final String domainName, final String serverName) {
        synchronized (s_Synchro) {
            ObjectName jmsServMB = JonasObjectName.jmsService(domainName);
            Set topics = (Set) JonasManagementRepr.getAttribute(jmsServMB
                    , "AllJmsTopicDestinationNames", serverName);
            ArrayList<Object> al = new ArrayList<Object>();
            Iterator itNames = topics.iterator();
            while (itNames.hasNext()) {
                al.add(itNames.next());
            }
            return al;
        }
    }

    /**
     * Create a list for each family of Mbean.
     *
     * @return An array of lists
     * @throws ManagementException
     */
    public static ArrayList getMbeansLists(final String serverName)
        throws ManagementException {

        MbeanItem oItem;

        // Create the new list
        ArrayList<MbeanItem> al = new ArrayList<MbeanItem>();
        // Dispath each Mbean in its list
        Iterator it = getListMbean(null, serverName).iterator();
        while (it.hasNext()) {
            oItem = MbeanItem.build((ObjectName) it.next());
            al.add(oItem);
        }
        return al;
    }

    /**
     * Create a list for each family of Mbean.
     *
     * @return An array of lists
     * @throws ManagementException
     */
    public static ArrayList[] getFamiliesMbeansLists(final String serverName)
        throws ManagementException {

        MbeanItem oItem;

        // Create the new lists
        ArrayList[] als = new ArrayList[3];
        for (int i = 0; i < MbeanItem.SIZE_FAMILIES; i++) {
            als[i] = new ArrayList();
        }
        // Dispath each Mbean in its list
        Iterator it = getListMbean(null, serverName).iterator();
        while (it.hasNext()) {
            oItem = MbeanItem.build((ObjectName) it.next());
            int i = oItem.getFamily();
            als[i].add(oItem);
        }
        return als;
    }

    /**
     * Get all StatefulSessionBeans in a server
     * @param serverName the server name
     * @return the number of StatefulSessionBeans
     */
    public static int getTotalNbStatefulSessionBeans(final String domainName, final String serverName) {
        int nb = 0;
        ObjectName moduleOns = J2eeObjectName.getEJBModules(domainName, serverName);
        Iterator it = null;
        synchronized (s_Synchro) {
            it = JonasManagementRepr.queryNames(moduleOns, serverName).iterator();
        }
        while (it.hasNext()) {
            ObjectName moduleOn = (ObjectName) it.next();
            String[] ejbs = (String[]) JonasManagementRepr.getAttribute(moduleOn, "ejbs", serverName);
            for (int i = 0; i < ejbs.length; i++) {
                String ejbON = ejbs[i];
                try {
                    ObjectName ejbOn = ObjectName.getInstance(ejbON);
                    String ejbType = ejbOn.getKeyProperty("j2eeType");
                    if ("StatefulSessionBean".equals(ejbType)) {
                        nb++;
                    }
                } catch (MalformedObjectNameException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        return nb;
    }

    /**
     * Get the total number of EJBs in a server
     * @param serverName the server name
     * @param domainName the domain name
     * @return map with the number of EJBs of different types where the key is the JSR77 type name
     */
    public static Map getTotalEJB(final String domainName, final String serverName) {
        HashMap<String, Integer> table = new HashMap<String, Integer>();
        int nbStatefulSessionBean = 0;
        int nbStatelessSessionBean = 0;
        int nbEntityBean = 0;
        int nbMessageDrivenBean = 0;
        ObjectName moduleOns = J2eeObjectName.getEJBModules(domainName, serverName);
        Iterator it = null;
        synchronized (s_Synchro) {
            it = JonasManagementRepr.queryNames(moduleOns, serverName).iterator();
        }
        while (it.hasNext()) {
            ObjectName moduleOn = (ObjectName) it.next();
            Map moduleTable = getTotalEJB(moduleOn);
            nbStatefulSessionBean = nbStatefulSessionBean + ((Integer) moduleTable.get("StatefulSessionBean")).intValue();
            nbStatelessSessionBean = nbStatelessSessionBean + ((Integer) moduleTable.get("StatelessSessionBean")).intValue();
            nbEntityBean = nbEntityBean + ((Integer) moduleTable.get("EntityBean")).intValue();
            nbMessageDrivenBean = nbMessageDrivenBean + ((Integer) moduleTable.get("MessageDrivenBean")).intValue();
        }
        table.put("StatefulSessionBean", nbStatefulSessionBean);
        table.put("StatelessSessionBean", nbStatelessSessionBean);
        table.put("EntityBean", nbEntityBean);
        table.put("MessageDrivenBean", nbMessageDrivenBean);
        return table;
    }

    /**
     * Get the total number of EJBs in a EJBModule
     * @param moduleOn the EJBModule's ObjectName
     * @return map with the number of EJBs of different types where the key is the JSR77 type name
     */
    public static Map getTotalEJB(final ObjectName moduleOn) {
        return ContainerManagement.getInstance().getTotalEJB(moduleOn);
    }

    /**
     * Gives the JMX connection Url used in the domain to connect to a remote server.
     * @param remoteServerName name of the remote server
     * @param domainName domain name
     * @param serverName local server name
     * @return the JMX connection Url used in the domain to connect to a remote server
     * @throws MalformedObjectNameException
     */
    public static String getConnectionUrl(final String remoteServerName, final String domainName, final String serverName) throws MalformedObjectNameException {
        ObjectName on = JonasObjectName.serverProxy(domainName, remoteServerName);
        return (String) JonasManagementRepr.getAttribute(on, "ConnectionUrl", serverName);
    }
}
