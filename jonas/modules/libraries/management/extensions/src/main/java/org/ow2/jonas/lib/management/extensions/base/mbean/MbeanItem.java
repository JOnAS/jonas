/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.management.extensions.base.mbean;

import javax.management.ObjectName;

import org.ow2.jonas.lib.management.extensions.base.NameItem;


/**
 * @author Michel-Ange ANTON
 */
public class MbeanItem implements NameItem {

// --------------------------------------------------------- Constants

    public static final int FAMILY_UNKNOWN = 0;
    public static final int FAMILY_OWNER = 1;
    public static final int FAMILY_J2EE = 2;

    public static final int SIZE_FAMILIES = 3;
    public static final String[] ALL_FAMILY_TEXT = {
        "unknown", "owner", "j2ee"};

// --------------------------------------------------------- Properties Variables

    private int family = FAMILY_UNKNOWN;
    private String familyText = null;
    private String objectName = null;
    private String domain = null;
    private String name = null;

// --------------------------------------------------------- Constructors

    public MbeanItem() {
        initialize();
    }

    public MbeanItem(ObjectName p_ObjectName) {
        initialize();
        setObjectName(p_ObjectName.toString());
        setDomain(p_ObjectName.getDomain());
    }

// --------------------------------------------------------- Protected Methods

    protected void initialize() {
        setFamily(FAMILY_UNKNOWN);
    }

// --------------------------------------------------------- Public Methods

    /**
     * Build the good instance of MbeanItem, J2eeMbeanItem or OwnerMbeanItem.
     *
     * @param p_ObjectName The MBean to build
     * @return A instance of MbeanItem, J2eeMbeanItem or OwnerMbeanItem.
     */
    public static MbeanItem build(ObjectName p_ObjectName) {
        MbeanItem oItem = null;
        if (p_ObjectName.getKeyProperty("j2eeType") != null) {
            oItem = new J2eeMbeanItem(p_ObjectName);
        }
        else if (p_ObjectName.getKeyProperty("type") != null) {
            oItem = new OwnerMbeanItem(p_ObjectName);
        }
        else {
            oItem = new MbeanItem(p_ObjectName);
        }
        return oItem;
    }

// --------------------------------------------------------- Properties Methods

    public int getFamily() {
        return family;
    }

    public void setFamily(int family) {
        this.family = family;
    }

    public int sizeFamilies() {
        return SIZE_FAMILIES;
    }

    public String getTextFamily() {
        return ALL_FAMILY_TEXT[family];
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}