/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2009 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Michel-Ange ANTON
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.management.extensions.container.ejb;

import java.util.Comparator;

public class EjbItemByNameComparator implements Comparator {

// --------------------------------------------------------- Public Methods

    public int compare(final Object p_O1, final Object p_O2) {
        EjbItem oEjb1 = (EjbItem) p_O1;
        EjbItem oEjb2 = (EjbItem) p_O2;

        int iRet = oEjb1.getName().compareToIgnoreCase(oEjb2.getName());
        if (iRet == 0) {
            iRet = oEjb1.getFilename().compareToIgnoreCase(oEjb2.getFilename());
        }
        return iRet;
    }

    @Override
    public boolean equals(final Object p_Obj) {
        if (p_Obj instanceof EjbItem) {
            return true;
        }
        return false;
    }
}
