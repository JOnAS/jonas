package org.ow2.jonas.lib.management.extensions.base;

import javax.management.remote.JMXConnector;

public class ConnectorItem {

    private String jmxUrlString = null;
    private String username = null;
    private String password = null;
    private JMXConnector connector = null;

    public String getJmxUrlString() {
        return jmxUrlString;
    }
    public void setJmxUrlString(final String jmxUrlString) {
        this.jmxUrlString = jmxUrlString;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(final String username) {
        this.username = username;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(final String password) {
        this.password = password;
    }
    public JMXConnector getConnector() {
        return connector;
    }
    public void setConnector(final JMXConnector connector) {
        this.connector = connector;
    }

    public boolean equals(final String jmxUrlString, final String username, final String password) {
        boolean result = true;
        if (!this.jmxUrlString.equals(jmxUrlString)) {
            result = false;
            return result;
        }
        if (this.password != null && !this.password.equals(password)) {
            result = false;
            return result;
        }
        if (this.username != null && !this.username.equals(username)) {
            result = false;
            return result;
        }
        return result;
    }
}
