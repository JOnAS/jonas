/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2009 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.management.extensions.base.mbean;

import javax.management.ObjectName;


/**
 * @author Michel-Ange ANTON
 */
public class J2eeMbeanItem extends MbeanItem {

// --------------------------------------------------------- Constants

    public static final String KEY_TYPE = "j2eeType";
    public static final String KEY_SERVER = "J2EEServer";
    public static final String KEY_APPLICATION = "J2EEApplication";
    public static final String KEY_NAME = "name";

    public static final String NONE = "none";


// --------------------------------------------------------- Properties Variables

    private String j2eeType = null;
    private String j2eeServer = null;
    private String j2eeApplication = null;

// --------------------------------------------------------- Constructors

    public J2eeMbeanItem() {
        super();
    }

    public J2eeMbeanItem(final ObjectName p_ObjectName) {
        super(p_ObjectName);
        setJ2eeType(p_ObjectName.getKeyProperty(KEY_TYPE));
        setJ2eeServer(p_ObjectName.getKeyProperty(KEY_SERVER));
        setJ2eeApplication(p_ObjectName.getKeyProperty(KEY_APPLICATION));
        setName(p_ObjectName.getKeyProperty(KEY_NAME));
    }

// --------------------------------------------------------- Protected Methods

    @Override
    protected void initialize() {
        setFamily(FAMILY_J2EE);
    }

// --------------------------------------------------------- Properties Methods

    public String getJ2eeType() {
        return j2eeType;
    }

    public void setJ2eeType(final String j2eeType) {
        this.j2eeType = j2eeType;
    }

    public String getJ2eeServer() {
        return j2eeServer;
    }

    public void setJ2eeServer(final String j2eeServer) {
        this.j2eeServer = j2eeServer;
    }

    public String getJ2eeApplication() {
        return j2eeApplication;
    }

    public void setJ2eeApplication(final String j2eeApplication) {
        this.j2eeApplication = j2eeApplication;
    }
}