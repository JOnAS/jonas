/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2009 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.management.extensions.base;


/**
 * @author Michel-Ange ANTON
 */
public class RealmItem implements NameItem {

// --------------------------------------------------------- Properties Variables

    private String name = null;
    private String type = null;
    private String contextPath = null;
    private boolean usedBySecurityService = false;

// --------------------------------------------------------- Constructors

    public RealmItem() {
    }

    public RealmItem(String p_Name, String p_Type) {
        setName(p_Name);
        setType(p_Type);
    }

    public RealmItem(String p_Name, String p_Type, boolean p_UsedBySecurityService) {
        setName(p_Name);
        setType(p_Type);
        setUsedBySecurityService(p_UsedBySecurityService);
    }

	/**
	 * @param name
	 * @param type
	 * @param contextPath
	 * @param usedBySecurityService
	 */
	public RealmItem(String p_Name, String p_Type, String p_ContextPath,
			boolean p_UsedBySecurityService) {
		super();
        setName(p_Name);
        setType(p_Type);
        setUsedBySecurityService(p_UsedBySecurityService);
        setContextPath(p_ContextPath);
	}
// --------------------------------------------------------- Properties Methods

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isUsedBySecurityService() {
        return usedBySecurityService;
    }

    public void setUsedBySecurityService(boolean usedBySecurityService) {
        this.usedBySecurityService = usedBySecurityService;
    }

	/**
	 * @return Returns the contextPath.
	 */
	public String getContextPath() {
		return contextPath;
	}
	/**
	 * @param contextPath The contextPath to set.
	 */
	public void setContextPath(String contextPath) {
		this.contextPath = contextPath;
	}
}