/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Michel-Ange ANTON
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.management.extensions.container.ejb;

import java.net.URL;

import javax.management.ObjectName;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.management.extensions.base.NameItem;
import org.ow2.jonas.lib.management.javaee.J2eeObjectName;
import org.ow2.jonas.management.extensions.base.api.ManagementException;

public class EjbItem implements NameItem {

// --------------------------------------------------------- Public Constants

    //public final static String EJB_BMP_STRING = "Bmp";
    //public final static String EJB_CMP_STRING = "Cmp";
    public final static String EJB_ENT_STRING = "Ent";
    public final static String EJB_SBF_STRING = "Sbf";
    public final static String EJB_SBL_STRING = "Sbl";
    public final static String EJB_MDB_STRING = "Mdb";

// --------------------------------------------------------- Properties Variables

    private String type = null; // J2eeType
    private String filename = null;
    private String name = null;
    private String typeString = null;// this is used to construct action names
    //private String typeStringResource = null;
    private String objectName = null;

// --------------------------------------------------------- Constructors

    public EjbItem() {
        type = null;
        filename = null;
        name = null;
        objectName = null;
    }
    public EjbItem(ObjectName p_On, String serverName) throws ManagementException {
        setName(p_On.getKeyProperty("name"));
        setType(p_On.getKeyProperty("j2eeType"));
        String fname = null;
        try {
            fname = (String) JonasManagementRepr.getAttribute(p_On, "fileName", serverName);
        } catch (ManagementException e) {
            Throwable cause = e.getCause();
            if (cause.getClass().getName().equals("javax.management.AttributeNotFoundException")) {
                // maybe ejb3 - TO DO check this
                String moduleName = p_On.getKeyProperty("EJBModule");
                String applicationName = p_On.getKeyProperty("J2EEApplication");
                ObjectName moduleOn = J2eeObjectName.getEJBModule(p_On.getDomain(), serverName, applicationName, moduleName);
                if (JonasManagementRepr.isRegistered(moduleOn, serverName)) {
                    URL moduleUrl = (URL) JonasManagementRepr.getAttribute(moduleOn, "url", serverName);
                    fname = moduleUrl.getPath();
                }
            }
        }
        setFilename(fname);
        setObjectName(p_On.toString());
    }

// --------------------------------------------------------- Properties Methods

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
        if (type.equals("StatelessSessionBean")) {
            typeString = EJB_SBL_STRING;
        } else if (type.equals("StatefulSessionBean")) {
            typeString = EJB_SBF_STRING;
        } else if (type.equals("EntityBean")) {
            typeString = EJB_ENT_STRING;
        } else if (type.equals("MessageDrivenBean")) {
            typeString = EJB_MDB_STRING;
        } else {
            typeString = "?";
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getTypeString() {
        return typeString;
    }
}
