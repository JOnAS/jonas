/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.lib.management.extensions.base;

import org.ow2.jonas.service.ServiceException;
/**
 * Any administration Exception.
 * @author eyindanga
 *
 */

public class AdminException extends ServiceException {
 /**
     *
     */
    private static final long serialVersionUID = 1L;
// --------------------------------------------------------- Protected variables

    /**
     *  Error ID
     */
    protected String m_Id = null;

// --------------------------------------------------------- Public Methods

    public AdminException(final String p_Id, final String p_Message, final Throwable p_Throwable) {
        super(p_Message, p_Throwable);
        m_Id = p_Id;
    }

    public AdminException(final String p_Id, final String p_Message) {
        super(p_Message);
        m_Id = p_Id;
    }

    public AdminException(final String p_Id) {
        super("");
        m_Id = p_Id;
    }

    /**
     * Accessor to the error ID.
     * @return String The error ID
     */
    public String getId() {
        return m_Id;
    }

}
