/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.lib.management.extensions.base.mbean;

import javax.management.JMException;
import javax.management.ObjectName;

/**
 * Utility class used to construct J2EE MBean ObjectNames.
 * @author danesa
 *
 */
public class J2EEObjectNames {
    public static final String NAME_KEY = "name";

    /**
     * @return J2EEServer MBean ObjectNames.
     * @throws JMException
     * @throws Exception
     */
    public static ObjectName getJ2EEServerObjectNames() throws JMException, Exception {
        return ObjectName.getInstance("*:j2eeType=J2EEServer,*");
    }
    /**
     * @return J2EEDomain MBean ObjectNames.
     * @throws JMException
     * @throws Exception
     */
    public static ObjectName getJ2EEDomainObjectNames() throws JMException, Exception {
        return ObjectName.getInstance("*:j2eeType=J2EEDomain,*");
    }
}
