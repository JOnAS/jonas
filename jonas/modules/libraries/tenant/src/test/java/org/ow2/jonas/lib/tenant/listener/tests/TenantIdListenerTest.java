/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.tenant.listener.tests;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.ow2.easybeans.api.event.bean.EZBMessageDrivenInfo;
import org.ow2.jonas.lib.tenant.context.TenantContext;
import org.ow2.jonas.lib.tenant.context.TenantCurrent;
import org.ow2.jonas.lib.tenant.listener.TenantEventListener;
import org.ow2.util.ee.metadata.ejbjar.api.struct.IActivationConfigProperty;
import org.ow2.util.ee.metadata.ejbjar.impl.struct.JActivationConfigProperty;
import org.ow2.util.event.api.IEvent;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

/**
 * Test TenantId Event listener
 * @author Mohammed Boukada
 */
public class TenantIdListenerTest {

    TenantEventListener eventListener = new TenantEventListener("/");

    @Mock
    IEvent event;

    @Mock
    EZBMessageDrivenInfo messageDrivenInfo;

    List<IActivationConfigProperty> properties = new ArrayList();

    TenantContext oldTenantContext;

    @BeforeTest
    public void init() {
        MockitoAnnotations.initMocks(this);
        when(messageDrivenInfo.getActivationConfigProperties()).thenReturn(properties);
        oldTenantContext = TenantCurrent.getCurrent().getTenantContext();
        TenantContext tenantContext = new TenantContext("T1");
        TenantCurrent.getCurrent().setTenantContext(tenantContext);
    }

    @Test
    public void testAccept() {
        boolean r = eventListener.accept(messageDrivenInfo);
        Assert.assertEquals(r, false, "Must return false because there is no properties in MessageDrivenInfo object");
        
        r = eventListener.accept(event);
        Assert.assertEquals(r, false, "Event is not a EZBMessageDrivenInfo instance");

        JActivationConfigProperty property = new JActivationConfigProperty("name", "value");
        properties.add(property);
        r = eventListener.accept(messageDrivenInfo);
        Assert.assertEquals(r, true, "Must return false because properties is not empty");
    }

    @Test(dependsOnMethods="testAccept")
    public void testHandle() {
        // event is not an instance of EZBMessageDrivenInfo
        // This case will never happen since accept() method filters that
        eventListener.handle(event);

        String value =  properties.get(0).propertyValue();
        eventListener.handle(messageDrivenInfo);
        Assert.assertEquals(properties.get(0).propertyValue(), value, "Must not be modified since it is not a destination property");

        JActivationConfigProperty property = new JActivationConfigProperty("destination", "destinationValue");
        value = property.propertyValue();
        properties.add(property);
        eventListener.handle(messageDrivenInfo);
        Assert.assertEquals(properties.get(1).propertyValue(),
                TenantCurrent.getCurrent().getTenantContext().getTenantId() + "/" + value,
                "'destination' property value must be updated by adding tenantId");

    }

    @AfterTest
    public void unSetTenantContext () {
        TenantCurrent.getCurrent().setTenantContext(oldTenantContext);
    }
}
