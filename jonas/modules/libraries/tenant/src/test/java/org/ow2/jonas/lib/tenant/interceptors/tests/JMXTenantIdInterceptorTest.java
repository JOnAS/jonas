/**
* JOnAS: Java(TM) Open Application Server
* Copyright (C) 2012 Bull S.A.S.
* Contact: jonas-team@ow2.org
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*
* --------------------------------------------------------------------------
* $Id$
* --------------------------------------------------------------------------
*/

package org.ow2.jonas.lib.tenant.interceptors.tests;

import org.ow2.jonas.lib.tenant.context.TenantContext;
import org.ow2.jonas.lib.tenant.context.TenantCurrent;
import org.ow2.jonas.lib.tenant.interceptor.jmx.JMXTenantIdInterceptor;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.interceptor.InvocationContext;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.QueryExp;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
* Test MBeanServer calls interception
* @author Mohammed Boukada
*/
public class JMXTenantIdInterceptorTest {

    JMXTenantIdInterceptor JMXTenantIdInterceptor = new JMXTenantIdInterceptor("tenant-id", true);

    TenantContext oldTenantContext;

    @BeforeMethod
    public void setUp() throws Exception {
        oldTenantContext = TenantCurrent.getCurrent().getTenantContext();
        TenantContext tenantContext = new TenantContext("T1");
        TenantCurrent.getCurrent().setTenantContext(tenantContext);
    }

    @AfterMethod
    public void unSetTenantContext () {
        TenantCurrent.getCurrent().setTenantContext(oldTenantContext);
    }

    @Test
    public void testUpdateObjectName() throws Exception {
        // Construct list of methods to test
        List<Method> methodNames0 = new ArrayList<Method>();
        List<Method> methodNames1 = new ArrayList<Method>();

        for (Method m : MBeanServer.class.getMethods()) {
            String methodName = m.getName();
            if (methodName.equals("registerMBean")
                    || methodName.equals("createMBean")
                    ){

                methodNames1.add(m);
            } else if (methodName.equals("unregisterMBean")
                    || methodName.equals("getObjectInstance")
                    || methodName.equals("queryMBeans")
                    || methodName.equals("queryNames")
                    || methodName.equals("isRegistered")
                    || methodName.equals("getAttribute")
                    || methodName.equals("getAttributes")
                    || methodName.equals("setAttribute")
                    || methodName.equals("setAttributes")
                    || methodName.equals("invoke")
                    || methodName.equals("addNotificationListener")
                    || methodName.equals("removeNotificationListener")
                    || methodName.equals("getMBeanInfo")
                    || methodName.equals("isInstanceOf")) {

                methodNames0.add(m);
            }
        }

        ObjectName expectedObjectName = new ObjectName("domaine:name=mbean");
        Object[] expectedParameters = new Object[]{expectedObjectName};
        ObjectName objectName = new ObjectName("domaine:name=mbean");
        MBeanServer targetMBeanServer = mock(MBeanServer.class);
        Set mbeans = new HashSet();
        for (Method m : methodNames0) {
            Object[] parameters = new Object[]{objectName, null};

            InvocationContext invocationContext = mock(InvocationContext.class);
            when(invocationContext.getMethod()).thenReturn(m);
            when(invocationContext.getParameters()).thenReturn(parameters);
            when(invocationContext.getTarget()).thenReturn(targetMBeanServer);
            when(targetMBeanServer.queryNames(any(ObjectName.class), any(QueryExp.class))).thenReturn(mbeans);

            JMXTenantIdInterceptor.invoke(invocationContext);

            verify(invocationContext, never()).setParameters(expectedParameters);
            verify(invocationContext).proceed();
        }

        expectedObjectName = new ObjectName("domaine:name=mbean,tenant-id=T1");
        expectedParameters = new Object[]{null, expectedObjectName};
        for (Method m : methodNames1) {
            Object[] parameters = new Object[]{null, objectName};

            InvocationContext invocationContext = mock(InvocationContext.class);
            when(invocationContext.getMethod()).thenReturn(m);
            when(invocationContext.getParameters()).thenReturn(parameters);

            JMXTenantIdInterceptor.invoke(invocationContext);

            verify(invocationContext).setParameters(expectedParameters);
            verify(invocationContext).proceed();
        }
    }
}