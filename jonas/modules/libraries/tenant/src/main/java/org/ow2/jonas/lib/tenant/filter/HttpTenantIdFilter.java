/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.tenant.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.tenant.context.TenantContext;
import org.ow2.jonas.lib.tenant.context.TenantCurrent;

/**
 * Implementation of the HTTP filter for the tenant context
 * @author Mohammed Boukada
 */
public class HttpTenantIdFilter implements Filter {

    /**
     * Context of the filter
     */
    private TenantContext ctx = null;

    /**
     * Config of the filter.
     */
    private FilterConfig filterConfig = null;

    /**
     * The constructor with build of the tenant context.
     * @param tenantId The tenant identifier
     */
    public HttpTenantIdFilter(String tenantId){
        this.ctx = new TenantContext(tenantId);
    }

    /**
     * Init method for this filter.
     * @param filterConfig the filter config to init.
     */
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
    }

    /**
     * Do the filter process.
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     * @exception java.io.IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
            throws IOException, ServletException {

        TenantContext old = null;
        try{
            // Save the current context
            old = TenantCurrent.getCurrent().getTenantContext();
            TenantCurrent.getCurrent().setTenantContext(this.ctx);

            // Invoke request
            HttpServletResponse httpServletResponse = (HttpServletResponse) response;
            chain.doFilter(request, httpServletResponse);

        } catch (Exception e){

        } finally {
            // Restore the old context
            TenantCurrent.getCurrent().setTenantContext(old);
        }
    }

    /**
     * Destroy method for this filter.
     */
    public void destroy() {}
}
