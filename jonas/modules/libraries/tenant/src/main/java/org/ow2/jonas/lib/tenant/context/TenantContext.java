/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.tenant.context;

import java.io.Serializable;

/**
 * Implementation of the JOnAS Tenant Context
 * @author Mohammed Boukada
 */
public class TenantContext implements Serializable {
    /**
     * Tenant identifier
     */
    private String tenantId = null;

    /**
     * Tenant identifier used during a JMX session
     */
    private String jmxSessionTenantId = null;

    /**
     * Application instance name
     */
    private String instanceName = null;

    /**
     * Default tenant identifier
     */
    public static final String DEFAULT_TENANT_ID = "T0";


    /**
     * Constructor TenantContext use the default tenant id
     */
    public TenantContext() {}

    /**
     * Constructor TenantContext
     * @param tenantId tenant identifier
     */
    public TenantContext(String tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * Constructor TenantContext
     * @param tenantId
     * @param jmxSessionTenantId
     */
    public TenantContext(String tenantId, String jmxSessionTenantId) {
        this.tenantId = tenantId;
        this.jmxSessionTenantId = jmxSessionTenantId;
    }

    /**
     * Constructor TenantContext
     * @param tenantId
     * @param jmxSessionTenantId
     * @param instanceName
     */
    public TenantContext(String tenantId, String jmxSessionTenantId, String instanceName) {
        this.tenantId = tenantId;
        this.jmxSessionTenantId = jmxSessionTenantId;
        this.instanceName = instanceName;
    }

    /**
     * Get the tenant identifier
     * @return tenantId
     */
    public String getTenantId(){
        return this.tenantId;
    }

    /**
     * Get the JMX Session Tenant identifier
     * @return tenantId
     */
    public String getJmxSessionTenantId() {
        return this.jmxSessionTenantId;
    }

    /**
     * Get application instance name
     * @return instance name
     */
    public String getInstanceName() {
        return this.instanceName;
    }

    /**
     * Set tenant id in TenantContext
     * @param tenantId
     */
    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * Set the JMX Session tenant id in TenantContext
     * @param jmxSessionTenantId
     */
    public void setJmxSessionTenantId(String jmxSessionTenantId) {
        this.jmxSessionTenantId = jmxSessionTenantId;
    }

    /**
     * Set application instance name in TenantContext
     * @param instanceName
     */
    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }
}
