/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.service.manager;

import org.ow2.util.ee.deploy.api.deployable.EARDeployable;
import org.ow2.util.ee.deploy.api.deployable.EJB21Deployable;
import org.ow2.util.ee.deploy.api.deployable.EJB3Deployable;
import org.ow2.util.ee.deploy.api.deployable.IDeployable;
import org.ow2.util.ee.deploy.api.deployable.RARDeployable;
import org.ow2.util.ee.deploy.api.deployable.WARDeployable;

/**
 * Enumeration of JOnAS deployables and their associated service (deployer starter).
 * @author Francois Fornaciari
 */
public enum DeployableEnumeration {
    EAR ("ear", EARDeployable.class),
    EJB2 ("ejb2", EJB21Deployable.class),
    EJB3 ("ejb3", EJB3Deployable.class),
    RAR ("resource", RARDeployable.class),
    WAR ("web", WARDeployable.class);

    /**
     * The class of the deployable associated to the service.
     */
    private Class<?> clazz = null;

    /**
     * The service name.
     */
    private String service = null;

    /**
     * Private constructor.
     * @param serviceName The service name.
     * @param clazz The class of the deployable associated to the service.
     */
    private DeployableEnumeration(final String serviceName, final Class<?> clazz) {
        this.clazz = clazz;
        this.service = serviceName;
    }

    /**
     * Returns the service name for a given deployable.
     * @param deployable The given deployable
     * @return The service name for a given deployable.
     */
    public static String getService(final IDeployable<?> deployable) {
        for (DeployableEnumeration value : DeployableEnumeration.values()) {
            if (value.clazz.isInstance(deployable)) {
                return value.service;
            }
        }
        return null;
    }
}
