/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.service.manager;

import org.ow2.jonas.management.ServiceManager;
import org.ow2.util.ee.deploy.api.deployable.IDeployable;
import org.ow2.util.ee.deploy.api.deployer.IDeployerManagerCallback;

/**
 * Implementation of the {@IDeployerManagerCallback}.
 * Allow to start required JOnAS services declared in the MANIFEST file of the deployable.
 * @author Francois Fornaciari
 */
public class RequireJOnASServicesHandler implements IDeployerManagerCallback {

    /**
     * Reference to the {@link ServiceManager} service.
     */
    private ServiceManager serviceManager = null;

    /**
     * Build the RequireJOnASServicesHandler
     * @param serviceManager The ServiceManager service reference
     */
    public RequireJOnASServicesHandler(final ServiceManager serviceManager) {
        this.serviceManager = serviceManager;
    }

    /**
     * {@inheritDoc}
     */
    public void preDeploy(final IDeployable<?> deployable) {
        // Start required JOnAS services declared in the MANIFEST file of the deployable
        serviceManager.startRequiredServices(deployable);
    }

    /**
     * {@inheritDoc}
     */
    public void postDeploy(final IDeployable<?> deployable) {
        // Do nothing
    }

    /**
     * {@inheritDoc}
     */
    public void preUndeploy(final IDeployable<?> deployable) {
        // Do nothing
    }

    /**
     * {@inheritDoc}
     */
    public void postUndeploy(final IDeployable<?> deployable) {
        // Do nothing
    }
}
