/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.jmbeans.log;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.TreeSet;

import org.objectweb.util.monolog.api.Handler;
import org.objectweb.util.monolog.api.HandlerFactory;
import org.objectweb.util.monolog.api.Level;
import org.objectweb.util.monolog.api.Logger;
import org.objectweb.util.monolog.api.LoggerFactory;
import org.ow2.jonas.lib.management.javaee.ManagedObject;
import org.ow2.jonas.lib.management.reconfig.PropertiesConfigurationData;
import org.ow2.jonas.lib.util.Log;

/**
 * MBean implementation for log management.
 * The managed resource is the monolog {@link LoggerFactory}.
 */
public class LogManagement extends ManagedObject {

    /**
     * Value used as sequence number by reconfiguration notifications.
     */
    private long sequenceNumber = 0;

    /**
     *  Name as used to label configuration properties.
     */
    public static final String SERVICE_NAME = "log";

    /**
     * Configuration properties.
     */
    private Properties props = null;

    /**
     * Configuration file name.
     */
    private String configFile = null;

    /**
     * The unique instance of {@link org.ow2.jonas.lib.jmbeans.log.LogManagement}.
     */
    private static LogManagement unique = null;

    public LogManagement() {

    }

    /**
     * @return the topics list. Assumes that all Loggers are TopicalLoggers.
     */
    public String[] getTopics() {
        Logger[] logs = Log.getLoggerFactory().getLoggers();
        // put names in alphabetical order
        TreeSet tset = new TreeSet();
        for (int i = 0; i < logs.length; i++) {
            tset.add(logs[i].getName());
        }
        return (String[]) tset.toArray(new String[0]);
    }

    /**
     * Return a topic's level
     * @param topic the topic we need ti know its level
     * @return the topic's level
     */
    public String getTopicLevel(final String topic) {
        Logger topicLogger = Log.getLoggerFactory().getLogger(topic);
        Level lev = topicLogger.getCurrentLevel();
        return lev.getName();
    }

    /**
     * set Topic Level
     * @param topic topic to set
     * @param level the level to set
     */
    public void setTopicLevel(final String topic, final String level) {
        Logger topicLogger = Log.getLoggerFactory().getLogger(topic);
        Level lev = Log.getLevelFactory().getLevel(level);
        // must check null (bug monolog)
        if (lev != null) {
            topicLogger.setLevel(lev);
        } else {
            // TO DO maybe a better error treatement could be found
            throw new RuntimeException("Unknown level " + level);
        }
        // the modified property name is 'logger.topic.level'
        String propName = "logger." + topic + ".level";
        // Send a notification containing the new value of this property to the
        // listner MBean
        sendReconfigNotification(++sequenceNumber, SERVICE_NAME, new PropertiesConfigurationData(propName, level));
    }

    /**
     * @return list of properties for logging system
     */
    public Properties getProperties() {
        Properties props = Log.getProperties();
        if (props == null) {
            Log.getLoggerFactory();
            props = Log.getProperties();
        }
        return props;
    }


    /**
     * Save updated configuration.
     */
    public void saveConfig() {
        // Send save reconfig notification
        sendSaveNotification(++sequenceNumber, SERVICE_NAME);
    }


    /**
     * Returns the names of the Monolog handlers
     * @return The handler names defines in Monolog
     */
    public String[] getHandlerNames() {
        LoggerFactory lf = Log.getLoggerFactory();

        if (lf instanceof HandlerFactory) {
            HandlerFactory mf = (HandlerFactory) lf;
            Handler[] hs = mf.getHandlers();
            String[] hns = new String[hs.length];
            for (int i = 0; i < hs.length; i++) {
                hns[i] = hs[i].getName();
            }
            return hns;
        }
        return null;
    }

    /**
     * Getter for the map of the attributes of a handler.
     * @param handlername the handler name
     * @return The map of the attributes defines for the handler
     */
    public Map getHandlerAttributes(final String handlername) {
        LoggerFactory lf = Log.getLoggerFactory();

        if (lf instanceof HandlerFactory) {
            HandlerFactory mf = (HandlerFactory) lf;

            Handler h = mf.getHandler(handlername);
            String[] ans = h.getAttributeNames();
            Map m = new HashMap(ans.length);
            for (int i = 0; i < ans.length; i++) {
                m.put(ans[i], h.getAttribute(ans[i]));
            }
            return m;
        }
        return null;
    }

    /**
     * @return Configuration properties
     */
    public Properties getProps() {
        return props;
    }

    /**
     * @param props Configuration properties
     */
    public void setProps(final Properties props) {
        this.props = props;
    }

    /**
     * @return Configuration file name
     */
    public String getConfigFile() {
        return configFile;
    }

    /**
     * @param configFile Configuration file name
     */
    public void setConfigFile(final String configFile) {
        this.configFile = configFile;
    }
}
