/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.jmbeans.log;


import java.util.LinkedList;
import java.util.TreeSet;
import java.util.logging.LogRecord;

/**
 * @author waeselyf
 *
 */
public class RecordFeeder implements Runnable {

    /**
     * Construct the LogBuffer's <code>recordSet</code> feeder.
     * The feeder extracts maxRecFerch records from the LogRecords <code>source</code>
     * and adds them to the <code>records</code> TreeSet.
     *
     * @param queue source of LogRecords
     * @param recordSet LogRecords TreeSet to update
     */
    public RecordFeeder(final LinkedList queue, final TreeSet recordSet) {
        source = queue;
        records = recordSet;
    }

    /**
     */
    public void run() {
        LogRecord[] recs = null;

        while (!Thread.currentThread().isInterrupted()) {

            recs = readRecords();
            if (recs != null) {

                /*
                 * prevent overflow
                 */
                int overflow = recs.length + records.size() - maxRecords;
                if (overflow > 0) {
                    // remove the first overflow records
                    synchronized (records) {
                        for (int i = 0; i < overflow; i++) {
                            records.remove(records.first());
                        }
                    }
                }
                /*
                 * Add records
                 */
                synchronized (records) {
                    for (int i = 0; i < recs.length; i++) {
                        records.add(recs[i]);
                    }
                }
                Thread.yield();
            }
        }
        synchronized (records) {
            records.clear();
        }

        synchronized (source) {
            source.clear();
        }
    }

    /**
     * Read from the source queue Waits for a while if necessary A post is
     * assumed to notify the thread.
     * @return an array of records containing maximum maxRecFetch LogRecord extracted from
     * the source queue, or null if the thread is interrupted
     */
    public LogRecord[] readRecords() {

        LogRecord[] records = null;
        while (!Thread.currentThread().isInterrupted() && (records == null)) {
            synchronized (source) {
                int sz = source.size();
                if (sz > 0) {
                    sz = (sz > maxRecFetch ? maxRecFetch : sz);
                    records = new LogRecord[sz];
                    for (int i = 0; i < sz; i++) {
                        records[i] = (LogRecord) source.removeFirst();
                    }
                }
            }
            if (records == null) {
                try {
                    synchronized (this) {
                        wait(timeout);
                    }
                } catch (InterruptedException e) {
                    // Nothing to do, simply loops
                }
            }
        }

        return records;
    }

    public void setMaxRecords(final int max) {
        maxRecords = max;
    }

    public int getMaxRecords() {
        return maxRecords;
    }

    public void setMaxRecFetch(final int max) {
        maxRecFetch = max;
    }

    public int getMaxRecFetch() {
        return maxRecords;
    }

    private int maxRecFetch = 10;

    private long timeout = 333;

    private LinkedList source = null;

    private TreeSet records = null;

    private int maxRecords = 100000;

}
