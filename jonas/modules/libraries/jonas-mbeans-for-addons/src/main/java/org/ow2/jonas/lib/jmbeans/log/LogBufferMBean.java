/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.jmbeans.log;
/**
 *
 * @author waeselyf
 */
public interface LogBufferMBean {

    /**
     * @return current capacity of the recent LogRecord list
     */
    int getRecentCapacity();
    /**
     * @param recentCapacity capacity of the recent LogRecord list
     */
    void setRecentCapacity(int recentCapacity);

    /**
     * Max number of records to hold (max records of the feeder)
     */
    public void setCapacity(int records);

    public int getCapacity();

    /**
     *
     */
    long getRecordCount();
    long getInfoCount();
    long getWarningCount();
    long getSevereCount();
    long getOtherCount();
    long getLatestDate();
    long getOldestDate();

    /**
     *
     */
    public String getRecent();

    /**
     * Fetch records
     */
    public StringBuffer fetchRecords();
    public StringBuffer fetchRecords(Long from, Long to);
    public StringBuffer fetchRecords(Long from, Long to, int level);


    /**
     * Level
     */
    public int getLevel();
    public void setLevel(int level);

    public static final int INFO = 1;
    public static final int WARNING = 2;
    public static final int SEVERE = 4;
}
