/**
* High Availability Service (HA) for JOnAS
*
* Copyright (C) 2007,2008 Bull S.A.S.
* Copyright (C) 2006 Distributed Systems Lab.
* Universidad Politecnica de Madrid (Spain)
* Contact: http://lsd.ls.fi.upm.es/lsd
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
* USA
*
* --------------------------------------------------------------------------
* $Id$
* --------------------------------------------------------------------------
*/
package org.ow2.jonas.lib.ejb21.ha;

import java.io.IOException;
import java.io.OutputStream;

import org.objectweb.util.monolog.api.BasicLevel;
import org.ow2.cmi.ha.SessionId;
import org.ow2.jonas.lib.ejb21.JLocalHome;
import org.ow2.jonas.lib.ejb21.JStatefulOutputStream;
import org.ow2.jonas.lib.ejb21.TraceEjb;

public class JRepStatefulOutputStream extends JStatefulOutputStream {

    public JRepStatefulOutputStream(final OutputStream out) throws IOException {
        super(out);
    }

    @Override
    protected Object replaceObject(final Object obj) throws IOException {
        Object ret;
        /*if(obj instanceof DataSource) {
        ConnectionManager cm = (ConnectionManager) obj;
        String jndiname = cm.getDSName();
        ret = new JWrapper(JWrapper.DATASOURCE, jndiname);
        } else */
        if (obj instanceof JRepStatefulLocal) {
            // Local stateful -> a wrapper
            TraceEjb.ssfpool.log(BasicLevel.DEBUG, "JRepStatefulLocal");
            JRepStatefulLocal jrsl = (JRepStatefulLocal) obj;
            String jndi = ((JLocalHome) jrsl.getSessionSwitch().getLocal().getEJBLocalHome()).getJndiLocalName();
            SessionId clusterOId = jrsl.getClusterOId();
            ret = new JRepStatefulObjectId(jndi, clusterOId);
        } else {
            ret = super.replaceObject(obj);
        }

        return ret;
    }
}
