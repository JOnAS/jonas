/**
* High Availability Service (HA) for JOnAS
*
* Copyright (C) 2007,2008 Bull S.A.S.
* Copyright (C) 2006 Distributed Systems Lab.
* Universidad Politecnica de Madrid (Spain)
* Contact: http://lsd.ls.fi.upm.es/lsd
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
* USA
*
* --------------------------------------------------------------------------
* $Id$
* --------------------------------------------------------------------------
*/
package org.ow2.jonas.lib.ejb21.ha;

import java.io.Serializable;

import org.ow2.cmi.ha.SessionId;


/**
 * Object ID for a replicated sfsb
 * @author Francisco Perez-Sorrosal (fpsorrosal@no-spam@fi.upm.es)
 * @author Alberto Paz-Jimenez (apaz@no-spam@fi.upm.es)
 */
public class JRepStatefulObjectId implements Serializable {

    /**
     * Id for serializable class.
     */
    private static final long serialVersionUID = 1114912130943131021L;

    /**
     * Local Home jndi
     */
    private final String jndi;

    /**
     * object id in the cluster
     */
    private final SessionId clusterOId;

    /**
     * Default constructor
     * @param jndi the jndi
     * @param oid the clusterOId
     */
    public JRepStatefulObjectId(final String jndi, final SessionId oid) {
        this.jndi = jndi;
        this.clusterOId = oid;
    }

    /**
     * gets the jndi
     * @return the jndi
     */
    public String getJndi() {
        return jndi;
    }

    /**
     * gets the clusterOId
     * @return the clusterOId
     */
    public SessionId getClusterOId() {
        return clusterOId;
    }
}
