/**
* High Availability Service (HA) for JOnAS
*
* Copyright (C) 2007,2008 Bull S.A.S.
* Copyright (C) 2006 Distributed Systems Lab.
* Universidad Politecnica de Madrid (Spain)
* Contact: http://lsd.ls.fi.upm.es/lsd
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
* USA
*
* --------------------------------------------------------------------------
* $Id$
* --------------------------------------------------------------------------
*/
package org.ow2.jonas.lib.ejb21.ha;


import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;
import org.ow2.jonas.lib.ejb21.JEntityContext;
import org.ow2.jonas.lib.ejb21.JEntityFactory;
import org.ow2.jonas.lib.ejb21.JEntityLocal;
import org.ow2.jonas.lib.ejb21.RequestCtx;
import org.ow2.jonas.lib.util.Log;

/**
 * Generic part of the EJBObject implementation for replicated SFSBs
 * @author Francisco Perez-Sorrosal (fpsorrosal@no-spam@fi.upm.es)
 * @author Alberto Paz-Jimenez (apaz@no-spam@fi.upm.es)
 */
public abstract class JRepEntityLocal extends JEntityLocal {

    /**
     * Logger for traces
     */
    private static Logger cmilogger = Log.getLogger("org.ow2.jonas.lib.ejb21.ha");

    /**
     * Constructor
     * @param bf
     */
    public JRepEntityLocal(final JEntityFactory bf) {
        super(bf);
    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.lib.ejb21.JEntityLocal#postInvoke(org.ow2.jonas.lib.ejb21.RequestCtx)
     */
    public void postInvoke(final RequestCtx rctx, final JEntityContext bctx) {
        try {
            postInvokeHook(bctx);
        } catch (Exception e) {
            e.printStackTrace();
            cmilogger.log(BasicLevel.ERROR, "Error calling postInvokeHook in sfsb.", e);
        } finally {
            super.postInvoke(rctx);
        }
    }

    /**
     * Implements the hook to perform the required replication
     * tasks after the bean context has been established
     * @param rctx The RequestCtx that was returned at preInvoke()
     */
    private void postInvokeHook(final JEntityContext bctx) {
        if (cmilogger.isLoggable(BasicLevel.DEBUG)) {
            cmilogger.log(BasicLevel.DEBUG, "In PostInvoke hook for: " + bctx.getPrimaryKey());
        }

        // Create the rootId from the current HACtx/Thread association
//        HACurrentDelegateImpl current = HACurrentDelegateImpl.getCurrent();
        // Obtain the root request
//        RequestId rootId = null;
//        try {
//            rootId = (RequestId) current.getRequests().get(0);
//            JRepUtil.addEntityBean(bctx, rootId);
//        } catch (ArrayIndexOutOfBoundsException e) {
//            cmilogger.log(BasicLevel.DEBUG, "Don't need to be replicate.");
//        }
    }
}
