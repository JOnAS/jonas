/**
* High Availability Service (HA) for JOnAS
*
* Copyright (C) 2007,2008 Bull S.A.S.
* Copyright (C) 2006 Distributed Systems Lab.
* Universidad Politecnica de Madrid (Spain)
* Contact: http://lsd.ls.fi.upm.es/lsd
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
* USA
*
* --------------------------------------------------------------------------
* $Id$
* --------------------------------------------------------------------------
*/
package org.ow2.jonas.lib.ejb21.ha;


import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Stack;

import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;
import org.ow2.cmi.ha.RequestId;
import org.ow2.cmi.ha.interceptor.HACurrent;
import org.ow2.jonas.deployment.ejb.EntityDesc;
import org.ow2.jonas.lib.ejb21.JEntityContext;
import org.ow2.jonas.lib.ejb21.JEntityFactory;
import org.ow2.jonas.lib.ejb21.JEntityHome;
import org.ow2.jonas.lib.util.Log;


/**
 * This class is the Standard Home for clustered entity objects It exists only
 * for beans that have declared a Remote Interface.
 * @author Francisco Perez-Sorrosal (fpsorrosal@no-spam@fi.upm.es)
 * @author Alberto Paz-Jimenez (apaz@no-spam@fi.upm.es)
 */
public abstract class JRepEntityHome extends JEntityHome implements Remote {

    /**
     * Logger for traces
     */
    private static Logger cmilogger = Log.getLogger("org.ow2.jonas.lib.ejb21.ha");

    public JRepEntityHome(final EntityDesc dd, final JEntityFactory bf) throws RemoteException {
        super(dd, bf);
    }

    /**
     * Called if we have to replicate a request
     * @param rctx The RequestCtx that was returned at preInvoke()
     */
    protected void replicateCreate(final JEntityContext bctx, final Object response) {
        if (cmilogger.isLoggable(BasicLevel.DEBUG)) {
            cmilogger.log(BasicLevel.DEBUG, "replicateBean : this=" + this);
        }
        // Create the rootId from the current HACtx/Thread association
        HACurrent current = HACurrent.getHACurrent();
        // Obtain the root request
        try {
            Stack<RequestId> requests = current.getRequests();
            if ((requests != null) && !requests.isEmpty()) {
//            JRepUtil.addEntityBean(bctx, rootId);

                // If this is the root request, then associate response with rootId
                if (requests.size() == 1) {
                    RequestId rootId = requests.firstElement();
                    try {
                        JRepUtil.getRepMgr().addResponse(rootId, response);
                    } catch (Exception e) {
                        cmilogger.log(BasicLevel.ERROR, "unable to obtain replication manager.", e);
                    }
                }
            }

        } catch (ArrayIndexOutOfBoundsException e) {
            cmilogger.log(BasicLevel.DEBUG, "Not in a remote method call chain.");
            // do nothing
        } finally {
            // We don't need the request anymore so remove it
            JRepUtil.removeCurrentRequest();
        }
    }

}
