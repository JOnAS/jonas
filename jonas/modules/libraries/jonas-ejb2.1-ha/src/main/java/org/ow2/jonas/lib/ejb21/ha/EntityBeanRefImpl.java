/**
* High Availability Service (HA) for JOnAS
*
* Copyright (C) 2007,2008 Bull S.A.S.
* Copyright (C) 2006-2007 Distributed Systems Lab.
* Universidad Polit??cnica de Madrid (Spain)
* Contact: http://lsd.ls.fi.upm.es/lsd
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
* USA
*
* --------------------------------------------------------------------------
* $Id$
* --------------------------------------------------------------------------
*/
package org.ow2.jonas.lib.ejb21.ha;

import java.lang.reflect.Method;

import javax.ejb.EntityBean;

import org.ow2.cmi.ha.EntityBeanReference;
import org.ow2.jonas.deployment.ejb.EntityCmp1Desc;
import org.ow2.jonas.deployment.ejb.EntityDesc;
import org.ow2.jonas.lib.ejb21.JEntityContext;

/**
 * This class implements reference to entity beans for CMI HA
 * @author Francisco Perez-Sorrosal (fpsorrosal@no-spam@fi.upm.es)
 * @author Alberto Paz-Jimenez (apaz@no-spam@fi.upm.es)
 */
public class EntityBeanRefImpl implements EntityBeanReference {

    private JEntityContext jec;

    /**
     * Constructor
     * @param bs the entity switch
     */
    public EntityBeanRefImpl(final JEntityContext jec) {
        this.jec = jec;
    }

    public Object getPrimaryKey() {
        return jec.getPrimaryKey();
    }

    public boolean isModified() {
        EntityDesc ed = (EntityDesc) jec.getEntityFactory().getDeploymentDescriptor();
        if (ed instanceof EntityCmp1Desc) {
            EntityCmp1Desc ecd = (EntityCmp1Desc) ed;
            if (ecd.hasIsModifiedMethod()) {
                Method isModifiedMethod = ecd.getIsModifiedMethod();
                try {
                    EntityBean sb = jec.getInstance();//    getStatefulContext().getInstance();
                    return (Boolean) isModifiedMethod.invoke(sb);
                } catch (Exception e) {
                    e.printStackTrace();
                    return true;
                }
            } else {
                return true;
            }
        } else {
            return true;
        }
    }


}
