/**
* High Availability Service (HA) for JOnAS
*
* Copyright (C) 2007,2008 Bull S.A.S.
* Copyright (C) 2006 Distributed Systems Lab.
* Universidad Politecnica de Madrid (Spain)
* Contact: http://lsd.ls.fi.upm.es/lsd
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
* USA
*
* --------------------------------------------------------------------------
* $Id$
* --------------------------------------------------------------------------
*/
package org.ow2.jonas.lib.ejb21.ha;


import java.util.Stack;

import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;
import org.ow2.cmi.ha.RequestId;
import org.ow2.cmi.ha.interceptor.HACurrent;
import org.ow2.jonas.deployment.ejb.SessionDesc;
import org.ow2.jonas.lib.ejb21.BeanFactory;
import org.ow2.jonas.lib.ejb21.JSessionFactory;
import org.ow2.jonas.lib.ejb21.JSessionLocal;
import org.ow2.jonas.lib.ejb21.JSessionLocalHome;
import org.ow2.jonas.lib.ejb21.JStatefulSwitch;
import org.ow2.jonas.lib.util.Log;

/**
 * Generic part of the EJBLocalHome implementation for replicated SFSBs
 * @author Francisco Perez-Sorrosal (fpsorrosal@no-spam@fi.upm.es)
 * @author Alberto Paz-Jimenez (apaz@no-spam@fi.upm.es)
 */
public abstract class JRepStatefulLocalHome extends JSessionLocalHome {

    /**
     * Logger for traces
     */
    protected static Logger cmilogger = Log.getLogger("org.ow2.jonas.lib.ejb21.ha");

    /**
     * constructor
     * @param dd The Session Deployment Descriptor
     * @param bf The Session Factory
     * @throws Exception
     */
    public JRepStatefulLocalHome(final SessionDesc dd, final JSessionFactory bf) {
        super(dd, bf);
        if (cmilogger.isLoggable(BasicLevel.DEBUG)) {
            cmilogger.log(BasicLevel.DEBUG, "JRepStatefulLocalHome : this=" + this);
        }
    }

    /**
     * Called if we have to replicate a request
     * @param rctx The RequestCtx that was returned at preInvoke()
     */
    protected void replicateCreate(final JStatefulSwitch sfsw) {
        if (cmilogger.isLoggable(BasicLevel.DEBUG)) {
            cmilogger.log(BasicLevel.DEBUG, "replicateBean : this=" + this);
        }
        // Create the rootId from the current HACtx/Thread association
        HACurrent current = HACurrent.getHACurrent();
        // Obtain the root request
        Stack<RequestId> requests = current.getRequests();
        if (requests != null && !requests.isEmpty()) {
            try {
                RequestId rootId = requests.get(0);
                JRepStatefulLocal jrsl = (JRepStatefulLocal) sfsw.getLocal();
                JRepUtil.addModifiedBean(sfsw, jrsl.getClusterOId(), rootId);
            } catch (ArrayIndexOutOfBoundsException e) {
                cmilogger.log(BasicLevel.DEBUG, "Not in a remote method call chain.");
                // do nothing
            }
        }
    }

    protected BeanFactory getBeanFactory() {
        return bf;
    }

    /**
     * Creates the EJBLocalObject This is in the generated class because it is
     * mainly "new objectClass()"
     * @return The Local Object
     */
    @Override
    abstract public JSessionLocal createLocalObject();

}
