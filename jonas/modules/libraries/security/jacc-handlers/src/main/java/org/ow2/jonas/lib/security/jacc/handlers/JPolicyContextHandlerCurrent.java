/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004-2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.security.jacc.handlers;

/**
 * This class manages the current data object which is given to the
 * PolicyContext
 * @author Florent Benoit
 */
public class JPolicyContextHandlerCurrent {

    /**
     * Unique instance
     */
    private static JPolicyContextHandlerCurrent current  = new JPolicyContextHandlerCurrent();

    /**
     * Local thread
     */
    private static ThreadLocal threadData;

    /**
     * Method getCurrent
     * @return JPolicyContextHandlerDataCurrent the current
     */
    public static JPolicyContextHandlerCurrent getCurrent() {
        return current;
    }

    /**
     * Default constructor
     */
    private JPolicyContextHandlerCurrent() {
        threadData = new ThreadLocal();
        threadData.set(new JPolicyContextHandlerData());
    }

    /**
     * Method getJPolicyContextHandlerData
     * @return the JPolicyContextHandlerData associated to the current thread
     */
    public synchronized JPolicyContextHandlerData getJPolicyContextHandlerData() {
        if (threadData.get() == null) {
            threadData.set(new JPolicyContextHandlerData());
        }
        return (JPolicyContextHandlerData) threadData.get();
    }

    /**
     * Method setJPolicyContextHandlerData
     * @param data JPolicyContextHandlerData to associate to the current thread
     */
    public void setJPolicyContextHandlerData(JPolicyContextHandlerData data) {
        threadData.set(data);
    }

}
