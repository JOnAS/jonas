/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.security.jacc.handlers;

import javax.ejb.EnterpriseBean;
import javax.security.auth.Subject;
import javax.servlet.http.HttpServletRequest;

import org.ow2.jonas.lib.security.auth.JPrincipal;
import org.ow2.jonas.lib.security.context.SecurityContext;
import org.ow2.jonas.lib.security.context.SecurityCurrent;



/**
 * This class is given to PolicyContext. This allow to associate thread-scoped
 * object with the PolicyContext
 * @see javax.security.jacc.PolicyContext
 * @author Florent Benoit
 */
public class JPolicyContextHandlerData {

    /**
     * HttpServletRequest object
     * @see jacc 4.6.1.3
     */
    private HttpServletRequest httpServletRequest = null;

    /**
     * EJB arguments object
     * @see jacc 4.6.1.5
     */
    private Object[] ejbArguments = null;

    /**
     * Current Enterprise Bean
     * @see jacc 4.6.1.4
     */
    private EnterpriseBean processingBean = null;

    /**
     * Default private constructor
     */
    public JPolicyContextHandlerData() {
        super();
    }

    /**
     * @return Returns the httpServletRequest.
     */
    public HttpServletRequest getHttpServletRequest() {
        return httpServletRequest;
    }

    /**
     * @param httpServletRequest The httpServletRequest to set.
     */
    public void setHttpServletRequest(HttpServletRequest httpServletRequest) {
        this.httpServletRequest = httpServletRequest;
    }

    /**
     * @return the ejb Arguments.
     */
    public Object[] getEjbArguments() {
        return ejbArguments;
    }

    /**
     * Set the EJB arguments which can be used by policy provider
     * @param ejbArguments The ejb Arguments to set.
     */
    public void setEjbArguments(Object[] ejbArguments) {
        this.ejbArguments = ejbArguments;
    }

    /**
     * Gets the current subject (if no user is authenticated, return null)
     * @return the container's subject
     */
    public Subject getContainerSubject() {
        Subject subject = null;

        SecurityCurrent current = SecurityCurrent.getCurrent();
        if (current != null) {
            SecurityContext ctx = current.getSecurityContext();
            if (ctx != null) {
                subject = new Subject();
                String runAsRole = ctx.peekRunAsRole();
                if (runAsRole != null) {
                    subject.getPrincipals().add(new JPrincipal(runAsRole));
                } else {
                    subject.getPrincipals().add(ctx.getCallerPrincipal(false));
                }
                return subject;
            }
        }
        return subject;
    }

    /**
     * @return the processingBean.
     */
    public EnterpriseBean getProcessingBean() {
        return processingBean;
    }

    /**
     * @param processingBean The bean being processed
     */
    public void setProcessingBean(EnterpriseBean processingBean) {
        this.processingBean = processingBean;
    }
}