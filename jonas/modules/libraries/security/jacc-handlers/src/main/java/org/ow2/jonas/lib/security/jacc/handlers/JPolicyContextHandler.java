/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.security.jacc.handlers;

import javax.security.jacc.PolicyContextException;
import javax.security.jacc.PolicyContextHandler;

import org.ow2.jonas.lib.util.Log;


import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * JOnAS class which contains all Context Handler to register inside the
 * PolicyContext
 * @author Florent Benoit
 * @see jacc Section 4.6.1
 */
public class JPolicyContextHandler implements PolicyContextHandler {

    /**
     * Container Subject Policy Context Handler
     * @see jacc section 4.6.1.1
     */
    private static final String CONTAINER_SUBJECT = "javax.security.auth.Subject.container";

    /**
     * SOAPMessage Policy Context Handler
     * @see jacc 4.6.1.2
     */
    private static final String SOAP_MESSAGE = "javax.xml.soap.SOAPMessage";

    /**
     * HttpServletRequest Policy Context Handler
     * @see jacc 4.6.1.3
     */
    private static final String HTTP_SERVLET_REQUEST = "javax.servlet.http.HttpServletRequest";

    /**
     * EnterpriseBean Policy Context Handler
     * @see jacc 4.6.1.4
     */
    private static final String ENTERPRISE_BEAN = "javax.ejb.EnterpriseBean";

    /**
     * EJB Arguments Policy Context Handler
     * @see jacc 4.6.1.5
     */
    private static final String EJB_ARGUMENTS = "javax.ejb.arguments";

    /**
     * List of supported Context Handler
     */
    private static final String[] SUPPORTED_KEYS = new String[] {CONTAINER_SUBJECT, SOAP_MESSAGE, HTTP_SERVLET_REQUEST,
            ENTERPRISE_BEAN, EJB_ARGUMENTS};

    /**
     * Logger
     */
    private static Logger logger = Log.getLogger(Log.JONAS_JACC_SECURITY_PREFIX);

    /**
     * Default constructor
     */
    public JPolicyContextHandler() {
        super();

    }

    /**
     * @param key the supported key
     * @return true if the key is supported, else false.
     * @throws PolicyContextException (never used here)
     * @see javax.security.jacc.PolicyContextHandler#supports(java.lang.String)
     */
    public boolean supports(String key) throws PolicyContextException {
        for (int k = 0; k < SUPPORTED_KEYS.length; k++) {
            if (key.equals(SUPPORTED_KEYS[k])) {
                return true;
            }
        }
        return false;
    }

    /**
     * Supported keys by this JOnAS Context Handler
     * @see javax.security.jacc.PolicyContextHandler#getKeys()
     */
    public String[] getKeys() throws PolicyContextException {
        return SUPPORTED_KEYS;
    }

    /**
     * @param key the key for the object that we want to retrieve
     * @param data the handler data given by PolicyContext class
     * @return the object for the specified key and with the given data object
     * @throws PolicyContextException (never used)
     * @see javax.security.jacc.PolicyContextHandler#getContext(java.lang.String,
     *      java.lang.Object)
     */
    public Object getContext(String key, Object data) throws PolicyContextException {

        if (logger.isLoggable(BasicLevel.DEBUG)) {
            logger.log(BasicLevel.DEBUG, "Asking key" + key);
        }

        // Handle only JOnAS HandleData
        if (data == null || !(data instanceof JPolicyContextHandlerData)) {
            logger.log(BasicLevel.WARN, "No data object or Data object not instance of JPolicyContextHandlerData");
            return null;
        }

        Object contextObject = null;
        JPolicyContextHandlerData jPolicyContextHandlerData = (JPolicyContextHandlerData) data;

        if (key.equals(HTTP_SERVLET_REQUEST)) {
            if (logger.isLoggable(BasicLevel.DEBUG)) {
                logger.log(BasicLevel.DEBUG, "Key == '" + HTTP_SERVLET_REQUEST + "'");
            }
            contextObject = jPolicyContextHandlerData.getHttpServletRequest();
        } else if (key.equals(EJB_ARGUMENTS)) {
            if (logger.isLoggable(BasicLevel.DEBUG)) {
                logger.log(BasicLevel.DEBUG, "Key == '" + EJB_ARGUMENTS + "'");
            }
            contextObject = jPolicyContextHandlerData.getEjbArguments();
        } else if (key.equals(CONTAINER_SUBJECT)) {
            if (logger.isLoggable(BasicLevel.DEBUG)) {
                logger.log(BasicLevel.DEBUG, "Key == '" + CONTAINER_SUBJECT + "'");
            }
            contextObject = jPolicyContextHandlerData.getContainerSubject();

        }

        if (logger.isLoggable(BasicLevel.DEBUG)) {
            logger.log(BasicLevel.DEBUG, "Returning object '" + contextObject + "' for key '" + key + "'");
        }
        return contextObject;

    }

}