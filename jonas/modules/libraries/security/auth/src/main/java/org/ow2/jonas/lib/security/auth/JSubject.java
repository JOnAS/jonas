/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.lib.security.auth;

import java.io.Serializable;
import java.security.Principal;

/**
 * Designed to embeds Object for JResourceLoginModule.
 * This is due to the fact that Subject class isn't serializable with JacORB
 * Could be removed when it will be the case
 * @author Florent.
 */
public class JSubject implements Serializable {

    /**
     * principal name of the user
     */
    private Principal name = null;

    /**
     * Roles of the user
     */
    private Principal group = null;

    /**
     * Build new object with name and group
     * @param name the given name
     * @param group the group containing all roles
     */
    public JSubject(Principal name, Principal group) {
        this.name = name;
        this.group = group;
    }


    /**
     * @return the group (containing groups)
     */
    public Principal getGroup() {
        return group;
    }

    /**
     * @return name of user
     */
    public Principal getName() {
        return name;
    }


}
