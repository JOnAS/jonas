/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.security.auth;


/**
 * Class used to store a signature.
 * @author Florent Benoit
 */
public class JSigned extends JGroup {

    /**
     * Serial version UID.
     */
    private static final long serialVersionUID = -5687215670167207012L;

    /**
     * Signature.
     */
    private byte[] signature = null;

    /**
     * Default constructor.
     * @param signature the given array of bytes.
     */
    public JSigned(final byte[] signature) {
        super("signed");
        this.signature = signature;
    }

    /**
     * Gets the signature.
     * @return array of bytes
     */
    public byte[] getSignature() {
        return signature;
    }
}
