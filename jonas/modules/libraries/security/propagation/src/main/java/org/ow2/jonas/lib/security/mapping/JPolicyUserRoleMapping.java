/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.lib.security.mapping;

import java.util.HashMap;
import java.util.Map;

/**
 * Helper class to manage user to role mapping
 * It uses the contextId as identifier.
 * For clients, as there is no contextId, this is a per jvm property with contextId equals to "global"
 * @author Florent Benoit
 */
public class JPolicyUserRoleMapping {

    /**
     * Global contextID (per JVM configuration)
     */
    public static final String GLOBAL_CTXID = "global";

    /**
     * Internal list of Mapping for JACC context ID Map (ctxId) --> map
     * (principal name --> roles)
     */
    private static Map jaccIdMappings = new HashMap();

    /**
     * Utility class, no constructor
     */
    private JPolicyUserRoleMapping() {
    }

    /**
     * Add a mapping for the given principal name
     * @param contextId Id of the application
     * @param principalName mapping for this principal name
     * @param roles roles for the principal
     */
    public static void addUserToRoleMapping(String contextId, String principalName, String[] roles) {
        Map mapping = (Map) jaccIdMappings.get(contextId);
        if (mapping == null) {
            mapping = new HashMap();
            jaccIdMappings.put(contextId, mapping);
        }
        mapping.put(principalName, roles);
    }

    /**
     * Add a mapping for the given principal name (on all JVM)
     * @param principalName mapping for this principal name
     * @param roles roles for the principal
     */
    public static void addGlobalUserToRoleMapping(String principalName, String[] roles) {
        addUserToRoleMapping(GLOBAL_CTXID, principalName, roles);
    }

    /**
     * Gets the mapping for a principal name
     * @param contextId Id of the application
     * @param principalName name of the principal on which we want to retrieve
     *        roles
     * @return array of roles
     */
    public static String[] getMappingForPrincipal(String contextId, String principalName) {
        Map mapping = (Map) jaccIdMappings.get(contextId);
        if (mapping == null) {
            mapping = new HashMap();
            jaccIdMappings.put(contextId, mapping);
        }
        return (String[]) mapping.get(principalName);
    }

    /**
     * Gets the mapping for a principal name
     * @param principalName name of the principal on which we want to retrieve
     *        roles
     * @return array of roles
     */
    public static String[] getGlobalMappingForPrincipal(String principalName) {
        return getMappingForPrincipal(GLOBAL_CTXID, principalName);
    }

    /**
     * Remove a mapping for the given context Id
     * @param contextId Id of the application
     */
    public static void removeUserToRoleMapping(String contextId) {
        jaccIdMappings.remove(contextId);
    }

}