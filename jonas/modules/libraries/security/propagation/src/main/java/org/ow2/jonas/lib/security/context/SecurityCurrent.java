/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Jeff Mesnil.
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 *
 */

package org.ow2.jonas.lib.security.context;

/**
 * For handling the association SecurityContext/ Thread
 * @author Jeff Mesnil
 */
public class SecurityCurrent {

    /**
     * Local thread
     */
    private static InheritableThreadLocal threadCtx;

    /**
     * Security Context for all the JVM
     */
    private static SecurityContext sctx = null;

    /**
     * Init the thread
     */
    static {
        threadCtx = new InheritableThreadLocal();
        threadCtx.set(new SecurityContext());
    }

    /**
     * Unique instance
     */
    private static SecurityCurrent current = new SecurityCurrent();

    /**
     * Method getCurrent
     * @return SecurityCurrent return the current
     */
    public static SecurityCurrent getCurrent() {
        return current;
    }

    /**
     * Method setSecurityContext
     * @param ctx Security context to associate to the current thread
     */
    public void setSecurityContext(SecurityContext ctx) {
        threadCtx.set(ctx);
    }

    /**
     * Method setSecurityContext used for client container
     * @param ctx Security context to associate to the JVM
     */
    public void setGlobalSecurityContext(SecurityContext ctx) {
    	SecurityCurrent.sctx = ctx;
    }

    /**
     * Method getSecurityContext
     * @return SecurityContext return the Security context associated to the
     *         current thread
     */
    public SecurityContext getSecurityContext() {
        if (sctx != null) {
            return sctx;
        } else {
            return (SecurityContext) threadCtx.get();
        }
    }

}