/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.security.interceptors.iiop;

import org.omg.IOP.ServiceContext;
import org.omg.PortableInterceptor.ClientRequestInfo;
import org.omg.PortableInterceptor.ClientRequestInterceptor;
import org.omg.PortableInterceptor.ForwardRequest;
import org.omg.PortableInterceptor.ORBInitInfo;
import org.ow2.jonas.lib.security.context.SecurityContext;
import org.ow2.jonas.lib.security.context.SecurityCurrent;

/**
 * Class <code>CorbaClientSecurityInterceptor</code> is a Client Security
 * Interceptor Java Client
 * @author Guillaume Riviere (Guillaume.Riviere@inrialpes.fr)
 * @version 1.0, 13/09/2002
 */
public class CorbaClientSecurityInterceptor extends SecurityInterceptor implements ClientRequestInterceptor {

    /**
     * interceptor name
     */
    private String interceptorName = "CorbaClientSecurityInteceptor";

    /**
     * constructor
     */
    public CorbaClientSecurityInterceptor(ORBInitInfo info) {
    }

    /**
     * get the name of this interceptor
     * @return name
     */
    public String name() {
        return interceptorName;
    }

    public void destroy() {
    }

    /**
     * send client transaction context with the request, if existed.
     * @param ClientRequestInfo iiop client info
     * @throws ForwardRequest if an exception occured with the ObjectOutput
     */
    public void send_request(ClientRequestInfo jri) throws ForwardRequest {
        // Gets Current object (always existing in JOnAS Server)
        SecurityCurrent current = SecurityCurrent.getCurrent();
        if (current != null) {
            SecurityContext jsc = current.getSecurityContext();
            if (jsc != null) {
                ServiceContext ssc = new ServiceContext(SEC_CTX_ID, marshallSecurityContext(jsc));
                jri.add_request_service_context(ssc, true);
            }
        }

    }

    /**
     * Receive reply interception
     * @param jri client info
     */
    public void receive_reply(ClientRequestInfo jri) {
    }

    // empty method
    public void send_poll(ClientRequestInfo jri) {
    }

    public void receive_exception(ClientRequestInfo jri) throws ForwardRequest {
    }

    public void receive_other(ClientRequestInfo jri) throws ForwardRequest {
    }
}

