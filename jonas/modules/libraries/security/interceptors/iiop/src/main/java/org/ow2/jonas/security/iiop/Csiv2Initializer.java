/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.security.iiop;

import org.omg.IOP.Codec;
import org.omg.IOP.ENCODING_CDR_ENCAPS;
import org.omg.IOP.Encoding;
import org.omg.IOP.CodecFactoryPackage.UnknownEncoding;
import org.omg.PortableInterceptor.ORBInitInfo;
import org.omg.PortableInterceptor.ORBInitializer;
import org.ow2.jonas.lib.util.Log;

import org.ow2.carol.util.csiv2.SasPolicy;
import org.ow2.carol.util.csiv2.SasPolicyFactory;


import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * Initializer of SAS interceptor
 * @author Florent Benoit
 */
public class Csiv2Initializer extends org.omg.CORBA.LocalObject implements ORBInitializer {

    /**
     * Logger
     */
    private static Logger logger = Log.getLogger(Log.JONAS_CSIV2_SECURITY_PREFIX);

    /**
     * Logger details (On catching exception)
     */
    private static Logger loggerDetails = Log.getLogger(Log.JONAS_CSIV2_DETAILS_SECURITY_PREFIX);

    /**
     * Called during ORB initialization. If a service must resolve initial
     * references as part of its initialization, it can assume that all initial
     * references will be available at this point.
     * @param info provides initialization attributes and operations by which
     *        Interceptors can be registered.
     */
    public void post_init(ORBInitInfo info) {
        // Interceptors (server, client, IOR)
        if (logger.isLoggable(BasicLevel.DEBUG)) {
            logger.log(BasicLevel.DEBUG, "Initializing SAS Interceptors");
        }

        // Codec for GIOP 1.2
        Encoding encoding = new Encoding(ENCODING_CDR_ENCAPS.value, (byte) 1, (byte) 2);
        Codec codec = null;
        try {
            codec = info.codec_factory().create_codec(encoding);
        } catch (UnknownEncoding ue) {
            String err = "Cannot use a given encoding : '" + ue.getMessage() + "'.";
            logger.log(BasicLevel.ERROR, err);
            throw new RuntimeException(err);
        }

        try {
            info.add_server_request_interceptor(new Csiv2ServerInterceptor(codec, logger, loggerDetails));
        } catch (Exception e) {
            logger.log(BasicLevel.ERROR, "Unable to register CSIv2 server interceptor : '" + e.getMessage() + "'.");
        }
        try {
            info.add_client_request_interceptor(new Csiv2ClientInterceptor(codec, logger, loggerDetails));
        } catch (Exception e) {
            logger.log(BasicLevel.ERROR, "Unable to register CSIv2 client interceptor : '" + e.getMessage() + "'.");
        }

        try {
            info.add_ior_interceptor(new Csiv2IorInterceptor(codec, logger, loggerDetails));
        } catch (Exception e) {
            logger.log(BasicLevel.ERROR, "Unable to register CSIv2 IOR interceptor : '" + e.getMessage() + "'.");
        }

        // Factory
        if (logger.isLoggable(BasicLevel.DEBUG)) {
            logger.log(BasicLevel.DEBUG, "Initializing SAS policy factory");
        }
        info.register_policy_factory(SasPolicy.POLICY_TYPE, new SasPolicyFactory());

    }

    /**
     * Called during ORB initialization. If it is expected that initial services
     * registered by an interceptor will be used by other interceptors, then
     * those initial services shall be registered at this point via calls to
     * <code>ORBInitInfo.register_initial_reference</code>.
     * @param info provides initialization attributes and operations by which
     *        Interceptors can be registered.
     */
    public void pre_init(ORBInitInfo info) {
        // TODO Auto-generated method stub

    }
}
