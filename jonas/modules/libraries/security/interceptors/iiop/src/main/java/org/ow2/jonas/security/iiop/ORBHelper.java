/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.security.iiop;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

import org.omg.CORBA.ORB;


/**
 * Gets the ORB (java:comp/ORB)
 * @author Florent Benoit
 */
public class ORBHelper {

    /**
     * ORB to use
     */
    private static ORB orb = null;


    /**
     * Utility class. No constructor.
     */
    private ORBHelper() {

    }

    /**
     * @return an ORB
     * @throws Csiv2InterceptorException if the ORB cannot be retrieved
     */
    public static ORB getOrb() throws Csiv2InterceptorException {
        if (orb != null) {
            return orb;
        }

        // Get orb for building Any
        Context ictx = null;
        try {
            ictx = new InitialContext();
        } catch (NamingException ne) {
            throw new Csiv2InterceptorException("Cannot instatiate InitialContext", ne);
        }

        try {
            orb = (ORB) PortableRemoteObject.narrow(ictx.lookup("java:comp/ORB"), ORB.class);
        } catch (NamingException ne) {
            throw new Csiv2InterceptorException("Cannot lookup java:comp/ORB", ne);
        }
        return orb;
    }


}
