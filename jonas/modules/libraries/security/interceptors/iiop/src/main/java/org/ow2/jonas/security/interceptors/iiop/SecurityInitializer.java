/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 */
package org.ow2.jonas.security.interceptors.iiop;

import org.omg.CORBA.LocalObject;
import org.omg.PortableInterceptor.ORBInitInfo;
import org.omg.PortableInterceptor.ORBInitializer;

/**
 * Class <code>SecurityInitializer</code> is a Security Interceptor
 * initialisation for jonas
 * @author Guillaume Riviere (Guillaume.Riviere@inrialpes.fr)
 * @version 1.0, 13/09/2002
 */
public class SecurityInitializer extends LocalObject implements ORBInitializer {

    public void pre_init(ORBInitInfo info) {
        // do nothing
    }

    public void post_init(ORBInitInfo info) {
        try {
            info.add_client_request_interceptor(new CorbaClientSecurityInterceptor(info));
            info.add_server_request_interceptor(new CorbaServerSecurityInterceptor(info));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}