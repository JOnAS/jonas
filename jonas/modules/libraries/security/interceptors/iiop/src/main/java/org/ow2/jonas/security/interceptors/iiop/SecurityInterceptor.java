/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 */
package org.ow2.jonas.security.interceptors.iiop;

import org.omg.CORBA.LocalObject;
import org.ow2.jonas.lib.security.context.Marshalling;
import org.ow2.jonas.lib.security.context.SecurityContext;


/**
 * Marshall/Unmarshall security context
 * @author Guillaume Riviere (initial developer)
 * @author Florent Benoit
 */
public abstract class SecurityInterceptor extends LocalObject {

    /**
     * security context id
     */
    public static final int SEC_CTX_ID = 101;

    /**
     * Custom UTF8 marshalling SecurityContext
     * The resulting bute array is composed of the following elements:
     * principal-name, roles-number, role1, ...., runas-number, runas1, ....
     * @return byte [] the marshalled context
     * @param ctx SecurityContext
     */
    public byte[] marshallSecurityContext(SecurityContext ctx) {
        return Marshalling.marshallSecurityContext(ctx);
    }

    /**
     * Custom UTF8 marshalling SecurityContext
     * @param byteCtx the marshalled context
     * @return SecurityContext
     */
    public SecurityContext unmarshallSecurityContext(byte[] byteCtx) {
        return Marshalling.unmarshallSecurityContext(byteCtx);
    }

    /**
     * Return string representation of a security context
     * @param scx given security context
     * @return string representation of a security context
     */
    public String contextString(SecurityContext scx) {
        return (scx.toString());
    }

}

