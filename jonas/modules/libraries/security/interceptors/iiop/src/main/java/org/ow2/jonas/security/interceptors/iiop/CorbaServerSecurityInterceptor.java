/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.security.interceptors.iiop;

import org.omg.IOP.ServiceContext;
import org.omg.PortableInterceptor.ForwardRequest;
import org.omg.PortableInterceptor.ORBInitInfo;
import org.omg.PortableInterceptor.ServerRequestInfo;
import org.omg.PortableInterceptor.ServerRequestInterceptor;
import org.ow2.jonas.lib.security.context.SecurityContext;
import org.ow2.jonas.lib.security.context.SecurityCurrent;


/**
 * Class <code>CorbaServerSecurityInterceptor</code> is a Server Interceptor
 * for Security
 * @author Guillaume Riviere (Guillaume.Riviere@inrialpes.fr)
 * @version 1.0, 13/09/2002
 */
public class CorbaServerSecurityInterceptor extends SecurityInterceptor implements ServerRequestInterceptor {

    /**
     * interceptor name
     */
    private String interceptorName = "CorbaServerSecurityInteceptor";

    /**
     * Security context
     */
    private SecurityContext sCtx = null;

    /**
     * constructor
     */
    public CorbaServerSecurityInterceptor(ORBInitInfo info) {
    }

    /**
     * get the name of this interceptor
     * @return name
     */
    public String name() {
        return interceptorName;
    }

    /**
     * Receive request context
     * @param JServerRequestInfo the server request information
     * @throws ForwardRequest if an exception occur with the ObjectOutput
     */
    public void receive_request_service_contexts(ServerRequestInfo jri) throws ForwardRequest {
        // Gets SecurityCurrent object (always existing in JOnAS Server)
        SecurityCurrent current = SecurityCurrent.getCurrent();
        if (current != null) {
            try {
                ServiceContext serviceCtx = (ServiceContext) jri.get_request_service_context(SEC_CTX_ID);
                if (serviceCtx != null) {
                    sCtx = unmarshallSecurityContext(serviceCtx.context_data);
                }
            } catch (org.omg.CORBA.BAD_PARAM e) {
                // No Security context, do nothing
            }
        }
    }

    /**
     * Receive request
     * @param JServerRequestInfo the server request information
     * @throws ForwardRequest if an exception occur with the ObjectOutput
     */
    public void receive_request(ServerRequestInfo jri) throws ForwardRequest {
        SecurityCurrent current = SecurityCurrent.getCurrent();
        if ((current != null) && (sCtx != null)) {
            // put into the the Current object
            current.setSecurityContext(sCtx);
            sCtx = null;
        }
    }

    /**
     * send reply with context
     * @param JServerRequestInfo the server request information
     */
    public void send_reply(ServerRequestInfo jri) {
    }

    public void send_exception(ServerRequestInfo jri) throws ForwardRequest {
    }

    public void send_other(ServerRequestInfo jri) throws ForwardRequest {
    }

    public void destroy() {
    }
}