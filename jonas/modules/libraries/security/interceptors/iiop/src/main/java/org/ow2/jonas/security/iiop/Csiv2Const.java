/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.security.iiop;


/**
 * Defines some constants for Csiv2 exchange
 * @author Florent Benoit
 */
public class Csiv2Const {

    /**
     * Stateful or not ? false as support of stateful mode in not required in
     * Csiv2 level 0
     */
    public static final boolean STATEFUL_MODE = false;


    /**
     * Replacement of security attribute service
     * Should be true if stateless, else false
     */
    public static final boolean REPLACE_SECURITY_ATTRIBUTE_SERVICE = !STATEFUL_MODE;



    /**
     * Context Id stateless (Csiv2 level 0)
     */
    public static final long STATELESS_CONTEXT_ID = 0;


    /**
     * Empty array of bytes
     */
    public static final byte[] EMPTY_BYTES = new byte[0];


}
