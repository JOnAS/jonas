/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 *
 */
package org.ow2.jonas.security.interceptors.jrmp;

// jonas import
import org.ow2.carol.rmi.interceptor.spi.JServiceContext;
import org.ow2.jonas.lib.security.context.SecurityContext;

/**
 * Class <code>SecurityServiceContext</code> is a JRMP Class for Security
 * Context Propagation
 * @author Guillaume Riviere
 * @version 1.0, 10/03/2003
 */
public class SecurityServiceContext implements JServiceContext {

    /**
     * context id
     */
    private int context_id;

    /**
     * the JServiceContext id
     */
    public int getContextId() {
        return context_id;
    }

    /**
     * Security Context
     */
    SecurityContext sctx = null;

    /**
     * Empty constructor for Externalizable
     */
    public SecurityServiceContext() {
        this.context_id = ServerSecurityInterceptor.SEC_CTX_ID;
    }

    /**
     * constructor
     * @param int the context_id
     * @param SecurityContext the RMI (Serializable) Security Context
     */
    public SecurityServiceContext(final int context_id, final SecurityContext sctx) {
        this.context_id = ServerSecurityInterceptor.SEC_CTX_ID;
        this.sctx = sctx;
    }

    /**
     * get the security context
     * @return SecurityContext the Security context
     */
    public SecurityContext getSecurityContext() {
        return sctx;
    }

}
