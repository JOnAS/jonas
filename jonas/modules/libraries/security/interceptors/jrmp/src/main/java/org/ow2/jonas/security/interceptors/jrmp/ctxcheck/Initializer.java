/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 *
 */
package org.ow2.jonas.security.interceptors.jrmp.ctxcheck;

import org.ow2.carol.rmi.interceptor.api.JInitInfo;
import org.ow2.carol.rmi.interceptor.spi.JInitializer;

/**
 * Adds on the server side an interceptor which will validate the security
 * context.
 * @author Florent Benoit
 */
public class Initializer implements JInitializer {

    /**
     * In JRMP the 2 method( per and post init have the same consequences ...
     * @param JInitInfo the JInit Information
     */
    public void preInit(final JInitInfo info) {

        try {
            info.addServerRequestInterceptor(new ServerInterceptor());
        } catch (Exception e) {
            throw new IllegalStateException("Cannot built interceptor: " + e.getMessage());
        }
    }

    /**
     * In JRMP the 2 method( per and post init have the same consequences ...
     * @param JInitInfo the JInit Information
     */
    public void postInit(final JInitInfo info) {
        // do nothing
    }

}
