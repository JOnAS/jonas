/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 *
 */
package org.ow2.jonas.security.interceptors.jrmp;

// java import
import java.io.IOException;

import org.ow2.carol.rmi.interceptor.api.JClientRequestInfo;
import org.ow2.carol.rmi.interceptor.spi.JClientRequestInterceptor;
import org.ow2.jonas.lib.security.context.SecurityContext;
import org.ow2.jonas.lib.security.context.SecurityCurrent;

/**
 * Class <code>ClientSecurityInterceptor</code> is a JRMP Security client
 * interceptor for Security Context propagation
 * @author Jeff Mesnil
 * @author Guillaume Riviere
 * @version 1.0, 10/03/2003
 */
public class ClientSecurityInterceptor implements JClientRequestInterceptor {

    /**
     * security context id
     */
    public static int SEC_CTX_ID = 1;

    /**
     * interceptor name
     */
    private String interceptorName = "ClientSecurityInterceptor";

    /**
     * constructor
     */
    public ClientSecurityInterceptor() {
    }

    /**
     * send client context with the request. The sendingRequest method of the
     * JPortableInterceptors is called prior to marshalling arguments and
     * contexts
     * @param JClientRequestInfo jri the jrmp client info
     * @exception IOException if an exception occur with the ObjectOutput
     */
    public void sendRequest(final JClientRequestInfo jri) throws IOException {

        // Gets Current object (always existing in JOnAS Server)
        SecurityCurrent current = SecurityCurrent.getCurrent();
        if (current != null) {
            // Get the Security Context
            SecurityContext ctx = current.getSecurityContext();
            SecurityServiceContext ssc = new SecurityServiceContext(SEC_CTX_ID, ctx);
            jri.addRequestServiceContext(ssc);
        }
    }

    /**
     * we do not use the received security context to set it (EJB, v1.1, chapter
     * 15.5) Receive reply interception
     * @param JClientRequestInfo jri the jrmp client info
     * @exception IOException if an exception occur with the ObjectOutput
     */
    public void receiveReply(final JClientRequestInfo jri) throws IOException {
    }

    /**
     * get the name of this interceptor
     * @return name
     */
    public String name() {
        return interceptorName;
    }

    // empty method
    public void sendPoll(final JClientRequestInfo jri) throws IOException {
    }

    public void receiveException(final JClientRequestInfo jri) throws IOException {
    }

    public void receiveOther(final JClientRequestInfo jri) throws IOException {
    }
}

