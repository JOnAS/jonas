/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 *
 */
package org.ow2.jonas.security.interceptors.jrmp;

import java.io.IOException;

import org.ow2.carol.rmi.interceptor.api.JServerRequestInfo;
import org.ow2.carol.rmi.interceptor.spi.JServerRequestInterceptor;
import org.ow2.jonas.lib.security.context.SecurityContext;
import org.ow2.jonas.lib.security.context.SecurityCurrent;

/**
 * Class <code>ServerSecurityInterceptor</code> is a JRMP security server interceptor for
 * Security Context propagation
 *
 * @autors Jeff Mesnil
 * @contributor  Guillaume Riviere
 * @version 1.0, 10/03/2003
 */
public class ServerSecurityInterceptor implements JServerRequestInterceptor {

    /**
     * security context id
     */
    public static int SEC_CTX_ID = 1;

    /**
     * interceptor name
     */
    private String interceptorName = "ServerSecurityInterceptor";

    /**
     * Empty constructor
     */
    public ServerSecurityInterceptor() {
    }

    /**
     * Receive request
     * @param JServerRequestInfo the jrmp server request information
     * @exception IOException if an exception occurs with the ObjectOutput
     */
    public void receiveRequest(final JServerRequestInfo jri) throws IOException {
        // Gets SecurityCurrent object (always existing in JOnAS Server)
        SecurityCurrent current = SecurityCurrent.getCurrent();
        if (current != null) {
            SecurityServiceContext sctx = (SecurityServiceContext) jri.getRequestServiceContext(SEC_CTX_ID);
            if (sctx != null) {
                // put into the the Current object (true for client side context
                current.setSecurityContext(sctx.getSecurityContext());
            }
        }
    }

    /**
     * send reply with context
     * @param JServerRequestInfo the jrmp server request information
     * @exception IOException if an exception occur with the ObjectOutput
     */
    public void sendReply(final JServerRequestInfo jri) throws IOException {
        SecurityCurrent current = SecurityCurrent.getCurrent();
        if (current != null) {
            current.setSecurityContext(new SecurityContext());
        }
    }

    /**
     * get the name of this interceptor
     * @return name
     */
    public String name() {
        return interceptorName;
    }

    public void sendException(final JServerRequestInfo jri) throws IOException {
    }

    public void sendOther(final JServerRequestInfo jri) throws IOException {
    }
}
