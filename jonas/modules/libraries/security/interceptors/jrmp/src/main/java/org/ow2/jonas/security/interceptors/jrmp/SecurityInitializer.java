/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 *
 */
package org.ow2.jonas.security.interceptors.jrmp;

// carol import
import org.ow2.carol.rmi.interceptor.api.JInitInfo;
import org.ow2.carol.rmi.interceptor.spi.JInitializer;

/**
 * Class <code>SecurityInitializer</code> is a JRMP Initiliazer for security
 * context propagation
 * @author Guillaume Riviere (Guillaume.Riviere@inrialpes.fr)
 * @version 1.0, 13/09/2002
 */
public class SecurityInitializer implements JInitializer {

    /**
     * In JRMP the 2 method( per and post init have the same consequences ...
     * @param JInitInfo the JInit Information
     */
    public void preInit(final JInitInfo info) {
        try {
            info.addClientRequestInterceptor(new ClientSecurityInterceptor());
            info.addServerRequestInterceptor(new ServerSecurityInterceptor());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * In JRMP the 2 method( per and post init have the same consequences ...
     * @param JInitInfo the JInit Information
     */
    public void postInit(final JInitInfo info) {
        // do nothing
    }

}
