/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 *
 */

package org.ow2.jonas.security.interceptors.jrmp.ctxcheck;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;

import org.ow2.jonas.lib.bootstrap.JProp;


/**
 * Class used to store the public key used to check the signature of a security context.
 * @author Florent Benoit
 */
public class CtxCheckConfig {

    /**
     * Instance of the public key.
     */
    private static PublicKey publicKey = null;

    /**
     * Default constructor.
     */
    public CtxCheckConfig() {
        if (publicKey == null) {
            initConfig();
        }
    }

    /**
     * @return the public key to use to validate the security context signature.
     */
    public PublicKey getPublicKey() {
        return publicKey;
    }


    /**
     * Init a configuration which include the public key to use.
     */
    protected synchronized void initConfig() {

        // Get JOnAS properties
        JProp props = JProp.getInstance();

        // Keystore file
        String keystoreFile = props.getValue("jonas.service.security.context.check.keystoreFile");
        if (keystoreFile == null) {
            throw new IllegalStateException("The 'jonas.service.security.context.check.keystoreFile' attribute was not found in the JOnAS configuration file but this attribute is mandatory");
        }

        // Keystore pass
        String keystorePass = (String) props.getValue("jonas.service.security.context.check.keystorePass");
        if (keystorePass == null) {
            throw new IllegalStateException("The 'jonas.service.security.context.check.keystorePass' attribute was not found in the JOnAS configuration file but this attribute is mandatory");
        }

        // Alias
        String alias = (String) props.getValue("jonas.service.security.context.check.alias");
        if (alias == null) {
            throw new IllegalStateException("The 'jonas.service.security.context.check.alias' attribute was not found in the JOnAS configuration file but this attribute is mandatory");
        }


        // Check that the file exists
        File f = new File(keystoreFile);
        if (!f.exists()) {
            throw new IllegalStateException("The keystore file named '" + f + "' was not found.");
        }

        // Gets the keystore instance
        KeyStore keyStore = null;
        try {
            keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
        } catch (KeyStoreException e) {
            throw new IllegalStateException("Error while getting a keystore ':" + e.getMessage());
        }

        // Load the keystore file
        try {
            keyStore.load(new BufferedInputStream(new FileInputStream(f)), keystorePass.toCharArray());
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("Error while loading the keystore file '" + f + "'." + e.getMessage());
        } catch (CertificateException e) {
            throw new IllegalStateException("Error while loading the keystore file '" + f + "'." + e.getMessage());
        } catch (FileNotFoundException e) {
            throw new IllegalStateException("Error while loading the keystore file '" + f + "'." + e.getMessage());
        } catch (IOException e) {
            throw new IllegalStateException("Error while loading the keystore file '" + f + "'." + e.getMessage());
        }

        // Get certificate
        Certificate cert;
        try {
            cert = keyStore.getCertificate(alias);
        } catch (KeyStoreException e) {
            throw new IllegalStateException("Error while getting the alias '" + alias + "' in the keystore file '" + keystoreFile + "':" + e.getMessage());
        }

        // Get the public key
        publicKey = cert.getPublicKey();

    }

}
