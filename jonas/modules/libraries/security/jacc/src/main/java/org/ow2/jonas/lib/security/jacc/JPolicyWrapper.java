/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.security.jacc;

import java.security.CodeSource;
import java.security.Permission;
import java.security.PermissionCollection;
import java.security.Policy;
import java.security.ProtectionDomain;


/**
 * Wrapper to the JPolicy class. This is used to instantiate
 * the real Policy object by using Thread classloader.
 * @author Florent Benoit
 */
public class JPolicyWrapper extends Policy {

    /**
     * Internal policy object
     */
    private Policy wrappedPolicy = null;

    /**
     * Name of the implementation class
     */
    private static final String CLASS_NAME = "org.ow2.jonas.lib.security.jacc.JPolicy";


    /**
     * Default constructor
     */
    public JPolicyWrapper() {
        super();
        try {
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            Class clazz = classLoader.loadClass(CLASS_NAME);
            wrappedPolicy = (Policy) clazz.newInstance();
        } catch (Exception e) {
            // no logger available (class packaged in bootstrap jar)
            throw new RuntimeException("JPolicyWrapper : Error with JACC/Policy object", e);
        }
    }


    /**
     * Refreshes/reloads the policy configuration.
     */
    public void refresh() {
        wrappedPolicy.refresh();
    }

    /**
     * Evaluates the global policy and returns a PermissionCollection object
     * specifying the set of permissions allowed given the characteristics
     * of the protection domain.
     * @param codesource the given codesource on which retrieve permissions
     * @return permissions
     */
    public PermissionCollection getPermissions(CodeSource codesource) {
        return wrappedPolicy.getPermissions(codesource);
    }

    /**
     * Evaluates the global policy and returns a PermissionCollection object
     * specifying the set of permissions allowed given the characteristics
     * of the protection domain.
     * @param domain the given domain on which retrieve permissions
     * @return permissions
     */
    public PermissionCollection getPermissions(ProtectionDomain domain) {
        return wrappedPolicy.getPermissions(domain);
    }

    /**
     * Evaluates the global policy for the permissions granted
     * to the ProtectionDomain and tests whether the permission is granted.
     * @param domain the ProtectionDomain to test.
     * @param permission the Permission object to be tested for implication.
     * @return true if "permission" is a proper subset of a permission
     *         granted to this ProtectionDomain.
     */
    public boolean implies(ProtectionDomain domain, Permission permission) {
        return wrappedPolicy.implies(domain, permission);
    }

}
