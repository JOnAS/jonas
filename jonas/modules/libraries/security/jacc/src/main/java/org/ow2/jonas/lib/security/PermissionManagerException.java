/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.security;

/**
 * The PermissionManagerException occurs if JACC permission could not be added
 *
 * @author Florent Benoit
 */
public class PermissionManagerException extends Exception {

    /**
     * Constructs a new PermissionManagerException with the specified message.
     *
     * @param message the detail message.
     */
    public PermissionManagerException(String message) {
        super(message);
    }

    /**
     * Constructs a new PermissionManagerException with the specified message.
     *
     * @param message the detail message.
     * @param throwable the exception
     */
    public PermissionManagerException(String message, Throwable throwable) {
        super(message, throwable);
    }
}

