/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.lib.security.jacc;

import java.util.HashMap;
import java.util.Map;

import javax.security.jacc.PolicyContextException;

/**
 * This class keep the JPolicyConfiguration. This is used when
 * JPolicyConfigurationFactory is used as a delegate factory.
 * @author Florent Benoit
 */
public class JPolicyConfigurationKeeper {

    /**
     * Internal list of JPolicyConfiguration objects
     */
    private static Map policyConfigurations = new HashMap();

    /**
     * Utility class, no constructor
     */
    private JPolicyConfigurationKeeper() {
    }

    /**
     * Add a JOnAS policy configuration
     * @param config policy configuration object to add
     */
    public static void addConfiguration(JPolicyConfiguration config) {
        try {
            policyConfigurations.put(config.getContextID(), config);
        } catch (PolicyContextException pce) {
            throw new RuntimeException("Cannot add the policy configuration object '" + config + "'");
        }
    }

    /**
     * Remove a JOnAS policy configuration
     * @param config policy configuration object to remove
     */
    public static void removeConfiguration(JPolicyConfiguration config) {
        try {
            if (policyConfigurations.containsKey(config.getContextID())) {
                policyConfigurations.remove(config.getContextID());
            }
        } catch (PolicyContextException pce) {
            throw new RuntimeException("Cannot remove the policy configuration object '" + config + "'");
        }
    }

    /**
     * Gets the JOnAS policy configuration by its contextId
     * @param contextId given ID to retrieve policy configuration object
     * @return the JOnAS policy configuration specified by its contextId
     */
    public static JPolicyConfiguration getConfiguration(String contextId) {
        return (JPolicyConfiguration) policyConfigurations.get(contextId);
    }
}