/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.security.jacc;

import javax.security.jacc.PolicyConfiguration;
import javax.security.jacc.PolicyConfigurationFactory;
import javax.security.jacc.PolicyContextException;


/**
 * Defines a wrapper for the PolicyConfigurationFactory
 * Uses a wrapper because JACC provider factory is loaded by abstract Factory
 * with the system classloader. The problem is that System classloader loads only
 * few JOnAS classes. All classes are loaded by JOnAS classloader
 * This wrapper could be removed if the class is no more loaded by System classloader
 * @author Florent Benoit
 */
public class PolicyConfigurationFactoryWrapper extends PolicyConfigurationFactory {

    /**
     * List of PolicyConfiguration objects
     * Manage all configurations available
     */
    private PolicyConfigurationFactory policyConfigurationFactory = null;

    /**
     * Name of the implementation class
     */
    private static final String CLASS_NAME = "org.ow2.jonas.lib.security.jacc.JPolicyConfigurationFactory";

    /**
     * Update policyConfigurationFactory object for delegating requests
     */
    public PolicyConfigurationFactoryWrapper() {
        try {
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            Class clazz = classLoader.loadClass(CLASS_NAME);
            Object o = clazz.newInstance();
            policyConfigurationFactory = (PolicyConfigurationFactory) o;
        } catch (Exception e) {
            // no logger available (bootstrap jar)
            System.err.println("PolicyConfigurationFactoryWrapper : Error with JACC :" + e.getMessage());
        }
    }


    /**
     * This method is used to obtain an instance of the provider specific
     * class that implements the PolicyConfiguration interface that corresponds
     * to the identified policy context within the provider.
     * @param contextID A String identifying the policy context whose
     *        PolicyConfiguration interface is to be returned. The value passed
     *        to this parameter must not be null.
     * @param remove A boolean value that establishes whether or not the policy
     *        statements of an existing policy context are to be removed before
     *        its PolicyConfiguration object is returned. If the value passed to
     *        this parameter is true, the policy statements of an existing
     *        policy context will be removed. If the value is false,
     *        they will not be removed.
     * @return an Object that implements the PolicyConfiguration Interface
     *         matched to the Policy provider and corresponding to the
     *         identified policy context.
     * @throws SecurityException when called by an AccessControlContext that
     *         has not been granted the "setPolicy" SecurityPermission.
     * @throws PolicyContextException if the implementation throws a checked
     *         exception that has not been accounted for by the
     *         getPolicyConfiguration method signature. The exception thrown
     *         by the implementation class will be encapsulated
     *         (during construction) in the thrown PolicyContextException.
     */
    public PolicyConfiguration getPolicyConfiguration(String contextID, boolean remove) throws PolicyContextException, SecurityException {
        return policyConfigurationFactory.getPolicyConfiguration(contextID, remove);
    }

    /**
     * This method determines if the identified policy context exists
     * with state "inService" in the Policy provider associated with
     * the factory.
     * @param contextID A string identifying a policy context
     * @return true if the identified policy context exists within
     *         the provider and its state is "inService", false otherwise.
     * @throws SecurityException when called by an AccessControlContext
     *         that has not been granted the "setPolicy" SecurityPermission.
     * @throws PolicyContextException if the implementation throws a checked
     *         exception that has not been accounted for by the inService
     *         method signature. The exception thrown by the implementation
     *         class will be encapsulated (during construction) in the thrown
     *         PolicyContextException.
     */
    public boolean inService(String contextID) throws PolicyContextException, SecurityException {
        return policyConfigurationFactory.inService(contextID);
    }


}
