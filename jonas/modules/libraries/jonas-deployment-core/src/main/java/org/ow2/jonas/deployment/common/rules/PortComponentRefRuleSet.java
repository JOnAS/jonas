/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.common.rules;

import org.apache.commons.digester.Digester;

/**
 * This class defines a rule to analyze port-component-ref
 * @author Florent Benoit
 */
public class PortComponentRefRuleSet extends JRuleSetBase {

    /**
     * Construct an object with a specific prefix
     * @param prefix prefix to use during the parsing
     */
    public PortComponentRefRuleSet(String prefix) {
        super(prefix);
    }

    /**
     * Add a set of rules to the digester object
     * @param digester Digester instance
     */
    public void addRuleInstances(Digester digester) {
        digester.addObjectCreate(prefix + "port-component-ref",
                                 "org.ow2.jonas.deployment.common.xml.PortComponentRef");

        digester.addSetNext(prefix + "port-component-ref",
                            "addPortComponentRef",
                            "org.ow2.jonas.deployment.common.xml.PortComponentRef");
        digester.addCallMethod(prefix + "port-component-ref/service-endpoint-interface",
                               "setServiceEndpointInterface", 0);
        digester.addCallMethod(prefix + "port-component-ref/port-component-link",
                               "setPortComponentLink", 0);
    }

    /**
     * @return the namespaceURI that is relevant for this RuleSet.
     */
    @Override
    public String getNamespaceURI() {
        return "http://java.sun.com/xml/ns/j2ee";
    }
    
}
