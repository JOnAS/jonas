/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.domain.xml;

import org.ow2.jonas.deployment.common.CommonsSchemas;
import org.ow2.jonas.deployment.common.xml.AbsDescriptionElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;
import org.ow2.jonas.deployment.common.xml.TopLevelElement;
import org.ow2.jonas.deployment.domain.DomainSchemas;

/**
 * This class defines the implementation of the domain
 *
 * @author Adriana Danes
 * @author S. Ali Tokmen
 */

public class Domain
    extends AbsDescriptionElement
    implements TopLevelElement {

    /**
     * Version UID
     */
    private static final long serialVersionUID = 3866056043763651049L;

    /**
     * servers
     */
    private JLinkedList serverList = null;

    /**
     * cluster daemons
     */
    private JLinkedList clusterDaemonList = null;

    /**
     * cluster
     */
    private JLinkedList clusterList = null;

    /**
     * Domain name
     */
    private String name = null;

    /**
     * Default user name of the domain
     */
    private String username = null;

    /**
     * Default password of the domain, may be encoded
     */
    private String password = null;

    /**
     * Latest XML Header.
     */
    private static final String header = CommonsSchemas.getHeaderForElement("domain",
                                                                            DomainSchemas.getLastSchema());

    /**
     * Constructor
     */
    public Domain() {
        super();
        serverList = new  JLinkedList("server");
        clusterList = new  JLinkedList("cluster");
        clusterDaemonList = new  JLinkedList("clusterDaemon");
    }

    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name to set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return Returns the default username of the domain.
     */
    public final String getUsername() {
        return username;
    }

    /**
     * @param username The default username of the domain to set.
     */
    public final void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return Returns the default password of the domain
     *         as encoded in the XML file.
     */
    public final String getPassword() {
        return password;
    }

    /**
     * @param password The default password of the domain to set
     *                 as encoded in the XML file.
     */
    public final void setPassword(String password) {
        this.password = password;
    }

    /**
     * Add a new cluster element to this object
     * @param cluster the Cluster object
     */
    public void addCluster(Cluster cluster) {
        clusterList.add(cluster);
    }

    /**
     * Add a new cluster daemon element to this object
     * @param clusterDaemon the ClusterDaemon object
     */
    public void addClusterDaemon(ClusterDaemon clusterDaemon) {
        clusterDaemonList.add(clusterDaemon);
    }

    /**
     * Add a new server element to this object
     * @param server the Server object
     */
    public void addServer(Server server) {
        serverList.add(server);
    }

    /**
     * @return Returns the clusterList.
     */
    public JLinkedList getClusterList() {
        return clusterList;
    }

    /**
     * @param clusterList The clusterList to set.
     */
    public void setClusterList(JLinkedList clusterList) {
        this.clusterList = clusterList;
    }

    /**
     * @param clusterDaemonList The clusterDaemonList to set.
     */
    public void setClusterDaemonList(JLinkedList clusterDaemonList) {
        this.clusterDaemonList = clusterDaemonList;
    }

    /**
     * @return Returns the clusterDaemonList.
     */
    public JLinkedList getClusterDaemonList() {
        return clusterDaemonList;
    }

    /**
     * @return Returns the serverList.
     */
    public JLinkedList getServerList() {
        return serverList;
    }

    /**
     * @param serverList The serverList to set.
     */
    public void setServerList(JLinkedList serverList) {
        this.serverList = serverList;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prefixing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append(header);
        indent += 2;

        // name
        if (name != null) {
            sb.append(xmlElement(getName(), "name", indent));
        }

        // description
        if (getDescription() != null) {
            sb.append(xmlElement(getDescription(), "description", indent));
        }

        // username and password
        if (username != null && password != null) {
            sb.append(xmlElement(username, "username", indent));
            sb.append(xmlElement(password, "password", indent));
        }

        // cluster daemons
        sb.append(getClusterDaemonList().toXML(indent));

        // servers
        sb.append(getServerList().toXML(indent));

        // clusters
        sb.append(getClusterList().toXML(indent));

        indent -= 2;
        sb.append(indent(indent));
        sb.append("</domain>\n");

        return sb.toString();
    }

}
