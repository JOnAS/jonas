/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.common.xml;

/**
 * This class defines the implementation of the element security-role-mapping.
 * It allow to define mapping between roles and principal which use these roles.
 * @author Florent Benoit
 */

public class SecurityRoleMapping extends AbsElement {

    private static final long serialVersionUID = -6101737383745789441L;
    /**
     * role name
     */
    private String roleName = null;

    /**
     * principal names
     */
    private JLinkedList principalNamesList = null;

    /**
     * Constructor
     */
    public SecurityRoleMapping() {
        super();
        principalNamesList = new JLinkedList("principal-name");
    }

    /**
     * Gets the principal-name list
     * @return the principal-name list
     */
    public JLinkedList getPrincipalNamesList() {
        return principalNamesList;
    }

    /**
     * Set the role-name element of this object
     * @param roleName the name of the role
     */
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    /**
     * @return the role Name.
     */
    public String getRoleName() {
        return roleName;
    }

    /**
     * Add the given principal to the list of principals
     * @param principalName name of the principal to add.
     */
    public void addPrincipalName(String principalName) {
        principalNamesList.add(principalName);
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<security-role-mapping>\n");

        indent += 2;

        // role-name
        sb.append(xmlElement(roleName, "role-name", indent));

        // principal-name
        sb.append(principalNamesList.toXML(indent));

        indent -= 2;
        sb.append(indent(indent));
        sb.append("</security-role-mapping>\n");

        return sb.toString();
    }
}