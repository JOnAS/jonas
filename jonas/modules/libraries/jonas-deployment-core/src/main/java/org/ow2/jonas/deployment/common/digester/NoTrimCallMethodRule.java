/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.common.digester;

import org.apache.commons.digester.CallMethodRule;

/**
 * Do not apply trim on the given body. 
 * @author Florent Benoit
 */
public class NoTrimCallMethodRule extends CallMethodRule {

    public NoTrimCallMethodRule(String methodName, int paramCount) {
        super(methodName, paramCount);
    }
    
    /**
     * Process the body text of this element.
     * @param bodyText The body text of this element
     */
    public void body(String bodyText) throws Exception {
        if (paramCount == 0) {
            // Do not apply trim()
            this.bodyText = bodyText;
        }

    }

}
