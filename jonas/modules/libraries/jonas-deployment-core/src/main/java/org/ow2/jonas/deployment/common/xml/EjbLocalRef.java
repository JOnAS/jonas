/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.common.xml;

/**
 * This class defines the implementation of the element ejb-local-ref.
 * @author Florent Benoit
 */
public class EjbLocalRef extends AbsElement {

    /**
     * Description of the ejb-local-ref
     */
    private String description = null;

    /**
     * Name of this ejb-local-ref
     */
    private String ejbRefName = null;

    /**
     * Type of this ejb-local-ref
     */
    private String ejbRefType = null;

    /**
     * Local Home of this ejb-local-ref
     */
    private String localHome = null;

    /**
     * Local interface of this ejb-local-ref
     */
    private String local = null;

    /**
     * ejb-link of this ejb-local-ref
     */
    private String ejbLink = null;


    // Setters

    /**
     * Sets the description
     * @param description the description to use
     */
    public void setDescription(String description) {
        this.description = description;
    }


    /**
     * Sets the name
     * @param ejbRefName the name to use
     */
    public void setEjbRefName(String ejbRefName) {
        this.ejbRefName = ejbRefName;
    }


    /**
     * Sets the type
     * @param ejbRefType the type to use
     */
    public void setEjbRefType(String ejbRefType) {
        this.ejbRefType = ejbRefType;
    }


    /**
     * Sets the local home interface
     * @param localHome the local home interface to use
     */
    public void setLocalHome(String localHome) {
        this.localHome = localHome;
    }
    /**
     * Sets the local interface
     * @param local the local interface to use
     */
    public void setLocal(String local) {
        this.local = local;
    }


    /**
     * Sets the ejb-link
     * @param ejbLink the ejb-link to use
     */
    public void setEjbLink(String ejbLink) {
        this.ejbLink = ejbLink;
    }


    // Getters

    /**
     * @return the description of the ejb-local-ref
     */
    public String getDescription() {
        return description;
    }


    /**
     * @return the name of the ejb-local-ref
     */
    public String getEjbRefName() {
        return ejbRefName;
    }


    /**
     * @return the type of the ejb-local-ref
     */
    public String getEjbRefType() {
        return ejbRefType;
    }

    /**
     * @return the local home interface  of the ejb-local-ref
     */
    public String getLocalHome() {
        return localHome;
    }

    /**
     * @return the local interface of the ejb-local-ref
     */
    public String getLocal() {
        return local;
    }

    /**
     * @return the ejb-link of the ejb-local-ref
     */
    public String getEjbLink() {
        return ejbLink;
    }





    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<ejb-local-ref>\n");

        indent += 2;

        // Description
        sb.append(xmlElement(description, "description", indent));

        // name
        sb.append(xmlElement(ejbRefName, "ejb-ref-name", indent));

        // type
        sb.append(xmlElement(ejbRefType, "ejb-ref-type", indent));

        // local home interface
        sb.append(xmlElement(localHome, "local-home", indent));

        // local interface
        sb.append(xmlElement(local, "local", indent));

        // ejb-link
        sb.append(xmlElement(ejbLink, "ejb-link", indent));

        indent -= 2;
        sb.append(indent(indent));
        sb.append("</ejb-local-ref>\n");

        return sb.toString();
    }


}
