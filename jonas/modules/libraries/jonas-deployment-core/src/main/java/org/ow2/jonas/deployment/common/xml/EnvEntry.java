/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.common.xml;

/**
 * This class defines the implementation of the element env-entry.
 * @author Florent Benoit
 */
public class EnvEntry extends AbsElement {

    /**
     * Description of the env-entry
     */
    private String description = null;

    /**
     * Name of the env-entry
     */
    private String envEntryName = null;

    /**
     * Value of the env-entry
     */
    private String envEntryValue = null;

    /**
     * Type of the env-entry
     */
    private String envEntryType = null;

    /**
     * Lookup-name of the env-entry
     */
    private String envEntryLookupName = null;

    // Setters

    /**
     * Sets the description
     * @param description the description to use
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Sets the name
     * @param envEntryName the name to use
     */
    public void setEnvEntryName(final String envEntryName) {
        this.envEntryName = envEntryName;
    }

    /**
     * Sets the value
     * @param envEntryValue the value to use
     */
    public void setEnvEntryValue(final String envEntryValue) {
        this.envEntryValue = envEntryValue;
    }

    /**
     * Sets the type
     * @param envEntryType the type to use
     */
    public void setEnvEntryType(final String envEntryType) {
        this.envEntryType = envEntryType;
    }

    /**
     * Sets the lookup name
     * @param envEntryLookupName the value to use
     */
    public void setEnvEntryLookupName(final String envEntryLookupName) {
        this.envEntryLookupName = envEntryLookupName;
    }

    // Getters

    /**
     * @return the description of the env-entry
     */
    public String getDescription() {
        return description;
    }


    /**
     * @return the name of the env-entry
     */
    public String getEnvEntryName() {
        return envEntryName;
    }


    /**
     * @return the value of the env-entry
     */
    public String getEnvEntryValue() {
        return envEntryValue;
    }

    /**
     * @return the value of the lookup-name
     */
    public String getEnvEntryLookupName() {
        return envEntryLookupName;
    }

    
    /**
     * @return the type of the env-entry
     */
    public String getEnvEntryType() {
        return envEntryType;
    }


    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    @Override
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<env-entry>\n");

        indent += 2;

        // Description
        sb.append(xmlElement(description, "description", indent));

        // name
        sb.append(xmlElement(envEntryName, "env-entry-name", indent));

        // type
        sb.append(xmlElement(envEntryType, "env-entry-type", indent));

        // value
        if (envEntryValue != null) {
            sb.append(xmlElement(envEntryValue, "env-entry-value", indent));
        }
        
        // value
        if (envEntryLookupName != null) {
            sb.append(xmlElement(envEntryLookupName, "lookup-name", indent));
        }

        indent -= 2;
        sb.append(indent(indent));
        sb.append("</env-entry>\n");

        return sb.toString();
    }

}
