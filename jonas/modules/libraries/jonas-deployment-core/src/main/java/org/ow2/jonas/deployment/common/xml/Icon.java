/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.common.xml;

/**
 * This class defines the implementation of the element icon.
 * @author Florent Benoit
 */
public class Icon extends AbsElement {

    /**
     * Name of the small icon
     */
    private String smallIcon = null;

    /**
     * Name of the large icon
     */
    private String largeIcon = null;


    // Setters

    /**
     * Sets the small icon name
     * @param  smallIcon the name of the file for small GIF or JPEG icon image
     */
    public void setSmallIcon(String smallIcon) {
        this.smallIcon = smallIcon;
    }


     /**
     * Sets the large icon name
     * @param  largeIcon the name of the file for large GIF or JPEG icon image
     */
    public void setLargeIcon(String largeIcon) {
        this.largeIcon = largeIcon;
    }




    // Getters

    /**
     * @return the small icon name
     */
    public String getSmallIcon() {
        return smallIcon;
    }


    /**
     * @return the large icon name
     */
    public String getLargeIcon() {
        return largeIcon;
    }


    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<icon>\n");

        indent += 2;

        // small-icon
        sb.append(xmlElement(smallIcon, "small-icon", indent));

        // large-icon
        sb.append(xmlElement(largeIcon, "large-icon", indent));

        indent -= 2;
        sb.append(indent(indent));
        sb.append("</icon>\n");

        return sb.toString();
    }


}
