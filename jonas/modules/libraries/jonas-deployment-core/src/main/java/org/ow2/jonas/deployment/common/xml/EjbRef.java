/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.common.xml;

/**
 * This class defines the implementation of the element ejb-ref.
 * @author Florent Benoit
 */
public class EjbRef extends AbsElement {

    /**
     * Description of the ejb-ref
     */
    private String description = null;

    /**
     * Name of this ejb-ref
     */
    private String ejbRefName = null;

    /**
     * Type of this ejb-ref
     */
    private String ejbRefType = null;

    /**
     * Home of this ejb-ref
     */
    private String home = null;

    /**
     * Remote of this ejb-ref
     */
    private String remote = null;

    /**
     * ejb-link of this ejb-ref
     */
    private String ejbLink = null;


    // Setters

    /**
     * Sets the description
     * @param description the description to use
     */
    public void setDescription(String description) {
        this.description = description;
    }


    /**
     * Sets the name
     * @param ejbRefName the name to use
     */
    public void setEjbRefName(String ejbRefName) {
        this.ejbRefName = ejbRefName;
    }


    /**
     * Sets the type
     * @param ejbRefType the type to use
     */
    public void setEjbRefType(String ejbRefType) {
        this.ejbRefType = ejbRefType;
    }


    /**
     * Sets the home interface
     * @param home the home interface to use
     */
    public void setHome(String home) {
        this.home = home;
    }
    /**
     * Sets the remote interface
     * @param remote the remote interface to use
     */
    public void setRemote(String remote) {
        this.remote = remote;
    }


    /**
     * Sets the ejb-link
     * @param ejbLink the ejb-link to use
     */
    public void setEjbLink(String ejbLink) {
        this.ejbLink = ejbLink;
    }


    // Getters

    /**
     * @return the description of the ejb-ref
     */
    public String getDescription() {
        return description;
    }


    /**
     * @return the name of the ejb-ref
     */
    public String getEjbRefName() {
        return ejbRefName;
    }


    /**
     * @return the type of the ejb-ref
     */
    public String getEjbRefType() {
        return ejbRefType;
    }

    /**
     * @return the home interface  of the ejb-ref
     */
    public String getHome() {
        return home;
    }

    /**
     * @return the remote interface of the ejb-ref
     */
    public String getRemote() {
        return remote;
    }

    /**
     * @return the ejb-link of the ejb-ref
     */
    public String getEjbLink() {
        return ejbLink;
    }





    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<ejb-ref>\n");

        indent += 2;

        // Description
        sb.append(xmlElement(description, "description", indent));

        // name
        sb.append(xmlElement(ejbRefName, "ejb-ref-name", indent));

        // type
        sb.append(xmlElement(ejbRefType, "ejb-ref-type", indent));

        // home interface
        sb.append(xmlElement(home, "home", indent));

        // remote interface
        sb.append(xmlElement(remote, "remote", indent));

        // ejb-link
        sb.append(xmlElement(ejbLink, "ejb-link", indent));

        indent -= 2;
        sb.append(indent(indent));
        sb.append("</ejb-ref>\n");

        return sb.toString();
    }


}
