/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.common.lib;

import java.io.InputStream;
import java.io.Reader;

import org.w3c.dom.ls.LSInput;
import org.xml.sax.InputSource;

/**
 * A wrapper around the InputSource class returned by EntityResolver
 * to be returned by the ResourceResolver class.
 * 
 * @author Patrick Smith
 * @author Greg Lapouchnian
 */
public class JLSInput implements LSInput {

    /**
     * The InputSource that this class is wrapping.
     */
    private InputSource source;

    /**
     * Creates a wrapper around the given InputSource object.
     * @param source
     */
    public JLSInput(InputSource source) {
        this.source = source;
    }

    /**
     * Return the character stream representation of the resource.
     * @return character stream representation of the resource
     */
    public Reader getCharacterStream() {
        return source.getCharacterStream();
    }

    public void setCharacterStream(Reader arg0) {}

    /**
     * Return the byte stream representation of the resource.
     * @return byte stream representation of the resource
     */
    public InputStream getByteStream() {
        return source.getByteStream();
    }

    public void setByteStream(InputStream arg0) {}

    public String getStringData() {
        // No corresponding method in InputSource
        return null;
    }

    public void setStringData(String arg0) {}

    /**
     * Returns the System ID of the resource.
     * @return the system ID of the resource
     */
    public String getSystemId() {
        return source.getSystemId();
    }

    public void setSystemId(String id) {}

    /**
     * Returns the Public ID of the resource.
     * @return the public ID of the resource
     */
    public String getPublicId() {
        return source.getPublicId();
    }

    public void setPublicId(String id) {}

    public String getBaseURI() {
        return null;
    }

    public void setBaseURI(String arg0) {}

    /**
     * Returns the encoding of the resource.
     * @return the encoding of the resource
     */
    public String getEncoding() {
        return source.getEncoding();
    }

    public void setEncoding(String arg0) {}

    public boolean getCertifiedText() {
        return false;
    }

    public void setCertifiedText(boolean arg0) {}

}
