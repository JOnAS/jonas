/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Eric Hardesty
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.common.xml;

/**
 * This class defines the implementation of the element message-destination-ref.
 * @author Eric Hardesty
 */
public class MessageDestinationRef extends AbsElement {

    /**
     * Description of the message-destination-ref
     */
    private String description = null;

    /**
     * Name of this message-destination-ref
     */
    private String messageDestinationRefName = null;

    /**
     * Type of this message-destination-ref
     */
    private String messageDestinationType = null;

    /**
     * Usage of this message-destination-ref
     */
    private String messageDestinationUsage = null;

    /**
     * Link of this message-destination-ref
     */
    private String messageDestinationLink = null;


    // Setters

    /**
     * Sets the description
     * @param description the description to use
     */
    public void setDescription(String description) {
        this.description = description;
    }


    /**
     * Sets the name
     * @param refName the name to use
     */
    public void setMessageDestinationRefName(String refName) {
        this.messageDestinationRefName = refName;
    }


    /**
     * Sets the type
     * @param type the type to use
     */
    public void setMessageDestinationType(String type) {
        this.messageDestinationType = type;
    }


    /**
     * Sets the usage
     * @param usage the usage to use
     */
    public void setMessageDestinationUsage(String usage) {
        this.messageDestinationUsage = usage;
    }


    /**
     * Sets the link
     * @param link the link to use
     */
    public void setMessageDestinationLink(String link) {
        this.messageDestinationLink = link;
    }


    // Getters

    /**
     * @return the description of the messageDestination-ref
     */
    public String getDescription() {
        return description;
    }


    /**
     * @return the name of the messageDestination-ref
     */
    public String getMessageDestinationRefName() {
        return messageDestinationRefName;
    }


    /**
     * @return the type of the messageDestination-ref
     */
    public String getMessageDestinationType() {
        return messageDestinationType;
    }

    /**
     * @return the usage of the messageDestination-ref
     */
    public String getMessageDestinationUsage() {
        return messageDestinationUsage;
    }

    /**
     * @return the link of the messageDestination-ref
     */
    public String getMessageDestinationLink() {
        return messageDestinationLink;
    }



    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<message-destination-ref>\n");

        indent += 2;

        // Description
        sb.append(xmlElement(description, "description", indent));

        // name
        sb.append(xmlElement(messageDestinationRefName, "message-destination-ref-name", indent));

        // type
        sb.append(xmlElement(messageDestinationType, "message-destination-type", indent));

        // usage
        sb.append(xmlElement(messageDestinationUsage, "message-destination-usage", indent));

        // link
        sb.append(xmlElement(messageDestinationLink, "message-destination-link", indent));

        indent -= 2;
        sb.append(indent(indent));
        sb.append("</message-destination-ref>\n");

        return sb.toString();
    }


}
