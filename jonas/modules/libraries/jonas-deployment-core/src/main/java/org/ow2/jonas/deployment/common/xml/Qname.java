/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.common.xml;

import javax.xml.namespace.QName;

/**
 * This class defines an implementation for a QName
 * It is use by Soap-Header or Service-qname
 * @author Florent Benoit
 */
public class Qname extends AbsElement {

    /**
     * Name of the element
     */
    private String name = null;

    /**
     * Internal QName
     */
    private QName qName = null;


    /**
     * Constructor : build a new object
     */
    public Qname() {
        super();
    }


    // Setters

    /**
     * Sets the QName of this object
     * @param qName QName of this object
     */
    public void setQName(QName qName) {
        this.qName = qName;
    }

    /**
     * Sets the Name of this object
     * @param name name of this object
     */
    public void setName(String name) {
        this.name = name;
    }

    // Getters

    /**
     * @return the QName of this object
     */
    public QName getQName() {
        return qName;
    }



    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        //String prefix = qName.getPrefix();
        String prefix = "pr";

        if (qName == null) {
            return "";
        }

        String namespaceURI = qName.getNamespaceURI();
        String localPart = qName.getLocalPart();


        sb.append("<");
        sb.append(name);
        sb.append(" xmlns:");
        sb.append(prefix);
        sb.append("=\"");
        sb.append(namespaceURI);
        sb.append("\">");
        sb.append(prefix);
        sb.append(":");
        sb.append(localPart);
        sb.append("</");
        sb.append(name);
        sb.append(">\n");

        return sb.toString();
    }

}
