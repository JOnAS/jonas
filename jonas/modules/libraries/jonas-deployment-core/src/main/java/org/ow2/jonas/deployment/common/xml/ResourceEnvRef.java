/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.common.xml;

/**
 * This class defines the implementation of the element resource-env-ref.
 * @author Florent Benoit
 */
public class ResourceEnvRef extends AbsElement {

    /**
     * Description of the resource-env-ref
     */
    private String description = null;

    /**
     * Name of this resource-env-ref
     */
    private String resourceEnvRefName = null;

    /**
     * Type of this resource-env-ref
     */
    private String resourceEnvRefType = null;


    // Setters

    /**
     * Sets the description
     * @param description the description to use
     */
    public void setDescription(String description) {
        this.description = description;
    }


    /**
     * Sets the name
     * @param resourceEnvRefName the name to use
     */
    public void setResourceEnvRefName(String resourceEnvRefName) {
        this.resourceEnvRefName = resourceEnvRefName;
    }


    /**
     * Sets the type
     * @param resourceEnvRefType the type to use
     */
    public void setResourceEnvRefType(String resourceEnvRefType) {
        this.resourceEnvRefType = resourceEnvRefType;
    }



    // Getters

    /**
     * @return the description of the resource-env-ref
     */
    public String getDescription() {
        return description;
    }


    /**
     * @return the name of the resource-env-ref
     */
    public String getResourceEnvRefName() {
        return resourceEnvRefName;
    }


    /**
     * @return the type of the resource-env-ref
     */
    public String getResourceEnvRefType() {
        return resourceEnvRefType;
    }



    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<resource-env-ref>\n");

        indent += 2;

        // Description
        sb.append(xmlElement(description, "description", indent));

        // name
        sb.append(xmlElement(resourceEnvRefName, "resource-env-ref-name", indent));

        // type
        sb.append(xmlElement(resourceEnvRefType, "resource-env-ref-type", indent));

        indent -= 2;
        sb.append(indent(indent));
        sb.append("</resource-env-ref>\n");

        return sb.toString();
    }



}
