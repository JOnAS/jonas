/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.common.xml;

import java.io.Serializable;

/**
 * This class defines the interface that all Element objects must use.
 * These elements are used by Digester during the xml parsing
 * @author Florent Benoit
 */
public interface Element extends Serializable {


    /**
     * Represents this element by it's XML description
     * @param indent use this indent for prexifing XML representation
     * @return the XML description of this object
     */
    String toXML(int indent);


    /**
     * Represents this element by it's XML description
     * Use a default indent set to 0
     * @return the XML description of this object
     */
    String toXML();

}
