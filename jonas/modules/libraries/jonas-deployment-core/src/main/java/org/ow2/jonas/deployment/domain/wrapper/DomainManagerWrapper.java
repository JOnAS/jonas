/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.domain.wrapper;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.ow2.jonas.deployment.domain.DomainMap;
import org.ow2.jonas.deployment.domain.DomainMapException;
import org.ow2.jonas.lib.bootstrap.LoaderManager;


/**
 *
 *
 * @author Adriana Danes
 */
public class DomainManagerWrapper {

    /**
     * DomainMapManager fully qualified classname
     */
    private static final String DOMAIN_MANAGER_CLASSNAME = "org.ow2.jonas.deployment.domain.lib.DomainMapManager";

    /**
     * Empty private constructor for utility classes
     */
    private DomainManagerWrapper() { }

    /**
     * Wrap DomainMapManager.getDeploymentDesc(domainFileName, cl)
     *
     * @param domainFileName name of the file containing the domain map
     * @param cl Domain ClassLoader
     *
     * @return the DomainMap containing the map description
     *
     * @throws DomainMapException When DomainMap can't be created
     */
    public static DomainMap getDomainMap(final String domainFileName, final ClassLoader cl)
    throws DomainMapException {
        LoaderManager lm = LoaderManager.getInstance();
        DomainMap map = null;
        try {
            ClassLoader ext = lm.getExternalLoader();
            Class manager = ext.loadClass(DOMAIN_MANAGER_CLASSNAME);
            Method m = manager.getDeclaredMethod("getDomainMap", new Class[] {String.class, ClassLoader.class});
            map = (DomainMap) m.invoke(null, new Object[] {domainFileName, cl});
        } catch (InvocationTargetException ite) {
            Throwable t = ite.getTargetException();
            if (DomainMapException.class.isInstance(t)) {
                throw (DomainMapException) ite.getTargetException();
            } else {
                throw new DomainMapException("DomainMapManager.getDomainMap fails", t);
            }
        } catch (Exception e) {
            // TODO add i18n here
            throw new DomainMapException("Problems when using reflection on DomainMapManager", e);
        }

        return map;
    }

    /**
     * Wrap DomainMapManager.setParsingWithValidation(b)
     *
     * @param b true/false
     */
    public static void setParsingWithValidation(final boolean b) {
        LoaderManager lm = LoaderManager.getInstance();

        try {
            ClassLoader ext = lm.getExternalLoader();
            Class manager = ext.loadClass(DOMAIN_MANAGER_CLASSNAME);
            Method m = manager.getDeclaredMethod("setParsingWithValidation", new Class[] {boolean.class});
            m.invoke(null, new Object[] {new Boolean(b)});
        } catch (Exception e) {
            // Should never occurs
            //logger.log(BasicLevel.ERROR, e);
        }
    }

}
