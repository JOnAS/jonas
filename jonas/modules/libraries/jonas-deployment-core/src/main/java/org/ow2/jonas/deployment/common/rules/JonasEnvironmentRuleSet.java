/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.common.rules;

import org.apache.commons.digester.Digester;

/**
 * This class defines a rule to analyze JOnAS environment of bean, web app,
 * client, etc
 * @author Florent Benoit
 */
public class JonasEnvironmentRuleSet extends JRuleSetBase {

    /**
     * Construct an object with a specific prefix
     * @param prefix prefix to use during the parsing
     */
    public JonasEnvironmentRuleSet(String prefix) {
        super(prefix);
    }

    /**
     * Add a set of rules to the digester object
     * @param digester Digester instance
     */
    public void addRuleInstances(Digester digester) {
        digester.addRuleSet(new JonasResourceEnvRuleSet(prefix));
        digester.addRuleSet(new JonasResourceRuleSet(prefix));
        digester.addRuleSet(new JonasEjbRefRuleSet(prefix));
        digester.addRuleSet(new JonasServiceRefRuleSet(prefix));
        digester.addRuleSet(new JonasMessageDestinationRefRuleSet(prefix));
        digester.addCallMethod(prefix + "run-as/principal-name", "setRunAsPrincipalName", 0);
    }

}