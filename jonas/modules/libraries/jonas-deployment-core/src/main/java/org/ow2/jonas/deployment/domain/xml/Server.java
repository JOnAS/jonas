/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.domain.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
/**
 * JOnAS instance
 *
 * @author Adriana Danes
 * @author S. Ali Tokmen
 */
public class Server extends AbsElement  {

    /**
     * Version UID
     */
    private static final long serialVersionUID = 3775047449169303947L;

    /**
     * Instance name
     */
    private String name = null;

    /**
     * description
     */
    private String description = null;

    /**
     * URL
     */
    private Location location = null;

    /**
     * User name
     */
    private String username = null;

    /**
     * Password, may be encoded
     */
    private String password = null;

    /**
     * Cluster Daemon
     */
    private String clusterDaemon = null;

    // TO DO
    // Add state element

    /**
     * Constructor
     */
    public Server() {
        super();
    }

    /**
     * @return Returns the location.
     */
    public Location getLocation() {
        return location;
    }

    /**
     * @param location The location to set.
     */
    public void setLocation(Location location) {
        this.location = location;
    }

    /**
     * @param location The location to set.
     */
    public void addLocation(Location location) {
        this.location = location;
    }

    /**
     * @return Returns the username.
     */
    public final String getUsername() {
        return username;
    }

    /**
     * @param username The username to set.
     */
    public final void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return Returns the password as encoded in the XML file.
     */
    public final String getPassword() {
        return password;
    }

    /**
     * @param password The password to set as encoded in the XML file.
     */
    public final void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name to set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return Returns the description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description to set.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<server>\n");

        indent += 2;

        // name
        if (name != null) {
            sb.append(xmlElement(name, "name", indent));
        }

        // description
        if (getDescription() != null) {
            sb.append(xmlElement(getDescription(), "description", indent));
        }

        // location
        if (location != null) {
            sb.append(location.toXML(indent));
        }

        // username and password
        if (username != null && password != null) {
            sb.append(xmlElement(username, "username", indent));
            sb.append(xmlElement(password, "password", indent));
        }

        // cluster-daemon
        if (clusterDaemon != null) {
            sb.append(xmlElement(clusterDaemon, "cluster-daemon", indent));
        }

        indent -= 2;
        sb.append(indent(indent));
        sb.append("</server>\n");

        return sb.toString();
    }

    /**
     *
     * @return the cluster daemon name
     */
    public String getClusterDaemon() {
        return clusterDaemon;
    }

    /**
     * Set the cluster daemon name
     * @param clusterDaemon name
     */
    public void setClusterDaemon(String clusterDaemon) {
        this.clusterDaemon = clusterDaemon;
    }
}