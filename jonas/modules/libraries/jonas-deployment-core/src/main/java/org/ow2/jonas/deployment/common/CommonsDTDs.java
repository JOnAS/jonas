/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.common;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.ow2.jonas.deployment.api.DTDs;


/**
 * This class defines the declarations of defaults DTDs used for J2EE 1.4 and less
 * @author Florent Benoit
 */
public abstract class CommonsDTDs implements DTDs {

    /**
     * Commons DTDs location.
     */
    private static final String PACKAGE = "org/ow2/jonas/deployment/";

    /**
     * List of default dtds.
     */
    private static final String[] DEFAULT_DTDS = new String[] {
        PACKAGE + "XMLSchema.dtd",
        PACKAGE + "datatypes.dtd"
    };

    /**
     * List of default publicId.
     */
    private static final String[] DEFAULT_DTDS_PUBLIC_ID = new String[] {
        "-//W3C//DTD XMLSCHEMA 200102//EN",
        "datatypes"
    };


    /**
     * Map where mapping publicId/dtds are stored.
     */
    private static HashMap dtdsMapping = null;

    /**
     * Build a new object for DTDs handling.
     */
    public CommonsDTDs() {
        dtdsMapping = new HashMap();
        addMapping(DEFAULT_DTDS, DEFAULT_DTDS_PUBLIC_ID, CommonsDTDs.class.getClassLoader());
    }

    /**
     * Gets the mapping between publicIds and DTDs.
     * @return the mapping between publicIds and DTDs.
     */
    public Map getMapping() {
        return dtdsMapping;
    }



    /**
     * Add to the list of DTDS the given dtds/publicId.
     * @param dtds array of dtds
     * @param publicIds array of publicIds
     */
    protected void addMapping(String[] dtds, String[] publicIds, ClassLoader loader) {
        if (dtds.length != publicIds.length) {
            throw new IllegalStateException("SEVERE ERROR !!! Number of dtds is different of the number of PublicId !!! check the source code");
        }

        URL url = null;
        for (int i = 0; i < dtds.length; i++) {
            url = loader.getResource(dtds[i]);
            if (url == null) {
                throw new IllegalStateException("'" + dtds[i] + "' was not found in the current classloader !");
            }
            dtdsMapping.put(publicIds[i], url.toString());
        }
    }

}
