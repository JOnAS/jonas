/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Philippe Coq
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.common.xml;


/**
 * This interface provides to the value of Xml elements of a JndiEnvRefsGroup
 * @author Philippe Coq
 */

public interface JndiEnvRefsGroupXml extends DescriptionGroupXml {
    /**
     * @return the list of all ejb-local-ref elements
     */
    JLinkedList getEjbLocalRefList();

    /**
     * @return the list of all ejb-ref elements
     */
    JLinkedList getEjbRefList();

    /**
     * @return the list of all env-entry elements
     */
    JLinkedList getEnvEntryList();

    /**
     * @return the list of all resource-env-ref elements
     */
    JLinkedList getResourceEnvRefList();

    /**
     * @return the list of all resource-ref elements
     */
    JLinkedList getResourceRefList();

    /**
     * @return the list of all service-ref elements
     */
    JLinkedList getServiceRefList();

    /**
     * @return the list of all message-destination-ref elements
     */
    JLinkedList getMessageDestinationRefList();

    /**
     * @return the list of all persistence-unit-ref elements
     */
    JLinkedList getPersistenceUnitRefList();
}
