/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.common.xml;



/**
 * This class defines the implementation of the element jonas-port-component-ref.
 * @author Florent Benoit
 */
public class JonasPortComponentRef extends AbsElement {

    /**
     * service endpoint interface
     */
    private String serviceEndpointInterface = null;

    /**
     * wsdl port
     */
    private Qname wsdlPort = null;

    /**
     * jonas-stub-property list
     */
    private JLinkedList jonasStubPropList = null;

    /**
     * jonas-call-property list
     */
    private JLinkedList jonasCallPropList = null;

    /**
     * Constructor : build a new JonasServiceRef object
     */
    public JonasPortComponentRef() {
        jonasStubPropList = new JLinkedList("jonas-stub-property");
        jonasCallPropList = new JLinkedList("jonas-call-property");
    }


    // Setters


    /**
     * Sets the wsdl port QName of the jonas-port-component-ref
     * @param wsdlPort wsdl port QName of the jonas-port-component-ref
     */
    public void setWsdlPort(Qname wsdlPort) {
        this.wsdlPort = wsdlPort;
    }

    /**
     * Sets the service endpoint interface of the port-component-ref
     * @param serviceEndpointInterface service endpoint interface of the port-component-ref
     */
    public void setServiceEndpointInterface(String serviceEndpointInterface) {
        this.serviceEndpointInterface = serviceEndpointInterface;
    }

    /**
     * Add a parameter
     * @param jonasCallProperty the JonasCallProperty object to add to our list
     */
    public void addJonasCallProperty(JonasCallProperty jonasCallProperty) {
        jonasCallPropList.add(jonasCallProperty);
    }

    /**
     * Add a parameter
     * @param jonasStubProperty the JonasStubProperty object to add to our list
     */
    public void addJonasStubProperty(JonasStubProperty jonasStubProperty) {
        jonasStubPropList.add(jonasStubProperty);
    }


    // Getters

    /**
     * @return the service endpoint interface of the port-component-ref
     */
    public String getServiceEndpointInterface() {
        return serviceEndpointInterface;
    }

    /**
     * @return the wsdl port QName of the jonas-port-component-ref
     */
    public Qname getWsdlPort() {
        return wsdlPort;
    }

    /**
     * @return the list of jonas-call-property
     */
    public JLinkedList getJonasCallPropertyList() {
        return jonasCallPropList;
    }

    /**
     * @return the list of jonas-stub-property
     */
    public JLinkedList getJonasStubPropertyList() {
        return jonasStubPropList;
    }


    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<jonas-port-component-ref>\n");

        indent += 2;

        // service-endpoint-interface
        if (serviceEndpointInterface != null) {
            sb.append(xmlElement(serviceEndpointInterface, "service-endpoint-interface", indent));
        }

        // wsdl-port
        if (wsdlPort != null) {
            sb.append(wsdlPort.toXML(indent));
        }

        // jonas-stub-property
        sb.append(jonasStubPropList.toXML(indent));

        // jonas-call-property
        sb.append(jonasCallPropList.toXML(indent));

        indent -= 2;
        sb.append(indent(indent));
        sb.append("</jonas-port-component-ref>\n");

        return sb.toString();
    }
}
