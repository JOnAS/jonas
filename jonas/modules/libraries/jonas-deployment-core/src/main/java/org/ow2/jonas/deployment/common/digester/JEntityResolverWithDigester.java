/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.common.digester;


import java.io.IOException;

import org.ow2.jonas.deployment.common.lib.JEntityResolver;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * This class defines the entity resolver used to resolve DTDs/Schemas during
 * the xml parsing. It extends the JEntityResolver
 * in order to allow for that parser to be used without having the digester classes.
 */
public class JEntityResolverWithDigester extends JEntityResolver implements EntityResolver {

    /**
     * JDigester Object associated
     */
    private JDigester jDigester = null;


    /**
     * Constructor
     *
     * @param jd linked JDigester object
     */
    public JEntityResolverWithDigester(JDigester jd) {
        super();
        jDigester = jd;
    }

    /**
     * The Parser will call this method before opening any external entity except
     * the top-level document entity.
     * @param publicId The public identifier of the external entity being referenced,
     *                 or null if none was supplied.
     * @param systemId The system identifier of the external entity being referenced.
     * @return An InputSource object describing the new input source, or null to request that
     *         the parser open a regular URI connection to the system identifier.
     * @throws SAXException Any SAX exception, possibly wrapping another exception.
     * @throws IOException A Java-specific IO exception, possibly the result of creating
     *                     a new InputStream or Reader for the InputSource.
     */
    public InputSource resolveEntity(String publicId, String systemId)
        throws IOException, SAXException {

        if (jDigester != null) {
            jDigester.setPublicId(publicId);
        }

        return super.resolveEntity(publicId, systemId);
    }
}

