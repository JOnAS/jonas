/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.common.lib;

import org.w3c.dom.ls.LSInput;
import org.w3c.dom.ls.LSResourceResolver;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;

/**
 * A custom ResourceResolver to use in SchemaFactory. It uses the JEntityResolver
 * to resolve system IDs for DTDs and Schemas to local copies of those files.
 * 
 * @author Patrick Smith
 * @author Greg Lapouchnian
 */
public class JLSResourceResolver implements LSResourceResolver {
    
        /**
         * The entity resolver to use for lookups.
         */
        EntityResolver er;
    
        /**
         * 
         * @param er the EntityResolver to use for resolving external
         * resources
         */
        public JLSResourceResolver(EntityResolver er) {
            this.er = er;
        }
        
        /**
         * Uses the EntityResolver to resolve an external resource. 
         */
        public LSInput resolveResource(String type, String namespaceURI,
                                       String publicId, String systemId,
                                       String baseURI) {
            try {
                InputSource source = er.resolveEntity(publicId, systemId);
                return new JLSInput(source);

            } catch (Exception e) {
                return null;
            }
        }
    }

