/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.common.xml;

/**
 * This class defines the implementation of the element jonas-security
 * @author Florent Benoit
 */
public class JonasSecurity extends AbsElement {

    private static final long serialVersionUID = 4560424191873577086L;
    /**
     * security-role-mapping
     */
    private JLinkedList securityRoleMappingList = null;

    /**
     * Constructor
     */
    public JonasSecurity() {
        super();
        securityRoleMappingList = new JLinkedList("security-role-mapping");
    }


    /**
     * Gets the security-role-mapping
     * @return the security-role-mapping
     */
    public JLinkedList getSecurityRoleMappingList() {
        return securityRoleMappingList;
    }

    /**
     * Add a new security-role-mapping element to this object
     * @param securityRoleMapping the securityRoleMapping object
     */
    public void addSecurityRoleMapping(SecurityRoleMapping securityRoleMapping) {
        securityRoleMappingList.add(securityRoleMapping);
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<jonas-security>\n");
        indent += 2;

        // security-role-mapping
        sb.append(securityRoleMappingList.toXML(indent));

        indent -= 2;
        sb.append(indent(indent));
        sb.append("</jonas-security>\n");

        return sb.toString();
    }

}