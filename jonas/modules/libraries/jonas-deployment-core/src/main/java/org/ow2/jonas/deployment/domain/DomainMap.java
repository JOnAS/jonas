/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.domain;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;

import org.ow2.jonas.deployment.common.AbsDeploymentDesc;
import org.ow2.jonas.deployment.domain.xml.Cluster;
import org.ow2.jonas.deployment.domain.xml.ClusterDaemon;
import org.ow2.jonas.deployment.domain.xml.Domain;
import org.ow2.jonas.deployment.domain.xml.Server;


/**
 * This class provides the map of the domain as described by the
 * domain description file.
 * It extends the AbsDeploymentDesc which requires to implement toString()
 * and provides getSAXMsg() method (displayName is not used by DomainMap).
 * @author Adriana Danes
 * @author S. Ali Tokmen
 */
public class DomainMap extends AbsDeploymentDesc {

    /**
     * the domain description
     */
    private String description = null;

    /**
     * Default user name of the domain
     */
    private String username = null;

    /**
     * Default password of the domain, may be encoded
     */
    private String password = null;

    /**
     * the clusterDaemons in the domain
     */
    private Vector clusterDaemons = null;

    /**
     * the clusters in the domain
     */
    private Vector clusters = null;

    /**
     * the servers in the domain
     */
    private Vector servers = null;

    /**
     * @return Returns the clusterDaemons.
     */
    public Vector getClusterDaemons() {
        return clusterDaemons;
    }

    /**
     * @param clusters The clusterDaemons to set.
     */
    public void setClusterDaemons(final Vector clusterDaemons) {
        this.clusterDaemons = clusterDaemons;
    }

    /**
     * @return Returns the clusters.
     */
    public Vector getClusters() {
        return clusters;
    }

    /**
     * @param clusters The clusters to set.
     */
    public void setClusters(final Vector clusters) {
        this.clusters = clusters;
    }

    /**
     * @return Returns the description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description to set.
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * @return Returns the default username of the domain.
     */
    public final String getUsername() {
        return username;
    }

    /**
     * @param username The default username of the domain to set.
     */
    public final void setUsername(final String username) {
        this.username = username;
    }

    /**
     * @return Returns the default password of the domain
     *         as encoded in the XML file.
     */
    public final String getPassword() {
        return password;
    }

    /**
     * @param password The default password of the domain to set
     *                 as encoded in the XML file.
     */
    public final void setPassword(final String password) {
        this.password = password;
    }

    /**
     * @return Returns the servers.
     */
    public Vector getServers() {
        return servers;
    }

    /**
     * @param servers The servers to set.
     */
    public void setServers(final Vector servers) {
        this.servers = servers;
    }

    /**
     * Constructor
     * @param domain domain
     */
    public DomainMap(final Domain domain) {
        description = domain.getDescription();
        username = domain.getUsername();
        password = domain.getPassword();
        clusterDaemons = new Vector();
        clusters = new Vector();
        servers = new Vector();

        for (Iterator i = domain.getClusterDaemonList().iterator(); i.hasNext();) {
            ClusterDaemon clusterDaemon = (ClusterDaemon) i.next();
            clusterDaemons.add(clusterDaemon);
        }

        for (Iterator i = domain.getClusterList().iterator(); i.hasNext();) {
            Cluster cluster = (Cluster) i.next();
            clusters.add(cluster);
        }

        for (Iterator i = domain.getServerList().iterator(); i.hasNext();) {
            Server server = (Server) i.next();
            servers.add(server);
        }

    }

    /**
     * Return a String representation of the DomainMap
     * @return a String representation of the DomainMap
     */
    @Override
    public String toString() {

        StringBuffer ret = new StringBuffer();
        ret.append("\ndescription=" + description);

        ret.append("\nclusterDaemons=");
        for (Enumeration e = clusterDaemons.elements(); e.hasMoreElements();) {
            ret.append(e.nextElement() + ",");
        }

        ret.append("\nservers=");
        for (Enumeration e = servers.elements(); e.hasMoreElements();) {
            ret.append(e.nextElement() + ",");
        }
        ret.append("\nclusters=");
        for (Enumeration e = clusters.elements(); e.hasMoreElements();) {
            ret.append(e.nextElement() + ",");
        }

        return ret.toString();
    }
}
