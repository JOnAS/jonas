/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.common.util;

/**
 * Resource name helper.
 * @author Guillaume Sauthier
 */
public final class ResourceHelper {

    /**
     * Default private constructor for utility class.
     */
    private ResourceHelper() { }

    /**
     * @param clazz Class located in the package.
     * @return the class package name that can be used to load resources (. replaced by /).
     */
    public static String getResourcePackage(final Class<?> clazz) {
        String packageName = clazz.getPackage().getName();
        return convertPackageName(packageName);
    }

    /**
     * @param packageName a java package name.
     * @return the package name that can be used to load resources (. replaced by /).
     */
    public static String convertPackageName(final String packageName) {
        return packageName.replace('.', '/').concat("/");
    }
}
