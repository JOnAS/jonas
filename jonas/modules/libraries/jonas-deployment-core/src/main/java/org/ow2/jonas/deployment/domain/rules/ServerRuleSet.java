/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.domain.rules;

import org.ow2.jonas.deployment.common.rules.JRuleSetBase;

import org.apache.commons.digester.Digester;

/**
 * This class defines the rules to analyze the server element
 *
 * @author Adriana Danes
 * @author S. Ali Tokmen
 */

public class ServerRuleSet extends JRuleSetBase {

    /**
     * Construct an object with the prefix "application"
     */
    public ServerRuleSet(String prefix) {
        super(prefix);
    }

    /**
     * Add a set of rules to the digester object
     * @param digester Digester instance
     */
    public void addRuleInstances(Digester digester) {
        digester.addObjectCreate(prefix + "server",
                "org.ow2.jonas.deployment.domain.xml.Server");
        digester.addSetNext(prefix + "server",
                "addServer",
                "org.ow2.jonas.deployment.domain.xml.Server");
        digester.addCallMethod(prefix + "server/name", "setName", 0);
        digester.addCallMethod(prefix + "server/password", "setPassword", 0);
        digester.addCallMethod(prefix + "server/username", "setUsername", 0);
        digester.addCallMethod(prefix + "server/description", "setDescription", 0);
        digester.addCallMethod(prefix + "server/cluster-daemon", "setClusterDaemon", 0);
        digester.addRuleSet(new LocationRuleSet(prefix + "server/"));
    }

}
