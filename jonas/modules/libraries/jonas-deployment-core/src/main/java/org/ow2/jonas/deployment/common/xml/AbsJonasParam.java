/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.common.xml;

/**
 * This class defines the implementation of the element jonas-param
 * @author Guillaume Sauthier
 */
public abstract class AbsJonasParam extends AbsElement {

    /**
     * Name of this parameter
     */
    private String paramName = null;

    /**
     * Value of this parameter
     */
    private String paramValue = null;

    /**
     * Element name
     */
    private String elementName = null;

    /**
     * Construct a JOnASParam with given Element name.
     * @param ename Element name
     */
    protected AbsJonasParam(String ename) {
        elementName = ename;
    }

    // Setters

    /**
     * Sets the name
     * @param paramName the name to use
     */
    public void setParamName(String paramName) {
        this.paramName = paramName;
    }


    /**
     * Sets the value
     * @param paramValue the value
     */
    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    // Getters


    /**
     * @return the name of the parameter
     */
    public String getParamName() {
        return paramName;
    }


    /**
     * @return the value of the parameter
     */
    public String getParamValue() {
        return paramValue;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<" + elementName + ">\n");

        indent += 2;

        // name
        sb.append(xmlElement(paramName, "param-name", indent));

        // value
        sb.append(xmlElement(paramValue, "param-value", indent));

        indent -= 2;
        sb.append(indent(indent));
        sb.append("</" + elementName + ">\n");

        return sb.toString();
    }



}
