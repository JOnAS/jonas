/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.common.rules;

import org.apache.commons.digester.Digester;

/**
 * This class defines a rule to analyze param-valueType
 * @author Guillaume Sauthier
 */
public class JonasCustomParamRuleSet extends JRuleSetBase {

    /**
     * paramvalueType element name
     */
    private String elementName;

    /**
     * xml element classname element name
     */
    private String classname;

    /**
     * Construct an object with a specific prefix
     * @param prefix prefix to use during the parsing
     * @param ename element name to use
     * @param classname element classname to use
     */
    public JonasCustomParamRuleSet(String prefix, String ename, String classname) {
        super(prefix);
        elementName = ename;
        this.classname = classname;
    }

    /**
     * Add a set of rules to the digester object
     * @param digester Digester instance
     */
    public void addRuleInstances(Digester digester) {
        digester.addObjectCreate(prefix + elementName,
                                 "org.ow2.jonas.deployment.common.xml." + classname);
        digester.addSetNext(prefix + elementName,
                            "add" + classname,
                            "org.ow2.jonas.deployment.common.xml." + classname);
        digester.addCallMethod(prefix + elementName + "/param-name",
                               "setParamName", 0);
        digester.addCallMethod(prefix + elementName + "/param-value",
                               "setParamValue", 0);
    }


}
