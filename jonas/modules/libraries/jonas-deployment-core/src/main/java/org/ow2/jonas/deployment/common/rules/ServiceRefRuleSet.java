/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.common.rules;

import org.apache.commons.digester.Digester;

/**
 * This class defines a rule to analyze element service-ref
 * @author Florent Benoit
 */
public class ServiceRefRuleSet extends JRuleSetBase {


    /**
     * Construct an object with a specific prefix
     * @param prefix prefix to use during the parsing
     */
    public ServiceRefRuleSet(String prefix) {
        super(prefix);
    }


    /**
     * Add a set of rules to the digester object
     * @param digester Digester instance
     */
    public void addRuleInstances(Digester digester) {
        digester.addObjectCreate(prefix + "service-ref",
                                 "org.ow2.jonas.deployment.common.xml.ServiceRef");

        digester.addSetNext(prefix + "service-ref",
                            "addServiceRef",
                            "org.ow2.jonas.deployment.common.xml.ServiceRef");

        digester.addCallMethod(prefix + "service-ref/service-ref-name",
                               "setServiceRefName", 0);


        digester.addCallMethod(prefix + "service-ref/service-interface",
                               "setServiceInterface", 0);

        digester.addCallMethod(prefix + "service-ref/wsdl-file",
                               "setWsdlFile", 0);

        digester.addCallMethod(prefix + "service-ref/jaxrpc-mapping-file",
                               "setJaxrpcMappingFile", 0);


        digester.addRuleSet(new ServiceQnameRuleSet(prefix + "service-ref/"));
        digester.addRuleSet(new PortComponentRefRuleSet(prefix + "service-ref/"));
        digester.addRuleSet(new HandlerRuleSet(prefix + "service-ref/"));
    }

    /**
     * @return the namespaceURI that is relevant for this RuleSet.
     */
    @Override
    public String getNamespaceURI() {
        return "http://java.sun.com/xml/ns/j2ee";
    }
}
