/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.common.xml;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * This class defines a linked list with the ability to display its content in XML.
 * @author Florent Benoit
 */
public class JLinkedList extends LinkedList {


    /**
     * serial ID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Tag of the root element.
     */
    private String tag = null;


    /**
     * Constructor with a specified tag
     * @param tag of the root element (use for xml representation)
     */
    public JLinkedList(final String tag) {
        super();
        this.tag = tag;
    }


    /**
     * Represents this element by it's XML description.
     * Use a default indent set to 0.
     * @return the XML description of this object.
     */
    public String toXML() {
        return toXML(0);
    }

    /**
     * Return the representation of this element.
     * Use the XML representation of the object for the toString() method.
     * @return the XML description of this object.
     */
    public String toString() {
        return toXML();
    }


    /**
     * Return indent spaces.
     * @param indent number of indentation.
     * @return the indent space string.
     */
    private String indent(final int indent) {
        String txt = "";
        for (int i = 0; i < indent; i++) {
            txt += " ";
        }
        return txt;
    }


    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    @SuppressWarnings("unchecked")
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        // Only if there are elements
        if (this.size() > 0) {
            for (Iterator i = this.iterator(); i.hasNext();) {
                Object o = i.next();
                // Element or String ?
                if (o instanceof Element) {
                    sb.append(((Element) o).toXML(indent));
                } else {
                    sb.append(indent(indent));
                    sb.append("<");
                    sb.append(tag);
                    sb.append(">");
                    sb.append(o);
                    sb.append("</");
                    sb.append(tag);
                    sb.append(">\n");
                }
            }
        }
        return sb.toString();
    }




}
