/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: JOnAS team
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.common.xml;

/**
 * This class defines the implementation of the element security-role-ref
 *
 * @author JOnAS team
 */

public class SecurityRoleRef extends AbsElement  {

    /**
     * description
     */
    private String description = null;

    /**
     * role-name
     */
    private String roleName = null;

    /**
     * role-link
     */
    private String roleLink = null;


    /**
     * Constructor
     */
    public SecurityRoleRef() {
        super();
    }

    /**
     * Gets the description
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the description
     * @param description description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the role-name
     * @return the role-name
     */
    public String getRoleName() {
        return roleName;
    }

    /**
     * Set the role-name
     * @param roleName roleName
     */
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    /**
     * Gets the role-link
     * @return the role-link
     */
    public String getRoleLink() {
        return roleLink;
    }

    /**
     * Set the role-link
     * @param roleLink roleLink
     */
    public void setRoleLink(String roleLink) {
        this.roleLink = roleLink;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<security-role-ref>\n");

        indent += 2;

        // description
        sb.append(xmlElement(description, "description", indent));
        // role-name
        sb.append(xmlElement(roleName, "role-name", indent));
        // role-link
        sb.append(xmlElement(roleLink, "role-link", indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</security-role-ref>\n");

        return sb.toString();
    }
}
