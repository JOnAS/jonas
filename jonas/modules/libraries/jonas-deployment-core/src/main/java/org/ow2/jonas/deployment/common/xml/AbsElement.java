/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.common.xml;

/**
 * This class defines the implementation of the interface Element.
 * These elements are used by Digester during the xml parsing.
 * @author Florent Benoit
 */
public abstract class AbsElement implements Element {

    /**
     * Tenant identifier
     */
    private String tenantId = null;

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public abstract String toXML(int indent);


    /**
     * Represents this element by it's XML description.
     * Use a default indent set to 0.
     * @return the XML description of this object.
     */
    public String toXML() {
        return toXML(0);
    }

    /**
     * Return the representation of this element.
     * Use the XML representation of the object for the toString() method.
     * @return the XML description of this object.
     */
    public String toString() {
        return toXML();
    }


    /**
     * Return indent spaces.
     * @param indent number of indentation.
     * @return the indent space string.
     */
    protected String indent(int indent) {
        String txt = "";
        for (int i = 0; i < indent; i++) {
            txt += " ";
        }
        return txt;
    }

    /**
     * Return the xml representation of the specified value with the root-element xmlTag
     * @param value String value to represent in XML
     * @param xmlTag tag of the root-element
     * @param indent indent to use
     * @return xml representation of the specified value
     */
    protected String xmlElement(String value, String xmlTag, int indent) {
        if (value == null) {
            return "";
        }

        // else

        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<");
        sb.append(xmlTag);
        sb.append(">");
        sb.append(value);
        sb.append("</");
        sb.append(xmlTag);
        sb.append(">\n");
        return sb.toString();
    }

    /**
     * Return the xml representation of the specified attribute value
     * @param value String value to represent in XML
     * @param xmlTag tag of the attribute
     * @return xml representation of the specified value
     */
    protected String xmlAttribute(String value, String xmlTag) {
        if (value == null) {
            return "";
        }

        // else

        StringBuffer sb = new StringBuffer();
        sb.append(" ");
        sb.append(xmlTag);
        sb.append("=\"");
        sb.append(value);
        sb.append("\"");
        return sb.toString();
    }

    /**
     * Set the tenant identifier
     * @param tenantId tenant identifier
     */
    public void setTenantId(final String tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * @return the tenant identifier
     */
    public String getTenantId() {
        return tenantId;
    }

}
