/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.domain.rules;

import org.ow2.jonas.deployment.common.rules.JRuleSetBase;

import org.apache.commons.digester.Digester;

/**
 * This class defines the rules to analyze the cluster element.
 * A Cluster element may contain another cluster element.
 *
 * @author Adriana Danes
 */

public class ClusterRuleSet extends JRuleSetBase {

	private String CLUSTER = "cluster";

	/**
	 * If greater than 0 it gives the level of an inner cluster element
	 */
	int indentLevel = 0;

	/**
	 * Maximum sub-elements
	 */
	int maxLevel = 3;

    /**
     * Construct an object with the given prefix
     * @param prefix prefix to use during the parsing
     */
    public ClusterRuleSet(String prefix) {
        super(prefix);
        int previousIndex = 0;
        int index;
        while(true) {
        	index = prefix.indexOf(CLUSTER, previousIndex);
        	if (index < 0) {
        		break;
        	} else {
        		indentLevel++;
        		previousIndex = index + CLUSTER.length();
        	}
        }
    }

    /**
     * Add a set of rules to the digester object
     * @param digester Digester instance
     */
    public void addRuleInstances(Digester digester) {
        digester.addObjectCreate(prefix + "cluster",
                "org.ow2.jonas.deployment.domain.xml.Cluster");
        digester.addSetNext(prefix + "cluster",
                "addCluster",
                "org.ow2.jonas.deployment.domain.xml.Cluster");
        digester.addCallMethod(prefix + "cluster/name", "setName", 0);
        digester.addCallMethod(prefix + "cluster/description", "setDescription", 0);
        digester.addRuleSet(new ServerRuleSet(prefix + "cluster/"));
        if (indentLevel < maxLevel) {
        	digester.addRuleSet(new ClusterRuleSet(prefix + "cluster/"));
        }
    }

}
