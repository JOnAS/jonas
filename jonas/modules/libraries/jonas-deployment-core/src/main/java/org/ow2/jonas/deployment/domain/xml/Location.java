/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.domain.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;

/**
 *
 * @author Adriana Danes
 *
 * A location element is composed of a list of urls, where a url is
 * <url>the-string-representation-of-the-url</url>
 */
public class Location extends AbsElement  {

    private JLinkedList urlList = null;
    /**
     * Constructor
     */
    public Location() {
        super();
        urlList = new JLinkedList("url");
    }

    /**
     * @return Returns the urlList.
     */
    public JLinkedList getUrlList() {
        return urlList;
    }
/**
 * Add a url to the urlList.
 * @param url
 */
    public void addUrl(String url) {
        urlList.add(url);
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<location>\n");

        indent += 2;

        // principal-name
        sb.append(urlList.toXML(indent));

        indent -= 2;
        sb.append(indent(indent));
        sb.append("</location>\n");

        return sb.toString();
    }
}