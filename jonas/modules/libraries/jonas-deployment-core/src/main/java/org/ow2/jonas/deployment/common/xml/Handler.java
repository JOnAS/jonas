/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.common.xml;

/**
 * This class defines the implementation of the element handler.
 * @author Florent Benoit
 */
public class Handler extends AbsElement {

    /**
     * Name of the handler
     */
    private String handlerName = null;

    /**
     * List of elements init-param
     */
    private JLinkedList initParamList = null;

    /**
     * List of elements soap-header
     */
    private JLinkedList soapHeaderList = null;

    /**
     * List of elements soap-role
     */
    private JLinkedList soapRoleList = null;

    /**
     * List of elements port-name
     */
    private JLinkedList portNameList = null;


    /**
     * Class of the handler
     */
    private String handlerClass = null;

    /**
     * Constructor : build a new Handler object
     */
    public Handler() {
        super();
        initParamList = new JLinkedList("init-param");
        soapHeaderList = new JLinkedList("soap-header");
        soapRoleList = new JLinkedList("soap-role");
        portNameList = new JLinkedList("port-name");
    }


    // Setters

    /**
     * Sets the name
     * @param handlerName the name to use
     */
    public void setHandlerName(String handlerName) {
        this.handlerName = handlerName;
    }

    /**
     * Add a new port-name element to this object
     * @param portName the port-name object
     */
    public void addPortName(String portName) {
        portNameList.add(portName);
    }


    /**
     * Add a new soap-role element to this object
     * @param soapRole the soap-role object
     */
    public void addSoapRole(String soapRole) {
        soapRoleList.add(soapRole);
    }

    /**
     * Add a new soap-header element to this object
     * @param soapHeader the soap-header object
     */
    public void addSoapHeader(Qname soapHeader) {
        soapHeaderList.add(soapHeader);
    }

    /**
     * Add a new init-param element to this object
     * @param initParam the init-param object
     */
    public void addInitParam(InitParam initParam) {
        initParamList.add(initParam);
    }

    /**
     * Sets the class
     * @param handlerClass the class to use
     */
    public void setHandlerClass(String handlerClass) {
        this.handlerClass = handlerClass;
    }

    // Getters

    /**
     * @return the name of the handler
     */
    public String getHandlerName() {
        return handlerName;
    }

    /**
     * @return the class of the handler
     */
    public String getHandlerClass() {
        return handlerClass;
    }

    /**
     * @return the list of all init-param elements
     */
    public JLinkedList getInitParamList() {
        return initParamList;
    }

    /**
     * @return the list of all soap-header elements
     */
    public JLinkedList getSoapHeaderList() {
        return soapHeaderList;
    }

    /**
     * @return the list of all soap-role elements
     */
    public JLinkedList getSoapRoleList() {
        return soapRoleList;
    }

    /**
     * @return the list of all port-name elements
     */
    public JLinkedList getPortNameList() {
        return portNameList;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<handler>\n");

        indent += 2;

        // handler-name
        sb.append(xmlElement(handlerName, "handler-name", indent));

        // handler-class
        sb.append(xmlElement(handlerClass, "handler-class", indent));

        // init-param
        sb.append(initParamList.toXML(indent));

        // soap-header
        sb.append(soapHeaderList.toXML(indent));

        // soap-role
        sb.append(soapRoleList.toXML(indent));

        // port-name
        sb.append(portNameList.toXML(indent));

        indent -= 2;
        sb.append(indent(indent));
        sb.append("</handler>\n");

        return sb.toString();
    }


}
