/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.common.rules;

import javax.xml.namespace.QName;

import org.ow2.jonas.deployment.common.xml.Qname;



/**
 * Rule that create a new QName object for anonymous-type-qname in jaxrpc mapping file.
 * @author Guillaume Sauthier
 */
public class AnonymousQNameRule extends QNameRule {

    /**
     * This method is called when the body of a matching XML element is
     * encountered. If the element has no body, this method is not called at
     * all. The default implementation delegates to the deprecated method
     * body without the namespace and name parameters, to retain backwards
     * compatibility.
     * @param namespace the namespace URI of the matching element,
     *                  or an empty string if the parser is not namespace
     *                  aware or the element has no namespace
     * @param name the local name if the parser is namespace aware,
     *             or just the element name otherwise
     * @param text The text of the body of this element
     */
    public void body(String namespace, String name, String text) {

        // Check that there the value of the element is not null
        if (text == null) {
            throw new IllegalArgumentException("No QName found in the body of the tag " + name);
        }

        // Extract namespace and localpart
        int colonIndex = text.lastIndexOf(":");
        if (colonIndex == -1) {
            throw new IllegalArgumentException("QName must be on the form namespace:localpart for element " + name);
        }
        String namespaceURI = text.substring(0, colonIndex);
        String localPart = text.substring(colonIndex + 1, text.length());

        // Build QName
        QName qName = new QName(namespaceURI, localPart);

        // Add this QName to the element at the top of the stack
        Qname qNameObject = (Qname) digester.peek();
        qNameObject.setName(name);
        qNameObject.setQName(qName);

    }

}
