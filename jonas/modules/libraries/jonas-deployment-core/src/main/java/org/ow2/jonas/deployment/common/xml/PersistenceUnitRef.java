/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.common.xml;

/**
 * This class defines the implementation of the persistence-unit-ref XML element.
 * @author Adriana Danes
 */
public class PersistenceUnitRef extends AbsElement {
    /**
     * Description of the persistence-unit-ref element.
     */
    private String description = null;

    /**
     * Name of the persistence-unit-ref element.
     */
    private String persistenceUnitRefName = null;

    /**
     * Optional persistence unit name.
     */
    private String persistenceUnitName = null;

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getPersistenceUnitRefName() {
        return persistenceUnitRefName;
    }

    public void setPersistenceUnitRefName(final String persistenceUnitRefName) {
        this.persistenceUnitRefName = persistenceUnitRefName;
    }

    public String getPersistenceUnitName() {
        return persistenceUnitName;
    }

    public void setPersistenceUnitName(final String persistenceUnitName) {
        this.persistenceUnitName = persistenceUnitName;
    }
    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prefixing XML representation.
     * @return the XML description of this object.
     */
    @Override
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<persistence-unit-ref>\n");

        indent += 2;

        // Description
        sb.append(xmlElement(description, "description", indent));

        // peristence unit ref name
        sb.append(xmlElement(persistenceUnitRefName, "persistence-unit-ref-name", indent));

        // peristence unit name
        sb.append(xmlElement(persistenceUnitName, "persistence-unit-name", indent));

        indent -= 2;
        sb.append(indent(indent));
        sb.append("</persistence-unit-ref>\n");

        return sb.toString();
    }
}
