/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.common.xml;

/**
 * This class defines the implementation of the element service-ref.
 * @author Florent Benoit
 */
public class ServiceRef extends AbsElement {

    /**
     * Service name
     */
    private String serviceRefName = null;


    /**
     * Service Interface
     */
    private String serviceInterface = null;

    /**
     * Mapping jaxrpc
     */
    private String jaxrpcMappingFile = null;

    /**
     * WSDL file
     */
    private String wsdlFile = null;


    /**
     * Service-qname element
     */
    private Qname serviceQname = null;

    /**
     * List of elements port-component-ref
     */
    private JLinkedList portComponentRefList = null;


    /**
     * List of elements handler
     */
    private JLinkedList handlerList = null;


    /**
     * Constructor : build a new ServiceRef object
     */
    public ServiceRef() {
        super();
        portComponentRefList = new JLinkedList("port-component-ref");
        handlerList = new JLinkedList("handler");
    }


    // Setters

    /**
     * Add a new port-component-ref element to this object
     * @param portComponentRef the port-component-ref object
     */
    public void addPortComponentRef(PortComponentRef portComponentRef) {
        portComponentRefList.add(portComponentRef);
    }

    /**
     * Add a new handler element to this object
     * @param handler the handler object
     */
    public void addHandler(Handler handler) {
        handlerList.add(handler);
    }

    /**
     * Sets the service-ref-name of the service-ref
     * @param serviceRefName of the service-ref
     */
    public void setServiceRefName(String serviceRefName) {
        this.serviceRefName = serviceRefName;
    }


    /**
     * Sets the service-interface of the service-ref
     * @param serviceInterface name of the service-ref
     */
    public void setServiceInterface(String serviceInterface) {
        this.serviceInterface = serviceInterface;
    }


    /**
     * Sets the jaxrpc-mapping-file element of the service-ref
     * @param jaxrpcMappingFile jaxrpc-mapping-file of the service-ref
     */
    public void setJaxrpcMappingFile(String jaxrpcMappingFile) {
        this.jaxrpcMappingFile = jaxrpcMappingFile;
    }


    /**
     * Sets the wsdl-file element of the service-ref
     * @param wsdlFile name of the service-ref
     */
    public void setWsdlFile(String wsdlFile) {
        this.wsdlFile = wsdlFile;
    }


    /**
     * Sets the service-qname of the service-ref
     * @param serviceQname of the service-ref
     */
    public void setServiceQname(Qname serviceQname) {
        this.serviceQname = serviceQname;
    }

    // Getters

    /**
     * @return the service-ref-name of the service-ref
     */
    public String getServiceRefName() {
        return serviceRefName;
    }

    /**
     * @return the service-interface of the service-ref
     */
    public String getServiceInterface() {
        return serviceInterface;
    }

    /**
     * @return the Jaxrpc-mapping-file of the service-ref
     */
    public String getJaxrpcMappingFile() {
        return jaxrpcMappingFile;
    }

    /**
     * @return the wsdl-file of the service-ref
     */
    public String getWsdlFile() {
        return wsdlFile;
    }


    /**
     * @return the service-qname element
     */
    public Qname getServiceQname() {
        return serviceQname;
    }

    /**
     * @return the list of all handler elements
     */
    public JLinkedList getHandlerList() {
        return handlerList;
    }

    /**
     * @return the list of all port-component-ref elements
     */
    public JLinkedList getPortComponentRefList() {
        return portComponentRefList;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<service-ref>\n");

        indent += 2;

        // service-ref-name
        sb.append(xmlElement(serviceRefName, "service-ref-name", indent));

        // service-interface
        sb.append(xmlElement(serviceInterface, "service-interface", indent));

        // wsdl-file
        sb.append(xmlElement(wsdlFile, "wsdl-file", indent));

        // jaxrpc-mapping-file
        sb.append(xmlElement(jaxrpcMappingFile, "jaxrpc-mapping-file", indent));

        //service-qname
        if (serviceQname != null) {
            sb.append(serviceQname.toXML(indent));
        }

        // port-component-ref
        sb.append(portComponentRefList.toXML(indent));

        // handler
        sb.append(handlerList.toXML(indent));

        indent -= 2;
        sb.append(indent(indent));
        sb.append("</service-ref>\n");

        return sb.toString();
    }


}
