/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.common.rules;

import org.apache.commons.digester.Digester;
import org.ow2.jonas.deployment.common.digester.NoTrimCallMethodRule;

/**
 * This class defines a rule to analyze environment entries
 * @author Florent Benoit
 */
public class EnvEntryRuleSet extends JRuleSetBase {

    /**
     * Construct an object with a specific prefix
     * @param prefix prefix to use during the parsing
     */
    public EnvEntryRuleSet(String prefix) {
        super(prefix);
    }

    /**
     * Add a set of rules to the digester object
     * @param digester Digester instance
     */
    public void addRuleInstances(Digester digester) {
        digester.addObjectCreate(prefix + "env-entry",
                                 "org.ow2.jonas.deployment.common.xml.EnvEntry");

        digester.addSetNext(prefix + "env-entry",
                            "addEnvEntry",
                            "org.ow2.jonas.deployment.common.xml.EnvEntry");
        digester.addCallMethod(prefix + "env-entry/description",
                               "setDescription", 0);
        digester.addCallMethod(prefix + "env-entry/env-entry-name",
                               "setEnvEntryName", 0);
        
        digester.addRule(prefix + "env-entry/env-entry-value", new NoTrimCallMethodRule("setEnvEntryValue", 0));
        digester.addCallMethod(prefix + "env-entry/lookup-name",
                "setEnvEntryLookupName", 0);

        digester.addCallMethod(prefix + "env-entry/env-entry-type",
                               "setEnvEntryType", 0);

    }


}
