/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.common.xml;

/**
 * This class defines the implementation of the element resource-ref.
 * @author Florent Benoit
 */
public class ResourceRef extends AbsElement {

    /**
     * Description of the resource-ref
     */
    private String description = null;

    /**
     * Name of this resource-ref
     */
    private String resRefName = null;

    /**
     * Type of this resource-ref
     */
    private String resType = null;

    /**
     * Auth of this resource-ref
     */
    private String resAuth = null;

    /**
     * Sharing-scope of this resource-ref
     */
    private String resSharingScope = null;



    // Setters

    /**
     * Sets the description
     * @param description the description to use
     */
    public void setDescription(String description) {
        this.description = description;
    }


    /**
     * Sets the name
     * @param resRefName the name to use
     */
    public void setResRefName(String resRefName) {
        this.resRefName = resRefName;
    }


    /**
     * Sets the type
     * @param resType the type to use
     */
    public void setResType(String resType) {
        this.resType = resType;
    }


    /**
     * Sets the auth
     * @param resAuth the auth  to use
     */
    public void setResAuth(String resAuth) {
        this.resAuth = resAuth;
    }


    /**
     * Sets the sharing-scope
     * @param resSharingScope the sharing-scope to use
     */
    public void setResSharingScope(String resSharingScope) {
        this.resSharingScope = resSharingScope;
    }



    // Getters

    /**
     * @return the description of the resource-ref
     */
    public String getDescription() {
        return description;
    }


    /**
     * @return the name of the resource-ref
     */
    public String getResRefName() {
        return resRefName;
    }


    /**
     * @return the type of the resource-ref
     */
    public String getResType() {
        return resType;
    }


    /**
     * @return the auth of the resource-ref
     */
    public String getResAuth() {
        return resAuth;
    }


    /**
     * @return the sharing-scope of the resource-ref
     */
    public String getResSharingScope() {
        return resSharingScope;
    }


    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<resource-ref>\n");

        indent += 2;

        // Description
        sb.append(xmlElement(description, "description", indent));

        // name
        sb.append(xmlElement(resRefName, "res-ref-name", indent));

        // type
        sb.append(xmlElement(resType, "res-type", indent));

        // auth
        sb.append(xmlElement(resAuth, "res-auth", indent));

        // sharing-scope
        sb.append(xmlElement(resSharingScope, "res-sharing-scope", indent));

        indent -= 2;
        sb.append(indent(indent));
        sb.append("</resource-ref>\n");

        return sb.toString();
    }



}
