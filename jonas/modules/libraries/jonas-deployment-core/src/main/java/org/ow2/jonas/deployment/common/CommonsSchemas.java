/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.common;


import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.ow2.jonas.deployment.api.Schemas;


/**
 * This class defines the declarations of the default Java EE 5 Schemas.
 * @author Florent Benoit
 */
public abstract class CommonsSchemas implements Schemas {

    /**
     * Commons XML Schemas location.
     */
    private static final String PACKAGE = "org/ow2/jonas/deployment/";

    /**
     * List of schemas used by components.
     */
    private static final String[] DEFAULT_SCHEMAS = new String[] {
        PACKAGE + "javaee_6.xsd",
        PACKAGE + "javaee_web_services_client_1_3.xsd",
        PACKAGE + "javaee_5.xsd",
        PACKAGE + "javaee_web_services_client_1_2.xsd",
        PACKAGE + "j2ee_1_4.xsd",
        PACKAGE + "j2ee_web_services_client_1_1.xsd",
        PACKAGE + "xml.xsd",
        PACKAGE + "jonas_j2ee_4_0.xsd",
        PACKAGE + "jonas_j2ee_4_1.xsd",
        PACKAGE + "jonas_j2ee_4_1_2.xsd",
        PACKAGE + "jonas_j2ee_4_1_4.xsd",
        PACKAGE + "jonas_j2ee_4_2.xsd"
    };


    /**
     * List where the local schemas URLs are stored.
     */
    private static List<String> localSchemas = null;


    /**
     * Build a new object for Schemas handling.
     */
    public CommonsSchemas() {
        localSchemas = new ArrayList<String>();
        addSchemas(DEFAULT_SCHEMAS, CommonsSchemas.class.getClassLoader());
    }

    /**
     * Gets the URLs of the local schemas.
     * @return the URLs of the local schemas
     */
    public List<String> getlocalSchemas() {
        return localSchemas;
    }


    /**
     * Add to our repository the given local schemas.
     * @param schemas schemas to add to the repository
     * @throws IllegalStateException if the dtds is not found as resource
     */
    protected static void addSchemas(final String[] schemas, ClassLoader loader) throws IllegalStateException {
        URL url = null;
        for (int i = 0; i <  schemas.length; i++) {
            url = loader.getResource(schemas[i]);
            if (url == null) {
                throw new IllegalStateException("'" +  schemas[i] + "' was not found in the current classloader !");
            }
            localSchemas.add(url.toString());
        }
    }

    /**
     * @param element name of the root element (jonas-ejb-jar, ...)
     * @param schema XML Schema
     * @return a header for the right element with last element
     */
    public static String getHeaderForElement(final String element, final String schema) {
        StringBuffer header = new StringBuffer();
        header.append("<?xml version=\"1.0\"?>\n");
        header.append("<");
        header.append(element);
        header.append(" xmlns=\"http://www.objectweb.org/jonas/ns\"\n");
        header.append("               xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n");
        header.append("               xsi:schemaLocation=\"http://www.objectweb.org/jonas/ns\n");
        header.append("                                   http://jonas.ow2.org/ns/");
        // add the schema
        header.append(schema);
        header.append("\" >\n");

        return header.toString();
    }

    /**
     * @param element name of the root element (jonas-ejb-jar, ...)
     * @param schema XML Schema
     * @return a header for the right element with last element
     */
    public static String getHeaderForStandardElement(final String element, final String schema) {
        StringBuffer header = new StringBuffer();
        header.append("<?xml version=\"1.0\"?>\n");
        header.append("<");
        header.append(element);
        header.append(" xmlns=\"http://java.sun.com/xml/ns/javaee\"\n");
        header.append("             xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n");
        header.append("             version=\"5\"\n");
        header.append("             xsi:schemaLocation=\"http://java.sun.com/xml/ns/javaee\n");
        header.append("                                 http://java.sun.com/xml/ns/javaee/");
        // add the schema
        header.append(schema);
        header.append("\" >\n");

        return header.toString();
    }

    /**
     * @param schemas array of schema locations
     * @param packageName package name to remove from schema location.
     * @return Returns the last Schema for a given set of schemas.
     */
    protected static String getLastSchema(final String[] schemas, final String packageName) {
        String schema = schemas[schemas.length - 1];
        return schema.substring(packageName.length());
    }

}
