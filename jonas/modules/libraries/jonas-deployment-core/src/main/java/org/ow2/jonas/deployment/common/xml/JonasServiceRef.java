/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.common.xml;

/**
 * This class defines the implementation of the element jonas-service-ref.
 * @author Florent Benoit
 */
public class JonasServiceRef extends AbsElement {

    /**
     * Name of this jonas-service-ref
     */
    private String serviceRefName = null;

    /**
     * URL pointing to an alternate WSDL Definition
     */
    private String altWsdl = null;

    /**
     * List of init parameters
     */
    private JLinkedList jonasInitParamList = null;

    /**
     * List of jonas-port-component-ref
     */
    private JLinkedList jonasPortComponentRefList = null;

    /**
     * Constructor : build a new JonasServiceRef object
     */
    public JonasServiceRef() {
        jonasInitParamList = new JLinkedList("jonas-init-param");
        jonasPortComponentRefList = new JLinkedList("jonas-port-component-ref");
    }



    // Setters

    /**
     * Sets the name
     * @param serviceRefName the name to use
     */
    public void setServiceRefName(String serviceRefName) {
        this.serviceRefName = serviceRefName;
    }


    /**
     * Add a parameter
     * @param jonasInitParam the JonasInitParam object to add to our list
     */
    public void addJonasInitParam(JonasInitParam jonasInitParam) {
        jonasInitParamList.add(jonasInitParam);
    }

    /**
     * Add a parameter
     * @param jonasPortComponentRef the JonasPortComponentRef object to add to our list
     */
    public void addJonasPortComponentRef(JonasPortComponentRef jonasPortComponentRef) {
        jonasPortComponentRefList.add(jonasPortComponentRef);
    }

    /**
     * @param altWsdl The altWsdl to set.
     */
    public void setAltWsdl(String altWsdl) {
        this.altWsdl = altWsdl;
    }

    // Getters

    /**
     * @return the name of the service-ref
     */
    public String getServiceRefName() {
        return serviceRefName;
    }


    /**
     * @return the list of init parameters
     */
    public JLinkedList getJonasInitParamList() {
        return jonasInitParamList;
    }

    /**
     * @return the list of jonas port component ref
     */
    public JLinkedList getJonasPortComponentRefList() {
        return jonasPortComponentRefList;
    }

    /**
     * @return Returns the altWsdl.
     */
    public String getAltWsdl() {
        return altWsdl;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<jonas-service-ref>\n");

        indent += 2;

        // name
        sb.append(xmlElement(serviceRefName, "service-ref-name", indent));

        // jonas port component ref
        sb.append(jonasPortComponentRefList.toXML(indent));

        // alt-wsdl
        sb.append(xmlElement(altWsdl, "alt-wsdl", indent));

        // init parameters
        sb.append(jonasInitParamList.toXML(indent));

        indent -= 2;
        sb.append(indent(indent));
        sb.append("</jonas-service-ref>\n");
        return sb.toString();
    }


}
