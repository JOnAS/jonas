/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.common.xml;


/**
 * This class defines the implementation of the element port-component-ref.
 * @author Florent Benoit
 */
public class PortComponentRef extends AbsElement {

    /**
     * service endpoint interface
     */
    private String serviceEndpointInterface = null;

    /**
     * port component link
     */
    private String portComponentLink = null;


    // Setters


    /**
     * Sets the service endpoint interface of the port-component-ref
     * @param serviceEndpointInterface service endpoint interface of the port-component-ref
     */
    public void setServiceEndpointInterface(String serviceEndpointInterface) {
        this.serviceEndpointInterface = serviceEndpointInterface;
    }


    /**
     * Sets the port component link of the port-component-ref
     * @param portComponentLink port component link of the port-component-ref
     */
    public void setPortComponentLink(String portComponentLink) {
        this.portComponentLink = portComponentLink;
    }


    // Getters

    /**
     * @return the service endpoint interface of the port-component-ref
     */
    public String getServiceEndpointInterface() {
        return serviceEndpointInterface;
    }

    /**
     * @return the port component link of the port-component-ref
     */
    public String getPortComponentLink() {
        return portComponentLink;
    }


    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<port-component-ref>\n");

        indent += 2;

        // service-endpoint-interface
        sb.append(xmlElement(serviceEndpointInterface, "service-endpoint-interface", indent));

        // port-component-link
        sb.append(xmlElement(portComponentLink, "port-component-link", indent));


        indent -= 2;
        sb.append(indent(indent));
        sb.append("</port-component-ref>\n");

        return sb.toString();
    }
}
