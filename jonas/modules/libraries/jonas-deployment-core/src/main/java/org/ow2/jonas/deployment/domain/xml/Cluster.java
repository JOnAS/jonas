/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.domain.xml;

import org.ow2.jonas.deployment.common.xml.AbsElement;
import org.ow2.jonas.deployment.common.xml.JLinkedList;

public class Cluster extends AbsElement  {

    private String name = null;

    /**
     * description
     */
    private String description = null;
    /**
     * servers in the cluster
     */
    private JLinkedList serverList = null;

    /**
     * sub-clusters
     */
    private JLinkedList clusterList = null;
    /**
     * Constructor
     */
    public Cluster() {
        super();
        serverList = new  JLinkedList("server");
        clusterList = new  JLinkedList("cluster");
    }


    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name to set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return Returns the description.
     */
    public String getDescription() {
        return description;
    }


    /**
     * @param description The description to set.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Add a new sub-cluster (cluster element) to this object
     * @param cluster the Cluster object representing a sub-cluster
     */
    public void addCluster(Cluster cluster) {
        clusterList.add(cluster);
    }
    /**
     * Add a new server element to this object
     * @param server the Server object
     */
    public void addServer(Server server) {
        serverList.add(server);
    }

    /**
     * @return Returns the clusterList.
     */
    public JLinkedList getClusterList() {
        return clusterList;
    }
    /**
     * @param clusterList The clusterList to set.
     */
    public void setClusterList(JLinkedList clusterList) {
        this.clusterList = clusterList;
    }
    /**
     * @return Returns the serverList.
     */
    public JLinkedList getServerList() {
        return serverList;
    }

    /**
     * @param serverList The serverList to set.
     */
    public void setServerList(JLinkedList serverList) {
        this.serverList = serverList;
    }

    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<cluster>\n");

        indent += 2;

        // cluster name
        if (name != null) {
            sb.append(xmlElement(name, "name", indent));
        }
        // description
        if (getDescription() != null) {
            sb.append(xmlElement(getDescription(), "description", indent));
        }
        // servers
        sb.append(getServerList().toXML(indent));
        // clusters
        sb.append(getClusterList().toXML(indent));
        indent -= 2;
        sb.append(indent(indent));
        sb.append("</cluster>\n");

        return sb.toString();
    }
}