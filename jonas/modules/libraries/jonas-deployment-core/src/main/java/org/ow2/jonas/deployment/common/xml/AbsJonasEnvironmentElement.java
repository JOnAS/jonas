/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.common.xml;

/**
 * This class defines an abstract implementation for all jonas environment element
 * (jonas-entity, jonas-session, jonas-web-app, jonas-client, etc.).
 *
 * @author Florent Benoit
 */
public abstract class AbsJonasEnvironmentElement extends AbsElement implements JonasJndiEnvRefsGroupXml {

    /**
     * List of jonas-ejb-ref
     */
    private JLinkedList jonasEjbRefList = null;

    /**
     * List of jonas-resource-env
     */
    private JLinkedList jonasResourceEnvRefList = null;

    /**
     * List of jonas-resource
     */
    private JLinkedList jonasResourceRefList = null;


    /**
     * List of jonas-service-ref
     */
    private JLinkedList jonasServiceRefList = null;

    /**
     * List of jonas-message-destination-ref
     */
    private JLinkedList jonasMessageDestinationRefList = null;

    /**
     * Constructor : build a new object which is common to environment elements
     */
    public AbsJonasEnvironmentElement() {
        super();
        jonasEjbRefList = new JLinkedList("jonas-ejb-ref");
        jonasResourceEnvRefList = new JLinkedList("jonas-resource-env");
        jonasResourceRefList = new JLinkedList("jonas-resource");
        jonasServiceRefList = new JLinkedList("jonas-service-ref");
        jonasMessageDestinationRefList = new JLinkedList("jonas-message-destination-ref");
    }



    // Setters

    /**
     * Add a new jonas-ejb-ref element to this object
     * @param jonasEjbRef the jonas-ejb-ref object
     */
    public void addJonasEjbRef(JonasEjbRef jonasEjbRef) {
        jonasEjbRefList.add(jonasEjbRef);
    }


    /**
     * Add a new jonas-resource-env element to this object
     * @param jonasResourceEnv the jonas-resource-env object
     */
    public void addJonasResourceEnv(JonasResourceEnv jonasResourceEnv) {
        jonasResourceEnvRefList.add(jonasResourceEnv);
    }

    /**
     * Add a new jonas-resource element to this object
     * @param jonasResource the jonas-resource object
     */
    public void addJonasResource(JonasResource jonasResource) {
        jonasResourceRefList.add(jonasResource);
    }

    /**
     * Add a new jonas-service-ref element to this object
     * @param jonasServiceRef the jonas-service-ref object
     */
    public void addJonasServiceRef(JonasServiceRef jonasServiceRef) {
        jonasServiceRefList.add(jonasServiceRef);
    }


    /**
     * Add a new jonas-message-destination-ref element to this object
     * @param jonasMessageDestinationRef the jonas-message-destination-ref object
     */
    public void addJonasMessageDestinationRef(JonasMessageDestinationRef jonasMessageDestinationRef) {
        jonasMessageDestinationRefList.add(jonasMessageDestinationRef);
    }

    // Getters

    /**
     * @return the list of all jonas-ejb-ref elements
     */
    public JLinkedList getJonasEjbRefList() {
        return jonasEjbRefList;
    }

    /**
     * @return the list of all jonas-resource-env elements
     */
    public JLinkedList getJonasResourceEnvList() {
        return jonasResourceEnvRefList;
    }

    /**
     * @return the list of all jonas-resource elements
     */
    public JLinkedList getJonasResourceList() {
        return jonasResourceRefList;
    }


    /**
     * @return the list of all jonas-service-ref elements
     */
    public JLinkedList getJonasServiceRefList() {
        return jonasServiceRefList;
    }

    /**
     * @return the list of all jonas-message-destination-ref elements
     */
    public JLinkedList getJonasMessageDestinationRefList() {
        return jonasMessageDestinationRefList;
    }

}
