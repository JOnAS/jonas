/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): ____________________________________.
 * Contributor(s): Lutris Technologies Inc http://www.lutris.com
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
*/


package org.ow2.jonas.deployment.common;

import java.io.PrintStream;
import java.io.PrintWriter;

/**
 * The DeploymentDesc exception uses the pattern defined by the Enhydra
 * com.lutris.util.ChainedException
 * The source has been temporary duplicated to prevent Jonas using
 * from external dependencies.
 * @author Christophe Ney
 */
public class DeploymentDescException extends Exception {

    /**
     * Cause of the exception
     */
    private Throwable cause;

   /**
     * Construct an exception without anything
     */
    public DeploymentDescException() {
        super();
        cause = null;
    }


    /**
     * Construct an exception without a specified cause.
     *
     * @param msg The message associated with the exception.
     */
    public DeploymentDescException(String msg) {
        super(msg);
        cause = null;
    }

    /**
     * Construct an exception with an associated causing exception.
     *
     * @param msg The message associated with the exception.
     * @param cause The error or exception that cause this
     *  exception.
     */
    public DeploymentDescException(String msg,
                            Throwable cause) {
        super(msg);
        this.cause = cause;
    }

    /**
     * Construct an exception from a causing exception.
     *
     * @param cause The error or exception that cause this
     *  exception. The message will be take be this object's
     *  messasge.
     */
    public DeploymentDescException(Throwable cause) {
        super(cause.getMessage());
        this.cause = cause;
    }

    /**
     * @return the message associated with this exception.  If causes
     * are included, they will be appended to the message.
     */
    public String getMessage() {
        String msg = super.getMessage();
        if (cause == null) {
            return msg;
        } else {
            return msg + ": " + cause.getMessage();
        }
    }

    /**
     * Gets the causing exception associated with this exception.
     * @return The causing exception or null if no cause is specified.
     */
    public Throwable getCause() {
        return cause;
    }

    /**
     * Prints this DeploymentDescException and its backtrace, and the causes
     * and their stack traces to the standard error stream.
     */
    public void printStackTrace() {
        super.printStackTrace();
        if (cause != null) {
            System.err.println();
            System.err.println("*** Caused by:");
            cause.printStackTrace();
        }
    }

    /**
     * Prints this DeploymentDescException and its backtrace, and the causes
     * and their stack traces to the e specified print stream.
     * @param s print the trace on a specific print stream
     */
    public void printStackTrace(PrintStream s) {
        super.printStackTrace(s);
        if (cause != null) {
            s.println();
            s.println ("*** Caused by:");
            cause.printStackTrace(s);
        }
    }

    /**
     * Prints this DeploymentDescException and its backtrace, and the causes
     * and their stack traces to the e specified print writer.
     * @param s print the trace on a specific print stream
     */
    public void printStackTrace(PrintWriter s) {
        super.printStackTrace(s);
        if (cause != null) {
            s.println();
            s.println("*** Caused by:");
            cause.printStackTrace(s);
        }
    }
}
