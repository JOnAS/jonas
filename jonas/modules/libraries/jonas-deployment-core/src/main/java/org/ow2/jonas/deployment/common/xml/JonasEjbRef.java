/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.common.xml;

/**
 * This class defines the implementation of the element jonas-ejb-ref.
 * @author Florent Benoit
 */
public class JonasEjbRef extends AbsElement {

    /**
     * Name of this jonas-ejb-ref
     */
    private String ejbRefName = null;

    /**
     * jndi name of this jonas-ejb-ref
     */
    private String jndiName = null;


    // Setters

    /**
     * Sets the name
     * @param ejbRefName the name to use
     */
    public void setEjbRefName(String ejbRefName) {
        this.ejbRefName = ejbRefName;
    }


    /**
     * Sets the jndi name
     * @param jndiName the jndi-name to use
     */
    public void setJndiName(String jndiName) {
        this.jndiName = jndiName;
    }



    // Getters


    /**
     * @return the name of the jonas-ejb-ref
     */
    public String getEjbRefName() {
        return ejbRefName;
    }


    /**
     * @return the jndi-name of the jonas-ejb-ref
     */
    public String getJndiName() {
        return jndiName;
    }



    /**
     * Represents this element by it's XML description.
     * @param indent use this indent for prexifing XML representation.
     * @return the XML description of this object.
     */
    public String toXML(int indent) {
        StringBuffer sb = new StringBuffer();
        sb.append(indent(indent));
        sb.append("<jonas-ejb-ref>\n");

        indent += 2;

        // name
        sb.append(xmlElement(ejbRefName, "ejb-ref-name", indent));

        // jndi-name
        sb.append(xmlElement(jndiName, "jndi-name", indent));

        indent -= 2;
        sb.append(indent(indent));
        sb.append("</jonas-ejb-ref>\n");

        return sb.toString();
    }



}
