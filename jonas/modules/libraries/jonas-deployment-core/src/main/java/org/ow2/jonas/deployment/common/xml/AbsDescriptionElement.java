/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.common.xml;

/**
 * This class defines an abstract implementation for all statndard environment element
 * (entity, session, web-app, application-client, etc.)..
 *
 * @author Florent Benoit
 */
public abstract class AbsDescriptionElement extends AbsElement implements DescriptionGroupXml {

    /**
     * icon
     */
    private Icon icon = null;

    /**
     * description
     */
    private String description = null;

    /**
     * Display name
     */
    private String displayName = null;

    /**
     * Construct an empty AbsDescriptionElement.
     */
    public AbsDescriptionElement() {
        super();
        icon = new Icon();
    }

    /**
     * @return the display-name element
     */
    public String getDisplayName() {
        return displayName;
    }


    /**
     * Set the display-name
     * @param displayname displayname
     */
    public void setDisplayName(String displayname) {
        displayName = displayname;
    }
    /**
     * @return the icon
     */
    public Icon getIcon() {
        return icon;
    }


    /**
     * Set the icon
     * @param icon icon
     */
    public void setIcon(Icon icon) {
        this.icon = icon;
    }


    /**
     * Set the small icon
     * @param small small icon
     */
    public void setSmallIcon(String small) {
        icon.setSmallIcon(small);
    }

    /**
     * get the small icon
     * @return  String small icon
     */
    public String setSmallIcon() {
        return icon.getSmallIcon();
    }
    /**
     * Set the large icon
     * @param large large icon
     */
    public void setLargeIcon(String large) {
        icon.setLargeIcon(large);
    }
      /**
     * get the large icon
     * @return  String large icon
     */
    public String setLargeIcon() {
        return icon.getLargeIcon();
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the description
     * @param description description
     */
    public void setDescription(String description) {
        this.description = description;
    }

}
