/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 *
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.deployment.domain.rules;

import org.ow2.jonas.deployment.common.rules.JRuleSetBase;

import org.apache.commons.digester.Digester;

/**
 * This class defines the rules to analyze the server element
 *
 * @author Adriana Danes
 * @author S. Ali Tokmen
 */

public class ClusterDaemonRuleSet extends JRuleSetBase {

    /**
     * Construct an object with the prefix "application"
     */
    public ClusterDaemonRuleSet(String prefix) {
        super(prefix);
    }

    /**
     * Add a set of rules to the digester object
     * @param digester Digester instance
     */
    public void addRuleInstances(Digester digester) {
        digester.addObjectCreate(prefix + "cluster-daemon",
                "org.ow2.jonas.deployment.domain.xml.ClusterDaemon");
        digester.addSetNext(prefix + "cluster-daemon",
                "addClusterDaemon",
                "org.ow2.jonas.deployment.domain.xml.ClusterDaemon");
        digester.addCallMethod(prefix + "cluster-daemon/name", "setName", 0);
        digester.addCallMethod(prefix + "cluster-daemon/description", "setDescription", 0);
        digester.addCallMethod(prefix + "cluster-daemon/password", "setPassword", 0);
        digester.addCallMethod(prefix + "cluster-daemon/username", "setUsername", 0);
        digester.addRuleSet(new LocationRuleSet(prefix + "cluster-daemon/"));
    }

}
