/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Information about the current version of JOnAS.
 * @author JOnAS team
 */
public final class Version {

    /**
     * The Version number is read from the package.
     * Default Version number of JOnAS if package version not available.
     */
    private static final String DEFAULT_VERSION_NUMBER = "5.x";

    /**
     * Version number.
     */
    private static String versionNumber = null;

    /**
     *  JOnAS Vendor.
     */
    public static final String VENDOR = "OW2 (former ObjectWeb)";

    /**
     * Private constructor (utility class).
     */
    private Version() { }

    /**
     * Main method of this class which display the version number.
     * @param args the arguments for this program
     */
    public static void main(final String[] args) {
        System.out.println("JOnAS version: " + getNumber());
        System.out.println("JOnAS vendor: " + VENDOR);
    }

    /**
     * @return Returns the JOnAS Version Number.
     */
    public static String getNumber() {
        if (versionNumber == null) {
            // Read version from the package
            Package pkg = Version.class.getPackage();
            if (pkg != null) {
                String implVersion = pkg.getImplementationVersion();
                if (implVersion != null) {
                    versionNumber = implVersion;
                }
            }
            // if not null, try to load a maven file
            if (versionNumber == null || "".equals(versionNumber)) {
                ClassLoader loader = Version.class.getClassLoader();
                String resource = "META-INF/maven/org.ow2.jonas/jonas-version/pom.properties";
                InputStream stream = loader.getResourceAsStream(resource);
                if (stream != null) {
                    Properties properties = new Properties();
                    try {
                        properties.load(stream);
                        String version = properties.getProperty("version");
                        if (version != null) {
                            versionNumber = version;
                        }
                    } catch (IOException e) {
                        // Ignore
                    } finally {
                        try {
                            stream.close();
                        } catch (IOException e) {
                            // Ignore
                        }
                    }

                }
            }
            // Still not found, return default value
            if (versionNumber == null || "".equals(versionNumber)) {
                versionNumber = DEFAULT_VERSION_NUMBER;
            }
        }

        return versionNumber;
    }

}
