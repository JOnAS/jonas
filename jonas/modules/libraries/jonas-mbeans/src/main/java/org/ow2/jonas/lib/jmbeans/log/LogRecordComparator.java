/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.jmbeans.log;
import java.util.Comparator;
import java.util.logging.LogRecord;

/**
 * @author waeselyf
 *
 */
public class LogRecordComparator implements Comparator {

    /* (non-Javadoc)
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    public int compare(final Object o1, final Object o2) {
        LogRecord r1 = (LogRecord) o1;
        LogRecord r2 = (LogRecord) o2;

        long delta = r1.getMillis() - r2.getMillis();
        if (delta > 0) {
            return 1;
        } else if (delta < 0) {
            return -1;
        }
        // delta == 0
        delta = r1.getSequenceNumber() - r2.getSequenceNumber();
        if (delta > 0) {
            return 1;
        } else if (delta < 0) {
            return -1;
        }
        return 0;
    }

}