/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.jmbeans.log;

import java.util.logging.Level;
import java.util.logging.LogRecord;

import javax.management.Notification;
import javax.management.NotificationListener;

/**
 *
 * @author Adriana Danes
 */
public class LogNotificationListener implements NotificationListener {
    /**
     * LogBuffer objects keeping the LogRecords contained in the received notifications
     */
    private LogBuffer logBuffer = null;
    /**
     * Constructs a NotificationListener for treatement of notifications containining LogRecords
     * @param buf LogBuffer objects keeping the LogRecords contained in the received notifications
     */
    public LogNotificationListener(final LogBuffer buf) {
        this.logBuffer = buf;
    }
    /**
     * Treatement of notifications containining LogRecords
     * @param nf received notification
     * @param handback object
     */
    public void handleNotification(final Notification nf, final Object handback) {
        LogRecord record = null;
        if (nf.getUserData() instanceof LogRecord) {
            record = (LogRecord) nf.getUserData();
        } else {
            return;
        }
        /*
         * update statistics
         */
        logBuffer.incRecordCount();

        if (logBuffer.getOldestDate() == 0) {
            logBuffer.setOldestDate(record.getMillis());
        }

        int level = record.getLevel().intValue();
        if (Level.INFO.intValue() == level) {
            logBuffer.incInfoCount();
        } else if (Level.WARNING.intValue() == level) {
            logBuffer.incWarningCount();
        } else if (Level.SEVERE.intValue() == level) {
            logBuffer.incSevereCount();
        } else {
            logBuffer.incOtherCount();
        }
        logBuffer.setLatestDate(record.getMillis());

        /*
         * update the recent buffer
         */
        logBuffer.addToRecentLog(record);

        /*
         * update the long buffer
         */
        logBuffer.addToLogRecordsQueue(record);

    }
}
