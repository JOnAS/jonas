/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.jmbeans.log;

import javax.management.MBeanException;
import javax.management.NotificationFilter;
import javax.management.NotificationEmitter;
import javax.management.NotificationListener;

import org.apache.commons.modeler.BaseModelMBean;

import org.objectweb.util.monolog.api.Handler;

import org.ow2.jonas.lib.util.Log;
/**
 * MBean interface for Logger management.
 * @author Philippe Durieux
 * @author Adriana Danes
 */
public class LogManagementMBean extends BaseModelMBean {
    // -----------------------------  Contructor
    public LogManagementMBean() throws MBeanException {
        super();
    }


    /**
     * Super charge of the methode to take advantage of JMX notification
     * offered by Monolog 2.0
     * @param arg0 The notification Listener
     * @param arg1 The notification Filter
     * @param arg2 Handback object
     */
    public void addNotificationListener(NotificationListener arg0
            , NotificationFilter arg1, final Object arg2) {
        // Get jmx handler, if any
        Handler handler = Log.getJmxHandler();
        if (handler != null && (handler instanceof NotificationEmitter)) {
            // Add arg0 as listener to notifications emitted by the jmx handler
            ((NotificationEmitter) handler).addNotificationListener(arg0, arg1, arg2);
        }
        // If the listener is the ReconfigManager, the following
        // instruction allows the ReconfigManager to treat the reconfiguration notifications
        // emitted by the LogManager
        super.addNotificationListener(arg0, arg1, arg2);
    }
}
