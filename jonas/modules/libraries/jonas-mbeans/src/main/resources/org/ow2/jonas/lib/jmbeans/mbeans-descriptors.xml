<?xml version="1.0"?>
<!DOCTYPE mbeans-descriptors PUBLIC
 "-//Apache Software Foundation//DTD Model MBeans Configuration File"
 "http://jakarta.apache.org/commons/dtds/mbeans-descriptors.dtd">

<!--
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    - JOnAS: Java(TM) Open Application Server
    - Copyright (C) 2007 Bull S.A.
    - Contact: jonas-team@ow2.org
    -
    - This library is free software; you can redistribute it and/or
    - modify it under the terms of the GNU Lesser General Public
    - License as published by the Free Software Foundation; either
    - version 2.1 of the License, or any later version.
    -
    - This library is distributed in the hope that it will be useful,
    - but WITHOUT ANY WARRANTY; without even the implied warranty of
    - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    - Lesser General Public License for more details.
    -
    - You should have received a copy of the GNU Lesser General Public
    - License along with this library; if not, write to the Free Software
    - Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
    - USA
    -
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    - $Id$
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
-->

<mbeans-descriptors>

    <!-- J2EE Server =============================================================== -->
    <mbean name="J2EEServer"
        className="org.ow2.jonas.lib.jmbeans.J2EEServerMBean"
        description="J2EEServer Component" domain="JOnAS" group="J2EEManaged"
        type="org.ow2.jonas.lib.jmbeans.J2EEServer">

        <attribute name="eventProvider"
            description="Event provider support for this managed object"
            is="true" type="boolean" writeable="false" />

        <attribute name="objectName" description="Name of the object"
            type="java.lang.String" writeable="false" />

        <attribute name="stateManageable"
            description="State management support for this managed object"
            is="true" type="boolean" writeable="false" />

        <attribute name="statisticsProvider"
            description="Performance statistics support for this managed object"
            is="true" type="boolean" writeable="false" />

        <attribute name="eventTypes"
            description="Types of events the J2EEServer emits"
            type="[Ljava.lang.String;" writeable="false" />

        <attribute name="state"
            description="JOnAS server state"
            type="java.lang.String" writeable="false" />

        <attribute name="startTime"
            description="The time that the JOnAS server was started as nb. of milliseconds since January 1, 1970, 00:00:00"
            type="long" writeable="false" />

        <attribute name="uptime"
            description="The elapsed time between this request and the start of the server. Result is in milliseconds"
            type="long" writeable="false" />

        <attribute name="serverName" description="JOnAS server name"
            type="java.lang.String" writeable="false" />

        <attribute name="serverVendor" description="JOnAS server vendor"
            type="java.lang.String" writeable="false" />

        <attribute name="serverVersion"
            description="JOnAS server version" type="java.lang.String"
            writeable="false" />

        <attribute name="jonasBase"
            description="jonas.base environment property" type="java.lang.String"
            writeable="false" />

        <attribute name="jonasRoot"
            description="jonas.root environment property" type="java.lang.String"
            writeable="false" />

        <attribute name="deployedObjects"
            description="The list of MBean names corresponding to the deployed J2EEModules and J2EEApplications"
            type="[Ljava.lang.String;" writeable="false" />

        <attribute name="deployableFiles"
            description="The list of file names that can be deployed."
            type="[Ljava.lang.String;" writeable="false" />

        <attribute name="deployableJars"
            description="The list of JAR file names that can be deployed."
            type="[Ljava.lang.String;" writeable="false" />

        <attribute name="deployableWars"
            description="The list of WAR file names that can be deployed."
            type="[Ljava.lang.String;" writeable="false" />

        <attribute name="deployableRars"
            description="The list of RAR file names that can be deployed."
            type="[Ljava.lang.String;" writeable="false" />

        <attribute name="deployableEars"
            description="The list of EAR file names that can be deployed."
            type="[Ljava.lang.String;" writeable="false" />

        <attribute name="deployedFiles"
            description="The list of deployed files."
            type="[Ljava.lang.String;" writeable="false" />

        <attribute name="deployedJars"
            description="The list of deployed Jars."
            type="[Ljava.lang.String;" writeable="false" />

        <attribute name="deployedWars"
            description="The list of deployed Wars."
            type="[Ljava.lang.String;" writeable="false" />

        <attribute name="deployedEars"
            description="The list of deployed Ears."
            type="[Ljava.lang.String;" writeable="false" />

        <attribute name="deployedRars"
            description="The list of deployed Rars."
            type="[Ljava.lang.String;" writeable="false" />

        <attribute name="protocols"
            description="Protocols supported by this Server"
            type="java.lang.String" writeable="false" />

        <attribute name="resources"
            description="The list of MBean names corresponding to the resources available on this server"
            type="[Ljava.lang.String;" writeable="false" />

        <attribute name="javaVMs"
            description="The list of MBean names corresponding to the JVMs on which this server has running threads"
            type="[Ljava.lang.String;" writeable="false" />

        <attribute name="versions" description="VERSIONS file content"
            type="java.lang.String" writeable="false" />

        <attribute name="jvmInfos" description="Infos about the JVM aand OS"
            type="java.lang.String" writeable="false" />

        <attribute name="services" description="The services"
            type="[Ljava.lang.String;" writeable="false" />

       <!-- Monitoring attributes -->

        <attribute name="currentUsedMemory"
            description="JVM free memory" type="long" writeable="false" />

        <attribute name="currentTotalMemory"
            description="JVM total memory" type="long" writeable="false" />

        <attribute name="range"
            description="For free memory measurement" type="int" />

        <attribute name="sizeTableMeasures"
            description="The size of the table of measures" type="int" />

        <attribute name="activated" description="Monitoring activation"
            is="true" type="boolean" />

        <attribute name="tableMeasures"
            description="The table of values" type="[Ljava.lang.Long;"
            writeable="false" />

        <attribute name="threadStackDump"
            description="JVM thread stack dump" type="[Ljava.lang.String;"
            writeable="false" />

        <attribute name="classLoaderFilters"
            description="Gets the filters used by all Java EE applications" type="[Ljava.lang.String;" writeable="false" >
        </attribute>

        <operation name="runGC"
            description="Run the Garbage Collector"
            impact="ACTION" />

        <operation name="start"
            description="Start the server"
            impact="ACTION" />

            <operation name="start"
            description="Start the server. Uses standby mode as parameter."
            impact="ACTION" >
            <parameter name="standby"
                description="enable/disable standby mode" type="java.lang.boolean" />
            </operation>

        <operation name="startRecursive"
            description="Start the server"
            impact="ACTION" />

            <operation name="startRecursive"
            description="Start the server. Uses standby mode as parameter."
            impact="ACTION">
            <parameter name="standby"
                description="enable/disable standby mode" type="java.lang.boolean" />
            </operation>

        <operation name="stop"
            description="Stop the server"
            impact="ACTION" />

        <operation name="halt"
            description="Halt the server"
            impact="ACTION" />

        <operation name="deploy"
            description="deploy a module or application"
            impact="ACTION">
            <parameter name="fileName"
                description="Name of the file" type="java.lang.String" />
        </operation>

        <operation name="undeploy"
            description="undeploy a module or application"
            impact="ACTION">
            <parameter name="fileName"
                description="Name of the file" type="java.lang.String" />
        </operation>

        <operation name="isDeployed"
            description="check if a module or application is deployed"
            impact="ACTION"
            returnType="boolean">
            <parameter name="fileName"
                description="Name of the file" type="java.lang.String" />
        </operation>

        <operation name="sendFile"
            description="Send a file into JOnAS BASE" impact="ACTION"
            returnType="java.lang.String">
            <parameter name="type"
                description="Binary content of the file" type="[B" />
            <parameter name="type" description="Name of the file"
                type="java.lang.String" />
            <parameter name="type" description="Replace existing file ?"
                type="boolean" />
        </operation>

       <operation name="distribute"
            description="distribute (jsr88 meaning) a file into JOnAS BASE" impact="ACTION"
            returnType="java.lang.String">
            <parameter name="fileName" description="Name of the file"
                type="java.lang.String" />
            <parameter name="fileContent"
                description="File binary content" type="[B" />

        </operation>

        <operation name="removeModuleFile"
            description="Remove a J2EE module file into JOnAS BASE"
            impact="ACTION" returnType="boolean">
            <parameter name="type" description="Name of the file"
                type="java.lang.String" />
        </operation>

        <operation name="getConfigFileEnv"
            description="Returns server environment as configured with properties files only."
            impact="ACTION"
            returnType="java.util.Properties">
        </operation>

        <operation name="setRepositories"
            description="Set additional deployment repositories"
            impact="ACTION">
            <parameter name="dirs" description="repository directories name"
                type="[Ljava.lang.String;" />
        </operation>
        
       <operation name="startService"
            description="Start a service"
            impact="ACTION">
            <parameter name="service" description="Name of the service"
                type="java.lang.String" />
        </operation>

       <operation name="stopService"
            description="Stop a service"
            impact="ACTION">
            <parameter name="service" description="Name of the service"
                type="java.lang.String" />
        </operation>

       <operation name="getServiceState"
            description="Return the state of a service"
            impact="ACTION" returnType="String">
            <parameter name="service" description="Name of the service"
                type="java.lang.String" />
        </operation>

       <operation name="getServiceDescription"
            description="Return a service description"
            impact="ACTION" returnType="String">
            <parameter name="service" description="Name of the service"
                type="java.lang.String" />
        </operation>

        <operation name="getThreadStackDump"
            description="Return a JVM thread stack dump" impact="ACTION"
            returnType="java.lang.String">
        </operation>

        <operation name="logThreadStackDump"
            description="Log a JVM thread stack dump" impact="ACTION"
            returnType="void">
        </operation>

        <operation name="printThreadStackDump"
            description="Print a JVM thread stack dump in a given file" impact="ACTION"
            returnType="void">
             <parameter name="filename" description="Name of the file"
                type="java.lang.String" />
        </operation>
         <operation name="getThreadStackDumpList"
            description="Return a TabularData containing a JVM thread stack dump" impact="ACTION"
            returnType="javax.management.openmbean.TabularData;">
        </operation>

        <operation name="getExportedPackages"
            description="Gets data about exported packages for a given class/package name" impact="ACTION"
            returnType="java.lang.String">
             <parameter name="name" description="Name of the class/package"
                type="java.lang.String" />
             <parameter name="isClass" description="Name is a class or a package"
                type="boolean" />
        </operation>

        <operation name="getResources"
            description="Gets all URLs available on the platform for a given resource name" impact="ACTION"
            returnType="[Ljava.net.URL;">
             <parameter name="name" description="Name of the resource to search"
                type="java.lang.String" />
        </operation>

        <operation name="loadClass"
            description="Loads a given class and return data about it" impact="ACTION"
            returnType="java.lang.String">
             <parameter name="name" description="Name of the class to search"
                type="java.lang.String" />
        </operation>

    </mbean>

    <!-- JVM ======================================================================= -->
    <mbean name="JVM" description="JVM Component" domain="JOnAS"
        group="J2EEManaged" type="org.ow2.jonas.lib.jmbeans.JavaVm">

        <attribute name="eventProvider"
            description="Event provider support for this managed object"
            is="true" type="boolean" writeable="false" />

        <attribute name="javaVendor"
            description="The Java Runtime Environnement vendor"
            type="java.lang.String" writeable="false" />

        <attribute name="javaVersion"
            description="The Java Runtime Environnement version"
            type="java.lang.String" writeable="false" />

        <attribute name="javaHome"
            description="The java.home environnement variable"
            type="java.lang.String" writeable="false" />

        <attribute name="node"
            description="The node (machine) this JVM is running on"
            type="java.lang.String" writeable="false" />

        <attribute name="objectName" description="Name of the object"
            type="java.lang.String" writeable="false" />

        <attribute name="stateManageable"
            description="State management support for this managed object"
            is="true" type="boolean" writeable="false" />

        <attribute name="statisticsProvider"
            description="Performance statistics support for this managed object"
            is="true" type="boolean" writeable="false" />

        <attribute name="allThreadsCount"
            description="The number of active threads in the JVM" type="int"
            writeable="false" />

        <attribute name="threadGroups"
            description="List of names of all thread groups"
            type="[Ljava.lang.String;" writeable="false" />

        <operation name="listThreads"
            description="Get the list of thread names for a thread group"
            impact="ACTION" returnType="[Ljava.lang.String;">
            <parameter name="threadgroupname"
                description="Name of the thread group" type="java.lang.String" />
        </operation>
    </mbean>

    <!-- J2EEDomain ======================================================================= -->
    <mbean name="J2EEDomain" description="J2EEDomain Component"
        domain="JOnAS" group="J2EEManaged"
        type="org.ow2.jonas.lib.management.domain.J2EEDomain">

        <!-- JSR77 -->

        <attribute name="eventProvider"
            description="Event provider support for this managed object"
            is="true" type="boolean" writeable="false" />

        <attribute name="objectName" description="Name of the object"
            type="java.lang.String" writeable="false" />

        <attribute name="stateManageable"
            description="State management support for this managed object"
            is="true" type="boolean" writeable="false" />

        <attribute name="statisticsProvider"
            description="Performance statistics support for this managed object"
            is="true" type="boolean" writeable="false" />

        <!-- JSR77 J2EEDomain -->

        <attribute name="servers"
            description="The list of MBean names corresponding to the J2EEServers in the domain"
            type="[Ljava.lang.String;" writeable="false" />

        <!-- JOnAS Domain Management attributes -->

        <attribute name="master"
            description="true if the current server is a master" is="true"
            type="boolean" writeable="false" />

        <attribute name="startedServers"
            description="The list of MBean names corresponding to the running J2EEServers in the domain"
            type="[Ljava.lang.String;" writeable="false" />

        <attribute name="serverNames"
            description="The list of names corresponding to the servers in the domain"
            type="[Ljava.lang.String;" writeable="false" />

        <attribute name="clusters"
            description="The list of MBean names corresponding to the clusters in the domain"
            type="[Ljava.lang.String;" writeable="false" />

        <attribute name="clusterDaemons"
            description="The list of MBean names corresponding to the clusterDaemons in the domain"
            type="[Ljava.lang.String;" writeable="false" />

        <attribute name="myName"
            description="Domain name or cluster name if this is a cluster"
            type="java.lang.String" writeable="false" />

        <!-- JOnAS Domain Management operations -->
        <operation name="createCluster"
            description="Create a cluster in the management domain"
            impact="ACTION" returnType="java.lang.String">
            <parameter name="clusterName" description="cluster name"
                type="java.lang.String" />
        </operation>

        <operation name="getErrorMessage"
            description="Get the error message" impact="ACTION"
            returnType="java.lang.String">
            <parameter name="fileName"
                description="Name of the file to deploy" type="java.lang.String" />
            <parameter name="serverName"
                description="Name of the server where the file is deployed"
                type="java.lang.String" />
        </operation>

        <operation name="getServersInCluster"
            description="Get the list of servers that are in a given cluster"
            impact="ACTION" returnType="[Ljava.lang.String;">
            <parameter name="clusterName"
                description="Name of the cluster" type="java.lang.String" />
        </operation>

        <operation name="getServerState"
            description="Get the state of a server in the domain" impact="ACTION"
            returnType="java.lang.String">
            <parameter name="serverName"
                description="Name of the server" type="java.lang.String" />
        </operation>

        <operation name="getClusterState"
            description="Get the state of a cluster in the domain"
            impact="ACTION" returnType="java.lang.String">
            <parameter name="clusterName"
                description="Name of the cluster" type="java.lang.String" />
        </operation>

        <operation name="getClusterdaemonState"
            description="Get the state of a cluster daemon in the domain"
            impact="ACTION" returnType="java.lang.String">
            <parameter name="clusterdaemonName"
                description="Name of the cluster daemon" type="java.lang.String" />
        </operation>

        <operation name="getClusterType"
            description="Get the type of a cluster in the domain" impact="ACTION"
            returnType="java.lang.String">
            <parameter name="clusterName"
                description="Name of the cluster" type="java.lang.String" />
        </operation>

        <operation name="getServerClusterdaemon"
            description="Get the cluster daemon name of a server in the domain"
            impact="ACTION" returnType="java.lang.String">
            <parameter name="serverName"
                description="Name of the server" type="java.lang.String" />
        </operation>

        <operation name="getServersNotInCluster"
            description="Get the list of servers that are not in a given cluster"
            impact="ACTION" returnType="[Ljava.lang.String;">
            <parameter name="clusterName"
                description="Name of the cluster" type="java.lang.String" />
        </operation>

        <operation name="deployJar"
            description="Deploy a JAR file on a management target"
            impact="ACTION" returnType="void">
            <parameter name="target"
                description="J2EEServer ObjectNames of servers in the target"
                type="[Ljava.lang.String;" />
            <parameter name="fileName"
                description="Name of the JAR file to deploy on the target"
                type="java.lang.String" />
        </operation>

        <operation name="unDeployJar"
            description="Undeploy a JAR file from a management target"
            impact="ACTION" returnType="void">
            <parameter name="target"
                description="J2EEServer ObjectNames of servers in the target"
                type="[Ljava.lang.String;" />
            <parameter name="fileName"
                description="Name of the JAR file" type="java.lang.String" />
        </operation>

        <operation name="deployWar"
            description="Deploy a WAR file on a management target"
            impact="ACTION" returnType="void">
            <parameter name="target"
                description="J2EEServer ObjectNames of servers in the target"
                type="[Ljava.lang.String;" />
            <parameter name="fileName"
                description="Name of the WAR file to deploy on the target"
                type="java.lang.String" />
        </operation>

        <operation name="unDeployWar"
            description="Undeploy a WAR file from a management target"
            impact="ACTION" returnType="void">
            <parameter name="target"
                description="J2EEServer ObjectNames of servers in the target"
                type="[Ljava.lang.String;" />
            <parameter name="fileName"
                description="Name of the WAR file" type="java.lang.String" />
        </operation>

        <operation name="deployRar"
            description="Deploy a RAR file on a management target"
            impact="ACTION" returnType="void">
            <parameter name="target"
                description="J2EEServer ObjectNames of servers in the target"
                type="[Ljava.lang.String;" />
            <parameter name="fileName"
                description="Name of the RAR file to deploy on the target"
                type="java.lang.String" />
        </operation>

        <operation name="unDeployRar"
            description="Undeploy a RAR file from a management target"
            impact="ACTION" returnType="void">
            <parameter name="target"
                description="J2EEServer ObjectNames of servers in the target"
                type="[Ljava.lang.String;" />
            <parameter name="fileName"
                description="Name of the RAR file" type="java.lang.String" />
        </operation>

        <operation name="deployEar"
            description="Deploy a EAR file on a management target"
            impact="ACTION" returnType="void">
            <parameter name="target"
                description="J2EEServer ObjectNames of servers in the target"
                type="[Ljava.lang.String;" />
            <parameter name="fileName"
                description="Name of the EAR file to deploy on the target"
                type="java.lang.String" />
        </operation>

        <operation name="unDeployEar"
            description="Undeploy a EAR file from a management target"
            impact="ACTION" returnType="void">
            <parameter name="target"
                description="J2EEServer ObjectNames of servers in the target"
                type="[Ljava.lang.String;" />
            <parameter name="fileName"
                description="Name of the EAR file" type="java.lang.String" />
        </operation>

        <operation name="uploadFile" description="Upload a new file"
            impact="ACTION" returnType="void">
            <parameter name="target"
                description="J2EEServer ObjectNames of servers in the target"
                type="[Ljava.lang.String;" />
            <parameter name="fileName"
                description="Name of the file to upload" type="java.lang.String" />
            <parameter name="replaceExisting"
                description="Replace the file on the target server with the same name"
                type="boolean" />
        </operation>

        <operation name="uploadDeployEar"
            description="Upload a new EAR file and (re)deploy it" impact="ACTION"
            returnType="void">
            <parameter name="target"
                description="J2EEServer ObjectNames of servers in the target"
                type="[Ljava.lang.String;" />
            <parameter name="fileName"
                description="Name of the EAR file" type="java.lang.String" />
            <parameter name="replaceExisting"
                description="Replace the file on the target server with the same name"
                type="boolean" />
        </operation>

        <operation name="uploadDeployRar"
            description="Upload a new RAR file and (re)deploy it" impact="ACTION"
            returnType="void">
            <parameter name="target"
                description="J2EEServer ObjectNames of servers in the target"
                type="[Ljava.lang.String;" />
            <parameter name="fileName"
                description="Name of the RAR file" type="java.lang.String" />
            <parameter name="replaceExisting"
                description="Replace the file on the target server with the same name"
                type="boolean" />
        </operation>

        <operation name="uploadDeployWar"
            description="Upload a new WAR file and (re)deploy it" impact="ACTION"
            returnType="void">
            <parameter name="target"
                description="J2EEServer ObjectNames of servers in the target"
                type="[Ljava.lang.String;" />
            <parameter name="fileName"
                description="Name of the WAR file" type="java.lang.String" />
            <parameter name="replaceExisting"
                description="Replace the file on the target server with the same name"
                type="boolean" />
        </operation>

        <operation name="uploadDeployJar"
            description="Upload a new JAR file and (re)deploy it" impact="ACTION"
            returnType="void">
            <parameter name="target"
                description="J2EEServer ObjectNames of servers in the target"
                type="[Ljava.lang.String;" />
            <parameter name="fileName"
                description="Name of the JAR file" type="java.lang.String" />
            <parameter name="replaceExisting"
                description="Replace the file on the target server with the same name"
                type="boolean" />
        </operation>

        <operation name="getDeployServers"
            description="Get the list of servers where a file is being deployed"
            impact="ACTION" returnType="[Ljava.lang.String;">
            <parameter name="fileName" description="Name of the file"
                type="java.lang.String" />
        </operation>

        <operation name="getDeployState"
            description="Get the current state of deployment operation"
            impact="ACTION" returnType="java.lang.String">
            <parameter name="fileName" description="Name of the file"
                type="java.lang.String" />
            <parameter name="serverName"
                description="Name of the target server" type="java.lang.String" />
        </operation>

        <operation name="startServer"
            description="Ask cluster daemon to start a server" impact="ACTION"
            returnType="void">
            <parameter name="serverName"
                description="Server to start name" type="java.lang.String" />
        </operation>

        <operation name="stopServer"
            description="Ask cluster daemon to stop a server" impact="ACTION"
            returnType="void">
            <parameter name="serverName"
                description="Server to start name" type="java.lang.String" />
        </operation>

        <operation name="haltServer"
            description="Ask cluster daemon to stop a server" impact="ACTION"
            returnType="void">
            <parameter name="serverName"
                description="Server to start name" type="java.lang.String" />
        </operation>

        <operation name="forgetAllDeploy"
            description="Forget all deploy information" impact="ACTION"
            returnType="void">
        </operation>

        <operation name="startRemoteTarget"
            description="Start remote target server"
            impact="ACTION">
            <parameter name="target"
              description="Name of the remote server" type="java.lang.String" />
        </operation>

        <operation name="stopRemoteTarget"
            description="Stop remote target server"
            impact="ACTION">
            <parameter name="target"
              description="Name of the remote server" type="java.lang.String" />
        </operation>
    </mbean>

    <!-- OsgiMonitor ======================================================================= -->
    <mbean name="OsgiMonitor" description="This MBean is monitoring the OSGI framework"
        className="org.ow2.jonas.lib.jmbeans.osgi.monitoring.OsgiMonitorMBean"
        domain="JOnAS" group="JonasManaged"
        type="org.ow2.jonas.lib.jmbeans.osgi.monitoring.OsgiMonitor">

       <attribute name="att1"
           description="test attribute"
           type="java.lang.String"
           writeable="true" />

    </mbean>

    <!-- WorkManager ======================================================================= -->
    <mbean name="WorkManager" description="WorkManager Component"
        className="org.ow2.jonas.workmanager.internal.JWorkManagerMBean"
        domain="JOnAS" group="JonasManaged"
        type="org.ow2.jonas.workmanager.internal.JWorkManager">

        <attribute name="currentPoolSize"
            description="Current number of threads in the pool" type="int"
            writeable="false" />

        <attribute name="minPoolSize"
            description="Minimum number of threads in the pool" type="int"
            writeable="true" />

        <attribute name="maxPoolSize"
            description="Maximum number of threads in the pool" type="int"
            writeable="true" />

        <operation name="saveConfig"
            description="save configuration"
            impact="ACTION">
        </operation>
    </mbean>
        

    <!-- WorkCleaner ======================================================================= -->
    <mbean name="WorkCleaner" description="WorkCleaner Component"
        className="org.ow2.jonas.workcleaner.internal.JOnASWorkCleanerServiceMBean"
        domain="JOnAS" group="JonasManaged"
        type="org.ow2.jonas.workcleaner.internal.JOnASWorkCleanerService">

        <attribute name="executePeriod"
            description="Execution period" type="int"
            writeable="true" />
            
        <operation name="executeTasks"
            description="execute the registered tasks"
            impact="ACTION">
        </operation>
        
        <operation name="saveConfig"
            description="save configuration"
            impact="ACTION">
        </operation>
    </mbean>
    
    
    <!-- LogManagement ======================================================================= -->
    <mbean name="LogManagement" description="LogManagement Component"
        className="org.ow2.jonas.lib.jmbeans.log.LogManagementMBean"
        domain="JOnAS" group="JonasManaged"
        type="org.ow2.jonas.lib.jmbeans.log.LogManagement">

        <attribute name="topics"
            description="log topics list" type="[Ljava.lang.String;"
            writeable="false" />

        <attribute name="props"
            description="Get logging system properties" type="java.util.Properties"
            writeable="false" />

        <attribute name="configFile"
            description="Get logging system configuration file name" type="java.lang.String"
            writeable="false" />

        <operation name="getTopicLevel"
            description="Return the debug level of a topic"
            impact="ACTION" returnType="java.lang.String">
            <parameter name="topic"
                description="the topic name" type="java.lang.String" />
        </operation>

        <operation name="setTopicLevel"
            description="Set debug level for a topic"
            impact="ACTION">
            <parameter name="topic"
                description="the topic name" type="java.lang.String" />
            <parameter name="level"
                description="the level to set" type="java.lang.String" />
        </operation>

        <operation name="saveConfig"
            description="save configuration"
            impact="ACTION">
        </operation>

    </mbean>

    <!-- ReconfigManager ======================================================================= -->
    <mbean name="ReconfigManager" description="ReconfigManager Component"
        domain="JOnAS" group="JonasManaged"
        type="org.ow2.jonas.lib.jmbeans.ReconfigManager">

        <attribute name="eventProvider"
            description="Event provider support for this managed object"
            is="true" type="boolean" writeable="false" />

        <attribute name="objectName" description="Name of the object"
            type="java.lang.String" writeable="false" />

        <attribute name="stateManageable"
            description="State management support for this managed object"
            is="true" type="boolean" writeable="false" />

        <attribute name="statisticsProvider"
            description="Performance statistics support for this managed object"
            is="true" type="boolean" writeable="false" />

    </mbean>
</mbeans-descriptors>
