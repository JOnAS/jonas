/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.monolog;

import org.objectweb.util.monolog.Monolog;
import org.objectweb.util.monolog.api.LogInfo;

import org.osgi.framework.ServiceReference;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

/**
 * This class implements monolog extensions registry
 * @author Mohammed Boukada
 */
public class MonologExtension {

    /**
     * Logger.
     */
    private Log logger = LogFactory.getLog(MonologExtension.class);

    /**
     * Add an extension to Monolog
     * @param logInfoProvider
     */
    public void addExtension(final LogInfo logInfoProvider, ServiceReference ref) {
        Character pattern = (Character) ref.getProperty("pattern");
        try {
            Monolog.monologFactory.addLogInfo(pattern, logInfoProvider);
        } catch (Exception e) {

        }
        logger.info("Extension ''{0}'' was added by ''{1}'' to Monolog", pattern, logInfoProvider.getClass().getName());
    }


    /**
     * Remove and extension from Monolog
     */
    public void removeExtension(ServiceReference ref) {
        Character pattern = (Character) ref.getProperty("pattern");
        Monolog.monologFactory.removeLogInfo(pattern);
        logger.info("Extension ''{0}'' was removerd from Monolog.", pattern);
    }

    /**
     * Start ipojo component
     */
    public void start() {
        logger.debug("Monolog extension component was started");
    }

    /**
     * Stop ipojo component
     */
    public void stop() {
        logger.debug("Monolog extension component was stopped");
    }
}
