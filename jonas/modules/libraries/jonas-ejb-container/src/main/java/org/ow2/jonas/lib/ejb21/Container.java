/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.ejb21;


import org.ow2.jonas.jms.JmsManager;
import org.ow2.jonas.naming.JComponentContextFactory;
import org.ow2.jonas.naming.JNamingManager;
import org.ow2.jonas.tm.TransactionManager;



/**
 * A Container is what a JOnAS Server can see of a JOnAS EJB Container.
 * It holds all beans coming from the same ejbjar file.
 * There is no possibility to add or remove dynamically beans in
 * a Container since it is build with all its beans.
 * JMX can use this interface, but no specific JMX classes should
 * be inside the Container. Its management is done outside jonas_ejb.
 * @author Philippe Coq
 * @author Jeff Mesnil (security)
 * @author Christophe Ney (PrincipalFactory)
 * @author Philippe Durieux (add missing methods)
 * @author  Benjamin Bonnet (max size for thread pool)
 * @author Florent Benoit : JACC
 */
public interface Container {

    /**
     * @return List of beans hosted in this Container
     */
    String [] listBeanNames();

    /**
     * @return name of this Container
     */
    String getName();

    /**
     * @return the file name of the container (.xml or .jar)
     */
    String getFileName();


    /**
     * @return the external(user) file name of the container
     */
    String getExternalFileName();


    /**
     * @return the classloader used for this Container
     */
    ClassLoader getClassLoader();

    /**
     * Set the PrincipalFactory. This factory can be JOnAS Server dependant.
     * The Container makes no assumption on how to get the Principal.
     * @param pf the PrincipalFactory
     */
    void setPrincipalFactory(PrincipalFactory pf);

    /**
     * @return the PrincipalFactory of the Container
     */
    PrincipalFactory getPrincipalFactory();

    /**
     * Set the {@link JNamingManager} object.
     * @param naming the {@link JNamingManager} object
     */
    void setContainerNaming(JNamingManager naming);

    /**
     * @return the {@link JNamingManager} object.
     */
    JNamingManager getContainerNaming();

    /**
     * Set the {@link JComponentContextFactory} instance.
     * @param factory the {@link JComponentContextFactory} instance
     */
    void setComponentContextFactory(JComponentContextFactory factory);

    /**
     * @return the {@link JComponentContextFactory} object.
     */
    JComponentContextFactory getComponentContextFactory();

    /**
     * Synchronize all entity beans.
     * @param alwaysStore if true store even if passivationTimeout not reached
     * @param passivate true if bean instances will be released after
     * having been written on storage.
     */
    void syncAll(final boolean alwaysStore, final boolean passivate);

    /**
     * set the Transaction Manager.
     * @param tm the Transaction Manager.
     */
    void setTransactionManager(TransactionManager tm);

    /**
     * @return the Transaction Manager
     */
    TransactionManager getTransactionManager();

    /**
     * set the JmsManager object.
     * @param jms the JmsManager
     */
    void setJmsManager(JmsManager jms);

    /**
     * @return the JmsManager object
     */
    JmsManager getJmsManager();

    /**
     * set the name of the ear application containing this container.
     * @param fileName the name of the ear application containing this
     * container.
     */
    void setEarFileName(String fileName);

    /**
     * get the name of the ear application containing this container.
     * @return the name of the ear application containing this
     * container.
     */
    String getEarFileName();

    /**
     * Remove the JOnAS container and unregister all beans.
     */
    void remove();

    /**
     * Set the security flag to enable or disable security.
     * @param b true or false to enable/disable security
     */
    void setSecurity(boolean b);

    /**
     * Set the permission manager object.
     * @param permissionManager permission manager object
     */
    void setPermissionManager(PermissionManager permissionManager);

    /**
     * Gets the permission manager.
     * @return the permission manager
     */
    PermissionManager getPermissionManager();

    /**
     * Gets the context ID of this container (for jacc).
     * @return contextID used for JACC
     */
    String getContextId();

    /**
     * Get the bean factory for the given bean.
     * @param beanName the name of the bean
     * @return the bean factory for this bean
     */
    BeanFactory getBeanFactory(String beanName);

    /**
     * Gets the Java EE Application Name of this container.
     * @return the Java EE Application name if there is one
     */
    String getJavaEEApplicationName();

    /**
     * Sets the Java EE Application Name of this container.
     * @param javaEEApplicationName the Java EE Application name
     */
    void setJavaEEApplicationName(String javaEEApplicationName);


}
