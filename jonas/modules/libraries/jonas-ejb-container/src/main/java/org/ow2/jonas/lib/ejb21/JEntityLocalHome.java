/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.ejb21;

import org.ow2.jonas.deployment.ejb.EntityDesc;

import org.objectweb.util.monolog.api.BasicLevel;

/**
 * This class is the Standard LocalHome for Entity objects It exists only for
 * beans that have declared a Local Interface. It implements
 * javax.ejb.EJBLocalHome interface
 * @author Philippe Durieux
 */
public abstract class JEntityLocalHome extends JLocalHome {

    /**
     * constructor
     * @param dd The Entity Deployment Decriptor
     * @param bf The Entity Factory
     */
    public JEntityLocalHome(EntityDesc dd, JEntityFactory bf) {
        super(dd, bf);
        if (TraceEjb.isDebugIc()) {
            TraceEjb.interp.log(BasicLevel.DEBUG, "");
        }
    }

    // ---------------------------------------------------------------
    // EJBLocalHome implementation
    // The only method is remove(pk) and is in the generated part.
    // ---------------------------------------------------------------

    // ---------------------------------------------------------------
    // other public methods, for internal use.
    // ---------------------------------------------------------------

    /**
     * Find the EJBLocalObject for this PK
     * @param pk the primary key
     * @return EntityLocal matching this PK
     */
    public JEntityLocal findLocalByPK(Object pk) {
        TraceEjb.interp.log(BasicLevel.DEBUG, "");
        JEntityFactory ef = (JEntityFactory) bf;
        JEntitySwitch es = ef.getEJB(pk);
        return es.getLocal();
    }

    /**
     * Creates a new Local Object for that bean. This is in the generated class
     * because it is mainly "new objectClass()"
     * @return The Local Object
     */
    public abstract JEntityLocal createLocalObject();

}