/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2008 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.ejb21;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.ejb.EJBHome;
import javax.ejb.EJBObject;
import javax.ejb.spi.HandleDelegate;
import javax.rmi.PortableRemoteObject;

import org.objectweb.util.monolog.api.BasicLevel;
import org.ow2.carol.rmi.util.RmiMultiUtility;

/**
 * Implementation for HandleDelegate SPI.
 * @author durieuxp
 */
public class JHandleDelegate implements HandleDelegate {

    // -----------------------------------------------------------------------
    // HandleDelegate implementation
    // ------------------------------------------------------------------------

    /**
     * Serialize the EJBObject reference corresponding to a Handle. This method
     * is called from the writeObject method of portable Handle implementation
     * classes. The ostream object is the same object that was passed in to the
     * Handle class's writeObject.
     * @param ejb The EJBObject reference to be serialized.
     * @param out The output stream.
     * @throws IOException The EJBObject could not be serialized because of a
     *         system-level failure.
     */
    public void writeEJBObject(final EJBObject ejb, final ObjectOutputStream out) throws IOException {
        if (TraceEjb.isDebugIc()) {
            TraceEjb.interp.log(BasicLevel.DEBUG, "");
        }
        out.writeObject(ejb);
    }

    /**
     * Deserialize the EJBObject reference corresponding to a Handle.
     * readEJBObject is called from the readObject method of portable Handle
     * implementation classes. The istream object is the same object that was
     * passed in to the Handle class's readObject. When readEJBObject is called,
     * istream must point to the location in the stream at which the EJBObject
     * reference can be read. The container must ensure that the EJBObject
     * reference is capable of performing invocations immediately after
     * deserialization.
     * @param in The input stream.
     * @return The deserialized EJBObject reference.
     * @throws IOException The EJBObject could not be deserialized because of a
     *         system-level failure.
     * @throws ClassNotFoundException The EJBObject could not be deserialized
     *         because some class could not be found.
     */
    public EJBObject readEJBObject(final ObjectInputStream in) throws IOException, ClassNotFoundException {
        if (TraceEjb.isDebugIc()) {
            TraceEjb.interp.log(BasicLevel.DEBUG, "");
        }
        Object ejb = in.readObject();
        RmiMultiUtility.reconnectStub2Orb(ejb);
        return (EJBObject) PortableRemoteObject.narrow(ejb, EJBObject.class);
    }

    /**
     * Serialize the EJBHome reference corresponding to a HomeHandle. This
     * method is called from the writeObject method of portable HomeHandle
     * implementation classes. The ostream object is the same object that was
     * passed in to the Handle class's writeObject.
     * @param home The EJBHome reference to be serialized.
     * @param out The output stream.
     * @throws IOException The EJBObject could not be deserialized because of a
     *         system-level failure.
     */
    public void writeEJBHome(final EJBHome home, final ObjectOutputStream out) throws IOException {
        if (TraceEjb.isDebugIc()) {
            TraceEjb.interp.log(BasicLevel.DEBUG, "");
        }
        out.writeObject(home);
    }

    /**
     * Deserialize the EJBHome reference corresponding to a HomeHandle.
     * readEJBHome is called from the readObject method of portable HomeHandle
     * implementation classes. The istream object is the same object that was
     * passed in to the HomeHandle class's readObject. When readEJBHome is
     * called, istream must point to the location in the stream at which the
     * EJBHome reference can be read. The container must ensure that the EJBHome
     * reference is capable of performing invocations immediately after
     * deserialization.
     * @param in The input stream.
     * @return The deserialized EJBHome reference.
     * @throws IOException The EJBHome could not be deserialized because of a
     *         system-level failure.
     * @throws ClassNotFoundException The EJBHome could not be deserialized
     *         because some class could not be found.
     */
    public EJBHome readEJBHome(final ObjectInputStream in) throws IOException, ClassNotFoundException {
        if (TraceEjb.isDebugIc()) {
            TraceEjb.interp.log(BasicLevel.DEBUG, "");
        }
        Object home = in.readObject();
        RmiMultiUtility.reconnectStub2Orb(home);
        return (EJBHome) PortableRemoteObject.narrow(home, EJBHome.class);
    }
}
