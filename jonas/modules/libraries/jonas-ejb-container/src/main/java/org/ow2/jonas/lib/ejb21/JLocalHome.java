/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Philippe Durieux
 * Contributor(s):
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.ejb21;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJBLocalHome;
import javax.ejb.RemoveException;
import javax.naming.NamingException;
import javax.naming.Reference;
import javax.naming.StringRefAddr;

import org.ow2.jonas.deployment.ejb.BeanDesc;


import org.objectweb.util.monolog.api.BasicLevel;

/**
 * This class represents an EJBLocalHome It is shared between Sessions and
 * Entities.
 * @author Philippe Durieux
 */
public abstract class JLocalHome implements EJBLocalHome {

    protected BeanDesc dd;

    protected JFactory bf;

    // The static table of all JLocalHome objects
    protected static Map homeList = new HashMap();

    /**
     * Constructor for the base class of the specific generated Home object.
     * @param dd The Bean Deployment Descriptor
     * @param bf The Bean Factory
     */
    public JLocalHome(BeanDesc dd, JFactory bf) {
        if (TraceEjb.isDebugIc()) {
            TraceEjb.interp.log(BasicLevel.DEBUG, "");
        }
        this.dd = dd;
        this.bf = bf;
    }

    // ---------------------------------------------------------------
    // EJBLocalHome Implementation
    // ---------------------------------------------------------------

    /**
     * Removes an EJB object identified by its primary key.
     * @param primaryKey The Primary Key
     */
    public abstract void remove(Object primaryKey) throws RemoveException;

    // ---------------------------------------------------------------
    // other public methods
    // ---------------------------------------------------------------

    /**
     * @return The JNDI name
     */
    public String getJndiLocalName() {
        return dd.getJndiLocalName();
    }

    /**
     * register this bean to JNDI (rebind) We register actually a Reference
     * object.
     */
    protected void register() throws NamingException {
        if (TraceEjb.isDebugIc()) {
            TraceEjb.interp.log(BasicLevel.DEBUG, "");
        }
        String name = dd.getJndiLocalName();

        // Keep it in the static list for later retrieving.
        homeList.put(name, this);

        Reference ref = new Reference("org.ow2.jonas.lib.ejb21.JLocalHome",
                "org.ow2.jonas.lib.ejb21.HomeFactory", null);
        ref.add(new StringRefAddr("bean.name", name));
        bf.getInitialContext().rebind(name, ref);
    }

    /**
     * unregister this bean in JNDI (unbind)
     */
    protected void unregister() throws NamingException {
        if (TraceEjb.isDebugIc()) {
            TraceEjb.interp.log(BasicLevel.DEBUG, "");
        }
        String name = dd.getJndiLocalName();
        // unregister in default InitialContext
        bf.getInitialContext().unbind(name);
        // remove from the static list
        homeList.remove(name);
    }

    /**
     * Get JLocalHome by its name
     * @param beanName The Bean JNDI local Name
     * @return The Bean LocalHome
     */
    public static JLocalHome getLocalHome(String beanName) {
        if (TraceEjb.isDebugIc()) {
            TraceEjb.interp.log(BasicLevel.DEBUG, beanName);
        }
        JLocalHome lh = (JLocalHome) homeList.get(beanName);
        return lh;
    }

    /**
     * preInvoke is called before any request.
     * @param txa Transaction Attribute (Supports, Required, ...)
     * @return A RequestCtx object
     * @throws EJBException
     */
    public RequestCtx preInvoke(int txa) {
        if (TraceEjb.isDebugIc()) {
            TraceEjb.interp.log(BasicLevel.DEBUG, "");
        }
        return bf.preInvoke(txa);
    }

    /**
     * Check if the access to the bean is authorized
     * @param ejbInv object containing security signature of the method, args of
     *        method, etc
     */
     public void checkSecurity(EJBInvocation ejbInv) {
         if (TraceEjb.isDebugIc()) {
             TraceEjb.interp.log(BasicLevel.DEBUG, "");
         }
         bf.checkSecurity(ejbInv);
     }

    /**
     * postInvoke is called after any request.
     * @param rctx The RequestCtx that was returned at preInvoke()
     */
    public void postInvoke(RequestCtx rctx) {
        if (TraceEjb.isDebugIc()) {
            TraceEjb.interp.log(BasicLevel.DEBUG, "");
        }
        bf.postInvoke(rctx);
    }

}