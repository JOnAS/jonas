/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.ejb21;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import javax.ejb.RemoveException;

import org.ow2.jonas.deployment.ejb.SessionDesc;

import org.objectweb.util.monolog.api.BasicLevel;

/**
 * This class is the Standard Home for Session objects It exists only for beans
 * that have declared a Remote Interface. It implements javax.ejb.EJBHome
 * interface (by the inherited class JHome) It implements a pool of EJBObject's
 * @author Philippe Durieux
 */
public abstract class JSessionHome extends JHome implements Remote {

    /**
     * constructor
     * @param dd The Session Bean Deployment Descriptor
     * @param bf THe Session Bean Factory
     */
    public JSessionHome(SessionDesc dd, JSessionFactory bf) throws RemoteException {
        super(dd, bf);
        if (TraceEjb.isDebugIc()) {
            TraceEjb.interp.log(BasicLevel.DEBUG, "");
        }
    }

    public RequestCtx preInvoke(int txa) throws RemoteException {
        // The connection List depends on the instance, so it must be
        // set to null in case of Home method (create).
        // only for stateful
        bf.getTransactionManager().pushConnectionList(null);
        return super.preInvoke(txa);
    }

    public void postInvoke(RequestCtx rctx) throws RemoteException {
        super.postInvoke(rctx);
        // save the connection List for future methods on the bean.
        // only for stateful
        List newList = bf.getTransactionManager().popConnectionList();
        JSessionContext ctx = (JSessionContext) rctx.ejbContext;
        if (ctx != null) {
            ctx.setConnectionList(newList);
        }
    }

    // ---------------------------------------------------------------
    // EJBHome implementation
    // remove(Handle) method is in the generated part.
    // other methods are in JHome (identical for Sessions and Entities)
    // ---------------------------------------------------------------

    /**
     * remove(pk) is not allowed for session beans
     * @param pk the primary key
     * @throws RemoveException Always.
     */
    public void remove(java.lang.Object pk) throws RemoteException, RemoveException {
        throw new RemoveException("remove by PK Cannot be called in a session bean");
    }

    // ---------------------------------------------------------------
    // other public methods, for internal use.
    // ---------------------------------------------------------------

    /**
     * Creates the EJBObject (remote) this is in the generated class because it
     * is mainly "new objectClass()"
     * @return A JSessionRemote object
     */
    public abstract JSessionRemote createRemoteObject() throws RemoteException;

}
