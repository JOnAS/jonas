/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.ejb21;

import java.io.Serializable;
import java.rmi.RemoteException;

import javax.ejb.EJBObject;
import javax.ejb.Handle;

import org.objectweb.util.monolog.api.BasicLevel;

/**
 * This class implements javax.ejb.Handle interface. For a Session Bean a Handle
 * is directly its RMI Reference because its life time is limited by the JOnAS
 * Server life time. (no need to retrieve it after the JOnAS Server has been
 * restarted)
 * @author Philippe Coq
 */
public class JSessionHandle implements Handle, Serializable {

    /**
     * @serial
     */
    private EJBObject ejbO = null;

    /**
     * constructor
     * @param jb the EJBObject represented by this handle.
     */
    public JSessionHandle(EJBObject jb) {
        if (TraceEjb.isDebugIc()) {
            TraceEjb.interp.log(BasicLevel.DEBUG, "");
        }
        ejbO = jb;
    }

    /**
     * @return the EJBObject represented by this handle.
     * @throws RemoteException e The EJB object could not be obtained because of
     *         a system-level failure.
     */
    public EJBObject getEJBObject() throws RemoteException {
        return ejbO;
    }
}