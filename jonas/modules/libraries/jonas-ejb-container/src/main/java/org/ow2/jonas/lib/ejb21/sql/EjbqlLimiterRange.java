/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.lib.ejb21.sql;

/**
 * Limiter range representation that may be a literal integer or a parameter
 * @author Helene Joanin
 */
public class EjbqlLimiterRange {

    /**
     * None kind
     */
    public static final int KIND_NONE = -1;

    /**
     * Literal integer kind
     */
    public static final int KIND_LITERAL = 1;

    /**
     * Parameter kind
     */
    public static final int KIND_PARAMETER = 2;

    /**
     * Kind of limit range
     */
    private int kind = KIND_NONE;

    /**
     * Value of limit range: value of literal or number of parameter
     */
    private int value;

    /**
     * Constructs an EjbqlLimitRange
     * @param kind kind of limiter range
     * @param value value of limiter range (value of literal or number of parameter)
     */
    public EjbqlLimiterRange(int kind, int value) {
        this.kind = kind;
        this.value = value;
    }

    /**
     * @return returns the kind of limiter range, literal or parameter
     */
    public int getKind() {
        return kind;
    }

    /**
     * @return returns the value of the limiter range (value of literal or number of parameter)
     */
    public int getValue() {
        return value;
    }

}