/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Sebastien Chassande
 * Contributor(s):
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.lib.ejb21.jorm;

import org.objectweb.jorm.api.PIndexedElem;
import org.objectweb.jorm.api.PException;
import org.objectweb.jorm.naming.api.PName;
import org.objectweb.jorm.type.api.PExceptionTyping;
import org.objectweb.util.monolog.api.BasicLevel;

import java.util.Date;
import java.io.Serializable;
import java.math.BigDecimal;

import org.ow2.jonas.lib.ejb21.TraceEjb;

/**
 * This class is a basic implementation of the PIndexedElem interface used by
 * the GenClassImpl class. This implementation manages only references (no
 * primitive elements).
 *
 * @author S.Chassande-Barrioz
 */
public class GenClassElement implements PIndexedElem {

    /**
     * This field describes the status of the PIndexedElem in according to the
     * constant defined in the PIndexedElem interface.
     */
    public byte status = PIndexedElem.ELEM_UNMODIFIED;

    public boolean hasBeenCreated = false;

    /**
     * This field references the associated pname. This field is never null
     * after data has been loaded.
     */
    public PName pname = null;

    /**
     * This field references the user object. This is a PObject which permits
     * to reach its PName.
     */
    public PObject value = null;

    /**
     * This field references the GenClass inside which the PIndexedElem is
     * store.
     */
    public GenClassImpl gc = null;


    public GenClassElement(GenClassImpl gc) {
        this.gc = gc;
    }

    // IMPLEMENTATION OF THE PIndexedElem INTERFACE //
    //----------------------------------------------//

    public byte getElemStatus() {
        return status;
    }

    public PName pieGetRefElem() throws PException {
        if (pname == null) {
            pname = gc.gcObject2ref(value);
        }
        if (TraceEjb.genclass.isLoggable(BasicLevel.DEBUG)) {
            TraceEjb.genclass.log(BasicLevel.DEBUG, "pn=" + pname);
        }
        return pname;
    }
    /**
     * The jorm assignes the PName of the referenced object
     */
    public void pieSetRefElem(PName pn) throws PException {
        pname = pn;
        value = null;
        if (TraceEjb.genclass.isLoggable(BasicLevel.DEBUG)) {
            TraceEjb.genclass.log(BasicLevel.DEBUG, "pn=" + pname);
        }
    }

    public boolean pieGetBooleanElem() throws PException {
        throw new PExceptionTyping("Bad elem type: asked: boolean, been: object");
    }
    public Boolean pieGetObooleanElem() throws PException {
        throw new PExceptionTyping("Bad elem type: asked: Boolean, been: object");
    }
    public byte pieGetByteElem() throws PException {
        throw new PExceptionTyping("Bad elem type: asked: byte, been: object");
    }
    public Byte pieGetObyteElem() throws PException {
        throw new PExceptionTyping("Bad elem type: asked: Byte, been: object");
    }
    public byte pieGetByteIndexField(String fn) throws PException {
        throw new PExceptionTyping("No index");
    }
    public Byte pieGetObyteIndexField(String fn) throws PException {
        throw new PExceptionTyping("No index");
    }
    public char pieGetCharElem() throws PException {
        throw new PExceptionTyping("Bad elem type: asked: char, been: object");
    }
    public Character pieGetOcharElem() throws PException {
        throw new PExceptionTyping("Bad elem type: asked: Char, been: object");
    }
    public char pieGetCharIndexField(String fn) throws PException {
        throw new PExceptionTyping("No index");
    }
    public Character pieGetOcharIndexField(String fn) throws PException {
        throw new PExceptionTyping("No index");
    }
    public short pieGetShortElem() throws PException {
        throw new PExceptionTyping("Bad elem type: asked: short, been: object");
    }
    public Short pieGetOshortElem() throws PException {
        throw new PExceptionTyping("Bad elem type: asked: Short, been: object");
    }
    public short pieGetShortIndexField(String fn) throws PException {
        throw new PExceptionTyping("No index");
    }
    public Short pieGetOshortIndexField(String fn) throws PException {
        throw new PExceptionTyping("No index");
    }
    public int pieGetIntElem() throws PException {
        throw new PExceptionTyping("Bad elem type: asked: int, been: object");
    }
    public Integer pieGetOintElem() throws PException {
        throw new PExceptionTyping("Bad elem type: asked: Int, been: object");
    }
    public int pieGetIntIndexField(String fn) throws PException {
        throw new PExceptionTyping("No index");
    }
    public Integer pieGetOintIndexField(String fn) throws PException {
        throw new PExceptionTyping("No index");
    }
    public long pieGetLongElem() throws PException {
        throw new PExceptionTyping("Bad elem type: asked: long, been: object");
    }
    public Long pieGetOlongElem() throws PException {
        throw new PExceptionTyping("Bad elem type: asked: Long, been: object");
    }
    public long pieGetLongIndexField(String fn) throws PException {
        throw new PExceptionTyping("No index");
    }
    public Long pieGetOlongIndexField(String fn) throws PException {
        throw new PExceptionTyping("No index");
    }
    public float pieGetFloatElem() throws PException {
        throw new PExceptionTyping("Bad elem type: asked: float, been: object");
    }
    public Float pieGetOfloatElem() throws PException {
        throw new PExceptionTyping("Bad elem type: asked: Float, been: object");
    }
    public double pieGetDoubleElem() throws PException {
        throw new PExceptionTyping("Bad elem type: asked: double, been: object");
    }
    public Double pieGetOdoubleElem() throws PException {
        throw new PExceptionTyping("Bad elem type: asked: Double, been: object");
    }
    public String pieGetStringElem() throws PException {
        throw new PExceptionTyping("Bad elem type: asked: String, been: object");
    }
    public String pieGetStringIndexField(String fn) throws PException {
        return null;
    }
    public Date pieGetDateElem() throws PException {
        throw new PExceptionTyping("Bad elem type: asked: java.util.Date, been: object");
    }
    public Date pieGetDateIndexField(String fn) throws PException {
        throw new PExceptionTyping("No index");
    }
    public char[] pieGetCharArrayElem() throws PException {
        throw new PExceptionTyping("Bad elem type: asked: char[], been: object");
    }
    public byte[] pieGetByteArrayElem() throws PException {
        throw new PExceptionTyping("Bad elem type: asked: byte[], been: object");
    }
    public Serializable pieGetSerializedElem() throws PException {
        throw new PExceptionTyping("Bad elem type: asked: Serializable, been: object");
    }
    public void pieSetBooleanElem(boolean value) throws PException {
        throw new PExceptionTyping("Bad elem type: asked: boolean, been: object");
    }
    public void pieSetObooleanElem(Boolean value) throws PException {
        throw new PExceptionTyping("Bad elem type: asked: boolean, been: object");
    }
    public void pieSetByteElem(byte value) throws PException {
        throw new PExceptionTyping("Bad elem type: asked: byte, been: object");
    }
    public void pieSetObyteElem(Byte value) throws PException {
        throw new PExceptionTyping("Bad elem type: asked: Byte, been: object");
    }
    public void pieSetByteIndexField(String fn, byte value) throws PException {
        throw new PExceptionTyping("No index");
    }
    public void pieSetObyteIndexField(String fn, Byte value) throws PException {
        throw new PExceptionTyping("No index");
    }
    public void pieSetCharElem(char value) throws PException {
        throw new PExceptionTyping("Bad elem type: asked: char, been: object");
    }
    public void pieSetOcharElem(Character value) throws PException {
        throw new PExceptionTyping("Bad elem type: asked: Char, been: object");
    }
    public void pieSetCharIndexField(String fn, char value) throws PException {
        throw new PExceptionTyping("No index");
    }
    public void pieSetOcharIndexField(String fn, Character value) throws PException {
        throw new PExceptionTyping("No index");
    }
    public void pieSetShortElem(short value) throws PException {
        throw new PExceptionTyping("Bad elem type: asked: short, been: object");
    }
    public void pieSetOshortElem(Short value) throws PException {
        throw new PExceptionTyping("Bad elem type: asked: Short, been: object");
    }
    public void pieSetShortIndexField(String fn, short value) throws PException {
        throw new PExceptionTyping("No index");
    }
    public void pieSetOshortIndexField(String fn, Short value) throws PException {
        throw new PExceptionTyping("No index");
    }
    public void pieSetIntElem(int value) throws PException {
        throw new PExceptionTyping("Bad elem type: asked: int, been: object");
    }
    public void pieSetOintElem(Integer value) throws PException {
        throw new PExceptionTyping("Bad elem type: asked: Int, been: object");
    }
    public void pieSetIntIndexField(String fn, int value) throws PException {
        throw new PExceptionTyping("No index");
    }
    public void pieSetOintIndexField(String fn, Integer value) throws PException {
        throw new PExceptionTyping("No index");
    }
    public void pieSetLongElem(long value) throws PException {
        throw new PExceptionTyping("Bad elem type: asked: long, been: object");
    }
    public void pieSetOlongElem(Long value) throws PException {
        throw new PExceptionTyping("Bad elem type: asked: Long, been: object");
    }
    public void pieSetLongIndexField(String fn, long value) throws PException {
        throw new PExceptionTyping("No index");
    }
    public void pieSetOlongIndexField(String fn, Long value) throws PException {
        throw new PExceptionTyping("No index");
    }
    public void pieSetFloatElem(float value) throws PException {
        throw new PExceptionTyping("Bad elem type: asked: float, been: object");
    }
    public void pieSetOfloatElem(Float value) throws PException {
        throw new PExceptionTyping("Bad elem type: asked: Float, been: object");
    }
    public void pieSetDoubleElem(double value) throws PException {
        throw new PExceptionTyping("Bad elem type: asked: double, been: object");
    }
    public void pieSetOdoubleElem(Double value) throws PException {
        throw new PExceptionTyping("Bad elem type: asked: Double, been: object");
    }
    public void pieSetStringElem(String value) throws PException {
        throw new PExceptionTyping("Bad elem type: asked: String, been: object");
    }
    public void pieSetStringIndexField(String fn, String value) throws PException {
        throw new PExceptionTyping("No index");
    }
    public void pieSetDateElem(Date value) throws PException {
        throw new PExceptionTyping("Bad elem type: asked: java.util.Date, been: object");
    }
    public void pieSetDateIndexField(String fn, Date value) throws PException {
        throw new PExceptionTyping("No index");
    }
    public void pieSetCharArrayElem(char[] value) throws PException {
        throw new PExceptionTyping("Bad elem type: asked: char[], been: object");
    }
    public void pieSetByteArrayElem(byte[] value) throws PException {
        throw new PExceptionTyping("Bad elem type: asked: byte[], been: object");
    }
    public void pieSetSerializedElem(Serializable value) throws PException {
        throw new PExceptionTyping("Bad elem type: asked: Serializable, been: object");
    }
    public BigDecimal pieGetBigDecimalElem() throws PException {
        throw new PExceptionTyping("Bad elem type: asked: BigDecimal, been: object");
    }
    public void pieSetBigDecimalElem(BigDecimal value) throws PException {
        throw new PExceptionTyping("Bad elem type: asked: BigDecimal, been: object");
    }
    public java.math.BigInteger pieGetBigIntegerElem() throws PException {
        throw new PExceptionTyping("Bad elem type: asked: BigInteger, been: object");
    }
    public void pieSetBigIntegerElem(java.math.BigInteger value) throws PException {
        throw new PExceptionTyping("Bad elem type: asked: BigInteger, been: object");
    }
}
