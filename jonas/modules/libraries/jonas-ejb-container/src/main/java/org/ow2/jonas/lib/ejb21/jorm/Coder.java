/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Sebastien Chassande
 * Contributor(s):
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.ejb21.jorm;

import org.objectweb.jorm.api.PException;
import org.objectweb.jorm.naming.api.PName;

/**
 * This interface specifies two methods to translate a PName into a PK or the
 * opposite action.
 *
 * @author Sebastien Chassande-Barrioz
 */
public interface Coder {

    /**
     * This method build a PName with the primary key specifed in parameter.
     * @param pk the primary key
     * @return the PName
     * @throws PException the operation has failed
     * @throws UnsupportedOperationException operation not supported.
     */
    PName decode(Object pk) throws PException, UnsupportedOperationException;

    /**
     * This method build a primary key with the PName specifed in parameter.
     * @param pn the Jorm PName
     * @return the primary key
     * @throws PException the operation has failed
     * @throws UnsupportedOperationException operation not supported.
     */
    Object encode(PName pn) throws PException, UnsupportedOperationException;
}
