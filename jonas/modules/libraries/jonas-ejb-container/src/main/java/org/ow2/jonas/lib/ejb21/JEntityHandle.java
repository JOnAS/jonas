/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.ejb21;

import java.io.Serializable;
import java.rmi.RemoteException;

import javax.ejb.EJBObject;
import javax.ejb.Handle;
import javax.ejb.HomeHandle;

import org.objectweb.util.monolog.api.BasicLevel;

/**
 * This class implements javax.ejb.Handle interface. For a Entity Bean a Handle
 * is a serializable class that contains the HomeHandle and the primary key
 * @author Philippe Coq
 */
public abstract class JEntityHandle implements Handle, Serializable {

    /**
     * @serial
     */
    protected HomeHandle homehandle = null;

    /**
     * @serial
     */
    protected Serializable pk = null;

    /**
     * constructor
     * @param remote The Remote object
     */
    public JEntityHandle(JEntityRemote remote) {
        try {
            homehandle = remote.getEJBHome().getHomeHandle();
        } catch (RemoteException e) {
            TraceEjb.logger.log(BasicLevel.ERROR, "cannot get HomeHandle: ", e);
        }
    }

    /**
     * Obtains the EJB object represented by this handle.
     * @return The EJB object
     * @throws RemoteException The EJB object could not be obtained because of a
     *         system-level failure.
     */
    public abstract EJBObject getEJBObject() throws RemoteException;

    /**
     * @return the Primary Key embedded in the Handle
     */
    public java.lang.Object getPK() {
        return pk;
    }
}

