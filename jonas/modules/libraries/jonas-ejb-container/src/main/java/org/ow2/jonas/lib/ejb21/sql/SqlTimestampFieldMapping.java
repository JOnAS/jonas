/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.ejb21.sql;

/**
 * Conversion java.util.Date <-> java.sql.Timestamp. <br>
 * The StorageType is the JORM type, and the MemoryType is the JOnAS type (bean field type).
 * @author Helene Joanin
 */
public class SqlTimestampFieldMapping {

    /**
     * Retrieves the java type corresponding to the type into the data support.
     * @return a Class object (never null).
     */
    public static Class getStorageType() {
        return java.util.Date.class;
    }

    /**
     * Retrieves the java type corresponding to the type in memory.
     * @return a Class object (never null).
     */
    public static Class getMemoryType() {
        return java.sql.Timestamp.class;
    }

    /**
     * Converts a value from the data support into a value in memory
     * @param storagevalue is the value store in the support (can be null).
     * @return the value in memory (can be null).
     */
    public static Object toMemory(Object storagevalue) {
        if (storagevalue == null) {
            return null;
        }
        return (storagevalue instanceof java.sql.Timestamp
                ? storagevalue
                : new java.sql.Timestamp(((java.util.Date) storagevalue).getTime()));
    }

    /**
     * Converts a value from the data support into a value in memory
     * @param memoryvalue the value in memory (can be null).
     * @return is the value store in the support (can be null).
     */
    public static Object toStorage(Object memoryvalue) {
        return memoryvalue;
    }
}
