/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s):
 * Contributor(s):
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.lib.ejb21.jorm;

import javax.resource.cci.ConnectionFactory;
import javax.sql.DataSource;

import org.ow2.jonas.deployment.ejb.EntityDesc;
import org.ow2.jonas.lib.ejb21.JContainer;
import org.ow2.jonas.lib.ejb21.TraceEjb;


import org.objectweb.jorm.api.PClassMapping;
import org.objectweb.jorm.api.PException;
import org.objectweb.jorm.lib.MapperJCA;
import org.objectweb.jorm.mapper.rdb.genclass.RdbGenClassProp;
import org.objectweb.jorm.mapper.rdb.lib.MapperJDBC;
import org.objectweb.jorm.mapper.rdb.lib.RdbPrefetchablePCM;

import org.objectweb.util.monolog.api.BasicLevel;

/**
 * @author Sebastien Chassande-Barrioz
 * @author Philippe Durieux (new Jorm mapping)
 */
public abstract class RdbFactory extends MedorFactory {

    public RdbFactory() {
        super();
    }

    public void init(EntityDesc dd, JContainer cont, String mapperName) {
        super.init(dd, cont, mapperName);
    }

    public Object getConnection(Object hints) throws PException {
        if (hints == null) {
            return mapper.getConnection();
        } else {
            return mapper.getConnection(hints);
        }
    }

    public void releaseConnection(Object conn) throws PException {
        mapper.closeConnection(conn);
    }

    protected void setMapper(String mapperName) throws PException {
        MapperManager mm = MapperManager.getInstance();
        synchronized (mm) {
            mapper = mm.getMapper(cont, datasource);
            if (mapper == null) {
                // datasource has been found by its JNDI name in the bean DD
                if (datasource instanceof DataSource) {
                    mapper = new MapperJDBC(mm.getJormConfigurator());
                } else if (datasource instanceof ConnectionFactory) {
                    mapper = new MapperJCA(mm.getJormConfigurator());
                } else {
                    TraceEjb.logger.log(BasicLevel.ERROR, "datasource is neither a DataSource nor a ConnectionFactory:" + datasource);
                    throw new PException("datasource is neither a DataSource nor a ConnectionFactory");
                }
                mapper.setMapperName(mapperName);

                // to redirect getConnection() on our DataSource
                mapper.setConnectionFactory(datasource);

                //register and finish to configure the new mapper
                mm.addMapper(mapper, cont, datasource);
            }
        }
    }

    /**
     * It initializes the prefetching of a genclassMapping with the
     * PClassMapping of the target class of the multivalued CMR.
     * @param gcm is the GenClassMapping to initialized
     * @param targetPCM is the PClassMapping of the target class
     */
    protected void initGenClassPrefetch(PClassMapping gcm, PClassMapping targetPCM) {
        if (gcm instanceof RdbGenClassProp && targetPCM instanceof RdbPrefetchablePCM) {
            ((RdbGenClassProp) gcm).setPrefetchElementPCM((RdbPrefetchablePCM) targetPCM);
        }
    }

}
