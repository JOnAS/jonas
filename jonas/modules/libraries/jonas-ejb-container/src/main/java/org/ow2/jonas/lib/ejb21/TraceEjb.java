/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.ejb21;

import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;
import org.objectweb.util.monolog.api.LoggerFactory;
import org.objectweb.util.monolog.wrapper.printwriter.LoggerImpl;

/**
 * For message logging
 * @author Sebastien Chassande-Barrioz sebastien.chassande@inrialpes.fr
 */

class DummyLogger extends LoggerImpl {

    /**
     * Dummy Logger used when jotm classes are used on client side
     */
    public void log(int level, java.lang.Object o) {
    }
}

public class TraceEjb {

    public static Logger logger = new DummyLogger();
    private static boolean isWarnLogger = false;

    public static Logger query = new DummyLogger();
    private static boolean isDebugQuery = false;

    public static Logger interp = new DummyLogger();
    private static boolean isDebugInterp = false;

    public static Logger genclass = new DummyLogger();
    private static boolean isDebugGenclass = false;

    public static Logger coherence = new DummyLogger();
    private static boolean isDebugCoherence = false;

    public static Logger thread = new DummyLogger();
    private static boolean isDebugThread = false;

    public static Logger synchro = new DummyLogger();
    private static boolean isDebugSynchro = false;

    public static Logger ssfpool = new DummyLogger();
    private static boolean isDebugSsfpool = false;

    public static Logger swapper = new DummyLogger();
    private static boolean isDebugSwapper = false;

    public static Logger mapper = new DummyLogger();
    private static boolean isDebugMapper = false;

    public static Logger context = new DummyLogger();
    private static boolean isDebugContext = false;

    public static Logger security = new DummyLogger();
    private static boolean isDebugSecurity = false;

    public static Logger factory = new DummyLogger();
    private static boolean isDebugFactory = false;

    public static Logger mdb = new DummyLogger();
    private static boolean isDebugMdb = false;

    public static Logger tx = new DummyLogger();
    private static boolean isDebugTx = false;

    public static Logger txlistener = new DummyLogger();
    private static boolean isDebugTxlistener = false;

    public static Logger dd = new DummyLogger();
    private static boolean isDebugDd = false;

    public static Logger loaderlog = new DummyLogger();
    private static boolean isDebugLoaderlog = false;


    public static final String prefix = "org.ow2.jonas.lib.ejb21";

    public static LoggerFactory loggerFactory = null;

    /**
     * Configure loggers for monolog
     * @param lf the logger factory
     */
    public static void configure(LoggerFactory lf) {
        loggerFactory = lf;
        logger = lf.getLogger(prefix);
        interp = lf.getLogger(prefix + ".interp");
        query = lf.getLogger(prefix + ".query");
        genclass = lf.getLogger(prefix + ".genclass");
        coherence = lf.getLogger(prefix + ".coherence");
        thread = lf.getLogger(prefix + ".thread");
        synchro = lf.getLogger(prefix + ".synchro");
        ssfpool = lf.getLogger(prefix + ".ssfpool");
        swapper = lf.getLogger(prefix + ".swapper");
        mapper = lf.getLogger(prefix + ".mapper");
        context = lf.getLogger(prefix + ".context");
        security = lf.getLogger(prefix + ".security");
        factory = lf.getLogger(prefix + ".factory");
        mdb = lf.getLogger(prefix + ".mdb");
        tx = lf.getLogger(prefix + ".tx");
        txlistener = lf.getLogger(prefix + ".txlistener");
        dd = lf.getLogger(prefix + ".dd");
        loaderlog = lf.getLogger("org.ow2.jonas.loader");
        syncLevels();
    }

    /**
     * Sets booleans which enable debugging (debug level or Warn)
     */
    public static void syncLevels() {
        // Warn
        isWarnLogger = logger.isLoggable(BasicLevel.WARN);

        // debug
        isDebugQuery = query.isLoggable(BasicLevel.DEBUG);
        isDebugInterp = interp.isLoggable(BasicLevel.DEBUG);
        isDebugGenclass = genclass.isLoggable(BasicLevel.DEBUG);
        isDebugCoherence = coherence.isLoggable(BasicLevel.DEBUG);
        isDebugThread = thread.isLoggable(BasicLevel.DEBUG);
        isDebugSynchro  = synchro.isLoggable(BasicLevel.DEBUG);
        isDebugSsfpool = ssfpool.isLoggable(BasicLevel.DEBUG);
        isDebugSwapper = swapper.isLoggable(BasicLevel.DEBUG);
        isDebugMapper = mapper.isLoggable(BasicLevel.DEBUG);
        isDebugContext = context.isLoggable(BasicLevel.DEBUG);
        isDebugSecurity = security.isLoggable(BasicLevel.DEBUG);
        isDebugFactory = factory.isLoggable(BasicLevel.DEBUG);
        isDebugMdb = mdb.isLoggable(BasicLevel.DEBUG);
        isDebugTx = tx.isLoggable(BasicLevel.DEBUG);
        isDebugTxlistener = txlistener.isLoggable(BasicLevel.DEBUG);
        isDebugDd = dd.isLoggable(BasicLevel.DEBUG);
        isDebugLoaderlog = loaderlog.isLoggable(BasicLevel.DEBUG);
    }


    public static final boolean isVerbose() {
        return isWarnLogger;
    }

    public static final boolean isDebugThread() {
        return isDebugThread;
    }

    public static final boolean isDebugSynchro() {
        return isDebugSynchro;
    }

    public static final boolean isDebugSsfpool() {
        return isDebugSsfpool;
    }

    public static final boolean isDebugContext() {
        return isDebugContext;
    }

    public static final boolean isDebugSwapper() {
        return isDebugSwapper;
    }

    public static final boolean isDebugMapper() {
        return isDebugMapper;
    }

    public static final boolean isDebugIc() {
        return isDebugInterp;
    }

    public static final boolean isDebugSecurity() {
        return isDebugSecurity;
    }

    public static final boolean isDebugFactory() {
        return isDebugFactory;
    }

    public static final boolean isDebugJms() {
        return isDebugMdb;
    }

    public static final boolean isDebugTx() {
        return isDebugTx;
    }

    public static final boolean isDebugTxlistener() {
        return isDebugTxlistener;
    }

    public static final boolean isDebugDd() {
        return isDebugDd;
    }

    public static final boolean isDebugQuery() {
        return isDebugQuery;
    }

    public static final boolean isDebugLoaderLog() {
        return isDebugLoaderlog;
    }
}
