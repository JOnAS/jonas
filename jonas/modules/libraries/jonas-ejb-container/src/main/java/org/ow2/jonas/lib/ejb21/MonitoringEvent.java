/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.lib.ejb21;

import org.ow2.util.event.api.IEvent;
import org.ow2.util.event.api.IEventListener;

/**
 * implementation of the monitoring event to get statistics for the EJB2 sessions
 * @author Malek Chahine
 */
public class MonitoringEvent implements IEvent {

    private long businessStartTime;
    private long businessStopTime;
    private long totalStartTime;
    private long totalStopTime;
    private int beanFactoryId;
    private String ejbName;
    private String filename;
    private String methodName;

    public MonitoringEvent(final int beanFactoryId, final String ejbName, final String filename, final String methodName,
        final long businessStartTime, final long businessStopTime, final long totalStartTime, final long totalStopTime) {
        this.businessStartTime = businessStartTime;
        this.businessStopTime = businessStopTime;
        this.totalStartTime = totalStartTime;
        this.totalStopTime = totalStopTime;
        this.beanFactoryId = beanFactoryId;
        this.ejbName = ejbName;
        this.filename = filename;
        this.methodName = methodName;
    }

    public long getBusinessStartTime() {
        return businessStartTime;
    }

    public long getBusinessStopTime() {
        return businessStopTime;
    }

    public long getTotalStartTime() {
        return totalStartTime;
    }

    public long getTotalStopTime() {
        return totalStopTime;
    }

    public long getBeanFactoryId() {
        return beanFactoryId;
    }

    public String getEJBName() {
        return ejbName;
    }

    public String getFilename() {
        return filename;
    }

    public String getMethodName() {
        return methodName;
    }
    
    /**
     * Check whether the event listener has permission to receive this event.
     * @param eventListener The event listener to check.
     * @return True if the listener has the permission, false otherwise.
     */
    public boolean checkPermission(final IEventListener eventListener) {
        return true;
    }
}
