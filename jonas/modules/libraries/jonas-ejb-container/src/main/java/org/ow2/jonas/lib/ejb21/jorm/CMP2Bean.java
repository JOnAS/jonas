/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.lib.ejb21.jorm;

import java.util.ArrayList;

import org.ow2.jonas.deployment.ejb.EntityJdbcCmp2Desc;


import org.objectweb.jorm.metainfo.api.MetaObject;

/**
 * This Class represents a CMP2 EntityBean, mapped on Jorm.
 * @author durieuxp
 */
public class CMP2Bean {

    private ArrayList jormMOList = new ArrayList();

    private String ejbName;

    public CMP2Bean(EntityJdbcCmp2Desc bd) {
        ejbName = bd.getEjbName();
    }

    /**
     * add a Jorm MetaObject in the list of Jorm Meta Objects to generate
     * @param jobject the Jorm MetaObject to add in the list
     */
    public void addToJormList(MetaObject jobject) {
        jormMOList.add(jobject);
    }

    public ArrayList getJormList() {
        return jormMOList;
    }

    public String getName() {
        return ejbName;
    }

}