/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Sebastien Chassande
 * Contributor(s):
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.lib.ejb21.jorm;

import org.objectweb.jorm.api.PException;

/**
 * This class is a specialization of the GenClassElement with an index to
 * distinguish the dupicated element.
 *
 * @author S.Chassande-Barrioz
 */
public class CollectionElement extends GenClassElement {

    private int index = -1;

    /**
     * It builds a CollectionElement with the specified index.
     */
    public CollectionElement(GenClassImpl gc, int idx) {
        super(gc);
        index = idx;
    }

    /**
     * There is a single index, then the index field name is not used
     */
    public int pieGetIntIndexField(String fn) throws PException {
        return index;
    }

    /**
     * There is a single index, then the index field name is not used
     */
    public void pieSetIntIndexField(String fn, int value) throws PException {
        index = value;
    }
}
