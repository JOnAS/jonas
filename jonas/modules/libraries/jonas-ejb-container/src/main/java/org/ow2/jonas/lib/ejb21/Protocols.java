/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Red Hat
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.ejb21;

import java.util.Iterator;
import java.util.SortedSet;
import java.util.StringTokenizer;
import java.util.TreeSet;

/**
 * Class used to manage protocol names
 */

public class Protocols {

    /**
     * JRMP protocol
     */
    public static final String RMI_JRMP = "jrmp";

    /**
     * JRMP protocol
     */
    public static final String RMI_IRMI = "irmi";

    /**
     * IIOP protocol
     */
    public static final String RMI_IIOP = "iiop";

    /**
     * CMI protocol
     */
    public static final String CMI_RMI = "cmi";

    /**
     * List of protocols
     */
    private SortedSet protocolNames = new TreeSet();

    /**
     * Build a new object to check protocols
     * @param pns names of protocols
     */
    public Protocols(String pns) {
        this(pns, false);
    }

    public Protocols(String pns, boolean configure) {
        if (pns != null && pns.length() > 0) {
            StringTokenizer st = new StringTokenizer(pns, ",", false);
            while (st.hasMoreTokens()) {
                protocolNames.add(st.nextToken().trim());
            }
        }

        if (configure) {
            if (protocolNames.size() == 0) {
                protocolNames.add(RMI_JRMP);
            }

            if (protocolNames.contains(CMI_RMI)
                || protocolNames.contains(RMI_IRMI)) {
                protocolNames.add(RMI_JRMP);
            }

            if (protocolNames.contains(RMI_JRMP)) {
                protocolNames.add(RMI_IRMI);
            }
        }
    }

    /**
     * Return true if the given protocol is supported
     * @param pn protocol name
     * @return true if the given protocol is supported
     */
    public boolean isSupported(String pn) {
        return protocolNames.contains(pn) ||
            (pn.equals(RMI_IRMI) && protocolNames.contains(RMI_JRMP)) ||
            (pn.equals(RMI_JRMP) && protocolNames.contains(RMI_IRMI));
    }

    /**
     * Return true if the given protocol is supported
     * @param pns protocol names
     * @return true if the given protocol is supported
     */
    public boolean isSupported(Protocols pns) {
        for (Iterator it = pns.iterator(); it.hasNext(); ) {
            if (isSupported((String) it.next())) {
                return true;
            }
        }

        return false;
    }

    public Iterator iterator() {
        return protocolNames.iterator();
    }

    /**
     * Return comma-separated list of protocol names
     */
    public String list() {
        StringBuffer buf = new StringBuffer();
        for (Iterator it = iterator(); it.hasNext(); ) {
            buf.append(it.next());
            if (it.hasNext()) {
                buf.append(",");
            }
        }

        return buf.toString();
    }

    public String toString() {
        return list();
    }
}
