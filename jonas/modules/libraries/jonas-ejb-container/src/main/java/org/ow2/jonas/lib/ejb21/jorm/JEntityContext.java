/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Sebastien Chassande
 * Contributor(s):
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.lib.ejb21.jorm;

import javax.ejb.EntityBean;

import org.ow2.jonas.lib.ejb21.JEntityFactory;

import org.objectweb.jorm.naming.api.PName;

/**
 * This class extends the JEntityContext of the JOnAS container in order to take
 * in consideration the fact that the container manages PName instances and not
 * primary keys. This is the reason why the getPrimaryKey method is overriden.
 *
 * @author Sebastien Chassande-Barrioz
 */
public class JEntityContext
    extends org.ow2.jonas.lib.ejb21.JEntityContext {

    public JEntityContext(JEntityFactory bf, EntityBean eb) {
        super(bf, eb);
    }

    /**
     * Obtains the primary key of the EJB object that is currently associated
     *  with this instance.
     * @return  The EJB object currently associated with the instance.
     * @exception  IllegalStateException  Thrown if the instance invokes this
     *             method while the instance is in a state that does not allow
     *             the instance to invoke this method.
     */
    public Object getPrimaryKey() throws IllegalStateException {
        PName pn = (PName) super.getPrimaryKey();
        Coder c = (Coder) bf.getLocalHome();
        if (c==null) {
            c = (Coder) bf.getHome();
        }
        try {
            return c.encode(pn);
        }
        catch (Exception e) {
            throw new IllegalStateException("Inner: Encoding error:"
                                            + e.getMessage());
        }
    }
}
