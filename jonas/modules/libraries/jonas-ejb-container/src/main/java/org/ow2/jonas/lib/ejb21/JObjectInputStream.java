/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.ejb21;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;

/**
 * This class is used when we need a special class loader to
 * resolve the class.
 * (The default class loader is the System Class Loader)
 * @author durieuxp
 */
public class JObjectInputStream extends ObjectInputStream {

    /**
     * Class Loader to be used
     */
    private ClassLoader loader;

    /**
     * Constructor
     * @param in input stream
     * @param loader class loader to be used for resolveClass
     * @throws java.io.IOException
     */
    public JObjectInputStream(InputStream in, ClassLoader cl) throws java.io.IOException {
        super(in);
        loader = cl;
    }

    /**
     * Use the thread context class loader to resolve the class
     * @param v class to resolve
     * @return class resolved
     * @throws java.io.IOException thrown by the underlying InputStream.
     * @throws ClassNotFoundException thrown by the underlying InputStream.
     */
    protected Class resolveClass(ObjectStreamClass v) throws IOException, ClassNotFoundException {
       return loader.loadClass(v.getName());
    }

}