/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.ejb21;

import java.util.Hashtable;
import java.util.List;

import javax.ejb.EJBContext;
import javax.naming.Context;
import javax.transaction.Transaction;

import org.objectweb.util.monolog.api.BasicLevel;

/**
 * Context associated to a request to a bean.
 * @author Philippe Coq, Philippe Durieux
 */
public class RequestCtx {

    /**
     * must commit current Tx at postInvoke
     */
    public boolean mustCommit = false;

    /**
     * system exception or error raised in business method
     */
    public Throwable sysExc = null;

    /**
     * Tx to be resumed at postInvoke
     */
    public Transaction clientTx = null;

    /**
     * saved JNDICtx to be resumed at postInvoke
     */
    public Context jndiCtx = null;

    /**
     * saved class loader to be resumed at postInvoke
     */
    public ClassLoader cloader = null;

    /**
     * transaction in which the request will execute
     */
    public Transaction currTx = null;

    /**
     * Used for stateful session at create and for stateless session beans
     */
    public JSessionContext ejbContext = null;

    /**
     * ejb business method has been called
     */
    public boolean bmcalled = false;

    /**
     * Transaction Attribute
     */
    public int txAttr = 0;

    /**
     * Holds the state of the SFSB before calling the method (in preinvoke)
     * COMPLETE Added for the replication mechanishm in HA service 
     */
    public byte[] state;
    
    /**
     * constructor. Called at preInvoke.
     * @param txa Transaction Attribute.
     */
    public RequestCtx(int txa) {
        if (TraceEjb.isDebugIc()) {
            TraceEjb.interp.log(BasicLevel.DEBUG, "");
        }
        this.txAttr = txa;
    }

}
