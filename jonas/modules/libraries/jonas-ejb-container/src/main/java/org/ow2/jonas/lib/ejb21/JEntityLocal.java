/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.ejb21;

import javax.ejb.EJBException;
import javax.ejb.EJBLocalHome;
import javax.ejb.EJBLocalObject;
import javax.ejb.RemoveException;


import org.objectweb.util.monolog.api.BasicLevel;

/**
 * Generic part of the EJBLocalObject implementation
 * @author Philippe Durieux
 */
public abstract class JEntityLocal extends JLocal {

    protected JEntityFactory bf;

    protected JEntitySwitch bs;

    /**
     * constructor
     * @param bf The Entity Factory
     */
    public JEntityLocal(JEntityFactory bf) {
        super(bf);
        if (TraceEjb.isDebugIc()) {
            TraceEjb.interp.log(BasicLevel.DEBUG, "");
        }
        this.bf = bf;
    }

    /**
     * finish initialization
     * @param bs The Entity Bean Switch
     */
    public void setEntitySwitch(JEntitySwitch bs) {
        if (TraceEjb.isDebugIc()) {
            TraceEjb.interp.log(BasicLevel.DEBUG, "");
        }
        this.bs = bs;
    }

    // --------------------------------------------------------------------------
    // EJBLocalObject implementation
    // remove() is implemented in the generated part.
    // --------------------------------------------------------------------------

    /**
     * Remove this instance.
     * @throws RemoveException Instance could not be removed.
     */
    public abstract void remove() throws RemoveException;

    /**
     * @return the enterprise Bean's local home interface.
     */
    public EJBLocalHome getEJBLocalHome() {
        if (TraceEjb.isDebugIc()) {
            TraceEjb.interp.log(BasicLevel.DEBUG, "");
        }
        return bf.getLocalHome();
    }

    /**
     * @return the Primary Key for this EJBLocalObject
     */
    public Object getPrimaryKey() {
        if (TraceEjb.isDebugIc()) {
            TraceEjb.interp.log(BasicLevel.DEBUG, "");
        }
        if (bs == null) {
            throw new EJBException("No Primary Key yet");
        }
        return bs.getPrimaryKey();
    }

    /**
     * Tests if a given EJB is identical to the invoked EJB object.
     * @param obj - An object to test for identity with the invoked object.
     * @return True if the given EJB object is identical to the invoked object.
     * @throws EJBException Thrown when the method failed due to a system-level
     *         failure.
     */
    public boolean isIdentical(EJBLocalObject obj) {
        if (TraceEjb.isDebugIc()) {
            TraceEjb.interp.log(BasicLevel.DEBUG, "");
        }
        boolean ret = false;
        if (obj != null) {
            // Get the home class name
            String homeClassName = getEJBLocalHome().getClass().getName();
            String objHomeClassName = obj.getEJBLocalHome().getClass().getName();

            // Tests the home equality and the primary key equality
            ret = ((objHomeClassName.equals(homeClassName)) && (obj.getPrimaryKey().equals(getPrimaryKey())));
        }
        return ret;
    }

    /**
     * preInvoke is called before any request.
     * @param txa Transaction Attribute (Supports, Required, ...)
     * @return A RequestCtx object
     */
    public RequestCtx preInvoke(int txa) {
        if (TraceEjb.isDebugIc()) {
            TraceEjb.interp.log(BasicLevel.DEBUG, "");
        }
        return bf.preInvoke(txa);
    }

    /**
     * Check if the access to the bean is authorized
     * @param ejbInv object containing security signature of the method, args of
     *        method, etc
     */
     public void checkSecurity(EJBInvocation ejbInv) {
         if (TraceEjb.isDebugIc()) {
             TraceEjb.interp.log(BasicLevel.DEBUG, "");
         }
         bf.checkSecurity(ejbInv);
     }

    /**
     * postInvoke is called after any request.
     * @param rctx The RequestCtx that was returned at preInvoke()
     */
    public void postInvoke(RequestCtx rctx) {
        if (TraceEjb.isDebugIc()) {
            TraceEjb.interp.log(BasicLevel.DEBUG, "");
        }
        try {
            bf.postInvoke(rctx);
        } finally {
            bs.releaseICtx(rctx.currTx, rctx.sysExc != null);
        }
    }

    /**
     * Get the JWrapper for serialization
     * @return the JWrapper
     */
    public abstract JWrapper getJWrapper();
}