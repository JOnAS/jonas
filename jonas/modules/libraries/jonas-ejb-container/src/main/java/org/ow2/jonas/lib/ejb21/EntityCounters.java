/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.ejb21;

/**
 * This class is used by JMX to return a dump of instance's current use
 * @author Philippe Durieux
 */
public class EntityCounters {

    /**
     * nb of instances currently used inside a tx
     */
    public int inTx = 0;

    /**
     * nb of instances currently used outside a tx
     */
    public int outTx = 0;

    /**
     * nb of instances not currently used, but active
     */
    public int idle = 0;

    /**
     * nb of instances passivated
     */
    public int passive = 0;

    /**
     * nb of instances marked removed
     */
    public int removed = 0;
    
    /**
     * nb of pks
     */
    public int pk = 0;

    public int getMemoryInstanceNb() {
        return idle + inTx + outTx;
    }
}
