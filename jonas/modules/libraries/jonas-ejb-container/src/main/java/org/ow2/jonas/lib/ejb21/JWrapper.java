/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

 package org.ow2.jonas.lib.ejb21;

import java.io.Serializable;

 /**
  * This class is just a wrapper to encapsulate some fields of a Stateful
  * Session bean when it is serialized.
  * @author durieuxp
  */
 public class JWrapper implements Serializable {

    private static final long serialVersionUID = 18L;

    int type;

    public static final int USER_TX = 1;

    public static final int SESSION_CTX = 2;

    public static final int HANDLE = 3;

    public static final int LOCAL_HOME = 4;

    public static final int LOCAL_ENTITY = 5;

    public static final int NAMING_CONTEXT = 6;

	//public static final int DATASOURCE = 7; // Needed by HA service

    Object obj;
    Object pk;

    public JWrapper(int type, Object arg) {
        this.type = type;
        this.obj = arg;
    }

    public JWrapper(int type, String jndi, Object pk) {
        this.type = type;
        this.obj = jndi;
        this.pk = pk;
    }

    public int getType() {
        return type;
    }

    public Object getObject() {
        return obj;
    }

    public Object getPK() {
        return pk;
    }

}
