/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.lib.ejb21;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.Name;
import javax.naming.Reference;
import javax.naming.spi.ObjectFactory;

import org.objectweb.util.monolog.api.BasicLevel;

/**
 * This Factory return Home or LocalHome objects to clients.
 * No need to use it in case of RMI Remote Home because Remote References are
 * registered directly in JNDI.
 * This class should be used only for local Home or ServiceEndpointHome
 * @author Guillaume Riviere (Inria)
 */
public class HomeFactory implements ObjectFactory {

    /**
     * Used in case of local Home or ServiceEndpointHome
     * @param refObj - The possibly null object containing location or reference
     * information that can be used in creating an object.
     * @param name - The name of this object relative to nameCtx, or null if no name is specified.
     * @param nameCtx - The context relative to which the name parameter is specified,
     * or null if name is relative to the default initial context.
     * @param env - The possibly null environment that is used in creating the object.
     * @return ServiceEndpointHome or LocalHome object
     * @throws Exception - if this object factory encountered an exception while attempting
     * to create an object, and no other object factories are to be tried.
     */
    public Object getObjectInstance(Object refObj, Name name, Context nameCtx, Hashtable env) throws Exception {
        Reference ref = (Reference) refObj;
        String clname = ref.getClassName();

        if (TraceEjb.isDebugIc()) {
            TraceEjb.interp.log(BasicLevel.DEBUG, clname);
        }

        if (clname.equals("org.ow2.jonas.lib.ejb21.JLocalHome")) {
            // Local Home
            String beanName = (String) ref.get("bean.name").getContent();
            JLocalHome localhome = JLocalHome.getLocalHome(beanName);
            if (localhome == null) {
                TraceEjb.logger.log(BasicLevel.ERROR, "cannot get " + beanName);
            }
            return localhome;
        } else if (clname.equals("org.ow2.jonas.lib.ejb21.JServiceEndpointHome")) {
            // Service Endpoint Home
            String beanName = (String) ref.get("bean.name").getContent();
            JServiceEndpointHome sehome = JServiceEndpointHome.getSEHome(beanName);
            if (sehome == null) {
                TraceEjb.logger.log(BasicLevel.ERROR, "cannot get " + beanName);
            }
            return sehome;
        } else {
            TraceEjb.logger.log(BasicLevel.ERROR, "bad class name: " + clname);
        }
        // not found !
        return null;
    }
}
