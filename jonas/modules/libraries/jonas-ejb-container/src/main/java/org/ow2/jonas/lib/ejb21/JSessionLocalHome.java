/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.ejb21;

import javax.ejb.EJBException;
import javax.ejb.RemoveException;

import org.ow2.jonas.deployment.ejb.SessionDesc;


import org.objectweb.util.monolog.api.BasicLevel;

/**
 * This class is the Standard LocalHome for Session objects It exists only for
 * beans that have declared a Local Interface. It implements
 * javax.ejb.EJBLocalHome interface It implements a pool of EJBLocalObject's
 * @author Philippe Durieux
 */
public abstract class JSessionLocalHome extends JLocalHome {

    /**
     * constructor
     * @param dd The Session Deployment Decriptor
     * @param bf The Session Factory
     */
    public JSessionLocalHome(SessionDesc dd, JSessionFactory bf) {
        super(dd, bf);
        if (TraceEjb.isDebugIc()) {
            TraceEjb.interp.log(BasicLevel.DEBUG, "");
        }
    }

    // ---------------------------------------------------------------
    // EJBLocalHome implementation
    // The only method is remove(pk)
    // ---------------------------------------------------------------

    /**
     * remove(pk) is not allowed for session beans
     * @param pk the primary key
     * @throws RemoveException Always.
     */
    public void remove(java.lang.Object pk) throws EJBException, RemoveException {
        throw new RemoveException("remove by PK Cannot be called in a session bean");
    }

    // ---------------------------------------------------------------
    // other public methods, for internal use.
    // ---------------------------------------------------------------

    /**
     * Creates the EJBLocalObject This is in the generated class because it is
     * mainly "new objectClass()"
     * @return The Local Object
     */
    abstract public JSessionLocal createLocalObject();

}