/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.ejb21;

import javax.ejb.EJBException;
import javax.ejb.NoSuchObjectLocalException;
import javax.transaction.SystemException;
import javax.transaction.Transaction;

import org.ow2.jonas.deployment.ejb.EntityDesc;


import org.objectweb.util.monolog.api.BasicLevel;

/**
 * Container Read Uncommitted lock-policy.
 * Only 1 thread can write. All other threads can read, without
 * waiting for a committed state. There is no wait.
 * This policy is deprecated : CRW should be used instead.
 * @author Philippe Durieux
 */
public class JEntitySwitchCRU extends JEntitySwitchCST {

    /**
     * empty constructor. Object is initialized via init() because it is
     * implemented differently according to jorm mappers.
     */
    public JEntitySwitchCRU() {
        lockpolicy = EntityDesc.LOCK_CONTAINER_READ_UNCOMMITTED;
    }

    protected void initpolicy(JEntityFactory bf) {
        lazyregister = true;
    }

    public void waitmyturn(Transaction tx) {
        // No synchro for this policy.
    }

    /**
     * Map a context and its instance.
     * Could use the inherited method here. This is just a simplified
     * version for performances.
     * @param tx - the Transaction object
     * @param bctx - the JEntityContext to bind if not null
     * @param forced - force to take this context. (case of create)
     * @param holdit - increment count to hold it, a release will be called
     *        later.
     * @return JEntityContext actually mapped
     */
    public synchronized JEntityContext mapICtx(Transaction tx, JEntityContext bctx, boolean forced, boolean holdit, boolean notused) {

        waitmyturn(tx);

        // Choose the context to use.
        boolean newtrans = false;
        JEntityContext jec = itContext;
        if (forced) {
            // If the new context is enforced, we must first release the older
            if (jec != null) {
                if (TraceEjb.isDebugContext()) {
                    TraceEjb.context.log(BasicLevel.DEBUG, ident + "new context is enforced!");
                }
                discardContext(tx, false, true);
            }
            jec = bctx;
            itContext = jec;
            jec.initEntityContext(this);
            newtrans = true;
        } else {
            // First check if bean still exists
            if (isremoved) {
                TraceEjb.logger.log(BasicLevel.WARN, ident + " has been removed.");
                throw new NoSuchObjectLocalException("Try to access a bean previously removed");
            }
            if (jec != null) {
                if (todiscard) {
                    TraceEjb.logger.log(BasicLevel.WARN, ident + " has been discarded.");
                    throw new NoSuchObjectLocalException("Try to access a bean previously discarded");
                }
                // Reuse the Context for this transaction.
                // If a context was supplied, release it first.
                if (bctx != null) {
                    bf.releaseJContext(bctx, 2);
                }
                if (runningtx == null) {    // TODO change this checking
                    newtrans = true;
                }
                jec.reuseEntityContext(newtrans);
            } else {
                if (bctx != null) {
                    jec = bctx;
                } else {
                    // no Context available : get one from the pool.
                    jec = (JEntityContext) bf.getJContext(this);
                }
                jec.initEntityContext(this);
                jec.activate(true);
                itContext = jec; // after activate
                newtrans = true;
            }
        }

        if (tx != null) {
            if (holdit) {
                // not really used for CRU.
                countIT++;
            }
        } else {
            if (holdit) {
                countIH++;
                if (TraceEjb.isDebugSynchro()) {
                        TraceEjb.synchro.log(BasicLevel.DEBUG, ident + "mapICtx IH count=" + countIH);
                }
                if (shared && countIH == 1) {
                    // reload state that could have been modified by
                    // transactions.
                    jec.activate(false);
                }
            }
        }

        return jec;
    }

}
