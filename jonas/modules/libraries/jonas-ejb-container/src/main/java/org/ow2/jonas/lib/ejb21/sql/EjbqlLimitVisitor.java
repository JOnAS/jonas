/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.lib.ejb21.sql;

import java.util.Stack;

import org.ow2.jonas.deployment.ejb.ejbql.ASTEJBQL;
import org.ow2.jonas.deployment.ejb.ejbql.ASTInputParameter;
import org.ow2.jonas.deployment.ejb.ejbql.ASTIntegerLiteral;
import org.ow2.jonas.deployment.ejb.ejbql.ASTLimitClause;
import org.ow2.jonas.deployment.ejb.ejbql.ASTLimitExpression;
import org.ow2.jonas.deployment.ejb.ejbql.ParseException;
import org.ow2.jonas.deployment.ejb.ejbql.SimpleNode;


/**
 * Implementation of a visitor that creates the limiter ranges corresponding to
 * the LIMIT clause
 * @author Cyrille Blot : Initial developer
 * @author Helene Joanin
 */
public class EjbqlLimitVisitor extends EjbqlAbstractVisitor {

    /**
     * Types of the parameters list of the finder/select method
     */
    private Class[] paramTypes;

    /**
     * Limiter ranges
     */
    private EjbqlLimiterRange[] ranges = new EjbqlLimiterRange[0];

    /**
     * Constructor
     * @param ejbql root of the lexical tree of the query
     * @param paramTypes Types of the parameters list of the finder/select
     *        method
     * @throws Exception in error case
     */
    public EjbqlLimitVisitor(ASTEJBQL ejbql, Class[] paramTypes) throws Exception {
        this.paramTypes = paramTypes;
        visit(ejbql);
    }

    /**
     * @return returns the limiter ranges of the LIMIT clause.
     * May be 0 element if no LIMIT clause, 1 or 2 elements otherwise.
     */
    public EjbqlLimiterRange[] getLimiterRanges() {
        return ranges;
    }

    /**
     * Visit child node. LIMIT LimitExpression() ( , LimitExpression())?
     * @param node sub-root of the lexical tree
     * @param data stack
     * @return returns null
     */
    public Object visit(ASTLimitClause node, Object data) {
        visit((SimpleNode) node, data);
        Stack s = (Stack) data;
        ranges = new EjbqlLimiterRange[s.size()];
        if (s.size() > 1) {
            // s = [row, nbr]
            ranges[1] = (EjbqlLimiterRange) s.pop();
        }
        // s = [row]
        ranges[0] = (EjbqlLimiterRange) s.pop();
        return null;
    }

    /**
     * Visit child node LimitExpression().
     * @param node sub-root of the lexical tree
     * @param data stack
     * @return returns null
     */
    public Object visit(ASTLimitExpression node, Object data) {
        visit((SimpleNode) node, data);
        return null;
    }

    /**
     * Visit child nodes literal ::= integer_literal Push the corresponding
     * EjbqlLimiterRange to the stack
     * @param node sub-root of the lexical tree
     * @param data stack
     * @return returns null
     */
    public Object visit(ASTIntegerLiteral node, Object data) {
        ((Stack) data).push(new EjbqlLimiterRange(EjbqlLimiterRange.KIND_LITERAL, ((Long) node.value).intValue()));
        return null;
    }

    /**
     * Node with value set to parameter index (1..n) string. Push the
     * corresponding EjbqlLimiterRange to the stack
     * @param node sub-root of the lexical tree
     * @param data stack
     * @return returns null
     */
    public Object visit(ASTInputParameter node, Object data) {
        try {
            int pIndex = ((Integer) node.value).intValue() - 1;
            if (pIndex >= paramTypes.length) {
                throw new ParseException("Parameter ?" + (pIndex + 1) + " is out of range (max=" + paramTypes.length
                        + ")");
            }
            // TODO : check if the associated parameter type is compatible to
            // integer
            ((Stack) data).push(new EjbqlLimiterRange(EjbqlLimiterRange.KIND_PARAMETER, pIndex));
            return null;
        } catch (ParseException e) {
            throw new VisitorException(e);
        }
    }

}