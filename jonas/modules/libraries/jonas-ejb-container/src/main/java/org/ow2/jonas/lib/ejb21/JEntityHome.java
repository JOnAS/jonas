/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.ejb21;

import java.rmi.Remote;
import java.rmi.RemoteException;

import org.ow2.jonas.deployment.ejb.EntityDesc;


import org.objectweb.util.monolog.api.BasicLevel;

/**
 * This class is the Standard Home for Entity objects It exists only for beans
 * that have declared a Remote Interface. It implements javax.ejb.EJBHome
 * interface
 * @author Philippe Coq
 * @author Philippe Durieux
 */
public abstract class JEntityHome extends JHome implements Remote {

    /**
     * constructor
     * @param dd The Entity Deployment Decriptor
     * @param bf The Entity Factory
     * @throws RemoteException failed to create the Home
     */
    public JEntityHome(EntityDesc dd, JEntityFactory bf) throws RemoteException {
        super(dd, bf);
        if (TraceEjb.isDebugIc()) {
            TraceEjb.interp.log(BasicLevel.DEBUG, "");
        }
    }

    // ---------------------------------------------------------------
    // EJBHome implementation
    // both remove() methods are in the generated part.
    // other methods are in JHome (identical for Sessions and Entities)
    // ---------------------------------------------------------------

    // ---------------------------------------------------------------
    // other public methods, for internal use.
    // ---------------------------------------------------------------

    /**
     * creates a new Remote Object for that bean. this is in the generated class
     * because it is mainly "new objectClass()"
     * @return The Remote Object
     * @throws RemoteException could not create the remote object
     */
    public abstract JEntityRemote createRemoteObject() throws RemoteException;
}