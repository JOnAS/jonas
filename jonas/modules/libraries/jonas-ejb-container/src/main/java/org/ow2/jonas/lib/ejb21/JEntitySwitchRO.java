/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.ejb21;

import javax.ejb.EJBException;
import javax.transaction.SystemException;
import javax.transaction.Transaction;

import org.ow2.jonas.deployment.ejb.EntityDesc;


import org.objectweb.util.monolog.api.BasicLevel;

/**
 * ReadOnly lock policy : Instance are never written to database.
 * They are regularly read from database if bean is shared.
 * @author Philippe Durieux
 */
public class JEntitySwitchRO extends JEntitySwitchCST {



    protected long nextRead;

    /**
     * empty constructor. Object is initialized via init() because it is
     * implemented differently according to jorm mappers.
     */
    public JEntitySwitchRO() {
        lockpolicy = EntityDesc.LOCK_READ_ONLY;
    }

    protected void initpolicy(JEntityFactory bf) {
        lazyregister = true;	// never write anyway.
    }


    public void waitmyturn(Transaction tx) {
        // No synchro for this policy.
    }

    /**
     * Map a context and its instance.
     * @param tx - the Transaction object
     * @param bctx - the JEntityContext to bind if not null
     * @param forced - force to take this context. (case of create)
     * @param holdit - increment count to hold it, a release will be called
     *        later.
     * @return JEntityContext actually mapped
     */
    public synchronized JEntityContext mapICtx(Transaction tx, JEntityContext bctx, boolean forced, boolean holdit, boolean notused) {

        waitmyturn(tx);

        // Choose the context to use.
        boolean newtrans = false;
        JEntityContext jec = itContext;
        if (forced) {
            TraceEjb.context.log(BasicLevel.ERROR, ident + "create cannot be called on read only bean");
            throw new EJBException("Read Only bean");
        } else {
            if (jec != null) {
                // Reuse the Context for this transaction.
                // If a context was supplied, release it first.
                if (bctx != null) {
                    if (TraceEjb.isDebugContext())
                            TraceEjb.context.log(BasicLevel.DEBUG, ident + "a context was supplied!");
                    bf.releaseJContext(bctx, 2);
                }
                newtrans = true;
                jec.reuseEntityContext(newtrans);
            } else {
                if (bctx != null) {
                    jec = bctx;
                } else {
                    // no Context available : get one from the pool.
                    jec = (JEntityContext) bf.getJContext(this);
                }
                jec.initEntityContext(this);
                jec.activate(true);
                nextRead = System.currentTimeMillis() + readTimeout;
                //TraceEjb.synchro.log(BasicLevel.WARN, "loading state " + nextRead);
                itContext = jec; // after activate
                newtrans = true;
            }
        }

        if (tx != null) {
            if (holdit) {
                // Not really used for RO
                countIT++;
                if (shared) {
                    if (System.currentTimeMillis() > nextRead) {
                        // may have been modified outside this server.
                        jec.activate(false);
                        nextRead = System.currentTimeMillis() + readTimeout;
                        //TraceEjb.synchro.log(BasicLevel.WARN, "TX reloading state " + nextRead);
                    }
                }
            }
        } else {
            if (holdit) {
                countIH++;
                if (TraceEjb.isDebugSynchro())
                        TraceEjb.synchro.log(BasicLevel.DEBUG, ident + "mapICtx IH count=" + countIH);
                if (shared && countIH == 1) {
                    if (System.currentTimeMillis() > nextRead) {
                        // may have been modified outside this server.
                        jec.activate(false);
                        nextRead = System.currentTimeMillis() + readTimeout;
                        //TraceEjb.synchro.log(BasicLevel.WARN, "HT reloading state " + nextRead);
                    }
                }
            }
        }

        return jec;
    }

    /**
     * This transaction is now over. We can dispose of the instance for another
     * transaction or discard it.
     * @param tx the transaction object
     * @param committed true if transaction was committed.
     */
    public synchronized void txCompleted(Transaction tx, boolean committed) {
        TraceEjb.context.log(BasicLevel.ERROR, ident + "should not happen (RO)");
    }

}
