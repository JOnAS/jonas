/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.ejb21.jorm;

import java.util.Iterator;
import java.lang.reflect.Array;
import javax.ejb.EJBException;
import org.objectweb.jorm.api.PIndexedElem;
import org.objectweb.jorm.api.PClassMapping;
import org.objectweb.jorm.api.PException;

/**
 * This class is a basic implementation of the java.util.Collection based on the
 * generic class implementation (GenClassImpl). This class can be used to
 * represent a relation between bean.
 *
 * @author S.Chassande-Barrioz : Initial developer
 * @author Helene Joanin : fix bugs in the toArray(...) methods
 */
public class Collection extends GenClassImpl implements java.util.Collection {

    int nextIndex = -1;

    public Collection() {
        super();
    }

    /**
     * 
     */
    public PIndexedElem createPIndexedElem() {
        if (pIndexedElems.size() == 0) {
            nextIndex = 0;
        }
        return new CollectionElement(this, nextIndex++);
    }

    // IMPLEMENT THE Collection INTERFACE //
    //------------------------------------//

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public boolean contains(Object o) {
        try {
            return gcContains((PObject) o, null);
        } catch (PException e) {
            e.printStackTrace();
            throw new ArrayStoreException(e.getMessage());
        }
    }

    public Iterator iterator() {
        try {
            return gcIterator();
        } catch (PException e) {
            e.printStackTrace();
            throw new ArrayStoreException(e.getMessage());
        }
    }

    /**
     * It returns an array of the elements.
     * @return array of the elements
     */
    public Object[] toArray() {
        return toArray(new Object[size]);
    }

    /**
     * It returns an array of the elements.
     * It is built by an iteration over the existing elements.
     * @param objects the array into which the elements of this collection are to be stored
     * @return array of the elements
     */
    public Object[] toArray(Object[] objects) {
        try {
            int i = 0;
            for (Iterator it = gcIterator(); it.hasNext();) {
                objects[i++] = it.next();
            }
        } catch (PException e) {
            e.printStackTrace();
            throw new ArrayStoreException(e.getMessage());
        }
        return objects;
    }

    /**
     *
     */
    public boolean add(Object o) {
        gcAdd((PObject) o, true);
        return true;
    }

    /**
     *
     */
    public boolean remove(Object o) {
        try {
            return gcRemove(o, true) != null;
        } catch (PException e) {
            throw new EJBException(e);
        }
    }

    /**
     *
     */
    public boolean remove(Object o, boolean callListener) {
        try {
            return gcRemove(o, callListener) != null;
        } catch (PException e) {
            throw new EJBException(e);
        }
    }

    /**
     *
     */
    public boolean containsAll(java.util.Collection collection) {
        if (collection == null) {
            return true;
        }
        try {
            boolean res = true;
            Object conn = gcm.getPMapper().getConnection();
            for (Iterator it = collection.iterator(); it.hasNext() && res;) {
                res = gcContains((PObject) it.next(), conn);
            }
            gcm.getPMapper().closeConnection(conn);
            return res;
        } catch (PException e) {
            e.printStackTrace();
            throw new ArrayStoreException(e.getMessage());
        }
    }

    /**
     * It iterates over the collection parameter to add each element in the
     * collection.
     */
    public boolean addAll(java.util.Collection collection) {
        if (collection == null) {
            return true;
        }
        boolean res = true;
        for (Iterator it = collection.iterator(); it.hasNext();) {
            res &= add((PObject) it.next());
        }
        return res;
    }

    /**
     * It iterates over the collection parameter to remove each element in the
     * collection.
     */
    public boolean removeAll(java.util.Collection collection) {
        if (collection == null) {
            return true;
        }
        try {
            for (Iterator it = collection.iterator(); it.hasNext();) {
                gcRemove((PObject) it.next(), true);
            }
        } catch (PException e) {
            throw new EJBException(e);
        }
        return true;
    }

    /**
     * For each element of the current collection, it checks if it exist into
     * the collection parameter. If it does not found then it is removed from
     * the current collection.
     */
    public boolean retainAll(java.util.Collection collection) {
        if (collection == null) {
            clear();
            return true;
        }
        try {
            for (Iterator it = iterator(); it.hasNext();) {
                PObject o = (PObject) it.next();
                if (!collection.contains(o))
                    gcRemove(o, true);
            }
        } catch (PException e) {
            throw new EJBException(e);
        }
        return true;
    }

    /**
     * It removes all elements.
     */
    public void clear() {
        gcClear(false);
    }
}
