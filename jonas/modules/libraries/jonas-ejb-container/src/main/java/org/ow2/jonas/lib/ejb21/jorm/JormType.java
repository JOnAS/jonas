/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.ejb21.jorm;

import javax.ejb.EJBLocalObject;


import org.objectweb.jorm.type.api.PType;
import org.objectweb.jorm.type.api.PTypeSpace;
import org.objectweb.jorm.naming.api.PNamingContext;
import org.objectweb.jorm.naming.api.PExceptionNaming;
import org.objectweb.medor.type.lib.PTypeSpaceMedor;


/**
 * This class contains several methods to manipulate a PType.
 *
 * @author Sebastien Chassande-Barrioz : Initial developper
 * @author Helene Joanin
 */
public class JormType {

    /**
     * This method converts a java.lang.Class into a jorm PType.
     * @param cl is a java.lang.Class
     * @param isPkField true if this is for a field of the primary key, false otherwise
     * @return The PType which matches the class specified in parameter.
     */
    public static PType getPType(java.lang.Class cl, boolean isPkField) {
        if (cl.equals(Boolean.TYPE)) {
            return PTypeSpace.BOOLEAN;
        } else if (cl.equals(Boolean.class)) {
            return PTypeSpace.OBJBOOLEAN;
        } else if (cl.equals(Character.TYPE)) {
            return PTypeSpace.CHAR;
        } else if (cl.equals(Character.class)) {
            return PTypeSpace.OBJCHAR;
        } else if (cl.equals(Byte.TYPE)) {
            return PTypeSpace.BYTE;
        } else if (cl.equals(Byte.class)) {
            return PTypeSpace.OBJBYTE;
        } else if (cl.equals(Short.TYPE)) {
            return PTypeSpace.SHORT;
        } else if (cl.equals(Short.class)) {
            return PTypeSpace.OBJSHORT;
        } else if (cl.equals(Integer.TYPE)) {
            return PTypeSpace.INT;
        } else if (cl.equals(Integer.class)) {
            return PTypeSpace.OBJINT;
        } else if (cl.equals(Long.TYPE)) {
            return PTypeSpace.LONG;
        } else if (cl.equals(Long.class)) {
            return PTypeSpace.OBJLONG;
        } else if (cl.equals(Float.TYPE)) {
            return PTypeSpace.FLOAT;
        } else if (cl.equals(Float.class)) {
            if (isPkField) {
                return (JormType.getPType(FloatPkFieldMapping.getStorageType(), false));
            } else {
                return PTypeSpace.OBJFLOAT;
            }
        } else if (cl.equals(Double.TYPE)) {
            return PTypeSpace.DOUBLE;
        } else if (cl.equals(Double.class)) {
            return PTypeSpace.OBJDOUBLE;
        } else if (cl.equals(String.class)) {
            return PTypeSpace.STRING;
        } else if (cl.equals(java.util.Date.class)
                   || cl.equals(java.sql.Date.class)
                   || cl.equals(java.sql.Time.class)
                   || cl.equals(java.sql.Timestamp.class)) {
            return PTypeSpace.DATE;
        } else if (cl.equals(java.math.BigDecimal.class)) {
            return PTypeSpace.BIGDECIMAL;
        } else if (EJBLocalObject.class.isAssignableFrom(cl)) {
            return PTypeSpaceMedor.PNAME;
        } else if (cl.isArray() && java.lang.Byte.TYPE.equals(cl.getComponentType())) {
            return PTypeSpace.BYTEARRAY;
        } else if (cl.isArray() && java.lang.Character.TYPE.equals(cl.getComponentType())) {
            return PTypeSpace.CHARARRAY;
        }
        return PTypeSpace.SERIALIZED;
    }

    /**
     * This method converts a java.lang.Class into a jorm PType.
     * @param cl is a java.lang.Class
     * @param isPkField true if this is for a field of the primary key, false otherwise
     * @return A String that represents The PType which matches the class.
     */
    public static String getPTypeDef(Class cl, boolean isPkField) {
        if (cl.equals(Boolean.TYPE)) {
            return "PTypeSpace.BOOLEAN";
        } else if (cl.equals(Boolean.class)) {
            return "PTypeSpace.OBJBOOLEAN";
        } else if (cl.equals(String.class)) {
            return "PTypeSpace.STRING";
        } else if (cl.equals(Character.TYPE)) {
            return "PTypeSpace.CHAR";
        } else if (cl.equals(Character.class)) {
            return "PTypeSpace.OBJCHAR";
        } else if (cl.equals(Byte.TYPE)) {
            return "PTypeSpace.BYTE";
        } else if (cl.equals(Byte.class)) {
            return "PTypeSpace.OBJBYTE";
        } else if (cl.equals(Short.TYPE)) {
            return "PTypeSpace.SHORT";
        } else if (cl.equals(Short.class)) {
            return "PTypeSpace.OBJSHORT";
        } else if (cl.equals(Integer.TYPE)) {
            return "PTypeSpace.INT";
        } else if (cl.equals(Integer.class)) {
            return "PTypeSpace.OBJINT";
        } else if (cl.equals(Long.TYPE)) {
            return "PTypeSpace.LONG";
        } else if (cl.equals(Long.class)) {
            return "PTypeSpace.OBJLONG";
        } else if (cl.equals(Float.TYPE)) {
            return "PTypeSpace.FLOAT";
        } else if (cl.equals(Float.class)) {
            if (isPkField) {
                return (JormType.getPTypeDef(FloatPkFieldMapping.getStorageType(), false));
            } else {
                return "PTypeSpace.OBJFLOAT";
            }
        } else if (cl.equals(Double.TYPE)) {
            return "PTypeSpace.DOUBLE";
        } else if (cl.equals(Double.class)) {
            return "PTypeSpace.OBJDOUBLE";
        } else if (cl.equals(java.util.Date.class)
                   || cl.equals(java.sql.Date.class)
                   || cl.equals(java.sql.Time.class)
                   || cl.equals(java.sql.Timestamp.class)) {
            return "PTypeSpace.DATE";
        } else if (cl.equals(java.math.BigDecimal.class)) {
            return "PTypeSpace.BIGDECIMAL";
        } else if (EJBLocalObject.class.isAssignableFrom(cl)) {
            return "PTypeSpaceMedor.PNAME";
        } else if (cl.isArray() && java.lang.Byte.TYPE.equals(cl.getComponentType())) {
            return "PTypeSpace.BYTEARRAY";
        } else if (cl.isArray() && java.lang.Character.TYPE.equals(cl.getComponentType())) {
            return "PTypeSpace.CHARARRAY";
        }
        return "PTypeSpace.SERIALIZED";
    }

    /**
     * It retrieves a coding type either a Class
     * @param the Class
     * @param isPkField true if this is for a field of the primary key, false otherwise
     * @return a coding type wich matches to one of CTxxx fields of the
     * PNamingcontext interface.
     * @throws PExceptionNaming when the class specified is not support in
     * the jorm naming.
     */
    public static short getCodingType(Class c, boolean isPkField) throws PExceptionNaming {
        if (c == null) {
            throw new PExceptionNaming("No CodingType associated to a null Class");
        }
        return getCodingType(getPType(c, isPkField).getTypeCode());
    }

    /**
     * It converts a PType into a coding type.
     * @param a  PType
     * @return a coding type wich matches to one of CTxxx fields of the
     * PNamingcontext interface.
     * @throws PExceptionNaming when the PType specified is not support in
     * the jorm naming.
     */
    public static short getCodingType(PType pt) throws PExceptionNaming {
        if (pt == null) {
            throw new PExceptionNaming("No CodingType associated to a null PType");
        }
        return getCodingType(pt.getTypeCode());
    }

    /**
     * It converts a type code into a coding type.
     * @param a type code of a PType
     * @return a coding type wich matches to one of CTxxx fields of the
     * PNamingcontext interface.
     * @throws PExceptionNaming when the type code specified is not support in
     * the jorm naming.
     */
    public static short getCodingType(int typeCode) throws PExceptionNaming {
        switch(typeCode) {
        case PType.TYPECODE_CHAR:
            return PNamingContext.CTCHAR;
        case PType.TYPECODE_OBJCHAR:
            return PNamingContext.CTOCHAR;

        case PType.TYPECODE_BYTE:
            return PNamingContext.CTBYTE;
        case PType.TYPECODE_OBJBYTE:
            return PNamingContext.CTOBYTE;

        case PType.TYPECODE_SHORT:
            return PNamingContext.CTSHORT;
        case PType.TYPECODE_OBJSHORT:
            return PNamingContext.CTOSHORT;

        case PType.TYPECODE_INT:
            return PNamingContext.CTINT;
        case PType.TYPECODE_OBJINT:
            return PNamingContext.CTOINT;

        case PType.TYPECODE_LONG:
            return PNamingContext.CTLONG;
        case PType.TYPECODE_OBJLONG:
            return PNamingContext.CTOLONG;

        case PType.TYPECODE_BYTEARRAY:
            return PNamingContext.CTBYTEARRAY;

        case PType.TYPECODE_CHARARRAY:
            return PNamingContext.CTCHARARRAY;

        case PType.TYPECODE_STRING:
            return PNamingContext.CTSTRING;

        case PType.TYPECODE_DATE:
            return PNamingContext.CTDATE;

        default:
            throw new PExceptionNaming("This type is not suportted: typeCode=" + typeCode);
        }
    }

}
