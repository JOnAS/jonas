/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.ejb21;

import java.rmi.NoSuchObjectException;
import java.rmi.RemoteException;

import javax.ejb.EJBObject;
import javax.rmi.PortableRemoteObject;

import org.objectweb.util.monolog.api.BasicLevel;

/**
 * This class is the common part of the EJBObject implementation class. Here
 * goes the code common for Entity beans and Session beans
 * @author Philippe Durieux
 */
public abstract class JRemote extends PortableRemoteObject implements EJBObject {

    protected JFactory bf;

    /**
     * constructor
     * @param bf The Bean Factory
     */
    public JRemote(JFactory bf) throws RemoteException {
        if (TraceEjb.isDebugIc()) {
            TraceEjb.interp.log(BasicLevel.DEBUG, "");
        }
        this.bf = bf;

        // For CMI needs, we have to keep the link between jndiname/classloader
        // The link has already been registered in the JHome constructor
    }

    // --------------------------------------------------------------------------
    // EJBObject implementation
    // --------------------------------------------------------------------------

    // --------------------------------------------------------------------------
    // Export / UnExport Object
    // Due to the fact that this object extends RemoteObject, it is
    // automatically
    // exported when built. We just need to unexport it when unused, and to re
    // export
    // it if we reuse it again.
    // --------------------------------------------------------------------------

    /**
     * Make this object accessible again thru the Orb.
     * @return True if export is OK.
     */
    public boolean exportObject() {
        if (TraceEjb.isDebugIc()) {
            TraceEjb.interp.log(BasicLevel.DEBUG, "");
        }
        try {
            exportObject(this);
        } catch (Exception e) {
            TraceEjb.logger.log(BasicLevel.ERROR, "cannot export", e);
            return false;
        }
        return true;
    }

    /**
     * Make this object unaccessible thru the Orb.
     */
    public void unexportObject() throws NoSuchObjectException {
        if (TraceEjb.isDebugIc()) {
            TraceEjb.interp.log(BasicLevel.DEBUG, "");
        }
        unexportObject(this);
    }

    /**
     * @return the bean factory
     */
    public JFactory getBf() {
        return bf;
    }
}