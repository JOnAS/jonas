/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.lib.ejb21;

import java.io.Serializable;
import java.lang.Object;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import java.util.Vector;

/**
 * This class implements the java.util.Enumeration and the java.io.Serializable interfaces.
 * <br>
 * This class is used as the return value type of the implementation of the finder methods
 * which return a collection.
 * <br>
 * Indeed, the EJB spec. tells that the type for a collection is the java.util.Enumeration.
 * In addition the return value type must be legal type for Java RMI (ie. serializable type).
 * @author Helene Joanin : Initial developer
 */

public class CollectionEnum implements Serializable, Enumeration {

    /**
     * @serial
     */
    private Vector mCollection = null;
    /**
     * @serial
     */
    private int mIndex = 0;

    /**
     * Create an empty CollectionEnum
     *
     */
    public CollectionEnum() {
	mCollection = new Vector();
	mIndex = 0;
    }

    /**
     * Add an element
     */
    public synchronized void addElement(Object obj) {
	mCollection.addElement(obj);
    }

    /**
     * Create a CollectionEnum from a vector
     */
    public CollectionEnum(Vector v) {
	mCollection = new Vector();
	for (int i=0; i<v.size(); i++) {
	    mCollection.addElement(v.elementAt(i));
	}
	mIndex = 0;
    }

    /**
     * Implements Enumeration.hasMoreElements()
     */
    public boolean hasMoreElements() {
	return(mIndex<mCollection.size());
    }

    /**
     * Implements Enumeration.nextElement()
     */
    public Object nextElement() throws NoSuchElementException {
	if (mIndex>=mCollection.size()) {
	    throw new NoSuchElementException("CollectionEnum ("+mIndex+">="+mCollection.size()+")");
	}
	mIndex++;
	return(mCollection.elementAt(mIndex-1));
    }

}

