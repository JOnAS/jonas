/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Sebastien Chassande
 * Contributor(s):
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.ejb21.jorm;

/**
 * This interface is a listener over the actions which modify a GenClass.
 *
 * @author S.Chassande-Barrioz
 */
public interface GenClassListener {

    /**
     * Check if the element is valid or not with the GenClass
     * @param element element to check
     * @throws IllegalArgumentException if the element is not valid
     */
    void isLegalElement(GenClassElement element) throws IllegalArgumentException;;

    /**
     * An element has been added in the gen class.
     * @param element is the added element.
     */
    void gcAdd(GenClassElement element);

    /**
     * An element has been removed in the gen class.
     * @param element is the removed element.
     * @param callListener indicates if the gen class listener must be call
     * about this action
     */
    void gcRemove(GenClassElement element, boolean callListener);
}
