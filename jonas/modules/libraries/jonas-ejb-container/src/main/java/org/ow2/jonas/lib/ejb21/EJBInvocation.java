/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 * 
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.ejb21;

import java.util.Arrays;

import javax.ejb.EnterpriseBean;


/**
 * Class used for preInvoke.
 * Allow to give arguments like security string, EJB arguments.
 * It is used in jacc
 * @author Florent Benoit : Intial developer
 */
public class EJBInvocation {

    /**
     * List of arguments for the EJB
     */
    public Object[] arguments = null;

    /**
     * Security signature for the method which is invoked
     * This string is used to build EJBMethodPermission
     */
    public String methodPermissionSignature = null;

    /**
     * Enterprise Bean being processed
     */
    public EnterpriseBean bean = null;

    /**
     * Default constructor.
     * Build an empty invocation object
     */
    public EJBInvocation() {
    }

    /**
     * String representation
     * @return String representation
     */
    public String toString() {
        return "ejbInvocation[signature=" + methodPermissionSignature
        + ",bean=" + bean
        + ",args=" + Arrays.asList(arguments) + "]";
    }

}
