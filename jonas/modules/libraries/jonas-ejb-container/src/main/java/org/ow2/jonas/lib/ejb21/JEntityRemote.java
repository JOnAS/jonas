/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.ejb21;

import java.rmi.Remote;
import java.rmi.RemoteException;

import javax.ejb.EJBException;
import javax.ejb.EJBHome;
import javax.ejb.EJBObject;

import org.ow2.carol.rmi.exception.RmiUtility;


import org.objectweb.util.monolog.api.BasicLevel;

/**
 * Generic part of the EJBObject implementation
 * @author Philippe Coq
 * @author Philippe Durieux
 */
public abstract class JEntityRemote extends JRemote implements Remote {

    protected JEntityFactory ebf;

    protected JEntitySwitch bs;

    /**
     * constructor
     * @param bf The Entity Factory
     */
    public JEntityRemote(JEntityFactory bf) throws RemoteException {
        super(bf);
        if (TraceEjb.isDebugIc()) {
            TraceEjb.interp.log(BasicLevel.DEBUG, "");
        }
        this.ebf = bf;
    }

    /**
     * finish initialization
     * @param bs The Entity Bean Switch
     */
    public void setEntitySwitch(JEntitySwitch bs) {
        if (TraceEjb.isDebugIc()) {
            TraceEjb.interp.log(BasicLevel.DEBUG, "");
        }
        this.bs = bs;
    }

    // --------------------------------------------------------------------------
    // EJBObject implementation
    // remove() is implemented in the generated part.
    // --------------------------------------------------------------------------

    /**
     * @return the enterprise Bean's home interface.
     */
    public EJBHome getEJBHome() {
        if (TraceEjb.isDebugIc()) {
            TraceEjb.interp.log(BasicLevel.DEBUG, "");
        }

        /*
         * try/catch block is commented because the encapsulated code don't
         * throw a RemoteException for the moment
         * If the code changes and throws a such exception, let's think
         * to uncomment it
         *
         * try {
         */

        return ebf.getHome();

        /*
         * } catch (RemoteException e) {
         * // check if rmi exception mapping is needed - if yes the method  rethrows it
         * RmiUtility.rethrowRmiException(e);
         * // if not, throws the exception just as it is
         * throw e;
         * }
         */

    }

    /**
     * @return the Primary Key for this EJBObject
     * @throws EJBException Bean has no primary key yet.
     */
    public Object getPrimaryKey() {
        if (TraceEjb.isDebugIc()) {
            TraceEjb.interp.log(BasicLevel.DEBUG, "");
        }
        /*
         * try/catch block is commented because the encapsulated code don't
         * throw a RemoteException for the moment
         * If the code changes and throws a such exception, let's think
         * to uncomment it
         *
         * try {
         */

        if (bs == null) {
            throw new EJBException("No Primary Key yet");
        }
        return bs.getPrimaryKey();

        /*
         * } catch (RemoteException e) {
         * // check if rmi exception mapping is needed - if yes the method  rethrows it
         * RmiUtility.rethrowRmiException(e);
         * // if not, throws the exception just as it is
         * throw e;
         * }
         */

    }

    /**
     * Tests if a given EJB is identical to the invoked EJB object.
     * @param obj - An object to test for identity with the invoked object.
     * @return True if the given EJB object is identical to the invoked object.
     * @throws RemoteException Thrown when the method failed due to a
     *         system-level failure.
     */
    public boolean isIdentical(EJBObject obj) throws RemoteException {
        if (TraceEjb.isDebugIc()) {
            TraceEjb.interp.log(BasicLevel.DEBUG, "");
        }

        try {

            boolean ret = false;

            if (obj != null) { // Get the home class name
                String homeClassName = getEJBHome().getEJBMetaData().getHomeInterfaceClass().getName();
                String objHomeClassName = obj.getEJBHome().getEJBMetaData().getHomeInterfaceClass().getName();

                // Tests the home equality and the primary key equality
                ret = ((obj.equals(this)) || ((objHomeClassName.equals(homeClassName)) && (obj.getPrimaryKey()
                        .equals(getPrimaryKey()))));
            }

            return ret;

        } catch (RemoteException e) {
         // check if rmi exception mapping is needed - if yes the method rethrows it
         RmiUtility.rethrowRmiException(e);
         // if not, throws the exception just as it is
         throw e;
         }

    }

    // ---------------------------------------------------------------
    // other public methods, for internal use.
    // ---------------------------------------------------------------

    /**
     * preInvoke is called before any request.
     * @param txa Transaction Attribute (Supports, Required, ...)
     * @return A RequestCtx object
     * @throws RemoteException
     */
    public RequestCtx preInvoke(int txa) throws RemoteException {
        if (TraceEjb.isDebugIc()) {
            TraceEjb.interp.log(BasicLevel.DEBUG, "");
        }
        return bf.preInvokeRemote(txa);
    }

    /**
     * Check if the access to the bean is authorized
     * @param ejbInv object containing security signature of the method, args of
     *        method, etc
     */
     public void checkSecurity(EJBInvocation ejbInv) {
         if (TraceEjb.isDebugIc()) {
             TraceEjb.interp.log(BasicLevel.DEBUG, "");
         }
         bf.checkSecurity(ejbInv);
     }

     /**
     * postInvoke is called after any request.
     * @param rctx The RequestCtx that was returned at preInvoke()
     * @throws RemoteException
     */
    public void postInvoke(RequestCtx rctx) throws RemoteException {
        if (TraceEjb.isDebugIc()) {
            TraceEjb.interp.log(BasicLevel.DEBUG, "");
        }
        try {
            bf.postInvokeRemote(rctx);
        } finally {
            bs.releaseICtx(rctx.currTx, rctx.sysExc != null);
        }
    }

}