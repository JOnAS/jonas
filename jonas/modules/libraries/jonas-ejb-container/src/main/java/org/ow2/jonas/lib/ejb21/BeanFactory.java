/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.lib.ejb21;

import java.util.Hashtable;

import javax.naming.InitialContext;
import javax.transaction.Transaction;

import org.ow2.jonas.deployment.ejb.BeanDesc;
import org.ow2.jonas.tm.TransactionManager;
import org.ow2.jonas.jndi.checker.api.IResourceCheckerManager;


/**
 * Interface to the Bean Factories
 * used by the Container, JMX, or generic parts of this package.
 * @author Philippe Durieux, Philippe Coq (Bull)
 */
public interface BeanFactory {

    /**
     * get the bean name
     * @return The name of the bean
     */
    String getEJBName();

    /**
     * Get the Deployement descriptor of this Ejb
     * @return BeanDesc The bean deployment descriptor
     */
    BeanDesc getDeploymentDescriptor();

    /**
     * Get the size of the instance pool for this bean
     * @return number of instances in the pool
     */
    int getPoolSize();

    /**
     * stop this EJB (unregister it in JNDI)
     */
    void stop();

    /**
     * synchronize bean instances if needed
     * @param alwaysStore True if store even if passivation timeout not elapsed
     */
    void syncDirty(boolean alwaysStore);

    /**
     * reduce cache of instances
     */
    void reduceCache();

    /**
     * returns the home if exist or null if not
     * @return Home for that bean, if exists.
     */
    JHome getHome();

    /**
     * returns the local home if exist or null if not
     * @return LocalHome for that bean, if exists.
     */
    JLocalHome getLocalHome();

    /**
     * returns the TransactionManager
     * @return The Transaction Manager
     */
    TransactionManager getTransactionManager();

    /**
     * Returns the JContainer
     * @return The Container where the bean lives.
     */
    JContainer getContainer();

    /**
     * Return the JNDI Environment
     * @return List of Environment used by the bean
     */
    Hashtable getEnv();

    /**
     * Returns the InitialContext
     * @return JNDI Initial Context
     */
    InitialContext getInitialContext();

    /**
     * Init the pool of instances
     */
    void initInstancePool();

    /**
     * Restart the Timers after jonas restart
     */
    void restartTimers();

    /**
     * Store intances modified (used before finder ou select methods)
     * @param tx current transaction
     */
    void storeInstances(Transaction tx);

    /**
     * @return the resource checker manager
     */
    IResourceCheckerManager getResourceCheckerManager();

    /**
     * Sets the resource checker manager.
     * @param resourceCheckerManager the given instance
     */
    void setResourceCheckerManager(IResourceCheckerManager resourceCheckerManager);
}
