/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Philippe Durieux, Guillaume Sauthier.
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.client.naming;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.naming.spi.InitialContextFactory;

import org.ow2.jonas.ejb2.internal.delegate.HandleDelegateCCFDelegate;
import org.ow2.jonas.lib.naming.SingletonComponentContextFactory;
import org.ow2.jonas.lib.util.Log;
import org.ow2.jonas.naming.JComponentContextFactory;
import org.ow2.jonas.registry.carol.delegate.ORBCCFDelegate;
import org.ow2.jonas.tm.jotm.delegate.UserTransactionCCFDelegate;


import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * This {@link InitialContextFactory}, to be used on the client side only,
 * has the role of initializing the {@link JComponentContextFactory} (for example
 * <code>java:comp/UserTransaction</code>).
 */
public class ClientInitialContextFactory implements InitialContextFactory {

    /**
     * Logger used for traces.
     */
    private static Logger logger = Log.getLogger(Log.JONAS_NAMING_PREFIX);

    /**
     * This object is recreated each time. SO, we use a static here to
     * avoid multiple initializations of SingletonComponentContextFactory
     */
    private static JComponentContextFactory ccf = null;

    /**
     * Creates an Initial Context for begining name resolution.
     * This class is necessary for the Client to provide a correct Context
     * with the delegate objects added (UserTransaction, ...).
     * @param env The possibly null environment specifying information
     *        to be used in the creation of the initial context.
     * @return A non-null initial context object that implements the Context
     *         interface.
     * @exception NamingException If cannot create an initial context.
     */
    @SuppressWarnings("unchecked")
    public Context getInitialContext(final Hashtable env) throws NamingException {
        if (ccf == null) {
            // Initialize here the ComponentContextFactory
            logger.log(BasicLevel.DEBUG, "Init ComponentContextFactory");
            ccf = SingletonComponentContextFactory.getInstance();
            ccf.addDelegate(new UserTransactionCCFDelegate());
            ccf.addDelegate(new HandleDelegateCCFDelegate());
            ccf.addDelegate(new ORBCCFDelegate());
        }
        // redirect to carol MultiOrb InitialContextFactory
        env.put(Context.INITIAL_CONTEXT_FACTORY, "org.ow2.carol.jndi.spi.MultiOrbInitialContextFactory");
        return new InitialContext(env);
    }

}
