/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.client;



/**
 * The class ClientContainerException that indicates conditions that a
 * reasonable application might want to catch.
 *
 * @author Florent Benoit
 */
public class ClientContainerException extends Exception {
    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = 5038487298301979152L;

    /**
     * Constructs a new ClientContainerException with the specified  message.
     *
     * @param message the detail message.
     */
    public ClientContainerException(final String message) {
        this(message, null);
    }

    /**
     * Constructs a new ClientContainerException with the specified  message.
     *
     * @param message the detail message.
     * @param throwable the exception
     */
    public ClientContainerException(final String message, final Throwable throwable) {
        super(message, throwable);
    }
}
