/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployablemonitor;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Comparator used to sort the deployables in the wanted order.
 * @author Florent Benoit
 */
public class SortableFileDeployableComparator implements Comparator<SortableFileDeployable> {

    /**
     * Ordered deployables.
     */
    private List<String> orderedDeployables = null;

    /**
     * Default constructor.
     */
    public SortableFileDeployableComparator() {
        this.orderedDeployables = new ArrayList<String>();
    }

    /**
     * Sets the given list of ordered deployables that we should use to select our order.
     * @param orderedDeployables the given list
     */
    public void setOrderedDeployables(final List<String> orderedDeployables) {
        this.orderedDeployables = orderedDeployables;
    }

    /**
     * @param sortableFileDeployable the deployable from which we will get the index
     * @return the index value from the ordered list
     */
    private int index(final SortableFileDeployable sortableFileDeployable) {
        // Unknown, return the max value so that it will be ordered at the end
        if (sortableFileDeployable == null) {
            return Integer.MAX_VALUE;
        }

        String deployableClassName = sortableFileDeployable.getDeployable().getClass().getName();

        // Search with the full name
        int index = this.orderedDeployables.indexOf(deployableClassName);
        if (index == -1) {
            // search with fragment
            int i = 0;
            boolean found = false;
            while (i < orderedDeployables.size() && !found) {
                String name = orderedDeployables.get(i);
                if (deployableClassName.toLowerCase().contains(name.toLowerCase())) {
                    // found it
                    found = true;
                    break;
                }
                i++;
            }
            if (!found) {
                index = Integer.MAX_VALUE;
            } else {
                index = i;
            }
        }
        return index;
    }

    /**
     * Compare two deployables and return a value to sort them.
     * @param sortableFileDeployable1 first archive to compare
     * @param sortableFileDeployable2 the other archive to compare
     * @return a compare value that allows to sort the given deployables
     */
    @Override
    public int compare(final SortableFileDeployable sortableFileDeployable1,
            final SortableFileDeployable sortableFileDeployable2) {

        // Get index of the two deployables
        int index1 = index(sortableFileDeployable1);
        int index2 = index(sortableFileDeployable2);

        // Return order
        if (index1 > index2) {
            return 1;
        } else if (index1 == index2) {
            // use of lexicographic order when the deployable are with the same kind of deployable
            return sortableFileDeployable1.getFile().getName().compareTo(sortableFileDeployable2.getFile().getName());
        } else {
            return -1;
        }

    }

}
