/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployablemonitor;

import java.io.File;

import org.ow2.util.ee.deploy.api.deployable.IDeployable;

/**
 * Class that stores a file and the associated deployable and that can be
 * sorted.
 * @author Florent Benoit
 */
public class SortableFileDeployable {

    /**
     * File that represents the associated deployabled.
     */
    private File file = null;

    /**
     * Deployable object linked to the given file.
     */
    private IDeployable<?> deployable = null;

    /**
     * Defines the file and its deployable.
     * @param file the given file
     * @param deployable the given deployable
     */
    public SortableFileDeployable(final File file, final IDeployable<?> deployable) {
        this.file = file;
        this.deployable = deployable;
    }

    /**
     * @return the file
     */
    public File getFile() {
        return file;
    }

    /**
     * @return the deployable
     */
    public IDeployable<?> getDeployable() {
        return deployable;
    }

    /**
     * @return string representation of this sorted deployable.
     */
    @Override
    public String toString() {
        String deployableClassName = deployable.getClass().getSimpleName();
        String DEP = "DeployableImpl";
        if (deployableClassName.endsWith(DEP)) {
            deployableClassName = deployableClassName.substring(0, deployableClassName.length() - DEP.length());
        }

        return "[".concat(deployableClassName).concat(":").concat(file.getPath()).concat("]");
    }


}
