/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007-2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployablemonitor;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.ow2.util.archive.api.IArchive;
import org.ow2.util.archive.impl.ArchiveManager;
import org.ow2.util.ee.deploy.api.deployable.IDeployable;
import org.ow2.util.ee.deploy.api.deployable.UnknownDeployable;
import org.ow2.util.ee.deploy.api.deployer.IDeployerManager;
import org.ow2.util.ee.deploy.api.deployer.UnsupportedDeployerException;
import org.ow2.util.ee.deploy.api.helper.DeployableHelperException;
import org.ow2.util.ee.deploy.api.report.IDeploymentReport;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;
import org.ow2.util.plan.deploy.deployable.api.DeploymentPlanDeployable;

/**
 * This monitor will search all the deployable from a list of directories.<br>
 * In development mode, this monitor will detect changes on the deployables and
 * then restart or undeploy them if they have been changed or removed.
 * @author Florent BENOIT
 */
public class DeployableMonitor extends Thread {

    /**
     * Logger.
     */
    private static Log logger = LogFactory.getLog(DeployableMonitor.class);

    /**
     * List of directories to analyze.
     */
    private List<File> directories = null;

    /**
     * Development mode or not ?
     */
    private boolean developmentMode = false;

    /**
     * Monitor interval between each directory scan.
     */
    private int monitorInterval;

    /**
     * Stop order received ?
     */
    private boolean stopped = false;

    /**
     * Map between a File (monitored) and the last updated file.
     */
    private Map<File, Long> modifiedFiles = null;

    /**
     * List of deployed files (by this monitor).
     */
    private Map<File, IDeployable<?>> deployed = null;

    /**
     * Map between a File and its last length known.
     */
    private Map<File, Long> fileLengths = null;

    /**
     * List of File that have a failed deployment (by this monitor).
     */
    private List<File> failed = null;

    /**
     * List of File that are not known deployable.
     */
    private List<File> unknown = null;

    /**
     * Deployer Manager. (that allow us to deploy Deployable).
     */
    private IDeployerManager deployerManager = null;

    /**
     * First launch of this monitor.
     */
    private boolean firstCheck = true;

    /**
     * Exclusion filter.
     */
    private ExclusionFilenameFilter filter = null;

    /**
     * Allow to inform the Service that the firstCheck ended.
     */
    private DeployableMonitorService myService = null;

    /**
     * Comparator used to sort the deployables.
     */
    private Comparator<SortableFileDeployable> sortDeployablesComparator = null;

    /**
     * The Bundle context
     */
    private BundleContext bundleContext = null;

    /**
     * Max waiting time in milliseconds (0,5 second)
     */
    public static final long MAX_WAITING_TIME = 500L;

    /**
     * the length of time to sleep in milliseconds (0,05 second)
     */
    public static final long SLEEPING_TIME = 50;

    /**
     * Constructor of this monitor.
     * @param service the {@link DeployableMonitorService} instance
     * @param bundleContext the bundle context
     */
    public DeployableMonitor(final DeployableMonitorService service, final BundleContext bundleContext) {
        this.directories = new LinkedList<File>();
        this.modifiedFiles = new WeakHashMap<File, Long>();
        this.deployed = new ConcurrentHashMap<File, IDeployable<?>>();
        this.fileLengths = new HashMap<File, Long>();
        this.failed = new ArrayList<File>();
        this.unknown = new ArrayList<File>();
        this.filter  = new ExclusionFilenameFilter();
        this.myService = service;
        this.sortDeployablesComparator = new SortableFileDeployableComparator();
        this.bundleContext = bundleContext;
    }

    /**
     * reset the state of all deployables.
     */
    public void reset() {
        if (firstCheck) {
            return;
        }

        this.modifiedFiles.clear();
        this.deployed.clear();
        this.fileLengths.clear();

        for (File deployDirectory : directories) {
            File[] files = deployDirectory.listFiles(filter);

            if (files == null) {
                continue;
            }

            for (File file : files) {
                try {
                    IArchive archive = ArchiveManager.getInstance().getArchive(file);
                    if (archive == null) {
                        continue;
                    }
                    IDeployable<?> deployable = this.myService.getDeployableHelper().getDeployable(archive);
                    if (deployerManager.isDeployed(deployable)) {

                        deployed.put(file, deployable);
                        try {
                            fileLengths.put(file, getFileSize(file));
                            // hasBeenUpdated will update the modifiedFiles
                            hasBeenUpdated(file);
                        } catch (IOException e) {
                            logger.warn("Cannot reset file {0}", file, e);
                        } catch (DeployableMonitorException e) {
                            if (!failed.contains(file)) {
                                failed.add(file);
                                logger.error("Cannot reset file {0}.", file, e);
                            }
                            // Store file length to 0 anyways otherwise we wont be able to detect that the file had changed in
                            // development mode
                            // if this is fixed at runtime
                            fileLengths.put(file, 0L);
                            // Initialize modifiedFiles for the same reason
                            modifiedFiles.put(file, 1L);
                        }

                    }
                } catch (Throwable t) {
                    logger.warn("Cannot reset file {0}", file, t);
                }
            }
        }
    }

    /**
     * Start the thread of this class. <br>
     * It will search and deploy files to deploy.<br>
     * In development mode, it will check the changes.
     */
    @Override
    public void run() {
        for (;;) {
            if (stopped) {
                // Stop the thread
                return;
            }

            if (developmentMode || firstCheck) {
                // Save file lengths in order to avoid deploying file for which copy is in process
                saveFileLengths();

                // 20% of the monitor interval is spent to wait in order to guarantee the file integrity
                // If the file is being copied, the file size increases in the time
                try {
                    Thread.sleep((long) (monitorInterval * 0.2));
                } catch (InterruptedException e) {
                    throw new RuntimeException("Thread fail to sleep");
                }

                // Undeploy/ReDeploy archives for deployed modules
                updateArchives();

                // Check new archives/containers to start
                checkNewArchives();

                if (firstCheck) {
                    final String failStartOnError = System.getProperty("jonas.start.nofail");
                    if (!this.developmentMode && failStartOnError != null && Boolean.parseBoolean(failStartOnError)) {
                        //check if there is at least one deployable with a fail deployment
                        if (!this.failed.isEmpty()) {
                            StringBuilder stringBuilder = new StringBuilder();
                            stringBuilder.append("The deployment of " + this.failed.size() + " deployable");
                            if (this.failed.size() > 1) {
                                stringBuilder.append("s");
                            }
                            stringBuilder.append(" has failed. The server is going to shut down.");
                            logger.error(stringBuilder.toString());
                            //stop the server
                            Bundle bundle = this.bundleContext.getBundle(0);
                            try {
                                bundle.stop();
                            } catch (BundleException e) {
                                logger.error("Cannot stop bundle " + bundle.getLocation(), e);
                            }
                        }
                    }
                    myService.firstCheckEnded();
                }

                // Next step, we will be in a checking step.
                firstCheck = false;
            }

            try {
                Thread.sleep((long) (monitorInterval * 0.8));
            } catch (InterruptedException e) {
                throw new RuntimeException("Thread fail to sleep");
            }
        }
    }

    /**
     * Save file lengths in scanned directories.
     */
    private void saveFileLengths() {
        // Clear Map
        fileLengths.clear();

        for (File deployDirectory : directories) {
            // Get files
            File[] files = deployDirectory.listFiles(filter);

            // Next directory if there are no files to scan.
            if (files == null) {
                continue;
            }

            for (File file : files) {
                try {
                    fileLengths.put(file, getFileSize(file));
                } catch (IOException e) {
                    // File cannot be read
                    // or a sub file is not completed (in case of a directory)
                } catch (DeployableMonitorException e) {
                    if (!failed.contains(file)) {
                        failed.add(file);
                        logger.error("Unable to deploy {0}.", file, e);
                    }
                    // Store file length to 0 anyways otherwise we wont be able to detect that the file had changed in development mode
                    // if this is fixed at runtime
                    fileLengths.put(file, 0L);
                    // Initialize modifiedFiles for the same reason
                    modifiedFiles.put(file, 1L);
                }
            }
        }
    }

    /**
     * The windows related method test if the file is complete (any write
     * operations on the given file are finished).
     * @param file tested file (regular file only)
     * @throws IOException if the given file is not completed (only thrown
     *                     under windows when file is not completed)
     */
    private static void checkFileCompleteness(final File file) throws IOException {
        if (file.isFile()) {
            // Try to open an input stream on the file
            // This should throw an exception if the file copy is not finished (Windows only)
            FileInputStream fis = new FileInputStream(file);
            fis.close();
        }
    }

    /**
     * Return the file or directory size.
     * @param file a file or directory
     * @return the total file size (files in folders included)
     * @throws Exception if one of the analyzed files was not completed
     */
    private static long getFileSize(final File file) throws IOException, DeployableMonitorException {
        if (file.isFile()) {
            checkFileCompleteness(file);
            return file.length();
        } else if (file.isDirectory()) {
            // Directory case
            long size = 0;
            File[] childs = file.listFiles();
            for (File child : childs) {
                // Add the size of the child
                size += getFileSize(child);
            }
            return size;
        }
        // Not a valid file
        throw new DeployableMonitorException("Invalid file " + file.getAbsolutePath());
    }

    /**
     * Check new archives/containers.
     */
    private void checkNewArchives() {
        try {
            detectNewArchives();
        } catch (Exception e) { // Catch all exception
            logger.error("Problem when trying to find and deploy new archives", e);
        } catch (Error e) {
            logger.error("Error when trying to find and deploy new archives", e);
        }
    }

    /**
     * Undeploy/ReDeploy archives for deployed modules.
     */
    private void updateArchives() {
        try {
            checkModifiedDeployables();
        } catch (Exception e) { // Catch all exception
            logger.error("Problem when checking current deployables", e);
        } catch (Error e) {
            logger.error("Error when checking current deployables", e);
        }
    }

    /**
     * Scan all files present in the deploy directory and deploy them. (if not
     * deployed).
     */
    private void detectNewArchives() {

        for (File deployDirectory : directories) {
            // get files
            File[] files = deployDirectory.listFiles(filter);

            // next directory if there are no files to scan.
            if (files == null) {
                continue;
            }

            // Sort the files by names
            Arrays.sort(files, new LexicographicallyFileComparator());

            // Sort the file by type (first check only)
            if (firstCheck) {
                // Build a list of deployable
                List<SortableFileDeployable> deployables = new LinkedList<SortableFileDeployable>();

                // for each file
                for (File f : files) {
                    // Get deployable
                    IArchive archive = ArchiveManager.getInstance().getArchive(f);
                    if (archive == null) {
                        logger.warn("Ignoring invalid file ''{0}''", f);
                        continue;
                    }

                    IDeployable<?> deployable;
                    try {
                        deployable = this.myService.getDeployableHelper().getDeployable(archive);
                    } catch (DeployableHelperException e) {
                        // Deployment of this deployable has failed
                        failed.add(f);
                        logger.error("Cannot get a deployable for the archive ''{0}''", archive, e);
                        continue;
                    }

                    deployables.add(new SortableFileDeployable(f, deployable));
                }

                // Sort the deployables by using the given comparator
                Collections.sort(deployables, sortDeployablesComparator);


                // Add files sorted from the comparator
                List<File> filesToDeploy = new ArrayList<File>();
                for (SortableFileDeployable sortableFileDeployable : deployables) {
                    filesToDeploy.add(sortableFileDeployable.getFile());
                }

                if (filesToDeploy.size() > 0) {
                    logger.info("Deployables to deploy at startup: [{0}]", deployables);
                }

                // Update the array
                files = filesToDeploy.toArray(new File[filesToDeploy.size()]);

            }

            //list of deployables to deploy
            Map<File, IDeployable<?>> deployablesToDeploy = new LinkedHashMap<File, IDeployable<?>>();
            List<File> unknownDeployables = new ArrayList<File>();

            // analyze each file to detect new modules that are not yet deployed.
            for (File file : files) {
                // Already deployed ?
                if (deployed.containsKey(file)) {
                    // yes, then check other files
                    continue;
                }

                // File length has changed: file copy is not finished ?
                try {
                    if (fileLengthHasChanged(file)) {
                        continue;
                    }
                } catch (DeployableMonitorException e) {
                    if (!failed.contains(file)) {
                        failed.add(file);
                        logger.error("Unable to deploy {0}.", file, e);
                    }
                    continue;
                }

                // This module has failed previously ?
                if (failed.contains(file)) {
                    // If the module hasn't been updated, no need to deploy it again as it will fails again
                    try {
                        if (!hasBeenUpdated(file)) {
                            continue;
                        }
                    } catch (DeployableMonitorException e) {
                        if (!failed.contains(file)) {
                            failed.add(file);
                            logger.error("Unable to deploy {0}.", file, e);
                        }
                        continue;
                    }
                    // Cleanup the previous failure and try again the deployment
                    failed.remove(file);
                }

                // Else, get the deployable
                IArchive archive = ArchiveManager.getInstance().getArchive(file);
                if (archive == null) {
                    logger.warn("Ignoring invalid file ''{0}''", file);
                    continue;
                }
                IDeployable<?> deployable;
                try {
                    deployable = this.myService.getDeployableHelper().getDeployable(archive);
                } catch (DeployableHelperException e) {
                    // Deployment of this deployable has failed
                    failed.add(file);
                    logger.error("Cannot get a deployable for the archive ''{0}''", archive, e);
                    continue;
                }

                // Unkown deployable, do not deploy it for now
                if (UnknownDeployable.class.isInstance(deployable)) {
                    unknownDeployables.add(file);
                    continue;
                }

                // File is not an unknown deployable anymore, remove it
                if (unknown.contains(file)) {
                    unknown.remove(file);
                    logger.info("The archive ''{0}'' is not anymore an unknown deployable, deploy it now as ''{1}''", archive,
                            deployable);
                }

                // Already deployed ? (don't check DeploymentPlanDeployable)
                try {
                    if (deployerManager.isDeployed(deployable) && !DeploymentPlanDeployable.class.isInstance(deployable)) {
                        deployed.put(file, deployable);
                        continue;
                    }
                } catch (Exception e) {
                    // Deployable not supported.
                }

                deployablesToDeploy.put(file, deployable);
                logger.debug("Detect a new Deployable ''{0}'' and deploying it.", deployable);
            }

            if (stopped) {
                // Don't deploy new archives after the reception of a stop order
                return;
            }

            //Now deploy files
            deploy(deployablesToDeploy);

            //check if unknown deployables are still unknown
            deployablesToDeploy.clear();
            for (File file: unknownDeployables) {

                // get the deployable
                IArchive archive = ArchiveManager.getInstance().getArchive(file);
                if (archive == null) {
                    logger.warn("Ignoring invalid file ''{0}''", file);
                    continue;
                }
                IDeployable<?> deployable = null;

                //wait some time in order to fix synchronisation issues
                boolean ok = false;
                boolean exception = false;
                long maxWaitTime = System.currentTimeMillis() + MAX_WAITING_TIME;
                while (!exception && !ok && System.currentTimeMillis() < maxWaitTime) {
                    try {
                        deployable = this.myService.getDeployableHelper().getDeployable(archive);
                        if (!UnknownDeployable.class.isInstance(deployable)) {
                            ok = true;
                        } else {
                            try {
                                Thread.sleep(SLEEPING_TIME);
                            } catch (InterruptedException e) {
                                //do nothing
                            }
                        }
                    } catch (DeployableHelperException e) {
                        logger.error("Cannot get a deployable for the archive ''{0}''", archive, e);
                        exception = true;
                    }
                }

                if (exception) {
                    failed.add(file);
                    continue;
                }

                // Unkown deployable, do not deploy it for now
                if (UnknownDeployable.class.isInstance(deployable)) {
                    if (!unknown.contains(file)) {
                        unknown.add(file);
                        logger.error("The archive ''{0}'' is flagged as unkwnown deployable. Do not deploy it", archive);
                    }
                    continue;
                }

                // File is not an unknown deployable anymore, remove it
                if (unknown.contains(file)) {
                    unknown.remove(file);
                }
                logger.info("The archive ''{0}'' is not anymore an unknown deployable, deploy it now as ''{1}''", archive,
                        deployable);

                // Already deployed ? (don't check DeploymentPlanDeployable)
                try {
                    if (deployerManager.isDeployed(deployable) && !DeploymentPlanDeployable.class.isInstance(deployable)) {
                        deployed.put(file, deployable);
                        continue;
                    }
                } catch (Exception e) {
                    // Deployable not supported.
                }

                deployablesToDeploy.put(file, deployable);
                logger.debug("Detect a new Deployable ''{0}'' and deploying it.", deployable);
            }

            if (stopped) {
                // Don't deploy new archives after the reception of a stop order
                return;
            }

            //deploy deployables which are not unknown deployables anymore
            deploy(deployablesToDeploy);
        }
    }

    /**
     * @param deployables Map between a {@link File} and its associated {@link IDeployable} to deploy
     */
    private void deploy(Map<File, IDeployable<?>> deployables) {
        if (!deployables.isEmpty()) {
            try {
                List<IDeploymentReport> deploymentReports = this.deployerManager.deploy(new ArrayList<IDeployable<?>>(
                        deployables.values()));

                for (Map.Entry<File, IDeployable<?>> entry: deployables.entrySet()) {
                    File file = entry.getKey();
                    IDeployable<?> deployable = entry.getValue();
                    IDeploymentReport deploymentReport = getDeploymentReport(deployable, deploymentReports);

                    if (deploymentReport == null || deploymentReport.isDeploymentOk()) {
                        // Store the new deployable
                        this.deployed.put(file, deployable);
                    } else {
                        if (!(deploymentReport.getException() instanceof UnsupportedDeployerException)) {
                            this.failed.add(file);
                            logger.error("DeployerException : Cannot deploy the deployable ''{0}''", deployable,
                                    deploymentReport.getException());
                        } else {
                            // Deployment of this deployable is not supported.
                            // We will try to deploy this file in the next detection cycle.
                        }
                    }
                }
                // Perform a garbage collector to avoid file lock
                System.gc();
            } catch (RuntimeException e) {
                // Runtime exception but deployment has failed
                this.failed.addAll(deployables.keySet());
                for (IDeployable deployable: deployables.values()) {
                    logger.error("RuntimeException : Cannot deploy the deployable ''{0}''", deployable, e);
                }
            } catch (Error e) {
                // Error but deployment has failed
                this.failed.addAll(deployables.keySet());
                for (IDeployable deployable: deployables.values()) {
                    logger.error("Error : Cannot deploy the deployable ''{0}''", deployable, e);
                }
            }
        }
    }

    /**
     *
     * @param deployable The {@link IDeployable}
     * @param deploymentReports The list of {@link IDeploymentReport}
     * @return the {@link IDeploymentReport} associated to the given deployable
     */
    private IDeploymentReport getDeploymentReport(final IDeployable deployable, final List<IDeploymentReport> deploymentReports) {
        for (IDeploymentReport deploymentReport: deploymentReports) {
            if (deploymentReport.getDeployable().equals(deployable)) {
                return deploymentReport;
            }
        }
        return null;
    }

    /**
     * Gets the last modified attribute of a given archive.<br> If it is a
     * directory, returns the last modified file of the archive.
     * @param archive the archive to monitor.
     * @return the last modified version of the given archive.
     */
    protected long getLastModified(final File archive) {
        if (archive.isFile()) {
            return archive.lastModified();
        }
        // else
        File[] files = archive.listFiles();
        long last = 0;
        if (files != null) {
            for (File f : files) {
                last = Math.max(last, getLastModified(f));
            }
        }
        return last;
    }

    /**
     * Check if the given file has been updated since the last check.
     * @param file the file to test
     * @return true if the archive has been updated
     */
    protected boolean hasBeenUpdated(final File file) throws DeployableMonitorException {
        // File length has changed: file copy is not finished ?
        if (fileLengthHasChanged(file)) {
            return false;
        }

        // get lastmodified for this URL
        long previousLastModified = 0;
        Long l = modifiedFiles.get(file);
        if (l != null) {
            previousLastModified = l.longValue();
        }

        long updatedModified = getLastModified(file);

        // first check. nothing to do
        if (previousLastModified == 0) {
            // Store initial time
            modifiedFiles.put(file, Long.valueOf(updatedModified));
            return false;
        }
        // URL has been updated since the last time
        if (updatedModified > previousLastModified) {
            modifiedFiles.put(file, Long.valueOf(updatedModified));
            return true;
        }

        return false;
    }

    /**
     * Check if the current deployables that are deployed have been updated.
     * If it is the case, undeploy them and then deploy it again (except for EJB3 Deployable where there is a stop/start).
     */
    private void checkModifiedDeployables() {
        // Get list of files that are failed
        Iterator<File> fileIterator = failed.iterator();

        // For each failed module, check if the module has been removed
        while (fileIterator.hasNext()) {
            File f = fileIterator.next();
            // File has been removed
            if (!f.exists()) {
                fileIterator.remove();
            }
        }

        // Get list of files that are undeployed
        Set<File> files = deployed.keySet();
        for (File f : files) {
            IDeployable<?> deployable = deployed.get(f);
            try {
                if (!deployerManager.isDeployed(deployable)) {
                    deployed.remove(f);
                }
            } catch (Exception e) {
                deployed.remove(f);
            }
        }

        // TODO files can't be null files.isEmpty should be used instead
        // Nothing to do if no modules are deployed.
        if (files == null) {
            return;
        }

        //list of deployables to undeploy
        Map<File, IDeployable<?>> deployablesToUnDeploy = new LinkedHashMap<File, IDeployable<?>>();

        //list of deployables to update
        Map<File, IDeployable<?>> deployablesToUpdate = new LinkedHashMap<File, IDeployable<?>>();

        // For each deployed module that is not an EJB3, check if the module has been updated
        for (File f : files) {
            IDeployable<?> deployable = deployed.get(f);

            // Not yet deployed ?
            if (deployable == null) {
                continue;
            }

            // File has been removed
            if (!f.exists()) {
                // undeploy
                logger.info("Deployable ''{0}'' has been removed on the filesystem, undeploy it", deployable);
                deployablesToUnDeploy.put(f, deployable);
                continue;
            }

            // Update has been detected, need to undeploy and then to deploy again
            try {
                if (hasBeenUpdated(f)) {
                    logger.info("Deployable ''{0}'' has been updated, reloading it", deployable);
                    deployablesToUpdate.put(f, deployable);
                }
            } catch (DeployableMonitorException e) {
                // Have been updated and became invalid
                deployablesToUnDeploy.put(f, deployable);
                if (!failed.contains(f)) {
                    failed.add(f);
                }
                logger.error("Deployable ''{0}'' has been updated and became invalid, undeploying it.", deployable, e);
            }
        }

        //Undeploy files which have been removed
        if (!deployablesToUnDeploy.isEmpty()) {
            try {
                List<IDeploymentReport> deploymentReports = this.deployerManager.undeploy(new ArrayList<IDeployable<?>>(
                        deployablesToUnDeploy.values()));

                for (Map.Entry<File, IDeployable<?>> entry: deployablesToUnDeploy.entrySet()) {
                    File file = entry.getKey();
                    IDeployable<?> deployable = entry.getValue();
                    IDeploymentReport deploymentReport = getDeploymentReport(deployable, deploymentReports);
                    if (deploymentReport != null && !deploymentReport.isDeploymentOk()) {
                        if (!(deploymentReport.getException() instanceof UnsupportedDeployerException)) {
                            logger.error("Undeploy of the deployable '" + deployable + "' has failed",
                                    deploymentReport.getException());
                            this.failed.add(file);
                        } else {
                            // Undeployment of this deployable is not supported.
                            // We will try to undeploy this file in the next detection cycle.
                        }
                    }
                }

                // Perform a garbage collector to avoid file lock during redeployment
                System.gc();
            } catch (RuntimeException e) {
                for (Map.Entry<File, IDeployable<?>> entry: deployablesToUnDeploy.entrySet()) {
                    logger.error("Undeploy of the deployable '" + entry.getValue() + "' has failed", e);
                    this.failed.add(entry.getKey());
                }
            } finally {
                for (File file: deployablesToUnDeploy.keySet()) {
                    // even in error case, the file should have been removed
                    this.deployed.remove(file);
                }
            }
        }

        //update deployables
        if (!deployablesToUpdate.isEmpty()) {
            List<IDeploymentReport> deploymentReports = this.deployerManager.undeploy(new ArrayList<IDeployable<?>>(
                    deployablesToUpdate.values()));
            List<File> toRemove = new ArrayList<File>();
            for (Map.Entry<File, IDeployable<?>> entry: deployablesToUpdate.entrySet()) {
                boolean undeploymentIsOk = true;
                File file = entry.getKey();
                IDeployable<?> deployable = entry.getValue();
                IDeploymentReport deploymentReport = getDeploymentReport(deployable, deploymentReports);
                if (deploymentReport != null && !deploymentReport.isDeploymentOk()) {
                    if (!(deploymentReport.getException() instanceof UnsupportedDeployerException)) {
                        logger.error("Undeploy of the deployable '" + deployable + "' has failed",
                                deploymentReport.getException());
                        // Deployment has failed, it is now undeployed
                        this.failed.add(file);
                    } else {
                        // Undeployment of this deployable is not supported.
                        // We will try to undeploy this file in the next detection cycle.
                        undeploymentIsOk = false;
                    }
                }
                if (undeploymentIsOk) {
                    this.deployed.remove(file);
                } else {
                    //if the deployment of this deployable is not support, we will update the deployable in the next detection cycle
                    toRemove.add(file);
                }
            }
            for (File file: toRemove) {
                deployablesToUpdate.remove(file);
            }

            // Perform a garbage collector to avoid file lock during redeployment
            System.gc();
        }

        // Get a new deployables
        Map<File, IDeployable<?>> newDeployables = new LinkedHashMap<File, IDeployable<?>>();
        for (File file: deployablesToUpdate.keySet()) {
            IArchive archive = ArchiveManager.getInstance().getArchive(file);
            if (archive == null) {
                logger.warn("Ignoring invalid file ''{0}''", file);
            } else {
                try {
                    newDeployables.put(file, this.myService.getDeployableHelper().getDeployable(archive));
                } catch (DeployableHelperException e) {
                    logger.error("Cannot get a deployable for the archive '" + archive + "'", e);
                }
            }
        }

        //deploy new deployables
        deploy(newDeployables);
    }

    /**
     * Check if the file length has changed.
     * @param file The given file
     * @return True if the file length has changed
     */
    private boolean fileLengthHasChanged(final File file) throws DeployableMonitorException {
        // File length not known
        if (!fileLengths.containsKey(file)) {
            return true;
        }

        long storedFileLength = fileLengths.get(file);
        long currentFileLength = 0;
        try {
            currentFileLength = getFileSize(file);
        } catch (IOException e) {
            // File cannot be checked, probably the file is being written.
            return true;
        }
        if (storedFileLength != currentFileLength) {
            // File length has changed
            return true;
        }

        // No change in file length
        return false;
    }

    /**
     * @return the list of directories that this monitor is monitoring.
     */
    public List<File> getDirectories() {
        return directories;
    }

    /**
     * Sets the list of directories to monitor.
     * @param directories the list of directories to use.
     */
    public void setDirectories(final List<File> directories) {
        this.directories = directories;
    }

    /**
     * Add a directory to the list of directories to monitor.
     * @param directory the directory to add
     */
    public void addDirectory(final File directory) {
        directories.add(directory);
    }

    /**
     * Remove a directory to the list of directories to monitor.
     * @param directory the directory to remove
     */
    public void removeDirectory(final File directory) {
        directories.remove(directory);
    }

    /**
     * @return true if the development is enabled, else false (production mode).
     */
    public boolean isDevelopmentMode() {
        return developmentMode;
    }

    /**
     * Enable or disable the development mode.
     * @param developmentMode true if it has to be enabled.
     */
    public void setDevelopmentMode(final boolean developmentMode) {
        this.developmentMode = developmentMode;
    }

    /**
     * Set the monitor interval between each directory scan.
     * @param monitorInterval value to set
     */
    public void setMonitorInterval(final int monitorInterval) {
        this.monitorInterval = monitorInterval;
    }

    /**
     * Receives a stop order.
     */
    public void stopOrder() {
        this.stopped = true;
    }

    /**
     * @return the instance of the deployer manager.
     */
    public IDeployerManager getDeployerManager() {
        return deployerManager;
    }

    /**
     * Sets the deployer manager for deployers.
     * @param deployerManager the instance of the deployer manager.
     */
    public void setDeployerManager(final IDeployerManager deployerManager) {
        this.deployerManager = deployerManager;
    }

    /**
     * @param patterns List of exclusion patterns.
     */
    public void setExclusionPatterns(final List<String> patterns) {
        filter.setExclusionList(patterns);
    }

    /**
     * @return the sort comparator for deployables
     */
    public Comparator<SortableFileDeployable> getSortDeployablesComparator() {
        return sortDeployablesComparator;
    }

    /**
     * Defines the comparator used for deployables.
     * @param sortDeployablesComparator the given comparator
     */
    public void setSortDeployablesComparator(final Comparator<SortableFileDeployable> sortDeployablesComparator) {
        this.sortDeployablesComparator = sortDeployablesComparator;
    }
}
