# ---------------------------------------------------------------------------
# JOnAS: Java(TM) Open Application Server
# Copyright (C) 2007-2008 Bull S.A.S.
# Contact: jonas-team@ow2.org
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
# USA
# ---------------------------------------------------------------------------
# $Id$
# ---------------------------------------------------------------------------

# Export the Carol packages
-exportcontents org.ow2.carol.rmi.exception,\
                org.ow2.carol.util.configuration,\
                org.ow2.carol.util.csiv2,\
                org.ow2.carol.util.csiv2.struct,\
                org.ow2.carol.util.csiv2.gss,\
                org.ow2.carol.jndi.ns,\
                org.ow2.carol.jndi.intercept.*,\
                org.ow2.carol.jndi.spi,\
                org.ow2.carol.jndi.wrapping,\
                org.ow2.carol.rmi.jrmp.server,\
                org.ow2.carol.rmi.multi,\
                org.ow2.carol.rmi.util,\
                org.ow2.jonas.registry,\
                org.objectweb.carol.*

# Rest of Carol classes are private
Private-Package org.ow2.jonas.registry.carol;-split-package:=merge-first,\
                org.ow2.jonas.registry.carol.osgi

Bundle-Activator org.ow2.jonas.registry.carol.osgi.ConfigurationActivator

# must import jotm for the stubs that must be registered in jndi.
DynamicImport-Package javax.ejb,\
                      javax.ejb.spi,\
                      javax.management.j2ee,\
                      org.objectweb.jotm,\
                      org.objectweb.jotm.jta.rmi,\
                      org.ow2.cmi.admin,\
                      org.ow2.cmi.config,\
                      org.ow2.cmi.controller.common,\
                      org.ow2.cmi.controller.client,\
                      org.ow2.cmi.controller.factory,\
                      org.ow2.cmi.controller.factory.client,\
                      org.ow2.cmi.controller.factory.server,\
                      org.ow2.cmi.jndi.context,\
                      org.ow2.cmi.*,\
                      org.ow2.jonas.dbm.internal.cm.naming,\
                      org.ow2.easybeans.proxy.factory,\
                      org.ow2.easybeans.rpc.rmi.server,\
                      org.ow2.jonas.lib.ejb21,\
                      org.ow2.jonas.lib.ejb21.ha.interceptors.jrmp,\
                      org.ow2.jonas.lib.security.auth,\
                      org.ow2.jonas.lib.security.context,\
                      org.ow2.jonas.mail.internal.factory,\
                      org.ow2.jonas.resource.internal.naming,\
                      org.ow2.jonas.security.*,\
                      org.ow2.jonas.tm.jotm,\
                      org.objectweb.joram.client.jms.admin,\
                      org.ow2.util.ee.deploy.api.deployable, \
                      org.ow2.carol.irmi, \
                      org.ow2.easybeans.component.jdbcpool, \
                      org.ow2.util.auditreport.impl,\
                      org.ow2.easybeans.component.audit.rmi.interceptor.jrmp, \
                      org.ow2.easybeans.component.remotejndiresolver


Export-Package org.ow2.jonas.registry.carol.delegate,\
               org.ow2.carol.jndi.spi,\
               org.ow2.carol.jndi.intercept.spi

# Directly include the carol jars 'as is'
Embed-Dependency jonas-apis-services-registry;inline=true, \
                 carol;inline=true, \
                 carol-iiop-delegate;inline=true

