/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id:RegistryServiceImpl.java 10372 2007-05-14 13:58:42Z sauthieg $
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.registry.carol.osgi;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Dictionary;

import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.ConfigurationPlugin;
import org.ow2.jonas.lib.bootstrap.JProp;

/**
 * JOnAS CarolRegistryConfigurationPlugin component.
 * @author Francois Fornaciari
 */
public class CarolRegistryConfigurationPlugin implements ConfigurationPlugin {

    /**
     * Carol configuration URL.
     */
    private static final String CONFIGURATION_URL = "configuration.url";

    /**
     * {@inheritDoc}
     */
    public void modifyConfiguration(ServiceReference serviceReference, Dictionary props) {
        // Find the carol.properties URL
        File conf = new File(JProp.getJonasBase(), "conf");
        try {
            URL carolConfig = new File(conf, "carol.properties").toURL();
            props.put(CONFIGURATION_URL, carolConfig.toString());
        } catch (MalformedURLException e) {
            System.out.println("The carol.properties cannot be found.");
        }
    }

}
