/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id:RegistryServiceImpl.java 10372 2007-05-14 13:58:42Z sauthieg $
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.registry.carol.osgi;

import java.util.Dictionary;
import java.util.Hashtable;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.service.cm.ConfigurationPlugin;
import org.ow2.jonas.registry.carol.CarolRegistryService;

/**
 * JOnAS Configuration Activator. It registers the ConfigurationPlugin service.
 * @author Francois Fornaciari
 */
public class ConfigurationActivator implements BundleActivator {

    /**
     * {@inheritDoc}
     */
    public void start(final BundleContext bc) throws Exception {
        Dictionary<String, String> dictionary = new Hashtable<String, String>();
        dictionary.put(ConfigurationPlugin.CM_TARGET, CarolRegistryService.class.getName());

        // Register the service
        bc.registerService(ConfigurationPlugin.class.getName(), new CarolRegistryConfigurationPlugin(), dictionary);
    }

    /**
     * {@inheritDoc}
     */
    public void stop(final BundleContext bc) throws Exception {
    }

}
