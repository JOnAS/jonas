/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id:ORBCCFDelegate.java 10372 2007-05-14 13:58:42Z sauthieg $
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.registry.carol.delegate;

import javax.naming.Context;
import javax.naming.NamingException;

import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;
import org.omg.CORBA.ORB;
import org.ow2.carol.jndi.ns.JacORBCosNaming;
import org.ow2.carol.rmi.exception.NamingExceptionHelper;
import org.ow2.jonas.lib.execution.ExecutionResult;
import org.ow2.jonas.lib.execution.IExecution;
import org.ow2.jonas.lib.execution.RunnableHelper;
import org.ow2.jonas.lib.util.Log;
import org.ow2.jonas.naming.JComponentContextFactoryDelegate;

/**
 * Store the {@link org.omg.CORBA.ORB} CORBA ORB under the <code>java:comp/ORB</code> name.
 * @author Guillaume Sauthier
 */
public class ORBCCFDelegate implements JComponentContextFactoryDelegate {

    /**
     * ORB JNDI Binding name.
     */
    private static final String ORB = "ORB";

    /**
     * Logger used for traces.
     */
    private static Logger logger = Log.getLogger(Log.JONAS_NAMING_PREFIX);

    /**
     * {@inheritDoc}
     */
    public void modify(final Context componentContext) throws NamingException {
        // Get the ORB.
        final ORB orb = JacORBCosNaming.getOrb();

        // Get JOnAS ORB instance (that will load IIOP interceptors and then needs the correct classloader)
        IExecution<Void> initORB = new IExecution<Void>() {
            public Void execute() throws Exception {
                if (orb != null) {
                    try {
                        // Bind the java:comp/ORB object instance
                        componentContext.rebind(ORB, orb);
                    } catch (NamingException ne) {
                        // Just log the error, then rethrow the original Exception
                        String err = "Cannot bind java:comp/ORB object : " + ne.getMessage();
                        logger.log(BasicLevel.ERROR, err);
                        throw ne;
                    }
                }
                return (Void) null;
            }
        };
        ExecutionResult<Void> result = RunnableHelper.execute(getClass().getClassLoader(), initORB);

        // Throw a NamingException if needed
        if (result.hasException()) {
            logger.log(BasicLevel.ERROR, "Cannot init the ORB", result.getException());
            throw NamingExceptionHelper.create("Cannot init the ORB", result.getException());
        }


    }

    /**
     * Undo the changes done by this delegate on the given java:comp context.
     * @param componentContext <code>java:comp/</code> component context to be modified.
     * @throws NamingException thrown if something goes wrong
     *         during the {@link javax.naming.Context} update.
     */
    public void undo(final Context componentContext) throws NamingException {
        // Get the JOnAS instance ORB.
        if (JacORBCosNaming.getOrb() != null) {
            componentContext.unbind(ORB);
        }
    }

}
