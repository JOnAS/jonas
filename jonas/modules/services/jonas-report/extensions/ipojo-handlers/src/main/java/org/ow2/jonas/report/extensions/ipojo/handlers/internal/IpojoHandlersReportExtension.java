/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.report.extensions.ipojo.handlers.internal;

import org.apache.felix.ipojo.Factory;
import org.apache.felix.ipojo.HandlerFactory;
import org.ow2.jonas.report.api.IReportExtension;
import org.ow2.jonas.report.extensions.ipojo.handlers.generated.IpojoHandlerType;
import org.ow2.jonas.report.extensions.ipojo.handlers.generated.IpojoHandlers;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents an iPOJO handlers report extension
 * @author Jeremy Cazaux
 */
public class IpojoHandlersReportExtension implements IReportExtension {

    /**
     * The logger
     */
    private Log logger = LogFactory.getLog(IpojoHandlersReportExtension.class);

    /**
     * Path to the XSD
     */
    public static final String XSD_PATH =  "xsd/ipojo-handlers-report.xsd";

    /**
     * List of {@link HandlerFactory}
     */
    private List<HandlerFactory> handlerFactories;

    /**
     * Handler valide state token
     */
    public static final String VALID_STATE = "VALID";

    /**
     * Handler invalid state token
     */
    public static final String INVALID_STATE = "INVALID";

    /**
     * Default constructor
     */
    public IpojoHandlersReportExtension() {
        this.handlerFactories = new ArrayList<HandlerFactory>();
    }

    /**
     * {@inheritDoc}
     */
    public Object generateReport() {
        IpojoHandlers ipojoHandlers =  new IpojoHandlers();
        List<IpojoHandlerType> handlers = ipojoHandlers.getIpojoHandler();
        for (HandlerFactory handlerFactory: this.handlerFactories) {
            IpojoHandlerType handler = new IpojoHandlerType();
            handler.setName(handlerFactory.getName());
            handler.setBundleId(handlerFactory.getBundleContext().getBundle().getBundleId());

            int state = handlerFactory.getState();
            if (Factory.VALID == state) {
                handler.setState(VALID_STATE);
            } else {
                handler.setState(INVALID_STATE);
            }

            handlers.add(handler);
        }
        return ipojoHandlers;
    }

    /**
     * {@inheritDoc}
     */
    public Source getXsd() {
        InputStream xsd = getRootClass().getClassLoader().getResourceAsStream(XSD_PATH);
        Source source = null;
        if (xsd != null) {
            source = new StreamSource(xsd);
        } else {
            logger.error("Cannot find XSD " + XSD_PATH);
        }
        return source;
    }

    /**
     * {@inheritDoc}
     */
    public Class getRootClass() {
        return IpojoHandlers.class;
    }

    /**
     * @param handlerFactory The {@link HandlerFactory} to bind
     */
    public void bindHandlerFactory(final HandlerFactory handlerFactory) {
        this.handlerFactories.add(handlerFactory);
    }

    /**
     * @param handlerFactory The {@link HandlerFactory} to unbind
     */
    public void unbindHandlerFactory(final HandlerFactory handlerFactory) {
        this.handlerFactories.remove(handlerFactory);
    }
}
