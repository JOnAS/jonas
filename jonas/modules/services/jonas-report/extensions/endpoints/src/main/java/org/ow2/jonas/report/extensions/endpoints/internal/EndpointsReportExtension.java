/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.report.extensions.endpoints.internal;

import org.ow2.jonas.endpoint.collector.IEndpoint;
import org.ow2.jonas.report.api.IReportExtension;
import org.ow2.jonas.report.extensions.endpoints.generated.EndpointType;
import org.ow2.jonas.report.extensions.endpoints.generated.Endpoints;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents an Endpoint report extension
 * @author Jeremy Cazaux
 */
public class EndpointsReportExtension implements IReportExtension {

    /**
     * The logger
     */
    public static final Log logger = LogFactory.getLog(EndpointsReportExtension.class);

    /**
     * Path to the XSD
     */
    public static final String XSD_PATH = "xsd/endpoints-report.xsd";

    /**
     * List of {@link IEndpoint}
     */
    private List<IEndpoint> endpoints;

    /**
     * Default constructor
     */
    public EndpointsReportExtension() {
        this.endpoints = new ArrayList<IEndpoint>();
    }

    /**
     * {@inheritDoc}
     */
    public Object generateReport() {
        Endpoints endpoints = new Endpoints();
        List<EndpointType> endpointTypeList = endpoints.getEndpoint();

        for (IEndpoint endpoint: this.endpoints) {
            EndpointType endpointType = new EndpointType();
            endpointType.setHost(endpoint.getHost());
            endpointType.setProtocol(endpoint.getProtocol());
            endpointType.setUrl(endpoint.getUrl());
            endpointType.setSource(endpoint.getSource());
            endpointType.setPort(endpoint.getPort());
            endpointTypeList.add(endpointType);
        }

        return endpoints;
    }

    /**
     * {@inheritDoc}
     */
    public Source getXsd() {
        InputStream xsd = getRootClass().getClassLoader().getResourceAsStream(XSD_PATH);
        Source source = null;
        if (xsd != null) {
            source = new StreamSource(xsd);
        } else {
            logger.error("Cannot find XSD " + XSD_PATH);
        }
        return source;
    }

    /**
     * {@inheritDoc}
     */
    public Class getRootClass() {
        return Endpoints.class;
    }

    /**
     * @param endpoint The {@link IEndpoint} to bind
     */
    public void bindEndpoint(final IEndpoint endpoint) {
        if (!this.endpoints.contains(endpoint)) {
            this.endpoints.add(endpoint);
        }
    }

    /**
     * @param endpoint The {@link IEndpoint to bind}
     */
    public void unbindEndpoint(final IEndpoint endpoint) {
        //this.endpoints.remove(endpoint);
    }
}
