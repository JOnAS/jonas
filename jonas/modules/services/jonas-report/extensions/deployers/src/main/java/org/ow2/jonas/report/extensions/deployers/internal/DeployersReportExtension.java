/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.report.extensions.deployers.internal;

import org.ow2.jonas.report.api.IReportExtension;
import org.ow2.jonas.report.extensions.deployers.generated.Deployers;
import org.ow2.util.ee.deploy.api.deployer.IDeployerManager;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import java.io.InputStream;
import java.util.List;

/**
 * Represents a deployer report extension
 * @author Jeremy Cazaux
 */
public class DeployersReportExtension implements IReportExtension {

    /**
     * The logger
     */
    public static final Log logger = LogFactory.getLog(DeployersReportExtension.class);

    /**
     * Path to the XSD
     */
    public static final String XSD_PATH = "xsd/deployers-report.xsd";

    /**
     * The {@link org.ow2.util.ee.deploy.api.deployer.IDeployerManager}
     */
    private IDeployerManager deployerManager;

    /**
     * {@inheritDoc}
     */
    public Object generateReport() {
        Deployers  deployers = new Deployers();
        List<String> deployersList = deployers.getDeployer();
        for (String className: this.deployerManager.getDeployerClasses()) {
            deployersList.add(className);
        }
        return deployers;
    }

    /**
     * {@inheritDoc}
     */
    public Source getXsd() {
        InputStream xsd = getRootClass().getClassLoader().getResourceAsStream(XSD_PATH);
        Source source = null;
        if (xsd != null) {
            source = new StreamSource(xsd);
        } else {
            logger.error("Cannot find XSD " + XSD_PATH);
        }
        return source;
    }

    /**
     * {@inheritDoc}
     */
    public Class getRootClass() {
        return Deployers.class;
    }

    /**
     * @param deployerManager The {@link IDeployerManager} to bind
     */
    public void bindDeployerManager(final IDeployerManager deployerManager) {
        this.deployerManager = deployerManager;
    }

    /**
     * @param deployerManager The {@link IDeployerManager} to unbind
     */
    public void unbindDeployerManager(final IDeployerManager deployerManager) {
        this.deployerManager = null;
    }
}
