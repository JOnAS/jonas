/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.report.extensions.jvm.internal;

import org.ow2.jonas.report.api.IReportExtension;
import org.ow2.jonas.report.extensions.jvm.generated.Jvm;
import org.ow2.jonas.report.extensions.jvm.generated.PropertyType;
import org.ow2.jonas.report.extensions.jvm.generated.SystemPropertiesType;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * Represents a JVM report extension
 * @author Jeremy Cazaux
 */
public class JvmReportExtension implements IReportExtension {

    /**
     * Path to the XSD
     */
    public static final String XSD_PATH = "xsd/jvm-report.xsd";

    /**
     * The logger
     */
    public static Log logger = LogFactory.getLog(JvmReportExtension.class);

    /**
     * {@inheritDoc} 
     */
    public Object generateReport() {
        SystemPropertiesType systemPropertiesType = new SystemPropertiesType();
        List<PropertyType> properties = systemPropertiesType.getProperty();
        for (Map.Entry property: System.getProperties().entrySet()) {
            PropertyType propertyType = new PropertyType();
            propertyType.setName(String.class.cast(property.getKey()));
            propertyType.setValue(String.class.cast(property.getValue()));
            properties.add(propertyType);
        }
        Jvm jvm = new Jvm();
        jvm.setSystemProperties(systemPropertiesType);
        return jvm;
    }

    /**
     * {@inheritDoc}
     */
    public Source getXsd() {
        InputStream xsd = getRootClass().getClassLoader().getResourceAsStream(XSD_PATH);
        Source source = null;
        if (xsd != null) {
            source = new StreamSource(xsd);
        } else {
            logger.error("Cannot find XSD " + XSD_PATH);
        }
        return source;
    }

    /**
     * {@inheritDoc}
     */
    public Class getRootClass() {
        return Jvm.class;
    }
}
