/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.report.extensions.ipojo.factories.internal;

import org.apache.felix.ipojo.Factory;
import org.ow2.jonas.report.api.IReportExtension;
import org.ow2.jonas.report.extensions.ipojo.factories.generated.IpojoFactories;
import org.ow2.jonas.report.extensions.ipojo.factories.generated.IpojoFactoryType;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents an iPOJO factories extension
 * @author Jeremy Cazaux
 */
public class IpojoFactoriesReportExtension implements IReportExtension {

    /**
     * The logger
     */
    public static final Log logger = LogFactory.getLog(IpojoFactoriesReportExtension.class);

    /**
     * Path to the XSD
     */
    public static final String XSD_PATH = "xsd/ipojo-factories-report.xsd";

    /**
     * List of {@link Factory}
     */
    private List<Factory> factories;

    /**
     * Default constructor
     */
    public IpojoFactoriesReportExtension() {
        this.factories = new ArrayList<Factory>();
    }

    /**
     * {@inheritDoc}
     */
    public Object generateReport() {
        IpojoFactories ipojoFactories = new IpojoFactories();
        List<IpojoFactoryType> factoryTypeList = ipojoFactories.getIpojoFactory();
        for (Factory factory: this.factories) {
            IpojoFactoryType factoryType = new IpojoFactoryType();
            factoryType.setName(factory.getName());
            factoryType.setBundleId(factory.getBundleContext().getBundle().getBundleId());
            int state = factory.getState();
            if (Factory.VALID == state) {
                factoryType.setState("VALID");
            } else {
                factoryType.setState("INVALID");
                factoryType.setArch(factory.getDescription().toString());
            }
            factoryTypeList.add(factoryType);
        }
        return ipojoFactories;
    }

    /**
     * {@inheritDoc}
     */
    public Source getXsd() {
        InputStream xsd = getRootClass().getClassLoader().getResourceAsStream(XSD_PATH);
        Source source = null;
        if (xsd != null) {
            source = new StreamSource(xsd);
        } else {
            logger.error("Cannot find XSD " + XSD_PATH);
        }
        return source;
    }

    /**
     * {@inheritDoc}
     */
    public Class getRootClass() {
        return IpojoFactories.class;
    }

    /**
     * @param factory The {@link Factory} to bind
     */
    public void bindFactory(final Factory factory) {
        this.factories.add(factory);    
    }

    /**
     * @param factory The {@link Factory} to bind
     */
    public void unbindFactory(final Factory factory) {
        this.factories.remove(factory);
    }
}
