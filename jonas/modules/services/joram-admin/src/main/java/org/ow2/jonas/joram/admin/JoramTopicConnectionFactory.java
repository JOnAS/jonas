/**
 * JOnAS
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.joram.admin;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.felix.ipojo.annotations.Component;
import org.apache.felix.ipojo.annotations.Invalidate;
import org.apache.felix.ipojo.annotations.Property;
import org.apache.felix.ipojo.annotations.Requires;
import org.apache.felix.ipojo.annotations.Validate;
import org.objectweb.joram.client.connector.JoramAdapterMBean;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

@Component(name="JoramTopicConnectionFactory",immediate=false)
public class JoramTopicConnectionFactory {

    /**
     * Logger
     */
    private static Log logger = LogFactory.getLog(JoramTopicConnectionFactory.class);

    /**
     * Topic parameter : jndi name
     */
    @Property(name="jndi.name", mandatory=true)
    private String jndiName;

    /**
     * Service delegation to joram
     */
    @Requires
    private JoramAdapterMBean service;

    @Validate
    public void start() {
        ClassLoader old = null;
        try {
            // set the classloader to the bundle one, otherwise the object is not bound in the registry
            ClassLoader ext = this.getClass().getClassLoader();
            old = Thread.currentThread().getContextClassLoader();
            Thread.currentThread().setContextClassLoader(ext);

            service.createTopicCF(jndiName);
            logger.info("Joram Topic Connection Factory ''{0}'' created", jndiName);
        } catch (Exception e) {
            logger.error("Error when creating the topic CF '" + jndiName + "'", e);
        } finally {
            if (old != null) {
                Thread.currentThread().setContextClassLoader(old);
            }
        }
    }

    @Invalidate
    public void stop() {
        Context ctx;
        ClassLoader ext = this.getClass().getClassLoader();
        ClassLoader old = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(ext);
            ctx = new InitialContext();
        } catch (NamingException e) {
            logger.error("Unable to get InitialContext", e);
            return;
        } finally {
            Thread.currentThread().setContextClassLoader(old);
        }

        try {
            ctx.unbind(jndiName);
            logger.info("Joram Topic Connection Factory ''{0}'' removed", jndiName);
        } catch (NamingException e) {
            logger.error("Unable to unbind Topic Connection Factory ''{0}''", jndiName, e);
        }
    }
}
