/**
 * JOnAS
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.joram.admin;

import org.apache.felix.ipojo.annotations.Bind;
import org.apache.felix.ipojo.annotations.Component;
import org.apache.felix.ipojo.annotations.Invalidate;
import org.apache.felix.ipojo.annotations.Property;
import org.apache.felix.ipojo.annotations.Requires;
import org.apache.felix.ipojo.annotations.Unbind;
import org.apache.felix.ipojo.annotations.Validate;
import org.objectweb.joram.client.connector.JoramAdapterMBean;
import org.objectweb.joram.client.jms.admin.UserMBean;
import org.osgi.framework.ServiceReference;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

@Component(name="JoramUser",immediate=false)
public class JoramUser {

    /**
     * Logger
     */
    private static Log logger = LogFactory.getLog(JoramUser.class);

    /**
     * User parameter : name
     */
    @Property(name="name", mandatory=true)
    private String name;

    /**
     * User parameter : password
     */
    @Property(name="password", mandatory=true)
    private String password;


    /**
     * Service delegation to joram
     */
    @Requires
    private JoramAdapterMBean service;

    /**
     * UserMBean service
     */
    private UserMBean user;

    @Validate
    public void start() {
        try {
            String res = service.createUser(name, password);
            logger.info("Create the User '" + name + "' : " + res);

        } catch (Exception e) {
            logger.error("Error when creating the User '" + name + "'", e);

        }
    }

    @Invalidate
    public void stop() {

        try {
            if (user != null) {
                user.delete();
                logger.info("Remove the user '" + name + "'");
            } else {
                logger.warn("Unable to remove the user service '" + name + "'. Service unavailable");
            }
        } catch (Exception e) {
            logger.error("Error when removing the user '" + name + "'", e);

        }
    }

    @Bind(optional = true, aggregate = true)
    public void bindUserService(final UserMBean service, final ServiceReference ref) {

        String serviceName = (String) ref.getProperty("name");
        if (serviceName.startsWith(name + "[#")) {
            user = service;
            logger.debug("Bind UserService - " + serviceName);
        }
    }

    @Unbind
    public void unbindUserService(final ServiceReference ref) {
            String name = (String) ref.getProperty("name");
            if (name.startsWith(name + "[#")) {
                user = null;
                logger.debug("Unbind Service - " + name);
            }

    }

}
