/**
 * JOnAS
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.joram.admin;

import org.apache.felix.ipojo.annotations.Bind;
import org.apache.felix.ipojo.annotations.Component;
import org.apache.felix.ipojo.annotations.Invalidate;
import org.apache.felix.ipojo.annotations.Property;
import org.apache.felix.ipojo.annotations.Requires;
import org.apache.felix.ipojo.annotations.Unbind;
import org.apache.felix.ipojo.annotations.Validate;
import org.objectweb.joram.client.connector.JoramAdapterMBean;
import org.objectweb.joram.client.jms.TopicMBean;
import org.osgi.framework.ServiceReference;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

@Component(name="JoramTopic",immediate=false)
public class JoramTopic {

    /**
     * Logger
     */
    private static Log logger = LogFactory.getLog(JoramTopic.class);

    /**
     * Topic parameter : jndi name
     */
    @Property(name="jndi.name", mandatory=true)
    private String jndiName;

    /**
     * Service delegation to joram
     */
    @Requires
    private JoramAdapterMBean service;

    /**
     * TopicMBean service
     */
    private TopicMBean topic;

    @Validate
    public void start() {

        ClassLoader old = null;
        try {
            // set the classloader to the bundle one, otherwise the object is not bound in the registry
            ClassLoader ext = this.getClass().getClassLoader();
            old = Thread.currentThread().getContextClassLoader();
            Thread.currentThread().setContextClassLoader(ext);

            String res = service.createTopic(jndiName);
            logger.info("Create the topic '" + jndiName + "' : " + res);

        } catch (Exception e) {
            logger.error("Error when creating the topic '" + jndiName + "'", e);

        } finally {
            if (old != null) {
                Thread.currentThread().setContextClassLoader(old);
            }
        }
    }

    @Invalidate
    public void stop() {

        try {
            if (topic != null) {
                topic.delete();
                logger.info("Remove the topic '" + jndiName + "'");
            } else {
                logger.warn("Unable to remove the topic service '" + jndiName + "'. Service unavailable");
            }
        } catch (Exception e) {
            logger.error("Error when removing the topic '" + jndiName + "'", e);

        }
    }

    @Bind(optional = true, aggregate = true)
    public void bindTopicService(final TopicMBean service, final ServiceReference ref) {

        String serviceName = (String) ref.getProperty("name");
        if (serviceName.startsWith(jndiName + "[#")) {
            topic = service;
            logger.debug("Bind TopicService - " + serviceName);
        }
    }

    @Unbind
    public void unbindTopicService(final ServiceReference ref) {
            String name = (String) ref.getProperty("name");
            if (name.startsWith(jndiName + "[#")) {
                topic = null;
                logger.debug("Unbind Service - " + name);
            }

    }

}
