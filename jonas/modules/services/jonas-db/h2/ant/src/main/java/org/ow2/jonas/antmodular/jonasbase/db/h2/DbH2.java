/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004-2011 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.antmodular.jonasbase.db.h2;

import org.ow2.jonas.antmodular.jonasbase.bootstrap.JTask;
import org.ow2.jonas.antmodular.jonasbase.bootstrap.JOnASBaseTask;
import org.ow2.jonas.antmodular.jonasbase.db.base.Db;

import java.io.File;

/**
 * Allow to configure the DB H2 service.
 * @author Jeremy Cazaux
 */
public class DbH2 extends Db {

    /**
     * Name of the implementation class of the db service
     */
    private final static String DB_CLASS = "org.ow2.jonas.db.h2.H2DBServiceImpl";

    /**
     * Name of the implementation class of H2
     */
    public final static String H2_CLASS_NAME = "org.h2.Driver";

    /**
     * H2 mapper
     */
    public final static String H2_MAPPER = "rdb.hsql";

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUrl(final String host) {
        String hostname = host;
        if (hostname == null) {
            hostname = DEFAULT_HOST;
        } 
        String port = this.portNumber;
        if (port == null) {
            port = DEFAULT_PORT;
        }
        String database = this.dbName;
        if (database == null) {
            database = DEFAULT_DATABASE;
        }
        return "jdbc:h2:tcp://" + hostname + ":" + port + "/" + database;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getClassname() {
        return H2_CLASS_NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getMapper() {
        return H2_MAPPER;
    }

    /**
     * Execute all tasks
     */
    public void execute() {
        super.execute();
        String jBaseConf = this.destDir.getPath() + File.separator + "conf";
        JTask jTask = new JTask();
        jTask.setDestDir(this.destDir);
        jTask.changeValueForKey(this.INFO, jBaseConf, JOnASBaseTask.JONAS_CONF_FILE,
                this.DB_CLASS_PROPERTY, this.DB_CLASS, false);
        addTask(jTask);
        super.executeAllTask();
    }
}
