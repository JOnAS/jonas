/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004-2012 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.db.base;

/**
 * MBean interface for the Database service
 * @author Florent Benoit
 */
public interface AbsDBServiceImplMBean {

    /**
     * @return the BD port
     */
    String getPortNumber();

    /**
     * @return the name of the db
     */
    String getDbName();

    /**
     * @return the list of pair of <username, password>
     */
    String getDbUsers();
}