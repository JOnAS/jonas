/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004-2011 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.antmodular.jonasbase.db.base;

import java.io.File;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.tools.ant.BuildException;
import org.ow2.jonas.antmodular.jonasbase.bootstrap.JOnASBaseTask;
import org.ow2.jonas.antmodular.jonasbase.bootstrap.AbstractJOnASBaseAntTask;
import org.ow2.jonas.antmodular.jonasbase.bootstrap.JTask;

/**
 * Allow to configure the DB service.
 * @author Florent Benoit
 */
public abstract class Db extends AbstractJOnASBaseAntTask {

    /**
     * Info for the logger.
     */
    protected static final String INFO = "[DB] ";

    /**
     * Name of the property for changing the port.
     */
    private static final String PORT_PROPERTY = "jonas.service.db.port";

    /**
     * Name of the property for changing the database name
     */
    private static final String DB_NAME = "jonas.service.db.dbname";

    /**
     * Name of the property for changing login and password
     */
    private static final String USERS = "jonas.service.db.users";

    /**
     * Name of the property for changing the db class
     */
    protected final static String DB_CLASS_PROPERTY = "jonas.service.db.class";

    /**
     * Default database name
     */
    protected final static String DEFAULT_DATABASE = "db_jonas";

    /**
     * Default user
     */
    public final static String DEFAULT_USER = "jonas";

    /**
     * Default password
     */
    public final static String DEFAULT_PASSWORD = "jonas";

    /**
     * Default host
     */
    protected final static String DEFAULT_HOST = "localhost";

    /**
     * Default port number
     */
    protected final static String DEFAULT_PORT = "9001";

    /**
     * Port number.
     */
    protected String portNumber = null;

    /**
     * Name of the database
     */
    protected String dbName = null;

    /**
     * Login and password of users
     */
    protected String users = null;

    /**
     * Default constructor.
     */
    public Db() {
        super();
    }

    /**
     * Set the port number for the Db service.
     * @param portNumber the port for the Db service
     */
    public void setPort(final String portNumber) {
            this.portNumber = portNumber;
    }

    /***
     * Set the name of the Database
     * @param dbName  name of the database
     */
    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    /**
     * Set all login and password of all users
     * @param users login:password of all users
     */
    public void setUsers(String users) {
        this.users = users;
    }

    /**
     * @return db users
     */
    public String getUsers() {
        return this.users;
    }

    /**
     * @return the pair of username,password of the first user
     */
    public AbstractMap.SimpleEntry<String, String> getFirstUsernameAndPassword() {        
        //search of the first user
        StringTokenizer stringTokenizer = new StringTokenizer(this.users.trim(), ",");
        String firstUser = null;
        if (stringTokenizer.hasMoreElements()) {
            firstUser = stringTokenizer.nextElement().toString();
        }
        stringTokenizer = new StringTokenizer(firstUser.trim(), ":");
        //addition of the login
        if (stringTokenizer.hasMoreElements()) {           
            String login = stringTokenizer.nextElement().toString();
            //addition of the password
            if (stringTokenizer.hasMoreElements()) {
                String password = stringTokenizer.nextElement().toString();
                return new AbstractMap.SimpleEntry<String, String>(login, password);
            }
        }

        return new AbstractMap.SimpleEntry<String, String>(DEFAULT_USER, DEFAULT_PASSWORD);
    }

    /**
     * Check the properties.
     */
    private void checkProperties() {
        if (portNumber == null) {
            throw new BuildException(INFO + "Property 'portNumber' is missing.");
        }
    }

    /**
     * @param host Hostname
     * @return the url of the database
     */
    public abstract String getUrl(final String host);

    /**
     * @return classname of the database
     */
    public abstract String getClassname();

    /**
     * @return the mapper of the database
     */
    public abstract String getMapper();

    /**
     * Execute this task.
     */
    public void execute() {
        super.execute();
        checkProperties();

        // Path to JONAS_BASE
        String jBaseConf = this.destDir.getPath() + File.separator + "conf";

        JTask jTask = new JTask();
        jTask.setDestDir(this.destDir);
        jTask.changeValueForKey(INFO, jBaseConf, JOnASBaseTask.JONAS_CONF_FILE,
                                PORT_PROPERTY, portNumber, false);

        if (this.dbName != null) {
            jTask.changeValueForKey(INFO, jBaseConf, JOnASBaseTask.JONAS_CONF_FILE,
                                    DB_NAME, this.dbName, false);
        }
        if (this.users != null) {
            jTask.changeValueForKey(INFO, jBaseConf, JOnASBaseTask.JONAS_CONF_FILE,
                                    USERS, this.users, false);
        }

        addTask(jTask);

    }
}
