/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004-2011 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.antmodular.jonasbase.db.hsql;

import org.ow2.jonas.antmodular.jonasbase.bootstrap.JOnASBaseTask;
import org.ow2.jonas.antmodular.jonasbase.bootstrap.JTask;
import org.ow2.jonas.antmodular.jonasbase.db.base.Db;

import java.io.File;

/**
 * Allow to configure the DB HSQL service.
 * @author Jeremy Cazaux
 */
public class DbHsql extends Db {

    /**
     * Name of the HSQL implementation class
     */
    private final static String DB_CLASS = "org.ow2.jonas.db.hsqldb.HsqlDBServiceImpl";


    /**
     * Name of the implementation class of H2
     */
    public final static String HSQL_CLASS_NAME = "org.hsqldb.jdbcDriver";

    /**
     * H2 mapper
     */
    public final static String HSQL_MAPPER = "rdb.hsql";

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUrl(final String host) {
        String hostname = host;
        if (hostname == null) {
            hostname = DEFAULT_HOST;
        }
        String port = this.portNumber;
        if (port == null) {
            port = DEFAULT_PORT;
        }
        String database = this.dbName;
        if (database == null) {
            database = DEFAULT_DATABASE;
        }
        return "jdbc:hsqldb:hsql://" + hostname + ":" + port + "/" + database;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getClassname() {
        return HSQL_CLASS_NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getMapper() {
        return HSQL_MAPPER;
    }

    /**
     * Execute all tasks
     */
    public void execute() {
        super.execute();
        String jBaseConf = this.destDir.getPath() + File.separator + "conf";
        JTask jTask = new JTask();
        jTask.setDestDir(this.destDir);
        jTask.changeValueForKey(this.INFO, jBaseConf, JOnASBaseTask.JONAS_CONF_FILE,
                this.DB_CLASS_PROPERTY, this.DB_CLASS, false);
        addTask(jTask);
        super.executeAllTask();
    }
}
