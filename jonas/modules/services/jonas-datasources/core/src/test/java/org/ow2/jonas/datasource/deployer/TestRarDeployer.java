/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2010 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.datasource.deployer;

import java.util.ArrayList;
import java.util.List;

import org.ow2.util.ee.deploy.api.deployable.IDeployable;
import org.ow2.util.ee.deploy.api.deployable.RARDeployable;
import org.ow2.util.ee.deploy.api.deployer.DeployerException;
import org.ow2.util.ee.deploy.api.deployer.IDeployer;
import org.ow2.util.ee.deploy.impl.deployer.AbsDeployerList;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

/**
 * Fake RAR deployer for the tests.
 * @author mleduque
 *
 */
public class TestRarDeployer extends AbsDeployerList<RARDeployable> implements IDeployer<RARDeployable> {

    /**
     * The logger.
     */
    private Log logger = LogFactory.getLog(TestRarDeployer.class);

    /**
     * The deployed deployables.
     */
    private List<IDeployable<?>> deployed = new ArrayList<IDeployable<?>>();

    /**
     * {@inheritDoc}
     */
    public void deploy(final IDeployable<RARDeployable> deployable) throws DeployerException {
        logger.info("Deploying deployable {0}", deployable);
        if (deployable != null && supports(deployable)) {
            logger.info("Deployable {0} supported by this deployer.", deployable);
            this.deployed.add(deployable);
        }
    }

    /**
     * {@inheritDoc}
     */
    public boolean isDeployed(final IDeployable<RARDeployable> deployable) throws DeployerException {
        logger.info("Deployed objects : {0}", this.deployed);
        return (this.deployed.contains(deployable));
    }

    /**
     * {@inheritDoc}
     */
    public boolean supports(final IDeployable<?> deployable) {
        return (RARDeployable.class.isAssignableFrom(deployable.getClass()));
    }

    /**
     * {@inheritDoc}
     */
    public void undeploy(final IDeployable<RARDeployable> deployable) throws DeployerException {
        if (deployable != null && supports(deployable)) {
            if (!isDeployed(deployable)) {
                throw new DeployerException("Deplyable is not deployed.");
            }
            this.deployed.remove(deployable);
        } else {
            throw new DeployerException("Invalid deployable");
        }
    }

    /**
     * Returns the list of deployed objects.
     * @return the list of deployed objects.
     */
    public List<IDeployable<?>> getDeployed() {
        return this.deployed;
    }

}
