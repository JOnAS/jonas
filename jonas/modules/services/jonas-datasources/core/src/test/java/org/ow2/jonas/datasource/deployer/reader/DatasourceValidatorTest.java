/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2010 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.datasource.deployer.reader;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.ExpectedExceptions;
import org.testng.annotations.Test;
import org.testng.Assert;

import org.xml.sax.SAXException;

public class DatasourceValidatorTest {

    /**
     * Path to the valid xml file for datasource.
     */
    private static final String DATASOURCE_VALID_XML_PATH =
        "src/test/resources/exemple-datasource-good.xml";

    /**
     * Path to the invalide xml file for datasource.
     */
    private static final String DATASOURCE_INVALID_XML_PATH =
        "src/test/resources/exemple-datasource-bad.xml";

    /**
     * Valid XML File
     */
    private File goodXML;


    /**
     * Invalid XML File
     */
    private File badXML;

    @BeforeClass
    public void init(){
        goodXML = new File(DATASOURCE_VALID_XML_PATH);
        Assert.assertNotNull(goodXML);

        badXML = new File(DATASOURCE_INVALID_XML_PATH);
        Assert.assertNotNull(badXML);
    }

    @Test
    public void testValidXmlFile() throws Exception {
        DatasourceValidator xmlTest = null;
        xmlTest = new DatasourceValidator(goodXML);
        Assert.assertFalse(xmlTest == null);
    }


    @Test(expectedExceptions = {SAXException.class})
    public void testInvalidXmlFile() throws SAXException, ParserConfigurationException, IOException {
        DatasourceValidator xmlTest = new DatasourceValidator(badXML);

    }
}
