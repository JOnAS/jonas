/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2010 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.datasource.deployer.reader;

import java.io.File;
import java.util.List;

import org.ow2.jonas.datasource.deployer.binding.DatasourceType;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * This is the class test of the XmlTransform class.
 * @author Rodrigue SOUBEIGA
 */

public class DatasourceReaderTest {

    /**
     * Path to the valid xml file for datasource.
     */
    private static final String DATASOURCE_VALID_XML_PATH = "src/test/resources/exemple-datasource-good.xml";

    /**
     * The list of DatasourceType provide by the xml file.
     */
    private List<DatasourceType> datasourceList = null;

    /**
     * The instance of XmlTransfor class it is used in order to test the
     * differents method of the XmlTransform class.
     */
    private DatasourceXmlReader classtransform;

    /**
     * The file object created from the xmlFile.
     */
    private File datasourceFile;

    /**
     * The method used to initialise the different objects needed to execute
     * tests methods.
     */
    @BeforeClass
    public void init() {
        this.datasourceFile = new File(DATASOURCE_VALID_XML_PATH);

        this.classtransform = new DatasourceXmlReader();

    }

    /**
     * We test if the xml file exists.
     */
    @Test
    public void fileExist() {
        Assert.assertTrue(datasourceFile.exists());
    }

    /**
     * Test of the extraction of the datasourceType list from the xml file.
     * @throws Exception
     */

    @Test
    public void extractDataSourcesTest() throws Exception {

        datasourceList = classtransform.extractDataSources(datasourceFile).getDatasources();
        Assert.assertNotNull(datasourceList, "Datasources unmarshalling failed");

    }

    /**
     * Test of the DatasourceType initialisation.
     */
    @Test(dependsOnMethods = {"datasoureConfigurationContentsTest"})
    public void initdataSourceTest() {
        for (DatasourceType ds : datasourceList) {
            ds.getConnectionManagerConfiguration().setConnteststmt(null);
            ds = classtransform.initDataSource(ds);
            String response = ds.getConnectionManagerConfiguration().getConnteststmt();
            Assert.assertNotNull(response, "Initialisation failure : connteststmt.");

            ds.getDatasourceConfiguration().setUsername(null);
            ds = classtransform.initDataSource(ds);
            response = ds.getDatasourceConfiguration().getUsername();
            Assert.assertNotNull(response, "Initialisation failure : username.");

        }
    }

    /**
     * Test of the DatasourceConfiguration initialisation with a specific xml
     * file.
     */
    @Test(dependsOnMethods = {"extractDataSourcesTest"})
    public void datasoureConfigurationContentsTest() {
        // Check the first datasource
        DatasourceType ds1 = datasourceList.get(0);
        Assert.assertEquals(ds1.getDatasourceConfiguration().getName(), "jdbc_1");
        Assert.assertEquals(ds1.getDatasourceConfiguration().getUrl(), "jdbc:oracle:thin://localhost:1521/db_oracle");
        Assert.assertEquals(ds1.getDatasourceConfiguration().getClassname(), "oracle.jdbc.driver.OracleDriver");
        Assert.assertEquals(ds1.getDatasourceConfiguration().getUsername(), "jonasoracle");
        Assert.assertEquals(ds1.getDatasourceConfiguration().getPassword(), "jonaspassoracle");
        Assert.assertEquals(ds1.getDatasourceConfiguration().getMapper(), "rdb.oracle");
        Assert.assertEquals(ds1.getDatasourceConfiguration().getDescription(), null);
        Assert.assertEquals(ds1.getDatasourceConfiguration().getIsolationlevel(), null);

        // Check the second datasource
        DatasourceType ds2 = datasourceList.get(1);
        Assert.assertEquals(ds2.getDatasourceConfiguration().getName(), "jdbc_1");
        Assert.assertEquals(ds2.getDatasourceConfiguration().getUrl(), "jdbc:hsqldb:hsql://localhost:9001/db_jonas");
        Assert.assertEquals(ds2.getDatasourceConfiguration().getClassname(), "org.hsqldb.jdbcDriver");
        Assert.assertEquals(ds2.getDatasourceConfiguration().getUsername(), "jonas");
        Assert.assertEquals(ds2.getDatasourceConfiguration().getPassword(), "jonas");
        Assert.assertEquals(ds2.getDatasourceConfiguration().getMapper(), "rdb.hsql");
        Assert.assertEquals(ds2.getDatasourceConfiguration().getDescription(), null);
        Assert.assertEquals(ds2.getDatasourceConfiguration().getIsolationlevel(), null);

    }
}
