/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2010 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.datasource.deployer;

import org.ow2.jonas.datasource.deployer.DatasourceDeployable;
import org.ow2.util.archive.api.IFileArchive;
import org.ow2.util.archive.impl.ArchiveManager;
import org.ow2.util.ee.deploy.impl.deployable.factory.utils.XmlHelper;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.Assert;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

import java.io.File;

public class DatasourceDeployableTest {

    /**
     * Path to the test example for datasource.
     */
    private static final String DATASOURCE_XML_PATH =
        "src/test/resources/exemple-datasource.xml";

    /**
     * Datasource namespace.
     */
    private static final String DATASOURCE_NS =
    "http://jonas.ow2.org/ns/datasource/1.1";

    /**
     * Archive made from the datasource.
     */
    private IFileArchive datasourceArchive = null;


    /**
     * xmlHelper
     */
    private XmlHelper xmlHelper = null;

    /**
     * DatasourceDeployable
     */
    DatasourceDeployable dsDeployable = null;



    /**
     * Prepares test data.
     * @throws Exception
     */
    @BeforeClass
    public void init() throws Exception {

        datasourceArchive = (IFileArchive) ArchiveManager.getInstance()
                                    .getArchive(new File(DATASOURCE_XML_PATH));
        Assert.assertNotNull(datasourceArchive);
        xmlHelper = new XmlHelper();

    }

    @Test
    public void testDatasourceConstructor() throws Exception {
        dsDeployable = new DatasourceDeployable(datasourceArchive);
        Assert.assertNotNull(dsDeployable);
    }

    @Test
    public void testRootElementNameSpace() throws Exception {
        Assert.assertEquals(xmlHelper.getRootElementNameSpace(
                                datasourceArchive.getFile()), DATASOURCE_NS);
    }
}
