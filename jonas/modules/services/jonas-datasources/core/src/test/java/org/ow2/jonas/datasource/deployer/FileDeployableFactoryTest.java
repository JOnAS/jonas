/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2010 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.datasource.deployer;

import java.io.File;

import org.ow2.jonas.datasource.deployer.DatasourceDeployable;
import org.ow2.util.archive.api.IFileArchive;
import org.ow2.util.archive.impl.ArchiveManager;
import org.ow2.util.ee.deploy.api.deployable.IDeployable;
import org.ow2.util.ee.deploy.impl.helper.DeployableHelper;
import org.ow2.util.ee.deploy.api.deployable.FileDeployable;
import org.ow2.util.ee.deploy.api.deployable.factory.IFileDeployableFactoryManager;
import org.ow2.util.ee.deploy.api.deployable.factory.XmlFileDeployableFactory;
import org.ow2.util.ee.deploy.impl.deployable.factory.DefaultFileDeployableFactoryManager;
import org.ow2.util.ee.deploy.impl.deployable.factory.XmlFileDeployableFactoryImpl;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Test class for the deployable factories.
 * @author Alexis RENOUX
 */
public class FileDeployableFactoryTest {

    /**
     * Path to the test example for datasource.
     */
    private static final String DATASOURCE_XML_PATH =
                            "src/test/resources/exemple-datasource.xml";

    /**
     * Datasource namespace.
     */
    private static final String DATASOURCE_NS =
                                "http://jonas.ow2.org/ns/datasource/1.1";

    /**
     * Archive made from the datasource.
     */
    private IFileArchive datasourceArchive = null;

    /**
     * The xml deployable factory.
     */
    private XmlFileDeployableFactory factory = null;

    /**
     * The deployable factory manager.
     */
    private IFileDeployableFactoryManager manager = null;

    /**
     * Prepares test data.
     * @throws Exception
     */
    @BeforeClass
    public void init() throws Exception {
        // ClassCastException
        datasourceArchive = (IFileArchive) ArchiveManager.getInstance()
                                    .getArchive(new File(DATASOURCE_XML_PATH));
        Assert.assertNotNull(datasourceArchive);

        factory = new XmlFileDeployableFactoryImpl();
        factory.registerFileDeployable(
                DatasourceDeployable.class,
                DATASOURCE_NS);

        manager = new DefaultFileDeployableFactoryManager();
        manager.addFileDeployableFactory(factory);
    }

    /**
     * Test datasource conversion with the factory.
     * @throws Exception
     */
    @Test
    public void convertDatasourceArchiveToDeployable() throws Exception {
        FileDeployable<?, ?> fileDeployable = factory.getFileDeployable(datasourceArchive);
        Assert.assertEquals(fileDeployable.getClass(),DatasourceDeployable.class);
    }

    /**
     * Test datasource conversion with the manager.
     * @throws Exception
     */
    @Test
    public void convertDatasourceArchiveWithManager() throws Exception {
        FileDeployable<?, ?> fileDeployable = manager.getFileDeployable(datasourceArchive);
        Assert.assertEquals(fileDeployable.getClass(),DatasourceDeployable.class);
    }


    /**
     * Test deployment plan conversion with the DeployableHelper+factory.
     * @throws Exception
     */
    @Test
    public void convertWithDeployableHelper() throws Exception {
        DeployableHelper.setFileDeployableFactory(factory);
        IDeployable<?> deployable = DeployableHelper.getDeployable(datasourceArchive);
        Assert.assertEquals(deployable.getClass(), DatasourceDeployable.class);
    }

    /**
     * Test deployment plan conversion with the DeployableHelper+manager.
     * @throws Exception
     */
    @Test
    public void convertWithDeployableHelperAndManager() throws Exception {
        DeployableHelper.setFileDeployableFactory(manager);
        IDeployable<?> deployable = DeployableHelper.getDeployable(datasourceArchive);
        Assert.assertEquals(deployable.getClass(), DatasourceDeployable.class);
    }

}
