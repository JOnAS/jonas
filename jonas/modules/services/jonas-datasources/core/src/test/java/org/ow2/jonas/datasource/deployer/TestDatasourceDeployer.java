/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2010 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.datasource.deployer;

import java.io.File;

import org.ow2.jonas.Version;
import org.ow2.util.archive.api.IFileArchive;
import org.ow2.util.archive.impl.FileArchiveImpl;
import org.ow2.util.ee.deploy.api.deployer.DeployerException;
import org.ow2.util.ee.deploy.impl.deployer.DeployerManager;
import org.testng.Assert;
import org.testng.FileAssert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.ow2.jonas.properties.ServerProperties;
import java.util.Properties;


/**
 * Test class for the DatasourceDeployer.
 * What needs to be tested :
 *  - Given a Datasource XML file, the deployer accepts to deploy it
 *  - a RAR file is generated
 *  - The jonas-rar.xml is inside and contains the required data
 *  - undeployment works
 * @author Alexis Renoux.
 * @author Mickaël LEDUQUE.
 */
public class TestDatasourceDeployer {

    /**
     * The XML File.
     */
    private File xmlfile = null;

    /**
     * The archive.
     */
    private IFileArchive archive = null;

    /**
     * The deployable.
     */
    private DatasourceDeployable deployable = null;

    /**
     * The datasource deployer.
     */
    private DatasourceDeployer datasourceDeployer;

    /**
     * The directory to resources.
     */
    private static final String TEST_XML_DIR = "src" + File.separator + "test" + File.separator + "resources";

    /**
     * The path to the datasource test file.
     */
    private static final String TEST_XML_PATH = TEST_XML_DIR + File.separator + "datasources.xml";

    /**
     * The fake rar deployer.
     */
    private TestRarDeployer rarDeployer;

    @BeforeClass
    public void init() throws Exception {
        xmlfile = new File(TEST_XML_PATH);
        archive = new FileArchiveImpl(xmlfile);
        datasourceDeployer = new DatasourceDeployer();
        datasourceDeployer.setServerProperties(new MyProperties());
        datasourceDeployer.setDmRar(System.getProperty("rar.path"));
        deployable = new DatasourceDeployable(archive);
        DeployerManager deployerManager = new DeployerManager();
        rarDeployer = new TestRarDeployer();
        deployerManager.register(rarDeployer);
        datasourceDeployer.setDeployerManager(deployerManager);

    }

    @Test()
    public void deployerSupportsDeployable() {
        Assert.assertTrue(datasourceDeployer.supports(deployable));
    }

    @Test(dependsOnMethods = {"deployerSupportsDeployable"})
    public void tryDeploy() throws Exception {
        datasourceDeployer.deploy(deployable);
    }

    @Test(dependsOnMethods = {"tryDeploy"})
    public void rarDeployed() throws DeployerException {
        Assert.assertTrue(this.datasourceDeployer.isDeployed(deployable));
        Assert.assertEquals(this.rarDeployer.getDeployed().size(), 2);
    }

   private class MyProperties implements ServerProperties {


    public String getValue(String key) {
        return System.getenv("JONAS_ROOT");
    }

    public String getValue(String key, String defaultVal) {
        return defaultVal;
    }

    public boolean getValueAsBoolean(String key, boolean def) {
        return def;
    }

    public String[] getValueAsArray(String key) {
        String [] str = new String[5];
        return str;
    }

    public String getDomainName() {
        return "domain";
    }

    public String getServerName() {
        return "server";
    }

    public String getPropFileName() {
        return "file";
    }

    public Properties getConfigFileEnv() {
        return new Properties();
    }

    public String getVersionsFile() {
        return "file";
    }

    public boolean isMaster() {
        return false;
    }

    public boolean isDevelopment() {
        return false;
    }

    public String getWorkDirectory() {
        return System.getProperty("java.io.tmpdir") ;

    }

   }
}
