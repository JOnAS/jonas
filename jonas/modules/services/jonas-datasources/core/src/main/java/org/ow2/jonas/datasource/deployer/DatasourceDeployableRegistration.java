/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2010 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.datasource.deployer;

import org.ow2.util.ee.deploy.api.deployable.factory.XmlFileDeployableFactory;

/**
 * Class to register the deployable type to the XML deployable factory.
 * @author mleduque
 */
public class DatasourceDeployableRegistration {

    /**
     * Bind method to register the deployable class.
     * @param factory the XML deployable factory to deploy to.
     */
    public void registerToXmlFileDeployableFactory(final XmlFileDeployableFactory factory) {
        if (factory != null) {
            factory.registerFileDeployable(DatasourceDeployable.class, DatasourceDeployable.NAMESPACE_10);
            factory.registerFileDeployable(DatasourceDeployable.class, DatasourceDeployable.NAMESPACE);
        }
    }

    /**
     * Unbind method to unregister the deployable class.
     * @param factory the XML deployable factory to undeploy from.
     */
    public void unregisterFromXmlFileDeployableFactory(final XmlFileDeployableFactory factory) {
        if (factory != null) {
            factory.unregisterFileDeployable(DatasourceDeployable.NAMESPACE_10);
            factory.unregisterFileDeployable(DatasourceDeployable.NAMESPACE);
        }
    }
}
