/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2010 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.datasource.deployer;

import org.ow2.jonas.lib.work.AbsCleanTask;
import org.ow2.jonas.workcleaner.IDeployerLog;
import org.ow2.jonas.workcleaner.LogEntry;
import org.ow2.jonas.workcleaner.WorkCleanerException;

/**
 * JOnAS datasource unused directory clean task class. This class provides a way for
 * removing files which are inconsistent files.
 * @author Benoit Pelletier
 */
public class JonasDatasourceCleanTask extends AbsCleanTask {

    /**
     * {@link IDeployerLog} of the task.
     */
    private IDeployerLog deployerLog = null;

    /**
     * {@link JonasDatasourceDeployer} reference.
     */
    private JonasDatasourceDeployer dsDeployerService;

    /**
     * Construct a new EAR clean task.
     * @param dsDeployerService JonasDatasourceDeployer reference
     * @param deployerLog DeployerLog of the task
     */
    public JonasDatasourceCleanTask(final JonasDatasourceDeployer dsDeployerService, final IDeployerLog deployerLog) {
        this.dsDeployerService = dsDeployerService;
        this.deployerLog = deployerLog;
    }

    /**
     * Check if the package pointed by the log entry is currently deploy.
     * @param logEntry entry in a deploy log
     * @return true if the package pointed by the log entry is currently deployed
     * @throws WorkCleanerException If the method fails
     */
    @Override
    protected boolean isDeployedLogEntry(final LogEntry logEntry) throws WorkCleanerException {
        // Check if the EJB3 file is deployed
        return dsDeployerService.isDsDeployedByWorkName(logEntry.getCopy().getPath());
    }

    /**
     * {@inheritDoc}
     * @see org.ow2.jonas.lib.work.AbsCleanTask#getDeployerLog()
     */
    @Override
    public IDeployerLog getDeployerLog() {
        return deployerLog;
    }
}
