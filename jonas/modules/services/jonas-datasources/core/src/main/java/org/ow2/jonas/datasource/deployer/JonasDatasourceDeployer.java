/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2010 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.datasource.deployer;

import java.io.File;
import java.io.IOException;

import org.ow2.jonas.lib.work.DeployerLog;
import org.ow2.jonas.properties.ServerProperties;
import org.ow2.jonas.workcleaner.CleanTask;
import org.ow2.jonas.workcleaner.DeployerLogException;
import org.ow2.jonas.workcleaner.WorkCleanerService;
import org.ow2.util.ee.deploy.api.deployer.IDeployerManager;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;
import org.ow2.util.plan.fetcher.api.IResourceFetcherFactoryManager;
import org.ow2.util.plan.repository.api.IRepositoryManager;

/**
 * DatasourceDeployer with bind and unbind methods.
 * @author mleduque
 */
public class JonasDatasourceDeployer extends DatasourceDeployer  {

    /**
     * Logger.
     */
    private static Log logger = LogFactory.getLog(JonasDatasourceDeployer.class);

    /**
     * Constructor to handle Exception throwing.
     * @throws IOException if seed rar creation failed.
     */
    public JonasDatasourceDeployer() throws IOException {
    }

    /**
     * Set the Server Properties.
     * @param props {@link ServerProperties} instance
     */
    @Override
    public void setServerProperties(final ServerProperties props) {
        this.serverProps = props;
    }

    /**
     * Method called when the workCleanerService is bound to the component.
     * @param workCleanerService the workCleanerService reference
     */
    protected void setWorkCleanerService(final WorkCleanerService workCleanerService) {
        initWorkingDirectory();
        File fileLog = new File(archiveFile.getPath() + File.separator + serverProps.getServerName() + ".log");

        if (!fileLog.exists()) {
            try {
                // Create log file
                fileLog.createNewFile();
            } catch (IOException e) {
                logger.error("Cannot create the log file " + fileLog);
            }
        }

        try {
            // Create the logger
            deployerLog = new DeployerLog(fileLog);
            CleanTask cleanTask = new JonasDatasourceCleanTask(this, deployerLog);

            workCleanerService.registerTask(cleanTask);
            workCleanerService.executeTasks();
        } catch (DeployerLogException e) {
            logger.error("Cannot register the clean task", e);
        }
    }

    public void setResourceFetcherFactoryManager(IResourceFetcherFactoryManager resourceFetcherFactoryManager) {
        this.resourceFetcherFactoryManager = resourceFetcherFactoryManager;
    }

    public void setRepositoryManager(IRepositoryManager repositoryManager) {
        this.repositoryManager = repositoryManager;
    }

    /**
     * Bind method for the DeployerManager.
     * @param deployerManager the deployer manager.
     */
    public void registerToDeployerManager(final IDeployerManager deployerManager) {
        deployerManager.register(this);
        this.setDeployerManager(deployerManager);
    }

    /**
     * Unbind method for the DeployerManager.
     * @param deployerManager the deployer manager.
     */
    public void unregisterfromDeployerManager(final IDeployerManager deployerManager) {
        deployerManager.unregister(this);

    }

    /**
     * Test if the specified unpack name is already deployed or not.
     * @param name the name of the DS file.
     * @return true if the EJB3 is deployed, else false.
     */
    public boolean isDsDeployedByWorkName(final String name) {
        return this.isDeployed(name);

    }
}
