/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2010 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.datasource.deployer;

import org.ow2.jonas.datasource.deployer.binding.Datasources;
import org.ow2.util.archive.api.IFileArchive;
import org.ow2.util.ee.deploy.api.deployable.FileDeployable;
import org.ow2.util.ee.deploy.impl.deployable.AbsDeployable;

/**
 * @author Djamel eddine ZABCHI
 */
public class DatasourceDeployable extends AbsDeployable<DatasourceDeployable>
        implements FileDeployable<DatasourceDeployable, Datasources> {

    /**
     * Namespace for datasources 1.0.
     */
    public static final String NAMESPACE_10 = "http://jonas.ow2.org/ns/datasource/1.0";
    /**
     * Namespace for datasources.
     */
    public static final String NAMESPACE = "http://jonas.ow2.org/ns/datasource/1.1";
    /**
     * The attached DatasourceType object.
     */
    private Datasources attachedDatasources = null;

    /**
     * Defines and create a deployable for the given archive.
     * @param archive the given archive.
     */
    public DatasourceDeployable(final IFileArchive archive) {
        super(archive);
    }

    public Datasources getAttachedData() {
        return this.attachedDatasources;
    }

    public void setAttachedData(final Datasources attachedData) {
        this.attachedDatasources = attachedData;

    }

}
