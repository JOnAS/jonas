/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.jmx.internal.interceptor;

import org.ow2.jonas.jmx.Interceptor;

import javax.management.MBeanServer;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Call invocation which will dynamically implement the instance using interceptors
 * @author Mohammed Boukada
 */
public class InvocationHandlerImpl implements InvocationHandler {

    /**
     * List of interceptors
     */
    private List<Interceptor> interceptors;

    /**
     * Invocation handler delegate
     */
    private InvocationHandlerImpl delegate;

    /**
     * Target MBeanServer
     */
    private MBeanServer target;


    /**
     * Construct an InvocationHandler
     */
    public InvocationHandlerImpl() {
        interceptors = new ArrayList<Interceptor>();
    }

    /**
     * Construct an InvocationHandler with target MBeanServer
     */
    public InvocationHandlerImpl(MBeanServer target) {
        interceptors = new ArrayList<Interceptor>();
        this.target = target;
    }

    /**
     * Invoke method
     * @param proxy
     * @param method
     * @param args
     * @return invocation result
     * @throws Throwable
     */
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (interceptors.size() > 0) {
            // If there is at least one interceptor it is a call
            // on principal MBeanServer
            InvocationContextImpl invocationContextImpl = new InvocationContextImpl(method, args, interceptors, target);
            return invocationContextImpl.proceed();
        } else {
            // In this case, it is a call on outer MBeanServer
            // See org.ow2.jonas.services.bootstrap.mbeanbuilder.JOnASMBeanServerBuilder
            if (delegate == null) {
                throw new Exception("Invocation handler delegate not set");
            }
            return delegate.invoke(proxy, method, args);
        }
    }

    /**
     * Add an interceptor
     * @param interceptor
     */
    public void addInterceptor (Interceptor interceptor) {
        this.interceptors.add(interceptor);
    }

    /**
     * Remove an interceptor
     * @param interceptor
     */
    public void removeInterceptor (Interceptor interceptor) {
        this.interceptors.remove(interceptor);
    }

    /**
     * Get invocation handler delegate
     * @return invocation handler delegate
     */
    public InvocationHandlerImpl getDelegate() {
        return delegate;
    }

    /**
     * Set invocation handler delegate
     * @param delegate invocation handler delegate
     */
    public void setDelegate(InvocationHandlerImpl delegate) {
        this.delegate = delegate;
    }
}
