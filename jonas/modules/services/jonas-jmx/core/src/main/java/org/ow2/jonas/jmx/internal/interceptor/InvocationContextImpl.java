/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.jmx.internal.interceptor;

import org.ow2.jonas.jmx.Interceptor;

import javax.interceptor.InvocationContext;
import javax.management.MBeanServer;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

/**
 * Manage the sequential performance of interceptors
 * @author Mohammed Boukada
 */
public class InvocationContextImpl implements InvocationContext {

    /**
     * List of interceptors
     */
    private List<Interceptor> interceptors;

    /**
     * Intercepted method.
     */
    private Method method;

    /**
     * Parameters of the method invocation.
     */
    private Object[] parameters;

    /**
     * Index of the invokers. It is increased after each call on the proceed
     * method.
     */
    private int index;

    /**
     * Target MBeanServer
     */
    private MBeanServer target;

    /**
     * Construct an InvocationContext
     * @param method
     * @param parameters
     * @param interceptors
     */
    public InvocationContextImpl(Method method, Object[] parameters, List<Interceptor> interceptors, MBeanServer target) {
        this.method = method;
        this.parameters = parameters;
        this.interceptors = interceptors;
        this.target = target;
        this.index = interceptors.size() - 1;
    }

    /**
     * Return the target MBeanServer
     * @return
     */
    public Object getTarget() {
        return this.target;
    }

    /**
     * returns the timer associated with an @AroundTimeout method.
     * @since EJB 3.1 version.
     * @return <code>null</code>
     */
    @Override
    public Object getTimer() {
        return null;
    }

    /**
     * returns the called method
     * @return
     */
    public Method getMethod() {
        return this.method;
    }

    /**
     * Returns parameters of the called method
     * @return
     */
    public Object[] getParameters() {
        return this.parameters;
    }

    /**
     * Sets parameters for the called method
     * @param objects
     */
    public void setParameters(Object[] objects) {
        this.parameters = objects;
    }

    public Map<String, Object> getContextData() {
        return null;
    }

    /**
     * Calls interceptors
     * @return
     * @throws Exception
     */
    public Object proceed() throws Exception {
        try {
            return this.interceptors.get(index--).invoke(this);
        } finally {
            index++;
        }
    }
}
