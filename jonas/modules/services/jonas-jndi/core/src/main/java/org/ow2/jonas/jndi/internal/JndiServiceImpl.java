/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id:
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.jndi.internal;

import org.ow2.jonas.jndi.JndiService;
import org.ow2.jonas.lib.service.AbsServiceImpl;
import org.ow2.jonas.service.ServiceException;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

/**
 * JOnAS JNDI service
 * @author Jeremy Cazaux
 */
public class JndiServiceImpl extends AbsServiceImpl implements JndiService {

    /**
     * The default JNDI policy
     */
    private Policy defaultPolicy;

    /**
     * Logger
     */
    private static final Log logger = LogFactory.getLog(JndiServiceImpl.class);


    /**
     * @throws ServiceException {@link ServiceException}
     */
    @Override
    protected void doStart() throws ServiceException {
    }

    /**
     * @throws ServiceException {@link ServiceException}
     */
    @Override
    protected void doStop() throws ServiceException {
    }

    /**
     * @param defaultPolicy The default policy to set
     */
    public void setDefaultPolicy(final String defaultPolicy) throws JndiEntriesException {
        this.defaultPolicy = JndiEntriesService.getPolicyAsEnum(defaultPolicy);
    }

    /**
     * @param defaultPolicy The {@link Policy} to set
     */
    public void setDefaultPolicy(final Policy defaultPolicy) {
        this.defaultPolicy = defaultPolicy;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Policy getDefaultPolicy() {
        return this.defaultPolicy;
    }
}
