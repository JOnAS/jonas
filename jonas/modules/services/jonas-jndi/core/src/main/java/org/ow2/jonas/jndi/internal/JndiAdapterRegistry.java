/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id:
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.jndi.internal;

import org.ow2.jonas.configadmin.XmlConfigurationAdapter;
import org.ow2.jonas.configadmin.XmlConfigurationAdapterRegistry;
import org.ow2.jonas.jndi.JndiService;

/**
 * Component that will be registered as an OSGi service with a 'namespace' service property
 * @author Jeremy Cazaux
 */
public class JndiAdapterRegistry implements XmlConfigurationAdapterRegistry {

    /**
     * The {@link XmlConfigurationAdapter} for the JNDI service
     */
    private XmlConfigurationAdapter jndiAdapter;

    /**
     * The jndi service
     */
    private JndiService jndiService;

    /**
     * Default constructor
     */
    public JndiAdapterRegistry() {
    }

    /**
     * Call when the component is registered
     */
    public void start() {
        this.jndiAdapter = new JndiAdapter(this.jndiService);
    }

    /**
     * Call when the component is unregistered
     */
    public void stop() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public XmlConfigurationAdapter getAdapter(final String name) {
        return this.jndiAdapter;
    }

    /**
     * @param jndiService The JNDI service to bind
     */
    public void bindJndiService(final JndiService jndiService) {
        this.jndiService = jndiService;
    }

    /**
     * @param jndiService The JNDI service to unbind
     */
    public void unbindJndiService(final JndiService jndiService) {
        this.jndiService = null;
    }
}
