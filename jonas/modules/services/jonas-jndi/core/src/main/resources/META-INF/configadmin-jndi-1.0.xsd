<?xml version="1.0" encoding="UTF-8"?>

<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema"
            xmlns="http://jonas.ow2.org/ns/jndientries/1.0"
            targetNamespace="http://jonas.ow2.org/ns/jndientries/1.0"
            xmlns:tns="http://jonas.ow2.org/ns/jndientries/1.0"
            elementFormDefault="qualified"
            attributeFormDefault="unqualified">
  <xsd:annotation>
    <xsd:documentation>
      <![CDATA[
      JOnAS: Java(TM) Open Application Server
      Copyright (C) 2012 Bull S.A.S
      Contact: jonas-team@ow2.org

      This library is free software; you can redistribute it and/or
      modify it under the terms of the GNU Lesser General Public
      License as published by the Free Software Foundation; either
      version 2.1 of the License, or any later version.

      This library is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
      Lesser General Public License for more details.

      You should have received a copy of the GNU Lesser General Public
      License along with this library; if not, write to the Free Software
      Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
      USA
      ]]>
    </xsd:documentation>
  </xsd:annotation>

  <!-- The Entry type -->
  <xsd:complexType name="entryType">
    <xsd:annotation>
      <xsd:documentation>
        The &lt;entry> describe an entry to inject in the JNDI context.

        Example:
        &lt;entry">
          &lt;name>java:global/developper&lt;/name>
          &lt;description>this is my entry&lt;/description>
          &lt;type>java.lang.String&lt;/type>
          &lt;value>The JOnAS Team&lt;/value>
        &lt;/entry>
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>

      <!-- entry name -->
      <xsd:element name="name"
                   type="xsd:string"
                   minOccurs="1"
                   maxOccurs="1">
        <xsd:annotation>
          <xsd:documentation>
            The &lt;name> is the name of an entry to inject in the JNDI context.

            Example:
            &lt;name>java:global/jndi/developper&lt;/name>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>

      <!-- entry description -->
      <xsd:element name="description"
                   type="xsd:string"
                   minOccurs="0"
                   maxOccurs="1">
        <xsd:annotation>
          <xsd:documentation>
            The &lt;description> is the description of an entry to inject in the JNDI context.

            Example:
            &lt;description>This is the description of my JNDI entry&lt;/description>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>

      <!-- entry type -->
      <xsd:element name="type"
                   type="xsd:string"
                   minOccurs="1"
                   maxOccurs="1">
        <xsd:annotation>
          <xsd:documentation>
            The &lt;type> is the type of the value of an entry to inject in
            the JNDI context. The type should be a type available in the Java
            plateform (java.lang.String, java.lang.Integer,java.lang.Float,
            ...)

            Example:
            &lt;type">java.lang.String&lt;/type>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>

      <!-- entry value -->
      <xsd:element name="value"
                   minOccurs="1"
                   maxOccurs="1"
                   nillable="false">
          <xsd:annotation>
            <xsd:documentation>
              The &lt;value> is the value of an entry to inject in
              the JNDI context.

              Example:
              &lt;value>The JOnAS Team&lt;/value>
            </xsd:documentation>
          </xsd:annotation>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>

  <!-- Type for the JNDI entries element -->
  <xsd:complexType name="jndiEntriesType">
    <xsd:annotation>
      <xsd:documentation>
        The &lt;jndiEntries> type contains an optional policy and a list of entries
        to inject in the JNDI context.

        Example:
        &lt;jndi-entries xmlns="http://jonas.ow2.org/ns/jndientries/1.0"
                         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
          &lt;policy>strict-consistency&lt;/policy>
          &lt;entry>
            &lt;name>java:global/jndi/developper&lt;/name>
            &lt;description>Developper name&lt;/description>
            &lt;type>java.lang.String&lt;/type>
            &lt;value>JOnAS Team&lt;/value>
          &lt;/entry>
        &lt;/jndi-entries>
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element minOccurs="0"
                   maxOccurs="1"
                   name="policy">
        <xsd:simpleType>
          <xsd:annotation>
            <xsd:documentation>
              The &lt;policy> describe the JNDI injection policy of each entries of the XML file.
              Possible values are 'strict-consistency' (default value) and 'last-update'.

              - strict-consistency: requires an exact match between entries of the XML file and the
              directory content. if an entry of the XML file isn't already bound in the JNDI
              context, it'll be injected. Otherwise, the deployment of the whole file will fail.
              When the deployable is undeployed, each entries will be removed from the JNDI context.

              - last-update: the directory contains the latest value injected. If an entry of the XML
              file isn't already bound in the JNDI context, it'll be injected. Otherwise the entry
              will be updated with the new value.

              Example:
              &lt;policy>strict-consistency&lt;/policy>
            </xsd:documentation>
          </xsd:annotation>
          <xsd:restriction base="xsd:string">
            <xsd:enumeration value="strict-consistency"/>
            <xsd:enumeration value="last-update"/>
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs="1"
                   maxOccurs="unbounded"
                   name="entry"
                   type="entryType" />
    </xsd:sequence>
  </xsd:complexType>

  <!-- Root element -->
  <xsd:element name="jndi-entries" type="jndiEntriesType">
    <xsd:annotation>
      <xsd:documentation>
        Example:
        &lt;jndi-entries xmlns="http://jonas.ow2.org/ns/jndientries/1.0"
                         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
          &lt;!-- ... -->
        &lt;/jndi-entries>
      </xsd:documentation>
    </xsd:annotation>
    <xsd:unique name="uniques">
      <xsd:selector xpath="tns:entry" />
      <xsd:field xpath="tns:name" />
    </xsd:unique>
  </xsd:element>
</xsd:schema>
