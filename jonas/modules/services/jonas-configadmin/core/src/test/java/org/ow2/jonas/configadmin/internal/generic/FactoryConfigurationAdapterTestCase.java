/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.configadmin.internal.generic;

import java.util.Set;

import org.ow2.jonas.configadmin.ConfigurationInfo;
import org.ow2.util.xml.DocumentParser;
import org.testng.annotations.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * A {@code DefaultFactoryConfigurationAdapterTestCase} is ...
 *
 * @author Guillaume Sauthier
 */
public class FactoryConfigurationAdapterTestCase extends AbstractConfigurationAdapterTestCase {
    @Test
    public void testConvert() throws Exception {
        Document doc = DocumentParser.getDocument(getUrl("/one-factory-configuration.xml"), false, null);
        Element node = findFirstElementNamed(doc, "factory-configuration");

        DefaultFactoryConfigurationAdapter adapter = new DefaultFactoryConfigurationAdapter();
        Set<ConfigurationInfo> info = adapter.convert(node);

        assertThat(info, is( notNullValue() ));
        assertThat(info.size(), is( equalTo( 1 ) ));

        ConfigurationInfo ci = info.iterator().next();
        assertThat(ci.getPid(), is( equalTo( "org.ow2.jonas.db.h2.H2DBServiceImpl" ) ));
        assertThat(ci.isFactory(), is( equalTo( true ) ));
    }

}
