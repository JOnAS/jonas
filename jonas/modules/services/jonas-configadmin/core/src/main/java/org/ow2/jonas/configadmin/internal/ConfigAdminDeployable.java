/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id:RegistryServiceImpl.java 10372 2007-05-14 13:58:42Z sauthieg $
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.configadmin.internal;

import java.util.List;

import org.ow2.util.archive.api.IFileArchive;
import org.ow2.util.ee.deploy.impl.deployable.AbsDeployable;
import org.ow2.util.ee.deploy.api.deployable.FileDeployable;

/**
 * A {@code ConfigAdminDeployable} represents an XML files containing ConfigAdmin Configurations.
 * The root DOM Element has the {@literal 'http://jonas.ow2.org/ns/configadmin/1.0'} namespace URI.
 * <br/>
 * Example:
 * <pre>
 * {@code <configadmin xmlns="http://jonas.ow2.org/ns/configadmin/1.0">
 *
 *    <configuration pid="org.ow2.jonas.db.h2.H2DBServiceImpl">
 *      <property name="dbname">woooot</property>
 *      <property name="users">john</property>
 *      <property name="port">9999</property>
 *    </configuration>
 *
 *  </configadmin>}</pre>
 *
 * @author Guillaume Sauthier
 */
public class ConfigAdminDeployable extends AbsDeployable<ConfigAdminDeployable>
                                   implements FileDeployable<ConfigAdminDeployable, List<ConfigurationItem>> {

    /**
     * Namespace for config-admin.
     */
    public static final String NAMESPACE = "http://jonas.ow2.org/ns/configadmin/1.0";

    /**
     * Items contained in this file (one for each child of the root element).
     */
    private List<ConfigurationItem> data;


    /**
     * Defines and create a deployable for the given archive.
     *
     * @param archive the given archive.
     */
    public ConfigAdminDeployable(IFileArchive archive) {
        super(archive);
    }


    public List<ConfigurationItem> getAttachedData() {
        return data;
    }

    public void setAttachedData(List<ConfigurationItem> attachedData) {
        this.data = attachedData;
    }
}
