/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.configadmin.internal.generic;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.ow2.jonas.configadmin.AdapterException;
import org.ow2.jonas.configadmin.ConfigurationInfo;
import org.ow2.jonas.configadmin.internal.model.ConfigurationType;
import org.ow2.jonas.configadmin.internal.model.PropertyType;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;
import org.w3c.dom.Element;

/**
 * A {@code DefaultConfigurationAdapter} is ...
 *
 * @author Guillaume Sauthier
 */
public class DefaultConfigurationAdapter extends AbstractConfigurationAdapter {
    /**
     * Logger.
     */
    private static final Log logger = LogFactory.getLog(DefaultConfigurationAdapter.class);

    public DefaultConfigurationAdapter() throws JAXBException {
        super();
    }

    public Set<ConfigurationInfo> convert(Element node) throws AdapterException {

        try {

            Unmarshaller unmarshaller = context.createUnmarshaller();
            JAXBElement<ConfigurationType> element = unmarshaller.unmarshal(node, ConfigurationType.class);
            ConfigurationType factoryConfiguration = element.getValue();

            String servicePid = factoryConfiguration.getPid();

            // Create a Factory Configuration
            ConfigurationInfo info = new ConfigurationInfo(servicePid);

            logger.debug("Creating Configuration for pid {{0}}", servicePid);

            Map<String, Object> properties = info.getProperties();
            for (PropertyType property : factoryConfiguration.getProperties()) {
                properties.put(property.getName(), property.getValue());
            }

            logger.debug("Resulting Configuration: {{0}}", info);

            return Collections.singleton(info);

        } catch (JAXBException e) {
            logger.warn("Cannot unmarshall node {{0}}.", node, e);
        }

        return null;
    }
}
