/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.configadmin.internal;

import java.util.Set;

import org.ow2.jonas.configadmin.ConfigurationInfo;

/**
 * A {@code ConfigurationAssociationEvent} is fired when a {@code ConfigurationItem}
 * is associated or dissociated to/from a {@code XmlConfigurationAdapterRegistry}.
 *
 * @author Guillaume Sauthier
 */
public class ConfigurationAssociationEvent {

    /**
     * Item being associated / dissociated.
     */
    private ConfigurationItem source;

    /**
     * Adapted configuration (may be null).
     */
    private Set<ConfigurationInfo> info;

    public ConfigurationAssociationEvent(ConfigurationItem source, Set<ConfigurationInfo> info) {
        this.source = source;
        this.info = info;
    }

    public ConfigurationItem getSource() {
        return source;
    }

    public Set<ConfigurationInfo> getInfo() {
        return info;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ConfigurationAssociationEvent that = (ConfigurationAssociationEvent) o;

        if (info != null ? !info.equals(that.info) : that.info != null) {
            return false;
        }
        return source.equals(that.source);
    }

    @Override
    public int hashCode() {
        int result = source.hashCode();
        result = 31 * result + (info != null ? info.hashCode() : 0);
        return result;
    }
}
