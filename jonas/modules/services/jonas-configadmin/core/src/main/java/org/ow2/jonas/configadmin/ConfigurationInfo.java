/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.configadmin;

import java.util.HashMap;
import java.util.Map;

/**
 * A {@code DefaultConfigurationInfo} is ...
 *
 * @author Guillaume Sauthier
 */
public class ConfigurationInfo {

    private String pid;
    private boolean factory;
    private Map<String, Object> properties;

    public ConfigurationInfo(String pid) {
        this(pid, false);
    }

    public ConfigurationInfo(String pid, boolean factory) {
        this.pid = pid;
        this.factory = factory;
        this.properties = new HashMap<String, Object>();
    }

    public String getPid() {
        return pid;
    }

    public boolean isFactory() {
        return factory;
    }

    public Map<String, Object> getProperties() {
        return properties;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ConfigurationInfo");
        sb.append("[pid='").append(pid).append('\'');
        sb.append(", factory=").append(factory);
        sb.append(", properties=").append(properties);
        sb.append(']');
        return sb.toString();
    }
}
