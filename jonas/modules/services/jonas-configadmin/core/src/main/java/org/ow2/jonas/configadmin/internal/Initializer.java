/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id:RegistryServiceImpl.java 10372 2007-05-14 13:58:42Z sauthieg $
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.configadmin.internal;

import org.ow2.util.ee.deploy.api.deployable.factory.XmlFileDeployableFactory;

/**
 * Created by IntelliJ IDEA.
 * User: guillaume
 * Date: 11/05/11
 * Time: 17:51
 * To change this template use File | Settings | File Templates.
 */
public class Initializer {
    /**
     * Bind method to register the deployable class.
     * @param factory the XML deployable factory to deploy to.
     */
    public void registerXmlFileDeployableFactory(final XmlFileDeployableFactory factory) {
        factory.registerFileDeployable(ConfigAdminDeployable.class, ConfigAdminDeployable.NAMESPACE);
    }

    /**
     * Unbind method to unregister the deployable class.
     * @param factory the XML deployable factory to undeploy from.
     */
    public void unregisterXmlFileDeployableFactory(final XmlFileDeployableFactory factory) {
        factory.unregisterFileDeployable(ConfigAdminDeployable.NAMESPACE);
    }
}
