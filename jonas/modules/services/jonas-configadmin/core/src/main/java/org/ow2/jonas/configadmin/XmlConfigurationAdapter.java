/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.configadmin;

import java.util.Set;

import org.w3c.dom.Element;

/**
 * A {@code XmlConfigurationAdapter} is responsible to convert a
 * DOM Element into one or multiple ConfigurationInfo (equivalent to the
 * ConfigAdmin Configuration object).
 *
 * @author Guillaume Sauthier
 */
public interface XmlConfigurationAdapter {

    /**
     * Convert the given Node into one or more ConfigurationInfo instance(s).
     * @param node DOM Element to be converted
     * @return Adapted ConfigurationInfo(s)
     * @throws AdapterException if the Element cannot be converted into ConfigurationInfo(s)
     */
    Set<ConfigurationInfo> convert(Element node) throws AdapterException;
}
