/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.configadmin.internal;

import org.ow2.jonas.configadmin.XmlConfigurationAdapterRegistry;

/**
 * An {@code AdapterRegistryEvent} is fired when a {@code XmlConfigurationAdapterRegistry}
 * is being registered or unregistered.
 *
 * @author Guillaume Sauthier
 */
public class AdapterRegistryEvent {

    /**
     * Registry being registered/unregistered.
     */
    private XmlConfigurationAdapterRegistry source;

    /**
     * Associated namespace URI.
     */
    private String namespace;

    public AdapterRegistryEvent(XmlConfigurationAdapterRegistry source, String namespace) {
        this.source = source;
        this.namespace = namespace;
    }

    public XmlConfigurationAdapterRegistry getSource() {
        return source;
    }

    public String getNamespace() {
        return namespace;
    }
}
