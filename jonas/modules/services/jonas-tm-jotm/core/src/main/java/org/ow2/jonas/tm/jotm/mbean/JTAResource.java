/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id:JTAResource.java 10430 2007-05-22 11:44:51Z sauthieg $
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.tm.jotm.mbean;

// JOnAS imports
import javax.transaction.xa.Xid;

import org.ow2.jonas.lib.management.javaee.J2EEResource;
import org.ow2.jonas.lib.management.reconfig.PropertiesConfigurationData;
import org.ow2.jonas.tm.jotm.JOTMTransactionService;


/**
 * MBean class for JTAResource Management
 * @author Eric Hardesty JSR 77 (J2EE Management Standard)
 */
public class JTAResource extends J2EEResource {

    /**
     * Service name as used to label configuration properties
     */
    public static final String SERVICE_NAME = "jtm";

    // Transaction Service configuration properties
    /**
     * name of the 'timeout' configuration parameter
     */
    static final String TIMEOUT = "jonas.service.jtm.timeout";

    /**
     * Value used as sequence number by reconfiguration notifications
     */
    private long sequenceNumber = 0;

    /**
     * Transactions time-out
     */
    private Integer timeOut;

    /**
     * JTM port number
     */
    private Integer portNumber;

    /**
     * JTM host name
     */
    private String hostName;

    /**
     * true if JTM is local, false if remote
     */
    private Boolean localJtm;

    /**
     * Managed JTM
     */
    private JOTMTransactionService jtm;

    /**
     * JTA Resource constructor
     * @param objectName String conformant to a JTAResource OBJECT_NAME in JSR77
     * @param jtm Managed JTM
     * @param timeOut transactions time-out
     * @param localJtm true if JTM is local, false if remote
     * @param portNumber JTM port number
     * @param hostName JTM host name
     */
    public JTAResource(final String objectName, final JOTMTransactionService jtm, final Integer timeOut, final Boolean localJtm,
            final Integer portNumber, final String hostName) {
        super(objectName);
        this.jtm = jtm;
        this.timeOut = timeOut;
        this.localJtm = localJtm;
        this.portNumber = portNumber;
        this.hostName = hostName;
    }

    /**
     * @return Returns the timeOut.
     */
    public Integer getTimeOut() {
        return timeOut;
    }

    /**
     * @param timeOut The timeOut to set.
     */
    public void setTimeOut(final Integer timeOut) {
        this.timeOut = timeOut;
        jtm.setTimeout(timeOut.intValue());
        // Send a notification containing the new value of this property to the
        // listner MBean
        sendReconfigNotification(++sequenceNumber, SERVICE_NAME, new PropertiesConfigurationData(TIMEOUT, timeOut.toString()));
    }

    /**
     * @return true if JTM is local, false if remote
     */
    public Boolean isLocalJtm() {
        return localJtm;
    }

    /**
     * @return JTM port number
     */
    public Integer getPortNumber() {
        return portNumber;
    }

    /**
     * @return JTM host name
     */
    public String getHostName() {
        return hostName;
    }

    /**
     * Save updated configuration
     */
    public void saveConfig() {
        sendSaveNotification(++sequenceNumber, SERVICE_NAME);
    }

    /**
     * @return Returns the totalBegunTransactions.
     */
    public Integer getTotalBegunTransactions() {
        return new Integer(jtm.getTotalBegunTransactions());
    }

    /**
     * @return Returns the totalCommittedTransactions.
     */
    public Integer getTotalCommittedTransactions() {
        return new Integer(jtm.getTotalCommittedTransactions());
    }

    /**
     * @return Returns the totalCurrentTransactions.
     */
    public Integer getTotalCurrentTransactions() {
        return new Integer(jtm.getTotalCurrentTransactions());
    }

    /**
     * @return Returns the totalExpiredTransactions.
     */
    public Integer getTotalExpiredTransactions() {

        return new Integer(jtm.getTotalExpiredTransactions());
    }
    /**
     * @return Returns the totalRolledbackTransactions.
     */
    public Integer getTotalRolledbackTransactions() {
        return new Integer(jtm.getTotalRolledbackTransactions());
    }

    /**
     * Reset all transaction counters
     */
    public void resetAllCounters() {
        jtm.resetAllTxTotalCounters();
    }

    /**
     * @return Returns all active Xids.
     */
    public Xid[] getAllActiveXids() {
        return jtm.getAllActiveXids();
    }

    /**
     * @return Returns all active Transactions.
     */
    public String [] getAllActiveTx() {
        String [] mysArray;

        mysArray = jtm.getAllActiveTx();
        return mysArray;
    }

    /**
     * @return Returns all Transactions that require administrator recovery action.
     */
    public String [] getAllRecoveryTx() {
        String [] mysArray;

        mysArray = jtm.getAllRecoveryTx();
        return mysArray;
    }

    /**
     * @return Returns all XAResources that require administrator recovery action.
     */
    public String [] getAllXAResource(final String xatx) {
        String [] mysArray;
        mysArray = jtm.getAllXAResource(xatx);
        return mysArray;
    }

    /**
     * administrator recovery action: Commit.
     * @return int error, or 0 if OK.
     */
    public int commitXAResource(final String xatx) {
        int commiterror;
        commiterror = jtm.commitXAResource(xatx);
        return commiterror;
    }

    /**
     * administrator recovery action: rollback
     * @return int error, or 0 if OK.
     */
    public int rollbackXAResource(final String xatx) {
        int rollbackerror;
        rollbackerror = jtm.rollbackXAResource(xatx);
        return rollbackerror;
    }

    /**
     * administrator recovery action: forget
     * @return int error, or 0 if OK.
     */
    public int forgetXAResource(final String xatx) {
        int forgeterror;
        forgeterror = jtm.forgetXAResource(xatx);
        return forgeterror;
    }
}