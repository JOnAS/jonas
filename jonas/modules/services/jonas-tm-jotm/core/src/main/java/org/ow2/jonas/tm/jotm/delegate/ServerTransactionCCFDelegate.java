/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.tm.jotm.delegate;

import org.ow2.jonas.naming.JComponentContextFactoryDelegate;
import org.ow2.jonas.tm.TransactionService;

/**
 * Implements the TransactionCCFDelegate abstract class for server usage.
 * @author Francois Fornaciari
 */
// TODO remove interface
public class ServerTransactionCCFDelegate extends TransactionCCFDelegate implements JComponentContextFactoryDelegate {

    /**
     * Transaction Service reference.
     */
    private TransactionService transactionService;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void init() {
        userTransaction = transactionService.getUserTransaction();
        tsr = transactionService.getTransactionSynchronizationRegistry();
    }

    /**
     * @param transactionService Transaction Service reference.
     */
    public void setTransactionService(final TransactionService transactionService) {
        this.transactionService = transactionService;
    }

}
