/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.tm.jotm.delegate;

import org.ow2.jonas.naming.JComponentContextFactoryDelegate;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.transaction.TransactionSynchronizationRegistry;
import javax.transaction.UserTransaction;

/**
 * Store the {@link javax.transaction.UserTransaction} object
 * under the <code>java:comp/UserTransaction</code> name.
 * @author Francois Fornaciari
 */
public abstract class TransactionCCFDelegate implements JComponentContextFactoryDelegate {

    /**
     * UT Context Name (in java:comp/).
     */
    private static final String USER_TRANSACTION = "UserTransaction";
    private static final String TR_SYNC_REGISTRY = "TransactionSynchronizationRegistry";

    /**
     * UserTransaction object, to be shared by all components.
     */
    protected UserTransaction userTransaction = null;

    /**
     * TransactionSynchronizationRegistry object, to be shared by all components.
     */
    protected TransactionSynchronizationRegistry tsr = null;

    /**
     * Initialize the Context.
     * @throws NamingException if unable to create the InitialContext.
     */
    protected void init() throws NamingException {}

    /**
     * {@inheritDoc}
     */
    public void modify(final Context componentContext) throws NamingException {
        // init if needed
        if (userTransaction == null) {
            init();
        }

        // Bind objects to java:comp/
        if (userTransaction != null) {
            componentContext.rebind(USER_TRANSACTION, userTransaction);
        }
        if (tsr  != null) {
            componentContext.rebind(TR_SYNC_REGISTRY, tsr);
        }
    }

    /**
     * Undo the changes done by this delegate on the given java:comp context.
     * @param componentContext <code>java:comp/</code> component context to be modified.
     * @throws NamingException thrown if something goes wrong
     *         during the {@link javax.naming.Context} update.
     */
    public void undo(final Context componentContext) throws NamingException {
        if (userTransaction != null) {
            componentContext.unbind(USER_TRANSACTION);
        }
        if (tsr != null) {
            componentContext.unbind(TR_SYNC_REGISTRY);
        }
    }

}
