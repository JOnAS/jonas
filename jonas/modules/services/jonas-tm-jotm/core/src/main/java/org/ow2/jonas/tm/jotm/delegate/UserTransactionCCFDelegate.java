/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.tm.jotm.delegate;

import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;
import org.ow2.jonas.lib.util.Log;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.TransactionSynchronizationRegistry;
import javax.transaction.UserTransaction;

/**
 * Implements the TransactionCCFDelegate abstract class for client usage.
 * @author Guillaume Sauthier
 */
public class UserTransactionCCFDelegate extends TransactionCCFDelegate {

    /**
     * JNDI Global name for the UserTransaction.
     */
    private static final String USER_TRANSACTION_JNDI_NAME = "javax.transaction.UserTransaction";

    /**
     * JNDI Global name for the TransactionSynchronizationRegistry.
     */
    private static final String TR_SYNC_REGISTRY_JNDI_NAME = "javax.transaction.TransactionSynchronizationRegistry";

    /**
     * Logger used for traces.
     */
    private static Logger logger = Log.getLogger(Log.JONAS_NAMING_PREFIX);

    /**
     * Base InitialContext.
     */
    private InitialContext ictx;

    /**
     * {@inheritDoc}
     */
    protected void init() throws NamingException {
        // Initial Context
        ictx = new InitialContext();

        // Get userTransaction instance
        try {
            userTransaction = (UserTransaction) ictx.lookup(USER_TRANSACTION_JNDI_NAME);
        } catch (NamingException ne) {
            logger.log(BasicLevel.WARN, "Cannot lookup " + USER_TRANSACTION_JNDI_NAME);
        }

        // Get TransactionSynchronizationRegistry instance
        try {
            tsr = (TransactionSynchronizationRegistry) ictx.lookup(TR_SYNC_REGISTRY_JNDI_NAME);
        } catch (NamingException ne) {
            logger.log(BasicLevel.WARN, "Cannot lookup " + TR_SYNC_REGISTRY_JNDI_NAME);
        }
    }

}
