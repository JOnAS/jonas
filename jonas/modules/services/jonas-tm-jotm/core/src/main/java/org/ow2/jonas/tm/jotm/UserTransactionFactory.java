/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.tm.jotm;

import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.Name;
import javax.naming.Reference;
import javax.naming.spi.ObjectFactory;

public class UserTransactionFactory implements ObjectFactory {

    public Object getObjectInstance(Object objref, Name name, Context ctx, Hashtable env) throws Exception {

        Reference ref = (Reference) objref;
        JOTMTransactionManager ut = null;

        if (ref.getClassName().equals("javax.transaction.UserTransaction")
            || ref.getClassName().equals("org.ow2.jonas.tm.jotm.JOTMTransactionManager")) {
            // create the UserTransaction object
            // No need to init a TMFactory in the client!
            ut = JOTMTransactionManager.getUnique();

            if (ut == null) {
                ut = new JOTMTransactionManager();

                // Get the timeout default value that was configured in the server
                String timeoutStr = (String) ref.get("jotm.timeout").getContent();
                Integer i = new Integer(timeoutStr);
                int timeout = i.intValue();
                ut.setDefaultTimeout(timeout);
            }
        }
       return ut;
    }
}
