/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.ws.cxf.wsdl;

import java.util.List;
import java.util.Map;

import javax.wsdl.Definition;
import javax.wsdl.extensions.schema.SchemaReference;
import javax.xml.namespace.QName;

import org.apache.cxf.frontend.WSDLGetUtils;
import org.apache.cxf.helpers.DOMUtils;
import org.apache.cxf.message.Message;
import org.ow2.jonas.ws.cxf.jaxws.CXFWSEndpoint;
import org.ow2.jonas.ws.cxf.jaxws.WebservicesContainer;
import org.ow2.jonas.ws.jaxws.PortIdentifier;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * A {@code WsdlUpdater} is responsible to update all the
 * {@literal soap:address[@location]} of the ports in the wsdl:service.
 *
 * @author Guillaume Sauthier
 */
public class WsdlUpdater extends WSDLGetUtils {

    /**
     * Group all the endpoints defined in a unique Wsdl.
     */
    private WebservicesContainer<? extends CXFWSEndpoint> container;

    public WsdlUpdater(WebservicesContainer<? extends CXFWSEndpoint> container) {
        this.container = container;
    }

    @Override
    protected void updateDoc(final Document doc,
                             final String base,
                             final Map<String, Definition> mp,
                             final Map<String, SchemaReference> smp,
                             final Message message) {

        // Let CXF do most of the work
        super.updateDoc(doc, base, mp, smp, message);

        // The processed service
        QName serviceQName = message.getExchange().getService().getName();
        
        // Iterates on wsdl:service
        // ---------------------------------------------------
        List<Element> serviceList = DOMUtils.findAllElementsByTagNameNS(doc.getDocumentElement(),
                        "http://schemas.xmlsoap.org/wsdl/",
                        "service");
        for (Element serviceEl : serviceList) {
            
            // Only select the right service
            String serviceName = serviceEl.getAttribute("name");
            if (serviceName.equals(serviceQName.getLocalPart())) {

                // Iterates on wsdl:port
                // ---------------------------------------------------
                List<Element> elementList = DOMUtils.findAllElementsByTagNameNS(doc.getDocumentElement(),
                                                                  "http://schemas.xmlsoap.org/wsdl/",
                                                                  "port");
                for (Element el : elementList) {
                    String name = el.getAttribute("name");

                    // Search the updated Url for the current port
                    String url = container.findUpdatedURL(new PortIdentifier(serviceQName, name));
                    if (url != null) {
                        rewriteAddress(url, el, "http://schemas.xmlsoap.org/wsdl/soap/");
                        rewriteAddress(url, el, "http://schemas.xmlsoap.org/wsdl/soap12/");
                    }
                }
            }
        }
    }
}
