/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.cxf.easybeans.ext;

import org.ow2.easybeans.api.bean.EasyBeansBean;
import org.ow2.easybeans.container.EmptyResourceInjector;
import org.ow2.jonas.ws.cxf.util.WebServiceContextInjectionHandler;
import org.ow2.util.annotation.processor.DefaultAnnotationProcessor;
import org.ow2.util.annotation.processor.IAnnotationProcessor;
import org.ow2.util.annotation.processor.ProcessorException;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;


/**
 * Inject JAX-WS 2.x Resource (WebServiceContext) into the EasyBeans EJB instance.
 * @author Guillaume Sauthier
 * @author Florent Benoit
 */
public class JAXWS20ResourceInjector extends EmptyResourceInjector {

    /**
     * Logger.
     */
    private static Log logger = LogFactory.getLog(JAXWS20ResourceInjector.class);

    /**
     * The processor.
     */
    private IAnnotationProcessor processor = null;

    public JAXWS20ResourceInjector() {
        processor = new DefaultAnnotationProcessor();
        processor.addAnnotationHandler(new WebServiceContextInjectionHandler());
    }

    /**
     * Called <b>before</b> EasyBeans resolution and injection of dependencies.
     * @param bean the Bean instance.
     */
    @Override
    public void preEasyBeansInject(final EasyBeansBean bean) {
        // Will inject only @Resource WebServiceContext
        try {
            processor.process(bean);
        } catch (ProcessorException e) {
            logger.info("Cannot inject WebServiceContext in bean {0}", bean, e);
        }

    }

}
