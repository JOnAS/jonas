/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.cxf.util;

import org.ow2.jonas.ws.cxf.jaxws.WebservicesContainer;
import org.ow2.jonas.ws.jaxws.IWebServiceEndpoint;
import org.ow2.jonas.ws.jaxws.PortIdentifier;

import javax.wsdl.Definition;
import javax.wsdl.Service;
import javax.wsdl.Port;
import javax.wsdl.extensions.soap12.SOAP12Address;
import javax.wsdl.extensions.soap.SOAPAddress;
import javax.xml.namespace.QName;
import java.util.List;

/**
 * The WSDLUtils class contains WSDL related tools.
 *
 * @author Guillaume Sauthier
 */
public class WSDLUtils {

    /**
     * Update the endpoints addresses of the given WSDL Definition.
     * @param definition WSDL document
     * @param container group of endpoints sharing the same WSDL definition
     */
    public static void updateEndpointAddresses(final Definition definition,
                                               final WebservicesContainer<? extends IWebServiceEndpoint> container) {

        // Update port info for each endpoint
        for (IWebServiceEndpoint endpoint : container.getEndpoints()) {

            PortIdentifier id = endpoint.getIdentifier();
            QName serviceName = id.getServiceName();
            String portName = id.getPortName();

            Service service = definition.getService(serviceName);
            if (service != null) {
                Port port = service.getPort(portName);

                if (port != null) {
                    String url = container.findUpdatedURL(id);
                    if (url != null) {
                        updateSoapAddressLocation(port, url);
                    }
                }
            }
        }

    }

    /**
     * Update the soap:address extensibility element contained in the given port.
     * @param port wsdl:port instance
     * @param url new value
     */
    private static void updateSoapAddressLocation(final Port port,
                                                  final String url) {
        List extensions = port.getExtensibilityElements();
        for (Object extension : extensions) {
            if (extension instanceof SOAP12Address) {
                ((SOAP12Address) extension).setLocationURI(url);
            } else if (extension instanceof SOAPAddress) {
                ((SOAPAddress) extension).setLocationURI(url);
            }
        }
    }


}
