/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.ws.cxf.wsdl;

import java.util.Map;

import org.apache.cxf.frontend.WSDLGetInterceptor;
import org.apache.cxf.message.Message;
import org.apache.cxf.service.model.EndpointInfo;
import org.ow2.jonas.ws.cxf.jaxws.CXFWSEndpoint;
import org.ow2.jonas.ws.cxf.jaxws.WebservicesContainer;
import org.w3c.dom.Document;

/**
 * A {@code ContainerWsdlInterceptor} is responsible to generate
 * (or update) the Wsdl of a given endpoint's group (in a wsdl:service).
 *
 * @author Guillaume Sauthier
 */
public class ContainerWsdlInterceptor extends WSDLGetInterceptor {

    /**
     * Update the Wsdl with appropriate endpoints Url.
     */
    private WsdlUpdater updater;

    public ContainerWsdlInterceptor(WebservicesContainer<? extends CXFWSEndpoint> container) {
        updater = new WsdlUpdater(container);
    }

    @Override
    public Document getDocument(Message message, String base, Map<String, String> params, String ctxUri, EndpointInfo endpointInfo) {
        return updater.getDocument(message, base, params, ctxUri, endpointInfo);
    }
}
