/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008-2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.cxf.http;

import java.io.IOException;

import org.apache.cxf.Bus;
import org.apache.cxf.service.model.EndpointInfo;
import org.apache.cxf.transport.Destination;
import org.apache.cxf.transport.DestinationFactory;
import org.apache.cxf.transport.http.HTTPTransportFactory;

/**
 * JOnAS specialized HTTP TransportFactory.
 * It's mainly used as a {@link DestinationFactory} for {@link JOnASDestination}.
 * @author Guillaume Sauthier
 * @author Florent Benoit
 */
public final class JOnASHTTPTransportFactory extends HTTPTransportFactory
                                                 implements DestinationFactory {

    /**
     * Build a new JOnAS transport factory for the given bus.
     * @param bus the CXF bus.
     */
    public JOnASHTTPTransportFactory(final Bus bus) {
        setBus(bus);
    }

    /**
     * @return a new {@link JOnASDestination} for the given {@link EndpointInfo}.
     * @param endpointInfo the given endpoint information
     * @throws IOException if the {@link JOnASDestination} is not created.
     */
    public Destination getDestination(final EndpointInfo endpointInfo) throws IOException {

        // Create the new JOnASDestination and configure it
        JOnASDestination destination = new JOnASDestination(getBus(), getRegistry(), endpointInfo);
        configure(destination);
        return destination;
    }

}
