/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.cxf.easybeans;

import org.apache.cxf.jaxws.JAXWSMethodInvoker;
import org.apache.cxf.interceptor.Fault;

/**
 * Invoker that will allow to call release() method on the pool after each call.
 * @author Florent Benoit
 */
public class EasyBeansMethodInvoker extends JAXWSMethodInvoker {

    /**
     * Keep EasyBeans invoker factory (in order to avoid a cast).
     */
    private EasyBeansInvokerFactory easyBeansInvokerFactory = null;

    /**
     * Delegate to the super constructor.
     * @param easyBeansInvokerFactory the given factory.
     */
    public EasyBeansMethodInvoker(EasyBeansInvokerFactory easyBeansInvokerFactory) {
        super(easyBeansInvokerFactory);
        this.easyBeansInvokerFactory = easyBeansInvokerFactory;
    }

    /**
     * Call after each message call. We need to release the bean instance in the pool.
     * @param exchange the given message exchange
     * @param o the object to release in the pool
     */
    public void releaseServiceObject(org.apache.cxf.message.Exchange exchange, java.lang.Object o) {
        super.releaseServiceObject(exchange, o);
        easyBeansInvokerFactory.release(exchange, o);
    }

    /**
     * Create an instance of bean at each call.
     * @param ex exchange object
     * @return a stateless session bean instance
     */
     public Object getServiceObject(org.apache.cxf.message.Exchange ex) {
        try {
            return easyBeansInvokerFactory.create(ex);
        } catch (Fault e) {
            throw e;
        } catch (Throwable e) {
            throw new Fault(e);
        }
    }

}
