/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.ws.cxf.client;

import javax.naming.Reference;

import org.ow2.jonas.ws.cxf.client.factory.CxfWebServiceRefObjectFactory;
import org.ow2.jonas.ws.jaxws.client.JAXWSWebServiceRefBuilder;

/**
 * A {@code CxfWebServiceRefBuilder} is ...
 *
 * @author Guillaume Sauthier
 */
public class CxfWebServiceRefBuilder extends JAXWSWebServiceRefBuilder {

    @Override
    protected Reference createReference(String classname) {
        return new Reference(classname, CxfWebServiceRefObjectFactory.class.getName(), null);
    }
}
