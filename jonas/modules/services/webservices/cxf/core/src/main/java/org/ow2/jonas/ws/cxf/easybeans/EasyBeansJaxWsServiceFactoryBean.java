/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.cxf.easybeans;

import org.apache.cxf.service.invoker.Invoker;
import org.apache.cxf.jaxws.JAXWSMethodInvoker;
import org.apache.cxf.jaxws.support.JaxWsServiceFactoryBean;
import org.ow2.easybeans.container.session.stateless.StatelessSessionFactory;
import org.ow2.jonas.ws.cxf.JOnASJaxWsImplementorInfo;

/**
 * Extended {@link JaxWsServiceFactoryBean} in order to use the EasyBeans Session factory pool
 * for managing the EJB instances.
 * @author Florent Benoit
 */
public class EasyBeansJaxWsServiceFactoryBean extends JaxWsServiceFactoryBean {

    /**
     * EJB Factory.
     */
    private StatelessSessionFactory factory;

    /**
     * Delagating constructor.
     * @param info the implementor info
     * @param factory EasyBeans EJB Factory
     */
    public EasyBeansJaxWsServiceFactoryBean(final JOnASJaxWsImplementorInfo info,
                                           final StatelessSessionFactory factory) {
        super(info);
        this.factory = factory;
    }

    /**
     * Set our own {@link Invoker}.
     * @see org.apache.cxf.jaxws.JaxWsServiceFactoryBean#createInvoker()
     */
    @Override
    protected Invoker createInvoker() {
        return new EasyBeansMethodInvoker(new EasyBeansInvokerFactory(factory));
    }
}
  
