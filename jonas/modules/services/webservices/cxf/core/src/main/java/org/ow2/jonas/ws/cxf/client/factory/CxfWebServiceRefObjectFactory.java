/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.ws.cxf.client.factory;

import java.util.Collections;

import javax.xml.ws.Service;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;

import org.apache.cxf.Bus;
import org.apache.cxf.BusFactory;
import org.ow2.jonas.ws.cxf.BusCreator;
import org.ow2.jonas.ws.jaxws.client.factory.JAXWSWebServiceRefObjectFactory;
import org.ow2.util.ee.builder.webserviceref.ReferenceHelper;

/**
 * A {@code CxfWebServiceRefObjectfactory} is ...
 *
 * @author Guillaume Sauthier
 */
public class CxfWebServiceRefObjectFactory extends JAXWSWebServiceRefObjectFactory {

    @Override
    protected void processObjectInstance(Service service, ReferenceHelper reference) throws Exception {
        // Let the parent do its job
        super.processObjectInstance(service, reference);

        Bus bus = BusFactory.getDefaultBus();

        // Manage Audit handler
        Handler audit = (Handler) bus.getProperty(BusCreator.class.getName() + ".CLIENT");

        HandlerResolver resolver = service.getHandlerResolver();
        HandlerResolver delegating = new HandlerResolverDelegate(resolver,
                                                                 Collections.singletonList(audit));

        service.setHandlerResolver(delegating);
    }
}
