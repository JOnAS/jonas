/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.cxf.easybeans;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.cxf.jaxws.EndpointImpl;
import org.ow2.easybeans.api.EZBContainer;
import org.ow2.easybeans.api.bean.info.IBeanInfo;
import org.ow2.jonas.ws.cxf.jaxws.CXFWSEndpoint;
import org.ow2.jonas.ws.cxf.jaxws.WebservicesContainer;
import org.ow2.jonas.ws.jaxws.PortMetaData;
import org.ow2.jonas.ws.jaxws.easybeans.EasyBeansContextNamingInfo;
import org.ow2.jonas.ws.jaxws.easybeans.EasyBeansSecurityConstraint;
import org.ow2.jonas.ws.jaxws.ejb.IEJBWebserviceEndpoint;
import org.ow2.jonas.ws.jaxws.ejb.ISecurityConstraint;
import org.ow2.jonas.ws.jaxws.ejb.context.IContextNamingInfo;

/**
 * The EJBCXFWebserviceEndpoint is the EJB based endpoint implementation on CXF.
 *
 * @author Guillaume Sauthier
 */
public class CXFEJBWebserviceEndpoint extends CXFWSEndpoint implements IEJBWebserviceEndpoint {

    /**
     * Structure holding data used to create a web context.
     */
    private IContextNamingInfo contextNamingInfo;

    /**
     * Security reference (may be null).
     */
    private ISecurityConstraint securityConstraint;

    /**
     * storage for values set during deployment.
     */
    private Map<String, Object> deploymentInfos;

    public CXFEJBWebserviceEndpoint(final EndpointImpl endpoint,
                                    final PortMetaData pmd,
                                    final WebservicesContainer wsContainer,
                                    final EZBContainer container,
                                    final IBeanInfo info) {
        super(endpoint, EndpointType.EJB, pmd, wsContainer);
        this.contextNamingInfo = new EasyBeansContextNamingInfo(this, container, info.getName());
        this.deploymentInfos = new HashMap<String, Object>();

        List<String> roles = info.getSecurityInfo().getDeclaredRoles();
        if (roles != null && !roles.isEmpty()) {
            // There are some roles declared here
            securityConstraint = new EasyBeansSecurityConstraint(pmd, info);
        }
    }

    /**
     * Get the data used to construct a web context.
     *
     * @return the Context naming info structure.
     */
    public IContextNamingInfo getContextNamingInfo() {
        return this.contextNamingInfo;
    }

    /**
     * Get the data structure used to secure a bean's endpoint.
     *
     * @return security constraint structure
     */
    public ISecurityConstraint getSecurityConstraint() {
        return securityConstraint;
    }

    /**
     * @return a map of values accessibles after web context deployment.
     */
    public Map<String, Object> getDeploymentInfos() {
        return deploymentInfos;
    }

}
