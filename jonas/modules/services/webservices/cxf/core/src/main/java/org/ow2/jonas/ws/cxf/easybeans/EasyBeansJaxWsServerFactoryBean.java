/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008-2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.cxf.easybeans;

import javax.naming.Context;
import javax.naming.NamingException;

import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxws.JaxWsServerFactoryBean;
import org.apache.cxf.jaxws.support.JaxWsServiceFactoryBean;
import org.apache.cxf.service.Service;
import org.apache.cxf.service.invoker.Invoker;
import org.ow2.easybeans.api.bean.info.IWebServiceInfo;
import org.ow2.easybeans.container.session.stateless.StatelessSessionFactory;
import org.ow2.jonas.ws.cxf.JOnASJaxWsServerFactoryBean;
import org.ow2.jonas.ws.cxf.jaxws.WebservicesContainer;
import org.ow2.jonas.ws.jaxws.handler.builder.DescriptorHandlerChainBuilder;
import org.ow2.util.ee.metadata.common.api.xml.struct.IHandlerChains;

/**
 * Extended {@link JaxWsServerFactoryBean} to inhibit the resource injection.
 * This is done because we (EasyBeans) want to control resource injection.
 * @author Guillaume Sauthier
 */
public class EasyBeansJaxWsServerFactoryBean extends JOnASJaxWsServerFactoryBean {

    /**
     * EJB Factory.
     */
    private StatelessSessionFactory factory;

    /**
     * Delagating constructor.
     * @param serviceFactory Factory for {@link Service} instances.
     * @param factory EasyBeans EJB Factory
     */
    public EasyBeansJaxWsServerFactoryBean(final JaxWsServiceFactoryBean serviceFactory,
                                           final StatelessSessionFactory factory,
                                           final WebservicesContainer<CXFEJBWebserviceEndpoint> container) {
        super(serviceFactory, container);
        this.factory = factory;
        // Register ClassLoader (for handlers resources injections)
        container.getBus().setExtension(factory.getContainer().getClassLoader(), ClassLoader.class);

        // Check if we have a handlerChain defined in descriptor
        IWebServiceInfo info = factory.getSessionBeanInfo().getWebServiceInfo();
        if (info != null && (info.getHandlerChains() != null)) {

            // OK, a HandlerChain have been specified, use it ...
            IHandlerChains handlerChains = info.getHandlerChains();
            setHandlerChainBuilder(new DescriptorHandlerChainBuilder(handlerChains));
        }

    }

    /**
     * Overrided with no-op because we want to control injection.
     * @param instance unused
     * @see org.apache.cxf.jaxws.JaxWsServerFactoryBean#injectResources(java.lang.Object)
     */
    @Override
    protected void injectResources(final Object instance) {
        // Do nothing (Don't worry, that's OK :) )
    }

    /**
     * Set our own {@link Invoker}.
     * @see org.apache.cxf.jaxws.JaxWsServerFactoryBean#createInvoker()
     */
    @Override
    protected Invoker createInvoker() {
        return new EasyBeansMethodInvoker(new EasyBeansInvokerFactory(factory));
    }

    /**
     * @return the real Bean class.
     * @see org.apache.cxf.frontend.ServerFactoryBean#getServiceBeanClass()
     */
    @Override
    public Class<?> getServiceBeanClass() {
        return this.factory.getBeanClass();
    }

    @Override
    public Server create() {
        try {
            compEnvContext = (Context) factory.getJavaContext().lookup("comp/env");
        } catch (NamingException e) {
            throw new IllegalStateException("Can't get java:comp/env context for bean " + factory.getBeanInfo().getName());
        }
        // Override classloader as SoapBindingUtil will use Thread Context classloader in order to create proxies.
        // We're setting Context classloader to CXF classloader
        // http://jira.ow2.org/browse/JONAS-369 
        ClassLoader old = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(JaxWsServiceFactoryBean.class.getClassLoader());
            return super.create();
        } finally {
            Thread.currentThread().setContextClassLoader(old);
        }
    }
}

