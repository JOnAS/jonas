/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.cxf.easybeans;

import org.apache.cxf.common.annotation.AnnotationProcessor;
import org.apache.cxf.common.injection.ResourceInjector;
import org.apache.cxf.resource.ResourceManager;


/**
 * Slightly change the original CXF {@link ResourceInjector} to avoid
 * the call to PostConstruct annotated methods.
 * @author Guillaume Sauthier
 */
public class CXFResourceInjector extends ResourceInjector {

    /**
     * Creates an new CXFResourceInjector.
     * @param rm CXF ResourceManager
     */
    public CXFResourceInjector(final ResourceManager rm) {
        super(rm);
    }

    /**
     * Do injection on the given object.
     * @param instance the given instance on which do the injection.
     */
    @Override
    public void inject(final Object instance) {
        AnnotationProcessor processor = new AnnotationProcessor(instance);
        processor.accept(this);
    }


}
