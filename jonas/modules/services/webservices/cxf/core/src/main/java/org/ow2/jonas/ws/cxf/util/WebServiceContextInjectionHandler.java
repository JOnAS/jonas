/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.cxf.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import javax.annotation.Resource;
import javax.xml.ws.WebServiceContext;

import org.apache.cxf.jaxws.context.WebServiceContextImpl;
import org.ow2.util.annotation.processor.ProcessorException;
import org.ow2.util.annotation.processor.handler.AbstractInjectionHandler;

/**
 * React to @Resource on WebServiceContext.
 */
public class WebServiceContextInjectionHandler extends AbstractInjectionHandler {

    /**
     * Supports @Resource annotation only
     * @param aClass
     * @return true if the annotation is a Resource
     */
    public boolean isSupported(final Class<? extends Annotation> aClass) {
        return Resource.class.equals(aClass);
    }

    @Override
    public void process(final Annotation annotation, final Field field, final Object o) throws ProcessorException {
        if (WebServiceContext.class.equals(field.getType())) {
            doInjectField(field, o, new WebServiceContextImpl());
        }
    }

    @Override
    public void process(final Annotation annotation, final Method method, final Object o) throws ProcessorException {
        if (method.getParameterTypes().length == 1) {
            if (WebServiceContext.class.equals(method.getParameterTypes()[0])) {
                doInjectMethod(method, o, new WebServiceContextImpl());
            }
        }
    }
}
