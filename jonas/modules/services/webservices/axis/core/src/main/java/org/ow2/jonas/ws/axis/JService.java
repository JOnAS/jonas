/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ws.axis;

import java.io.InputStream;
import java.net.URL;
import java.rmi.Remote;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;

import javax.xml.namespace.QName;
import javax.xml.rpc.Call;
import javax.xml.rpc.ServiceException;

import org.apache.axis.EngineConfiguration;
import org.apache.axis.client.Service;
import org.apache.axis.wsdl.gen.Parser;

/**
 * JService is the JOnAS J2EE layer on top of axis Service implementation. It is
 * currently a no-op class.
 * @author Guillaume Sauthier
 */
public class JService extends Service {

    /**
     * Serial version UID.
     */
    private static final long serialVersionUID = -6590290303291040871L;

    /**
     * Internal Map that store service-endpoint-interface names to wsdl:port
     * QName
     */
    private Map<String, QName> classname2wsdlPort = null;

    /**
     * Hastable to store call instances properties
     */
    private Hashtable<String, Properties> mapCallProperties = new Hashtable<String, Properties>();

    /**
     * Hastable to store stub instances properties
     */
    private Hashtable<String, Properties> mapStubProperties = new Hashtable<String, Properties>();

    /**
     * @see org.apache.axis.client.Service#Service()
     */
    public JService() {
        super();
    }

    /**
     * @see org.apache.axis.client.Service#Service(javax.xml.rpc.QName)
     */
    public JService(QName serviceName) {
        super(serviceName);
    }

    /**
     * @see org.apache.axis.client.Service#Service(org.apache.axis.EngineConfiguration)
     */
    public JService(EngineConfiguration config) {
        super(config);
    }

    /**
     * @see org.apache.axis.client.Service#Service(java.net.URL,
     *      javax.xml.rpc.QName)
     */
    public JService(URL wsdlDoc, QName serviceName) throws ServiceException {
        super(wsdlDoc, serviceName);
    }

    /**
     * @see org.apache.axis.client.Service#Service(org.apache.axis.wsdl.gen.Parser,
     *      javax.xml.rpc.QName)
     */
    public JService(Parser parser, QName serviceName) throws ServiceException {
        super(parser, serviceName);
    }

    /**
     * @see org.apache.axis.client.Service#Service(java.lang.String,
     *      javax.xml.rpc.QName)
     */
    public JService(String wsdlLocation, QName serviceName) throws ServiceException {
        super(wsdlLocation, serviceName);
    }

    /**
     * @see org.apache.axis.client.Service#Service(java.io.InputStream,
     *      javax.xml.rpc.QName)
     */
    public JService(InputStream wsdlInputStream, QName serviceName) throws ServiceException {
        super(wsdlInputStream, serviceName);
    }

    /**
     * @see javax.xml.rpc.Service#getPort(java.lang.Class)
     */
    public Remote getPort(Class proxyInterface) throws ServiceException {
        if (this.classname2wsdlPort != null) {
            QName portname = this.classname2wsdlPort.get(proxyInterface.getName());
            if (portname != null) {
                return getPort(portname, proxyInterface);
            } else {
                return super.getPort(proxyInterface);
            }
        }
        return super.getPort(proxyInterface);
    }

    /**
     * @see javax.xml.rpc.Service#createCall()
     */
    public Call createCall() throws ServiceException {

        return new JCall(this);
    }

    /**
     * Assign the classname 2 port Map.
     * @param map Map to be used
     */
    public void assignSEIClassnameToWSDLPort(Map<String, QName> map) {
        this.classname2wsdlPort = map;
    }

    /**
     * @param name port name
     * @param callProperties properties used to configure the Call instances
     */
    public void assignCallProperties(String name, Properties callProperties) {
        this.mapCallProperties.put(name, callProperties);
    }

    /**
     * @param name port name
     * @param stubProperties properties used to configure the Stub instances
     */
    public void assignStubProperties(String name, Properties stubProperties) {
        this.mapStubProperties.put(name, stubProperties);
    }

    /**
     * @param name port name
     * @return Returns the callProperties.
     */
    public Properties getCallProperties(String name) {
        Properties props = null;
        if (mapCallProperties.containsKey(name)) {
            props = (Properties) mapCallProperties.get(name);
        }
        return props;
    }

    /**
     * @param name port name
     * @return Returns the stubProperties.
     */
    public Properties getStubProperties(String name) {
        Properties props = null;
        if (mapStubProperties.containsKey(name)) {
            props = (Properties) mapStubProperties.get(name);
        }
        return props;
    }

}