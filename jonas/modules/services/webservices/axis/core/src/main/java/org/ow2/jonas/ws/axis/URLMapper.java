/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ws.axis;

import javax.servlet.http.HttpServletRequest;

import org.apache.axis.AxisFault;
import org.apache.axis.MessageContext;
import org.apache.axis.handlers.BasicHandler;
import org.apache.axis.transport.http.HTTPConstants;

/**
 * The <code>URLMapper</code> is given an HTTPServletRequest and
 * knows from the invoked URL which service should be used.
 * Old style URL :
 * http://<host>:<port>/<context>/<url-mapping>/<name>
 * pathInfo = /<name>
 * servletPath = /<url-mapping>
 *
 * New Style URL:
 * http://<host>:<port>/<context>/<name>
 * pathInfo = empty
 * servletPath = /<name>
 *
 * @author Guillaume Sauthier
 */
public class URLMapper extends BasicHandler {

    private static final long serialVersionUID = 5261537788705414094L;

    /**
     * @see org.apache.axis.Handler#invoke(org.apache.axis.MessageContext)
     */
    public void invoke(MessageContext mc) throws AxisFault {
        /**
         * If there's already a targetService then just return.
         */
        if (mc.getService() == null) {
            String pathInfo = (String) mc.getProperty(HTTPConstants.MC_HTTP_SERVLETPATHINFO);
            HttpServletRequest req = (HttpServletRequest) mc.getProperty(HTTPConstants.MC_HTTP_SERVLETREQUEST);
            String servletPath = req.getServletPath();
            String path = null;

            if ((pathInfo == null) || (pathInfo.length() == 0)) {
                // use the servletPath as target service
                // new mode
                if (servletPath.startsWith("/")) {
                    path = servletPath.substring(1); //chop the extra "/"
                }
            } else {
                // directly take the pathinfo value
                // compatibility mode
                if (pathInfo.startsWith("/")) {
                    path = pathInfo.substring(1); //chop the extra "/"
                }
            }
            mc.setTargetService(path);
        }
    }
}
