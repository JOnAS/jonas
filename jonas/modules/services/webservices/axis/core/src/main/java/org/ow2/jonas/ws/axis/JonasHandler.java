/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ws.axis;

import java.util.Iterator;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;

import org.ow2.jonas.lib.ejb21.EJBInvocation;
import org.ow2.jonas.lib.ejb21.JServiceEndpointHome;
import org.ow2.jonas.lib.ejb21.JStatelessFactory;
import org.ow2.jonas.lib.util.BeanNaming;
import org.ow2.jonas.lib.util.I18n;
import org.ow2.jonas.lib.util.Log;
import org.ow2.jonas.security.ws.SecurityContextHelper;

import org.apache.axis.AxisFault;
import org.apache.axis.Handler;
import org.apache.axis.MessageContext;
import org.apache.axis.handlers.BasicHandler;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * This handler is called before all application specific handlers It allows to
 * process some jonas specific stuff like setting the bean environment for
 * example.
 *
 * @author Philippe Durieux
 */
public class JonasHandler extends BasicHandler {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = -8901070778405459776L;

    /**
     * MC property : service.endpoint.home
     */
    private static final String SE_HOME = "service.endpoint.home";

    /**
     * MC property : service.endpoint.context
     */
    private static final String SE_CONTEXT = "service.endpoint.context";

    /**
     * logger
     */
    private static Logger logger = Log.getLogger("org.ow2.jonas.lib.ejb21.endpoint");

    /**
     * i18n instance
     */
    private static I18n i18n = I18n.getInstance(JonasHandler.class);

    /**
     * cached initial context
     */
    private static InitialContext cachedContext;

    /**
     * Thread local storage used for locating the active message context. This
     * information is only valid for the lifetime of this request.
     */
    private static ThreadLocal<MessageContext> currentMessageContext = new ThreadLocal<MessageContext>();

    /**
     * Set the active message context.
     *
     * @param mc the new active message context.
     */
    protected static void setCurrentMessageContext(MessageContext mc) {
        currentMessageContext.set(mc);
    }

    /**
     * Get the active message context.
     *
     * @return the current active message context
     */
    public static MessageContext getCurrentMessageContext() {
        return currentMessageContext.get();
    }

    /**
     * Called on reply if a fault occured.
     *
     * @param msgContext MessageContext
     */
    public void onFault(MessageContext msgContext) {
        logger.log(BasicLevel.ERROR, "*------* Fault");

        // save previous context
        MessageContext previousContext = getCurrentMessageContext();

        try {
            // set active context
            setCurrentMessageContext(msgContext);

            JServiceEndpointHome sehome = getJServiceEndpointHome(msgContext);
            Context savedCtx = getSavedContext(msgContext);
            if (sehome != null) {
                logger.log(BasicLevel.DEBUG, "*--* Fault");
                sehome.resetCompCtx(savedCtx);
                if (savedCtx != null) {
                    String msg = i18n.getMessage("JonasHandler.onFault.settingNonNullContext", savedCtx);
                    logger.log(BasicLevel.ERROR, msg);
                }
                unsetJServiceEndpointHome(msgContext);
                unsetSavedContext(msgContext);
            }
        } finally {
            // restore previous state
            setCurrentMessageContext(previousContext);
        }
    }

    /**
     * @param msgContext the MC which store the context
     */
    private void unsetSavedContext(MessageContext msgContext) {
        msgContext.removeProperty(SE_CONTEXT);
    }

    /**
     * @param msgContext the MC which store the home
     */
    private void unsetJServiceEndpointHome(MessageContext msgContext) {
        msgContext.removeProperty(SE_HOME);
    }

    /**
     * @param msgContext the MC to explore
     * @return Returns the Context stored inside msgContext
     */
    private Context getSavedContext(MessageContext msgContext) {
        return (Context) msgContext.getProperty(SE_CONTEXT);
    }

    /**
     * @param msgContext the MC to explore
     * @return Returns the JServiceEndpointHome stored inside msgContext
     */
    private JServiceEndpointHome getJServiceEndpointHome(MessageContext msgContext) {
        return (JServiceEndpointHome) msgContext.getProperty(SE_HOME);
    }

    /**
     * Called on request and then on reply if no fault occured.
     *
     * @param msgContext
     *            MessageContext
     * @throws AxisFault
     *             if the handler cannot get the service endpoint home
     */
    public void invoke(MessageContext msgContext) throws AxisFault {

        // save previous context
        MessageContext previousContext = getCurrentMessageContext();

        try {
            // set active context
            setCurrentMessageContext(msgContext);

            if (msgContext.getPastPivot()) {
                // Response
                // ---------------------------------------------------
                logger.log(BasicLevel.DEBUG, "*------* Response");
                JServiceEndpointHome sehome = getJServiceEndpointHome(msgContext);
                Context savedCtx = getSavedContext(msgContext);
                if (sehome != null) {
                    logger.log(BasicLevel.DEBUG, "*--* Response");
                    sehome.resetCompCtx(savedCtx);
                    unsetJServiceEndpointHome(msgContext);
                }
                // ----------------------------------------------------
                // /Response
            } else {
                // Request
                // ---------------------------------------------------
                logger.log(BasicLevel.DEBUG, "*------* Request");

                // Get the service class name
                Handler service = msgContext.getService();
                String clsName = (String) service
                        .getOption(JOnASEJBProvider.OPTION_SEINTERFACENAME);
                if (clsName == null) {
                    // We are not in the case of jonas ejb call: do nothing.
                    return;
                }
                logger.log(BasicLevel.DEBUG, "*--* Request");

                // Get ServiceEndpointHome in JNDI
                String jndiName = (String) service.getOption(JOnASEJBProvider.OPTION_SEJNDINAME);
                if (jndiName == null) {
                    String msg = i18n.getMessage("JonasHandler.invoke.noSEJNDI", JOnASEJBProvider.OPTION_SEJNDINAME);
                    logger.log(BasicLevel.ERROR, msg);
                    throw new AxisFault(msg);
                }
                JServiceEndpointHome sehome = null;
                try {
                    InitialContext ic = getCachedContext();
                    sehome = (JServiceEndpointHome) ic.lookup(jndiName);
                    setJServiceEndpointHome(msgContext, sehome);
                } catch (NamingException ne) {
                    String msg = i18n.getMessage("JonasHandler.invoke.cannotLookupHome", jndiName);
                    logger.log(BasicLevel.ERROR, msg, ne);
                    throw new AxisFault(msg, ne);
                }

                // add the security context
                String username = msgContext.getUsername();
                if (username != null) {
                    // Do not forget to initialize the security context
                    SecurityContextHelper.getInstance().login(username,
                            msgContext.getPassword());
                    // Check the security and throw exception if user is not
                    // authorized
                    // to access the EJB before lauching handlers
                    checkSecurity(sehome, msgContext);
                }

                // Switch Context on the context of the target bean
                // (java:comp/env)
                Context savedCtx = sehome.setCompCtx();
                setSavedContext(msgContext, savedCtx);
                if (savedCtx != null) {
                    String msg = i18n.getMessage("JonasHandler.invoke.savingNonNullCtx", savedCtx);
                    logger.log(BasicLevel.WARN, msg);
                }
                // ---------------------------------------------------
                // /Request
            }
        } finally {
            // restore previous state
            setCurrentMessageContext(previousContext);
        }
    }

    private void checkSecurity(JServiceEndpointHome sehome, MessageContext msgContext) {
        JStatelessFactory bf = sehome.getBeanFactory();
        EJBInvocation ejb = new EJBInvocation();
        QName  q = null;
        try {
            SOAPEnvelope env = msgContext.getMessage().getSOAPPart().getEnvelope();
            SOAPBody body = env.getBody();
            Iterator it = body.getChildElements();
            SOAPElement operation = (SOAPElement) it.next();

            q = new QName(operation.getNamespaceURI(), operation.getLocalName());
            ejb.methodPermissionSignature = BeanNaming.getSignature(bf.getEJBName(), msgContext.getOperationByQName(q).getMethod());
        } catch (AxisFault e ) {
           // error during getting the operation from the message
           // not possible to check the security
                logger.log(BasicLevel.WARN, "can't retreive the operation from message...can not check the security");
           return;
        } catch (SOAPException e) {
            // error during getting the operation from the message
            // not possible to check the security
                 logger.log(BasicLevel.WARN, "can't retreive the operation from message...can not check the security");
            return;
        }
        bf.checkSecurity(ejb);
    }

    /**
     * @param msgContext the MC which will store the Context
     * @param savedCtx Context to store
     */
    private void setSavedContext(MessageContext msgContext, Context savedCtx) {
        msgContext.setProperty(SE_CONTEXT, savedCtx);
    }

    /**
     * @param msgContext the MC which will store the EndpointHome
     * @param sehome JServiceEndpointHome to be stored
     */
    private void setJServiceEndpointHome(MessageContext msgContext, JServiceEndpointHome sehome) {
        msgContext.setProperty(SE_HOME, sehome);
    }

    /**
     * @return Returns the cached InitialContext (or created a new one)
     * @throws javax.naming.NamingException
     *             when InitialContext creation fails
     */
    private static InitialContext getCachedContext()
            throws javax.naming.NamingException {
        if (cachedContext == null) {
            cachedContext = new InitialContext();
        }
        return cachedContext;
    }
}
