/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ws.axis;

import org.apache.axis.EngineConfiguration;
import org.apache.axis.Handler;
import org.apache.axis.deployment.wsdd.WSDDProvider;
import org.apache.axis.deployment.wsdd.WSDDService;

/**
 * JOnAS EJB Provider.
 *
 * @author Guillaume Sauthier
 */
public class WSDDJOnASEJBProvider extends WSDDProvider {

    /**
     * JOnAS EJB Provider name
     */
    public static final String PROVIDER_NAME = "JOnASEJB";

    /**
     * @return Returns the JOnAS EJB Provider name
     */
    public String getName() {
        return PROVIDER_NAME;
    }

    /**
     * Returns the JOnAS EJB Provider.
     *
     * @param service not used
     * @param registry not used
     *
     * @return Returns the JOnAS EJB Provider.
     *
     * @throws Exception never thrown
     */
    public Handler newProviderInstance(WSDDService service, EngineConfiguration registry) throws Exception {
        return new org.ow2.jonas.ws.axis.JOnASEJBProvider();
    }

}

