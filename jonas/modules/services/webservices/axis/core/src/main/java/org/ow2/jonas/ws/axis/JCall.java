/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 */
package org.ow2.jonas.ws.axis;

import java.util.Enumeration;
import java.util.Properties;

import javax.xml.namespace.QName;
import javax.xml.rpc.Stub;

import org.apache.axis.client.Call;

/**
 * JCall is the JOnAS J2EE layer on top of axis Call implementation.
 * It is used to assign Call properties.
 * @author Jerome Camilleri
 */
public class JCall extends Call {

    /**
     * @see org.apache.axis.client.Call#Call(Service)
     */
    public JCall(JService service) {
        super(service);
    }

    /**
     * Sets the port name of this Call object.  This call will not set
     * any additional fields, nor will it do any checking to verify that
     * this port name is actually defined in the WSDL - for now anyway.
     *
     * @param portName Fully qualified name of the port
     */
    public void setPortName(QName portName) {
        super.setPortName(portName);
        JService service = (JService) this.getService();
        Properties propCall = service.getCallProperties(portName.getLocalPart());
        if (propCall != null) {
            for (Enumeration<?> e = propCall.propertyNames(); e.hasMoreElements();) {
                String name = (String) e.nextElement();
                Object value = propCall.getProperty(name);
                if (Call.SESSION_MAINTAIN_PROPERTY.equals(name)) {
                    value = Boolean.valueOf((String) value);
                }
                this.setProperty(name, value);

            }
        }
        Properties propStub = service.getStubProperties(portName.getLocalPart());
        if (propStub != null) {
            for (Enumeration<?> e = propStub.propertyNames(); e.hasMoreElements();) {
                String name = (String) e.nextElement();
                if (!Stub.ENDPOINT_ADDRESS_PROPERTY.equals(name)) {
                    Object value = propStub.getProperty(name);
                    if (Call.SESSION_MAINTAIN_PROPERTY.equals(name)) {
                        value = Boolean.valueOf((String) value);
                    }
                    this.setProperty(name, value);
                }
            }

        }
    }
}
