/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ws.axis2.easybeans;

import javax.xml.ws.Provider;

import org.apache.axis2.jaxws.core.MessageContext;
import org.apache.axis2.jaxws.server.EndpointController;
import org.apache.axis2.jaxws.server.dispatcher.EndpointDispatcher;
import org.apache.axis2.jaxws.server.dispatcher.ProviderDispatcher;
import org.ow2.easybeans.api.bean.EasyBeansSLSB;
import org.ow2.easybeans.container.session.stateless.StatelessSessionFactory;
import org.ow2.util.pool.api.Pool;

/**
 * Rewrite EndpointController to use our own dispather class
 * @author youchao
 * @author xiaoda
 */
public class Axis2EndpointController extends EndpointController {

    /**
     * The corresponding ejb's factory which can get instance
     */
    private StatelessSessionFactory factory = null;

    /**
     * Constructor
     * @param factory The corresponding ejb's factory which can get instance
     */
    public Axis2EndpointController(final StatelessSessionFactory factory) {
        this.factory = factory;
    }

    /**
     * @see org.apache.axis2.jaxws.server.EndpointController#getEndpointDispatcher(java.lang.Class, java.lang.Object)
     * Use our own dispathers
     */
    @Override
    protected EndpointDispatcher getEndpointDispatcher(final Class serviceImplClass, final Object serviceInstance) throws Exception {
        if (Provider.class.isAssignableFrom(serviceImplClass)) {
            return new ProviderDispatcher(serviceImplClass, serviceInstance);
        } else {
            //Get ejb pool instance
            Pool<EasyBeansSLSB, Long> pool = factory.getPool();
            EasybeansAxis2Invoker easybeansAxis2Invoker = new EasybeansAxis2Invoker(pool);
            //use our own dispather class
            return new Axis2ServiceDispatcher(serviceImplClass, easybeansAxis2Invoker);
        }
    }
}
