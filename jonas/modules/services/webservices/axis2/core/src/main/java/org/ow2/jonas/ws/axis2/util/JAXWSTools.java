
/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2010 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ws.axis2.util;

import java.io.File;
import java.util.ArrayList;

import org.ow2.jonas.Version;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

public class JAXWSTools {

    private static final Log logger = LogFactory.getLog(JAXWSTools.class);

    private final static String TOOLS = "tools.jar";


    public JAXWSTools() {
    }

    public static File[] getClasspath() throws Exception {

        String osName = System.getProperty("os.name", "");
        ArrayList<File> jars = new ArrayList<File>();

        addWsGenJarsLocation(jars);

        // add tools.jar to classpath except on Mac OS. On Mac OS there is classes.jar with the
        // same contents as tools.jar and is automatically included in the classpath.
        if(osName.indexOf("mac") < 0 ) {
            addToolsJarLocation(jars);
        }

        return jars.toArray(new File[jars.size()]);

    }
    private static void addWsGenJarsLocation(final ArrayList<File> jars) {
        String jonasHomePath = System.getProperty("jonas.root");
        //4 kinds of jars
        //first is from axis2 bundle //lib/sun-wsgen
        //second is javaeeapi.jar
        //third is ow2-bundles-externals-jaxb2.jar
        //fourth is lib/jaxb-xjc.jar

        String version = Version.getNumber();

        //axis2 Bundle .jar
        String axis2BundlePath = jonasHomePath + "/repositories/maven2-internal/org/ow2/jonas/jonas-webservices-axis2/" + version + "/jonas-webservices-axis2-" + version + "-ipojo.jar";

        if(new File(axis2BundlePath).exists()) {
            jars.add(new File(axis2BundlePath));
        } 

        //javaEE-API.jar

        String javaEEAPIPath = jonasHomePath + "/repositories/maven2-internal/org/ow2/jonas/osgi/javaee-api/" + version + "/javaee-api-" + version + ".jar";

        if(new File(javaEEAPIPath).exists()) {
            jars.add(new File(javaEEAPIPath));
        }

        //ow2-bundles-externals-jaxb2.jar
        File ow2JAXBDir = new File(jonasHomePath + "/repositories/maven2-internal/org/ow2/bundles/ow2-bundles-externals-jaxb2");
        File jaxbDir = ow2JAXBDir.listFiles()[0];
        String ow2JAXBVersion = null;
        if(jaxbDir.exists()) {
            ow2JAXBVersion = jaxbDir.getName();
        }

        String ow2JAXBPath = jonasHomePath + "/repositories/maven2-internal/org/ow2/bundles/ow2-bundles-externals-jaxb2/" + ow2JAXBVersion + "/ow2-bundles-externals-jaxb2-" + ow2JAXBVersion + ".jar";
        if(new File(ow2JAXBPath).exists()) {
            jars.add(new File(ow2JAXBPath));
        }

        //jaxb-xjc.jar
        String jaxbxjcPath = jonasHomePath + "/lib/jaxb-xjc.jar";
        if(new File(jaxbxjcPath).exists()) {
            jars.add(new File(jaxbxjcPath));
        }

    }

    private static void addToolsJarLocation(final ArrayList<File> jars) {
        //create a new File then check exists()
        String jreHomePath = System.getProperty("java.home");
        String javaHomePath = "";
        int jreHomePathLength = jreHomePath.length();
        if (jreHomePathLength > 0) {
            int i = jreHomePath.substring(0, jreHomePathLength -1).lastIndexOf(java.io.File.separator);
            javaHomePath = jreHomePath.substring(0, i);
        }
        File jdkhomelib = new File(javaHomePath, "lib");
        if (!jdkhomelib.exists()) {
            logger.warn("Missing " + jdkhomelib.getAbsolutePath()
                    + ". This may be required for wsgen to run. ");
        }
        else {
            File tools = new File(jdkhomelib, TOOLS);
            if (!tools.exists()) {
                logger.warn("Missing tools.jar in" + jdkhomelib.getAbsolutePath()
                        + ". This may be required for wsgen to run. ");
            } else {
                jars.add(tools.getAbsoluteFile());
            }
        }
    }
}
