/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ws.axis2.jaxws;

import org.ow2.jonas.ws.jaxws.IWebServiceEndpoint;
import org.ow2.jonas.ws.jaxws.base.JAXWSWebservicesContainer;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

public class WebservicesContainer<T extends IWebServiceEndpoint> extends JAXWSWebservicesContainer<T> {
    /**
     * Logger.
     */
    private static Log logger = LogFactory.getLog(WebservicesContainer.class);

    public WebservicesContainer(final String name) {
        super(name);
    }

    /**
     * Starts the container.
     * Recursively starts inner endpoints.
     */
    @Override
    public void start() {
        super.start();
        logger.debug("Start WebservicesContainer[{0}]", getName());
    }

    /**
     * Stops the container.
     * Recursively stops inner endpoints.
     */
    @Override
    public void stop() {

        super.stop();

        logger.debug("Stop WebservicesContainer[{0}]", getName());
    }
}
