/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ws.axis2.http;

import org.apache.axis2.transport.OutTransportInfo;
import org.apache.axis2.transport.http.HTTPConstants;
import org.ow2.jonas.ws.jaxws.IWSResponse;

/**
 * Wrap IWSResponse to fit axis2's OutTransportInfo
 * @author youchao
 */
public class Axis2TransportInfo implements OutTransportInfo {
    private IWSResponse response;

    public Axis2TransportInfo(final IWSResponse response) {
        this.response = response;
    }

    public void setContentType(final String contentType) {
        response.setHeader(HTTPConstants.HEADER_CONTENT_TYPE, contentType);
    }
}
