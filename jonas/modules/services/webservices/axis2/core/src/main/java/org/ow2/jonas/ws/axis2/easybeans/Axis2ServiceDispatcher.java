/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ws.axis2.easybeans;

import java.lang.reflect.Method;

import org.apache.axis2.jaxws.core.MessageContext;
import org.apache.axis2.jaxws.server.dispatcher.JavaBeanDispatcher;

/**
 * Dispatchers that get instace from ejb's pool
 * @author youchao
 */
public class Axis2ServiceDispatcher extends JavaBeanDispatcher {

    private EasybeansAxis2Invoker easybeansAxis2Invoker = null;

    public Axis2ServiceDispatcher(final Class implClass, final EasybeansAxis2Invoker easybeansAxis2Invoker) {
        super(implClass, null);
        this.easybeansAxis2Invoker = easybeansAxis2Invoker;
    }

    /* (non-Javadoc)
     * @see org.apache.axis2.jaxws.server.dispatcher.JavaBeanDispatcher#invokeService(org.apache.axis2.jaxws.core.MessageContext, java.lang.reflect.Method, java.lang.Object, java.lang.Object[])
     */
    //@Override
    protected Object invokeService(final MessageContext ctx, final Method method, final Object obj, final Object args[]) throws Exception {
        Object instance = null;
        try {
            //Get the ejb instance from pool
            instance = easybeansAxis2Invoker.getServiceObject();
            //invoke it
            return method.invoke(instance, args);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //return the ejb instance to pool
            easybeansAxis2Invoker.releaseServiceObject(instance);
        }
        return null;
    }
}
