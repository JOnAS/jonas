/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ws.axis2.jaxws.description.impl;

import javax.xml.namespace.QName;
import javax.xml.ws.handler.PortInfo;

import org.apache.axis2.jaxws.ExceptionFactory;
import org.apache.axis2.jaxws.i18n.Messages;

public class PortInfoImpl implements PortInfo {
    private QName serviceName = null;
    private QName portName = null;
    private String bindingId = null;

    /**
     * @param serviceName
     * @param portName
     * @param bindingId
     */
    PortInfoImpl(final QName serviceName, final QName portName, final String bindingId) {
        super();
        if (serviceName == null) {
            throw ExceptionFactory
                    .makeWebServiceException(Messages.getMessage("portInfoErr0", "<null>"));
        }
        if (portName == null) {
            throw ExceptionFactory
                    .makeWebServiceException(Messages.getMessage("portInfoErr1", "<null>"));
        }
        if (bindingId == null) {
            throw ExceptionFactory
                    .makeWebServiceException(Messages.getMessage("portInfoErr2", "<null>"));
        }
        this.serviceName = serviceName;
        this.portName = portName;
        this.bindingId = bindingId;
    }

    public QName getServiceName() {
        return serviceName;
    }

    public QName getPortName() {
        return portName;
    }

    public String getBindingID() {
        return bindingId;
    }
}
