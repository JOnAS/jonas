/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ws.axis2.easybeans;

import org.apache.axis2.jaxws.server.endpoint.lifecycle.EndpointLifecycleException;
import org.apache.axis2.jaxws.server.endpoint.lifecycle.EndpointLifecycleManager;
import org.apache.axis2.jaxws.server.endpoint.lifecycle.factory.EndpointLifecycleManagerFactory;

/**
 * Substitue the EndpointLifecycleManagerFactory to create
 * Axis2EndpointLifecycleManager
 * @author youchao
 */
public class Axis2EndpointLifecycleManagerFactory implements EndpointLifecycleManagerFactory {

    private EndpointLifecycleManager lifecycleManager;

    public Axis2EndpointLifecycleManagerFactory() {
        this.lifecycleManager = new Axis2EndpointLifecycleManager();
    }

    /* (non-Javadoc)
     * @see org.apache.axis2.jaxws.server.endpoint.lifecycle.factory.EndpointLifecycleManagerFactory#createEndpointLifecycleManager(java.lang.Object)
     */
    public EndpointLifecycleManager createEndpointLifecycleManager(final Object endpointInstance) throws EndpointLifecycleException {
        throw new UnsupportedOperationException();
    }

    /* (non-Javadoc)
     * @see org.apache.axis2.jaxws.server.endpoint.lifecycle.factory.EndpointLifecycleManagerFactory#createEndpointLifecycleManager()
     */
    public EndpointLifecycleManager createEndpointLifecycleManager() {
        return this.lifecycleManager;
    }

}
