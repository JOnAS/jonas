/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ws.axis2.easybeans;

import org.apache.axis2.jaxws.core.MessageContext;
import org.apache.axis2.jaxws.injection.ResourceInjectionException;
import org.apache.axis2.jaxws.lifecycle.LifecycleException;
import org.apache.axis2.jaxws.server.endpoint.lifecycle.EndpointLifecycleException;
import org.ow2.jonas.ws.jaxws.IWebServiceEndpoint;

/**
 * Axis2EndpointLifecycleManager is try to make axis2's serverinstance is null
 * for EJB, and then use our own instance
 * @author youchao
 */
public class Axis2EndpointLifecycleManager extends org.apache.axis2.jaxws.server.endpoint.lifecycle.impl.EndpointLifecycleManagerImpl {

    /**
     * This method is called on each web service call. Make axis2's
     * serverinstance is null for EJB, and then use our own instance
     * @throws ResourceInjectionException
     * @throws LifecycleException
     */
    @Override
    public Object createServiceInstance(final MessageContext context, final Class serviceClass) throws LifecycleException, ResourceInjectionException {
        // TODO:just for ejb to return null now, for pojo do super is ok.
        if(context.getProperty(org.ow2.jonas.ws.axis2.jaxws.Axis2WSEndpoint.END_POINT_TYPE) == IWebServiceEndpoint.EndpointType.EJB) {
            return null;
        }
        return super.createServiceInstance(context, serviceClass);
    }

    @Override
    public void invokePostConstruct() throws EndpointLifecycleException {
    }

    @Override
    public void invokePreDestroy() throws EndpointLifecycleException {
    }

}
