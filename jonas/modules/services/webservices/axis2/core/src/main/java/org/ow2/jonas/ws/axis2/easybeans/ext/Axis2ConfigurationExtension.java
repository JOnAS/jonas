/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ws.axis2.easybeans.ext;

import org.ow2.easybeans.api.EZBContainerConfig;
import org.ow2.easybeans.api.EZBConfigurationExtension;
import org.ow2.jonas.ws.jaxws.ejb.IWebDeployer;

/**
 * Called by Easybeans Runtime to provide a Hook for Axis2.
 * @author youchao
 * @author Guillaume Sauthier
 * @author Florent Benoit
 */
public class Axis2ConfigurationExtension implements EZBConfigurationExtension {

    /**
     * Inner callback.
     */
    private LifeCycleCallback lifeCycleCallback;

    public Axis2ConfigurationExtension() {
        // Init the callback before configuration of the EZBContainerConfig
        lifeCycleCallback = new LifeCycleCallback();
    }

    /**
     * Adapt the given configuration.
     * @param easybeansContainerConfig JContainerConfig instance.
     */
    public void configure(final EZBContainerConfig easybeansContainerConfig) {

        // TODO: add injections
        // create the ResourceInjector to add
        JAXWS20ResourceInjector resourceInjector = new JAXWS20ResourceInjector();

        // add them in the Configuration
        easybeansContainerConfig.addCallback(lifeCycleCallback);
        easybeansContainerConfig.addInjectors(resourceInjector);
    }

    /**
     * Bind an {@link IWebDeployer} that will be used to
     * deploy web services enabled EjbJars
     * @param deployer deployer manager
     */
    public void bindWebDeployer(final IWebDeployer deployer) {
        lifeCycleCallback.setWebDeployer(deployer);
    }

}
