/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ws.axis2.http;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.concurrent.CountDownLatch;

import org.apache.axis2.AxisFault;
import org.apache.axis2.context.MessageContext;
import org.apache.axis2.transport.RequestResponseTransport;
import org.ow2.jonas.ws.jaxws.IWSResponse;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

public class Axis2RequestResponseTransport implements RequestResponseTransport {

    private static final Log LOG = LogFactory.getLog(Axis2RequestResponseTransport.class);

    private IWSResponse response;

    private CountDownLatch responseReadySignal = new CountDownLatch(1);

    private RequestResponseTransportStatus status = RequestResponseTransportStatus.INITIAL;

    private AxisFault faultToBeThrownOut = null;

    private boolean responseWritten;

    public Axis2RequestResponseTransport(final IWSResponse response) {
        this.response = response;
    }

    public void acknowledgeMessage(final MessageContext msgContext) throws AxisFault {
        LOG.debug("acknowledgeMessage");
        LOG.debug("Acking one-way request");

        response.setContentType("text/xml; charset=" + msgContext.getProperty("message.character-set-encoding"));

        response.setStatus(HttpURLConnection.HTTP_ACCEPTED);
        try {
            response.flushBuffer();
        } catch (IOException e) {
            throw new AxisFault("Error sending acknowledgement", e);
        }

        signalResponseReady();
    }

    public void awaitResponse() throws InterruptedException, AxisFault {
        LOG.debug("Blocking servlet thread -- awaiting response");
        status = RequestResponseTransportStatus.WAITING;
        responseReadySignal.await();
        if (faultToBeThrownOut != null) {
            throw faultToBeThrownOut;
        }
    }

    public void signalFaultReady(final AxisFault fault) {
        faultToBeThrownOut = fault;
        signalResponseReady();
    }

    public void signalResponseReady() {
        LOG.debug("Signalling response available");
        status = RequestResponseTransportStatus.SIGNALLED;
        responseReadySignal.countDown();
    }

    public RequestResponseTransportStatus getStatus() {
        return status;
    }

    public boolean isResponseWritten() {
        return responseWritten;
    }

    public void setResponseWritten(final boolean responseWritten) {
        this.responseWritten = responseWritten;
    }
}
