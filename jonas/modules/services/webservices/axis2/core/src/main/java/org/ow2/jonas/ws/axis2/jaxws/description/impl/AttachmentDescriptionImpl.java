/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ws.axis2.jaxws.description.impl;

import org.apache.axis2.jaxws.description.AttachmentType;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class AttachmentDescriptionImpl implements
        org.apache.axis2.jaxws.description.AttachmentDescription {

    private static final Log log = LogFactory.getLog(AttachmentDescriptionImpl.class);

    private AttachmentType attachmentType;
    private String[] mimeTypes;

    /**
     * @param attachmentType
     * @param mimeTypes
     */
    public AttachmentDescriptionImpl(final AttachmentType attachmentType, final String[] mimeTypes) {
        this.attachmentType = attachmentType;
        this.mimeTypes = mimeTypes;
        if (log.isDebugEnabled()) {
            String debugString = toString();
            log.debug("Created AttachmentDescriptionImpl");
            log.debug(debugString);
        }
    }

    public AttachmentType getAttachmentType() {
        return attachmentType;
    }

    public String[] getMimeTypes() {
        return mimeTypes;
    }

    @Override
    public String toString() {
        final String newline = "\n";
        final String sameline = "; ";
        StringBuffer string = new StringBuffer();

        string.append(super.toString());
        string.append(newline);
        string.append("  Attachment Type: " + getAttachmentType());
        //
        string.append(newline);
        string.append("  Mime Types: " + getMimeTypes());
        return string.toString();
    }

}
