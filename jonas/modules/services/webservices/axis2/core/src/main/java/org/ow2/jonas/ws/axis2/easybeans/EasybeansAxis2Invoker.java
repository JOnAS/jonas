/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ws.axis2.easybeans;

import org.apache.axis2.AxisFault;
import org.ow2.easybeans.api.bean.EasyBeansSLSB;
import org.ow2.util.pool.api.Pool;
import org.ow2.util.pool.api.PoolException;

/**
 * EasyBeans invoker that will take an instance from the pool, call the method
 * and then put it back in the pool.
 * @author youchao
 */
public class EasybeansAxis2Invoker {

    private Pool<EasyBeansSLSB, Long> pool = null;

    /**
     * Build a new invoker for the given stateless' pool.
     * @param pool the stateless' pool of the factory
     */
    public EasybeansAxis2Invoker(final Pool<EasyBeansSLSB, Long> pool) {
        this.pool = pool;
    }

    /**
     * Creates and returns a service object
     */
    public Object getServiceObject() {
        try {
            return pool.get();
        } catch (PoolException e) {
            try {
                throw AxisFault.makeFault(e);
            } catch (AxisFault e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }
        return null;

    }

    /**
     * Called when the invoker is done with the object. Replace the bean in
     * pool.
     * @param context CXF Exchange
     * @param obj object to be released
     */
    public void releaseServiceObject(final Object obj) {
        try {
            pool.release((EasyBeansSLSB) obj);
        } catch (PoolException e) {
            try {
                throw AxisFault.makeFault(e);
            } catch (AxisFault e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }
    }

}
