/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.ws.jaxws.http.servlet.test;

import org.ow2.easybeans.api.EZBContainer;
import org.ow2.easybeans.api.FactoryException;
import org.ow2.easybeans.api.bean.EasyBeansSLSB;
import org.ow2.easybeans.container.session.stateless.StatelessSessionFactory;
import org.ow2.util.pool.api.Pool;

public class StatelessSessionFactoryForTest extends StatelessSessionFactory{

    public StatelessSessionFactoryForTest(final String className, final EZBContainer container) throws FactoryException {
        super(className, container);
        // TODO Auto-generated constructor stub
    }
    @Override
    public Pool<EasyBeansSLSB, Long> getPool() {

        System.out.println("Calling customized pool*********************");
        return null;
    }

}
