/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.jaxws.http.servlet.test;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.ws.jaxws.IWSResponse;

public class ServletResponseAdapterForTest  implements IWSResponse,HttpServletResponse {

    private int contentLength;

    private String contentType;

    private String host;

    private ServletOutputStreamForTest out;

    private int method;

    private Map parameters;

    private String path;

    private URL uri;

    private int port;

    private Map headers;

    private int statusCode;

    private String statusMessage;


    public ServletResponseAdapterForTest(final String contentType, final String host, final String path, final URL uri,
            final int port, final ServletOutputStreamForTest out) {
        this.contentType = contentType;
        this.host = host;
        this.parameters = new HashMap();
        this.path = path;
        this.uri = uri;
        this.port = port;
        this.headers = new HashMap();
        this.out = out;
    }



    public ServletOutputStream getOutputStream() throws IOException {
        // TODO Auto-generated method stub
        return out;
    }



    public void setHeader(final String name, final String value) {
        // TODO Auto-generated method stub
        this.headers.put(name, value);
    }


    public void setStatus(final int code) {
        // TODO Auto-generated method stub

    }


    public void addCookie(final Cookie arg0) {
        // TODO Auto-generated method stub

    }


    public void addDateHeader(final String arg0, final long arg1) {
        // TODO Auto-generated method stub

    }


    public void addHeader(final String arg0, final String arg1) {
        // TODO Auto-generated method stub
        this.headers.put(arg0, arg1);
    }


    public void addIntHeader(final String arg0, final int arg1) {
        // TODO Auto-generated method stub

    }


    public boolean containsHeader(final String arg0) {
        // TODO Auto-generated method stub
        return headers.containsKey(arg0);
    }


    public String encodeRedirectURL(final String arg0) {
        // TODO Auto-generated method stub
        return null;
    }


    public String encodeRedirectUrl(final String arg0) {
        // TODO Auto-generated method stub
        return null;
    }


    public String encodeURL(final String arg0) {
        // TODO Auto-generated method stub
        return null;
    }


    public String encodeUrl(final String arg0) {
        // TODO Auto-generated method stub
        return null;
    }


    public void sendError(final int arg0) throws IOException {
        // TODO Auto-generated method stub

    }


    public void sendError(final int arg0, final String arg1) throws IOException {
        // TODO Auto-generated method stub

    }


    public void sendRedirect(final String arg0) throws IOException {
        // TODO Auto-generated method stub

    }


    public void setDateHeader(final String arg0, final long arg1) {
        // TODO Auto-generated method stub

    }


    public void setIntHeader(final String arg0, final int arg1) {
        // TODO Auto-generated method stub

    }


    public void setStatus(final int arg0, final String arg1) {
        // TODO Auto-generated method stub

    }

    public void flushBuffer() throws IOException {
        // TODO Auto-generated method stub

    }



    public void setContentType(final String type) {
        // TODO Auto-generated method stub

    }



    public int getBufferSize() {
        // TODO Auto-generated method stub
        return 0;
    }



    public String getCharacterEncoding() {
        // TODO Auto-generated method stub
        return null;
    }



    public String getContentType() {
        // TODO Auto-generated method stub
        return null;
    }



    public Locale getLocale() {
        // TODO Auto-generated method stub
        return null;
    }



    public PrintWriter getWriter() throws IOException {
        // TODO Auto-generated method stub
        return null;
    }



    public boolean isCommitted() {
        // TODO Auto-generated method stub
        return false;
    }



    public void reset() {
        // TODO Auto-generated method stub

    }



    public void resetBuffer() {
        // TODO Auto-generated method stub

    }



    public void setBufferSize(final int arg0) {
        // TODO Auto-generated method stub

    }



    public void setCharacterEncoding(final String arg0) {
        // TODO Auto-generated method stub

    }



    public void setContentLength(final int arg0) {
        // TODO Auto-generated method stub

    }



    public void setLocale(final Locale arg0) {
        // TODO Auto-generated method stub

    }
}
