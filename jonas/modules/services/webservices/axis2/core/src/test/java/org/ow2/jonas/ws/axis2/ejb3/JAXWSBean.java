/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.axis2.ejb3;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPFault;
import javax.xml.ws.soap.SOAPFaultException;

@WebService(name="jaxwsbean",
            serviceName="jaxwsbean",
            wsdlLocation="META-INF/wsdl/Jaxwsbean.wsdl")

@Stateless(mappedName="JAXWSBean")
@Remote(JAXWSGreeter.class)
@HandlerChain(file="META-INF/handlers.xml", name = "Handler")
@SOAPBinding(style=SOAPBinding.Style.RPC,
             use=SOAPBinding.Use.LITERAL,
             parameterStyle=SOAPBinding.ParameterStyle.WRAPPED
)
public class JAXWSBean implements JAXWSGreeter {

    @WebMethod(exclude=false)
    public String greetMe(final String me) {
        System.out.println("i'm a ejb ws: " + me);
        if (!"foo bar".equals(me)) {
            throw new RuntimeException("Wrong parameter");
        }
        return "Hello " + me;
    }

    public void greetMeFault(final String me) {
        System.out.println("generate SOAP fault");
        SOAPFault fault = null;
        try {
            fault = SOAPFactory.newInstance().createFault();
            fault.setFaultCode(new QName("http://foo", "MyFaultCode"));
            fault.setFaultString("my error");
            fault.setFaultActor("my actor");
        } catch (SOAPException ex) {
            throw new RuntimeException(ex);
        }

        throw new SOAPFaultException(fault);
    }

}
