/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011-2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.ws.base.audit;

import javax.xml.namespace.QName;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.ow2.util.auditreport.api.ICurrentInvocationID;
import org.ow2.util.auditreport.impl.AuditIDImpl;
import org.ow2.util.auditreport.impl.CurrentInvocationID;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * A {@code JaxwsClientAuditHandlerTestCase} is ...
 *
 * @author Guillaume Sauthier
 */
public class JaxwsEndpointAuditHandlerTestCase {

    private ICurrentInvocationID current;

    @BeforeMethod
    public void setUpAuditIdInThreadLocal() throws Exception {
        current = CurrentInvocationID.getInstance();
        // Erase any audit ID in place
        current.setAuditID(new AuditIDImpl("parent/0:local/0"));
    }

    @Test
    public void testReportGeneration() throws Exception {
        SOAPMessageContext context = mock(SOAPMessageContext.class);
        QName description = new QName("uri:test", "description");
        QName service = new QName("uri:test", "service");
        QName port = new QName("uri:test", "port");
        QName operation = new QName("uri:test", "operation");
        
        // First call: inbound, then outbound
        when(context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY)).thenReturn(Boolean.FALSE, Boolean.TRUE);
        when(context.get(MessageContext.WSDL_DESCRIPTION)).thenReturn(description);
        when(context.get(MessageContext.WSDL_SERVICE)).thenReturn(service);
        when(context.get(MessageContext.WSDL_PORT)).thenReturn(port);
        when(context.get(MessageContext.WSDL_OPERATION)).thenReturn(operation);

    }

}
