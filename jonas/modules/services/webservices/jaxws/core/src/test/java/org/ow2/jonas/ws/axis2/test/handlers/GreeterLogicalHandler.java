/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 */

package org.ow2.jonas.ws.axis2.test.handlers;


import javax.ejb.EJB;
import javax.xml.ws.handler.LogicalMessageContext;
import javax.xml.ws.handler.MessageContext;

import org.ow2.jonas.ws.axis2.pojo.HelloService;
import org.ow2.jonas.ws.axis2.pojo.word.Word;


public class GreeterLogicalHandler implements javax.xml.ws.handler.LogicalHandler<LogicalMessageContext> {

    //@Resource(name = "greeting")
    //private String greeting;

   @EJB(name="org.ow2.jonas.ws.axis2.test.ejb3.Hello")
   HelloService hello;

    public void close(final MessageContext arg0) {
        // TODO Auto-generated method stub
        System.out.println("CLOSE in LogicalHandler");
    }

    public boolean handleFault(final LogicalMessageContext arg0) {
        // TODO Auto-generated method stub
        return true;
    }

    public boolean handleMessage(final LogicalMessageContext arg0) {

        // TODO Auto-generated method stub
        System.out.println("GREATERLogicalHandler HandleMessage");
        //System.out.println(greeting);
        Word word = new Word();
        word.setId(0);
        word.setName("JOnAS");
        hello.greetMe(word);

        return true;
    }
}


