/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 */

package org.ow2.jonas.ws.jaxws.test;

import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.PortInfo;

import org.ow2.jonas.ws.axis2.pojo.HelloService;
import org.ow2.jonas.ws.jaxws.handler.JOnASHandlerResolver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class JOnASHandlerResolverTest {

    @BeforeClass
    public void setUp() {
    }

    @AfterClass
    public void tearDown() {
    }

    @Test(groups = { "Handler" })
    public void testBasic() {
        JOnASHandlerResolver resolver = null;
        try {
            resolver = new JOnASHandlerResolver(HelloService.class,getClass().getClassLoader());
        } catch (Throwable t) {
            t.printStackTrace();
        }
        List<Handler> handlers = null;

        handlers = resolver.getHandlerChain(new TestPortInfo(null, null, null));
        System.out.println(handlers.size());

        Assert.assertEquals(handlers.size(), 2);





        /*
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        InputStream in = cl.getResourceAsStream("WEB-INF/handlers.xml");

        System.out.println(in == null);
        HandlerChainsType handlerChains = DescriptionUtils.loadHandlerChains(in);
        Assert.assertTrue(in != null);

        Assert.assertEquals(handlerChains.getHandlerChain().size(), 2);

        System.out.println("*********************Start get HandlerChain 1 *********************");

        System.out.println(getClass()!=null);
        System.out.println(getClass().getClassLoader()!=null);

        Class clazz = getClass();
        ClassLoader loader = clazz.getClassLoader();
        JOnASHandlerResolver resolver = new JOnASHandlerResolver(getClass(),getClass().getClassLoader());

        List<Handler> handlers = resolver.getHandlerChain(new TestPortInfo(null, null, null));

        Assert.assertEquals(2, handlers.size());
        */
    }

    private static class TestPortInfo implements PortInfo {

        private String bindingID;
        private QName portName;
        private QName serviceName;

        public TestPortInfo(final String bindingID, final QName portName, final QName serviceName) {
            this.bindingID = bindingID;
            this.portName = portName;
            this.serviceName = serviceName;
        }

        public String getBindingID() {
            return this.bindingID;
        }

        public QName getPortName() {
            return this.portName;
        }

        public QName getServiceName() {
            return this.serviceName;
        }

    }
}
