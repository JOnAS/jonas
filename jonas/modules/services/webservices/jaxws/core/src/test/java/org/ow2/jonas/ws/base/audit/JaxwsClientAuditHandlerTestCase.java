/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.ws.base.audit;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.ow2.jonas.ws.jaxws.base.audit.JaxwsClientAuditHandler;
import org.ow2.util.auditreport.api.IAuditID;
import org.ow2.util.auditreport.api.ICurrentInvocationID;
import org.ow2.util.auditreport.impl.AuditIDImpl;
import org.ow2.util.auditreport.impl.CurrentInvocationID;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * A {@code JaxwsClientAuditHandlerTestCase} is ...
 *
 * @author Guillaume Sauthier
 */
public class JaxwsClientAuditHandlerTestCase {

    private ICurrentInvocationID current;

    @BeforeMethod
    public void setUpAuditIdInThreadLocal() throws Exception {
        current = CurrentInvocationID.getInstance();
        // Erase any audit ID in place
        current.setAuditID(new AuditIDImpl("parent/0:local/0"));
    }

    @Test
    public void testInvocationIdIsPropagatedIntoHttpHeaders() throws Exception {
        Map<String, List<String>> headers = new HashMap<String, List<String>>();
        SOAPMessageContext context = mock(SOAPMessageContext.class);

        when(context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY)).thenReturn(Boolean.TRUE);
        when(context.get(MessageContext.HTTP_REQUEST_HEADERS)).thenReturn(headers);

        JaxwsClientAuditHandler handler = new JaxwsClientAuditHandler(current);
        handler.handleMessage(context);

        String header = headers.get("Invocation-ID").get(0);
        assertThat(header, notNullValue());

        String[] parts = header.split(":");
        assertThat(parts[0], is("local/1"));
    }

    @Test
    public void testNoChangesForInboundMessage() throws Exception {
        SOAPMessageContext context = mock(SOAPMessageContext.class);
        IAuditID before = current.getAuditID();

        when(context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY)).thenReturn(Boolean.FALSE);

        JaxwsClientAuditHandler handler = new JaxwsClientAuditHandler(current);
        handler.handleMessage(context);

        assertThat(current.getAuditID(), is( before ));
    }

    @Test
    public void testInvocationIdIsIncremented() throws Exception {
        Map<String, List<String>> headers = new HashMap<String, List<String>>();
        SOAPMessageContext context = mock(SOAPMessageContext.class);

        when(context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY)).thenReturn(Boolean.TRUE);
        when(context.get(MessageContext.HTTP_REQUEST_HEADERS)).thenReturn(headers);

        JaxwsClientAuditHandler handler = new JaxwsClientAuditHandler(current);

        // First call
        handler.handleMessage(context);

        String header = headers.get("Invocation-ID").get(0);
        assertThat(header, notNullValue());

        String[] parts = header.split(":");
        assertThat(parts[0], is( "local/1" ));

        // Second call
        handler.handleMessage(context);

        String header2 = headers.get("Invocation-ID").get(0);
        assertThat(header2, notNullValue());

        String[] parts2 = header2.split(":");
        assertThat(parts2[0], is( "local/2" ));
    }
}
