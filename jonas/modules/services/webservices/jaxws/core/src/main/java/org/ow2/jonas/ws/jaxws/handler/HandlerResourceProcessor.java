/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.ws.jaxws.handler;


import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

import javax.annotation.Resource;

import org.ow2.util.annotation.processor.ProcessorException;
import org.ow2.util.annotation.processor.handler.AbstractInjectionHandler;

/**
 * The HandlerResourceProcessor is responsible of injecting init params of the handler.
 *
 * @author Guillaume Sauthier
 */
public class HandlerResourceProcessor extends AbstractInjectionHandler {

    private Map<String, String> params;

    public HandlerResourceProcessor(final Map<String, String> params) {
        this.params = params;
    }

    public boolean isSupported(final Class<? extends Annotation> aClass) {
        return Resource.class.equals(aClass);
    }

    @Override
    public void process(final Annotation annotation, final Method method, final Object o) throws ProcessorException {

        // Find name
        Resource resource = (Resource) annotation;
        String name = resource.name();
        if ((name == null) || ("".equals(name))) {
            // If @Resource does not provides a name, find it from the Method's name
            name = getAttributeName(method);
        }

        // Get the value
        String value = params.get(name);

        // Convert to the expected type
        if (method.getParameterTypes().length == 1) {
            Class<?> type = method.getParameterTypes()[0];
            Object injectableValue = convertValue(value, type);

            // Inject it
            doInjectMethod(method, o, injectableValue);
        }
    }

    private Object convertValue(final String value, final Class<?> type) {

        // String, int, double, char, long, float, byte, short, boolean
        Object ret = null;
        if (String.class.isAssignableFrom(type)) {
            ret = value;
        } else if (Integer.class.isAssignableFrom(type)
                || Integer.TYPE.isAssignableFrom(type)) {
            ret = Integer.valueOf(value);
        } else if (Double.class.isAssignableFrom(type)
                || Double.TYPE.isAssignableFrom(type)) {
            ret = Double.valueOf(value);
        } else if (Character.class.isAssignableFrom(type)
                || Character.TYPE.isAssignableFrom(type)) {
            ret = value.charAt(0);
        } else if (Long.class.isAssignableFrom(type)
                || Long.TYPE.isAssignableFrom(type)) {
            ret = Long.valueOf(value);
        } else if (Float.class.isAssignableFrom(type)
                || Float.TYPE.isAssignableFrom(type)) {
            ret = Float.valueOf(value);
        } else if (Byte.class.isAssignableFrom(type)
                || Byte.TYPE.isAssignableFrom(type)) {
            ret = Byte.valueOf(value);
        } else if (Short.class.isAssignableFrom(type)
                || Short.TYPE.isAssignableFrom(type)) {
            ret = Short.valueOf(value);
        } else if (Boolean.class.isAssignableFrom(type)
                || Boolean.TYPE.isAssignableFrom(type)) {
            ret = Boolean.valueOf(value);
        }

        return ret;

    }

    @Override
    public void process(final Annotation annotation, final Field field, final Object o) throws ProcessorException {

        // Find name
        Resource resource = (Resource) annotation;
        String name = resource.name();
        if ((name == null) || ("".equals(name))) {
            // If @Resource does not provides a name, find it from the Field's name
            name = field.getName();
        }

        // Get the value
        String value = params.get(name);

        // Convert to the expected type
        Class<?> type = field.getType();
        Object injectableValue = convertValue(value, type);

        // Inject it
        doInjectField(field, o, injectableValue);
    }
}

