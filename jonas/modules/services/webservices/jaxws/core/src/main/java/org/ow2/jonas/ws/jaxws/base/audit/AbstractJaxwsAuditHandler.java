/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.ws.jaxws.base.audit;

import java.util.Set;

import javax.management.MBeanException;
import javax.management.Notification;
import javax.management.modelmbean.ModelMBeanNotificationBroadcaster;
import javax.xml.namespace.QName;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

/**
 * A {@code AbstractJaxwsAuditHandler} is ...
 *
 * @author Guillaume Sauthier
 */
public abstract class AbstractJaxwsAuditHandler implements SOAPHandler<SOAPMessageContext> {
    /**
     * Notification sender.
     */
    private ModelMBeanNotificationBroadcaster broadcaster = null;

    public void setBroadcaster(ModelMBeanNotificationBroadcaster broadcaster) {
        this.broadcaster = broadcaster;
    }

    public void sendNotification(Notification notification) throws MBeanException {
        broadcaster.sendNotification(notification);
    }

    public boolean isOutbound(MessageContext context) {
        return (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
    }

    public boolean handleFault(SOAPMessageContext context) {
        return true;
    }

    public void close(MessageContext context) {

    }

    public Set<QName> getHeaders() {
        return null;
    }

}
