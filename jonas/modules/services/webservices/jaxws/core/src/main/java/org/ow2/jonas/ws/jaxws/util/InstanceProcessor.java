/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 *  $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.jaxws.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.naming.Context;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.xml.ws.WebServiceContext;

import org.ow2.util.annotation.processor.ProcessorException;
import org.ow2.util.annotation.processor.handler.AbstractJNDIInjectionHandler;

/**
 * This class manage injection of {@link Resource} and {@link EJB} annotations into a WS POJO instance
 *
 * @author Loic Albertin
 */
public class InstanceProcessor extends AbstractJNDIInjectionHandler {
    public InstanceProcessor(Context envContext) {
        super(envContext);
    }

    public boolean isSupported(Class<? extends Annotation> aClass) {
        return Resource.class.equals(aClass) || EJB.class.equals(aClass) || PersistenceUnit.class.equals(aClass) ||
                PersistenceContext.class.equals(aClass);
    }

    @Override
    public void process(Annotation annotation, Field field, Object target) throws ProcessorException {
        if (WebServiceContext.class.equals(field.getType())) {
            // Lets underlying implementation inject the right WebServiceContext
            return;
        }
        String name = getNameFromAnnotation(annotation);
        if ((name == null) || ("".equals(name))) {
            name = this.getStandardBindingName(field);
        }
        Object value = this.doContextLookup(name);
        this.doInjectField(field, target, value);
    }

    @Override
    public void process(Annotation annotation, Method method, Object target) throws ProcessorException {
        if (method.getParameterTypes().length == 1) {
            if (WebServiceContext.class.equals(method.getParameterTypes()[0])) {
                // Lets underlying implementation inject the right WebServiceContext
                return;
            }
        }
        String name = getNameFromAnnotation(annotation);
        if ((name == null) || ("".equals(name))) {
            name = this.getStandardBindingName(method);
        }
        Object value = this.doContextLookup(name);
        this.doInjectMethod(method, target, value);
    }

    private String getNameFromAnnotation(Annotation annotation) {
        String name = null;
        if (annotation instanceof Resource) {
            name = ((Resource) annotation).name();
        } else if (annotation instanceof EJB) {
            name = ((EJB) annotation).name();
        } else if (annotation instanceof PersistenceUnit) {
            name = ((PersistenceUnit) annotation).name();
        } else if (annotation instanceof PersistenceContext) {
            name = ((PersistenceContext) annotation).name();
        }
        return name;
    }
}
