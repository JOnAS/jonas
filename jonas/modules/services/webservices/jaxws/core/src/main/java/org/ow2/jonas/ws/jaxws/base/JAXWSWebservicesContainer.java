/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.jaxws.base;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.ow2.jonas.ws.jaxws.IWebServiceEndpoint;
import org.ow2.jonas.ws.jaxws.PortIdentifier;
import org.ow2.jonas.ws.jaxws.IWebservicesContainer;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

public abstract class JAXWSWebservicesContainer<T extends IWebServiceEndpoint> implements IWebservicesContainer<T> {

    /**
     * Logger.
     */
    private static final Log logger = LogFactory.getLog(JAXWSWebservicesContainer.class);

    /**
     * This represents the WSDL Location [META-INF/wsdl/definition.wsdl].
     */
    private String name;

    /**
     * The list of endpoints sharing the WSDL.
     */
    private Map<PortIdentifier, T> endpoints;

    /**
     * Where do the user wants to publish the WSDL ?
     */
    private String wsdlPublicationDirectory;

    /**
     * Construct the group.
     * @param name Shared WSDL Location
     */
    public JAXWSWebservicesContainer(final String name) {
        this.name = name;
        endpoints = new HashMap<PortIdentifier, T>();
    }

    /**
     * Add a new endpoint in the group.
     * @param endpoint newly added endpoint.
     */
    public void addEndpoint(final T endpoint) {
        endpoints.put(endpoint.getIdentifier(), endpoint);
    }

    /**
     * Remove an endpoint from the group.
     * @param endpoint removed endpoint.
     */
    public void removeEndpoint(final T endpoint) {
        endpoints.remove(endpoint.getIdentifier());
    }

    /**
     * Given a service/port pair, try to find a matching endpoint URL.
     * @param identifier Port identifier
     * @return an endpoint URL or null if no match was found.
     */
    public String findUpdatedURL(final PortIdentifier identifier) {

        IWebServiceEndpoint matching = endpoints.get(identifier);
        if (matching != null) {
            return matching.getPortMetaData().getEndpointURL();
        }
        return null;
    }

    /**
     * @return the name of this container (WSDL location)
     */
    public String getName() {
        return name;
    }

    /**
     * @return All the inner endpoints.
     */
    public Collection<T> getEndpoints() {
        return endpoints.values();
    }

    /**
     * Starts the container.
     * Recursively starts inner endpoints.
     */
    public void start() {

        logger.debug("Start WebservicesContainer[{0}]", name);
        for (IWebServiceEndpoint endpoint : endpoints.values()) {
            endpoint.start();
        }
    }

    /**
     * Stops the container.
     * Recursively stops inner endpoints.
     */
    public void stop() {

        logger.debug("Stop WebservicesContainer[{0}]", name);
        for (IWebServiceEndpoint endpoint : endpoints.values()) {
            endpoint.stop();
        }
    }

    /**
     * Set the WSDL publication directory for this WSDL.
     * @param wsdlPublicationDirectory directory
     */
    public void setWsdlPublicationDirectory(final String wsdlPublicationDirectory) {
        this.wsdlPublicationDirectory = wsdlPublicationDirectory;
    }

    /**
     * @return the WSDL publication directory for this WSDL (may be null).
     */
    public String getWsdlPublicationDirectory() {
        return wsdlPublicationDirectory;
    }

}
