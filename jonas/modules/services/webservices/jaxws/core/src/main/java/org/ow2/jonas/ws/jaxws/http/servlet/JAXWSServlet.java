/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.jaxws.http.servlet;

import org.ow2.jonas.ws.jaxws.IWSRequest;
import org.ow2.jonas.ws.jaxws.IWSResponse;
import org.ow2.jonas.ws.jaxws.IWebServiceEndpoint;
import org.ow2.jonas.ws.jaxws.WSException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * JAX-WS 2.x Servlet. It wraps an {@link IWebServiceEndpoint} instance that will be invoked.
 * @author Guillaume Sauthier
 */
public class JAXWSServlet extends HttpServlet {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = 2241539363010941679L;

    /**
     * The wrapped web service endpoint.
     */
    private IWebServiceEndpoint endpoint;

    /**
     * Set the endpoint.
     * @param endpoint wrapped endpoint
     */
    public void setWebServiceEndpoint(final IWebServiceEndpoint endpoint) {
        this.endpoint = endpoint;
    }

    /**
     * Called by the server (via the service method) to allow a servlet to
     * handle a POST request.
     * @param request an HttpServletRequest object that contains the request the
     *        client has made of the servlet
     * @param response an HttpServletResponse object that contains the response
     *        the servlet sends to the client
     * @throws ServletException if the request for the POST could not be handled
     */
    @Override
    protected void doPost(final HttpServletRequest request,
                          final HttpServletResponse response) throws ServletException {
        invokeDestination(request, response);
    }

    /**
     * Called by the server (via the service method) to allow a servlet to
     * handle a GET request.
     * @param request an HttpServletRequest object that contains the request the
     *        client has made of the servlet
     * @param response an HttpServletResponse object that contains the response
     *        the servlet sends to the client
     * @throws ServletException if the request for the GET could not be handled
     */
    @Override
    protected void doGet(final HttpServletRequest request,
                         final HttpServletResponse response) throws ServletException {
        invokeDestination(request, response);
    }

    /**
     * Invoke the Destination object by analyzing the given request.
     * @param request the given request
     * @param response an HttpServletResponse object that contains the response
     *        the servlet sends to the client
     * @throws ServletException if the invocation fails
     */
    protected void invokeDestination(final HttpServletRequest request,
                                     final HttpServletResponse response)
            throws ServletException {

        // Init Request + Response
        IWSRequest req = new ServletRequestAdapter(request);
        req.setAttribute(HttpServletRequest.class, request);
        req.setAttribute(HttpServletResponse.class, response);
        req.setAttribute(ServletContext.class, getServletContext());

        IWSResponse res = new ServletResponseAdapter(response);

        try {
            endpoint.invoke(req, res);
        } catch (WSException e) {
            throw new ServletException(e);
        }

    }
}
