/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009-2011-2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.jaxws.base;

import javax.management.ObjectName;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.ow2.jonas.jmx.JmxService;
import org.ow2.jonas.lib.service.AbsServiceImpl;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.service.ServiceException;
import org.ow2.jonas.ws.jaxws.IJAXWSService;
import org.ow2.jonas.ws.jaxws.base.audit.JaxwsClientAuditHandler;
import org.ow2.jonas.ws.jaxws.base.audit.JaxwsEndpointAuditHandler;
import org.ow2.jonas.ws.jaxws.base.audit.mbean.AuditHandlerAdapter;
import org.ow2.util.auditreport.impl.CurrentInvocationID;
import org.ow2.util.event.api.IEventService;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

/**
 * Common abstract class for JAX-WS service implementations.
 * @author Francois Fornaciari
 */
public abstract class JAXWSService extends AbsServiceImpl implements IJAXWSService, JAXWSServiceMBean {

    /**
     * Logger.
     */
    private static Log logger = LogFactory.getLog(JAXWSService.class);

    private static final String OBJECTNAME_BASE = ":type=Auditors,auditorType=";

    /**
     * JMX Service reference.
     */
    private JmxService jmxService = null;
    private JaxwsClientAuditHandler clientHandler;
    private JaxwsEndpointAuditHandler serverHandler;

    /**
     * eventService to send events
     */
    private IEventService eventService = null;

    /**
     * Starts the JAX-WS service.
     * 
     * @throws ServiceException
     * If the service startup fails
     */
    @Override
    protected void doStart() throws ServiceException {
        // Register JAXWSService MBean
        try {
            jmxService.registerMBean(this, JonasObjectName.jaxwsService(getDomainName()));
        } catch (Exception e) {
            logger.warn("Cannot register MBean for JAX-WS service", e);
        }

        // Load MBean descriptors for Handlers
        jmxService.loadDescriptors(AuditHandlerAdapter.class.getPackage().getName(),AuditHandlerAdapter.class.getClassLoader());
    }

    /**
     * Stops the JAX-WS service.
     * @throws ServiceException
     * If the service stop fails
     */
    @Override
    protected void doStop() throws ServiceException {
        if (jmxService != null) {
            // Unregister JAXWSService MBean
            try {
                jmxService.unregisterMBean(JonasObjectName.jaxwsService(getDomainName()));
            } catch (Exception e) {
                logger.debug("Cannot unregister MBean for JAX-WS service", e);
            }
        }
    }

    /**
     * @param jmxService
     * the jmxService to set
     */
    public void setJmxService(final JmxService jmxService) {
        this.jmxService = jmxService;
    }

    /**
     * @return the jmxService
     */
    protected JmxService getJmxService() {
        return this.jmxService;
    }

    /**
     * @param eventService
     * the eventService to set
     */
    public void setEventService(IEventService eventService) {
        this.eventService = eventService;
    }

    public SOAPHandler<SOAPMessageContext> getClientAuditHandler() {
        if (clientHandler == null) {
            clientHandler = new JaxwsClientAuditHandler(CurrentInvocationID.getInstance());
            try {
                ObjectName name = new ObjectName(jmxService.getDomainName() + OBJECTNAME_BASE + "wsclient");
                jmxService.registerModelMBean(clientHandler, name);
            } catch (Exception e) {
                logger.debug("Cannot register MBean for JAX-WS Client Audit Handler", e);
            }
        }
        return clientHandler;
    }

    public SOAPHandler<SOAPMessageContext> getEndpointAuditHandler() {
        if (serverHandler == null) {
            serverHandler = new JaxwsEndpointAuditHandler(CurrentInvocationID.getInstance(),eventService);
            try {
                ObjectName name = new ObjectName(jmxService.getDomainName() + OBJECTNAME_BASE+ "wsendpoint");
                jmxService.registerModelMBean(serverHandler, name);
            } catch (Exception e) {
                logger.debug("Cannot register MBean for JAX-WS Endpoint Audit Handler", e);
            }
        }
        return serverHandler;
    }
}
