/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.ws.jaxws.handler.builder;

import java.util.HashMap;
import java.util.Map;

/**
 * The BindingUtils is responsible to resolve aliased URI bindings.
 *
 * @author Guillaume Sauthier
 */
public class BindingUtils {

    public static final String SOAP11_HTTP_TOKEN = "##SOAP11_HTTP";
    public static final String SOAP11_HTTP = "http://schemas.xmlsoap.org/wsdl/soap/http";

    public static final String SOAP11_HTTP_MTOM_TOKEN = "##SOAP11_HTTP_MTOM";
    public static final String SOAP11_HTTP_MTOM = SOAP11_HTTP + "?mtom=true";

    public static final String SOAP12_HTTP_TOKEN = "##SOAP12_HTTP";
    public static final String SOAP12_HTTP = "http://www.w3.org/2003/05/soap/bindings/HTTP/";

    public static final String SOAP12_HTTP_MTOM_TOKEN = "##SOAP12_HTTP_MTOM";
    public static final String SOAP12_HTTP_MTOM = SOAP12_HTTP + "?mtom=true";

    public static final String XML_HTTP_TOKEN = "##XML_HTTP";
    public static final String XML_HTTP = "http://www.w3.org/2004/08/wsdl/http";

    private static Map<String, String> bindings;

    static {
        bindings = new HashMap<String, String>();
        bindings.put(SOAP11_HTTP_TOKEN, SOAP11_HTTP);
        bindings.put(SOAP11_HTTP_MTOM_TOKEN, SOAP11_HTTP_MTOM);
        bindings.put(SOAP12_HTTP_TOKEN, SOAP12_HTTP);
        bindings.put(SOAP12_HTTP_MTOM_TOKEN, SOAP12_HTTP_MTOM);
        bindings.put(XML_HTTP_TOKEN, XML_HTTP);
    }

    /**
     * Resolve the given binding URI.
     * @param binding Binding ID
     * @return resolved binding ID (may not change if no aliases were found)
     */
    public static String resolve(final String binding) {

        // Is the binding aliased ?
        String resolved = bindings.get(binding);
        if (resolved == null) {
            // If not, consider it as already resolved
            // Anyway, we only deal with JAX-WS bindings here
            resolved = binding;
        }
        return resolved;
    }
}
