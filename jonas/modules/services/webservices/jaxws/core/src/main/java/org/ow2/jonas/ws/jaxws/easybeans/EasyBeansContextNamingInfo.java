/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.jaxws.easybeans;

import org.ow2.jonas.ws.jaxws.ejb.context.IContextNamingInfo;
import org.ow2.jonas.ws.jaxws.ejb.IEJBWebserviceEndpoint;
import org.ow2.easybeans.api.EZBContainer;

/**
 * The EasyBeansContextNamingInfo is an implementation of
 * IContextNamingInfo for easybeans.
 *
 * @author Guillaume Sauthier
 */
public class EasyBeansContextNamingInfo implements IContextNamingInfo {

    /**
     * EasyBeans container.
     */
    private EZBContainer container;

    /**
     * Exposed bean's name.
     */
    private String beanName;

    /**
     * EJB Endpoint.
     */
    private IEJBWebserviceEndpoint endpoint;

    /**
     * Creates a new ContextNamingInfo structure dedicated to easybeans.
     * @param endpoint WS endpoint.
     * @param container EasyBeans container.
     * @param beanName name of the bean.
     */
    public EasyBeansContextNamingInfo(final IEJBWebserviceEndpoint endpoint,
                                      final EZBContainer container,
                                      final String beanName) {
        this.endpoint = endpoint;
        this.container = container;
        this.beanName = beanName;
    }

    /**
     * Get the name of the EJB container hosting the EJB exposed endpoint.
     *
     * @return the EJB container name
     */
    public String getContainerName() {
        return this.container.getName();
    }

    /**
     * Get J2EE Application name.
     *
     * @return application name (is be null for standalone EjbJar)
     */
    public String getApplicationName() {
        return this.container.getConfiguration().getApplicationName();
    }

    /**
     * Get the name of the bean exposed as WS.
     *
     * @return bean's name
     */
    public String getBeanName() {
        return this.beanName;
    }

    /**
     * Get the realm name to use if this bean is secured.
     *
     * @return a real name (may be null if the bean is not secured)
     */
    public String getRealmName() {
        // TODO implement the realm support
        return null;
    }

    /**
     * Get the endpoint for which the context needs to be created.
     *
     * @return the WS endpoint.
     */
    public IEJBWebserviceEndpoint getEndpoint() {
        return this.endpoint;
    }
}
