/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.jaxws.http.servlet;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.ws.jaxws.IWSResponse;

public class ServletResponseAdapter implements IWSResponse {

    private HttpServletResponse response;

    public ServletResponseAdapter(final HttpServletResponse response) {
        this.response = response;
    }

    public OutputStream getOutputStream() throws IOException {
        return response.getOutputStream();
    }

    public void flushBuffer() throws IOException {
        // TODO Auto-generated method stub
        response.flushBuffer();
    }

    public void setContentType(final String type) {
        // TODO Auto-generated method stub
        response.setContentType(type);
    }

    public void setHeader(final String name, final String value) {
        // TODO Auto-generated method stub
        response.setHeader(name, value);
    }

    public void setStatus(final int code) {
        // TODO Auto-generated method stub
        response.setStatus(code);
    }

}
