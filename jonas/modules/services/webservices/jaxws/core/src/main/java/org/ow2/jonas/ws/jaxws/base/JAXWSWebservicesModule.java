/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.jaxws.base;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;
import org.ow2.jonas.ws.jaxws.IWebServiceEndpoint;
import org.ow2.jonas.ws.jaxws.IWebservicesContainer;
import org.ow2.jonas.ws.jaxws.IWebservicesModule;

/**
 * The WebservicesModule represents an archive containing web services.
 *
 * @author Guillaume Sauthier
 */
public class JAXWSWebservicesModule<T extends JAXWSWebservicesContainer<? extends IWebServiceEndpoint>> implements IWebservicesModule<T> {

    /**
     * Logger.
     */
    private static Log logger = LogFactory.getLog(JAXWSWebservicesModule.class);


    /**
     * The archive name, or a unique archive identifier.
     */
    private String name;

    /**
     * All the WSContainers available in the archive.
     */
    private Map<String, T> containers;

    /**
     * Build a new module.
     * @param name identifier
     */
    public JAXWSWebservicesModule(final String name) {
        this.name = name;
        containers = new HashMap<String, T>();
    }

    /**
     * Add the given container.
     * @param container added container
     */
    public void addContainer(final T container) {
        containers.put(container.getName(), container);
    }

    /**
     * Remove a given container.
     * @param container removed container
     */
    public void removeContainer(final T container) {
        containers.remove(container.getName());
    }


    /**
     * Find a container with the given name.
     * @param name name of the container
     * @return the container or null if none was found.
     */
    public T findContainer(final String name) {
        return containers.get(name);
    }

    /**
     * @return All the containers in this module
     */
    public Collection<T> getContainers() {
        return containers.values();
    }


    /**
     * Starts the module.
     * Recursively starts inner containers.
     */
    public void start() {
        logger.debug("Start WebservicesModule[{0}]", name);
        for (IWebservicesContainer container : containers.values()) {
            container.start();
        }
    }

    /**
     * Stop the module.
     * Recursively stops inner containers.
     */
    public void stop() {
        logger.debug("Stop WebservicesModule[{0}]", name);
        for (IWebservicesContainer container : containers.values()) {
            container.stop();
        }
    }

    /**
     * @return the name of this module
     */
    public String getName() {
        return name;
    }
}
