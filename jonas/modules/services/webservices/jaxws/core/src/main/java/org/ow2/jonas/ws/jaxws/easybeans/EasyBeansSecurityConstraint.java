/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.jaxws.easybeans;

import java.util.List;

import org.ow2.jonas.ws.jaxws.ejb.ISecurityConstraint;
import org.ow2.jonas.ws.jaxws.PortMetaData;
import org.ow2.easybeans.api.bean.info.IBeanInfo;
import org.ow2.easybeans.api.bean.info.IWebServiceInfo;

/**
 * The EasyBeansSecurityConstraint is the representation of an endpoint's security contraint.
 *
 * @author Guillaume Sauthier
 */
public class EasyBeansSecurityConstraint implements ISecurityConstraint {

    /**
     * Hold the url-pattern.
     */
    private PortMetaData pmd;

    private IBeanInfo info;

    public EasyBeansSecurityConstraint(final PortMetaData pmd, final IBeanInfo info) {
        this.pmd = pmd;
        this.info = info;
    }

    /**
     * @return the secured Url pattern.
     */
    public String getUrlPattern() {
        return pmd.getUrlPattern();
    }

    /**
     * @return the list of protected HTTP methods.
     */
    public List<String> getHttpMethods() {
        IWebServiceInfo wsInfo = info.getWebServiceInfo();
        if (wsInfo == null) {
            return null;
        }
        return wsInfo.getHttpMethods();
    }

    /**
     * @return the required guarantee.
     */
    public String getTransportGuarantee() {
        IWebServiceInfo wsInfo = info.getWebServiceInfo();
        if (wsInfo == null) {
            return null;
        }
        return wsInfo.getTransportGuarantee();
    }

    /**
     * @return the list of references roles in the EJB.
     */
    public List<String> getRoleNames() {
        return info.getSecurityInfo().getDeclaredRoles();
    }

    /**
     * @return the authentication method's name.
     */
    public String getAuthMethod() {
        IWebServiceInfo wsInfo = info.getWebServiceInfo();
        if (wsInfo == null) {
            return null;
        }
        return wsInfo.getAuthMethod();
    }

    /**
     * @return the name of the realm to be used for authentication.
     */
    public String getRealmName() {
        IWebServiceInfo wsInfo = info.getWebServiceInfo();
        if (wsInfo == null) {
            return null;
        }
        return wsInfo.getRealmName();
    }
}
