/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.ws.jaxws.handler;


import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.annotation.PostConstruct;

import org.ow2.util.annotation.processor.ProcessorException;
import org.ow2.util.annotation.processor.handler.AbstractAnnotationHandler;

/**
 * The PostConstructProcessor is responsible of calling @PostConstruct annotated methods.
 *
 * @author Guillaume Sauthier
 */
public class PostConstructProcessor extends AbstractAnnotationHandler {

    /**
     * Cached no params Object.
     */
    private static Object[] NO_PARAMS = new Object[0];

    public boolean isSupported(final Class<? extends Annotation> aClass) {
        return PostConstruct.class.equals(aClass);
    }

    @Override
    public void process(final Annotation annotation, final Method method, final Object o) throws ProcessorException {

        // Only methods without parameters can be called
        if (method.getParameterTypes().length == 0) {

            boolean accessible = method.isAccessible();
            try {
                method.setAccessible(true);

                try {
                    method.invoke(o, NO_PARAMS);
                } catch (IllegalAccessException e) {
                    throw new ProcessorException("Cannot access method " + method, e);
                } catch (InvocationTargetException e) {
                    throw new ProcessorException("Cannot invoke method " + method, e);
                }
            } finally {
                method.setAccessible(accessible);
            }
        }
    }




}
