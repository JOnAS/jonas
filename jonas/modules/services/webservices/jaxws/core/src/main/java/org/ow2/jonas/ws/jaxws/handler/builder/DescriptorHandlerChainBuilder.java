/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.jaxws.handler.builder;

import java.util.List;

import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.PortInfo;

import org.ow2.util.ee.metadata.common.api.xml.struct.IHandlerChains;

/**
 * The DescriptorHandlerChainBuilder is responsible to build a Handler chain
 * from a pre parsed handler-chains.xml file.
 *
 * @author Guillaume Sauthier
 */
public class DescriptorHandlerChainBuilder extends HandlerChainBuilder {

    /**
     * Already parsed HandlerChains structure.
     */
    private IHandlerChains handlerChainsElement;

    /**
     * Constructs a new builder using the given structure.
     * @param handlerChains structure representing the handler chain
     */
    public DescriptorHandlerChainBuilder(final IHandlerChains handlerChains) {
        this.handlerChainsElement = handlerChains;
    }

    /**
     * Build the chain.
     * @param info identify the target port for the builded chain
     * @param loader classloader used for Handler class loading
     * @return a new Handler's List
     */
    @Override
    public List<Handler> buildHandlerChain(final PortInfo info, final ClassLoader loader) {
        return buildHandlerChain(handlerChainsElement, info, loader);
    }
}