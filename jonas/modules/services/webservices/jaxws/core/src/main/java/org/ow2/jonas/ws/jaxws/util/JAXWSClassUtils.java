/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.jaxws.util;

import javax.jws.WebService;
import javax.xml.ws.WebServiceProvider;

/**
 * The JAXWSClassUtils is a utility class used to extract informations from the JAXWS annotations.
 *
 * @author Guillaume Sauthier
 */
public class JAXWSClassUtils {

    /**
     * Private default constructor for utility class.
     */
    private JAXWSClassUtils() {}

    /**
     * @param klass class supporting annotations
     * @return <code>true</code> is the given class support @WebService or @WebServiceProvider
     */
    public static boolean isWebService(Class<?> klass) {

        return ((klass.getAnnotation(WebService.class) != null)
                || (klass.getAnnotation(WebServiceProvider.class) != null));
    }

    /**
     * @param klass class supporting annotations
     * @return the 'wsdlLocation' attribute's value (from @WebService or @WebServiceProvider)
     */
    public static String getWsdlLocation(Class<?> klass) {
        WebService ws = getWebService(klass);
        if (ws != null) {
            return ws.wsdlLocation();
        }
        WebServiceProvider wsp = getWebServiceProvider(klass);
        if (wsp != null) {
            return wsp.wsdlLocation();
        }

        throw new IllegalStateException("Class '" + klass + "' have neither @WebService nor @WebServiceProvider annotation");
    }

    /**
     * @param klass class supporting annotations
     * @return the 'serviceName' attribute's value (from @WebService or @WebServiceProvider)
     */
    public static String getServiceName(Class<?> klass) {
        WebService ws = getWebService(klass);
        if (ws != null) {
            return ws.serviceName();
        }
        WebServiceProvider wsp = getWebServiceProvider(klass);
        if (wsp != null) {
            return wsp.serviceName();
        }

        throw new IllegalStateException("Class '" + klass + "' have neither @WebService nor @WebServiceProvider annotation");
    }

    /**
     * @param klass class supporting annotations
     * @return the WebService annotation (null if no annotation is found)
     */
    private static WebService getWebService(Class<?> klass) {
        return klass.getAnnotation(WebService.class);
    }

    /**
     * @param klass class supporting annotations
     * @return the WebServiceProvider annotation (null if no annotation is found)
     */
    private static WebServiceProvider getWebServiceProvider(Class<?> klass) {
        return klass.getAnnotation(WebServiceProvider.class);
    }
}
