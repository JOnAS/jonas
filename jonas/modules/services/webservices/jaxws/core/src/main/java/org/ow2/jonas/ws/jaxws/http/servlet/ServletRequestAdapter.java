/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.jaxws.http.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.util.Hashtable;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.ow2.jonas.ws.jaxws.IWSRequest;

/**
 * Wraps an {@link HttpServletRequest}.
 * @author Guillaume Sauthier
 */
public class ServletRequestAdapter implements IWSRequest {

    /**
     * Wrapped HTTP request.
     */
    private HttpServletRequest request;

    /**
     * Storage for attributes.
     */
    private Map<Class<?>, Object> attributes;

    /**
     * Construct a new {@link IWSRequest} from an {@link HttpServletRequest}.
     * @param request wrapped request
     */
    public ServletRequestAdapter(final HttpServletRequest request) {
        this.request = request;
        attributes = new Hashtable<Class<?>, Object>();
    }

    /* (non-Javadoc)
     * @see org.ow2.jonas.ws.jaxws.IWSRequest#getAttribute(java.lang.Class)
     */
    public <T> T getAttribute(final Class<T> type) {
        return type.cast(attributes.get(type));
    }

    public String getContentType() {
        return request.getContentType();
    }

    public InputStream getInputStream() throws IOException {
        return request.getInputStream();
    }

    public String getRemoteAddr() {
        return request.getRemoteAddr();
    }

    public String getHeader(final String name) {
        return request.getHeader(name);
    }

    public String getParameter(final String parameter) {
        return request.getParameter(parameter);
    }

    public <T> T setAttribute(final Class<T> type, final T value) {
        Object old = attributes.put(type, value);
        return type.cast(old);
    }

    public StringBuffer getRequestURL() {
        // TODO Auto-generated method stub
        return request.getRequestURL();
    }

}
