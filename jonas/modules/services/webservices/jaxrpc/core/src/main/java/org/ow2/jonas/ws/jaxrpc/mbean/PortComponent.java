/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ws.jaxrpc.mbean;

import java.util.ArrayList;
import java.util.List;

/**
 * A <code>PortComponent</code> represents a port-component
 * in webservices.xml.
 *
 * @author Guillaume Sauthier
 */
public class PortComponent extends AbstractWebServiceMBean {

    /**
     * PortComponent name
     */
    private String name = null;

    /**
     * wsdl:port QName
     */
    private String wsdlPort = null;

    /**
     * ssei fully qualified classname
     */
    private String serviceEndpointInterface = null;

    /**
     * Implementation Bean's ObjectName
     */
    private String implementationBeanOName = null;

    /**
     * Handler list (childs)
     */
    private List handlers = new ArrayList();

    /**
     * Handler's ObjectName list
     */
    private List handlerONames = new ArrayList();

    /**
     * Endpoint URL : where the PortComponent can be accessed
     */
    private String endpoint = null;

    /**
     * PortComponent Constructor
     * @param objectName PortComponent's ObjectName
     */
    public PortComponent(final String objectName) {
        super(objectName);
    }

    /**
     * @return Returns the endpoint.
     */
    public String getEndpoint() {
        return endpoint;
    }

    /**
     * @param endpoint The endpoint to set.
     */
    public void setEndpoint(final String endpoint) {
        this.endpoint = endpoint;
    }

    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name to set.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return Returns the serviceEndpointInterface.
     */
    public String getServiceEndpointInterface() {
        return serviceEndpointInterface;
    }

    /**
     * @param serviceEndpointInterface The serviceEndpointInterface to set.
     */
    public void setServiceEndpointInterface(final String serviceEndpointInterface) {
        this.serviceEndpointInterface = serviceEndpointInterface;
    }

    /**
     * @return Returns the wsdlPort.
     */
    public String getWsdlPort() {
        return wsdlPort;
    }

    /**
     * @param wsdlPort The wsdlPort to set.
     */
    public void setWsdlPort(final String wsdlPort) {
        this.wsdlPort = wsdlPort;
    }

    /**
     * @return Returns the handlers.
     */
    public List getHandlersMBean() {
        return handlers;
    }

    /**
     * @return Returns the handlers.
     */
    public String[] getHandlers() {
        return (String[]) handlerONames.toArray(new String[handlerONames.size()]);
    }

    /**
     * Add a handler
     * @param h Handler MBean
     */
    public void addHandlerMBean(final Handler h) {
        handlers.add(h);
        handlerONames.add(h.getObjectName());
    }

    /**
     * @return Returns the implementationBeanOName.
     */
    public String getImplementationBean() {
        return implementationBeanOName;
    }

    /**
     * @param implementationBean The implementationBeanOName to set.
     */
    public void setImplementationBean(final String implementationBean) {
        this.implementationBeanOName = implementationBean;
    }

    /**
     * @return Returns the PortComponent MBean subtype
     */
    @Override
    protected String getMBeanType() {
        return WebServicesObjectName.PORTCOMPONENT_TYPE;
    }

    /**
     * @return Returns the childs MBeans (if any)
     */
    @Override
    protected List getChildsMBeans() {
        return handlers;
    }


}
