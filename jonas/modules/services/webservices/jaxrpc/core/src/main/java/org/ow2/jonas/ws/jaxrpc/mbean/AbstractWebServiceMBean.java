/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ws.jaxrpc.mbean;

import java.util.Iterator;
import java.util.List;

import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;

import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;
import org.ow2.jonas.jmx.JmxService;
import org.ow2.jonas.lib.management.javaee.J2EEManagedObject;
import org.ow2.jonas.lib.util.Log;

/**
 * Base Class for WebServices MBeans. It makes registration/unregistration
 * of hierarchical MBeans very easy. Needs to be subclassed for each WebService MBean.
 *
 * @author Guillaume Sauthier
 */
public abstract class AbstractWebServiceMBean extends J2EEManagedObject {

    /**
     * logger
     */
    private static Logger log = Log.getLogger("org.ow2.jonas.ws");

    /**
     * ObjectName instance : avoid recreating an ObjectName each time we want one
     */
    private ObjectName realObjectName = null;

    /**
     * Constructor for default J2eeManagedObject
     * @param objectName the MBean's ObjectName
     */
    public AbstractWebServiceMBean(final String objectName) {
        super(objectName);
        try {
            realObjectName = ObjectName.getInstance(objectName);
        } catch (MalformedObjectNameException e) {
            // Cannot go here
            realObjectName = null;
        }
    }

    /**
     * Registers the MBean (and its childs) in the specified MBeanServer
     * @param jmx reference on the jmx service which allows registering of MBeans
     */
    public void register(final JmxService jmx) {

        try {
            jmx.registerModelMBean(this, realObjectName);
        } catch (Exception e) {
            // should never goes here
            e.printStackTrace();
        }

        // register "childs"
        List childs = getChildsMBeans();
        if (childs != null) {
            for (Iterator i = childs.iterator(); i.hasNext();) {
                AbstractWebServiceMBean child = (AbstractWebServiceMBean) i.next();
                child.register(jmx);
            }
        }
    }

    /**
     * @return Returns the MBean type (as used in mbean-descriptor.xml)
     */
    protected abstract String getMBeanType();

    /**
     * Unregisters the MBean (and its childs) from the specified MBeanServer
     * @param jmx reference on the jmx service which allows unregistering of MBeans
     */
    public void unregister(final JmxService jmx) {
        // unregister MBean
        try {
            jmx.unregisterModelMBean(ObjectName.getInstance(getObjectName()));
        } catch (Exception e) {
            // nothing to do, we simply continue the unregistering process
            log.log(BasicLevel.DEBUG, "Should never goes here", e);
        }

        // unregister "childs"
        List childs = getChildsMBeans();
        if (childs != null) {
            for (Iterator i = childs.iterator(); i.hasNext();) {
                AbstractWebServiceMBean child = (AbstractWebServiceMBean) i.next();
                child.unregister(jmx);
            }
        }
    }

    /**
     * @return Returns the Child List
     */
    protected abstract List getChildsMBeans();

    /**
     * @return Returns the ObjectName instance
     */
    public ObjectName getRealObjectName() {
        return this.realObjectName;
    }
}
