/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ws.jaxrpc.mbean;

import java.util.List;
import java.util.Properties;

/**
 * A <code>Handler</code> MBean represents a
 * handler element in webservices.xml
 *
 * @author Guillaume Sauthier
 */
public class Handler extends AbstractWebServiceMBean {

    /**
     * Handler name
     */
    private String name = null;

    /**
     * Handler classname
     */
    private String classname = null;

    /**
     * Properties used to init the handler
     */
    private Properties initParams = new Properties();

    /**
     * list of soap:header the handler will access
     */
    private String[] soapHeaders = null;

    /**
     * list of soap-roles under which the handler acts
     */
    private String[] soapRoles = null;

    /**
     * Handler constructor
     * @param objectName Handler ObjectName
     */
    public Handler(final String objectName) {
        super(objectName);
    }

    /**
     * @return Returns the classname.
     */
    public String getClassname() {
        return classname;
    }

    /**
     * @param classname The classname to set.
     */
    public void setClassname(final String classname) {
        this.classname = classname;
    }

    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name to set.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return Returns the initParams.
     */
    public Properties getInitParams() {
        return initParams;
    }

    /**
     * @param initParams The initParams to set.
     */
    public void setInitParams(final Properties initParams) {
        this.initParams = initParams;
    }

    /**
     * @return Returns the soapHeaders.
     */
    public String[] getSoapHeaders() {
        return soapHeaders;
    }

    /**
     * @return Returns the soapRoles.
     */
    public String[] getSoapRoles() {
        return soapRoles;
    }

    /**
     * @param soapHeaders The soapHeaders to set.
     */
    public void setSoapHeaders(final String[] soapHeaders) {
        this.soapHeaders = soapHeaders;
    }

    /**
     * @param soapRoles The soapRoles to set.
     */
    public void setSoapRoles(final String[] soapRoles) {
        this.soapRoles = soapRoles;
    }

    /**
     * @return Returns the Handler MBean subtype
     */
    @Override
    protected String getMBeanType() {
        return WebServicesObjectName.HANDLER_TYPE;
    }

    /**
     * @return Returns the childs MBeans (if any)
     */
    @Override
    protected List getChildsMBeans() {
        // Handlers are in the bottom of the Chain, no childs
        return null;
    }

}
