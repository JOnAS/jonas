/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2009 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
*/

package org.ow2.jonas.ws.jaxrpc.factory;

import javax.naming.Reference;
import javax.naming.spi.ObjectFactory;

import org.ow2.jonas.deployment.api.IServiceRefDesc;
import org.ow2.jonas.ws.jaxrpc.WSException;


/**
 * JServiceFactory is used to create the JaxRpc Service.
 * It must be implemented for all different WebServices Engine.
 *
 * @author Xavier Delplanque
 * @author Guillaume Sauthier
 */
public interface JServiceFactory extends ObjectFactory {

    /**
     * Create the Reference associated with the service-ref (to be bound in JNDI).
     *
     * @param sr The service-ref description
     * @param cl The classloader used to load Service class
     *
     * @return The Reference
     *
     * @throws WSException When Reference creation fails
     */
    Reference getServiceReference(IServiceRefDesc  sr, ClassLoader cl) throws WSException;


}
