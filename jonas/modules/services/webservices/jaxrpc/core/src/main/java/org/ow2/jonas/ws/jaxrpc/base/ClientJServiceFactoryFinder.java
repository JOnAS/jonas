/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2009 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ws.jaxrpc.base;

import java.io.InputStream;
import java.util.Properties;

import org.ow2.jonas.ws.jaxrpc.WSException;
import org.ow2.jonas.ws.jaxrpc.factory.JServiceFactory;



/**
 * Used to retrieve from jonas-client.properties the factory to use for Service
 * creation (WebServices)
 * @author Guillaume Sauthier
 */
public class ClientJServiceFactoryFinder {

    /**
     * jonas-client properties
     */
    private static Properties props;

    /**
     * Property name
     */
    private static final String JONAS_SERVICE_FACTORY = "jonas.service.ws.factory.class";

    /**
     * Private empty constructor for utility class
     */
    private ClientJServiceFactoryFinder() {
    }

    /**
     * Factory class name
     */
    private static String factoryClassName = null;

    /**
     * Return the JServiceFactory specified in jonas.properties
     * @return the JServiceFactory specified in jonas.properties
     * @throws WSException When JServiceFactory instantiation fails
     */
    public static JServiceFactory getJOnASServiceFactory() throws WSException {
        JServiceFactory factory = null;
        ClassLoader current = Thread.currentThread().getContextClassLoader();

        if (props == null) {
            props = new Properties();
            try {
                InputStream is = current.getResourceAsStream("jonas-client.properties");
                if (is != null) {
                    props.load(is);
                    factoryClassName = props.getProperty(JONAS_SERVICE_FACTORY);
                } else {
                    System.err.println("Cannot load jonas-client.properties from ClassLoader");
                }
            } catch (Exception e) {
                String err = "Error when trying to get jonas property '" + JONAS_SERVICE_FACTORY
                        + "' from jonas-client.properties";
                throw new WSException(err, e);
            }
        }

        if (factoryClassName == null) {
            String err = "jonas property '" + JONAS_SERVICE_FACTORY + "' must be set! in jonas-client.properties";
            throw new WSException(err);
        }

        // instanciation
        try {
            factory = (JServiceFactory) current.loadClass(factoryClassName).newInstance();
        } catch (ClassNotFoundException cnfe) {
            String err = "ClassNotFound '" + factoryClassName + "' in JOnAS ClassLoader";
            throw new WSException(err);
        } catch (InstantiationException ie) {
            String err = "Instantiation error for new '" + factoryClassName + "'";
            throw new WSException(err);
        } catch (IllegalAccessException ie) {
            String err = "Illegal Access for new '" + factoryClassName + "'";
            throw new WSException(err);
        }

        return factory;

    }

}