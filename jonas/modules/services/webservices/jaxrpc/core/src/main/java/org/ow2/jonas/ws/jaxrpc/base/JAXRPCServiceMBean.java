/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.jaxrpc.base;

/**
 * MBean interface of the JAX-RPC service.
 * @author Francois Fornaciari
 */
public interface JAXRPCServiceMBean {
    /**
     * Automatic WSGen is enabled ?
     * @return True if automatic WSGen needs to be Called.
     */
    boolean isAutoWsGenEngaged();

    /**
     * Returns the URL prefixing all generated endpoint URL.
     * @return The URL prefixing all generated endpoint URL
     */
    String getUrlPrefix();

    /**
     * Validating parser or not ?
     * @return Validating parser or not
     */
    public boolean isParsingWithValidation();

    /**
     * Engage Automatic WsGen ?
     * @param engage the engage value
     */
    void setAutoWsGenEngaged(final boolean engage);

    /**
     * Sets the URL prefixing all generated endpoint URL.
     * @param prefix URL prefixing all generated endpoint URL
     */
    void setUrlPrefix(final String prefix);

    /**
     * Use validating parser or not .
     * @param validate validating parser or not
     */
    void setParsingWithValidation(final boolean validate);
}
