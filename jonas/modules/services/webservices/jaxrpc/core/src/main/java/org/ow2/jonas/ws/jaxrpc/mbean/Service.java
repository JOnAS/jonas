/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ws.jaxrpc.mbean;

import java.util.ArrayList;
import java.util.List;

/**
 * A <code>WebService</code> MBean represents a <code>webservice-description</code>
 * in webservices.xml.
 *
 * @author Guillaume Sauthier
 */
public class Service extends AbstractWebServiceMBean {

    /**
     * Service's name
     */
    private String name = null;

    /**
     * Published WSDL URL
     */
    private String wsdlURL = null;

    /**
     * JAX-RPC mapping file name
     */
    private String mappingFilename = null;

    /**
     * WSDL file name
     */
    private String wsdlFilename = null;

    /**
     * PortComponent MBean list
     */
    private List portComponents = new ArrayList();

    /**
     * PortComponent's ObjectName list
     */
    private List portComponentONames = new ArrayList();

    /**
     * Service Constructor
     * @param objectName Service's ObjectName
     */
    public Service(final String objectName) {
        super(objectName);
    }

    /**
     * @return Returns the mappingFile.
     */
    public String getMappingFilename() {
        return mappingFilename;
    }

    /**
     * @param mappingFile The mappingFile to set.
     */
    public void setMappingFilename(final String mappingFile) {
        this.mappingFilename = mappingFile;
    }

    /**
     * @return Returns the wsdlFilename.
     */
    public String getWsdlFilename() {
        return wsdlFilename;
    }

    /**
     * @param wsdlFilename The wsdlFilename to set.
     */
    public void setWsdlFilename(final String wsdlFilename) {
        this.wsdlFilename = wsdlFilename;
    }

    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name to set.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return Returns the wsdlURL.
     */
    public String getWsdlURL() {
        return wsdlURL;
    }

    /**
     * @param wsdlURL The wsdlURL to set.
     */
    public void setWsdlURL(final String wsdlURL) {
        this.wsdlURL = wsdlURL;
    }

    /**
     * @return Returns the portComponents MBean.
     */
    public List getPortComponentsMBean() {
        return portComponents;
    }

    /**
     * @return Returns the portComponentONames.
     */
    public String[] getPortComponents() {
        return (String[]) portComponentONames.toArray(new String[portComponentONames.size()]);
    }

    /**
     * Add a portComponent
     * @param pc PortComponent MBean
     */
    public void addPortComponentMBean(final PortComponent pc) {
        portComponents.add(pc);
        portComponentONames.add(pc.getObjectName());
    }

    /**
     * @return Returns the Service MBean subtype
     */
    @Override
    protected String getMBeanType() {
        return WebServicesObjectName.WEBSERVICE_TYPE;
    }

    /**
     * @return Returns the childs MBeans (if any)
     */
    @Override
    protected List getChildsMBeans() {
        return portComponents;
    }

}
