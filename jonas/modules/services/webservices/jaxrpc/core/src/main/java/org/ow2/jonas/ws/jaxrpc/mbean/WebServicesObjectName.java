/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ws.jaxrpc.mbean;

import java.util.Hashtable;
import java.util.Iterator;

import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;


/**
 * ToolKit for WebServices Mbeans. It Helps to create ObjectNames for the
 * WebServices MBeans, simplify MBeans queries, ...
 *
 * @author Guillaume Sauthier
 */
public class WebServicesObjectName {

    /**
     * WebService type property
     */
    public static final String WEBSERVICE_TYPE = "WebService";

    /**
     * WebServicePortComponent type property
     */
    public static final String PORTCOMPONENT_TYPE = "WebServicePortComponent";

    /**
     * WebServiceHandler type property
     */
    public static final String HANDLER_TYPE = "WebServiceHandler";

    /**
     * j2eeType for EJB modules
     */
    public static final String EJBMODULE = "EJBModule";

    /**
     * j2eeType for Web modules
     */
    public static final String WEBMODULE = "WebModule";

    /**
     * j2eeType for ear
     */
    public static final String J2EEAPPLICATION = "J2EEApplication";

    /**
     * j2eeType for server instance
     */
    public static final String J2EESERVER = "J2EEServer";

    /**
     * domain name
     */
    private static String domain = null;

    /**
     * Default private Constructor for tool class
     */
    private WebServicesObjectName() {
    }

    /**
     * @return Returns the domain name of the JOnAS server.
     *
     * TODO refactor this out (duplicated code from J2eeObjectName)
     */
    private static String getDomain() {
        return domain;
    }

    /**
     * Sets the domain name of the JOnAS server.
     * @param myDomain domain name
     */
    public static void setDomain(final String myDomain) {
        domain = myDomain;
    }

    /**
     * @param name
     *            base ObjectName
     * @return Returns a String that contains "inherited" properties from
     *         parent's ObjectName
     */
    private static String getInheritedPropertiesAsString(final ObjectName name) {
        Hashtable table = (Hashtable) name.getKeyPropertyList().clone();
        // we remove some attributes from the ObjectName
        table.remove("j2eeType");
        table.remove("type");
        table.remove("subtype");
        table.remove("name");
        StringBuffer sb = new StringBuffer();
        for (Iterator i = table.keySet().iterator(); i.hasNext();) {
            String key = (String) i.next();
            sb.append(key + "=" + table.get(key) + ",");
        }
        if (sb.length() > 1) {
            // remove the trailing comma
            sb.setLength(sb.length() - 1);
        }
        return sb.toString();
    }

    /**
     * @return ObjectName for Service
     * @param name
     *            Service id
     * @param parent
     *            parent name
     * @exception MalformedObjectNameException
     *                Could not create ObjectName with the given String
     */
    public static ObjectName service(final String name, final ObjectName parent)
            throws MalformedObjectNameException {
        String parentModule = "";
        if (parent.getKeyProperty("j2eeType").equals(EJBMODULE)) {
            parentModule = EJBMODULE + "="
                    + parent.getKeyProperty("name");
        } else {
            parentModule = WEBMODULE + "="
                    + parent.getKeyProperty("name");
        }
        return ObjectName.getInstance(getDomain()
                + ":type=WebService,name=" + name + "," + parentModule + ","
                + getInheritedPropertiesAsString(parent));
    }

    /**
     * @return ObjectName for PortComponent
     * @param name
     *            PortComponent id
     * @param parent
     *            parent name
     * @exception MalformedObjectNameException
     *                Could not create ObjectName with the given String
     */
    public static ObjectName portComponent(final String name, final ObjectName parent)
            throws MalformedObjectNameException {
        String parentModule = "";
        if (parent.getKeyProperty("type").equals(WEBSERVICE_TYPE)) {
            parentModule = WEBSERVICE_TYPE + "="
                    + parent.getKeyProperty("name");
        }
        return ObjectName.getInstance(getDomain()
                + ":type=" + PORTCOMPONENT_TYPE + ",name=" + name + ","
                + parentModule + "," + getInheritedPropertiesAsString(parent));
    }

    /**
     * @return Returns the ObjectName for PortComponent
     * @param name PortComponent id
     * @param parent parent name
     * @exception MalformedObjectNameException
     *                Could not create ObjectName with the given String
     */
    public static ObjectName handler(final String name, final ObjectName parent)
            throws MalformedObjectNameException {
        String parentModule = "";
        if (parent.getKeyProperty("type").equals(PORTCOMPONENT_TYPE)) {
            parentModule = PORTCOMPONENT_TYPE + "="
                    + parent.getKeyProperty("name");
        }
        return ObjectName.getInstance(getDomain()
                + ":type=" + HANDLER_TYPE + ",name=" + name + ","
                + parentModule + "," + getInheritedPropertiesAsString(parent));
    }

    /**
     * @param parent parent ObjectName (J2EEApplication or J2EEModule subtype)
     * @param ejbName SSB name
     * @return Returns a Query ObjectName used to find a StatelessSessionBean
     *  coming from a particular J2EEApplication/J2EEModule
     */
    public static ObjectName getStatelessSessionBeanQuery(final ObjectName parent,
            final String ejbName) {
        try {
            StringBuffer sb = new StringBuffer(parent.getDomain());
            sb.append(":j2eeType=StatelessSessionBean");
            sb.append(",name=");
            sb.append(ejbName);
            sb.append(",J2EEApplication=");
            if (parent.getKeyProperty("J2EEApplication") != null) {
                // parent MBean maybe is not a J2EEApplication, but an EJBModule
                sb.append(parent.getKeyProperty("J2EEApplication"));
            } else {
                sb.append(parent.getKeyProperty("name"));
            }
            sb.append(",J2EEServer=");
            sb.append(parent.getKeyProperty("J2EEServer"));
            sb.append(",*");
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * @param parent parent J2EEApplication's ObjectName
     * @param servletName servlet name
     * @return Returns a Query ObjectName used to find a Servlet
     *  coming from a particular J2EEApplication
     */
    public static ObjectName getServletQuery(final ObjectName parent, final String servletName) {
        try {
            StringBuffer sb = new StringBuffer(parent.getDomain());
            sb.append(":j2eeType=Servlet");
            sb.append(",name=");
            sb.append(servletName);
            sb.append(",J2EEApplication=");
            if (parent.getKeyProperty("J2EEApplication") != null) {
                // parent MBean maybe is not a J2EEApplication, but an WebModule
                sb.append(parent.getKeyProperty("J2EEApplication"));
            } else {
                sb.append(parent.getKeyProperty("name"));
            }
            sb.append(",J2EEServer=");
            sb.append(parent.getKeyProperty("J2EEServer"));
            sb.append(",*");
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * @param ear parent J2EEApplication's ObjectName
     * @param key EJBModule name
     * @return Returns a Query ObjectName used to find an EJBModule
     *  coming from a particular J2EEApplication
     */
    public static ObjectName getEJBModule(final ObjectName ear, final String key) {
        try {
            StringBuffer sb = new StringBuffer(ear.getDomain());
            sb.append(":j2eeType=EJBModule");
            sb.append(",name=");
            sb.append(key);
            sb.append(",J2EEApplication=");
            sb.append(ear.getKeyProperty("name"));
            sb.append(",J2EEServer=");
            sb.append(ear.getKeyProperty("J2EEServer"));
            sb.append(",*");
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    /**
     * @param ear parent J2EEApplication's ObjectName
     * @param key WebModule name
     * @return Returns a Query ObjectName used to find an WebModule
     *  coming from a particular J2EEApplication
     */
    public static ObjectName getWebModule(final ObjectName ear, final String key) {
        try {
            StringBuffer sb = new StringBuffer(ear.getDomain());
            sb.append(":j2eeType=WebModule");
            sb.append(",name=");
            sb.append(key);
            sb.append(",J2EEApplication=");
            sb.append(ear.getKeyProperty("name"));
            sb.append(",J2EEServer=");
            sb.append(ear.getKeyProperty("J2EEServer"));
            sb.append(",*");
            return new ObjectName(sb.toString());
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }
}
