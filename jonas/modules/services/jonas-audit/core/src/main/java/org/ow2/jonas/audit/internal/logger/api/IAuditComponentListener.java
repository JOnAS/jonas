/**
 * JOnAS
 * Copyright (C) 2010 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.audit.internal.logger.api;

import javax.management.ObjectName;

import org.ow2.jonas.audit.internal.logger.AuditLogServiceException;

/**
 * Defines the interface of all listeners.
 * @author Florent Benoit
 */
public interface IAuditComponentListener {

    /**
     * This method add a listener on a MBean if this MBean is present in the JMX server.
     * @param jmxName the objectname on which we needs to be registered
     * @throws AuditLogServiceException A communication problem occurred when talking to
     *                               the MBean server.
     */
    void register(final ObjectName jmxName) throws AuditLogServiceException;

}
