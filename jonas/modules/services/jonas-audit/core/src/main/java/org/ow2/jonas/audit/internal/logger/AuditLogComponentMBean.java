/**
 * JOnAS
 * Copyright (C) 2010 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.audit.internal.logger;

/**
 * MBean of the logging component.
 * @author Vincent Michaud
 */
public interface AuditLogComponentMBean {

    /**
     * Remove a specific log file.
     * @param year The year of the log file
     * @param month The month of the log file
     * @param day The day of the log file
     */
    void cleanLogFile(final int year, final int month, final int day);

    /**
     * Clean the log pool.
     */
    void cleanLogPool();

    /**
     * Clean the log pool by removing old files.
     * @param nbFiles Number of files to remove
     */
    void cleanOldLogFiles(final int nbFiles);

    /**
     * Clean all log files created before a date.
     * If a log file with the specified date exists,
     * the log file is not removed.
     * @param year The year of the date
     * @param month The month of the date
     * @param day The day of the date
     */
    void cleanOldLogFiles(final int year, final int month, final int day);

    /**
     * Clean the log pool by removing recent files.
     * @param nbFiles Number of files to remove
     */
    void cleanRecentLogFiles(final int nbFiles);

    /**
     * Clean all log files created after a date.
     * If a log file with the specified date exists,
     * the log file is not removed.
     * @param year The year of the date
     * @param month The month of the date
     * @param day The day of the date
     */
    void cleanRecentLogFiles(final int year, final int month, final int day);

    /**
     * Count the number of log files in the pool.
     * @return The number of log files in the pool
     */
    int countLogFiles();

    /**
     * Disable the EJB logger.
     * @throws AuditLogServiceException If connection with audit component can not be etablished
     */
    void disableEJBLogger() throws AuditLogServiceException;

    /**
     * Disable the Web logger.
     * @throws AuditLogServiceException If connection with audit component can not be etablished
     */
    void disableWebLogger() throws AuditLogServiceException;

    /**
     * Enable the EJB logger.
     * @throws AuditLogServiceException If connection with audit component can not be etablished
     */
    void enableEJBLogger() throws AuditLogServiceException;

    /**
     * Enable the Web logger.
     * @throws AuditLogServiceException If connection with audit component can not be etablished
     */
    void enableWebLogger() throws AuditLogServiceException;

    /**
     * Get all audit reports for a specific date.
     * Audit reports can be decode with the ObjectEncoder.
     * @param year The year of the date
     * @param month The month of the date
     * @param day The day of the date
     * @return An array of encoded audit reports. The array
     * can contains 'null' if the class of the audit
     * report is not found in the classpath.
     * Return null if there is not any audit report found
     */
    String[] getAuditReports(final int year, final int month, final int day);

    /**
     * Get the last stored audit reports.
     * Audit reports can be decode with the ObjectEncoder.
     * @param number The number of audit reports
     * @return An array of encoded audit reports with size less or equal
     * to 'number'. The array can contains 'null' if the class of
     * the audit report is not found in the classpath. Return null
     * if the specified number of audit report is not positive.
     * Return null if there is not any audit report found
     */
    String[] getLastAuditReports(final int number);

    /**
     * Get all audit reports before a specific date. If an audit report
     * has been created at the specified date, it is not included
     * in the result. Audit reports can be decode with the ObjectEncoder.
     * @param year The year of the date
     * @param month The month of the date
     * @param day The day of the date
     * @return An array of encoded audit reports. The array can contains 'null' if the
     * class of the audit report is not found in the classpath.
     * Return null if there is not any audit report found
     */
    String[] getOldAuditReports(final int year, final int month, final int day);

    /**
     * Get all audit reports before a specific hour. If an audit
     * report has been created at the specified hour, it is not included
     * in the result. Audit reports can be decode with the ObjectEncoder.
     * @param timestamp The hour
     * @return An array of encoded audit reports. The array can contains 'null'
     * if the class of the audit report is not found in the classpath.
     * Return null if there is not any audit report found
     */
    String[] getOldAuditReports(final long timestamp);

    /**
     * Get all audit reports after a specific date. If an audit report
     * has been created at the specified date, it is not included
     * in the result. Audit reports can be decode with the ObjectEncoder.
     * @param year The year of the date
     * @param month The month of the date
     * @param day The day of the date
     * @return An array of encoded audit reports. The array can contains 'null' if the
     * class of the audit report is not found in the classpath.
     * Return null if there is not any audit report found
     */
    String[] getRecentAuditReports(final int year, final int month, final int day);

    /**
     * Get all audit reports after a specific hour. If an audit
     * report has been created at the specified hour, it is not included
     * in the result. Audit reports can be decode with the ObjectEncoder.
     * @param timestamp The hour
     * @return An array of encoded audit reports. The array can contains 'null'
     * if the class of the audit report is not found in the classpath.
     * Return null if there is not any audit report found
     */
    String[] getRecentAuditReports(final long timestamp);

    /**
     * Start the component.
     * @throws AuditLogServiceException If connection with audit component can not be etablished
     */
    void start() throws AuditLogServiceException;

    /**
     * Stop the component.
     * @throws AuditLogServiceException If connection with audit component can not be stopped
     */
    void stop() throws AuditLogServiceException;

}
