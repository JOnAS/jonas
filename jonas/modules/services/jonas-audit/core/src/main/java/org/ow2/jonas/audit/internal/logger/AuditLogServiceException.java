/**
 * JOnAS
 * Copyright (C) 2010 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.audit.internal.logger;


/**
 * Exeption for component.
 * @author Vincent Michaud
 */
public class AuditLogServiceException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = -1063843459511091097L;

    /**
     * Creates a new instance of <code>AuditLogServiceException</code> without detail message.
     */
    public AuditLogServiceException() {
    }

    /**
     * Constructs an instance of <code>AuditLogServiceException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public AuditLogServiceException(final String msg) {
        super(msg);
    }

    /**
     * Constructs an instance of <code>AuditLogServiceException</code> with the specified detail message.
     * @param msg the detail message.
     * @param e the given exception
     */
    public AuditLogServiceException(final String msg, final Exception e) {
        super(msg, e);
    }

    /**
     * Constructor for the given exception.
     * @param ex the exception
     */
    AuditLogServiceException(final Exception ex) {
        super(ex);
    }
}
