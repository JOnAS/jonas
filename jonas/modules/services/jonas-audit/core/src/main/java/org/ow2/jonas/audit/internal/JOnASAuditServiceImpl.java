/**
 * JOnAS
 * Copyright (C) 2010-2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.audit.internal;

import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.servlet.Filter;

import org.ow2.jonas.audit.AuditService;
import org.ow2.jonas.audit.internal.logger.AuditLogService;
import org.ow2.jonas.audit.internal.logger.AuditLogServiceException;
import org.ow2.jonas.audit.internal.webauditor.filters.HttpAuditFilter;
import org.ow2.jonas.jmx.JmxService;
import org.ow2.jonas.lib.service.AbsServiceImpl;
import org.ow2.jonas.service.ServiceException;
import org.ow2.util.auditreport.api.AuditorJMXObjectNames;
import org.ow2.util.event.api.IEventService;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;


/**
 * JOnAS Audit service.
 * @author Florent Benoit
 */
public class JOnASAuditServiceImpl extends AbsServiceImpl implements AuditService {

    /**
     * Logger.
     */
    private static Log logger = LogFactory.getLog(JOnASAuditServiceImpl.class);

    /**
     * JMX Service.
     */
    private JmxService jmxService = null;

    /**
     * Audit Log component.
     */
    private AuditLogService auditLogService = null;


    /**
     * HTTP filter used to intercept http calls.
     */
    private Filter httpFilter = null;

    /**
     * ObjectName of the http filter.
     */
    private ObjectName httpFilterObjectName = null;

    /**
     * eventService to send events
     */
    private IEventService eventService = null;

    /**
     * Start the Service. Only need to create a RMI connector
     * 
     * @exception ServiceException
     * the service could not be started
     */
    @Override
    public void doStart() throws ServiceException {

        this.auditLogService = new AuditLogService();
        auditLogService.setJmxService(jmxService);

        try {
            auditLogService.start();
        } catch (AuditLogServiceException e) {
            throw new ServiceException("Cannot init audit log component", e);
        }

        // Create the filter
        httpFilter = new HttpAuditFilter(jmxService.getJmxServer(), eventService);

        // Load commons modeler
        jmxService.loadDescriptors(HttpAuditFilter.class.getPackage().getName(),HttpAuditFilter.class.getClassLoader());

        // Register MBean
        try {
            httpFilterObjectName = new ObjectName(jmxService.getDomainName() + AuditorJMXObjectNames.WEBAUDITOR_TYPE_COMPONENT + ",name=JOnAS");
        } catch (MalformedObjectNameException e) {
            throw new ServiceException("Cannot build objectname", e);
        } catch (NullPointerException e) {
            throw new ServiceException("Cannot build objectname", e);
        }

        try {
            jmxService.registerModelMBean(httpFilter, httpFilterObjectName);
        } catch (Exception e) {
            throw new ServiceException("Cannot register MBean", e);
        }

        logger.info("Audit service started.");
    }

    /**
     * @return an HTTP filter that can be used for the audit system.
     */
    public Filter getWebAuditFilter() {
        return httpFilter;
    }

    /**
     * Stop this service.
     */
    @Override
    public void doStop() {

        try {
            jmxService.unregisterModelMBean(httpFilterObjectName);
        } catch (Exception e) {
            logger.error("Cannot unregister Http filter MBean", e);
        }

        try {
            auditLogService.stop();
        } catch (AuditLogServiceException e) {
            logger.error("Cannot stop the audit log component", e);
        }
        logger.info("Audit service stopped.");
    }

    /**
     * @param jmxService
     * the jmxService to set
     */
    public void setJmxService(final JmxService jmxService) {
        this.jmxService = jmxService;
    }

    /**
     * @param eventService
     * the eventService to set
     */
    public void setEventService(final IEventService eventService) {
        this.eventService = eventService;
    }
}