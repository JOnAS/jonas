/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.jaxrs.jersey.internal;

import javax.servlet.ServletContainerInitializer;
import javax.ws.rs.ext.RuntimeDelegate;

import com.sun.jersey.server.impl.container.servlet.JerseyServletContainerInitializer;
import com.sun.jersey.server.impl.provider.RuntimeDelegateImpl;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.ow2.jonas.lib.service.AbsServiceImpl;
import org.ow2.jonas.service.ServiceException;

/**
 * A {@code JerseyComponent} is ...
 *
 * @author Guillaume Sauthier
 */
public class JerseyService extends AbsServiceImpl {

    private BundleContext bundleContext;
    private ServiceRegistration registration;
    private ServiceRegistration registration2;

    public JerseyService(BundleContext bundleContext) {
        this.bundleContext = bundleContext;
    }

    @Override
    protected void doStart() throws ServiceException {

        // Register the ServletContainerInitializer to activate Tomcat7 integration
        registration = bundleContext.registerService(
                ServletContainerInitializer.class.getName(),
                new JerseyServletContainerInitializer(),
                null);

        // Register the RuntimeDelegate service for the API
        registration2 = bundleContext.registerService(
                RuntimeDelegate.class.getName(),
                new RuntimeDelegateImpl(),
                null
        );
    }

    @Override
    protected void doStop() throws ServiceException {

        // Do not forget to un-register services
        registration.unregister();
        registration2.unregister();
    }
}
