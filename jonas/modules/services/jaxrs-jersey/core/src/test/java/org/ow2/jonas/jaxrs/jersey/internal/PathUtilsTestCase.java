/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id: $
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.jaxrs.jersey.internal;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.mockito.Mock;
import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.Version;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;

/**
 * A {@code PathUtilsTestCase} is ...
 *
 * @author Guillaume Sauthier
 */
public class PathUtilsTestCase {

    @Mock
    private ServiceReference reference;

    @Mock
    private Bundle bundle;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
    }

    @Test
    public void testApplicationWithoutContextPathAndNoApplicationPath() throws Exception {

        when(reference.getBundle()).thenReturn(bundle);
        when(bundle.getSymbolicName()).thenReturn("symbolic-name");
        when(bundle.getVersion()).thenReturn(new Version(1, 0, 0));

        assertEquals(PathUtils.getContextPath(new Application(), reference), "/symbolic-name/1.0.0");

    }

    @Test
    public void testApplicationWithoutContextPathAndApplicationPath() throws Exception {

        when(reference.getBundle()).thenReturn(bundle);
        when(bundle.getSymbolicName()).thenReturn("symbolic-name");
        when(bundle.getVersion()).thenReturn(new Version(1, 0, 0));

        assertEquals(PathUtils.getContextPath(new ApplicationWithPath(), reference), "/symbolic-name/1.0.0/system");

    }

    @Test
    public void testApplicationWithContextPathAndNoApplicationPath() throws Exception {

        when(reference.getProperty(JerseyApplicationPublisher.CONTEXT_PROPERTY))
                .thenReturn("app");

        assertEquals(PathUtils.getContextPath(new Application(), reference), "/app");

    }

    @Test
    public void testApplicationWithContextPathAndApplicationPath() throws Exception {

        when(reference.getProperty(JerseyApplicationPublisher.CONTEXT_PROPERTY))
                .thenReturn("app");

        assertEquals(PathUtils.getContextPath(new ApplicationWithPath(), reference), "/app/system");
    }

    @Test
    public void testCleaningPathWithNoLeadingSlash() throws Exception {

        when(reference.getProperty(JerseyApplicationPublisher.CONTEXT_PROPERTY))
                .thenReturn("app");

        assertEquals(PathUtils.getContextPath(new Application(), reference), "/app");

    }

    @Test
    public void testCleaningPathWithTrailingSlash() throws Exception {

        when(reference.getProperty(JerseyApplicationPublisher.CONTEXT_PROPERTY))
                .thenReturn("app/");

        assertEquals(PathUtils.getContextPath(new Application(), reference), "/app");

    }

    @Test
    public void testCleaningPathWithLeadingSlash() throws Exception {

        when(reference.getProperty(JerseyApplicationPublisher.CONTEXT_PROPERTY))
                .thenReturn("/app");

        assertEquals(PathUtils.getContextPath(new Application(), reference), "/app");

    }


    @ApplicationPath("system")
    private static class ApplicationWithPath extends Application {}

}
