/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id: $
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.jaxrs.jersey.internal;

import javax.ws.rs.core.Application;

import org.mockito.Mock;
import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceReference;
import org.osgi.service.http.HttpService;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * A {@code JerseyApplicationPublisherTestCase} is ...
 *
 * @author Guillaume Sauthier
 */
public class JerseyApplicationPublisherTestCase {

    @Mock
    private HttpService httpService;

    @Mock
    private ServiceReference reference1;
    @Mock
    private ServiceReference reference2;
    @Mock
    private ServiceReference reference3;

    @Mock
    private Bundle bundle;
    
    
    private JerseyApplicationPublisher publisher;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);

        publisher = new JerseyApplicationPublisher();
        publisher.setHttpService(httpService);
    }

    @Test
    public void testApplicationDisappearance() throws Exception {

        when(reference1.getProperty(JerseyApplicationPublisher.CONTEXT_PROPERTY))
                .thenReturn("app");

        publisher.bindApplication(new Application(), reference1);
        publisher.unbindApplication(reference1);

        verify(httpService).unregister("/app");

    }

    @Test
    public void testShutdown() throws Exception {

        when(reference1.getProperty(JerseyApplicationPublisher.CONTEXT_PROPERTY))
                .thenReturn("app1");
        when(reference2.getProperty(JerseyApplicationPublisher.CONTEXT_PROPERTY))
                .thenReturn("app2");
        when(reference3.getProperty(JerseyApplicationPublisher.CONTEXT_PROPERTY))
                .thenReturn("app3");

        publisher.bindApplication(new Application(), reference1);
        publisher.bindApplication(new Application(), reference2);
        publisher.bindApplication(new Application(), reference3);

        publisher.stop();

        verify(httpService).unregister("/app1");
        verify(httpService).unregister("/app2");
        verify(httpService).unregister("/app3");


    }

}
