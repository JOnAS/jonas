/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007-2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.extension.loader;

import java.io.File;

/**
 * Represents a jar file in lib/ext that will be converted in bundle.
 * @author Guillaume Sauthier
 */
public class Bundlizable {

    /**
     * The jar in lib/ext.
     */
    private File source;

    /**
     * The generated bundle file.
     */
    private File target;

    /**
     * Set the source jar file.
     * @param source the source jar file
     */
    public void setSource(final File source) {
        this.source = source;
    }

    /**
     * Set the target bundle file.
     * @param target generated bundle file
     */
    public void setTarget(final File target) {
        this.target = target;
    }

    /**
     * @return the source file (may be null).
     */
    public File getSource() {
        return this.source;
    }

    /**
     * @return the target bundle file (may be null).
     */
    public File getTarget() {
        return this.target;
    }

    /**
     * @return <code>true</code> if the target bundle is outdated compared to the source file.
     */
    public boolean isTargetOutdated() {

        // no target, so it's outdated
        if (!target.exists()) {
            return true;
        }

        // target older than source
        if (target.lastModified() > source.lastModified()) {
            return true;
        }

        return false;
    }

    /**
     * @return <code>true</code> is there is no source associated to the target bundle.
     */
    public boolean isSourceSuppressed() {
        return source == null;
    }

    /**
     * @return <code>true</code> if there is no target bundle.
     */
    public boolean isFirstUsage() {
        return target == null;
    }

}
