/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.log.provider.internal;

import org.ow2.jonas.event.provider.api.Event;
import org.ow2.jonas.event.provider.api.IEventProvider;
import org.ow2.jonas.log.provider.api.ILogProvider;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 * JOnAS log handler
 * @author Jeremy Cazaux
 */
public class LogProvider extends Handler implements ILogProvider {

    /**
     * The {@link IEventProvider}
     */
    private IEventProvider eventProvider;

    /**
     * Default constructor
     */
    public LogProvider(final IEventProvider eventProvider) {
        this.eventProvider = eventProvider;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void publish(final LogRecord record) {
        if (record != null) {
            Event event = new Event();
            event.setTimestamp(BigInteger.valueOf(record.getMillis()));
            event.setValue(record.getMessage());
            event.setType(record.getLevel().toString());
            this.eventProvider.addEvent(event);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void flush() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close() throws SecurityException {
    }
}
