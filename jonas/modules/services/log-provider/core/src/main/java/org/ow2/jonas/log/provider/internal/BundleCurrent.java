package org.ow2.jonas.log.provider.internal;

/**
 * Created by IntelliJ IDEA.
 * User: cazauxj
 * Date: 4/10/12
 * Time: 3:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class BundleCurrent {

    /**
     * The {@link InheritableThreadLocal}
     */
    private static InheritableThreadLocal threadLocal;

    /**
     * The bundle id
     */
    private static String bundleId = null;

    /**
     * Unique instance
     */
    private static BundleCurrent current = new BundleCurrent();

    /**
     * Init the thread
     */
    static {
        threadLocal = new InheritableThreadLocal();
        threadLocal.set(bundleId);
    }

    /**
     * {@inheritDoc}
     */
    public static void setBundleId(final String bundleId) {
        BundleCurrent.bundleId = bundleId;
    }

    /**
     * @return the bundle id
     */
    public static String getBundleId() {
        if (bundleId != null) {
            return bundleId;
        } else {
            return String.valueOf(threadLocal.get());
        }
    }

    /**
     * @return the instance
     */
    public static BundleCurrent getCurrent() {
        return current;
    }
}
