/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.log.provider.internal;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.ow2.jonas.event.provider.api.IEventProvider;
import org.ow2.jonas.lib.service.AbsServiceImpl;
import org.ow2.jonas.log.provider.api.ILogProvider;
import org.ow2.jonas.service.ServiceException;

import java.util.logging.Logger;

/**
 * Log provider component
 * @author Jeremy Cazaux
 */
public class LogProviderComponent extends AbsServiceImpl {

    /**
     * The logger
     */
    private Logger logger;

    /**
     * The {@link LogProvider}
     */
    private LogProvider logProvider;

    /**
     * The bundle context
     */
    private BundleContext bundleContext;

    /**
     * The {@link ServiceRegistration}
     */
    private ServiceRegistration serviceRegistration;

    /**
     * The {@link IEventProvider}
     */
    private IEventProvider eventProvider;

    /**
     * Default constructor
     *
     * @param bundleContext The {@link BundleContext}
     */
    public LogProviderComponent(final BundleContext bundleContext) {
        this.bundleContext = bundleContext;
    }

    @Override
    protected void doStart() throws ServiceException {
        //addition of logger handler
        this.logger = Logger.getLogger(getClass().getName()).getParent();
        this.logProvider = new LogProvider(this.eventProvider);
        this.logger.addHandler(this.logProvider);
        this.serviceRegistration = this.bundleContext.registerService(ILogProvider.class.getName(), this.logProvider, null);

    }

    @Override
    protected void doStop() throws ServiceException {
        this.logger.removeHandler(this.logProvider);
        this.serviceRegistration.unregister();
    }

    /**
     * @param eventProvider The {@link IEventProvider} to bind
     */
    public void bindEventProvider(final IEventProvider eventProvider) {
        this.eventProvider = eventProvider;
    }

    /**
     * Unbind event provider
     */
    public void unbindEventProvider() {
        this.eventProvider = null;
    }
}
