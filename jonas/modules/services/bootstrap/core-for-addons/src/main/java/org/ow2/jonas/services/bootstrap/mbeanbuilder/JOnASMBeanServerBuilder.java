/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.services.bootstrap.mbeanbuilder;

import org.ow2.jonas.jmx.internal.interceptor.InvocationHandlerImpl;
import org.ow2.jonas.jmx.internal.interceptor.MBeanServerDelegateInterceptor;

import javax.management.MBeanServer;
import javax.management.MBeanServerBuilder;
import javax.management.MBeanServerDelegate;
import java.lang.reflect.Proxy;

/**
 * This class represents a builder that creates a proxy of default
 * MBeanServer implementation.
 * @author Mohammed Boukada
 */
public class JOnASMBeanServerBuilder extends MBeanServerBuilder {

    /**
     * Empty constructor
     */
    public JOnASMBeanServerBuilder() {}

    /**
     * Creates a new MBeanServer
     * @param defaultDomain
     * @param initialOuter
     * @param delegate
     * @return proxy of the MBeanServer
     */
    public MBeanServer newMBeanServer(String              defaultDomain,
                                      MBeanServer         initialOuter,
                                      MBeanServerDelegate delegate) {

        // handler for outer proxy
        InvocationHandlerImpl proxyHandler = new InvocationHandlerImpl();

        // Create an empty proxy with no interceptor handler which
        // will delegate invocations to real MBeanServer
        //
        // Outer proxy will be used when a call is done
        // on a MBean which implements MBeanRegistration
        MBeanServer outerProxy = (MBeanServer) Proxy.newProxyInstance(MBeanServer.class.getClassLoader(),
                                                                      new Class<?>[]{MBeanServer.class},
                                                                      proxyHandler);

        // Create real MBeanServer with outerProxy
        MBeanServer origin = super.newMBeanServer(defaultDomain, outerProxy, delegate);

        // Create handler for MBeanServer proxy and add
        // the default interceptor
        InvocationHandlerImpl invocationHandler = new InvocationHandlerImpl(origin);
        invocationHandler.addInterceptor(new MBeanServerDelegateInterceptor(origin));

        // Create the MBeanServer proxy
        MBeanServer proxy = (MBeanServer) Proxy.newProxyInstance(origin.getClass().getClassLoader(),
                                                                 new Class<?>[]{MBeanServer.class},
                                                                 invocationHandler);

        // Set delegate handler when outer MBeanServer is used
        proxyHandler.setDelegate(invocationHandler);

        // return "proxyfied" MBeanServer
        return proxy;
    }

    /**
     * Delegate creating MBeanServerDelegate to MBeanServerBuilder
     * @return
     */
    public MBeanServerDelegate newMBeanServerDelegate() {
        return super.newMBeanServerDelegate();
    }
}
