/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2013 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 *  $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.services.bootstrap.internal.properties;

import java.io.File;

import org.ow2.jonas.lib.bootstrap.JProp;
import org.ow2.jonas.lib.util.ConfigurationConstants;
import org.ow2.jonas.properties.ServerProperties;

/**
 * A {@code UpdatedServerProperties} is a concrete implementation of a {@link org.ow2.jonas.services.bootstrap.internal.properties.ForwardingServerProperties} which provides as delegate, a new
 * {@link JProp} implementation each time the jonas.properties file is updated.
 *
 * @author Loic Albertin
 */
public class UpdatedServerProperties extends ForwardingServerProperties {

    private JProp latestProperties;

    /**
     * Last modification date of the jonas.properties file.
     */
    private long lastModified = -1;

    private File jonasPropertyFile;

    /**
     * Returns JOnAS Server properties. Read jonas.properties file only if modified.
     *
     * @return Returns JOnAS Server properties.
     */
    @Override
    protected ServerProperties delegate() {
        if (jonasPropertyFile == null) {
            File conf = new File(JProp.getJonasBase(), ConfigurationConstants.DEFAULT_CONFIG_DIR);
            jonasPropertyFile = new File(conf, ConfigurationConstants.JONAS_PROPERTIES_PROP);
        }
        long currentLastModified = jonasPropertyFile.lastModified();

        if (currentLastModified != this.lastModified) {
            this.latestProperties = new JProp();
            this.lastModified = currentLastModified;
        }
        return this.latestProperties;
    }
}
