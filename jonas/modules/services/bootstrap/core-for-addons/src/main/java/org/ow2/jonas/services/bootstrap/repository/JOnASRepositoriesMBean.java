/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.services.bootstrap.repository;

import java.util.List;

/**
 * MBean interface for the JOnASRepositories.
 * @author Mickaël LEDUQUE
 * @author Adriana Danes
 */
public interface JOnASRepositoriesMBean {

    /**
     * Returns the number of available repositories.
     * @return the number of repositories.
     */
    public Integer getRepositoriesNumber();

    /**
     * Returns the list of repository ids.
     * @return the list of repository ids.
     */
    public List<String> getRepositoriesDescriptions();

    /**
     * Returns the list of URL repository names.
     * @return the list of repository ids.
     */
    public List<String> getURLRepositories();

    /**
     * Returns the number of deployed repositories deployables.
     * @return the number of deployed repositories deployables.
     */
    public Integer getRepositoryDeployablesCount();
}
