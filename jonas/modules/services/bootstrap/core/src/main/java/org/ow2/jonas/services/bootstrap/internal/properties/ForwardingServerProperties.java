/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2013 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 *  $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.services.bootstrap.internal.properties;

import java.util.Properties;

import org.ow2.jonas.properties.ServerProperties;

/**
 * A {@code ForwardingServerProperties} is an abstract {@link ServerProperties} implementation that delegates all its methods to a delegate
 * provided by the concrete implementation.
 *
 * @author Loic Albertin
 */
public abstract class ForwardingServerProperties implements ServerProperties {

    /**
     * @return The {@link ServerProperties} delegate
     */
    protected abstract ServerProperties delegate();

    public String getValue(String key) {
        return delegate().getValue(key);
    }

    public String getValue(String key, String defaultVal) {
        return delegate().getValue(key, defaultVal);
    }

    public boolean getValueAsBoolean(String key, boolean def) {
        return delegate().getValueAsBoolean(key, def);
    }

    public String[] getValueAsArray(String key) {
        return delegate().getValueAsArray(key);
    }

    public String getDomainName() {
        return delegate().getDomainName();
    }

    public String getServerName() {
        return delegate().getServerName();
    }

    public String getPropFileName() {
        return delegate().getPropFileName();
    }

    public Properties getConfigFileEnv() {
        return delegate().getConfigFileEnv();
    }

    public String getVersionsFile() {
        return delegate().getVersionsFile();
    }

    public boolean isMaster() {
        return delegate().isMaster();
    }

    public boolean isDevelopment() {
        return delegate().isDevelopment();
    }

    public String getWorkDirectory() {
        return delegate().getWorkDirectory();
    }
}
