/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2013 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 *  $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.services.bootstrap.internal.configuration;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Enumeration;

import org.osgi.framework.Constants;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.cm.ConfigurationEvent;
import org.osgi.service.cm.ConfigurationListener;
import org.ow2.jonas.configuration.ConfigurationManager;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

/**
 * A {@code ConfigurationUpdater} is a {@link org.osgi.service.cm.ConfigurationListener} used to listen configuration updates on a JOnAS
 * Service and which ensure that latest configuration parameters from the jonas.properties file are used.
 *
 * @author Loic Albertin
 */
public class ConfigurationUpdater implements ConfigurationListener {

    /**
     * Property name for the service name.
     */
    public static final String JONAS_SERVICE = "jonas.service";

    private static Log logger = LogFactory.getLog(ConfigurationUpdater.class);

    private ConfigurationManager configurationManager;

    private ConfigurationAdmin admin;

    /**
     *
     * @param configurationManager The JOnAS Configuration Manager used to retrieve actually in use properties for a JOnAS Service
     * @param admin Configuration admin used to retrieve currently stored properties for a JOnAS Service
     */
    public ConfigurationUpdater(ConfigurationManager configurationManager, ConfigurationAdmin admin) {
        this.configurationManager = configurationManager;
        this.admin = admin;
    }

    @Override
    public void configurationEvent(ConfigurationEvent event) {
        if (event.getType() == ConfigurationEvent.CM_UPDATED) {
            String pid = event.getPid();
            if (pid != null) {
                try {
                    Configuration configuration = admin.getConfiguration(pid, "?");
                    Dictionary<String, Object> storedProperties = configuration.getProperties();
                    if (storedProperties != null) {
                        String serviceName = (String) storedProperties.get(ConfigurationUpdater.JONAS_SERVICE);
                        if (serviceName != null) {
                            // Get the last service properties
                            Dictionary<String, Object> newProperties = configurationManager.getServiceProperties(serviceName);

                            // If the configuration for the service has changed
                            if (ConfigurationUpdater.isConfigurationModified(storedProperties, newProperties)) {
                                for (Enumeration<String> keys = newProperties.keys(); keys.hasMoreElements();) {
                                    String key = keys.nextElement();
                                    storedProperties.put(key, newProperties.get(key));
                                }
                                configuration.update(storedProperties);
                            }
                        }
                    }
                } catch (IOException e) {
                    logger.error("Unable to read configuration for factory {0}", pid, e);
                }
            }
        }
    }


    /**
     * Get 'true' if the two dictionaries don't contain the same properties.
     *
     * @param storedProperties Stored properties
     * @param newProperties    New properties for JOnAS properties
     *
     * @return A <code>boolean</code> value representing the fact that the two dictionaries contain or not the same properties
     */
    public static boolean isConfigurationModified(final Dictionary<String, Object> storedProperties,
            final Dictionary<String, Object> newProperties) {
        // Remove automatic properties added by the Configuration Admin service
        storedProperties.remove(Constants.SERVICE_PID);
        storedProperties.remove(ConfigurationAdmin.SERVICE_FACTORYPID);
        storedProperties.remove(ConfigurationAdmin.SERVICE_BUNDLELOCATION);

        if (storedProperties.size() < newProperties.size()) {
            return true;
        } else {
            for (Enumeration<String> enProps = storedProperties.keys(); enProps.hasMoreElements();) {
                String key = enProps.nextElement();

                Object newPropertyValue = newProperties.get(key);
                if (newPropertyValue != null && !newPropertyValue.equals(storedProperties.get(key))) {
                    return true;
                }
            }
        }
        return false;
    }
}
