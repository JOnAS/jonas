/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.services.bootstrap.deploymentplan;

import java.util.List;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.properties.ServerProperties;
import org.ow2.jonas.service.ServiceException;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;
import org.ow2.util.plan.deployer.api.IDeploymentPlanDeployer;

/**
 * Implementation of the Deployment plan MBean.
 * @author Mickaël LEDUQUE
 * @author Francois Fornaciari
 */
public class JOnASDeploymentPlan implements JOnASDeploymentPlanMBean {
    /**
     * The logger.
     */
    private Log logger = LogFactory.getLog(JOnASDeploymentPlan.class);

    /**
     * JMX server (MBean server) instance.
     */
    private MBeanServer mbeanServer = null;

    /**
     * ServerProperties service object.
     */
    private ServerProperties serverProperties = null;

    /**
     * The deployment plan deployer.
     */
    private IDeploymentPlanDeployer deploymentPlanDeployer = null;

    /**
     * This callback method is called during the service starting as soon as all
     * service dependencies are resolved.
     */
    public void start() throws Exception {
        try {
            ObjectName objectName = JonasObjectName.deploymentPlan(serverProperties.getDomainName());
            mbeanServer.registerMBean(this, objectName);
        } catch (Exception e) {
            logger.error("MBean registration error, {0}", e);
        }
    }

    /**
     * This callback method is called during the service stopping.
     * The unregistration of one of the required service can trigger the method invocation.
     */
    public void stop() throws ServiceException {
        // MBean unregistration
        if (mbeanServer != null) {
            try {
                ObjectName objectName = JonasObjectName.deploymentPlan(serverProperties.getDomainName());
                mbeanServer.unregisterMBean(objectName);
            } catch (Exception e) {
                logger.error("MBean unregistration error, {0}", e);
            }
        }
    }

    /**
     * Set the MBean Server.
     * @param mbeanServer the new JMX service.
     */
    public void setMBeanServer(final MBeanServer mbeanServer) {
        this.mbeanServer = mbeanServer;
    }

    /**
     * Set the server properties service.
     * @param serverProperties the serverProperties to set
     */
    public void setServerProperties(final ServerProperties serverProperties) {
        this.serverProperties = serverProperties;
    }

    /**
     * Sets the deployment plan deployer.
     * @param deploymentPlanDeployer the new deployment plan deployer.
     */
    public void setDeploymentPlanDeployer(final IDeploymentPlanDeployer deploymentPlanDeployer) {
        this.deploymentPlanDeployer = deploymentPlanDeployer;
    }

    /**
     * {@inheritDoc}
     */
    public Integer getDeploymentPlansCount() {
        return this.deploymentPlanDeployer.getDeploymentPlansCount();
    }

    /**
     * Returns the list of deployed deployment plans.
     * @return the list of deployed deployment plans.
     */
    public String[] getDeploymentPlans() {
        List<String> planPaths = this.deploymentPlanDeployer.getDeploymentPlansUrls();
        String[] result = new String[planPaths.size()];
        int index = 0;
        for (String path : planPaths) {
            result[index++] = path;
        }
        return result;
    }
}
