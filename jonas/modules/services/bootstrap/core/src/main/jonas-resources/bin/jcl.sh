#!/bin/sh
# ---------------------------------------------------------------------------
# JOnAS: Java(TM) Open Application Server
# Copyright (C) 2007 Bull S.A.S.
# Contact: jonas-team@objectweb.org
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
# USA
#
# Initial developer(s) 4.8: Jerome Pioux
#
#
# ---------------------------------------------------------------------------
# $Id$
# ---------------------------------------------------------------------------

# jcl.ksh|bash launcher script
# Jerome

# Recup jcl scripts location
if [ -L $0 ]; then
	JCL_DIR=`dirname $(ls -l "$0" |sed "s/^.*-> //")`
else
	JCL_DIR=`dirname $0`
fi

# Check what shell is available (priority to bash)
if [ -x /bin/bash ]; then
	shell=bash
elif [ -x /bin/ksh ]; then
	shell=ksh
else
	echo "Error: Could not find either /bin/bash or /bin/ksh"
	exit 2
fi

# Setup appropriate script
JCL_SCP=$JCL_DIR/jcl.$shell

# Should never see this if the install did its job
if [ ! -x $JCL_SCP ]; then
	echo "Error: Could not find or execute $JCL_SCP"
	exit 3
fi

# Runit
$JCL_SCP $*
exit 0

