/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ant;

import java.io.File;

/**
* Class only used for compatibility with previous version of JOnAS
* @author Jeremy Cazaux
*/
public class ServerDeployDeprecated extends AbstractAntDeprecated {

    /**
     * The filename of the component to be deployed; optional depending upon the
     * tool and the action.
     * @param source String representing the "source" attribute.
     */
    public void setSource(File source) throws Exception {
        deprecated();
    }

    /**
     * The action to be performed, usually "deploy"; required. Some tools
     * support additional actions, such as "delete", "list", "undeploy",
     * "update"...
     * @param action A String representing the "action" attribute.
     */
    public void setAction(String action) throws Exception {
        deprecated();
    }

    /**
     * Creates a generic deployment tool. <p>Ant calls this method on creation
     * to handle embedded "generic" elements in the ServerDeploy task.
     */
    public Object createGeneric() throws Exception {
        return deprecated();
    }

    /**
     * Creates a WebLogic deployment tool, for deployment to WebLogic servers.
     * <p>Ant calls this method on creation to handle embedded "weblogic"
     * elements in the ServerDeploy task.
     */
    public Object createWeblogic() throws Exception {
        return deprecated();
    }

    /**
     * Creates a JOnAS deployment tool, for deployment to JOnAS servers. <p>Ant
     * calls this method on creation to handle embedded "jonas" elements in the
     * ServerDeploy task.
     */
    public Object createJonas() throws Exception {
        return deprecated();
    }
}
