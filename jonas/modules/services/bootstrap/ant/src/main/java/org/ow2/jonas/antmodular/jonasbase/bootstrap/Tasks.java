/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004-2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.antmodular.jonasbase.bootstrap;

import java.util.ArrayList;
import java.util.List;

import org.apache.tools.ant.Task;

/**
 * @author Florent Benoit
 */
public class Tasks {

    /**
     * List of tasks.
     */
    private List<Task> tasks = null;

    /**
     * Default constructor.
     */
    public Tasks() {
        tasks = new ArrayList<Task>();
    }

    /**
     * Add a task to the list of defined tasks.
     * @param task to add
     */
    public void addTask(final Task task) {
        tasks.add(task);
    }

    /**
     * @return a list of all tasks to do
     */
    public List<Task> getTasks() {
        return tasks;
    }

    /**
     * Add several tasks.
     * @param subTasks some tasks to do
     */
    public void addTasks(final Tasks subTasks) {
        if (subTasks != null) {
            for (Task task : subTasks.getTasks()) {
                addTask(task);
            }
        }
    }

    /**
     * Add several tasks.
     * @param tasks some tasks to do
     */
    public void addTasks(final List<Task> tasks) {
        if (tasks != null) {
            for (Task task : tasks) {
                addTask(task);
            }
        }
    }
 }
