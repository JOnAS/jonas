/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ant;

import java.io.File;

/**
* Class only used for compatibility with previous version of JOnAS
* @author Jeremy Cazaux
*/
public class WsGenTaskDeprecated extends AbstractAntDeprecated {

    /**
     * Set the output Directory.
     * @param d output dir
     */
    public void setDestdir(final File d) throws Exception {
        deprecated();
    }

    /**
     * Set where source jar files are located.
     * @param s sources dir
     */
    public void setSrcdir(final File s) throws Exception {
        deprecated();
    }

    /**
     * Set XML Validation on/off.
     * @param v on/off
     */
    public void setValidation(final boolean v) throws Exception {
        deprecated();
    }

    /**
     * Set the javac command line.
     * @param j javac to use
     */
    public void setJavac(final String j) throws Exception {
        deprecated();
    }

    /**
     * Set Java Compiler Options.
     * @param opts Options
     */
    public void setJavacopts(final String opts) throws Exception {
        deprecated();
    }

    /**
     * Set keep Generated mode to on/off.
     * @param k on/off
     */
    public void setKeepgen(final boolean k) throws Exception {
        deprecated();
    }

    /**
     * Set noConfig mode to on/off.
     * @param c on/off
     */
    public void setNoconfig(final boolean c) throws Exception {
        deprecated();
    }

    /**
     * Set WSDL2Java verbose mode to on/off.
     * @param v on/off
     */
    public void setVerbose(final boolean v) throws Exception {
        deprecated();
    }

    /**
     * Set WSDL2Java debug mode to on/off.
     * @param b on/off
     */
    public void setDebug(final boolean b) throws Exception {
        deprecated();
    }

    /**
     * Set debug mode on/off. Used only by developers that wants to Debug WsGen.
     * @param d boolean
     */
    public void setJvmdebug(final boolean d) throws Exception {
        deprecated();
    }

    /**
     * Set the JONAS_ROOT to use.
     * @param jr JONAS_ROOT
     */
    public void setJonasroot(final File jr) throws Exception {
        deprecated();
    }

    /**
     * Set the JONAS_BASE to use.
     * @param jb JONAS_BASE
     */
    public void setJonasbase(final File jb) throws Exception {
        deprecated();
    }
}
