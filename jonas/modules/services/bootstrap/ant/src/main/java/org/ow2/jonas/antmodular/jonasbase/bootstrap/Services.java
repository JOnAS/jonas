/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004-2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.antmodular.jonasbase.bootstrap;

import java.io.File;

import org.apache.tools.ant.BuildException;

/**
 * Allow to configure the list of services in jonas.properties file.
 * @author Florent Benoit
 */
public class Services extends JTask implements BaseTaskItf {

    /**
     * Info for the logger.
     */
    private static final String INFO = "[JOnAS] ";

    /**
     * Enable the Security manager
     */
    private Boolean securitymanager = Boolean.TRUE;

    /**
     * SecurityManager property
     */
    private static final String SECURITYMANAGER_PROPERTY = "jonas.security.manager";

    /**
     * Services property.
     */
    private static final String SERVICES_PROPERTY = "jonas.services";

    /**
     * List of services names.
     */
    private String serviceNames = null;

    /**
     * Default constructor.
     */
    public Services() {
        super();
    }

    /**
     * Set the names of the services.
     * @param serviceNames list of services
     */
    public void setNames(final String serviceNames) {
        this.serviceNames = serviceNames;
    }
    
    /**
     * @return the names of the services
     */ 
    public String getNames() {
		return this.serviceNames;
	}

    /**
     * Enable or disable SecurityManager
     * @param securitymanager (true or false)
     */
    public void setSecurityManager(final boolean securitymanager) {
        this.securitymanager = Boolean.valueOf(securitymanager);
    }

    /**
     * Check the properties.
     */
    private void checkProperties() {
        if (serviceNames == null) {
            throw new BuildException(INFO + "Property 'services ' is missing.");
        }
    }

    /**
     * Execute this task.
     */
    @Override
    public void execute() {
        checkProperties();

        // Path to JONAS_BASE
        String jBaseConf = getDestDir().getPath() + File.separator + "conf";
        changeValueForKey(INFO, jBaseConf, JOnASBaseTask.JONAS_CONF_FILE, SERVICES_PROPERTY, serviceNames, false);
        changeValueForKey(INFO, jBaseConf, JOnASBaseTask.JONAS_CONF_FILE, SECURITYMANAGER_PROPERTY, securitymanager.toString(), false);
    }

}
