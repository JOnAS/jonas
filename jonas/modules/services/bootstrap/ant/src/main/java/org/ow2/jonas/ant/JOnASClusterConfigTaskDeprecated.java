/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ant;

import java.io.File;

/**
* Class only used for compatibility with previous version of JOnAS
* @author Jeremy Cazaux
*/
public class JOnASClusterConfigTaskDeprecated extends AbstractAntDeprecated {

    /**
     * Set the destination directory prefix
     * @param destDirPrefix the destination directory prefix
     */
    public void setDestDirPrefix(final String destDirPrefix) throws Exception {
        deprecated();
    }

    /**
     * Set the cluster daemon directory
     * @param cdDir the destination directory for the cluster daemon configuration
     */
    public void setCdDir(final String cdDir) throws Exception {
        deprecated();
    }

    /**
     * Set the source directory for the replacement
     * @param jonasRoot the source directory
     */
    public void setJonasRoot(final File jonasRoot) throws Exception {
        deprecated();
    }

    /**
     * Set architecture
     * @param arch the architecture
     */
    public void setArch(final String arch) throws Exception {
        deprecated();
    }

    /**
     * Set the web instances number
     * @param webInstNb number of web instances
     */
    public void setWebInstNb(final int webInstNb) throws Exception {
        deprecated();
    }
    /**
     * Set if this is only an update or a new JONAS_BASE
     * @param onlyUpdate If true update, else create and then update
     */
    public void setUpdate(final boolean onlyUpdate) throws Exception {
        deprecated();
    }

    /**
     *
     * @return
     * @throws Exception
     */
    public Object createClusterDaemon() throws Exception {
     	return deprecated();
    }

}
