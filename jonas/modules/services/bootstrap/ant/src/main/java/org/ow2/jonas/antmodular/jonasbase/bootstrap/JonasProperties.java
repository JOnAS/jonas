/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007-2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

/**
 *
 */
package org.ow2.jonas.antmodular.jonasbase.bootstrap;

import java.io.File;

/**
 * Allow to configure the global properties in jonas.properties file.
 * <code>services</code> <code>jonas.security.manager</code>
 * <code>jonas.security.propagation</code> <code>jonas.csiv2.propagation</code>
 * <code>jonas.transaction.propagation</code> <code>jonas.log.configfile</code>
 * <code>jonas.master</code> <code>jonas.service.ear.genstub</code>
 * <code>jonas.development</code> <code>jonas.service.ejb3.jpa.provider</code>
 * @author coqp
 */
public class JonasProperties extends AbstractJOnASBaseAntTask {

    /**
     * Info for the logger.
     */
    private static final String INFO = "[JOnAS] ";

    /**
     * Services property.
     */
    private static final String SERVICES_PROPERTY = "jonas.services";

    /**
     * SecurityManager property.
     */
    private static final String SECURITYMANAGER_PROPERTY = "jonas.security.manager";

    /**
     * Security Propagation property.
     */
    private static final String SECURITYPROPAGATIONPROPERTY = "jonas.security.propagation";

    /**
     * Csiv2 Propagation property.
     */
    private static final String CSIV2PROPAGATIONPROPERTY = "jonas.csiv2.propagation";

    /**
     * Transaction Propagation property.
     */
    private static final String TRANSACTIONPROPAGATIONPROPERTY = "jonas.transaction.propagation";

    /**
     * JONAS Log Config file property.
     */
    private static final String LOGCONFIGFILEPROPERTY = "jonas.log.configfile";

    /**
     * JONAS work directory property.
     */
    private static final String JONAS_WORKDIRECTORY_PROPERTY = "jonas.workdirectory";

    /**
     * JONAS master property.
     */
    private static final String MASTER_PROPERTY = "jonas.master";

    /**
     * JONAS EAR genClientStub property.
     */
    private static final String EAR_GENCLIENT_STUB = "jonas.service.ear.genstub";

    /**
     * JONAS development property.
     */
    private static final String DEVELOPMENTMODE = "jonas.development";

    /**
     * List of services names.
     */
    private String services = null;

    /**
     * Enable the Security manager.
     */
    private Boolean securitymanager = true;

    /**
     * Enable the Security propagation.
     */
    private Boolean securitypropagation = true;

    /**
     * Enable the Csiv2 propagation.
     */
    private Boolean csiv2propagation = true;

    /**
     * Enable the transaction propagation.
     */
    private Boolean transactionpropagation = true;

    /**
     * Enable the GenClientStub.
     */
    private Boolean genClientStub = false;

    /**
     * JOnAS log config file name.
     */
    private String configfile = "trace";

    /**
     * Enable the development mode.
     */
    private Boolean development = true;

    /**
     * Enable the transaction propagation.
     */
    private Boolean master = false;

    /**
     * Work directory.
     */
    private String workDirectory = "work";

    /**
     * Default constructor.
     */
    public JonasProperties() {
        super();
    }

    /**
     * Set the names of the services.
     * @param services list of services
     */
    public void setServices(final String services) {
        this.services = services;
    }

    /**
     * Enable or disable SecurityManager.
     * @param securitymanager enabled (true or false)
     */
    public void setSecurityManager(final boolean securitymanager) {
        this.securitymanager = securitymanager;
    }

    /**
     * Enable or disable Security propagation.
     * @param securitypropagation enabled (true or false)
     */
    public void setSecurityPropagation(final boolean securitypropagation) {
        this.securitypropagation = securitypropagation;
    }

    /**
     * Enable or disable Csiv2 propagation.
     * @param csiv2propagation enabled (true or false)
     */
    public void setCsiv2Propagation(final boolean csiv2propagation) {
        this.csiv2propagation = csiv2propagation;
    }

    /**
     * Enable or disable Transaction propagation.
     * @param transactionpropagation enabled (true or false)
     */
    public void setTransactionPropagation(final boolean transactionpropagation) {
        this.transactionpropagation = transactionpropagation;
    }

    /**
     * Set the name of the JOnAS log config file.
     * @param configfile log config file name
     */
    public void setConfigfile(final String configfile) {
        this.configfile = configfile;
    }

    /**
     * Enable or disable JOnaS master property.
     * @param master enabled (true or false)
     */
    public void setMaster(final boolean master) {
        this.master = master;
    }

    /**
     * Enable or disable JOnaS development property.
     * @param development enabled (true or false)
     */
    public void setDevelopment(final boolean development) {
        this.development = development;
    }

    /**
     * Enable or disable JOnaS EAR gen Client Stub call.
     * @param genClientStub enabled (true or false)
     */
    public void setEarClientStub(final boolean genClientStub) {
        this.genClientStub = genClientStub;
    }

    /**
     * Sets the work directory.
     * @param workDirectory the working directory
     */
    public void setWorkDirectory(final String workDirectory) {
        this.workDirectory = workDirectory;
    }

    /**
     * Execute this task.
     */
    @Override
    public void execute() {
        super.execute();

        // Path to JONAS_BASE
        String jBaseConf = this.destDir.getPath() + File.separator + "conf";
        log("jBaseConf:"+jBaseConf);
        JTask jtask = new JTask();
        jtask.setDestDir(destDir);

        if (this.services != null && (!this.services.startsWith("${") && !this.services.endsWith("}"))) {
            jtask.changeValueForKey(INFO, jBaseConf, JOnASBaseTask.JONAS_CONF_FILE,
                    SERVICES_PROPERTY, services, false);
        }

        jtask.changeValueForKey(INFO, jBaseConf, JOnASBaseTask.JONAS_CONF_FILE,
                SECURITYMANAGER_PROPERTY, securitymanager.toString(), false);
        jtask.changeValueForKey(INFO, jBaseConf, JOnASBaseTask.JONAS_CONF_FILE,
                SECURITYPROPAGATIONPROPERTY, securitypropagation.toString(), false);
        jtask.changeValueForKey(INFO, jBaseConf, JOnASBaseTask.JONAS_CONF_FILE,
                CSIV2PROPAGATIONPROPERTY, csiv2propagation.toString(), false);
        jtask.changeValueForKey(INFO, jBaseConf, JOnASBaseTask.JONAS_CONF_FILE,
                TRANSACTIONPROPAGATIONPROPERTY, transactionpropagation.toString(), false);
        jtask.changeValueForKey(INFO, jBaseConf, JOnASBaseTask.JONAS_CONF_FILE,
                LOGCONFIGFILEPROPERTY, configfile, false);
        jtask.changeValueForKey(INFO, jBaseConf, JOnASBaseTask.JONAS_CONF_FILE,
                MASTER_PROPERTY, master.toString(), false);
        jtask.changeValueForKey(INFO, jBaseConf, JOnASBaseTask.JONAS_CONF_FILE,
                EAR_GENCLIENT_STUB, genClientStub.toString(), false);
        jtask.changeValueForKey(INFO, jBaseConf, JOnASBaseTask.JONAS_CONF_FILE,
                DEVELOPMENTMODE, development.toString(), false);
        jtask.changeValueForKey(INFO, jBaseConf, JOnASBaseTask.JONAS_CONF_FILE,
                JONAS_WORKDIRECTORY_PROPERTY, workDirectory, false);

        tasks.add(jtask);
        executeAllTask();
    }
}
