/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ant;

import java.io.File;

/**
* Class only used for compatibility with previous version of JOnAS
* @author Jeremy Cazaux
*/
public class JOnASBaseTaskDeprecated extends AbstractAntDeprecated {

    /**
     * Set the destination directory for the replacement.
     * @param destDir the destination directory
     */
    public void setDestDir(final File destDir) throws Exception {
    	deprecated();
    }

    /**
     * Set the source directory for the replacement.
     * @param jonasRoot the source directory
     */
    public void setJonasRoot(final File jonasRoot) throws Exception{
    	deprecated();
    }

    /**
     * Set if this is only an update or a new JONAS_BASE.
     * @param onlyUpdate If true update, else create and then update
     */
    public void setUpdate(final boolean onlyUpdate) throws Exception{
    	deprecated();
    }

    /**
     * task for jonas.properties global properties configuration.
     */
    public Object createJonasProperties() throws Exception {
     	return deprecated();
    }

    /**
     * tasks for JMS configuration.
     * @throws Exception
     */
    public Object createJms() throws Exception {
    	return deprecated();
    }

    /**
     * Add tasks for Carol configuration.
     */
    public Object createCarol() throws Exception {
        return deprecated();
    }

    /**
     * Add tasks for the web container configuration.
     */
    public Object createWebcontainer() throws Exception {
        return deprecated();
    }

    /**
     * Add task for the DB service.
     */
    public Object createDb() throws Exception {
        return deprecated();
    }

    /**
     * Add tasks for Discovery configuration.
     */
    public Object createDiscovery() throws Exception {
        return deprecated();
    }

    /**
     * Add task for Resource adaptor.
     */
    public Object createJdbcXml() throws Exception {
        return deprecated();
    }

    /**
     * Add task for Resource adaptor.
     */
    public Object createJdbcRa() throws Exception {
        return deprecated();
    }

    /**
     * Add task for Resource adaptor.
     */
    public Object createMail() throws Exception {
        return deprecated();
    }

     /**
     * Add task for the DBM service.
     */
    public Object createDbm() throws Exception {
        return deprecated();
    }

    /**
     * Add task for WSDL.
     */
    public Object createWsdlPublish() throws Exception {
        return deprecated();
    }

    /**
     * Add task for library to put in JONAS_BASE/lib/ext.
     */
    public Object createLib() throws Exception {
        return deprecated();
    }
}
