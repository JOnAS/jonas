/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ant;

import org.apache.tools.ant.Task;
import org.apache.tools.ant.taskdefs.Taskdef;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

/**
 * Abstract class for deprecated Ant class
 * @author Jeremy Cazaux
 */
public class AbstractAntDeprecated extends Task {

    /*
     * Logger
     */
    protected Log logger = LogFactory.getLog(this.getClass());

    /**
     * Throw an exception
     * @throws Exception
     */
    protected Object deprecated() throws Exception {
    	throw new Exception("In order to improve assembly modularity, ant tasks are now fully modular. As a consequence, " +
                "taskdef jonasbase is deprecated. There are new ant tasks in $JONAS_ROOT/lib/common/ow_jonas_ant.jar." +
                "If you still want to use this deprecated task, you need to use lib/common/ow_jonas_ant_deprecated.jar file.");
    }

}
