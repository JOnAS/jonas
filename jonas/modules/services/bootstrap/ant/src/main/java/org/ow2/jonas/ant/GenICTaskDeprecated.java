/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ant;

/**
* Class only used for compatibility with previous version of JOnAS
* @author Jeremy Cazaux
*/
public class GenICTaskDeprecated extends AbstractAntDeprecated {

    /**
     * Set additional arguments for GenIC command line.
     * @param added additional args
     */
    public void setAdditionalargs(final String added) throws Exception {
        deprecated();
    }

    /**
     * Set verbose mode on/off.
     * @param v boolean
     */
    public void setVerbose(final boolean v) throws Exception {
        deprecated();
    }

    /**
     * Set debug mode on/off. Used only by developers that wants to Debug GenIC
     * @param d boolean
     */
    public void setJvmdebug(final boolean d) throws Exception {
        deprecated();
    }

    /**
     * Use InvokeCmd option on/off.
     * @param inv boolean
     */
    public void setInvokecmd(final boolean inv) throws Exception {
        deprecated();
    }

    /**
     * Do not compile generated java files.
     * @param noc on/off
     */
    public void setNocompil(final boolean noc) throws Exception {
        deprecated();
    }

    /**
     * Set the optios to be passed to the RMI compiler.
     * @param opts list of options
     */
    public void setRmicopts(final String opts) throws Exception {
        deprecated();
    }

    /**
     * Validate XML descriptors.
     * @param v on/off
     */
    public void setValidation(final boolean v) throws Exception {
        deprecated();
    }

    /**
     * Set the javac command line to be used.
     * @param j path to javac executable
     */
    public void setJavac(final String j) throws Exception {
        deprecated();
    }

    /**
     * Set the options to be given to javac.
     * @param opts options
     */
    public void setJavacopts(final String opts) throws Exception {
        deprecated();
    }

    /**
     * Keep already generated files.
     * @param k on/off
     */
    public void setKeepgenerated(final boolean k) throws Exception {
        deprecated();
    }

    /**
     * Specifies which RMIC compiler to use: the built-in fast one or the
     * slower external one.
     * @param value if true, use the external RMIC compiler
     */
    public void setNoFastRMIC(final boolean value) throws Exception {
        deprecated();
    }

    /**
     * Set the set of protocols for the generation.
     * @param p protocol list (comma separated)
     */
    public void setProtocols(final String p) throws Exception {
        deprecated();
    }
}
