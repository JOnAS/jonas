/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004-2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.antmodular.jonasbase.bootstrap;

import java.io.File;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.Copy;
import org.apache.tools.ant.taskdefs.Delete;
import org.apache.tools.ant.types.FileSet;
import org.ow2.jonas.antmodular.bootstrap.JOnASAntTool;

/**
 * Class used to create a JOnAS base with different configuration like port, url
 * for JNDI, etc.
 * @author Florent Benoit
 */
public class JOnASBaseTask extends AbstractJOnASBaseAntTask {

    /**
     * Name of JOnAS configuration file.
     */
    public static final String JONAS_CONF_FILE = "jonas.properties";

    /**
     * Name of domain management file.
     */
    public static final String DOMAIN_CONF_FILE = "domain.xml";

    /**
     * Name of logs directory.
     */
    public static final String LOG_DIR_NAME = "logs";

    /**
     * Name of deploy directory.
     */
    public static final String DEPLOY_DIR_NAME = "deploy";

    /**
     * Name of lib/ext directory.
     */
    public static final String LIB_EXT_DIR_NAME = "lib/ext";

    /**
     * Update only JONAS_BASE without erasing it.
     */
    protected boolean onlyUpdate = false;

    /**
     * Package of mandatory deployables
     */
    public static final String OPTIONAL_DEPLOYABLE_PACKAGE = "templates/bootstrap/deploy/optional";

    /**
     * Constructor.
     */
    public JOnASBaseTask() {
        super();
        //tasks = new ArrayList();
    }

    /**
     * Run this task.
     * @see org.apache.tools.ant.Task#execute()
     */
    @Override
    public void execute() {
        super.execute();

        File jprops = new File(destDir.getPath() + File.separator + "conf" + File.separator + "jonas.properties");

        if (onlyUpdate) {
            if (!jprops.exists()) {
                throw new BuildException("JOnAS base directory '" + destDir.getPath() + "' doesn't exists. Cannot update.");
            }
        }

        if (!onlyUpdate) {
            //First, delete previous JONAS_BASE directory (if exist)
            if (this.destDir.exists()) {
                Delete delete = new Delete();
                delete.setDir(this.destDir);
                delete.execute();
            }

            // Then, create JOnAS base
            log("Creating JONAS_BASE in the directory '" + destDir + "' from source directory '" + jonasRoot + "'",
                    Project.MSG_INFO);
            Copy copy = new Copy();
            JOnASAntTool.configure(this, copy);
            copy.setTodir(destDir);
            FileSet confFileSet = new FileSet();
            confFileSet.setDir(new File(new File(jonasRoot, "templates"), "conf"));
            copy.addFileset(confFileSet);

            //keep support to the deprecated templates deploy directory
            File deployablesTemplateDirectory = new File(new File(jonasRoot, "templates"), "deploy");
            if (deployablesTemplateDirectory.exists()) {
                FileSet deployFileSet = new FileSet();
                deployFileSet.setDir(deployablesTemplateDirectory);
                copy.addFileset(deployFileSet);
            }

            copy.setOverwrite(true);
            copy.execute();

            log("Creating logs in the directory '" + destDir, Project.MSG_INFO);
            JMkdir mkdir = new JMkdir();
            JOnASAntTool.configure(this, mkdir);
            mkdir.setDestDir(new File(destDir + File.separator + LOG_DIR_NAME));
            mkdir.execute();

            log("Creating lib/ext in the directory '" + destDir, Project.MSG_INFO);
            mkdir = new JMkdir();
            JOnASAntTool.configure(this, mkdir);
            mkdir.setDestDir(new File(destDir + File.separator + LIB_EXT_DIR_NAME));
            mkdir.execute();

            //create deploy directory if !exist
            File deployDir = new File(new File(getJOnASBase()), "deploy");
            if (!deployDir.exists()) {
                deployDir.mkdir();
            }

            //copy bootstrap deployables templates
            if (!this.skipOptionalDeployablesCopy) {
                copyTemplateDeployables(OPTIONAL_DEPLOYABLE_PACKAGE);
            }
        } else {
            log("Only updating JONAS_BASE in the directory '" + destDir + "' from source directory '" + jonasRoot + "'",
                    Project.MSG_INFO);
        }

        super.executeAllTask();

        // Then update JonasBase
        JOnASAntTool.updateJonasBase(this, jonasRoot, destDir, null);
    }

    /**
     * Add tasks for services (wrapped to default method).
     * @param servicesTasks tasks to do on files
     */
    public void addConfiguredServices(final Services servicesTasks) {
        addTask(servicesTasks);
    }

    /**
     * Set if this is only an update or a new JONAS_BASE.
     * @param onlyUpdate If true update, else create and then update
     */
    public void setUpdate(final boolean onlyUpdate) {
        this.onlyUpdate = onlyUpdate;
    }
}
