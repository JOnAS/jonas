/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004-2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.antmodular.bootstrap;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.taskdefs.Property;

/**
 * Allow to define property with the value of my.${name}.property.
 * @author Florent Benoit
 */
public class JProperty extends Property {

    /**
     * Internal value (to evaluate).
     */
    private String internalValue = null;

    /**
     * Default value if the key is not found.
     */
    private String defaultValue = null;

    /**
     * The value of the property to set.
     * @param value value to set
     */
    public void setValue(final String value) {
        // Called with Ant <= 1.7.x
        this.internalValue = value;
    }

    /**
     * The value of the property to set.
     * @param value value to set
     * @since Ant 1.8.0
     */
    public void setValue(final Object value) {
        // Called with Ant >= 1.8.x
        if (value != null) {
            this.internalValue = value.toString();
        }
        // Adhoc fix, this will probably break something if execute() is called twice
        super.setValue(value);
    }

    /**
     * The default value if the property cannot be found.
     * @param defaultValue value to set
     */
    public void setDefaultValue(final String defaultValue) {
        this.defaultValue = defaultValue;
    }

    /**
     * Execute the task. It sets the value by evaluating variable name.
     * @throws BuildException if value is not set
     * @see org.apache.tools.ant.Task#execute()
     */
    public void execute() throws BuildException {

        if (internalValue == null) {
            throw new BuildException("The property '" + getName()
                    + "' was not set.");
        }

        String valueEvaluated = getProject().getProperty(internalValue);

        // set default value
        if (valueEvaluated == null && defaultValue != null) {
            valueEvaluated = defaultValue;
        }

        // No value ? error
        if (valueEvaluated == null) {
            throw new BuildException("The property '" + internalValue
                    + "' cannot be evaluated in the current project.");
        }

        // Set the value with the value evaluated
        super.setValue(valueEvaluated);

        // Now execute the super method
        super.execute();
    }

}
