/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ant;

/**
* Class only used for compatibility with previous version of JOnAS
* @author Jeremy Cazaux
*/
public class JPropertyDeprecated extends AbstractAntDeprecated {

    /**
     * The value of the property to set.
     * @param value value to set
     */
    public void setValue(final String value) throws Exception {
        deprecated();
    }

    /**
     * The name of the property to set
     * @param name name to set
     * @throws Exception
     */
    public void setName(final String name) throws  Exception {
        deprecated();
    }

    /**
     * The default value if the property cannot be found.
     * @param defaultValue value to set
     */
    public void setDefaultValue(final String defaultValue) throws Exception {
        deprecated();
    }
}
