/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ant;

import org.apache.tools.ant.taskdefs.optional.ejb.EJBDeploymentTool;
import org.apache.tools.ant.taskdefs.optional.ejb.EjbJar;
import org.apache.tools.ant.types.Path;

import java.io.File;

/**
* Class only used for compatibility with previous version of JOnAS
* @author Jeremy Cazaux
*/
public class EjbJarDeprecated extends AbstractAntDeprecated {

    /**
     * Set the Manifest file to use when jarring. As of EJB 1.1, manifest
     * files are no longer used to configure the EJB. However, they still
     * have a vital importance if the EJB is intended to be packaged in an
     * EAR file. By adding "Class-Path" settings to a Manifest file, the EJB
     * can look for classes inside the EAR file itself, allowing for easier
     * deployment. This is outlined in the J2EE specification, and all J2EE
     * components are meant to support it.
     *
     * @param manifest the manifest to be used in the EJB jar
     */
     public void setManifest(File manifest) throws Exception {
         deprecated();
     }

    /**
     * Sets the source directory, which is the directory that
     * contains the classes that will be added to the EJB jar. Typically
     * this will include the home and remote interfaces and the bean class.
     *
     * @param inDir the source directory.
     */
    public void setSrcdir(File inDir) throws Exception {
        deprecated();
    }

    /**
     * Set the descriptor directory. The descriptor directory contains the
     * EJB deployment descriptors. These are XML files that declare the
     * properties of a bean in a particular deployment scenario. Such
     * properties include, for example, the transactional nature of the bean
     * and the security access control to the bean's methods.
     *
     * @param inDir the directory containing the deployment descriptors.
     */
    public void setDescriptordir(File inDir) throws Exception {
        deprecated();
    }

    /**
     * Set the analyzer to use when adding in dependencies to the JAR.
     *
     * @param analyzer the name of the dependency analyzer or a class.
     */
    public void setDependency(String analyzer) throws Exception {
        deprecated();
    }

    /**
     * Set the base name of the EJB JAR that is to be created if it is not
     * to be determined from the name of the deployment descriptor files.
     *
     * @param inValue the basename that will be used when writing the jar
     *      file containing the EJB
     */
    public void setBasejarname(String inValue) throws Exception {
        deprecated();
    }

    /**
     * Set the naming scheme used to determine the name of the generated jars
     * from the deployment descriptor
     *
     * @param namingScheme the naming scheme to be used
     */
    public void setNaming(EjbJar.NamingScheme namingScheme) throws Exception {
        deprecated();
    }

        /**
     * Set the destination directory. The EJB jar files will be written into
     * this directory. The jar files that exist in this directory are also
     * used when determining if the contents of the jar file have changed.
     * Note that this parameter is only used if no deployment tools are
     * specified. Typically each deployment tool will specify its own
     * destination directory.
     *
     * @param inDir the destination directory in which to generate jars
     */
    public void setDestdir(File inDir) throws Exception {
        deprecated();
    }

        /**
     * Sets the CMP version.
     *
     * @param version CMP version.
     * Must be either <code>1.0</code> or <code>2.0</code>.<br/>
     * Default is <code>1.0</code>.<br/>
     * Initially, only the JBoss implementation does something specific for CMP 2.0.<br/>
     * @since ant 1.6
     */
    public void setCmpversion(EjbJar.CMPVersion version) throws Exception {
        deprecated();
    }

    /**
     * Set the classpath to use when resolving classes for inclusion in the jar.
     *
     * @param classpath the classpath to use.
     */
    public void setClasspath(Path classpath) throws Exception {
        deprecated();
    }

    /**
     * Controls whether the
     * destination JARs are written out in the destination directory with
     * the same hierarchical structure from which the deployment descriptors
     * have been read. If this is set to true the generated EJB jars are
     * written into the root of the destination directory, otherwise they
     * are written out in the same relative position as the deployment
     * descriptors in the descriptor directory.
     *
     * @param inValue the new value of the flatdestdir flag.
     */
    public void setFlatdestdir(boolean inValue) throws Exception {
        deprecated();
    }

    /**
     * Set the suffix for the generated jar file. When generic jars are
     * generated, they have a suffix which is appended to the the bean name
     * to create the name of the jar file. Note that this suffix includes
     * the extension fo te jar file and should therefore end with an
     * appropriate extension such as .jar or .ear
     *
     * @param inString the string to use as the suffix.
     */
    public void setGenericjarsuffix(String inString) throws Exception {
        deprecated();
    }

    /**
     * The string which terminates the bean name.
     * The convention used by this task is
     * that bean descriptors are named as the BeanName with some suffix. The
     * baseNameTerminator string separates the bean name and the suffix and
     * is used to determine the bean name.
     *
     * @param inValue a string which marks the end of the basename.
     */
    public void setBasenameterminator(String inValue) throws Exception {
        deprecated();
    }

    /**
     * Adds a deployment tool for JOnAS server.
     *
     * @return the deployment tool instance to be configured.
     */
    public Object createJonas() throws Exception {
        return deprecated();
    }

    /**
     * Adds to the classpath used to locate the super classes and
     * interfaces of the classes that will make up the EJB JAR.
     *
     * @return the path to be configured.
     */
    public Object createClasspath() throws Exception {
        return deprecated();
    }

    /**
     * Create a DTD location record. This stores the location of a DTD. The
     * DTD is identified by its public Id. The location may either be a file
     * location or a resource location.
     *
     * @return the DTD location object to be configured by Ant
     */
    public Object createDTD() throws Exception {
        return deprecated();
    }

    /**
     * Adds a fileset for support elements.
     *
     * @return a fileset which can be populated with support files.
     */
    public Object createSupport() throws Exception {
        return deprecated();
    }

    /**
     * Add a deployment tool to the list of deployment tools that will be
     * processed
     *
     * @param deploymentTool a deployment tool instance to which descriptors
     *        will be passed for processing.
     */
    protected Object createDeploymentTool(EJBDeploymentTool deploymentTool) throws Exception {
        return deprecated();
    }

}
