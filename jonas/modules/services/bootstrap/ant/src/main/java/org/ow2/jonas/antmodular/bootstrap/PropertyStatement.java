/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 *
 */
package org.ow2.jonas.antmodular.bootstrap;
/**
 * @author eyindanga
 * Represents a statement for property adding or replacement.
 */
public class PropertyStatement {

    /**
     * Property name.
     */
    String name = null;

    /**
     * replace value.
     */
    String value = null;

    /**
     * <code>true</code>, means add.
     */
    boolean add;
    /**
     * @return the name.
     */
    public String getName() {
        return name;
    }
    /**
     * @param name the name to set.
     */
    public void setName(final String name) {
        this.name = name;
    }
    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }
    /**
     * @param value the value to set.
     */
    public void setValue(final String value) {
        this.value = value;
    }
    /**
     * @return the add.
     */
    public boolean isAdd() {
        return add;
    }
    /**
     * @param add the add to set.
     */
    public void setAdd(final boolean add) {
        this.add = add;
    }
    /**
     * Default constructor.
     */
    public PropertyStatement() {

    }

    /**
     * Default constructor.
     */
    public PropertyStatement(final String name, final String value, final boolean add) {
        this.name = name;
        this.value = value;
        this.add = add;
    }

}
