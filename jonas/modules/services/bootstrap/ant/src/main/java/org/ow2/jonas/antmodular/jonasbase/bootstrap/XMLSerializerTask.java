/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006-2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.antmodular.jonasbase.bootstrap;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.ow2.util.xml.XMLSerializer;
import org.w3c.dom.Document;

import org.apache.tools.ant.BuildException;


/**
 * Allow to serialize a XML DOM structure.
 * @author Philippe Coq
 */
public class XMLSerializerTask extends JTask implements BaseTaskItf {

    /**
     * Info for the logger.
     */
    private static final String INFO = "[XML Serializer] ";

    /**
     * XML document to serialize.
     */
    private Document xmlDoc = null;

    /**
     * XML file name.
     */
    private String fileName = null;

    /**
     * Default constructor.
     */
    public XMLSerializerTask() {
        super();
    }

    /**
     * Set the xml document to serialize.
     * @param xmlDoc xml document
     */
    public void setXmlDoc(final Document xmlDoc) {
            this.xmlDoc = xmlDoc;
    }

    /**
     * Set the xml filename.
     * @param fileName xml filename
     */
    public void setXmlFileName(final String fileName) {
            this.fileName = fileName;
    }

    /**
     * Check the properties.
     */
    private void checkProperties() {
        if (xmlDoc == null) {
            throw new BuildException(INFO + "XML document is missing.");
        }
        if (fileName == null) {
            throw new BuildException(INFO + "XML filename is missing.");
        }

    }

    /**
     * Execute this task.
     */
    public void execute() {
        checkProperties();

        // Path to JONAS_BASE
        String jBaseConf = getDestDir().getPath() + File.separator + "conf";
        String xmlFile = jBaseConf + File.separator + fileName;

        // Serialize the XML document
        FileOutputStream os;
        try {
            os = new FileOutputStream(xmlFile);
        } catch (FileNotFoundException e) {
            throw new BuildException(INFO + "XML filename " + xmlFile + " is not valid", e);
        }
        XMLSerializer xmlSer = new XMLSerializer(xmlDoc);
        try {
            xmlSer.serialize(os);
        } catch (IOException e) {
            throw new BuildException(INFO + "Error during serialization of " + xmlFile, e);
        }

    }
}
