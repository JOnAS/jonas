/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ant;

import org.apache.tools.ant.types.Path;

import java.io.File;

/**
* Class only used for compatibility with previous version of JOnAS
* @author Jeremy Cazaux
*/
public class JOnASTaskDeprecated extends AbstractAntDeprecated {

    /**
     * Sets the mode for the server (start or stop).
     * @param mode The mode to set.
     */
    public void setMode(final String mode) throws Exception {
        deprecated();
    }
        /**
     * Set domainName.
     * @param domainName The domainName to set.
     */
    public void setDomainName(final String domainName) throws Exception {
        deprecated();
    }

        /**
     * Set the destination directory prefix
     * @param destDirPrefix the destination directory prefix
     */
    public void setDestDirPrefix(final String destDirPrefix) throws Exception {
        deprecated();
    }

    /**
     * Set the cluster daemon directory
     * @param cdDir the destination directory for the cluster daemon configuration
     */
    public void setCdDir(final String cdDir) throws Exception {
        deprecated();
    }

    /**
     * Set architecture
     * @param arch the architecture
     */
    public void setArch(final String arch) throws Exception {
        deprecated();
    }


    /**
     * Set the name of the server.
     * @param serverName The serverName to set.
     */
    public void setServerName(final String serverName) throws Exception {
        deprecated();
    }

    /**
     * Set catalina Home.
     * @param catalinaHome The catalinaHome to set.
     * @deprecated
     */
    @Deprecated
    public void setCatalinaHome(final String catalinaHome) throws Exception {
        deprecated();
    }

    /**
     * Set jetty home path.
     * @param jettyHome The jettyHome to set.
     * @deprecated
     */
    @Deprecated
    public void setJettyHome(final String jettyHome) throws Exception {
        deprecated();
    }

    /**
     * Set the JOnAS root directory.
     * @param jonasRoot the JOnAS root directory.
     */
    public void setJonasRoot(final File jonasRoot) throws Exception {
        deprecated();
    }

    /**
     * Set the JOnAS base directory.
     * @param jonasBase the JOnAS base directory.
     */
    public void setJonasBase(final File jonasBase) throws Exception {
        deprecated();
    }

    /**
     * Set the additional args to pass to the JVM.
     * @param jvmOpts the options.
     */
    public void setJvmopts(final String jvmOpts) throws Exception {
        deprecated();
    }

    /**
     * Adds to the classpath the class of the project.
     * @return the path to be configured.
     */
    public Object createClasspath() throws Exception {
        return deprecated();
    }

    /**
     * Set the classpath for this task.
     * @param classpath the classpath to use.
     */
    public void setClasspath(final Path classpath) throws Exception {
        deprecated();
    }
}
