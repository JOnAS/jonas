/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004-2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.antmodular.jonasbase.bootstrap;

import java.io.File;
import java.util.logging.Level;

/**
 * Defines common replacement methods.
 * @author Florent Benoit
 * @author Shenheng Liang
 */
public class JReplace extends Replace implements BaseTaskItf {

    /**
     * configuration file used.
     */
    private String configurationFile = null;

    /**
     * Information for the logger.
     */
    private String logInfo = null;

    /**
     * JONAS_ROOT directory.
     */
    private File jonasRoot = null;

    /**
     * Deployable file used
     */
    private String deployableFile = null;
    

    /**
     * Sets the configuration file.
     * @param configurationFile The configurationFile to set.
     */
    public void setConfigurationFile(final String configurationFile) {
        this.configurationFile = configurationFile;
    }

    /**
     * Sets the deployable file
     * @param deployableFile The deployable file to set
     */
    public void setDeployableFile(final String deployableFile) {
        this.deployableFile = deployableFile;
    }

    /**
     * @param destDir The destDir to set.
     */
    public void setDestDir(final File destDir) {
        if (this.configurationFile != null) {
            setFile(new File(new File(destDir, "conf"), this.configurationFile));
        } else if (this.deployableFile != null) {
            setFile(new File(new File(destDir, "deploy"), this.deployableFile));
        }

    }

    /**
     * @param destFile The destination file to set.
     */
    public void setDestFile(final File destFile) {
        setFile(destFile);
    }

    /**
     * Gets logger info (to be displayed).
     * @return logger info
     * @see org.ow2.jonas.ant.jonasbase.BaseTaskItf#getLogInfo()
     */
    public String getLogInfo() {
        return logInfo;
    }

    /**
     * Set the info to be displayed by the logger.
     * @param logInfo information to be displayed
     * @see org.ow2.jonas.ant.jonasbase.BaseTaskItf#setLogInfo(java.lang.String)
     */
    public void setLogInfo(final String logInfo) {
        this.logInfo = logInfo;
    }

    /**
     * @param jonasRoot The jonasRoot directory.
     */
    public void setJonasRoot(final File jonasRoot) {
        this.jonasRoot = jonasRoot;
    }

    /**
     * @return the jonasRoot.
     */
    protected File getJonasRoot() {
        return jonasRoot;
    }

}
