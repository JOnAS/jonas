/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.antmodular.bootstrap;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.Java;

/**
 * Class used to manage deployment on JOnAS (with ant and ServerDeploy task)
 * @author Florent Benoit
 */
public class JonasHotDeploymentTool extends BootstrapTask implements HotDeploymentTool {

    /**
     * Admin class (JonasAdmin class)
     */
    private static final String ADMIN_CLASS = "org.ow2.jonas.commands.admin.ClientAdmin";

    /**
     * The parent task
     */
    private ServerDeploy task;

    /**
     * All actions supported by JOnAS
     */
    private static final String[] VALID_ACTIONS = {ACTION_DELETE, ACTION_DEPLOY, ACTION_LIST, ACTION_UNDEPLOY,
            ACTION_UPDATE};

    /**
     * Validates the passed in attributes. Subclasses should chain to this
     * super-method to insure validation of boilerplate attributes. <p>Only the
     * "action" attribute is required in the base class. Subclasses should check
     * attributes accordingly.
     * @throws BuildException if the attributes are
     *            invalid or incomplete.
     */
    public void validateAttributes() throws BuildException {
        if (task.getAction() == null) {
            throw new BuildException("The \"action\" attribute must be set");
        }

        if (!isActionValid()) {
            throw new BuildException("Invalid action \"" + task.getAction() + "\" passed");
        }

    }

    /**
     *  Sets the parent task.
     *  @param task a ServerDeploy object representing the parent task.
     *  @ant.attribute ignore="true"
     */
    public void setTask(ServerDeploy task) {
        this.task = task;
    }

    /**
     * Returns the task field, a ServerDeploy object.
     * @return An ServerDeploy representing the parent task.
     */
    protected ServerDeploy getTask() {
        return task;
    }

    /**
     * Determines if the "action" attribute defines a valid action. <p>
     * Subclasses should determine if the action passed in is supported by the
     * vendor's deployment tool. <p>Actions may by "deploy", "delete", etc...
     * It all depends on the tool.
     * @return true if the "action" attribute is valid, false if not.
     */
    protected boolean isActionValid() {

        String action = getTask().getAction();

        for (int i = 0; i < VALID_ACTIONS.length; i++) {
            if (action.equals(VALID_ACTIONS[i])) {
                return true;
            }
        }

        return false;
    }

    /**
     * Perform the actual deployment. It's up to the subclasses to implement the
     * actual behavior.
     * @exception org.apache.tools.ant.BuildException if the attributes are
     *            invalid or incomplete.
     */
    public void deploy() throws BuildException {

        String action = getTask().getAction();
        Java bootstrapTask = getBootstraptask(ADMIN_CLASS);
        String fileName = getTask().getSource().getPath();

        if (action.equals(ACTION_DEPLOY) || action.equals(ACTION_UPDATE)) {
            bootstrapTask.setTaskName("JOnAS/Deploy");
            bootstrapTask.createArg().setValue("-a");
            bootstrapTask.createArg().setValue(fileName);
            bootstrapTask.log("Deploying '" + fileName + "'...", Project.MSG_INFO);
        } else if (action.equals(ACTION_DELETE) || action.equals(ACTION_UNDEPLOY)) {
            bootstrapTask.setTaskName("JOnAS/Undeploy");
            bootstrapTask.createArg().setValue("-r");
            bootstrapTask.createArg().setValue(fileName);
            bootstrapTask.log("Undeploying '" + fileName + "'...", Project.MSG_INFO);
        } else if (action.equals(ACTION_LIST)) {
            bootstrapTask.setTaskName("JOnAS/List");
            bootstrapTask.createArg().setValue("-l");
            getTask().log("Listing beans ...", Project.MSG_INFO);
        } else {
            throw new BuildException("Invalid action \"" + action + "\" passed");
        }

        bootstrapTask.createArg().setValue("-n");
        bootstrapTask.createArg().setValue(getServerName());
        bootstrapTask.executeJava();
    }

}
