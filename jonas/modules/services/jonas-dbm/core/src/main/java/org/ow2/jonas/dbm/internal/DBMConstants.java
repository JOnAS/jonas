/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.dbm.internal;

/**
 * Holds all the constants used in the 'dbm' service.
 * @author Guillaume Sauthier
 */
public interface DBMConstants {

    // The Datasource configuration parameters
    public static final String NAME = "datasource.name"; // JNDI name of the datasource

    public static final String CLASSNAME = "datasource.classname";

    public static final String DEF_CLASSNAME = "no class name";

    public static final String URL = "datasource.url";

    public static final String DEF_URL = "no url";

    public static final String DESCRIPTION = "datasource.description";

    public static final String DEF_DESCRIPTION = "no desc";

    public static final String USERNAME = "datasource.username";

    public static final String DEF_USERNAME = "";

    public static final String PASSWORD = "datasource.password";

    public static final String DEF_PASSWORD = "";

    public static final String ISOLATIONLEVEL = "datasource.isolationlevel";

    public static final String DEF_ISOLATIONLEVEL = "";

    public static final String MAPPERNAME = "datasource.mapper";

    public static final String DEF_MAPPERNAME = "rdb";

    // The ConnectionManager configuration parameters
    public static final String CONNCHECKLEVEL = "jdbc.connchecklevel";

    public static final String DEF_CONNCHECKLEVEL = "1";

    public static final String CONNMAXAGE = "jdbc.connmaxage";

    public static final String DEF_CONNMAXAGE = "1440";

    public static final String MAXOPENTIME = "jdbc.maxopentime";

    public static final String DEF_MAXOPENTIME = "1440";

    public static final String CONNTESTSTMT = "jdbc.connteststmt";

    public static final String DEF_CONNTESTSTMT = "SELECT 1";

    public static final String PSTMTMAX = "jdbc.pstmtmax";

    public static final String DEF_PSTMTMAX = "12";

    public static final String INITCONPOOL = "jdbc.initconpool";

    public static final String DEF_INITCONPOOL = "0";

    public static final String MINCONPOOL = "jdbc.minconpool";

    public static final String DEF_MINCONPOOL = "0";

    public static final String MAXCONPOOL = "jdbc.maxconpool";

    public static final String DEF_MAXCONPOOL = "-1";

    public static final String MAXWAITTIME = "jdbc.maxwaittime";

    public static final String DEF_MAXWAITTIME = "10";

    public static final String MAXWAITERS = "jdbc.maxwaiters";

    public static final String DEF_MAXWAITERS = "1000";

    public static final String SAMPLINGPERIOD = "jdbc.samplingperiod";

    public static final String DEF_SAMPLINGPERIOD = "30";

    public static final String ADJUSTPERIOD = "jdbc.adjustperiod";

    public static final String DEF_ADJUSTPERIOD = "30";

}
