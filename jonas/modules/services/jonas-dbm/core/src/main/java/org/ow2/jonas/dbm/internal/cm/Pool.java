/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.dbm.internal.cm;

import java.sql.SQLException;

public interface Pool {

    public int getPoolMin();
    public void setPoolMin(int min);
    public int getPoolMax();
    public void setPoolMax(int max);
    public int getMaxAge();
    public long getMaxAgeMilli();
    public void setMaxAge(int mn);
    public int getMaxOpenTime();
    public long getMaxOpenTimeMilli();
    public void setMaxOpenTime(int mn);
    public int getMaxWaitTime();
    public void setMaxWaitTime(int sec);
    public int getMaxWaiters();
    public void setMaxWaiters(int nb);
    public int getSamplingPeriod();
    public void setSamplingPeriod(int sec);
    public int getAdjustPeriod();
    public void setAdjustPeriod(int sec);
    public int getCheckLevel();
    public void setCheckLevel(int level);
    public String getTestStatement();
    public void setTestStatement(String s);
    public String checkConnection(String s) throws SQLException;
    public int getBusyMaxRecent();
    public int getBusyMinRecent();
    public int getCurrentWaiters();
    public int getOpenedCount();
    public int getConnectionFailures();
    public int getConnectionLeaks();
    public int getServedOpen() ;
    public int getRejectedFull();
    public int getRejectedTimeout();
    public int getRejectedOther();
    public int getRejectedOpen();
    public int getWaitersHigh();
    public int getWaitersHighRecent();
    public int getWaiterCount() ;
    public long getWaitingTime();
    public long getWaitingHigh();
    public long getWaitingHighRecent();
    public int getCurrentOpened();
    public int getCurrentBusy();
    public int getCurrentInTx();
}