/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.dbm.internal.mbean;

import javax.management.MBeanException;
import javax.management.NotificationEmitter;
import javax.management.NotificationFilter;
import javax.management.NotificationListener;

import org.apache.commons.modeler.BaseModelMBean;

/**
 * Allow creation of a model MBean instance to registered in the MBean server.
 * This class is necessary to support persistent reconfiguration
 * based on JMX notifications.
 * @author Adriana Danes
 */
public class JDBCDataSourceMBean extends BaseModelMBean {

    /**
     * Default constructor
     * @throws MBeanException if super constructor fails
     */
    public JDBCDataSourceMBean() throws MBeanException {
        super();
    }

    /**
     * Delegates the notification support to the wrapped resource.
     * @param pListner Listener to notify
     * @param pFilter Notification filter
     * @param pHandback ??
     * @throws java.lang.IllegalArgumentException if notification is not done
     */
    public void addNotificationListener(NotificationListener pListner, NotificationFilter pFilter,
            java.lang.Object pHandback) throws java.lang.IllegalArgumentException {
        ((NotificationEmitter) (this.resource)).addNotificationListener(pListner, pFilter, pHandback);
    }
}