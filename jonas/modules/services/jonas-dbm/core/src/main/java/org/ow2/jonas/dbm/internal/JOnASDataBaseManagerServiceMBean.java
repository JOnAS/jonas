/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.dbm.internal;

import java.util.List;
import java.util.Properties;

import org.ow2.jonas.service.ServiceException;


/**
 * MBean Interface for DataBase Service Management
 * MBean type: Standard
 * MBean model: Inheritance (JOnASDataBaseManagerService)
 *
 * @author Michel Bruno
 * @author Guillaume Riviere
 *
 * Contributor(s):
 *
 * 03/01/14 Adriana Danes
 *       Change loadDataSource() signature : take additional argument, the datasource name
 *       Additionnal MBean method : getDatasourceName()
 */
public interface JOnASDataBaseManagerServiceMBean {

   /**
     * @return the list of properties files describing datasources found in JONAS_BASE/conf
     */
    public List getDataSourcePropertiesFiles() throws Exception;

    /**
     * @return Integer Total Number of Datasource available in JOnAS
     */
    public Integer getCurrentNumberOfDataSource();

    /**
     * @return Integer Total Number of JDBC connection open
     */
    public Integer getTotalCurrentNumberOfJDBCConnectionOpen();

    /**
     * @return datasource properties from a local file
     */
    public Properties getDataSourcePropertiesFile(String dsFile) throws Exception ;

    /**
     * Load new datasource
     * @param name datasource name
     * @param prop datasource properties
     * @param loadFromFile true if the datasource is loaded from a .properties file
     */
    public void loadDataSource(String name, Properties prop, Boolean loadFromFile) throws ServiceException ;

    /**
     * Determine if a datasource with a given name is already loaded in the dbm service
     * @param dsName the name of the datasource to be checked if loaded
     */
    public boolean isLoadedDataSource(String dsName);

    /**
     * unload existing datasource
     */
    public void unloadDataSource(String dsName);

    /**
     * @param jndiName The jndi name of a datasource
     * @return The datasource name
     */
    public String getDatasourceName(String jndiName);
}
