/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2010 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.dbm.internal.cm;

import java.io.InputStream;
import java.io.Reader;
import java.sql.NClob;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.sql.SQLXML;
import java.sql.Types;
import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;

import org.ow2.jonas.lib.util.Log;

import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * Wrapper on a PreparedStatement. This wrapper is used to track close method
 * in order to avoid closing the statement, and putting it instead in a pool.
 * @author durieuxp
 */
public class JStatement implements PreparedStatement {

    private boolean changed = false;
    private boolean opened;
    private boolean closing = false;

    /**
     * Physical PreparedStatement object on which the wrapper is.
     */
    private PreparedStatement ps;

    /**
     * Managed Connection the Statement belongs to
     */
    private JManagedConnection mc;

    private int hashCode;
    private String sql;
    private Logger logger = Log.getLogger(Log.JONAS_DBM_PREFIX + ".ps");

    public JStatement(PreparedStatement ps, JManagedConnection mc, String sql) {
        logger.log(BasicLevel.DEBUG, "constructor");
        this.ps = ps;
        this.mc = mc;
        this.sql = sql;
        hashCode = sql.hashCode();
        opened = true;
    }

    public String getSql() {
        return sql;
    }

    /**
     * @return hashcode of the object
     */
    public int hashCode() {
        return hashCode;
    }

    /**
     * @param stmt given statement for comparing it
     * @return true if given object is equals to this current object
     */
    public boolean equals(Object stmt) {
        if (stmt == null) {
            return false;
        }
        // different hashcode, cannot be equals
        if (this.hashCode != stmt.hashCode()) {
            return false;
        }

        // if got same hashcode, try to see if cast is ok.
        if (!(stmt instanceof JStatement)) {
            logger.log(BasicLevel.WARN, "Bad class:" + stmt);
            return false;
        }
        JStatement psw = (JStatement) stmt;
        if (sql == null && psw.getSql() != null) {
            return false;
        }
        if (sql != null && !sql.equals(psw.getSql())) {
            return false;
        }
        try {
            if (psw.getResultSetType() != getResultSetType()) {
                return false;
            }
            if (psw.getResultSetConcurrency() != getResultSetConcurrency()) {
                return false;
            }
        } catch (SQLException e) {
            logger.log(BasicLevel.WARN, "Cannot compare statements:" + e);
            return false;
        }
        logger.log(BasicLevel.DEBUG, "Found");
        return true;
    }

    /**
     * Force a close on the Prepare Statement.
     * Usually, it's the caller that did not close it explicitly
     * @return true if it was open
     */
    public boolean forceClose() {
        if (opened) {
            logger.log(BasicLevel.WARN, "Statements should be closed explicitly.");
            opened = false;
            return true;
        } else {
            return false;
        }
    }

    public void reuse() throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        ps.clearParameters();
        ps.clearWarnings();
        opened = true;
        if (changed) {
            logger.log(BasicLevel.DEBUG, "changed");
            ps.clearBatch();
            ps.setFetchDirection(ResultSet.FETCH_FORWARD);
            ps.setMaxFieldSize(0);
            ps.setMaxRows(0);
            ps.setQueryTimeout(0);
            changed = false;
        }
    }

    /**
     * Retrieves whether this <code>Statement</code> object has been closed. A <code>Statement</code> is closed if the method
     * close has been called on it, or if it is automatically closed.
     * @return true if this <code>Statement</code> object is closed; false if it is still open
     * @throws SQLException if a database access error occurs
     * @since 1.6
     */
    public boolean isClosed() {
        return ! opened && ! closing;
    }

    /**
     * Physically close this Statement
     * @throws SQLException
     */
    public void forget() {
        logger.log(BasicLevel.DEBUG, "");
        try {
            ps.close();
        } catch (SQLException e) {
            mc.notifyStatementError(this, e);
            logger.log(BasicLevel.ERROR, "Cannot close the PreparedStatement:" + e);
        }
        
    }

    // ------------------------------------------------------------------
    // PreparedStatement implementation.
    // Most of the methods are just redirected on the PreparedStatement.
    // ------------------------------------------------------------------

    public int executeUpdate() throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        return ps.executeUpdate();
    }

    public void addBatch() throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        changed = true;
        ps.addBatch();
    }

    public void clearParameters() throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        ps.clearParameters();
    }

    public boolean execute() throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        return ps.execute();
    }

    public void setByte(int parameterIndex, byte x) throws SQLException {
        ps.setByte(parameterIndex, x);
    }

    public void setDouble(int parameterIndex, double x) throws SQLException {
        ps.setDouble(parameterIndex, x);
    }

    public void setFloat(int parameterIndex, float x) throws SQLException {
        ps.setFloat(parameterIndex, x);
    }

    public void setInt(int parameterIndex, int x) throws SQLException {
        ps.setInt(parameterIndex, x);
    }

    public void setNull(int parameterIndex, int sqlType) throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        ps.setNull(parameterIndex, sqlType);
    }

    public void setLong(int parameterIndex, long x) throws SQLException {
        ps.setLong(parameterIndex, x);
    }

    public void setShort(int parameterIndex, short x) throws SQLException {
        ps.setShort(parameterIndex, x);
    }

    public void setBoolean(int parameterIndex, boolean x) throws SQLException {
        ps.setBoolean(parameterIndex, x);
    }

    public void setBytes(int parameterIndex, byte[] x) throws SQLException {
        ps.setBytes(parameterIndex, x);
    }

    public void setAsciiStream(int parameterIndex, InputStream x, int length) throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        ps.setAsciiStream(parameterIndex, x, length);
    }

    public void setBinaryStream(int parameterIndex, InputStream x, int length) throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        ps.setBinaryStream(parameterIndex, x, length);
    }

    public void setUnicodeStream(int parameterIndex, InputStream x, int length) throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        ps.setUnicodeStream(parameterIndex, x, length);
    }

    public void setCharacterStream(int parameterIndex, Reader reader, int length) throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        ps.setCharacterStream(parameterIndex, reader, length);
    }

    public void setObject(int parameterIndex, Object x) throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        ps.setObject(parameterIndex, x);
    }

    public void setObject(int parameterIndex, Object x, int targetSqlType) throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        ps.setObject(parameterIndex, x, targetSqlType);
    }

    public void setObject(int parameterIndex, Object x, int targetSqlType, int scale) throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        ps.setObject(parameterIndex, x, targetSqlType, scale);
    }

    public void setNull(int paramIndex, int sqlType, String typeName) throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        ps.setNull(paramIndex, sqlType, typeName);
    }

    public void setString(int parameterIndex, String x) throws SQLException {
        ps.setString(parameterIndex, x);
    }

    public void setBigDecimal(int parameterIndex, BigDecimal x) throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        ps.setBigDecimal(parameterIndex, x);
    }

    public void setURL(int parameterIndex, URL x) throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        ps.setURL(parameterIndex, x);
    }

    public void setArray(int i, Array x) throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        ps.setArray(i, x);
    }

    public void setBlob(int i, Blob x) throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        ps.setBlob(i, x);
    }

    public void setClob(int i, Clob x) throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        ps.setClob(i, x);
    }

    public void setDate(int parameterIndex, Date x) throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        ps.setDate(parameterIndex, x);
    }

    public ParameterMetaData getParameterMetaData() throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        return ps.getParameterMetaData();
    }

    public void setRef(int i, Ref x) throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        ps.setRef(i, x);
    }

    public ResultSet executeQuery() throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        return ps.executeQuery();
    }

    public ResultSetMetaData getMetaData() throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        return ps.getMetaData();
    }

    public void setTime(int parameterIndex, Time x) throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        ps.setTime(parameterIndex, x);
    }

    public void setTimestamp(int parameterIndex, Timestamp x) throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        ps.setTimestamp(parameterIndex, x);
    }

    public void setDate(int parameterIndex, Date x, Calendar cal) throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        ps.setDate(parameterIndex, x, cal);
    }

    public void setTime(int parameterIndex, Time x, Calendar cal) throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        ps.setTime(parameterIndex, x, cal);
    }

    public void setTimestamp(int parameterIndex, Timestamp x, Calendar cal) throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        ps.setTimestamp(parameterIndex, x, cal);
    }

    public int getFetchDirection() throws SQLException {
        return ps.getFetchDirection();
    }

    public int getFetchSize() throws SQLException {
        return ps.getFetchSize();
    }

    public int getMaxFieldSize() throws SQLException {
        return ps.getMaxFieldSize();
    }

    public int getMaxRows() throws SQLException {
        return ps.getMaxRows();
    }

    public int getQueryTimeout() throws SQLException {
        return ps.getQueryTimeout();
    }

    public int getResultSetConcurrency() throws SQLException {
        return ps.getResultSetConcurrency();
    }

    public int getResultSetHoldability() throws SQLException {
        return ps.getResultSetHoldability();
    }

    public int getResultSetType() throws SQLException {
        return ps.getResultSetType();
    }

    public int getUpdateCount() throws SQLException {
        return ps.getUpdateCount();
    }

    public void cancel() throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        ps.cancel();
    }

    public void clearBatch() throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        ps.clearBatch();
    }

    public void clearWarnings() throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        ps.clearWarnings();
    }

    public void close() throws SQLException {
        if (! opened) {
            logger.log(BasicLevel.DEBUG, "Statement already closed");
            return;
        }
        logger.log(BasicLevel.DEBUG, "");
        opened = false;
        closing  = true;
        mc.notifyPsClose(this);
        closing = false;
    }

    public boolean getMoreResults() throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        return ps.getMoreResults();
    }

    public int[] executeBatch() throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        return ps.executeBatch();
    }

    public void setFetchDirection(int direction) throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        changed = true;
        ps.setFetchDirection(direction);
    }

    public void setFetchSize(int rows) throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        changed = true;
        ps.setFetchSize(rows);
    }

    public void setMaxFieldSize(int max) throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        changed = true;
        ps.setMaxFieldSize(max);
    }

    public void setMaxRows(int max) throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        changed = true;
        ps.setMaxRows(max);
    }

    public void setQueryTimeout(int seconds) throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        changed = true;
        ps.setQueryTimeout(seconds);
    }

    public boolean getMoreResults(int current) throws SQLException {
        return ps.getMoreResults(current);
    }

    public void setEscapeProcessing(boolean enable) throws SQLException {
        logger.log(BasicLevel.DEBUG, "");
        ps.setEscapeProcessing(enable);
    }

    public int executeUpdate(String sql) throws SQLException {
        logger.log(BasicLevel.DEBUG, sql);
        return ps.executeUpdate(sql);
    }

    public void addBatch(String sql) throws SQLException {
        logger.log(BasicLevel.DEBUG, sql);
        changed = true;
        ps.addBatch(sql);
    }

    public void setCursorName(String name) throws SQLException {
        logger.log(BasicLevel.DEBUG, name);
        ps.setCursorName(name);
    }

    public boolean execute(String sql) throws SQLException {
        logger.log(BasicLevel.DEBUG, sql);
        changed = true;
        return ps.execute(sql);
    }

    public int executeUpdate(String sql, int autoGeneratedKeys) throws SQLException {
        logger.log(BasicLevel.DEBUG, sql);
        changed = true;
        return ps.executeUpdate(sql, autoGeneratedKeys);
    }

    public boolean execute(String sql, int autoGeneratedKeys) throws SQLException {
        logger.log(BasicLevel.DEBUG, sql);
        changed = true;
        return ps.execute(sql, autoGeneratedKeys);
    }

    public int executeUpdate(String sql, int[] columnIndexes) throws SQLException {
        logger.log(BasicLevel.DEBUG, sql);
        changed = true;
        return ps.executeUpdate(sql, columnIndexes);
    }

    public boolean execute(String sql, int[] columnIndexes) throws SQLException {
        logger.log(BasicLevel.DEBUG, sql);
        changed = true;
        return ps.execute(sql, columnIndexes);
    }

    public Connection getConnection() throws SQLException {
        return ps.getConnection();
    }

    public ResultSet getGeneratedKeys() throws SQLException {
        return ps.getGeneratedKeys();
    }

    public ResultSet getResultSet() throws SQLException {
        return ps.getResultSet();
    }

    public SQLWarning getWarnings() throws SQLException {
        return ps.getWarnings();
    }

    public int executeUpdate(String sql, String[] columnNames) throws SQLException {
        logger.log(BasicLevel.DEBUG, sql);
        return ps.executeUpdate(sql, columnNames);
    }

    public boolean execute(String sql, String[] columnNames) throws SQLException {
        logger.log(BasicLevel.DEBUG, sql);
        return ps.execute(sql, columnNames);
    }

    public ResultSet executeQuery(String sql) throws SQLException {
        logger.log(BasicLevel.DEBUG, sql);
        return ps.executeQuery(sql);
    }

    /**
     * Sets the designated parameter to the given <code>java.sql.RowId</code> object. The driver converts this to a SQL
     * <code>ROWID</code> value when it sends it to the database
     * @param parameterIndex the first parameter is 1, the second is 2, ...
     * @param x the parameter value
     * @throws SQLException if parameterIndex does not correspond to a parameter marker in the SQL statement; if a database access
     * error occurs or this method is called on a closed <code>PreparedStatement</code>
     * @throws SQLFeatureNotSupportedException if the JDBC driver does not support this method
     * @since 1.6
     */
    public void setRowId(int parameterIndex, RowId x) throws SQLException {
        ps.setRowId(parameterIndex, x);
    }

    /**
     * Sets the designated paramter to the given <code>String</code> object. The driver converts this to a SQL <code>NCHAR</code>
     * or <code>NVARCHAR</code> or <code>LONGNVARCHAR</code> value (depending on the argument's size relative to the driver's
     * limits on <code>NVARCHAR</code> values) when it sends it to the database.
     * @param parameterIndex of the first parameter is 1, the second is 2, ...
     * @param value the parameter value
     * @throws SQLException if parameterIndex does not correspond to a parameter marker in the SQL statement; if the driver does
     * not support national character sets; if the driver can detect that a data conversion error could occur; if a database
     * access error occurs; or this method is called on a closed <code>PreparedStatement</code>
     * @throws SQLFeatureNotSupportedException if the JDBC driver does not support this method
     * @since 1.6
     */
    public void setNString(int parameterIndex, String value) throws SQLException {
        ps.setNString(parameterIndex, value);
    }

    /**
     * Sets the designated parameter to a <code>Reader</code> object. The <code>Reader</code> reads the data till end-of-file is
     * reached. The driver does the necessary conversion from Java character format to the national character set in the database.
     * @param parameterIndex of the first parameter is 1, the second is 2, ...
     * @param value the parameter value
     * @param length the number of characters in the parameter data.
     * @throws SQLException if parameterIndex does not correspond to a parameter marker in the SQL statement; if the driver does
     * not support national character sets; if the driver can detect that a data conversion error could occur; if a database
     * access error occurs; or this method is called on a closed <code>PreparedStatement</code>
     * @throws SQLFeatureNotSupportedException if the JDBC driver does not support this method
     * @since 1.6
     */
    public void setNCharacterStream(int parameterIndex, Reader value, long length) throws SQLException {
        ps.setNCharacterStream(parameterIndex, value, length);
    }

    /**
     * Sets the designated parameter to a <code>java.sql.NClob</code> object. The driver converts this to a SQL <code>NCLOB</code>
     * value when it sends it to the database.
     * @param parameterIndex of the first parameter is 1, the second is 2, ...
     * @param value the parameter value
     * @throws SQLException if parameterIndex does not correspond to a parameter marker in the SQL statement; if the driver does
     * not support national character sets; if the driver can detect that a data conversion error could occur; if a database
     * access error occurs; or this method is called on a closed <code>PreparedStatement</code>
     * @throws SQLFeatureNotSupportedException if the JDBC driver does not support this method
     * @since 1.6
     */
    public void setNClob(int parameterIndex, NClob value) throws SQLException {
        ps.setNClob(parameterIndex, value);
    }

    /**
     * Sets the designated parameter to a <code>Reader</code> object. The reader must contain the number of characters specified
     * by length otherwise a <code>SQLException</code> will be generated when the <code>PreparedStatement</code> is executed. This
     * method differs from the <code>setCharacterStream (int, Reader, int)</code> method because it informs the driver that the
     * parameter value should be sent to the server as a <code>CLOB</code>. When the <code>setCharacterStream</code> method is
     * used, the driver may have to do extra work to determine whether the parameter data should be sent to the server as a
     * <code>LONGVARCHAR</code> or a <code>CLOB</code>
     * @param parameterIndex index of the first parameter is 1, the second is 2, ...
     * @param reader An object that contains the data to set the parameter value to.
     * @param length the number of characters in the parameter data.
     * @throws SQLException if parameterIndex does not correspond to a parameter marker in the SQL statement; if a database access
     * error occurs; this method is called on a closed <code>PreparedStatement</code> or if the length specified is less than
     * zero.
     * @throws SQLFeatureNotSupportedException if the JDBC driver does not support this method
     * @since 1.6
     */
    public void setClob(int parameterIndex, Reader reader, long length) throws SQLException {
        ps.setClob(parameterIndex, reader, length);
    }

    /**
     * Sets the designated parameter to a <code>InputStream</code> object. The inputstream must contain the number of characters
     * specified by length otherwise a <code>SQLException</code> will be generated when the <code>PreparedStatement</code> is
     * executed. This method differs from the <code>setBinaryStream (int, InputStream, int)</code> method because it informs the
     * driver that the parameter value should be sent to the server as a <code>BLOB</code>. When the <code>setBinaryStream</code>
     * method is used, the driver may have to do extra work to determine whether the parameter data should be sent to the server
     * as a <code>LONGVARBINARY</code> or a <code>BLOB</code>
     * @param parameterIndex index of the first parameter is 1, the second is 2, ...
     * @param inputStream An object that contains the data to set the parameter value to.
     * @param length the number of bytes in the parameter data.
     * @throws SQLException if parameterIndex does not correspond to a parameter marker in the SQL statement; if a database access
     * error occurs; this method is called on a closed <code>PreparedStatement</code>; if the length specified is less than zero
     * or if the number of bytes in the inputstream does not match the specfied length.
     * @throws SQLFeatureNotSupportedException if the JDBC driver does not support this method
     * @since 1.6
     */
    public void setBlob(int parameterIndex, InputStream inputStream, long length) throws SQLException {
        ps.setBlob(parameterIndex, inputStream, length);
    }

    /**
     * Sets the designated parameter to a <code>Reader</code> object. The reader must contain the number of characters specified
     * by length otherwise a <code>SQLException</code> will be generated when the <code>PreparedStatement</code> is executed. This
     * method differs from the <code>setCharacterStream (int, Reader, int)</code> method because it informs the driver that the
     * parameter value should be sent to the server as a <code>NCLOB</code>. When the <code>setCharacterStream</code> method is
     * used, the driver may have to do extra work to determine whether the parameter data should be sent to the server as a
     * <code>LONGNVARCHAR</code> or a <code>NCLOB</code>
     * @param parameterIndex index of the first parameter is 1, the second is 2, ...
     * @param reader An object that contains the data to set the parameter value to.
     * @param length the number of characters in the parameter data.
     * @throws SQLException if parameterIndex does not correspond to a parameter marker in the SQL statement; if the length
     * specified is less than zero; if the driver does not support national character sets; if the driver can detect that a data
     * conversion error could occur; if a database access error occurs or this method is called on a closed
     * <code>PreparedStatement</code>
     * @throws SQLFeatureNotSupportedException if the JDBC driver does not support this method
     * @since 1.6
     */
    public void setNClob(int parameterIndex, Reader reader, long length) throws SQLException {
        ps.setNClob(parameterIndex, reader, length);
    }

    /**
     * Sets the designated parameter to the given <code>java.sql.SQLXML</code> object. The driver converts this to an SQL
     * <code>XML</code> value when it sends it to the database.
     * <p>
     * @param parameterIndex index of the first parameter is 1, the second is 2, ...
     * @param xmlObject a <code>SQLXML</code> object that maps an SQL <code>XML</code> value
     * @throws SQLException if parameterIndex does not correspond to a parameter marker in the SQL statement; if a database access
     * error occurs; this method is called on a closed <code>PreparedStatement</code> or the
     * <code>java.xml.transform.Result</code>, <code>Writer</code> or <code>OutputStream</code> has not been closed for the
     * <code>SQLXML</code> object
     * @throws SQLFeatureNotSupportedException if the JDBC driver does not support this method
     * @since 1.6
     */
    public void setSQLXML(int parameterIndex, SQLXML xmlObject) throws SQLException {
        ps.setSQLXML(parameterIndex, xmlObject);
    }



    /**
     * Sets the designated parameter to the given input stream, which will have the specified number of bytes. When a very large
     * ASCII value is input to a <code>LONGVARCHAR</code> parameter, it may be more practical to send it via a
     * <code>java.io.InputStream</code>. Data will be read from the stream as needed until end-of-file is reached. The JDBC driver
     * will do any necessary conversion from ASCII to the database char format.
     * <P>
     * <B>Note:</B> This stream object can either be a standard Java stream object or your own subclass that implements the
     * standard interface.
     * @param parameterIndex the first parameter is 1, the second is 2, ...
     * @param x the Java input stream that contains the ASCII parameter value
     * @param length the number of bytes in the stream
     * @exception SQLException if parameterIndex does not correspond to a parameter marker in the SQL statement; if a database
     * access error occurs or this method is called on a closed <code>PreparedStatement</code>
     * @since 1.6
     */
    public void setAsciiStream(int parameterIndex, java.io.InputStream x, long length) throws SQLException {
        ps.setAsciiStream(parameterIndex, x, length);
    }


    /**
     * Sets the designated parameter to the given input stream, which will have the specified number of bytes. When a very large
     * binary value is input to a <code>LONGVARBINARY</code> parameter, it may be more practical to send it via a
     * <code>java.io.InputStream</code> object. The data will be read from the stream as needed until end-of-file is reached.
     * <P>
     * <B>Note:</B> This stream object can either be a standard Java stream object or your own subclass that implements the
     * standard interface.
     * @param parameterIndex the first parameter is 1, the second is 2, ...
     * @param x the java input stream which contains the binary parameter value
     * @param length the number of bytes in the stream
     * @exception SQLException if parameterIndex does not correspond to a parameter marker in the SQL statement; if a database
     * access error occurs or this method is called on a closed <code>PreparedStatement</code>
     * @since 1.6
     */
    public void setBinaryStream(int parameterIndex, java.io.InputStream x, long length) throws SQLException {
        ps.setBinaryStream(parameterIndex, x, length);
    }

    /**
     * Sets the designated parameter to the given <code>Reader</code> object, which is the given number of characters long. When a
     * very large UNICODE value is input to a <code>LONGVARCHAR</code> parameter, it may be more practical to send it via a
     * <code>java.io.Reader</code> object. The data will be read from the stream as needed until end-of-file is reached. The JDBC
     * driver will do any necessary conversion from UNICODE to the database char format.
     * <P>
     * <B>Note:</B> This stream object can either be a standard Java stream object or your own subclass that implements the
     * standard interface.
     * @param parameterIndex the first parameter is 1, the second is 2, ...
     * @param reader the <code>java.io.Reader</code> object that contains the Unicode data
     * @param length the number of characters in the stream
     * @exception SQLException if parameterIndex does not correspond to a parameter marker in the SQL statement; if a database
     * access error occurs or this method is called on a closed <code>PreparedStatement</code>
     * @since 1.6
     */
    public void setCharacterStream(int parameterIndex, java.io.Reader reader, long length) throws SQLException {
        ps.setCharacterStream(parameterIndex, reader, length);
    }

    // -----
    /**
     * Sets the designated parameter to the given input stream. When a very large ASCII value is input to a
     * <code>LONGVARCHAR</code> parameter, it may be more practical to send it via a <code>java.io.InputStream</code>. Data will
     * be read from the stream as needed until end-of-file is reached. The JDBC driver will do any necessary conversion from ASCII
     * to the database char format.
     * <P>
     * <B>Note:</B> This stream object can either be a standard Java stream object or your own subclass that implements the
     * standard interface.
     * <P>
     * <B>Note:</B> Consult your JDBC driver documentation to determine if it might be more efficient to use a version of
     * <code>setAsciiStream</code> which takes a length parameter.
     * @param parameterIndex the first parameter is 1, the second is 2, ...
     * @param x the Java input stream that contains the ASCII parameter value
     * @exception SQLException if parameterIndex does not correspond to a parameter marker in the SQL statement; if a database
     * access error occurs or this method is called on a closed <code>PreparedStatement</code>
     * @throws SQLFeatureNotSupportedException if the JDBC driver does not support this method
     * @since 1.6
     */
    public void setAsciiStream(int parameterIndex, java.io.InputStream x) throws SQLException {
        ps.setAsciiStream(parameterIndex, x);
    }

    /**
     * Sets the designated parameter to the given input stream. When a very large binary value is input to a
     * <code>LONGVARBINARY</code> parameter, it may be more practical to send it via a <code>java.io.InputStream</code> object.
     * The data will be read from the stream as needed until end-of-file is reached.
     * <P>
     * <B>Note:</B> This stream object can either be a standard Java stream object or your own subclass that implements the
     * standard interface.
     * <P>
     * <B>Note:</B> Consult your JDBC driver documentation to determine if it might be more efficient to use a version of
     * <code>setBinaryStream</code> which takes a length parameter.
     * @param parameterIndex the first parameter is 1, the second is 2, ...
     * @param x the java input stream which contains the binary parameter value
     * @exception SQLException if parameterIndex does not correspond to a parameter marker in the SQL statement; if a database
     * access error occurs or this method is called on a closed <code>PreparedStatement</code>
     * @throws SQLFeatureNotSupportedException if the JDBC driver does not support this method
     * @since 1.6
     */
    public void setBinaryStream(int parameterIndex, java.io.InputStream x) throws SQLException {
        ps.setBinaryStream(parameterIndex, x);
    }

    /**
     * Sets the designated parameter to the given <code>Reader</code> object. When a very large UNICODE value is input to a
     * <code>LONGVARCHAR</code> parameter, it may be more practical to send it via a <code>java.io.Reader</code> object. The data
     * will be read from the stream as needed until end-of-file is reached. The JDBC driver will do any necessary conversion from
     * UNICODE to the database char format.
     * <P>
     * <B>Note:</B> This stream object can either be a standard Java stream object or your own subclass that implements the
     * standard interface.
     * <P>
     * <B>Note:</B> Consult your JDBC driver documentation to determine if it might be more efficient to use a version of
     * <code>setCharacterStream</code> which takes a length parameter.
     * @param parameterIndex the first parameter is 1, the second is 2, ...
     * @param reader the <code>java.io.Reader</code> object that contains the Unicode data
     * @exception SQLException if parameterIndex does not correspond to a parameter marker in the SQL statement; if a database
     * access error occurs or this method is called on a closed <code>PreparedStatement</code>
     * @throws SQLFeatureNotSupportedException if the JDBC driver does not support this method
     * @since 1.6
     */
    public void setCharacterStream(int parameterIndex, java.io.Reader reader) throws SQLException {
        ps.setCharacterStream(parameterIndex, reader);
    }

    /**
     * Sets the designated parameter to a <code>Reader</code> object. The <code>Reader</code> reads the data till end-of-file is
     * reached. The driver does the necessary conversion from Java character format to the national character set in the database.
     * <P>
     * <B>Note:</B> This stream object can either be a standard Java stream object or your own subclass that implements the
     * standard interface.
     * <P>
     * <B>Note:</B> Consult your JDBC driver documentation to determine if it might be more efficient to use a version of
     * <code>setNCharacterStream</code> which takes a length parameter.
     * @param parameterIndex of the first parameter is 1, the second is 2, ...
     * @param value the parameter value
     * @throws SQLException if parameterIndex does not correspond to a parameter marker in the SQL statement; if the driver does
     * not support national character sets; if the driver can detect that a data conversion error could occur; if a database
     * access error occurs; or this method is called on a closed <code>PreparedStatement</code>
     * @throws SQLFeatureNotSupportedException if the JDBC driver does not support this method
     * @since 1.6
     */
    public void setNCharacterStream(int parameterIndex, Reader value) throws SQLException {
        ps.setNCharacterStream(parameterIndex, value);
    }

    /**
     * Sets the designated parameter to a <code>Reader</code> object. This method differs from the
     * <code>setCharacterStream (int, Reader)</code> method because it informs the driver that the parameter value should be sent
     * to the server as a <code>CLOB</code>. When the <code>setCharacterStream</code> method is used, the driver may have to do
     * extra work to determine whether the parameter data should be sent to the server as a <code>LONGVARCHAR</code> or a
     * <code>CLOB</code>
     * <P>
     * <B>Note:</B> Consult your JDBC driver documentation to determine if it might be more efficient to use a version of
     * <code>setClob</code> which takes a length parameter.
     * @param parameterIndex index of the first parameter is 1, the second is 2, ...
     * @param reader An object that contains the data to set the parameter value to.
     * @throws SQLException if parameterIndex does not correspond to a parameter marker in the SQL statement; if a database access
     * error occurs; this method is called on a closed <code>PreparedStatement</code>or if parameterIndex does not correspond to a
     * parameter marker in the SQL statement
     * @throws SQLFeatureNotSupportedException if the JDBC driver does not support this method
     * @since 1.6
     */
    public void setClob(int parameterIndex, Reader reader) throws SQLException {
        ps.setClob(parameterIndex, reader);
    }

    /**
     * Sets the designated parameter to a <code>InputStream</code> object. This method differs from the
     * <code>setBinaryStream (int, InputStream)</code> method because it informs the driver that the parameter value should be
     * sent to the server as a <code>BLOB</code>. When the <code>setBinaryStream</code> method is used, the driver may have to do
     * extra work to determine whether the parameter data should be sent to the server as a <code>LONGVARBINARY</code> or a
     * <code>BLOB</code>
     * <P>
     * <B>Note:</B> Consult your JDBC driver documentation to determine if it might be more efficient to use a version of
     * <code>setBlob</code> which takes a length parameter.
     * @param parameterIndex index of the first parameter is 1, the second is 2, ...
     * @param inputStream An object that contains the data to set the parameter value to.
     * @throws SQLException if parameterIndex does not correspond to a parameter marker in the SQL statement; if a database access
     * error occurs; this method is called on a closed <code>PreparedStatement</code> or if parameterIndex does not correspond to
     * a parameter marker in the SQL statement,
     * @throws SQLFeatureNotSupportedException if the JDBC driver does not support this method
     * @since 1.6
     */
    public void setBlob(int parameterIndex, InputStream inputStream) throws SQLException {
        ps.setBlob(parameterIndex, inputStream);
    }

    /**
     * Sets the designated parameter to a <code>Reader</code> object. This method differs from the
     * <code>setCharacterStream (int, Reader)</code> method because it informs the driver that the parameter value should be sent
     * to the server as a <code>NCLOB</code>. When the <code>setCharacterStream</code> method is used, the driver may have to do
     * extra work to determine whether the parameter data should be sent to the server as a <code>LONGNVARCHAR</code> or a
     * <code>NCLOB</code>
     * <P>
     * <B>Note:</B> Consult your JDBC driver documentation to determine if it might be more efficient to use a version of
     * <code>setNClob</code> which takes a length parameter.
     * @param parameterIndex index of the first parameter is 1, the second is 2, ...
     * @param reader An object that contains the data to set the parameter value to.
     * @throws SQLException if parameterIndex does not correspond to a parameter marker in the SQL statement; if the driver does
     * not support national character sets; if the driver can detect that a data conversion error could occur; if a database
     * access error occurs or this method is called on a closed <code>PreparedStatement</code>
     * @throws SQLFeatureNotSupportedException if the JDBC driver does not support this method
     * @since 1.6
     */
    public void setNClob(int parameterIndex, Reader reader) throws SQLException {
        ps.setNClob(parameterIndex, reader);
    }


    /**
     * Requests that a <code>Statement</code> be pooled or not pooled. The value specified is a hint to the statement pool
     * implementation indicating whether the applicaiton wants the statement to be pooled. It is up to the statement pool manager
     * as to whether the hint is used.
     * <p>
     * The poolable value of a statement is applicable to both internal statement caches implemented by the driver and external
     * statement caches implemented by application servers and other applications.
     * <p>
     * By default, a <code>Statement</code> is not poolable when created, and a <code>PreparedStatement</code> and
     * <code>CallableStatement</code> are poolable when created.
     * <p>
     * @param poolable requests that the statement be pooled if true and that the statement not be pooled if false
     * <p>
     * @throws SQLException if this method is called on a closed <code>Statement</code>
     * <p>
     * @since 1.6
     */
    public void setPoolable(boolean poolable) throws SQLException {
        ps.setPoolable(poolable);

    }

    /**
     * Returns a value indicating whether the <code>Statement</code> is poolable or not.
     * <p>
     * @return <code>true</code> if the <code>Statement</code> is poolable; <code>false</code> otherwise
     * <p>
     * @throws SQLException if this method is called on a closed <code>Statement</code>
     * <p>
     * @since 1.6
     * <p>
     * @see java.sql.Statement#setPoolable(boolean) setPoolable(boolean)
     */
    public boolean isPoolable() throws SQLException {
        return ps.isPoolable();
    }

    /**
     * Returns true if this either implements the interface argument or is directly or indirectly a wrapper for an object that does.
     * Returns false otherwise. If this implements the interface then return true, else if this is a wrapper then return the result
     * of recursively calling isWrapperFor on the wrapped object. If this does not implement the interface and is not a wrapper,
     * return false. This method should be implemented as a low-cost operation compared to unwrap so that callers can use this method
     * to avoid expensive unwrap calls that may fail. If this method returns true then calling unwrap with the same argument should succeed.
     * @param iface a Class defining an interface. 
     * @returns true if this implements the interface or directly or indirectly wraps an object that does. 
     * @throws SQLException if an error occurs while determining whether this is a wrapper for an object with the given interface.
     */
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return ps.isWrapperFor(iface);
    }

    /**
     * Returns an object that implements the given interface to allow access to non-standard methods, or standard methods not exposed by the proxy.
     * If the receiver implements the interface then the result is the receiver or a proxy for the receiver. If the receiver is a wrapper
     * and the wrapped object implements the interface then the result is the wrapped object or a proxy for the wrapped object.
     * Otherwise return the the result of calling unwrap recursively on the wrapped object or a proxy for that result. 
     * If the receiver is not a wrapper and does not implement the interface, then an SQLException is thrown.
     * @param iface A Class defining an interface that the result must implement. 
     * @returns an object that implements the interface. May be a proxy for the actual implementing object. 
     * @throws SQLException If no object found that implements the interface
     */
    public <T> T unwrap(Class<T> iface) throws SQLException {
        return ps.unwrap(iface);
    }
}
