/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.dbm.internal.mbean;

// Java imports
import java.util.ArrayList;

import org.ow2.jonas.lib.management.javaee.J2EEResource;


/**
 * MBean class for JDBCResource Management
 *
 * @author Eric Hardesty JSR 77 (J2EE Management Standard)
 * @author Adriana Danes improve JDBCDataSource management
 */
public class JDBCResource extends J2EEResource {
    /**
     * List of available JDBC data sources given by the list of
     * OBJECT_NAMEs of the JDBCDataSource MBeans.
     */
    private ArrayList jdbcDataSources = new ArrayList();

    /**
     * Create q JDBCResource MBean.
     * @param objectName This MBean's OBJECT_NAME
     */
    public JDBCResource(String objectName) {
        super(objectName);
    }

    /**
     * @return The OBJECT_NAMEs of the JDBCDataSource MBeans.
     */
    public String[] getJdbcDataSources() {
        return ((String[]) jdbcDataSources.toArray(new String[jdbcDataSources.size()]));
    }

    /**
     * @param jdbcDataSourceName Add an OBJECT_NAME to the list of OBJECT_NAMEs
     * associated to the JDBCResource MBean (means new JDBCDataSource created).
     */
    public void addJdbcDataSource(String jdbcDataSourceName) {
        jdbcDataSources.add(jdbcDataSourceName);
    }

    /**
     * @param jdbcDataSourceName Remove an OBJECT_NAME from the list of OBJECT_NAMEs
     * associated to the JDBCResource MBean (means JDBCDataSource removed).
     */
    public void removeJdbcDataSource(String jdbcDataSourceName) {
        jdbcDataSources.remove(jdbcDataSourceName);
    }
}
