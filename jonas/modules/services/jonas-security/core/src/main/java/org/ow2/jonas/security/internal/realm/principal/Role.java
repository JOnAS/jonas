/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.security.internal.realm.principal;

import java.io.Serializable;

/**
 * This class define the Role class which define a role with its name and
 * description.
 * @author Florent Benoit
 */
public class Role implements Serializable, RoleMBean {

    /**
     * Name of the role
     */
    private String name = null;

    /**
     * Description of the role
     */
    private String description = null;

    /**
     * Constructor
     */
    public Role() {

    }

    /**
     * Constructor with a specific role name
     * @param name the role name to use
     */
    public Role(String name) {
        setName(name);
    }

    /**
     * Set the name of this role
     * @param name Name of the role
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get the name of this role
     * @return the name of this role
     */
    public String getName() {
        return name;
    }

    /**
     * Set the description of this role
     * @param description description of the role
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Get the description of this role
     * @return the description of this role
     */
    public String getDescription() {
        return description;
    }

    /**
     * String representation of the role
     * @return the xml representation of the role
     */
    public String toXML() {
        StringBuffer xml = new StringBuffer("<role name=\"");
        xml.append(name);
        xml.append("\" description=\"");
        if (description != null) {
            xml.append(description);
        }
        xml.append("\" />");
        return xml.toString();
    }

    /**
     * Use the XML representation of this object
     * @return the XML representation of this object
     */
    public String toString() {
        return this.toXML();
    }

}