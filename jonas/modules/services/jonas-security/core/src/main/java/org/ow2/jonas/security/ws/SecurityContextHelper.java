/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.security.ws;

import org.ow2.jonas.lib.util.Log;
import org.ow2.jonas.security.internal.AbsSecurityContextHelper;

import org.objectweb.util.monolog.api.Logger;

/**
 * This class is used by the JOnAS EJB provider Web Service.
 * It allows to authenticate users.
 * @author Florent Benoit : Initial developper
 * @author Helene Joanin : Refactoring
 */
public class SecurityContextHelper extends AbsSecurityContextHelper {

    /**
     * The singleton instance.
     */
    private static SecurityContextHelper instance = null;

    /**
     * WS Realm key.
     */
    private static final String WS_REALM_KEY = "jonas.service.security.ws.realm";

    /**
     * Default ws resource name.
     */
    private static final String DEFAULT_WS_REALM = "memrlm_1";

    /**
     * Logger.
     */
    private static Logger logger = Log.getLogger(Log.JONAS_WS_SECURITY_PREFIX);

    /**
     *  Private constructor because of singleton.
     */
    private SecurityContextHelper() {
    }

    /**
     * @return return the singleton instance.
     */
    public static SecurityContextHelper getInstance() {
        if (instance == null) {
            instance = new SecurityContextHelper();
        }
        return instance;
    }

    /**
     * @return the associated logger.
     */
    protected Logger getLogger() {
        return logger;
    }

    /**
     * @return return the WS Realm key.
     */
    protected String getRealmKey() {
        return WS_REALM_KEY;
    }

    /**
     * @return return the WS default Realm.
     */
    protected String getRealmDefault() {
        return DEFAULT_WS_REALM;
    }

}
