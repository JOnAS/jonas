/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.security.internal.realm.lib;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

/**
 * Useful class. Make xml representation for some objects used for the security
 * @author Florent Benoit
 */
public class XML {

    /**
     * Default constructor (Utility class, private constructor)
     */
    private XML() {

    }

    /**
     * Append to the given buffer the hashtable elements with comma separated
     * list
     * @param name name of the element
     * @param buffer the buffer on which append the Hashtable
     * @param vector the vector where are the elements
     */
    public static void appendVectorToBuffer(String name, StringBuffer buffer, Vector vector) {
        if (vector.size() > 0) {
            buffer.append(" " + name + "\"");
            int nb = 0;
            for (Enumeration e = vector.elements(); e.hasMoreElements();) {
                if (nb > 0) {
                    buffer.append(",");
                }
                String s = (String) e.nextElement();
                buffer.append(s);
                nb++;
            }
            buffer.append("\"");
        }
    }

    /**
     * Append to the given buffer the hashtable elements.
     * @param buffer the buffer on which append the Hashtable
     * @param vector the vector where are the elements
     */
    public static void xmlVector(StringBuffer buffer, Vector vector) {
        if (vector.size() > 0) {
            for (Enumeration e = vector.elements(); e.hasMoreElements();) {
                Object o = e.nextElement();
                buffer.append(o.toString());
                buffer.append("\n");
            }
        }
    }

    /**
     * Append to the given buffer the hashtable elements.
     * @param buffer the buffer on which append the Hashtable
     * @param hashtable the hashtable where are the elements
     */
    public static void xmlHashtable(StringBuffer buffer, Hashtable hashtable) {
        xmlHashtable(buffer, hashtable, "");
    }

    /**
     * Append to the given buffer the hashtable elements.
     * @param buffer the buffer on which append the Hashtable
     * @param hashtable the hashtable where are the elements
     * @param indent the indent to put before the lines
     */
    public static void xmlHashtable(StringBuffer buffer, Hashtable hashtable, String indent) {
        if (hashtable.size() > 0) {
            for (Enumeration e = hashtable.elements(); e.hasMoreElements();) {
                Object o = e.nextElement();
                buffer.append(indent + o.toString());
                buffer.append("\n");
            }
        }
    }

}