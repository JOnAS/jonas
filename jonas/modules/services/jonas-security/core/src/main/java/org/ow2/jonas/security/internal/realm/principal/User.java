/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.security.internal.realm.principal;

import java.io.Serializable;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.Vector;

import org.ow2.jonas.security.internal.realm.lib.HashHelper;
import org.ow2.jonas.security.internal.realm.lib.XML;
import org.ow2.jonas.security.realm.lib.HashPassword;
import org.ow2.jonas.security.realm.principal.JUser;


/**
 * This class define the User class which represent a user by its name,
 * password, etc.
 * @author Florent Benoit (initial developer)
 * @author Alexandre Thaveau (add DN for the certificates in method setName)
 * @author Marc-Antoine Bourgeot (add DN for the certificates in method setName)
 */
public class User implements Principal, Serializable, UserMBean, JUser {

    /**
     * Separator of the groups/roles
     */
    protected static final String SEPARATOR = ",";

    /**
     * Name of the user
     */
    private String name = null;

    /**
     * Password of the user
     */
    private String password = null;

    /**
     * Hash password of the user
     */
    private HashPassword hashPassword = null;

    /**
     * Groups
     */
    private Vector groups = new Vector();

    /**
     * Roles
     */
    private Vector roles = new Vector();

    /**
     * Combined roles
     */
    private ArrayList combinedRoles = new ArrayList();

    /**
     * Constructor
     */
    public User() {
    }

    /**
     * Constructor with a given login / password
     * @param name the given name
     * @param password the given password
     */
    public User(String name, String password) {
        setName(name);
        setPassword(password);
    }

    /*
     */
    public void setName(String name) {
        if (name.startsWith("##DN##")) {
            //replace problematic caracters(for mbeans) by special caracters
            this.name = name.replace('=', '#').replace(',', '%').replace(' ', '$');
        } else {
            this.name = name;
        }
    }

    /*
     */
    public String getName() {
        return name;
    }

    /*
     */
    public String getPassword() {
        return password;
    }

    /*
     */
    public void setPassword(String password) {
        this.password = password;
        setHashPassword(HashHelper.getHashPassword(password));
    }

    /**
     * Set the hashed password of this user
     * @param hashPassword hashed password of this user
     */
    protected void setHashPassword(HashPassword hashPassword) {
        this.hashPassword = hashPassword;
    }

    /*
     */
    public HashPassword getHashPassword() {
        return hashPassword;
    }

    /*
     */
    public void setGroups(String groups) {
        StringTokenizer st = new StringTokenizer(groups, SEPARATOR);
        String group = null;
        while (st.hasMoreTokens()) {
            group = st.nextToken().trim();
            addGroup(group);
        }
    }

    /*
     */
    public String getGroups() {
        String groupsList = "";
        Enumeration g = groups.elements();
        int nb = 0;
        String group = null;

        while (g.hasMoreElements()) {
            if (nb > 0) {
                groupsList += ", ";
            }
            group = (String) g.nextElement();
            groupsList += group;
        }
        return groupsList;

    }

    /*
     */
    public String[] getArrayGroups() {
        return ((String[]) groups.toArray(new String[groups.size()]));
    }

    /*
     */
    public void setRoles(String roles) {
        if (roles != null) {
            StringTokenizer st = new StringTokenizer(roles, SEPARATOR);
            String role = null;
            while (st.hasMoreTokens()) {
                role = st.nextToken().trim();
                addRole(role);
            }
        }
    }

    /*
     */
    public void addGroup(String group) {
        if (!groups.contains(group)) {
            this.groups.addElement(group);
        }
    }

    /*
     */
    public void addRole(String role) {
        if (!roles.contains(role)) {
            this.roles.addElement(role);
        }
    }

    /*
     */
    public void removeGroup(String group) {
        if (groups.contains(group)) {
            this.groups.removeElement(group);
        }
    }

    /*
     */
    public void removeRole(String role) {
        if (roles.contains(role)) {
            this.roles.removeElement(role);
        }
    }

    /*
     */
    public String getRoles() {
        String rolesList = "";
        Enumeration r = roles.elements();
        int nb = 0;
        String role = null;

        while (r.hasMoreElements()) {
            if (nb > 0) {
                rolesList += ", ";
            }
            role = (String) r.nextElement();
            rolesList += role;
        }
        return rolesList;
    }

    /*
     */
    public void setCombinedRoles(ArrayList combinedRoles) {
        this.combinedRoles = combinedRoles;
    }

    /*
     */
    public ArrayList getCombinedRoles() {
        return combinedRoles;
    }

    /*
     */
    public String[] getArrayRoles() {
        return ((String[]) roles.toArray(new String[roles.size()]));
    }

    /*
     */
    public String toXML() {
        StringBuffer xml = new StringBuffer("<user name=\"");
        xml.append(name);
        xml.append("\" password=\"");
        if (password != null) {
            xml.append(password);
        }
        xml.append("\"");
        XML.appendVectorToBuffer("groups=", xml, groups);
        XML.appendVectorToBuffer("roles=", xml, roles);
        xml.append(" />");
        return xml.toString();
    }

    /**
     * Use the XML representation of this object
     * @return the XML representation of this object
     */
    public String toString() {
        return this.toXML();
    }

}