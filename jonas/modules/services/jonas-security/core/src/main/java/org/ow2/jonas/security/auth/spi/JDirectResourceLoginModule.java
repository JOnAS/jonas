/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.security.auth.spi;

import java.util.ArrayList;
import java.util.Map;

import javax.naming.InitialContext;
import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;

import org.ow2.jonas.lib.security.auth.JGroup;
import org.ow2.jonas.lib.security.auth.JPrincipal;
import org.ow2.jonas.lib.security.auth.JRole;
import org.ow2.jonas.security.auth.callback.CertificateCallback;
import org.ow2.jonas.security.realm.factory.JResource;
import org.ow2.jonas.security.realm.factory.JResourceException;
import org.ow2.jonas.security.realm.principal.JUser;


/**
 * Define a login module for the authentication by using one of the JOnAS
 * resource Datasource, LDAP or file
 * @author Florent Benoit (initial developer)
 * @author Alexandre Thaveau (add callback for certificates)
 * @author Marc-Antoine Bourgeot (add callback for certificates)
 */
public class JDirectResourceLoginModule implements LoginModule {

    /**
     * Subject used
     */
    private Subject subject = null;

    /**
     * The callbackhandler user for identification
     */
    private CallbackHandler callbackHandler = null;

    /**
     * Options for this login module
     */
    private Map options = null;

    /**
     * Name of the principal
     */
    private String principalName = null;

    /**
     * Password of the principal
     */
    private String password = null;

    /**
     * Roles of the principal
     */
    private ArrayList principalRoles = null;

    /**
     * Indicates if the login was successfull or not
     */
    private boolean loginWasDoneWithSuccess = false;

    /**
     * Initialize this LoginModule. This method is called by the LoginContext
     * after this LoginModule has been instantiated. The purpose of this method
     * is to initialize this LoginModule with the relevant information. If this
     * LoginModule does not understand any of the data stored in sharedState or
     * options parameters, they can be ignored.
     * @param subject the Subject to be authenticated.
     * @param callbackHandler a CallbackHandler for communicating with the end
     *        user (prompting for usernames and passwords, for example).
     * @param sharedState state shared with other configured LoginModules.
     * @param options options specified in the login Configuration for this
     *        particular LoginModule.
     */
    public void initialize(Subject subject, CallbackHandler callbackHandler, Map sharedState, Map options) {
        this.subject = subject;
        this.callbackHandler = callbackHandler;
        this.options = options;
    }

    /**
     * Method to authenticate a Subject (phase 1). The implementation of this
     * method authenticates a Subject. For example, it may prompt for Subject
     * information such as a username and password and then attempt to verify
     * the password. This method saves the result of the authentication attempt
     * as private state within the LoginModule.
     * @return true if the authentication succeeded, or false if this
     *         LoginModule should be ignored.
     * @throws LoginException if the authentication fails
     */
    public boolean login() throws LoginException {

        // No handler
        if (callbackHandler == null) {
            throw new LoginException("No handler has been defined.");
        }

        // Resource to be used (jndi name)
        String resourceName = (String) options.get("resourceName");

        // Use certificate callback
        String certCallback = (String) options.get("certCallback");

        // No resource is specified -> fail
        if (resourceName == null) {
            throw new LoginException(
                    "You have to give an argument to this login module. The 'resourceName' parameter is required.");
        }

        // Get the resource and perform authentication
        try {
            InitialContext ictx = new InitialContext();
            JResource jResource = null;
            try {
                jResource = (JResource) ictx.lookup(resourceName);
            } catch (Exception e) {
                throw createChainedLoginException("Cannot retrieve the resource '" + resourceName
                        + "'. Check that this resource is bound in the registry", e);
            }

            // Handle callbacks
            NameCallback nameCallback = new NameCallback("User :");
            // False to hide password when it is entered
            PasswordCallback passwordCallback = new PasswordCallback("Password :", false);
            CertificateCallback certificateCallback = new CertificateCallback();
            Callback[] callbacks = null;

            if ((certCallback != null) && (Boolean.valueOf(certCallback).booleanValue())) {
                callbacks = new Callback[] {nameCallback, passwordCallback, certificateCallback};
            } else {
                callbacks = new Callback[] {nameCallback, passwordCallback};
            }
            callbackHandler.handle(callbacks);

            // Get values
            principalName = nameCallback.getName();
            if (principalName == null) {
                throw new LoginException("A null username is not a valid username");
            }
            if (principalName.startsWith("##DN##") && (certificateCallback.getUserCertificate() == null)) {
                throw new LoginException("Name must have a certificate to access this certificate based access login");
            }
            char[] arrayPass = passwordCallback.getPassword();
            if (arrayPass == null) {
                throw new LoginException("A null password is not a valid password");
            }

            // Authentication - step 1 (user)
            JUser user = null;
            try {
                user = jResource.findUser(principalName);
            } catch (Exception jre) {
                // could not retrieve user
                throw createChainedLoginException("Can not find the user", jre);
            }
            // User was not found
            if (user == null) {
                throw new LoginException("User '" + principalName + "' not found.");
            }

            // Authentication - step 2 (password)
            boolean validated = jResource.isValidUser(user, new String(arrayPass));
            if (!validated) {
                throw new LoginException("The password for the user '" + principalName + "' is not valid");
            }

            // user password
            if (user.getPassword() != null) {
                this.password = user.getPassword();
            } else {
                this.password = new String(arrayPass);
            }

            // Authentication - step 3 (roles)
            try {
                principalRoles = jResource.getArrayListCombinedRoles(user);
            } catch (JResourceException jre) {
                throw createChainedLoginException(jre.getMessage(), jre);
            }

        } catch (Exception e) {
            throw createChainedLoginException("Error during the login phase : " + e.getMessage(), e);
        }
        loginWasDoneWithSuccess = true;
        return true;
    }

    /**
     * Create a LoginException with the given message and set the cause to the given Exception
     * @param msg Exception message
     * @param e Root cause
     * @return LoginException the chained LoginException
     */
    private static LoginException createChainedLoginException(String msg, Exception e) {
        LoginException le = new LoginException(msg);
        le.initCause(e);
        return le;
    }

    /**
     * Method to commit the authentication process (phase 2). This method is
     * called if the LoginContext's overall authentication succeeded (the
     * relevant REQUIRED, REQUISITE, SUFFICIENT and OPTIONAL LoginModules
     * succeeded). If this LoginModule's own authentication attempt succeeded
     * (checked by retrieving the private state saved by the login method), then
     * this method associates relevant Principals and Credentials with the
     * Subject located in the LoginModule. If this LoginModule's own
     * authentication attempted failed, then this method removes/destroys any
     * state that was originally saved.
     * @return true if this method succeeded, or false if this LoginModule
     *         should be ignored.
     * @throws LoginException if the commit fails
     */
    public boolean commit() throws LoginException {

            //overall authentication succeeded
        if (loginWasDoneWithSuccess) {
            // Add principal to the current subject
            subject.getPrincipals().add(new JPrincipal(principalName));
            subject.getPrivateCredentials().add(password);
            JGroup group = new JGroup("Roles");

            // Convert list into array
            String[] roles = new String[principalRoles.size()];
            roles = (String[]) principalRoles.toArray(roles);
            int size = principalRoles.size();
            for (int i = 0; i < size; i++) {
                group.addMember(new JRole(roles[i]));
            }

            // Add group
            subject.getPrincipals().add(group);
        }
        return loginWasDoneWithSuccess;
    }

    /**
     * Method to abort the authentication process (phase 2). This method is
     * called if the LoginContext's overall authentication failed. (the relevant
     * REQUIRED, REQUISITE, SUFFICIENT and OPTIONAL LoginModules did not
     * succeed). If this LoginModule's own authentication attempt succeeded
     * (checked by retrieving the private state saved by the login method), then
     * this method cleans up any state that was originally saved.
     * @return true if this method succeeded, or false if this LoginModule
     *         should be ignored.
     * @throws LoginException if the abort fails
     */
    public boolean abort() throws LoginException {
        //overall authentication succeeded
        if (loginWasDoneWithSuccess) {
            // Reset temp values
            principalName = null;
            principalRoles = null;
        }
        return loginWasDoneWithSuccess;
    }

    /**
     * Method which logs out a Subject. An implementation of this method might
     * remove/destroy a Subject's Principals and Credentials.
     * @return true if this method succeeded, or false if this LoginModule
     *         should be ignored.
     * @throws LoginException if the logout fails
     */
    public boolean logout() throws LoginException {

        //overall authentication succeeded
        if (loginWasDoneWithSuccess) {
            // Remove principal name
            subject.getPrincipals().remove(new JPrincipal(principalName));
        }

        return loginWasDoneWithSuccess;
    }

}