/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.security.ws.initializer;

import org.ow2.jonas.security.SecurityService;

/**
 * Initializes ws/iiop security context helpers.
 * @author eyindanga
 *
 */
public class SecurityContextInitializer {
    /**
     * The security service.
     */
    private SecurityService securityService = null;
    /**
     * Bind the security service.
     * @param securityService the security service to bind.
     */
    public void setSecurityService(final SecurityService securityService) {
        this.securityService = securityService;
        org.ow2.jonas.security.ws.SecurityContextHelper.getInstance().setSecurityService(securityService);
    }

    /**
     * Remove the security service.
     */
    public void removeSecurityService() {
        org.ow2.jonas.security.ws.SecurityContextHelper.getInstance().setSecurityService(null);
        this.securityService = null;
    }

}
