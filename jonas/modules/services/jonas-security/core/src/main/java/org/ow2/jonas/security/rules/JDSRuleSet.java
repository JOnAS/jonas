/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.security.rules;

import org.apache.commons.digester.Digester;
import org.ow2.jonas.deployment.common.rules.JRuleSetBase;
import org.ow2.jonas.security.internal.realm.factory.JResourceDS;
import org.ow2.jonas.security.realm.factory.JResource;



/**
 * Add the rules for the datasource in the jonas-realm.xml file
 *
 * @author Guillaume Sauthier
 */
public class JDSRuleSet extends JRuleSetBase {

    /**
     * Construct an object with a specific prefix
     * @param prefix prefix to use during the parsing
     */
    public JDSRuleSet(final String prefix) {
        super(prefix);
   }
     /**
     * Add a set of rules to the digester object
     * @param digester Digester instance
     */
    @Override
    public void addRuleInstances(final Digester digester) {

        // Configure the actions we will be using
        digester.addObjectCreate(prefix + "dsrealm",
                                 JResourceDS.class.getName());

        digester.addSetProperties(prefix + "dsrealm");
        digester.addSetNext(prefix + "dsrealm",
                            "addJResource",
                            JResource.class.getName());
    }

}
