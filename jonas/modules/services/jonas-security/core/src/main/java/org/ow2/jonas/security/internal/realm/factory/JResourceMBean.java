/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2009 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent Benoit
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.security.internal.realm.factory;

import javax.management.MBeanException;
import javax.management.NotificationFilter;
import javax.management.NotificationListener;
import javax.management.RuntimeOperationsException;

import org.apache.commons.modeler.BaseModelMBean;
/**
 * 
 */
public class JResourceMBean extends BaseModelMBean {

    public JResourceMBean() throws MBeanException, RuntimeOperationsException {
        super();
    }

    /**
     * This method is 'called' by the ReconfigManager in order to receive the reconfiguration and the save notifications.
     * @param arg0 The notification Listener
     * @param arg1 The notification Filter
     * @param arg2 Handback object
     */
    @Override
    public void addNotificationListener(final NotificationListener arg0
            , final NotificationFilter arg1, final Object arg2) {
        super.addNotificationListener(arg0, arg1, arg2);
    }

}