/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.security.internal.realm.factory;

import java.rmi.Remote;
import java.rmi.RemoteException;

import javax.security.auth.Subject;

import org.ow2.jonas.lib.security.auth.JSubject;


/**
 * This interface defines a method to allow authentication on server side even for Client container or remote applications
 * @author Florent Benoit
 */
public interface JResourceRemote extends Remote {

    /**
     * Authenticate a given user
     * @param principalName name of the user
     * @param arrayPass password of the user
     * @param resourceName type of resource to use to register ( memory, jdbc, ldap)
     * @throws RemoteException if the authentication failed
     * @return an authenticated subject if it succeed
     */
    JSubject authenticate(String principalName, char[] arrayPass, String resourceName) throws RemoteException;


    /**
     * Authenticate a given user
     * @param principalName name of the user
     * @param arrayPass password of the user
     * @param entryName the name of the JAAS entry to search in jaas configuration file
     * @throws RemoteException if the authentication failed
     * @return an authenticated subject if it succeed
     */
    Subject authenticateJAAS(String principalName, char[] arrayPass, String entryName) throws RemoteException;

}
