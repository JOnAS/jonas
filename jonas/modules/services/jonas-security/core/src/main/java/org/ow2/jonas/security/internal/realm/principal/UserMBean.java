/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.security.internal.realm.principal;


/**
 * This interface defines the MBean method for the User
 * @author Florent Benoit
 */
public interface UserMBean {

    /**
     * Get the roles
     * @return the array of the roles
     */
    String[] getArrayRoles();

    /**
     * Get the groups
     * @return the array of the groups
     */
    String[] getArrayGroups();

    /**
     * Get the name of this user
     * @return the name of this user
     */
    String getName();

    /**
     * Get the password of this user
     * @return the password of this user
     */
    String getPassword();

    /**
     * Set the password of this user
     * @param password password of the user
     */
    void setPassword(String password);

    /**
     * Add the specified group to this user
     * @param group the group to add
     */
    void addGroup(String group);

    /**
     * Add a role to this user
     * @param role the given role
     */
    void addRole(String role);

    /**
     * Remove a group from this user
     * @param group the given group
     */
    void removeGroup(String group);

    /**
     * Remove a role from this user
     * @param role the given role
     */
    void removeRole(String role);

}
