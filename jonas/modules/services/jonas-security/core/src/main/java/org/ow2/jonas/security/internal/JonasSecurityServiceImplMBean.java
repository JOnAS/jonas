/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Bruno Michel, Guillaume Riviere
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.security.internal;

import java.security.NoSuchAlgorithmException;

/**
 * MBean interface for Security Service Management
 * MBean type: Standard
 * MBean model: Inheritance (JonasSecurityServiceImpl)
 * @author Bruno Michel, Guillaume Riviere
 * @author Florent Benoit : add MBean methods
 */
public interface JonasSecurityServiceImplMBean {

    /**
     * Encrypt a string with an algorithm
     * @param string the string to encode
     * @param algo algorithm to apply on the given string
     * @return the encoded string
     * @throws NoSuchAlgorithmException One reason could be a bad algorithm
     */
    String encryptPassword(String string, String algo) throws NoSuchAlgorithmException;


    /**
     * Add JResources with a given xml configuration
     * @param xml xml representation of the resources to add
     * @throws Exception if the resources can't be added
     */
    void addResources(String xml) throws Exception;

    /**
     * Remove the Resource (memory, ldap, datasource,...)
     * @param resourceName name of the resource
     * @throws Exception if the resource name does not exist
     */
    void removeJResource(String resourceName) throws Exception;

    /**
     * Check if the given algorithm is a valid algorithm
     * @param algo algorithm to apply on the given string
     * @return true if it is a valid algorithm
     */
    boolean isValidAlgorithm(String algo);

    /**
     * Add a Memory resource
     * @param name the name of the JResourceMemory to create
     * @throws Exception if the resource can't be added
     */
    void addJResourceMemory(String name) throws Exception;

    /**
     * Add a DS resource
     * @param name the name of the JResourceDS to create
     * @param dsName Name of the datasource resource to use.
     * @param userTable Name of table which have the username/password
     * @param userTableUsernameCol Column of the username of the user table
     * @param userTablePasswordCol Column of the password of the user table
     * @param roleTable Name of table which have the username/role
     * @param roleTableUsernameCol Column of the username of the role table
     * @param roleTableRolenameCol Column of the role of the role table
     * @param algorithm Default algorithm. If specified, the default is not 'clear' password
     * @throws Exception if the resource can't be added
     */
    void addJResourceDS(String name,
                        String dsName,
                        String userTable,
                        String userTableUsernameCol,
                        String userTablePasswordCol,
                        String roleTable,
                        String roleTableUsernameCol,
                        String roleTableRolenameCol,
                        String algorithm) throws Exception;


    /**
     * Add a LDAP resource
     * @param name the name of the JResourceLDAP to create
     * @param initialContextFactory Initial context factory for the LDAp server
     * @param providerUrl Url of the ldap server
     * @param securityAuthentication Type of the authentication used during the authentication to the LDAP server
     * @param securityPrincipal DN of the Principal(username). He can retrieve the information from the user
     * @param securityCredentials Credential(password) of the principal
     * @param securityProtocol Constant that holds the name of the environment property for specifying the security protocol to use.
     * @param language Constant that holds the name of the environment property for specifying the preferred language to use with the service.
     * @param referral Constant that holds the name of the environment property for specifying how referrals encountered by the service provider are to be processed.
     * @param stateFactories Constant that holds the name of the environment property for specifying the list of state factories to use.
     * @param authenticationMode Mode for validate the authentication (BIND_AUTHENTICATION_MODE or COMPARE_AUTHENTICATION_MODE)
     * @param userPasswordAttribute Attribute in order to get the password from the ldap server
     * @param userRolesAttribute Attribute in order to get the user role from the ldap server
     * @param roleNameAttribute Attribute for the role name when performing a lookup on a role
     * @param baseDN DN used for the lookup
     * @param userDN DN used when searching the user DN. Override the baseDN if it is defined
     * @param userSearchFilter Filter used when searching the user
     * @param roleDN DN used when searching the role DN. Override the baseDN if it is defined
     * @param roleSearchFilter Filter used when searching the role
     * @param algorithm Default algorithm. If specified, the default is not 'clear' password
     * @throws Exception if the resource can't be added
     */
    void addJResourceLDAP(String name,
                          String initialContextFactory,
                          String providerUrl,
                          String securityAuthentication,
                          String securityPrincipal,
                          String securityCredentials,
                          String securityProtocol,
                          String language,
                          String referral,
                          String stateFactories,
                          String authenticationMode,
                          String userPasswordAttribute,
                          String userRolesAttribute,
                          String roleNameAttribute,
                          String baseDN,
                          String userDN,
                          String userSearchFilter,
                          String roleDN,
                          String roleSearchFilter,
                          String algorithm) throws Exception;

}
