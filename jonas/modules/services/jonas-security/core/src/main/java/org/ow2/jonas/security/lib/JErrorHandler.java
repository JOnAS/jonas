/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.security.lib;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * Define an error handler which throw an exception
 * as Digester not (only print stack trace).
 * Security service use this handler for throwing/catching in
 * a convenient way the xml parsing error of jonas-realm.xml file.
 */
public class JErrorHandler implements ErrorHandler {

    /**
     * Receive notification of a warning.
     * @param exception exception to throw
     * @throws SAXException if an error is thrown
     */
    public void warning(SAXParseException exception) throws SAXException {

    }

    /**
     * Receive notification of a recoverable error.
     * @param exception exception to throw
     * @throws SAXException if an error is thrown
     */
    public void error(SAXParseException exception) throws SAXException {
        throw exception;
    }

    /**
     * Receive notification of a non-recoverable error.
     * @param exception exception to throw
     * @throws SAXException if an error is thrown
     */
    public void fatalError(SAXParseException exception) throws SAXException {
        throw exception;
    }
}
