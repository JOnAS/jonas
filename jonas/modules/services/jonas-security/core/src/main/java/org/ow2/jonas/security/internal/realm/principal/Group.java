/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent BENOIT
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.security.internal.realm.principal;

import java.io.Serializable;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.Vector;

import org.ow2.jonas.security.internal.realm.lib.XML;


/**
 * This class define the Group class which represent a group with its associated
 * roles
 * @author Florent Benoit
 */

public class Group implements Serializable, GroupMBean {

    /**
     * Separator of the roles
     */
    protected static final String SEPARATOR = ",";

    /**
     * Name of the user
     */
    private String name = null;

    /**
     * Roles
     */
    private Vector roles = new Vector();

    /**
     * Description of the role
     */
    private String description = null;

    /**
     * Default Constructor
     */
    public Group() {

    }

    /**
     * Constructor with a given name
     * @param name the name of this group
     */
    public Group(String name) {
        setName(name);
    }

    /**
     * Set the name of this user
     * @param name Name of the user
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get the name of this user
     * @return the name of this user
     */
    public String getName() {
        return name;
    }

    /**
     * Set the description of this group
     * @param description description of the group
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Get the description of this group
     * @return the description of this group
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the roles of the group
     * @param roles the list of the roles of the group
     */
    public void setRoles(String roles) {
        StringTokenizer st = new StringTokenizer(roles, SEPARATOR);
        String role = null;
        while (st.hasMoreTokens()) {
            role = st.nextToken().trim();
            addRole(role);
        }
    }

    /**
     * Add a role to this group
     * @param role the given role
     */
    public void addRole(String role) {
        if (!roles.contains(role)) {
            this.roles.addElement(role);
        }
    }

    /**
     * Remove a role from this group
     * @param role the given role
     */
    public void removeRole(String role) {
        if (roles.contains(role)) {
            this.roles.removeElement(role);
        }
    }

    /**
     * Get the roles
     * @return the array of the roles
     */
    public String getRoles() {
        String rolesList = "";
        Enumeration r = roles.elements();
        int nb = 0;
        String role = null;

        while (r.hasMoreElements()) {
            if (nb > 0) {
                rolesList += ", ";
            }
            role = (String) r.nextElement();
            rolesList += role;
        }
        return rolesList;
    }

    /**
     * Get the roles
     * @return the array of the roles
     */
    public String[] getArrayRoles() {
        return ((String[]) roles.toArray(new String[roles.size()]));
    }

    /**
     * String representation of the group
     * @return the xml representation of the group
     */
    public String toXML() {
        StringBuffer xml = new StringBuffer("<group name=\"");
        xml.append(name);
        xml.append("\" description=\"");
        if (description != null) {
            xml.append(description);
        }
        xml.append("\"");
        XML.appendVectorToBuffer("roles=", xml, roles);
        xml.append(" />");
        return xml.toString();
    }

    /**
     * Use the XML representation of this object
     * @return the XML representation of this object
     */
    public String toString() {
        return this.toXML();
    }

}