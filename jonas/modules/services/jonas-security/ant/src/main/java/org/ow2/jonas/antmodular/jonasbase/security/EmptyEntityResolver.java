/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.antmodular.jonasbase.security;

import java.io.IOException;
import java.io.StringReader;

import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


/**
 * Dummy empty resolver (we don't need to validate the schema)
 * @author Florent Benoit
 */
public class EmptyEntityResolver implements EntityResolver {


    /**
     * The Parser will call this method before opening any external entity
     * except the top-level document entity.
     * @param publicId The public identifier of the external entity being
     *        referenced, or null if none was supplied.
     * @param systemId The system identifier of the external entity being
     *        referenced.
     * @return An InputSource object describing the new input source, or null to
     *         request that the parser open a regular URI connection to the
     *         system identifier.
     * @throws SAXException Any SAX exception, possibly wrapping another
     *         exception.
     * @throws IOException A Java-specific IO exception, possibly the result of
     *         creating a new InputStream or Reader for the InputSource.
     */
    public InputSource resolveEntity(final String publicId, final String systemId) throws IOException, SAXException {
        // Return empty resolver
        return new InputSource(new StringReader(""));
    }
    
}
