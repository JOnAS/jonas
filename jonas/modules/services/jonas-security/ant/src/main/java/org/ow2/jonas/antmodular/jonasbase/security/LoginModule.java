/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.antmodular.jonasbase.security;

/**
 * @author Julien Legrand
 */
public abstract class LoginModule {

    /**
     * Default behaviour flag
     */
    protected static final String DEFAUT_FLAG = "required";

    /**
     * Flag for the behaviour of the login module.
     */
    protected String flag = DEFAUT_FLAG;

    /**
     * @return Returns the flag of the login module
     */
    public String getFlag() {
        return flag;
    }

    /**
     * @param flag the flag to set.
     */
    public void setFlag(String flag) {
        this.flag = flag;
    }
}
