/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.antmodular.jonasbase.security;

/**
 * Represents an User
 * @author Jeremy Cazaux
 */
public class User {

    /**
     * User's name
     */
    private String name;

    /**
     * User's password
     */
    private String password;

    /**
     * User's roles
     */
    private String roles;

    /**
     * Role's description
     */
    private String description;

    /**
     * Group description
     */
    private String groups;

    /**
     * Default constructor
     */
    public User() {
        super();
    }

    /**
     * @return the name of the user
     */
    public String getName() {
        return name;
    }

    /**
     * @return the password of the user
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param name User's name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @param password User's password to set
     */
    public void setPassword(final String password) {
        this.password = password;
    }

    /**
     * @return the user's roles
     */
    public String getRoles() {
        return roles;
    }

    /**
     * @param roles The user's roles to set
     */
    public void setRoles(final String roles) {
        this.roles = roles;
    }

    /**
     * @return the description of the role
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description of the role to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the group of the user if exist
     */
    public String getGroups() {
        return groups;
    }

    /**
     * @param groups The group of the user to set
     */
    public void setGroups(String groups) {
        this.groups = groups;
    }
}
