/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.antmodular.jonasbase.security;

import org.ow2.jonas.antmodular.jonasbase.bootstrap.Tasks;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Julien Legrand
 */
public class JaasEntry extends Tasks {

    /**
     * Name of the JAAS entry
     */
    private String name;

    /**
     * List of login modules for the JAAS entry
     */
    private List<LoginModule> loginModules;

    /**
     * Constructor
     */
    public JaasEntry(){
        super();
        loginModules = new ArrayList<LoginModule>();
    }

    /**
     * Configure the resource login module
     * @param lm JResourceLoginModule
     */
    public void addConfiguredJResourceLoginModule(final JResourceLoginModule lm){
        loginModules.add(lm);
    }

    /**
     * @return Returns the list of login modules.
     */
    public List<LoginModule> getLoginModules(){
        return loginModules;
    }

    /**
     * @return Returns the name of JAAS entry
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name of the JAAS entry to set.
     */
    public void setName(final String name){
        this.name = name;
    }
}

