/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull SAS
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.resourcemonitor.internal;

import java.util.List;

/**
 * Management interface for the Resource Monitor Service.
 * @author Mickaël LEDUQUE
 *
 */
public interface JOnASResourceMonitorServiceMBean {
    /**
     * Returns the number of monitored resources.
     * @return the number of monitored resources.
     */
    Integer getMonitoredResourcesCount();

    /**
     * Returns a list of descriptions for the monitored resources.
     * @return
     */
    List<String> getMonitoredResourcesDescriptions();

    /**
     * Sets the approximate time interval between two resource
     * checks.
     * @param interval the new interval in milliseconds.
     */
    void setMonitoringInterval(Long interval);

    /**
     * Returns the approximate time interval between two resource
     * checks.
     * @return interval the interval in milliseconds.
     */
    Long getMonitoringInterval();
}
