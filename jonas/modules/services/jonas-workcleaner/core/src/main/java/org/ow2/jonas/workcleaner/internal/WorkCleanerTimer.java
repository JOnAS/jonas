/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.workcleaner.internal;

import org.ow2.jonas.workcleaner.WorkCleanerService;

/**
 * The WorkCleanerTimer executes tasks periodically.
 * @author Francois Fornaciari
 */
public class WorkCleanerTimer extends Thread {

    /**
     * 1 second duration (in ms).
     */
    private static final int ONE_SECOND = 1000;

    /**
     * Reference to the WorkCleaner service.
     */
    private WorkCleanerService workCleanerService = null;

    /**
     * Clean period (in seconds).
     */
    private int period;

    /**
     * True until the time must be canceled.
     */
    private boolean isStarted = true;

    /**
     * Start the thread of this class It will clean all the work entries.
     */
    @Override
    public void run() {
        while (isStarted) {
            workCleanerService.executeTasks();
            try {
                // Convert seconds in milliseconds
                Thread.sleep(period * ONE_SECOND);
            } catch (InterruptedException e) {
                throw new RuntimeException("Thread fail to sleep");
            }
        }
    }

    /**
     * Set the WorkCleaner service.
     * @param workCleanerService The WorkCleaner service to set
     */
    public void setWorkCleanerService(final WorkCleanerService workCleanerService) {
        this.workCleanerService = workCleanerService;
    }

    /**
     * Set the clean period.
     * @param period The clean period
     */
    public void setPeriod(final int period) {
        this.period = period;
    }

    /**
     * Set the timer started value.
     * @param isStarted The timer started value
     */
    public void setStarted(final boolean isStarted) {
        this.isStarted = isStarted;
    }
}
