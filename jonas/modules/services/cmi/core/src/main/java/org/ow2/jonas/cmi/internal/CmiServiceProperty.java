/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007,2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.cmi.internal;

/**
 * Properties for CMI service.
 * @author eyindanga
 *
 */
public enum CmiServiceProperty {
    /**
     * Property name for specifying if the replication is enabled.
     */
    DEPLOY_DIRECTORY_KEY("cmi.server.deploy.directory"),
    /**
     * Default cluster factory.
     */
    DEFAULT_CLUSTER_FACTORY("org.ow2.jonas.cmi.internal.DefaultClusterFactory"),

    /**
     * Key for cluster factory.
     */
    CLUSTER_FACTORY_KEY("cmi.cluster.factory"),
    /**
     * Key for cluster factory.
     */
    CLUSTER_BUILD_METHOD("getCluster"),
    /**
     * Property name for specifying if the replication is enabled.
     */
    DEFAULT_DEPLOY_DIRECTORY("deploy");

    /**
     * A name of property.
     */
    private final String propertyName;

    /**
     * @param propertyName
     *            a name of property
     */
    CmiServiceProperty(final String propertyName) {
        this.propertyName = propertyName;
    }

    /**
     * @return the name of property
     */
    @Override
    public String toString() {
        return propertyName;
    }

    /**
     * @return the name of property
     */
    public String getPropertyName() {
        return propertyName;
    }
}
