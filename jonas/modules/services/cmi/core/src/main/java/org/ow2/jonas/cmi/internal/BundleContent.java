/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007-2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.cmi.internal;

import org.ow2.cmi.lb.policy.IPolicy;
import org.ow2.cmi.lb.strategy.IStrategy;

/**
 * Wrapper for a policy/strategy bundle.
 * @author eyindanga
 *
 */
public class BundleContent {
    /**
     * Bundle location.
     */
    private String location;
    /**
     * Policies of the bundle.
     */
    private String[] policyNames;
    /**
     * Strategies of the bundle.
     */
    private String[] strategyNames;

    /**
     * Strategy classes.
     */
    private Class<?>[] strategyClasses;

    /**
     * Policy classes.
     */
    private Class<?>[] policyClasses;


    /**
     * Constructor using fields.
     * @param location bundle location.
     * @param policies Policies of the bundle
     * @param strategies Strategies of the bundle
     */
    public BundleContent(final String location, final Class<? extends IPolicy<?>>[] policies,
            final Class<? extends IStrategy<?>>[] strategies) {
        this.location = location;
        this.policyNames = new String[policies.length];
        this.strategyNames = new String[strategies.length];
        strategyClasses = strategies;
        policyClasses = policies;
        extractPoliciesAndStrategiesNames(policies, strategies);
    }

    /**
     * Extracts strategies and policy names,
     *  and store them in dedicated containers.
     * @param policies Policies of the bundle
     * @param strategies Strategies of the bundle
     */
    private void extractPoliciesAndStrategiesNames(
            final Class<? extends IPolicy<?>>[] policies,
            final Class<? extends IStrategy<?>>[] strategies) {
        for (int i = 0; i < strategies.length; i++) {
            this.strategyNames[i] = strategies[i].getName();
        }
        for (int i = 0; i < policies.length; i++) {
            this.policyNames[i] = policies[i].getName();
        }

    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @return the policies
     */
    public String[] getPolicyNames() {
        return policyNames;
    }

    /**
     * @return the strategies
     */
    public String[] getStrategyNames() {
        return strategyNames;
    }

    /**
     * @return the strategyClasses
     */
    public Class<?>[] getStrategyClasses() {
        return strategyClasses;
    }

    /**
     * @param strategyClasses the strategyClasses to set
     */
    public void setStrategyClasses(final Class<?>[] strategyClasses) {
        this.strategyClasses = strategyClasses;
    }

    /**
     * @return the policyClasses
     */
    public Class<?>[] getPolicyClasses() {
        return policyClasses;
    }

    /**
     * @param policyClasses the policyClasses to set
     */
    public void setPolicyClasses(final Class<?>[] policyClasses) {
        this.policyClasses = policyClasses;
    }

}
