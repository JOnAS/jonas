/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007,2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.cmi.internal;

import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;
import org.ow2.cmi.osgi.ILoadBalancingService;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

/**
 * Listener for services that export Load balancing policies and strategies.
 *
 * @author eyindanga
 *
 */
public class LoadBalancingListener implements ServiceListener {

    /**
     * Logger.
     */
    private static final Log LOGGER = LogFactory
            .getLog(LoadBalancingListener.class);

    /**
     * Current cmi service.
     */
    private CmiServicePolicyStrategyManager policyStrategyManager = null;

    /**
     * Contructor without fields.
     */
    public LoadBalancingListener() {

    }

    /**
     * Constructor with current cmi service.
     *
     * @param cmiPolicyStrategyManager
     *            the policy strategy manager that will be used by the listener.
     *            This manager will be notified all events from a service that export
     *            policies/strategies.
     *
     */
    public LoadBalancingListener(
            final CmiServicePolicyStrategyManager cmiPolicyStrategyManager) {
        this.policyStrategyManager = cmiPolicyStrategyManager;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.osgi.framework.ServiceListener#serviceChanged(org.osgi.framework.ServiceEvent)
     */
    public void serviceChanged(final ServiceEvent arg0) {
        ServiceReference srvRef = arg0.getServiceReference();
        String location = srvRef.getBundle().getLocation();
        switch (arg0.getType()) {
        case ServiceEvent.REGISTERED:
            ILoadBalancingService loadBalancingService = (ILoadBalancingService) srvRef.getBundle().getBundleContext().getService(srvRef);
            LOGGER.debug("Sending new load balancing archive '{0}' step 1: handling new archive registration.",
                            location);
            try {
                policyStrategyManager.addPolicyStrategyBundle(location, loadBalancingService.getPolicies(),
                        loadBalancingService.getStrategies());
            } catch (CmiServiceException e) {
                LOGGER.error("Unable to load archive for load balacing policies/strategies located at {0} because: {1}",
                                location, e);
            }

            break;
        case ServiceEvent.MODIFIED:
            LOGGER.debug("Registering modifications for load balacing policies/strategies archive located at: {0}",
                            location);
            break;
        case ServiceEvent.UNREGISTERING:
            LOGGER.debug("Unregistering load balacing policies/strategies archive located on: {0}",
                            location);
            try {
                policyStrategyManager.removePolicyStrategyBundle(srvRef
                        .getBundle().getLocation());
            } catch (Exception e) {
                // TODO Auto-generated catch block
                LOGGER.error("Unable to unload archive for load balacing policies/strategies located at '"
                                + location + "' because " + e);
            }
            break;

        }

    }
}
