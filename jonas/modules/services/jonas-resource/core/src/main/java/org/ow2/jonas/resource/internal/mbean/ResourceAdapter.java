/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.resource.internal.mbean;

import java.io.File;
import java.net.URL;
import java.util.Properties;

import org.ow2.jonas.lib.management.javaee.J2EEManagedObject;


/**
 * MBean class for ResourceAdapter management
 *
 * @author Michel Bruno and Guillaume Riviere<br>
 * @contributor Michel-Ange Anton (add 'filename', 'inEarCase', 'earURL' properties)<br>
 * @contributor Adriana Danes JSR 77 (J2EE Management Standard)
 */
public class ResourceAdapter extends J2EEManagedObject {

    // JSR 77
    /**
     * JCA Resource Object Name
     */
    private String jcaResourceObjectName = null;

    /**
     * Properties associated with the RAR
     */
    private Properties prop = null;

    /**
     * Jndi name
     */
    private String jndiName = null;

    /**
     * Filename of the RAR
     */
    private String filename = null;

    /**
     * Boolean if the RAR is in an EAR file
     */
    private boolean inEarCase = false;

    /**
     * URL of the EAR if used
     */
    private URL earURL = null;

    /**
     * Spec version
     */
    private String specVersion = null;

    /**
      * The JNDI name of the linked rar resource if any, null otherwise.
      */
    private String rarLink = null;

    /**
     * RA Classname (only for 1.5 connectors)
     */
    private String resourceAdapterClassname = null;

    /**
     * Constructor
     * @param objectName String of J2EE Managed Object
     * @param prop Properties of RAR
     * @param jndiName String of RAR
     * @param filename String of RAR filename
     * @param inEarCase boolean if in an EAR file
     * @param earURL URL of EAR file
     * @param specVersion String of spec version
     */
    public ResourceAdapter(final String objectName, final Properties prop, final String jndiName, final String filename,
                           final boolean inEarCase, final URL earURL, final String specVersion, final String rarLink) {
        super(objectName);
        this.prop = prop;
        this.jndiName =  jndiName;
        // To avoid trouble between OS (Unix <-> Windows)
        try {
            this.filename = (new File(filename)).toURL().getPath();
        } catch (Exception e) {
            this.filename = filename;
        }
        this.inEarCase = inEarCase;
        this.earURL = earURL;
        this.specVersion = specVersion;
        this.rarLink = rarLink;
    }

    /**
     * return the Adaptor Properties
     * @return Properties adaptor properties
     */
    public Properties getProperties() {
        return prop;
    }

    /**
     * return the jndi name
     * @return String jndi name
     */
    public String getJndiName() {
        return jndiName;
    }

    /**
     * Accessor the filename of the resource adapter.
     * @return The filename
     */
    public String getFileName() {
        return filename;
    }

    /**
     * Accessor the flag indicating if the resource adapter is in Ear.
     * @return Flag if this resource adapter is in Ear
     */
    public boolean getInEarCase() {
        return inEarCase;
    }

    /**
     * Accessor the URL of the Ear if the resource adapter is in Ear.
     * @return The URL of the Ear or null
     */
    public URL getEarURL() {
        return earURL;
    }

    /**
     * return the jcaResourceObjectName
     * @return String jcaResourceObjectName
     */
    public String getJcaResource() {
        return jcaResourceObjectName;
    }

    /**
     * return the specVersion
     * @return String specVersion
     */
    public String getSpecVersion() {
        return specVersion;
    }

    /**
     * @return The JNDI name of the linked rar resource if any, null otherwise.
     */
    public String getRarLink() {
        return rarLink;
    }
    
    /**
     * set the jcaResourceObjectName
     * @param jcaResourceObjectName to set
     */
    public void setJcaResource(final String jcaResourceObjectName) {
        this.jcaResourceObjectName = jcaResourceObjectName;
    }

    /**
     * @return Returns the RA classname.
     */
    public String getResourceAdapterClassname() {
        return resourceAdapterClassname;
    }

    /**
     * @param resourceAdapterClassname the ra classname.
     */
    public void setResourceAdapterClassname(final String resourceAdapterClassname) {
        this.resourceAdapterClassname = resourceAdapterClassname;
    }
}
