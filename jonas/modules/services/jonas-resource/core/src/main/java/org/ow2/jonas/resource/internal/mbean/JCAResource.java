/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.resource.internal.mbean;

// Java imports
import java.util.ArrayList;

import org.ow2.jonas.lib.management.javaee.J2EEResource;


/**
 * MBean class for JCA Resource Management
 *
 * @author Adriana Danes JSR 77 (J2EE Management Standard)
 *
 */
public class JCAResource extends J2EEResource {

    // JSR 77
    /**
     * Array list of Connection Factories
     */
    private ArrayList connectionFactories = new ArrayList();
    /**
     * Array list of Admin Objects
     */
    private ArrayList adminObjects = new ArrayList();
    /**
     * Array list of ActivationSpecs
     */
    private ArrayList activationSpecs = new ArrayList();

    /**
     * Constructor
     * @param objectName String of object name
     */
    public JCAResource(String objectName) {
        super(objectName);
    }

    /**
     * Get the Activationspec names
     * @return String [] list of Activationspec names
     */
    public String [] getActivationSpecs() {
//        return activationSpecs;
        return ((String[]) activationSpecs.toArray(new String[activationSpecs.size()]));
    }

    /**
     * Get the AdminObject names
     * @return String [] list of AdminObject names
     */
    public String [] getAdminObjects() {
//        return adminObjects;
        return ((String[]) adminObjects.toArray(new String[adminObjects.size()]));
    }

    /**
     * get the ConnectionFactory names
     * @return String [] list of ConnectionFactory names
     */
    public String [] getConnectionFactories() {
//        return connectionFactories;
        return ((String[]) connectionFactories.toArray(new String[connectionFactories.size()]));
    }

    /**
     * set the AdminObject name
     * @param adminObjectName String of AdminObject name
     */
    public void setAdminObjects(String adminObjectName) {
        adminObjects.add(adminObjectName);
    }

    /**
     * set the ActivationSpec name
     * @param activationSpecsObjectName String of activationSpecs name
     */
    public void setActivationSpecs(String activationSpecsObjectName) {
        activationSpecs.add(activationSpecsObjectName);
    }
    /**
     * set the ConnectionFactory name
     * @param connectionFactoryObjectName String of ConnectionFactory name
     */
    public void setConnectionFactory(String connectionFactoryObjectName) {
        connectionFactories.add(connectionFactoryObjectName);
    }
}
