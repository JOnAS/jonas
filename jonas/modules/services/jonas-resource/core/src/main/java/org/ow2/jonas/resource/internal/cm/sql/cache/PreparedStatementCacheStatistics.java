/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.resource.internal.cm.sql.cache;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Statistics about the prepared statement caches
 * There's one instance of this class per datasource
 * @author pelletib
 *
 */
public class PreparedStatementCacheStatistics {

    /**
     * Count of the number of times that the statement cache was accessed.
     */
    private AtomicLong prepStmtCacheAccessCount = new AtomicLong();

    /**
     * Count of the number of statements added to the statement cache.
     */
    private AtomicLong prepStmtCacheAddCount = new AtomicLong();

    /**
     * Number of prepared statements currently cached in the statement cache.
     */
    private AtomicLong prepStmtCacheCurrentSize = new AtomicLong();

    /**
     * Cumulative count of statements discarded from the cache.
     */
    private AtomicLong prepStmtCacheDeleteCount = new AtomicLong();

    /**
     * Cumulative count of the number of times that statements from the cache were used.
     */
    private AtomicLong prepStmtCacheHitCount = new AtomicLong();

    /**
     * Number of times that a statement request could not be satisfied with a statement from the cache.
     */
    private AtomicLong prepStmtCacheMissCount = new AtomicLong();

    // increment methods (atomic)
    public void incrementPrepStmtCacheAccessCount() {
        prepStmtCacheAccessCount.incrementAndGet();
    }

    public void incrementPrepStmtCacheAddCount() {
        prepStmtCacheAddCount.incrementAndGet();
    }

    public void incrementPrepStmtCacheCurrentSize() {
        prepStmtCacheCurrentSize.incrementAndGet();
    }

    public void decrementPrepStmtCacheCurrentSize() {
        prepStmtCacheCurrentSize.decrementAndGet();
    }

    public void incrementPrepStmtCacheDeleteCount() {
        prepStmtCacheDeleteCount.incrementAndGet();
    }

    public void incrementPrepStmtCacheHitCount() {
        prepStmtCacheHitCount.incrementAndGet();
    }

    public void incrementPrepStmtCacheMissCount() {
        prepStmtCacheMissCount.incrementAndGet();
    }

    // getters / setters
    public long getPrepStmtCacheAccessCount() {
        return prepStmtCacheAccessCount.get();
    }

    public void setPrepStmtCacheAccessCount(final long prepStmtCacheAccessCount) {
        this.prepStmtCacheAccessCount.set(prepStmtCacheAccessCount);
    }

    public long getPrepStmtCacheAddCount() {
        return prepStmtCacheAddCount.get();
    }

    public void setPrepStmtCacheAddCount(final long prepStmtCacheAddCount) {
        this.prepStmtCacheAddCount.set(prepStmtCacheAddCount);
    }

    public long getPrepStmtCacheCurrentSize() {
        return prepStmtCacheCurrentSize.get();
    }

    public void setPrepStmtCacheCurrentSize(final long prepStmtCacheCurrentSize) {
        this.prepStmtCacheCurrentSize.set(prepStmtCacheCurrentSize);
    }

    public long getPrepStmtCacheDeleteCount() {
        return prepStmtCacheDeleteCount.get();
    }

    public void setPrepStmtCacheDeleteCount(final long prepStmtCacheDeleteCount) {
        this.prepStmtCacheDeleteCount.set(prepStmtCacheDeleteCount);
    }

    public long getPrepStmtCacheHitCount() {
        return prepStmtCacheHitCount.get();
    }

    public void setPrepStmtCacheHitCount(final long prepStmtCacheHitCount) {
        this.prepStmtCacheHitCount.set(prepStmtCacheHitCount);
    }

    public long getPrepStmtCacheMissCount() {
        return prepStmtCacheMissCount.get();
    }

    public void setPrepStmtCacheMissCount(final long prepStmtCacheMissCount) {
        this.prepStmtCacheMissCount.set(prepStmtCacheMissCount);
    }

}
