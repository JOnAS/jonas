/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Eric Hardesty
 * Contributor(s): ______________________________________.
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.resource.internal;

import java.util.Timer;

import javax.resource.spi.BootstrapContext;
import javax.resource.spi.UnavailableException;
import javax.resource.spi.work.WorkManager;
import javax.resource.spi.XATerminator;

/**
 * This class implements the BootstrapContext interface of the Connector Architecture
 * 1.5 specification.
 */
public class ResourceBootstrapContext implements BootstrapContext {

    /**
     * Work Manager
     */
    private WorkManager wrkMgr = null;
    /**
     * XATerminator object
     */
    private XATerminator xaTerm = null;

    /**
     * Constructor
     * @param wm WorkManager
     * @param xa XATerminator
     */
    public ResourceBootstrapContext(WorkManager wm, XATerminator xa) {
        wrkMgr = wm;
        xaTerm = xa;
    }


    /**
     * Create a timer for use by the Resource Adapter
     * @return Timer object
     * @throws UnavailableException if a Timer instance is unavailable
     */
    public Timer createTimer() throws UnavailableException {
        return new Timer(true);
    }

    /**
     * Return the associated WorkManager
     * @return WorkManger object
     */
    public WorkManager getWorkManager() {
        return wrkMgr;
    }

    /**
     * Return an XATerminator
     * @return XATerminator object
     */
    public XATerminator getXATerminator() {
        return xaTerm;
    }
}