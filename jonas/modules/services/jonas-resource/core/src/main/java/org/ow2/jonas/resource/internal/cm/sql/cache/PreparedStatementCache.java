/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.resource.internal.cm.sql.cache;

import java.sql.SQLException;

import org.ow2.jonas.resource.internal.cm.sql.PreparedStatementWrapper;

/**
 * Prepared Statement Cache Interface
 * @author pelletib
 *
 */
public interface PreparedStatementCache {

    /**
     * Policy : list
     */
    public final String CACHE_POLICY_LIST="list";

    /**
     * Policy : map
     */
    public final String CACHE_POLICY_MAP="map";

    /**
     * Add an entry in the cache
     * @param psw : wrapped prepared statement to add
     * @throws CacheException no more space in cache
     * @throws SQLException
     */
    public void put(PreparedStatementWrapper psw) throws CacheException, SQLException;

    /**
     * Retrieve an entry in the cache
     * @param psw : parameters of the prepared statement to retrieve
     * @return wrapped prepared statement or null if not found
     */
    public PreparedStatementWrapper get(PreparedStatementWrapper psw);

    /**
     * @return Get the current cache size.
     */
    public int getPstmtCacheSize();

    /**
     * @return Get the max cache size.
     */
    public int getMaxPstmtCacheSize();

    /**
     * Set the max cache size
     * @param maxSize The max size to set.
     */
    public void setMaxPstmtCacheSize(final int maxSize);

    /**
     * @param prefix indentation
     * @return a string with a cache state (dump)
     */
    public String getState(String prefix);

    /**
     * Destroy the cache entries
     */
    public void destroy();

    /**
     * Close logically the cache entries
     */
    public void close();

    /**
     * Close really the cache entries
     */
    public void reallyClose();

}
