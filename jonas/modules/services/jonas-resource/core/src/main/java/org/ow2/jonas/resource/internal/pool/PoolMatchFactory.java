/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * ObjectWeb Connector: an implementation of JCA Sun specification along
 *                      with some extensions of this specification.
 * Copyright (C) 2001-2002 France Telecom R&D - INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Based on PoolMatchFactory in ObjectWeb common
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 *
 */

/**
 * Package definition.
 *
 * Author: E. Hardesty
 */
package org.ow2.jonas.resource.internal.pool;

import java.util.Set;
import javax.resource.spi.ValidatingManagedConnectionFactory;

/**
 * The interface <b>PoolMatchFactory</b> defines the object used by a Pool to
 * test if a given resource of a Pool matches with the hints passed with the
 * Pool getResource method. It also allows such a Pool to allocate a
 * Object as needed, conforming to the passed hints.
 */
public interface PoolMatchFactory {
        /**
         * <b>createResource</b> creates a new Object.
         * @param hints        The "properties" that the created Object should
         *                                conform to.
         * @return                The created Object.
         * @throws Exception if an error occurs
         */
        Object createResource(Object hints) throws Exception;

    /**
     * <b>matchResource</b> tests if a given resource of a Pool matches with
         * the hints passed with the Pool getResource method.
         * @param pr        The Object to test its matching with some
         *                                "properties" specified by hints.
         * @param hints        The "properties" that the Object specified by pr
         *                                should match.
         * @return                <b>true</b> if the pr Object matches the hints
         *                                "properties".
         */
        boolean matchResource(Object pr, Object hints);

    /**
     * <b>matchResource</b> tests if a given resource of a Pool matches with
         * the hints passed with the Pool getResource method.
         * @param res          A set of Objects to test if matching with some
         *                                "properties" specified by hints.
         * @param hints        The "properties" that the Object specified by pr
         *                                should match.
         * @return                <b>Object</b> that matches the hints "properties".
         *                              or null if no match.
         * @exception  Exception  Description of Exception
         */
        Object matchResource(Set res, Object hints) throws Exception;
    /**
     * <b>releaseResource</b> make sure that any cleanup needed for this entry is done
         * @param pr          The Object to release
         * @exception  Exception  Description of Exception
         */
        void releaseResource(Object pr) throws Exception;

    /**
     * @return the ValidatingManagedConnectionFactory if the RA implements it
     */
    public ValidatingManagedConnectionFactory getValidatingMCFactory();
}


