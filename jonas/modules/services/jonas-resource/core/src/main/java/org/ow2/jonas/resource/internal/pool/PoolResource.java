/**
 * ObjectWeb Connector: an implementation of JCA Sun specification along
 *                      with some extensions of this specification.
 * Copyright (C) 2001-2002 France Telecom R&D - INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Release: 0.1
 *
 * Contact: jorm@objectweb.org
 *
 * Author: S. Chassande-Barrioz, P. Dechamboux
 *
 */

/**
 * Package definition.
 */
package org.ow2.jonas.resource.internal.pool;

/**
 * Import clauses: one for each external Java definition used. Do not use
 * clauses in the form "import package.*".
 */


/**
 * The interface <b>PoolResource</b> defines the minimal behaviour of a
 * resource managed by a Pool.
 */
public interface PoolResource {
    /**
     * <b>destroy</b> is called when this resource is removed from a Pool.
     *
     * @throws Exception if an error occurs
     */
    void destroy() throws Exception;
}


