/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.resource.internal.cm;

import javax.resource.spi.ConnectionRequestInfo;
import javax.security.auth.Subject;

/**
 *  This class describes the context of a getConnection call: - the mcf to
 *  request a ManagedConnection - the security subject - the
 *  ConnectionRequestInfo transmitted with the request
 *
 *@author     sebastien.chassande@inrialpes.fr
 */
public class ConnectionResourceHint {
    /**
     * Subject object
     */
    protected Subject subject;
    /**
     * ConnectionRequestInfo object
     */
    protected ConnectionRequestInfo cxRequestInfo;

    /**
     * Constructor for the ConnectionResourceHint object
     * @param s Subject to associate
     * @param cri ConnectionRequestInfo
     */
    public ConnectionResourceHint(Subject s, ConnectionRequestInfo cri) {
            subject = s;
            cxRequestInfo = cri;
    }

    /**
     * Clear the current contents
     */
    public void clear() {
            subject = null;
            cxRequestInfo = null;
    }

    /**
     *
     * @return String value of object
     */
    public String toString() {
            return "subject=" + subject + " / cri=" + cxRequestInfo;
    }
}
