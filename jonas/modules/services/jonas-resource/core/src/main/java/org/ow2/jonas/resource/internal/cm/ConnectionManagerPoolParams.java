/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.resource.internal.cm;


/**
 *
 * @author Eric Hardesty
 * Contributor(s):
 *
 */
public class ConnectionManagerPoolParams {

    /**
     *  The number of seconds per minute
     */
    static final int SECS_PER_MIN = 60;
    /**
     *  The default pstmts per ManagedConnection.
     */
    static final int INIT_PSTMT_SIZE = 10;
    /**
     *  The default sampling time of the pool
     */
    static final int INIT_SAMPLING_TIME = 30;

    /**
     *  The default cache policy for pstmt
     */
    static final String INIT_CACHE_POLICY = "List";

    /**
     * Initial pool size
     */
    private int poolInit = 0;
    /**
     * Minimum pool size
     */
    private int poolMin = 0;
    /**
     * Maximum pool size, -1 unlimited
     */
    private int poolMax = -1;
    /**
     * Maximium pool age in seconds of a connection, 0 unlimited
     */
    private long poolMaxAge = 0L;
    /**
     * Maximium pool age in minutes of a connection, 0 unlimited
     */
    private int poolMaxAgeMinutes = 0;
    /**
     * Maximium open time in minutes of a connection, 0 unlimited
     */
    private int poolMaxOpentime = 0;
    /**
     * Maximum number of threads waiting for connection
     */
    private int poolMaxWaiters = 0;
    /**
     * Maximum time to wait for a connection
     */
    private int poolMaxWaittime = 0;
    /**
     * Pool statistics sampling period in seconds
     */
    private int poolSamplingPeriod = INIT_SAMPLING_TIME;

    /**
     * Maximum size of the PreparedStatement cache, -1 to disable
     */
    private int pstmtMax = INIT_PSTMT_SIZE;

    /**
     * Policy of the PreparedStatement cache
     */
    private String pstmtCachePolicy = INIT_CACHE_POLICY;


    /**
     * Jdbc connection level
     */
    private int jdbcConnLevel = 0;
    /**
     * Jdbc sql string to send to a connection if level requires it
     */
    private String jdbcConnTestStmt = "";

    /**
     * True if there is a JDBC connection to manage
     */
    private boolean jdbcConnSetUp = false;

    /**
     * Default Constructor
     *
     */
    public ConnectionManagerPoolParams() {
    }

    /**
     * Constructor to build new object based on another set of pool params
     * @param cmpp ConnectionManagerPoolParams to initialize this object with
     */
    public ConnectionManagerPoolParams(final ConnectionManagerPoolParams cmpp) {
        poolInit = cmpp.getPoolInit();
        poolMin = cmpp.getPoolMin();
        poolMax = cmpp.getPoolMax();
        poolMaxAge = cmpp.getPoolMaxAge();
        poolMaxAgeMinutes = cmpp.getPoolMaxAgeMinutes();
        poolMaxOpentime = cmpp.getPoolMaxOpentime();
        poolMaxWaiters = cmpp.getPoolMaxWaiters();
        poolMaxWaittime = cmpp.getPoolMaxWaittime();
        poolSamplingPeriod = cmpp.getPoolSamplingPeriod();
        pstmtMax = cmpp.getPstmtMax();
        jdbcConnLevel = cmpp.getJdbcConnLevel();
        jdbcConnTestStmt = cmpp.getJdbcConnTestStmt();
        jdbcConnSetUp = cmpp.isJdbcConnSetUp();
    }
    /**
     * Return the pool init size
     * @return int initial size of the pool
     */
    public int getPoolInit() {
        return poolInit;
    }
    /**
     * Set the pool init size
     * @param val pool initial size
     */
    public void setPoolInit(final int val) {
        poolInit = val;
    }

    /**
     * Return the pool min size
     * @return int minimum size of the pool
     */
    public int getPoolMin() {
        return poolMin;
    }
    /**
     * Set the pool minimum size
     * @param val pool minimum size
     */
    public void setPoolMin(final int val) {
        poolMin = val;
    }

    /**
     * Return the pool maximum size
     * @return int maximum size of the pool
     */
    public int getPoolMax() {
        return poolMax;
    }
    /**
     * Set the pool maximum size
     * @param val pool maximum size
     */
    public void setPoolMax(final int val) {
        poolMax = val;
    }

    /**
     * Return the pool maximum age in seconds
     * @return long maximum age of a connection in the pool
     */
    public long getPoolMaxAge() {
        return poolMaxAge;
    }
    /**
     * Set the pool maximum age value
     * @param val pool maximum age value
     */
    public void setPoolMaxAge(final long val) {
        poolMaxAge = val;
    }

    /**
     * Return the pool maximum age in minutes
     * @return int maximum age of a connection in the pool
     */
    public int getPoolMaxAgeMinutes() {
        return poolMaxAgeMinutes;
    }
    /**
     * Set the pool maximum age value
     * @param val pool maximum age value
     */
    public void setPoolMaxAgeMinutes(final int val) {
        poolMaxAgeMinutes = val;
    }

    /**
     * Return the pool maximum open time in minutes
     * @return int maximum open time of a connection in the pool
     */
    public int getPoolMaxOpentime() {
        return poolMaxOpentime;
    }
    /**
     * Set the pool maximum open time value
     * @param val pool maximum open time value
     */
    public void setPoolMaxOpentime(final int val) {
        poolMaxOpentime = val;
    }

    /**
     * Return the pool maximum waiters size
     * @return int maximum waiters of the pool
     */
    public int getPoolMaxWaiters() {
        return poolMaxWaiters;
    }
    /**
     * Set the pool maximum waiters size
     * @param val pool maximum waiters size
     */
    public void setPoolMaxWaiters(final int val) {
        poolMaxWaiters = val;
    }

    /**
     * Return the pool maximum waiter time
     * @return int maximum waiter time of the pool
     */
    public int getPoolMaxWaittime() {
        return poolMaxWaittime;
    }
    /**
     * Set the pool maximum waiter time
     * @param val pool maximum waiters time
     */
    public void setPoolMaxWaittime(final int val) {
        poolMaxWaittime = val;
    }

    /**
     * Return the pool sampling period
     * @return int pool sampling period
     */
    public int getPoolSamplingPeriod() {
        return poolSamplingPeriod;
    }
    /**
     * Set the pool sampling period
     * @param val pool sampling period
     */
    public void setPoolSamplingPeriod(final int val) {
        poolSamplingPeriod = val;
    }

    /**
     * Return the maximum PreparedStatement cache
     * @return int maximum PreparedStatement cache
     */
    public int getPstmtMax() {
        return pstmtMax;
    }
    /**
     * Set the maximum PreparedStatement cache
     * @param val maximum PreparedStatement cache
     */
    public void setPstmtMax(final int val) {
        pstmtMax = val;
    }

    /**
     * Return the cache policy of PreparedStatement cache
     * @return String PreparedStatement cache policy
     */
    public String getPstmtCachePolicy() {
        return pstmtCachePolicy;
    }
    /**
     * Set the PreparedStatement cache policy
     * @param val PreparedStatement cache policy
     */
    public void setPstmtCachePolicy(final String val) {
        pstmtCachePolicy = val;
    }

    /**
     * Return the JDBC connection level
     * @return int JDBC connection level
     */
    public int getJdbcConnLevel() {
        return jdbcConnLevel;
    }
    /**
     * Set the JDBC connection level
     * @param val JDBC connection level
     */
    public void setJdbcConnLevel(final int val) {
        jdbcConnLevel = val;
    }

    /**
     * Return the JDBC connection test SQL string
     * @return String JDBC connection test SQL string
     */
    public String getJdbcConnTestStmt() {
        return jdbcConnTestStmt;
    }
    /**
     * Set the JDBC connection test SQL string
     * @param val JDBC connection test SQL string
     */
    public void setJdbcConnTestStmt(final String val) {
        jdbcConnTestStmt = val;
    }

    /**
     * @return true if there is a JDBC connection to manage
     */
    public boolean isJdbcConnSetUp() {
        return jdbcConnSetUp;
    }

    public void setJdbcConnSetUp(final boolean val) {
        jdbcConnSetUp = val;
    }

    /**
     * Output pool information
     * @return String representing the pool
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("poolInit=");
        sb.append(poolInit);
        sb.append("\npoolMin=");
        sb.append(poolMin);
        sb.append("\npoolMax=");
        sb.append(poolMax);
        sb.append("\npoolMaxAge=");
        sb.append(poolMaxAge);
        sb.append("\npoolMaxAgeMinutes=");
        sb.append(poolMaxAgeMinutes);
        sb.append("\npoolMaxOpentime=");
        sb.append(poolMaxOpentime);
        sb.append("\npoolMaxWaiters=");
        sb.append(poolMaxWaiters);
        sb.append("\npoolMaxWaittime=");
        sb.append(poolMaxWaittime);
        sb.append("\npoolSamplingPeriod=");
        sb.append(poolSamplingPeriod);
        sb.append("\njdbcConnSetUp=");
        sb.append(jdbcConnSetUp);
        sb.append("\npstmtMax=");
        sb.append(pstmtMax);
        sb.append("\njdbcConnLevel=");
        sb.append(jdbcConnLevel);
        sb.append("\njdbcConnTestStmt=");
        sb.append(jdbcConnTestStmt);
        sb.append("\n");
        return sb.toString();
    }
}
