/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.resource.internal.mbean;

// JOnAS imports
import org.ow2.jonas.lib.management.javaee.J2EEManagedObject;

/**
 * MBean class for JCA ManagedConnection Factory Management
 *
 * @author Adriana Danes JSR 77 (J2EE Management Standard)
 *
 */
public class JCAManagedConnectionFactory extends J2EEManagedObject {

    /**
     * Constructor
     * @param objectName String of J2EE ManagedObject name
     */
    public JCAManagedConnectionFactory(String objectName) {
        super(objectName);
    }
}
