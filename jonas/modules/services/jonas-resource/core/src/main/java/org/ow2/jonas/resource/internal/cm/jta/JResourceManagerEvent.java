/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.resource.internal.cm.jta;

import javax.resource.ResourceException;
import javax.resource.spi.ManagedConnection;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.Transaction;
import javax.transaction.xa.XAResource;

import org.ow2.jonas.resource.internal.cm.ManagedConnectionInfo;
import org.ow2.jonas.tm.Enlistable;

import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 *  This class implements {@link Enlistable}. An instance of this object is
 *  also an event transmited by the Resource adapter manager to the user
 *  transaction. This event is linked to a {@link ManagedConnection}, more exatcly this
 *  implementation is linked to {@link ManagedConnectionInfo} instance.
 *
 *@author     sebastien.chassande@inrialpes.fr
 */
public class JResourceManagerEvent implements Enlistable {

    /**
     *  The ManagedConnectionInfo instance which represents the physical connection
     */
    private ManagedConnectionInfo mci = null;

    /**
     *  The Logger instance where messages are written.
     */
    private Logger trace = null;

    /**
     *  Set if the RME is valid to call enlist Resource
     *  Issue with JOTM and one thread calling ConnectionOpened, but a
     *   different thread calling Closed or Error and not enough information
     *   in JOTM to keep information straight.
     */
    private boolean isValid = false;

    /**
     *  This constructor permits to specify the MCInfp corresponding to the
     *  ManagedConnection (the physical connection).
     *
     * @param mci ManagedConnectionInfo
     * @param trace Logger
     */
    public JResourceManagerEvent(ManagedConnectionInfo mci, Logger trace) {
        this.mci = mci;
        this.trace = trace;
    }


    /**
     * Check if equals
     * @param o Object to test equality
     * @return boolean if JResourceManagerEvent objets are equal
     */
    public boolean equals(Object o) {
        return (o instanceof JResourceManagerEvent) && mci.equals(((JResourceManagerEvent) o).mci);
    }


    /**
     *  This method is in charge of the enlisting of the managedConnection linked
     *  to this instance, in the transaction specified in parameter.
     * @param tx TransactionConnection
     * @throws SystemException if an Exception occurs
     */
    public void enlistConnection(Transaction tx) throws SystemException {
        try {
            if (isValid) {
                if (trace.isLoggable(BasicLevel.DEBUG)) {
                    trace.log(BasicLevel.DEBUG, "Enlist the XA Resource " + mci.getXAResource()
                            + " in Tx:" + tx);
                }
                tx.enlistResource(mci.getXAResource());
            }
        } catch (RollbackException rbe) {
            throw new SystemException(rbe.getMessage());
        } catch (ResourceException re) {
            throw new SystemException(re.getMessage());
        }
    }

    /**
     * Used when going out of a Stateful beans
     */
    public void delistConnection(Transaction tx) throws SystemException {
        try {
            if (isValid) {
                if (trace.isLoggable(BasicLevel.DEBUG)) {
                    trace.log(BasicLevel.DEBUG, "Delist the XA Resource " + mci.getXAResource()
                            + " from Tx:" + tx);
                }
                tx.delistResource(mci.getXAResource(), XAResource.TMSUCCESS);
            }
        } catch (Exception e) {
            trace.log(BasicLevel.ERROR, "Cannot delist Resource:" + e);
        }
    }

    /**
     * @return the isValid
     */
    public boolean isValid() {
        return isValid;
    }


    /**
     * @param isValid the isValid to set
     */
    public void setValid(boolean isValid) {
        this.isValid = isValid;
    }
}
