/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.resource.internal;

import org.ow2.jonas.resource.ResourceService;
import org.ow2.util.ee.deploy.api.deployable.IDeployable;
import org.ow2.util.ee.deploy.api.deployable.RARDeployable;
import org.ow2.util.ee.deploy.api.deployer.DeployerException;
import org.ow2.util.ee.deploy.impl.deployer.AbsDeployer;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

/**
 * This deployer will deploy RAR module.
 * @author Florent BENOIT
 */
public class RARDeployer extends AbsDeployer<RARDeployable> {

    /**
     * Logger.
     */
    private Log logger = LogFactory.getLog(RARDeployer.class);

    /**
     * Resource service used by this deployer.
     */
    private ResourceService resourceService = null;

    /**
     * Undeploy the given RAR.
     * @param deployable the deployable to remove.
     * @throws DeployerException if the RAR is not undeployed.
     */
    @Override
    public void doUndeploy(final IDeployable deployable) throws DeployerException {
        logger.info("Undeploying {0}", deployable.getShortName());

        // Undeploy the RAR file
        try {
            resourceService.unDeployRar(getFile(deployable).getAbsolutePath());
        } catch (Exception e) {
            throw new DeployerException("Cannot deploy the RAR deployable '" + deployable + "'.", e);
        }
    }

    /**
     * Deploy the given RAR.
     * @param deployable the deployable to add.
     * @throws DeployerException if the RAR is not deployed.
     */
    @Override
    public void doDeploy(final IDeployable deployable) throws DeployerException {
        logger.info("Deploying {0}", deployable.getShortName());

        // Deploy the RAR file
        try {
            resourceService.deployRar(getFile(deployable).getPath());
        } catch (Exception e) {
            throw new DeployerException("Cannot deploy the RAR deployable '" + deployable + "'.", e);
        }
    }

    /**
     * Checks if the given deployable is supported by the Deployer.
     * @param deployable the deployable to be checked
     * @return true if it is supported, else false.
     */
    @Override
    public boolean supports(final IDeployable deployable) {
        return RARDeployable.class.isInstance(deployable);
    }

    /**
     * Sets the RAR service.
     * @param resourceService RAR service.
     */
    public void setResourceService(final ResourceService resourceService) {
        this.resourceService = resourceService;
    }

}
