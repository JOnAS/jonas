/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.resource.internal;

import javax.naming.Reference;

import org.ow2.jonas.resource.ResourceService;


/**
 * {@link ResourceService} constants holder interface.
 * Used for {@link Reference} filling.
 * @author Guillaume Sauthier
 */
public interface ResourceServiceConstants {

    /**
     * Jndiname property name
     */
    public static final String JNDI_NAME = "jndiname";

    /**
     * Rar object property name
     */
    public static final String RAR_OBJNAME = "rarobjname";

    /**
     * Factory offset property name
     */
    public static final String FACTORY_OFFSET = "factoryoffset";

    /**
     * Factory type property name
     */
    public static final String FACTORY_TYPE = "factorytype";

    /**
     * Rar filename property name
     */
    public static final String RAR_FILENAME = "rarfilename";

    /**
     * Jndiname link property name
     */
    public static final String LNK_JNDI_NAME = "lnkjndiname";

    /**
     * Link Rar filename property name
     */
    public static final String LNK_RAR_FILENAME = "lnkrarfilename";

    /**
     * Jonas ra.xml property name
     */
    public static final String JONAS_RA_XML = "jonasraxml";

    /**
     * ra.xml property name
     */
    public static final String RA_XML = "raxml";

}