/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.resource.internal.cm.sql.cache;

import java.sql.SQLException;

import org.objectweb.util.monolog.api.Logger;
import org.ow2.jonas.resource.internal.cm.sql.PreparedStatementWrapper;

/**
 * Abstract class for the prepared statement cache implementation
 * @author pelletib
 *
 */
public abstract class AbstractPreparedStatementCacheImpl implements PreparedStatementCache {

    /**
     * Logger
     */
    protected static Logger cacheLogger = null;

    /**
     * max cache size
     */
    protected int maxPstmtCacheSize = 0;

    /**
     * current cache size
     */
    protected int pstmtCacheSize = 0;

    /**
     * Statistics
     */
    protected PreparedStatementCacheStatistics stats;

    /**
     * Local statistic key
     */
    public static String LOCAL_STATISTIC_KEY = "local";

    /**
     * Constructor
     * @param maxCacheSize : max cache size
     * @param logger : logger
     */
    public AbstractPreparedStatementCacheImpl(final int maxCacheSize, final PreparedStatementCacheStatistics pstmtStats, final Logger logger) {
        maxPstmtCacheSize = maxCacheSize;
        cacheLogger = logger;
        stats = pstmtStats;
    }

    @Override
    abstract public PreparedStatementWrapper get(final PreparedStatementWrapper psw);

    @Override
    abstract public void put(final PreparedStatementWrapper psw) throws CacheException, SQLException;

    /**
     * Set the max pstmt cache size
     */
    final public void setMaxPstmtCacheSize(final int maxSize) {
        maxPstmtCacheSize = maxSize;
    }

    /**
     * Get the max pstmt cache size
     */
    public int getMaxPstmtCacheSize() {
        return maxPstmtCacheSize;
    }

    /**
     * Get the current pstmt cache size
     */
    @Override
    public int getPstmtCacheSize() {
        return pstmtCacheSize;
    }

    /**
     * Dump the pstmt cache state
     */
    @Override
    public abstract String getState(String prefix);

    /**
     * Destroy the cache entries
     */
    @Override
    public abstract void destroy();


    /**
     * Close really the cache entries
     */
    @Override
    public abstract void reallyClose();

    /**
     * Close logically the cache entries
     */
    @Override
    public abstract void close();

}
