/**
 * ObjectWeb Connector: an implementation of JCA Sun specification along
 *                      with some extensions of this specification.
 * Copyright (C) 2001-2002 France Telecom R&D - INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Release: 0.1
 *
 * Contact: jorm@objectweb.org
 *
 * Author: S. Chassande-Barrioz, P. Dechamboux
 *
 */
package org.ow2.jonas.resource.internal.pool;

/**
 * The interface <b>PoolProvider</b> defines the minimal behaviour of a
 * Pool Provider.
 */
public interface PoolProvider {

    /**
     * Return the Pool
     * @return Pool associated with this provider
     */
    Pool getPool();
}
