/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.resource.internal.mbean;

import java.net.URL;
import java.util.ArrayList;

import javax.management.ObjectName;

import org.ow2.jonas.lib.management.javaee.J2EEModule;
import org.ow2.jonas.lib.management.javaee.J2eeObjectName;


/**
 * MBean class for ResourceAdapterModule management
 *
 * @author  Adriana Danes JSR 77 (J2EE Management Standard)
 */
public class ResourceAdapterModule extends J2EEModule /*J2EEManagedObject*/ {

    // JSR 77
    private ArrayList resourceAdapters = new ArrayList();

    private boolean inEarCase = false;
    private URL earURL = null;
    private String fileName = null;
    private URL rarURL = null;

    /**
     * J2EEApplication MBean OBJECT_NAME in ear case
     */
    private String earON = null;

    public ResourceAdapterModule(final ObjectName objectName, final boolean inEarCase, final URL earURL) {
        super(objectName.toString());
        this.inEarCase = inEarCase;
        this.earURL = earURL;
        ObjectName earOn = null;
        if (inEarCase) {
            String domainName = objectName.getDomain();
            String serverName = objectName.getKeyProperty("J2EEServer");
            String appName = objectName.getKeyProperty("J2EEApplication");
            earOn = J2eeObjectName.J2EEApplication(domainName, serverName, appName);
        }
        if (earOn != null) {
            earON = earOn.toString();
        }
    }

    /**
     * Accessor the flag indicating if the resource adapter is in Ear.
     * @return Flag if this resource adapter is in Ear
     */
    public boolean getInEarCase() {
        return inEarCase;
    }

    /**
     * Accessor the URL of the Ear if the resource adapter is in Ear.
     * @return The URL of the Ear or null
     */
    public URL getEarURL() {
        return earURL;
    }

    public String[] getResourceAdapters() {
        return ((String[]) resourceAdapters.toArray(new String[resourceAdapters.size()]));
    }

    public void setResourceAdapter(final String resourceAdapterObjectName) {
        resourceAdapters.add(resourceAdapterObjectName);
    }
    /**
     * @return Returns the fileName.
     */
    public String getFileName() {
        return fileName;
    }
    /**
     * @param fileName The fileName to set.
     */
    public void setFileName(final String fileName) {
        this.fileName = fileName;
    }
    /**
     *
     * @return The J2EEApplication MBean OBJECT_NAME in ear case
     */
    public String getEarON() {
        return earON;
    }

    public URL getRarURL() {
        return rarURL;
    }

    public void setRarURL(final URL rarURL) {
        this.rarURL = rarURL;
    }

}
