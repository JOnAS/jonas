/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.resource.internal;

import java.sql.Connection;
import java.sql.SQLException;

import org.ow2.jonas.resource.internal.cm.ManagedConnectionInfo;


/**
 * A JOnAS Specific JDBC SQL {@link Connection}.
 * @author Guillaume Sauthier
 */
public interface IJDBCConnection extends Connection {

    /**
     * @return true if the connection is physically closed.
     * @throws SQLException
     */
    boolean isPhysicallyClosed() throws SQLException;

    /**
     * Give JOnAS specific data to the connection.
     * @param info {@link ManagedConnectionInfo}
     * @param manager the connection manager
     */
    void setJonasInfo(ManagedConnectionInfo info, SQLManager manager);

    /**
     * Use the username provided in meta-data.
     */
    void setUser();

    long getKey();
}
