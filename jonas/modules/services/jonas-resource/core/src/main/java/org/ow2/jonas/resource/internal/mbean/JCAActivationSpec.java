/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.resource.internal.mbean;

// JOnAS Imports
import java.util.List;

import org.ow2.jonas.lib.management.javaee.J2EEManagedObject;


/**
 * MBean class for JCA ActivationSpec Management
 *
 * @author Eric Hardesty
 *
 */
public class JCAActivationSpec extends J2EEManagedObject {

    // JOnAS Specific
    /**
     * Description
     */
    private String description = null;
    /**
     * Jndi name
     */
    private String jndiname = null;
    /**
     * Properties list associated with the ActivationSpec
     */
    private List prop = null;

    /**
     * MBean constructor
     * @param oName    object name of the managed object
     * @param jndiname String of ActivationSpec jndi name
     * @param description String of ActivationSpec description 
     * @param prop     Properties of the ActivationSpec required properties
     */
    public JCAActivationSpec(String oName, String jndiname, String description, List prop) {
        super(oName);
        this.jndiname =  jndiname;
        this.description =  description;
        this.prop = prop;
    }

    /**
     * return the description
     * @return String description
     */
    public String getDescription() {
        return description;
    }

    /**
     * return the jndi name
     * @return String jndi name
     */
    public String getJndiName() {
        return jndiname;
    }

    /**
     * return the ActivationSpec required property list
     * @return List ActivationSpec properties
     */
    public List getPropertiesList() {
        return prop;
    }

}
