/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006-2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.antmodular.jonasbase.ha;

import org.ow2.jonas.antmodular.jonasbase.bootstrap.AbstractJOnASBaseAntTask;
import org.ow2.jonas.antmodular.jonasbase.bootstrap.JReplace;

/**
 * Defines properties for ha service.
 * @author Benoit Pelletier
 */
public class Ha extends AbstractJOnASBaseAntTask {

    /**
     * Info for the logger.
     */
    private static final String INFO = "[Ha] ";

    /**
     * CMI mcast addr attribut.
     */
    private static final String MCASTADDR_ATTR = "mcast_addr";

    /**
     * CMI mcast port attribut.
     */
    private static final String MCASTPORT_ATTR = "mcast_port";

    /**
     * Default cmi mcast addr.
     */
    private static final String DEFAULT_MCASTADDR = "224.0.0.36";

    /**
     * Default cmi mcast port.
     */
    private static final String DEFAULT_MCASTPORT = "35468";

    /**
     * Name of JGroups HA configuration file.
     */
    public static final String JGROUPS_HA_CONF_FILE = "jgroups-ha.xml";


    /**
     * Default constructor.
     */
    public Ha() {
        super();
    }

    /**
     * Set mcastAddr.
     * @param mcastAddr multicast address
     */
    public void setMcastAddr(final String mcastAddr) {

        // Token to replace the multicast addr
        String token = MCASTADDR_ATTR + "=" + "\"" + DEFAULT_MCASTADDR + "\"";
        String value = MCASTADDR_ATTR + "=" + "\"" + mcastAddr + "\"";
        JReplace mcastAddrReplace = new JReplace();
        mcastAddrReplace.setLogInfo(INFO + "Setting mcastaddr");
        mcastAddrReplace.setConfigurationFile(JGROUPS_HA_CONF_FILE);
        mcastAddrReplace.setToken(token);
        mcastAddrReplace.setValue(value);
        addTask(mcastAddrReplace);
    }

    /**
     * Set mcastPort.
     * @param mcastPort multicast port
     */
    public void setMcastPort(final String mcastPort) {

        // Token to replace the multicast port
        String token = MCASTPORT_ATTR + "=" + "\"" + DEFAULT_MCASTPORT + "\"";
        String value = MCASTPORT_ATTR + "=" + "\"" + mcastPort + "\"";
        JReplace mcastPortReplace = new JReplace();
        mcastPortReplace.setLogInfo(INFO + "Setting mcastport");
        mcastPortReplace.setConfigurationFile(JGROUPS_HA_CONF_FILE);
        mcastPortReplace.setToken(token);
        mcastPortReplace.setValue(value);
        addTask(mcastPortReplace);
    }

    /**
     * Execute this task.
     */
    public void execute() {
        super.execute();
        super.executeAllTask();
    }

}
