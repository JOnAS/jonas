/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006-2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 * Copyright (C) 2006 Distributed Systems Lab.
 * Universidad Politecnica de Madrid (Spain)
 * Contact: http://lsd.ls.fi.upm.es/lsd
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ha.internal;

import java.util.Stack;

import javax.ejb.EntityContext;

import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;
import org.ow2.carol.util.configuration.ConfigurationException;
import org.ow2.carol.util.configuration.ConfigurationRepository;
import org.ow2.cmi.ha.ReplicationManager;
import org.ow2.cmi.ha.ReplicationManagerImpl;
import org.ow2.cmi.ha.RequestId;
import org.ow2.cmi.ha.interceptor.HACurrent;
import org.ow2.cmi.ha.interceptor.jrmp.HAInterceptorInitializer;
import org.ow2.jonas.cmi.CmiService;
import org.ow2.jonas.ha.HaService;
import org.ow2.jonas.lib.ejb21.ha.JRepUtil;
import org.ow2.jonas.lib.service.AbsServiceImpl;
import org.ow2.jonas.lib.util.Log;
import org.ow2.jonas.service.ServiceException;

/**
 * This class binds the HA service to JOnAS and initializes the HA architecture.
 *
 * @author Francisco Perez-Sorrosal (fpsorrosal@no-spam@fi.upm.es)
 * @author Alberto Paz-Jimenez (apaz@no-spam@fi.upm.es)
 * @author benoit pelletier
 */
public class HaServiceImpl extends AbsServiceImpl implements HaService {

    /**
     * Logger.
     */
    private static final Logger logger = Log.getLogger("org.ow2.jonas.ha");

    /**
     * CMI HA replication manager.
     */
    private ReplicationManager replicationMgr;

    private HACurrent haCurrent;

    /**
     * CMI Service reference.
     */
    private CmiService cmiService;

    /**
     * Backup info timeout.
     */
    private int gcPeriod;

    /**
     * Tx table datasource.
     */
    private String datasource;

    /**
     * JGroups configuration file property name.
     */
    private String jgroupsConf;

    /**
     * JGroups channel property name.
     */
    private String jgroupsGroupname;

    /**
     *  Reconnection timeout for JGroups Channel. This property is injected.
     */
    private long reconnectionTimeout;

    /**
     * Set a reference for the CMI service
     * @param cmiService a CMI service reference
     */
    public void setCmiService(final CmiService cmiService) {
        this.cmiService = cmiService;
    }

    public void setGcPeriod(final int gcPeriod) {
        this.gcPeriod = gcPeriod;
    }

    public void setDatasource(final String datasource){
        this.datasource = datasource;
    }

    public void setJgroupsConf(final String jgroupsConf) {
        this.jgroupsConf = jgroupsConf;
    }

    public void setJgroupsGroupname(final String jgroupsGroupname) {
        this.jgroupsGroupname = jgroupsGroupname;
    }

    public void setReconnectionTimeout(final long reconnectionTimeout) {
        this.reconnectionTimeout = reconnectionTimeout;
    }

    @Override
    public void doStart() throws ServiceException {
        logger.log(BasicLevel.INFO, "Starting replication service...");

        try {
            replicationMgr = new ReplicationManagerImpl(
                    gcPeriod, datasource, jgroupsConf, jgroupsGroupname, reconnectionTimeout);
        } catch (Exception e) {
            logger.log(BasicLevel.ERROR, "Error during HA Service startup", e);
            throw new ServiceException("Error during HA Service startup", e);
        }
        // Init the HA current class
        haCurrent = HACurrent.getHACurrent();

        if (logger.isLoggable(BasicLevel.DEBUG)) {
            logger.log(BasicLevel.DEBUG, "replicationMgr=" + replicationMgr);
            logger.log(BasicLevel.DEBUG, "haCurrent=" + haCurrent);
        }

        JRepUtil.setRepMgr(replicationMgr);

        try {
            ConfigurationRepository.addInterceptors("jrmp", HAInterceptorInitializer.class);
        } catch (ConfigurationException e) {
            logger.log(BasicLevel.ERROR, "Cannot register the HA interceptors", e);
            throw new ServiceException("Cannot register the HA interceptors", e);
        }

        // COMPLETE: Allow to start and shutdown service dynamically
        logger.log(BasicLevel.INFO, "Replication service started");
    }

    @Override
    public void doStop() throws ServiceException {
        try {
            ConfigurationRepository.removeInterceptors("jrmp", HAInterceptorInitializer.class);
        } catch (ConfigurationException e) {
            logger.log(BasicLevel.ERROR, "Cannot unregister the HA interceptors", e);
            throw new ServiceException("Cannot unregister the HA interceptors", e);
        }
        // COMPLETE: Allow to start and shutdown service dynamically
        replicationMgr.clear(); // Turn off the GCL
        logger.log(BasicLevel.INFO, "Replication service stopped");
    }

    /**
     * Send commit/abort message.
     * @param committed <tt>true</tt> if the transaction has committed
     */
    public void replicateCommit(final boolean committed) {
        Stack<RequestId> requests = haCurrent.getRequests();
        if ((requests != null)
                && (requests.size() == 1)) {
            RequestId reqId = requests.peek();
            if (reqId != null) {
                try {
                    replicationMgr.replicateCommit(reqId, committed);
                } catch (Exception e) {
                    logger.log(BasicLevel.ERROR, "Unable to replicate commit/abort", e);
                }
            }
        }
    }

    public void replicate() {
        Stack<RequestId> requests = haCurrent.getRequests();
        if ((requests != null)
                && (requests.size() == 1)) {
            RequestId reqId = requests.peek();
            try {
                replicationMgr.replicate(reqId);
            } catch (Exception e) {
                logger.log(BasicLevel.ERROR, "Unable to replicate", e);
            }
        }
    }

    public void addEntityBean(final EntityContext jec) {
        Stack<RequestId> requests = haCurrent.getRequests();
        if ((requests != null) && !requests.isEmpty()) {
            RequestId rootId = requests.firstElement();
            JRepUtil.addEntityBean(jec, rootId);
        }
    }


}
