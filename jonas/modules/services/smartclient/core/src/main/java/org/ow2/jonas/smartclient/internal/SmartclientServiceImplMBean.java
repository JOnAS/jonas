/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.smartclient.internal;

import org.ow2.jonas.smartclient.SmartclientServiceBase;

/**
 * Management bean interface for the Smartclient service. This interface
 * inherits from the smartclient service's base interface and doesn't add any
 * method to it, its name guarantees that the JMX server will automatically
 * deploy it with the good bindings.
 *
 * @author S. Ali Tokmen
 */
public interface SmartclientServiceImplMBean extends SmartclientServiceBase {
    // This interface should be empty.
}
