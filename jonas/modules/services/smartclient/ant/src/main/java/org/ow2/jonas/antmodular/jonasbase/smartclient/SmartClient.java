/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.antmodular.jonasbase.smartclient;

import org.ow2.jonas.antmodular.jonasbase.bootstrap.AbstractJOnASBaseAntTask;
import org.ow2.jonas.antmodular.jonasbase.bootstrap.JReplace;
import org.ow2.jonas.antmodular.jonasbase.bootstrap.JTask;

import java.io.File;

/**
 * Allow to configure properties of the smartclient
 * @author Jeremy Cazaux
 */
public class SmartClient extends AbstractJOnASBaseAntTask {

    /**
     * Info for the logger.
     */
    private static final String INFO = "[Smartclient] "; 

    /**
     * Name of JOnAS properties file.
     */
    public static final String CONF_FILE = "jonas.properties";

    /**
     * The port property
     */
    private static final String PORT_PROPERTY = "jonas.service.smartclient.port";

    /**
     * The smartclient port number
     */
    private String smartclientPort;

    /**
     * Default constructor.
     */
    public SmartClient() {
        super();
    }

    /**
     * Set the smartclient port number
     *
     * @param portNumber port for the smartclient
     */
    public void setPort(final String portNumber) { 
        this.smartclientPort = portNumber; 
    }

    /**
     * Execute this task.
     */
    @Override
    public void execute() {
        super.execute(); 

        // Path to JONAS_BASE
        String jBaseConf = this.destDir.getPath() + File.separator + "conf";

        JTask jTask = new JTask();
        jTask.setDestDir(this.destDir);
        if (this.smartclientPort != null) { 
            jTask.changeValueForKey(INFO, jBaseConf, CONF_FILE,
                                    PORT_PROPERTY, this.smartclientPort, false);
        } 

        addTask(jTask);

    }
}
