/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006-2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.antmodular.jonasbase.web.tomcat;

import org.ow2.jonas.antmodular.jonasbase.bootstrap.JReplace;
import org.ow2.jonas.antmodular.web.base.Ajp;
import org.ow2.jonas.antmodular.web.base.Connectors;
import org.ow2.jonas.antmodular.web.base.Director;
import org.ow2.jonas.antmodular.web.base.Http;
import org.ow2.jonas.antmodular.web.base.Https;

/**
 * Configure Tomcat connector
 * @author Jeremy Cazaux
 */
public abstract class TomcatConnectors extends Connectors {

    /**
     * Is HTTP Configured ?
     */
    protected boolean httpConfigured = false;

    /**
     * Tomcat AJP Connector token.
     */
    private static final String AJP_CONNECTOR_TOKEN = "<!-- Define an AJP";

    /**
     * Tomcat HTTPS Connector token.
     */
    private static final String HTTPS_CONNECTOR_TOKEN = "<!-- Define a SSL Coyote HTTP/1.1";

    /**
     * Proxied HTTP/1.1 COnnector Token used to insert Director Connector.
     */
    private static final String DIRECTOR_CONNECTOR_TOKEN = "<!-- Define a Proxied HTTP/1.1";

    /**
     * Configure a HTTP Connector.
     * @param http HTTP Configuration.
     */
    public void addConfiguredHttp(final Http http, final String confFile) {
        httpConfigured = true;
        super.addConfiguredHttp(http, confFile, Tomcat.INFO);
    }

    /**
     * Configure a HTTPS Connector.
     * @param https HTTPS Configuration.
     */
    public void addConfiguredHttps(final TomcatHttps https, final String confFile) {
        // Add the HTTPS Connector
        JReplace propertyReplace = new JReplace();
        propertyReplace.setConfigurationFile(confFile);
        propertyReplace.setToken(HTTPS_CONNECTOR_TOKEN);
        StringBuffer value = new StringBuffer();
        value.append("<!-- Define a SSL Coyote HTTP/1.1 Connector on port " + https.getPort() + " -->\n");
        value.append("    <Connector port=\"" + https.getPort() + "\" maxHttpHeaderSize=\"8192\"\n");
        value.append("               maxThreads=\"150\" minSpareThreads=\"25\" maxSpareThreads=\"75\"\n");
        value.append("               enableLookups=\"false\" disableUploadTimeout=\"true\"\n");
        value.append("               acceptCount=\"100\" scheme=\"https\" secure=\"true\"\n");
        value.append("               clientAuth=\"false\" sslProtocol=\"TLS\"\n");
        if (https.getKeystoreFile() != null) {
            value.append("               keystoreFile=\"" + https.getKeystoreFile() + "\"\n");
        }
        if (https.getKeystorePass() != null) {
            value.append("               keystorePass=\"" + https.getKeystorePass() + "\"\n");
        }
        value.append("               />\n\n");
        value.append("    " + HTTPS_CONNECTOR_TOKEN);
        propertyReplace.setValue(value.toString());
        propertyReplace.setLogInfo(Tomcat.INFO + "Setting HTTPS Connector to : " + https.getPort());
        addTask(propertyReplace);

        // Replace redirect port value in HTTP Connector
        propertyReplace = new JReplace();
        propertyReplace.setConfigurationFile(confFile);
        propertyReplace.setToken(" redirectPort=\"" + Https.DEFAULT_PORT + "\" ");
        propertyReplace.setValue(" redirectPort=\"" + https.getPort() + "\" ");
        propertyReplace.setLogInfo(Tomcat.INFO + "Fix HTTP redirect port number to : " + https.getPort());
        addTask(propertyReplace);

    }

    /**
     * Configure an AJP Connector.
     * @param ajp AJP Configuration.
     */
    public void addConfiguredAjp(final Ajp ajp, final String confFile) {
        JReplace propertyReplace = new JReplace();
        propertyReplace.setConfigurationFile(confFile);
        propertyReplace.setToken(AJP_CONNECTOR_TOKEN);
        StringBuffer value = new StringBuffer();
        value.append(" <!-- Define an AJP 1.3 Connector on port " + ajp.getPort() + " -->\n");
        value.append("    <Connector port=\"" + ajp.getPort() + "\" enableLookups=\"false\"\n");
        value.append("               redirectPort=\"" + TomcatAjp.DEFAULT_REDIRECT_PORT + "\" protocol=\"AJP/1.3\" />\n\n");
        value.append("    " + AJP_CONNECTOR_TOKEN);
        propertyReplace.setValue(value.toString());
        propertyReplace.setLogInfo(Tomcat.INFO + "Setting AJP Connector to : " + ajp.getPort());
        addTask(propertyReplace);

    }

    /**
     * Configure a Director Connector.
     * @param dir Director Configuration.
     */
    public void addConfiguredDirector(final Director dir, final String confFile) {
        JReplace propertyReplace = new JReplace();
        propertyReplace.setConfigurationFile(confFile);

        propertyReplace.setToken(DIRECTOR_CONNECTOR_TOKEN);
        StringBuffer value = new StringBuffer();
        value.append("<!--  Define a Director Connector on port " + dir.getPort() + " -->\n");
        value.append("    <Connector protocol=\"org.enhydra.servlet.connectionMethods.EnhydraDirector.DirectorProtocol\"\n");
        value.append("               port=\"" + dir.getPort() + "\"\n");
        value.append("               threadTimeout = \"300\"\n");
        value.append("               clientTimeout = \"30\"\n");
        value.append("               sessionAffinity = \"false\"\n");
        value.append("               queueSize = \"400\"\n");
        value.append("               numThreads = \"200\"\n");
        value.append("               bindAddress = \"(All Interfaces)\"\n");
        value.append("               authKey = \"(Unauthenticated)\"\n");
        value.append("               />\n\n");
        value.append("    " + DIRECTOR_CONNECTOR_TOKEN);

        propertyReplace.setValue(value.toString());
        propertyReplace.setLogInfo(Tomcat.INFO + "Setting Director Connector to : " + dir.getPort());
        addTask(propertyReplace);

    }

    /**
     * @return Returns <code>true</code> if HTTP is configured.
     */
    public boolean isHttpConfigured() {
        return httpConfigured;
    }

    /**
     * Configure a HTTP Connector.
     * @param http HTTP Configuration.
     */
    public abstract void addConfiguredHttp(final TomcatHttp http);

    /**
     * Configure a HTTPS Connector.
     * @param https HTTPS Configuration.
     */
    public abstract void addConfiguredHttps(final TomcatHttps https);

    /**
     * Configure a Director Connector.
     * @param dir Director Configuration.
     */
    public abstract void addConfiguredDirector(final Director dir);

    /**
     * Configure an AJP Connector.
     * @param tomcatAjp AJP Configuration.
     */
    public abstract void addConfiguredAjp(final TomcatAjp tomcatAjp);
}
