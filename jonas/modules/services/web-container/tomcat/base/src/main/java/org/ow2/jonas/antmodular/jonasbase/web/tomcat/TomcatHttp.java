/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.antmodular.jonasbase.web.tomcat;

import org.ow2.jonas.antmodular.web.base.Http;

/**
 * Allow to configure tomcat Http connector
 * @author Jeremy Cazaux
 */
public class TomcatHttp extends Http {

    /**
     * use specifies enableLookups value 
     */
    protected String enableLookups;

    /**
     * use specifies minSpareThreads value
     */
    protected String minSpareThreads;

    /**
     * use specifies maxThreads value
     */
    protected String maxThreads;

    /**
     * use specifies connectionTimeout value
     */
    protected String connectionTimeout;

    /**
     * use specifies acceptCount value
     */
    protected String acceptCount;

    /**
     * use specifies maxKeepAliveRequest
     */
    protected String maxKeepAliveRequest;

    /**
     * use specifies compression value
     */
    protected String compression;

    /**
     * use specifies redirectPort value
     */
    protected String redirectPort;

    /**
     * Default redirect port for tomcat 7 Http connector
     */
    public static String DEFAULT_REDIRECT_PORT = "9043";

    /**
     * Default constructor
     */
    public TomcatHttp() {
    }

    /**
     * @return the enableLookups value
     */
    public String getEnableLookups() {
        return this.enableLookups;
    }

    /**
     * @return the minSpareThreads value
     */
    public String getMinSpareThreads() {
        return this.minSpareThreads;
    }

    /**
     * @return the maxThreads value
     */
    public String getMaxThreads() {
        return this.maxThreads;
    }

    /**
     * @return the connectionTimeout value
     */
    public String getConnectionTimeout() {
        return this.connectionTimeout;
    }

    /**
     * @return the acceptCount value
     */
    public String getAcceptCount() {
        return this.acceptCount;
    }

    /**
     * @return the maxKeepAliveRequest value
     */
    public String getMaxKeepAliveRequest() {
        return this.maxKeepAliveRequest;
    }

    /**
     * @return the compression value
     */
    public String getCompression() {
        return this.compression;
    }

    /**
     * @return the redirect port value
     */
    public String getRedirectPort() {
        return this.redirectPort;
    }

    /**
     * @param enableLookups The enableLookups property to set
     */
    public void setEnableLookups(String enableLookups) {
        this.enableLookups = enableLookups;
    }

    /**
     * @param minSpareThreads The minSpareThreads property to set
     */
    public void setMinSpareThreads(String minSpareThreads) {
        this.minSpareThreads = minSpareThreads;
    }

   /**
     * @param maxThreads The maxThreads property to set
     */
    public void setMaxThreads(String maxThreads) {
        this.maxThreads = maxThreads;
    }

    /**
     * @param connectionTimeout The connectionTimeout property to set
     */
    public void setConnectionTimeout(String connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    /**
     * @param acceptCount The acceptCount property to set
     */
    public void setAcceptCount(String acceptCount) {
        this.acceptCount = acceptCount;
    }

    /**
     * @param maxKeepAliveRequest The maxKeepAliveRequest property to set
     */
    public void setMaxKeepAliveRequest(String maxKeepAliveRequest) {
        this.maxKeepAliveRequest = maxKeepAliveRequest;
    }

    /**
     * @param compression The compression property to set
     */
    public void setCompression(String compression) {
        this.compression = compression;
    }

    /*
     * @param redirectPort The redirect port property to set
     */
    public void setRedirectPort(String redirectPort) {
        this.redirectPort = redirectPort;
    }
}


