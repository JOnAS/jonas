/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.web.tomcat7.ws.security;

import java.util.List;

import org.ow2.jonas.deployment.web.WebContainerDeploymentDesc;
import org.ow2.jonas.deployment.web.xml.JonasWebApp;
import org.ow2.jonas.deployment.web.xml.WebApp;
import org.ow2.jonas.deployment.web.xml.SecurityConstraint;
import org.ow2.jonas.deployment.web.xml.AuthConstraint;
import org.ow2.jonas.deployment.web.xml.UserDataConstraint;
import org.ow2.jonas.deployment.web.xml.WebResourceCollection;
import org.ow2.jonas.deployment.common.xml.SecurityRole;
import org.ow2.jonas.deployment.common.DeploymentDescException;
import org.ow2.jonas.ws.jaxws.ejb.ISecurityConstraint;
import org.ow2.jonas.ws.jaxws.WSException;

/**
 * The SecureWebDeploymentDescBuilder is responsible of building, from a list of
 * endpoints defined security constraints, a web deployment descriptor strctured
 * tailored for web security.
 *
 * @author Guillaume Sauthier
 */
public class SecureWebDeploymentDescBuilder {

    /**
     * Creates the Web DD structure.
     * @param security list of security constraints
     * @return a security tailored deployment descriptor
     * @throws WSException if the descriptor cannot be created
     */
    public WebContainerDeploymentDesc createJOnASWebDescriptor(final List<ISecurityConstraint> security) throws WSException {
        try {
            return new WebContainerDeploymentDesc(null, // filename: none
                                                  this.getClass().getClassLoader(), // unused in our case: no classes to load
                                                  createWebApp(security), // the structure mirroring a security only web.xml
                                                  new JonasWebApp(), // empty jonas-web.xml
                                                  null); // no metadata associated
        } catch (DeploymentDescException e) {
            throw new WSException("Cannot construct secured web deployment descriptor", e);
        }
    }

    /**
     * Construct the web.xml mirroring structure.
     * @param constraints list of constraints to be translated into a web.xml
     * @return a WebApp XML structure
     */
    private WebApp createWebApp(final List<ISecurityConstraint> constraints) {

        // In all case, creates an empty webapp
        WebApp webApp = new WebApp();
        
        if (!constraints.isEmpty()) {

            // Process all the constraints
            for (ISecurityConstraint constraint : constraints) {
                // security-constraint
                SecurityConstraint sc = new SecurityConstraint();
                webApp.addSecurityConstraint(sc);

                // security-constraint/user-data-constraint
                UserDataConstraint udc = new UserDataConstraint();
                sc.setUserDataConstraint(udc);
                // security-constraint/user-data-constraint/transport-guarantee
                String transportGuarantee = constraint.getTransportGuarantee();
                if (transportGuarantee == null) {
                    transportGuarantee = "NONE";
                }
                udc.setTransportGuarantee(transportGuarantee);

                // security-constraint/auth-constraint
                AuthConstraint ac = new AuthConstraint();
                sc.setAuthConstraint(ac);
                // security-constraint/auth-constraint/role-name
                for (String role : constraint.getRoleNames()) {
                    ac.addRoleName(role);
                }

                // security-constraint/web-resource-collection
                WebResourceCollection collection = new WebResourceCollection();
                collection.setWebResourceName("default");
                sc.addWebResourceCollection(collection);
                // security-constraint/web-resource-collection/url-pattern
                collection.addUrlPattern(constraint.getUrlPattern());
                // security-constraint/web-resource-collection/http-method
                if (constraint.getHttpMethods() != null) {
                    for (String method : constraint.getHttpMethods()) {
                        collection.addHttpMethod(method);
                    }
                }

                // security-role
                for (String role : constraint.getRoleNames()) {
                    SecurityRole sr = new SecurityRole();
                    sr.setRoleName(role);
                    webApp.addSecurityRole(sr);
                }

            }
        }

        return webApp;
    }
}
