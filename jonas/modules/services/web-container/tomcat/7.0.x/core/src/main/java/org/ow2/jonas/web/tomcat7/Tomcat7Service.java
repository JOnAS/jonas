/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2010-2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.web.tomcat7;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.naming.NamingException;
import javax.servlet.Filter;
import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.catalina.Container;
import org.apache.catalina.Context;
import org.apache.catalina.Engine;
import org.apache.catalina.Host;
import org.apache.catalina.Lifecycle;
import org.apache.catalina.LifecycleEvent;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.LifecycleListener;
import org.apache.catalina.LifecycleState;
import org.apache.catalina.Server;
import org.apache.catalina.Service;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.core.DefaultInstanceManager;
import org.apache.catalina.core.StandardContext;
import org.apache.catalina.core.StandardEngine;
import org.apache.catalina.startup.CatalinaProperties;
import org.apache.catalina.startup.ContextConfig;
import org.apache.catalina.util.LifecycleMBeanBase;
import org.apache.tomcat.InstanceManager;
import org.apache.tomcat.util.digester.Digester;
import org.objectweb.util.monolog.api.BasicLevel;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.http.HttpContext;
import org.osgi.service.http.HttpService;
import org.osgi.service.http.NamespaceException;
import org.ow2.easybeans.deployment.api.EZBInjectionHolder;
import org.ow2.jonas.addon.deploy.api.config.IAddonConfig;
import org.ow2.jonas.deployment.web.WebContainerDeploymentDesc;
import org.ow2.jonas.jndi.checker.api.IResourceCheckerManager;
import org.ow2.jonas.lib.bootstrap.LoaderManager;
import org.ow2.jonas.lib.execution.ExecutionResult;
import org.ow2.jonas.lib.execution.IExecution;
import org.ow2.jonas.lib.execution.RunnableHelper;
import org.ow2.jonas.lib.loader.FilteringClassLoader;
import org.ow2.jonas.lib.management.javaee.J2eeObjectName;
import org.ow2.jonas.lib.util.ConfigurationConstants;
import org.ow2.jonas.security.SecurityService;
import org.ow2.jonas.service.ServiceException;
import org.ow2.jonas.tm.TransactionService;
import org.ow2.jonas.versioning.VersioningService;
import org.ow2.jonas.web.JWebContainerService;
import org.ow2.jonas.web.JWebContainerServiceException;
import org.ow2.jonas.web.base.BaseWebContainerService;
import org.ow2.jonas.web.base.War;
import org.ow2.jonas.web.base.lib.PermissionManager;
import org.ow2.jonas.web.base.osgi.httpservice.HttpServiceFactory;
import org.ow2.jonas.web.base.proxy.HttpOnDemandProxy;
import org.ow2.jonas.web.tomcat7.custom.ContextCustomizer;
import org.ow2.jonas.web.tomcat7.osgi.OSGiTldListener;
import org.ow2.jonas.web.tomcat7.osgi.configadmin.Tomcat7Connector;
import org.ow2.jonas.web.tomcat7.security.Realm;
import org.ow2.jonas.web.tomcat7.tx.TransactionValve;
import org.ow2.jonas.web.tomcat7.versioning.ContextFinder;
import org.ow2.jonas.web.tomcat7.versioning.EmptyServlet;
import org.ow2.jonas.web.tomcat7.ws.WSContextLifecycleListener;
import org.ow2.jonas.web.tomcat7.ws.WSDeployment;
import org.ow2.jonas.web.tomcat7.ws.WebservicesWebDeployer;
import org.ow2.jonas.ws.jaxws.IWebServiceDeploymentManager;
import org.ow2.jonas.ws.jaxws.ejb.IWebDeployer;
import org.ow2.util.ee.deploy.api.deployable.WARDeployable;
import org.ow2.util.ee.metadata.war.api.IWarClassMetadata;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;
import org.ow2.util.url.URLUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import static org.ow2.jonas.ws.jaxws.IJAXWSService.KEY_WEB_SERVICES_METADATAS;

/**
 * Implementation of the web container service for Tomcat 7.
 * This service also adds virtual path support to Tomcat 7.
 *
 * @author Florent BENOIT
 * @author S. Ali Tokmen
 */
public class Tomcat7Service extends BaseWebContainerService implements JWebContainerService, Tomcat7Connector {

    /**
     * Logger.
     */
    private static Log logger = LogFactory.getLog(Tomcat7Service.class);

    /**
     * Name of the tomcat 7 server configuration file
     */
    protected static final String TOMCAT7_SERVER_CONFIG_FILENAME = "tomcat7-server.xml";

    /**
     * Name of the tomcat 7 context configuration file
     */
    protected static final String TOMCAT7_CONTEXT_CONFIG_FILENAME = "tomcat7-context.xml";

    /**
     * Name of the tomcat 7 web configuration file
     */
    protected static final String TOMCAT7_WEB_CONFIG_FILENAME = "tomcat7-web.xml";

    /**
     * Relative path of the configuration file.
     */
    protected static final String CONFIG_FILE = ConfigurationConstants.DEFAULT_CONFIG_DIR + File.separator +
            TOMCAT7_SERVER_CONFIG_FILENAME;

    /*
      * Path to the tomcat 7 server configuration file
    */
    protected String tomcat7ServerConfPath;

    /**
     * Path to the tomcat 7 context configuration file
     */
    protected String tomcat7ContextConfPath;

    /**
     * Path to the tomcat 7 web configuration file
     */
    protected String tomcat7WebConfPath;

    /**
     * The reference on the catalina server.
     */
    private Server server = null;

    /**
     * External class loader.
     */
    private ClassLoader externalLoader = null;

    /**
     * Web container started ?
     */
    private boolean tomcatStarted = false;

    /**
     * Security service.
     */
    private SecurityService securityService = null;

    /**
     * {@link IWebServiceDeploymentManager} registration key.
     */
    private ServiceRegistration reg;

    /**
     * {@link IWebDeployer} registration key.
     */
    private ServiceRegistration reg2;

    /**
      * Transaction service.
      */
     private TransactionService transactionService = null;

    /**
     * Link to the resource checker manager.
     */
    private IResourceCheckerManager resourceCheckerManager = null;

    /**
     * Customizers to be applied on the created Contexts.
     */
    private List<ContextCustomizer> customizers;

    /**
     * SCIs of third party frameworks.
     * (need to be stateless 'cause the same instance will be invoked for each deployed web-app).
     */
    private List<ServletContainerInitializer> initializers;

    /**
     * Default constructor.
     * @param bundleContext the OSGi bundle context
     */
    public Tomcat7Service(final BundleContext bundleContext) {
        super(bundleContext);
        this.customizers = new ArrayList<ContextCustomizer>();
        this.initializers = new ArrayList<ServletContainerInitializer>();
    }

    /**
     * Init the environment of catalina set catalina.home, catalina.base and
     * unset the tomcat naming.
     * @throws ServiceException if catalina home is not set
     */
    protected void initCatalinaEnvironment() throws ServiceException {

        // Assume that CATALINA_HOME = JONAS_ROOT and CATALINA_BASE = JONAS_BASE
        String jonasRoot = System.getProperty("jonas.root");
        String jonasBase = System.getProperty("jonas.base");

        String catalinaHome = System.getProperty("catalina.home");
        String catalinaBase = System.getProperty("catalina.base");

        // check that this value is ok ?
        if (catalinaHome != null && !jonasRoot.equals(catalinaHome)) {
            logger.warn("The CATALINA_HOME property was set to ''{0}'', fix it by using JONAS_ROOT ''{1}''", catalinaHome,
                    jonasRoot);
        }
        // override
        System.setProperty("catalina.home", jonasRoot);

        // CATALINA_BASE = JONAS_BASE ?
        if (catalinaBase != null && !jonasBase.equals(catalinaBase)) {
            logger.warn("The CATALINA_BASE property was set to ''{0}'', fix it by using JONAS_BASE ''{1}''", catalinaBase,
                    jonasBase);
        }
        // override
        System.setProperty("catalina.base", jonasBase);

        // use the JOnAS naming instead of the Tomcat naming
        System.setProperty("catalina.useNaming", "false");

        // Init properties
        CatalinaProperties.getProperty("catalina.base");

    }

    /**
     * Start the Catalina service in a new thread.
     * @throws ServiceException if the startup failed.
     */
    @Override
    public void doStart() throws ServiceException {

        initCatalinaEnvironment();

        // On Demand feature not enabled, start the web container now
        if (!isOnDemandFeatureEnabled()) {
            startInternalWebContainer();
        } // else delay this launch

        // Here, register the IWSDM
        WSDeployment manager = new WSDeployment(this);
        manager.setWorkDirectory(getServerProperties().getWorkDirectory());
        reg = getBundleContext().registerService(IWebServiceDeploymentManager.class.getName(),
                                      manager,
                                      null);

        // Here, register the IWebDeployer
        WebservicesWebDeployer webDeployer = new WebservicesWebDeployer(this);
        webDeployer.setWorkDirectory(getServerProperties().getWorkDirectory());
        reg2 = getBundleContext().registerService(IWebDeployer.class.getName(),
                                       webDeployer,
                                       null);

        // ... and run super method
        super.doStart();

        addDummyContextRoot();
    }


    /**
     * Starts the specific code for the web container implementation.
     * This allows to start the internal container on demand (if there is an access on the http proxy port for example)
     * @throws JWebContainerServiceException if container is not started
     */
    @Override
    public synchronized void startInternalWebContainer() throws JWebContainerServiceException {
        // Already started
        if (tomcatStarted) {
            return;
        }

        // parent ClassLoader
        LoaderManager lm = LoaderManager.getInstance();
        try {
            externalLoader = lm.getExternalLoader();
        } catch (Exception e1) {
            throw new ServiceException("Cannot get Application/Tomcat ClassLoader", e1);
        }

        // Initialize the context finder for the optional versioning service
        // Context finder will not do anything if the versioning service is not
        // present (can be removed or added on-the-fly).
        ContextFinder.setParent(this);
        ContextFinder.setJmxService(jmxService);

        // Create the digester for the parsing of the server.xml.
        Digester digester = createServerDigester();

        // Execute the digester for the parsing of the server.xml.
        // And configure the catalina server.
        File configFile = null;

        try {
            configFile = getConfigFile();
        } catch (FileNotFoundException e) {
            logger.error("Cannot find the file ''{0}''", getConfigPath(), e);
            throw new ServiceException("Cannot find the configuration file", e);
        }

        try {
            InputSource is = new InputSource("file://" + configFile.getAbsolutePath());
            FileInputStream fis = new FileInputStream(configFile);
            is.setByteStream(fis);
            digester.setClassLoader(this.getClass().getClassLoader());
            digester.push(this);
            digester.parse(is);
            fis.close();
        } catch (Exception e) {
            logger.error("Cannot parse the configuration file ''{0}''", configFile, e);
            throw new ServiceException("Cannot parse the configuration file '" + configFile + "'", e);
        }

        // Set the Domain and the name for each known Engine
        for (StandardEngine engine : getEngines()) {
            // WARNING : the order of th two next lines is very important.
            // The domain must be set in first and the name after.
            // In the others cases, Tomcat 6 doesn't set correctly these two
            // properties
            // because there are somes controls that forbid to have a difference
            // between
            // the name and the domain. Certainly a bug !
            engine.setDomain(getDomainName());
            engine.setName(getDomainName());
        }

        // If OnDemand Feature is enabled, the http connector port needs to be changed
        // And keep-alive feature should be turned-off (to monitor all requests)
        if (isOnDemandFeatureEnabled()) {
            Service[] services = getServer().findServices();

            // set name of the first service
            if (services.length > 0) {
                services[0].setName(getDomainName());
            }

            // Get connector of each service
            for (int s = 0; s < services.length; s++) {
                Connector[] connectors = services[s].findConnectors();
                if (connectors.length >= 1) {
                    // Only for the first connector
                    Connector connector = connectors[0];
                    connector.setProperty("maxKeepAliveRequests", "1");
                    connector.setPort(getOnDemandRedirectPort());
                    connector.setProxyPort(Integer.parseInt(getDefaultHttpPort()));
                }
            }
        }


        // Start Tomcat server in an execution block
        IExecution<Void> startExec = new IExecution<Void>() {
            public Void execute() throws ServiceException {
                // Finaly start catalina ...
                if (server instanceof LifecycleMBeanBase) {
                    try {
                        ((LifecycleMBeanBase) server).setDomain(getDomainName());
                        server.init();
                        ((LifecycleMBeanBase) server).setDomain(getDomainName());
                        Service[] services = getServer().findServices();
                        // set name of the first service
                        if (services.length > 0) {
                            services[0].setName(getDomainName());
                        }
                        ((Lifecycle) server).start();
                    } catch (Exception e) {
                        logger.error("Cannot start the Tomcat server", e);
                        throw new ServiceException("Cannot start the Tomcat server", e);
                    }
                }

                return null;
            }
        };

        // Execute
        ExecutionResult<Void> startExecResult = RunnableHelper.execute(getClass().getClassLoader(), startExec);

        // Throw an ServiceException if needed
        if (startExecResult.hasException()) {
            logger.error("Cannot start the Tomcat server", startExecResult.getException());
            throw new ServiceException("Cannot start the Tomcat Server", startExecResult.getException());
        }

        // Tomcat is started
        tomcatStarted = true;
    }

    /**
     * Checks if the internal web container has been started.
     * @return true if it is already started
     */
    @Override
    public boolean isInternalContainerStarted() {
        return tomcatStarted;
    }



    /**
     * Stop the Catalina service.
     * @throws ServiceException if the stop failed.
     */
    @Override
    protected void doStop() throws ServiceException {
        // Undeploy the wars ...
        super.doStop();

        // Unregister the WSDM
        if (reg != null) {
            reg.unregister();
        }

        if (reg2 != null) {
            reg2.unregister();
        }


        // ... and shut down embedded catalina
        if (tomcatStarted && server instanceof Lifecycle) {
            try {
                ((Lifecycle) server).stop();
            } catch (Exception e) {
                throw new ServiceException(e.getMessage(), e);
            }
        }
        tomcatStarted = false;
    }

    /**
     * Deploy a specific WAR file specified in the context.
     * @param ctx the context which contains the configuration in order to
     *        deploy the WAR.
     * @throws JWebContainerServiceException if the registration of the WAR
     *         failed.
     */
    @Override
    protected void doRegisterWar(final javax.naming.Context ctx) throws JWebContainerServiceException {
        // Get the 7 parameters :
        // - warURL is the URL of the war to register (required param).
        // - contextRoot is the context root to which this application
        // should be installed (must be unique) (required param).
        // - hostName is the name of the host on which deploy the war
        // (optional param taken into account only if no <context> element
        // was declared in server.xml for this web application) .
        // - Name of the Ear application of this war if any
        // - java2DelegationModel the compliance to java2 delegation model
        // - parentCL the war classloader of this war.
        // - jonasDD JOnAS Deployment Desc content
        // - permissionManager JACC permission manager

        URL warURL = null;
        URL earURL = null;
        URL unpackedWarURL = null;
        String contextRoot = null;
        boolean java2DelegationModel = true;
        PermissionManager permissionManager = null;
        boolean inEarCase = true;
        String earAppName = null;
        WebContainerDeploymentDesc webDD = null;
        String userURI = null;
        Map<String, IWarClassMetadata> webservices = null;

        // War 'management' instance
        War war = null;

        try {
            war = (War) ctx.lookup("war");

            // Values loading
            unpackedWarURL = (URL) ctx.lookup("unpackedWarURL");
            webDD = (WebContainerDeploymentDesc) ctx.lookup("webDD");
            userURI = (String) ctx.lookup("userURI");
            webservices = (Map<String, IWarClassMetadata>) ctx.lookup(KEY_WEB_SERVICES_METADATAS);

        } catch (NamingException e) {
            logger.error("Unable to get default parameters", e);
            throw new JWebContainerServiceException("Unable to get default parameters", e);
        }

        try {
            earAppName = (String) ctx.lookup("earAppName");
        } catch (NamingException e) {
            // no ear case, so no ear application name
            inEarCase = false;
        }

        ClassLoader webClassLoader = null;

        try {
            webClassLoader = (ClassLoader) ctx.lookup("parentCL");
        } catch (NamingException e) {
            logger.error("Unable to get parentCL parameter", e);
            throw new JWebContainerServiceException("Unable to get parentCL parameter", e);
        }

        FilteringClassLoader filteringClassLoader = null;

        try {
            filteringClassLoader = (FilteringClassLoader) ctx.lookup(FilteringClassLoader.class.getName());
        } catch (NamingException e) {
            logger.error("Unable to get filteringClassLoader parameter", e);
            throw new JWebContainerServiceException("Unable to get filteringClassLoader parameter", e);
        }


        // optional parameters
        ClassLoader ejbClassLoader = null;
        try {
            ejbClassLoader = (ClassLoader) ctx.lookup("ejbClassLoader");
        } catch (NamingException e) {
            // no ejb classloader
            logger.debug("no ejb Class loader");
        }
        ClassLoader earClassLoader = null;
        try {
            earClassLoader = (ClassLoader) ctx.lookup("earClassLoader");
        } catch (NamingException e) {
            // no EAR classloader
            logger.debug("no EAR Class loader");
        }

        // Get injection holder
        WARDeployable deployable = null;
        try {
            deployable = (WARDeployable) ctx.lookup(WARDeployable.class.getName());
        } catch (NamingException e) {
            logger.debug("No war deployable (denotes currently an ear case)");
        }

        String hostName = null;

        // Initialize variable values from the war object
        warURL = war.getWarURL();
        contextRoot = war.getContextRoot();
        java2DelegationModel = war.getJava2DelegationModel();
        permissionManager = war.getPermissionManager();

        // Special handling for earURL
        earURL = war.getEarURL();
        if (earURL == null) {
            // no ear case, so no ear application name
            // TODO this flag has probably already been set before
            inEarCase = false;
            // set earURL to something only to avoid null value
            earURL = warURL;
        }
        // Special handling for hostName
        hostName = war.getHostName();
        if (hostName == null) {
            hostName = "";
        }

        // Install a new web application, whose web application archive is
        // at the specified URL, into this container with the specified
        // context root.
        // A context root of "" (the empty string) should be used for the root
        // application for this container. Otherwise, the context root must
        // start with a slash.

        if (contextRoot.equals("/")) {
            contextRoot = "";
        } else {
            contextRoot = "/" + contextRoot;
        }

        // Install the war.
        File fWar = URLUtils.urlToFile(warURL);

        // Unpacked path of the war
        File destDir = null;
        if (fWar.isDirectory()) {
            destDir = URLUtils.urlToFile(warURL);
        } else {
            destDir = URLUtils.urlToFile(unpackedWarURL);
        }

        // META-INF/context.xml file
        File contextXmlFile = new File(destDir, File.separator + "META-INF" + File.separator + "context.xml");

        // Check if some contexts were configured in server.xml
        List<JOnASStandardContext> jonasContexts = getConfiguredMatchingJonasContexts(contextRoot, fWar, destDir.getPath());

        Host deployer = null;
        // The context was not found in server.xml, a new context will be
        // created.
        if (jonasContexts.isEmpty()) {
            // Find host on which deploy the context
            deployer = findHost(hostName);

            // Create context (false because not in server.xml)
            JOnASStandardContext context = new JOnASStandardContext(false, java2DelegationModel, inEarCase);
            context.setDocBase(destDir.getPath());
            context.setPath(contextRoot);

            // Use the JOnAS context config
            ContextConfig config = createContextConfig();

            // add the config
            context.addLifecycleListener(config);
            jonasContexts.add(context);
        }

        // Configure these contexts and start them
        for (final JOnASStandardContext jStdCtx : jonasContexts) {

            // FIXME Only needed once (static reference)
            jStdCtx.setTomcatService(this);

            // Set the parent class loader
            jStdCtx.setParentClassLoader(webClassLoader);

            // Delegation model
            jStdCtx.setDelegate(java2DelegationModel);
            if (getLogger().isLoggable(BasicLevel.DEBUG)) {
                getLogger().log(BasicLevel.DEBUG, "Webapp class loader java 2 delegation model set to " + java2DelegationModel);
            }

            // Add the TLD that comes from OSGi services
            List<URL> resourcesToMonitor = getTLDResources();
            OSGiTldListener osGiTldListener = new OSGiTldListener(resourcesToMonitor);
            jStdCtx.addLifecycleListener(osGiTldListener);


            // Add valves
            // for setting unauthenticated user
            jStdCtx.addValve(new ResetAuthenticationValve());
            // For checking transactions
            if (transactionService != null) {
                jStdCtx.addValve(new TransactionValve(transactionService.getTransactionManager()));
            }
            // For checking resources
            if (resourceCheckerManager != null) {
                jStdCtx.addValve(new CheckOpenResourcesValve(resourceCheckerManager));
            }

            // For the audit
            Filter auditHttpFilter = null;
            if (getAuditService() != null) {
                auditHttpFilter = getAuditService().getWebAuditFilter();
                // needs to add this filter on the context
                jStdCtx.addValve(new FilterValveWrapper(auditHttpFilter));
            }

            // For the tenantId
            Filter tenantIdHttpFilter = null;
            String tenantId = null;
            if (getMultitenantService() != null){
                // get an instance of the filtre
                tenantId = super.getTenantId(war.getWarDeployable());
                tenantIdHttpFilter = getMultitenantService().getTenantIdFilter(tenantId);
                // needs to add this filter on the context
                jStdCtx.addValve(new FilterValveWrapper(tenantIdHttpFilter));
            }

            // Add the WS related ContextListener only if there are some webservices in the webapp
            if (!webservices.isEmpty() && (getJAXWSService() != null)) {
                WSContextLifecycleListener listener = new WSContextLifecycleListener(webservices,
                                                                                     getJAXWSService());
                jStdCtx.addLifecycleListener(listener);
            }

            // META-INF/context.xml file support
            if (contextXmlFile.exists()) {
                jStdCtx.setConfigFile(URLUtils.fileToURL(contextXmlFile));
            }

            // The Annotation processor object needs to be initialized with the
            // component context. Else, the processor does nothing.
            javax.naming.Context envCtx = null;
            try {
                envCtx = (javax.naming.Context) getNaming().getComponentContext(webClassLoader).lookup("comp/env");
            } catch (NamingException e) {
                 getLogger().log(BasicLevel.ERROR, "Cannot get the context of the webapplication '" + warURL + "'." , e);
            }

            // Set data for injection
            jStdCtx.setInjectionMap(webDD.getENCBindingHolder().getInjectionMap());
            jStdCtx.setEnvContext(envCtx);

            // JSR 77
            jStdCtx.setJ2EEServer(getJonasServerName());
            jStdCtx.setServer(J2eeObjectName.J2EEServer(getDomainName(), getJonasServerName()).toString());
            // Get info about JavaVM in standard J2EEServer MBean
            MBeanServer mbeanServer = jmxService.getJmxServer();
            ObjectName j2eeServerOn = J2eeObjectName.J2EEServer(getDomainName(), getJonasServerName());
            try {
                String[] as = (String[]) mbeanServer.getAttribute(j2eeServerOn, "javaVMs");
                jStdCtx.setJavaVMs(as);
            } catch (Exception e) {
                // String err =
                // i18n.getMessage("CatJWebServ.doRegisterWar.startContextError",
                // jStdCtx);
                // getLogger().log(BasicLevel.WARN, err + e.getMessage());
                getLogger().log(BasicLevel.WARN, "Set MBean JVM error : " + e.getClass().getName() + " " + e.getMessage());
            }
            if (earAppName != null) {
                jStdCtx.setJ2EEApplication(earAppName);
            } else {
                jStdCtx.setJ2EEApplication("null");
            }
            jStdCtx.setJonasDeploymentDescriptor(webDD.getJOnASXmlContent());
            jStdCtx.setWebDeploymentDescriptor(webDD);

            // Set the realm
            org.apache.catalina.Realm ctxRealm = jStdCtx.getRealm();

            // Take realm of parent
            if (ctxRealm == null) {
                ctxRealm = deployer.getRealm();
            }

            if (ctxRealm != null && ctxRealm instanceof Realm) {
                Realm jaccRealm = null;
                try {
                    jaccRealm = (Realm) ((Realm) ctxRealm).clone();
                } catch (CloneNotSupportedException cnse) {
                    String err = "Cannot clone the realm used by the existing context or its parent realm";
                    getLogger().log(BasicLevel.ERROR, err + cnse.getMessage());
                    throw new JWebContainerServiceException(err, cnse);
                }
                if (getLogger().isLoggable(BasicLevel.DEBUG)) {
                    getLogger().log(BasicLevel.DEBUG, "Setting permission manager to " + permissionManager);
                }
                jaccRealm.setPermissionManager(permissionManager);
                jaccRealm.setSecurityService(securityService);
                jaccRealm.setContext(jStdCtx);
                // Set the new realm
                jStdCtx.setRealm(jaccRealm);
            }

            // Set the attributes transfered from the JOnAS War MBean.
            // Note that 'java2DelegationModel' and 'inEarCase' attributes
            // are passed as arguments to the JOnASStandardContext's constructor
            jStdCtx.setWarURL(warURL);
            jStdCtx.setFilteringClassLoader(filteringClassLoader);
            if (inEarCase) {
                jStdCtx.setEarURL(earURL);
                jStdCtx.setEjbClassLoader(ejbClassLoader);
                jStdCtx.setEarClassLoader(earClassLoader);
            }

            // Create our own InstanceManager using a Lifecycle event callback
            // This should be done before calling customizers as they may register lifecycle callbacks too.
            final javax.naming.Context envContext = envCtx;
            final WebContainerDeploymentDesc webContainerDeploymentDesc = webDD;

            jStdCtx.addLifecycleListener(new LifecycleListener() {
                public void lifecycleEvent(LifecycleEvent event) {
                    if (Lifecycle.CONFIGURE_START_EVENT.equals(event.getType())) {
                        final StandardContext standardContext = ((StandardContext) event.getLifecycle());
                        Map<String, Map<String, String>> injectionMap = webContainerDeploymentDesc.getENCBindingHolder().getInjectionMap();
                        if (injectionMap == null) {
                            injectionMap = new HashMap<String, Map<String, String>>();
                        }
                        final DefaultInstanceManager instanceManager = new DefaultInstanceManager(
                                envContext,
                                injectionMap,
                                standardContext,
                                this.getClass().getClassLoader());
                        standardContext.setInstanceManager(instanceManager);
                        standardContext.getServletContext()
                                .setAttribute(InstanceManager.class.getName(), standardContext.getInstanceManager());
                    }
                }
            });

            // Call the Context Customizers just before startup
            for (ContextCustomizer customizer : customizers) {
                customizer.customize(jStdCtx, deployable);
            }

            // ... and start it or add it
            if (jStdCtx.isInServerXml()) {
                try {
                    jStdCtx.setLoader(null);
                    jStdCtx.start();
                } catch (LifecycleException lce) {
                    logger.error("Cannot start the context '" + jStdCtx + "'.", lce);
                    throw new JWebContainerServiceException("Cannot start the context '" + jStdCtx + "'.", lce);
                }
            } else {
                // add it
                if (deployer == null) {
                    logger.error("No deployer found for the deployment of the context '" + jStdCtx + "'.");
                    throw new JWebContainerServiceException("No deployer found for the deployment of the context '" + jStdCtx
                            + "'.");
                }
                if ("".equals(hostName) && "".equals(contextRoot)) {
                    removeDummyContextRoot();
                }

                if (isMultitenantEnabled()) {
                    Object oldTenantCtx = getMultitenantService().getTenantContext();
                    try {
                        if (tenantId != null) {
                            getMultitenantService().setTenantIdInContext(tenantId);
                        }
                        deployer.addChild(jStdCtx);
                    } finally {
                        getMultitenantService().setTenantContext(oldTenantCtx);
                    }
                } else {
                    deployer.addChild(jStdCtx);
                }
            }

            // Finalize webservices deployment when the context is started
            if (this.getJAXWSService() != null) {
                // Tell the JAXWS Service that this context deployment is over
                getJAXWSService().finalizePOJODeployment(jStdCtx.getServletContext());
            }

            // ...and check if it is now configured
            checkStartedContext(jStdCtx, permissionManager);

            // Store the Tomcat web-app ClassLoader
            war.setClassLoader(jStdCtx.getLoader().getClassLoader());

            // Store WebModule ObjectName in Context
            try {
                ctx.rebind("WebModule", jStdCtx.getObjectName());
            } catch (Exception e) {
                // NamingException or Mbean related Exception
                // TODO i18n
                String err = "Cannot rebind WebModule ObjectName in Context";
                logger.error(err, e);
                throw new JWebContainerServiceException(err, e);
            }
            ContextFinder.addNonVersionedContext(contextRoot);

            try {
                if (isVersioningEnabled() && contextRoot.length() > 0 && userURI.length() > 0) {
                    if (contextRoot.charAt(0) != '/') {
                        contextRoot = '/' + contextRoot;
                    }
                    if (userURI.charAt(0) != '/') {
                        userURI = '/' + userURI;
                    }

                    if (!contextRoot.equals(userURI)) {
                        logger.info("Deploying versioned application " + userURI
                                + ", path for the added version is " + contextRoot);
                        String policy = getVersioningService().getDefaultDeploymentPolicy();
                        if (earAppName != null) {
                            ContextFinder.bindContextRoot(earAppName, userURI, jStdCtx, policy);
                        } else {
                            ContextFinder.bindContextRoot(URLUtils.urlToFile(warURL).getName(), userURI, jStdCtx, policy);
                        }
                    }
                }
            } catch (Exception e) {
                throw new JWebContainerServiceException("Failed binding versioned web context", e);
            }
        }
    }

    /**
     * @return a new {@link ContextConfig} for JOnAS.
     */
    protected ContextConfig createContextConfig() {
        JOnASContextConfig config = new JOnASContextConfig();
        config.setSecurityService(securityService);

        if (this.tomcat7ContextConfPath != null) {
            config.setDefaultContextXml(this.tomcat7ContextConfPath);
        }
        if (this.tomcat7WebConfPath != null) {
            config.setDefaultWebXml(this.tomcat7WebConfPath);
        }

        // Add all the System wide ServletContainerInitializers registered ATM
        // TODO As theses are services, how do we support their dynamism here ?
        for (ServletContainerInitializer initializer : initializers) {
            config.addServletContainerInitializer(initializer);
        }

        return config;
    }

    /**
     * Creates a new JOnAS Realm (JACC or JAAS).
     * @param name resource name or entry name (depends if a JACC or JAAS realm is required)
     * @param isJaccRealm <code>true</code> if a JACC realm is required, false for a JAAS realm.
     * @return a configured Realm.
     */
    public Realm createJOnASRealm(final String name, final boolean isJaccRealm) {
        Realm realm = new Realm();
        realm.setSecurityService(getSecurityService());
        if (isJaccRealm) {
            realm.setResourceName(name);
        } else {
            realm.setJaasEntry(name);
        }
        return realm;
    }

    /**
     * Check if there is a previous JOnASStandardContext which match the current
     * context If true, deploy our context into the configured context.
     * @param contextRoot name of the context that we want to find preconfigured
     *        contexts
     * @param fpackedWar file of the original war file
     * @param destDir name of the unpacked directory of the war file
     * @return true if a context was found and used
     */
    protected List<JOnASStandardContext> getConfiguredMatchingJonasContexts(final String contextRoot,
                                                                            final File fpackedWar,
                                                                            final String destDir) {
        List<JOnASStandardContext> jonasContexts = new ArrayList<JOnASStandardContext>();
        // Check contexts of all services to see if there is a configured
        // context.
        // that we must use instead of creating a new one.
        // If a context is matching, we set its docbase and start it.
        for (Context ctx : getContexts()) {
            // Continue if it is not a JOnAS context
            if (!(ctx instanceof JOnASStandardContext)) {
                continue;
            }
            JOnASStandardContext jStdCtx = (JOnASStandardContext) ctx;
            if (jStdCtx != null) {
                // Not created in server.xml file
                if (!jStdCtx.isInServerXml()) {
                    continue;
                }

                // The context was configured ?
                String serverCtxRoot = jStdCtx.getPath();
                if (!serverCtxRoot.equals(contextRoot)) {
                    continue;
                }
                if (jStdCtx.getPrivileged()) {
                    // Can deploy a privileged context only if it's a .war file
                    if (fpackedWar.isDirectory()) {
                        logger.error("Can deploy a privileged context '" + jStdCtx
                                + "' only if it's a war file (and not a directory)");
                    }
                    jStdCtx.setDocBase(fpackedWar.getPath());
                } else {
                    jStdCtx.setDocBase(destDir);
                }
                jonasContexts.add(jStdCtx);
            }
        }
        return jonasContexts;
    }

    /**
     * Check that the context that was started was right configured.
     * @param context context to check
     * @param permissionManager the permission manager used for JACC
     * @throws JWebContainerServiceException if the context was not right
     *         configured
     */
    protected void checkStartedContext(final Context context, final PermissionManager permissionManager)
            throws JWebContainerServiceException {
        // Check if the context was configured
        if (!context.getConfigured()) {
            throw new JWebContainerServiceException("Context '" + context + "' was not configured");
        }

        if (!LifecycleState.STARTED.equals(context.getState())) {
            throw new JWebContainerServiceException("Context '" + context + "' has failed to start.");
        }

        // Check that JACC realm is correctly set
        org.apache.catalina.Realm ctxRealm = context.getRealm();
        if (ctxRealm != null && ctxRealm instanceof Realm) {
            Realm jaccRealm = (Realm) ctxRealm;
            PermissionManager ctxPerm = jaccRealm.getPermissionManager();
            if (ctxPerm == null && permissionManager != null) {
                jaccRealm.setPermissionManager(permissionManager);
                // set context on the realm.
                jaccRealm.setContext(context);
            }
        }
        if (getLogger().isLoggable(BasicLevel.DEBUG)) {
            getLogger().log(BasicLevel.DEBUG, "context " + context + " started");
        }

    }

    /**
     * Gets all the engines of the current Tomcat server.
     * @return all the engines of the current Tomcat server
     * @throws JWebContainerServiceException if engines can not be retrieved
     */
    protected synchronized List<StandardEngine> getEngines() throws JWebContainerServiceException {
        List<StandardEngine> engines = new ArrayList<StandardEngine>();
        Service[] services = getServer().findServices();

        // Get contexts of each engine of each service
        for (int s = 0; s < services.length; s++) {
            Container cont = services[s].getContainer();
            if (!(cont instanceof StandardEngine)) {
                String err = "The container of the service must be an engine (server.xml)";
                throw new JWebContainerServiceException(err);
            }
            engines.add((StandardEngine) cont);
        }
        return engines;
    }

    /**
     * Gets all the contexts of the current Tomcat server.
     * @return all the contexts of the current Tomcat server
     * @throws JWebContainerServiceException if contexts can not be retrieved
     */
    protected synchronized List<Context> getContexts() throws JWebContainerServiceException {
        List<Context> contexts = new ArrayList<Context>();
        for (Engine engine : getEngines()) {
            Container[] hosts = engine.findChildren();

            for (int j = 0; j < hosts.length; j++) {
                Container[] containers = hosts[j].findChildren();
                for (int k = 0; k < containers.length; k++) {
                    Container container = containers[k];
                    if (container instanceof Context) {
                        contexts.add((Context) container);
                    }
                }
            }
        }
        return contexts;
    }

    /**
     * Undeploy a specific WAR file specified in the context.
     * @param ctx the context which contains the configuration in order to
     *        undeploy a WAR.
     * @throws JWebContainerServiceException if the unregistration failed.
     */
    @Override
    protected void doUnRegisterWar(final javax.naming.Context ctx) throws JWebContainerServiceException {

        // Get the 2 parameters :
        // - contextRoot is the context root to be removed (required param).
        // - hostName is the name of the host to remove the war (optional).
        String contextRoot = null;
        try {
            contextRoot = (String) ctx.lookup("contextRoot");
        } catch (NamingException e) {
            throw new JWebContainerServiceException("Unable to find the contextRoot parameter", e);
        }

        // Change the root context to ""
        if (contextRoot.equals("/")) {
            contextRoot = "";
        } else {
            contextRoot = "/" + contextRoot;
        }
        ContextFinder.removeNonVersionedContext(contextRoot);

        String hostName = null;
        try {
            hostName = (String) ctx.lookup("hostName");
        } catch (NamingException e) {
            logger.debug("No hostname was defined for this context");
        }

        // Undeploy all matching context if there is no hostname provided
        if (hostName == null) {
            boolean found = false;
            for (Context context : getContexts()) {
                String serverCtxRoot = context.getPath();
                if (serverCtxRoot.equals(contextRoot)) {
                    removeContext(context);
                    found = true;
                    break;
                }
            }
            if (!found) {
                throw new JWebContainerServiceException("Unable to remove a context with the context name '"
                        + contextRoot + "'.");
            }
        } else {
            // Remove the specified Context from the set of defined Contexts
            // for its associated Host.
            Host host = findHost(hostName);
            org.apache.catalina.Context context = (org.apache.catalina.Context) host.findChild(contextRoot);
            if (context != null) {
                removeContext(context);
            } else {
                throw new JWebContainerServiceException("Unable to find a context for the given hostname '" + hostName + "'.");
            }
        }

        if (("".equals(hostName) || hostName == null) && "".equals(contextRoot)) {
            addDummyContextRoot();
        }

        try {
            if (isVersioningEnabled() && contextRoot.length() > 0) {
                if (contextRoot.charAt(0) != '/') {
                    contextRoot = '/' + contextRoot;
                }
                if (ContextFinder.unbindContextRoot(contextRoot)) {
                    logger.info("Undeployed version " + contextRoot);
                }
            }
        } catch (Exception e) {
            throw new JWebContainerServiceException("Failed unbinding web context", e);
        }
    }

    /**
     * Remove the specified Context from the set of defined Contexts for its
     * associated Host. If this is the last Context for this Host, the Host will
     * also be removed.
     * @param context The Context to be removed
     * @throws JWebContainerServiceException if the context can not be removed
     */
    public synchronized void removeContext(final Context context) throws JWebContainerServiceException {

        // Is this Context actually among those that are deployed ?
        boolean found = false;
        Iterator<Context> it = getContexts().iterator();
        while (it.hasNext() && !found) {
            if (context == it.next()) {
                found = true;
            }
        }

        if (!found) {
            return;
        }
        // Remove this Context from the associated Host
        // Or stop it if it is a JOnAS standard context (to keep the
        // configuration)
        if (context instanceof JOnASStandardContext) {
            JOnASStandardContext jctx = (JOnASStandardContext) context;
            if (jctx.isInServerXml()) {
                try {
                    ((JOnASStandardContext) context).stop();
                } catch (LifecycleException le) {
                    throw new JWebContainerServiceException("Cannot stop context (" + le.getMessage() + ")");
                }
            } else {
                // must not persist
                context.getParent().removeChild(context);
            }
        } else {
            context.getParent().removeChild(context);
            // Bug Tomcat 5 doesn't remove MBean WebModule
            // Delete the next line when will be fixed
            // Bug Tomcat 5 : begin
            if (context instanceof StandardContext) {
                StandardContext ctx = (StandardContext) context;
                try {
                    // unregister WebModule MBean
                    jmxService.getJmxServer().unregisterMBean(ctx.getObjectName());
                } catch (Exception e) {
                    getLogger().log(BasicLevel.ERROR,
                            "Cannot remove the MBean for the WebModule " + ctx.getObjectName() + " : " + e.getMessage());
                }
            }
        }

    }

    /**
     * The server is started ?
     * @return boolean true if the catalina container is running.
     */
    public boolean isTomcatStarted() {
        return tomcatStarted;
    }

    /**
     * Set the server instance we are configuring.
     * @param server The new server
     */
    public void setServer(final Server server) {
        this.server = server;
    }

    /**
     * @return the server instance
     */
    public synchronized Server getServer() {
        // Needs access to the server object
        if (server == null) {
            startInternalWebContainer();
        }
        return server;
    }


    /**
     * Return a File object representing the server.xml configuration file.
     * @return a File object representing the server.xml configuration file.
     * @throws FileNotFoundException if the configuration file is not found.
     */
    protected File getConfigFile() throws FileNotFoundException {

        if (this.tomcat7ServerConfPath != null) {
            return new File(this.tomcat7ServerConfPath);
        }

        //TODO to remove when JOnAS'll be assembled with addons
        String fileName = System.getProperty("catalina.base");
        fileName = fileName + File.separator + CONFIG_FILE;
        File file = new File(fileName);
        if (!file.exists()) {
            throw new FileNotFoundException("The configuration file '" + fileName + "' does not exists.");
        }
        return (file);
    }

    /**
     * Find the specified host.
     * @param hostName the name of the host to find.
     * @return the host found.
     * @throws JWebContainerServiceException if the specified host cannot be
     *         found.
     */
    public Host findHost(final String hostName) throws JWebContainerServiceException {

        Service[] services = getServer().findServices();
        // Check number of services
        if (services.length < 1) {
            String err = "At least one service must be define in the server.xml of Tomcat";
            throw new JWebContainerServiceException(err);
        }

        // Two cases :
        // 1/ No host specified
        // -> defaulthost of the engine of the first service.
        // 2) A host is specified in the jonas-web.xml
        // -> find the host and deploy on this host

        if (hostName == null || hostName.equals("")) {
            // First case

            // Take first service
            Service service = services[0];

            Container cont = service.getContainer();
            if (!(cont instanceof Engine)) {
                String err = "The container of the service must be an engine";
                throw new JWebContainerServiceException(err);
            }

            Engine engine = (Engine) cont;
            String defaultHost = engine.getDefaultHost();
            if (defaultHost == null) {
                String err = "Default host must be specified in server.xml or host must be specified in jonas-web.xml";
                throw new JWebContainerServiceException(err);
            }
            Container child = engine.findChild(defaultHost);
            // found, return it
            if (child instanceof Host) {
                return (Host) child;
            }
            // else error
            String err = "Default host " + defaultHost + " not found";
            throw new JWebContainerServiceException(err);
        }
        // Get all hosts.
        List<Host> hosts = new ArrayList<Host>();

        for (int s = 0; s < services.length; s++) {
            Container cont = services[s].getContainer();
            if (!(cont instanceof Engine)) {
                String err = "The container of a service must be an engine";
                throw new JWebContainerServiceException(err);
            }
            Engine engine = (Engine) cont;
            Container child = engine.findChild(hostName);
            if (child instanceof Host) {
                hosts.add((Host) child);
            }
        }

        // error
        if (hosts.size() == 0) {
            // No host found.
            String err = "Host " + hostName + " not found in all services/Engine of server.xml";
            throw new JWebContainerServiceException(err);
        }

        // first element
        return hosts.get(0);
    }

    /**
     * Create and configure the Digester that will be used for the xml parsing
     * of the configuration file.
     * @return Digester the digester containing the rules for the xml parsing of
     *         the server.xml.
     */
    protected Digester createServerDigester() {

        // Initialize the digester
        Digester digester = new Digester();
        digester.setValidating(false);
        digester.addRuleSet(new JCatalinaRuleSet(externalLoader, securityService));

        // Use context class loader.
        // Could avoid problem for users putting digester in JONAS_ROOT/lib/ext
        // folder
        digester.setUseContextClassLoader(true);
        return (digester);

    }

    /**
     * Update info of the serverName and serverVersion.
     */
    @Override
    protected void updateServerInfos() {
        String infos = org.apache.catalina.util.ServerInfo.getServerInfo();
        StringTokenizer st = new StringTokenizer(infos, "/");
        if (st.countTokens() != 2) {
            setServerName(infos);
            setServerVersion("");
        } else {
            setServerName(st.nextToken());
            setServerVersion(st.nextToken());
        }
    }

    /**
     * Return the Default host name of the web container.
     * @return the Default host name of the web container.
     * @throws JWebContainerServiceException when default host cannot be
     *         resolved (multiple services).
     */
    @Override
    public String getDefaultHost() throws JWebContainerServiceException {
        Engine engine = (Engine) getFirstService().getContainer();
        return engine.getDefaultHost();
    }

    /**
     * Return the Default HTTP port number of the web container. Returns the
     * first connector port if more than one are defined.
     * @return the Default HTTP port number of the web container.
     * @throws JWebContainerServiceException when default HTTP port cannot be
     *         resolved or none are defined.
     */
    @Override
    public String getDefaultHttpPort() throws JWebContainerServiceException {
        // if server not yet started, analyze the .xml file
        if (!tomcatStarted) {

            File configFile = null;
            try {
                configFile = getConfigFile();
            } catch (FileNotFoundException e) {
                throw new JWebContainerServiceException("Cannot get configuration file", e);
            }

            InputStream is = null;
            try {
                is = new FileInputStream(configFile);
                DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
                domFactory.setNamespaceAware(true);
                DocumentBuilder builder = null;
                try {
                    builder = domFactory.newDocumentBuilder();
                } catch (ParserConfigurationException e) {
                    throw new JWebContainerServiceException("Cannot build document builder", e);
                }

                Document doc = null;
                try {
                    doc = builder.parse(is);
                } catch (SAXException e) {
                    throw new JWebContainerServiceException("Cannot analyze configuration file", e);
                } catch (IOException e) {
                    throw new JWebContainerServiceException("Cannot analyze configuration file", e);
                }
                XPathFactory xPathFactory = XPathFactory.newInstance();
                XPath xPath = xPathFactory.newXPath();
                XPathExpression xPathExpr = null;
                try {
                    xPathExpr = xPath.compile("//Server/Service/Connector[@protocol='HTTP/1.1']");
                } catch (XPathExpressionException e) {
                    throw new JWebContainerServiceException("Cannot analyze configuration file", e);
                }
                Node node = null;
                try {
                    node = (Node) xPathExpr.evaluate(doc, XPathConstants.NODE);
                } catch (XPathExpressionException e) {
                    throw new JWebContainerServiceException("Cannot analyze configuration file", e);
                }
                if (node == null) {
                    // No HTTP connector found, return null
                    return null;
                }
                String text = node.getAttributes().getNamedItem("port").getNodeValue();
                return text;
            } catch (FileNotFoundException e) {
                throw new JWebContainerServiceException("Cannot get input stream", e);
            } finally {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e) {
                        logger.debug("Cannot close input stream", e);
                    }
                }
            }
        }
        int port = getPort("http");
        if (port == -1) {
            return null;
        }
        return String.valueOf(port);
    }

    /**
     * Return the Default HTTPS port number of the web container (can be null if
     * multiple HTTPS connector has been set).
     * @return the Default HTTPS port number of the web container.
     * @throws JWebContainerServiceException when default HTTPS port cannot be
     *         resolved (0 occurences).
     */
    @Override
    public String getDefaultHttpsPort() throws JWebContainerServiceException {
        int port = getPort("https");
        if (port == -1) {
            return null;
        }
        return String.valueOf(port);
    }


    /**
     * Finds the first connector supporting the given URL scheme
     * and return its listening port.
     * @param scheme URL schema (<tt>http</tt>/<tt>https</tt>)
     * @return Port number for a given scheme.
     * @throws JWebContainerServiceException if no connectors were found
     */
    protected int getPort(final String scheme) throws JWebContainerServiceException {
        Service svc = getFirstService();
        List<Connector> conn = new ArrayList<Connector>();
        for (Connector c : svc.findConnectors()) {
            if (c.getScheme().equalsIgnoreCase(scheme)) {
                conn.add(c);
            }
        }

        if (conn.isEmpty()) {
            // No connectors defined
            return -1;
        }

        Connector c = conn.get(0);

        // Warn the administrator if there are more than one connectors
        // specified for a single scheme.
        if (conn.size() > 1) {
            if (getLogger().isLoggable(BasicLevel.WARN)) {
                getLogger().log(
                        BasicLevel.WARN,
                        "Found multiple Connectors for scheme '" + scheme + "' in " + getConfigPath()
                                + ", using first by default! (port:" + c.getPort() + ")");
            }
        }

        return c.getPort();
    }

    /**
     * @return Returns the first found Service
     */
    protected Service getFirstService() {
        Service[] svc = getServer().findServices();
        // throw exception if no service is defined
        if ((svc == null) || (svc.length == 0)) {
            // No Services specified.
            throw new JWebContainerServiceException("No Services found in " + getConfigPath());
        }
        // Warn the administrator if there are more than one services
        // specified in the configuration file.
        if (svc.length > 1) {
            if (getLogger().isLoggable(BasicLevel.WARN)) {
                getLogger().log(BasicLevel.WARN, "Found multiple Services in " + getConfigPath() + ", using first by default!");
            }
        }
        return svc[0];
    }

    /**
     * Register a WAR by delegating the operation to the registerWar() method.
     * This is used for JMX management.
     * @param fileName the name of the war to deploy.
     * @throws RemoteException if rmi call failed.
     * @throws JWebContainerServiceException if the registration failed.
     */
    @Override
    public void registerWar(final String fileName) throws RemoteException, JWebContainerServiceException {
        ClassLoader old = null;
        try {
            old = Thread.currentThread().getContextClassLoader();
            Thread.currentThread().setContextClassLoader(externalLoader);
            super.registerWar(fileName);
        } catch (Exception e) {
            throw new ServiceException("Exception during registering war", e);
        } finally {
            if (old != null) {
                Thread.currentThread().setContextClassLoader(old);
            }
        }
    }

    /**
     * Unregister a WAR by delegating the operation to the unRegisterWar()
     * method. This is used for JMX management.
     * @param fileName the name of the war to undeploy.
     * @throws RemoteException if rmi call failed.
     * @throws JWebContainerServiceException if the unregistration failed.
     */
    @Override
    public void unRegisterWar(final String fileName) throws RemoteException, JWebContainerServiceException {
        ClassLoader old = null;
        try {
            old = Thread.currentThread().getContextClassLoader();
            Thread.currentThread().setContextClassLoader(externalLoader);
            super.unRegisterWar(fileName);
        } catch (Exception e) {
            throw new ServiceException("Exception during unregistering war", e);
        } finally {
            if (old != null) {
                Thread.currentThread().setContextClassLoader(old);
            }
        }
    }

    /**
     * @param securityService Security service to set.
     */
    public void setSecurityService(final SecurityService securityService) {
        this.securityService = securityService;
    }

    /**
     * @return Security service.
     */
    protected SecurityService getSecurityService() {
        return securityService;
    }

        /**
     * @param transactionService TX service to set.
     */
    public void setTransactionService(final TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    /**
     * @return TX service.
     */
    protected TransactionService getTransactionService() {
        return transactionService;
    }

    /**
     * @return the resource checker manager
     */
    public IResourceCheckerManager getResourceCheckerManager() {
        return resourceCheckerManager;
    }

    /**
     * Sets the resource checker manager.
     * @param resourceCheckerManager the given instance
     */
    public void setResourceCheckerManager(final IResourceCheckerManager resourceCheckerManager) {
        this.resourceCheckerManager = resourceCheckerManager;
    }

    /**
     * Unset the resource checker manager.
     */
    public void unsetResourceCheckerManager() {
        this.resourceCheckerManager = null;
    }

    /**
     * Creates an instance of the http service factory.
     * @return an instance of the httpservice factory
     */
    @Override
    protected HttpServiceFactory<Tomcat7Service> createHttpServiceFactory() {
        return new org.ow2.jonas.web.tomcat7.osgi.httpservice.HttpServiceFactory(this);
    }

    /**
     * Add a new Context Customizer.
     * @param customizer the added Customizer
     */
    public void addContextCustomizer(final ContextCustomizer customizer) {
        this.customizers.add(customizer);
    }

    /**
     * Remove the given Customizer.
     * @param customizer the removed Customizer
     */
    public void removeContextCustomizer(final ContextCustomizer customizer) {
        this.customizers.remove(customizer);
    }

    public void addServletContainerInitializer(final ServletContainerInitializer initializer) {
        this.initializers.add(initializer);
    }

    public void removeServletContainerInitializer(final ServletContainerInitializer initializer) {
        this.initializers.remove(initializer);
    }

    /**
     * Registers a dummy servlet that always returns an HTTP error 404.<br/>
     * <br/>
     * That is required to implement http://jira.ow2.org/browse/JONAS-250 on Tomcat 7.x
     */
    protected void addDummyContextRoot() {
        final HttpService httpService = getService(HttpService.class);

        final HttpContext httpContext = httpService.createDefaultHttpContext();

        final VersioningService versioning = this.getVersioningService();
        boolean versioningEnabled = false;
        if (versioning != null) {
            versioningEnabled = versioning.isVersioningEnabled();
        }
        try {
            if (versioning != null) {
                versioning.setVersioningEnabled(false);
            }
            httpService.registerServlet("/", new EmptyServlet(), null, httpContext);
        } catch (ServletException e) {
            logger.warn("Cannot register the fake root servlet", e);
        } catch (NamespaceException e) {
            logger.warn("Cannot register the fake root servlet", e);
        } finally {
            if (versioning != null) {
                versioning.setVersioningEnabled(versioningEnabled);
            }
        }
    }

    /**
     * Unregisters the dummy servlet that always returns an HTTP error 404.<br/>
     * <br/>
     * That is required to implement http://jira.ow2.org/browse/JONAS-250 on Tomcat 7.x
     */
    protected void removeDummyContextRoot() {
        final HttpService httpService = getService(HttpService.class);

        final VersioningService versioning = this.getVersioningService();
        boolean versioningEnabled = false;
        if (versioning != null) {
            versioningEnabled = versioning.isVersioningEnabled();
        }
        try {
            if (versioning != null) {
                versioning.setVersioningEnabled(false);
            }
            httpService.unregister("/");
        } finally {
            if (versioning != null) {
                versioning.setVersioningEnabled(versioningEnabled);
            }
        }
    }

    protected <T> T getService(final Class<T> serviceType) {
        final String serviceName = serviceType.getName();
        ServiceReference serviceReference = this.getBundleContext().getServiceReference(serviceName);

        if (serviceReference != null) {
            Object service = this.getBundleContext().getService(serviceReference);
            if (service != null) {
                if (!serviceType.isInstance(service)) {
                    throw new IllegalStateException("OSGi service " + service + " not instance of " + serviceName);
                }

                return serviceType.cast(service);
            }
        }

        throw new IllegalStateException("Cannot find OSGi service " + serviceName);
    }

    /**
     * Add the given TLD URL as a resource.
     * @param tldResource the given TLD resource
     */
    @Override
    public void addTldResource(final URL tldResource) {
       super.addTldResource(tldResource);
    }

    /**
     * Remove the given TLD URL as a resource.
     * @param tldResource the given TLD resource
     */
    @Override
    public void removeTldResource(final URL tldResource) {
        super.removeTldResource(tldResource);
    }

    /**
     * ${@inheritDoc}
     */
    @Override
    public void registerAddonConfig(final IAddonConfig addonConfig) {
        super.registerAddonConfig(addonConfig);
        File file = addonConfig.getConfigurationFile(TOMCAT7_CONTEXT_CONFIG_FILENAME);
        if (file != null) {
            this.tomcat7ContextConfPath = file.getAbsolutePath();
        }
        file = addonConfig.getConfigurationFile(TOMCAT7_SERVER_CONFIG_FILENAME);
        if (file != null) {
            this.tomcat7ServerConfPath = file.getAbsolutePath();
        }
        file = addonConfig.getConfigurationFile(TOMCAT7_WEB_CONFIG_FILENAME);
        if (file != null) {
            this.tomcat7WebConfPath = file.getAbsolutePath();
        }
    }

    /**
     * ${@inheritDoc}
     */
    @Override
    public void unregisterAddonConfig(final IAddonConfig addonConfig) {
        super.unregisterAddonConfig(addonConfig);
        this.tomcat7ServerConfPath = null;
        this.tomcat7WebConfPath = null;
        this.tomcat7ContextConfPath = null;
    }

    //TODO to remove when JOnAS will be assembled with addons
    private String getConfigPath() {
        if (this.tomcat7ServerConfPath != null) {
            return this.tomcat7ServerConfPath;
        } else {
            return CONFIG_FILE;
        }
    }

    /**
     * Tomcat7Connector interface: add a connector
     * @param serviceName name of the service on which the connector must be added
     * @param address address of the connector
     * @param port port number
     * @param isAjp true if AJP protocol must be used
     * @param isSSL true if HTTPS scheme must be used
     * @param enableLookups true if enableLookups must be set
     */
    public void addConnector(String serviceName, String address, int port, boolean isAjp, boolean isSSL, boolean enableLookups) throws Exception {

        Service service = getService(serviceName);
        // Warning currently (Tomcat 7.0.27) the only way to actually initialize the protocol handler, is to specify the protocol name
        // through the constructor.
        Connector connector = new Connector(isAjp ? "AJP/1.3" : "HTTP/1.1");
        if ((address != null) && (address.length() > 0)) {
            connector.setProperty("address", address);
        }

        // Set port number
        connector.setPort(port);
        // Set SSL
        connector.setSecure(isSSL);
        connector.setScheme(isSSL ? "https" : "http");

        // Set enableLookups
        connector.setEnableLookups(enableLookups);

        // OnDemand feature enabled ?
        if (isOnDemandFeatureEnabled()) {
            HttpOnDemandProxy onDemandProxy = getWarDeployer().getOnDemandProxy();

            // Not yet existing
            if (onDemandProxy == null) {

                // Build a new random port
                int randomPort = getRandomPort();

                // Add the connector
                connector.setProperty("maxKeepAliveRequests", "1");
                connector.setPort(randomPort);
                connector.setProxyPort(port);
                service.addConnector(connector);

                // Build a new OnDemand Proxy
                setOnDemandRedirectPort(randomPort);
                setDefaultHttpPort(port);

                // connector already added. skip
                return;
            }
        }
        // No HTTP port yet, so as we're adding a connector, sets the value
        if (getDefaultHttpPort() == null) {
            setDefaultHttpPort(port);

        }


        service.addConnector(connector);
    }


    /**
     * Tomcat7Connector interface: destroy a connector
     * @param serviceName name of the service on which the connector must be added
     * @param address address of the connector
     * @param port port number
     */
    public void destroyConnector(String serviceName, String address, int port) throws Exception {

        Service service = getService(serviceName);

        Connector connector = getConnector(service, address, port);

        if (connector != null) {

            connector.stop();
            service.removeConnector(connector);
            logger.debug("Tomcat connector stopped '" + address + "/" + port + "'");

            connector.destroy();
            logger.debug("Tomcat connector destroyed '" + address + "/" + port + "'");

        } else {
            throw new ServiceException("Unable to find a connector '" + serviceName + "/" + address + "/" + port + "'.");
        }

    }

    /**
     * Tomcat7Connector interface: start a connector
     * @param serviceName name of the service on which the connector must be added
     * @param address address of the connector
     * @param port port number
     */
    public void startConnector(String serviceName, String address, int port) throws Exception {

        Service service = getService(serviceName);
        Connector connector = getConnector(service, address, port);
        connector.start();
    }

    /**
     * Tomcat7Connector interface: stop a connector
     * @param serviceName name of the service on which the connector must be added
     * @param address address of the connector
     * @param port port number
     */
    public void stopConnector(String serviceName, String address, int port) throws Exception {

        Service service = getService(serviceName);
        Connector connector = getConnector(service, address, port);
        connector.stop();
    }


    /**
     * Set an attribute no a specified connector
     * @param serviceName tomcat standard service
     * @param address ip address
     * @param port port number
     * @param attribute attribute name
     * @param value attribute value
     */
    public void setConnectorAttribute(String serviceName, String address, int port, String attribute, String value) throws Exception {
        Service service = getService(serviceName);

        Connector connector = getConnector(service, address, port);

        if (connector != null) {
            connector.setProperty(attribute, value);
            logger.debug("Tomcat connector updated '" + address + "/" + port + "' - '" + attribute + "'='" + value + "'");

            // check the value
            logger.debug("Check value '" + connector.getProperty(attribute) + "'");

        } else {
            throw new ServiceException("Unable to find a connector '" + serviceName + "/" + address + "/" + port + "'.");
        }
    }

    /**
     * Get a connector
     * @param service tomcat standard service
     * @param address ip address
     * @param port port number
     */
    private Connector getConnector(Service service, String address, int port) {

        Connector conns[] = service.findConnectors();

        for (int i = 0; i < conns.length; i++) {
            String connAddress = String.valueOf(conns[i].getProperty("address"));
            int connPort = conns[i].getPort();
            // If OnDemand is enabled, the connector port may be the internal onDemand connector
            // So we've to check this match too
            if (isOnDemandFeatureEnabled()) {
                if (connPort == getOnDemandRedirectPort()) {
                    connPort = getWarDeployer().getOnDemandProxy().getHttpPortNumber();
                }
            }


            // if (((address.equals("null")) &&
            if ((connAddress == null) && port == connPort) {
                return conns[i];
            }
            // } else if (address.equals(connAddress))
            if (connAddress.contains(address) && port == connPort) {
                return conns[i];
            }
        }
        return null;

    }

    /**
     * Get a service
     * @param serviceName service name
     */
    private Service getService(String serviceName) {

        Service service;
        if (serviceName == null) {
            service = getFirstService();
        } else {
            service = getServer().findService(serviceName);
        }
        return service;

    }

}
