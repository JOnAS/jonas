/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2010 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.web.tomcat7.custom;

import org.apache.catalina.core.StandardContext;
import org.ow2.util.ee.deploy.api.deployable.WARDeployable;

/**
 * A {@code ContextCustomizer} is used to customize a Tomcat Context before its startup.
 * @author Guillaume Sauthier
 *
 */
public interface ContextCustomizer {

    /**
     * Customize the given Catalina Context instance using the given WAR deployable.
     * This is called just before the Context start-up.
     * @param context Catalina Context to be customized
     * @param deployable the WAR being deployed
     */
    void customize(final StandardContext context, final WARDeployable deployable);
}
