/**
 * JOnAS
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.web.tomcat7.osgi.configadmin;

import org.apache.felix.ipojo.annotations.Component;
import org.apache.felix.ipojo.annotations.Invalidate;
import org.apache.felix.ipojo.annotations.Property;
import org.apache.felix.ipojo.annotations.Requires;
import org.apache.felix.ipojo.annotations.Validate;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

@Component(name = "Tomcat7AjpConnector")
public class Tomcat7AjpConnector {

    /**
     * Logger
     */
    private static Log logger = LogFactory.getLog(Tomcat7AjpConnector.class);

    /**
     * Parameter : address
     */
    @Property(name = "address", mandatory = true)
    private String address;

    /**
     * Parameter : port
     */
    @Property(name = "port", mandatory = true)
    private int port;

    /**
     * Parameter : maxThreads
     */
    @Property(name = "maxThreads", mandatory = false)
    private String maxThreads;

    /**
     * Parameter : minSpareThreads
     */
    @Property(name = "minSpareThreads", mandatory = false)
    private String minSpareThreads;

    /**
     * Parameter : enableLookups
     */
    @Property(name = "enableLookups", mandatory = false)
    private boolean enableLookups;

    /**
     * Parameter : connectionTimeout
     */
    @Property(name = "connectionTimeout", mandatory = false)
    private String connectionTimeout;

    /**
     * Parameter : acceptCount
     */
    @Property(name = "acceptCount", mandatory = false)
    private String acceptCount;

    /**
     * Parameter : maxKeepAliveRequests
     */
    @Property(name = "maxKeepAliveRequests", mandatory = false)
    private String maxKeepAliveRequests;

    /**
     * Parameter : compression
     */
    @Property(name = "compression", mandatory = false)
    private String compression;

    /**
     * Parameter: redirectPort
     */
    @Property(name = "redirectPort", mandatory = false)
    private String redirectPort;

    /**
     * Service delegation
     */
    @Requires
    private Tomcat7Connector tomcat7ConnectorService;


    @Validate
    public void start() {
        try {

            boolean isAjp = true;
            boolean isSSL = false;

            tomcat7ConnectorService.addConnector(null, address, port, isAjp, isSSL, enableLookups);
            logger.info("Tomcat AJP connector {0}/{1} created - enableLookups={2}", address, String.valueOf(port), enableLookups);

            // set the maxThreads
            logger.debug("maxThreads={0}", maxThreads);
            if (maxThreads != null) {
                tomcat7ConnectorService.setConnectorAttribute(null, address, port, "maxThreads", maxThreads);
                logger.debug("Tomcat AJP connector {0}/{1} - attribute maxThreads updated -> {2}", address, String.valueOf(port),
                        maxThreads);
            }

            // set the minSpareThreads
            logger.debug("minSpareThreads={0}", minSpareThreads);
            if (minSpareThreads != null) {
                tomcat7ConnectorService.setConnectorAttribute(null, address, port, "minSpareThreads", minSpareThreads);
                logger.debug("Tomcat AJP connector {0}/{1} - attribute minSpareThreads updated -> {2}", address, String.valueOf(port),
                        minSpareThreads);
            }

            // set the connectionTimeout
            logger.debug("connectionTimeout={0}", connectionTimeout);
            if (connectionTimeout != null) {
                tomcat7ConnectorService.setConnectorAttribute(null, address, port, "connectionTimeout", connectionTimeout);
                logger.debug("Tomcat AJP connector {0}/{1} - attribute connectionTimeout updated -> {2}", address, String.valueOf(port),
                        connectionTimeout);
            }

            // set the acceptCount
            logger.debug("acceptCount={0}", acceptCount);
            if (acceptCount != null) {
                tomcat7ConnectorService.setConnectorAttribute(null, address, port, "acceptCount", acceptCount);
                logger.debug("Tomcat AJP connector {0}/{1} - attribute acceptCount updated -> {2}", address, String.valueOf(port),
                        acceptCount);
            }

            // set the maxKeepAliveRequests
            logger.debug("maxKeepAliveRequests={0}", maxKeepAliveRequests);
            if (maxKeepAliveRequests != null) {
                tomcat7ConnectorService.setConnectorAttribute(null, address, port, "maxKeepAliveRequests", maxKeepAliveRequests);
                logger.debug("Tomcat AJP connector {0}/{1} - attribute maxKeepAliveRequests updated -> {2}", address, String.valueOf(port),
                        maxKeepAliveRequests);
            }

            // set the compression
            logger.debug("compression={0}", compression);
            if (compression != null) {
                tomcat7ConnectorService.setConnectorAttribute(null, address, port, "compression", compression);
                logger.debug("Tomcat AJP connector {0}/{1} - attribute compression updated -> {2}", address, String.valueOf(port),
                        compression);
            }

            // set the redirect port
            logger.debug("redirectPort={0}", redirectPort);
            if (redirectPort != null) {
                tomcat7ConnectorService.setConnectorAttribute(null, address, port, "redirectPort", redirectPort);
                logger.debug("Tomcat AJP connector {0}/{1} - attribute redirectPort updated -> {2}", address, String.valueOf(port),
                        redirectPort);
            }

        } catch (Exception e) {
            logger.error("Error when creating the connector {0}/{1} - exception={2}", address, String.valueOf(port), e);
        }
    }

    @Invalidate
    public void stop() {

        try {

            tomcat7ConnectorService.destroyConnector(null, address, port);
            logger.debug("Tomcat AJP connector {0}/{1} destroyed", address, String.valueOf(port));

        } catch (Exception e) {
            logger.error("Error when creating the connector {0}/{1} - exception={2}", address, String.valueOf(port), e);

        }
    }
}
