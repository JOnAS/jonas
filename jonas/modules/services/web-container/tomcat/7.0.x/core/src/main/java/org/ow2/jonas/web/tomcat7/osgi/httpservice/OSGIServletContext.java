/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.web.tomcat7.osgi.httpservice;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.catalina.core.ApplicationContext;
import org.osgi.service.http.HttpContext;

/**
 * {@link javax.servlet.ServletContext} implementation for servlets or resources registered by
 * {@link HttpServiceImpl}.
 * @author Guillaume Porcher
 */
public class OSGIServletContext extends ApplicationContext {

    /**
     * The {@link HttpContext} used during servlet or resource registration.
     */
    private final HttpContext httpContext;

    /**
     * Default constructor.
     * @param context the {@link OSGIContext} holding the servlet.
     * @param httpContext the http context used during servlet registration.
     */
    public OSGIServletContext(final OSGIContext context, final HttpContext httpContext) {
        super(context);
        this.httpContext = httpContext;
    }

    /**
     * Returns the MIME type of the specified file, or null if the MIME type is
     * not known. The MIME type is determined by calling
     * {@link HttpContext#getMimeType(String)}. If this method returns
     * <code>null</code>, the {@link ApplicationContext#getMimeType(String)}
     * method is called.
     * @param file a String specifying the name of a file
     * @return a String specifying the file's MIME type
     */
    @Override
    public String getMimeType(final String file) {
        final String mime = this.httpContext.getMimeType(file);
        if (mime != null) {
            return mime;
        } else {
            return super.getMimeType(file);
        }
    }

    /**
     * Returns a URL to the resource that is mapped to a specified path. The
     * path must begin with a "/" and is interpreted as relative to the current
     * context root. This method uses {@link HttpContext#getResource(String)} to
     * find the resource.
     * @param path a String specifying the path to the resource
     * @return the resource located at the named path, or null if there is no
     *         resource at that path
     * @throws MalformedURLException if the pathname is not given in the correct
     *         form
     */
    @Override
    public URL getResource(final String path) throws MalformedURLException {
        return this.httpContext.getResource(path);
    }

    /**
     * Returns the resource located at the named path as an InputStream object.
     * The data in the InputStream can be of any type or length. This method
     * gets the resource URL by calling {@link #getResource(String)}.
     * @param path a String specifying the path to the resource
     * @return the InputStream returned to the servlet, or null if no resource
     *         exists at the specified path
     */
    @Override
    public InputStream getResourceAsStream(final String path) {
        URL url = null;
        try {
            url = this.getResource(path);
        } catch (final MalformedURLException e) {
            return null;
        }
        if (url != null) {
            try {
                return url.openStream();
            } catch (final IOException e) {
                return null;
            }
        }
        return null;
    }

}
