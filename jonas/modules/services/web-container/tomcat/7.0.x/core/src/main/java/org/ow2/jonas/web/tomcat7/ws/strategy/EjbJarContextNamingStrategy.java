/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.web.tomcat7.ws.strategy;

import java.io.File;

import org.ow2.jonas.ws.jaxws.ejb.context.IContextNamingStrategy;
import org.ow2.jonas.ws.jaxws.ejb.context.ContextNamingStrategyException;
import org.ow2.jonas.ws.jaxws.ejb.context.IContextNamingInfo;

/**
 * The EjbJarContextNamingStrategy computes a web context name based on the EjbJar filename.
 *
 * @author Guillaume Sauthier
 */
public class EjbJarContextNamingStrategy implements IContextNamingStrategy {
    /**
     * Creates a context name (web server root / context root).
     *
     * @param info used to determine the context name.
     * @return a context name
     */
    public String getContextName(final IContextNamingInfo info) throws ContextNamingStrategyException {
        return getCleanedName(info.getContainerName());
    }

    /**
     * Clean-up the name given in parameter.
     * @param name name to be cleaned up
     * @return cleaned name
     */
    private String getCleanedName(final String name) {

        // Strip off everything before the last '/' or '\'
        int pathSeparatorIndex = name.lastIndexOf(File.separatorChar);
        String cleanedName = name.substring(pathSeparatorIndex + 1, name.length());

        // Strip off everything after the first remaining '_'
        int underscoreIndex = cleanedName.indexOf('_');
        if (underscoreIndex != -1) {
            return cleanedName.substring(0, underscoreIndex);
        }

        // Strip off the extension
        int dotIndex = cleanedName.lastIndexOf('.');
        if (dotIndex != -1) {
            return cleanedName.substring(0, dotIndex);
        }

        // return as-is
        return cleanedName;
    }
}
