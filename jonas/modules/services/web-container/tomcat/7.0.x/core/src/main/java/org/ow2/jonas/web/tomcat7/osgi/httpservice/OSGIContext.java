/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009-2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.web.tomcat7.osgi.httpservice;

import org.ow2.jonas.web.tomcat7.JOnASStandardContext;

import javax.servlet.ServletContext;

import org.osgi.framework.Bundle;
import org.osgi.service.http.HttpContext;

/**
 * {@link Context} implementation for holding OSGI servlet wrappers. Only the
 * {@link #getServletContext()} method is overridden.
 * @author Guillaume Porcher
 */
public class OSGIContext extends JOnASStandardContext {

    /**
     * Serial version UID.
     */
    private static final long serialVersionUID = -8093637123939061548L;

    /**
     * The {@link HttpContext} used during servlet or resource registration.
     */
    private final HttpContext httpContext;

    /**
     * Default constructor.
     * @param context The HttpContext used during servlet or resource
     *        registration.
     * @param callerBundle The bundle that uses the HttpService.
     */
    public OSGIContext(final HttpContext context) {
        this.httpContext = context;
    }

    /**
     * Return the servlet context for which this Context is a facade. The
     * returned context is an instance of {@link OSGIServletContext}.
     * @return the servlet context for which this Context is a facade.
     */
    @Override
    public ServletContext getServletContext() {
        if (context == null) {
            context = new OSGIServletContext(this, this.httpContext);
        }
        return context;
    }

    /**
     * @return The httpContext used during servlet or resource registration.
     */
    public HttpContext getHttpContext() {
        return this.httpContext;
    }



}
