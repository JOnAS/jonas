/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.web.tomcat7.handler;

import java.net.URL;
import java.net.URLConnection;
import java.net.InetAddress;
import java.io.IOException;

import org.apache.naming.resources.DirContextURLStreamHandler;
import org.osgi.service.url.URLStreamHandlerService;
import org.osgi.service.url.URLStreamHandlerSetter;

/**
 * The DirContextURLStreamHandlerService is a wrapper around the Tomcat's
 * DirContextURLStreamHandler that is responsible of loading URL of the
 * scheme 'jndi' (jndi://...).
 *
 * This class is exposed as a URLStreamHandlerService (OSGi URL service interface).
 *
 * @author Guillaume Sauthier
 */
public class DirContextURLStreamHandlerService extends DirContextURLStreamHandler
                                               implements URLStreamHandlerService {

    /**
     * The <code>URLStreamHandlerSetter</code> object passed to the parseURL
     * method.
     */
    protected volatile URLStreamHandlerSetter realHandler;

    public URLConnection openConnection(final URL url) throws IOException {
        return super.openConnection(url);
    }

    public void parseURL(final URLStreamHandlerSetter realHandler,
                         final URL url,
                         final String spec,
                         final int start,
                         final int limit) {
        this.realHandler = realHandler;
        parseURL(url, spec, start, limit);
    }

    public String toExternalForm(final URL url) {
        return super.toExternalForm(url);
    }

    public boolean equals(final URL url, final URL url1) {
        return super.equals(url, url1);
    }

    public int getDefaultPort() {
        return super.getDefaultPort();
    }

    public InetAddress getHostAddress(final URL url) {
        return super.getHostAddress(url);
    }

    public int hashCode(final URL url) {
        return super.hashCode(url);
    }

    public boolean hostsEqual(final URL url, final URL url1) {
        return super.hostsEqual(url, url1);
    }

    public boolean sameFile(final URL url, final URL url1) {
        return super.sameFile(url, url1);
    }


	/**
	 * This method calls
	 * <code>realHandler.setURL(URL,String,String,int,String,String)</code>.
	 *
	 * @see "java.net.URLStreamHandler.setURL(URL,String,String,int,String,String)"
	 * @deprecated This method is only for compatibility with handlers written
	 *             for JDK 1.1.
	 */
	protected void setURL(final URL u,
                          final String proto,
                          final String host,
                          final int port,
			final String file, final String ref) {
		realHandler.setURL(u, proto, host, port, file, ref);
	}

	/**
	 * This method calls
	 * <code>realHandler.setURL(URL,String,String,int,String,String,String,String)</code>.
	 *
	 * @see "java.net.URLStreamHandler.setURL(URL,String,String,int,String,String,String,String)"
	 */
	protected void setURL(final URL u,
                          final String proto,
                          final String host,
                          final int port,
                          final String auth,
                          final String user,
                          final String path,
                          final String query,
                          final String ref) {
		realHandler.setURL(u, proto, host, port, auth, user, path, query, ref);
	}
}
