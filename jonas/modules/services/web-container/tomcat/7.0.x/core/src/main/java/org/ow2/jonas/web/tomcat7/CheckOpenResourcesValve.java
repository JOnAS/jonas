/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 France Telecom R&D
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.web.tomcat7;

import java.io.IOException;

import javax.servlet.ServletException;

import org.apache.catalina.connector.Request;
import org.apache.catalina.connector.Response;
import org.apache.catalina.valves.ValveBase;
import org.ow2.jonas.jndi.checker.api.IResourceCheckerInfo;
import org.ow2.jonas.jndi.checker.api.IResourceCheckerManager;
import org.ow2.jonas.jndi.checker.api.ResourceCheckpoints;

/**
 * Valve used to check if resources open in an http method are all closed at the end of the method call.
 * @author Florent Benoit
 */
public class CheckOpenResourcesValve extends ValveBase {

    /**
     * Link to the resource checker manager.
     */
    private IResourceCheckerManager resourceCheckerManager = null;

    /**
     * Default constructor with a given resource checker manager.
     * @param resourceCheckerManager the given instance
     */
    public CheckOpenResourcesValve(final IResourceCheckerManager resourceCheckerManager) {
        super(true);
        this.resourceCheckerManager = resourceCheckerManager;
    }

    /**
     * Remove the current authenticated user by setting the anonymous user.
     * @param request The servlet request to be processed
     * @param response The servlet response to be created
     * @exception java.io.IOException if an input/output error occurs
     * @exception javax.servlet.ServletException if a servlet error occurs
     */
    @Override
    public void invoke(final Request request, final Response response) throws IOException, ServletException {
        try {
            // call the next valve
            getNext().invoke(request, response);
        } finally {
            // Check if resources are closed
            resourceCheckerManager.detect(new ValveResourceCheckerInfo(request));
            resourceCheckerManager.delistAll();
        }
    }

}

/**
 * Checker info for valves.
 * @author Florent Benoit
 */
class ValveResourceCheckerInfo implements IResourceCheckerInfo {

    /**
     * Http request.
     */
    private Request request = null;

    /**
     * Build a checker info object based on the given request.
     * @param request the given request
     */
    public ValveResourceCheckerInfo(final Request request) {
        this.request = request;
    }

    /**
     * @return checkpoint of the caller.
     */
    public ResourceCheckpoints getCheckPoint() {
            return ResourceCheckpoints.HTTP_AFTER_CALL;
        }

    /**
     * @return data for identifying the current caller (EJB Name, Servlet Name, etc)
     */
    public String getCallerInfo() {
           return request.getRequestURL().toString();
    }
}
