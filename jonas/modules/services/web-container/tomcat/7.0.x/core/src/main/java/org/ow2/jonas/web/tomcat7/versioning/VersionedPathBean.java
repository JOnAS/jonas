/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008-2010 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * Application Versioning System
 * Copyright (C) 2008 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.web.tomcat7.versioning;

/**
 * Information about a versioned path.
 * @author Frederic Germaneau
 * @author S. Ali Tokmen
 */
public class VersionedPathBean {

    /**
     * User path.
     */
    private String userPath;

    /**
     * Versioned path.
     */
    private String versionedPath;

    /**
     * Original decoded URI of the request.
     */
    private String originalDecodedUri;

    /**
     * Sets all members to null.
     */
    public VersionedPathBean() {
        this.userPath = null;
        this.versionedPath = null;
    }

    /**
     * Initializes all members.
     * @param userPath User path.
     * @param versionedPath Versioned path.
     */
    public VersionedPathBean(final String userPath, final String versionedPath) {
        this.userPath = userPath;
        this.versionedPath = versionedPath;
    }

    /**
     * @return User path.
     */
    public String getUserPath() {
        return userPath;
    }

    /**
     * @param userPath User path to set.
     */
    public void setUserPath(final String userPath) {
        this.userPath = userPath;
    }

    /**
     * @return Versioned path.
     */
    public String getVersionedPath() {
        return versionedPath;
    }

    /**
     * @param versionedPath Versioned path to set. This doesn't set
     *        userPathEndWithSlash.
     */
    public void setVersionedPath(final String versionedPath) {
        this.versionedPath = versionedPath;
    }

    /**
     * @return Original decoded URI of the request.
     */
    public String getOriginalDecodedUri() {
        return originalDecodedUri;
    }

    /**
     * @param originalDecodedUri Original decoded URI of the request.
     */
    public void setOriginalDecodedUri(final String originalDecodedUri) {
        this.originalDecodedUri = originalDecodedUri;
    }

}
