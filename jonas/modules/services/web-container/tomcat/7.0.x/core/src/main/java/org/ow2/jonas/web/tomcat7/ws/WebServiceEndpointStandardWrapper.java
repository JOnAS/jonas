/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.web.tomcat7.ws;

import org.apache.catalina.core.StandardWrapper;
import org.ow2.jonas.ws.jaxws.IWebServiceEndpoint;
import org.ow2.jonas.ws.jaxws.http.servlet.JAXWSServlet;

import javax.servlet.Servlet;
import javax.servlet.ServletException;


public class WebServiceEndpointStandardWrapper extends StandardWrapper {

    /**
     * Default value for the <code>servlet-name</code> element.
     */
    public static final String JAX_WS_GENERIC_SERVLET = "JAX-WS 2.x Generic Servlet";

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = -8186120043063306566L;

    /**
     * Endpoint to be injected in the servlet instance.
     */
    private IWebServiceEndpoint endpoint;

    /**
     * Creates a customized Tomcat Wrapper
     * @param endpoint web service endpoint
     * @param servletName The name of this servlet (not the class name !)
     */
    public WebServiceEndpointStandardWrapper(final IWebServiceEndpoint endpoint,
                                             final String servletName) {
        this.endpoint = endpoint;

        // Hardcoded JAXWSServlet name ?
        setServletClass(JAXWSServlet.class.getName());
        setServletName(servletName);
    }

    /**
     * Creates a customized Tomcat Wrapper
     * @param endpoint web service endpoint
     */
    public WebServiceEndpointStandardWrapper(final IWebServiceEndpoint endpoint) {
        this(endpoint, JAX_WS_GENERIC_SERVLET);
    }

    /* (non-Javadoc)
     * @see org.apache.catalina.core.StandardWrapper#loadServlet()
     */
    @Override
    public synchronized Servlet loadServlet() throws ServletException {

        // Delegate Servlet instanciation
        JAXWSServlet servlet = (JAXWSServlet) super.loadServlet();

        // Inject the Endpoint
        servlet.setWebServiceEndpoint(endpoint);

        return servlet;
    }

}
