/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.web.tomcat7.ws.strategy;

import org.ow2.jonas.ws.jaxws.ejb.context.IContextNamingStrategy;
import org.ow2.jonas.ws.jaxws.ejb.context.ContextNamingStrategyException;
import org.ow2.jonas.ws.jaxws.ejb.context.IContextNamingInfo;

/**
 * The FixedContextNamingStrategy simply return the web context name
 * that has been assigned to this strategy.
 *
 * @author Guillaume Sauthier
 */
public class FixedContextNamingStrategy implements IContextNamingStrategy {

    /**
     * Fixed context name.
     */
    private String contextName;

    public FixedContextNamingStrategy() {
    }

    public FixedContextNamingStrategy(final String contextName) {
        setContextName(contextName);
    }

    public void setContextName(final String contextName) {
        this.contextName = contextName;
    }

    /**
     * Creates a context name (web server root / context root).
     *
     * @param info used to determine the context name.
     * @return a context name
     */
    public String getContextName(final IContextNamingInfo info) throws ContextNamingStrategyException {
        if (contextName == null) {
            throw new ContextNamingStrategyException("Missing contextName value.");
        }
        return contextName;
    }
}
