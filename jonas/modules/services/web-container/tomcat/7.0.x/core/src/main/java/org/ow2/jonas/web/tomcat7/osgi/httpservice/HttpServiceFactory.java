/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.web.tomcat7.osgi.httpservice;

import org.osgi.framework.Bundle;
import org.ow2.jonas.web.base.osgi.httpservice.JOnASHttpService;
import org.ow2.jonas.web.tomcat7.Tomcat7Service;

/**
 * Service factory for Tomcat7 service.
 * @author Florent Benoit
 */
public class HttpServiceFactory extends org.ow2.jonas.web.base.osgi.httpservice.HttpServiceFactory<Tomcat7Service> {

    /**
     * Constructor.
     * @param tomcat7Service the web container service to use.
     */
    public HttpServiceFactory(final Tomcat7Service tomcat7Service) {
        super(tomcat7Service);
    }

    /**
     * Build an implementation of the Http service.
     * @param callerBundle The bundle using the service.
     * @param tomcat7Service the web container service instance
     * @return an implementation of the httpService
     */
    @Override
    protected JOnASHttpService buildHttpServiceImpl(final Bundle callerBundle, final Tomcat7Service tomcat7Service) {
        return new HttpServiceImpl(callerBundle, tomcat7Service, getWorkDirectory());
    }

}
