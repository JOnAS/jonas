/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006-2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.antmodular.jonasbase.web.tomcat7;

import org.ow2.jonas.antmodular.jonasbase.web.tomcat.Tomcat;
import org.ow2.jonas.antmodular.jonasbase.web.tomcat.TomcatSessionManager;
import org.ow2.jonas.antmodular.web.base.Cluster;
import org.ow2.jonas.antmodular.web.base.SessionReplication;

/**
 * Support Tomcat 7 Connector Configuration.
 * Prefered order : http, director, ajp, https, cluster.
 *
 * @author Jeremy Cazaux
 */
public class Tomcat7 extends Tomcat {

    /**
     * Name of the implementation class for Tomcat.
     */
    private static final String TOMCAT_SERVICE = "org.ow2.jonas.web.tomcat7.Tomcat7Service";

    /**
     * Name of the tomcat server file
     */
    public static final String TOMCAT_SERVER_CONF_FILE = "tomcat7-server.xml";

    /**
     * Name of the tomcat context file
     */
    public static final String TOMCAT_CONTEXT_CONF_FILE = "tomcat7-context.xml";

    /**
     * Default Constructor.
     */
    public Tomcat7() {
        super();
    }

    /**
     * Configure a Cluster HTTP Session.
     * @param cluster Cluster HTTP Session.
     */
    @Override
    @Deprecated
    public void addConfiguredCluster(final Cluster cluster) {
        addConfiguredSessionReplication(cluster);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addConfiguredSessionReplication(final SessionReplication sessionReplication) {
        super.addConfiguredSessionReplication(sessionReplication, TOMCAT_SERVER_CONF_FILE);
    }

   /**
     * Configure Tomcat7's connectors
     * @param tomcat7Connectors
     */
    public void addConfiguredConnectors(final Tomcat7Connectors tomcat7Connectors) {
        this.tasks.add(tomcat7Connectors);
    }

    /**
     * Configure a jvmRoute.
     * @param jvmRoute jvm route name
     */
    @Override
    public void setJvmRoute(final String jvmRoute) {
        super.setJvmRoute(jvmRoute, TOMCAT_SERVER_CONF_FILE);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addConfiguredSessionManager(final TomcatSessionManager tomcatSessionManager) {
        super.addConfiguredSessionManager(tomcatSessionManager, TOMCAT_CONTEXT_CONF_FILE);
    }

    /**
     * Set the port number for the WebContainer
     * @param portNumber the port number
     */
    @Override
    public void setPort(String portNumber) {
        super.setPort(portNumber, TOMCAT_SERVER_CONF_FILE, "tomcat");
    }

    /**
     * Execute all tasks
     */
    @Override
    public void execute() {
        super.execute();
        super.createServiceNameReplace(this.TOMCAT_SERVICE, this.INFO, this.destDir.getAbsolutePath() + this.CONF_DIR);
    }
}
