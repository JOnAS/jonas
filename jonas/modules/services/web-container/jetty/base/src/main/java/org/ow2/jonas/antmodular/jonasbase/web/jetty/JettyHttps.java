/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006-2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.antmodular.jonasbase.web.jetty;

import org.ow2.jonas.antmodular.web.base.Https;

/**
 * Created by IntelliJ IDEA.
 * User: cazaux-j
 * Date: 09/06/11
 * Time: 14:04
 * To change this template use File | Settings | File Templates.
 */
public class JettyHttps extends Https{

    /**
     * Sun VM name.
     */
    public static final String SUN_VM = "sun";

    /**
     * IBM VM name.
     */
    public static final String IBM_VM = "ibm";

    /**
     * Default VM name.
     */
    private static final String DEFAULT_VM = SUN_VM;

    /**
     * Targeted VM (Only used for Jetty HTTPS Listener).
     */
    private String vm = DEFAULT_VM;

    /**
     * key password (only for Jetty).
     */
    private String keyPassword = null;

        /**
     * @return Returns the vm.
     */
    public String getVm() {
        return vm;
    }

    /**
     * @param vm The vm to set.
     */
    public void setVm(final String vm) {
        this.vm = vm;
    }

    /**
     * @return Returns the keyPassword.
     */
    public String getKeyPassword() {
        return keyPassword;
    }

    /**
     * @param keyPassword The keyPassword to set.
     */
    public void setKeyPassword(final String keyPassword) {
        this.keyPassword = keyPassword;
    }
}
