/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006-2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.antmodular.jonasbase.web.jetty;

import org.ow2.jonas.antmodular.web.base.WebContainer;

/**
 * Support Jetty Connectors Configuration.
 *
 * @author Guillaume Sauthier
 */
public abstract class Jetty extends WebContainer {

    /**
     * Info for the logger.
     */
    public static final String INFO = "[Jetty] ";

    /**
     * HTTPS TOKEN.
     */
    public static final String HTTPS_TOKEN = "<!-- Add a HTTPS SSL listener on port 9043";

    /**
     * AJP TOKEN.
     */
    public static final String AJP_TOKEN = "<!-- Add a AJP13 listener on port 8009";

    /**
     * Default Constructor.
     */
    public Jetty() {
        super();
    }

}
