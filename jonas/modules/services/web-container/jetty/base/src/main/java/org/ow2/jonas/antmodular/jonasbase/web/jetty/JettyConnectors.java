/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006-2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.antmodular.jonasbase.web.jetty;

import org.ow2.jonas.antmodular.jonasbase.bootstrap.JReplace;
import org.ow2.jonas.antmodular.web.base.Ajp;
import org.ow2.jonas.antmodular.web.base.Connectors;
import org.ow2.jonas.antmodular.web.base.Https;

/**
 * Configure Jetty connector
 * @author Jeremy Cazaux
 */
public abstract class JettyConnectors extends Connectors {

    /**
     * Configure a JettyHTTPS Connectors.
     * @param jettyHttps JettyHTTPS Configuration.
     */
    public void addConfiguredHttps(final JettyHttps jettyHttps, final String confFile) {
        JReplace propertyReplace = new JReplace();
        propertyReplace.setConfigurationFile(confFile); //to replace
        propertyReplace.setToken(Jetty.HTTPS_TOKEN);
        StringBuffer value = new StringBuffer();
        value.append("<!-- Add a HTTPS SSL listener on port " + jettyHttps.getPort() + "                           -->\n");
        value.append("  <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->\n");
        value.append("  <Call name=\"addListener\">\n");
        value.append("    <Arg>\n");
        if (JettyHttps.IBM_VM.equalsIgnoreCase(jettyHttps.getVm())) {
            value.append("      <New class=\"org.mortbay.http.IbmJsseListener\">\n");
        } else {
                // default to Sun
            value.append("      <New class=\"org.mortbay.http.SunJsseListener\">\n");
        }
        value.append("        <Set name=\"Port\">" + jettyHttps.getPort() + "</Set>\n");
        value.append("        <Set name=\"MinThreads\">5</Set>\n");
        value.append("        <Set name=\"MaxThreads\">100</Set>\n");
        value.append("        <Set name=\"MaxIdleTimeMs\">30000</Set>\n");
        value.append("        <Set name=\"LowResourcePersistTimeMs\">2000</Set>\n");
        if (jettyHttps.getKeystoreFile() != null) {
            value.append("        <Set name=\"Keystore\">" + jettyHttps.getKeystoreFile() + "</Set>\n");
        }
        if (jettyHttps.getKeystorePass() != null) {
            value.append("        <Set name=\"Password\">" + jettyHttps.getKeystorePass() + "</Set>\n");
        }
        if (jettyHttps.getKeyPassword() != null) {
            value.append("  <Set name=\"KeyPassword\">" + jettyHttps.getKeyPassword() + "</Set>\n");
        }
        value.append("      </New>\n");
        value.append("    </Arg>\n");
        value.append("  </Call>\n\n");
        value.append("  <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->\n");
        value.append("  " + Jetty.HTTPS_TOKEN);
        propertyReplace.setValue(value.toString());
        propertyReplace.setLogInfo(Jetty.INFO + "Setting HTTPS port number to : " + jettyHttps.getPort());
        addTask(propertyReplace);

        propertyReplace = new JReplace();
        propertyReplace.setConfigurationFile(confFile);
        propertyReplace.setToken(Https.DEFAULT_PORT);
        propertyReplace.setValue(jettyHttps.getPort());
        propertyReplace.setLogInfo(Jetty.INFO + "Fix HTTP redirect port number to : " + jettyHttps.getPort());
        addTask(propertyReplace);
    }

    /**
     * Configure an AJP Connectors.
     * @param ajp AJP Configuration.
     */
    public void addConfiguredAjp(final Ajp ajp, final String confFile) {
        JReplace propertyReplace = new JReplace();
        propertyReplace.setConfigurationFile(confFile);
        propertyReplace.setToken(Jetty.AJP_TOKEN);
        StringBuffer value = new StringBuffer();
        value.append("<!-- Add a AJP13 listener on port " + ajp.getPort() + "                               -->\n");
        value.append("  <!-- This protocol can be used with mod_jk in apache, IIS etc.       -->\n");
        value.append("  <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->\n");
        value.append("  <Call name=\"addListener\">\n");
        value.append("    <Arg>\n");
        value.append("      <New class=\"org.mortbay.http.ajp.AJP13Listener\">\n");
        value.append("        <Set name=\"Port\">" + ajp.getPort() + "</Set>\n");
        value.append("        <Set name=\"MinThreads\">5</Set>\n");
        value.append("        <Set name=\"MaxThreads\">20</Set>\n");
        value.append("        <Set name=\"MaxIdleTimeMs\">0</Set>\n");
        value.append("        <Set name=\"confidentialPort\">9043</Set>\n");
        value.append("      </New>\n");
        value.append("    </Arg>\n");
        value.append("  </Call>\n\n");
        value.append("  <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->\n");
        value.append("  " + Jetty.AJP_TOKEN);
        propertyReplace.setValue(value.toString());
        propertyReplace.setLogInfo(Jetty.INFO + "Setting AJP Connectors to : " + ajp.getPort());
        addTask(propertyReplace);
    }

    /**
     * Configure a Jetty HTTPS Connectors.
     * @param jettyHttps JettyHTTPS Configuration.
     */
    public abstract void addConfiguredHttps(final JettyHttps jettyHttps);

    /**
     * Configure an AJP Connectors.
     * @param ajp AJP Configuration.
     */
    public abstract void addConfiguredAjp(final Ajp ajp);

}
