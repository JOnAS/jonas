/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.web.jetty8;

import java.lang.reflect.Field;
import java.net.URL;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.jasper.EmbeddedServletOptions;
import org.apache.jasper.runtime.TldScanner;
import org.apache.jasper.servlet.JspServlet;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.component.AbstractLifeCycle;
import org.eclipse.jetty.util.component.LifeCycle;

/**
 * Lifecycle listener that will replace the current TldScanner of the JSP servlet by a JOnAS TLD Scanner.
 * @author Florent Benoit
 */
public class JSPLifecycleListener extends AbstractLifeCycle.AbstractLifeCycleListener implements LifeCycle.Listener{

    /**
     * List of resources to analyze.
     */
    private List<URL> resourcesToMonitor = null;

    /**
     * Default constructor.
     * @param resourcesToMonitor the TLDs resources that needs to be scanned
     */
    public JSPLifecycleListener(final List<URL> resourcesToMonitor) {
        this.resourcesToMonitor = resourcesToMonitor;
    }

    /**
     * Replace TLD Scanner by JOnAS TLD scanner.
     * @param lifecycle the servlet holder
     */
    @Override
    public void lifeCycleStarted(final LifeCycle lifecycle) {
        // Lifecycle should be a servletHolder
        if (lifecycle instanceof ServletHolder) {
            ServletHolder servletHolder = (ServletHolder) lifecycle;
            Servlet servlet;
            try {
                servlet = servletHolder.getServlet();
            } catch (ServletException e) {
                // Unable to get the servlet
                return;
            }
            // Should be a JSP Servlet
            if (servlet instanceof JspServlet) {
                JspServlet jspServlet = (JspServlet) servlet;
                Field optionsField = null;
                try {
                    optionsField = JspServlet.class.getDeclaredField("options");
                } catch (SecurityException e) {
                    return;
                } catch (NoSuchFieldException e) {
                    return;
                }

                optionsField.setAccessible(true);
                Object options = null;
                try {
                    options = optionsField.get(jspServlet);
                } catch (IllegalArgumentException e) {
                    return;
                } catch (IllegalAccessException e) {
                    return;
                }

                if (options instanceof EmbeddedServletOptions) {
                    EmbeddedServletOptions embeddedServletOptions = (EmbeddedServletOptions) options;
                    TldScanner tldScanner = embeddedServletOptions.getTldScanner();

                    Field tldScannerField;
                    try {
                        tldScannerField = EmbeddedServletOptions.class.getDeclaredField("tldScanner");
                    } catch (SecurityException e) {
                        return;
                    } catch (NoSuchFieldException e) {
                        return;
                    }
                    tldScannerField.setAccessible(true);

                    // Replace by our own TldScanner
                    JOnASTldScanner jOnASTldScanner = new JOnASTldScanner(tldScanner, resourcesToMonitor);
                    try {
                        tldScannerField.set(embeddedServletOptions, jOnASTldScanner);
                    } catch (IllegalArgumentException e) {
                        return;
                    } catch (IllegalAccessException e) {
                        return;
                    }
                }


            }
        }

    }

}
