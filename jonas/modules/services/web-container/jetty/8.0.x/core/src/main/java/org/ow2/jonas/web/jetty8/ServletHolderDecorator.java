/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.web.jetty8;

import java.net.URL;
import java.util.EventListener;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.jasper.servlet.JspServlet;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.servlet.ServletContextHandler.Decorator;

/**
 * Allows to add listener for a given servlet holder.
 * @author Florent Benoit
 *
 */
public class ServletHolderDecorator implements Decorator {

    /**
     * List of resources to analyze.
     */
    private List<URL> resourcesToMonitor = null;

    /**
     * Default constructor.
     * @param resourcesToMonitor the TLDs resources that needs to be scanned
     */
    public ServletHolderDecorator(final List<URL> resourcesToMonitor) {
        this.resourcesToMonitor = resourcesToMonitor;
    }



    @Override
    public void decorateFilterHolder(final FilterHolder arg0) throws ServletException {

    }

    @Override
    public <T extends Filter> T decorateFilterInstance(final T arg0) throws ServletException {
        return arg0;
    }

    @Override
    public <T extends EventListener> T decorateListenerInstance(final T arg0) throws ServletException {
        return arg0;
    }

    /**
     * Adds a new lifecycle listener for the JSP servlet.
     * @param servletHolder the given servlet holder of the servlet
     */
    @Override
    public void decorateServletHolder(final ServletHolder servletHolder) throws ServletException {
        if (JspServlet.class.getName().equals(servletHolder.getClassName())) {
            servletHolder.addLifeCycleListener(new JSPLifecycleListener(resourcesToMonitor));
        }

    }

    @Override
    public <T extends Servlet> T decorateServletInstance(final T servlet) throws ServletException {
        return servlet;
    }

    @Override
    public void destroyFilterInstance(final Filter arg0) {
    }

    @Override
    public void destroyListenerInstance(final EventListener arg0) {

    }

    @Override
    public void destroyServletInstance(final Servlet arg0) {

    }

}
