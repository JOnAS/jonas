/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.web.jetty8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jetty.webapp.Configuration;
import org.eclipse.jetty.webapp.WebAppContext;

/**
 * Own JOnAS webAppcontext allowing to add new Jetty configurations.
 * @author Florent Benoit
 */
public class JOnASWebAppContext extends WebAppContext {

    /**
     * Configurations to load after the default one.
     */
    private List<Configuration> jonasConfigurations = null;

    /**
     * Configuration has been done.
     */
    private boolean configurationDone = false;

    /**
     * This constructor allows to specify our own configuration objects.
     */
    public JOnASWebAppContext() {
        super();
        this.jonasConfigurations = new ArrayList<Configuration>();
    }

    /**
     * Override the super method by adding our own configurations.
     * @throws Exception if it fails
     */
    @Override
    protected void loadConfigurations() throws Exception {
        if (configurationDone) {
            return;
        }
        super.loadConfigurations();

        Configuration[] configurations = getConfigurations();
        List<Configuration> configurationList = new ArrayList<Configuration>();
        if (configurations != null) {
            configurationList.addAll(Arrays.asList(configurations));
        }
        configurationList.addAll(jonasConfigurations);

        setConfigurations(configurationList.toArray(new Configuration[configurationList.size()]));
        configurationDone = true;

    }

    /**
     * Adds a given configuration.
     * @param configuration the given configuration
     */
    public void addConfiguration(final Configuration configuration) {
        jonasConfigurations.add(configuration);
    }

}
