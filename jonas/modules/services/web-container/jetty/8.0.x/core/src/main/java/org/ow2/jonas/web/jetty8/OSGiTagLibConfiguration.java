/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.web.jetty8;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.jetty.util.resource.Resource;
import org.eclipse.jetty.webapp.TagLibConfiguration;
import org.eclipse.jetty.webapp.WebAppContext;

/**
 * This Configuration enhances the default taglib configuration by using the dynamically TLD that have been registered.
 * @author Florent Benoit
 */
public class OSGiTagLibConfiguration extends TagLibConfiguration {

    /**
     * List of resources to analyze.
     */
    private List<URL> resourcesToMonitor = null;

    /**
     * Default constructor.
     * @param resourcesToMonitor the TLDs resources that needs to be scanned
     */
    public OSGiTagLibConfiguration(final List<URL> resourcesToMonitor) {
        this.resourcesToMonitor = resourcesToMonitor;
    }


    /**
     * Adds our own TLD resources that have been found.
     * @param context the given webApp context
     * @throws Exception if resources can't be added
     */
    @Override
    @SuppressWarnings("unchecked")
    public void preConfigure(final WebAppContext context) throws Exception {
        if (resourcesToMonitor != null && !resourcesToMonitor.isEmpty()) {
            List<Resource> resources = new ArrayList<Resource>();
            boolean useCache = Resource.getDefaultUseCaches();
            Resource.setDefaultUseCaches(false);
            try {
                for (URL url : resourcesToMonitor) {
                    resources.add(Resource.newResource(url));
                }
            } finally {
                Resource.setDefaultUseCaches(useCache);
            }
            Collection<Resource> previouslySet = (Collection<Resource>) context.getAttribute(TagLibConfiguration.TLD_RESOURCES);
            if (previouslySet != null) {
                resources.addAll(previouslySet);
            }
            context.setAttribute(TagLibConfiguration.TLD_RESOURCES, resources);
        }
        super.preConfigure(context);

    }
}
