/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006-2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.antmodular.jonasbase.web.jetty8;

import org.ow2.jonas.antmodular.jonasbase.web.jetty.Jetty;

/**
 * Support Jetty 8 Connector Configuration.
 * @author Jeremy Cazaux
 */
public class Jetty8 extends Jetty {

    /**
     * Name of Jetty configuration file.
     */
    public static final String JETTY_CONF_FILE = "jetty8.xml";

    /**
     * Name of the implementation class for Jetty.
     */
    private static final String JETTY_SERVICE = "org.ow2.jonas.web.jetty8.Jetty8Service";

    /**
     * Default Constructor.
     */
    public Jetty8() {
        super();
    }

    /**
     * Configure Jetty8's connectors
     * @param jetty8Connectors Jetty 8 Connectors
     */
    public void addConfiguredConnectors(final Jetty8Connectors jetty8Connectors) {
        this.tasks.add(jetty8Connectors);
    }

    /**
     * Set the port number for the WebContainer
     * @param portNumber the port number
     */
    @Override
    public void setPort(String portNumber) {
        super.setPort(portNumber,JETTY_CONF_FILE,"jetty");
    }

    /**
     * Execute this task
     */
    @Override
    public void execute() {
        super.execute();
        super.createServiceNameReplace(this.JETTY_SERVICE, this.INFO, this.destDir.getAbsolutePath() + this.CONF_DIR);
    }

}
