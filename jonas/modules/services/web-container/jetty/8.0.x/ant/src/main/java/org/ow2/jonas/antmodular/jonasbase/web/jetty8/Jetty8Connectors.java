/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006-2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.antmodular.jonasbase.web.jetty8;

import org.ow2.jonas.antmodular.jonasbase.web.jetty.JettyConnectors;
import org.ow2.jonas.antmodular.jonasbase.web.jetty.JettyHttps;
import org.ow2.jonas.antmodular.web.base.Ajp;
import org.ow2.jonas.antmodular.web.base.Http;

/**
 * Configure Jetty 8 connector
 * @author Jeremy Cazaux
 */
public class Jetty8Connectors extends JettyConnectors {

    /**
     * Default constructor
     */
    public Jetty8Connectors() {
        super();
    }

    /**
     * Configure a HTTP Connector.
     * @param http HTTP Configuration.
     */
    public void addConfiguredHttp(Http http) {
        super.addConfiguredHttp(http, Jetty8.JETTY_CONF_FILE, Jetty8.INFO);
    }

    /**
     * Configure a JettyHTTPS Connector.
     * @param jettyHttps JettyHTTPS Configuration.
     */
    @Override
    public void addConfiguredHttps(final JettyHttps jettyHttps) {
        super.addConfiguredHttps(jettyHttps, Jetty8.JETTY_CONF_FILE);
    }

    /**
     * Configure an AJP Connector.
     * @param ajp AJP Configuration.
     */
    @Override
    public void addConfiguredAjp(final Ajp ajp) {
        super.addConfiguredAjp(ajp, Jetty8.JETTY_CONF_FILE);
    }

    @Override
    public void execute() {
        super.execute();
        super.executeAllTask();
    }
}
