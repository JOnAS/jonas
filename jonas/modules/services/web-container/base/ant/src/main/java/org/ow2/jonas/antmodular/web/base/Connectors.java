/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006-2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.antmodular.web.base;

import org.ow2.jonas.antmodular.jonasbase.bootstrap.AbstractJOnASBaseAntTask;
import org.ow2.jonas.antmodular.jonasbase.bootstrap.JReplace;
import org.ow2.jonas.antmodular.jonasbase.bootstrap.Tasks;

/**
 * Configure a Connectors
 * @author Jeremy Cazaux
 */
public abstract class Connectors extends AbstractJOnASBaseAntTask {

    protected void addConfiguredHttp(final Http http, final String confFile, final String info) {
        JReplace propertyReplace = new JReplace();
        propertyReplace.setConfigurationFile(confFile);
        propertyReplace.setToken(Http.DEFAULT_PORT);
        propertyReplace.setValue(http.getPort());
        propertyReplace.setLogInfo(info + "Setting HTTP port number to : " + http.getPort());
        addTask(propertyReplace);
    }

   // public abstract void addConfiguredHttp(final Http http);
}
