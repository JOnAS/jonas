/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006-2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.antmodular.web.base;

/**
 * Support for HTTP Session Clustering. Tomcat only.
 *
 * @author Guillaume Sauthier
 */
public class SessionReplication  {

    /**
     * Default MultiCast Host for HTTP Session Clustering.
     */
    public static final String DEFAULT_MCAST_ADDR = "228.0.0.4";

    /**
     * Default MultiCast Port for HTTP Session Clustering.
     */
    public static final String DEFAULT_MCAST_PORT = "45564";

    /**
     * Default listening port for HTTP Session Clustering.
     */
    public static final String DEFAULT_LISTEN_PORT = "4001";

    /**
     * Default cluster name.
     */
    public static final String DEFAULT_CLUSTER_NAME = "myTomcatCluster";

    /**
     * User specified Cluster listening port.
     */
    private String listenPort = DEFAULT_LISTEN_PORT;

    /**
     * User specified MultiCast port.
     */
    private String mcastPort = DEFAULT_MCAST_PORT;

    /**
     * User specified MultiCast address.
     */
    private String mcastAddr = DEFAULT_MCAST_ADDR;

    /**
     * Cluster name.
     */
    private String name = DEFAULT_CLUSTER_NAME;

    /**
     * Clustering algo (delta or backup)
     */
    private SessionReplicationAlgorithm algorithm;

    /**
     * Default constructor
     */
    public SessionReplication() {
    }

    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The cluster name.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @param algorithm The algorithm of the session replication
     */
    public void setAlgorithm(final String algorithm) {
        if ("backup".equals(algorithm)) {
            this.algorithm = SessionReplicationAlgorithm.BACKUP_MANAGER;
        } else if ("delta".equals(algorithm)) {
            this.algorithm = SessionReplicationAlgorithm.DELTA_MANAGER;
        }
    }

    /**
     * @return {@link SessionReplicationAlgorithm}
     */
    public SessionReplicationAlgorithm getAlgorithm() {
        if (this.algorithm == null) {
            return SessionReplicationAlgorithm.DELTA_MANAGER;
        } else {
            return this.algorithm;
        }
    }

    /**
     * @return Returns the listenPort.
     */
    public String getListenPort() {
        return listenPort;
    }

    /**
     * @param listenPort The listenPort to set.
     */
    public void setListenPort(final String listenPort) {
        this.listenPort = listenPort;
    }

    /**
     * @return Returns the mcastAddr.
     */
    public String getMcastAddr() {
        return mcastAddr;
    }

    /**
     * @param mcastAddr The mcastAddr to set.
     */
    public void setMcastAddr(final String mcastAddr) {
        this.mcastAddr = mcastAddr;
    }

    /**
     * @return Returns the mcastPort.
     */
    public String getMcastPort() {
        return mcastPort;
    }

    /**
     * @param mcastPort The mcastPort to set.
     */
    public void setMcastPort(final String mcastPort) {
        this.mcastPort = mcastPort;
    }
}
