/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004-2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.antmodular.web.base;

import org.ow2.jonas.antmodular.jonasbase.bootstrap.AbstractJOnASBaseAntTask;
import org.ow2.jonas.antmodular.jonasbase.bootstrap.JOnASBaseTask;
import org.ow2.jonas.antmodular.jonasbase.bootstrap.JReplace;
import org.ow2.jonas.antmodular.jonasbase.bootstrap.JTask;


/**
 * Allow to configure the WebContainer service.
 *
 * @author Florent Benoit
 */
public abstract class WebContainer extends AbstractJOnASBaseAntTask {

    /**
     * Info for the logger.
     */
    private static final String INFO = "[WebContainer] ";

    /**
     * Default HTTP Port.
     */
    public static final String DEFAULT_PORT = "9000";

    /**
     * ondemand.enabled property.
     */
    private static final String ONDEMANDENABLED = "jonas.service.web.ondemand.enabled";
    
    /**
     * ondemand.enabled property token default value.
     */
    private static final String ONDEMANDENABLEDTOKEN = "jonas.service.web.ondemand.enabled    true";

    /**
     * ejbinwar property.
     */
    private static final String EJB_IN_WAR_PROPERTY = "jonas.service.web.ejbinwar";
    
    /**
     * ejbinwar property token default value.
     */
    private static final String EJB_IN_WAR_TOKEN = EJB_IN_WAR_PROPERTY.concat("    true");

     /**
     * ondemand.redirectPort property.
     */
    private static final String ONDEMANDREDIRECTPORT = "jonas.service.web.ondemand.redirectPort";

    /**
     * ondemand.redirectPort property token default value
     */
    private static final String ONDEMANDREDIRECTPORTTOKEN = "jonas.service.web.ondemand.redirectPort    0";

    /**
     * Name of the implementation class property
     */
    private static final String CLASS_PROPERTY = "jonas.service.web.class";

    /**
     * Default constructor.
     */
    public WebContainer() {
        super();
    }

    /**
     * Set the value of the jonas.service.web.ondemand.enabled property in jonas.properties.
     *
     * @param ondemandenabled ondemand.enabled property
     */
    public void setOndemandenabled(final String ondemandenabled) {
        // Ondemandenabled Token to replace
        String token = ONDEMANDENABLEDTOKEN;
        String value = ONDEMANDENABLED + "    "  + ondemandenabled;
        JReplace propertyReplace = new JReplace();
        propertyReplace.setConfigurationFile(JOnASBaseTask.JONAS_CONF_FILE);
        propertyReplace.setToken(token);
        propertyReplace.setValue(value);
        propertyReplace.setLogInfo(INFO + "Setting "+ ONDEMANDENABLED+" to "+ondemandenabled);

        addTask(propertyReplace);
    }

    /**
     * Set the value of the jonas.service.web.ejbinwar property in jonas.properties.
     *
     * @param ejbInWar ejbInWar property
     */
    public void setEjbInWar(final String ejbInWar) {
        // Ondemandenabled Token to replace
        String token = EJB_IN_WAR_TOKEN;
        String value = EJB_IN_WAR_PROPERTY.concat("    ").concat(ejbInWar);
        JReplace propertyReplace = new JReplace();
        propertyReplace.setConfigurationFile(JOnASBaseTask.JONAS_CONF_FILE);
        propertyReplace.setToken(token);
        propertyReplace.setValue(value);
        propertyReplace.setLogInfo(INFO + "Setting " + EJB_IN_WAR_PROPERTY + " to " + ejbInWar);

        addTask(propertyReplace);
    }
    
    /**
     * Set the value of the jonas.service.web.ondemand.redirectPort property in jonas.properties.
     *
     * @param portNumber ondemand redirect port
     */
    public void setOndemandredirectPort(final String portNumber) {
        // OndemanderedirectPort Token to replace
        String token = ONDEMANDREDIRECTPORTTOKEN;
        String value =  ONDEMANDREDIRECTPORT+ "    "  + portNumber;
        JReplace propertyReplace = new JReplace();
        propertyReplace.setConfigurationFile(JOnASBaseTask.JONAS_CONF_FILE);
        propertyReplace.setToken(token);
        propertyReplace.setValue(value);
        propertyReplace.setLogInfo(this.INFO + "Setting "+ ONDEMANDREDIRECTPORT +" to "+portNumber);

        addTask(propertyReplace);
    }


    /**
     * Create a JReplace Task for changing service classname in jonas.properties.
     *
     * @param serviceName service classname to use.
     * @return Returns a JReplace Task.
     */
    protected JTask createServiceNameReplace(final String serviceName, final String info, final String confDir) {
        return super.createServiceNameReplace(serviceName, info, confDir, CLASS_PROPERTY);
    }

    /**
     * Set the port number for the WebContainer.
     * Optionnal, if missing, we look at the inner element &lt;jetty>/&lt;http> or &lt;tomcat>/&lt;http>.
     * It overrides both &lt;jetty>/&lt;http> or &lt;tomcat>/&lt;http> values if set.
     *
     * @param portNumber the port for the HTTP web Container
     * @param confFile  the name of the configuration file of the web container
     * @param name the name of the web container
     */
    @Deprecated
    protected void setPort(String portNumber, String confFile, String name) {
        //To change body of implemented methods use File | Settings | File Templates.
        JReplace propertyReplace = new JReplace();
        propertyReplace.setConfigurationFile(confFile);
        propertyReplace.setToken(DEFAULT_PORT);
        propertyReplace.setValue(portNumber);
        propertyReplace.setLogInfo(INFO + "Setting " + name + " port number to : " + portNumber);
        addTask(propertyReplace);
    }

    /**
     * Set the port number for the WebContainer
     * @param portNumber the port number
     */
    public abstract void setPort(String portNumber);

    /**
     * Execute the task
     */
    public void execute() {
        super.execute();
        executeAllTask();
    }
}
