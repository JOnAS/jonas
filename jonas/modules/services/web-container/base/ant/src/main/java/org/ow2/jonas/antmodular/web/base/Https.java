/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006-2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.antmodular.web.base;

/**
 * Configure an HTTPS Connector for Tomcat/Jetty.
 *
 * @author Guillaume Sauthier
 */
public class Https {

   /**
     * Default HTTPS Port.
     */
    public static final String DEFAULT_PORT = "9043";

    /**
     * user specified port value.
     */
    private String port = DEFAULT_PORT;

    /**
     * keystore filename.
     */
    private String keystoreFile = null;

    /**
     * keystore password.
     */
    private String keystorePass = null;

    /**
     * @return Returns the keystore.
     */
    public String getKeystoreFile() {
        return keystoreFile;
    }

    /**
     * @param keystore The keystore to set.
     */
    public void setKeystoreFile(final String keystore) {
        this.keystoreFile = keystore;
    }

    /**
     * @return Returns the password.
     */
    public String getKeystorePass() {
        return keystorePass;
    }

    /**
     * @param password The password to set.
     */
    public void setKeystorePass(final String password) {
        this.keystorePass = password;
    }

    /**
     * @return Returns the port.
     */
    public String getPort() {
        return port;
    }

    /**
     * @param port The port to set.
     */
    public void setPort(final String port) {
        this.port = port;
    }
}
