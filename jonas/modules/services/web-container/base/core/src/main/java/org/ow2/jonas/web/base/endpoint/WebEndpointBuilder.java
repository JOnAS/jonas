/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.web.base.endpoint;

import org.ow2.jonas.endpoint.collector.Endpoint;
import org.ow2.jonas.endpoint.collector.IEndpoint;
import org.ow2.jonas.endpoint.collector.IEndpointBuilder;
import org.ow2.jonas.endpoint.collector.util.Util;
import org.ow2.jonas.jmx.JmxService;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.util.regex.Pattern;

/**
 * The Web {@link IEndpointBuilder} implementation
 */
public class WebEndpointBuilder implements IEndpointBuilder {

    /**
     * The {@link JmxService}
     */
    private JmxService jmxService;

    /**
     * @param jmxService The {@link JmxService} to bind
     */
    public void bindJmxService(final JmxService jmxService) {
        this.jmxService = jmxService;
    }

    /**
     * @param jmxService The {@link JmxService} to unbind
     */
    public void unbindJmxService(final JmxService jmxService) {
        this.jmxService = null;
    }

    /**
     * {@inheritDoc}
     */
    public boolean isSupport(final ObjectName objectName) {
        String token = this.jmxService.getDomainName() + ":type=Connector,port=.*";
        Pattern pattern = Pattern.compile(token);
        return pattern.matcher(objectName.toString()).matches();
    }

    /**
     * {@inheritDoc}
     */
    public IEndpoint buildEndpoint(final ObjectName objectName) {
        MBeanServer mBeanServer = this.jmxService.getJmxServer();
        IEndpoint endpoint = new Endpoint();
        Object value = Util.getMBeanAttributeValue(objectName, "address", mBeanServer);
        if (value != null) {
            endpoint.setHost(String.valueOf(value));
        } else {
            endpoint.setHost("localhost");
        }
        value = Util.getMBeanAttributeValue(objectName, "scheme", mBeanServer);
        if (value != null) {
            endpoint.setProtocol(String.valueOf(value));
        }
        value = Util.getMBeanAttributeValue(objectName, "port", mBeanServer);
        if (value != null) {
            endpoint.setPort(String.valueOf(value));
        }

        if (endpoint.getProtocol() != null && endpoint.getHost() != null && endpoint.getPort() != null) {
            endpoint.setUrl(endpoint.getProtocol() + "://" + endpoint.getHost() + ":" + endpoint.getPort());
        }

        endpoint.setSource(IEndpoint.SOURCE_PREFIX + endpoint.getProtocol());
        
        return endpoint;
    }
}
