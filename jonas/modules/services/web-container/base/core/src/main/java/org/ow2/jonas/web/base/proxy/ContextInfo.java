/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.web.base.proxy;

/**
 * Keep details on a context (like war file, isStarting, etc)
 * @author Florent Benoit
 */
public class ContextInfo {

    /**
     * War File.
     */
    private String warFile = null;

    /**
     * The context has been asked to start.
     */
    private boolean starting = false;

    /**
     * @return the war file associated to this context.
     */
    public String getWarFile() {
        return warFile;
    }

    /**
     * Sets the war File of this context.
     * @param warFile the given war file.
     */
    public void setWarFile(String warFile) {
        this.warFile = warFile;
    }

    /**
     * @return true if this context has been requested and is starting
     */
    public synchronized boolean isStarting() {
        return starting;
    }

    /**
     * Sets the starting mode.
     * @param starting if true, it means that the context will be loaded by the container.
     */
    public synchronized void setStarting(boolean starting) {
        this.starting = starting;
    }


}
