/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.web.base;

import org.ow2.jonas.web.JWebContainerService;
import org.ow2.jonas.web.base.proxy.HttpOnDemandProxy;
import org.ow2.jonas.web.base.proxy.HttpOnDemandProxyException;
import org.ow2.util.ee.deploy.api.deployable.IDeployable;
import org.ow2.util.ee.deploy.api.deployable.WARDeployable;
import org.ow2.util.ee.deploy.api.deployer.DeployerException;
import org.ow2.util.ee.deploy.impl.deployer.AbsDeployer;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

/**
 * Deployer of the WAR deployable.
 * @author Florent BENOIT
 */
public class WARDeployer extends AbsDeployer<WARDeployable> {

    /**
     * Logger.
     */
    private Log logger = LogFactory.getLog(WARDeployer.class);

    /**
     * Web container service used by this deployer.
     */
    private JWebContainerService webContainerService = null;

    /**
     * OnDemand proxy.
     */
    private HttpOnDemandProxy onDemandProxy = null;

    /**
     * Deploy the given WAR.
     * @param deployable the deployable to add.
     * @throws DeployerException if the WAR is not deployed.
     */
    @Override
    public void doDeploy(final IDeployable<WARDeployable> deployable) throws DeployerException {
        logger.info("Deploying {0}", deployable.getShortName());

        // OnDemand Proxy is here, keep the deployable for later
        if (onDemandProxy != null) {
            try {
                onDemandProxy.addWar(WARDeployable.class.cast(deployable));
            } catch (HttpOnDemandProxyException e) {
                throw new DeployerException("Cannot deploy the War deployable '" + deployable + "'.", e);
            }
        } else {

            // Deploy the WAR file
            try {
                webContainerService.registerWar(WARDeployable.class.cast(deployable));
            } catch (Exception e) {
                throw new DeployerException("Cannot deploy the War deployable '" + deployable + "'.", e);
            }
        }
    }

    /**
     * Undeploy the given WAR.
     * @param deployable the deployable to remove.
     * @throws DeployerException if the WAR is not undeployed.
     */
    @Override
    public void doUndeploy(final IDeployable<WARDeployable> deployable) throws DeployerException {
        logger.info("Undeploying {0}", deployable.getShortName());

        // OnDemand Proxy is here, so delegate the removal
        if (onDemandProxy != null) {
            try {
                onDemandProxy.removeWar(WARDeployable.class.cast(deployable));
            } catch (HttpOnDemandProxyException e) {
                throw new DeployerException("Cannot undeploy the War '" + deployable + "'.", e);
            }
        } else {
            // Undeploy the WAR file
            try {
                webContainerService.unRegisterWar(WARDeployable.class.cast(deployable));
            } catch (Exception e) {
                throw new DeployerException("Cannot undeploy the War '" + deployable + "'.", e);
            }
        }
    }

    /**
     * Checks if the given deployable is supported by the Deployer.
     * @param deployable the deployable to be checked
     * @return true if it is supported, else false.
     */
    @Override
    public boolean supports(final IDeployable<?> deployable) {
        return WARDeployable.class.isInstance(deployable);
    }

    /**
     * Sets the WEB container service.
     * @param webContainerService the web container service.
     */
    public void setWebContainerService(final JWebContainerService webContainerService) {
        this.webContainerService = webContainerService;
    }


    /**
     * @return the OnDemand proxy.
     */
    public HttpOnDemandProxy getOnDemandProxy() {
        return onDemandProxy;
    }

    /**
     * Sets the OnDemand proxy to use.
     * @param onDemandProxy the proxy to use
     */
    public void setOnDemandProxy(final HttpOnDemandProxy onDemandProxy) {
        this.onDemandProxy = onDemandProxy;
    }

}
