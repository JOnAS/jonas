/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.web.base.osgi.httpservice;

import java.io.IOException;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.framework.Bundle;
import org.osgi.service.http.HttpContext;


/**
 * Default OSGI HttpContext returned by {@link HttpServiceImpl#createDefaultHttpContext()}.
 * @author Guillaume Porcher
 */
public class DefaultHttpContextImpl implements HttpContext {

  /**
   * Bundle that is used to get resources.
   */
  private final Bundle bundle;

  /**
   * Constructor.
   * @param bundle the bundle that will be used to get resources.
   */
  public DefaultHttpContextImpl(final Bundle bundle) {
    this.bundle = bundle;
  }

  /**
   * Maps a name to a MIME type.
   *
   * Called by the Http Service to determine the MIME type for the name. For
   * servlet registrations, the Http Service will call this method to support
   * the <code>ServletContext</code> method <code>getMimeType</code>. For
   * resource registrations, the Http Service will call this method to
   * determine the MIME type for the Content-Type header in the response.
   *
   * @param file determine the MIME type for this name.
   * @return <code>null</code> to indicate that the Http Service should determine the MIME type
   *         itself.
   */
  public String getMimeType(final String file) {
    return null;
  }

  /**
   * Maps a resource name to a URL.
   *
   * <p>
   * Called by the Http Service to map a resource name to a URL. For servlet
   * registrations, Http Service will call this method to support the
   * <code>ServletContext</code> methods <code>getResource</code> and
   * <code>getResourceAsStream</code>. For resource registrations, Http Service
   * will call this method to locate the named resource.
   * The resource is mapped to a resource in
   * the context's bundle via <code>bundle.getResource(name)</code>
   *
   * @param name the name of the requested resource
   * @return URL that Http Service can use to read the resource or
   *         <code>null</code> if the resource does not exist.
   */
  public URL getResource(final String name) {
    return this.bundle.getResource(name);
  }

  /**
   * Handles security for the specified request.
   * <p>
   * The Http Service calls this method prior to servicing the specified
   * request. This method controls whether the request is processed in the
   * normal manner or an error is returned.
   *
   *
   * @param request the HTTP request
   * @param response the HTTP response
   * @return <code>true</code> if the request should be serviced, <code>false</code>
   *         if the request should not be serviced and Http Service will send
   *         the response back to the client.
   *         This implementation always returns <code>true</code>.
   * @throws java.io.IOException may be thrown by this method. If this
   *            occurs, the Http Service will terminate the request and close
   *            the socket.
   */
  public boolean handleSecurity(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
    return true;
  }

}
