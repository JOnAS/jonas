/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.web.base.proxy;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

/**
 * Allow to copy inputstream of the client to the remote output stream.
 * @author Florent Benoit
 */
public class CopyingStream extends Thread {

    /**
     * Logger.
     */
    private static Log logger = LogFactory.getLog(CopyingStream.class);

    /**
     * Input stream of the client.
     */
    private InputStream inputStream;

    /**
     * Output stream of the remote connection.
     */
    private OutputStream outputStream;

    /**
     * Bytes to append on the remote output stream.
     */
    private ByteArrayOutputStream byteArrayOutputStream;

    /**
     * Link to the socket handler.
     */
    private HttpSocketHandler socketHandler = null;


    /**
     * Build a new copying stream with the given parameters.
     * @param socketHandler object handling the client socket
     * @param byteArrayOutputStream the bytes to add on the remote connection
     * @param inputStream the client's input stream
     * @param outputStream the remote connection output stream
     */
    public CopyingStream(HttpSocketHandler socketHandler, ByteArrayOutputStream byteArrayOutputStream, InputStream inputStream, OutputStream outputStream) {
        this.socketHandler = socketHandler;
        this.byteArrayOutputStream = byteArrayOutputStream;
        this.inputStream = inputStream;
        this.outputStream = outputStream;
    }

    /**
     * Send the bytes of the byte array stream and then send all the client's input stream.
     */
    public void run() {
        byte[] buf = new byte[1024];

        // First, add the stored bytes
        byte[] headerBytes = byteArrayOutputStream.toByteArray();
        try {
            outputStream.write(headerBytes, 0, headerBytes.length);
        } catch (IOException e) {
            logger.debug("Cannot write bytes", e);
        }

        // Send to output stream, the input stream data
        int count;
        try {
            while (((count = inputStream.read(buf)) > 0)) {
                outputStream.write(buf, 0, count);
            }
        } catch (Exception e) {
            socketHandler.cleanup("Unable to handle data from server to the client", e);
        }
    }
}
