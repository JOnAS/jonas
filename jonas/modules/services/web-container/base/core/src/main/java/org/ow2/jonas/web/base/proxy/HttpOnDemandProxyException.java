/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.web.base.proxy;

/**
 * Exception thrown if there is a problem with the HttpOnDemand proxy.
 * @author Florent Benoit
 */
public class HttpOnDemandProxyException extends Exception {

    /**
     * Serial version UID.
     */
    private static final long serialVersionUID = -1013568505738598722L;

    /**
     * Build a new Proxy exception with the given message.
     * @param message the error message
     */
    public HttpOnDemandProxyException(final String message) {
        super(message);
    }

    /**
     * Build a new Exception with the given message and given exception
     * @param message the given error message
     * @param t the exception
     */
    public HttpOnDemandProxyException(final String message, final Throwable t) {
        super(message, t);
    }

}
