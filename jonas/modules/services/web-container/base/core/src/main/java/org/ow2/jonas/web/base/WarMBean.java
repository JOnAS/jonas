/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.web.base;

import java.net.URL;

/**
 * This interface defines the MBean methods which can be done on a war file.
 * @author Florent Benoit
 */
public interface WarMBean {

    /**
     * Return true if only if this war is in an ear file.
     * @return true if only if this war is in an ear file.
     */
    boolean isInEarCase();

    /**
     * Get the URL of the war file.
     * @return the URL of the war file.
     */
    URL getWarURL();

    /**
     * Get the URL of the ear containing this war.
     * @return the URL of the war file.
     */
    URL getEarURL();

    /**
     * Get the name of the host on which this war is deployed.
     * @return the name of the host on which this war is deployed.
     */
    String getHostName();

    /**
     * Get the context root of this war.
     * @return the context root of this war.
     */
    String getContextRoot();

    /**
     * Context classloader must follow the java2 delegation model ?
     * @return true if the context's classloader must follow the java 2
     *         delegation model.
     */
    boolean getJava2DelegationModel();

    /**
     * Return the standard xml file.
     * @return the standard xml file
     */
    String getXmlContent();

    /**
     * Return the jonas-specific xml file.
     * @return the jonas-specific xml file
     */
    String getJOnASXmlContent();

    /**
     * Return a list of all servlets available.
     * @return a list of all servlets available
     */
    String[] getServletsName();

}
