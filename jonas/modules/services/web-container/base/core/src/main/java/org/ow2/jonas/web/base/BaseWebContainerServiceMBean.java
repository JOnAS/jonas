/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.web.base;

import java.util.List;

/**
 * This interface provides a description for the web container management.
 * @author Ludovic Bert
 * @author Florent Benoit
 * @author Michel-Ange Anton (Contributor)
 */
public interface BaseWebContainerServiceMBean {

    /**
     * @return current number of wars deployed in the JOnAS server
     */
    Integer getCurrentNumberOfWars();

    /**
     * Return the list of all loaded web applications.
     * @return The list of deployed web applications
     */
    List<String> getDeployedWars();
    
    /**
     * Test if the specified filename is already deployed or not.
     * @param fileName the name of the war file.
     * @return true if the war is deployed, else false.
     */
    boolean isWarLoaded(String fileName);

    /**
     * Gets the name of the server which is the web container.
     * @return the name of the server which is the web container
     */
    String getServerName();

    /**
     * Gets the version of the server which is the web container.
     * @return the version of the server which is the web container
     */
    String getServerVersion();
}
