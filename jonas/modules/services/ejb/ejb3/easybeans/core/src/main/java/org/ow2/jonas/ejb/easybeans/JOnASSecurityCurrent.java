/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ejb.easybeans;

import org.ow2.easybeans.security.api.EZBSecurityContext;
import org.ow2.easybeans.security.api.EZBSecurityCurrent;
import org.ow2.jonas.lib.security.context.SecurityCurrent;

/**
 * Wrapper for the Security of JOnAS.
 * @author Florent BENOIT
 */
public class JOnASSecurityCurrent implements EZBSecurityCurrent {


    /**
     * Gets the current context.
     * @return SecurityContext return the Security context associated to the
     *         current thread or the JVM
     */
    public EZBSecurityContext getSecurityContext() {
        // wrap it
        return new JOnASSecurityContextWrapper(SecurityCurrent.getCurrent().getSecurityContext());

    }

    /**
     * Associates the given security context to the current thread.
     * @param securityContext Security context to associate to the current thread.
     */
    public void setSecurityContext(final EZBSecurityContext securityContext) {
        // Do nothing, JOnAS interceptor will call JOnAS method.
    }

    /**
     * Associates the given security context to all threads (JVM).
     * @param securityContext Security context to associate to the JVM
     */
    public void setGlobalSecurityContext(final EZBSecurityContext securityContext) {
        // Do nothing, JOnAS interceptor will call JOnAS method.
    }

}
