/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ejb.easybeans;

import javax.resource.ResourceException;
import javax.resource.spi.ResourceAdapter;

import org.ow2.easybeans.container.mdb.helper.IResourceAdapterFinder;
import org.ow2.jonas.resource.Rar;
import org.ow2.jonas.resource.ResourceService;

/**
 * Allow to find the Resource Adapter object for a given JNDI name.
 * @author Florent BENOIT
 */
public class JOnASResourceAdapterFinder implements IResourceAdapterFinder {

    /**
     * Easybeans service.
     */
    private EasyBeansService easyBeansService;

    /**
     * Sets the easybeans service.
     * @param easyBeansService the given service to set.
     */
    public void setEasyBeansService(final EasyBeansService easyBeansService) {
        this.easyBeansService = easyBeansService;
    }

    /**
     * Gets the resource adapter object for the given jndi name (activation
     * spec) and the given embedded object.
     * @param jndiName the name of the activation spec bound in the registry
     * @return an instance of the resource adapter that provides the MDB
     *         activation spec.
     * @throws ResourceException if an error occurs while trying to get the
     *         resource adapter.
     */
    public ResourceAdapter getResourceAdapter(final String jndiName) throws ResourceException {
        ResourceService resourceService = easyBeansService.getResourceService();
        if (resourceService == null) {
            throw new ResourceException(
                    "No Resource Service running in JOnAS, unable to get the resource adapter with JNDI Name '"
                            + jndiName + "'.");
        }

        // Get RAR object
        Rar rar = resourceService.getRar(jndiName);

        // Check found an object
        if (rar == null) {
            throw new ResourceException(
                    "Unable to find the Resource Adapter object in the resource service with the JNDI Name '"
                            + jndiName + "'.");
        }

        // return the resource adapter
        return rar.getResourceAdapter();

    }

}
