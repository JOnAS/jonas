/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ejb.easybeans;

import org.ow2.jonas.ejb3.IEasyBeansService;
import org.ow2.jonas.lib.work.AbsCleanTask;
import org.ow2.jonas.workcleaner.IDeployerLog;
import org.ow2.jonas.workcleaner.LogEntry;
import org.ow2.jonas.workcleaner.WorkCleanerException;

/**
 * JOnAS EB3 unused directory clean task class. This class provides a way for
 * removing directories which are inconsistent directories for jar files.
 * @author Francois Fornaciari
 */
public class EJB3CleanTask extends AbsCleanTask {

    /**
     * {@link IDeployerLog} of the task.
     */
    private IDeployerLog deployerLog = null;

    /**
     * {@link IEasyBeansService} reference.
     */
    private IEasyBeansService easyBeansService;

    /**
     * Construct a new EAR clean task.
     * @param easyBeansService IEasyBeansService reference
     * @param deployerLog DeployerLog of the task
     */
    public EJB3CleanTask(final IEasyBeansService easyBeansService, final IDeployerLog deployerLog) {
        this.easyBeansService = easyBeansService;
        this.deployerLog = deployerLog;
    }

    /**
     * Check if the package pointed by the log entry is currently deploy.
     * @param logEntry entry in a deploy log
     * @return true if the package pointed by the log entry is currently deployed
     * @throws WorkCleanerException If the method fails
     */
    @Override
    protected boolean isDeployedLogEntry(final LogEntry logEntry) throws WorkCleanerException {
        // Check if the EJB3 file is deployed
        return easyBeansService.isEJB3DeployedByWorkName(logEntry.getCopy().getName());
    }

    /**
     * {@inheritDoc}
     * @see org.ow2.jonas.lib.work.AbsCleanTask#getDeployerLog()
     */
    @Override
    public IDeployerLog getDeployerLog() {
        return deployerLog;
    }
}
