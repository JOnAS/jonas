/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008-2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ejb.easybeans;

import org.ow2.easybeans.api.bean.info.EZBBeanNamingInfo;
import org.ow2.easybeans.api.naming.EZBJNDINamingInfo;
import org.ow2.easybeans.api.naming.EZBNamingStrategy;
import org.ow2.easybeans.naming.BeanNamingInfo;
import org.ow2.easybeans.naming.strategy.EasyBeansV1NamingStrategy;
import org.ow2.easybeans.naming.strategy.JNDINamingInfo;
import org.ow2.easybeans.naming.strategy.JavaEE6NamingStrategy;

/**
 * JNDI naming strategy with prefixes. In this strategy, the JNDI name is
 * rewritten by adding a prefix in front of it.
 *
 * @author S. Ali Tokmen
 */
public class PrefixedNamingStrategy implements EZBNamingStrategy {
    /**
     * JNDI prefix.
     */
    private String prefix;

    /**
     * Old naming strategy.
     */
    private EZBNamingStrategy oldNamingStrategy;

    /**
     * Saves the arguments.
     *
     * @param prefix JNDI prefix.
     * @param oldNamingStrategy Old naming strategy.
     */
    public PrefixedNamingStrategy(final String prefix, final EZBNamingStrategy oldNamingStrategy) {
        this.prefix = prefix;
        this.oldNamingStrategy = oldNamingStrategy;
    }

    /**
     * Gets the prefixed JNDI information for a given bean.
     * @param beanInfo Bean information.
     * @return JNDI name for this beanInfo.
     */
    public EZBJNDINamingInfo getJNDINaming(final EZBBeanNamingInfo beanInfo) {

        // Update app name
        if (oldNamingStrategy instanceof EasyBeansV1NamingStrategy) {
            EZBJNDINamingInfo info = oldNamingStrategy.getJNDINaming(beanInfo);
            JNDINamingInfo newInfo = new JNDINamingInfo(prefix
                    + info.jndiName());
            newInfo.setAliases(info.aliases());
            return newInfo;
        }
        // Update app name
        ((BeanNamingInfo) beanInfo).setJavaEEApplicationName(prefix
                + "javaEEApplicationName");
        return oldNamingStrategy.getJNDINaming(beanInfo);
    }
}
