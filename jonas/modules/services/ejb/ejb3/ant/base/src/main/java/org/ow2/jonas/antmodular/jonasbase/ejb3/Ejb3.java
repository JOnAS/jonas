/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.antmodular.jonasbase.ejb3;

import org.ow2.jonas.antmodular.jonasbase.bootstrap.AbstractJOnASBaseAntTask;
import org.ow2.jonas.antmodular.jonasbase.bootstrap.JOnASBaseTask;
import org.ow2.jonas.antmodular.jonasbase.bootstrap.JTask;

import java.io.File;

/**
 * Allow to configure EJB3 service
 * @author Jeremy Cazaux
 */
public class Ejb3 extends AbstractJOnASBaseAntTask {

    /**
     * Info for the logger.
     */
    protected static final String INFO = "[EJB3] ";

     /**
     * EJB3 JPA provider property.
     */
    private static final String EJB3_JPA_PROVIDER_PROPERTY = "jonas.service.ejb3.jpa.provider";

     /**
     * EJB3 JPA provider.
     */
    private String ejb3JpaProvider;

    /**
     * Sets the EJB3 JPA provider.
     * @param ejb3JpaProvider the JPA provider
     */
    public void setEjb3JpaProvider(final String ejb3JpaProvider) {
        this.ejb3JpaProvider = ejb3JpaProvider;
    }

    /**
     * Execute this task.
     */
    @Override
    public void execute() {
        super.execute();

        // Path to JONAS_BASE
        String jBaseConf = this.destDir.getPath() + File.separator + "conf";
        log("jBaseConf:"+jBaseConf);
        JTask jtask = new JTask();
        jtask.setDestDir(destDir);

        /*if (this.services != null && (!this.services.startsWith("${") && !this.services.endsWith("}"))) {
            jtask.changeValueForKey(INFO, jBaseConf, JOnASBaseTask.JONAS_CONF_FILE,
                    SERVICES_PROPERTY, services, false);
        }*/

        jtask.changeValueForKey(INFO, jBaseConf, JOnASBaseTask.JONAS_CONF_FILE,
                EJB3_JPA_PROVIDER_PROPERTY, ejb3JpaProvider, false);

        tasks.add(jtask);
        executeAllTask();
    }
}

