/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ejb2.internal;

import org.ow2.jonas.ejb2.EJBService;
import org.ow2.util.ee.deploy.api.deployable.EJB21Deployable;
import org.ow2.util.ee.deploy.api.deployable.IDeployable;
import org.ow2.util.ee.deploy.api.deployer.DeployerException;
import org.ow2.util.ee.deploy.impl.deployer.AbsDeployer;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

/**
 * Deployer of the EJB 2.1 deployable.
 * @author Florent BENOIT
 */
public class EJB21Deployer extends AbsDeployer<EJB21Deployable> {

    /**
     * Logger.
     */
    private Log logger = LogFactory.getLog(EJB21Deployer.class);

    /**
     * EJb 2.1 service used by this deployer.
     */
    private EJBService ejb21Service = null;

    /**
     * Undeploy the given EJB-JAR.
     * @param deployable the deployable to remove.
     * @throws DeployerException if the EJB-JAR is not undeployed.
     */
    @Override
    public void doUndeploy(final IDeployable<EJB21Deployable> deployable) throws DeployerException {
        logger.info("Undeploying {0}", deployable.getShortName());

        // Undeploy the EJB 2.1 file
        try {
            ejb21Service.removeContainer(getFile(deployable).getAbsolutePath());
        } catch (Exception e) {
            throw new DeployerException("Cannot deploy the EJB 2.1 deployable '"  + deployable + "'.", e);
        }
    }

    /**
     * Deploy the given EJB-JAR.
     * @param deployable the deployable to add.
     * @throws DeployerException if the EJB-JAR is not deployed.
     */
    @Override
    public void doDeploy(final IDeployable<EJB21Deployable> deployable) throws DeployerException {
        logger.info("Deploying {0}", deployable.getShortName());

        // Deploy
        try {
            ejb21Service.createContainer(getFile(deployable).getAbsolutePath());
        } catch (Exception e) {
            throw new DeployerException("Cannot deploy the EJB 2.1 deployable '"  + deployable + "'.", e);
        }
    }

    /**
     * Checks if the given deployable is supported by the Deployer.
     * @param deployable the deployable to be checked
     * @return true if it is supported, else false.
     */
    @Override
    public boolean supports(final IDeployable<?> deployable) {
        return EJB21Deployable.class.isInstance(deployable);
    }

    /**
     * Sets the EJB 2.1 service.
     * @param ejb21Service the EJB 2.1 service.
     */
    public void setEjb21Service(final EJBService ejb21Service) {
        this.ejb21Service = ejb21Service;
    }

}
