/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ejb2.internal.delegate;

import javax.ejb.Timer;
import javax.naming.Context;
import javax.naming.NamingException;

import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;
import org.ow2.jonas.ejb2.EJBService;
import org.ow2.jonas.ejb2.JTimerHandleInfo;
import org.ow2.jonas.ejb2.TimerHandleDelegate;
import org.ow2.jonas.lib.util.Log;
import org.ow2.jonas.naming.JComponentContextFactoryDelegate;

/**
 * Implementation for TimerHandleDelegate
 * Must be added to JComponentContextFactory by addDelegate().
 * @author durieuxp
 */
public class JTimerHandleDelegate implements TimerHandleDelegate, JComponentContextFactoryDelegate {

    /**
     * Logger used for traces.
     */
    private static Logger logger = Log.getLogger(Log.JONAS_NAMING_PREFIX);

    /**
     * JNDI Binding name.
     */
    private static final String JNDI_NAME = "TimerHandleDelegate";

    /**
     * A transient (non serialized) reference to the {@link EJBService}.
     */
    private transient EJBService ejbService = null;

    /**
     * @param ejbserv the {@link EJBService} to use.
     */
    public void setEJBService(final EJBService ejbserv) {
        this.ejbService = ejbserv;
    }

    /**
     * Retrieve the {@link Timer} in the {@link EJBService}.
     * @param info Timer informations
     * @return the Timer
     * @see org.ow2.jonas.ejb2.TimerHandleDelegate#getTimer(org.ow2.jonas.ejb2.JTimerHandleInfo)
     */
    public Timer getTimer(final JTimerHandleInfo info) {
        if (ejbService == null) {
            logger.log(BasicLevel.ERROR, "EJBService not set");
            return null;
        }
        return ejbService.getTimer(info);
    }

    /**
     * Retrieve the {@link Timer} in the {@link EJBService} and restart it.
     * @param info Timer informations
     * @return the Timer
     * @see org.ow2.jonas.ejb2.TimerHandleDelegate#restartTimer(org.ow2.jonas.ejb2.JTimerHandleInfo)
     */
    public Timer restartTimer(final JTimerHandleInfo info) {
        if (ejbService == null) {
            logger.log(BasicLevel.ERROR, "EJBService not set");
            return null;
        }
        return ejbService.restartTimer(info);
    }

    /**
     * Add the {@link TimerHandleDelegate} into the ENC {@link Context}.
     * @param componentContext the ENC {@link Context}.
     * @throws NamingException if unable to bind <tt>java:comp/TimerHandleDelegate</tt>
     * @see org.ow2.jonas.naming.JComponentContextFactoryDelegate#modify(javax.naming.Context)
     */
    public void modify(final Context componentContext) throws NamingException {
        try {
            // Bind me as java:comp/TimerHandleDalegate
            componentContext.rebind(JNDI_NAME, this);
        } catch (NamingException ne) {
            // Just log the error, then rethrow the original Exception
            String err = "Cannot bind java:comp/TimerHandleDelegate object : " + ne.getMessage();
            logger.log(BasicLevel.ERROR, err);
            throw ne;
        }
    }

    /**
     * Remove the {@link TimerHandleDelegate} from the ENC {@link Context}.
     * @param componentContext the ENC {@link Context}.
     * @throws NamingException if unable to unbind <tt>java:comp/TimerHandleDelegate</tt>
     * @see org.ow2.jonas.naming.JComponentContextFactoryDelegate#undo(javax.naming.Context)
     */
    public void undo(final Context componentContext) throws NamingException {
        componentContext.unbind(JNDI_NAME);
    }

}
