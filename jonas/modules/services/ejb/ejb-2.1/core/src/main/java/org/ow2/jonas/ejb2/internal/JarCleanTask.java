/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ejb2.internal;

import org.ow2.jonas.lib.work.AbsCleanTask;
import org.ow2.jonas.workcleaner.IDeployerLog;
import org.ow2.jonas.workcleaner.LogEntry;
import org.ow2.jonas.workcleaner.WorkCleanerException;

/**
 * JOnAS JAR unused directory clean task class. This class provides a way for removing directories which are inconsistent
 * directories for jar files.
 * @author Florent BENOIT
 * @author Benoit PELLETIER
 */
public class JarCleanTask extends AbsCleanTask {

    /**
     * {@link IDeployerLog} of the task.
     */
    private IDeployerLog deployerLog = null;

    /**
     * {@link JOnASEJBService} reference.
     */
    private JOnASEJBService ejbService;

    /**
     * Construct a new EAR clean task.
     * @param ejbService EJBService reference
     * @param deployerLog DeployerLog of the task
     */
    public JarCleanTask(final JOnASEJBService ejbService, final IDeployerLog deployerLog) {
        this.ejbService = ejbService;
        this.deployerLog = deployerLog;
    }

    /**
     * Check if the package pointed by the log entry is currently deploy.
     * @param logEntry entry in a deploy log
     * @return true if the package pointed by the log entry is currently deployed
     * @throws WorkCleanerException if it fails
     */
    @Override
    protected boolean isDeployedLogEntry(final LogEntry logEntry) throws WorkCleanerException {
        // Check if the JAR file is deployed
        return ejbService.isJarDeployedByWorkName(logEntry.getCopy().getName());
    }

    /**
     * {@inheritDoc}
     * @see org.ow2.jonas.lib.work.AbsCleanTask#getDeployerLog()
     */
    @Override
    public IDeployerLog getDeployerLog() {
        return deployerLog;
    }

    public void setEjbService(final JOnASEJBService ejbService) {
        this.ejbService = ejbService;
    }
}
