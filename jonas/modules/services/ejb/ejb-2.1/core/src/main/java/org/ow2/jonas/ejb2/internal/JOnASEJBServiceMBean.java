/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2006 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): ____________________________________.
 * Contributor(s): Michel Bruno and Guillaume Riviere
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ejb2.internal;

import java.util.List;
import java.util.Set;

import javax.management.ObjectName;

/**
 * MBean Interface for EJB Service Management.
 * MBean type: Standard
 * MBean model: Inheritance (EJBServiceImpl)
 */
public interface JOnASEJBServiceMBean {

    /**
     * @return Integer Total Number of Container actually in JOnAS
     */
    Integer getCurrentNumberOfContainer();

    /**
     * @return Integer Total Number of Bean Type actually in JOnAS
     */
    Integer getTotalCurrentNumberOfBeanType();

    /**
     * @return Integer Total Number of Bmp Type actually in JOnAS
     */
    Integer getTotalCurrentNumberOfBMPType();

    /**
     * @return Integer Total Number of Cmp Type actually in JOnAS
     */
    Integer getTotalCurrentNumberOfCMPType();

    /**
     * @return Integer Total Number of Sbf Type actually in JOnAS
     */
    Integer getTotalCurrentNumberOfSBFType();

    /**
     * @return Integer Total Number of Sbl Type actually in JOnAS
     */
    Integer getTotalCurrentNumberOfSBLType();

    /**
     * @return Integer Total Number of Mdb Type actually in JOnAS
     */
    Integer getTotalCurrentNumberOfMDBType();

    /**
     * @return the ObjectName of all the ejbs using this datasource
     */
    Set<ObjectName> getDataSourceDependence(String dsName);

    /**
     * @return the ObjectName of all the ejb using this destination.
     */
    Set<ObjectName> getJmsDestinationDependence(String dsName);

    /**
     * @return the ObjectName of all the ejb using this Connection Factory.
     */
    Set<ObjectName> getJmsConnectionFactoryDependence(String cfName);

    /**
     * @return the ObjectName of all the ejb using a given Mail Factory.
     */
    Set<ObjectName> getMailFactoryDependence(String mfName);

    /**
     * Return the list of all loaded EJB container.
     * @return The list of deployed EJB container
     */
    List<String> getDeployedJars();

    /**
     * Synchronized all entity bean containers.
     * @param passivate passivate instances after synchronization.
     */
    void syncAllEntities(boolean passivate);

    /**
     * Test if the specified filename is already deployed or not.
     * @param fileName the name of the jar file.
     * @return true if the jar is deployed, else false.
     */
    boolean isJarLoaded(String fileName);


    /**
     * @return Will EJB monitoring enabled for EJBs deployed in the future?
     */
    public boolean getMonitoringEnabled();

    /**
     * @param monitoringEnabled Whether EJB monitoring will enabled for EJBs
     *                      deployed in the future.
     */
    public void setMonitoringEnabled(final boolean monitoringEnabled);

    /**
     * @return Number of milliseconds after which methods of EJBs deployed in
     *         the future will start warning.
     */
    public int getWarningThreshold();

    /**
     * @param warningThreshold Number of milliseconds after which methods of
     *                            EJBs deployed in the future will start
     *                            warning.
     */
    public void setWarningThreshold(final int warningThreshold);

    /**
     * get the total number of calls on all beans of this ejb container
     * @return the total number of calls on all beans of this ejb container
     */
    public long getNumberOfCalls();

    /**
     * get the total time spent in container + business execution for all beans
     * of this ejb container
     * @return the total time spent in container + business execution for all
     *         beans of this ejb container
     */
    public long getTotalProcessingTime();

    /**
     * get the total time spent in business execution for all beans of this
     * ejb container
     * @return the total time spent in business execution for all beans of this
     *         ejb container
     */
    public long getTotalBusinessProcessingTime();

    /**
     * get the average time spent in container + business execution for all beans
     * of this ejb container per request
     * @return the average time spent in container + business execution for all
     *         beans of this ejb container per request
     */
    public long getAverageProcessingTime();

    /**
     * get the average time spent in business execution for all beans of this
     * ejb container per request
     * @return the average time spent in business execution for all beans of this
     *         ejb container per request
     */
    public long getAverageBusinessProcessingTime();

    /**
     * Applies the current monitoring settings to deployed EJBs.
     */
    public void applyMonitorSettings(String scope);


}
