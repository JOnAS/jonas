/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): ____________________________________.
 * Contributor(s): Christophe Ney
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ejb2.internal;


import java.security.Principal;

import org.ow2.jonas.lib.ejb21.PrincipalFactory;
import org.ow2.jonas.lib.security.context.SecurityContext;
import org.ow2.jonas.lib.security.context.SecurityCurrent;

/**
 * Defines an implementation of PrincipalFactory returning a Principal object
 */
public class PrincipalFactoryImpl implements PrincipalFactory {

    /**
     * @param inRunAs request comes from a runAs component ?
     * @return a Principal object
     */
    public Principal getCallerPrincipal(boolean inRunAs) {
        SecurityCurrent current = SecurityCurrent.getCurrent();
        SecurityContext ctx = current.getSecurityContext();
        if (ctx == null) {
            return null;
        }
        return ctx.getCallerPrincipal(inRunAs);
    }
}
