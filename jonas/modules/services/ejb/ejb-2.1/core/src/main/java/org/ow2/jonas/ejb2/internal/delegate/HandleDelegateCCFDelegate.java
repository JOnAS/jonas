/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ejb2.internal.delegate;

import org.ow2.jonas.lib.ejb21.JHandleDelegate;
import org.ow2.jonas.naming.JComponentContextFactoryDelegate;

import javax.ejb.spi.HandleDelegate;
import javax.naming.Context;
import javax.naming.NamingException;


/**
 * Store the {@link javax.ejb.spi.HandleDelegate} shared instance in the <code>java:comp/</code> Context.
 * @author Guillaume Sauthier
 */
public class HandleDelegateCCFDelegate implements JComponentContextFactoryDelegate {

    /**
     * bound name.
     */
    private static final String HANDLE_DELEGATE = "HandleDelegate";

    /**
     * HandleDelegate implementation is shared by all component contexts.
     */
    private HandleDelegate handleDelegate = null;

    /**
     * {@inheritDoc}
     */
    public void modify(final Context componentContext) throws NamingException {
        // Bind java:comp/HandleDelegate
        componentContext.rebind(HANDLE_DELEGATE, getHandleDelegate());
    }

    /**
     * @return The HandleDelegate implementation for this server
     */
    private HandleDelegate getHandleDelegate() {
        if (handleDelegate == null) {
            handleDelegate = new JHandleDelegate();
        }
        return handleDelegate;
    }

    /**
     * Undo the changes done by this delegate on the given java:comp context.
     * @param componentContext <code>java:comp/</code> component context to be modified.
     * @throws NamingException thrown if something goes wrong
     *         during the {@link javax.naming.Context} update.
     */
    public void undo(final Context componentContext) throws NamingException {
        componentContext.unbind(HANDLE_DELEGATE);
    }

}
