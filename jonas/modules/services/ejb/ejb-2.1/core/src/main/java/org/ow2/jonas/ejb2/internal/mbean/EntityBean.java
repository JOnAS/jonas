/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ejb2.internal.mbean;

import org.ow2.jonas.deployment.ejb.EntityDesc;
import org.ow2.jonas.jmx.JmxService;
import org.ow2.jonas.lib.ejb21.EntityCounters;
import org.ow2.jonas.lib.ejb21.JEntityFactory;


/**
 * This class implements the EntityBean type specified in JSR77
 * @author Adriana Danes
 *
 */
public class EntityBean extends EJB {

    private String persistency;

    public EntityBean(String objectName, JEntityFactory factoryToManage, String persistency, JmxService jmx) {
        super(objectName, factoryToManage, jmx);
        this.persistency = persistency;
    }

    /**
     * get persistency type
     */
    public String getPersistency() {
        return persistency;
    }

    /**
     * get passivation time out
     * @return passivation timeout in seconds
     */
    public int getPassivationTimeOut() {
        return ((JEntityFactory) ejbToManage).getPassivationTimeout();
    }

    /**
     * set passivation time out
     * @param timeOut passivation timeout in seconds
     */
    public void setPassivationTimeOut(int timeOut) {
        ((JEntityFactory) ejbToManage).setPassivationTimeout(timeOut);
    }

    /**
     * get inactivity time out
     * @return inactivity timeout in seconds
     */
    public int getInactivityTimeOut() {
        return ((JEntityFactory) ejbToManage).getInactivityTimeout();
    }

    /**
     * get deadlock time out
     * @return deadlock timeout in seconds
     */
    public int getDeadlockTimeOut() {
        return ((JEntityFactory) ejbToManage).getDeadlockTimeout();
    }

    /**
     * get read time out
     * @return read timeout in seconds
     */
    public int getReadTimeOut() {
        return ((JEntityFactory) ejbToManage).getReadTimeout();
    }

    /**
     * @return true if bean is shared
     */
    public boolean getShared() {
        return ((JEntityFactory) ejbToManage).isShared();
    }

    /**
     * @return true if bean has hard limit
     */
    public boolean getHardLimit() {
        return ((JEntityFactory) ejbToManage).isHardLimit();
    }

    /**
     * @return min-pool-size value
     */
    public int getMinPoolSize() {
        return ((JEntityFactory) ejbToManage).getMinPoolSize();
    }

    /**
     * @return max-cache-size value
     */
    public int getMaxCacheSize() {
        return ((JEntityFactory) ejbToManage).getMaxCacheSize();
    }

    /**
     * @return pool-size value
     */
    public int getPoolSize() {
        return ((JEntityFactory) ejbToManage).getPoolSize();
    }

    /**
     * @return current number of waiters for a bean instance
     */
    public int getCurrentWaiters() {
        return ((JEntityFactory) ejbToManage).getCurrentWaiters();
    }

    /**
     * @return EJB Container lock policy
     */
    public String getLockPolicy() {
        int lockPolicy = ((JEntityFactory) ejbToManage).getLockPolicy();
        String sLockPolicy = null;
        switch (lockPolicy) {
            case EntityDesc.LOCK_CONTAINER_READ_UNCOMMITTED:
                sLockPolicy = "container-read-uncommitted";
                break;
            case EntityDesc.LOCK_CONTAINER_SERIALIZED:
                sLockPolicy = "container-serialized";
                break;
            case EntityDesc.LOCK_CONTAINER_SERIALIZED_TRANSACTED:
                sLockPolicy = "container-serialized-transacted";
                break;
            case EntityDesc.LOCK_CONTAINER_READ_COMMITTED:
                sLockPolicy = "container-read-committed";
                break;
            case EntityDesc.LOCK_DATABASE:
                sLockPolicy = "database";
                break;
            case EntityDesc.LOCK_READ_ONLY:
                sLockPolicy = "read-only";
                break;
            case EntityDesc.LOCK_CONTAINER_READ_WRITE:
                sLockPolicy = "container-read-write";
                break;
        }
        return sLockPolicy;
    }

    public boolean getPrefetch() {
        return ((JEntityFactory) ejbToManage).isPrefetch();
    }
    /**
     * @return Cache Size value
     */
    public int getCacheSize() {
        return ((JEntityFactory) ejbToManage).getCacheSize();
    }

    /**
     * Instance Counters (inTx, outTx, idle, passive, removed)
     * @return table of int values for Entity counters
     */
    public Integer[] getEntityCounters() {
        EntityCounters ec = ((JEntityFactory) ejbToManage).getEntityCounters();
        Integer[] result = new Integer[6];
        result[0] = new Integer(ec.inTx);
        result[1] = new Integer(ec.outTx);
        result[2] = new Integer(ec.idle);
        result[3] = new Integer(ec.passive);
        result[4] = new Integer(ec.removed);
        result[5] = new Integer(ec.pk);
        return result;
    }

    /**
     * Synchronize bean state for all its instances outside transactions
     */
    public void synchronize() {
        ((JEntityFactory) ejbToManage).syncDirty(true);
    }

    /**
     * Reduce number of instances in memory
     */
    public void reduceCache() {
        ((JEntityFactory) ejbToManage).reduceCache();
    }
}
