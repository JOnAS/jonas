/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ejb2.internal.mbean;

import org.ow2.jonas.jmx.JmxService;
import org.ow2.jonas.lib.ejb21.JSessionFactory;

/**
 * This class implements the SessionBean type specified in JSR77
 * @author Adriana Danes
 *
 */
public class SessionBean extends EJB {

    /**
     * @param objectName JMX object name
     * @param factoryToManage JSessionFactory factory to manage
     */
    public SessionBean(final String objectName, final JSessionFactory factoryToManage, final JmxService jmx) {
        super(objectName, factoryToManage, jmx);
    }

    /**
     * get the session time out for this session ejb
     * @return integer session time out
     */
    public int getSessionTimeOut() {
        return ((JSessionFactory) ejbToManage).getTimeout();
    }

    /**
     * set the session time out for this session bean
     * @param timeOut new session time out
     */
    public void setSessionTimeOut(final int timeOut) {
        ((JSessionFactory) ejbToManage).setTimeout(timeOut);
    }
    /**
     * @return true if EJB monitoring settings have been defined in the
     *              deployment descriptor
     */
    public boolean getMonitoringSettingsDefinedInDD() {
        return ((JSessionFactory) ejbToManage).getMonitoringSettingsDefinedInDD();
    }

    /**
     * @return true if ejb monitoring active
     */
    public boolean getMonitoringEnabled() {
        return ((JSessionFactory) ejbToManage).getMonitoringEnabled();
    }

    /**
     * @param monitoringEnabled whether ejb monitoring will be active
     */
    public void setMonitoringEnabled(final boolean monitoringEnabled) {
        ((JSessionFactory) ejbToManage).setMonitoringEnabled(monitoringEnabled);
    }

    /**
     * @return the method time threshold (in ms)
     */
    public int getWarningThreshold() {
        return ((JSessionFactory) ejbToManage).getWarningThreshold();
    }

    /**
     * @param warningThreshold new method time threshold (in ms)
     */
    public void setWarningThreshold(final int warningThreshold) {
        ((JSessionFactory) ejbToManage).setWarningThreshold(warningThreshold);
    }

    /**
     * @return the total number of calls on this ejb
     */
    public long getNumberOfCalls() {
        return ((JSessionFactory) ejbToManage).getNumberOfCalls();
    }

    /**
     * @return the total time spent in business execution
     */
    public long getTotalBusinessProcessingTime() {
        return ((JSessionFactory) ejbToManage).getTotalBusinessProcessingTime();
    }

    /**
     * @return the total time spent in container + business execution
     */
    public long getTotalProcessingTime() {
        return ((JSessionFactory) ejbToManage).getTotalProcessingTime();
    }

    /**
     * @return the average time per request spent in business execution
     */
    public long getAverageBusinessProcessingTime() {
        long numberOfCalls = getNumberOfCalls();
        if (numberOfCalls > 0) {
            return getTotalBusinessProcessingTime() / numberOfCalls;
        } else {
            return 0;
        }
    }

    /**
     * @return the average total time per request spent in container + business
     *         execution
     */
    public long getAverageProcessingTime() {
        long numberOfCalls = getNumberOfCalls();
        if (numberOfCalls > 0) {
            return getTotalProcessingTime() / numberOfCalls;
        } else {
            return 0;
        }
    }
}
