/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ejb2.internal.mbean;

import java.util.Set;

import org.ow2.jonas.deployment.ejb.MessageDrivenDesc;
import org.ow2.jonas.jmx.JmxService;
import org.ow2.jonas.lib.ejb21.JFactory;


/**
 * This class implements the MessageDrivenBean type specified in JSR77
 * @author Adriana Danes
 *
 */
public class MessageDrivenBean extends EJB {

    /**
     * @param objectName JMX object name
     * @param factoryToManage JFactory factory to manage
     */
    public MessageDrivenBean(String objectName, JFactory factoryToManage, JmxService jmx) {
        super(objectName, factoryToManage, jmx);
    }

    /**
     * @return the name of the associate destination
     */
    public String getMdbJMSAssociateDestinationName() {
        return ((MessageDrivenDesc)ejbToManage.getDeploymentDescriptor()).getDestinationJndiName();
    }

    /**
     * return all the destination used by this ejb, including his associate destination
     * @return String set with the name of the Ejb Destinations
     */
    public Set getAllJMSDestinationName() {
        // call  the super method
        Set result = super.getAllJMSDestinationName();
        result.add(getMdbJMSAssociateDestinationName());
        return result;
    }

    /**
     * Reduce number of instances in memory
     */
    public void reduceCache() {
        ((JFactory) ejbToManage).reduceCache();
    }
}
