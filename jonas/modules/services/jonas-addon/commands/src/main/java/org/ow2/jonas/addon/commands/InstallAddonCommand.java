/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2013 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.addon.commands;

import org.apache.felix.gogo.commands.Action;
import org.apache.felix.gogo.commands.Argument;
import org.apache.felix.gogo.commands.Command;
import org.apache.felix.ipojo.annotations.Component;
import org.apache.felix.ipojo.annotations.HandlerDeclaration;
import org.apache.felix.ipojo.annotations.Requires;
import org.apache.felix.service.command.CommandSession;
import org.ow2.jonas.addon.deploy.api.deployable.IAddonDeployable;
import org.ow2.jonas.addon.deploy.api.deployer.IAddonService;
import org.ow2.util.archive.api.IArchive;
import org.ow2.util.archive.impl.ArchiveManager;
import org.ow2.util.ee.deploy.api.deployable.IDeployable;
import org.ow2.util.ee.deploy.api.deployer.DeployerException;
import org.ow2.util.ee.deploy.api.helper.DeployableHelperException;
import org.ow2.util.ee.deploy.api.helper.IDeployableHelper;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

import java.io.File;

/**
 * Shelbie Command which allow to install an addon
 * @author Jeremy Cazaux
 */
@Component
@Command(name="install", scope="addon", description="install an addon")
@HandlerDeclaration("<sh:command xmlns:sh='org.ow2.shelbie'/>")
public class InstallAddonCommand implements Action {

    /**
     * The logger
     */
    private static Log logger = LogFactory.getLog(InstallAddonCommand.class);

    /**
     * The {@link org.ow2.jonas.addon.deploy.api.deployer.IAddonService}
     */
    @Requires(optional = false)
    private IAddonService addonService;

    /**
     * The {@link IDeployableHelper}
     */
    @Requires(optional = false)
    private IDeployableHelper deployableHelper = null;

    /**
     * The name of the addon to stop
     */
    @Argument(name = "path", required = true, description = "The path to the file to install")
    private String path;

    /**
     * {@inheritDoc}
     */
    @Override
    public Object execute(CommandSession commandSession) {
        IAddonDeployable addon = null;
        File file = new File(this.path);
        if (file.exists()) {
            IArchive archive = ArchiveManager.getInstance().getArchive(file);
            try {
                IDeployable<?> deployable = this.deployableHelper.getDeployable(archive);
                if (IAddonDeployable.class.isInstance(deployable)) {
                    addon = IAddonDeployable.class.cast(deployable);
                } else {
                    logger.error("The deployable " + deployable.getShortName() + " is not an Addon deployable. It won't be deploy.");
                }
            } catch (DeployableHelperException e) {
                logger.error("Could get the Deployable object for the given URL.");
            }

            if (addon != null) {
                try {
                    this.addonService.install(addon,true);
                } catch (DeployerException e) {
                    logger.error("The addon " + addon.getShortName() + " could not be installed.", e);
                }
            }
        } else {
            logger.error("The file " + file + " doesn't exist");
        }

        return null;
    }
}
