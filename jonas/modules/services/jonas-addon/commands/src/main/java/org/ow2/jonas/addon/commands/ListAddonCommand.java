/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2013 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.addon.commands;

import org.apache.felix.gogo.commands.Action;
import org.apache.felix.gogo.commands.Command;
import org.apache.felix.ipojo.annotations.Component;
import org.apache.felix.ipojo.annotations.HandlerDeclaration;
import org.apache.felix.ipojo.annotations.Requires;
import org.apache.felix.service.command.CommandSession;
import org.ow2.jonas.addon.deploy.api.deployable.IAddonDeployable;
import org.ow2.jonas.addon.deploy.api.deployer.IAddonService;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

import java.util.List;
import java.util.Map;

/**
 * Shelbie Command which allow to show the list of addons
 * @author Jeremy Cazaux
 */
@Component
@Command(name="list", scope="addon", description="list all installed addons")
@HandlerDeclaration("<sh:command xmlns:sh='org.ow2.shelbie'/>")
public class ListAddonCommand implements Action {

    /**
     * Eol
     */
    public static final String EOL = "\n";

    /**
     * The {@link IAddonService}
      */
    @Requires(optional = false)
    private IAddonService addonService;

    /**
     * The logger
     */
    private static Log logger = LogFactory.getLog(ListAddonCommand.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public Object execute(CommandSession commandSession) throws Exception {

        //get the list of installed addons
        List<IAddonDeployable> addons = this.addonService.getAddons();

        StringBuilder stringBuilder = new StringBuilder();
        try {
            stringBuilder.append("   ID|Parent ID|State      |Name");
            for (IAddonDeployable addon: addons) {
                String state = getStateAsString(addon.getState());
                if (state != null) {
                    String id = String.valueOf(addon.getAddonId());
                    while (id.length() < 4) {
                        id = " " + id;
                    }
                    IAddonDeployable parent = addon.getParent();
                    String parentId;
                    if (parent != null) {
                        parentId = String.valueOf(parent.getAddonId());
                    } else {
                        parentId = "none";
                    }
                    while (parentId.length() < 4) {
                        parentId = " " + parentId;
                    }
                    stringBuilder.append(EOL + " " + id + "|     " + parentId + "|" + state + "|" + addon.getMetadata().getName());
                }
            }
        } catch (Exception e) {
            logger.error("Could get the list of addons", e);
        }

        return stringBuilder.toString();
    }

    /**
     * @param state The state of an addon as an {@link Integer}
     * @return the state as a {@link String}
     */
    public static String getStateAsString(final Integer state) {
        if (state == IAddonDeployable.ACTIVE) {
            return "Active     ";
        } else if (state == IAddonDeployable.INSTALLED) {
            return "Installed  ";
        } else if (state == IAddonDeployable.RESOLVED) {
            return "Resolved   ";
        } else if (state == IAddonDeployable.STARTING) {
            return "Starting   ";
        } else if (state == IAddonDeployable.STOPPING) {
            return "Stopping   ";
        } else {
            return null;
        }
    }
}
