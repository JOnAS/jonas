/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2013 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.addon.commands;

import org.apache.felix.gogo.commands.Action;
import org.apache.felix.gogo.commands.Argument;
import org.apache.felix.gogo.commands.Command;
import org.apache.felix.ipojo.annotations.Component;
import org.apache.felix.ipojo.annotations.HandlerDeclaration;
import org.apache.felix.ipojo.annotations.Requires;
import org.apache.felix.service.command.CommandSession;
import org.ow2.jonas.addon.deploy.api.deployable.IAddonDeployable;
import org.ow2.jonas.addon.deploy.api.deployer.IAddonService;

import java.lang.String;
import java.util.ArrayList;
import java.util.List;

/**
 * Inspect capabilities / requirement
 * @author Jeremy Cazaux
 */
@Component
@Command(name="inspect", scope="addon", description="inspect addon dependency information")
@HandlerDeclaration("<sh:command xmlns:sh='org.ow2.shelbie'/>")
public class InspectCommand implements Action {

    /**
     * Requirement token
     */
    public static final String REQUIREMENT_TOKEN = "requirement";

    /**
     * Capability token
     */
    public static final String CAPABILITY_TOKEN = "capability";

    /**
     * The {@link IAddonService}
     */
    @Requires(optional = false)
    private IAddonService addonService;

    /**
     * List of arguments
     */
    @Argument(name = "arguments", required = true, multiValued = true)
    private List<String> arguments;

    /**
     * {@inheritDoc}
     */
    @Override
    public Object execute(CommandSession commandSession) throws Exception {
        int nbArgs = this.arguments.size();

        if (nbArgs < 1) {
            return "Invalid arguments";
        }
        String type = this.arguments.get(0);
        if (REQUIREMENT_TOKEN.equals(type)) {
            if (nbArgs != 2) {
                return "Invalid arguments";
            } else {
                //inspect requirements of an addon
                Long addonId;
                try {
                    addonId = Long.parseLong(this.arguments.get(1));
                } catch (NumberFormatException e) {
                    return "Invalid addon id";
                }

                IAddonDeployable addon = this.addonService.getAddon(addonId);
                if (addon == null) {
                    return "Could not retrieve addon with id " + addonId;
                } else {
                    List<String> requirements = addon.getRequirements();
                    return printList(requirements);
                }
            }
        } else if (CAPABILITY_TOKEN.equals(type)) {
            if (nbArgs == 1) {
                //inspect all capabilities
                List<IAddonDeployable> addons = this.addonService.getAddons();
                List<String> capabilities = new ArrayList<String>();
                for (IAddonDeployable addon: addons) {
                    List<String> list = addon.getCapabilities();
                    for (String capability: list) {
                        if (!capabilities.contains(capability)) {
                            capabilities.add(capability);
                        }
                    }
                }
                return printList(capabilities);
            }  else if (nbArgs == 2) {
                //inspect capability of an addon
                Long addonId;
                try {
                    addonId = Long.parseLong(this.arguments.get(1));
                } catch (NumberFormatException e) {
                    return "Invalid addon id";
                }

                IAddonDeployable addon = this.addonService.getAddon(addonId);
                if (addon == null) {
                    return "Could not retrieve addon with id " + addonId;
                } else {
                    List<String> capabilities = addon.getCapabilities();
                    return printList(capabilities);
                }
            } else {
                return "Invalid arguments";
            }
        } else {
            return "Invalid arguments";
        }
    }

    public static String printList(List<String> list) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[");
        if (list != null && !list.isEmpty()) {
            for (int i = 0; i < list.size(); i++) {
                stringBuilder.append(list.get(i));
                if (i < list.size() - 1) {
                    stringBuilder.append(",");
                }
            }
        }
        stringBuilder.append("]");
        return stringBuilder.toString();
    }
}
