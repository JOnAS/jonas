/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2013 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.addon.commands;

import org.apache.felix.gogo.commands.Action;
import org.apache.felix.gogo.commands.Command;
import org.apache.felix.ipojo.annotations.Component;
import org.apache.felix.ipojo.annotations.HandlerDeclaration;
import org.apache.felix.ipojo.annotations.Requires;
import org.apache.felix.service.command.CommandSession;
import org.ow2.jonas.addon.deploy.api.deployer.IAddonService;

/**
 * Shelbie Command which allow to resolve all unresolved addons
 * @author Jeremy Cazaux
 */
@Component
@Command(name="resolve", scope="addon", description="resolve addons")
@HandlerDeclaration("<sh:command xmlns:sh='org.ow2.shelbie'/>")
public class ResolveAddonsCommand implements Action {

    /**
     * The {@link org.ow2.jonas.addon.deploy.api.deployer.IAddonService}
     */
    @Requires(optional = false)
    private IAddonService addonService;

    /**
     * {@inheritDoc}
     */
    @Override
    public Object execute(final CommandSession commandSession) {
        this.addonService.resolve();
        return null;
    }
}
