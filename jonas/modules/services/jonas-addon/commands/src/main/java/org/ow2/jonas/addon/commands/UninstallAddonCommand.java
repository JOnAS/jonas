/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2013 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.addon.commands;

import org.apache.felix.gogo.commands.Action;
import org.apache.felix.gogo.commands.Argument;
import org.apache.felix.gogo.commands.Command;
import org.apache.felix.ipojo.annotations.Component;
import org.apache.felix.ipojo.annotations.HandlerDeclaration;
import org.apache.felix.ipojo.annotations.Requires;
import org.apache.felix.service.command.CommandSession;
import org.ow2.jonas.addon.deploy.api.deployable.IAddonDeployable;
import org.ow2.jonas.addon.deploy.api.deployer.IAddonService;
import org.ow2.util.ee.deploy.api.deployer.DeployerException;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Shelbie Command which allow to uninstall an addon
 * @author Jeremy Cazaux
 */
@Component
@Command(name="uninstall", scope="addon", description="uninstall addons")
@HandlerDeclaration("<sh:command xmlns:sh='org.ow2.shelbie'/>")
public class UninstallAddonCommand implements Action {

    /**
     * The logger
     */
    private static Log logger = LogFactory.getLog(UninstallAddonCommand.class);

    /**
     * The {@link org.ow2.jonas.addon.deploy.api.deployer.IAddonService}
     */
    @Requires(optional = false)
    private IAddonService addonService;

    /**
     * The name of the addon to uninstall
     */
    @Argument(name = "addonIdList", required = true, description = "Id of the Addon to uninstall", multiValued = true)
    private List<String> addonIdList;

    /**
     * {@inheritDoc}
     */
    @Override
    public Object execute(CommandSession commandSession)  {
        List<IAddonDeployable> addons = new ArrayList<IAddonDeployable>();

        //get the list of adon to uninstall
        if (this.addonIdList != null) {
            for (String addonId: this.addonIdList) {
                Long id = null;
                try {
                    id = Long.parseLong(addonId);
                } catch (NumberFormatException e) {
                    logger.error("Invalid addon id " + addonId, e);
                }
                IAddonDeployable addon = this.addonService.getAddon(id);
                if (addon != null) {
                    addons.add(addon);
                } else {
                    logger.error("Unable to cache addon: " + id);
                }
            }

            //uninstall all addons
            for (IAddonDeployable addon: addons) {
                try {
                    this.addonService.uninstall(addon.getMetadata().getName());
                } catch (DeployerException e) {
                    logger.error("The addon " + addon.getMetadata().getName() + " could not be uninstalled.", e);
                }
            }
        }

        return null;
    }
}
