/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2013 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.addon.commands;

import org.apache.felix.gogo.commands.Action;
import org.apache.felix.gogo.commands.Command;
import org.apache.felix.ipojo.annotations.Component;
import org.apache.felix.ipojo.annotations.HandlerDeclaration;
import org.apache.felix.service.command.CommandSession;

/**
 * Addon help command
 * @author Jeremy Cazaux
 */
@Component
@Command(name="help", scope="addon", description="display available commands")
@HandlerDeclaration("<sh:command xmlns:sh='org.ow2.shelbie'/>")
public class HelpCommand implements Action {

    /**
     * {@inheritDoc}
     */
    @Override
    public Object execute(CommandSession commandSession) throws Exception {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(ListAddonCommand.EOL);
        stringBuilder.append("Addon commands:");
        stringBuilder.append(ListAddonCommand.EOL);
        stringBuilder.append("-----------------------------------");
        stringBuilder.append(ListAddonCommand.EOL);
        stringBuilder.append(print("addon:info <addon.id>"));
        stringBuilder.append("show addon information");
        stringBuilder.append(ListAddonCommand.EOL);
        stringBuilder.append(print("addon:inspect capability"));
        stringBuilder.append("inspect all exported capabilities");
        stringBuilder.append(ListAddonCommand.EOL);
        stringBuilder.append(print("addon:inspect capability <addon.id>"));
        stringBuilder.append("inspect capabilities of the addon");
        stringBuilder.append(ListAddonCommand.EOL);
        stringBuilder.append(print("addon:inspect requirement <addon.id>"));
        stringBuilder.append("inspect requirements of the addon");
        stringBuilder.append(ListAddonCommand.EOL);
        stringBuilder.append(print("addon:install <addon.path>"));
        stringBuilder.append("install an addon");
        stringBuilder.append(ListAddonCommand.EOL);
        stringBuilder.append(print("addon:list"));
        stringBuilder.append("list all installed addons");
        stringBuilder.append(ListAddonCommand.EOL);
        stringBuilder.append(print("addon:resolve"));
        stringBuilder.append("resolve addons");
        stringBuilder.append(ListAddonCommand.EOL);
        stringBuilder.append(print("addon:start [<addon.id>]"));
        stringBuilder.append("start addons");
        stringBuilder.append(ListAddonCommand.EOL);
        stringBuilder.append(print("addon:stop [<addon.id>]"));
        stringBuilder.append("stop addons");
        stringBuilder.append(ListAddonCommand.EOL);
        stringBuilder.append(print("addon:uninstall [<addon.id>]"));
        stringBuilder.append("uninstall addons");

        return stringBuilder.toString();
    }

    private String print(final String string) {
        String str = string.trim();
        str = "  " + str;
        while (str.length() < 50) {
            str = str + " ";
        }
        return str;
    }
}
