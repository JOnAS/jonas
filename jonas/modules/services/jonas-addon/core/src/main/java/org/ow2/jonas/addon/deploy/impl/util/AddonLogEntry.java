/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.addon.deploy.impl.util;

import org.ow2.jonas.addon.deploy.api.util.IAddonLogEntry;
import org.ow2.jonas.lib.work.LogEntryImpl;

import java.io.File;

/**
 * Class wich represent an entry in an addon log file
 * @author Jeremy Cazaux
 */
public class AddonLogEntry extends LogEntryImpl implements IAddonLogEntry {

    /**
     * The name of the addon
     */
    private String name;

    /**
     * The state of the addon
     */
    private int state;

    /**
     * true if the addon has been installed from a Shelbie command. Otherwise, the addon has been installed from the
     * deployment system
     */
    private boolean isInstalledFromACommand;

    /**
     * Constructor of a log entry.
     * @param name the name of the addon
     * @param state the state of the addon
     * @param original the file to copy
     * @param copy     name of the copy
     */
    public AddonLogEntry(final String name, final int state, final boolean isInstalledFromACommand, final File original, final File copy) {
        super(original, copy);
        this.name = name;
        this.state = state;
        this.isInstalledFromACommand = isInstalledFromACommand;
    }

    /**
     * @return the name of the addon
     */
    public String getName() {
        return name;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getState() {
        return state;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateState(final int state) {
        this.state = state;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isInstalledFromACommand() {
        return this.isInstalledFromACommand;
    }
}
