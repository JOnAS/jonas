/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.addon.deploy.impl.deployable;

import org.ow2.jonas.addon.deploy.api.deployable.ISortableDeployable;
import org.ow2.jonas.addon.deploy.impl.util.AddonUtil;
import org.ow2.util.ee.deploy.api.deployable.IDeployable;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;
import org.ow2.util.plan.deploy.deployable.api.DeploymentPlanDeployable;

import java.io.Serializable;
import java.util.Comparator;
import java.util.regex.Pattern;

/**
 * This class represents a comparator for ResourceOrderPreference Class
 * @author Jeremy Cazaux
 */
public class SortableDeployableComparator  implements Serializable, Comparator<ISortableDeployable> {

    /**
     * The logger
     */
    private static Log logger = LogFactory.getLog(SortableDeployableComparator.class);

    /**
     * @param sortableDeployable1 The first deployable to compare
     * @param sortableDeployable2 The second deployable to compare
     * @return a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater
     * than the second.
     */
    public int compare(final ISortableDeployable sortableDeployable1, final ISortableDeployable sortableDeployable2) {

        if (sortableDeployable1.getPriority() > sortableDeployable2.getPriority()) {
            return 1;
        } else if (sortableDeployable1.getPriority() < sortableDeployable2.getPriority()) {
            return -1;
        } else {

            IDeployable deployable1 = sortableDeployable1.getDeployable();
            IDeployable deployable2 = sortableDeployable2.getDeployable();

            //if deployable 1 & 2 are instance of DeploymentPlanDeployable, we check the type of deployment plan
            //Abstract deployment plan have the higthest priority
            if (deployable1 instanceof DeploymentPlanDeployable &&
                deployable2 instanceof DeploymentPlanDeployable) {

                //Pattern for abstract deployment plan
                Pattern pattern = Pattern.compile(".*" + AddonUtil.DASH + AddonUtil.ABSTRACT_DEPLOYMENT_PLAN_KEY +
                        AddonUtil.XML_EXTENSION);

                Boolean isAbstractDeploymentPlan1 = pattern.matcher(deployable1.getShortName()).matches();
                Boolean isAbstractDeploymentPlan2 = pattern.matcher(deployable2.getShortName()).matches();

                if (isAbstractDeploymentPlan1 && !isAbstractDeploymentPlan2)
                    return -1;
                else if (!isAbstractDeploymentPlan1 && isAbstractDeploymentPlan2) {
                    return 1;
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        }
    }
}
