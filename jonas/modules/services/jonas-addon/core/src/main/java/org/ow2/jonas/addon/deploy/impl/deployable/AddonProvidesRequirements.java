/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.addon.deploy.impl.deployable;

import org.ow2.jonas.addon.deploy.api.deployable.IAddonDeployable;
import org.ow2.util.ee.deploy.api.deployable.IDeployable;

import java.util.List;

/**
 * A {@code AddonProvidesRequirements} is...
 */
public class AddonProvidesRequirements {

    /**
     * The {@link IDeployable}
     */
    private IDeployable<IAddonDeployable> deployable;

    /**
     * Properties provides by the addon
     */
    private List<String> provides;

    /**
     * Requirements of the addon
     */
    private List<String> requirements;

    /**
     * Default constructor
     * @param deployable The {@link IDeployable}
     * @param provides Properties provides by the addon
     * @param requirements Requirements of the addon
     */
    public AddonProvidesRequirements(IDeployable<IAddonDeployable> deployable, List<String> provides, List<String> requirements) {
        this.deployable = deployable;
        this.provides = provides;
        this.requirements = requirements;
    }

    /**
     * @return the deployable
     */
    public IDeployable<IAddonDeployable> getDeployable() {
        return deployable;
    }

    /**
     * @return properties provides by the addon
     */
    public List<String> getProvides() {
        return provides;
    }

    /**
     * @return requirements of the addon
     */
    public List<String> getRequirements() {
        return requirements;
    }
}
