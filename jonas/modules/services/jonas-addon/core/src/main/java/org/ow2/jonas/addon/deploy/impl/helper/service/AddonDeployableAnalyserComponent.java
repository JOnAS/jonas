/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.addon.deploy.impl.helper.service;

import org.ow2.jonas.addon.deploy.api.deployable.IAddonDeployable;
import org.ow2.jonas.addon.deploy.impl.deployable.AddonDeployableImpl;
import org.ow2.jonas.lib.service.AbsServiceImpl;
import org.ow2.jonas.service.ServiceException;
import org.ow2.util.archive.api.ArchiveException;
import org.ow2.util.archive.api.IArchive;
import org.ow2.util.archive.api.IFileArchive;
import org.ow2.util.ee.deploy.api.deployable.IDeployable;
import org.ow2.util.ee.deploy.api.helper.IDeployableAnalyser;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

/**
 * Analyze the URL and create a deployable object.
 * @author Jeremy Cazaux
 */
public class AddonDeployableAnalyserComponent extends AbsServiceImpl implements IDeployableAnalyser {

    /**
     * The logger
     */
    private static Log logger = LogFactory.getLog(AddonDeployableAnalyserComponent.class);

    /**
     * Analyze the URL and create a deployable object.
     * @param archive The given archive
     * return the deployable object
     */
    public IDeployable<?> analyze(final IArchive archive) {
        AddonDeployableImpl addonDeployable = null;
        if (isSupport(archive)) {
            addonDeployable = new AddonDeployableImpl(archive);
        }
        return addonDeployable;
    }

    /**
     * @param archive The given archive
     * @return true if the archive is an addon deployable. Otherwise, false.
     */
    protected boolean isSupport(final IArchive archive) {

        if (!(archive instanceof IFileArchive)) {

            try {
                return (archive.getResource(IAddonDeployable.JONAS_ADDON_METADATA_ZIP_ENTRY) != null);
            } catch (ArchiveException e) {
                logger.error("Cannot get the following resource of the archive: " + IAddonDeployable.JONAS_ADDON_METADATA_ZIP_ENTRY, e);
            }
        }

        return false;
    }

    /**
     * Method for service starting
     * @throws ServiceException
     */
    @Override
    protected void doStart() throws ServiceException {
    }

    /**
     * Method for service stopping
     * @throws ServiceException
     */
    @Override
    protected void doStop() throws ServiceException {
    }

    /**
     * Default constructor
     */
    public AddonDeployableAnalyserComponent() {
    }
}
