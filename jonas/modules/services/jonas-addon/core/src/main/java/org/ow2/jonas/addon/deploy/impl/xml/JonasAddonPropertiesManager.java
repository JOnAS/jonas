/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.addon.deploy.impl.xml;

import java.util.List;
import java.util.Properties;

/**
 * Reads and manages the properties files
 * @author Mohammed Boukada
 */
public class JonasAddonPropertiesManager {

    /**
     * Jonas-addon properties
     */
    private static Properties properties;

    /**
     * Key for the jonas-addon 1.0 XSD path in the properties file
     */
    private static String JONAS_ADDON_1_KEY_XSD = "xsdJonasAddonV1";

    /**
     * Key for the jonas-addon 1.0's XML namespace in the properties file
     */
    private static String JONAS_ADDON_1_KEY_XMLNS = "xmlnsJonasAddonV1";

    /**
     * Key for the jonas-addon XML namespace in the properties file
     */
    private static String JONAS_ADDON_1_KEY_SCHEMA_LOCATION = "schemaLocationJonasAddonV1";

    /**
     * Return the jonas-addon schema path
     * @param jonasAddonVersion {@link JonasAddonVersion}
     * @return the jonas-addon schema path
     */
    public static String getXsdCloudApplicationPath(final JonasAddonVersion jonasAddonVersion) {
        if (JonasAddonVersion.JONAS_ADDON_1.equals(jonasAddonVersion)) {
            return properties.getProperty(JonasAddonPropertiesManager.JONAS_ADDON_1_KEY_XSD);
        }
        return null;
    }

    /**
     * Return the jonas-addon XML namespace
     * @param jonasAddonVersion {@link JonasAddonVersion}
     * @return the jonas-addon XML namespace
     */
    public static String getCloudApplicationXMLNS(final JonasAddonVersion jonasAddonVersion) {
        if (JonasAddonVersion.JONAS_ADDON_1.equals(jonasAddonVersion)) {
            return properties.getProperty(JonasAddonPropertiesManager.JONAS_ADDON_1_KEY_XMLNS);
        }
        return null;
    }

    /**
     * Return the jonas-addon schema location
     * @param jonasAddonVersion {@link JonasAddonVersion}
     * @return the jonas-addon schema location
     */
    public static String getCloudApplicationSchemaLocation(final JonasAddonVersion jonasAddonVersion) {
        if (JonasAddonVersion.JONAS_ADDON_1.equals(jonasAddonVersion)) {
            return properties.getProperty(JonasAddonPropertiesManager.JONAS_ADDON_1_KEY_SCHEMA_LOCATION);
        }
        return null;
    }

    /**
     * @param namespace A XML namesapce
     * @return the correct jonas-addon version according to the given namespace
     */
    public static JonasAddonVersion getJonasAddonVersion(final String namespace) {
        if (properties.getProperty(JonasAddonPropertiesManager.JONAS_ADDON_1_KEY_XMLNS).equals(namespace)) {
            return JonasAddonVersion.JONAS_ADDON_1;
        }
        return null;
    }

    /**
     * @param namespaces A list of namespaces
     * @return the correct jonas-addon version according to the given list of namespaces
     */
    public static JonasAddonVersion getJonasAddonVersion(final List<String> namespaces) {
        JonasAddonVersion jonasAddonVersion = null;
        for (String namespace: namespaces) {
            jonasAddonVersion = getJonasAddonVersion(namespace);
            if (jonasAddonVersion != null) {
                return jonasAddonVersion;
            }
        }
        return null;
    }

    /**
     * Set properties
     * @param props
     */
    public static void setProperties(Properties props) {
        properties = props;
    }
}
