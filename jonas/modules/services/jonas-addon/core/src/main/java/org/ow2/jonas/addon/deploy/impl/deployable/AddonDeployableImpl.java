/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.addon.deploy.impl.deployable;

import org.ow2.jonas.addon.deploy.api.deployable.IAddonDeployable;
import org.ow2.jonas.addon.deploy.api.deployable.IAddonMetadata;
import org.ow2.jonas.addon.deploy.api.deployable.ISortableDeployable;
import org.ow2.util.archive.api.IArchive;
import org.ow2.util.ee.deploy.api.deployable.UnknownDeployable;
import org.ow2.util.ee.deploy.impl.deployable.AbsDeployable;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents an Addon Deployable
 * @author Jeremy Cazaux
 */
public class AddonDeployableImpl extends AbsDeployable<IAddonDeployable> implements IAddonDeployable {

    /**
     * List of deployables
     */
    private List<ISortableDeployable> deployables = null;

    /**
     * The {@link IAddonMetadata} of the addon
     */
    private IAddonMetadata addonMetadata;

    /**
     * State of the addon
     */
    private int state;

    /**
     * the addon unique identifier.
     */
    private Long addonId;

    /**
     * List of exported capabilities
     */
    private List<String> capabilities;

    /**
     * {@link IAddonDeployable} parent (usefull for embedded addon)
     */
    private IAddonDeployable parent;

    /**
     * The the configuration directory of the addon
     */
    private File configurationDirectory;

    /**
     * list of configuration files
     */
    private List<String> configurationFileNames;

    /**
     * Defines and create a deployable for the given archive.
     * @param archive the given archive.
     */
    public AddonDeployableImpl(final IArchive archive) {
        super(archive);
        this.deployables = new ArrayList<ISortableDeployable>();
        this.capabilities = new ArrayList<String>();
        this.configurationFileNames = new ArrayList<String>();
        this.state = UNINSTALLED;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addDeployable(final ISortableDeployable sortableDeployable) {
        this.deployables.add(sortableDeployable);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeDeployable(final ISortableDeployable sortableDeployable) {
        this.deployables.remove(sortableDeployable);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        if (this.addonMetadata != null) {
            return this.addonMetadata.getName();
        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getState() {
        return this.state;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void updateState(final int state) {
       this.state = state;

        if (ACTIVE == this.state) {

            if (!this.capabilities.isEmpty()) {
                this.capabilities.clear();
            }

            if (this.addonMetadata != null) {
                List<String> capabilities = this.addonMetadata.getProvides();
                if (capabilities != null && !capabilities.isEmpty()) {
                    for (String capability: capabilities) {
                        if (!capability.isEmpty()) {
                            this.capabilities.add(capability);
                        }
                    }
                }
            }
        } else {
            if (!this.capabilities.isEmpty()) {
                this.capabilities.clear();
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getCapabilities() {
        return this.capabilities;
    }

    @Override
    public List<String> getRequirements() {
        if (this.addonMetadata != null) {
            return this.addonMetadata.getRequirements();
        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long getAddonId() {
        return this.addonId;
    }

    /**
     * @param addonId the addon id to set
     */
    public void setAddonId(final Long addonId) {
        if (this.addonId == null && addonId != null) {
            this.addonId = addonId;
        }
    }

    /**
     * {@inheritDoc}
     */
    public List<ISortableDeployable> getKnownDeployables() {
        List<ISortableDeployable> knownDeployables = new ArrayList<ISortableDeployable>();
        for (ISortableDeployable sortableDeployable: this.deployables) {
            if (!UnknownDeployable.class.isInstance(sortableDeployable.getDeployable())) {
                knownDeployables.add(sortableDeployable);
            }
        }
        return knownDeployables;
    }

    /**
     * {@inheritDoc}
     */
    public List<ISortableDeployable> getUnknownDeployables() {
        List<ISortableDeployable> unknownDeployables = new ArrayList<ISortableDeployable>();
        for (ISortableDeployable sortableDeployable: this.deployables) {
            if (UnknownDeployable.class.isInstance(sortableDeployable.getDeployable())) {
                unknownDeployables.add(sortableDeployable);
            }
        }
        return unknownDeployables;
    }

    /**
     * {@inheritDoc}
     */
    public IAddonMetadata getMetadata() {
        return this.addonMetadata;
    }

    /**
     * {@inheritDoc}
     */
    public void setMetadata(final IAddonMetadata addonMetadata) {
        this.addonMetadata = addonMetadata;
    }

    /**
     * {@inheritDoc}
     */
    public IAddonDeployable getParent() {
        return parent;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public File getConfigurationDirectory() {
        return this.configurationDirectory;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getConfigurationFileNames() {
        return this.configurationFileNames;
    }

    /**
     * @param configurationDirectory The configuration directory to set
     */
    public void setConfigurationDirectory(final File configurationDirectory) {
        if (configurationDirectory != null && configurationDirectory.exists()) {
            this.configurationDirectory = configurationDirectory;
            this.configurationFileNames.clear();

            File files[] = this.configurationDirectory.listFiles();
            if (files != null) {
                for (File file: files) {
                    this.configurationFileNames.add(file.getName());
                }
            }
        }
    }

    /**
     * @param parent The {@link IAddonDeployable} to set as a parent
     */
    public void setParent(final IAddonDeployable parent) {
        this.parent = parent;
    }
}
