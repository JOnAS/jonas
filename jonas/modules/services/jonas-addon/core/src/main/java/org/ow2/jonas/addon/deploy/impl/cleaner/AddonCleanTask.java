/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.addon.deploy.impl.cleaner;

import org.ow2.jonas.addon.deploy.api.deployer.IAddonDeployer;
import org.ow2.jonas.lib.work.AbsCleanTask;
import org.ow2.jonas.workcleaner.IDeployerLog;
import org.ow2.jonas.workcleaner.LogEntry;
import org.ow2.jonas.workcleaner.WorkCleanerException;

/**
 * JOnAS Addon clean task class.
 * @author Jeremy Cazaux
 */
public class AddonCleanTask extends AbsCleanTask {

    /**
     * The addon deployer
     */
    private IAddonDeployer addonDeployer;

    /**
     * The deployer log
     */
    private IDeployerLog deployerLog;

    /**
     * Default constructor
     * @param addonDeployer The addon deployer
     * @param deployerLog The deployer log
     */
    public AddonCleanTask(final IAddonDeployer addonDeployer, final IDeployerLog deployerLog) {
        this.addonDeployer = addonDeployer;
        this.deployerLog = deployerLog;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isDeployedLogEntry(final LogEntry logEntry) throws WorkCleanerException {
        return this.addonDeployer.isAddonDeployedByWorkName(logEntry.getCopy().getName());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IDeployerLog getDeployerLog() {
        return this.deployerLog;
    }
}
