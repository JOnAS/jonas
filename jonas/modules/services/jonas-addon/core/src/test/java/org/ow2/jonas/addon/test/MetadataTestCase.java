/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2013 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.addon.test;

import junit.framework.Assert;
import org.ow2.jonas.Version;
import org.ow2.jonas.addon.deploy.api.deployable.IAddonDeployable;
import org.ow2.jonas.addon.deploy.api.deployable.IAddonMetadata;
import org.ow2.jonas.addon.deploy.impl.util.AddonUtil;
import org.ow2.jonas.properties.ServiceProperties;
import org.ow2.util.archive.api.IArchive;
import org.ow2.util.archive.impl.ArchiveManager;
import org.ow2.util.ee.deploy.api.deployable.IDeployable;
import org.ow2.util.ee.deploy.api.deployer.DeployerException;
import org.testng.annotations.Test;

import java.io.File;

/**
 * Addon metadata test case
 * @author Jeremy Cazaux
 */
public class MetadataTestCase extends BaseTestCase {

    public static final File ADDON_METADATA_DIR = new File(ADDON_DIR, "metadata");
    public static final File ADDON_METADATA_PROPERTIES_DIR = new File(ADDON_METADATA_DIR, "properties");
    public static final File ADDON_METADATA_WRONG_DIR = new File(ADDON_METADATA_DIR, "wrong");
    public static final File ADDON_METADATA_PROPERTIES_EMPTY = new File(ADDON_METADATA_PROPERTIES_DIR, "empty");
    public static final File ADDON_METADATA_PROPERTIES_NOT_DEFINED = new File(ADDON_METADATA_PROPERTIES_DIR, "notdefined");
    public static final File ADDON_METADATA_WRONG_AUTOSTART = new File(ADDON_METADATA_WRONG_DIR, "autostart");
    public static final File ADDON_METADATA_WRONG_NAME = new File(ADDON_METADATA_WRONG_DIR, "name");
    public static final File ADDON_METADATA_WRONG_JONAS_VERSION = new File(ADDON_METADATA_WRONG_DIR, "jonasVersion");
    public static final File ADDON_METADATA_WRONG_JVM_VERSION = new File(ADDON_METADATA_WRONG_DIR, "jvmVersion");

    public static final String SAMPLE_DESCRIPTION = "Sample Addon";
    public static final String SAMPLE_TENNANT_ID = "10";
    public static final String SAMPLE_INSTANCE = "myInstance";
    public static final String SAMPLE_AUTHOR = "The JOnAS Team";
    public static final String SAMPLE_LICENCE = "LGPL";
    public static final Boolean SAMPLE_AUTOSTART = true;
    public static final String SAMPLE_JVM_VERSION_MIN = "1.0.0";
    public static final String SAMPLE_JVM_VERSION_MEDIUM = "1.5.0";
    public static final String SAMPLE_JVM_VERSION_MAX = "2.0.0";
    public static final int SAMPLE_NB_PROVIDES = 1;
    public static final String SAMPLE_PROVIDES = "jonas.service.A";
    public static final int SAMPLE_NB_REQUIREMENTS = 0;
    public static final int SAMPLE_PROP_NB = 3;
    public static final String SAMPLE_PROP1_KEY = "jonas.service.A.class";
    public static final String SAMPLE_PROP1_VAL = "myClass";
    public static final String SAMPLE_PROP2_KEY = "jonas.service.A.prop1";
    public static final String SAMPLE_PROP2_VAL = "val1";
    public static final String SAMPLE_PROP3_KEY = "jonas.service.A.prop2";
    public static final String SAMPLE_PROP3_VAL = "val2";
    public static final String SAMPLE_SERVICE = "A";

    @Test
    public void checkThatMetadataRetrievedByTheDeployerAreOk() throws Exception {
        //build the archive
        IArchive archive = ArchiveManager.getInstance().getArchive(BasicTestCase.SAMPLE_ADDON);

        //Gets the Deployable object for the IArchive object
        IDeployable<?> deployable = this.deployableHelper.getDeployable(archive);

        //cast the deployable into IAddonDeployable object
        IAddonDeployable addon = IAddonDeployable.class.cast(deployable);

        //install the addon
        IAddonDeployable installed = IAddonDeployable.class.cast(deployer.install(addon, false));

        //check metadata (in memory)
        IAddonMetadata metadata = installed.getMetadata();
        Assert.assertEquals(metadata.getName(), BasicTestCase.SAMPLE_NAME);
        Assert.assertEquals(metadata.getAutostart(), SAMPLE_AUTOSTART);
        Assert.assertEquals(metadata.getAuthor(), SAMPLE_AUTHOR);
        Assert.assertEquals(metadata.getDescription(), SAMPLE_DESCRIPTION);
        Assert.assertEquals(metadata.getImplementation(), null);
        Assert.assertEquals(metadata.getInstance(), SAMPLE_INSTANCE);
        Assert.assertEquals(metadata.getLicence(), SAMPLE_LICENCE);
        Assert.assertEquals(metadata.getProvides().size(), SAMPLE_NB_PROVIDES);
        Assert.assertEquals(metadata.getProvides().get(0), SAMPLE_PROVIDES);
        Assert.assertEquals(metadata.getRequirements().size(), SAMPLE_NB_REQUIREMENTS);
        Assert.assertEquals(metadata.getService(), SAMPLE_SERVICE);
        Assert.assertEquals(metadata.getTenantId(), SAMPLE_TENNANT_ID);
        Assert.assertEquals(metadata.isJOnASService(), true);
        Assert.assertTrue(metadata.isJOnASVersionSupported(Version.getNumber()));
        Assert.assertTrue(metadata.isJvmVersionSupported(SAMPLE_JVM_VERSION_MEDIUM));

        Assert.assertTrue(metadata.isJvmVersionSupported(SAMPLE_JVM_VERSION_MIN));
        Assert.assertTrue(metadata.isJvmVersionSupported(SAMPLE_JVM_VERSION_MAX));
        ServiceProperties serviceProperties = metadata.getServiceProperties();
        Assert.assertEquals(serviceProperties.getProperties().size(), SAMPLE_PROP_NB);
        Assert.assertEquals(serviceProperties.getValue(SAMPLE_PROP1_KEY), SAMPLE_PROP1_VAL);
        Assert.assertEquals(serviceProperties.getValue(SAMPLE_PROP2_KEY), SAMPLE_PROP2_VAL);
        Assert.assertEquals(serviceProperties.getValue(SAMPLE_PROP3_KEY), SAMPLE_PROP3_VAL);

        //check metadata from the configuration file
        File metadataFile = new File(AddonUtil.getAddonDirectoryPath(BasicTestCase.SAMPLE_NAME), IAddonDeployable.METADATA_FILENAME);
        metadata = AddonUtil.getAddonMetadata(metadataFile, null);
        Assert.assertEquals(metadata.getName(), BasicTestCase.SAMPLE_NAME);
        Assert.assertEquals(metadata.getAutostart(), SAMPLE_AUTOSTART);
        Assert.assertEquals(metadata.getAuthor(), SAMPLE_AUTHOR);
        Assert.assertEquals(metadata.getDescription(), SAMPLE_DESCRIPTION);
        Assert.assertEquals(metadata.getImplementation(), null);
        Assert.assertEquals(metadata.getInstance(), SAMPLE_INSTANCE);
        Assert.assertEquals(metadata.getLicence(), SAMPLE_LICENCE);
        Assert.assertEquals(metadata.getProvides().size(), SAMPLE_NB_PROVIDES);
        Assert.assertEquals(metadata.getProvides().get(0), SAMPLE_PROVIDES);
        Assert.assertEquals(metadata.getRequirements().size(), SAMPLE_NB_REQUIREMENTS);
        Assert.assertEquals(metadata.getService(), SAMPLE_SERVICE);
        Assert.assertEquals(metadata.getTenantId(), SAMPLE_TENNANT_ID);
        Assert.assertTrue(metadata.isJOnASVersionSupported(Version.getNumber()));
        Assert.assertTrue(metadata.isJvmVersionSupported(SAMPLE_JVM_VERSION_MAX));
        Assert.assertTrue(metadata.isJvmVersionSupported(SAMPLE_JVM_VERSION_MEDIUM));
        Assert.assertTrue(metadata.isJvmVersionSupported(SAMPLE_JVM_VERSION_MIN));
        serviceProperties = metadata.getServiceProperties();
        Assert.assertEquals(serviceProperties.getProperties().size(), SAMPLE_PROP_NB);
        Assert.assertEquals(serviceProperties.getValue(SAMPLE_PROP1_KEY), SAMPLE_PROP1_VAL);
        Assert.assertEquals(serviceProperties.getValue(SAMPLE_PROP2_KEY), SAMPLE_PROP2_VAL);
        Assert.assertEquals(serviceProperties.getValue(SAMPLE_PROP3_KEY), SAMPLE_PROP3_VAL);
    }

    @Test(expectedExceptions = DeployerException.class)
    public void testThatWeCannotDeployTwoAddonsWithTheSameName() throws Exception {
        IArchive archive = ArchiveManager.getInstance().getArchive(BasicTestCase.SAMPLE_ADDON);
        IDeployable<?> deployable = this.deployableHelper.getDeployable(archive);
        IAddonDeployable addon = IAddonDeployable.class.cast(deployable);
        this.deployer.install(addon, false);
        this.deployer.install(addon, false);
    }

    @Test(expectedExceptions =  DeployerException.class)
    public void testThatWeCannotInstallAnAddonWithAnInvalidJOnASVersion() throws Exception {
        this.deployer.install(getDeployable(ADDON_METADATA_WRONG_JONAS_VERSION), false);
    }

    @Test(expectedExceptions =  DeployerException.class)
    public void testThatWeCannotInstallAnAddonWithAnInvalidJvmVersion() throws Exception {
        this.deployer.install(getDeployable(ADDON_METADATA_WRONG_JVM_VERSION), false);
    }

    @Test(expectedExceptions =  DeployerException.class)
    public void testThatWeCannotInstallAnAddonWithAnEmptyName() throws Exception {
        this.deployer.install(getDeployable(ADDON_METADATA_WRONG_NAME), false);
    }

    @Test(expectedExceptions =  DeployerException.class)
    public void testThatWeCannotInstallAnAddonWithAnInvalidAutostartValue() throws Exception {
        this.deployer.install(getDeployable(ADDON_METADATA_WRONG_AUTOSTART), false);
    }

    @Test
    public void testThatWeCanInstallAndUninstallAnAddonWithNoPropertiesDefined() throws Exception {
        this.deployer.install(getDeployable(ADDON_METADATA_PROPERTIES_NOT_DEFINED), false);
    }

    @Test
    public void testThatWeCanInstallAndUninstallAnAddonWithEmptyProperties() throws Exception {
        this.deployer.install(getDeployable(ADDON_METADATA_PROPERTIES_EMPTY), false);
    }
}
