/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2013 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.addon.test;

import junit.framework.Assert;
import org.ow2.jonas.addon.deploy.impl.deployable.AddonMetaData;
import org.testng.annotations.Test;

/**
 * Range test case
 * @author Jeremy Cazaux
 */
public class RangeTestCase {

    @Test
    public void rangeTestCase1() throws Exception {
        Assert.assertTrue(AddonMetaData.isVersionSupported("[1.0.0,2.0.0]", "1.5.0"));
    }

    @Test
    public void rangeTestCase2() throws Exception {
        Assert.assertTrue(AddonMetaData.isVersionSupported("[1.0.0,2.0.0]", "1.0.5"));
    }

    @Test
    public void rangeTestCase3() throws Exception {
        Assert.assertTrue(AddonMetaData.isVersionSupported("[1.0.0,2.0.0]", "1.0.4-SNAPSHOT-1"));
    }

    @Test
    public void rangeTestCase4() throws Exception {
        Assert.assertTrue(AddonMetaData.isVersionSupported("[1.0.0,2.0.0]", "1.0.4-RC1"));
    }

    @Test
    public void rangeTestCase5() throws Exception {
        Assert.assertFalse(AddonMetaData.isVersionSupported("[1.0.0,2.0.0]", "1.0.0"));
    }

    @Test
    public void rangeTestCase6() throws Exception {
        Assert.assertFalse(AddonMetaData.isVersionSupported("[1.0.0,2.0.0]", "2.0.0"));
    }

    @Test
    public void rangeTestCase7() throws Exception {
        Assert.assertTrue(AddonMetaData.isVersionSupported("(1.0.0,2.0.0)", "1.0.0"));
    }

    @Test
    public void rangeTestCase8() throws Exception {
        Assert.assertTrue(AddonMetaData.isVersionSupported("(1.0.0,2.0.0)", "2.0.0"));
    }

    @Test
    public void rangeTestCase9() throws Exception {
        Assert.assertTrue(AddonMetaData.isVersionSupported("1.0.0", "1"));
    }

    @Test
    public void rangeTestCase10() throws Exception {
        Assert.assertTrue(AddonMetaData.isVersionSupported("1.0.0", "1.0"));
    }

    @Test
    public void rangeTestCase11() throws Exception {
        Assert.assertFalse(AddonMetaData.isVersionSupported("1.0.0", "1.0.0-SNAPSHOT"));
    }

    @Test(expectedExceptions = Exception.class)
    public void rangeTestCase12() throws Exception {
        AddonMetaData.isVersionSupported("1.0.0", "1.0.SNAPSHOT");
    }

    @Test(expectedExceptions = Exception.class)
    public void rangeTestCase13() throws Exception {
        AddonMetaData.isVersionSupported("1.0.0", "1.SNAPSHOT");
    }

    @Test(expectedExceptions = Exception.class)
    public void rangeTestCase14() throws Exception {
        AddonMetaData.isVersionSupported("1.0.0", "1-SNAPSHOT");
    }

    @Test(expectedExceptions = Exception.class)
    public void rangeTestCase15() throws Exception {
        AddonMetaData.isVersionSupported("1.0.0", "1-0-0");
    }

    @Test
    public void rangeTestCase16() throws Exception {
        Assert.assertFalse(AddonMetaData.isVersionSupported("1.0.0", "1.0.0-SNAPSHOT-1"));
    }

    @Test
    public void rangeTestCase17() throws Exception {
        Assert.assertTrue(AddonMetaData.isVersionSupported("(1.0.0-M1-SNAPSHOT,1.0.0-M3-SNAPSHOT)", "1.0.0-M2-SNAPSHOT"));
    }

    @Test
    public void rangeTestCase18() throws Exception {
        Assert.assertFalse(AddonMetaData.isVersionSupported("(1.0.0-M1,1.0.0-M3-SNAPSHOT)", "1.0.0"));
    }

    @Test
    public void rangeTestCase19() throws Exception {
        Assert.assertFalse(AddonMetaData.isVersionSupported("(1.0.0-M1,1.0.0-M3-SNAPSHOT)", "1.0.0-RC1"));
    }

    //TODO warning: the following test cases are not working due to the OSGi Version implementation

    //@Test
    public void rangeTestCase2O() throws Exception {
        Assert.assertTrue(AddonMetaData.isVersionSupported("(1.0.0,2.0.0)", "2.0.0-RC1"));
    }

    //@Test
    public void rangeTestCase21() throws Exception {
        Assert.assertFalse(AddonMetaData.isVersionSupported("(1.0.0,2.0.0)", "1.0.0-M1-SNAPSHOT"));
    }

    //@Test
    public void rangeTestCase22() throws Exception {
        Assert.assertTrue(AddonMetaData.isVersionSupported("(1.0.0-SNAPSHOT,2.0.0-SNAPSHOT)", "1.0.0-RC1"));
    }
}
