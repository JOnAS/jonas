/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2013 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.addon.test;

import junit.framework.Assert;
import org.ow2.jonas.addon.deploy.api.deployable.IAddonDeployable;
import org.ow2.jonas.addon.deploy.impl.util.AddonUtil;
import org.ow2.util.archive.api.IArchive;
import org.ow2.util.archive.impl.ArchiveManager;
import org.ow2.util.ee.deploy.api.deployable.IDeployable;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

/**
 * Conf test cases
 * @author Jeremy Cazaux
 */
public class ConfTestCase extends BaseTestCase {

    public static final String SAMPLE_CONF_FILENAME = "sample.conf";
    public static final int SAMPLE_CONF_FILE_NB_PROP = 1;
    public static final String SAMPLE_CONF_FILE_PROP1_KEY = "sample.conf";
    public static final String SAMPLE_CONF_FILE_PROP1_VALUE = "ok";

    @Test
    public void testThatTheInstallationOfConfigurationFilesIsOK() throws Exception {
        //install the addon
        IArchive archive = ArchiveManager.getInstance().getArchive(BasicTestCase.SAMPLE_ADDON);
        IDeployable<?> deployable = this.deployableHelper.getDeployable(archive);
        IAddonDeployable addon = IAddonDeployable.class.cast(deployable);
        IAddonDeployable.class.cast(this.deployer.install(addon, false));

        //check conf dir
        File addonConfDir = new File(AddonUtil.getAddonDirectoryPath(BasicTestCase.SAMPLE_NAME));
        Assert.assertTrue(addonConfDir.exists());
        Assert.assertTrue(addonConfDir.isDirectory());

        //check configuration files
        File sampleConfFile = new File(addonConfDir, SAMPLE_CONF_FILENAME);
        File metadataFile = new File(addonConfDir, IAddonDeployable.METADATA_FILENAME);
        Assert.assertTrue(metadataFile.exists());
        Assert.assertFalse(metadataFile.isDirectory());
        Properties properties = new Properties();
        properties.load(new FileInputStream(sampleConfFile));
        Assert.assertEquals(properties.size(), SAMPLE_CONF_FILE_NB_PROP);
        Assert.assertEquals(properties.getProperty(SAMPLE_CONF_FILE_PROP1_KEY), SAMPLE_CONF_FILE_PROP1_VALUE);

        //uninstall the addon
        this.deployer.uninstall(BasicTestCase.SAMPLE_NAME);

        //check configuration files
        Assert.assertFalse(sampleConfFile.exists());
    }
}
