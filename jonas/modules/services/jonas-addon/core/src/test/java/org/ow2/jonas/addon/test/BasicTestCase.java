/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2013 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.addon.test;

import junit.framework.Assert;
import org.ow2.jonas.addon.deploy.api.deployable.IAddonDeployable;
import org.ow2.util.archive.api.IArchive;
import org.ow2.util.archive.impl.ArchiveManager;
import org.ow2.util.ee.deploy.api.deployable.IDeployable;
import org.ow2.util.plan.bindings.repository.Repository;
import org.testng.annotations.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Sample test case
 * @author Jeremy Cazaux
 */
public class BasicTestCase extends BaseTestCase {

    public static final File SAMPLE_ADDON = new File(ADDON_DIR, "sample");
    public static final File SAMPLE_ADDON_JAR = new File(ADDON_DIR, "sample.jar");
    public static final File SAMPLE_ADDON_ZIP = new File(ADDON_DIR, "sample.zip");

    public static final String SAMPLE_NAME = "Sample";
    public static final String SAMPLE_CAPABILITY = "jonas.service.A";

    @Test
    public void testThatAnAddonDeployableIsSupportByTheAddonDeployer() throws Exception {
        IArchive archive = ArchiveManager.getInstance().getArchive(SAMPLE_ADDON);
        IDeployable<?> deployable = this.deployableHelper.getDeployable(archive);
        Assert.assertTrue(deployer.supports(deployable));
    }

    @Test
    public void testBasicDeploymentOperationsWithAnAddonDirDeployable() throws Exception {
        doBasicDeploymentOperations(SAMPLE_ADDON, SAMPLE_NAME);
    }

    @Test
    public void testBasicDeploymentOperationsWithAnAddonZipDeployable() throws Exception {
        doBasicDeploymentOperations(SAMPLE_ADDON_ZIP, SAMPLE_NAME);
    }

    @Test
    public void testBasicDeploymentOperationsWithAnAddonJarDeployable() throws Exception {
        doBasicDeploymentOperations(SAMPLE_ADDON_JAR, SAMPLE_NAME);
    }

    @Test
    public void testBasicDeploymentOperations2WithAnAddonDirDeployable() throws Exception {
        doBasicDeploymentOperations2(SAMPLE_ADDON, SAMPLE_NAME);
    }

    @Test
    public void testBasicDeploymentOperations2WithAnAddonZipDeployable() throws Exception {
        doBasicDeploymentOperations2(SAMPLE_ADDON_ZIP, SAMPLE_NAME);
    }

    @Test
    public void testBasicDeploymentOperations2WithAnAddonJarDeployable() throws Exception {
        doBasicDeploymentOperations2(SAMPLE_ADDON_JAR, SAMPLE_NAME);
    }

    @Test
    public void testBasicDeploymentOperations3WithAnAddonDirDeployable() throws Exception {
        doBasicDeploymentOperations3(SAMPLE_ADDON, SAMPLE_NAME);
    }

    @Test
    public void testBasicDeploymentOperations3WithAnAddonZipDeployable() throws Exception {
        doBasicDeploymentOperations3(SAMPLE_ADDON_ZIP, SAMPLE_NAME);
    }

    @Test
    public void testBasicDeploymentOperations3WithAnAddonJarDeployable() throws Exception {
        doBasicDeploymentOperations3(SAMPLE_ADDON_JAR, SAMPLE_NAME);
    }

    @Test
    public void testThatTheRepositoryHasBeenAddedWhenTheAddonIsInstalledAndTestThatItHasBeenStoppedWhenTheAddonIsUninstalled() throws Exception {
        //get the addon deployable
        IArchive archive = ArchiveManager.getInstance().getArchive(SAMPLE_ADDON);
        IDeployable<?> deployable = this.deployableHelper.getDeployable(archive);
        IAddonDeployable addon = IAddonDeployable.class.cast(deployable);

        //install the addon
        this.deployer.install(addon, false);

        //check that the repository has been added
        verify(this.repositoryManager, times(1)).addRepository(any(Repository.class));

        //uninstall the addon
        this.deployer.uninstall(SAMPLE_NAME);

        //check that the repository has been removed
        verify(this.repositoryManager, times(1)).removeRepository(any(Repository.class));


    }

    private void doBasicDeploymentOperations(final File file, final String addonName) throws Exception {
        //build the archive
        IArchive archive = ArchiveManager.getInstance().getArchive(file);

        //Gets the Deployable object for the IArchive object
        IDeployable<?> deployable = this.deployableHelper.getDeployable(archive);

        //cast the deployable into IAddonDeployable object
        IAddonDeployable addon = IAddonDeployable.class.cast(deployable);

        //install the addon
        this.deployer.install(addon, false);

        //start the addon
        this.deployer.start(addonName);

        //stop the addon
        this.deployer.stop(addonName);

        //Uninstall the addon
        this.deployer.uninstall(addonName);
    }

    private void doBasicDeploymentOperations2(final File file, final String addonName) throws Exception {
        //build the archive
        IArchive archive = ArchiveManager.getInstance().getArchive(file);

        //Gets the Deployable object for the IArchive object
        IDeployable<?> deployable = this.deployableHelper.getDeployable(archive);

        //cast the deployable into IAddonDeployable object
        IAddonDeployable addon = IAddonDeployable.class.cast(deployable);

        //deploy the addon
        this.deployer.deploy(addon);

        //undeploy the addon
        this.deployer.undeploy(addon);
    }

    private void doBasicDeploymentOperations3(final File file, final String addonName) throws Exception {
        //build the archive
        IArchive archive = ArchiveManager.getInstance().getArchive(file);

        //Gets the Deployable object for the IArchive object
        IDeployable<?> deployable = this.deployableHelper.getDeployable(archive);

        //cast the deployable into IAddonDeployable object
        IAddonDeployable addon = IAddonDeployable.class.cast(deployable);

        List<IDeployable<IAddonDeployable>> deployables = new ArrayList<IDeployable<IAddonDeployable>>();
        deployables.add(addon);

        //deploy the addon
        this.deployer.deploy(deployables);

        //undeploy the addon
        this.deployer.undeploy(deployables);
    }
}
