/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2013 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.addon.test;

import junit.framework.Assert;
import org.ow2.jonas.addon.deploy.api.deployable.IAddonDeployable;
import org.ow2.util.archive.api.IArchive;
import org.ow2.util.archive.impl.ArchiveManager;
import org.ow2.util.ee.deploy.api.deployable.IDeployable;
import org.testng.annotations.Test;

import java.io.File;

/**
 * Deployable Analyzer test cases
 * @author Jeremy Cazaux
 */
public class DeployableAnalyzerTestCase extends BaseTestCase {

    public static final File NOT_ADDON_DIR = new File(ADDON_DIR, "notaddon");
    public static final File INVALID_A = new File(NOT_ADDON_DIR, "A");
    public static final File INVALID_B = new File(NOT_ADDON_DIR, "B");
    public static final File INVALID_C = new File(NOT_ADDON_DIR, "C");

    @Test
    public void testThatAnAddonDirIsRecognizedAsAnAddonDeployable() throws Exception {
        IArchive archive = ArchiveManager.getInstance().getArchive(BasicTestCase.SAMPLE_ADDON);
        IDeployable<?> deployable = this.deployableHelper.getDeployable(archive);
        Assert.assertTrue(IAddonDeployable.class.isInstance(deployable));
    }

    @Test
    public void testThatAnAddonJarIsRecognizedAsAnAddonDeployable() throws Exception {
        IArchive archive = ArchiveManager.getInstance().getArchive(BasicTestCase.SAMPLE_ADDON_JAR);
        IDeployable<?> deployable = this.deployableHelper.getDeployable(archive);
        Assert.assertTrue(IAddonDeployable.class.isInstance(deployable));
    }

    @Test
    public void testThatAnAddonZipIsRecognizedAsAnAddonDeployable() throws Exception {
        IArchive archive = ArchiveManager.getInstance().getArchive(BasicTestCase.SAMPLE_ADDON_ZIP);
        IDeployable<?> deployable = this.deployableHelper.getDeployable(archive);
        Assert.assertTrue(IAddonDeployable.class.isInstance(deployable));
    }

    @Test
    public void testThatADirectoryWithAnInvalidStructure1IsNotRecognizedAsAnAddon() throws Exception {
        IArchive archive = ArchiveManager.getInstance().getArchive(INVALID_A);
        IDeployable<?> deployable = this.deployableHelper.getDeployable(archive);
        Assert.assertFalse(IAddonDeployable.class.isInstance(deployable));
    }

    @Test
    public void testThatADirectoryWithAnInvalidStructure2IsNotRecognizedAsAnAddon() throws Exception {
        IArchive archive = ArchiveManager.getInstance().getArchive(INVALID_B);
        IDeployable<?> deployable = this.deployableHelper.getDeployable(archive);
        Assert.assertFalse(IAddonDeployable.class.isInstance(deployable));
    }

    @Test
    public void testThatADirectoryWithAnInvalidMetadataFilenameIsNotRecognizedAsAnAddon() throws Exception {
        IArchive archive = ArchiveManager.getInstance().getArchive(INVALID_C);
        IDeployable<?> deployable = this.deployableHelper.getDeployable(archive);
        Assert.assertFalse(IAddonDeployable.class.isInstance(deployable));
    }
}
