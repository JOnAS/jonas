/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2013 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.addon.test;

import junit.framework.Assert;
import org.ow2.jonas.addon.deploy.api.deployable.IAddonDeployable;
import org.ow2.jonas.addon.deploy.api.deployer.IAddonDeployer;
import org.ow2.util.ee.deploy.api.deployer.DeployerException;
import org.testng.annotations.Test;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.spy;

/**
 * Dynamicity test case
 * @author Jeremy Cazaux
 */
public class DynamicityTestCase extends BaseTestCase {

    @Test
    public void testThatTheStateOfAnAddonWithAnInstalledStateAndAMissingRequirementWillMoveAutomaticallyOnTheResolvedStateWhenAnotherAddonWillProvideThisCapability() throws Exception {
        IAddonDeployable addon1 = getDeployable(ResolverTestCase.ADDON_1);
        IAddonDeployable addon2 = getDeployable(ResolverTestCase.ADDON_2);

        IAddonDeployer deployer = getAddonDeployerInstance();
        IAddonDeployable installed1 = IAddonDeployable.class.cast(deployer.install(addon1, false));
        IAddonDeployable installed2 = IAddonDeployable.class.cast(deployer.install(addon2, false));

        Assert.assertEquals(installed1.getState(), IAddonDeployable.INSTALLED);
        Assert.assertEquals(installed2.getState(), IAddonDeployable.INSTALLED);

        try {
            deployer.start(ResolverTestCase.ADDON_2_NAME);
        } catch (DeployerException e) {
            //do nothing
        }

        Assert.assertEquals(installed2.getState(), IAddonDeployable.INSTALLED);

        deployer.start(ResolverTestCase.ADDON_1_NAME);

        Assert.assertEquals(installed1.getState(), IAddonDeployable.ACTIVE);
        Assert.assertEquals(installed2.getState(), IAddonDeployable.RESOLVED);
    }

    @Test
    public void testThatTheActiveOrResolvedStateOfAnAddonWillMoveOnInstalledIfARequirementIsNoMoreSatisfied() throws Exception {
        IAddonDeployable addon1 = getDeployable(ResolverTestCase.ADDON_1);
        IAddonDeployable addon2 = getDeployable(ResolverTestCase.ADDON_2);

        IAddonDeployer deployer = getAddonDeployerInstance();
        IAddonDeployable installed1 = IAddonDeployable.class.cast(deployer.install(addon1, false));
        IAddonDeployable installed2 = IAddonDeployable.class.cast(deployer.install(addon2, false));
        Assert.assertEquals(installed1.getState(), IAddonDeployable.INSTALLED);
        Assert.assertEquals(installed2.getState(), IAddonDeployable.INSTALLED);

        deployer.start(ResolverTestCase.ADDON_1_NAME);
        Assert.assertEquals(installed1.getState(), IAddonDeployable.ACTIVE);
        Assert.assertEquals(installed2.getState(), IAddonDeployable.RESOLVED);

        deployer.start(ResolverTestCase.ADDON_2_NAME);
        Assert.assertEquals(installed1.getState(), IAddonDeployable.ACTIVE);
        Assert.assertEquals(installed2.getState(), IAddonDeployable.ACTIVE);

        deployer.stop(ResolverTestCase.ADDON_1_NAME);
        Assert.assertEquals(installed1.getState(), IAddonDeployable.RESOLVED);
        Assert.assertEquals(installed2.getState(), IAddonDeployable.INSTALLED);
    }

    @Test
    public void testThatTheActiveOrResolvedStateOfAnAddonWillMoveOnInstalledIfARequirementIsNoMoreSatisfied2() throws Exception {
        IAddonDeployable addon1 = getDeployable(ResolverTestCase.ADDON_1);
        IAddonDeployable addon2 = getDeployable(ResolverTestCase.ADDON_2);

        IAddonDeployer deployer = spy(getAddonDeployerInstance());
        IAddonDeployable installed1 = IAddonDeployable.class.cast(deployer.install(addon1, false));
        IAddonDeployable installed2 = IAddonDeployable.class.cast(deployer.install(addon2, false));
        Assert.assertEquals(installed1.getState(), IAddonDeployable.INSTALLED);
        Assert.assertEquals(installed2.getState(), IAddonDeployable.INSTALLED);

        deployer.start(ResolverTestCase.ADDON_1_NAME);
        Assert.assertEquals(installed1.getState(), IAddonDeployable.ACTIVE);
        Assert.assertEquals(installed2.getState(), IAddonDeployable.RESOLVED);

        deployer.start(ResolverTestCase.ADDON_2_NAME);
        Assert.assertEquals(installed1.getState(), IAddonDeployable.ACTIVE);
        Assert.assertEquals(installed2.getState(), IAddonDeployable.ACTIVE);

        doThrow(DeployerException.class).when(deployer).stop(ResolverTestCase.ADDON_2_NAME);

        deployer.stop(ResolverTestCase.ADDON_1_NAME);
        Assert.assertEquals(installed1.getState(), IAddonDeployable.RESOLVED);
        Assert.assertEquals(installed2.getState(), IAddonDeployable.INSTALLED);
    }
}
