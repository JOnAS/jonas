/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2013 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.addon.test;

import org.ow2.util.ee.deploy.api.deployer.DeployerException;
import org.testng.annotations.Test;

import java.io.File;

/**
 * Resolver test case
 * @author Jeremy Cazaux
 */
public class ResolverTestCase extends BaseTestCase {

    ///TODO test with JAR / ZIP

    public static final File ADDON_RESOLVER_DIR = new File(ADDON_DIR, "resolver");
    public static final String ADDON_1_NAME = "1";
    public static final String ADDON_2_NAME = "2";
    public static final String ADDON_3_NAME = "3";
    public static final String ADDON_4_NAME = "4";
    public static final String ADDON_6_NAME = "6";
    public static final String ADDON_7_NAME = "7";
    public static final String ADDON_8_NAME = "8";
    public static final String ADDON_9_NAME = "9";
    public static final String ADDON_11_NAME = "11";
    public static final String ADDON_18_NAME = "18";
    public static final File ADDON_1 = new File(ADDON_RESOLVER_DIR, ADDON_1_NAME);
    public static final File ADDON_2 = new File(ADDON_RESOLVER_DIR, "2");
    public static final File ADDON_3 = new File(ADDON_RESOLVER_DIR, "3");
    public static final File ADDON_4 = new File(ADDON_RESOLVER_DIR, "4");
    public static final File ADDON_5 = new File(ADDON_RESOLVER_DIR, "5");
    public static final File ADDON_6 = new File(ADDON_RESOLVER_DIR, "6");
    public static final File ADDON_8 = new File(ADDON_RESOLVER_DIR, "8");
    public static final File ADDON_14 = new File(ADDON_RESOLVER_DIR, "14");
    public static final File ADDON_16 = new File(ADDON_RESOLVER_DIR, "16");
    public static final File ADDON_18 = new File(ADDON_RESOLVER_DIR, "18");

    public static final String ADDON_1_CAPABILITY = "A";
    public static final String ADDON_2_CAPABILITY = "B";
    public static final String ADDON_4_CAPABILITY = "D";
    public static final String ADDON_6_CAPABILITY = "F";
    public static final String ADDON_7_CAPABILITY = "E";

    @Test
    public void testRequirement1() throws Exception {
        this.deployer.deploy(getDeployable(ADDON_1));
        this.deployer.deploy(getDeployable(ADDON_2));
    }

    @Test
    public void testRequirement2() throws Exception {
        this.deployer.deploy(getDeployable(ADDON_1));
        this.deployer.deploy(getDeployable(ADDON_2));
        this.deployer.deploy(getDeployable(ADDON_3));
    }

    @Test(expectedExceptions = DeployerException.class)
    public void testRequirement3() throws Exception {
        this.deployer.deploy(getDeployable(ADDON_1));
        this.deployer.deploy(getDeployable(ADDON_2));
        this.deployer.deploy(getDeployable(ADDON_4));
        this.deployer.deploy(getDeployable(ADDON_5));
    }

    @Test
    public void testRequirement4() throws Exception {
        this.deployer.deploy(getDeployable(ADDON_6));
    }

    @Test
    public void testRequirement5() throws Exception {
        this.deployer.deploy(getDeployable(ADDON_8));
    }

    @Test(expectedExceptions = DeployerException.class)
    public void testRequirement7() throws Exception {
        this.deployer.deploy(getDeployable(ADDON_14));
    }

    @Test(expectedExceptions = DeployerException.class)
    public void testRequirement8() throws Exception {
        this.deployer.deploy(getDeployable(ADDON_16));
    }
}
