/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 France Telecom R&D
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.jndi.interceptors.impl.easybeans;

import org.ow2.easybeans.api.EZBContainerConfig;
import org.ow2.easybeans.api.EZBContainerLifeCycleCallback;
import org.ow2.easybeans.api.LifeCycleCallbackException;
import org.ow2.easybeans.api.EZBContainerCallbackInfo;
import org.ow2.easybeans.api.binding.EZBRef;
import org.ow2.jonas.jndi.checker.api.IResourceCheckerManager;

/**
 * Allows to register some extensions
 * @author Florent Benoit
 */
public class ResourceCheckerConfigurationExtension implements org.ow2.easybeans.api.EZBConfigurationExtension, EZBContainerLifeCycleCallback {

    /**
     * Link to the resource checker manager.
     */
    private IResourceCheckerManager resourceCheckerManager = null;


    /**
     * Build an extension for the given checker manager.
     * @param resourceCheckerManager the given manager
     */
    public ResourceCheckerConfigurationExtension(IResourceCheckerManager resourceCheckerManager) {
        this.resourceCheckerManager = resourceCheckerManager;
    }

    /**
     * Adapt the given configuration.
     * @param containerConfig JContainerConfig instance.
     */
    public void configure(EZBContainerConfig containerConfig) {
        containerConfig.addCallback(this);

    }

    /**
     * Called when container is starting.
     * @param info some information on the container which is starting.
     * @throws LifeCycleCallbackException if the invocation of the callback failed
     */
    public void start(EZBContainerCallbackInfo info) throws LifeCycleCallbackException {
        info.getContainer().addExtension(IResourceCheckerManager.class, resourceCheckerManager);
    }

    /**
     * Called when container is stopping.
     * @param info some information on the container which is stopping.
     * @throws LifeCycleCallbackException if the invocation of the callback failed
    */
    public void stop(EZBContainerCallbackInfo info) throws LifeCycleCallbackException {

    }

    /**
     * Called before binding a reference into the registry.
     * @param info some information on the container which is running.
     * @param reference a reference on the bean that will be bound
     * @throws LifeCycleCallbackException if the invocation of the callback failed
     */
    public void beforeBind(EZBContainerCallbackInfo info, EZBRef reference) throws LifeCycleCallbackException {

    }


}
