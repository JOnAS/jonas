/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id: $
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.jndi.interceptors.impl.datasource;

import java.sql.Connection;

import javax.transaction.RollbackException;
import javax.transaction.Status;
import javax.transaction.Synchronization;
import javax.transaction.SystemException;
import javax.transaction.Transaction;

import org.ow2.jonas.jndi.checker.api.IResourceChecker;
import org.ow2.jonas.jndi.checker.api.IResourceCheckerInfo;
import org.ow2.jonas.jndi.checker.api.IResourceCheckerManager;
import org.ow2.jonas.jndi.checker.api.ResourceCheckpoints;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

/**
 * A {@code TransactionalConnectionProxy} is ...
 *
 * @author Guillaume Sauthier
 */
public class TransactionalConnectionProxy extends ConnectionProxy implements Synchronization {

    /**
     * Associated transaction at the time of opening.
     */
    private Transaction transaction;

    private boolean isClosed = false;

    /**
     * Constructor of the handler of the proxy.
     *
     * @param delegate    the wrapped connection
     * @param transaction associated transaction
     */
    public TransactionalConnectionProxy(final Connection delegate, final Transaction transaction) {
        super(delegate);
        this.transaction = transaction;
    }

    @Override
    protected void unregisterConnection() {
        this.isClosed = true;
    }

    @Override
    public void registerConnection() {
        try {
            transaction.registerSynchronization(this);
        } catch (RollbackException e) {
            // Ignored
        } catch (SystemException e) {
            // Ignored
        }
    }

    public void beforeCompletion() { }

    public void afterCompletion(int status) {

        if (!isClosed) {

            IResourceCheckerInfo info = null;
            switch (status) {
                case Status.STATUS_COMMITTED:
                    info = new TransactionalCheckerInfo(ResourceCheckpoints.TRANSACTION_COMMITTED, transaction);
                    break;
                case Status.STATUS_ROLLEDBACK:
                    info = new TransactionalCheckerInfo(ResourceCheckpoints.TRANSACTION_ROLLEDBACK, transaction);
                    break;
            }

            // info may be null for all other status
            if (info != null) {
                autoCloseOrWarn(info);
            }

        } // else the connection was closed by caller, no check to do
    }

    /**
     * Store information bound to a given transactional context.
     */
    private class TransactionalCheckerInfo implements IResourceCheckerInfo {

        private ResourceCheckpoints checkpoint;
        private Transaction transaction;

        public TransactionalCheckerInfo(ResourceCheckpoints checkpoint, Transaction transaction) {
            this.checkpoint = checkpoint;
            this.transaction = transaction;
        }

        public ResourceCheckpoints getCheckPoint() {
            return checkpoint;
        }

        public String getCallerInfo() {
            return transaction.toString();
        }
    }
}
