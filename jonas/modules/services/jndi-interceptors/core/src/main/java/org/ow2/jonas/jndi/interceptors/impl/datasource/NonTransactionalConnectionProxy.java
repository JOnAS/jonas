/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id: $
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.jndi.interceptors.impl.datasource;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;

import org.ow2.jonas.jndi.checker.api.IResourceChecker;
import org.ow2.jonas.jndi.checker.api.IResourceCheckerInfo;
import org.ow2.jonas.jndi.checker.api.IResourceCheckerManager;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

/**
 * A {@code NonTransactionalConnectionProxy} is ...
 *
 * @author Guillaume Sauthier
 */
public class NonTransactionalConnectionProxy extends ConnectionProxy implements IResourceChecker {

    /**
     * Resource checker manager.
     */
    private IResourceCheckerManager manager;

    /**
     * Constructor of the handler of the proxy.
     *
     * @param delegate the wrapped connection
     * @param manager  resource manager
     */
    public NonTransactionalConnectionProxy(final Connection delegate, final IResourceCheckerManager manager) {
        super(delegate);
        this.manager = manager;
    }

    @Override
    protected void unregisterConnection() {
        // Delist this checker from available resources
        manager.delistResource(this);
    }

    public void detect(IResourceCheckerInfo resourceCheckerInfo) {
        autoCloseOrWarn(resourceCheckerInfo);
        unregisterConnection();
    }

    @Override
    public void registerConnection() {
        // Enlist a ResourceChecker just for this Connection
        manager.enlistResource(this);
    }
}
