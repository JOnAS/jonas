/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 France Telecom R&D
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.jndi.interceptors.impl;

import java.util.ArrayList;
import java.util.List;

import org.ow2.carol.jndi.intercept.ContextInterceptor;

/**
 * Defines the object that is containing all the interceptors.
 * @author Florent Benoit
 */
public class Interceptors {

    /**
     * List of interceptors.
     */
    private List<ContextInterceptor> interceptors = null;

    /**
     * Default constructor.
     */
    public Interceptors() {
        this.interceptors = new ArrayList<ContextInterceptor>();
    }

    /**
     * Gets the list of interceptors.
     * @return the list of interceptors.
     */
    public List<ContextInterceptor> getContextInterceptors() {
        return interceptors;
    }

    /**
     * Sets the list of interceptors.
     * @param interceptors the list of interceptors.
     */
    public void setContextInterceptors(final List<ContextInterceptor> interceptors) {
        this.interceptors = interceptors;
    }
}
