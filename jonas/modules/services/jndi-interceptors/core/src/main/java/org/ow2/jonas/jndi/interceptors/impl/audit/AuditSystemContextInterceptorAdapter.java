/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2010 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.jndi.interceptors.impl.audit;

import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.RuntimeOperationsException;
import javax.management.modelmbean.InvalidTargetObjectTypeException;

import org.apache.commons.modeler.BaseModelMBean;

/**
 * Commons modeler adapter.
 * @author Florent Benoit
 */
public class AuditSystemContextInterceptorAdapter extends BaseModelMBean {

    /**
     * Create the mbean.
     * @throws MBeanException if the super constructor fails.
     */
    public AuditSystemContextInterceptorAdapter() throws MBeanException {
        super();
    }

    /**
     * @return the wrapped resource (managed object)
     */
    protected AuditSystemContextInterceptor getManagedComponent() {
        try {
            return (AuditSystemContextInterceptor) getManagedResource();
        } catch (InstanceNotFoundException e) {
            throw new IllegalStateException("Cannot get the managed resource of the MBean", e);
        } catch (RuntimeOperationsException e) {
            throw new IllegalStateException("Cannot get the managed resource of the MBean", e);
        } catch (MBeanException e) {
            throw new IllegalStateException("Cannot get the managed resource of the MBean", e);
        } catch (InvalidTargetObjectTypeException e) {
            throw new IllegalStateException("Cannot get the managed resource of the MBean", e);
        }
    }

    /**
     * Calls some operation on the wrapped object.
     * @param registrationDone if registration has been done or not
     */
    @Override
    public void postRegister(final Boolean registrationDone) {
        getManagedComponent().setBroadcaster(this);
    }

}
