/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 France Telecom R&D
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.jndi.interceptors.impl.datasource;

import java.lang.reflect.Method;

import javax.sql.DataSource;
import javax.transaction.TransactionManager;

import org.ow2.carol.jndi.intercept.ContextInterceptor;
import org.ow2.carol.jndi.intercept.InterceptionContext;
import org.ow2.jonas.jndi.interceptors.impl.AbsContextInterceptor;

/**
 * Context interceptor for Datasources.
 * @author Florent Benoit
 */
public class DataSourceLeakDetectorContextInterceptor extends AbsContextInterceptor implements ContextInterceptor {

    /**
     * JTA Transaction Manager.
     */
    private TransactionManager transactionManager;

    /**
     * Defines if the connections are automatically closed or not.
     */
    private boolean forceClose = true;

    /**
     * Intercept the lookup on the Datasources.
     * @param context the interception context with data
     * @return the wrapped datasource if intercepted, else it provides the original datasource
     * @throws Exception if interception fails
     */
    public Object intercept(final InterceptionContext context) throws Exception {

        // Invoke the call in order to get the result
        Object obj = context.proceed();

        // Do some stuff if we're interested in a give method
        Method method = context.getMethod();

        // Only react on lookup/lookupLink methods
        if (filterOnMethodName(method)) {

            // And if this is a managed JNDI name
            if (filterOnJndiName(context.getParameters()[0].toString())) {
                if (DataSource.class.isAssignableFrom(obj.getClass())) {

                    // Cast the DS
                    DataSource ds = (DataSource) obj;

                    // Wrap datasource
                    DatasourceWrapper detector = new DatasourceWrapper(ds, getResourceCheckerManager(), transactionManager);
                    // automatically close the connections ?
                    detector.setForceClose(forceClose);

                    // Return our detector DataSource
                    return detector;
                }
            }
        }

        // Let the Object unchanged
        return obj;
    }

    /**
     * @return true if the connection will be automatically closed if algorithm detects that the connection is still open.
     */
    public boolean isForceClose() {
        return forceClose;
    }

    /**
     * Sets the flag for automatically closing or not the connection.
     * @param forceClose the given boolean value
     */
    public void setForceClose(final boolean forceClose) {
        this.forceClose = forceClose;
    }

    public void setTransactionManager(TransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }
}
