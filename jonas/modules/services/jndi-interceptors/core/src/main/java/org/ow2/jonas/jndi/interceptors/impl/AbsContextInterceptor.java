/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 France Telecom R&D
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.jndi.interceptors.impl;

import java.lang.reflect.Method;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.management.ObjectName;

import org.ow2.carol.jndi.intercept.ContextInterceptor;
import org.ow2.jonas.jndi.checker.api.IResourceCheckerManager;

/**
 * Abstract class for some JOnAS context interceptors.
 * @author Florent Benoit
 */
public abstract class AbsContextInterceptor implements ContextInterceptor {

    /**
     * Resource checker manager.
     */
    private IResourceCheckerManager resourceCheckerManager = null;



    /**
     * Regexp for the jndi name.
     */
    private String jndiRegexp = ".*";

    /**
     * Regexp for the method name.
     */
    private String methodsRegexp = "lookup.*";

    /**
     * Allows to filter JNDI Name in order to enable interceptor.
     * @param jndiName the JNDI name used for the filter
     * @return true if interceptor may be applied
     */
    protected boolean filterOnJndiName(final String jndiName) {
        Pattern pattern = Pattern.compile(jndiRegexp);
        Matcher matcher = pattern.matcher(jndiName);
        return matcher.matches();
    }

    /**
     * For a given method, check if the interceptor is applied or not.
     * @param method the method to check
     * @return true if interceptor may be applied, else false
     */
    protected boolean filterOnMethodName(final Method method) {
        Pattern pattern = Pattern.compile(methodsRegexp);
        Matcher matcher = pattern.matcher(method.getName());
        return matcher.matches();
    }

    /**
     * @return the resource checker manager
     */
    public IResourceCheckerManager getResourceCheckerManager() {
        return resourceCheckerManager;
    }

    /**
     * Sets the resource checker manager.
     * @param resourceCheckerManager the given instance
     */
    public void setResourceCheckerManager(final IResourceCheckerManager resourceCheckerManager) {
        this.resourceCheckerManager = resourceCheckerManager;
    }


    /**
     * @return the JNDI regexp.
     */
    public String getJndiRegexp() {
        return jndiRegexp;
    }

    /**
     * Sets the JNDI regexp.
     * @param jndiRegexp the given value
     */
    public void setJndiRegexp(final String jndiRegexp) {
        this.jndiRegexp = jndiRegexp;
    }

    /**
     * @return the Method regexp.
     */
    public String getMethodsRegexp() {
        return methodsRegexp;
    }

    /**
     * Sets the Method regexp.
     * @param methodsRegexp the given value
     */
    public void setMethodsRegexp(final String methodsRegexp) {
        this.methodsRegexp = methodsRegexp;
    }


    /**
     * @param domain the domain name of the JMX Server.
     * @return a different objectname for this object.
     */
    public ObjectName getObjectName(final String domain) {
        return null;
    }

}
