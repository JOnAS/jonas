/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id: $
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.jndi.interceptors.impl.datasource;

import java.lang.reflect.Method;
import java.sql.Connection;

import javax.transaction.Status;
import javax.transaction.Synchronization;

import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.ow2.jonas.jndi.checker.api.IResourceChecker;
import org.ow2.jonas.jndi.checker.api.IResourceCheckerInfo;
import org.ow2.jonas.jndi.checker.api.IResourceCheckerManager;
import org.ow2.jonas.jndi.checker.api.ResourceCheckpoints;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

/**
 * A {@code NonTransactionalConnectionProxyTestCase} is ...
 *
 * @author Guillaume Sauthier
 */
public class NonTransactionalConnectionProxyTestCase {

    @Mock
    private Connection delegate;

    @Mock
    private IResourceCheckerManager manager;

    @Mock
    private IResourceChecker checker;

    @BeforeMethod
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCloseDelistTheResource() throws Throwable {
        NonTransactionalConnectionProxy proxy = new NonTransactionalConnectionProxy(delegate, manager);
        proxy.registerConnection();

        verify(manager).enlistResource(proxy);

        proxy.invoke(null, ConnectionClassUtils.getCloseMethod(), null);

        verify(manager).delistResource(proxy);
        verify(delegate).close();
    }

    @Test
    public void testThatNotClosedProxiedConnectionWillBeClosedWhenDetectionIsPerformed() throws Throwable {
        NonTransactionalConnectionProxy proxy = new NonTransactionalConnectionProxy(delegate, manager);
        proxy.setForceClose(true);
        proxy.registerConnection();

        verify(manager).enlistResource(proxy);

        proxy.detect(new EmptyCheckerInfo());

        verify(delegate).close();
    }


    private static class EmptyCheckerInfo implements IResourceCheckerInfo {
        public ResourceCheckpoints getCheckPoint() {
            return ResourceCheckpoints.EJB_POST_INVOKE;
        }

        public String getCallerInfo() {
            return "test";
        }
    }
}
