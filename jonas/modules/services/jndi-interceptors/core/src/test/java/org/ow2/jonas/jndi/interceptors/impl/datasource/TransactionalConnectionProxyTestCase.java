/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id: $
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.jndi.interceptors.impl.datasource;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.util.concurrent.SynchronousQueue;

import javax.net.ssl.SSLEngineResult;
import javax.transaction.Status;
import javax.transaction.Synchronization;
import javax.transaction.Transaction;

import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.ow2.jonas.jndi.checker.api.IResourceChecker;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

/**
 * A {@code NonTransactionalConnectionProxyTestCase} is ...
 *
 * @author Guillaume Sauthier
 */
public class TransactionalConnectionProxyTestCase {

    @Mock
    private Connection delegate;

    @Mock
    private Transaction transaction;

    @BeforeMethod
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCloseDelistTheResource() throws Throwable {
        TransactionalConnectionProxy proxy = new TransactionalConnectionProxy(delegate, transaction);
        proxy.setForceClose(true);
        proxy.registerConnection();

        ArgumentCaptor<Synchronization> captor = ArgumentCaptor.forClass(Synchronization.class);
        verify(transaction).registerSynchronization(captor.capture());

        // Closing the Connection by hand ...
        proxy.invoke(null, ConnectionClassUtils.getCloseMethod(), null);
        // ... will close the delegate connection
        verify(delegate).close();

        // When the transaction terminate, Synchronization is called
        captor.getValue().afterCompletion(Status.STATUS_COMMITTED);
        verifyNoMoreInteractions(delegate);
    }

    @Test
    public void testThatNotClosedProxiedConnectionWillBeClosedWhenTransactionIsCommitted() throws Throwable {
        TransactionalConnectionProxy proxy = new TransactionalConnectionProxy(delegate, transaction);
        proxy.setForceClose(true);
        proxy.registerConnection();

        ArgumentCaptor<Synchronization> captor = ArgumentCaptor.forClass(Synchronization.class);
        verify(transaction).registerSynchronization(captor.capture());

        // When the transaction terminate, Synchronization is called
        captor.getValue().afterCompletion(Status.STATUS_COMMITTED);
        verify(delegate).close();
    }

}
