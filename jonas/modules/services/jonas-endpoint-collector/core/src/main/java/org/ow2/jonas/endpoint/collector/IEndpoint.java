/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.endpoint.collector;

/**
 * Represents an endpoint
 * @author Jeremy Cazaux
 */
public interface IEndpoint {

    /**
     * Prefix of the source id
     */
    static final String SOURCE_PREFIX = "endpoint/";

    /**
     * @return the source
     */
    String getSource();

    /**
     * @return the port
     */
    String getPort();

    /**
     * @return the host
     */
    String getHost();

    /**
     * @return the protocol
     */
    String getProtocol();

    /**
     * @return the URL (if exist)
     */
    String getUrl();

    /**
     * @param source The source to set
     */
    void setSource(String source);

    /**
     * @param protocol The protocol to set
     */
    void setProtocol(String protocol);

    /**
     * @param port The port to set
     */
    void setPort(String port);

    /**
     * @param host The host to set
     */
    void setHost(String host);

    /**
     * @param url The url to set
     */
    void setUrl(String url);
}
