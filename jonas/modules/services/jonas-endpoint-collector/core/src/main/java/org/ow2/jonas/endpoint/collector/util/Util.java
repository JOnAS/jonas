/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.endpoint.collector.util;

import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;

/**
 * Utility MBean class
 */
public class Util {

    /**
     * The logger
     */
    private static final Log logger = LogFactory.getLog(Util.class);

    /**
     * @param objectName The {@link javax.management.ObjectName}
     * @param attribute An attribute of the associated MBean to retrieve
     * @return the value of the given attribute
     */
    public static Object getMBeanAttributeValue(final ObjectName objectName, final String attribute, final MBeanServer mBeanServer) {
        try {
            return mBeanServer.getAttribute(objectName, attribute);
        } catch (MBeanException e) {
            logger.error("Cannot get Attribute Address of ObjectName " + objectName, e);
        } catch (AttributeNotFoundException e) {
            logger.error("Cannot get Attribute Address of ObjectName " + objectName, e);
        } catch (InstanceNotFoundException e) {
            logger.error("Cannot get Attribute Address of ObjectName " + objectName, e);
        } catch (ReflectionException e) {
            logger.error("Cannot get Attribute Address of ObjectName " + objectName, e);
        }
        return null;
    }

    /**
     * @param pattern The pattern
     * @return an instance of the associated {@link ObjectName}
     */
    public static ObjectName getObjectName(final String pattern) {
        ObjectName objectName = null;
        try {
            objectName = new ObjectName(pattern);
        } catch (MalformedObjectNameException e) {
            logger.error("Cannot instanciate ObjectName with pattern " + pattern, e);
        }
        return objectName;
    }
}
