/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ear.internal;

import org.ow2.jonas.ear.EarService;
import org.ow2.jonas.lib.work.AbsCleanTask;
import org.ow2.jonas.workcleaner.IDeployerLog;
import org.ow2.jonas.workcleaner.LogEntry;
import org.ow2.jonas.workcleaner.WorkCleanerException;

/**
 * JOnAS EAR unused directory clean task class. This class provides a way for removing directories which are inconsistent
 * directories for ear files.
 * @author Florent BENOIT
 * @author Benoit PELLETIER
 */
public class EarCleanTask extends AbsCleanTask {

    /**
     * {@link IDeployerLog} of the task.
     */
    private IDeployerLog deployerLog = null;

    /**
     * {@link EarService} reference.
     */
    private EarService earService;

    /**
     * Construct a new EAR clean task.
     * @param earService EarService reference
     * @param deployerLog DeployerLog of the task
     */
    public EarCleanTask(final EarService earService, final IDeployerLog deployerLog) {
        this.earService = earService;
        this.deployerLog = deployerLog;
    }

    /**
     * Check if the package pointed by the log entry is currently deploy.
     * @param logEntry entry in a deploy log
     * @return true if the package pointed by the log entry is currently deployed
     * @throws WorkCleanerException if it fails
     */
    @Override
    protected boolean isDeployedLogEntry(final LogEntry logEntry) throws WorkCleanerException {
        // Check if the EAR file is deployed
        return earService.isEarDeployedByWorkName(logEntry.getCopy().getName());
    }

    /**
     * {@inheritDoc}
     * @see org.ow2.jonas.lib.work.AbsCleanTask#getDeployerLog()
     */
    @Override
    public IDeployerLog getDeployerLog() {
        return deployerLog;
    }
}
