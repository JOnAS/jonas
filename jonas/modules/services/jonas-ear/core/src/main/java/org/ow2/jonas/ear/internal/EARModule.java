/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ear.internal;

import org.ow2.jonas.lib.loader.FilteringClassLoader;
import org.ow2.util.ee.deploy.api.deployable.EARDeployable;

/**
 * Class representing an Ear structure which is composed of modules inside this
 * archive and about the current URL or unpacked folder, deployment descriptor.
 * @author Florent Benoit
 */
public class EARModule {

    /**
     * EAR deployable.
     */
    private EARDeployable earDeployable = null;

    /**
     * Deployment descriptor.
     */
    private String deploymentDescriptor = null;

    /**
     * ClassLoader.
     */
    private ClassLoader classLoader = null;

    /**
     * FilteringClassLoader.
     */
    private FilteringClassLoader filteringClassLoader = null;


    /**
     * Construct an Ear structure with the specified files.
     * @param earDeployable name of the working copy of the ear
     * @param deploymentDescriptor the deployment descriptor of the file
     */
    public EARModule(final EARDeployable earDeployable, final String deploymentDescriptor) {
        this.earDeployable = earDeployable;
        if (deploymentDescriptor == null) {
            this.deploymentDescriptor = "";
        } else {
            this.deploymentDescriptor = deploymentDescriptor;
        }
    }

    /**
     * Return the deployable of this Ear.
     * @return the deployable of this Ear
     */
    public EARDeployable getEARDeployable() {
        return earDeployable;
    }

    /**
     * @return the deployment descriptor of this EAR.
     */
    public String getDeploymentDescriptor() {
        return deploymentDescriptor;
    }

    /**
     * Return the classloader of this Ear.
     * @return the classloader of this Ear
     */
    public ClassLoader getClassLoader() {
        return classLoader;
    }

    /**
     * Sets the classloader
     */
    public void setClassLoader(final ClassLoader classLoader) {
        this.classLoader = classLoader;
    }

    /**
     * Return the FilteringClassLoader of this Ear.
     * @return the FilteringClassLoader of this Ear
     */
    public FilteringClassLoader getFilteringClassLoader() {
        return filteringClassLoader;
    }

    /**
     * Sets the FilteringClassLoader
     */
    public void setFilteringClassLoader(final FilteringClassLoader filteringClassLoader) {
        this.filteringClassLoader = filteringClassLoader;
    }

}
