/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ear.internal.mbean;

import org.ow2.jonas.ear.internal.EARModule;
import org.ow2.jonas.lib.management.javaee.JSR77ManagementIdentifier;

/**
 * Object for managing the ObjectName of the J2EE Application MBean.
 * @author Florent BENOIT
 */
public class EARModuleIdentifier extends JSR77ManagementIdentifier<EARModule> {

    /**
     * JMX MBean Type.
     */
    private static final String TYPE = "J2EEApplication";

    /**
     * @param instance Managed instance from which the additionnal properties
     *        will be extracted.
     * @return Returns a comma separated(,) list of properties (name=value)
     */
    public String getAdditionnalProperties(final EARModule instance) {
        StringBuilder sb = new StringBuilder();
        // J2EEServer=?
        sb.append(getJ2EEServerString());
        return sb.toString();
    }

    /**
     * @param instance Managed instance from which the name will be extracted.
     * @return Returns the ObjectName 'name' property value.
     */
    public String getNamePropertyValue(final EARModule instance) {
        return instance.getEARDeployable().getModuleName();
    }

    /**
     * This method has to be implemented by each {@link JSR77ManagementIdentifier}.
     * @return Returns the type value. (example : <code>J2EEServer</code>)
     */
    public String getTypeValue() {
        return TYPE;
    }

}
