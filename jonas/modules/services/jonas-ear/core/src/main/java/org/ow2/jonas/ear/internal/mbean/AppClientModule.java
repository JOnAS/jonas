/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ear.internal.mbean;

import org.ow2.jonas.lib.management.javaee.J2EEModule;

/**
 * MBean class for AppClientModule management
 * @author Adriana Danes JSR 77 (J2EE Management Standard)
 */
public class AppClientModule extends J2EEModule {

    /**
     * FileName of the client jar
     */
    private String fileName = null;

    /**
     * Constructor
     * @param objectName name of the MBean
     * @param fileName name of the client jar
     * @param deploymentDescriptor the XML standard DD
     * @param jonasDeploymentDescriptor the XML specific DD
     */
    public AppClientModule(String objectName, String fileName, String deploymentDescriptor, String jonasDeploymentDescriptor) {
        super(objectName);
        this.fileName = fileName;
        setDeploymentDescriptor(deploymentDescriptor);
        setJonasDeploymentDescriptor(jonasDeploymentDescriptor);
    }

    /**
     * @return the filename
     */
    public String getFileName() {
        return fileName;
    }

}
