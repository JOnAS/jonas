<?xml version="1.0" encoding="UTF-8"?>
<!--
  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  - JOnAS: Java(TM) Open Application Server
  - Copyright (C) 2012 Bull S.A.S.
  - Contact: jonas-team@ow2.org
  -
  - This library is free software; you can redistribute it and/or
  - modify it under the terms of the GNU Lesser General Public
  - License as published by the Free Software Foundation; either
  - version 2.1 of the License, or any later version.
  -
  - This library is distributed in the hope that it will be useful,
  - but WITHOUT ANY WARRANTY; without even the implied warranty of
  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  - Lesser General Public License for more details.
  -
  - You should have received a copy of the GNU Lesser General Public
  - License along with this library; if not, write to the Free Software
  - Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
  - USA
  -
  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  - $Id$
  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <parent>
    <artifactId>jonas-packaging</artifactId>
    <groupId>org.ow2.jonas</groupId>
    <version>6.0.0-M1-SNAPSHOT</version>
  </parent>
  <modelVersion>4.0.0</modelVersion>
  <name>JOnAS :: Services :: Packaging :: Core</name>
  <artifactId>jonas-packaging-core</artifactId>
  <packaging>bundle</packaging>

  <dependencies>
    <dependency>
      <groupId>org.ow2.jonas</groupId>
      <artifactId>jonas-apis-services-core</artifactId>
      <version>${project.version}</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.ow2.jonas</groupId>
      <artifactId>jonas-apis-services-jmx</artifactId>
      <version>${project.version}</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.ow2.jonas</groupId>
      <artifactId>jonas-services-api</artifactId>
      <version>${project.version}</version>
      <scope>provided</scope>
    </dependency>

    <dependency>
      <groupId>org.ow2.jonas.jpaas.util.cloud-desc</groupId>
      <artifactId>cloud-application-core</artifactId>
      <version>${jpaas-util.version}</version>
    </dependency>
    <dependency>
      <groupId>org.ow2.jonas.jpaas.util.cloud-desc</groupId>
      <artifactId>deployment-core</artifactId>
      <version>${jpaas-util.version}</version>
    </dependency>
    <dependency>
      <groupId>org.ow2.jonas.jpaas.util.cloud-desc</groupId>
      <artifactId>environment-template-core</artifactId>
      <version>${jpaas-util.version}</version>
    </dependency>
    <dependency>
      <groupId>org.ow2.util.plan</groupId>
      <artifactId>plan-schemas</artifactId>
      <version>${ow2-util-plan.version}</version>
    </dependency>
    <dependency>
      <groupId>org.ow2.jonas</groupId>
      <artifactId>jonas-addon-core</artifactId>
      <version>${project.version}</version>
    </dependency>
    <dependency>
      <groupId>org.ow2.jonas</groupId>
      <artifactId>jonas-commons</artifactId>
      <version>${project.version}</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.ow2.easybeans</groupId>
      <artifactId>easybeans-api</artifactId>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.jgrapht</groupId>
      <artifactId>jgrapht-jdk1.5</artifactId>
      <version>0.7.3</version>
    </dependency>
    <dependency>
      <groupId>graphlayout</groupId>
      <artifactId>graphlayout</artifactId>
      <version>1.2.1</version>
    </dependency>
    <dependency>
      <groupId>org.objectweb.fractal.fractalexplorer.jgraph</groupId>
      <artifactId>jgraph</artifactId>
      <version>fractal</version>
    </dependency>
    <dependency>
      <groupId>org.testng</groupId>
      <artifactId>testng</artifactId>
      <scope>test</scope>
    </dependency>
  </dependencies>

  <build>
    <plugins>
      <plugin>
        <groupId>org.ow2.util.maven</groupId>
        <artifactId>jbuilding-maven-plugin</artifactId>
        <inherited>false</inherited>
        <configuration>
          <deploymentPlans>
            <deploymentPlan>
              <name>packaging-deployment-plan</name>
              <artifactItems>
                <artifactItem>
                  <groupId>org.ow2.jonas</groupId>
                  <artifactId>jonas-packaging-core</artifactId>
                  <version>${project.version}</version>
                </artifactItem>
              </artifactItems>
            </deploymentPlan>
          </deploymentPlans>
        </configuration>
        <executions>
          <execution>
            <id>generate-jonas-services-maven2-deployment-plans</id>
            <goals>
              <goal>generate-maven2-deployment-plans</goal>
            </goals>
            <phase>pre-integration-test</phase>
            <configuration>
              <directory>url-internal</directory>
            </configuration>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <artifactId>maven-jar-plugin</artifactId>
        <executions>
          <execution>
            <id>attach-templates</id>
            <phase>compile</phase>
            <goals>
              <goal>jar</goal>
            </goals>
            <configuration>
              <classesDirectory>src/main/templates</classesDirectory>
              <outputDirectory>${project.build.directory}</outputDirectory>
              <classifier>templates</classifier>
              <excludes>
                <exclude>**/.svn/**</exclude>
              </excludes>
            </configuration>
          </execution>
          <execution>
            <id>attach-deployment-plan</id>
            <phase>integration-test</phase>
            <goals>
              <goal>jar</goal>
            </goals>
            <configuration>
              <classesDirectory>${project.build.directory}/configuration-resources/url-internal</classesDirectory>
              <outputDirectory>${project.build.directory}</outputDirectory>
              <classifier>deployment-plan</classifier>
              <excludes>
                <exclude>**/.svn/**</exclude>
              </excludes>
            </configuration>
          </execution>
        </executions>
      </plugin>
    </plugins>
  </build>

</project>
