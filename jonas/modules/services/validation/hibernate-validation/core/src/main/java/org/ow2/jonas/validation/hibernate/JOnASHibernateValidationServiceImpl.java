/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2010-2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.validation.hibernate;

import javax.validation.spi.ValidationProvider;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.ow2.jonas.jmx.JmxService;
import org.ow2.jonas.lib.service.AbsServiceImpl;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.management.J2EEServerService;
import org.ow2.jonas.service.ServiceException;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

/**
 * Implementation of the service that is providing Validation provider.
 * @author Florent Benoit
 */
public class JOnASHibernateValidationServiceImpl extends AbsServiceImpl {

    /**
     * Logger.
     */
    private static Log logger = LogFactory.getLog(JOnASHibernateValidationServiceImpl.class);

    /**
     * Reference to the JMX service.
     */
    private JmxService jmxService = null;

    /**
     * OSGi Bundle context.
     */
    private BundleContext bundleContext = null;

    /**
     * Reference to the MBeans library. This one register J2EEServer. It's only
     * here because we need to be started <b>after</b>.
     */
    private J2EEServerService j2eeServer = null;

    /**
     * Constructor in OSGi mode. It provides the bundle context.
     * @param bundleContext the given bundle context.
     */
    public JOnASHibernateValidationServiceImpl(final BundleContext bundleContext) {
        this.bundleContext = bundleContext;

    }

    /**
     * Abstract start-up method to be implemented by sub-classes.
     * @throws ServiceException service start-up failed
     */
    @Override
    protected void doStart() throws ServiceException {

        // register mbeans-descriptors
        jmxService.loadDescriptors(getClass().getPackage().getName(), getClass().getClassLoader());

        // Register MBean
        try {
            jmxService.registerModelMBean(this, JonasObjectName.validationService(getDomainName()));
        } catch (Exception e) {
            logger.warn("Cannot register MBean for validation service", e);
        }

    }


    /**
     * Abstract method for service stopping to be implemented by sub-classes.
     * @throws ServiceException service stopping failed
     */
    @Override
    protected void doStop() throws ServiceException {

        // Unregister MBean
        try {
            jmxService.unregisterModelMBean(JonasObjectName.validationService(getDomainName()));
        } catch (Exception e) {
            logger.debug("Cannot unregister MBean for validation service", e);
        }
    }

    /**
     * @param jmxService the jmxService to set
     */
    public void setJmxService(final JmxService jmxService) {
        this.jmxService = jmxService;
    }

    /**
     * @param j2eeServer the j2eeServer to set
     */
    public void setJ2EEServer(final J2EEServerService j2eeServer) {
        this.j2eeServer = j2eeServer;
    }

}
