/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id: $
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.cdi.weld.internal.tomcat;

import org.apache.catalina.LifecycleListener;
import org.apache.catalina.core.StandardContext;
import org.jboss.weld.servlet.WeldListener;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.verification.VerificationMode;
import org.ow2.jonas.cdi.weld.IWeldService;
import org.ow2.util.ee.deploy.api.deployable.WARDeployable;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * A {@code WeldContextCustomizerTestCase} is ...
 *
 * @author Guillaume Sauthier
 */
public class WeldContextCustomizerTestCase {

    @Mock
    private IWeldService weldService;

    @Spy
    private StandardContext standardContext = new StandardContext();

    @Mock
    private WARDeployable warDeployable;

    @BeforeMethod
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCustomizeWithNonCdiEnabledArchive() throws Exception {

        when(weldService.isCdiEnabled(null)).thenReturn(Boolean.FALSE);

        WeldContextCustomizer customizer = new WeldContextCustomizer();
        customizer.setWeldService(weldService);

        customizer.customize(standardContext, warDeployable);

        verify(standardContext, never()).addLifecycleListener(any(LifecycleListener.class));
        verify(standardContext, never()).addApplicationListener(any(String.class));
    }

    @Test
    public void testCustomizeWithCdiEnabledArchive() throws Exception {

        when(weldService.isCdiEnabled(null)).thenReturn(Boolean.TRUE);

        WeldContextCustomizer customizer = new WeldContextCustomizer();
        customizer.setWeldService(weldService);

        customizer.customize(standardContext, warDeployable);

        verify(standardContext).addLifecycleListener(any(WeldLifeCycleListener.class));
        verify(standardContext).addApplicationListener(WeldListener.class.getName());
    }
}
