/*
 * Copyright (C) 2012  Bull S. A. S.
 * Bull, Rue Jean Jaures, B.P.68, 78340, Les Clayes-sous-Bois
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation
 * version 2.1 of the License.
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA  02110-1301, USA.
 */

package org.ow2.jonas.cdi.weld.internal.easybeans;

import org.ow2.easybeans.api.EZBContainerCallbackInfo;
import org.ow2.easybeans.api.Factory;
import org.ow2.easybeans.api.LifeCycleCallbackException;
import org.ow2.easybeans.container.EmptyLifeCycleCallBack;

/**
 * This class intercepts startup and shutdown events for EasyBeans containers in order populate the {@link EZBFactoriesRegistry}
 *
 * @author Loic Albertin
 */
public class WeldLifecycleCallback extends EmptyLifeCycleCallBack {

    private EZBFactoriesRegistry factoriesRegistry;

    public WeldLifecycleCallback(EZBFactoriesRegistry factoriesRegistry) {
        this.factoriesRegistry = factoriesRegistry;
    }

    @Override
    public void start(EZBContainerCallbackInfo info) throws LifeCycleCallbackException {
        String moduleId = info.getContainer().getConfiguration().getModuleName();
        for (Factory<?, ?> factory : info.getFactories().values()) {
            factoriesRegistry.registerBeanFactory(factory);
        }
    }

    @Override
    public void stop(EZBContainerCallbackInfo info) throws LifeCycleCallbackException {
        String moduleId = info.getContainer().getConfiguration().getModuleName();
        factoriesRegistry.unregisterFactories(moduleId);
    }
}
