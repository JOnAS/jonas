/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 *  $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.cdi.weld.internal.ejb;

import org.jboss.weld.ejb.api.SessionObjectReference;
import org.ow2.easybeans.api.Factory;
import org.ow2.easybeans.api.bean.EasyBeansSB;
import org.ow2.easybeans.container.session.EasyBeansSessionContext;
import org.ow2.easybeans.container.session.SessionFactory;
import org.ow2.easybeans.container.session.stateless.StatelessSessionFactory;

/**
 * This class is a reference to a session object in the EJB container
 *
 * @author Loic Albertin
 */
public class EZBSessionObjectReference<BeanType extends EasyBeansSB<BeanType>> implements SessionObjectReference {

    private static final long serialVersionUID = 4302342856631289267L;

    /**
     * The {@link SessionFactory}
     */
    SessionFactory<BeanType> factory;

    /**
     * An {@link EasyBeansSessionContext}
     */
    EasyBeansSessionContext<SessionFactory<BeanType>> sessionContext;

    public EZBSessionObjectReference(SessionFactory<BeanType> factory) {
        this.factory = factory;
        sessionContext = new EasyBeansSessionContext<SessionFactory<BeanType>>(factory);
    }

    public <S> S getBusinessObject(Class<S> businessInterfaceType) {
        return sessionContext.getBusinessObject(businessInterfaceType);
    }

    public void remove() {
        throw new UnsupportedOperationException("Remove operation not supported for non-stateful beans");
    }

    public boolean isRemoved() {
        return false;
    }

    /**
     *
     * @return  The {@link SessionFactory}
     */
    protected SessionFactory<BeanType> getFactory() {
        return factory;
    }

    /**
     *
     * @return The {@link EasyBeansSessionContext}
     */
    protected EasyBeansSessionContext<SessionFactory<BeanType>> getSessionContext() {
        return sessionContext;
    }
}
