/*
 * Copyright (C) 2012  Bull S. A. S.
 * Bull, Rue Jean Jaures, B.P.68, 78340, Les Clayes-sous-Bois
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation
 * version 2.1 of the License.
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA  02110-1301, USA.
 */

package org.ow2.jonas.cdi.weld.internal.bootstrap;

import javax.servlet.ServletContext;

import org.jboss.weld.bootstrap.api.Bootstrap;
import org.jboss.weld.environment.servlet.deployment.ServletDeployment;
import org.jboss.weld.environment.servlet.deployment.WebAppBeanDeploymentArchive;

/**
 * This class overrides the {@link ServletDeployment#createWebAppBeanDeploymentArchive(javax.servlet.ServletContext,
 * org.jboss.weld.bootstrap.api.Bootstrap)}
 * method to provide a {@link JOnASWebAppBeanDeploymentArchive}.
 *
 * @author Loic Albertin
 */
public class JOnASServletDeployment extends ServletDeployment {

    public JOnASServletDeployment(ServletContext servletContext, Bootstrap bootstrap) {
        super(servletContext, bootstrap);
    }

    @Override
    protected WebAppBeanDeploymentArchive createWebAppBeanDeploymentArchive(ServletContext servletContext, Bootstrap bootstrap) {
        return new JOnASWebAppBeanDeploymentArchive(servletContext, bootstrap);
    }
}
