/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2010 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.cdi.weld.internal;

import java.net.URL;

import javax.servlet.jsp.JspFactory;

import org.ow2.jonas.cdi.weld.IWeldService;
import org.ow2.jonas.cdi.weld.internal.jsp.WeldJspFactory;
import org.ow2.jonas.lib.service.AbsServiceImpl;
import org.ow2.jonas.service.ServiceException;
import org.ow2.util.archive.api.ArchiveException;
import org.ow2.util.archive.api.IArchive;

/**
 * Weld CDI service implementation.
 *
 * @author Guillaume Sauthier
 */
public class DefaultWeldService extends AbsServiceImpl implements IWeldService {

    public static final String METAINF_BEANS_XML = "META-INF/beans.xml";
    public static final String WEBINF_BEANS_XML = "WEB-INF/beans.xml";

    private JspFactory jspFactoryDelegate;

    private WeldJspFactory  weldJspFactory = null;

    /**
     * Abstract start-up method to be implemented by sub-classes.
     *
     * @throws org.ow2.jonas.service.ServiceException service start-up failed
     */
    @Override
    protected void doStart() throws ServiceException {

    }

    /**
     * Abstract method for service stopping to be implemented by sub-classes.
     *
     * @throws org.ow2.jonas.service.ServiceException service stopping failed
     */
    @Override
    protected void doStop() throws ServiceException {
        // Un-wrap the JspFactory
        if (jspFactoryDelegate != null) {
            JspFactory.setDefaultFactory(jspFactoryDelegate);
        }
    }

    /**
     * Detect if the given {@code IArchive} contains a {@code beans.xml}.
     * Checked location includes:
     * <ul>
     * <li>{@code WEB-INF/beans.xml}</li>
     * <li>{@code META-INF/beans.xml}</li>
     * </ul>
     *
     * @param archive explored archive
     *
     * @return {@code true} if the given archive is CDI enabled, {@code false} otherwise.
     */
    public boolean isCdiEnabled(IArchive archive) {

        try {

            // Search in usual jar place ...
            URL resource = archive.getResource(METAINF_BEANS_XML);
            if (resource != null) {
                // CDI enabled archive (Jar)
                return true;
            }

            // ... and in the war usual location
            resource = archive.getResource(WEBINF_BEANS_XML);
            if (resource != null) {
                // CDI enabled archive (Web)
                return true;
            }

        } catch (ArchiveException e) {
            // Missing entry, not CDI enabled
        }

        return false;
    }

    @Override
    public synchronized JspFactory getJspFactory() {
        // If not already wrapped
        if (weldJspFactory == null) {
            jspFactoryDelegate = JspFactory.getDefaultFactory();
            // May not be already started
            if (jspFactoryDelegate != null) {
                // Wrap jspFactory
                weldJspFactory = new WeldJspFactory(jspFactoryDelegate);
                JspFactory.setDefaultFactory(weldJspFactory);
            }
        }
        return weldJspFactory;
    }

}
