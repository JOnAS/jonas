/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2010 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.cdi.weld;

import javax.servlet.jsp.JspFactory;

import org.ow2.util.archive.api.IArchive;

/**
 * A {@code IWeldService} is some kind of utility service that takes care of Weld related
 * operations.
 *
 * @author Guillaume Sauthier
 */
public interface IWeldService {

    /**
     * Detect if the given {@code IArchive} contains a {@code beans.xml}.
     * Checked location includes:
     * <ul>
     * <li>{@code WEB-INF/beans.xml}</li>
     * <li>{@code META-INF/beans.xml}</li>
     * </ul>
     *
     * @param archive explored archive
     *
     * @return {@code true} if the given archive is CDI enabled, {@code false} otherwise.
     */
    boolean isCdiEnabled(IArchive archive);


    /**
     * Return a Weld-aware {@link javax.servlet.jsp.JspFactory} by wrapping the actual factory. If the actual JspFactory could not be
     * retrieve (for example if the JSP service was not already started) then returns {@code null}
     *
     * @return Weld-aware JspFactory or {@code null} if the JSP service is not available
     */
    JspFactory getJspFactory();

}
