/*
 * Copyright (C) 2012  Bull S. A. S.
 * Bull, Rue Jean Jaures, B.P.68, 78340, Les Clayes-sous-Bois
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation
 * version 2.1 of the License.
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA  02110-1301, USA.
 */

package org.ow2.jonas.cdi.weld.internal.easybeans;

import java.util.Collection;

import org.ow2.easybeans.api.Factory;

/**
 * This interface defines a registry for EasyBeans factories {@link Factory} allowing to retrieve factories for a given (deployment)
 * moduleId
 *
 * @author Loic Albertin
 */
public interface EZBFactoriesRegistry {

    /**
     * Registers a {@link Factory} based on its module name ({@link org.ow2.easybeans.api.EZBContainerConfig#getModuleName()}) and bean name
     * ({@link org.ow2.easybeans.api.bean.info.IBeanInfo#getName()}
     *
     * @param factory the factory to register
     */
    void registerBeanFactory(Factory<?, ?> factory);

    /**
     * Retrieves a factory based on a given module name and bean name.
     *
     * @param moduleName The given module name
     * @param beanName   The given module name
     *
     * @return The retrieved {@link Factory} or null if there is no corresponding factories
     */
    Factory<?, ?> getBeanFactory(String moduleName, String beanName);

    /**
     * Retrieves all factories registered for a given module name
     *
     * @param moduleName The given module name
     *
     * @return A collection of all registered factories for this module name
     */
    Collection<Factory<?, ?>> getFactories(String moduleName);

    /**
     * Un-register all factories for a given module name
     *
     * @param moduleName The given module name
     */
    void unregisterFactories(String moduleName);
}
