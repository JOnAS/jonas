/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2010 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.cdi.weld.internal.tomcat;

import org.apache.catalina.core.StandardContext;
import org.apache.felix.ipojo.annotations.Component;
import org.apache.felix.ipojo.annotations.Instantiate;
import org.apache.felix.ipojo.annotations.Provides;
import org.apache.felix.ipojo.annotations.Requires;
import org.jboss.weld.servlet.WeldListener;
import org.ow2.easybeans.persistence.api.EZBPersistenceUnitManager;
import org.ow2.jonas.cdi.weld.IWeldService;
import org.ow2.jonas.cdi.weld.internal.easybeans.EZBFactoriesRegistry;
import org.ow2.jonas.ejb3.IEasyBeansService;
import org.ow2.jonas.web.tomcat7.custom.ContextCustomizer;
import org.ow2.util.ee.deploy.api.deployable.WARDeployable;
import org.ow2.util.execution.ExecutionResult;
import org.ow2.util.execution.IExecution;
import org.ow2.util.execution.helper.RunnableHelper;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

/**
 * A {@code WeldContextCustomizer} will add to the Tomcat Context the appropriate
 * application and Context listeners.
 *
 * @author Guillaume Sauthier
 */
@Component
@Instantiate(name = "weld-context-customizer")
@Provides
public class WeldContextCustomizer implements ContextCustomizer {

    /**
     * Logger.
     */
    private static Log logger = LogFactory.getLog(WeldContextCustomizer.class);

    /**
     * Our weld service.
     */
    @Requires
    private IWeldService weld;

    /**
     * The EasyBeans service for EJBs related services
     */
    @Requires(optional = true, nullable = false)
    private IEasyBeansService easyBeansService;

    /**
     *  The EasyBeans factories registry
     */
    @Requires
    private EZBFactoriesRegistry factoriesRegistry;

    /**
     * Bind the WeldService reference.
     */
    public void setWeldService(final IWeldService weld) {
        this.weld = weld;
    }

    /**
     * Customize the given Catalina Context instance using the given WAR deployable.
     * This is called just before the Context start-up.
     *
     * @param context    Catalina Context to be customized
     * @param deployable the WAR being deployed
     */
    public void customize(final StandardContext context, final WARDeployable deployable) {

        // Currently, only standalone applications have their WARDeployable passed through services
        // If no deployable is here, just return
        // TODO Should be removed when CDI support will be wider (ejbjar, ear, ...)
        if (deployable == null) {
            return;
        }

        // Only handle CDI enabled web applications
        if (weld.isCdiEnabled(deployable.getArchive())) {
            EZBPersistenceUnitManager persistenceUnitManager = null;
            if (easyBeansService != null) {
                // Retrieve the EZBPersistenceUnitManager for this webapp if it contains EJBs
                final ClassLoader webappClassLoader = context.getParentClassLoader();
                IExecution<EZBPersistenceUnitManager> persistenceUnitManagerIExecution = new IExecution<EZBPersistenceUnitManager>() {
                    public EZBPersistenceUnitManager execute() throws Exception {
                        return easyBeansService.getPersistenceUnitManager(deployable.getArchive(), webappClassLoader);
                    }
                };
                ExecutionResult<EZBPersistenceUnitManager> persistenceUnitManagerExecutionResult =
                        new RunnableHelper<EZBPersistenceUnitManager>()
                                .execute(webappClassLoader, persistenceUnitManagerIExecution);
                if (persistenceUnitManagerExecutionResult.hasException()) {
                    logger.error("Unable to  retrieve persistence unit manager for context {0} CDI JPA services will be disabled",
                            context.getName(), persistenceUnitManagerExecutionResult.getException());
                } else {
                    persistenceUnitManager = persistenceUnitManagerExecutionResult.getResult();
                }
            }

            // Retrieve the module name used to retrieve EasyBeans factories
            String moduleName;
            if (deployable.getUnpackedDeployable() != null) {
                moduleName = deployable.getUnpackedDeployable().getModuleName();
            } else {
                moduleName = deployable.getModuleName();
            }
            // Add a Tomcat LifecycleListener to hook up during Context lifecycle events
            context.addLifecycleListener(new WeldLifeCycleListener(weld, moduleName, persistenceUnitManager, factoriesRegistry));

            // Add an application listener that handle scope
            // boundaries (application, session, request)
            context.addApplicationListener(WeldListener.class.getName());

        }
    }
}
