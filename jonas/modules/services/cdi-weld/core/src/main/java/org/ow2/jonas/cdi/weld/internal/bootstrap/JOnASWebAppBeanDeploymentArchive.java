/*
 * Copyright (C) 2012  Bull S. A. S.
 * Bull, Rue Jean Jaures, B.P.68, 78340, Les Clayes-sous-Bois
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation
 * version 2.1 of the License.
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA  02110-1301, USA.
 */

package org.ow2.jonas.cdi.weld.internal.bootstrap;

import java.util.Collection;
import java.util.HashSet;

import javax.servlet.ServletContext;

import org.jboss.weld.bootstrap.api.Bootstrap;
import org.jboss.weld.ejb.spi.EjbDescriptor;
import org.jboss.weld.environment.servlet.deployment.WebAppBeanDeploymentArchive;

/**
 * This class extends {@link WebAppBeanDeploymentArchive} to allow to store {@link EjbDescriptor}s
 * @author Loic Albertin
 */
public class JOnASWebAppBeanDeploymentArchive extends WebAppBeanDeploymentArchive {

    Collection<EjbDescriptor<?>> ejbDescriptors;

    public JOnASWebAppBeanDeploymentArchive(ServletContext servletContext, Bootstrap bootstrap) {
        super(servletContext, bootstrap);
        ejbDescriptors = new HashSet<EjbDescriptor<?>>();
    }


    @Override
    public Collection<EjbDescriptor<?>> getEjbs() {
        return ejbDescriptors;
    }
}
