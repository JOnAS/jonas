/*
 * Copyright (C) 2012
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation
 * version 2.1 of the License.
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA  02110-1301, USA.
 */
package org.ow2.jonas.cdi.weld.internal.services;

import org.jboss.weld.injection.spi.EjbInjectionServices;

import javax.ejb.EJB;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.naming.Context;
import javax.naming.NamingException;
import java.beans.Introspector;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * This class is responsive to resolve EJBs that are targeted by an {@link InjectionPoint}.
 *
 * @author Loic Albertin
 */
public class JOnASEjbInjectionServices implements EjbInjectionServices {

    private static final String JAVA_COMP_ENV = "java:comp/env";
    private Context context;

    public JOnASEjbInjectionServices(Context context) {
        this.context = context;
    }

    public Object resolveEjb(InjectionPoint injectionPoint) {
        EJB ejb = injectionPoint.getAnnotated().getAnnotation(EJB.class);
        if (ejb == null) {
            throw new IllegalStateException(
                    "Unable to resolve EJB reference as injection point is not annotated with @EJB");
        }
        // TODO when supporting other types than just war archives we probably should also check through the EZB JNDI resolver
        String lookupName;
        String mappedName = ejb.mappedName();
        String name = ejb.name();
        if (!mappedName.equals("")) {
            lookupName = mappedName;
        } else if (!name.equals("")) {
            lookupName = JAVA_COMP_ENV + "/" + name;
        } else {
            String propertyName;
            if (injectionPoint.getMember() instanceof Field) {
                propertyName = injectionPoint.getMember().getName();
            } else if (injectionPoint.getMember() instanceof Method) {
                propertyName = injectionPoint.getMember().getName();
                if (propertyName.startsWith("get")) {
                    propertyName = Introspector.decapitalize(propertyName.substring(3));
                } else if (propertyName.startsWith("is")) {
                    propertyName = Introspector.decapitalize(propertyName.substring(2));
                } else {
                    throw new IllegalArgumentException("Unable to inject into " + injectionPoint +
                            " as this method doesn't follow JavaBeans naming convention.");
                }
            } else {
                throw new IllegalArgumentException("Unable to inject into " + injectionPoint +
                        " which is neither a field nor a method");
            }
            String className = injectionPoint.getMember().getDeclaringClass().getName();
            lookupName = JAVA_COMP_ENV + "/" + className + "/" + propertyName;
        }
        try {
            return context.lookup(lookupName);
        } catch (NamingException e) {
            throw new RuntimeException("Error looking up " + lookupName + " in JNDI", e);
        }
    }

    public void cleanup() {
        // Nothing to do
    }
}
