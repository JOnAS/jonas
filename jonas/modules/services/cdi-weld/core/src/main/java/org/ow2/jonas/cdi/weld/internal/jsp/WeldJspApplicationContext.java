/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id: $
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.cdi.weld.internal.jsp;

import javax.el.ExpressionFactory;
import javax.enterprise.inject.spi.BeanManager;
import javax.servlet.jsp.JspApplicationContext;

/**
 * A {@code WeldJspApplicationContext} intercepts {@link #getExpressionFactory()} to
 * produces a CDI enabled {@link ExpressionFactory}.
 *
 * @author Guillaume Sauthier
 */
public class WeldJspApplicationContext extends ForwardingJspApplicationContext {

    /**
     * Instance to delegate to.
     */
    private JspApplicationContext delegate;

    private BeanManager manager;

    public WeldJspApplicationContext(final JspApplicationContext delegate,
                                     final BeanManager manager) {
        this.delegate = delegate;
        this.manager = manager;
    }

    @Override
    protected JspApplicationContext delegate() {
        return delegate;
    }

    @Override
    public ExpressionFactory getExpressionFactory() {
        // TODO Optimize this, maybe the ExpressionFactory is available earlier and could be stored as instance property ?
        return manager.wrapExpressionFactory(super.getExpressionFactory());
    }
}
