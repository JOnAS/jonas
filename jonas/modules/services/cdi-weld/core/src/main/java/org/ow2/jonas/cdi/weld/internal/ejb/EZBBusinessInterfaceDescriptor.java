/*
 * Copyright (C) 2012  Bull S. A. S.
 * Bull, Rue Jean Jaures, B.P.68, 78340, Les Clayes-sous-Bois
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation
 * version 2.1 of the License.
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA  02110-1301, USA.
 */

package org.ow2.jonas.cdi.weld.internal.ejb;

import org.jboss.weld.ejb.spi.BusinessInterfaceDescriptor;
import org.jboss.weld.resources.spi.ResourceLoader;

/**
 * This class implements the {@link BusinessInterfaceDescriptor} interface to represents the business interface of an EJB
 *
 * @author Loic Albertin
 */
public class EZBBusinessInterfaceDescriptor<T> implements BusinessInterfaceDescriptor<T> {

    /**
     * The interface class
     */
    private Class<T> interfaceClass;

    /**
     * @param interfaceClassName The class name
     * @param resourceLoader     a {@link ResourceLoader} used to load the class
     */
    public EZBBusinessInterfaceDescriptor(String interfaceClassName, ResourceLoader resourceLoader) {
        this.interfaceClass = (Class<T>) resourceLoader.classForName(interfaceClassName);
    }

    public Class<T> getInterface() {
        return interfaceClass;
    }
}
