/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id: $
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.cdi.weld.internal.jsp;

import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.jsp.JspApplicationContext;
import javax.servlet.jsp.JspEngineInfo;
import javax.servlet.jsp.JspFactory;
import javax.servlet.jsp.PageContext;

/**
 * A {@code ForwardingJspFactory} is ...
 *
 * @author Guillaume Sauthier
 */
public abstract class ForwardingJspFactory extends JspFactory {

    protected abstract JspFactory delegate();

    public PageContext getPageContext(Servlet servlet, ServletRequest servletRequest, ServletResponse servletResponse, String s, boolean b, int i, boolean b1) {
        return delegate().getPageContext(servlet, servletRequest, servletResponse, s, b, i, b1);
    }

    public void releasePageContext(PageContext pageContext) {
        delegate().releasePageContext(pageContext);
    }

    public JspEngineInfo getEngineInfo() {
        return delegate().getEngineInfo();
    }

    public JspApplicationContext getJspApplicationContext(ServletContext servletContext) {
        return delegate().getJspApplicationContext(servletContext);
    }
}
