/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id: $
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.cdi.weld.internal.jsp;

import javax.el.ELContextListener;
import javax.el.ELResolver;
import javax.el.ExpressionFactory;
import javax.servlet.jsp.JspApplicationContext;

/**
 * A {@code ForwardingJspApplicationContext} is ...
 *
 * @author Guillaume Sauthier
 */
public abstract class ForwardingJspApplicationContext implements JspApplicationContext {

    protected abstract JspApplicationContext delegate();

    public void addELContextListener(ELContextListener elContextListener) {
        delegate().addELContextListener(elContextListener);
    }

    public void addELResolver(ELResolver elResolver) throws IllegalStateException {
        delegate().addELResolver(elResolver);
    }

    public ExpressionFactory getExpressionFactory() {
        return delegate().getExpressionFactory();
    }
}
