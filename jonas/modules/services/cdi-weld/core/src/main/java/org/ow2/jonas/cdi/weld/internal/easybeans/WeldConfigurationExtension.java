/*
 * Copyright (C) 2012  Bull S. A. S.
 * Bull, Rue Jean Jaures, B.P.68, 78340, Les Clayes-sous-Bois
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation
 * version 2.1 of the License.
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA  02110-1301, USA.
 */

package org.ow2.jonas.cdi.weld.internal.easybeans;

import org.apache.felix.ipojo.annotations.Component;
import org.apache.felix.ipojo.annotations.Instantiate;
import org.apache.felix.ipojo.annotations.Provides;
import org.apache.felix.ipojo.annotations.Requires;
import org.ow2.easybeans.api.EZBConfigurationExtension;
import org.ow2.easybeans.api.EZBContainerConfig;
import org.ow2.easybeans.ejbinwar.EJBInWarArchive;
import org.ow2.jonas.cdi.weld.IWeldService;
import org.ow2.util.archive.api.IArchive;

/**
 * This is an {@link EZBConfigurationExtension} used to configure a {@link EZBContainerConfig}s if the application is CDI enabled.
 * It adds an {@link org.ow2.easybeans.api.EZBContainerLifeCycleCallback} used to populate the {@link EZBFactoriesRegistry} and a
 * {@link  org.ow2.easybeans.api.injection.ResourceInjector} used to inject CDI beans into EJBs.
 *
 * @author Loic Albertin
 */
@Component(immediate = true, name = "EasyBeans-Extension/Weld", publicFactory = false)
@Instantiate
@Provides
public class WeldConfigurationExtension implements EZBConfigurationExtension {

    /**
     * The JOnAS Weld service
     */
    @Requires
    private IWeldService weldService;

    /**
     * The factories registry
     */
    @Requires
    private EZBFactoriesRegistry factoriesRegistry;

    /**
     * An {@link org.ow2.easybeans.api.EZBContainerLifeCycleCallback}
     */
    private WeldLifecycleCallback weldLifecycleCallback;

    public WeldConfigurationExtension() {
        weldLifecycleCallback = new WeldLifecycleCallback(factoriesRegistry);
    }

    public void configure(EZBContainerConfig ezbContainerConfig) {
        // in case of EJBInWarArchive we should retrieve the original wrapped archive to be able to detect if it's a CDI application
        IArchive archive;
        if (ezbContainerConfig.getArchive() instanceof EJBInWarArchive) {
            EJBInWarArchive ejbInWarArchive = (EJBInWarArchive) ezbContainerConfig.getArchive();
            archive = ejbInWarArchive.getWrappedWarArchive();
        } else {
            archive = ezbContainerConfig.getArchive();
        }

        if (weldService.isCdiEnabled(archive)) {
            ezbContainerConfig.addCallback(weldLifecycleCallback);
            ezbContainerConfig.addInjectors(new WeldResourceInjector());
        }
    }
}
