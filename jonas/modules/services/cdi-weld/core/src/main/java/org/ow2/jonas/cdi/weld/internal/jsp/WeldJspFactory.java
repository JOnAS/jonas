/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id: $
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.cdi.weld.internal.jsp;

import javax.enterprise.inject.spi.BeanManager;
import javax.servlet.ServletContext;
import javax.servlet.jsp.JspApplicationContext;
import javax.servlet.jsp.JspFactory;

import org.jboss.weld.environment.servlet.Listener;

/**
 * A {@code WeldJspFactory} is ...
 *
 * @author Guillaume Sauthier
 */
public class WeldJspFactory extends ForwardingJspFactory {

    /**
     * Instance to delegate to.
     */
    private JspFactory delegate;

    public WeldJspFactory(final JspFactory delegate) {
        this.delegate = delegate;
    }

    @Override
    protected JspFactory delegate() {
        return delegate;
    }

    /**
     * Encapsulate default {@link JspApplicationContext} in a BeanManager-aware
     * JspApplicationContext (if the application is CDI-ready).
     * @param servletContext Stores the WebApplication's BeanManager
     * @return a wrapped {@link JspApplicationContext} or the default {@link JspApplicationContext}
     *         if application is not CDI-ready.
     */
    @Override
    public JspApplicationContext getJspApplicationContext(final ServletContext servletContext) {

        // Get default context
        JspApplicationContext jspApplicationContext = super.getJspApplicationContext(servletContext);
        // Lookup for the BeanManager's instance bound earlier in the deployment pipeline
        BeanManager manager = (BeanManager) servletContext.getAttribute(Listener.BEAN_MANAGER_ATTRIBUTE_NAME);

        if (manager != null) {
            // Wraps the default context
            return new WeldJspApplicationContext(jspApplicationContext, manager);
        }
        // non CDI-enabled applications
        return jspApplicationContext;
    }
}
