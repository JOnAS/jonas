/*
 * Copyright (C) 2012  Bull S. A. S.
 * Bull, Rue Jean Jaures, B.P.68, 78340, Les Clayes-sous-Bois
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation
 * version 2.1 of the License.
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA  02110-1301, USA.
 */

package org.ow2.jonas.cdi.weld.internal.services;

import org.jboss.weld.ejb.api.SessionObjectReference;
import org.jboss.weld.ejb.spi.EjbDescriptor;
import org.jboss.weld.ejb.spi.EjbServices;
import org.jboss.weld.ejb.spi.InterceptorBindings;
import org.ow2.easybeans.api.Factory;
import org.ow2.easybeans.api.bean.EasyBeansSLSB;
import org.ow2.easybeans.api.bean.EasyBeansSingletonSB;
import org.ow2.easybeans.container.session.singleton.SingletonSessionFactory;
import org.ow2.easybeans.container.session.stateful.StatefulSessionFactory;
import org.ow2.easybeans.container.session.stateless.StatelessSessionFactory;
import org.ow2.jonas.cdi.weld.internal.ejb.EZBEjbDescriptor;
import org.ow2.jonas.cdi.weld.internal.ejb.EZBSessionObjectReference;
import org.ow2.jonas.cdi.weld.internal.ejb.SFSBObjectReference;

/**
 * This class provides Services related to EJBs. It allows to resolve EJBs and register interceptors
 *
 * @author Loic Albertin
 */
public class JOnASEjbServices implements EjbServices {

    public JOnASEjbServices() {
    }

    public SessionObjectReference resolveEjb(EjbDescriptor<?> ejbDescriptor) {
        if (ejbDescriptor instanceof EZBEjbDescriptor) {
            EZBEjbDescriptor<?> ezbEjbDescriptor = (EZBEjbDescriptor<?>) ejbDescriptor;
            Factory<?, ?> factory = ezbEjbDescriptor.getFactory();
            if (factory != null) {
                if (ejbDescriptor.isStateful()) {
                    return new SFSBObjectReference((StatefulSessionFactory) factory);
                } else if (ejbDescriptor.isSingleton()) {
                    return new EZBSessionObjectReference<EasyBeansSingletonSB>((SingletonSessionFactory) factory);
                } else if (ejbDescriptor.isStateless()) {
                    return new EZBSessionObjectReference<EasyBeansSLSB>((StatelessSessionFactory) factory);
                }
            }
        }
        return null;
    }

    public void registerInterceptors(EjbDescriptor<?> ejbDescriptor, InterceptorBindings interceptorBindings) {
        if (ejbDescriptor instanceof EZBEjbDescriptor) {
            EZBEjbDescriptor<?> ezbEjbDescriptor = (EZBEjbDescriptor<?>) ejbDescriptor;
            Factory<?, ?> factory = ezbEjbDescriptor.getFactory();
            if (factory != null) {
                // TODO implement it
            }
        }
    }

    public void cleanup() {

    }
}
