/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 *  $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.cdi.weld.internal.easybeans;

import javax.enterprise.inject.spi.InjectionTarget;

/**
 * This class holds the result of an CDI injection in order to be retrieved later for proper disposal.
 *
 * @author Loic Albertin
 */
public class InjectionItemHolder {


    private InjectionTarget<?> injectionTarget;
    private Object instance;

    /**
     * @param injectionTarget The CDI {@link InjectionTarget}
     * @param instance        The CDI bean instance
     */
    public InjectionItemHolder(InjectionTarget<?> injectionTarget, Object instance) {
        this.injectionTarget = injectionTarget;
        this.instance = instance;
    }

    /**
     * @return The CDI {@link InjectionTarget}
     */
    public InjectionTarget<?> getInjectionTarget() {
        return injectionTarget;
    }

    /**
     * @return The CDI bean instance
     */
    public Object getInstance() {
        return instance;
    }
}
