/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.mail.internal.factory.mbean;

import java.util.Properties;

import org.ow2.jonas.lib.management.javaee.J2EEResource;
import org.ow2.jonas.lib.management.reconfig.PropertiesConfigurationData;
import org.ow2.jonas.mail.factory.JavaMail;


/**
 * JSR77 JavaMail Resource base implementation.
 * @author Adriana Danes
 */
public abstract class JavaMailResource extends J2EEResource {

    /**
     * Wrapped managed Java mail factory.
     */
    protected JavaMail mailFactory = null;
    /**
     * JavaMail resource constructor.
     * @param objectName MBean ObjectName
     * @param stateManageable
     * @param statisticsProvider
     * @param eventProvider
     * @param mailFactory
     */
    public JavaMailResource(final String objectName, final boolean stateManageable, final boolean statisticsProvider, final boolean eventProvider, final JavaMail mailFactory) {
        super(objectName, stateManageable, statisticsProvider, eventProvider);
        this.mailFactory = mailFactory;
    }

    /**
     * Get the name of the factory.
     * @return the name of the mail factory (the resource name as defined in jonas.properties)
     */
    public String getFactoryName() {
        return mailFactory.getFactoryName();
    }

    /**
     * Get the name of the factory.
     * @return name of the mail factory.
     */
    public String getName() {
        return mailFactory.getName();
    }

    /**
     * Set the name of the factory.
     * @param name name of the mail factory.
     */
    public void setName(final String name) {
        mailFactory.setName(name);
    }

    /**
     * Get the type of the factory.
     * @return type of the mail factory.
     */
    public String getFactoryType() {
        return mailFactory.getType();
    }

    /**
     * Get the authentication properties.
     * @return properties of the authentication.
     */
    public Properties getAuthenticationProperties() {
        return mailFactory.getAuthenticationProperties();
    }

    /**
     * Set the authentication properties.
     * @param props the authentication properties.
     */
    public void setAuthenticationProperties(final Properties props) {
        mailFactory.setAuthenticationProperties(props);
        notifyReconfiguration(props);
    }

    /**
     * Get the session properties.
     * @return the properties of Session.
     */
    public Properties getSessionProperties() {
        return mailFactory.getSessionProperties();
    }

    /**
     * Set the session properties.
     * @param props the Session properties.
     */
    public void setSessionProperties(final Properties props) {
        mailFactory.setSessionProperties(props);
        notifyReconfiguration(props);
    }

    /**
     * Gets the sequence number for MBeans operations
     * @return the sequence number for MBeans operations
     */
    protected abstract long getSequenceNumber();

    /**
     * Save updated configuration
     */
    public void saveConfig() {
        sendSaveNotification(getSequenceNumber(), getFactoryName());
    }

    protected void notifyReconfiguration(final Properties props) {
        // Here we do not reconfigure a sole property, but a group of properties in props
        PropertiesConfigurationData data = new PropertiesConfigurationData("", null);
        data.setProps(props);
        // Send a reconfiguration notification to the listener MBean
        sendReconfigNotification(getSequenceNumber(), getFactoryName(), data);
    }
}
