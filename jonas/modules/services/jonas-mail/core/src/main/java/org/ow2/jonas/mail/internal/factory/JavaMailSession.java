/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.mail.internal.factory;

//import java
import java.util.Properties;

//import javax
import javax.naming.BinaryRefAddr;
import javax.naming.NamingException;
import javax.naming.Reference;

import org.ow2.jonas.lib.util.JNDIUtils;
import org.ow2.jonas.lib.util.PropDump;
import org.ow2.jonas.mail.MailService;

//import objectweb.util
import org.objectweb.util.monolog.api.BasicLevel;

//import jonas

/**
 * This class provides a way for referencing mail session.
 * @author Florent Benoit
 * @author Ludovic Bert
 */
public class JavaMailSession extends AbstractJavaMail {

    /**
     * Type of the factory.
     */
    private static final String FACTORY_TYPE = "javax.mail.Session";


   /**
     * Constructor of a JMailSession Object.
     * @param factoryName the name of the factory.
     * @param name the jndi name
     * @param mailProperties properties for configuring this object.
     */
    public JavaMailSession(String factoryName, String name, Properties mailProperties, final MailService mailService) {
        super(factoryName, name, mailProperties, mailService);
    }

    /**
     * Return the type of the factory.
     * @return the type of the mail factory
     */
    public String getType() {
        return FACTORY_TYPE;
    }

    /**
     * Retrieves the Reference of the javax.mail.Session object.
     * The Reference contains the factory used to create this object
     * (that is the JavaMailSessionFactory) and the optional parameters used to
     * configure the factory.
     * @return the non-null Reference of the javax.mail.Session object.
     * @throws NamingException if a naming exception was encountered while
     * retrieving the reference.
     */
    public Reference getReference() throws NamingException {

       //Build the reference to the factory JMailSessionFactory
        Reference reference = new Reference(FACTORY_TYPE,
                                            JavaMailSessionFactory.class.getName(),
                                            null);
        if (getLogger().isLoggable(BasicLevel.DEBUG)) {
            PropDump.print("Here are these properties:", getMailSessionProperties(), getLogger(), BasicLevel.DEBUG);
        }

        //Give the session properties
        byte[] bytes = JNDIUtils.getBytesFromObject(getMailSessionProperties());
        if (bytes != null) {
            reference.add(new BinaryRefAddr("javaxmailSession.properties", bytes));
        }

        //Give the authentication properties
        bytes = JNDIUtils.getBytesFromObject(getAuthenticationProperties());
        if (bytes != null) {
            reference.add(new BinaryRefAddr("authentication.properties", bytes));
        }

        return reference;

    }


}
