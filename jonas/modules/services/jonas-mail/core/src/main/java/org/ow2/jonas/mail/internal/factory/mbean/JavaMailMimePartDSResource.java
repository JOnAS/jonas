/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.mail.internal.factory.mbean;

import java.util.Properties;

import org.ow2.jonas.mail.internal.factory.JavaMailMimePartDS;


/**
 * JSR 77 Implementation of the Java Mail MimePart DS Resource.
 * @author Adriana Danes
 */
public class JavaMailMimePartDSResource extends JavaMailResource {

    /**
     * Value used as sequence number by reconfiguration notifications
     */
    protected long sequenceNumber = 0;

    public JavaMailMimePartDSResource(String objectName, boolean stateManageable, boolean statisticsProvider, boolean eventProvider, JavaMailMimePartDS mailFactory) {
        super(objectName, stateManageable, statisticsProvider, eventProvider, mailFactory);
    }

   /**
     * Get the mimePartDatasource properties.
     * @return the mimePartDatasource properties
     */
    public Properties getMimeMessageProperties() {
        return ((JavaMailMimePartDS) mailFactory).getMimeMessageProperties();
    }

    /**
     * Set the mimePartDatasource properties.
     * @param props the mimePartDatasource properties
     */
    public void setMimeMessageProperties(Properties props) {
        ((JavaMailMimePartDS) mailFactory).setMimeMessageProperties(props);
        notifyReconfiguration(props);
    }

    /**
     * Gets the sequence number for MBeans operations.
     * @return the sequence number for MBeans operations
     */
    protected long getSequenceNumber() {
        return ++sequenceNumber;
    }
}
