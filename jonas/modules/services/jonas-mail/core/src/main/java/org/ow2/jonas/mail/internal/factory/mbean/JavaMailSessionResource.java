/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.mail.internal.factory.mbean;

import org.ow2.jonas.mail.internal.factory.JavaMailSession;

/**
 * This class implements specific management methods provided by the JavaMailSession resources.
 */
public class JavaMailSessionResource extends JavaMailResource {

    /**
     * Value used as sequence number by reconfiguration notifications.
     */
    private long sequenceNumber = 0;

    public JavaMailSessionResource(final String objectName,
                                   final boolean stateManageable,
                                   final boolean statisticsProvider,
                                   final boolean eventProvider,
                                   final JavaMailSession mailFactory) {
        super(objectName, stateManageable, statisticsProvider, eventProvider, mailFactory);
    }

    /**
     * Gets the sequence number for saveConfig opeartions.
     * @return the sequence number for saveConfig operations
     */
    protected long getSequenceNumber() {
        return ++sequenceNumber;
    }
}
