/**
 * JOnAS
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.mail.internal.osgi.configadmin;

import java.util.Collections;
import java.util.Dictionary;
import java.util.List;
import java.util.Properties;

import org.apache.felix.ipojo.annotations.Component;
import org.apache.felix.ipojo.annotations.Invalidate;
import org.apache.felix.ipojo.annotations.Property;
import org.apache.felix.ipojo.annotations.Requires;
import org.apache.felix.ipojo.annotations.Updated;
import org.apache.felix.ipojo.annotations.Validate;
import org.ow2.jonas.mail.MailService;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

@Component(name="MailMimePartDSFactory",propagation=true)
public class MailMimePartDSFactory {

    /**
     * Logger
     */
    private static Log logger = LogFactory.getLog(MailMimePartDSFactory.class);

    /**
     * Parameter : name
     */
    @Property(name="name", mandatory=true)
    private String name;

    /**
     *  List of properties
     */
    Properties props;

    /**
     * Service delegation
     */
    @Requires
    private MailService mailService;

    @SuppressWarnings("unchecked")
    @Updated
    public void updated(final Dictionary conf) {
        // The instance was reconfigured, conf is the new configuration.
        props = new Properties();
        logger.debug("{0} properties were injected",conf.size());

        List<String> keys = Collections.list(conf.keys());
        for (String key:keys) {

            String value = conf.get(key).toString();

            logger.debug("{0}={1}", key, value);

            // name is already retrieved as a mandatory property
            if (!key.equals("name")) {
                props.setProperty(key, value);
            }

        }

    }

    @Validate
    public void start() {
        try {
            // add the mandatory properties
            props.setProperty(MailService.PROPERTY_TYPE, MailService.MIMEPART_PROPERTY_TYPE);
            props.setProperty(MailService.PROPERTY_NAME, name);

            mailService.createMailFactory(name, props);

            logger.debug("Mail Mime Part DS Factory created {0}", name);

        } catch (Exception e) {
            logger.error("Error when creating the mail mime part ds factory {0} - exception={1}", name, e);
        }
    }

    @Invalidate
    public void stop() {

        try {

            mailService.unbindMailFactory(name);

            logger.debug("Mail Mime Part DS Factory unbound {0}", name);

        } catch (Exception e) {
            logger.error("Error when removing the mail mime part ds factory {0} - exception={1}", name, e);

        }
    }
}
