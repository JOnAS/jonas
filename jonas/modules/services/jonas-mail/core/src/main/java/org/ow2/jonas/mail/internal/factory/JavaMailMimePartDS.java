/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.mail.internal.factory;

//import java
import java.util.Properties;

import javax.naming.BinaryRefAddr;
import javax.naming.NamingException;
import javax.naming.Reference;

import org.objectweb.util.monolog.api.BasicLevel;
import org.ow2.jonas.lib.util.JNDIUtils;
import org.ow2.jonas.lib.util.PropDump;
import org.ow2.jonas.mail.MailService;

/**
 * This class provides a way for referencing mail session.
 * @author Florent Benoit
 * @author Ludovic Bert
 */
public class JavaMailMimePartDS extends AbstractJavaMail {

    /**
     * Type of the factory.
     */
    private static final String FACTORY_TYPE = "javax.mail.internet.MimePartDataSource";

    /**
     * Properties for the javax.mail.MimeMessage object.
     */
    private Properties messageProperties = null;

    /**
     * JOnAS-specific property for Mime Messages configuration (mail.to).
     */
    protected static final String MIMEMESSAGE_TO = "mail.to";

    /**
     * JOnAS-specific property for Mime Messages configuration (mail.cc).
     */

    protected static final String MIMEMESSAGE_CC = "mail.cc";

    /**
     * JOnAS-specific property for Mime Messages configuration (mail.bcc).
     */
    protected static final String MIMEMESSAGE_BCC = "mail.bcc";

    /**
     * JOnAS-specific property for Mime Messages configuration (mail.subject).
     */
    protected static final String MIMEMESSAGE_SUBJECT = "mail.subject";

    /**
     * Value used as sequence number by reconfiguration notifications
     */
    //protected long sequenceNumber = 0;

    /**
     * Return the type of the factory.
     * @return the type of the mail factory
     */
    public String getType() {
        return FACTORY_TYPE;
    }


    /**
     * Constructor of a JavaMailMimePartDS Object.
     * @param factoryName the name of the factory.
     * @param name the name of this object.
     * @param mailProperties properties for configuring this object.
     */
    public JavaMailMimePartDS(final String factoryName, final String name, final Properties mailProperties, final MailService mailService) {
        super(factoryName, name, mailProperties, mailService);

        PropDump.print("Received props:", mailProperties, getLogger(), BasicLevel.DEBUG);

        //Get the message properties
        messageProperties = new Properties();
        String propValue = null;
        propValue = (String) getMailSessionProperties().remove(MIMEMESSAGE_TO);
        if (propValue != null) {
            messageProperties.setProperty(MIMEMESSAGE_TO, propValue);
        }
        propValue = (String) getMailSessionProperties().remove(MIMEMESSAGE_CC);
        if (propValue != null) {
            messageProperties.setProperty(MIMEMESSAGE_CC, propValue);
        }
        propValue = (String) getMailSessionProperties().remove(MIMEMESSAGE_BCC);
        if (propValue != null) {
            messageProperties.setProperty(MIMEMESSAGE_BCC, propValue);
        }
        propValue = (String) getMailSessionProperties().remove(MIMEMESSAGE_SUBJECT);
        if (propValue != null) {
            messageProperties.setProperty(MIMEMESSAGE_SUBJECT, propValue);
        }

        if (getLogger().isLoggable(BasicLevel.DEBUG)) {
            PropDump.print("Message props:", messageProperties, getLogger(), BasicLevel.DEBUG);
        }
    }


    /**
     * Retrieves the mimeMessage properties of this object.
     * @return the mimeMessage properties of this object.
     */
    public Properties getMimeMessageProperties() {
        return messageProperties;
    }

    /**
     * Set the mimeMessage properties of this object.
     * @param props the mimeMessage properties
     */
    public void setMimeMessageProperties(Properties props) {
        this.messageProperties = props;
        try {
            //((MailService) ServiceManager.getInstance().getMailService()).recreateJavaMailFactory(this);
            getMailService().recreateJavaMailFactory(this);
        } catch (Exception e) {
            // should never occurs
        }
        //notifyReconfiguration(props);
        if (getLogger().isLoggable(BasicLevel.DEBUG)) {
            PropDump.print("These are the udated message props",
                    this.messageProperties, getLogger(), BasicLevel.DEBUG);
        }
    }


    /**
     * Retrieves the Reference of the javax.mail.MimePartDataSource object.
     * The Reference contains the factory used to create this object
     * (that is the JavaMimePartDSFactory) and the optional parameters used to
     * configure the factory.
     * @return the non-null Reference of the javax.mail.MimePartDataSource
     * object.
     * @throws NamingException if a naming exception was encountered while
     * retrieving the reference.
     */
    public Reference getReference() throws NamingException {

        //Build the reference to the factory JMailSessionFactory
        Reference reference = new Reference(FACTORY_TYPE,
                                            JavaMailMimePartDSFactory.class.getName(),
                                            null);
        byte[] bytes = null;

        //Give the session properties
        bytes = JNDIUtils.getBytesFromObject(getMailSessionProperties());
        if (bytes != null) {
            reference.add(new BinaryRefAddr("javaxmailSession.properties", bytes));
        }

        //Give the mime message properties
        bytes = JNDIUtils.getBytesFromObject(messageProperties);
        if (bytes != null) {
            reference.add(new BinaryRefAddr("javaxInternetMimeMessage.properties", bytes));
        }

        //Give the authentication properties
        bytes = JNDIUtils.getBytesFromObject(getAuthenticationProperties());
        if (bytes != null) {
            reference.add(new BinaryRefAddr("authentication.properties", bytes));
        }

        return reference;

    }
}
