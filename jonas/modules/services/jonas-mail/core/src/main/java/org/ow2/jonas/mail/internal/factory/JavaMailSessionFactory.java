/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.mail.internal.factory;

//import java
import java.util.Hashtable;
import java.util.Properties;

import javax.mail.Session;
import javax.naming.Context;
import javax.naming.Name;
import javax.naming.RefAddr;
import javax.naming.Reference;
import javax.naming.spi.ObjectFactory;

import org.ow2.jonas.lib.util.JNDIUtils;
import org.ow2.jonas.lib.util.Log;
import org.ow2.jonas.lib.util.PropDump;
import org.ow2.jonas.mail.internal.factory.lib.JAuthenticator;

import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * This class provides an implementation of a mail session factory for
 * sending mail.
 * @author Florent Benoit
 * @author Ludovic Bert
 */
public class JavaMailSessionFactory implements ObjectFactory {

    /**
     * The Java type for which this factory knows how to create objects.
     */
    protected static final String FACTORY_TYPE = "javax.mail.Session";

    /**
     * The logger used in JOnAS.
     */
    private static Logger logger = null;


    /**
     * Creates a javax.mail.Session object using the location or reference
     * information specified.
     * @param obj the possibly null object containing location or reference
     * information that can be used in creating an object.
     * @param name the name of this object relative to nameCtx, or null if no
     * name is specified.
     * @param nameCtx the context relative to which the name parameter is
     * specified, or null if name is relative to the default initial context.
     * @param environment the possibly null environment that is used in
     * creating the object.
     * @return a newly created javax.mail.Session object with the specific
     * configuration; null if an object cannot be created.
     * @throws Exception if this object factory encountered an exception
     * while attempting to create an object, and no other object factories
     * are to be tried.
     */
    public Object getObjectInstance(final Object obj, final Name name, final Context nameCtx,
                                    Hashtable environment) throws Exception {

        //Get the logger
        if (logger == null) {
            logger = Log.getLogger(Log.JONAS_MAIL_PREFIX);
        }

        //Get the reference
        Reference ref = (Reference) obj;

        //Get the class name
        String clname = ref.getClassName();

        //Check the class name
        if (!ref.getClassName().equals(FACTORY_TYPE)) {
            logger.log(BasicLevel.ERROR, "Cannot create object : required type is '" + FACTORY_TYPE + "', but found type is '" + clname + "'.");
            return (null);
        }

        Properties props = new Properties();
        Properties authenticationProps = new Properties();
        RefAddr refAddr = null;

        refAddr =  ref.get("javaxmailSession.properties");
        if (refAddr != null) {
            props = (Properties) JNDIUtils.getObjectFromBytes((byte[]) refAddr.getContent());
            if (logger.isLoggable(BasicLevel.DEBUG)) {
                PropDump.print("These are the properties attached to the Reference object used to construct a Session",
                           props, logger, BasicLevel.DEBUG);
            }
        }

        refAddr =  ref.get("authentication.properties");
        if (refAddr != null) {
            authenticationProps = (Properties) JNDIUtils.getObjectFromBytes((byte[]) refAddr.getContent());
            if (logger.isLoggable(BasicLevel.DEBUG)) {
                PropDump.print("These are the authentication properties used to construct a Authenticator",
                           authenticationProps, logger, BasicLevel.DEBUG);
            }
        }

        //username (Authentication)
        String mailAuthenticationUsername =  authenticationProps.getProperty("mail.authentication.username");

        //Password (Authentication)
        String mailAuthenticationPassword = authenticationProps.getProperty("mail.authentication.password");

        JAuthenticator jAuthenticator = null;
        if ((mailAuthenticationUsername != null) && (mailAuthenticationPassword != null)) {
            jAuthenticator = new JAuthenticator(mailAuthenticationUsername, mailAuthenticationPassword);
        }

        // Create and return a new Session object
        Session session = Session.getInstance(props, jAuthenticator);

        return (session);
    }

}
