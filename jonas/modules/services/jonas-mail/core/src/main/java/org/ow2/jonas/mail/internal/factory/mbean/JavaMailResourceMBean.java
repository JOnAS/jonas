/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id:JavaMailResourceMBean.java 10421 2007-05-21 16:47:41Z sauthieg $
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.mail.internal.factory.mbean;

//import java
import javax.management.MBeanException;
import javax.management.RuntimeOperationsException;

import org.apache.commons.modeler.BaseModelMBean;

/**
 * This interface defines all the management methods provided by the JavaMailResource objects.
 * @author Florent Benoit
 * @author Ludovic Bert
 * @author Adriana Danes:
 *  - Refactor code: rename the management methods (use straightforward names, as they are to be used in a Management Console)
 *  - J2EEManagement conformance
 *  - Use jakarta-commons modeler
 */
public class JavaMailResourceMBean extends BaseModelMBean {

    /**
     * Default ModelMBean Constructor.
     * @throws MBeanException rethrown from {@link BaseModelMBean} constructor.
     * @throws RuntimeOperationsException rethrown from {@link BaseModelMBean} constructor.
     */
    public JavaMailResourceMBean() throws MBeanException, RuntimeOperationsException {
        super();
    }

}

