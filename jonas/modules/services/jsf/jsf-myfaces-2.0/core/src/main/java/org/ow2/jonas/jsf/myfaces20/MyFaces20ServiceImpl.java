/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.jsf.myfaces20;

import java.net.URL;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.servlet.ServletContainerInitializer;

import org.apache.myfaces.ee6.MyFacesContainerInitializer;
import org.osgi.framework.BundleContext;
import org.ow2.jonas.jmx.JmxService;
import org.ow2.jonas.jsf.JSFService;
import org.ow2.jonas.lib.service.AbsServiceImpl;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.service.ServiceException;
import org.ow2.util.log.Log;
import org.ow2.util.log.LogFactory;

/**
 * Implementation of the service that is providing JSF 2.0 implementation.
 * @author Florent Benoit
 */
public class MyFaces20ServiceImpl extends AbsServiceImpl implements JSFService {

    /**
     * Logger.
     */
    private static Log logger = LogFactory.getLog(MyFaces20ServiceImpl.class);

    /**
     * Reference to the JMX service.
     */
    private JmxService jmxService = null;

    /**
     * OSGi Bundle context.
     */
    private BundleContext bundleContext = null;


    /**
     * Constructor in OSGi mode. It provides the bundle context.
     * @param bundleContext the given bundle context.
     */
    public MyFaces20ServiceImpl(final BundleContext bundleContext) {
        this.bundleContext = bundleContext;
    }

    /**
     * Abstract start-up method to be implemented by sub-classes.
     * @throws ServiceException service start-up failed
     */
    @SuppressWarnings("unchecked")
    @Override
    protected void doStart() throws ServiceException {

        // register mbeans-descriptors
        jmxService.loadDescriptors(getClass().getPackage().getName(), getClass().getClassLoader());

        // Register MBean
        try {
            jmxService.registerModelMBean(this, JonasObjectName.jsfService(getDomainName()));
        } catch (Exception e) {
            logger.warn("Cannot register MBean for validation service", e);
        }

        // Register the TLD URLs
        Enumeration<URL> e = bundleContext.getBundle().findEntries("META-INF", "*.tld", true);
        if (e != null) {
            while (e.hasMoreElements()) {
                URL url = e.nextElement();
                Dictionary<String, String> dictionary = new Hashtable<String, String>();
                dictionary.put("name", bundleContext.getBundle().getSymbolicName());
                dictionary.put("urltype", "tld");
                dictionary.put("value", url.toString());

                // for each url
                bundleContext.registerService(URL.class.getName(), url, dictionary);
            }
        }

        // Register the servlet initializer
        bundleContext.registerService(
                ServletContainerInitializer.class.getName(),
                new MyFacesContainerInitializer(),
                null);

    }


    /**
     * Abstract method for service stopping to be implemented by sub-classes.
     * @throws ServiceException service stopping failed
     */
    @Override
    protected void doStop() throws ServiceException {

        // Unregister MBean
        try {
            jmxService.unregisterModelMBean(JonasObjectName.jsfService(getDomainName()));
        } catch (Exception e) {
            logger.debug("Cannot unregister MBean for validation service", e);
        }



    }

    /**
     * @param jmxService the jmxService to set
     */
    public void setJmxService(final JmxService jmxService) {
        this.jmxService = jmxService;
    }


}
