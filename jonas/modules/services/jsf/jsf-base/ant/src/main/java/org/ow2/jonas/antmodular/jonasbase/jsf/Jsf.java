/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009-2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.antmodular.jonasbase.jsf;

import org.ow2.jonas.antmodular.jonasbase.bootstrap.AbstractJOnASBaseAntTask;
import org.ow2.jonas.antmodular.jonasbase.bootstrap.JTask;

/**
 * Defines properties for JSF service.
 *
 * @author Jeremy Cazaux
 */
public abstract class Jsf extends AbstractJOnASBaseAntTask {

    /**
     * Info for the logger.
     */
    protected static final String INFO = "[JSF] ";

    /**
     * Name of the property for changing the JSF class
     */
    protected final static String JSF_CLASS_PROPERTY = "jonas.service.jsf.class";

    /**
     * Execute all tasks
      */
    public void execute() {
        super.execute();
    }

    /**
     * Create a JReplace Task for changing service classname in jonas.properties.
     *
     * @param serviceName service classname to use.
     * @return Returns a JReplace Task.
     */
    protected JTask createServiceNameReplace(final String serviceName, final String info, final String confDir) {
        return super.createServiceNameReplace(serviceName, info, confDir, this.JSF_CLASS_PROPERTY);
    }
}
