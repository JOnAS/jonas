/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009-2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.antmodular.jonasbase.jsf.mojarra20;

import org.ow2.jonas.antmodular.jonasbase.jsf.Jsf;

/**
 * Defines properties for JSF Mojarra2.0 service.
 *
 * @author Jeremy Cazaux
 */
public class JsfMojarra20 extends Jsf {

    /**
     * Name of the implementation class for Mojarra1.2
     */
    private static final String MOJARRA20_SERVICE = "org.ow2.jonas.jsf.mojarra20.Mojarra20ServiceImpl";

    /**
     * Execute all tasks
     */
    @Override
    public void execute() {
        super.execute();
        super.createServiceNameReplace(this.MOJARRA20_SERVICE, this.INFO, this.destDir.getAbsolutePath() + this.CONF_DIR);
    }
}
