/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004-2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.antmodular.jonasbase.discovery.jgroups;

import org.ow2.jonas.antmodular.jonasbase.discovery.base.Discovery;

/**
 * Defines properties for discovery-jgroups service
 * @Jeremy Cazaux
 */
public class DiscoveryJGroups extends Discovery {

    /**
     * Name of the implementation class for JGroups.
     */
    private static final String JGROUPS_SERVICE = "org.ow2.jonas.discovery.jgroups.JgroupsDiscoveryServiceImpl";

    /**
     * Execute all tasks
     */
    public  DiscoveryJGroups() {
        super();
    }

     /**
     * Execute this task.
     */
    public void execute() {
        super.execute();
        super.createServiceNameReplace(this.JGROUPS_SERVICE, this.INFO, this.destDir.getAbsolutePath() + this.CONF_DIR);
    }
}
