/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.discovery.jgroups.manager;

import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;
import org.ow2.jonas.discovery.jgroups.comm.DiscCommManager;
import org.ow2.jonas.discovery.jgroups.comm.api.DiscComm;
import org.ow2.jonas.discovery.jgroups.comm.exception.StopDiscException;
import org.ow2.jonas.lib.util.Log;



/**
 * @author   eyindanga
 */
public class DiscoveryManager {
    /**
     *  Current logger.
     */
    private static Logger logger = Log.getLogger(Log.JONAS_DISCOVERY_PREFIX);

    /**
     *  Communication manager.
     */
    private DiscComm discoveryComm;
    /**
     *  Current discovery type.
     */
    private String discoverType = null;
    /**
     * Constructor.
     * @param discoveryType Host type.
     */
    public DiscoveryManager(final String discoveryType) {
        this.discoverType  = discoveryType;
    }
    /**
     * Free the memory.
     */
    private void freeMem(){
        discoveryComm = null;
    }
    /* (non-Javadoc)
     * @see org.ow2.jonas.discovery.jgroups.DiscRunnable#stop()
     */
    public void stop() {
        logger.log(BasicLevel.DEBUG, "Stopping discovery manager");
        if (discoveryComm != null) {
            discoveryComm.stop();
            freeMem();
        }
        logger.log(BasicLevel.DEBUG, "Discovery manager stopped successfully");
    }

    /**
     * Starts discovery communication.
     * @throws StopDiscException Discovery will stop.
     */
    public void start() throws StopDiscException {
        logger.log(BasicLevel.DEBUG, "Starting discovery manager");
        //discoverType would be: MASTER,SLAVE or CLUSTERD
        discoveryComm = new DiscCommManager(discoverType);
        try {
            discoveryComm.start();
        }catch (StopDiscException e) {
            freeMem();
            throw e;
        }
        logger.log(BasicLevel.DEBUG, "Discovery manager started successfully");

    }
}
