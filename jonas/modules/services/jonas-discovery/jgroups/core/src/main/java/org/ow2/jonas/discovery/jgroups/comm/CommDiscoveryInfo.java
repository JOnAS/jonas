package org.ow2.jonas.discovery.jgroups.comm;
/**
 * JOnAS: Java(TM) Open Application Server Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General  License as published by the Free Software Foundation;
 * either version 2.1 of the License, or any later version.
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General  License for more details.
 * You should have received a copy of the GNU Lesser General  License along with this library;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 * -------------------------------------------------------------------------- $Id$ --------------------------------------------------------------------------
 */

 /**
 * @author eyindanga
 *
 */
interface CommDiscoveryInfo {
    /**
     * @return
     * @uml.property  name="greetingListeningPort"
     */
     int getGreetingListeningPort();

    /**Gets Greeting port.
     * @param  greetingListeningPort Greeting port.
     * @uml.property  name="greetingListeningPort"
     */
     void setGreetingListeningPort(final int greetingListeningPort);

    /**Gets discovery port.
     * @return Discovery port.
     * @uml.property  name="listeningPort"
     */
     int getListeningPort();

    /**Sets discovery port.
     * @param  listeningPort Discovery port.
     * @uml.property  name="listeningPort"
     */
     void setListeningPort(final int listeningPort);

    /**Gets discovery IP.
     * @return Discovery IP.
     * @uml.property  name="listeningIp"
     */
     String getListeningIp();
    /**
     * Sets discovery IP.
     * @param  listeningIp Discovry IP.
     * @uml.property  name="listeningIp"
     */
     void setListeningIp(final String listeningIp);
    /**Sets ttl.
     * @param  ttl
     * @uml.property  name="timeToLive"
     */
     void setTimeToLive(final int ttl);
    /**
     * @return
     * @uml.property  name="timeToLive"
     */
     int getTimeToLive();

}
