/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

/**
 *
 */
package org.ow2.jonas.discovery.jgroups.utils;

import java.io.Serializable;

/**
 * @author eyindanga
 *
 */
public class Data implements Serializable {
    /**
     * Constant for DiscMessage objects type.
     */
    public static int DISC_MESSAGE = 0;
    /**
     * Constant for DiscEvent objects type.
     */
    public static int DISC_EVENT = 1;
    /**
     * Constant for DiscGreeting objects type.
     */
    public static int DISC_GREETING = 2;
    private static final long serialVersionUID = 9193522684840201133L;

    int type = 0;
    Serializable payload = null;


    public Data(int type, Serializable payload) {
        this.type = type;
        this.payload = payload;
    }


    /**
     * @return the payload
     */
    public Serializable getPayload() {
        return payload;
    }


    /**
     * @param payload the payload to set
     */
    public void setPayload(Serializable payload) {
        this.payload = payload;
    }


    /**
     * @return the type
     */
    public int getType() {
        return type;
    }


    /**
     * @param type the type to set
     */
    public void setType(int type) {
        this.type = type;
    }

}
