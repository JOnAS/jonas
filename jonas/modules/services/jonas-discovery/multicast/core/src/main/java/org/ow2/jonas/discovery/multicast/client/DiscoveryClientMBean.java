/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 2.1 of the License, or any later
 * version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */
package org.ow2.jonas.discovery.multicast.client;

import org.ow2.jonas.discovery.multicast.enroller.EnrollerMBean;

/**
 *
 * @author <a href="mailto:Takoua.Abdellatif@inria.fr">Takoua Abdellatif</a>
 * @version 1.0
 */
public interface DiscoveryClientMBean extends EnrollerMBean {
    /**
     * gets the duration time the DiscoveryClient has to wait for discovery answers.
     * @return timeout value.
     */
    int getTimeout();
    /**
     * Sets the timeout value the DiscoveryClient for receiving discovery answers.
     * @param timeout
     */
    void setTimeout(int timeout);
    /**
     * Gets the source port that the discovery response has to use.
     * @return source port
     */
    int getSourcePort();
    /**
     * Sets the source port
     * @param sourcePort
     */
    void setSourcePort(int sourcePort);
    /**
     * Gets the source port
     * @return the source port
     */
    String getSourceIp();
    /**
     * Sets the source Ip
     * @param sourceIp
     */
    void setSourceIp(String sourceIp);
}
