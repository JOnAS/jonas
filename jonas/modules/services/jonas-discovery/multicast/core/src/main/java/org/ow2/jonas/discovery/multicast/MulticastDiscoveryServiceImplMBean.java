/**
* JOnAS: Java(TM) Open Application Server
* Copyright (C) 2004 Bull S.A.
* Contact: jonas-team@ow2.org
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
* USA
*
* --------------------------------------------------------------------------
* $Id:MulticastDiscoveryServiceImplMBean.java 10967 2007-07-12 07:50:01Z eyindanga $
* --------------------------------------------------------------------------
*/

package org.ow2.jonas.discovery.multicast;

import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;

import org.ow2.jonas.discovery.base.DiscoveryServiceImplMBean;

/**
 * Management interface for the discovery service.
 * @author Adriana Danes
 * @author eyindanga
 *
 */
public interface MulticastDiscoveryServiceImplMBean extends DiscoveryServiceImplMBean {

     /**
     * Gets the MBean of disovery client.
     * @return
     */
    public ObjectName getDiscoveryClient();

    /**
     * Gets the MBean of disovery manager.
     * @return
     * @throws MalformedObjectNameException
     */
     public ObjectName getDiscoveryManager() throws MalformedObjectNameException;

     /**
      * Gets the MBean of the enroller.
      * @return
      * @throws MalformedObjectNameException
      */
     public ObjectName getEnroller() throws MalformedObjectNameException;

     /**
      * Gets the greeting listening port.
      * @return  the greetingListeningPort
      * @uml.property  name="greetingListeningPort"
      */
     public int getGreetingPort();

     /**
      * sets the greeting listening port.
      * @param greetingListeningPort  the greetingListeningPort to set
      * @uml.property  name="greetingListeningPort"
      */
     public void setGreetingPort( final int greetingListeningPort);

     /**
      * Gets the discovery source port.
      * @return  the sourcePort
      * @uml.property  name="sourcePort"
      */
     public int getSourcePort();

     /**
      * sets discovery source port.
      * @param sourcePort  the sourcePort to set
      * @uml.property  name="sourcePort"
      */
     public void setSourcePort(final int sourcePort);

}