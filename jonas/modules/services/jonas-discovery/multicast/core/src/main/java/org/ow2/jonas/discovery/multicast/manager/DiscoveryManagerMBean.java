/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 2.1 of the License, or any later
 * version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

package org.ow2.jonas.discovery.multicast.manager;

/**
 * @author <a href="mailto:Takoua.Abdellatif@inria.fr">Takoua Abdellatif </a>
 * @version 1.0
 */
public interface DiscoveryManagerMBean {

    /**
     * gets the time out period within which the host will listen for acknowledgement messages after sending a greeting.
     *
     * @return time out period within which the host will listen for acknowledgement messages after sending a greeting.
     */
    public int getGreetingAckTimeOut();

    /**
     * sets the time out period within which the host will listen for acknowledgement messages after sending a greeting.
     */
    public void setGreetingAckTimeOut(int listeningPort);

    /**
     * gets the port on which the DiscoveryManager is listening for a discovery greeting response.
     *
     * @return port to listen to for greeting responses.
     */
    public int getGreetingListeningPort();

    /**
     * sets the port on which the DiscoveryManager is listening for a discovery greeting response.
     */
    public void setGreetingListeningPort(int listeningPort);


    /**
     * gets the port on which the DiscoveryManager is listening for a discovery
     * request.
     *
     * @return notification port.
     */
    public int getListeningPort();

    /**
     * sets the port on which the DiscoveryManager is listening.
     */
    public void setListeningPort(int listeningPort);

    /**
     * @return IP address the DiscoveryManager uses.
     */
    public String getListeningIp();

    /**
     * sets the IP address the listening manager uses.
     */
    public void setListeningIp(String listeningIp);

    /**
     * sets the time to live value to ttl. Defines the number of hops the
     * multicast socket does.
     *
     * @param ttl
     */
    public void setTimeToLive(int ttl);

    /**
     * gets the time to live of the multicast socket.
     *
     * @return ttl value.
     */
    public int getTimeToLive();

    /**
     * starts the listening task on the listeningPort and listening Ip defined
     * with the parameters.
     */
    void start();

    void stop();
}