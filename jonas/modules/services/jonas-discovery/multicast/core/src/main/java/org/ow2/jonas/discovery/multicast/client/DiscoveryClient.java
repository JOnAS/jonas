/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 2.1 of the License, or any later
 * version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.discovery.multicast.client;

import java.net.UnknownHostException;

import javax.management.MBeanRegistration;
import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.ow2.jonas.lib.util.Log;
import org.ow2.jonas.lib.util.NetUtils;

import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * The DiscoveryClient is in charge of sending discovery messages on the LAN to
 * discover resources. It waits during a Timeout period of time for the
 * discovery answers (discovery messages containing a response).
 *
 * @author <a href="mailto:Takoua.Abdellatif@inria.fr">Takoua Abdellatif </a>
 * @version 1.0
 */
public class DiscoveryClient implements DiscoveryClientMBean, MBeanRegistration {

    /**
     * Source port of the sent discovery message.
     */
    private int sourcePort;

    /**
     * Source IP of the sent discovery message (address of the local host)
     */
    private String sourceIp;

    /**
     * My ObjectName
     */
    private ObjectName myOn;

    /**
     * Runnable object that sends a discovery message and than listens for
     * response of servers in the management domain.
     */
    private DiscoveryClientListener dcl = null;

    /**
     * Thread associated to dcl
     */
    private Thread discoveryClientListener = null;
    private int ttl = 1;

    /**
     * Time to wait for a response
     */
    private int timeout = 1000;
    private int listeningPort;
    private String listeningIp;

    /**
     * Discovery logger.
     */
    private static Logger logger = Log.getLogger(Log.JONAS_DISCOVERY_PREFIX);

    /**
     * Creates a new Discovery client.
     * @param listeningPort
     * @param listeningIP
     * @param sourcePort
     */
    public DiscoveryClient(int listeningPort, String listeningIP, int sourcePort) {
      this.listeningIp = listeningIP;
      this.listeningPort = listeningPort;
      this.sourcePort = sourcePort;
    }

    /**
     */
    public int getTimeout() {
        return timeout;
    }

    /**
     */
    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    /**
    */
    public int getListeningPort() {
        return listeningPort;
    }

    /**
      */
    public void setListeningPort(int listeningPort) {
        this.listeningPort = listeningPort;

    }

    /**
    */
    public String getListeningIp() {
        return listeningIp;
    }

    /**
     */
    public void setListeningIp(String ipAddress) {
        this.listeningIp = ipAddress;

    }

    /**
     */
    public int getSourcePort() {
        return sourcePort;
    }

    /**
    */
    public void setSourcePort(int sourcePort) {
        this.sourcePort = sourcePort;

    }

    /**
     */
    public String getSourceIp(){
        return sourceIp;
    }

    /**
     */
    public int getTimeToLive() {
        return ttl;
    }

    /**
     */
    public void setSourceIp(String sourceIp) {
        this.sourceIp = sourceIp;

    }

    /**
     *
      */
    public void setTimeToLive(int ttl) {
        this.ttl = ttl;

    }

    /**
      */
    public void start() {
        // create the thread in charge of sending the discovery message and waiting
        // for the responses
        // creates an instance of the DiscoveryClientListener class
        try {
            this.setSourceIp(NetUtils.getLocalAddress());
        } catch (UnknownHostException e) {
            logger.log(BasicLevel.ERROR, "Unable to create a localhost.", e);
        }
        dcl = new DiscoveryClientListener(this);

        if (discoveryClientListener == null) {
            discoveryClientListener = new Thread(dcl, "discoveryClientListener");
        }
        discoveryClientListener.start();

    }

    public void stop() {

    }

    /**
     *
     * @see javax.management.MBeanRegistration#preRegister(javax.management.MBeanServer,
     *      javax.management.ObjectName)
     */
    public ObjectName preRegister(MBeanServer mbeanServer, ObjectName on) {
        this.myOn = on;
        return myOn;
    }

    /**
     *
     * @see javax.management.MBeanRegistration#postRegister(java.lang.Boolean)
     */
    public void postRegister(Boolean arg0) {
        start();
    }

    /**
     *
     * @see javax.management.MBeanRegistration#preDeregister()
     */
    public void preDeregister() throws Exception {
        dcl.stop();

    }

    /**
     *
     * @see javax.management.MBeanRegistration#postDeregister()
     */
    public void postDeregister() {
    }

}