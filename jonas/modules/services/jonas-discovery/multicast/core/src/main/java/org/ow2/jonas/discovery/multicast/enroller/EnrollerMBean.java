/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 2.1 of the License, or any later
 * version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */
package org.ow2.jonas.discovery.multicast.enroller;

/**
 *
 * @author <a href="mailto:Takoua.Abdellatif@inria.fr">Takoua Abdellatif</a>
 * @version 1.0
 */
public interface EnrollerMBean {
    /**
     * gets the port on which the Enroller is listening for a discovery
     * request.
     *
     * @return notification port.
     */
    int getListeningPort();

    /**
     * sets the port on which the Enroller is listening.
     * @param listeningPort port on which the Enroller is listening.
     */
    void setListeningPort(int listeningPort);

    /**
     * @return IP address the Enroller uses.
     */
    String getListeningIp();

    /**
     * sets the IP address the listening manager uses.
     * @param listeningIP address on which the Enroller is listening.
     */
    void setListeningIp(String listeningIp);

    /**
     * sets the time to live value to ttl. Defines the number of hops the
     * multicast socket does.
     *
     * @param ttl time to live
     */
    void setTimeToLive(int ttl);

    /**
     * gets the time to live of the multicast socket.
     *
     * @return ttl value.
     */
    int getTimeToLive();

    /**
     * starts the listening task on the listeningPort and listening Ip defined
     * with the parameters.
     */
    void start();

    /**
     * stops the listening task
     */
    void stop();
}
