/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 2.1 of the License, or any later
 * version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

package org.ow2.jonas.discovery.multicast.enroller;

import javax.management.MBeanRegistration;
import javax.management.MBeanServer;
import javax.management.ObjectName;

/**
 * In charge of waiting for starting or stopping notifications and notifies the
 * appropriate listeners.
 *
 * @author <a href="mailto:Takoua.Abdellatif@inria.fr">Takoua Abdellatif </a>
 * @version 1.0
 */
public class Enroller implements MBeanRegistration, EnrollerMBean {

    /**
     * <code>listeningPort</code> for multicast socket creation
     */
    private int listeningPort;
    /**
     * <code>listeningIp</code> for multicast socket creation
     */
    private String listeningIp = null;
    private ObjectName    myOn;
    /**
     * Thread associated to dl
     */
    private Thread        discoveryListener = null;
    /**
     * Runnable object that listens on multicast address notifications sent
     * by the servers in the management domain
     */
    private DiscoveryListener dl = null;
    private int ttl =1;

    public Enroller (int listeningPort, String listeningIp) {
      this.listeningIp = listeningIp;
      this.listeningPort = listeningPort;
    }

    /**
     * @see javax.management.MBeanRegistration#preRegister(javax.management.MBeanServer,
     *      javax.management.ObjectName)
     */
    public ObjectName preRegister(MBeanServer mbeanServer, ObjectName on)
    throws Exception {
        this.myOn = on;
        return myOn;
    }

    /**
     * @see javax.management.MBeanRegistration#postRegister(java.lang.Boolean)
     */
    public void postRegister(Boolean arg0) {
        start();
    }

    /**
     *
     * @see javax.management.MBeanRegistration#preDeregister()
     */
    public void preDeregister() throws Exception {
        // stop the listening thread
        dl.stopListener();
        discoveryListener.interrupt();
        discoveryListener= null;
    }

    /**
     *
     * @see javax.management.MBeanRegistration#postDeregister()
     */
    public void postDeregister() {

    }

    /**
     */
    public int getListeningPort() {
        return listeningPort;
    }

    /**
     */
    public void setListeningPort(int listeningPort) {
        this.listeningPort = listeningPort;

    }

    /**
     */
    public String getListeningIp() {
        return listeningIp;
    }

    /**
    */
    public void setListeningIp(String listeningIp) {
        this.listeningIp = listeningIp;

    }

    /**
     */
    public void setTimeToLive(int ttl) {
        this.ttl = ttl;

    }

    /**
     */
    public int getTimeToLive() {
        return ttl;
    }

    /**
     */
    public void start() {
        // creates an instance of the Discovery communication class
        dl = new DiscoveryListener(this);

        if (discoveryListener == null)
            discoveryListener = new Thread(dl, "discoveryListener");
        discoveryListener.start();

    }

    /**
     */
   public void stop() {
       dl.stopListener();
   }

}