/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 2.1 of the License, or any later
 * version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.discovery.base.comm;

import org.ow2.jonas.discovery.DiscoveryState;

/**
 * This class represents a special greeting message which has two uses: 1)
 * To act as a broadcast to the domain that a new server with a given server name has started.
 * It also includes a port that the server will listen to objections at, the domain name and state set to STARTUP. 2)
 * To inform the newly started server that server name is already in use by another server.
 * @author  Vivek Lakshmanan
 * @version  1.0
 */
public class DiscGreeting extends DiscMessage {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * @uml.property  name="state"
     */
    private String state = null;

    /**
     * @uml.property  name="serverName"
     */
    private String serverName = null;

    /**
     * @uml.property  name="domainName"
     */
    private String domainName = null;

    /**
     * @uml.property  name="serverId"
     */
    private String serverId = null;

    /**Constructor.
     * @param sourceAddress Discovery address
     * @param sourcePort Discovery source port
     */
    public DiscGreeting(final String sourceAddress, final int sourcePort) {
        super(sourceAddress, sourcePort);
    }

    /**
     * Constructor for a Discovery Greeting.
     * @param sourceAddress
     *          the host address to use to receive a response.
     * @param sourcePort
     *          is the port used in the case of a point to point response.
     * @param serverName
     *          is Jonas server name.
     * @param domainName
     *          is Jonas domain name.
     * @param startingUp
     *          is this server starting up or is it reporting back with a
     *          notification stating that it owns this server name?
     * @param serverId TODO
     */
    public DiscGreeting(final String sourceAddress, final int sourcePort,
            final String serverName, final String domainName, final boolean startingUp,
            final String serverId) {
        super(sourceAddress, sourcePort);
        this.serverName = serverName;
        this.domainName = domainName;
        this.serverId = serverId;

        if (startingUp) {
            this.state = DiscoveryState.STARTUP;
        } else {
            this.state = DiscoveryState.DUPLICATE_NAME;
        }

    }

    /**
     * returns server name.
     * @return  serverName
     * @uml.property  name="serverName"
     */
    public String getServerName() {
        return serverName;
    }

    /**
     * Gets domain name.
     * @return  domain name.
     * @uml.property  name="domainName"
     */
    public String getDomainName() {
        return domainName;
    }

    /**
     * Sets the domain name.
     * @param domainName  the management domain name
     * @uml.property  name="domainName"
     */
    public void setDomainName(final String domainName) {
        this.domainName = domainName;
    }

    /**
     * Sets the serverName.
     * @param serverName  the name of the server sending the discovery greeting
     * @uml.property  name="serverName"
     */
    public void setServerName(final String serverName) {
        this.serverName = serverName;
    }

    /**Gets host state.
     * @return  server state.
     * @uml.property  name="state"
     */
    public String getState() {
        return state;
    }

    /**
     * Sets the server state : STARTINGUP or DUPLICATE_NAME.
     * @param state  state of the server sending the discovery greeting, one of DiscGreeting.
     * STARTUP or DiscGreeting.DUPLICATE_NAME
     * @uml.property  name="state"
     */
    public void setState(final String state) {
        this.state = state;
    }

    /**
     * The string version of the message.
     * @return the message
     */
    public String toString() {
        String str = super.toString() + " State="
        + state + " DomainName=" + domainName + " ServerName= " + serverName + " serverId= " + serverId;
        return str;

    }
    /**
     * @return  serverId
     * @uml.property  name="serverId"
     */
    public String getServerId() {
        return serverId;
    }

    /**
     * Sets the serverId.
     * @param serverId  value of the serverId to set
     * @uml.property  name="serverId"
     */
    public void setServerId(final String serverId) {
        this.serverId = serverId;
    }
}
