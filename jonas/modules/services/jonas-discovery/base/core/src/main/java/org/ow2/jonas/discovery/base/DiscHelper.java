/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.discovery.base;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.ow2.jonas.discovery.base.comm.DiscEvent;
import org.ow2.jonas.discovery.base.comm.DiscGreeting;
import org.ow2.jonas.discovery.base.comm.DiscMessage;

/**
 * Discovery Helper. Convert objects to bytes and vice versa.
 * @author eyindanga
 *
 */
public class DiscHelper {
    /**
     * Constant for DiscMessage objects type.
     */
    private static int DISC_MESSAGE = 0;
    /**
     * Constant for DiscEvent objects type.
     */
    private static int DISC_EVENT = 1;

    /**
     * Constant for DiscGreeting objects type.
     */
    private static int DISC_GREETING = 2;

    /**
     * Construct a byte[] containing type info about a DiscMessage object to be sent,
     * plus the content of this message.
     * @param obj Object to be send. Only supported DiscMessage objects.
     * @return Null if the object is not an instance of DiscMessage or one of its subclasses.
     * @throws IOException Could not create an ObjectOutputStream to write into the
     * underlying ByteArrayOutputStream.
     */
    public static byte[] objectToBytes(final Object obj) throws IOException {
        byte[] resultBytes = null;
        if (!(obj instanceof DiscMessage)) {
            return null;
        }
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        ObjectOutputStream stream = new ObjectOutputStream(byteStream);
        try {
            if (obj instanceof DiscEvent) {
                stream.writeInt(DISC_EVENT);
            } else if (obj instanceof DiscGreeting) {
                stream.writeInt(DISC_GREETING);
            } else {
                stream.writeInt(DISC_MESSAGE);
            }
            stream.writeObject(obj);
            resultBytes = byteStream.toByteArray();
            byteStream.close();
            stream.close();
        } catch (IOException e) {
            throw e;
        } finally {
            byteStream.close();
            stream.close();
        }
        return resultBytes;
    }

    /**
     * Gets <code>DiscEvenet</code>, <code>DiscMessage</code>, or <code>DiscGreeting</code> from bytes array.
     * @param bytes  byte[] containing a received message
     * @return Null if the object in the received message is not of one of the known types, or the object which
     * have been sent.
     * @throws IOException Could not create an ObjectInputStream to read in it
     * @throws ClassNotFoundException Class of a serialized object cannot be found
     */
    public static Object bytesToObject(final byte[] bytes) throws IOException, ClassNotFoundException {
        Object resultObject = null;
        ByteArrayInputStream byteStream = new ByteArrayInputStream(bytes);
        ObjectInputStream stream = null;
        try {
            stream = new ObjectInputStream(byteStream);
            int type = stream.readInt();
            if (type == DISC_MESSAGE) {
                resultObject = (DiscMessage) stream.readObject();
            } else if (type == DISC_EVENT) {
                resultObject = (DiscEvent) stream.readObject();
            } else if (type == DISC_GREETING) {
                resultObject = (DiscGreeting) stream.readObject();
            }
        } catch (IOException e) {
            throw e;
        } catch (ClassNotFoundException e) {
            throw e;
        } finally {
            byteStream.close();
            stream.close();
        }
        return resultObject;
    }
}
