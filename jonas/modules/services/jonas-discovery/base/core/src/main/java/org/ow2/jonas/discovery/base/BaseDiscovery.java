/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id:BaseDiscovery.java 10967 2007-07-12 07:50:01Z eyindanga $
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.discovery.base;

import java.util.ArrayList;

import org.ow2.jonas.discovery.DiscoveryService;
import org.ow2.jonas.jmx.JmxService;
import org.ow2.jonas.lib.service.AbsServiceImpl;

/**
 * This class is inherited by all discovery Implementations It provides common discovery operations.
 * @author  eyindanga
 */

public abstract class BaseDiscovery extends AbsServiceImpl implements DiscoveryService , DiscoveryServiceImplMBean {

    /**
     * The time period in miliseconds to listen for greeting responses.
     * @uml.property  name="greetingAckTimeOut"
     */
    private int greetingAckTimeOut;

    /**
     * Default Time-To-Live (unit ? sec/ms ?).
     */
    private static final int DISCOVERY_TTL_DEFAULT = 1;

    /**
     * Time-To-Live.
     * @uml.property  name="ttl"
     */
    private int ttl = DISCOVERY_TTL_DEFAULT;

    /**
     * This attribute is injected.
     * set to true if enroller and discoveryClient started.
     * @uml.property  name="isDiscoveryMaster"
     */
    private boolean isDiscoveryMaster = false;

    /**
     * The {@link JmxService} instance.
     */
    private JmxService jmxService =  null;

    /**
     * Array list of urls retrived from jmxService.
     */
    protected ArrayList urlsList;

    /**
     * @return  the greetingAckTimeOut
     * @uml.property  name="greetingAckTimeOut"
     */
    public int getGreetingTimeout() {
        return greetingAckTimeOut;
    }

    /**
     * Set a new timeout.
     * @param greetingAckTimeOut new greeting timeout
     */
    public void setGreetingTimeout(final int greetingAckTimeOut) {
        this.greetingAckTimeOut = greetingAckTimeOut;
    }

    /**
     * @return  the isDiscoveryMaster
     * @uml.property  name="isDiscoveryMaster"
     */
    public boolean getIsDiscoveryMaster() {
        return isDiscoveryMaster;
    }

    /**
     * @param discoveryMaster the isDiscoveryMaster
     * @uml.property  name="isDiscoveryMaster"
     */
    public void setMaster(final boolean discoveryMaster) {
        isDiscoveryMaster = discoveryMaster;
    }

    /**
     * @return  the listeningIp
     * @uml.property  name="listeningIp"
     */
    public abstract String getListeningIp();

    /**
     * @param listeningIp  the listeningIp to set
     * @uml.property  name="listeningIp"
     */
    public abstract void setListeningIp(final String listeningIp);

    /**
     * @return  the listeningPort
     * @uml.property  name="listeningPort"
     */
    public abstract int getListeningPort();

    /**
     * @param listeningPort  the listeningPort to set
     * @uml.property  name="listeningPort"
     */
    public abstract void setListeningPort(final int listeningPort);

    /**
     * @return  the ttl
     * @uml.property  name="ttl"
     */
    public int getTtl() {
        return ttl;
    }

    /**
     * @param ttl  the ttl to set
     * @uml.property  name="ttl"
     */
    public void setTtl(final int ttl) {
        this.ttl = ttl;
    }

    /**
     * @return the jmxService
     */
    public JmxService getJmxService() {
        return jmxService;
    }

    /**
     * Sets the jmxService.
     * @param jmxService the jmxService to set. Used by the injector
     */
    public void setJmxService(final JmxService jmxService) {
        this.jmxService = jmxService;
    }

    /**
     * @return the urlsList
     */
    public abstract ArrayList getUrlsList();

    /**
     * @param urlsList the urlsList to set
     */
    public abstract void setUrlsList(final ArrayList urlsList);

}
