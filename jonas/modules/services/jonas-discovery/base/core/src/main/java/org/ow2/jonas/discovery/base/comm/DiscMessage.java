/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 2.1 of the License, or any later
 * version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.discovery.base.comm;

import java.io.Serializable;

/**
 * @author  <a href="mailto:Takoua.Abdellatif@inria.fr">Takoua Abdellatif </a>
 * @author  Adriana Danes
 * @version  1.1
 */
public class DiscMessage implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
    * The protocol version for the Discovery messages.
    */
    public static final String DISCOVERY_PROTOCOL_VERSION = "1.1";

    /**
     * The version for this message.
     * @uml.property  name="version"
     */
    private String version;

    /**
     * @uml.property  name="sourceAddress"
     */
    private String sourceAddress;

    /**
     * @uml.property  name="sourcePort"
     */
    private int sourcePort;

    /**
     * Creates a new <code>DiscoveryRequest</code>.
     * @param sourceAddress
     *          source address to use to send discovery responses.
     * @param sourcePort
     *          source port to use to send disovery responses.
     *
     */
    public DiscMessage(final String sourceAddress, final int sourcePort) {
        this.sourceAddress = sourceAddress;
        this.sourcePort = sourcePort;
        this.version = DISCOVERY_PROTOCOL_VERSION;
    }

    /**
     * Returns the destinationAddress value in String type.
     * @return  Returns the destinationAddress.
     * @uml.property  name="sourceAddress"
     */
    public String getSourceAddress() {
        return sourceAddress;
    }

    /**
     * Returns the sourcePort value.
     * @return  Returns the sourcePort.
     * @uml.property  name="sourcePort"
     */
    public int getSourcePort() {
        return sourcePort;
    }

    /**
     * Sets the source address.
     * @param  sourceAddress Discovery source address.
     * @uml.property  name="sourceAddress"
     */
    public void setSourceAddress(final String sourceAddress) {
        this.sourceAddress = sourceAddress;
    }

    /**
     * Sets the source port.
     * @param  sourcePort Discovery source port
     * @uml.property  name="sourcePort"
     */
    public void setSourcePort(final int sourcePort) {
        this.sourcePort = sourcePort;
    }

    /**
     * @return the object trandformed into a String
     */
    public String toString() {
        String messageString = null;
        messageString = "SourceAddress= " + sourceAddress + " SourcePort= " + sourcePort;
        return messageString;
    }

    /**
     * @return  discovery protocol version
     * @uml.property  name="version"
     */
    public String getVersion() {
        return version;
    }

}