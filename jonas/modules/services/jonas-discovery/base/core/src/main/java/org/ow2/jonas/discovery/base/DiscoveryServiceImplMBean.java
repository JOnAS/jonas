/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id:MulticastDiscoveryServiceImplMBean.java 10967 2007-07-12 07:50:01Z eyindanga $
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.discovery.base;

import java.util.ArrayList;

import javax.management.JMException;
import javax.management.ObjectName;

import org.ow2.jonas.service.ServiceException;

/**
 * Management interface for the discovery service.
 * @author Adriana Danes
 * @author eyindanga
 *
 */
public interface DiscoveryServiceImplMBean {
    /**
     * @return the time-to-live of the discovery multicast
     */
    String getDiscoveryTtl();
    /**
     * Make this server a discovery master.
     * @throws JMException a JMX exception occured when trying to make current server a discovery master
     */
    void startDiscoveryMaster() throws JMException;
    /**
     * @return  the ttl
     * @uml.property  name="ttl"
     */
    int getTtl();
    /**
     * @param ttl  the ttl to set
     * @uml.property  name="ttl"
     */
    void setTtl(final int ttl);
    /**
     * @return the urlsList
     */
    ArrayList getUrlsList();
    /**
     * @param urlsList the urlsList to set
     */
    void setUrlsList(final ArrayList urlsList);
    /**
     * @param isDiscoveryMaster : true if discovery is master.
     */
    void setMaster(final boolean isDiscoveryMaster);

    /**
     * @return the discovery protocol version number
     */
    String getDiscoveryProtocolVersion();

    /**
     * @throws ServiceException If discovery service cannot be started.
     */
    void start() throws ServiceException;

    /**
     * Stops discovery.
     */
    void stop();
    /**
     * @return  the greetingAckTimeOut
     */
    int getGreetingTimeout();
    /**
     * @param greetingAckTimeOut  the greetingAckTimeOut to set
     */
    void setGreetingTimeout(int greetingAckTimeOut);
    /**
     * @return  the listeningIp
     */
    String getListeningIp();
    /**
     * @param listeningIp  the listeningIp to set
     */
    void setListeningIp(String listeningIp);
    /**
     * @return  the listeningPort
     */
    int getListeningPort();
    /**
     * @param listeningPort  the listeningPort to set
     */
    void setListeningPort(int listeningPort);
    /**
     * @return  the domainName
     */
    String getDomainName();
    /**
     * @param domainName  the domainName to set
     */
    void setDomainName(String domainName);
    /**
     * @return  true if discovery is master
     */
    boolean getIsDiscoveryMaster();
    /**
     * @return  the jonasName
     */
    String getJonasName();
    /**
     * @param jonasName  the jonasName to set
     */
    void setJonasName(String jonasName);
    /**
     * @return  the myOn
     */
    ObjectName getMyOn();
    /**
     * @param myOn  the myOn to set
     */
    void setMyOn(ObjectName myOn);
    /**
     * @return  the serverId
     */
    String getServerId();
    /**
     * @param serverId  the serverId to set
     */
    void setServerId(String serverId);
    /**
     * @return  the urls
     */
    String[] getUrls();
    /**
     * @param urls  the urls to set
     */
    void setUrls(String[] urls);
    /**
     * @return the multicast group IP address used by the discovery service
     */
    String getMulticastAddress();
    /**
     * @return the multicast group port number used by the discovery service
     */
    String getMulticastPort();
}