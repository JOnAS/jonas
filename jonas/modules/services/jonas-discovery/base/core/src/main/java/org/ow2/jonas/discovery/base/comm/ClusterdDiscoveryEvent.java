/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

/**
 *
 */
package org.ow2.jonas.discovery.base.comm;

/**
 * Discovery Event sent by a cluster daemon. It's necessary to separate server comunication and cluster daemon communication
 * Since actions that will be performed by a master discovery when receiving this kind of message
 * are different from the ones performed when receiving <code>DiscEvent</code>
 * @author eyindanga
 *
 */
public class ClusterdDiscoveryEvent extends DiscEvent {

    /**Constructor.
     * @param sourceAddress Discovery IP.
     * @param sourcePort Discovery port
     * @param serverId Server ID
     */
    public ClusterdDiscoveryEvent(final String sourceAddress, final int sourcePort,
            final String serverId) {
        super(sourceAddress, sourcePort, serverId);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param discIp Discovery IP.
     * @param discPort Discovery port
     * @param hostName Host name
     * @param domainName Domain name
     * @param serverId Server ID
     * @param urls Connector urls of discovery host.
     */
    public ClusterdDiscoveryEvent(final String discIp, final Integer discPort,
            final String hostName, final String domainName, final String serverId, final String[] urls) {
        super(discIp, discPort, hostName, domainName, serverId, urls);
        // TODO Auto-generated constructor stub
    }

    /**
     *Serial version ID.
     */
    private static final long serialVersionUID = 1L;

}
