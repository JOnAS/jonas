/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): ____________________________________.
 * Contributor(s): ______________________________________.
 *
 *
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.discovery.base;

import org.ow2.jonas.service.ServiceException;

/**
 * This exception is thrown when a pre-existing server with the same server name is detected.
 * @author: Vivek Lakshmanan
 */
public class DuplicateServerNameException extends ServiceException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * Default constructor.
     */
    public DuplicateServerNameException() {
        super("A server with the given name already exists in the domain.");
    }

    /**
     * Default constructor.
     */
    public DuplicateServerNameException( String serverId, String srvName, String domainName) {
        super("A server with name: "+serverId+"the Id: "+srvName+"is already started in domain: "+domainName);
    }
}
