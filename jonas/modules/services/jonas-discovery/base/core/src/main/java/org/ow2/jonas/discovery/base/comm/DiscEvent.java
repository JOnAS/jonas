/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 2.1 of the License, or any later
 * version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.discovery.base.comm;

import org.ow2.jonas.discovery.DiscoveryEvent;


/**
 * @author  <a href="mailto:Takoua.Abdellatif@inria.fr">Takoua Abdellatif </a>
 * @author  Adriana Danes
 * @version  1.0
 */
public class DiscEvent extends DiscMessage implements DiscoveryEvent {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * @uml.property  name="serverName"
     */
    private String serverName = null;

    /**
     * @uml.property  name="domainName"
     */
    private String domainName = null;

    /**
     * @uml.property  name="state"
     */
    private String state = null;

    /**
     * @uml.property  name="serverId"
     */
    private String serverId = null;


    /**
     *Connector urls.
     */
    private String[] urls = null;


    /**
     * True if discovery is master.
     */
    private boolean  isDiscoveryMaster = false;

    /**Constructor.
     * @param sourceAddress Discovery address.
     * @param sourcePort Discovery port.
     * @param serverId The serverId for this server.
     */
    public DiscEvent(final String sourceAddress, final int sourcePort, final String serverId) {
        super(sourceAddress, sourcePort);
    }

    /**
     * Constructor for a Discovery Event.
     * @param sourceAddress
     *          the host address to use to receive a response.
     * @param sourcePort
     *          is the port used in the case of a point to point response.
     * @param serverName
     *          is Jonas server name.
     * @param domainName
     *          is Jonas domain name.
     * @param serverId TODO
     * @param connectorURLs
     *          contains the list of all connector urls registered in the mbean
     *          server.
     */
    public DiscEvent(final String sourceAddress, final int sourcePort, final String serverName,
            final String domainName, final String serverId, final String[] connectorURLs) {
        super(sourceAddress, sourcePort);
        this.serverName = serverName;
        this.domainName = domainName;
        this.serverId = serverId;
        this.urls = connectorURLs;
    }



    /**
     * Constructor for a Discovery Event.
     * @param sourceAddress
     *          the host address to use to receive a response.
     * @param sourcePort
     *          is the port used in the case of a point to point response.
     * @param serverName
     *          is Jonas server name.
     * @param domainName
     *          is Jonas domain name.
     * @param serverId TODO
     * @param connectorURLs
     *          contains the list of all connector urls registered in the mbean
     *          server.
     *@param isMaster
     *          True id the discovery is master
     */
    public DiscEvent(final String sourceAddress, final int sourcePort, final String serverName,
            final String domainName, final String serverId, final String[] connectorURLs, final boolean isMaster) {
        super(sourceAddress, sourcePort);
        this.serverName = serverName;
        this.domainName = domainName;
        this.serverId = serverId;
        this.urls = connectorURLs;
        this.isDiscoveryMaster = isMaster;
    }

    /**
     * returns server name.
     * @return  serverName
     * @uml.property  name="serverName"
     */
    public String getServerName() {
        return serverName;
    }

    /**
     * returns domain name.
     * @return  domain name.
     * @uml.property  name="domainName"
     */
    public String getDomainName() {
        return domainName;
    }

    /**
     * sets the domain name.
     * @param domainName  the management domain name
     * @uml.property  name="domainName"
     */
    public void setDomainName(final String domainName) {
        this.domainName = domainName;
    }

    /**
     * sets the serverName.
     * @param serverName  the name of the server sending the discovery event
     * @uml.property  name="serverName"
     */
    public void setServerName(final String serverName) {
        this.serverName = serverName;
    }

    /**
     * @return the connector URLs of the server sending the discovery event
     */
    public String[] getConnectorURL() {
        return urls;
    }

    /**
     * @param connectorURLs the connector URLs of the server sending the discovery event
     */
    public void setConnectorURL(final String[] connectorURLs) {
        this.urls = connectorURLs;
    }

    /**
     * @return  server state.
     * @uml.property  name="state"
     */
    public String getState() {
        return state;
    }

    /**
     * sets the server state : RUNNING or STOPPING.
     * @param state  state of the server sending the discovery event
     * @uml.property  name="state"
     */
    public void setState(final String state) {
        this.state = state;
    }

    /**
     * The string version of the message.
     * @return the message
     */
    public String toString() {
        String str = super.toString()
        + " State=" + state
        + " DomainName=" + domainName
        + " ServerName= " + serverName
        + " Serverid = " + serverId;
        if (urls != null) {
            str = str + " URLs= ";
            for (int i = 0; i < urls.length; i++) {
                str = str + urls[i] + " ";
            }
        }
        return str;

    }

    /**Gets the server ID.
     * @return Server ID.
     * @uml.property  name="serverId"
     */
    public String getServerId() {
        return serverId;
    }

    /**
     * Sets the server ID.
     * @param serverId Id of discovery Host.
     * @uml.property  name="serverId"
     */
    public void setServerId(final String serverId) {
        this.serverId = serverId;
    }

    /**
     * @return the urls
     */
    public String[] getUrls() {
        return urls;
    }

    /**Sets connector urls of discovery host.
     * @param urls the urls to set
     */
    public void setUrls(final String[] urls) {
        this.urls = urls;
    }

    /**Checks if discovery is master.
     * @return True if discovery is master.
     */
    public boolean isDiscoveryMaster() {
        return new Boolean(isDiscoveryMaster);
    }

    /**
     * Discovery becomes master.
     * @param isDiscoveryMaster the isDiscoveryMaster to set
     */
    public void setDiscoveryMaster(final boolean isDiscoveryMaster) {
        this.isDiscoveryMaster = isDiscoveryMaster;
    }

    /**
     * @return True if discovery is master.
     */
    public boolean isMaster() {
        // TODO Auto-generated method stub
        return new Boolean(isDiscoveryMaster);
    }

}