/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * Application Versioning System
 * Copyright (C) 2008 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.versioning.contexts;

import java.net.URL;
import java.util.List;
import java.util.Map;

import org.ow2.jonas.versioning.VirtualContextJMXInterface;

/**
 * Defines JMX methods for virtual JNDI binding management for one application.
 * This interface also includes all methods called by the JOnAS administration
 * Web panel when listing EJB modules.
 * @author S. Ali Tokmen
 */
public interface VirtualJNDIBindingMBean extends VirtualContextJMXInterface {
    /**
     * @return All JNDI names managed by this virtual JNDI binding grouped by
     *         prefix.
     */
    Map<String, List<String>> getNames();

    /**
     * @return Unique URL for this application.
     */
    URL geturl();

    /**
     * @return new String[0].
     */
    String[] getejbs();
}
