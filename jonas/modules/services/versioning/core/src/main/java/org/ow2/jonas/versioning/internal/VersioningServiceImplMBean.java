/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * Application Versioning System
 * Copyright (C) 2008 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.versioning.internal;

import org.ow2.jonas.versioning.VersioningServiceBase;

/**
 * Management bean interface for the versioning service. This interface
 * inherits from the versioning service's JMX interface and doesn't add any
 * method to it, its name guarantees that the JMX server will automatically
 * deploy it with the good bindings.
 *
 * @author Frederic Germaneau
 * @author S. Ali Tokmen
 */
public interface VersioningServiceImplMBean extends VersioningServiceBase {
    // This interface should be empty.
}
