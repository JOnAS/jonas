/**
 * JOnAS : Java(TM) OpenSource Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ee.scout;

import java.io.PrintWriter;
import java.util.Set;

import javax.resource.ResourceException;
import javax.resource.spi.ConnectionManager;
import javax.resource.spi.ConnectionRequestInfo;
import javax.resource.spi.ManagedConnection;
import javax.resource.spi.ManagedConnectionFactory;
import javax.security.auth.Subject;
import javax.xml.registry.ConnectionFactory;

import org.apache.ws.scout.registry.ConnectionFactoryImpl;

/**
 *
 *
 * @author Guillaume Sauthier
 */
public class ManagedConnectionFactoryImpl implements ManagedConnectionFactory {

    /**
     * UID.
     */
    private static final long serialVersionUID = 3258690988061833009L;

    /**
     * PrintWriter.
     */
    private PrintWriter pw = null;

    /**
     * Query Manager URL.
     */
    private String queryManagerURL = null;

    /**
     * LifeCycle Manager URL.
     */
    private String lifeCycleManagerURL = null;

    /**
     * semantic Equivalence property.
     */
    private String semanticEquivalences = null;

    /**
     * Authentication method name to use.
     */
    private String authenticationMethod = null;

    /**
     * maxRows property.
     */
    private Integer maxRows = null;

    /**
     * Scheme of the postal address.
     */
    private String postalAddressScheme = null;

    /**
     * @see javax.resource.spi.ManagedConnectionFactory#createConnectionFactory(javax.resource.spi.ConnectionManager)
     */
    public Object createConnectionFactory(final ConnectionManager manager)
            throws ResourceException {
        return newConnectionFactory();
    }

    /**
     * @return Returns a new JAXr ConnectionFactory instance
     */
    private ConnectionFactory newConnectionFactory() {
        // create the CF
        ConnectionFactoryImpl factory = new JConnectionFactory();
        // assign properties
        factory.setAuthenticationMethod(this.authenticationMethod);
        factory.setLifeCycleManagerURL(this.lifeCycleManagerURL);
        factory.setQueryManagerURL(this.queryManagerURL);
        factory.setMaxRows(this.maxRows);
        factory.setPostalAddressScheme(this.postalAddressScheme);
        factory.setSemanticEquivalences(this.semanticEquivalences);
        return factory;
    }

    /**
     * @see javax.resource.spi.ManagedConnectionFactory#createConnectionFactory()
     */
    public Object createConnectionFactory() throws ResourceException {
        return newConnectionFactory();
    }

    /**
     * Not used.
     * @see javax.resource.spi.ManagedConnectionFactory#createManagedConnection(javax.security.auth.Subject, javax.resource.spi.ConnectionRequestInfo)
     */
    public ManagedConnection createManagedConnection(final Subject subject,
                                                     final ConnectionRequestInfo cri)
                             throws ResourceException {
        // NA
        return null;
    }

    /**
     * Not used.
     * @see javax.resource.spi.ManagedConnectionFactory#matchManagedConnections(java.util.Set, javax.security.auth.Subject, javax.resource.spi.ConnectionRequestInfo)
     */
    public ManagedConnection matchManagedConnections(final Set set, final Subject subject,
            final ConnectionRequestInfo cri) throws ResourceException {
        // NA
        return null;
    }

    /**
     * @see javax.resource.spi.ManagedConnectionFactory#setLogWriter(java.io.PrintWriter)
     */
    public void setLogWriter(final PrintWriter pw) throws ResourceException {
        this.pw = pw;
    }

    /**
     * @see javax.resource.spi.ManagedConnectionFactory#getLogWriter()
     */
    public PrintWriter getLogWriter() throws ResourceException {
        return pw;
    }

    /**
     * @return Returns the authenticationMethod.
     */
    public String getAuthenticationMethod() {
        return authenticationMethod;
    }

    /**
     * @param authenticationMethod The authenticationMethod to set.
     */
    public void setAuthenticationMethod(final String authenticationMethod) {
        this.authenticationMethod = authenticationMethod;
    }

    /**
     * @return Returns the lifeCycleManagerURL.
     */
    public String getLifeCycleManagerURL() {
        return lifeCycleManagerURL;
    }

    /**
     * @param lifeCycleManagerURL The lifeCycleManagerURL to set.
     */
    public void setLifeCycleManagerURL(final String lifeCycleManagerURL) {
        this.lifeCycleManagerURL = lifeCycleManagerURL;
    }

    /**
     * @return Returns the maxRows.
     */
    public Integer getMaxRows() {
        return maxRows;
    }

    /**
     * @param maxRows The maxRows to set.
     */
    public void setMaxRows(final Integer maxRows) {
        this.maxRows = maxRows;
    }

    /**
     * @return Returns the postalAddressScheme.
     */
    public String getPostalAddressScheme() {
        return postalAddressScheme;
    }

    /**
     * @param postalAddressScheme The postalAddressScheme to set.
     */
    public void setPostalAddressScheme(final String postalAddressScheme) {
        this.postalAddressScheme = postalAddressScheme;
    }

    /**
     * @return Returns the queryManagerURL.
     */
    public String getQueryManagerURL() {
        return queryManagerURL;
    }

    /**
     * @param queryManagerURL The queryManagerURL to set.
     */
    public void setQueryManagerURL(final String queryManagerURL) {
        this.queryManagerURL = queryManagerURL;
    }

    /**
     * @return Returns the semanticEquivalences.
     */
    public String getSemanticEquivalences() {
        return semanticEquivalences;
    }

    /**
     * @param semanticEquivalences The semanticEquivalences to set.
     */
    public void setSemanticEquivalences(final String semanticEquivalences) {
        this.semanticEquivalences = semanticEquivalences;
    }

}
