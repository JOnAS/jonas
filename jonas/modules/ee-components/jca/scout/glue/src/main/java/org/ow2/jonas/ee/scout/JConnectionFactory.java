/**
 * JOnAS : Java(TM) OpenSource Application Server
 * Copyright (C) 2005-2008 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ee.scout;

import javax.naming.NamingException;
import javax.naming.Reference;
import javax.resource.Referenceable;

import org.apache.ws.scout.registry.ConnectionFactoryImpl;

/**
 * <code>JConnectionFactory</code> is an extension of Scout <code>ConnectionFactoryImpl</code>.
 * It's used to store the Naming Reference used by the resource service to recreate the
 * <code>ConnectionFactory</code> instance.
 *
 * @author Guillaume Sauthier
 */
public class JConnectionFactory extends ConnectionFactoryImpl implements Referenceable {

    /**
     * UID.
     */
    private static final long serialVersionUID = 3257569490513573686L;

    /**
     * stored Reference.
     */
    private Reference ref = null;

    /**
     * {@inheritDoc}
     * @see javax.naming.Referenceable#getReference()
     */
    public Reference getReference() throws NamingException {
        return this.ref;
    }

    /**
     * {@inheritDoc}
     * @see javax.resource.Referenceable#setReference(javax.naming.Reference)
     */
    public void setReference(final Reference ref) {
        this.ref = ref;
    }
}
