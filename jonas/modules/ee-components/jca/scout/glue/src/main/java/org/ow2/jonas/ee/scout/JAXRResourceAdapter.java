/**
 * JOnAS : Java(TM) OpenSource Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ee.scout;

import javax.resource.ResourceException;
import javax.resource.spi.ActivationSpec;
import javax.resource.spi.BootstrapContext;
import javax.resource.spi.ResourceAdapter;
import javax.resource.spi.ResourceAdapterInternalException;
import javax.resource.spi.endpoint.MessageEndpointFactory;
import javax.transaction.xa.XAResource;

import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;
import org.ow2.jonas.lib.util.Log;

/**
 * This is a very simple ResourceAdapter just used to display traces.
 *
 * @author Guillaume Sauthier
 */
public class JAXRResourceAdapter implements ResourceAdapter {

    /**
     * Logger for this ra.
     */
    private static Logger logger = null;

    /**
     * Display Info log at JAXR Connector start.
     *
     * @see javax.resource.spi.ResourceAdapter#start(javax.resource.spi.BootstrapContext)
     */
    public void start(final BootstrapContext bCtx) throws ResourceAdapterInternalException {
        logger = Log.getLogger(Log.JONAS_JAXR_PREFIX);
        logger.log(BasicLevel.INFO, "Starting Scout ResourceAdapter");
    }

    /**
     * Display Info log at JAXR Connector stop.
     * @see javax.resource.spi.ResourceAdapter#stop()
     */
    public void stop() {
        logger.log(BasicLevel.INFO, "Stopping Scout ResourceAdapter");
        logger = null;
    }

    /**
     * Not used.
     * @see javax.resource.spi.ResourceAdapter#endpointActivation(javax.resource.spi.endpoint.MessageEndpointFactory, javax.resource.spi.ActivationSpec)
     */
    public void endpointActivation(final MessageEndpointFactory mef,
                                   final ActivationSpec as) throws ResourceException {
        // NA
    }

    /**
     * Not used.
     * @see javax.resource.spi.ResourceAdapter#endpointDeactivation(javax.resource.spi.endpoint.MessageEndpointFactory, javax.resource.spi.ActivationSpec)
     */
    public void endpointDeactivation(final MessageEndpointFactory mef,
                                     final ActivationSpec as) {
        // NA
    }

    /**
     * Not used.
     * @see javax.resource.spi.ResourceAdapter#getXAResources(javax.resource.spi.ActivationSpec[])
     */
    public XAResource[] getXAResources(final ActivationSpec[] as) throws ResourceException {
        // NA
        return null;
    }
}
