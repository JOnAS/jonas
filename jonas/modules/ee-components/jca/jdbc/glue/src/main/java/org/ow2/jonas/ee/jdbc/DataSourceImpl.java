/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ee.jdbc;

import java.io.PrintWriter;
import java.io.Serializable;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Reference;

import javax.resource.Referenceable;
import javax.resource.ResourceException;
import javax.resource.spi.ConnectionManager;

import org.objectweb.util.monolog.api.BasicLevel;

/**
 * Description of the DataSourceImpl. This is the dataSource for the JDBC RA.
 * @author Eric Hardesty
 * @created 22 May 2003
 */
public class DataSourceImpl implements javax.sql.DataSource, Serializable, Referenceable {

    private ConnectionManager cm;

    private ManagedConnectionFactoryImpl mcf;

    private PrintWriter pw;

    private Reference reference;

    int loginTimeout = 0;

    /**
     * Logging (debug enabled)
     */
    private final boolean isDebugOn;

    public DataSourceImpl(ManagedConnectionFactoryImpl _mcf, ConnectionManager _cm) {
        if (_cm == null) {
            cm = new ConnectionManagerImpl();
        } else {
            cm = _cm;
        }
        mcf = _mcf;
        isDebugOn = mcf.trace.isLoggable(BasicLevel.DEBUG);
    }

    public Connection getConnection() throws SQLException {
        if (isDebugOn) {
            mcf.trace.log(BasicLevel.DEBUG, "");
        }
        try {
            Connection con = (Connection) cm.allocateConnection(mcf, null);
            if (con == null) {
                SQLException se = new SQLException("Null connection object returned");
                throw se;
            }
            return con;
        } catch (ResourceException re) {
            throw new SQLException(re.getMessage());
        }
    }

    public Connection getConnection(String user, String pwd) throws SQLException {
        if (isDebugOn) {
            mcf.trace.log(BasicLevel.DEBUG, "" + user);
        }
        try {
            ConnectionRequestInfoImpl info = new ConnectionRequestInfoImpl(user, pwd);
            Connection con = (Connection) cm.allocateConnection(mcf, info);
            if (con == null) {
                SQLException se = new SQLException("Null connection object returned");
                throw se;
            }
            return con;
        } catch (ResourceException re) {
            throw new SQLException(re.getMessage());
        }
    }

    public int getLoginTimeout() throws SQLException {
        return loginTimeout;
    }

    public PrintWriter getLogWriter() throws SQLException {
        return pw;
    }

    public Reference getReference() {
        return reference;
    }

    public void setLoginTimeout(int _loginTimeout) throws SQLException {
        loginTimeout = _loginTimeout;
    }

    public void setLogWriter(PrintWriter _pw) throws SQLException {
        pw = _pw;
    }

    public void setReference(Reference _ref) {
        reference = _ref;
    }

    /* JOnAS JDBC implementation for CMP */

    public String getMapperName() {
        String mn = mcf.getMapperName();
        if (isDebugOn) {
            mcf.trace.log(BasicLevel.DEBUG, mn);
        }
        return mn;
    }

    /**
     * Returns true if this either implements the interface argument or is directly or indirectly a wrapper for an object that does.
     * Returns false otherwise. If this implements the interface then return true, else if this is a wrapper then return the result
     * of recursively calling isWrapperFor on the wrapped object. If this does not implement the interface and is not a wrapper,
     * return false. This method should be implemented as a low-cost operation compared to unwrap so that callers can use this method
     * to avoid expensive unwrap calls that may fail. If this method returns true then calling unwrap with the same argument should succeed.
     * @param iface a Class defining an interface. 
     * @returns true if this implements the interface or directly or indirectly wraps an object that does. 
     * @throws SQLException if an error occurs while determining whether this is a wrapper for an object with the given interface.
     */
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return false;
    }

    /**
     * Returns an object that implements the given interface to allow access to non-standard methods, or standard methods not exposed by the proxy.
     * If the receiver implements the interface then the result is the receiver or a proxy for the receiver. If the receiver is a wrapper
     * and the wrapped object implements the interface then the result is the wrapped object or a proxy for the wrapped object.
     * Otherwise return the the result of calling unwrap recursively on the wrapped object or a proxy for that result. 
     * If the receiver is not a wrapper and does not implement the interface, then an SQLException is thrown.
     * @param iface A Class defining an interface that the result must implement. 
     * @returns an object that implements the interface. May be a proxy for the actual implementing object. 
     * @throws SQLException If no object found that implements the interface
     */
    public <T> T unwrap(Class<T> iface) throws SQLException {
        return null;
    }


}
