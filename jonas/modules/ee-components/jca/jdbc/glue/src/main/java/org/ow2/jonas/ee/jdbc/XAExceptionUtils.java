/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ee.jdbc;

import javax.transaction.xa.XAException;

/**
 * This helper class is here to ease XAException creation
 * when Exception chaining is wanted.
 * @author Guillaume Sauthier
 */
public class XAExceptionUtils {

	/**
	 * Construct and throw a new XAException with the given errorCode
	 * and initialized with the given cause.
	 * @param errorCode XA code
	 * @param cause cause of this Exception
	 * @throws XAException Always throw a new XAException
	 */
	public static void throwXAException(final int errorCode, final Throwable cause) throws XAException {
		XAException xae = new XAException(errorCode);
		xae.initCause(cause);
		throw xae;
	}

	/**
	 * Construct and throw a new XAException with the given message
	 * and initialized with the given cause.
	 * @param message Exception message
	 * @param cause cause of this Exception
	 * @throws XAException Always throw a new XAException
	 */
	public static void throwXAException(final String message, final Throwable cause) throws XAException {
		XAException xae = new XAException(message);
		xae.initCause(cause);
		throw xae;
	}
}
