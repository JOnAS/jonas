/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ee.jdbc;

import java.io.Serializable;
import javax.resource.spi.ConnectionRequestInfo;

/**
 * Basic implementation of JCA {@link ConnectionRequestInfo}.
 * @author Eric Hardesty
 */
public class ConnectionRequestInfoImpl
    implements ConnectionRequestInfo, Serializable {

    /* User/password for this connection */
    String user;
    String password;

    public ConnectionRequestInfoImpl() {
        user     = "";
        password = "";
    }

    public ConnectionRequestInfoImpl(String _user, String _password) {
        user     = _user;
        password = _password;
    }

    public boolean equals(Object obj) {
        if(obj == null) {
            return false;
        }
        if(obj instanceof ConnectionRequestInfoImpl) {
            ConnectionRequestInfoImpl cri =
                         (ConnectionRequestInfoImpl) obj;
            // TODO Object.equals(Object) should be used here
            return (user == cri.user) && (password == cri.password);
        } else {
            return false;
        }
    }

    public String getPassword() {
        return password;
    }

    public String getUser() {
        return user;
    }

    public int hashCode() {
        String val = "" + user + password;
        return val.hashCode();
    }

    public void setPassword(String pass) {
        password = pass;
    }

    public void setUser(String _user) {
        user = _user;
    }

}