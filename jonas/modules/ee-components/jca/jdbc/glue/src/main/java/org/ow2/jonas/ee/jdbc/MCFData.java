/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Eric HARDESTY
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ee.jdbc;

import java.util.Enumeration;
import java.util.Properties;

public class MCFData {

    /* Properties object to hold all MCF config information */ 
    Properties mcfData = null;
    
    public static String dsMethodNames[] = { 
        "setDSClass",
        "setDbSpecificMethods",
        "setDatabaseName",
        "setDataSourceName",
        "setDescription",
        "setPortNumber",
        "setServerName",
        "setURL",
        "setUser",
        "setPassword",
        "setLoginTimeout",
        "setIsolationLevel",
        "setInitialPoolSize",
        "setMinPoolSize",
        "setMaxIdleTime",
        "setMaxPoolSize",
        "setMaxStatements",
        "setPropertyCycle",
        "setMapperName",
        "setLogTopic",
        "setConnCheckLevel",
        "setConnMaxAge",
        "setConnTestStmt",
    };
    // Config properties for JDBC drivers 
    public static final int DSCLASS           =  0;
    public static final int DBSPECIFICMETHODS =  1;
    public static final int DATABASENAME      =  2;
    public static final int DATASOURCENAME    =  3;
    public static final int DESCRIPTION       =  4;
    public static final int PORTNUMBER        =  5;
    public static final int SERVERNAME        =  6;
    public static final int URL               =  7;
    public static final int USER              =  8;
    public static final int PASSWORD          =  9;
    public static final int LOGINTIMEOUT      = 10;
    public static final int ISOLATIONLEVEL    = 11;
    
    // Config values for JDBC 3.0
    public static final int INITIALPOOLSIZE   = 12;
    public static final int MINPOOLSIZE       = 13;
    public static final int MAXIDLETIME       = 14;
    public static final int MAXPOOLSIZE       = 15;
    public static final int MAXSTATEMENTS     = 16;
    public static final int PROPERTYCYCLE     = 17;

    //This must be set to the last nonJOnAS config item
    public static final int JONASOFFSET       = 17;    

    // JOnAS specific items
    // Config values for CMP 2.0 use with JORM 
    public static final int MAPPERNAME        = 18;
    
    // Config values for JOnAS logger 
    public static final int LOGTOPIC          = 19;
    
    // Config values for JOnAS connection testing
    public static final int CONNCHECKLEVEL    = 20;
    public static final int CONNMAXAGE        = 21;
    public static final int CONNTESTSTMT      = 22;
    

    public MCFData() {
        mcfData = new Properties();
    }


    public boolean equals(Object obj) {
        if (obj instanceof MCFData) {
            return mcfData.equals(((MCFData)obj).mcfData);
        } else {
            return false;
        }
    }

    /* Return the specified property */
    public String getMCFData(int prop) {
        return mcfData.getProperty(""+prop);
    }

    public int hashCode() {
        return mcfData.hashCode();
    }

    /* Set the specified property/value combination */
    public void setMCFData(int prop, String val) {
        mcfData.setProperty(""+prop, val);
    }
    
    public String getProperty(String key) {
        return mcfData.getProperty(key);
    }

    public Enumeration getProperties() {
        return mcfData.propertyNames();
    }

}