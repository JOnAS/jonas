/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Eric HARDESTY
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ee.jdbc;

import javax.resource.spi.ManagedConnection;

import java.io.Serializable;
import javax.resource.ResourceException;
import javax.resource.spi.ConnectionManager;
import javax.resource.spi.ConnectionRequestInfo;
import javax.resource.spi.ManagedConnectionFactory;

/**
 *  Description of the ConnectionManagerImpl.  Default ConnectionManager
 *  for the JDBC RA.
 *
 *@author    Eric Hardesty
 *@created    22 May 2003
 */
public class ConnectionManagerImpl
    implements ConnectionManager, Serializable
{

    public ConnectionManagerImpl()
    {
    }

    /**
     *  No pooling is done for non-managed connections, just get a new MC and 
     *  connection to the database.
     *
     */
    public Object allocateConnection(ManagedConnectionFactory mcf, ConnectionRequestInfo info)
        throws ResourceException
    {
        ManagedConnection mc = mcf.createManagedConnection(null, info);
        return mc.getConnection(null, info);
    }
}