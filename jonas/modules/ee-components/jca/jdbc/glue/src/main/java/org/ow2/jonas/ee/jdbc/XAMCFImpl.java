/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Eric HARDESTY
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ee.jdbc;


import java.sql.SQLException;

import javax.resource.ResourceException;
import javax.resource.spi.ConnectionRequestInfo;
import javax.resource.spi.ManagedConnection;
import javax.resource.spi.ResourceAllocationException;
import javax.resource.spi.security.PasswordCredential;
import javax.security.auth.Subject;
import javax.sql.XADataSource;
import org.objectweb.util.monolog.api.BasicLevel;

public class XAMCFImpl extends ManagedConnectionFactoryImpl {

    XADataSource ds = null;

    public ManagedConnection createManagedConnection(Subject subject, ConnectionRequestInfo cxReq)
        throws ResourceException {
        trace.log(BasicLevel.DEBUG, "");
        PasswordCredential pc = getPasswordCredential(this, subject, cxReq, pw);
        if (ds == null) {
            try {
                ds = (XADataSource) getDataSource(this, pc, trace);
            } catch(Exception ex) {
                trace.log(BasicLevel.WARN, "Cannot get DataSource:" + ex);
                throw new ResourceException(ex.getMessage(), ex);
            }
        }
        javax.sql.XAConnection xaConnection = null;
        java.sql.Connection connection = null;
        try {
            if (cxReq != null) {
                ConnectionRequestInfoImpl cx = (ConnectionRequestInfoImpl) cxReq;
                xaConnection = ds.getXAConnection(cx.getUser(), cx.getPassword());
            } else if (pc != null){
                xaConnection = ds.getXAConnection(pc.getUserName(), new String(pc.getPassword()));
            } else if (mcfData.getMCFData(MCFData.USER).length() > 0){
                xaConnection = ds.getXAConnection(mcfData.getMCFData(MCFData.USER),
                                                  mcfData.getMCFData(MCFData.PASSWORD));
            } else {
                xaConnection = ds.getXAConnection();
            }
            if (xaConnection != null) {
                connection = xaConnection.getConnection();
            }
        } catch(SQLException sqle) {
            trace.log(BasicLevel.WARN, "The connection could not be allocated: " + sqle);
            throw new ResourceAllocationException("The connection could not be allocated: " + sqle.getMessage(), sqle);
        }
        ManagedConnection mc = new ManagedConnectionImpl(this, pc, connection, null, xaConnection, null);
        if (trace.isLoggable(BasicLevel.DEBUG)) {
            trace.log(BasicLevel.DEBUG, "Returning: " + mc);
        }
        return mc;
    }

    /* Determine if the factories are equal
     */
    public boolean equals(Object obj) {
        if (obj instanceof XAMCFImpl) {
            return mcfData.equals(((XAMCFImpl)obj).mcfData);
        } else {
            return false;
        }
    }

    // JOnAS JDBC RA DataSource config properties
    public String getDatabaseName() {
        return mcfData.getMCFData(MCFData.DATABASENAME);
    }

    public void setDatabaseName(String val) {
        mcfData.setMCFData(MCFData.DATABASENAME, val);
    }

    public String getDescription() {
        return mcfData.getMCFData(MCFData.DESCRIPTION);
    }

    public void setDescription(String val) {
        mcfData.setMCFData(MCFData.DESCRIPTION, val);
    }

    public String getPortNumber() {
        return mcfData.getMCFData(MCFData.PORTNUMBER);
    }

    public void setPortNumber(String val) {
        mcfData.setMCFData(MCFData.PORTNUMBER, val);
    }

    public String getServerName() {
        return mcfData.getMCFData(MCFData.SERVERNAME);
    }

    public void setServerName(String val) {
        mcfData.setMCFData(MCFData.SERVERNAME, val);
    }

}
