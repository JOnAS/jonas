 /*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Eric HARDESTY
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ee.jdbc;

import java.sql.DatabaseMetaData;
import java.sql.SQLException;

import javax.resource.ResourceException;

/**
 *  Description of the MetaDataImpl.  This is the DatabaseMetadata for 
 *  the current JDBC connection.
 *
 *@author    Eric Hardesty
 *@created    22 May 2003
 */
public class MetaDataImpl 
       implements javax.resource.spi.ManagedConnectionMetaData {

  private ManagedConnectionImpl mcon;
  private DatabaseMetaData  dMetaData;

  public 
  MetaDataImpl(ManagedConnectionImpl _mcon) 
            throws ResourceException {
      dMetaData = null;
      try {
          mcon = _mcon;
          dMetaData = mcon.connection.getMetaData();
      } catch (SQLException se) {
          throw new ResourceException(se.getMessage());
      }
  }

  /** Returns the JDBC Product name
   *
   *  @return  String Product name of the JDBC instance.
  **/
  public
  String getEISProductName() throws ResourceException
  {
      try {
          return(dMetaData.getDatabaseProductName());
      } catch (SQLException se) {
          throw new ResourceException(se.getMessage());
      }
  }

  /** Returns the JDBC Product version
   *
   *  @return  String Product version of the JDBC instance
  **/
  public
  String getEISProductVersion() throws ResourceException
  {
      try {
          return(dMetaData.getDatabaseProductVersion());
      } catch (SQLException se) {
          throw new ResourceException(se.getMessage());
      }
  }

  /** Returns maximum limit on number of active concurrent connections 
   *  that this JDBC instance can support across client processes.
   *
   *  @return  int of maximum limit for number of active concurrent 
   *           connections.  0 is an unlimited number of connections
  **/
  public
  int getMaxConnections() throws ResourceException
  {
      try {
          return(dMetaData.getMaxConnections());
      } catch (SQLException se) {
          throw new ResourceException(se.getMessage());
      }
  }
  
  /** Returns the name of the user associated with the JdbcRaManagedConnection
   *  instance. The name corresponds to the resource principal under 
   *  whose security context, a connection to the DB has been
   *  established.
   *
   *  @return  String name of the user
  **/
  public
  String getUserName() throws ResourceException
  {
      try {
          return(dMetaData.getUserName());
      } catch (SQLException se) {
          throw new ResourceException(se.getMessage());
      }
  }
}
