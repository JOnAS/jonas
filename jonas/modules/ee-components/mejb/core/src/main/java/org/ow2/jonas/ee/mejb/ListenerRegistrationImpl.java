/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.ee.mejb;

import java.io.IOException;
import java.rmi.RemoteException;

import javax.management.InstanceNotFoundException;
import javax.management.ListenerNotFoundException;
import javax.management.MBeanServerConnection;
import javax.management.NotificationFilter;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.management.j2ee.ListenerRegistration;

/**
 * ListenerRegistration implementation.
 */
public class ListenerRegistrationImpl implements ListenerRegistration {

    /**
     * Wrapped MBeanServer instance.
     */
    transient private MBeanServerConnection connection = null;

    /**
     * A ListenerRegistration object allows to ad/remove listeners to notifications emitted by
     * MBeans which are registered in a remote MBeanServer.
     * @param connection the remote MBean server
     */
    public ListenerRegistrationImpl(final MBeanServerConnection connection) {
        this.connection = connection;
    }

    /**
     * Add notification listener.
     * @param name identifies the emitter MBean
     * @param listener the listener to add
     * @param filter the associated filter
     * @param o the associated handback object
     * @exception InstanceNotFoundException emitter MBean not registered in the MBeanServer
     * @exception RemoteException operation failed
     */
    public void addNotificationListener(final ObjectName name,
                                        final NotificationListener listener,
                                        final NotificationFilter filter,
                                        final Object o) throws InstanceNotFoundException,
                                                               RemoteException {
        try {
            connection.addNotificationListener(name, listener, filter, o);
        } catch (IOException e) {
            throw new RemoteException(e.toString());
        }
    }
    /**
     * Remove notification listener
     * @param name identifies the emitter MBean
     * @param listener the listener to remove
     * @exception InstanceNotFoundException emitter MBean not registered in the MBeanServer
     * @exception ListenerNotFoundException arg1 not registered as listener
     * @exception RemoteException operation failed
     */
    public void removeNotificationListener(final ObjectName name,
                                           final NotificationListener listener) throws InstanceNotFoundException,
                                                                                       ListenerNotFoundException,
                                                                                       RemoteException {
        try {
            connection.removeNotificationListener(name, listener);
        } catch (IOException  e) {
            throw new RemoteException(e.toString());
        }
    }
}
