/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $$Id$$
 * --------------------------------------------------------------------------
 */

/**
 *
 */
package org.ow2.jonas.webapp.jonasadmin.monitoring.graph;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author eyindanga
 *
 */
public class ServerGraphsAction extends JonasBaseAction{


    @Override
    public ActionForward executeAction(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException {

//		Form used
        ServerGraphForm oForm = (ServerGraphForm) form;
        WhereAreYou oWhere = (WhereAreYou) request.getSession().getAttribute(
                WhereAreYou.SESSION_NAME);
        //get the action to perform
        String actionToPerform = oForm.getAction();
        String sDomainName = oWhere.getCurrentDomainName();
        //admin server name
        String serverName = oWhere.getCurrentJonasServerName();
        String servletUrl = null;
        String selectedTabTitle = null;
        String actionName = null;
        String contextPath = request.getContextPath();
        try {
            ObjectName on = new ObjectName(sDomainName
                    + ":type=ServerProxy,name=" + serverName);
            if (actionToPerform.equals("totalCurrentTransactions")) {
                servletUrl = contextPath+"/ServerCurrentTxServlet";
                selectedTabTitle = serverName+" Transactions Count";
                actionName = "ActionSeverTransactionsCount";

            } else if (actionToPerform.equals("currentThreadCount")) {
                servletUrl = contextPath+"/ThreadCountServlet";
                selectedTabTitle = serverName+" Threads Count";
                actionName = "ActionServerThreadCount";


            } else {
                //JCA TRANSACTIONS
                if (actionToPerform.equals("totalBegunTransactions")) {
                    servletUrl = contextPath+"/BegunTransactionsServlet";
                    selectedTabTitle = serverName+" Begun Tx Count";

                } else if (actionToPerform.equals("totalCommittedTransactions")) {
                    servletUrl = contextPath+"/TotalCommittedTransactionsServlet";
                    selectedTabTitle = serverName+" Committed Tx Count";

                }else if (actionToPerform.equals("totalExpiredTransactions")) {
                    servletUrl = contextPath+"/TotalExpiredTransactionsServlet";
                    selectedTabTitle = serverName+" Expired Tx Count";

                }else if (actionToPerform.equals("totalRolledbackTransactions")) {
                    servletUrl = contextPath+"/TotalRolledbackTransactionsServlet";
                    selectedTabTitle = serverName+" Rolled-back Tx Count";

                //TOMCAT
                }else if (actionToPerform.equals("currentThreadBusyByConnectorTomcat")) {
                    servletUrl = contextPath+"/ThreadBusyConnectorTomcatServlet";
                    selectedTabTitle = serverName+" Tomcat Busy Threads Count";

                }else if (actionToPerform.equals("bytesReceivedByConnectorTomcat")) {
                    servletUrl = contextPath+"/BytesReceivedByConnectorTomcatServlet";
                    selectedTabTitle = serverName+" Tomcat Conn. Received Bytes";

                }else if (actionToPerform.equals("bytesSentByConnectorTomcat")) {
                    servletUrl = contextPath+"/BytesSentByConnectorTomcatServlet";
                    selectedTabTitle = serverName+" Tomcat conn. sent Bytes";

                }else if (actionToPerform.equals("errorCountByConnectorTomcat")) {
                    servletUrl = contextPath+"/ErrorCountByConnectorTomcatServlet";
                    selectedTabTitle = serverName+" Tomcat Conn. Errors Count";

                }else if (actionToPerform.equals("processingTimeByConnectorTomcat")) {
                    servletUrl = contextPath+"/ProcessingTimeByConnectorTomcatServlet";
                    selectedTabTitle = serverName+" Tomcat Conn. Processing Time";

                }else if (actionToPerform.equals("requestCountByConnectorTomcat")) {
                    servletUrl = contextPath+"/RequestCountByConnectorTomcatServlet";
                    selectedTabTitle = serverName+" Tomcat Conn. Request Count";

                    //JCA TRANSACTIONS
                }else if (actionToPerform.equals("currentBusyJCAConnection")) {
                    servletUrl = contextPath+"/CurrentBusyJCAConnectionServlet";
                    selectedTabTitle = serverName+" Busy JCA Conn. Count";

                }else if (actionToPerform.equals("connectionFailuresJCAConnection")) {
                    servletUrl = contextPath+"/ConnectionFailuresJCAConnectionServlet";
                    selectedTabTitle = serverName+" Failed JCA Conn. Count";

                }else if (actionToPerform.equals("connectionLeaksJCAConnection")) {
                    servletUrl = contextPath+"/ConnectionLeaksJCAConnectionServlet";
                    selectedTabTitle = serverName+" Failed JCA Conn. Count";

                }else if (actionToPerform.equals("rejectedOpenJCAConnection")) {
                    servletUrl = contextPath+"/RejectedOpenJCAConnectionServlet";
                    selectedTabTitle = serverName+" Rejected JCA Conn. Count";

                }else if (actionToPerform.equals("servedOpenJCAConnection")) {
                    servletUrl = contextPath+"/ServedOpenJCAConnectionServlet";
                    selectedTabTitle = serverName+" Served JCA Conn. Count";

                }else if (actionToPerform.equals("waiterCountJCAConnection")) {
                    servletUrl = contextPath+"/WaiterCountJCAConnectionServlet";
                    selectedTabTitle = serverName+" begun Tx Count";

                }else if (actionToPerform.equals("totalBegunTransactions")) {
                    servletUrl = contextPath+"/BegunTransactionsServlet";
                    selectedTabTitle = serverName+" Waiters JCA Conn. Count";

                    //JDBC DATA SoURCES
                }else if (actionToPerform.equals("currentBusyJDBCDatasource")) {
                    servletUrl = contextPath+"/CurrentBusyJDBCResourceServlet";
                    selectedTabTitle = serverName+" Busy JDBC Datasources Count";

                }else if (actionToPerform.equals("connectionLeaksJDBCDatasource")) {
                    servletUrl = contextPath+"/ConnectionLeaksJDBCResourceServlet";
                    selectedTabTitle = serverName+" Leaked JDBC Datasources Count";

                }else if (actionToPerform.equals("rejectedOpenJDBCDatasource")) {
                    servletUrl = contextPath+"/RejectedOpenJDBCResourceServlet";
                    selectedTabTitle = serverName+" Rejected JDBC Datasources Count";

                }else if (actionToPerform.equals("servedOpenJDBCDatasource")) {
                    servletUrl = contextPath+"/ServedOpenJDBCResourceServlet";
                    selectedTabTitle = serverName+" Served JDBC Datasources Count";

                }else if (actionToPerform.equals("waiterCountJDBCDatasource")) {
                    servletUrl = contextPath+"/WaiterCountJDBCResourceServlet";
                    selectedTabTitle = serverName+" Waiting JDBC Datasources Count";

                    //JMS
                }else if (actionToPerform.equals("jmsQueuesNbMsgsReceiveSinceCreation")) {
                    servletUrl = contextPath+"/JmsQMsgReceiveServlet";
                    selectedTabTitle = serverName+" Jms Queues Received Msg Count";

                }else if (actionToPerform.equals("jmsQueuesNbMsgsDeliverSinceCreation")) {
                    servletUrl = contextPath+"/JmsQMsgDeliverServlet";
                    selectedTabTitle = serverName+" Jms Queues Delivered Msg Count";

                }else if (actionToPerform.equals("jmsTopicsNbMsgsReceiveSinceCreation")) {
                    servletUrl = contextPath+"/JmsTopicMsgReceiveServlet";
                    selectedTabTitle = serverName+" Jms Topics Received Msg Count";

                }else if (actionToPerform.equals("jmsTopicsNbMsgsDeliverSinceCreation")) {
                    servletUrl = contextPath+"/JmsTopicMsgDeliverServlet";
                    selectedTabTitle = serverName+" Jms Topics Delivered Msg Count";

                    //BEANS
                }else if (actionToPerform.equals("currentNumberOfEJB")) {
                    servletUrl = contextPath+"/CurrentNumberOfEJBServlet";
                    selectedTabTitle = serverName+" Ejb count";

                }else if (actionToPerform.equals("currentNumberOfEntity")) {
                    servletUrl = contextPath+"/CurrentNumberOfEntityBeanServlet";
                    selectedTabTitle = serverName+" Entity Beans Count";

                }else if (actionToPerform.equals("currentNumberOfSBF")) {
                    servletUrl = contextPath+"/CurrentNumberOfSBFServlet";
                    selectedTabTitle = serverName+" SFSB Count";

                }else if (actionToPerform.equals("currentNumberOfSBL")) {
                    servletUrl = contextPath+"/CurrentNumberOfSBLServlet";
                    selectedTabTitle = serverName+" SLSB Count";

                }else if (actionToPerform.equals("currentNumberOfMDB")) {
                    servletUrl = contextPath+"/CurrentNumberOfMDBServlet";
                    selectedTabTitle = serverName+" MDB Count";

                }else if (actionToPerform.equals("currentUsedMemory")) {
                    servletUrl = contextPath+"/MemoryGraphServlet";
                    selectedTabTitle = serverName+" Current Used memory";

                }else {
                    servletUrl = contextPath+"/BegunTransactionsServlet";
                    selectedTabTitle = serverName+" Forgot this case";
                }
                // there is a single action for plotting all server graphs
                actionName = "ActionServerGraph";

            }

            //set attributes for jsp
            request.setAttribute("srvOn",on );
            request.setAttribute("currentServerName", serverName);
            //name in the web.xml
            request.setAttribute("servletUrl", servletUrl);
            //this name is in stuts-config.xml
            //TODO dynamically get stuts action name of the current action
            //and replaced hard coded name with this dynamic action name
            request.setAttribute("actionName", actionName);
            request.setAttribute("selectedTabTitle", selectedTabTitle);

            //set attribute for servlets
            m_Session.setAttribute("srvOn", on);
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(request, m_Errors);
            return (mapping.findForward("Global Error"));
        }
        // Forward to the jsp that dispalys all server graphs.
        return (mapping.findForward("ServerGraphsDisplay"));

    }



}



