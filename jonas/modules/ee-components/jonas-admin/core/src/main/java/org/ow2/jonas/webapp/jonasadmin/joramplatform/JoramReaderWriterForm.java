/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.joramplatform;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;


/**
 * @author Adriana Danes
 */
public class JoramReaderWriterForm extends ActionForm {

// --------------------------------------------------------- Constants

// --------------------------------------------------------- Properties variables

    private String name = null;
    private String id = null;

// --------------------------------------------------------- Public Methods

    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */

    @Override
    public void reset(final ActionMapping mapping, final HttpServletRequest request) {
        name = null;
        id = null;
    }

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     * @return List of errors
     */
    @Override
    public ActionErrors validate(final ActionMapping mapping, final HttpServletRequest request) {

        WhereAreYou oWhere = (WhereAreYou) request.getSession().getAttribute(WhereAreYou.
                SESSION_NAME);
        String serverName = oWhere.getCurrentJonasServerName();

        ActionErrors oErrors = new ActionErrors();
        if ((getName() == null) || (getName().length() == 0)) {
            oErrors.add("name", new ActionMessage("error.resource.jms.addreader.name.required"));
        }
        if (getId() == null) {
            oErrors.add("id", new ActionMessage("error.resource.jms.addreader.id.required"));
        }
        /*
        String ons = getName() + "[" + id + "]";
        try {
            ObjectName userOn = JoramObjectName.joramUser(ons);
            if (!JonasManagementRepr.isRegistered(userOn, serverName)) {
                oErrors.add("name", new ActionMessage("error.resource.jms.addreader.name.error"));
            }
        } catch (MalformedObjectNameException e) {
            oErrors.add("name", new ActionMessage("error.resource.jms.addreader.name.error"));

        }
        */
        return oErrors;
    }

    // --------------------------------------------------------- Properties Methods

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return Returns the id.
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id to set.
     */
    public void setId(final String id) {
        this.id = id;
    }

}
