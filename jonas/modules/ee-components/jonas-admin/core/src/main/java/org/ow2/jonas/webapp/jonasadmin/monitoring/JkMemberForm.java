package org.ow2.jonas.webapp.jonasadmin.monitoring;

import org.apache.struts.action.ActionForm;

public class JkMemberForm extends ActionForm {
    /**
     * The worker's name
     */
    private String name = null;
    /**
     * The worker's host
     */
    private String host = null;
    /**
     * The member's state
     */
    private String state = null;

    /**
     * The worker's port
     */
    private int port;

    /**
     * The worker's load balance factor
     */
    private int lbFactor;

    /**
     * worker type
     */
    private String type;

    /**
     * load balancing factor, as defined in workers.properties
     */
    private int lbfactor;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getLbfactor() {
        return lbfactor;
    }

    public void setLbfactor(int lbfactor) {
        this.lbfactor = lbfactor;
    }

    public int getLbFactor() {
        return lbFactor;
    }

    public void setLbFactor(int lbFactor) {
        this.lbFactor = lbFactor;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


}