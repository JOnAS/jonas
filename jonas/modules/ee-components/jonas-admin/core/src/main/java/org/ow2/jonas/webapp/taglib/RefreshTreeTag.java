/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

/**
 * @author Michel-Ange ANTON
 */
public class RefreshTreeTag extends WhereAreYouTag {

// ----------------------------------------------------- Constants

// ----------------------------------------------------- Instance Variables

// ----------------------------------------------------- Properties

// ----------------------------------------------------- Public Methods

    /**
     * Render this instant actions control.
     *
     * @return Constant EVAL_PAGE
     * @exception JspException if a processing error occurs
     */
    public int doEndTag()
        throws JspException {
        JspWriter out = pageContext.getOut();
        try {
            render(out);
        }
        catch (IOException e) {
            throw new JspException(e);
        }
        return (EVAL_PAGE);
    }

// -------------------------------------------------------- Protected Methods

    protected void render(JspWriter out)
        throws IOException, JspException {
        // Action to refresh associate tree
        if (isTreeToRefresh() == true) {
            out.print("<script language=\"JavaScript\">");
            out.print("refreshTree(\"");
            out.print(getUrlToRefreshSelectedNode());
            out.print("\");");
            out.println("</script>");
        }
    }
}
