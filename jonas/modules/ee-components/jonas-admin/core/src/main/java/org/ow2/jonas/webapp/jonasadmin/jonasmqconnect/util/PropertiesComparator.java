package org.ow2.jonas.webapp.jonasadmin.jonasmqconnect.util;

import java.util.Comparator;

public class PropertiesComparator implements Comparator {

    public int compare(Object obj1, Object obj2) {
        Property prop1 = (Property) obj1;
        Property prop2 = (Property) obj2;

        int iRet = prop1.getName().compareToIgnoreCase(prop2.getName());
        if (iRet == 0) {
            iRet = prop1.getValue().compareToIgnoreCase(prop2.getValue());
        }
        return iRet;
    }

    public boolean equals(Object obj) {
        if (obj instanceof Property) {
            return true;
        }
        return false;
    }
}