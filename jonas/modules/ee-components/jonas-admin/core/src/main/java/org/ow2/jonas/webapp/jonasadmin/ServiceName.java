package org.ow2.jonas.webapp.jonasadmin;


public enum ServiceName {
    REGISTRY ("registry"),
    JMX ("jmx"),
    JTM ("jtm"),
    SECURITY ("security"),
    DB ("db"),
    MAIL ("mail"),
    WM ("wm"),
    WC ("wc"),
    DBM ("dbm"),
    RESOURCE ("resource"),
    CMI ("cmi"),
    HA ("ha"),
    VERSIONING ("versioning"),
    SMARTCLIENT ("smartclient"),
    EJB2 ("ejb2"),
    EJB3 ("ejb3"),
    WEB ("web"),
    EAR ("ear"),
    JAXRPC ("jaxrpc"),
    JAXWS ("jaxws"),
    WSDLPUBLISHER ("wsdl-publisher"),
    WS ("ws"),
    DEPMONITOR ("depmonitor"),
    DISCOVERY ("discovery");

    /**
     * Service name.
     */
    private String name = null;

    /**
     * Private constructor.
     * @param name the given service name
     */
    private ServiceName(final String name) {
        this.name = name;
    }

    /**
     * Returns the service name.
     * @return the service name
     */
    public String getName(){
        return name;
    }
}
