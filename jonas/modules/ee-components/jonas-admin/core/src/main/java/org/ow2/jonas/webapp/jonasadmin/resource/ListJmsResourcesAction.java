/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Michel-Ange ANTON
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.resource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasAdminJmx;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 *
 */

public class ListJmsResourcesAction extends JonasBaseAction {

// --------------------------------------------------------- Public Methods

    /**
     */
    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws IOException, ServletException {

        try {

            // Current server
            String serverName = m_WhereAreYou.getCurrentJonasServerName();
            String domainName = m_WhereAreYou.getCurrentDomainName();

            String asDestName = null;

            // Prepare access to ejbContainer service in order to chech dependences
            ObjectName ejbServiceMB = JonasObjectName.ejbService(domainName);
            boolean registeredEjbService = JonasManagementRepr.isRegistered(ejbServiceMB, serverName);

            // Get queues
            ArrayList alQueues = new ArrayList();
            ArrayList al = JonasAdminJmx.getQueuesList(domainName, serverName);
            if (al != null) {
                for (int i = 0; i < al.size(); i++) {
                    asDestName = al.get(i).toString();
                    boolean deps = false;
                    // Chech dependences
                    if (registeredEjbService) {
                        deps = hasDeps(asDestName, ejbServiceMB, serverName);
                    }
                    alQueues.add(new DestinationItem(asDestName, "queue", deps));
                }
                Collections.sort(alQueues, new DestinationItemByNameComparator());
            }
            // Get topics
            ArrayList alTopics = new ArrayList();
            al = JonasAdminJmx.getTopicsList(domainName, serverName);
            if (al != null) {
                for (int i = 0; i < al.size(); i++) {
                    asDestName = al.get(i).toString();
                    boolean deps = false;
                    // Chech dependences
                    if (registeredEjbService) {
                        deps = hasDeps(asDestName, ejbServiceMB, serverName);
                    }
                    alTopics.add(new DestinationItem(al.get(i).toString(), "topic", deps));
                }
                Collections.sort(alTopics, new DestinationItemByNameComparator());
            }

            // merge alQueues and alTopics in alDestinations
            ArrayList alDestinations = new ArrayList(alQueues);
            alDestinations.addAll(alTopics);

            p_Request.setAttribute("destinationsList", alDestinations);
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }

        // Forward to the jsp.
        return (p_Mapping.findForward("Jms Resources"));
    }

    private boolean hasDeps(String destName, ObjectName ejbServiceMB, String serverName) {
        String[] asParam = {destName };
        String[] asSignature = {"java.lang.String" };
        Set deps = (java.util.Set) JonasManagementRepr.invoke(ejbServiceMB, "getJmsDestinationDependence", asParam, asSignature, serverName);
        boolean res =  deps.size() > 0 ? true : false;
        return res;
    }
}
