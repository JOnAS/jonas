/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.jonasserver;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.management.ObjectName;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.webapp.jonasadmin.cmi.ClusterAttribute;
import org.ow2.jonas.webapp.jonasadmin.cmi.CmiObject;

/**
 * Helper class which manipulates CMI MBean for registry content
 * @author Adriana Danes
 *
 */
public class CmiRegistryResource {

    public static final String PREFIX = "REG_";
    /**
     * local node
     */
    public static final String LOCAL = "local";

    public static ArrayList<CmiObject> getCmiRegistry(ObjectName cmiOn, final List<String> names, String serverName) {
        ArrayList<CmiObject> result = new ArrayList<CmiObject>();
        List<String> globalObjects = new ArrayList<String>();
        Set<String> clusterList = (Set<String>)JonasManagementRepr.getAttribute(cmiOn, "ClusterNames", serverName);
        for (Iterator<String> iter = clusterList.iterator(); iter.hasNext();) {
            String[] signature = {"java.lang.String"};
            String clusterName = (String) iter.next();
            String[] params = {clusterName};
            Set<String> objectList = (Set<String>)JonasManagementRepr.invoke(cmiOn, "getObjectNames", params, signature, serverName);
            //Add the global objects.
            globalObjects.addAll(objectList);
            //Set objectList = (Set)cmiMbeansServer.invoke(cmiOn, "getObjectNames", params, signature);
        }
        for (Iterator<String> iterator = names.iterator(); iterator.hasNext();) {
            String object = (String) iterator.next();
            if (globalObjects.contains(object)) {
                //the object is global
                result.add(new CmiObject(object, false, null));
            }else {
                //The object is local
                result.add(new CmiObject(object, true, null));
            }

        }
        return result;
    }
}