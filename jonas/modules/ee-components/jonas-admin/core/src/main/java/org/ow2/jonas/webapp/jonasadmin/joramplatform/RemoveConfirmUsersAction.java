/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.joramplatform;

import java.util.Set;

import javax.management.InstanceNotFoundException;
import javax.management.ObjectName;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.lib.util.JoramObjectName;
import org.ow2.jonas.webapp.jonasadmin.deploy.BaseDeployAction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * @author Adriana Danes
 */
public class RemoveConfirmUsersAction extends BaseDeployAction {

    public ActionForward executeAction(ActionMapping pMapping, ActionForm pForm
        , HttpServletRequest pRequest, HttpServletResponse pResponse) {

        // Current JOnAS server
        String jonasServerName =  m_WhereAreYou.getCurrentJonasServerName();

        // Form used
        RemoveUsersForm oForm = (RemoveUsersForm) m_Session.getAttribute("removeUsersForm");
        String localId = (String) m_Session.getAttribute("localId"); // Joram server to which client admin is connected
        String currentId = (String) m_Session.getAttribute("currentId"); // Joram server currently being managed
        try {
            String destinationName = null;
            Set adapterOns = JonasManagementRepr.queryNames(JoramObjectName.joramAdapter(), jonasServerName);
            if (adapterOns.isEmpty()) {
                throw new InstanceNotFoundException();
            }
            ObjectName joramAdapterON = (ObjectName) adapterOns.iterator().next();
            ObjectName destinationObjectName = null;
            for (int i = 0; i < oForm.getSelectedItems().length; i++) {
                destinationName = oForm.getSelectedItems()[i];
                if (currentId.equals(localId)) {
                    String[] asParam = {
                            destinationName
                    };
                    String[] asSignature = {
                            "java.lang.String"
                    };
                    JonasManagementRepr.invoke(joramAdapterON, "removeDestination", asParam, asSignature, jonasServerName);
                } else {
                    destinationObjectName = JoramObjectName.joramQueue(destinationName);
                    if (JonasManagementRepr.isRegistered(destinationObjectName, jonasServerName)) {
                        // the destination is a queue
                        JonasManagementRepr.invoke(destinationObjectName, "delete", null, null, jonasServerName);
                    } else {
                        destinationObjectName = JoramObjectName.joramTopic(destinationName);
                        if (JonasManagementRepr.isRegistered(destinationObjectName, jonasServerName)) {
                            // the destination is a topic
                            JonasManagementRepr.invoke(destinationObjectName, "delete", null, null, jonasServerName);
                        }
                    }
                }
            }
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(pRequest, m_Errors);
            return (pMapping.findForward("Global Error"));
        }

        // Forward to action
        return (pMapping.findForward("ActionEditJoramServer"));
    }
}
