/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.db;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.ow2.jonas.webapp.jonasadmin.Jlists;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author Michel-Ange ANTON
 */
public class DatasourceForm extends ActionForm {

    // --------------------------------------------------------- Constants

    // --------------------------------------------------------- Properties variables

    private String action = null;
    private String className = null;
    private String datasourceDescription = null;
    private String datasourceFactory = null;
    private String datasourceName = null;
    private String dsName = null;
    private String jdbcTestStatement = null;
    private String password = null;
    private String url = null;
    private String userName = null;
    private String datasourceMapper = null;
    private java.util.List checkingLevels = Jlists.getJdbcConnectionCheckingLevels();
    private String currentOpened = null;
    private String currentBusy = null;
    private String busyMaxRecent = null;
    private String busyMinRecent = null;
    private String currentInTx = null;
    private String openedCount = null;
    private String connectionFailures = null;
    private String connectionLeaks = null;
    private String currentWaiters = null;
    private String waitersHigh = null;
    private String waitersHighRecent = null;
    private String waiterCount = null;
    private String waitingTime = null;
    private String waitingHigh = null;
    private String waitingHighRecent = null;
    private String servedOpen = null;
    private String rejectedOpen = null;
    private String rejectedFull = null;
    private String rejectedTimeout = null;
    private String rejectedOther = null;
    private String jdbcConnCheckLevel = null;
    private String jdbcConnMaxAge = null;
    private String jdbcMaxOpenTime = null;
    private String jdbcMaxConnPool = null;
    private String jdbcMinConnPool = null;
    private String jdbcMaxWaitTime = null;
    private String jdbcMaxWaiters = null;
    private String jdbcSamplingPeriod = null;
    private String jdbcAdjustPeriod = null;
    private String jdbcPstmtMax = null;

    private ArrayList listUsedByEjb = new ArrayList();

    // --------------------------------------------------------- Public Methods

    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        action = "apply";
    }

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     * @return List of errors
     */
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors oErrors = new ActionErrors();

        validateInteger(oErrors, jdbcConnMaxAge, "jdbcConnMaxAge"
                        , "error.database.datasource.jdbcConnMaxAge.numberformat");
        validateInteger(oErrors, jdbcMaxOpenTime, "jdbcMaxOpenTime"
                        , "error.database.datasource.jdbcMaxOpenTime.numberformat");
        validateInteger(oErrors, jdbcMaxConnPool, "jdbcMaxConnPool"
                        , "error.database.datasource.jdbcMaxConnPool.numberformat");
        validateInteger(oErrors, jdbcMinConnPool, "jdbcMinConnPool"
                        , "error.database.datasource.jdbcMinConnPool.numberformat");
        validateInteger(oErrors, jdbcMaxWaitTime, "jdbcMaxWaitTime"
                        , "error.database.datasource.jdbcMaxWaitTime.numberformat");
        validateInteger(oErrors, jdbcMaxWaiters, "jdbcMaxWaiters"
                        , "error.database.datasource.jdbcMaxWaiters.numberformat");
        validateInteger(oErrors, jdbcPstmtMax, "jdbcPstmtMax"
                , "error.database.datasource.jdbcPstmtMax.numberformat");
        validateInteger(oErrors, jdbcSamplingPeriod, "jdbcSamplingPeriod"
                        , "error.database.datasource.jdbcSamplingPeriod.numberformat");
        validateInteger(oErrors, jdbcAdjustPeriod, "jdbcAdjustPeriod"
                , "error.database.datasource.jdbcAdjustPeriod.numberformat");

        return oErrors;
    }

    protected void validateInteger(ActionErrors p_Errors, String p_Value, String p_Tag
                                   , String p_ResError) {
        try {
            Integer.parseInt(p_Value);
        } catch (NumberFormatException e) {
            p_Errors.add(p_Tag, new ActionMessage(p_ResError));
        }
    }

    // --------------------------------------------------------- Properties Methods

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getCurrentOpened() {
        return currentOpened;
    }

    public void setCurrentOpened(String s) {
        currentOpened = s;
    }

    public String getCurrentBusy() {
        return currentBusy;
    }

    public void setCurrentBusy(String s) {
        currentBusy = s;
    }

    public String getCurrentInTx() {
        return currentInTx;
    }

    public void setCurrentInTx(String s) {
        currentInTx = s;
    }

    public String getOpenedCount() {
        return openedCount;
    }

    public void setOpenedCount(String s) {
        openedCount = s;
    }

    public String getConnectionFailures() {
        return connectionFailures;
    }

    public void setConnectionFailures(String s) {
        connectionFailures = s;
    }

    public String getConnectionLeaks() {
        return connectionLeaks;
    }

    public void setConnectionLeaks(String s) {
        connectionLeaks = s;
    }

    public String getCurrentWaiters() {
        return currentWaiters;
    }

    public void setCurrentWaiters(String s) {
        currentWaiters = s;
    }

    public String getWaitersHigh() {
        return waitersHigh;
    }

    public void setWaitersHigh(String s) {
        waitersHigh = s;
    }

    public String getWaitersHighRecent() {
        return waitersHighRecent;
    }

    public void setWaitersHighRecent(String s) {
        waitersHighRecent = s;
    }

    public String getBusyMaxRecent() {
        return busyMaxRecent;
    }

    public void setBusyMaxRecent(String s) {
        busyMaxRecent = s;
    }

    public String getBusyMinRecent() {
        return busyMinRecent;
    }

    public void setBusyMinRecent(String s) {
        busyMinRecent = s;
    }

    public String getWaiterCount() {
        return waiterCount;
    }

    public void setWaiterCount(String s) {
        waiterCount = s;
    }

    public String getWaitingTime() {
        return waitingTime;
    }

    public void setWaitingTime(String s) {
        waitingTime = s;
    }

    public String getWaitingHigh() {
        return waitingHigh;
    }

    public void setWaitingHigh(String s) {
        waitingHigh = s;
    }

    public String getWaitingHighRecent() {
        return waitingHighRecent;
    }

    public void setWaitingHighRecent(String s) {
        waitingHighRecent = s;
    }

    public String getServedOpen() {
        return servedOpen;
    }

    public void setServedOpen(String s) {
        servedOpen = s;
    }

    public String getRejectedOpen() {
        return rejectedOpen;
    }

    public void setRejectedOpen(String s) {
        rejectedOpen = s;
    }

    public String getRejectedFull() {
        return rejectedFull;
    }

    public void setRejectedFull(String s) {
        rejectedFull = s;
    }

    public String getRejectedTimeout() {
        return rejectedTimeout;
    }

    public void setRejectedTimeout(String s) {
        rejectedTimeout = s;
    }

    public String getRejectedOther() {
        return rejectedOther;
    }

    public void setRejectedOther(String s) {
        rejectedOther = s;
    }

    public String getDatasourceDescription() {
        return datasourceDescription;
    }

    public void setDatasourceDescription(String datasourceDescription) {
        this.datasourceDescription = datasourceDescription;
    }

    public String getDatasourceName() {
        return datasourceName;
    }

    public void setDatasourceName(String datasourceName) {
        this.datasourceName = datasourceName;
    }

    public String getDatasourceMapper() {
        return datasourceMapper;
    }

    public void setDatasourceMapper(String datasourceMapper) {
        this.datasourceMapper = datasourceMapper;
    }

    public String getDsName() {
        return dsName;
    }

    public void setDsName(String dsName) {
        this.dsName = dsName;
    }

    public String getJdbcConnCheckLevel() {
        return jdbcConnCheckLevel;
    }

    public void setJdbcConnCheckLevel(String jdbcConnCheckLevel) {
        this.jdbcConnCheckLevel = jdbcConnCheckLevel;
    }

    public String getJdbcConnMaxAge() {
        return jdbcConnMaxAge;
    }

    public void setJdbcConnMaxAge(String s) {
        jdbcConnMaxAge = s;
    }

    public String getJdbcMaxOpenTime() {
        return jdbcMaxOpenTime;
    }

    public void setJdbcMaxOpenTime(String s) {
        jdbcMaxOpenTime = s;
    }

    public String getJdbcMaxConnPool() {
        return jdbcMaxConnPool;
    }

    public void setJdbcMaxConnPool(String jdbcMaxConnPool) {
        this.jdbcMaxConnPool = jdbcMaxConnPool;
    }

    public String getJdbcMinConnPool() {
        return jdbcMinConnPool;
    }

    public void setJdbcMinConnPool(String jdbcMinConnPool) {
        this.jdbcMinConnPool = jdbcMinConnPool;
    }

    public String getJdbcMaxWaitTime() {
        return jdbcMaxWaitTime;
    }

    public void setJdbcMaxWaitTime(String jdbcMaxWaitTime) {
        this.jdbcMaxWaitTime = jdbcMaxWaitTime;
    }

    public String getJdbcMaxWaiters() {
        return jdbcMaxWaiters;
    }

    public void setJdbcMaxWaiters(String s) {
        this.jdbcMaxWaiters = s;
    }

    public String getJdbcSamplingPeriod() {
        return jdbcSamplingPeriod;
    }

    public void setJdbcSamplingPeriod(String s) {
        this.jdbcSamplingPeriod = s;
    }

    public String getJdbcAdjustPeriod() {
        return jdbcAdjustPeriod;
    }

    public void setJdbcAdjustPeriod(String s) {
        this.jdbcAdjustPeriod = s;
    }

    public String getJdbcTestStatement() {
        return jdbcTestStatement;
    }

    public void setJdbcTestStatement(String jdbcTestStatement) {
        this.jdbcTestStatement = jdbcTestStatement;
    }

    public String getJdbcPstmtMax() {
        return jdbcPstmtMax;
    }

    public void setJdbcPstmtMax(String jdbcPstmtMax) {
        this.jdbcPstmtMax = jdbcPstmtMax;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public java.util.List getCheckingLevels() {
        return checkingLevels;
    }

    public ArrayList getListUsedByEjb() {
        return listUsedByEjb;
    }

    public void setListUsedByEjb(ArrayList listUsedByEjb) {
        this.listUsedByEjb = listUsedByEjb;
    }

}
