/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.clusterd;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.monitoring.graph.utils.GraphUtils;


/**
 * @author Michel-Ange Anton
 */

public class EditClusterdVmMemoryAction extends JonasBaseAction {
    int sizeTableMeasuresVmMemory;
    int rangeVmMemory;


// --------------------------------------------------------- Public Methods
    @Override
    public ActionForward executeAction(final ActionMapping p_Mapping, final ActionForm p_Form
        , final HttpServletRequest p_Request, final HttpServletResponse p_Response)
        throws IOException, ServletException {
        // Form used
        MemoryForm oForm = (MemoryForm) p_Form;
        try {
            // Object name used
            String cdName = m_WhereAreYou.getCurrentClusterDaemonName();
            sizeTableMeasuresVmMemory = m_WhereAreYou.getClusterdGraphAttribute(cdName, "sizeTableMeasuresVmMemory");
            rangeVmMemory = m_WhereAreYou.getClusterdGraphAttribute(cdName, "rangeVmMemory");
            if (sizeTableMeasuresVmMemory == -1) {
                //session attribute has never been set
                sizeTableMeasuresVmMemory = GraphUtils.DEF_SIZE_TABLE_MEASURES;
                rangeVmMemory = GraphUtils.DEF_RANGE;
                m_WhereAreYou.setClusterdGraphAttribute(cdName, "sizeTableMeasuresVmMemory", sizeTableMeasuresVmMemory);
                m_WhereAreYou.setClusterdGraphAttribute(cdName, "rangeVmMemory", rangeVmMemory);
            }
            oForm.setNumberOfMeasures(sizeTableMeasuresVmMemory);
            oForm.setOwnerName(cdName);
            oForm.setRange(rangeVmMemory);
            oForm.refreshGraphic();
        }
        catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        // Forward to the jsp.
        return (p_Mapping.findForward("ClusterdVmMemory"));
    }
}
