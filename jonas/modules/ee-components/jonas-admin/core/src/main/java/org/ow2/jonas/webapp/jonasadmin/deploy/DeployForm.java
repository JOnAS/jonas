/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.deploy;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author Michel-Ange ANTON
 * @author Adriana Danes
 */

public class DeployForm extends ActionForm {

// --------------------------------------------------------- Constants

// --------------------------------------------------------- Properties variables

    private ArrayList listDeployable = new ArrayList();
    private ArrayList listDeployed = new ArrayList();
    private ArrayList listDeploy = new ArrayList();
    private ArrayList listUndeploy = new ArrayList();
    private String deploy = null;
    private String undeploy = null;
    private String[] deploySelected = new String[0];
    private String[] undeploySelected = new String[0];
    private ArrayList listAdd = new ArrayList();
    private ArrayList listRemove = new ArrayList();
    private boolean confirm = false;
    private boolean isConfigurable;
    /**
     * True if the deoable/udeployable is a JAR, WAR, RAR or EAR
     */
    private boolean isModule;

// --------------------------------------------------------- Public Methods

    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        deploySelected = new String[0];
        undeploySelected = new String[0];
    }

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors oErrors = new ActionErrors();
        return oErrors;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(" listDeployable = ").append(listDeployable).append("\n");
        sb.append(" listDeployed = ").append(listDeployed).append("\n");
        sb.append(" listDeploy = ").append(listDeploy).append("\n");
        sb.append(" listUndeploy = ").append(listUndeploy).append("\n");
        sb.append(" listAdd = ").append(listAdd).append("\n");
        sb.append(" listRemove = ").append(listRemove).append("\n");
        sb.append(" deploySelected = [");
        for (int i = 0; i < deploySelected.length; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(deploySelected[i]);
        }
        sb.append("]\n");
        sb.append(" undeploySelected = [");
        for (int i = 0; i < undeploySelected.length; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(undeploySelected[i]);
        }
        sb.append("]\n");

        sb.append(" deploy = ").append(deploy).append("\n");
        sb.append(" undeploy = ").append(undeploy).append("\n");

        return sb.toString();
    }

// --------------------------------------------------------- Properties Methods

    public ArrayList getListDeployable() {
        return listDeployable;
    }

    public void setListDeployable(ArrayList listDeployable) {
        this.listDeployable = listDeployable;
    }

    public ArrayList getListDeployed() {
        return listDeployed;
    }

    public void setListDeployed(ArrayList listDeployed) {
        this.listDeployed = listDeployed;
    }

    public String[] getDeploySelected() {
        return deploySelected;
    }

    public void setDeploySelected(String[] deploySelected) {
        this.deploySelected = deploySelected;
    }

    public String[] getUndeploySelected() {
        return undeploySelected;
    }

    public void setUndeploySelected(String[] undeploySelected) {
        this.undeploySelected = undeploySelected;
    }

    public ArrayList getListDeploy() {
        return listDeploy;
    }

    public void setListDeploy(ArrayList listDeploy) {
        this.listDeploy = listDeploy;
    }

    public ArrayList getListUndeploy() {
        return listUndeploy;
    }

    public void setListUndeploy(ArrayList listUndeploy) {
        this.listUndeploy = listUndeploy;
    }

    public String getDeploy() {
        return deploy;
    }

    public void setDeploy(String deploy) {
        this.deploy = deploy;
    }

    public String getUndeploy() {
        return undeploy;
    }

    public void setUndeploy(String undeploy) {
        this.undeploy = undeploy;
    }

    public ArrayList getListAdd() {
        return listAdd;
    }

    public void setListAdd(ArrayList listAdd) {
        this.listAdd = listAdd;
    }

    public ArrayList getListRemove() {
        return listRemove;
    }

    public void setListRemove(ArrayList listRemove) {
        this.listRemove = listRemove;
    }

    public boolean isConfirm() {
        return confirm;
    }

    public void setConfirm(boolean confirm) {
        this.confirm = confirm;
    }

    /**
     * Return if the current deployment type is configurable.
     * @return if the current deployment type is configurable.
     */
    public boolean getIsConfigurable() {
        return this.isConfigurable;
    }

    /**
     * Sets if the current deployment type is configurable.
     * @param isDomain if current deployment type is configurable.
     */
    public void setIsConfigurable(boolean isConfigurable) {
        this.isConfigurable = isConfigurable;
    }
/**
 * Return true if the current deployment on a server concerns a JAR, RAR, WAR or EAR
 * @return true if the current deployment on a server concerns a JAR, RAR, WAR or EAR
 */
    public boolean getIsModule() {
        return isModule;
    }
/**
 * Set isModule to true if the current deployment on a server concerns a JAR, RAR, WAR or EAR
 * @param isModule true if the current deployment on a server concerns a JAR, RAR, WAR or EAR
 */
    public void setIsModule(boolean isModule) {
        this.isModule = isModule;
    }

}
