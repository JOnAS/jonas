/*
 * $Header$
 * $Revision: 14503 $
 * $Date: 2008-07-10 17:13:06 +0200 (jeu 10 jui 2008) $
 *
 * ====================================================================
 *
 * The Apache Software License, Version 1.1
 *
 * Copyright (c) 2001-2002 The Apache Software Foundation.  All rights
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution, if
 *    any, must include the following acknowlegement:
 *       "This product includes software developed by the
 *        Apache Software Foundation (http://www.apache.org/)."
 *    Alternately, this acknowlegement may appear in the software itself,
 *    if and wherever such third-party acknowlegements normally appear.
 *
 * 4. The names "The Jakarta Project", "Tomcat", and "Apache Software
 *    Foundation" must not be used to endorse or promote products derived
 *    from this software without prior written permission. For written
 *    permission, please contact apache@apache.org.
 *
 * 5. Products derived from this software may not be called "Apache"
 *    nor may "Apache" appear in their names without prior written
 *    permission of the Apache Group.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE APACHE SOFTWARE FOUNDATION OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */

package org.ow2.jonas.webapp.jonasadmin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import org.ow2.jonas.webapp.taglib.LabelValueBean;


/**
 * General purpose utility methods to create lists of objects that are
 * commonly required in building the user interface.  In all cases, if there
 * are no matching elements, a zero-length list (rather than <code>null</code>)
 * is returned.
 *
 * @author Craig R. McClanahan
 * @version $Revision: 14503 $ $Date: 2008-07-10 17:13:06 +0200 (jeu 10 jui 2008) $
 */

public class Jlists {

// --------------------------------------------------------- Constants

    public final static String SEPARATOR = ",";

// ----------------------------------------------------------- Constructors

    /**
     * Protected constructor to prevent instantiation.
     */
    protected Jlists() {}

// ------------------------------------------------------- Static Variables

    /**
     * Precomputed list of debug level labels and values.
     */
    private static List debugLevels = new ArrayList();

    static {
        debugLevels.add(new LabelValueBean("0", "0"));
        debugLevels.add(new LabelValueBean("1", "1"));
        debugLevels.add(new LabelValueBean("2", "2"));
        debugLevels.add(new LabelValueBean("3", "3"));
        debugLevels.add(new LabelValueBean("4", "4"));
        debugLevels.add(new LabelValueBean("5", "5"));
        debugLevels.add(new LabelValueBean("6", "6"));
        debugLevels.add(new LabelValueBean("7", "7"));
        debugLevels.add(new LabelValueBean("8", "8"));
        debugLevels.add(new LabelValueBean("9", "9"));
    }

    /**
     * Precomputed list of verbosity level labels and values.
     */
    private static List verbosityLevels = new ArrayList();

    static {
        verbosityLevels.add(new LabelValueBean("0", "0"));
        verbosityLevels.add(new LabelValueBean("1", "1"));
        verbosityLevels.add(new LabelValueBean("2", "2"));
        verbosityLevels.add(new LabelValueBean("3", "3"));
        verbosityLevels.add(new LabelValueBean("4", "4"));
    }

    /**
     * Precomputed list of Jonas logger level labels and values.
     */
    private static List loggerJonasLevels = new ArrayList();

    static {
        loggerJonasLevels.add(new LabelValueBean("DEBUG", "DEBUG"));
        loggerJonasLevels.add(new LabelValueBean("WARN", "WARN"));
        loggerJonasLevels.add(new LabelValueBean("INFO", "INFO"));
        loggerJonasLevels.add(new LabelValueBean("ERROR", "ERROR"));
        loggerJonasLevels.add(new LabelValueBean("INHERIT", "INHERIT"));
    }

    /**
     * Precomputed list of (true,false) labels and values.
     */
    private static List booleanValues = new ArrayList();

    static {
        booleanValues.add(new LabelValueBean("True", "true"));
        booleanValues.add(new LabelValueBean("False", "false"));
    }

    /**
     * Precomputed list of JdbcConnectionCheckingLevel labels and values.
     */
    private static List jdbcConnectionCheckingLevels = new ArrayList();

    static {
        jdbcConnectionCheckingLevels.add(new LabelValueBean("0", "0"));
        jdbcConnectionCheckingLevels.add(new LabelValueBean("1", "1"));
        jdbcConnectionCheckingLevels.add(new LabelValueBean("2", "2"));
        jdbcConnectionCheckingLevels.add(new LabelValueBean("3", "3"));
    }

    /**
     * Precomputed list of SpecVersions labels and values.
     */
    private static List specVersion = new ArrayList();

    static {
        specVersion.add(new LabelValueBean("1.0", "1.0"));
        specVersion.add(new LabelValueBean("1.5", "1.5"));
    }

    /**
     * Precomputed list of SecurityAuthenticationLdap labels and values.
     */
    private static List securityAuthenticationLdapValues = new ArrayList();

    static {
        securityAuthenticationLdapValues.add(new LabelValueBean("none", "none"));
        securityAuthenticationLdapValues.add(new LabelValueBean("simple", "simple"));
        securityAuthenticationLdapValues.add(new LabelValueBean("strong", "strong"));
        securityAuthenticationLdapValues.add(new LabelValueBean("CRAM-MD5", "CRAM-MD5"));
        securityAuthenticationLdapValues.add(new LabelValueBean("DIGEST-MD5", "DIGEST-MD5"));
    }

    /**
     * Precomputed list of AuthenticationModeLdapValues labels and values.
     */
    private static List authenticationModeLdapValues = new ArrayList();

    static {
        authenticationModeLdapValues.add(new LabelValueBean("compare", "compare"));
        authenticationModeLdapValues.add(new LabelValueBean("bind", "bind"));
    }

    /**
     * Precomputed list of securityAlgorithms labels and values.
     */
    private static List securityAlgorithms = new ArrayList();

    static {
        securityAlgorithms.add(new LabelValueBean("", ""));
        securityAlgorithms.add(new LabelValueBean("MD5", "MD5"));
        securityAlgorithms.add(new LabelValueBean("MD2", "MD2"));
        securityAlgorithms.add(new LabelValueBean("SHA-1", "SHA-1"));
        securityAlgorithms.add(new LabelValueBean("SHA", "SHA"));
    }

    /**
     * Precomputed list of monitoring Apply labels and values.
     */
    private static List monitoringApplySettingsValues = new ArrayList();

    static {
        monitoringApplySettingsValues.add(new LabelValueBean("All EJBs", "allEjbs"));
        monitoringApplySettingsValues.add(new LabelValueBean("All EJBs with inherited settings", "inheritedEjbs"));
        monitoringApplySettingsValues.add(new LabelValueBean("All EJBs deployed in the future", "futureDeployedEjbs"));
        }

// --------------------------------------------------------- Public Methods

    /**
     * Return a <code>List</code> of {@link LabelValueBean}s for the legal
     * settings for <code>debug</code> properties.
     */
    public static List getDebugLevels() {
        return (debugLevels);
    }

    /**
     * Return a <code>List</code> of {@link LabelValueBean}s for the legal
     * settings for <code>verbosity</code> properties.
     */
    public static List getVerbosityLevels() {
        return (verbosityLevels);
    }

    /**
     * Return a <code>List</code> of {@link LabelValueBean}s for the legal
     * settings for <code>boolean</code> properties.
     */
    public static List getBooleanValues() {
        return (booleanValues);
    }

    /**
     * Return a <code>List</code> of {@link LabelValueBean}s for the legal
     * settings for <code>Logger Jonas Levels</code> properties.
     */
    public static List getLoggerJonasLevels() {
        return (loggerJonasLevels);
    }

    /**
     * Return a <code>List</code> of {@link LabelValueBean}s for the legal
     * settings for Jdbc Connection Checking Levels properties.
     */
    public static List getJdbcConnectionCheckingLevels() {
        return (jdbcConnectionCheckingLevels);
    }

    /**
     * Return a <code>List</code> of {@link LabelValueBean}s for the legal
     * settings for Spec-Version property.
     * @return a <code>List</code> of legal values.
     */
    public static List getSpecVersion() {
        return (specVersion);
    }

    /**
     * Return a <code>List</code> of {@link LabelValueBean}s for the legal
     * settings for SecurityAuthenticationLdap properties.
     */
    public static List getSecurityAuthenticationLdapValues() {
        return (securityAuthenticationLdapValues);
    }

    /**
     * Return a <code>List</code> of {@link LabelValueBean}s for the legal
     * settings for AuthenticationModeLdap properties.
     */
    public static List getAuthenticationModeLdapValues() {
        return (authenticationModeLdapValues);
    }

    /**
     * Return a <code>List</code> of {@link LabelValueBean}s for the legal
     * settings for SecurityAlgorithms properties.
     */
    public static List getSecurityAlgorithms() {
        return (securityAlgorithms);
    }

    /**
     * Transform the collection like a string, each element are separated by the separator.
     *
     * @param p_Collection The collection to transform
     * @param p_Separator The separator
     * @return The collection string
     */
    public static String getString(final Collection p_Collection, final String p_Separator) {
        StringBuffer sb = new StringBuffer();
        Iterator it = p_Collection.iterator();
        while (it.hasNext()) {
            sb.append(it.next());
            if (it.hasNext()) {
                sb.append(p_Separator);
            }
        }
        return sb.toString();
    }

    /**
     * Transform the string like a ArrayString, each element are separated by the separator.
     *
     * @param p_String
     * @param p_Separator The separator
     * @return The list
     */
    public static ArrayList getArrayList(final String p_String, final String p_Separator) {
        ArrayList al = new ArrayList();
        StringTokenizer st = new StringTokenizer(p_String, p_Separator);
        while (st.hasMoreTokens()) {
            al.add(st.nextToken().trim());
        }
        return al;
    }

    /**
     * Return a <code>List</code> of {@link LabelValueBean}s for the apply
     * settings for EJB monitoring .
     */
    public static List getMonitoringApplySettingsValues() {
        return (monitoringApplySettingsValues);
    }
}
