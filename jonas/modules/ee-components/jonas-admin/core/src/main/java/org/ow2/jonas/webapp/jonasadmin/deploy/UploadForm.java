/**
 * Copyright 1999-2004 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.deploy;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.upload.FormFile;
import org.apache.struts.upload.MultipartRequestHandler;

/**
 * This class is a placeholder for form values.  In a multipart request, files are represented by
 * set and get methods that use the class org.apache.struts.upload.FormFile, an interface with
 * basic methods to retrieve file information.  The actual structure of the FormFile is dependant
 * on the underlying impelementation of multipart request handling.  The default implementation
 * that struts uses is org.apache.struts.upload.CommonsMultipartRequestHandler.
 *
 * @version $Rev: 11749 $ $Date: 2007-10-08 11:47:44 +0200 (lun 08 oct 2007) $
 */
public class UploadForm extends ActionForm {

    /**
     * The file that the user has uploaded
     */
    private FormFile uploadedFile;
    
    /**
     * Is the current context a domain management action?
     */
    private boolean isDomain;
    
    /**
     * Is the current type of deployment currently configurable?
     */
    private boolean isConfigurable;
    
    /**
     * Overwrite value
     */
    private boolean overwrite = false;
    
    /**
     * Return if the action is a domain management operation.
     * @return if the action is a domain management operation.
     */
    public boolean getIsDomain() {
        return this.isDomain;
    }
    
    /**
     * Sets if the action is a domain management operation.
     * @param isDomain if the action is a domain management operation.
     */
    public void setIsDomain(boolean isDomain) {
        this.isDomain = isDomain;
    }
    
     /**
     * @return a representation of the file the user has uploaded
     */
    public FormFile getUploadedFile() {
        return uploadedFile;
    }

    /**
     * Set a representation of the file the user has uploaded
     * @param uploadedFile the file uploaded
     */
    public void setUploadedFile(FormFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }
    
    
    /**
     * Check to make sure the client hasn't exceeded the maximum allowed upload size inside of this
     * validate method.
     * @param mapping the mapping used to select this instance
     * @param request the servlet request we are processing
     * @return Errors if it fails
     */
    public ActionErrors validate(
        ActionMapping mapping,
        HttpServletRequest request) {

        ActionErrors errors = null;
        //has the maximum length been exceeded?
        Boolean maxLengthExceeded =
            (Boolean) request.getAttribute(
                MultipartRequestHandler.ATTRIBUTE_MAX_LENGTH_EXCEEDED);

        if ((maxLengthExceeded != null) && (maxLengthExceeded.booleanValue())) {
            errors = new ActionErrors();
            errors.add(
                ActionMessages.GLOBAL_MESSAGE ,
                new ActionMessage("maxLengthExceeded"));
            errors.add(
                ActionMessages.GLOBAL_MESSAGE ,
                new ActionMessage("maxLengthExplanation"));
        }
        return errors;

    }

    /**
     * @return true if the file will be overwritten
     */
    public boolean isOverwrite() {
        return overwrite;
    }


    /**
     * Replace the existing file ?
     * @param overwrite true/false
     */
    public void setOverwrite(boolean overwrite) {
        this.overwrite = overwrite;
    }
    
    /**
     * Return if the current deployment type is configurable.
     * @return if the current deployment type is configurable.
     */
    public boolean getIsConfigurable() {
        return this.isConfigurable;
    }
    
    /**
     * Sets if the current deployment type is configurable.
     * @param isDomain if current deployment type is configurable.
     */
    public void setIsConfigurable(boolean isConfigurable) {
        this.isConfigurable = isConfigurable;
    }    
}