/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin;

import javax.management.ObjectName;

/**
 * A set of static classes used to build the names of Jetty MBeans used in JOnAS.
 *
 * @author Michel-Ange ANTON
 */
public class JettyObjectName {

    public static ObjectName jettyServer(String pDomain) {
        try {
            return new ObjectName(pDomain + ":type=webContainer,name=Jetty");
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    public static ObjectName jettyContexts(String pDomain) {
        try {
            return new ObjectName(pDomain + ":type=webContainer,name=Jetty,*");
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }

    public static ObjectName jettyContexts(String pDomain, String pPath) {
        try {
            return new ObjectName(pDomain + ":type=webContainer,name=Jetty,context=" + pPath + ",*");
        } catch (javax.management.MalformedObjectNameException e) {
            // this should never occur
            return null;
        }
    }
}
