/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 */

package org.ow2.jonas.webapp.jonasadmin.joramplatform;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.util.JoramObjectName;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;


/**
 * @author Adriana Danes
 */

public class EditJoramServerAction extends EditJoramBaseAction {

//	--------------------------------------------------------- Public Methods

    @Override
    public ActionForward executeAction(final ActionMapping pMapping, final ActionForm pForm
            , final HttpServletRequest pRequest, final HttpServletResponse pResponse)
    throws IOException, ServletException {

        // Current JOnAS server
        String serverName =  m_WhereAreYou.getCurrentJonasServerName();
        String domainName = m_WhereAreYou.getCurrentDomainName();

        // Serve id currently in management
        String serverId = pRequest.getParameter("id");
        if (serverId == null) {
            serverId = (String) m_Session.getAttribute("currentId");
        } else {
            m_Session.setAttribute("currentId", serverId);
        }
        String localId = (String) m_Session.getAttribute("localId");
        boolean collocatedServer = ((Boolean) m_Session.getAttribute("collocatedServer")).booleanValue();
        boolean isLocalServer;
        if (localId.equals(serverId)) {
            isLocalServer = true;
        } else {
            isLocalServer = false;
        }

        m_Session.setAttribute("isLocalServer", new Boolean(isLocalServer));
        // Get reference on EJB container service in order to determine dependencies
        // between the deployed EJBs and JMS destinations
        initRefs(domainName, serverName);

        try {
            ObjectName joramAdapterON = JoramObjectName.joramAdapter();
            boolean isAdapterLoaded = JonasManagementRepr.isRegistered(joramAdapterON, serverName);

            ArrayList destinationsArray = new ArrayList();
            ArrayList topicsArray = new ArrayList();

            Short id = new Short(serverId);
            // Get all the destinations
            Object[] asParam = {
                    id
            };
            String[] asSignature = {
                    "short"
            };
            String[] destinations = (String[]) JonasManagementRepr.invoke(joramAdapterON, "getDestinations", asParam, asSignature, serverName);
            for (int i = 0; i < destinations.length; i++) {
                String dest = destinations[i];
                ItemDestination destinationItem = getDestinationItem(dest, serverName);
                if ("Queue".equals(destinationItem.getType())) {
                    // insert in head
                    destinationsArray.add(0, destinationItem);
                } else if ("Topic".equals(destinationItem.getType())) {
                    // append
                    topicsArray.add(destinationItem);
                }
            }

            // sort queues
            Collections.sort(destinationsArray, new ItemDestinationByName());
            // sort topics
            Collections.sort(topicsArray, new ItemDestinationByName());
            // append topics to queues
            destinationsArray.addAll(topicsArray);

            ArrayList usersArray = new ArrayList();
            String[] usersList = (String[]) JonasManagementRepr.invoke(joramAdapterON, "getUsers", asParam, asSignature, serverName);
            for (int i = 0; i < usersList.length; i++) {
                String userString = usersList[i];
                ItemUser userItem = getUserItem(userString, serverName);
                if (userItem.getName() != null) {
                    usersArray.add(userItem);
                }
            }

            m_Session.setAttribute("destinations", destinationsArray);
            m_Session.setAttribute("users", usersArray);
        } catch (Throwable t) {
            return (treatError(t, pMapping, pRequest));
        }

        // Force the node selected in tree
        String nodeName = null;
        if (isLocalServer) {
            if (collocatedServer) {
                nodeName = getTreeBranchName(DEPTH_DOMAIN) + WhereAreYou.NODE_SEPARATOR
                + "joramplatform" + WhereAreYou.NODE_SEPARATOR
                + "joramlocalserver";
            } else {
                nodeName = getTreeBranchName(DEPTH_DOMAIN) + WhereAreYou.NODE_SEPARATOR
                + "joramplatform" + WhereAreYou.NODE_SEPARATOR
                + "joramcurrentserver";

            }
            m_WhereAreYou.selectNameNode(nodeName, true);
            // Forward to the jsp.
            return (pMapping.findForward("JoramDestinations"));
        } else {
            nodeName = getTreeBranchName(DEPTH_DOMAIN) + WhereAreYou.NODE_SEPARATOR
            + "joramplatform" + WhereAreYou.NODE_SEPARATOR
            + "joramremoteserver" + serverId;
            m_WhereAreYou.selectNameNode(nodeName, true);
            return (pMapping.findForward("JoramRemoteDestinations"));
        }

    }

}
