/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2003-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.domain;

import java.io.IOException;
import java.util.ArrayList;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.management.javaee.J2eeObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Adriana Danes
 */
public class ApplyJonasServersConfirmAction extends JonasBaseAction {

//	--------------------------------------------------------- Public Methods

    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
            , HttpServletRequest p_Request, HttpServletResponse p_Response)
    throws IOException, ServletException {
        // List of servers selected to be removed
        ArrayList alSelected = (ArrayList) m_Session.getAttribute("listServersSelected");
        m_Session.setAttribute("listServersSelected", null);

        if (alSelected != null) {
            // DomainName
            String domainName = m_WhereAreYou.getCurrentDomainName();
            // Current server nmae
            String currentServerName = m_WhereAreYou.getCurrentJonasServerName();
            // Current cluster name
            String clusterName = (String) m_Session.getAttribute("currentCluster");
            // Check if the selected cluster is the domain's one
            boolean domainRemove = false;
            if (clusterName.equals(domainName)) {
                // In this case, the server have to be removed from all the clusters
                domainRemove = true;
            }
            // Construct a list with the cluster ons for removing
            ArrayList removeClOns = new ArrayList();
            ObjectName domainClusterOn = null;
            // Actions
            try {
                ObjectName domainOn = J2eeObjectName.J2EEDomain(domainName);
                // All clusters Ons
                String[] clOns = (String[]) JonasManagementRepr.getAttribute(domainOn, "clusters", currentServerName);
                for (int index = 0; index < clOns.length; index++) {
                    ObjectName clOn = ObjectName.getInstance(clOns[index]);
                    String clType = clOn.getKeyProperty("type");
                    String clName = clOn.getKeyProperty("name");
                    if (!domainRemove) {
                        // put clOn in the list for removing if this the current cluster's on
                        if ("LogicalCluster".equals(clType) && clusterName.equals(clName)) {
                            removeClOns.add(clOn);
                            break;
                        }
                    } else {
                        if ("LogicalCluster".equals(clType) && domainName.equals(clName)) {
                            // keep the domain cluster On in domainClusterOn variable
                            domainClusterOn = clOn;
                        } else {
                            removeClOns.add(clOn);
                        }
                    }
                }
                if (domainRemove) {
                    // add domainClusterOn et the end in order to be the last treated one
                    removeClOns.add(domainClusterOn);
                    m_Session.setAttribute("mapChanged", new Boolean(true));
                }
                // Remove all the servers from all the clusters in removeClOns
                for (int j = 0; j < alSelected.size(); j++) {
                    String serverName = (String) alSelected.get(j);
                    for (int i = 0; i < removeClOns.size(); i++) {
                        ObjectName clOn = (ObjectName) removeClOns.get(i);
                        String[] signature = {"java.lang.String" };
                        String[] params = {serverName };
                        JonasManagementRepr.invoke(clOn, "removeServer", params, signature, currentServerName);
                    }
                }
            } catch (Throwable t) {
                addGlobalError(t);
                saveErrors(p_Request, m_Errors);
                return (p_Mapping.findForward("Global Error"));
            }
        }
        // Forward to the domain page
        return p_Mapping.findForward("ActionEditDomain");
    }
}
