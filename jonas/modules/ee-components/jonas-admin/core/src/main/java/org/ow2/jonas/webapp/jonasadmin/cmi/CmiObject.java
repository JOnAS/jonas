/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.cmi;

import java.io.Serializable;

public class CmiObject implements Serializable {

    /**
     * Serial UID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Object name.
     */
    private String name;
    /**
     * True if the object is local
     */
    private boolean isLocal;
    /**
     * The uri for the object icon
     */
    private String iconSource;
    /**
     * Constructor using fields.
     * @param name
     * @param isLocal
     * @param iconSource
     * @param serverRefs
     */
    public CmiObject(final String name, final boolean isLocal, final String iconSource) {
        super();
        this.name = name;
        this.isLocal = isLocal;
        this.iconSource = iconSource;
    }
    /**
     * Default constructor.
     */
    public CmiObject() {

    }

    /**
     * @return
     */
    public String getName() {
        return name;
    }
    /**
     * @param name
     */
    public void setName(final String name) {
        this.name = name;
    }
    /**
     * @return
     */
    public boolean getIsLocal() {
        return isLocal;
    }
    /**
     * @param isLocal
     */
    public void setIsLocal(final boolean isLocal) {
        this.isLocal = isLocal;
    }
    /**
     * @return
     */
    public String getIconSource() {
        return iconSource;
    }
    /**
     * @param iconSource
     */
    public void setIconSource(final String iconSource) {
        this.iconSource = iconSource;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return name;
    }

}
