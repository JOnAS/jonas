/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.security;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * @author Michel-Ange ANTON
 */

public class ApplyLdapRealmAction extends BaseMemoryRealmAction {

// --------------------------------------------------------- Public Methods

    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws IOException, ServletException {

        WhereAreYou oWhere = (WhereAreYou) p_Request.getSession().getAttribute(WhereAreYou.
                SESSION_NAME);
        String serverName = oWhere.getCurrentJonasServerName();
        String domainName = oWhere.getCurrentDomainName();

        // Form used
        LdapRealmForm oForm = (LdapRealmForm) p_Form;

        // Populate MBean
        try {
            // Create new
            if (oForm.getAction().equals("create")) {
                oForm.setResource(oForm.getName());
                // Add resource
                ObjectName oObjectName = JonasObjectName.securityService(domainName);
                String[] asParam = {
                    oForm.getName(), oForm.getInitialContextFactory(), oForm.getProviderUrl()
                    , oForm.getSecurityAuthentication(), oForm.getSecurityPrincipal()
                    , oForm.getSecurityCredentials(), oForm.getSecurityProtocol()
                    , oForm.getLanguage(), oForm.getReferral(), oForm.getStateFactories()
                    , oForm.getAuthenticationMode(), oForm.getUserPasswordAttribute()
                    , oForm.getUserRolesAttribute(), oForm.getRoleNameAttribute(), oForm.getBaseDn()
                    , oForm.getUserDn(), oForm.getUserSearchFilter(), oForm.getRoleDn()
                    , oForm.getRoleSearchFilter(), oForm.getAlgorithm()};
                String[] asSignature = {
                    "java.lang.String", "java.lang.String", "java.lang.String", "java.lang.String"
                    , "java.lang.String", "java.lang.String", "java.lang.String"
                    , "java.lang.String", "java.lang.String", "java.lang.String"
                    , "java.lang.String", "java.lang.String", "java.lang.String"
                    , "java.lang.String", "java.lang.String", "java.lang.String"
                    , "java.lang.String", "java.lang.String", "java.lang.String"
                    , "java.lang.String"};
                JonasManagementRepr.invoke(oObjectName, "addJResourceLDAP", asParam, asSignature, serverName);
                // refresh tree
                refreshTree(p_Request);
                // Force the node selected in tree
                m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_SERVER)
                    + WhereAreYou.NODE_SEPARATOR + "security" + WhereAreYou.NODE_SEPARATOR
                    + "factory.ldap" + WhereAreYou.NODE_SEPARATOR + oForm.getName(), true);
                // To see the form
                oForm.setAction("apply");
            }
            else {
                // Modify existing
                ObjectName oObjectName = JonasObjectName.securityLdapFactory(domainName, oForm.getResource());
                setStringAttribute(oObjectName, "AuthenticationMode", oForm.getAuthenticationMode());
                setStringAttribute(oObjectName, "BaseDN", oForm.getBaseDn());
                setStringAttribute(oObjectName, "InitialContextFactory"
                    , oForm.getInitialContextFactory());
                setStringAttribute(oObjectName, "Language", oForm.getLanguage());
                setStringAttribute(oObjectName, "ProviderUrl", oForm.getProviderUrl());
                setStringAttribute(oObjectName, "Referral", oForm.getReferral());
                setStringAttribute(oObjectName, "RoleDN", oForm.getRoleDn());
                setStringAttribute(oObjectName, "RoleNameAttribute", oForm.getRoleNameAttribute());
                setStringAttribute(oObjectName, "RoleSearchFilter", oForm.getRoleSearchFilter());
                setStringAttribute(oObjectName, "SecurityAuthentication"
                    , oForm.getSecurityAuthentication());
                setStringAttribute(oObjectName, "SecurityCredentials", oForm.getSecurityCredentials());
                setStringAttribute(oObjectName, "SecurityPrincipal", oForm.getSecurityPrincipal());
                setStringAttribute(oObjectName, "SecurityProtocol", oForm.getSecurityProtocol());
                setStringAttribute(oObjectName, "StateFactories", oForm.getStateFactories());
                setStringAttribute(oObjectName, "UserDN", oForm.getUserDn());
                setStringAttribute(oObjectName, "UserPasswordAttribute"
                    , oForm.getUserPasswordAttribute());
                setStringAttribute(oObjectName, "UserRolesAttribute", oForm.getUserRolesAttribute());
                setStringAttribute(oObjectName, "UserSearchFilter", oForm.getUserSearchFilter());
                setStringAttribute(oObjectName, "Algorithm", oForm.getAlgorithm());
            }
            // Save
            if (oForm.isSave()) {
                ObjectName onRealm = JonasObjectName.securityLdapFactory(domainName, oForm.getResource());
                JonasManagementRepr.invoke(onRealm, "saveConfig", null, null, serverName);
            }
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        // Forward to the jsp.
        return (p_Mapping.findForward("Ldap Realm"));
    }

// --------------------------------------------------------- Protected Methods

}
