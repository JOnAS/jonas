/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2003-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.catalina;

import java.io.Serializable;
import java.net.InetAddress;

import javax.management.ObjectName;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;


/**
 * @author Michel-Ange ANTON
 * @author danesa
 */
public class ConnectorItem implements Serializable {

// --------------------------------------------------------- Properties Variables

    private String objectName = null;
    private String port = null;
    private String address = null;
    private String scheme = null;
    private String connectorType = null;
    private boolean applicationServerPort = false;

// --------------------------------------------------------- Constructors

    public ConnectorItem() {
    }

    public ConnectorItem(ObjectName p_OnConnector, int p_ApplicationServerPort, String serverName) {
        setObjectName(p_OnConnector.toString());
        setPort(p_OnConnector.getKeyProperty("port"));
        applicationServerPort = port.equals(String.valueOf(p_ApplicationServerPort));
        InetAddress address = (InetAddress) JonasManagementRepr.getAttribute(p_OnConnector, "address", serverName);
        if (address != null) {
            setAddress(address.toString());
        } else {
            setAddress(null);
        }
        setScheme((String) JonasManagementRepr.getAttribute(p_OnConnector, "scheme", serverName));

        String sHandlerClassName = (String) JonasManagementRepr.getAttribute(p_OnConnector
            , "protocolHandlerClassName", serverName);
        int period = sHandlerClassName.lastIndexOf('.');
        String sHandlerType = sHandlerClassName.substring(period + 1);
        setConnectorType("HTTPS");
        if ("JkCoyoteHandler".equalsIgnoreCase(sHandlerType)) {
            setConnectorType("AJP");
        } else if (("Http11Protocol".equalsIgnoreCase(sHandlerType))
            && ("http".equalsIgnoreCase(getScheme()))) {
            setConnectorType("HTTP");
        }
    }

// --------------------------------------------------------- Properties Methods

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getAddress() {
        if (address != null && address.startsWith("/")) {
            address = address.substring(1);
        }
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getConnectorType() {
        return connectorType;
    }

    public void setConnectorType(String connectorType) {
        this.connectorType = connectorType;
    }

    public boolean isApplicationServerPort() {
        return applicationServerPort;
    }

    public String getLabel() {
        StringBuffer sb = new StringBuffer(getPort());
        if (getAddress() != null) {
            sb.append(" (");
            sb.append(getAddress());
            sb.append(")");
        }
        return sb.toString();
    }
}