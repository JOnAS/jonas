/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.deploy;

import java.util.ArrayList;
import java.util.Arrays;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 * Set the list of removed modules choosen by the user
 * @author Florent Benoit
 */
public class ApplyRemoveAction extends BaseDeployAction {

    /**
     * Execute the action with given params
     * @param actionMapping The ActionMapping used to select this instance
     * @param actionForm The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     * @return a forward when action is finished
     * @exception ServletException if business logic throws an exception
     */
    public ActionForward executeAction(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request,
            HttpServletResponse response) throws ServletException {

        String sForward = "Remove Confirm";

        // Form used
        RemoveForm removeForm = (RemoveForm) actionForm;
        try {

            removeForm.setListToBeRemoved(Arrays.asList(removeForm.getRemoveSelected()));
            removeForm.setListRemoved(new ArrayList());
            removeForm.setConfirm(removeForm.getRemoveSelected().length > 0);
            if (!removeForm.isConfirm()) {
                m_Errors.add("error.remove.noselect", new ActionMessage("error.remove.noselect"));
                saveErrors(request, m_Errors);
                sForward = "Remove";
            }

        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(request, m_Errors);
            return (actionMapping.findForward("Global Error"));
        }
        // Forward to the jsp.
        return (actionMapping.findForward(sForward));
    }
}
