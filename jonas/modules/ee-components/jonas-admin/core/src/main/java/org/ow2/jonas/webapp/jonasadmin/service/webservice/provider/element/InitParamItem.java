/**
 *==============================================================================
 * Copyright (C) 2001-2005 by Allesta, LLC. All rights reserved.
 * Copyright (C) 2007 Bull S.A.S.
 *==============================================================================
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *==============================================================================
 * $Id$
 *==============================================================================
 */

package org.ow2.jonas.webapp.jonasadmin.service.webservice.provider.element;

import org.ow2.jonas.lib.management.extensions.base.NameItem;

/**
 * The init-param element contains a name/value pair as an initialization param
 * of the servlet Used in: handler
 *
 * @author Allesta, LLC
 */
public class InitParamItem implements NameItem {
    // ~ Instance fields
    // ------------------------------------------------------------------

    private String name = null;

    private String value = null;

    private String description = null;

    // ~ Constructors
    // ---------------------------------------------------------------------

    /**
     * Creates a new InitParam object.
     */
    public InitParamItem() {
    }

    /**
     * Creates a new InitParam object.
     *
     * @param name
     *            DOCUMENT ME!
     * @param value
     *            DOCUMENT ME!
     */
    public InitParamItem(String name, String value) {
        this(name, value, null);
    }

    /**
     * Creates a new InitParam object.
     *
     * @param name
     *            DOCUMENT ME!
     * @param value
     *            DOCUMENT ME!
     * @param description
     *            DOCUMENT ME!
     */
    public InitParamItem(String name, String value, String description) {
        this.name = name;
        this.value = value;
        this.description = description;
    }

    // ~ Methods
    // --------------------------------------------------------------------------

    /**
     * DOCUMENT ME!
     *
     * @param description
     *            DOCUMENT ME!
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getDescription() {
        return description;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getName() {
        return name;
    }

    /**
     * DOCUMENT ME!
     *
     * @param value
     *            DOCUMENT ME!
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getValue() {
        return value;
    }
}
