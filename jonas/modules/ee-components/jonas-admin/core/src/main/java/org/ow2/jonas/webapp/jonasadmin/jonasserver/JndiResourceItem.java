/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.jonasserver;
/*
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
*/

import javax.servlet.http.HttpServletRequest;

import org.ow2.jonas.lib.management.extensions.base.NameItem;

import org.apache.struts.action.ActionMapping;

/**
 * @author Michel-Ange ANTON
 */
public class JndiResourceItem implements NameItem {

// --------------------------------------------------------- Properties variables

    private String name = null;
    private String providerUrl = null;
    private String protocol = null;
    private String resourceON = null;

// --------------------------------------------------------- Public Methods

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        providerUrl = null;
        protocol = null;
        resourceON = null;
        name = null;
        //listNames = new ArrayList();
    }

// --------------------------------------------------------- Properties Methods

    public String getProviderUrl() {
        return providerUrl;
    }

    public void setProviderUrl(String providerUrl) {
        this.providerUrl = providerUrl;
    }
    /**
     * @return Returns the protocol.
     */
    public String getProtocol() {
        return protocol;
    }
    /**
     * @param protocol The protocol to set.
     */
    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }
    /**
     * @return Returns the resourceON.
     */
    public String getResourceON() {
        return resourceON;
    }
    /**
     * @param resourceON The resourceON to set.
     */
    public void setResourceON(String resourceON) {
        this.resourceON = resourceON;
    }
    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }
}