/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.db;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.management.javaee.J2eeObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Michel-Ange ANTON
 * @author Adriana Danes - update to use JSR 77 JDBCResource
 */

public class ListDatabasesAction extends JonasBaseAction {

// --------------------------------------------------------- Public Methods
    /**
     * Action executed to perform HTTP request
     *
     * @param pMapping The ActionMapping used to select this instance
     * @param pForm The optional ActionForm bean for this request (if any)
     * @param pRequest The HTTP request we are processing
     * @param pResponse The HTTP response we are creating
     *
     * @return The forward where redirect
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */
    public ActionForward executeAction(ActionMapping pMapping, ActionForm pForm
            , HttpServletRequest pRequest, HttpServletResponse pResponse)
    throws IOException, ServletException {

        // Force the node selected in tree
        m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_SERVER) + WhereAreYou.NODE_SEPARATOR
                + "services" + WhereAreYou.NODE_SEPARATOR + "database", true);

        // no Form used
        try {
            // Use JDBCResource MBean
            String domainName = m_WhereAreYou.getCurrentDomainName();
            String serverName = m_WhereAreYou.getCurrentJonasServerName();
            ObjectName on = J2eeObjectName.JDBCResource(domainName, serverName);
            String[] jdbcDataSources = (String[]) JonasManagementRepr.getAttribute(on, "jdbcDataSources", serverName);
            ArrayList al = new ArrayList();
            if (jdbcDataSources.length != 0) {
                String jdbcDataSourceON = null;
                ObjectName jdbcDataSourceObjectName = null;
                String sName = null;
                String sJndiName = null;
                int iJdbcConOpen;
                for (int i = 0; i < jdbcDataSources.length; i++) {
                    jdbcDataSourceON = jdbcDataSources[i];
                    jdbcDataSourceObjectName = ObjectName.getInstance(jdbcDataSourceON);
                    sName = (String) getStringAttribute(jdbcDataSourceObjectName, "name");
                    sJndiName = (String) getStringAttribute(jdbcDataSourceObjectName, "jndiName");
                    iJdbcConOpen = getIntegerAttribute(jdbcDataSourceObjectName, "currentOpened");
                    al.add(new DatasourceItem(sName, sJndiName, iJdbcConOpen, true));
                }

                Collections.sort(al, new DatasourceItemByNameComparator());
            }
            // Set list in the request
            pRequest.setAttribute("listDatabases", al);
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(pRequest, m_Errors);
            return (pMapping.findForward("Global Error"));
        }
        // Forward to the jsp.
        return (pMapping.findForward("Databases"));
    }
}
