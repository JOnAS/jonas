/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.ejb;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;

/**
 * @author Vivek Lakshmanan
 * Handles update of session time out information for session beans.
 */
public class ApplyEjbSessionConfigurationAction extends JonasBaseAction {
/**
 * Principal method
 * @param pMapping action mapping
 * @param pForm used form
 * @param pRequest request parameters
 * @param pResponse response parameters
 * @return action forward
 * @throws IOException action failed because a IOException
 * @throws ServletException a ServletException occured
 */
    @Override
    public ActionForward executeAction(final ActionMapping pMapping, final ActionForm pForm
            , final HttpServletRequest pRequest, final HttpServletResponse pResponse)
    throws IOException, ServletException {

        // Form used
        EjbSessionForm oForm = (EjbSessionForm) pForm;
        // Object name used
        String currentObjectName = oForm.getObjectName();
        try {
            ObjectName oObjectName = null;
            if (currentObjectName != null) {
                oObjectName = ObjectName.getInstance(currentObjectName);
                String timeOut = oForm.getSessionTimeOut();
                if (timeOut != null) {
                    setIntegerAttribute(oObjectName, "sessionTimeOut", timeOut);
                }
            }
            if (oForm.getType().equals("StatelessSessionBean")) {
                // Forward to stateless action
                pRequest.setAttribute("select", currentObjectName);
                return (pMapping.findForward("ActionEditEjbSbl"));
            } else {
                // Forward to stateful action
                pRequest.setAttribute("select", currentObjectName);
                return (pMapping.findForward("ActionEditEjbSbf"));
            }
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(pRequest, m_Errors);
            return (pMapping.findForward("Global Error"));
        }


    }

}
