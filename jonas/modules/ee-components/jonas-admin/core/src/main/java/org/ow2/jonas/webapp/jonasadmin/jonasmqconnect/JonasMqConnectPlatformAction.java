package org.ow2.jonas.webapp.jonasadmin.jonasmqconnect;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;
import org.ow2.jonas.webapp.jonasadmin.jonasmqconnect.util.MqObjectNames;
import org.ow2.jonas.webapp.jonasadmin.jonasmqconnect.util.Property;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class JonasMqConnectPlatformAction extends JonasBaseAction {

    public ActionForward executeAction(ActionMapping mapping, ActionForm form
            , HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException {

        JonasMqConnectPlatformForm fBean = (JonasMqConnectPlatformForm) form;
        WhereAreYou oWhere = (WhereAreYou) request.getSession().getAttribute(WhereAreYou.
                SESSION_NAME);
        String domainName = oWhere.getCurrentDomainName();
        String serverName = oWhere.getCurrentJonasServerName();
        ArrayList names = MqObjectNames.getConnectorsONList(domainName, serverName);
        ArrayList list = new ArrayList();
        for (int i = 0; i < names.size(); i++) {
            list.add(new Property("connector", (String) names.get(i),
                    "Jonas MQ Connector"));

        }
        m_Session.setAttribute("mqconnectors", list);

        /*
         * Select the right tree node
         */
        String nodeName = "domain" + WhereAreYou.NODE_SEPARATOR
            + oWhere.getCurrentJonasServerName()
            + WhereAreYou.NODE_SEPARATOR
            + "jonasmqconnect";

        m_WhereAreYou.selectNameNode(nodeName, true);


        return mapping.findForward("JonasMqConnectPlatform");
    }
}