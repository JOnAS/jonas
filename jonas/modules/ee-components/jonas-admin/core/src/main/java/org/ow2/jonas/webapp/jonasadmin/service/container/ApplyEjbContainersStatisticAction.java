/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2008 Bull S.A.S.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.container;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.management.extensions.base.api.J2EEMBeanAttributeInfo;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;

/**
 * Handles update of monitoring configuration for session beans.
 * @author S. Ali Tokmen, Malek Chahine
 */
public class ApplyEjbContainersStatisticAction extends JonasBaseAction {

    // --------------------------------------------------------- Public Methods

    @Override
    public ActionForward executeAction(final ActionMapping pMapping, final ActionForm pForm, final HttpServletRequest pRequest,
            final HttpServletResponse pResponse) throws IOException, ServletException {
        try {
            EjbContainersStatisticForm oForm = (EjbContainersStatisticForm) pForm;
            ObjectName oObjectName = ObjectName.getInstance(m_WhereAreYou.getCurrentDomainName()
                    + ":type=service,name=ejbContainers");
            for (J2EEMBeanAttributeInfo attribute : oForm.getAttributes()) {
                if (attribute.isWritable()) {
                    updateAttribute(oObjectName, attribute, oForm);
                }
            }
            String[] values = {oForm.getApplySettingsTo()};
            String[] signatures = {"java.lang.String"};
            JonasManagementRepr.invoke(oObjectName, "applyMonitorSettings", values, signatures, m_WhereAreYou
                    .getCurrentJonasServerName());

            pRequest.setAttribute("applySettingsTo", oForm.getApplySettingsTo());
            /**
             * Edit informations for all containers in the forward action.
             */
            m_Session.setAttribute("editAllContainers", "true");

        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(pRequest, m_Errors);
            return (pMapping.findForward("Global Error"));
        }
        return (pMapping.findForward("ActionEditEjbContainersStatistic"));
    }

    /**
     * Update value of the given attribute for the given object instance.
     * @param objectName managed object instance
     * @param attribute attribute to update
     * @param form the form-bean.
     **/
    private void updateAttribute(final ObjectName objectName, final J2EEMBeanAttributeInfo attribute,
            final EjbContainersStatisticForm form) {
        String atrName = attribute.getName();
        if ("monitoringEnabled".equalsIgnoreCase(atrName)) {
            attribute.setValue(form.getMonitoringEnabled());
        }

        if ("warningThreshold".equalsIgnoreCase(atrName)) {
            attribute.setValue(form.getWarningThreshold());
        }
        /**
         * Pass the first character to Uppercase.
         */
        if (boolean.class.getName().equals(attribute.getType()) || Boolean.class.getName().equals(attribute.getType())) {
            setBooleanAttribute(objectName, atrName, Boolean.parseBoolean(attribute.getValue().toString()));
        } else if (Integer.class.getName().equals(attribute.getType()) || int.class.getName().equals(attribute.getType())) {
            setIntegerAttribute(objectName, atrName, Integer.parseInt(attribute.getValue().toString()));
        }

    }
}
