/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.util.Log;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * @author Michel-Ange ANTON (initial developer)
 * @author Florent Benoit (changes for struts 1.2.2)
 */
public class SetupWhereAreYouAction extends JonasBaseAction {

// --------------------------------------------------------- Public Methods

    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws IOException, ServletException {
        // nothing to do
        return null;
    }

    public ActionForward execute(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws IOException, ServletException {

        // Initialize the instance variables
        initialize(p_Request);

        try {
            if (m_WhereAreYou == null) {
                m_WhereAreYou = new WhereAreYou();
                m_WhereAreYou.initialize(getServlet(), p_Request);
                m_Session.setAttribute(WhereAreYou.SESSION_NAME, m_WhereAreYou);
            } else {
                m_WhereAreYou.refreshServers(p_Request);
            }
        } catch (JonasAdminException e) {

            String message = m_Resources.getMessage(e.getId());
            //getServlet().log(message);
            Logger logger = Log.getLogger(Log.JONAS_ADMIN_PREFIX);
            if (logger.isLoggable(BasicLevel.DEBUG)) {
                logger.log(BasicLevel.DEBUG, message);
            }
            m_Errors.add(e.getId(), new ActionMessage(e.getId()));
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        return (p_Mapping.findForward("Frame Main"));
    }

}
