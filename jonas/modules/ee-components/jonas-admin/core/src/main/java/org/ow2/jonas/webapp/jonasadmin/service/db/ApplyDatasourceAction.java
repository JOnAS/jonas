/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.db;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.management.javaee.J2eeObjectName;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * @author Michel-Ange ANTON
 * @author Adriana Danes
 */

public class ApplyDatasourceAction extends JonasBaseAction {

// --------------------------------------------------------- Public Methods

    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws IOException, ServletException {

        // Form used
        DatasourceForm oForm = (DatasourceForm) p_Form;
        try {
        	String domainName = m_WhereAreYou.getCurrentDomainName();
        	String serverName = m_WhereAreYou.getCurrentJonasServerName();
            populate(oForm, domainName, serverName);
            if (oForm.getAction().equals("save") == true) {
                save(oForm, domainName, serverName);
            }
        }
        catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        // Forward to the jsp and add the parameter 'name' with the good value.
        return new ActionForward(p_Mapping.findForward("ActionEditDatasource").getPath() + "?name="
            + oForm.getDatasourceName());
    }

// --------------------------------------------------------- Protected Methods

    protected void populate(DatasourceForm p_Form, String domainName, String serverName)
        throws Exception {
        // Object name used
        ObjectName oObjectName = J2eeObjectName.getJDBCDataSource(domainName, serverName, p_Form.getDatasourceName());
        // Populate
        setIntegerAttribute(oObjectName, "jdbcConnMaxAge", p_Form.getJdbcConnMaxAge());
        setIntegerAttribute(oObjectName, "jdbcMaxOpenTime", p_Form.getJdbcMaxOpenTime());
        setIntegerAttribute(oObjectName, "jdbcMaxConnPool", p_Form.getJdbcMaxConnPool());
        setIntegerAttribute(oObjectName, "jdbcMinConnPool", p_Form.getJdbcMinConnPool());
        setIntegerAttribute(oObjectName, "jdbcMaxWaitTime", p_Form.getJdbcMaxWaitTime());
        setIntegerAttribute(oObjectName, "jdbcMaxWaiters", p_Form.getJdbcMaxWaiters());
        setIntegerAttribute(oObjectName, "jdbcSamplingPeriod", p_Form.getJdbcSamplingPeriod());
        setIntegerAttribute(oObjectName, "jdbcAdjustPeriod", p_Form.getJdbcAdjustPeriod());
        setIntegerAttribute(oObjectName, "jdbcPstmtMax", p_Form.getJdbcPstmtMax());
        setIntegerAttribute(oObjectName, "jdbcConnCheckLevel", p_Form.getJdbcConnCheckLevel());
        String testStmt = p_Form.getJdbcTestStatement();
        String[] values = { testStmt };
        String[] signatures = { "java.lang.String" };
        String op = "setJdbcTestStatement";
        String result = (String) JonasManagementRepr.invoke(oObjectName, op, values, signatures, serverName);
        if (!result.equals(testStmt)) {
        	throw new Exception(result);
        }
    }

    protected void save(DatasourceForm p_Form, String domainName, String serverName)
        throws Exception {
        // Save
        JonasManagementRepr.invoke(J2eeObjectName.getJDBCDataSource(domainName, serverName, p_Form.getDatasourceName())
            , "saveConfig", null, null, serverName);
    }

}
