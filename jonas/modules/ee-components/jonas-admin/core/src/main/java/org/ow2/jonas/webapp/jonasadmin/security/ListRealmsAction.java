/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006-2009 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.security;

import java.io.IOException;
import java.util.ArrayList;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.lib.management.extensions.base.BaseManagement;
import org.ow2.jonas.lib.management.extensions.base.RealmItem;
import org.ow2.jonas.lib.management.extensions.base.mbean.CatalinaObjectName;
import org.ow2.jonas.webapp.jonasadmin.ServiceName;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

/**
 * @author Michel-Ange ANTON
 * @author Adriana Danes
 */

public class ListRealmsAction extends BaseMemoryRealmAction {

    // --------------------------------------------------------- Public Methods

    @Override
    public ActionForward executeAction(final ActionMapping p_Mapping, final ActionForm p_Form,
            final HttpServletRequest p_Request, final HttpServletResponse p_Response) throws IOException, ServletException {

        String serviceName = ServiceName.SECURITY.getName();

        if (!isActive(serviceName)) {
            return (p_Mapping.findForward("Realms Stopped"));
        }

        WhereAreYou oWhere = (WhereAreYou) p_Request.getSession().getAttribute(WhereAreYou.SESSION_NAME);
        String serverName = oWhere.getCurrentJonasServerName();
        String domainName = oWhere.getCurrentDomainName();

        // Realms to list
        String sTypeRealm = p_Request.getParameter("realm");
        if (sTypeRealm == null) {
            sTypeRealm = (String) m_Session.getAttribute("typeRealm");
        } else {
            m_Session.setAttribute("typeRealm", sTypeRealm);
        }
        // Force view all realm if null
        if (sTypeRealm == null) {
            sTypeRealm = "all";
            m_Session.setAttribute("typeRealm", sTypeRealm);
        }

        // Populate list
        try {
            String sSecurityRealmUsed = null;
            // Detect realm used by the security service
            if (m_WhereAreYou.isCatalinaServer()) {
                try {
                    // Object name used Realm
                    ObjectName oObjectName = CatalinaObjectName.catalinaRealm(m_WhereAreYou.getCurrentCatalinaDomainName());
                    // Populate
                    sSecurityRealmUsed = getStringAttribute(oObjectName, "resourceName");
                } catch (Exception e) {
                    // no action
                    // Exception because catalina realm don't exists
                }
            }

            ArrayList<RealmItem> alRealms = (ArrayList<RealmItem>) BaseManagement.getInstance().getRealmItems(sTypeRealm,
                    sSecurityRealmUsed, domainName, serverName);
            // Detect the realm type
            if (sTypeRealm.equals("all")) {
                m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_SERVER) + WhereAreYou.NODE_SEPARATOR + "security", true);
            } else if (sTypeRealm.equals("memory")) {
                // Memory realm factories
                // Force the node selected in tree
                m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_SERVER) + WhereAreYou.NODE_SEPARATOR + "security"
                        + WhereAreYou.NODE_SEPARATOR + "factory.memory", true);
            } else if (sTypeRealm.equals("datasource")) {
                // Force the node selected in tree
                m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_SERVER) + WhereAreYou.NODE_SEPARATOR + "security"
                        + WhereAreYou.NODE_SEPARATOR + "factory.datasource", true);
            } else if (sTypeRealm.equals("ldap")) {
                // Ldap realm factories
                // Force the node selected in tree
                m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_SERVER) + WhereAreYou.NODE_SEPARATOR + "security"
                        + WhereAreYou.NODE_SEPARATOR + "factory.ldap", true);
            }
            // Unknown
            else {
                String sMess = m_Resources.getMessage("error.security.factory.realms.type.unknown", sTypeRealm);
                throw new Exception(sMess);
            }
            // Send list to display
            m_Session.setAttribute("listRealms", alRealms);

        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        // Forward to the jsp.
        return (p_Mapping.findForward("Realms"));
    }

}
