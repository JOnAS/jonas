package org.ow2.jonas.webapp.jonasadmin.jonasmqconnect;

import org.apache.struts.action.ActionForm;

public class MdbsForm extends ActionForm {
    /**
     * debug or not
     */
    private boolean debug = true;

    /**
     * printe debug msg
     * @param s msg
     */
    private void debug(String s) {
        if (debug)
            System.out.println(s);
    }
}
