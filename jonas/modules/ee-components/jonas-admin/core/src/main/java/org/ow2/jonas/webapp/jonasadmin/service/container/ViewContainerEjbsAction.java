/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2009 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.container;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * Action used to force the node in tree.
 *
 * @author Michel-Ange ANTON
 * @author Adriana Danes
 */

public class ViewContainerEjbsAction extends JonasBaseAction {

// --------------------------------------------------------- Public Methods

    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws IOException, ServletException {

        // Force the node selected in tree
        ContainerForm oForm = (ContainerForm) m_Session.getAttribute("containerForm");
        
        String fileName = oForm.getFilename();
        String parentName = oForm.getObjectName().getKeyProperty(WhereAreYou.J2EE_APPLICATION_KEY);
        if (parentName != null && !"none".equals(parentName) && !"null".equals(parentName)) {
            fileName = parentName.concat(WhereAreYou.NODE_NAME_SEPARATOR + fileName);
        }
        
        String nodeName = getTreeBranchName(DEPTH_SERVER) + WhereAreYou.NODE_SEPARATOR
            + "services" + WhereAreYou.NODE_SEPARATOR + "ejbContainers"
            + WhereAreYou.NODE_SEPARATOR + fileName;
        m_WhereAreYou.selectNameNode(nodeName, true); 

        // Forward to the jsp.
        return (p_Mapping.findForward("Container Ejbs"));
    }
}
