/**
 *==============================================================================
 * Copyright (C) 2001-2005 by Allesta, LLC. All rights reserved.
 * Copyright (C) 2007 Bull S.A.S.
 *==============================================================================
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *==============================================================================
 * $Id$
 *==============================================================================
 */

package org.ow2.jonas.webapp.jonasadmin.service.webservice.provider.element;

import java.util.Vector;

import javax.xml.namespace.QName;

import org.ow2.jonas.lib.management.extensions.base.NameItem;


/**
 * Declares the handler for a port-component. Handlers can access the init-param
 * name/value pairs using the HandlerInfo interface. Used in: port-component
 *
 * @author Allesta, LLC
 */
public class HandlerItem implements NameItem {
    // ~ Instance fields
    // ------------------------------------------------------------------

    private Vector initParams = new Vector();

    private Vector soapHeaders = new Vector();

    private Vector soapRoles = new Vector();

    private String handlerClass = null;

    private String handlerName = null;

    // ~ Constructors
    // ---------------------------------------------------------------------

    /**
     * Creates a new Handler object.
     */
    public HandlerItem(String handlerName, String handlerClass) {
        this.handlerName = handlerName;
        this.handlerClass = handlerClass;
    }

    // ~ Methods
    // --------------------------------------------------------------------------

    /**
     * DOCUMENT ME!
     *
     * @param handlerClass
     *            DOCUMENT ME!
     */
    public void setHandlerClass(String handlerClass) {
        this.handlerClass = handlerClass;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getHandlerClass() {
        return handlerClass;
    }

    /**
     * DOCUMENT ME!
     *
     * @param handlerName
     *            DOCUMENT ME!
     */
    public void setHandlerName(String handlerName) {
        this.handlerName = handlerName;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getHandlerName() {
        return handlerName;
    }

    /**
     * DOCUMENT ME!
     *
     * @param index
     *            DOCUMENT ME!
     * @param initParam
     *            DOCUMENT ME!
     */
    public void setInitParam(int index, InitParamItem initParam) {
        if ((initParams.size() == 0) || (index >= initParams.size())) {
            initParams.add(initParam);

            return;
        }

        this.initParams.insertElementAt(initParam, index);
    }

    /**
     * DOCUMENT ME!
     *
     * @param index
     *            DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public InitParamItem getInitParam(int index) {
        return (InitParamItem) initParams.get(index);
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public int getInitParamCount() {
        return initParams.size();
    }

    /**
     * DOCUMENT ME!
     *
     * @param index
     *            DOCUMENT ME!
     * @param soapHeader
     *            DOCUMENT ME!
     */
    public void setSoapHeader(int index, QName soapHeader) {
        if ((soapHeaders.size() == 0) || (index >= soapHeaders.size())) {
            soapHeaders.add(soapHeader);

            return;
        }

        this.soapHeaders.insertElementAt(soapHeader, index);
    }

    /**
     * DOCUMENT ME!
     *
     * @param index
     *            DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public QName getSoapHeader(int index) {
        return (QName) soapHeaders.get(index);
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public int getSoapHeaderCount() {
        return soapHeaders.size();
    }

    /**
     * DOCUMENT ME!
     *
     * @param index
     *            DOCUMENT ME!
     * @param soapRole
     *            DOCUMENT ME!
     */
    public void setSoapRole(int index, String soapRole) {
        if ((soapRoles.size() == 0) || (index >= soapRoles.size())) {
            soapRoles.add(soapRole);

            return;
        }

        this.soapRoles.insertElementAt(soapRole, index);
    }

    /**
     * DOCUMENT ME!
     *
     * @param index
     *            DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getSoapRole(int index) {
        return (String) soapRoles.get(index);
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public int getSoapRoleCount() {
        return soapRoles.size();
    }

    /**
     * DOCUMENT ME!
     *
     * @param index
     *            DOCUMENT ME!
     */
    public void removeInitParam(int index) {
        initParams.remove(index);
    }

    /**
     * DOCUMENT ME!
     *
     * @param index
     *            DOCUMENT ME!
     */
    public void removeSoapHeader(int index) {
        soapHeaders.remove(index);
    }

    /**
     * DOCUMENT ME!
     *
     * @param index
     *            DOCUMENT ME!
     */
    public void removeSoapRole(int index) {
        soapRoles.remove(index);
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String toString() {
        StringBuffer sb = new StringBuffer();

        sb.append(super.toString());

        if (soapHeaders.size() > 0) {
            sb.append("Soap Headers:\n");
            sb.append(soapHeaders);
        }

        if (soapRoles.size() > 0) {
            sb.append("Soap Roles:\n");
            sb.append(soapRoles);
        }

        return sb.toString();
    }

    public String getName() {
        return this.handlerName;
    }
}
