/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.logging;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.Jlists;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * @author Michel-Ange ANTON
 */

public class EditLoggingJonasAction extends JonasBaseAction {

// --------------------------------------------------------- Public Methods

    @Override
    public ActionForward executeAction(final ActionMapping p_Mapping, final ActionForm p_Form
        , final HttpServletRequest p_Request, final HttpServletResponse p_Response)
        throws IOException, ServletException {

        // Force the node selected in tree
        m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_SERVER)
                + WhereAreYou.NODE_SEPARATOR
                + "logging"
                + WhereAreYou.NODE_SEPARATOR
                + LoggerItem.LOGGER_JONAS, true);

        // Form used
        LoggingJonasForm oForm = (LoggingJonasForm) p_Form;

        // Current server
        String serverName = m_WhereAreYou.getCurrentJonasServerName();
        String domainName = m_WhereAreYou.getCurrentDomainName();

        try {
            ObjectName oObjectName = null;
            oObjectName = JonasObjectName.logService(domainName);
            String[] asTopic = (String[]) JonasManagementRepr.getAttribute(oObjectName, "topics", serverName);

            ArrayList alTopic = new ArrayList(Arrays.asList(asTopic));
            int iPos = alTopic.indexOf("root");
            if (iPos > -1) {
                alTopic.remove(iPos);
                alTopic.add(0, "root");
                Object[] ao = alTopic.toArray();
                for (int i = 0; i < ao.length; i++) {
                    asTopic[i] = ao[i].toString();
                }
            }

            // Search the level of each topic
            String[] asParam = new String[1];
            String[] asSignature = new String[1];
            asSignature[0] = "java.lang.String";
            String sLevel = null;
            ArrayList al = new ArrayList();
            String[] asLevel = new String[asTopic.length];
            for (int i = 0; i < asTopic.length; i++) {
                asParam[0] = asTopic[i];
                sLevel = (String) JonasManagementRepr.invoke(oObjectName, "getTopicLevel", asParam
                    , asSignature, serverName);
                asLevel[i] = sLevel;
                al.add(new TopicLevel(asTopic[i], sLevel));
            }
            oForm.setTopicLevelList(al);
            oForm.setTopics(asTopic);
            oForm.setLoggerJonasLevels(Jlists.getLoggerJonasLevels());
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        // Forward to the jsp.
        return (p_Mapping.findForward("Logging Jonas"));
    }
}
