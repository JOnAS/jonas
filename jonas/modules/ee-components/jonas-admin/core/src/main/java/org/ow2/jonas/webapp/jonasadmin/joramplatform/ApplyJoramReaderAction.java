/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.joramplatform;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * @author Adriana Danes
 * Add user as reader to a destination referenced (by OBJECT_NAME) in the session
 */

public class ApplyJoramReaderAction extends EditJoramBaseAction {

// --------------------------------------------------------- Public Methods

    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws IOException, ServletException {

        // Current server
        String serverName = m_WhereAreYou.getCurrentJonasServerName();
        // Form used
        JoramReaderWriterForm oForm = (JoramReaderWriterForm) p_Form;
        String userId = oForm.getId();
        String destON = (String) m_Session.getAttribute("currentDestination");
        try {
            ObjectName destOn = ObjectName.getInstance(destON);
            if (destOn == null) {
                throw new Exception("No destination ON to add reader");
            }

            String[] asParams = { userId };
            String[] asSignature = { "java.lang.String" };
            JonasManagementRepr.invoke(destOn, "addReader", asParams, asSignature, serverName);
            oForm.reset(p_Mapping, p_Request);
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }

        // Forward to the jsp.
        return (p_Mapping.findForward("Add Joram Reader"));
    }
}
