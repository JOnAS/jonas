/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.cmi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;


import org.apache.struts.action.ActionForm;

public class CmiForm extends ActionForm {

    /**
     * The serial Id.
     */
    private static final long serialVersionUID = 1L;
    /**
     * List of cmi clusters.
     */
    private List<ClusterAttribute> cmiClusterList = new ArrayList<ClusterAttribute>();
    /**
     * Protocol name and value(url).
     */
    private Set<Protocol> protocols = new HashSet<Protocol>();
    /**
     * How often ...
     */
    private int delayToRefresh = 0;
    /**
     * Action to perform.
     */
    private String action = null;

    /**
     * available LBpolicies.
     */
    private HashMap availableLBPolicies = new HashMap<String, String>();

    /**
     * available LBpolicies.
     */
    private HashMap availableLBStrategies = new HashMap<String, String>();

    /**
     * The server to blacklist/unblacklist
     */
    private String serverUrl;
    /**
     * @return the cmiClusterList
     */
    public List<ClusterAttribute> getCmiClusterList() {
        return cmiClusterList;
    }
    /**
     * @param cmiClusterList the cmiClusterList to set
     */
    public void setCmiClusterList(final List<ClusterAttribute> cmiClusterList) {
        this.cmiClusterList = cmiClusterList;
    }
    /**
     * @return the protocols
     */
    public Set<Protocol> getProtocols() {
        return protocols;
    }
    /**
     * @param protocols the protocols to set
     */
    public void setProtocols(final Set<Protocol> protocols) {
        this.protocols = protocols;
    }
    /**
     * @return the delayToRefresh
     */
    public int getDelayToRefresh() {
        return delayToRefresh;
    }
    /**
     * @param delayToRefresh the delayToRefresh to set
     */
    public void setDelayToRefresh(final int delayToRefresh) {
        this.delayToRefresh = delayToRefresh;
    }
    /**
     * @return the action
     */
    public String getAction() {
        return action;
    }
    /**
     * @param action the action to set
     */
    public void setAction(final String action) {
        this.action = action;
    }


    /**
     * Get a cluster.
     * @param clusterName The cluster name.
     * @return The cluster.
     * @throws Exception If cluster not found.
     */
    public ClusterAttribute getCluster(final String clusterName) throws Exception{
        // TODO Auto-generated method stub
        ListIterator<ClusterAttribute> it = cmiClusterList.listIterator();
        while(it.hasNext()){
            ClusterAttribute next = it.next();
            if (next.getName().equals(clusterName)) {
                return next;
            }
        }
        throw new Exception("Cluster named "+clusterName+" doesn't exist");
    }

    /**
     * @return
     */
    public HashMap getAvailableLBPolicies() {
        return availableLBPolicies;
    }
    /**
     * @param availableLBPolicies
     */
    public void setAvailableLBPolicies(final HashMap availableLBPolicies) {
        this.availableLBPolicies = availableLBPolicies;
    }
    /**
     * @return
     */
    public HashMap getAvailableLBStrategies() {
        return availableLBStrategies;
    }
    /**
     * @param availableLBStrageties
     */
    public void setAvailableLBStrategies(final HashMap availableLBStrategies) {
        this.availableLBStrategies = availableLBStrategies;
    }
    public String getServerUrl() {
        return serverUrl;
    }
    public void setServerUrl(final String serverUrl) {
        this.serverUrl = serverUrl;
    }
}
