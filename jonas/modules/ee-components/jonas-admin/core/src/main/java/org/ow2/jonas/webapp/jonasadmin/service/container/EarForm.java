/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.container;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import org.ow2.jonas.webapp.jonasadmin.common.ModuleItemByName;

/**
 * @author Michel-Ange ANTON
 * @author Adriana Danes
 */
public class EarForm extends ActionForm {

// --------------------------------------------------------- Properties variables

    /**
     * Name of the file containing the ear
     */
    private String filename = null;
    /**
     * Complete path of the file containing the ear
     */
    private String earPath = null;
    /**
     * The ear's deployement descriptor
     */
    private String xmlDeploymentDescriptor = null;
//    /**
//     * List of ContainerItems correspondig to the rars contained in the ear.
//     */
//    private ArrayList listRars = new ArrayList();
    /**
     * List of ModuleItems corresponding to the ejb-jars (EJBModules) contained in the ear.
     */
    private ArrayList ejbjars = new ArrayList();
    /**
     * List of ModuleItems corresponding to the wars (WebModules)  contained in the ear.
     */
    private ArrayList wars = new ArrayList();
    /**
     * List of ModuleItems corresponding to the rars contained in the ear.
     */
    private ArrayList rars = new ArrayList();

// --------------------------------------------------------- Public Methods

    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        filename = null;
        earPath = null;
        xmlDeploymentDescriptor = null;
//        listRars = new ArrayList();
        ejbjars = new ArrayList();
        wars = new ArrayList();
        rars = new ArrayList();
    }

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     * @return the ActionErrors
     */
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors oErrors = new ActionErrors();
        return oErrors;
    }

// --------------------------------------------------------- Properties Methods
    /**
     * @return the name of the file containing the ear
     */
    public String getFilename() {
        return filename;
    }
    /**
     * @param filename the name of the file containing the ear
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }

    /**
     * @return the complete path of the file containing the ear
     */
    public String getEarPath() {
        return earPath;
    }

    /**
     * @param pUrl the ear's URL
     */
    public void setEarPath(URL pUrl) {
        this.earPath = null;
        if (pUrl != null) {
            this.earPath = pUrl.getPath();
        }
    }

//    /**
//     * @return the list of ContainerItems correspondig to the rars contained in the ear.
//     */
//    public ArrayList getListRars() {
//        return listRars;
//    }
//    /**
//     * Set list of rars given an array of <code>URL</code>s corresponding to each rar.
//     * Each item of list is a <code>ContainerItem</code> containing 2 attributes:
//     * - file
//     * - path
//     *
//     * @param pUrl Array of Rar
//     */
//    public void setListRars(URL[] pUrl) {
//        listRars.clear();
//        if (pUrl != null) {
//            for (int i = 0; i < pUrl.length; i++) {
//                listRars.add(new ContainerItem(JonasAdminJmx.extractFilename(pUrl[i].getPath())
//                    , pUrl[i].getPath()));
//            }
//            // Sort list
//            Collections.sort(listRars, new ContainerItemByFile());
//        }
//    }
    /**
     * @return the ear's deployement descriptor
     */
    public String getXmlDeploymentDescriptor() {
        return xmlDeploymentDescriptor;
    }

    /**
     * @param xmlDeploymentDescriptor The ear's deployement descriptor
     */
    public void setXmlDeploymentDescriptor(String xmlDeploymentDescriptor) {
        this.xmlDeploymentDescriptor = xmlDeploymentDescriptor;
    }
    /**
     * @return The list of ModuleItems corresponding to the ejb-jars (EJBModules) contained in the ear.
     */
    public ArrayList getEjbjars() {
        return ejbjars;
    }
    /**
     * @param ejbjars The list of ModuleItems corresponding to the ejb-jars (EJBModules) contained in the ear.
     */
    public void setEjbjars(ArrayList ejbjars) {
        this.ejbjars = ejbjars;
    }
    /**
     * @return The list of ModuleItems corresponding to the wars (WebModules)  contained in the ear.
     */
    public ArrayList getWars() {
        return wars;
    }
    /**
     * @param wars The list of ModuleItems corresponding to the wars (WebModules)  contained in the ear.
     */
    public void setWars(ArrayList wars) {
        this.wars = wars;
        Collections.sort(wars, new ModuleItemByName());
    }

    /**
     * @return Returns the rars.
     */
    public ArrayList getRars() {
        return rars;
    }


    /**
     * @param rars The rars to set.
     */
    public void setRars(ArrayList rars) {
        this.rars = rars;
    }

}
