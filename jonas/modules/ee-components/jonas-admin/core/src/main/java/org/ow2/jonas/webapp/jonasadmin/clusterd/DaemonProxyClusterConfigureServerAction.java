/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
/**
 * this servlet will :
 * - get the server to configure
 * - get its current conf infos
 * - forward the request to a jsp
 */
package org.ow2.jonas.webapp.jonasadmin.clusterd;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.lib.management.domain.DomainMonitor;
import org.ow2.jonas.lib.management.domain.proxy.server.ServerProxy;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;

public class DaemonProxyClusterConfigureServerAction extends JonasBaseAction {
    /**
     * Default extra parameters.
     */
    public static final String DEF_XPREM = "-Djava.net.preferIPv4Stack=true";

    /**
     * Default Server description.
     */
    public static final String DEF_DESC = "jonas server";

    /**
     * Default atoboot value.
     */
    public static final String DEF_AUTO_BOOT = "false";

    @Override
    public ActionForward executeAction(final ActionMapping p_Mapping, final ActionForm p_Form,
            final HttpServletRequest p_Request, final HttpServletResponse p_Response) throws IOException, ServletException {
        String cdName = m_WhereAreYou.getCurrentClusterDaemonName();
        // getting the server name
        String sName = p_Request.getParameter("server");
        ClusterdServerForm oform = (ClusterdServerForm) p_Form;

        oform.setJavaHome(System.getProperty("java.home"));
        oform.setJonasRoot(System.getProperty("jonas.root"));
        oform.setJonasBase(System.getProperty("jonas.base"));
        oform.setDescription(DEF_DESC);
        oform.setAutoBoot(DEF_AUTO_BOOT);
        oform.setXprem(DEF_XPREM);
        oform.setName(sName);
        oform.setCdName(cdName);
        // getting current server infos
        try {
            DomainMonitor dm = DomainMonitor.getInstance();
            ServerProxy proxy = dm.findServerProxy(sName);
            if (proxy != null) {
                // that means the server is in the domain.xml but a server
                // should be affected to the domain without being in the
                // domain.xml file on start.
                // think about adding the server in domain manager when adding
                // it into a domain by jonasAdmin console
                String state = proxy.getState();

                // the server is not configured for clusterd affectation
                if (!state.equals("UNREACHABLE") && !state.equals("UNKNOWN") && !state.equals("FAILED")) {
                    // Get required infos.
                    oform.setJavaHome(proxy.getJavaHome());
                    oform.setJonasRoot(proxy.getJonasRoot());
                    oform.setJonasBase(proxy.getJonasBase());
                    oform.setAutoBoot(proxy.getAutoBoot());
                    oform.setXprem(proxy.getXprem());
                }
                oform.setState(state);
                // Configuration mode
                p_Request.setAttribute("isNotMonitoring", "true");
                // set this server as current server to configure
                m_WhereAreYou.setCurrentSrvToConfigure(sName);
            }
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        // next operation will set selected server's properties that are needed
        // to affect it to the current cluster daemon.
        return (p_Mapping.findForward("DaemonProxyClusterDisplayServerConfig"));
    }

}
