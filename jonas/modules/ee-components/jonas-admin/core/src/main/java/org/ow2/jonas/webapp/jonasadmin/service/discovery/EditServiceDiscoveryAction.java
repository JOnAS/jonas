/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.discovery;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * @author Adriana Danes
 */

public class EditServiceDiscoveryAction extends JonasBaseAction {

    // --------------------------------------------------------- Public Methods
    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
            , HttpServletRequest p_Request, HttpServletResponse p_Response)
    throws IOException, ServletException {

        // Force the node selected in tree
        m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_SERVER) + WhereAreYou.NODE_SEPARATOR
                + "services" + WhereAreYou.NODE_SEPARATOR + "discovery", true);
        String domainName = m_WhereAreYou.getCurrentDomainName();
        // Form used
        DiscoveryServiceForm oForm = (DiscoveryServiceForm) p_Form;

        ObjectName oObjectName = JonasObjectName.discoveryService(domainName);
        oForm.setMulticastAddress(getStringAttribute(oObjectName, "MulticastAddress"));
        oForm.setMulticastPort(getStringAttribute(oObjectName, "MulticastPort"));
        oForm.setMaster(getBooleanAttribute(oObjectName, "IsDiscoveryMaster"));
        oForm.setMulticastTtl(getStringAttribute(oObjectName, "DiscoveryTtl"));
        // Forward to the jsp.
        return (p_Mapping.findForward("Service Discovery"));

    }
}