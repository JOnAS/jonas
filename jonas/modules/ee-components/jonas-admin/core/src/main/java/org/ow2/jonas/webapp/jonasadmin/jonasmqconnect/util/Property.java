package org.ow2.jonas.webapp.jonasadmin.jonasmqconnect.util;


public class Property implements java.io.Serializable {
    private String name;
    private String value;
    private String text;

    public Property(String name, String value, String text) {
        this.name = name;
        this.value = value;
        this.text = text;
    }
    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }
    
    /**
     * @param name The name to set.
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * @return Returns the value.
     */
    public String getValue() {
        return value;
    }
    
    /**
     * @param value The value to set.
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @return Returns the text.
     */
    public String getText() {
        return text;
    }
    
    /**
     * @param value The text to set.
     */
    public void setText(String text) {
        this.text = text;
    }
}