/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.mail;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * Form used to present the Mail Service
 * @author Adriana Danes
 */
public final class MailServiceForm extends ActionForm {

// ------------------------------------------------------------- Properties Variables
    private int nbMimePartDatasourceFactories = 0;
    private int nbSessionFactories = 0;
    private int nbFactories = 0;
// ------------------------------------------------------------- Properties Methods

    public int getNbMimePartDatasourceFactories() {
        return nbMimePartDatasourceFactories;
    }
    public void setNbMimePartDatasourceFactories(int nbMimePartDatasourceFactories) {
        this.nbMimePartDatasourceFactories = nbMimePartDatasourceFactories;
    }

    public int getNbSessionFactories() {
        return nbSessionFactories;
    }
    public void setNbSessionFactories(int nbSessionFactories) {
        this.nbSessionFactories = nbSessionFactories;
    }

    public int getNbFactories() {
        return nbFactories;
    }
    public void setNbFactories(int nbFactories) {
        this.nbFactories = nbFactories;
    }

    // ------------------------------------------------------------- Public Methods

    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        nbMimePartDatasourceFactories = 0;
        nbSessionFactories = 0;
        nbFactories = 0;
    }

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        return new ActionErrors();
        // TO DO voir DatasourceForm
    }
}
