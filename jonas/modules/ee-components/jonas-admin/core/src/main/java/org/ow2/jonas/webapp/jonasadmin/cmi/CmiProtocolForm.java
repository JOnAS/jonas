/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.cmi;

import org.apache.struts.action.ActionForm;

public class CmiProtocolForm extends ActionForm{

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /**
     * Action to be performed.
     */
    String action;
    /**
     * The protocol.
     */
    Protocol protocol = new Protocol();
    /**
     * Gets current action.
     * @return current action.
     */
    public String getAction() {
        return action;
    }
    /**
     * sets an action value.
     * @param action action to set
     */
    public void setAction(final String action) {
        this.action = action;
    }
    /**
     * Gets the protocol.
     * @return Teh protocol.
     */
    public Protocol getProtocol() {
        return protocol;
    }

}
