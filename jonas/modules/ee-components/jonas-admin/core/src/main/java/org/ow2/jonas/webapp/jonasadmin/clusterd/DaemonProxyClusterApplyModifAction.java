/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.webapp.jonasadmin.clusterd;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.monitoring.DaemonProxyClusterForm;

public class DaemonProxyClusterApplyModifAction extends JonasBaseAction {
//	--------------------------------------------------------- Public Methods
    @Override
    public ActionForward executeAction(final ActionMapping p_Mapping, final ActionForm p_Form
            , final HttpServletRequest p_Request, final HttpServletResponse p_Response)
    throws IOException, ServletException {

        // Form used
        DaemonProxyClusterForm oForm = (DaemonProxyClusterForm) p_Form;

        // Actions
        try {
            // ObjectName of the domain in which the new cluster has to be created
            ObjectName cdOn = m_WhereAreYou.getCurrentClusterDaemon();
            String serverName = m_WhereAreYou.getCurrentJonasServerName();
            String[] signature = {"java.lang.String" };
            String[] params = {oForm.getName() };
            signature[0] = "java.lang.String";
            JonasManagementRepr.invoke(cdOn, "save", params, signature, serverName);


        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        p_Request.setAttribute("isNotMonitoring", "true");
        p_Request.setAttribute("node",oForm.getName() );
        // Forward to the connector display page or the list if create
        return p_Mapping.findForward("ActionDaemonProxyClusterInfo");
    }
}

