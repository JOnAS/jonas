/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2003-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.catalina;

import java.util.Comparator;

/**
 * @author Michel-Ange ANTON
 */
public class ConnectorItemByPort implements Comparator {

// --------------------------------------------------------- Public Methods

    public int compare(Object p_O1, Object p_O2) {
        ConnectorItem oConnector1 = (ConnectorItem) p_O1;
        ConnectorItem oConnector2 = (ConnectorItem) p_O2;
        int iRet = 0;
        try {
            int i1 = Integer.parseInt(oConnector1.getPort());
            int i2 = Integer.parseInt(oConnector2.getPort());
            if (i1 > i2) {
                iRet = 1;
            }
            else if (i1 < i2) {
                iRet = -1;
            }
            else if ((oConnector1.getAddress() != null) && (oConnector2.getAddress() != null)) {
                iRet = oConnector1.getAddress().compareToIgnoreCase(oConnector2.getAddress());
            }
            else if (oConnector1.getAddress() == null) {
                iRet = -1;
            }
            else {
                iRet = 1;
            }
        }
        catch (NumberFormatException e) {
            // None
        }

        return iRet;
    }

    public boolean equals(Object p_Obj) {
        if (p_Obj instanceof ConnectorItem) {
            return true;
        }
        return false;
    }
}