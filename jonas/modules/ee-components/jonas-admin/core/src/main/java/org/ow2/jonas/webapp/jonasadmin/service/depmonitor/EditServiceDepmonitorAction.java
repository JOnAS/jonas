package org.ow2.jonas.webapp.jonasadmin.service.depmonitor;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.Jlists;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

public class EditServiceDepmonitorAction extends JonasBaseAction {

    @Override
    public ActionForward executeAction(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request,
            final HttpServletResponse response) throws IOException, ServletException {
        // Force the node selected in tree
        m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_SERVER) + WhereAreYou.NODE_SEPARATOR
            + "services" + WhereAreYou.NODE_SEPARATOR + "depmonitor", true);
        // Form used
        DepMonitorServiceForm oForm = new DepMonitorServiceForm();
        m_Session.setAttribute("depmonitorServiceForm", oForm);
        oForm.setBooleanVals(Jlists.getBooleanValues());
        try {
            // Object name used
            String domainName = m_WhereAreYou.getCurrentDomainName();
            ObjectName on = JonasObjectName.deployableMonitorService(domainName);
            oForm.setDevelopmentMode(getBooleanAttribute(on, "development"));
            String[] dirs = getStringArrayAttribute(on, "directoryNames");
            StringBuffer buf = new StringBuffer();
            boolean inIteration = false;
            for (String dir : dirs) {
                if (inIteration) {
                    buf.append(", ");
                }
                buf.append(dir);
                inIteration = true;
            }
            oForm.setDirectories(buf.toString());
            int monitorInterval = getIntAttribute(on, "monitorPeriod");
            oForm.setMonitorInterval(monitorInterval);
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(request, m_Errors);
            return (mapping.findForward("Global Error"));
        }

        // Forward to the jsp.
        return (mapping.findForward("Depmonitor Service"));
    }

}
