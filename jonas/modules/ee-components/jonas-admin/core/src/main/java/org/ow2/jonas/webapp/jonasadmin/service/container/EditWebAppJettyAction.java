/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.container;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Michel-Ange ANTON
 */

public class EditWebAppJettyAction extends BaseWebAppAction {

// --------------------------------------------------------- Public Methods

    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws IOException, ServletException {

        // Current server
        WhereAreYou oWhere = (WhereAreYou) p_Request.getSession().getAttribute(WhereAreYou.
                SESSION_NAME);
        String serverName = oWhere.getCurrentJonasServerName();
        String domainName = oWhere.getCurrentDomainName();

        // Initialize form
        boolean bPopulate = initialize(p_Mapping, p_Request);

        try {
            // Force the node selected in tree
            if (mWebAppForm != null && mWebAppForm.getObjectName() != null) {
                m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_SERVER)
                        + WhereAreYou.NODE_SEPARATOR + "services" + WhereAreYou.NODE_SEPARATOR + "web"
                        + WhereAreYou.NODE_SEPARATOR + mWebAppForm.getObjectName(), true);
            }

            // Populate
            if (bPopulate) {
                // Populate WebApp
                populateWebAppJetty(mWebAppForm.getObjectName(), (WebAppJettyForm) mWebAppForm);
                // Detect War, still use the JOnAS War MBean as we could not transfer
                // management information we need to the Jetty MBean
                String pathContext = mWebAppForm.getPathContext();
                ObjectName on = findJonasMbeanWar(pathContext, domainName, serverName);
                if (on != null) {
                    // Populate War
                    mWarForm = createWarForm(p_Mapping, p_Request);
                    populateJettyWar(on, mWarForm, serverName);
                    // here we populated the WarForm very incompletlly,
                    // for example hostName is null
                }
            }
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        // Forward to the jsp.
        return (p_Mapping.findForward("WebApp Jetty"));
    }

// --------------------------------------------------------- Protected Methods

}
