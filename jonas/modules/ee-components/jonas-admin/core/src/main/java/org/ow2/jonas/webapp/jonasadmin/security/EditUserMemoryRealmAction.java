/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Michel-Ange ANTON
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 */

public class EditUserMemoryRealmAction extends BaseMemoryRealmAction {

// --------------------------------------------------------- Public Methods

    /**
     */
    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws IOException, ServletException {

        // Current server
        String serverName = m_WhereAreYou.getCurrentJonasServerName();
        String domainName = m_WhereAreYou.getCurrentDomainName();

        // Realm Form used
        MemoryRealmForm oRealmForm = getForm(p_Mapping, p_Request);

        // User to edit
        String sUser = p_Request.getParameter("user");
        // Verify if it is new in session
        UserMemoryRealmForm oForm = new UserMemoryRealmForm();
        m_Session.setAttribute("userMemoryRealmForm", oForm);
        oForm.reset(p_Mapping, p_Request);
        // Type action
        oForm.setAction("apply");

        // Populate
        try {
            // Protect to a direct access
            if (sUser == null) {
                throw new Exception(m_Resources.getMessage(
                    "error.security.factory.memory.realm.users.user.required"));
            }
            populateUserForm(oRealmForm, oForm, sUser, domainName, serverName);
        }
        catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        // Forward to the jsp.
        return (p_Mapping.findForward("User Memory Realm"));
    }

// --------------------------------------------------------- Protected Methods

}
