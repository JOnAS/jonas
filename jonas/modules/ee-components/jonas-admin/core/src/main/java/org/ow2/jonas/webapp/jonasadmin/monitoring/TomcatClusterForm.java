/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.monitoring;

public class TomcatClusterForm extends BaseClusterForm {

    /**
     * Multicast Port
     */
    private int mcastPort;
    public int getMcastPort() {
        return mcastPort;
    }
    public void setMcastPort(int port) {
        mcastPort = port;
    }

    /**
     * Multicast Addr
     */
    private String mcastAddr = null;
    public String getMcastAddr() {
        return mcastAddr;
    }
    public void setMcastAddr(String addr) {
        mcastAddr = addr;
    }

    /**
     * Multicast Drop Time
     */
    private long mcastDropTime;
    public long getMcastDropTime() {
        return mcastDropTime;
    }
    public void setMcastDropTime(long dropTime) {
        mcastDropTime = dropTime;
    }

    /**
     * Multicast Frequency
     */
    private long mcastFrequency;
    public long getMcastFrequency() {
        return mcastFrequency;
    }
    public void setMcastFrequency(long frequency) {
        mcastFrequency = frequency;
    }

    /**
     * Multicast Socket Timeout
     */
    private int mcastSocketTimeout;
    public int getMcastSocketTimeout() {
        return mcastSocketTimeout;
    }
    public void setMcastSocketTimeout(int socketTimeout) {
        mcastSocketTimeout = socketTimeout;
    }
}
