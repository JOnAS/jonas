/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.common;

import java.util.Comparator;

import org.ow2.jonas.webapp.jonasadmin.common.ModuleItem;
/**
 * @author Adriana.Danes@objectweb.org
 *
 * ModuleItem objects are used to show management information concerning
 * modules in a J2EEApplication.
 */
public class ModuleItemByName implements Comparator {
	// --------------------------------------------------------- Public Methods

    public int compare(Object pO1, Object pO2) {
        ModuleItem oContainer1 = (ModuleItem) pO1;
        ModuleItem oContainer2 = (ModuleItem) pO2;
        if ((oContainer1.getName() == null) || (oContainer2.getName() == null)) {
            if ((oContainer1.getName() == null) && (oContainer2.getName() == null)) {
                return 0;
            } else if ((oContainer1.getName() == null) && (oContainer2.getName() != null)) {
                return 1;
            } else {
                return -1;
            }
        }
        return oContainer1.getName().compareToIgnoreCase(oContainer2.getName());
    }

    public boolean equals(Object pObj) {
        if (pObj instanceof ModuleItem) {
            return true;
        }
        return false;
    }
}