/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2009 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service;

import java.io.IOException;
import java.util.ArrayList;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.lib.management.extensions.base.BaseManagement;
import org.ow2.jonas.lib.management.extensions.base.RealmItem;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.ServiceName;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

/**
 * @author Michel-Ange ANTON
 */

public class EditServiceSecurityAction extends JonasBaseAction {

    // --------------------------------------------------------- Public Methods

    @Override
    public ActionForward executeAction(final ActionMapping p_Mapping, final ActionForm p_Form,
            final HttpServletRequest p_Request, final HttpServletResponse p_Response) throws IOException, ServletException {

        // Force the node selected in tree
        m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_SERVER) + WhereAreYou.NODE_SEPARATOR + "services"
                + WhereAreYou.NODE_SEPARATOR + "security", true);

        String serviceName = ServiceName.SECURITY.getName();

        if (!isActive(serviceName)) {
            return (p_Mapping.findForward("Service Security Stopped"));
        }

        // Current server
        WhereAreYou oWhere = (WhereAreYou) p_Request.getSession().getAttribute(WhereAreYou.SESSION_NAME);
        String serverName = oWhere.getCurrentJonasServerName();
        String domainName = oWhere.getCurrentDomainName();

        try {
            ArrayList<RealmItem> al = new ArrayList<RealmItem>();
            String sUsedRealmName = null;
            // Catalina
            if (m_WhereAreYou.isCatalinaServer() == true) {
                try {
                    ObjectName on;
                    // Find used realm name
                    on =  BaseManagement.getInstance().getTomcatRealm(domainName, serverName);
                    sUsedRealmName = getStringAttribute(on, "resourceName");
                } catch (Exception e) {
                    // no action
                    // Exception because catalina realm don't exists
                }
            }
            al = (ArrayList<RealmItem>) BaseManagement.getInstance().getTomcatRealmItems(sUsedRealmName, domainName, serverName);
            // Set form in the request
            p_Request.setAttribute("listSecurityRealms", al);
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        // Forward to the jsp.
        return (p_Mapping.findForward("Service Security"));
    }
}
