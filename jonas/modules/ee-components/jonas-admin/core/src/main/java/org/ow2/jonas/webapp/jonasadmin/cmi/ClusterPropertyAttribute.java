/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.cmi;

import java.io.Serializable;

public class ClusterPropertyAttribute implements Serializable{
    /**
     * Serial UID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Attribute name.
     */
    private String name;

    /**
     * Attribute value.
     */
    private Object value;

    /**
     * Constructor using fields.
     * @param name attribute name.
     * @param value attribute value.
     */
    public ClusterPropertyAttribute(final String name, final Object value) {
        this.name =name;
        this.value = value;
    }
    /**
     * @return the value
     */
    public Object getValue() {
        return value;
    }
    /**
     * @param value the value to set
     */
    public void setValue(final Object value) {
        this.value = value;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * Sets the name.
     * @param name name to set.
     */
    void setName(final String name) {
        this.name = name;
    }

}
