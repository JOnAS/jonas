/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
/**
 * Needed to have an attribute class especially for memory values.
 * For the conversion unit should change, it's necessary to set the correct one
 * and its corresponding value.
 *
 * The unit field will be null for attribute that don't have unit.
 */
package org.ow2.jonas.webapp.jonasadmin.monitoring;

import java.io.Serializable;

/**
 * @author eyindanga
 *
 */
public class ClusterdAttribute implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    String unit = null;
    String name = null;
    Long value;
    public ClusterdAttribute() {
        //do nothing
    }

    public ClusterdAttribute(final String name, final Long value, final String unit) {
        this.unit = unit;
        this.name = name;
        this.value = value;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @param name the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }
    /**
     * @return the unit
     */
    public String getUnit() {
        return unit;
    }
    /**
     * @param unit the unit to set
     */
    public void setUnit(final String unit) {
        this.unit = unit;
    }
    /**
     * @return the value
     */
    public Long getValue() {
        return value;
    }
    /**
     * @param value the value to set
     */
    public void setValue(final Long value) {
        this.value = value;
    }
}
