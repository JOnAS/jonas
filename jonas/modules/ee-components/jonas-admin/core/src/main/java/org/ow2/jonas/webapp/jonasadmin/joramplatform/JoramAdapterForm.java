/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 */

package org.ow2.jonas.webapp.jonasadmin.joramplatform;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * Form bean for the Joram Adapter form page.
 * @author Adriana Danes
 */

public final class JoramAdapterForm extends ActionForm {

// ------------------------------------------------------------- Properties Variables
    /**
     * True: when deploying, the adapter starts a collocated JORAM server.
     * False: when deploying, the adapter connects to a remote JORAM server.
     */
    private boolean collocatedServer;
    // Following properties usefull when starting a collocated server
    /**
     * Directory where the a3servers.xml and joramAdmin.xml files are located
     */
    private String configDir = null;
    /**
     * Name of the file describing the administration tasks to perform
     * (by default joramAdmin.xml)
     */
    private String adminFile = null;
    /**
     * Persistence mode of the collocated JORAM server
     */
    private boolean persistentServer;
    /**
     * Identifier of the JORAM server to start
     */
    private String serverIdTxt = null;
    /**
     * Name of the JORAM server to start
     */
    private String serverName = null;
    // The following properties usefull when manageing a remote server
    private String version = null;
    /**
     * Name of the host where the JORAM server runs
     */
    private String hostName = null;
    /**
     * Port the JORAM server is listening on
     */
    private String serverPortTxt = null;
    /**
     *
     */
    private String cnxPendingTimerTxt = null;
    /**
     *
     */
    private String connectingTimerTxt = null;
    /**
     *
     */
    private String txPendingTimerTxt = null;

// ------------------------------------------------------------- Properties Methods

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * @return Returns the collocatedServer.
     */
    public boolean isCollocatedServer() {
        return collocatedServer;
    }

    /**
     * @param collocatedServer The collocatedServer to set.
     */
    public void setCollocatedServer(boolean collocatedServer) {
        this.collocatedServer = collocatedServer;
    }

    /**
     * @return Returns the configDir.
     */
    public String getConfigDir() {
        return configDir;
    }

    /**
     * @param configDir The configDir to set.
     */
    public void setConfigDir(String configDir) {
        this.configDir = configDir;
    }

    /**
     * @return Returns the adminFile.
     */
    public String getAdminFile() {
        return adminFile;
    }

    /**
     * @param adminFile The adminFile to set.
     */
    public void setAdminFile(String adminFile) {
        this.adminFile = adminFile;
    }

    /**
     * @return Returns the persistentServer.
     */
    public boolean isPersistentServer() {
        return persistentServer;
    }

    /**
     * @param persistentServer The persistentServer to set.
     */
    public void setPersistentServer(boolean persistentServer) {
        this.persistentServer = persistentServer;
    }

    /**
     * @return Returns the serverIdTxt.
     */
    public String getServerIdTxt() {
        return serverIdTxt;
    }

    /**
     * @param serverIdTxt The serverIdTxt to set.
     */
    public void setServerIdTxt(String serverIdTxt) {
        this.serverIdTxt = serverIdTxt;
    }

    /**
     * @return Returns the serverName.
     */
    public String getServerName() {
        return serverName;
    }

    /**
     * @param serverName The serverName to set.
     */
    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    /**
     * @return Returns the hostName.
     */
    public String getHostName() {
        return hostName;
    }

    /**
     * @param hostName The hostName to set.
     */
    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    /**
     * @return Returns the serverPortTxt.
     */
    public String getServerPortTxt() {
        return serverPortTxt;
    }

    /**
     * @param serverPortTxt The serverPortTxt to set.
     */
    public void setServerPortTxt(String serverPortTxt) {
        this.serverPortTxt = serverPortTxt;
    }

    /**
     * @return Returns the cnxPendingTimerTxt.
     */
    public String getCnxPendingTimerTxt() {
        return cnxPendingTimerTxt;
    }

    /**
     * @param cnxPendingTimerTxt The cnxPendingTimerTxt to set.
     */
    public void setCnxPendingTimerTxt(String cnxPendingTimerTxt) {
        this.cnxPendingTimerTxt = cnxPendingTimerTxt;
    }

    /**
     * @return Returns the connectingTimerTxt.
     */
    public String getConnectingTimerTxt() {
        return connectingTimerTxt;
    }

    /**
     * @param connectingTimerTxt The connectingTimerTxt to set.
     */
    public void setConnectingTimerTxt(String connectingTimerTxt) {
        this.connectingTimerTxt = connectingTimerTxt;
    }

    /**
     * @return Returns the txPendingTimerTxt.
     */
    public String getTxPendingTimerTxt() {
        return txPendingTimerTxt;
    }

    /**
     * @param txPendingTimerTxt The txPendingTimerTxt to set.
     */
    public void setTxPendingTimerTxt(String txPendingTimerTxt) {
        this.txPendingTimerTxt = txPendingTimerTxt;
    }

    // ------------------------------------------------------------- Public Methods

    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        version = null;
    }

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        return new ActionErrors();
    }

}
