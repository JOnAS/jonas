/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.deploy;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author Patrick Smith
 *         Greg Lapouchnian
 */

public class DomainDeployForm extends ActionForm {

// --------------------------------------------------------- Constants

    public static final String DEPLOY = "deploy";
    public static final String UPLOAD = "upload";
    public static final String UPLOADDEPLOY = "uploadDeploy";
    public static final String UNDEPLOY = "undeploy";

// --------------------------------------------------------- Properties variables
    /**
     * all the apps available for deployment
     */
    private ArrayList listDeployable = new ArrayList();
    /**
     * deployed apps
     */
    private ArrayList listDeployed = new ArrayList();
    /**
     * possible targets in the domain (contain corresponding ONs)
     */
    private ArrayList listTargets = new ArrayList();
    /**
     * possible target names in the domain
     */
    private ArrayList listTargetNames = new ArrayList();
    /**
     * the apps that were selected by the user to be deployed
     */
    private ArrayList listDeploy = new ArrayList();
    /**
     * the targets selected by the user
     */
    private ArrayList listTargetsSelected = new ArrayList();
    /**
     * the names selected by the user
     */
    private ArrayList listTargetSelectedNames = new ArrayList();

    // store the progress report to provide the user with feedback
    private Map reports = new TreeMap();

    private String deploy = null;

    // which deployment option was selected by the user
    private String selectedOption = null;
    // should applications with the same name be replaced during upload
    private boolean replaceOnTarget = false;
    // should applications be installed in the autoload directory
    private boolean autoload = false;
    // apps to deploy
    private String[] deploySelected = new String[0];
    /**
     * all the targets selected by the apply
     */
    private String[] targetSelected = new String[0];

    // has the user confirmed the deployment?
    private boolean confirm = false;
    // is there a deployment operation in progress?
    private boolean deploymentInProgress = false;
    // is there a deployment completed?
    private boolean deploymentCompleted = false;

    // the action to take
    private String selectedAction = null;
    // replace on target option
    private boolean replacementOption = false;

    // Has an exception occured during this operation.
    private boolean exception = false;

    // Is the archive type selected configurable.
    private boolean isConfigurable;

// --------------------------------------------------------- Public Methods

    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        deploySelected = new String[0];
        targetSelected = new String[0];
        setSelectedOption(DEPLOY);
        replaceOnTarget = false;
        exception = false;
    }

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors oErrors = new ActionErrors();
        return oErrors;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(" listDeployable = ").append(listDeployable).append("\n");
        sb.append(" listServers = ").append(listTargets).append("\n");
        sb.append(" listDeploy = ").append(listDeploy).append("\n");
        sb.append(" listTargetsSelected = ").append(listTargetsSelected).append("\n");
        sb.append(" reports = ").append(reports).append("\n");
        sb.append(" deploySelected = [");
        for (int i = 0; i < deploySelected.length; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(deploySelected[i]);
        }
        sb.append("]\n");
        sb.append(" serverSelected = [");
        for (int i = 0; i < targetSelected.length; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(targetSelected[i]);
        }
        sb.append("]\n");
        sb.append(" deploy = ").append(deploy).append("\n");
        sb.append(" progress = ").append(deploymentInProgress).append("\n");
        sb.append(" selectedOption = ").append(selectedOption).append("\n");
        sb.append(" replaceOnTarget = ").append(replaceOnTarget).append("\n");
        sb.append(" selectedAction = ").append(selectedAction).append("\n");
        sb.append(" replacementOption = ").append(replacementOption).append("\n");

        return sb.toString();
    }

// --------------------------------------------------------- Properties Methods

    public ArrayList getListTargets() {
        return listTargets;
    }

    public void setListTargets(ArrayList listTargets) {
        this.listTargets = listTargets;
    }

    public ArrayList getListTargetNames() {
        return listTargetNames;
    }

    public void setListTargetNames(ArrayList listTargetNames) {
        this.listTargetNames = listTargetNames;
    }

    public Map getReports() {
        return reports;
    }

    public void setReports(Map reports) {
        this.reports = reports;
    }

    public ArrayList getListTargetsSelected() {
        return listTargetsSelected;
    }

    public void setListTargetsSelected(ArrayList listTargetsSelected) {
        this.listTargetsSelected = listTargetsSelected;
    }

    public ArrayList getListTargetSelectedNames() {
        return listTargetSelectedNames;
    }

    public void setListTargetSelectedNames(ArrayList listTargetSelectedNames) {
        this.listTargetSelectedNames = listTargetSelectedNames;
    }

    public ArrayList getListDeployable() {
        return listDeployable;
    }

    public void setListDeployable(ArrayList listDeployable) {
        this.listDeployable = listDeployable;
    }

    public String[] getDeploySelected() {
        return deploySelected;
    }

    public void setDeploySelected(String[] deploySelected) {
        this.deploySelected = deploySelected;
    }

    public String[] getTargetSelected() {
        return targetSelected;
    }

    public void setTargetSelected(String[] serverSelected) {
        this.targetSelected = serverSelected;
    }

    public ArrayList getListDeploy() {
        return listDeploy;
    }

    public void setListDeploy(ArrayList listDeploy) {
        this.listDeploy = listDeploy;
    }

    public ArrayList getListDeployed() {
        return listDeployed;
    }

    public void setListDeployed(ArrayList listDeployed) {
        this.listDeployed = listDeployed;
    }
    
    public String getDeploy() {
        return deploy;
    }

    public void setDeploy(String deploy) {
        this.deploy = deploy;
    }

    public boolean isConfirm() {
        return confirm;
    }

    public void setConfirm(boolean confirm) {
        this.confirm = confirm;
    }

    public boolean getDeploymentInProgress() {
        return deploymentInProgress;
    }

    public void setDeploymentInProgress(boolean deploymentInProgress) {
        this.deploymentInProgress = deploymentInProgress;
    }

    public String getSelectedOption() {
        return this.selectedOption;
    }

    public void setSelectedOption(String selectedOption) {
        this.selectedOption = selectedOption;
    }

    public String getSelectedAction() {
        return this.selectedAction;
    }

    public void setSelectedAction(String selectedAction) {
        this.selectedAction = selectedAction;
    }

    public boolean getReplaceOnTarget() {
        return replaceOnTarget;
    }

    public void setReplaceOnTarget(boolean replaceChecked) {
        this.replaceOnTarget = replaceChecked;
    }

    public boolean getReplacementOption() {
        return replacementOption;
    }

    public void setReplacementOption(boolean replacementOption) {
        this.replacementOption = replacementOption;
    }

    public boolean getException() {
        return exception;
    }

    public void setException(boolean exception) {
        this.exception = exception;
    }


    /**
     * Return if the current deployment type is configurable.
     * @return if the current deployment type is configurable.
     */
    public boolean getIsConfigurable() {
        return this.isConfigurable;
    }

    /**
     * Sets if the current deployment type is configurable.
     * @param isDomain if current deployment type is configurable.
     */
    public void setIsConfigurable(boolean isConfigurable) {
        this.isConfigurable = isConfigurable;
    }

	public boolean isDeploymentCompleted() {
		return deploymentCompleted;
	}

	public void setDeploymentCompleted(boolean deploymentCompleted) {
		this.deploymentCompleted = deploymentCompleted;
	}

	public boolean isAutoload() {
		return autoload;
	}

	public void setAutoload(boolean autoload) {
		this.autoload = autoload;
	}

}
