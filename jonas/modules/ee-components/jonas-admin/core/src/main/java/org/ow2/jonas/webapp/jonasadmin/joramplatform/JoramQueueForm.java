/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 */

package org.ow2.jonas.webapp.jonasadmin.joramplatform;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

/**
 * Form bean for the Joram local queue form page.
 * @author Frederic MAISTRE
 */

public final class JoramQueueForm extends DestinationForm {

// ------------------------------------------------------------- Properties Variables
    /**
     *
     */
    private String messageIds = null;
    /**
     *
     */
    private int nbMaxMsg = -1;
    /**
     *
     */
    private int pendingMessages = 0;
    /**
     *
     */
    private int pendingRequests = 0;
    /**
     *
     */
    private int threshold = 0;
//  ------------------------------------------------------------- Properties Methods
    /**
     * @return Returns the messageIds.
     */
    public String getMessageIds() {
        return messageIds;
    }

    /**
     * @param messageIds The messageIds to set.
     */
    public void setMessageIds(String messageIds) {
        this.messageIds = messageIds;
    }

    /**
     * @return Returns the nbMaxMsg.
     */
    public int getNbMaxMsg() {
        return nbMaxMsg;
    }

    /**
     * @param nbMaxMsg The nbMaxMsg to set.
     */
    public void setNbMaxMsg(int nbMaxMsg) {
        this.nbMaxMsg = nbMaxMsg;
    }

    /**
     * @return Returns the pendingMessages.
     */
    public int getPendingMessages() {
        return pendingMessages;
    }

    /**
     * @param pendingMessages The pendingMessages to set.
     */
    public void setPendingMessages(int pendingMessages) {
        this.pendingMessages = pendingMessages;
    }

    /**
     * @return Returns the pendingRequests.
     */
    public int getPendingRequests() {
        return pendingRequests;
    }

    /**
     * @param pendingRequests The pendingRequests to set.
     */
    public void setPendingRequests(int pendingRequests) {
        this.pendingRequests = pendingRequests;
    }

    /**
     * @return Returns the threshold.
     */
    public int getThreshold() {
        return threshold;
    }

    /**
     * @param threshold The threshold to set.
     */
    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }

// ------------------------------------------------------------- Public Methods

    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        super.reset(mapping, request);
        messageIds = null;
        nbMaxMsg = -1;
        pendingMessages = 0;
        pendingRequests = 0;
        threshold = -1;
    }

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        return super.validate(mapping, request);
    }
}
