package org.ow2.jonas.webapp.jonasadmin.joramplatform;

public class ItemServer implements java.io.Serializable {
    private String id = null;
    private String defaultDMQ = null;
    private int defaultThreshold;

    public int getDefaultThreshold() {
        return defaultThreshold;
    }
    public void setDefaultThreshold(final int defaultThreshold) {
        this.defaultThreshold = defaultThreshold;
    }
    public ItemServer(final String id, final String defaultDMQ) {
        this.id = id;
        this.defaultDMQ = defaultDMQ;
    }
    public String getId() {
        return id;
    }
    public void setId(final String id) {
        this.id = id;
    }
    public String getDefaultDMQ() {
        return defaultDMQ;
    }
    public void setDefaultDMQ(final String defaultDMQ) {
        this.defaultDMQ = defaultDMQ;
    }


}
