/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.cmi;

import java.io.IOException;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;

public class CmiProtocolAction extends JonasBaseAction{

    @Override
    public ActionForward executeAction(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException {
        String forwardName = "displayCmiProtocolInfo";
        String objName = request.getParameter("protocol");
        CmiProtocolForm oForm = (CmiProtocolForm)form;
        CmiForm currentCmiForm = (CmiForm)m_Session.getAttribute("currentCmiForm");
        try {
            for (Iterator<Protocol> iterator = currentCmiForm.getProtocols().iterator(); iterator.hasNext();) {
                Protocol elt = (Protocol) iterator.next();
                if (elt.getName().equals(objName)) {
                    oForm.getProtocol().setName(elt.getName());
                    oForm.getProtocol().setIcon(elt.getIcon());
                    oForm.getProtocol().setObjectName(elt.getObjectName());
                    oForm.getProtocol().setProviders(elt.getProviders());
                    break;
                }

            }
        } catch (Throwable t) {
            // TODO: handle exception
             addGlobalError(t);
             saveErrors(request, m_Errors);
             return (mapping.findForward("Global Error"));
        }
        return (mapping.findForward(forwardName));
    }

}
