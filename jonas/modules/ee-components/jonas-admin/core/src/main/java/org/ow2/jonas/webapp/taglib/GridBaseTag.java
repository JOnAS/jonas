/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Michel-Ange ANTON
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import org.apache.struts.taglib.html.BaseHandlerTag;

abstract public class GridBaseTag extends BaseHandlerTag {

// ----------------------------------------------------- Protected Constants

    protected final static String QUOTE = "\"";

// ----------------------------------------------------- Instance Variables

    protected String ms_BodyText = null;

// ----------------------------------------------------- Public Methods

    /**
     * Start of Tag processing
     *
     * @exception JspException if a JSP exception occurs
     */
    public int doStartTag()
        throws JspException {
        ms_BodyText = null;
        // Continue processing this page
        return (EVAL_BODY_BUFFERED);
    }

    public int doAfterBody()
        throws JspException {
        if (bodyContent != null) {
            String value = bodyContent.getString().trim();
            if (value.length() > 0) {
                ms_BodyText = value;
            }
        }
        return (SKIP_BODY);
    }

    /**
     * End of Tag Processing
     *
     * @exception JspException if a JSP exception occurs
     */
    public int doEndTag()
        throws JspException {
        StringBuffer sb = new StringBuffer();
        // Before Tag option
        sb.append(prepareBeforeTag());

        // Create a ELEMENT element based on the parameters
        sb.append("<");
        sb.append(getHtmlElement());
        // Prepare this HTML elements attributes
        sb.append(prepareAttributes());
        sb.append(">");

        // Before Body Content option
        sb.append(prepareBeforeBody());
        // Add Body Content
        if (ms_BodyText != null) {
            sb.append(ms_BodyText);
        }
        else {
            sb.append(getDefaultBody());
        }
        // Before After Content option
        sb.append(prepareAfterBody());
        // End Tag
        sb.append("</");
        sb.append(getHtmlElement());
        sb.append(">");

        // After Tag option
        sb.append(prepareAfterTag());

        // Render this element to our writer
        JspWriter out = pageContext.getOut();
        try {
            out.print(sb.toString());
        }
        catch (IOException e) {
            throw new JspException("Exception in " + getClass().getName() + " doEndTag():"
                + e.toString());
        }
        return EVAL_PAGE;
    }

    /**
     * Prepare the attributes of the HTML element
     */
    protected String prepareAttributes() throws JspException {
        StringBuffer sb = new StringBuffer();

        // Append Event Handler details
        sb.append(prepareEventHandlers());
        // Append Style details
        sb.append(prepareStyles());

        return sb.toString();
    }

    /**
     * Format attribute="value" from the specified attribute & value
     */
    protected String prepareAttribute(String attribute, String value) {
        return value == null ? "" : " " + attribute + "=" + QUOTE + value + QUOTE;
    }

    /**
     * Format attribute="value" from the specified attribute & value
     */

    protected String prepareAttribute(String attribute, int value) {
        return value == -1 ? "" : " " + attribute + "=" + QUOTE + value + QUOTE;
    }

    /**
     * Release resources after Tag processing has finished.
     */
    public void release() {
        super.release();
        ms_BodyText = null;
    }

// ----------------------------------------------------- Protected Methods

    abstract protected String getHtmlElement();

    protected String prepareBeforeTag() {
        return "";
    }

    protected String prepareAfterTag() {
        return "";
    }

    protected String prepareBeforeBody() {
        return "";
    }

    protected String prepareAfterBody() {
        return "";
    }

    protected String getDefaultBody() {
        return "";
    }
}