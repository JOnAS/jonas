/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.container;

import java.util.List;
import java.util.Map;

import javax.management.ObjectName;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.webapp.jonasadmin.Jlists;
import org.ow2.jonas.webapp.jonasadmin.service.ModuleForm;

/**
 *
 * @author Michel-Ange ANTON
 * @author Adriana Danes
 *
 */
public class ContainerForm extends ModuleForm {

// --------------------------------------------------------- Properties variables

    private String path = null;
    private String filename = null;
    private String containerName = null;
    private int currentNumberOfMDBType = 0;
    private int currentNumberOfSBFType = 0;
    private int currentNumberOfBMPType = 0;
    private int currentNumberOfSBLType = 0;
    private int currentNumberOfCMPType = 0;
    private int currentNumberOfEntityType = 0;
    private int currentNumberOfBeanType = 0;
    private List ejbs = null;
    private Map<String, String> contexts = null;
    private String[] policies = null;
    private ObjectName objectName = null;
    private boolean monitoringEnabled = false;
    private int warningThreshold = 0;
    private long numberOfCalls = 0;
    private long totalBusinessProcessingTime = 0;
    private long totalProcessingTime = 0;
    private long averageBusinessProcessingTime = 0;
    private long averageProcessingTime = 0;
    private String monitoringApplySettings = null;

// --------------------------------------------------------- Public Methods

    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */

    @Override
    public void reset(final ActionMapping mapping, final HttpServletRequest request) {
        containerName = null;
        currentNumberOfMDBType = 0;
        currentNumberOfSBFType = 0;
        currentNumberOfBMPType = 0;
        currentNumberOfSBLType = 0;
        currentNumberOfCMPType = 0;
        currentNumberOfBeanType = 0;
    }

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    @Override
    public ActionErrors validate(final ActionMapping mapping, final HttpServletRequest request) {
        ActionErrors oErrors = new ActionErrors();
        return oErrors;
    }

// --------------------------------------------------------- Properties Methods

    public String getFilename() {
        return filename;
    }

    public String getContainerName() {
        return containerName;
    }

    public int getCurrentNumberOfMDBType() {
        return currentNumberOfMDBType;
    }

    public int getCurrentNumberOfSBFType() {
        return currentNumberOfSBFType;
    }

    public int getCurrentNumberOfBMPType() {
        return currentNumberOfBMPType;
    }

    public int getCurrentNumberOfSBLType() {
        return currentNumberOfSBLType;
    }

    public int getCurrentNumberOfCMPType() {
        return currentNumberOfCMPType;
    }

    public int getCurrentNumberOfBeanType() {
        return currentNumberOfBeanType;
    }

    public void setFilename(final String filename) {
        this.filename = filename;
    }

    public void setContainerName(final String containerName) {
        this.containerName = containerName;
    }

    public void setCurrentNumberOfMDBType(final int currentNumberOfMDBType) {
        this.currentNumberOfMDBType = currentNumberOfMDBType;
    }

    public void setCurrentNumberOfSBFType(final int currentNumberOfSBFType) {
        this.currentNumberOfSBFType = currentNumberOfSBFType;
    }

    public void setCurrentNumberOfBMPType(final int currentNumberOfBMPType) {
        this.currentNumberOfBMPType = currentNumberOfBMPType;
    }

    public void setCurrentNumberOfSBLType(final int currentNumberOfSBLType) {
        this.currentNumberOfSBLType = currentNumberOfSBLType;
    }

    public void setCurrentNumberOfCMPType(final int currentNumberOfCMPType) {
        this.currentNumberOfCMPType = currentNumberOfCMPType;
    }

    public void setCurrentNumberOfBeanType(final int currentNumberOfBeanType) {
        this.currentNumberOfBeanType = currentNumberOfBeanType;
    }

    public List getEjbs() {
        return ejbs;
    }

    public void setEjbs(final List ejbs) {
        this.ejbs = ejbs;
    }

    public String getPath() {
        return path;
    }

    public void setPath(final String path) {
        this.path = path;
    }

    public int getCurrentNumberOfEntityType() {
        return currentNumberOfEntityType;
    }

    public void setCurrentNumberOfEntityType(final int currentNumberOfEntityType) {
        this.currentNumberOfEntityType = currentNumberOfEntityType;
    }

    public Map<String, String> getContexts() {
        return this.contexts;
    }

    public void setContexts(final Map<String, String> contexts) {
        this.contexts = contexts;
    }

    public String[] getPolicies() {
        return this.policies;
    }

    public void setPolicies(final String[] policies) {
        this.policies = policies;
    }

    public boolean getMonitoringEnabled() {
        return monitoringEnabled;
    }

    public void setMonitoringEnabled(final boolean monitoringEnabled) {
        this.monitoringEnabled = monitoringEnabled;
    }

    public int getWarningThreshold() {
        return warningThreshold;
    }

    public void setWarningThreshold(final int warningThreshold) {
        this.warningThreshold = warningThreshold;
    }

    public long getNumberOfCalls() {
        return numberOfCalls;
    }

    public void setNumberOfCalls(final int numberOfCalls) {
        this.numberOfCalls = numberOfCalls;
    }

    public long getTotalBusinessProcessingTime() {
        return totalBusinessProcessingTime;
    }

    public void setTotalBusinessProcessingTime(final long totalBusinessProcessingTime) {
        this.totalBusinessProcessingTime = totalBusinessProcessingTime;
    }

    public long getTotalProcessingTime() {
        return totalProcessingTime;
    }

    public void setTotalProcessingTime(final long totalProcessingTime) {
        this.totalProcessingTime = totalProcessingTime;
    }

    public long getAverageBusinessProcessingTime() {
        return averageBusinessProcessingTime;
    }

    public void setAverageBusinessProcessingTime(final long averageBusinessProcessingTime) {
        this.averageBusinessProcessingTime = averageBusinessProcessingTime;
    }

    public long getAverageProcessingTime() {
        return averageProcessingTime;
    }

    public void setAverageProcessingTime(final long averageProcessingTime) {
        this.averageProcessingTime = averageProcessingTime;
    }

    public List getBooleanValues() {
        return Jlists.getBooleanValues();
    }

    public String getMonitoringApplySettings() {
        return monitoringApplySettings;
    }

    public void setMonitoringApplySettings(final String monitoringApplySettings) {
        this.monitoringApplySettings = monitoringApplySettings;
    }

    public List getMonitoringApplySettingsValues() {
        return Jlists.getMonitoringApplySettingsValues();
    }

    public ObjectName getObjectName() {
        return objectName;
    }

    public void setObjectName(final ObjectName objectName) {
        this.objectName = objectName;
    }
}