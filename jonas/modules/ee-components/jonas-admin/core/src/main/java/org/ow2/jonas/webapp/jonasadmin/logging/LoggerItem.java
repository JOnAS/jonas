/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.logging;

import org.ow2.jonas.lib.management.extensions.base.NameItem;

/**
 * @author Michel-Ange ANTON
 */
public class LoggerItem implements NameItem {

// --------------------------------------------------------- Constants

    public static String LOGGER_JONAS = "logger.jonas";
    public static String LOGGER_CATALINA_ACCESS_ENGINE = "logger.catalina.accesslogvalve.engine";
    public static String LOGGER_CATALINA_ACCESS_HOST = "logger.catalina.accesslogvalve.host";
    public static String LOGGER_CATALINA_ACCESS_CONTEXT = "logger.catalina.accesslogvalve.context";

// --------------------------------------------------------- Properties Variables

    private String name = null;
    private String forward = null;
    private String type = null;
    private String objectName = null;
    /**
     * The container type on which the logger is defined
     */
    private String containerType = null;
    /**
     * Name of the container on which the logger is defined
     * Used in case of Host and Context conatainers
     */
    private String containerName = null;

// --------------------------------------------------------- Constructors

    public LoggerItem() {
    }

    public LoggerItem(String p_Name, String p_Type, String p_Forward) {
        setName(p_Name);
        setType(p_Type);
        setForward(p_Forward);
    }

    public LoggerItem(String p_Name, String p_Type, String p_Forward, String p_ObjectName, String containerType, String containerName) {
        this(p_Name, p_Type, p_Forward);
        setObjectName(p_ObjectName);
		setContainerType(containerType);
        setContainerName(containerName);
    }

// --------------------------------------------------------- Properties Methods

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getForward() {
        return forward;
    }

    public void setForward(String forward) {
        this.forward = forward;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    /**
     * @return Returns the contextName.
     */
    public String getContainerName() {
        return containerName;
    }


    /**
     * @param contextName The contextName to set.
     */
    public void setContainerName(String contextName) {
        this.containerName = contextName;
    }

    /**
     * @return Returns the containerType.
     */
    public String getContainerType() {
        return containerType;
    }


    /**
     * @param containerType The containerType to set.
     */
    public void setContainerType(String containerType) {
        this.containerType = containerType;
    }


}