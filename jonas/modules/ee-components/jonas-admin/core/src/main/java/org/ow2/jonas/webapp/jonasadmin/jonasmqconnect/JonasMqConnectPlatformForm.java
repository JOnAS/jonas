package org.ow2.jonas.webapp.jonasadmin.jonasmqconnect;

import org.apache.struts.action.ActionForm;

public class JonasMqConnectPlatformForm  extends ActionForm {
    /**
     * debug or not
     */
    private boolean debug = false;

    /**
     * printe debug msg
     * @param s msg
     */
    private void debug(String s) {
        if (debug)
            System.out.println(s);
    }

    /**
     * Version of the connector
     */
    private String version = "1.0.0";

    /**
     * Name of the connector
     */
    private String connector;

    /**
     * Hostname of the connector
     */
    private String hostname;

    /**
     * Channel of the connector
     */
    private String channel;

    /**
     * Port of the connector
     */
    private String port;

    /**
     * QueueManager of the connector
     */
    private String queueManager;

    /**
     * TransportType of the connector
     */
    private String transportType;

    /**
     * @return the version
     */
    public String getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * get the name of the connector
     * @return hostname
     */
    public String getConnector() {
        debug("JonasMqConnectForm#getConnector");
        return connector;
    }

    /**
     * set the name of the connector
     * @param connector
     */
    public void setConnector(String connector) {
        debug("JonasMqConnectForm#setConnector("+connector+")");
        this.connector = connector;
    }

    /**
     * get the hostname of the connector
     * @return hostname
     */
    public String getHostname() {
        debug("JonasMqConnectForm#getHostname");
        return hostname;
    }

    /**
     * set the hostname of the connector
     * @param hostname
     */
    public void setHostname(String hostname) {
        debug("JonasMqConnectForm#setHostname("+hostname+")");
        this.hostname = hostname;
    }

    /**
     * get the channel of the connector
     * @return channel
     */
    public String getChannel() {
        debug("JonasMqConnectForm#getChannel");
        return channel;
    }

    /**
     * set the channel of the connector
     * @param channel
     */
    public void setChannel(String channel) {
        debug("JonasMqConnectForm#setChannel("+channel+")");
        this.channel = channel;
    }

    /**
     * get the port of the connector
     * @return port
     */
    public String getPort() {
        debug("JonasMqConnectForm#getPort");
        return port;
    }

    /**
     * set the port of the connector
     * @param port
     */
    public void setPort(String port) {
        debug("JonasMqConnectForm#setPort("+port+")");
        this.port = port;
    }

    /**
     * get the queueManager of the connector
     * @return queueManager
     */
    public String getQueueManager() {
        debug("JonasMqConnectForm#getQueueManager");
        return queueManager;
    }

    /**
     * set the queueManager of the connector
     * @param queueManager
     */
    public void setQueueManager(String queueManager) {
        debug("JonasMqConnectForm#setQueueManager("+queueManager+")");
        this.queueManager = queueManager;
    }

    /**
     * get the transportType of the connector
     * @return transportType
     */
    public String getTransportType() {
        debug("JonasMqConnectForm#getTransportType");
        return transportType;
    }

    /**
     * set the transportType of the connector
     * @param transportType
     */
    public void setTransportType(String transportType) {
        debug("JonasMqConnectForm#setTransportType("+transportType+")");
        this.transportType = transportType;
    }
}
