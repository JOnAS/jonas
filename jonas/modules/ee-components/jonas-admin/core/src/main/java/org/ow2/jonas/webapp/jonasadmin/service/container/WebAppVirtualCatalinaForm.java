/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.container;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

/**
 * @author S. Ali Tokmen
 */
public class WebAppVirtualCatalinaForm extends WebAppForm {
    /**
     * Contexts for this virtual path.
     */
    private Map<String, String> contexts;

    /**
     * List of possible policies.
     */
    private String[] policies;

    /**
     * Number of active sessions for each version on this virtual path.
     */
    private Map<String, Integer> nbSessions;

    /**
     * Object names for each version on this virtual path.
     */
    private Map<String, String> objectNames;

    /**
     * Reset all properties to their default values.
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    @Override
    public void reset(final ActionMapping mapping, final HttpServletRequest request) {
        super.reset(mapping, request);
    }

    /**
     * Validate the properties that have been set from this HTTP request, and
     * return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found. If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     * @return List of errors
     */
    @Override
    public ActionErrors validate(final ActionMapping mapping, final HttpServletRequest request) {
        ActionErrors oErrors = new ActionErrors();
        return oErrors;
    }

    /**
     * @return Contexts for this virtual path.
     */
    public Map<String, String> getContexts() {
        return contexts;
    }

    /**
     * @param contexts Contexts to set for this virtual path.
     */
    public void setContexts(final Map<String, String> contexts) {
        this.contexts = contexts;
    }

    /**
     * @return List of possible policies.
     */
    public String[] getPolicies() {
        return policies;
    }

    /**
     * @param policies List of possible policies to set.
     */
    public void setPolicies(final String[] policies) {
        this.policies = policies;
    }

    /**
     * @return Number of active sessions for each version on this virtual path.
     */
    public Map<String, Integer> getNbSessions() {
        return nbSessions;
    }

    /**
     * @param nbSessions Number of active sessions for each version on this
     *        virtual path.
     */
    public void setNbSessions(final Map<String, Integer> nbSessions) {
        this.nbSessions = nbSessions;
    }

    /**
     * @return Object names for each version on this virtual path.
     */
    public Map<String, String> getObjectNames() {
        return objectNames;
    }

    /**
     * @param objectNames Object names for each version on this virtual path.
     */
    public void setObjectNames(final Map<String, String> objectNames) {
        this.objectNames = objectNames;
    }
}
