/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.security;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * @author Michel-Ange ANTON
 */

public class EditMemoryRealmAction extends BaseMemoryRealmAction {

// --------------------------------------------------------- Public Methods

    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws IOException, ServletException {

        WhereAreYou oWhere = (WhereAreYou) p_Request.getSession().getAttribute(WhereAreYou.
                SESSION_NAME);
        String serverName = oWhere.getCurrentJonasServerName();
        String domainName = oWhere.getCurrentDomainName();

        // Form used
        MemoryRealmForm oForm = getForm(p_Mapping, p_Request);
        String sAction = p_Request.getParameter("action");
        // Creating
        if ((sAction != null) && (sAction.equals("create"))) {
            oForm = new MemoryRealmForm();
            oForm.reset(p_Mapping, p_Request);
            m_Session.setAttribute("memoryRealmForm", oForm);
            oForm.setAction("create");
        }

        // Populate
        try {
            // Populate only if action is 'apply'
            if (oForm.getAction().equals("apply")) {
                ObjectName oObjectName = JonasObjectName.securityMemoryFactory(domainName, oForm.getResource());
                oForm.setName(getStringAttribute(oObjectName, "Name"));
                oForm.setGroups((String[]) JonasManagementRepr.invoke(oObjectName, "listGroups", null, null, serverName));
                oForm.setRoles((String[]) JonasManagementRepr.invoke(oObjectName, "listRoles", null, null, serverName));

                oForm.setResource(oForm.getName());
                // Force the node selected in tree
                m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_SERVER)
                    + WhereAreYou.NODE_SEPARATOR + "security" + WhereAreYou.NODE_SEPARATOR
                    + "factory.memory" + WhereAreYou.NODE_SEPARATOR + oForm.getName(), true);
            }
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        // Forward to the jsp.
        return (p_Mapping.findForward("Memory Realm"));
    }

// --------------------------------------------------------- Protected Methods

}
