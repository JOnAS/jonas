/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.ejb;

import javax.management.ObjectName;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;


/**
 * @author Michel-Ange ANTON
 */
public class EditEjbSessionAction extends EditEjbAction {

// --------------------------------------------------------- Public Methods

    @Override
    public ActionForward executeAction(final ActionMapping pMapping,
            final ActionForm pForm,
            final HttpServletRequest pRequest,
            final HttpServletResponse pResponse) {
        ActionMessages messages = new ActionMessages();
        ActionMessage msg = new ActionMessage("default.ejb.type.session.sessionTimeOut");
        messages.add("defaultSessionTimeOut", msg);
        saveMessages(pRequest, messages);
        return super.executeAction(pMapping, pForm, pRequest, pResponse);
    }

// --------------------------------------------------------- Protected Methods

    /**
     * Return a <code>EjbSessionForm</code> instance associate to the EJB.
     *
     * @return A form instance
     */
    @Override
    protected EjbForm getEjbForm() {
        return new EjbSessionForm();
    }


    /**
     * Fill all infos of EJB Session in the <code>EjbSessionForm</code> instance.
     *
     * @param p_Form Instance to fill
     * @param p_ObjectName Instance to get infos
     * @throws Exception
     */
    @Override
    protected void fillEjbInfo(final boolean ejb3, final EjbForm p_Form, final ObjectName p_ObjectName, final String serverName)
        throws Exception {
        fillEjbGlobalInfo(ejb3, p_Form, p_ObjectName);

        EjbSessionForm oForm = (EjbSessionForm) p_Form;
        String sessionTimeOut = toStringIntegerAttribute(p_ObjectName, "sessionTimeOut");
        oForm.setSessionTimeOut(sessionTimeOut);
        oForm.setMonitoringSettingsDefinedInDD(getBooleanAttribute(p_ObjectName, "monitoringSettingsDefinedInDD"));
        oForm.setMonitoringEnabled(getBooleanAttribute(p_ObjectName, "monitoringEnabled"));
        oForm.setWarningThreshold(getIntegerAttribute(p_ObjectName, "warningThreshold"));
        oForm.setNumberOfCalls(getIntegerAttribute(p_ObjectName, "numberOfCalls"));
        oForm.setTotalBusinessProcessingTime(getLongAttribute(p_ObjectName, "totalBusinessProcessingTime"));
        oForm.setTotalProcessingTime(getLongAttribute(p_ObjectName, "totalProcessingTime"));
        oForm.setAverageBusinessProcessingTime(getLongAttribute(p_ObjectName, "averageBusinessProcessingTime"));
        oForm.setAverageProcessingTime(getLongAttribute(p_ObjectName, "averageProcessingTime"));
    }

    /**
     * The global forward to go.
     *
     * @return Forward
     */
    @Override
    protected String getEjbForward() {
        return "Ejb Session";
    }
}

