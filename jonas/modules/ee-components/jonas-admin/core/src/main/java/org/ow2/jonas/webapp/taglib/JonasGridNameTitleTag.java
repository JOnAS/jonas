/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Michel-Ange ANTON
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;

public class JonasGridNameTitleTag extends JonasGridNameTag {

// ------------------------------------------------------------- Instance Variables

    private boolean mb_ForceStyleClass = false;

// ----------------------------------------------------- Public Methods

    /**
     * Start of Tag processing
     *
     * @exception JspException if a JSP exception occurs
     */
    public int doStartTag()
        throws JspException {
        // Set default value
        mb_ForceStyleClass = false;
        if (getStyleClass() == null) {
            // Determine if embedded in an JonasGridRowTitleTag
            Tag oTag = findAncestorWithClass(this, JonasGridRowTitleTag.class);
            if (oTag == null) {
                setStyleClass(JonasGridRowTitleTag.CLASS_TITLE);
            }
            else {
                setStyleClass(((JonasGridRowTitleTag) oTag).getStyleClass());
            }
            mb_ForceStyleClass = true;
        }
        if (getHeight() == null) {
            setHeight("20");
        }
        return super.doStartTag();
    }

    /**
     * End of Tag Processing
     *
     * @exception JspException if a JSP exception occurs
     */
    public int doEndTag()
        throws JspException {
        int iRet = super.doEndTag();
        if (mb_ForceStyleClass == true) {
            mb_ForceStyleClass = false;
            setStyleClass(null);
        }
        return iRet;
    }

}