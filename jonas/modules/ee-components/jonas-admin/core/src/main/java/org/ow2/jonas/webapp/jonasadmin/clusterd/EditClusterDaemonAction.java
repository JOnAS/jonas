/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.webapp.jonasadmin.clusterd;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;
import org.ow2.jonas.webapp.jonasadmin.monitoring.DaemonProxyClusterForm;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


public class EditClusterDaemonAction extends JonasBaseAction {
//	--------------------------------------------------------- Public Methods

    /**
     * Execute action for a JOnAS server instance management
     * @param pMapping mapping info
     * @param pForm form object
     * @param pRequest HTTP request
     * @param pResponse HTTP response
     *
     * @return An <code>ActionForward</code> instance or <code>null</code>
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     *
     *
     */
    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm pForm
            , HttpServletRequest p_Request, HttpServletResponse pResponse)
    throws IOException, ServletException {

        String requestedObjectName = (String) p_Request.getParameter("select");
        boolean changeManagement = false;
        try {
            ObjectName on = ObjectName.getInstance(requestedObjectName);
            String requestedJonasServerName = on.getKeyProperty("name");
            String serverName = m_WhereAreYou.getCurrentJonasServerName();
            m_WhereAreYou.selectNameNode("domain"
                    + WhereAreYou.NODE_SEPARATOR
                    + requestedJonasServerName, true);

            // Form used
            //      JonasServerForm oForm = (JonasServerForm) pForm;
            DaemonProxyClusterForm oForm = (DaemonProxyClusterForm) pForm;


            oForm.setName((String) JonasManagementRepr.getAttribute(on,
                    "Name", serverName));

            oForm.setState((String) JonasManagementRepr.getAttribute(on,
                    "State", serverName));

//			DomainMonitor dm = oWhere.getCurrentDomain();



        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }

        // Forward to the jsp.
        return (p_Mapping.findForward("DaemonProxyCluster"));
    }
}
