/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2009 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.ow2.jonas.webapp.taglib.TreeBuilder;
import org.ow2.jonas.webapp.taglib.TreeControl;
import org.ow2.jonas.webapp.taglib.TreeControlNode;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionServlet;

/**
 * Test <code>Action</code> sets up  tree control data structure
 * for tree widget
 *
 * @author Jazmin Jonson
 * @author Manveen Kaur
 * @author Florent Benoit (changes for struts 1.2.2)
 * @version $Revision: 11749 $ $Date: 2007-10-08 11:47:44 +0200 (lun 08 oct 2007) $
 */

public class SetUpTreeAction extends Action {

    public static final int INIT_PLUGIN_MAX = 10;
    public static final String TREEBUILDER_KEY = "treebuilders";
    public static final String ROOTNODENAME_KEY = "rootnodename";

// --------------------------------------------------------- Public Methods

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param actionForm The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request
        , HttpServletResponse response)
        throws IOException, ServletException {

        //ApplicationServlet servlet = (ApplicationServlet) getServlet();
        ActionServlet servlet = (ActionServlet) getServlet();

        // Getting init parms from web.xml

        // Get the string to be displayed as root node while rendering the tree
        String rootnodeName = (String) servlet.getServletConfig().getInitParameter(ROOTNODENAME_KEY);

        String treeBuildersStr = (String) servlet.getServletConfig().getInitParameter(
            TREEBUILDER_KEY);

        // Make the root node and tree control

        // The root node gets rendered only if its value
        // is set as an init-param in web.xml

        TreeControlNode root = new TreeControlNode("ROOT-NODE", null, rootnodeName
            , "setUpTree.do?select=ROOT-NODE", "content", true);

        TreeControl control = new TreeControl(root);

        if (treeBuildersStr != null) {
            Class treeBuilderImpl;
            TreeBuilder treeBuilderBase;

            ArrayList treeBuilders = new ArrayList(INIT_PLUGIN_MAX);
            int i = 0;
            StringTokenizer st = new StringTokenizer(treeBuildersStr, ",");
            while (st.hasMoreTokens()) {
                treeBuilders.add(st.nextToken().trim());
            }

            if (treeBuilders.size() == 0) {
                treeBuilders.add(treeBuildersStr.trim());
            }
            for (i = 0; i < treeBuilders.size(); i++) {
                try {
                    treeBuilderImpl = Class.forName((String) treeBuilders.get(i));
                    treeBuilderBase = (TreeBuilder) treeBuilderImpl.newInstance();
                    treeBuilderBase.buildTree(control, servlet, request);
                }
                catch (Throwable t) {
                    t.printStackTrace(System.out);
                }
            }
        }

        HttpSession session = request.getSession();
        session.setAttribute("treeControl", control);

        WhereAreYou oWhere = (WhereAreYou) session.getAttribute(WhereAreYou.SESSION_NAME);
        if (oWhere != null) {
            oWhere.setTreeControl(control);
        } else {
            // no more WhereAreYou object in the session 
            oWhere = new WhereAreYou();
        }

        String name = request.getParameter("select");
        if (name != null) {
            control.selectNode(name);
            // Forward back to the Blank page
            return (mapping.findForward("Blank"));
        }

        return (mapping.findForward("Tree"));

    }
}
