/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.domain;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * Form used for EditDomainAction.
 * It is used for domain or cluster display
 * @author danesa
 */
public class DomainForm extends ActionForm {

    /**
     * Serial Id.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Domain name or cluster name (if this is a cluster).
     */
    private String name = null;

    /**
     * Used to define type - domain case.
     */
    public final String typeDomain = "Domain";

    /**
     * Used to define type - cluster case.
     */
    public final String typeCluster = "Cluster";
    /**
     * May be "Domain" or "Cluster".
     */
    private String type = null;

    /**
     * If cluster, gives cluster type (LogicalCluster, CmiCluster etc.).
     */
    private String clusterType = null;

    /**
     *  Domain name (used if this is a cluster).
     */
    private String domainName = null;

    /**
     * master server name (the local server name)
     */
    private String masterName = null;

    /**
     * domain or cluster description
     */
    private String description = null;

    /**
     * master server's MBean OBJECT_NAME
     */
    private String masterON = null;

    /**
     * true if the local server is master
     */
    private boolean isMaster = false;

    /**
     * True if this is a cluster, false if this is domain (root cluster)
     */
    private boolean isCluster = false;
    /**
     * Reset all properties to their default values.
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    @Override
    public void reset(final ActionMapping mapping, final HttpServletRequest request) {
    }

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    @Override
    public ActionErrors validate(final ActionMapping mapping, final HttpServletRequest request) {
        return new ActionErrors();
    }

    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name to set.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return Returns the domainName.
     */
    public String getDomainName() {
        return domainName;
    }

    /**
     * @param domainName The domainName to set.
     */
    public void setDomainName(final String domainName) {
        this.domainName = domainName;
    }

    /**
     * @return Returns the isCluster.
     */
    public boolean isCluster() {
        return isCluster;
    }

    /**
     * @param isCluster The isCluster to set.
     */
    public void setCluster(final boolean isCluster) {
        this.isCluster = isCluster;
    }

    /**
     * @return Returns the masterName.
     */
    public String getMasterName() {
        return masterName;
    }

    /**
     * @param masterName The masterName to set.
     */
    public void setMasterName(final String masterName) {
        this.masterName = masterName;
    }

    /**
     * @return Returns the masterON.
     */
    public String getMasterON() {
        return masterON;
    }

    /**
     * @param masterON The masterON to set.
     */
    public void setMasterON(final String masterON) {
        this.masterON = masterON;
    }

    /**
     * @return Returns the type.
     */
    public String getType() {
        return type;
    }

    /**
     * @param type The type to set.
     */
    public void setType(final String type) {
        this.type = type;
    }

    /**
     * @return Returns the description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description to set.
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * @return Returns the isMaster.
     */
    public boolean isMaster() {
        return isMaster;
    }

    /**
     * @param isMaster The isMaster to set.
     */
    public void setMaster(final boolean isMaster) {
        this.isMaster = isMaster;
    }

    public String getClusterType() {
        return clusterType;
    }

    public void setClusterType(final String clusterType) {
        this.clusterType = clusterType;
    }
}
