/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
/**
 * This action is used to set values before plotting server Thread count graph
 * the graph will be plot using cewolf librairies
 */
package org.ow2.jonas.webapp.jonasadmin.monitoring.graph;
import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ServerThreadCountAction extends JonasBaseAction {
     @Override
        public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form, HttpServletRequest p_Request, HttpServletResponse p_Response) throws IOException, ServletException {
//	    	 Form used
            WhereAreYou oWhere = (WhereAreYou) p_Request.getSession().getAttribute(
                    WhereAreYou.SESSION_NAME);
            String sDomainName = oWhere.getCurrentDomainName();
            String selectedServerName = p_Request.getParameter("srvSelected");
            String serverName = oWhere.getCurrentJonasServerName();
            try {
                ObjectName on = new ObjectName(sDomainName
                        + ":type=ServerProxy,name=" + selectedServerName);
                //set attributes for jsp
                p_Request.setAttribute("srvOn",on );
                p_Request.setAttribute("currentServerName", serverName);
                //name in the web.xml
                p_Request.setAttribute("servletUrl", "ThreadCountServlet");
                //this name is in stuts-config.xml
                //TODO dynamically get stuts action name of the current action
                //and replaced hard coded name with this dynamic action name
                p_Request.setAttribute("actionName", "ActionServerThreadCount");
                p_Request.setAttribute("selectedTabTitle", selectedServerName+" Threads Count");
                
                //set attribute for servlets
                m_Session.setAttribute("srvOn", on);
            } catch (Throwable t) {
                addGlobalError(t);
                saveErrors(p_Request, m_Errors);
                return (p_Mapping.findForward("Global Error"));
            }
            // Forward to the jsp that dispalys all server graphs.
            return (p_Mapping.findForward("ServerGraphsDisplay"));
        }


}
