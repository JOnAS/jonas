package org.ow2.jonas.webapp.jonasadmin.domain;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;

/**
 * Action allowing to halt a managed server in the domain.
 * @author Adriana Danes
 */
public class HaltJonasServerAction extends JonasBaseAction {


 // --------------------------------------------------------- Public Methods
     @Override
    public ActionForward executeAction(final ActionMapping p_Mapping, final ActionForm p_Form
             , final HttpServletRequest p_Request, final HttpServletResponse p_Response)
     throws IOException, ServletException {
         // server to stop
         String serverName = p_Request.getParameter("name");
         // current server
         String currentServerName = m_WhereAreYou.getCurrentJonasServerName();
         String domainName = m_WhereAreYou.getCurrentDomainName();
         try {
             ObjectName on = JonasObjectName.serverProxy(domainName, serverName);
             String opName = "haltit";
             JonasManagementRepr.invoke(on, opName, null, null, currentServerName);
         } catch (Throwable t) {
             addGlobalError(t);
             saveErrors(p_Request, m_Errors);
             return (p_Mapping.findForward("Global Error"));
         }
         // Forward to the jsp.
         return (p_Mapping.findForward("ActionEditDomain"));
     }
}
