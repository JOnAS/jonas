package org.ow2.jonas.webapp.jonasadmin.jonasmqconnect;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;
import org.ow2.jonas.webapp.jonasadmin.jonasmqconnect.util.MqObjectNames;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class DestinationDeleteConfirmAction extends JonasBaseAction {

    public ActionForward executeAction(ActionMapping mapping, ActionForm form
            , HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        DestinationDeleteForm fBean = (DestinationDeleteForm) form;

        WhereAreYou oWhere = (WhereAreYou) request.getSession().getAttribute(WhereAreYou.
                SESSION_NAME);
        String serverName = oWhere.getCurrentJonasServerName();
        String domainName = oWhere.getCurrentDomainName();

        try {
            ObjectName mbName = MqObjectNames.getConnectorONByName(
                    domainName,
                    (String) m_Session.getAttribute("mqconnector"));
            String[] destinations = {};
            if (fBean.getDestinationsStr() != null) {
                destinations = fBean.getDestinationsStr().split(";");
            }
            boolean deletePhysicalDestinations = false;
            if (fBean.getDeletePhysicalDestination() != null) {
                deletePhysicalDestinations =
                fBean.getDeletePhysicalDestination().toLowerCase().equals("yes");
            }
            for (int i = 0; i < destinations.length; i++) {
                String jndiName = destinations[i];
                Object[] params = {jndiName,
                    deletePhysicalDestinations ? Boolean.TRUE : Boolean.FALSE};
                String[] signature = {"java.lang.String", "boolean"};
                JonasManagementRepr.invoke(mbName, "deleteDestination", params,
                    signature, serverName);
            }
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(request, m_Errors);
            return (mapping.findForward("Global Error"));
        }
        return mapping.findForward("JonasMqConnectDestinationsAction");
    }
}