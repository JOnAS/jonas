/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.container;

import java.util.Comparator;

/**
 * @author Michel-Ange ANTON
 */
public class ContainerItemByFile implements Comparator {

// --------------------------------------------------------- Public Methods

    public int compare(Object pO1, Object pO2) {
        ContainerItem oContainer1 = (ContainerItem) pO1;
        ContainerItem oContainer2 = (ContainerItem) pO2;
        if ((oContainer1.getFile() == null) || (oContainer2.getFile() == null)) {
            if ((oContainer1.getFile() == null) && (oContainer2.getFile() == null)) {
                return 0;
            } else if ((oContainer1.getFile() == null) && (oContainer2.getFile() != null)) {
                return 1;
            } else {
                return -1;
            }
        }
        return oContainer1.getFile().compareToIgnoreCase(oContainer2.getFile());
    }

    public boolean equals(Object pObj) {
        if (pObj instanceof ContainerItem) {
            return true;
        }
        return false;
    }
}