/*
 * $Header$
 * $Revision: 11749 $
 * $Date: 2007-10-08 11:47:44 +0200 (lun 08 oct 2007) $
 *
 * ====================================================================
 *
 * The Apache Software License, Version 1.1
 *
 * Copyright (c) 2001 The Apache Software Foundation.  All rights
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution, if
 *    any, must include the following acknowlegement:
 *       "This product includes software developed by the
 *        Apache Software Foundation (http://www.apache.org/)."
 *    Alternately, this acknowlegement may appear in the software itself,
 *    if and wherever such third-party acknowlegements normally appear.
 *
 * 4. The names "The Jakarta Project", "Tomcat", and "Apache Software
 *    Foundation" must not be used to endorse or promote products derived
 *    from this software without prior written permission. For written
 *    permission, please contact apache@apache.org.
 *
 * 5. Products derived from this software may not be called "Apache"
 *    nor may "Apache" appear in their names without prior written
 *    permission of the Apache Group.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE APACHE SOFTWARE FOUNDATION OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */

package org.ow2.jonas.webapp.jonasadmin;

import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.ow2.jonas.lib.util.Log;
import org.ow2.jonas.webapp.taglib.TreeControl;
import org.ow2.jonas.webapp.taglib.TreeControlNode;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * Test <code>Action</code> that handles events from the tree control test
 * page.
 *
 * @author Craig R. McClanahan
 * @version $Revision: 11749 $ $Date: 2007-10-08 11:47:44 +0200 (lun 08 oct 2007) $
 */

public class TreeAction extends Action {

	private static Logger logger = Log.getLogger(Log.JONAS_ADMIN_PREFIX);

// --------------------------------------------------------- Public Methods

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param actionForm The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request
        , HttpServletResponse response)
        throws IOException, ServletException {

        String name = null;
        HttpSession session = request.getSession();
        WhereAreYou oWhere = (WhereAreYou) session.getAttribute(WhereAreYou.SESSION_NAME);
        // Detect if a WhereAreYou instance is present
        if (oWhere == null) {
            // Re-init
            //getServlet().log("Instance WhereAreYou not found in session : re-init");
			if (logger.isLoggable(BasicLevel.DEBUG)) {
                logger.log(BasicLevel.DEBUG, "Instance WhereAreYou not found in session : re-init");
            }
            return (mapping.findForward("Main Index"));
        }
        // Get the tree
        TreeControl control = oWhere.getTreeControl();
        // Handle a tree expand/contract event
        name = request.getParameter("tree");
        if (name != null) {
            //getServlet().log("Tree expand/contract on " + name);
			if (logger.isLoggable(BasicLevel.DEBUG)) {
                logger.log(BasicLevel.DEBUG, "Tree expand/contract on " + name);
            }
            TreeControlNode node = control.findNode(name);
            if (node != null) {
                //getServlet().log("Found Node: " + name + "[" + node.getLabel() + "]");
				if (logger.isLoggable(BasicLevel.DEBUG)) {
	                logger.log(BasicLevel.DEBUG, "Found Node: " + name + "[" + node.getLabel() + "]");
	            }
                node.setExpanded(!node.isExpanded());
                // Add the anchor
                request.setAttribute("anchorToGo", URLEncoder.encode(name, "UTF-8"));
            }
        }
        else {
            //getServlet().log("tree param is null");
			if (logger.isLoggable(BasicLevel.DEBUG)) {
                logger.log(BasicLevel.DEBUG, "tree param is null");
            }
        }

        // Handle a select item event
        name = request.getParameter("select");
        if (name != null) {
            //getServlet().log("Select event on " + name);
			if (logger.isLoggable(BasicLevel.DEBUG)) {
                logger.log(BasicLevel.DEBUG, "Select event on " + name);
            }
            control.selectNode(name);
            // Add the anchor
            request.setAttribute("anchorToGo", URLEncoder.encode(name, "UTF-8"));
        }

        // Forward back to the test page
        return (mapping.findForward("Tree"));
    }
}
