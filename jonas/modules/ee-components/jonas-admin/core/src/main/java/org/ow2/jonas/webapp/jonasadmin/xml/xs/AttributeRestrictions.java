/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.xml.xs;

/**
 * Interface for attribute restrictions from schema files.
 *
 * @author Gregory Lapouchnian
 * @author Patrick Smith
 */
public interface AttributeRestrictions {

    /**
     * Get the fixed string for this attribute.
     * @return the fixed string for this attribute.
     */
    String getFixed();

    /**
     * Sets the fixed string for this attribute
     * @param fixed the fixed string for this attribute.
     */
    void setFixed(String fixed);

    /**
     * If this attribute is optional.
     * @return true if this element is optional, false otherwise.
     */
    boolean isOptional();

    /**
     * Sets if this attribute is optional
     * @param isOptional the value to set.
     */
    void setOptional(boolean isOptional);

    /**
     * the name of this attribute.
     * @return the name of this attribute.
     */
    String getName();

    /**
     * Sets the name of this attribute.
     * @param name the name to set.
     */
    void setName(String name);
}
