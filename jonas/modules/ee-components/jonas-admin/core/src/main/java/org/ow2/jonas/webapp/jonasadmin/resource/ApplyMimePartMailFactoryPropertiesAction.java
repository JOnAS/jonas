/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.resource;

import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 *
 */
public class ApplyMimePartMailFactoryPropertiesAction extends EditMailFactoryPropertiesAction {
    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws ServletException {

        // Current server
        String serverName = m_WhereAreYou.getCurrentJonasServerName();
        String domainName = m_WhereAreYou.getCurrentDomainName();

        // Form used
        MailFactoryPropertiesForm oForm = (MailFactoryPropertiesForm) m_Session.getAttribute("mailFactoryPropertiesForm");
        // Mail factory name
        String name = oForm.getMailFactoryName();
        try {
            // Create and load the session mail factory
            boolean mimePartFactory = true;
            Properties props = getPropsFromForm(oForm, mimePartFactory);
            Boolean fromFile = new Boolean(true);
            Object[] aoParam = { name, props, fromFile };
            String[] asSign_3 = { "java.lang.String", "java.util.Properties", "java.lang.Boolean" };
            JonasManagementRepr.invoke(JonasObjectName.mailService(domainName), "createMailFactoryMBean", aoParam, asSign_3, serverName);
            // refresh tree
            m_WhereAreYou.setCurrentJonasDeploymentType(WhereAreYou.DEPLOYMENT_MAIL);
            refreshTree(p_Request);
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        // Remove form
        m_Session.removeAttribute("");
        return (p_Mapping.findForward("ActionListMailFactories"));
    }
}
