/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.jms;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * Principal action for JMS service management. Allows to get the configuration
 * attributes of the MOM implementing the jms service.
 * @author Adriana Danes
 */
public class EditJmsServiceAction extends JonasBaseAction {

    public static final String JORAM = "org.ow2.jonas.lib.jms.JmsAdminForJoram";

    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws IOException, ServletException {

        // Force the node selected in tree
        m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_SERVER) + WhereAreYou.NODE_SEPARATOR
            + "services" + WhereAreYou.NODE_SEPARATOR + "jms", true);
        String domainName = m_WhereAreYou.getCurrentDomainName();

        // Form used
        JmsServiceForm oForm = (JmsServiceForm) p_Form;
        try {
            // Object name used
            ObjectName oObjectName = JonasObjectName.jmsService(domainName);
            String momName = (String) getStringAttribute(oObjectName, "Mom");
            if (momName.equals(JORAM)) {
                momName = m_Resources.getMessage("tab.mom.joram");
            }
            oForm.setMomName(momName);

            String location;
            String currentServer = m_WhereAreYou.getCurrentJonasServerName();
            Boolean momLocal = (Boolean) JonasManagementRepr.invoke(oObjectName, "isMomLocal", null, null, currentServer);
            if (momLocal.booleanValue()) {
                // Collacated MOM
                location = m_Resources.getMessage("tab.mom.collocated");
                oForm.setMomLocation(location);
            } else {
                // Distant MOM
                location = m_Resources.getMessage("tab.mom.distant");
                oForm.setMomLocation(location);
            }

            String momUrl = (String) getStringAttribute(oObjectName, "Url");
            oForm.setMomUrl(momUrl);
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }

        // Forward to the jsp.
        return (p_Mapping.findForward("Jms Service"));
    }
}
