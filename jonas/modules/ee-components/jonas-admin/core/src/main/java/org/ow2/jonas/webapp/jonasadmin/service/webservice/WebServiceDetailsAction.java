/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.webservice;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;
import org.ow2.jonas.webapp.jonasadmin.service.webservice.provider.JOnASProvider;
import org.ow2.jonas.webapp.jonasadmin.service.webservice.provider.element.WebServiceDescriptionItem;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Vivek Lakshmanan
 */
public class WebServiceDetailsAction extends JonasBaseAction {

    /**
     * WebService Details page.
     */
    private static final String WEB_SERVICE_DETAILS_FORWARD = "WebService Details";

// --------------------------------------------------------- Public Methods

    public ActionForward executeAction(ActionMapping p_Mapping,
                                       ActionForm p_Form,
                                       HttpServletRequest p_Request,
                                       HttpServletResponse p_Response)
                         throws IOException, ServletException {

        // Selected web service description
        String wsDescId = p_Request.getParameter("select");

        try{
            JOnASProvider jonasProvider = new JOnASProvider(m_WhereAreYou.getCurrentJonasServerName());
            WebServiceDescriptionItem wsd = jonasProvider.getWebServiceDescription(wsDescId);
            // Set the selected description in the request.
            p_Request.setAttribute("selectedWebServiceDescription", wsd);
            m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_SERVER) + WhereAreYou.NODE_SEPARATOR
                    + "services" + WhereAreYou.NODE_SEPARATOR + "WebService" + WhereAreYou.NODE_SEPARATOR + wsd.getName(), true);
        }
        catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward(GLOBAL_ERROR_FORWARD));
        }
        // Forward to the jsp.
        return (p_Mapping.findForward(WEB_SERVICE_DETAILS_FORWARD));
    }



}
