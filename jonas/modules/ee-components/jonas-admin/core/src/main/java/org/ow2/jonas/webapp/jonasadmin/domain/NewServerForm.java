/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2003-2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.domain;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 * @author Adriana Danes
 * @author S. Ali Tokmen
 */

public final class NewServerForm extends ActionForm {

// ----------------------------------------------------- Properties Variables

    private boolean isCluster = false;
    // Used when a new server is to be add to the domain
    private String serverName = null;
    private String serverURL = null;
    private String serverCld = null;
    private String serverUsername = null;
    private String serverPassword = null;
    private String serverPasswordAgain = null;
    // Used when a new server is to be add to a cluster
    private String[] serverNames = null;
    private String[] selectedItems = new String[0];
// --------------------------------------------------------- Public Methods

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        super.reset(mapping, request);
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors oErrors = new ActionErrors();
        if (!isCluster) {
            if ((serverName == null) || (serverName.length() < 1)) {
                oErrors.add("serverName", new ActionMessage("error.logger.domain.servername.required"));
            }
            if ((serverURL == null) || (serverURL.length() < 1)) {
                oErrors.add("serverURL", new ActionMessage("error.logger.domain.serverurl.required"));
            }
            if ((serverPassword != null) && !(serverPassword.equals(serverPasswordAgain))) {
                oErrors.add("serverPassword", new ActionMessage(""));
                oErrors.add("serverPasswordAgain", new ActionMessage("error.logger.domain.passwords.mismatch"));
                serverPassword = "";
                serverPasswordAgain = "";
            }
        }
        return oErrors;
    }

//  ------------------------------------------------------------- Properties Methods

    /**
     * @return Returns the serverName.
     */
    public String getServerName() {
        return serverName;
    }

    /**
     * @param serverName The serverName to set.
     */
    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    /**
     * @return Returns the serverURL.
     */
    public String getServerURL() {
        return serverURL;
    }

    /**
     * @param serverURL The serverURL to set.
     */
    public void setServerURL(String serverURL) {
        this.serverURL = serverURL;
    }
    /**
     * @return the name of server's cluster daemon
     */
    public String getServerCld() {
        return serverCld;
    }

    /**
     * @param serverCld The server cluster daemon name to set.
     */
    public void setServerCld(String serverCld) {
        this.serverCld = serverCld;
    }

    /**
     * @return the serverUsername
     */
    public String getServerUsername() {
        return serverUsername;
    }

    /**
     * @param serverUsername the serverUsername to set
     */
    public void setServerUsername(String serverUsername) {
        this.serverUsername = serverUsername;
    }

    /**
     * @return the serverPassword
     */
    public String getServerPassword() {
        return serverPassword;
    }

    /**
     * @param serverPassword the serverPassword to set
     */
    public void setServerPassword(String serverPassword) {
        this.serverPassword = serverPassword;
    }

    /**
     * @return the serverPasswordAgain
     */
    public String getServerPasswordAgain() {
        return serverPasswordAgain;
    }

    /**
     * @param serverPasswordAgain the serverPasswordAgain to set
     */
    public void setServerPasswordAgain(String serverPasswordAgain) {
        this.serverPasswordAgain = serverPasswordAgain;
    }

    public boolean isCluster() {
        return isCluster;
    }

    public void setCluster(boolean isCluster) {
        this.isCluster = isCluster;
    }

    public String[] getServerNames() {
        return serverNames;
    }

    public void setServerNames(String[] serverNames) {
        this.serverNames = serverNames;
    }

    public String[] getSelectedItems() {
        return selectedItems;
    }

    public void setSelectedItems(String[] selectedItems) {
        this.selectedItems = selectedItems;
    }

}
