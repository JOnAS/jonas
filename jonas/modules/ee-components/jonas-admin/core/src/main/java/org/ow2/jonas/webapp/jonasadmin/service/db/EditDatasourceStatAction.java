/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.db;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.javaee.J2eeObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Adriana Danes
 */

public class EditDatasourceStatAction extends JonasBaseAction {

// --------------------------------------------------------- Public Methods

    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws IOException, ServletException {

        // Datasource to edit
        String sName = null;

        // Form used is already in session as we already executed one EditDatasource action
        DatasourceForm oForm = (DatasourceForm) m_Session.getAttribute("datasourceForm");

        if (oForm != null) {
            sName = oForm.getDatasourceName();
        } else {
            return (p_Mapping.findForward("Global Error"));
        }

        // Force the node selected in tree
        m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_SERVER) + WhereAreYou.NODE_SEPARATOR
            + "services" + WhereAreYou.NODE_SEPARATOR + "database" + WhereAreYou.NODE_SEPARATOR
            + sName, true);

        // Populate
        try {
            if (sName != null) {
                populate(oForm, m_WhereAreYou.getCurrentDomainName(), m_WhereAreYou.getCurrentJonasServerName());
            }
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        // Forward to the jsp.
        return (p_Mapping.findForward("DatasourceStat"));
    }

// --------------------------------------------------------- Protected Methods

    protected void populate(DatasourceForm p_Form, String domainName, String serverName)
        throws Exception {

        // ObjectName used for JDBCDataSource
        ObjectName oObjectName = J2eeObjectName.getJDBCDataSource(domainName, serverName, p_Form.getDatasourceName());
        
        p_Form.setCurrentOpened(toStringIntegerAttribute(oObjectName, "currentOpened"));
        p_Form.setCurrentBusy(toStringIntegerAttribute(oObjectName, "currentBusy"));
        p_Form.setBusyMaxRecent(toStringIntegerAttribute(oObjectName, "busyMax"));
        p_Form.setBusyMinRecent(toStringIntegerAttribute(oObjectName, "busyMin"));
        p_Form.setCurrentInTx(toStringIntegerAttribute(oObjectName, "currentInTx"));
        p_Form.setOpenedCount(toStringIntegerAttribute(oObjectName, "openedCount"));
        p_Form.setConnectionFailures(toStringIntegerAttribute(oObjectName, "connectionFailures"));
        p_Form.setConnectionLeaks(toStringIntegerAttribute(oObjectName, "connectionLeaks"));
        p_Form.setCurrentWaiters(toStringIntegerAttribute(oObjectName, "currentWaiters"));
        p_Form.setWaitersHigh(toStringIntegerAttribute(oObjectName, "waitersHigh"));
        p_Form.setWaitersHighRecent(toStringIntegerAttribute(oObjectName, "waitersHighRecent"));
        p_Form.setWaiterCount(toStringIntegerAttribute(oObjectName, "waiterCount"));
        p_Form.setWaitingTime(toStringLongAttribute(oObjectName, "waitingTime"));
        p_Form.setWaitingHigh(toStringLongAttribute(oObjectName, "waitingHigh"));
        p_Form.setWaitingHighRecent(toStringLongAttribute(oObjectName, "waitingHighRecent"));
        p_Form.setServedOpen(toStringIntegerAttribute(oObjectName, "servedOpen"));
        p_Form.setRejectedOpen(toStringIntegerAttribute(oObjectName, "rejectedOpen"));
        p_Form.setRejectedFull(toStringIntegerAttribute(oObjectName, "rejectedFull"));
        p_Form.setRejectedTimeout(toStringIntegerAttribute(oObjectName, "rejectedTimeout"));
        p_Form.setRejectedOther(toStringIntegerAttribute(oObjectName, "rejectedOther"));
    }
}
