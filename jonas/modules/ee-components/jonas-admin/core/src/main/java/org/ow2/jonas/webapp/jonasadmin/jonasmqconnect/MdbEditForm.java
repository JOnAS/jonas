package org.ow2.jonas.webapp.jonasadmin.jonasmqconnect;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

public class MdbEditForm  extends ActionForm {
    /**
     * debug or not
     */
    private boolean debug = false;

    /**
     * printe debug msg
     * @param s msg
     */
    private void debug(String s) {
        if (debug)
            System.out.println(s);
    }

    /**
     * operation : view,edit,apply
     */
    private String operation;

    /**
     * Selected queues
     */
    private String[] selectedQueues;

    /**
     * Selected topics
     */
    private String[] selectedTopics;

    /**
     * objectName
     */
    private String objectName;

    /**
     * name
     */
    private String name;

    /**
     * properties
     */
    private ArrayList properties;

    /**
     * stat properties
     */
    private ArrayList statProperties;

    /**
     * properties name
     */
    private ArrayList propertiesName;

    /**
     * properties value
     */
    private ArrayList propertiesValue;

    /**
     * isTopic
     */
    private boolean isTopic;

    /**
     * get name
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * set name
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * get objectName
     * @return
     */
    public String getObjectName() {
        return objectName;
    }

    /**
     * set objectName
     * @param objectName
     */
    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    /**
     * add,modify,delete
     * @return the operation
     */
    public String getOperation() {
        return operation;
    }

    /**
     * add,modify,delete
     * @param operation the operation to set
     */
    public void setOperation(String operation) {
        this.operation = operation;
    }

    /**
     * Selected Queues
     * @return the selected queues
     */
    public String[] getSelectedQueues() {
        return selectedQueues;
    }

    /**
     * selected queues
     * @param selectedQueues the selected queues to set
     */
    public void setSelectedQueues(String[] selectedQueues) {
        this.selectedQueues = selectedQueues;
    }

    /**
     * Selected Topics
     * @return the selected topics
     */
    public String[] getSelectedTopics() {
        return selectedTopics;
    }

    /**
     * selected topics
     * @param selectedTopics the selected topics to set
     */
    public void setSelectedTopics(String[] selectedTopics) {
        this.selectedTopics = selectedTopics;
    }

    /**
     * @return the propertiesName
     */
    public ArrayList getPropertiesName() {
        return propertiesName;
    }

    /**
     * @param propertiesName the propertiesName to set
     */
    public void setPropertiesName(ArrayList propertiesName) {
        this.propertiesName = propertiesName;
    }

    /**
     * @return the propertiesValue
     */
    public ArrayList getPropertiesValue() {
        return propertiesValue;
    }

    /**
     * @param propertiesValue the propertiesValue to set
     */
    public void setPropertiesValue(ArrayList propertiesValue) {
        this.propertiesValue = propertiesValue;
    }

    /**
     * @return the properties
     */
    public ArrayList getProperties() {
        return properties;
    }

    /**
     * @param statProperties the stat properties to set
     */
    public void setStatProperties(ArrayList statProperties) {
        this.statProperties = statProperties;
    }

    /**
     * @return the stat properties
     */
    public ArrayList getStatProperties() {
        return statProperties;
    }

    /**
     * @param properties the properties to set
     */
    public void setProperties(ArrayList properties) {
        this.properties = properties;
    }

    /**
     * @return isTopic
     */
    public boolean getIsTopic() {
        return isTopic;
    }

    /**
     * @param isTopic
     */
    public void setIsTopic(boolean isTopic) {
        this.isTopic = isTopic;
    }
}
