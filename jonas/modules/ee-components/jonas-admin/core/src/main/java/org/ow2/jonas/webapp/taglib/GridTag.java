/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.taglib;

import javax.servlet.jsp.JspException;

/**
 * @author Michel-Ange ANTON
 */
public class GridTag extends GridTableBaseTag {

// ----------------------------------------------------- Instance Variables

    private int m_iRow = 0;
    private boolean m_bEvenRow = false;

// ----------------------------------------------------- Properties

    private String oddStyleClass = null;
    private String evenStyleClass = null;
    private String globalRowStyleClass = null;
    private int periodRow = 1;
    private int border = 0;
    private int cellSpacing = 0;
    private int cellPadding = 0;

    /**
     * Return the Style Class of Odd rows
     */
    public String getOddStyleClass() {
        return oddStyleClass;
    }

    /**
     * Set the Style Class of Odd rows
     *
     * @param styleClass HTML Style Class value for Odd rows
     */
    public void setOddStyleClass(String styleClass) {
        this.oddStyleClass = styleClass;
    }

    /**
     *  Style Class of Even rows in a table
     */

    /**
     *  Return the Style Class of Even rows
     */
    public String getEvenStyleClass() {
        return evenStyleClass;
    }

    /**
     * Set the styleClass of Even rows
     *
     * @param styleClass HTML Style Class value for Even rows
     */
    public void setEvenStyleClass(String styleClass) {
        this.evenStyleClass = styleClass;
    }

    public int getPeriodRow() {
        return periodRow;
    }

    public void setPeriodRow(int periodRow) {
        this.periodRow = periodRow;
    }
    public void setPeriodRow(String p_PeriodRow) {
        this.periodRow = Integer.parseInt(p_PeriodRow);
    }

    public int getBorder() {
        return border;
    }

    public void setBorder(int border) {
        this.border = border;
    }

    public int getCellSpacing() {
        return cellSpacing;
    }

    public void setCellSpacing(int cellSpacing) {
        this.cellSpacing = cellSpacing;
    }

    public int getCellPadding() {
        return cellPadding;
    }

    public void setCellPadding(int cellPadding) {
        this.cellPadding = cellPadding;
    }

    public String getGlobalRowStyleClass() {
        return globalRowStyleClass;
    }

    public void setGlobalRowStyleClass(String globalRowStyleClass) {
        this.globalRowStyleClass = globalRowStyleClass;
    }

// ----------------------------------------------------- Public Methods

    /**
     * Start of Tag processing
     *
     * @exception JspException if a JSP exception occurs
     */
    public int doStartTag()
        throws JspException {
        m_bEvenRow = false;
        m_iRow = 0;
        return super.doStartTag();
    }

    /**
     * Release resources after Tag processing has finished.
     */
    public void release() {
        super.release();
        m_iRow = 0;
        m_bEvenRow = false;

        oddStyleClass = null;
        evenStyleClass = null;
        globalRowStyleClass = null;
        periodRow = 1;

        border = 0;
        cellSpacing = 0;
        cellPadding = 0;
        //border = -1;
        //cellSpacing = -1;
        //cellPadding = -1;
    }

// ----------------------------------------------------- Protected Methods

    /**
     * Prepare the attributes of the HTML element
     */
    protected String prepareAttributes() throws JspException {
        StringBuffer sb = new StringBuffer();

        // Append "border" parameter
        sb.append(prepareAttribute("border", border));
        // Append "cellspacing" parameter
        sb.append(prepareAttribute("cellspacing", cellSpacing));
        // Append "cellpadding" parameter
        sb.append(prepareAttribute("cellpadding", cellPadding));

        // Append Event Handler details
        sb.append(super.prepareAttributes());

        return sb.toString();

    }

    protected String getRowStyle(boolean p_bChange) {
        if (globalRowStyleClass == null) {
            if (periodRow > 0) {
                if (m_iRow >= periodRow) {
                    m_bEvenRow = !m_bEvenRow;
                    m_iRow = 0;
                }
                m_iRow++;
            }
            if (p_bChange == true) {
                m_bEvenRow = !m_bEvenRow;
                m_iRow = 1;
            }
            if (m_bEvenRow == true) {
                return evenStyleClass;
            }
            return oddStyleClass;
        }
        return globalRowStyleClass;
    }
}