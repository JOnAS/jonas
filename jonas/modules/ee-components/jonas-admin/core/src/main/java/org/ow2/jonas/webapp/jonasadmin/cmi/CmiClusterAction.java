/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.cmi;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasAdminJmx;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class CmiClusterAction extends JonasBaseAction {
    @Override
    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form, HttpServletRequest p_Request, HttpServletResponse p_Response) throws IOException, ServletException {
        String sDomainName = m_WhereAreYou.getCurrentDomainName();
        String serverName = m_WhereAreYou.getCurrentJonasServerName();
        String forwardName = "displayCmiClusterInfo";
        String clusterName = p_Request.getParameter("clusterItem");
        CmiClusterForm clusterForm = (CmiClusterForm) p_Form;
        try {
            ObjectName cmiOn = JonasObjectName.cmiServer(sDomainName, serverName);
            if (JonasAdminJmx.hasMBeanName(cmiOn, serverName)) {
                if (clusterName != null) {
                    CmiForm currentCmiForm =(CmiForm) m_Session.getAttribute("currentCmiForm");
                    ClusterAttribute cluster = currentCmiForm.getCluster(clusterName);
                    //Display cluster Infos
                    //Set the current session for eventual edition by user.
                    clusterForm.setName(cluster.getName());
                    clusterForm.setObjectList(cluster.getObjectList());
                    m_Session.setAttribute("currentCmiCluster", cluster);

                } else if (true) {
                    //remove a server
                }else if (true) {
                    //modify LBpolicy for an object
                }else if (true) {
                    //Edit CMI infos
                }else if (true) {
                    //set initial server list
                }
            }else {
                //cmi mbean is not registered
                p_Request.setAttribute("newcmi", false);
            }
        }catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        // Forward to the jsp.
        return (p_Mapping.findForward(forwardName));
    }
}
