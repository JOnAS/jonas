/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 */

package org.ow2.jonas.webapp.jonasadmin.joramplatform;

import java.util.ArrayList;
import java.util.List;

import javax.management.ObjectName;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * Form bean for the Joram queue and topic pages
 * @author Adriana Danes
 */

public class DestinationForm extends ActionForm {

// ------------------------------------------------------------- Properties Variables
    private String action = "edit";
    /**
     * admin name (alse jndi name)
     */
    private String name = null;
    /**
     * associated agent id
     */
    private String id = null;
    /**
     * name of the EJBs using this destination
     */
    private ArrayList listUsedByEjb = new ArrayList();
    /**
     * DeadMQueue
     */
    private String dmq = null;
    /**
     * freelyReadable
     */
    private boolean freelyReadable;
    /**
     * freelyWriteable
     */
    private boolean freelyWriteable;
    /**
     * list if readers
     */
    //private String readerList = null;
    private ArrayList readerList = null;
    /**
     * list of writers
     */
    //private String writerList = null;
    private ArrayList writerList = null;
    /**
     * destination type: queue or topic
     */
    private String type = null;
    /**
     * creation date
     */
    private String creationDate = null;
    /**
     * joram client 'queue' type MBean ObjectName
     */
    private ObjectName oName = null;
    /**
     * Represent boolean (true, false) values for freelyReadable/freelyWriteable
     */
    private List booleanVals = null;

//  ------------------------------------------------------------- Properties Methods
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
    /**
     * @return Returns the listUsedByEjb.
     */
    public ArrayList getListUsedByEjb() {
        return listUsedByEjb;
    }
    /**
     * @param listUsedByEjb The listUsedByEjb to set.
     */
    public void setListUsedByEjb(ArrayList listUsedByEjb) {
        this.listUsedByEjb = listUsedByEjb;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return Returns the dmq.
     */
    public String getDmq() {
        return dmq;
    }
    /**
     * @param dmq The dmq to set.
     */
    public void setDmq(String dmq) {
        this.dmq = dmq;
    }
    /**
     * @return Returns the freelyReadable.
     */
    public boolean getFreelyReadable() {
        return freelyReadable;
    }
    /**
     * @param freelyReadable The freelyReadable to set.
     */
    public void setFreelyReadable(boolean freelyReadable) {
        this.freelyReadable = freelyReadable;
    }
    /**
     * @return Returns the freelyWriteable.
     */
    public boolean getFreelyWriteable() {
        return freelyWriteable;
    }
    /**
     * @param freelyWriteable The freelyWriteable to set.
     */
    public void setFreelyWriteable(boolean freelyWriteable) {
        this.freelyWriteable = freelyWriteable;
    }
   /* *//**
     * @return Returns the readerList.
     *//*
    public String getReaderList() {
        return readerList;
    }
    *//**
     * @param readerList The readerList to set.
     *//*
    public void setReaderList(String readerList) {
        this.readerList = readerList;
    }
    *//**
     * @return Returns the writerList.
     *//*
    public String getWriterList() {
        return writerList;
    }
    *//**
     * @param writerList The writerList to set.
     *//*
    public void setWriterList(String writerList) {
        this.writerList = writerList;
    }*/
    /**
     * @return Returns the type.
     */
    public String getType() {
        return type;
    }
    /**
     * @param type The type to set.
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return Returns the creationDate.
     */
    public String getCreationDate() {
        return creationDate;
    }
    /**
     * @param creationDate The creationDate to set.
     */
    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }
// ------------------------------------------------------------- Public Methods

    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        name = null;
        id = null;
        listUsedByEjb = new ArrayList();
        dmq = null;
        freelyReadable = true;
        freelyWriteable = true;
        readerList = null;
        writerList = null;
        type = null;
    }

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        return new ActionErrors();
    }
    /**
     * @return Returns the oName.
     */
    public ObjectName getOName() {
        return oName;
    }
    /**
     * @param name The oName to set.
     */
    public void setOName(ObjectName name) {
        oName = name;
    }

	/**
	 * @return Returns the booleanVals.
	 */
	public List getBooleanVals() {
		return booleanVals;
	}

	/**
	 * @param booleanVals The booleanVals to set.
	 */
	public void setBooleanVals(List booleanVals) {
		this.booleanVals = booleanVals;
	}

	/**
	 * @return Returns the readerList.
	 */
	public ArrayList getReaderList() {
		return readerList;
	}

	/**
	 * @param readerList The readerList to set.
	 */
	public void setReaderList(ArrayList readerList) {
		this.readerList = readerList;
	}

	/**
	 * @return Returns the writerList.
	 */
	public ArrayList getWriterList() {
		return writerList;
	}

	/**
	 * @param writerList The writerList to set.
	 */
	public void setWriterList(ArrayList writerList) {
		this.writerList = writerList;
	}
}
