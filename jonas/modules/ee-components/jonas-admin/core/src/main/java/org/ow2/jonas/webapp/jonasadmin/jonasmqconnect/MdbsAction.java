package org.ow2.jonas.webapp.jonasadmin.jonasmqconnect;

import java.io.IOException;
import java.util.ArrayList;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;
import org.ow2.jonas.webapp.jonasadmin.jonasmqconnect.util.ItemMdb;
import org.ow2.jonas.webapp.jonasadmin.jonasmqconnect.util.LogUtils;
import org.ow2.jonas.webapp.jonasadmin.jonasmqconnect.util.MqObjectNames;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class MdbsAction extends JonasBaseAction {

    public ActionForward executeAction(ActionMapping mapping, ActionForm form
            , HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException {

        MdbsForm fBean = (MdbsForm) form;
        WhereAreYou oWhere = (WhereAreYou) request.getSession().getAttribute(WhereAreYou.
                SESSION_NAME);
        String serverName = oWhere.getCurrentJonasServerName();
        String domainName = oWhere.getCurrentDomainName();

        /*
         * The "mqconnector" session attribute should be set
         */
        String connector = (String) m_Session.getAttribute("mqconnector");
        //domain*jmq1*jonasmqconnect*jonasmqconnector*fwaMQJca
        // Force the node selection in tree
        String nodeName = "domain" + WhereAreYou.NODE_SEPARATOR
                            + serverName + WhereAreYou.NODE_SEPARATOR
                            + "jonasmqconnect" + WhereAreYou.NODE_SEPARATOR
                            + "jonasmqconnector" + WhereAreYou.NODE_SEPARATOR
                            + connector;
        m_WhereAreYou.selectNameNode(nodeName, true);

        try {
            ObjectName mbName = MqObjectNames.getConnectorONByName(domainName, connector);
            Object[] params = {};
            String[] signature = {};
            String[] onMdbs = (String[]) JonasManagementRepr.invoke(
                    mbName, "listConsumerObjectNames", params, signature, serverName);
            ArrayList mdbsArray = new ArrayList();
            for (int i = 0; i < onMdbs.length; i++) {
                ObjectName on = new ObjectName(onMdbs[i]);
                int numberMsg = ((Integer) JonasManagementRepr.getAttribute(
                        on, "NumberOfReceivedMessages", serverName)).intValue();

                String name = on.getKeyProperty("name");
                String mqDest = on.getKeyProperty("MQDestination");
                ItemMdb item = new ItemMdb(name, onMdbs[i], mqDest, numberMsg);
                mdbsArray.add(item);
            }

            m_Session.setAttribute("mqmdbs", mdbsArray);
        } catch (Exception ex) {
            LogUtils.print(ex.getMessage());
        }
        return mapping.findForward("JonasMqConnectMdbs");
    }
}