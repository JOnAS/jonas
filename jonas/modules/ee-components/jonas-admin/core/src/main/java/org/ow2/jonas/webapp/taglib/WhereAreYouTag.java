/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.taglib;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.tagext.BodyTagSupport;

import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;


/**
 * @author Michel-Ange ANTON
 */
public class WhereAreYouTag extends BodyTagSupport {

// ----------------------------------------------------- Instance Variables

// ----------------------------------------------------- Properties

    private boolean usingWhere = false;

    public boolean isUsingWhere() {
        return usingWhere;
    }

    public boolean getUsingWhere() {
        return usingWhere;
    }

    public void setUsingWhere(boolean usingWhere) {
        this.usingWhere = usingWhere;
    }

// --------------------------------------------------------- Public Methods

    public String getSelectedNameNode() {
        String sName = null;
        WhereAreYou oWhere = getWhereAreYouInstance();
        if (oWhere != null) {
            sName = oWhere.getSelectedNameNode();
        }
        return sName;
    }

    public TreeControlNode getSelectedTreeControlNode() {
        TreeControlNode oNode = null;
        WhereAreYou oWhere = getWhereAreYouInstance();
        if (oWhere != null) {
            oNode = oWhere.getSelectedTreeControlNode();
        }
        return oNode;
    }

    public String getImagesRoot() {
        String sImagesRoot = null;
        WhereAreYou oWhere = getWhereAreYouInstance();
        if (oWhere != null) {
            sImagesRoot = oWhere.getImagesRoot();
        }
        return sImagesRoot;
    }

    public boolean isTreeToRefresh() {
        boolean bRefresh = false;
        WhereAreYou oWhere = getWhereAreYouInstance();
        if (oWhere != null) {
            bRefresh = oWhere.isTreeToRefresh();
        }
        return bRefresh;
    }

    public String getUrlToRefreshSelectedNode() {
        String sUrl = null;
        WhereAreYou oWhere = getWhereAreYouInstance();
        if (oWhere != null) {
            sUrl = oWhere.getUrlToRefreshSelectedNode((HttpServletRequest) pageContext.getRequest()
                , (HttpServletResponse) pageContext.getResponse());
        }
        return sUrl;
    }

// --------------------------------------------------------- Protected Methods

    protected WhereAreYou getWhereAreYouInstance() {
        return (WhereAreYou) pageContext.getSession().getAttribute(WhereAreYou.SESSION_NAME);
    }
}
