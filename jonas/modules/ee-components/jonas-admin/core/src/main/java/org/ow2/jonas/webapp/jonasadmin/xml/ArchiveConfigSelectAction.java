/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.xml;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.webapp.jonasadmin.JonasAdminJmx;
import org.ow2.jonas.webapp.jonasadmin.deploy.BaseDeployAction;

/**
 * @author Patrick Smith
 * @author Gregory Lapouchnian
 */
public class ArchiveConfigSelectAction extends BaseDeployAction {

    // --------------------------------------------------------- Public Methods

    /**
     * Executes the struts action.
     * @param p_Mapping the struts action mapping.
     * @param p_Form the struts action form.
     * @param p_Request the HttpServletRequest.
     * @param p_Response the HttpServletResponse.
     * @throws IOException
     * @throws ServletException
     * @return the action forward to forward to.
     */
    @Override
    public ActionForward executeAction(final ActionMapping p_Mapping,
            final ActionForm p_Form, final HttpServletRequest p_Request,
            final HttpServletResponse p_Response) throws IOException,
            ServletException {

        ArchiveConfigForm form = (ArchiveConfigForm) p_Form;

        String serverName = m_WhereAreYou.getCurrentJonasServerName();
        String domainName = m_WhereAreYou.getCurrentDomainName();
        // reset all the values
        form.reset();

        // set the list of deployable files, these are the only files that
        // can be configured
        form.setDeployable(JonasAdminJmx.getRarFilesDeployable(domainName, serverName));
        form.setIsDomain(isDomain());
        form.setJonasBase(getJonasBase());

        // Forward to the jsp.
        return (p_Mapping.findForward("Archive Config Select"));
    }
}
