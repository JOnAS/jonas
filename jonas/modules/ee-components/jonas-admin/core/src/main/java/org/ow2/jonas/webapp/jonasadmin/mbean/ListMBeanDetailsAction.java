/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.mbean;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * List the detail of MBean.
 *
 * @author Michel-Ange ANTON
 */

public class ListMBeanDetailsAction extends JonasBaseAction {

// --------------------------------------------------------- Constants

    public static final int ACTION_PROPERTIES = 0;
    public static final int ACTION_ATTRIBUTES = 1;
    public static final int ACTION_OPERATIONS = 2;

// --------------------------------------------------------- Public Methods

    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws IOException, ServletException {

        // Forward to the corresponding display page
        String sForward = null;
        switch (getLastAction()) {
            case ACTION_PROPERTIES:
                sForward="ActionListMBeanProperties";
                break;
            case ACTION_ATTRIBUTES:
                sForward="ActionListMBeanAttributes";
                break;
            case ACTION_OPERATIONS:
                sForward="ActionListMBeanOperations";
                break;
        }
        return p_Mapping.findForward(sForward);
    }

// --------------------------------------------------------- Protected Methods

    protected int getLastAction()
    {
        Integer oInt = (Integer)m_Session.getAttribute("MBeanAction");
        if (oInt == null) {
            return ACTION_PROPERTIES;
        }
        return oInt.intValue();
    }

    protected void setAction(int p_Action)
    {
        m_Session.setAttribute("MBeanAction", new Integer(p_Action));
    }
}
