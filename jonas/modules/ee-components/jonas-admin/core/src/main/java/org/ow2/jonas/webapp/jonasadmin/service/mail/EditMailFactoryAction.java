/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.mail;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Properties;

import javax.management.ObjectName;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.management.extensions.container.ejb.EjbItem;
import org.ow2.jonas.lib.management.extensions.container.ejb.EjbItemByNameComparator;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;


/**
 * Helper for actions for management of Mail Factories
 * @author Adriana Danes
 */
public abstract class EditMailFactoryAction extends JonasBaseAction {

    // Populate the MailFactory form
    void populate(ObjectName pObjectName, MailFactoryForm pForm, String domainName, String serverName) throws Exception {
        // Set configuration attributes
        // - get the jndi name
        pForm.setJndiName(getStringAttribute(pObjectName, "Name"));
        // - get authentication properties
        Properties authProps = (Properties) JonasManagementRepr.getAttribute(pObjectName, "AuthenticationProperties", serverName);
        String username = (String) authProps.getProperty("mail.authentication.username");
        pForm.setUsername(username);
        String password = (String) authProps.getProperty("mail.authentication.password");
        pForm.setPassword(password);

        // - get the mail session properties
        Properties sessionProps = (Properties) JonasManagementRepr.getAttribute(pObjectName, "SessionProperties", serverName);
        StringBuffer bufSessionProps = new StringBuffer();
        String propName = null;
        String propValue;
        for (Enumeration propNames = sessionProps.propertyNames() ; propNames.hasMoreElements() ;) {
            propName = (String) propNames.nextElement();
            propValue = sessionProps.getProperty(propName);
            bufSessionProps.append(propName);
            bufSessionProps.append("=");
            bufSessionProps.append(propValue);
            bufSessionProps.append(",");
            bufSessionProps.append("\n");
        }
        if (!sessionProps.isEmpty()) {
            // delete the last 2 chars (, and \n)
            bufSessionProps.delete(bufSessionProps.length() - 2, bufSessionProps.length() - 1);
        } else {
            // Actually no chars in sessionPropsBuf
            bufSessionProps.append("mail.host=");
        }
        String sSessionProps = new String(bufSessionProps);
        pForm.setSessionProps(sSessionProps);

        // Set list of EJBs which use this mail factory
        ArrayList al = new ArrayList();
        String[] asParam = new String[1];
        String[] asSignature = new String[1];
        asSignature[0] = "java.lang.String";
        asParam[0] = pForm.getJndiName();
        ObjectName oObjectName = JonasObjectName.ejbService(domainName);
        if (JonasManagementRepr.isRegistered(oObjectName, serverName) == true) {
            java.util.Iterator it = ((java.util.Set) JonasManagementRepr.invoke(
                                                                                oObjectName,
                                                                                "getMailFactoryDependence",
                                                                                asParam,
                                                                                asSignature, serverName)).iterator();
            while (it.hasNext()) {
                al.add(new EjbItem((ObjectName) it.next(), serverName));
            }
            // Sort by name
            Collections.sort(al, new EjbItemByNameComparator());
        }

        // Set list in form
        pForm.setListUsedByEjb(al);
    }

    // Set the specific properties of a MimePartDataSource
    void setMimePartProps(ObjectName pObjectName, MailFactoryForm pForm, String serverName) throws Exception {
        Properties mimepartProps = (Properties)JonasManagementRepr.getAttribute(pObjectName, "MimeMessageProperties", serverName);

        String to = (String) mimepartProps.getProperty("mail.to");
        pForm.setTo(to);

        String subject = (String) mimepartProps.getProperty("mail.subject");
        pForm.setSubject(subject);

        String cc = (String) mimepartProps.getProperty("mail.cc");
        pForm.setCc(cc);

        String bcc = (String) mimepartProps.getProperty("mail.bcc");
        pForm.setBcc(bcc);
    }
}
