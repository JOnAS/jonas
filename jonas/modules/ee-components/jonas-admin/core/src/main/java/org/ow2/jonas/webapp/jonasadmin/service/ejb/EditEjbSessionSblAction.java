/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.ejb;

import javax.management.ObjectName;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;


/**
 * @author Michel-Ange ANTON
 */

public class EditEjbSessionSblAction extends EditEjbSessionAction {

    // --------------------------------------------------------- Public Methods

    public ActionForward executeAction(ActionMapping pMapping,
            ActionForm pForm,
            HttpServletRequest pRequest,
            HttpServletResponse pResponse) {
        ActionMessages messages = new ActionMessages();
        ActionMessage msg = new ActionMessage("default.ejb.type.session.instanceMaxSizePool");
        messages.add("defaultMaxSizePool", msg);
        saveMessages(pRequest, messages);
        return super.executeAction(pMapping, pForm, pRequest, pResponse);
    }


    // --------------------------------------------------------- Protected Methods

    /**
     * Get a <code>ObjectName</code> instance for the Ejb given
     * with the parameters of the HTPP request.
     *
     * @return The ObjectName instance
     * @throws Exception
     */

    /**
     * Return a <code>EjbSessionSblForm</code> instance associate to the EJB.
     *
     * @return A form instance
     */
    protected EjbForm getEjbForm() {
        return new EjbSessionForm();
    }


    /**
     * Fill all infos of EJB Session in the <code>EjbSessionForm</code> instance.
     *
     * @param p_Form Instance to fill
     * @param p_ObjectName Instance to get infos
     * @throws Exception
     */
    protected void fillEjbInfo(boolean ejb3, EjbForm p_Form, ObjectName p_ObjectName, String serverName)
    throws Exception {
        super.fillEjbInfo(ejb3, p_Form, p_ObjectName, serverName);
    }

    /**
     * The global forward to go.
     *
     * @return Forward
     */
    protected String getEjbForward() {
        return "Ejb Session Sbl";
    }

}

