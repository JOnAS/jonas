package org.ow2.jonas.webapp.jonasadmin.service.workmanager;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

public class EditWorkmanagerServiceAction extends JonasBaseAction {

    @Override
    public ActionForward executeAction(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request,
            final HttpServletResponse response) throws IOException, ServletException {

        // Form used
        WorkmanagerServiceForm oForm = (WorkmanagerServiceForm) form;

        // Force the node selected in tree
        m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_SERVER) + WhereAreYou.NODE_SEPARATOR
            + "services" + WhereAreYou.NODE_SEPARATOR + "workmanager", true);

        String domainName = m_WhereAreYou.getCurrentDomainName();

        try {
            // Object name used
            ObjectName objectName = JonasObjectName.workManager(domainName);
            int poolSize = getIntAttribute(objectName, "currentPoolSize");
            oForm.setCurrentPoolSize(poolSize);
            poolSize = getIntAttribute(objectName, "minPoolSize");
            oForm.setMinPoolSize(poolSize);
            poolSize = getIntAttribute(objectName, "maxPoolSize");
            oForm.setMaxPoolSize(poolSize);
        }
        catch (Throwable t) {
            addGlobalError(t);
            saveErrors(request, m_Errors);
            return (mapping.findForward(GLOBAL_ERROR_FORWARD));
        }

        // Forward to the jsp.
        return (mapping.findForward("Workmanager Service"));
    }

}
