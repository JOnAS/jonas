/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;

import org.apache.struts.util.MessageResources;
import org.ow2.jonas.lib.management.extensions.base.AdminJmxHelper;
import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.management.extensions.base.mbean.CatalinaObjectName;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.management.extensions.base.api.ManagementException;
import org.ow2.jonas.webapp.jonasadmin.common.BeanComparator;
import org.ow2.jonas.webapp.jonasadmin.logging.LoggerItem;
import org.ow2.jonas.webapp.jonasadmin.service.container.WebAppItem;

/**
 * Helper class supporting manipulation of MBeans (accessing MBeans).
 * @author Michel-Ange ANTON<p>
 * Contributors: Adriana Danes
 */
public class JonasAdminJmx extends AdminJmxHelper {


// ----------------------------------------------------------- Constructors

    /**
     * Protected constructor to prevent instantiation.
     */
    protected JonasAdminJmx() {
        super();
    }



    /**
     * Return the default Connection Factories.
     *
     * @param p_Resources The messages
     * @return The list
     */
    public static ArrayList getConnectionFactoriesList(final MessageResources p_Resources, final String domainName, final String serverName) {
        synchronized (s_Synchro) {
            ObjectName jmsServMB = JonasObjectName.jmsService(domainName);
            String cfName = (String) JonasManagementRepr.getAttribute(jmsServMB
                , "DefaultConnectionFactoryName", serverName);
            String tcfName = (String) JonasManagementRepr.getAttribute(jmsServMB
                , "DefaultTopicConnectionFactoryName", serverName);
            String qcfName = (String) JonasManagementRepr.getAttribute(jmsServMB
                , "DefaultQueueConnectionFactoryName", serverName);
            ArrayList<JmsConnFact> al = new ArrayList<JmsConnFact>();
            String comment = p_Resources.getMessage("tab.connfact.defaultconnfact");
            JmsConnFact cf = new JmsConnFact(cfName, comment);
            al.add(cf);
            comment = p_Resources.getMessage("tab.connfact.defaulttopicconnfact");
            cf = new JmsConnFact(tcfName, comment);
            al.add(cf);
            comment = p_Resources.getMessage("tab.connfact.defaultqueueconnfact");
            cf = new JmsConnFact(qcfName, comment);
            al.add(cf);
            return al;
        }
    }

    /**
     * Return the list of all used loggers in this JOnAS server.
     * @param p_Resources The used message resource
     * @param p_WhereAreYou The used WhereAreYou instance
     * @param p_Action True to get the action (to use directly in jsp) or False to get the same action but in forward (write in the file struts-config.xml)
     * @return The list of loggers
     * @throws ManagementException
     */
    public static ArrayList getLoggers(final MessageResources p_Resources, final WhereAreYou p_WhereAreYou
        , final boolean p_Action)
        throws MalformedObjectNameException {
        String serverName = p_WhereAreYou.getCurrentJonasServerName();
        ArrayList<LoggerItem> al = new ArrayList<LoggerItem>();
        String sActionForward;
        // Get JONAS logger
        if (p_Action == true) {
            sActionForward = "EditLoggingJonas.do";
        }
        else {
            sActionForward = "ActionEditLoggingJonas";
        }
        String itemName = p_Resources.getMessage("logger.jonas.name");
        String type = LoggerItem.LOGGER_JONAS;
        LoggerItem li = new LoggerItem(itemName, type, sActionForward);
        al.add(li);
        // Get Catalina Logger
        if (p_WhereAreYou.isCatalinaServer() == true) {
            ObjectName on = CatalinaObjectName.catalinaAccessLogValves(p_WhereAreYou.
                getCurrentCatalinaDomainName());

            Iterator it = getListMbean(on, serverName).iterator();
            while (it.hasNext()) {
                ObjectName oItem = (ObjectName) it.next();
                if (p_Action) {
                    sActionForward = "EditCatalinaAccessLogger.do?select=" + oItem.toString();
                }
                else {
                    sActionForward = "ActionEditCatalinaAccessLogger";
                }
                itemName = p_Resources.getMessage("logger.catalina.access.name");
                String hostName = oItem.getKeyProperty("host");
                String containerName = null;
                String containerType = null;
                String containerLabel = null;
                if (hostName == null) {
                    type = LoggerItem.LOGGER_CATALINA_ACCESS_ENGINE;
                    containerType = p_Resources.getMessage("label.loggers.container.engine");
                    containerName = p_Resources.getMessage("label.loggers.container.engine.name");
                    containerLabel = containerName;
                } else {
                    String pathName = oItem.getKeyProperty("path");
                    if (pathName != null) {
                        type = LoggerItem.LOGGER_CATALINA_ACCESS_CONTEXT;
                        containerName = pathName;
                        containerType = p_Resources.getMessage("label.loggers.container.context");
                        containerLabel = WebAppItem.extractLabelPathContext(containerName, p_WhereAreYou.getCurrentCatalinaDefaultHostName());
                    } else {
                        type = LoggerItem.LOGGER_CATALINA_ACCESS_HOST;
                        containerName = hostName;
                        containerType = p_Resources.getMessage("label.loggers.container.host");
                        containerLabel = containerName;
                    }
                }
                itemName = itemName.concat(" " + containerLabel);
                li = new LoggerItem(itemName, type, sActionForward, oItem.toString(), containerType, containerName);
                al.add(li);
            }
        }
        BeanComparator beanComparator = new BeanComparator(new String[]{"name"});
        // sort
        Collections.sort(al, beanComparator);
        return al;
    }
}
