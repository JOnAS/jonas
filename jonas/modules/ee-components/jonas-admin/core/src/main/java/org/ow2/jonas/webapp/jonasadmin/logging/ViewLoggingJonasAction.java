/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.logging;

import java.io.IOException;
import java.util.Set;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author
 */

public class ViewLoggingJonasAction extends JonasBaseAction {
    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
            , HttpServletRequest p_Request, HttpServletResponse p_Response)
    throws IOException, ServletException {
        // Force the node selected in tree
        m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_SERVER)
                + WhereAreYou.NODE_SEPARATOR
                + "logging"
                + WhereAreYou.NODE_SEPARATOR
                + LoggerItem.LOGGER_JONAS, true);

        // Form used
        ViewLoggingJonasForm oForm = (ViewLoggingJonasForm) p_Form;

        // Current server
        String serverName = m_WhereAreYou.getCurrentJonasServerName();
        String domainName = m_WhereAreYou.getCurrentDomainName();

        try {
            ObjectName on = JonasObjectName.logBuffers(domainName);
            Set ons = JonasManagementRepr.queryNames(on, serverName);
            if (ons.isEmpty()) {
                oForm.setBuffer(false);
            } else {
                ObjectName logon = (ObjectName) ons.iterator().next();
                oForm.setBuffer(true);
                oForm.setSevereCount(((Long) JonasManagementRepr.getAttribute(logon, "SevereCount", serverName)).longValue());
                oForm.setWarningCount(((Long) JonasManagementRepr.getAttribute(logon, "WarningCount", serverName)).longValue());
                oForm.setLatestStamp(((Long) JonasManagementRepr.getAttribute(logon, "LatestDate", serverName)).longValue());
                oForm.setOldestStamp(((Long) JonasManagementRepr.getAttribute(logon, "OldestDate", serverName)).longValue());
                oForm.setRecent((String) JonasManagementRepr.getAttribute(logon, "Recent", serverName));
            }
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        // Forward to the jsp.
        return (p_Mapping.findForward("LoggingView"));
    }
}
