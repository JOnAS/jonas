/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.discovery;

import org.apache.struts.action.ActionForm;

/**
 * Form used to present the discovery service properties
 * @author Adriana Danes
 */
public final class DiscoveryServiceForm extends ActionForm {

// ------------------------------------------------------------- Properties Variables
    private String multicastAddress = null;
    private String multicastPort = null;
    private boolean isMaster = false;
    private String multicastTtl = null;
// ------------------------------------------------------------- Properties Methods

    public String getMulticastTtl() {
        return multicastTtl;
    }
    public void setMulticastTtl(String multicastTtl) {
        this.multicastTtl = multicastTtl;
    }
/**
 * @return Returns the multicastAddress.
 */
public String getMulticastAddress() {
    return multicastAddress;
}
/**
 * @param multicastAddress The multicastAddress to set.
 */
public void setMulticastAddress(String multicastAddress) {
    this.multicastAddress = multicastAddress;
}
    /**
     * @return Returns the multicastPort.
     */
    public String getMulticastPort() {
        return multicastPort;
    }
    /**
     * @param multicastPort The multicastPort to set.
     */
    public void setMulticastPort(String multicastPort) {
        this.multicastPort = multicastPort;
    }
    /**
     * @return Returns the isMaster.
     */
    public boolean getMaster() {
        return isMaster;
    }
    /**
     * @param isMaster The isMaster to set.
     */
    public void setMaster(boolean isMaster) {
        this.isMaster = isMaster;
    }
}

