/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.container;

import javax.management.ObjectName;

/**
 * @author Michel-Ange ANTON
 */
public class WebAppItem extends ContainerItem {

// --------------------------------------------------------- Constants

    public final static String LABEL_ROOT_WEBMODULE = "ROOT";
    public final static String APPEND_CHAR = "_";

// --------------------------------------------------------- Properties Variables

    private String objectName = null;
    private String pathContext = null;
    private String labelPathContext = null;
    private boolean deployed = false;

// --------------------------------------------------------- Constructors

    public WebAppItem() {
    }

    public WebAppItem(ObjectName p_ObjectName, String defaultHost) {
        setObjectName(p_ObjectName.toString());
        setName(p_ObjectName.getKeyProperty("name"));
        setPathContext(extractPathContext(getName()));
        setLabelPathContext(extractLabelPathContext(getName(), defaultHost));
    }
    /**
     * Used for Jetty containers only
     * @param p_PathContext
     * @param p_ObjectName
     */
    public WebAppItem(String p_PathContext, String p_ObjectName) {
        setPathContext(p_PathContext);
        setObjectName(p_ObjectName);
        setLabelPathContext(extractLabelPathContext(getPathContext(), null));
    }

// --------------------------------------------------------- Public static Methods

    public static String extractLabelPathContext(String p_Name, String defaultHost) {
        String s = p_Name;
        if ((s != null) && (s.length() > 0)) {
            if (defaultHost == null) {
                // Old way - delete HOST
                s = extractPathContext(p_Name);
                // Delete '/'
                if (s.charAt(0) == '/') {
                    s = s.substring(1);
                    if (s.length() == 0) {
                        s = LABEL_ROOT_WEBMODULE;
                    }
                }
            } else {
                // Extract HOST and use it to construct label if this is not
                // the default host (localhost in default configuration)
                int iPos = s.indexOf("//");
                if (iPos > -1) {
                    s = s.substring(iPos + 2);
                    iPos = s.indexOf("/");
                    if (iPos > -1) {
                        String host = s.substring(0, iPos);
                        s = s.substring(iPos);
                        if (s.charAt(0) == '/') {
                            s = s.substring(1);
                            if (s.length() == 0) {
                                if (defaultHost.equals(host)) {
                                    s = LABEL_ROOT_WEBMODULE;
                                } else {
                                    s = host + APPEND_CHAR + LABEL_ROOT_WEBMODULE;
                                }
                            }
                        }
                    }
                }
            }
        }
        return s;
    }

    public static String extractPathContext(String p_Name) {
        String s = p_Name;
        // Delete //HOST
        int iPos = s.indexOf("//");
        if (iPos > -1) {
            s = s.substring(iPos + 2);
            iPos = s.indexOf("/");
            if (iPos > -1) {
                s = s.substring(iPos);
            }
        }
        return s;
    }

// --------------------------------------------------------- Properties Methods

    public String getPathContext() {
        return pathContext;
    }

    public void setPathContext(String pathContext) {
        this.pathContext = pathContext;
    }

    public boolean isDeployed() {
        return deployed;
    }

    public String getLabelPathContext() {
        return labelPathContext;
    }

    /**
     * Extend parent method to set the property <code>deployed</code>.
     *
     * @param p_Path The complete path (docBase) of the web application
     */
    public void setPath(String p_Path) {
        super.setPath(p_Path);
        if (getPath() != null) {
            deployed = true;
        }
    }

    public void setLabelPathContext(String labelPathContext) {
        this.labelPathContext = labelPathContext;
    }
}