/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2003-2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.logging;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author Michel-Ange ANTON
 * @author Adriana Danes
 */

public class CatalinaValveForm extends ActionForm {

// ----------------------------------------------------- Properties Variables

    /**
     *
     */
    private String action = "edit";
    /**
     *
     */
    private boolean save = false;
    /**
     *
     */
    private String objectName = null;
    /**
     *
     */
    private String valveName = null;
    /**
     *
     */
    private String valveType = null;
    /**
     * Type of container in which the valve is defined
     */
    private String containerType = null;
    /**
     * Possible container types
     */
    private String[] containerTypes = null;
    /**
     * Name of container in which the valve is defined
     */
    private String containerName = null;
    /**
     * Possible container names
     */
    private String[] containerNames = null;
	/**
	 * ObjectName of container in which the valve is defined
	 * when the container is a Web application (Catalina context)
	 */
	private String containerObjectName = null;

// --------------------------------------------------------- Public Methods

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        action = "edit";
        save = false;
    }

// ------------------------------------------------------------- Properties Methods

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getObjectName() {
        return this.objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getValveType() {
        return this.valveType;
    }

    public void setValveType(String valveType) {
        this.valveType = valveType;
    }

    public boolean isSave() {
        return save;
    }

    public void setSave(boolean save) {
        this.save = save;
    }

    /**
     * @return Returns the containerType.
     */
    public String getContainerType() {
        return containerType;
    }

    /**
     * @param containerType The containerType to set.
     */
    public void setContainerType(String containerType) {
        this.containerType = containerType;
    }

    /**
     * @return Returns the containerName.
     */
    public String getContainerName() {
        return containerName;
    }

    /**
     * @param containerName The containerName to set.
     */
    public void setContainerName(String containerName) {
        this.containerName = containerName;
    }

    /**
     * @return Returns the containerTypes.
     */
    public String[] getContainerTypes() {
        return containerTypes;
    }


    /**
     * @param containerTypes The containerTypes to set.
     */
    public void setContainerTypes(String[] containerTypes) {
        this.containerTypes = containerTypes;
    }

    /**
     * @return Returns the containerNames.
     */
    public String[] getContainerNames() {
        return containerNames;
    }


    /**
     * @param containerTypes The containerTypes to set.
     */
    public void setContainerNames(String[] containerNames) {
        this.containerNames = containerNames;
    }

	/**
	 * @return Returns the containerObjectName.
	 */
	public String getContainerObjectName() {
		return containerObjectName;
	}


	/**
	 * @param containerObjectName The containerObjectName to set.
	 */
	public void setContainerObjectName(String containerObjectName) {
		this.containerObjectName = containerObjectName;
	}


}
