/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.mail;

import java.io.IOException;
import java.util.Properties;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.management.javaee.J2eeObjectName;
import org.ow2.jonas.mail.MailService;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Adriana Danes
 */

public class ApplyMimePartMailFactoryAction extends JonasBaseAction {

// --------------------------------------------------------- Public Methods

    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws IOException, ServletException {

        // Form used
        MailFactoryForm oForm = (MailFactoryForm) p_Form;
        try {
            // process form ....
            // Mail factory name
            String mfName = oForm.getMailFactoryName();
            // Object name used corresponding to the Mail factory MBean
            String domainName = m_WhereAreYou.getCurrentDomainName();
            String serverName = m_WhereAreYou.getCurrentJonasServerName();
            ObjectName oObjectName = J2eeObjectName.JavaMailResource(domainName, mfName, serverName,
                                                                     MailService.MIMEPART_PROPERTY_TYPE);

            String jndiName = oForm.getJndiName();
            String currentJndiName = getStringAttribute(oObjectName, "Name");
            if (!jndiName.equals(currentJndiName)) {
                // Set new JNDI name
                JonasManagementRepr.setAttribute(oObjectName, "Name", jndiName, serverName);
            }

            // apply session properties
            String sSessionProps = oForm.getSessionProps();
            Properties pSessionProps = getPropsFromString(sSessionProps);
            String aName = null;
            String aValue = null;
            JonasManagementRepr.setAttribute(oObjectName, "SessionProperties", pSessionProps, serverName);

            // apply authentication properties if changed
            String username = oForm.getUsername();
            String password = oForm.getPassword();
            Properties authProps = new Properties();
            authProps.setProperty("mail.authentication.username", username);
            authProps.setProperty("mail.authentication.password", password);
            JonasManagementRepr.setAttribute(oObjectName, "AuthenticationProperties", authProps, serverName);

            // apply specific properties for mime part datasource
            String to = oForm.getTo();
            String subject = oForm.getSubject();
            String cc = oForm.getCc();
            String bcc = oForm.getBcc();
            Properties mimeProps = new Properties();
            mimeProps.setProperty("mail.to", to);
            mimeProps.setProperty("mail.subject", subject);
            mimeProps.setProperty("mail.cc", cc);
            mimeProps.setProperty("mail.bcc", bcc);
            JonasManagementRepr.setAttribute(oObjectName, "MimeMessageProperties", mimeProps, serverName);

            if (oForm.getAction().equals("save")) {
                JonasManagementRepr.invoke(oObjectName, "saveConfig", null, null, serverName);
            }
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        // Forward to the jsp and add the parameter 'name' with the good value.
        return new ActionForward(p_Mapping.findForward("ActionEditMimePartMailFactory").getPath()
            + "?name=" + oForm.getMailFactoryName());
    }

}
