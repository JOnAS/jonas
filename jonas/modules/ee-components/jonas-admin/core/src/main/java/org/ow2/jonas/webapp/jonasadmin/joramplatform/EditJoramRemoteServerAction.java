/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 */

package org.ow2.jonas.webapp.jonasadmin.joramplatform;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * @author Frederic MAISTRE
 */

public class EditJoramRemoteServerAction extends EditJoramBaseAction {

// --------------------------------------------------------- Public Methods

    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws IOException, ServletException {

        String id = p_Request.getParameter("id");
        if (id == null) {
            id = (String) m_Session.getAttribute("remoteServerId");
        }

        // Force the node selected in tree
        m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_DOMAIN) + WhereAreYou.NODE_SEPARATOR
                + "joramplatform" + WhereAreYou.NODE_SEPARATOR
                + "joramremoteserver" + id, true);

        // Form used
        JoramRemoteServerForm o_Form = (JoramRemoteServerForm) p_Form;

        try {
            // Object name used
            //ObjectName oObjectName = new ObjectName("joram:type=JMSremoteServer,id=" + id);
            o_Form.setServerId(id);
            
            m_Session.setAttribute("remoteServerId", id);
        } catch (Throwable t) {
            return (treatError(t, p_Mapping, p_Request));
        }

        // Forward to the jsp.
        return (p_Mapping.findForward("JoramRemoteServer"));
    }

}
