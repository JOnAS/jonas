package org.ow2.jonas.webapp.jonasadmin.monitoring;

import org.apache.struts.action.ActionForm;

public class EjbHaMemberForm extends ActionForm {

    /**
     * The member's state
     */
    private String state = null;
    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }

    /**
     * The member's name
     */
    private String name = null;
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

}
