/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.joramplatform;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.util.JoramObjectName;
import org.ow2.jonas.webapp.jonasadmin.deploy.BaseDeployAction;



/**
 * @author Adriana Danes
 * Creation of new destinations
 */

public class ApplyDestinationAction extends BaseDeployAction {

// --------------------------------------------------------- Public Methods

    @Override
    public ActionForward executeAction(final ActionMapping p_Mapping, final ActionForm p_Form
        , final HttpServletRequest p_Request, final HttpServletResponse p_Response)
        throws IOException, ServletException {

        // Current JOnAS server
        String jonasServerName =  m_WhereAreYou.getCurrentJonasServerName();

        // Form used
        JoramDestinationForm oForm = (JoramDestinationForm) p_Form;
        String idString = (String) m_Session.getAttribute("currentId");
        Short idShort = (new Short(idString));

        try {
            ObjectName oObjectName = null;
            ObjectName joramAdapterON = JoramObjectName.joramAdapter();
            if (JonasManagementRepr.isRegistered(joramAdapterON, jonasServerName)) {
                oObjectName = joramAdapterON;
            } else {
                // TODO
            }

            Object[] asParam = {
                    idShort, oForm.getName()
            };
            String[] asSignature = {
                    "short", "java.lang.String"
            };
            String type = oForm.getType();
            if (type.equals(m_Resources.getMessage("label.joramplatform.destinations.queue"))) {
                JonasManagementRepr.invoke(oObjectName, "createQueue", asParam, asSignature, jonasServerName);
            } else if (type.equals(m_Resources.getMessage("label.joramplatform.destinations.topic"))) {
                JonasManagementRepr.invoke(oObjectName, "createTopic", asParam, asSignature, jonasServerName);
            }
            refreshJoramTree(p_Request, idString);

            oForm.reset(p_Mapping, p_Request);

        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }

        // Forward to the jsp.
        return (p_Mapping.findForward("Create Joram Destination"));
    }
}
