/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.container;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.ow2.jonas.webapp.jonasadmin.Jlists;


/**
 * @author Michel-Ange ANTON
 */
public class WebAppForm extends ActionForm {

// --------------------------------------------------------- Properties variables

    private String action = "edit";
    private boolean save = false;
    private String pathContext = null;
    private String labelPathContext = null;
    private List booleanValues = Jlists.getBooleanValues();
    private String objectName = null;
    private String j2eeApplication = null;
    private String j2eeServer = null;
    private String name = null;

// --------------------------------------------------------- Public Methods

    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */

    @Override
    public void reset(final ActionMapping mapping, final HttpServletRequest request) {
        action = "edit";
        save = false;
    }

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     * @return List of errors
     */
    @Override
    public ActionErrors validate(final ActionMapping mapping, final HttpServletRequest request) {
        ActionErrors oErrors = new ActionErrors();
        // Path context
        if (getPathContext().length() == 0) {
            oErrors.add("context", new ActionMessage("error.webapp.context.required"));
        }
        return oErrors;
    }

// --------------------------------------------------------- Properties Methods

    public String getPathContext() {
        return pathContext;
    }

    public void setPathContext(final String pathContext) {
        this.pathContext = pathContext;
        if (pathContext.length() > 0) {
            if (pathContext.charAt(0) != '/') {
                this.pathContext = "/" + pathContext;
            }
        }
    }

    public String getAction() {
        return action;
    }

    public void setAction(final String action) {
        this.action = action;
    }

    public List getBooleanValues() {
        return booleanValues;
    }

    public String getLabelPathContext() {
        return labelPathContext;
    }

    public void setLabelPathContext(final String labelPathContext) {
        this.labelPathContext = labelPathContext;
    }

    public boolean isSave() {
        return save;
    }

    public void setSave(final boolean save) {
        this.save = save;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(final String objectName) {
        this.objectName = objectName;
    }

    public String getJ2eeApplication() {
        return j2eeApplication;
    }

    public void setJ2eeApplication(final String j2eeApplication) {
        this.j2eeApplication = j2eeApplication;
    }

    public String getJ2eeServer() {
        return j2eeServer;
    }

    public void setJ2eeServer(final String j2eeServer) {
        this.j2eeServer = j2eeServer;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
   }

}