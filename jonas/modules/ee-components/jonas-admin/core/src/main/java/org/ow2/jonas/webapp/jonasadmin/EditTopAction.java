/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.util.Log;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * Test <code>Action</code> that handles events from the tree control test
 * page.
 * @author Michel-Ange Anton (initial developer)<br>
 * @author Florent Benoit (changes for struts 1.2.2)
 */

public class EditTopAction extends JonasBaseAction {

// --------------------------------------------------------- Public Methods

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param actionForm The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */
    public ActionForward executeAction(ActionMapping mapping, ActionForm form, HttpServletRequest request
        , HttpServletResponse response)
        throws IOException, ServletException {

        // Test if force to reload all the frames
        ActionErrors oActionErrors = (ActionErrors) request.getAttribute(Globals.ERROR_KEY);
        if (oActionErrors != null) {
            request.setAttribute("errorServerSelect", new Boolean(true));
        } else {
            String sFirstCall = request.getParameter("firstCall");
            if (sFirstCall == null) {
                request.setAttribute("reloadAll", new Boolean(true));
            }
        }

        // Form used
        TopForm topFm = new TopForm();
        request.setAttribute("topForm", topFm);
        try {
            String serverName = m_WhereAreYou.getCurrentJonasServerName();
            topFm.setServerSelect(serverName);
        } catch (Throwable t) {
            String message = "Problem when accessing to JOnAS ! : " + t.getMessage();
            //getServlet().log(message, t);
            Logger logger = Log.getLogger(Log.JONAS_ADMIN_PREFIX);
            if (logger.isLoggable(BasicLevel.DEBUG)) {
                logger.log(BasicLevel.DEBUG, message);
            }
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, message);
            return (null);
        }

        // Forward to the jsp.
        return (mapping.findForward("Top"));
    }

}
