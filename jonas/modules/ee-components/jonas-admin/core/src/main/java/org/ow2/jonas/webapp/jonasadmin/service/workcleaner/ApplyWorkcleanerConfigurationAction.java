/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.webapp.jonasadmin.service.workcleaner;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;

/**
 * Implement action for WC service reconfiguration.
 * @author Adriana Danes
 */
public class ApplyWorkcleanerConfigurationAction extends JonasBaseAction {

    @Override
    public ActionForward executeAction(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request,
            final HttpServletResponse response) throws IOException, ServletException {

        String serverName = m_WhereAreYou.getCurrentJonasServerName();
        String domainName = m_WhereAreYou.getCurrentDomainName();
        ObjectName serviceOn = JonasObjectName.workCleaner(domainName);
        WorkcleanerServiceForm oForm = (WorkcleanerServiceForm) form;
        String sAction = oForm.getAction();
        int period = oForm.getPeriod();
        if ("apply".equals(sAction)) {
            // Configure period
            setIntegerAttribute(serviceOn, "executePeriod", period);
        } else if ("execute".equals(sAction)) {
            // Execute clean tasks
            String operation = "executeTasks";
            JonasManagementRepr.invoke(serviceOn, operation, null, null, serverName);
        } else if ("save".equals(sAction)) {
            JonasManagementRepr.invoke(serviceOn, "saveConfig", null, null, serverName);
        }
        // Forward to the jsp.
        return (mapping.findForward("Workcleaner Service"));
    }

}
