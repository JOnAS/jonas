/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2003-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.jtm;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * Form used to present the Transaction Manager properties
 * @author Adriana Danes
 */
public final class JtmServiceForm extends ActionForm {

// ------------------------------------------------------------- Properties Variables

    private String action = null;
    private String timeOutText = null;
    private String jtmHost = null;
    private String jtmLocation = null;
    private String jtmPort = null;

// ------------------------------------------------------------- Properties Methods

    public String getTimeOutText() {
        return timeOutText;
    }
    public void setTimeOutText(String timeOutText) {
        this.timeOutText = timeOutText;
    }
    public String getJtmHost() {
        return jtmHost;
    }
    public void setJtmHost(String jtmHost) {
        this.jtmHost = jtmHost;
    }
    public String getJtmLocation() {
        return jtmLocation;
    }
    public void setJtmLocation(String jtmLocation) {
        this.jtmLocation = jtmLocation;
    }
    public String getJtmPort() {
        return jtmPort;
    }
    public void setJtmPort(String jtmPort) {
        this.jtmPort = jtmPort;
    }
    public String getAction() {
        return action;
    }
    public void setAction(String action) {
        this.action = action;
    }
// ------------------------------------------------------------- Public Methods

    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        action = "apply";
    }

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors oErrors = new ActionErrors();
        numberCheck(oErrors, "timeOut", timeOutText, false, 0, 0);
        return oErrors;
    }

    /**
     * Helper method to check that it is a required number and
     * is a valid integer within the given range. (min, max).
     *
     * @param  field  The field name in the form for which this error occured.
     * @param  numText  The string representation of the number.
     * @param rangeCheck  Boolean value set to true of reange check should be performed.
     * @param  min  The lower limit of the range
     * @param  max  The upper limit of the range
     */
    public void numberCheck(ActionErrors p_Errors, String field, String numText, boolean rangeCheck
        , int min, int max) {
        // Check for 'is required'
        if ((numText == null) || (numText.length() < 1)) {
            p_Errors.add(field, new ActionMessage("error.jtm." + field + ".required"));
        } else {
            // check for 'must be a number' in the 'valid range'
            try {
                int num = Integer.parseInt(numText);
                // perform range check only if required
                if (rangeCheck) {
                    if ((num < min) || (num > max)) {
                        p_Errors.add(field
                            , new ActionMessage("error.jtm." + field + ".range"));
                    }
                }
            } catch (NumberFormatException e) {
                p_Errors.add(field, new ActionMessage("error.jtm." + field + ".format"));
            }
        }
    }

}
