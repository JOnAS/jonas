/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.joramplatform;

import java.util.ArrayList;

import javax.management.ObjectName;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.util.JoramObjectName;
import org.ow2.jonas.webapp.jonasadmin.deploy.BaseDeployAction;


/**
 * @author Adriana Danes
 */
public class RemoveConfirmDestinationsAction extends BaseDeployAction {

    @Override
    public ActionForward executeAction(final ActionMapping pMapping, final ActionForm pForm
        , final HttpServletRequest pRequest, final HttpServletResponse pResponse) {

        // Current JOnAS server
        String jonasServerName =  m_WhereAreYou.getCurrentJonasServerName();

        // Form used
        RemoveDestinationsForm oForm = (RemoveDestinationsForm) m_Session.getAttribute("removeDestinationsForm");
        // Array of ItemDestination
        ArrayList destinationsArray = (ArrayList) m_Session.getAttribute("destinations");

        String currentId = (String) m_Session.getAttribute("currentId"); // Joram server currently being managed
        try {
            ObjectName joramAdapterOn = JoramObjectName.joramAdapter();

            String destinationName = null;
            ObjectName destinationObjectName = null;
            for (int i = 0; i < oForm.getSelectedItems().length; i++) {
                destinationName = oForm.getSelectedItems()[i];
                destinationObjectName = getDestinationOn(destinationName, destinationsArray, jonasServerName);
                if (destinationObjectName != null) {
                        JonasManagementRepr.invoke(destinationObjectName, "delete", null, null, jonasServerName);
                        Object[] asParam = {
                                destinationName
                        };
                        String[] asSignature = {
                                "java.lang.String"
                        };
                        JonasManagementRepr.invoke(joramAdapterOn, "removeDestination", asParam, asSignature, jonasServerName);
                }
            }
            // refresh tree
            refreshJoramTree(pRequest, currentId);

        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(pRequest, m_Errors);
            return (pMapping.findForward("Global Error"));
        }

        // Forward to action
        return (pMapping.findForward("ActionEditJoramServer"));
    }

    private ObjectName getDestinationOn(final String destinationName, final ArrayList destinationItems, final String jonasServerName) {
        for (int i = 0; i < destinationItems.size(); i++) {
            ItemDestination item = (ItemDestination) destinationItems.get(i);
            ObjectName itemOn = item.getOn();
            if (itemOn != null) {
                String itemDestNameProp = itemOn.getKeyProperty("name");
                // compare with destinationName
                if (itemDestNameProp.startsWith(destinationName)) {
                    String itemDestName = (String) JonasManagementRepr.getAttribute(itemOn, "AdminName", jonasServerName);
                    if (destinationName.equals(itemDestName)) {
                        return itemOn;
                    }
                }
            }
        }
        return null;
    }
}
