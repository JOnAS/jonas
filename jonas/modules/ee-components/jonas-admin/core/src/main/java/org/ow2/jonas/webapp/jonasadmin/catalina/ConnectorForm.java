/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2003-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.catalina;

import java.net.InetAddress;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * Form bean for the connector page.
 *
 * @author Michel-Ange ANTON
 * @author Adriana Danes
 * 	- update to Tomact 5.0
 *  - update to Tomact 5.5 (04/2005)
 */

public final class ConnectorForm extends ActionForm {

    // ----------------------------------------------------- Instance Variables

    /**
     * The administrative action represented by this form.
     */
    private String action = "edit";

    /**
     * The administrative sub-action state.
     */
    private boolean save = false;

    /**
     * The object name of the Connector this bean refers to.
     */
    private String objectName = null;

    /**
     * The name of the service this connector belongs to.
     */
    private String serviceName = null;
    /**
     * The text for choosing the connector type at creation
     */
    private String connectorType = null;
    /**
     * Generic max value used by the numberCheck method
     */
    private int MAX_VALUE = Integer.MAX_VALUE;

    // Common Connector attributes
    // ---------------------------
    /**
     * Used to enable/disable the TRACE HTTP method
     */
    private boolean allowTrace = false;
    /**
     * The 'empty session path' flag for this Connector
     */
    private boolean emptySessionPath = false;
    /**
     * The 'enable DNS lookups' flag for this Connector
     */
    private boolean enableLookups = true;
    /**
     * Maximum size in bytes of a POST which will be handled by the servlet API provided features
     */
    private String maxPostSizeText = "2097152";
    /**
     * Coyote protocol handler in use
     */
    private String protocol  = "HTTP/1.1";
    /**
     * The text for the proxyName.
     */
    private String proxyName = null;
    /**
     * The text for the proxy Port Number.
     */
    private String proxyPortText = "0";
    /**
     * The text for the scheme.
     */
    private String scheme = "http";
    /**
     * Is this a secure (SSL) Connector?
     */
    private boolean secure = false;
    /**
     * Character encoding used to decode the URI
     */
    private String URIEncoding = null;
    /**
     * Should the body encoding be used for URI query parameters
     */
    private boolean useBodyEncodingForURI = false;
    /**
     * Is generation of X-Powered-By response header enabled/disabled?
     */
    private boolean xpoweredBy = false;

    // Additional attributes specific to org.apache.coyote.tomcat5.CoyoteConnector
    // ---------------------------------------------------------------------------
    /**
     * The text for the accept Count.
     */
    private String acceptCountText = "10";
    /**
     * The IP address on which to bind.
     */
    private String address = null;
    /**
     * The text for the TCP port the created socked awaits for incomming requests
     */
    private String portText = null;
    /**
     * The text for the redirect port for non-SSL to SSL redirects
     */
    private String redirectPortText = "-1";
    /**
     * Should we use TCP no delay?
     */
    private boolean tcpNoDelay = true;

    // Thread pool management
    /**
     * The text for the number of request processing threads that will be created
     * when the connector is first started
     */
    private String minSpareThreadsText = "4";
    /**
     * The text for the maximum number of unused request processing threads
     */
    private String maxSpareThreadsText = "50";
    /**
     * The text for the max Threads.
     */
    private String maxThreadsText = "200";

    // The followings are not usefull for AJP / JK connectors
    /**
     * The text for the buffer size to be provided fo input streams.
     */
    private String bufferSizeText = "2048";
    /**
     * Compression value
     */
    private String compression = "off";
    /**
     * Linger value on the incoming connection
     * Default value is -1 (socket linger is disabled).
     */
    private String connectionLingerText = "-1";
    /**
     * The text for the Timeout value on the incoming connection
     */
    private String connTimeOutText = "60000";
    /**
     * The text for the Timeout value on the incoming connection
     * during request processing
     */
    private String connectionUploadTimeoutText = null;
    /**
     * Should Tomcat ignore setting a timeout for uploads?
     */
    private boolean	disableUploadTimeout = false;
    /**
     * Text for Maximum size in bytes of the HTTP header
     */
    private String maxHttpHeaderSizeText  = "4096";
    /**
     * Text for Maximum number of Keep-Alive requests to honor per connection
     */
    private String maxKeepAliveRequestsText = "100";
    /**
     * Thread pool strategy
     */
    private String strategy = "If";
    /**
     * Text for the priority of request processing threads
     */
    private String threadPriorityText = "5";

    // The following is specific to AJP connectors only
    /**
     * The text for the output buffer size. If -1 buffering is disabled.
     */
    private String outputBufferSizeText = "-1";
    /**
     * Should Tomcat perform all authentications?
     */
    private boolean tomcatAuthentication = true;

    // The followings are dedicated to SSL support
    /**
     *  The certificate encoding algorithm to be used
     */
    private String algorithm = "SunX509";
    /**
     * Whether client authentication is supported.
     */
    private boolean clientAuth = false;
    /**
     * Pathname of the keystore file
     */
    private String keystoreFile = ".keystore";
    /**
     * The keyStore Password.
     */
    private String keystorePass = "changeit";
    /**
     * The keyStore type
     */
    private String keystoreType = "JKS";
    /**
     * SSL protocol
     */
    private String sslProtocol = "TLS";
    /**
     * list of encription ciphers
     */
    private String ciphers = null;

    // -------------------------
    /**
     * The text for the connectorName.
     */
    private String connectorName = null;

    /**
     * Represent boolean (true, false) values for enableLookups etc.
     */
    private List booleanVals = null;

    /**
     * Represent supported connector types.
     */
    private List connectorTypeVals = null;

    // --------------------------------------------------------- Public Methods
    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     * @return the ActionErrors
     */
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

        ActionErrors oErrors = new ActionErrors();

        // general
        numberCheck(oErrors, "acceptCountText", acceptCountText, true, 0, MAX_VALUE);
        numberCheck(oErrors, "connTimeOutText", connTimeOutText, true, 0, MAX_VALUE);
        numberCheck(oErrors, "bufferSizeText", bufferSizeText, true, 0, MAX_VALUE);
        numberCheck(oErrors, "outputBufferSizeText", outputBufferSizeText, true, -1, MAX_VALUE);

        // The IP address can also be null --
        // which means open the server socket on *all* IP addresses for this host
        if (address.length() > 0) {
            try {
                InetAddress.getByName(address);
            } catch (Exception e) {
                oErrors.add("address", new ActionMessage("error.catalina.connector.address.invalid"));
            }
        } else {
            address = null;
        }

        // ports
        numberCheck(oErrors, "portNumber", portText, true, 1, MAX_VALUE);
        numberCheck(oErrors, "redirectPortText", redirectPortText, true, -1, MAX_VALUE);

        numberCheck(oErrors, "maxThreadsText", maxThreadsText, true, 1, MAX_VALUE);
        numberCheck(oErrors, "maxSpareThreadsText", maxSpareThreadsText, true, 1, MAX_VALUE);
        numberCheck(oErrors, "minSpareThreadsText", minSpareThreadsText, true, 1, MAX_VALUE);
        try {
            // if min is a valid integer, then check that max >= min
            int min = Integer.parseInt(minSpareThreadsText);
            numberCheck(oErrors, "maxThreadsText", maxThreadsText, true, min, MAX_VALUE);
        } catch (Exception e) {
        }

        // proxy
        if ((proxyName != null) && (proxyName.length() > 0)) {
            try {
                /*
                 * The proxyName is the server name to be returned for calls to
                 * request.getServerName()
                 * This name is the value of the part before ":" in the Host  header value, if any,
                 * or the resolved server name, or the server IP address.
                InetAddress.getByName(proxyName);
                */
            } catch (Exception e) {
                oErrors.add("proxyName"
                    , new ActionMessage("error.catalina.connector.proxyName.invalid"));
            }
        }

        // supported only by Coyote HTTP and HTTPS connectors
        if (!("AJP".equalsIgnoreCase(connectorType))) {
            numberCheck(oErrors, "proxyPortText", proxyPortText, true, 0, MAX_VALUE);
        }

        return oErrors;
    }

    /**
     * Helper method to check that it is a required number and
     * is a valid integer within the given range. (min, max).
     *
     * @param  field  The field name in the form for which this error occured.
     * @param  numText  The string representation of the number.
     * @param rangeCheck  Boolean value set to true of reange check should be performed.
     *
     * @param  min  The lower limit of the range
     * @param  max  The upper limit of the range
     * @param  pErrors the ActionErrors
     */
    public void numberCheck(ActionErrors pErrors, String field, String numText, boolean rangeCheck
        , int min, int max) {
        // Check for 'is required'
        if ((numText == null) || (numText.length() < 1)) {
            pErrors.add(field, new ActionMessage("error.catalina.connector." + field + ".required"));
        } else {

            // check for 'must be a number' in the 'valid range'
            try {
                int num = Integer.parseInt(numText);
                // perform range check only if required
                if (rangeCheck) {
                    if ((num < min) || (num > max)) {
                        pErrors.add(field
                            , new ActionMessage("error.catalina.connector." + field + ".range"));
                    }
                }
            } catch (NumberFormatException e) {
                pErrors.add(field, new ActionMessage("error.catalina.connector." + field + ".format"));
            }
        }
    }

// ------------------------------------------------------------- Properties methods

    /**
     * Return the administrative action represented by this form.
     * @return the administrative action
     */
    public String getAction() {
        return this.action;
    }

    /**
     * Set the administrative action represented by this form.
     * @param action the action to set
     */
    public void setAction(String action) {
        this.action = action;
    }

    /**
     * Return the object name of the Connector this bean refers to.
     * @return the object name of the Connector MBean
     */
    public String getObjectName() {
        return this.objectName;
    }

    /**
     * Set the object name of the Connector this bean refers to.
     * @param objectName object name of the Connector MBean
     */
    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    /**
     * Return the object name of the service this connector belongs to.
     * @return the object name of the service this connector belongs to
     */
    public String getServiceName() {
        return this.serviceName;
    }

    /**
     * Set the object name of the Service this connector belongs to.
     * @param serviceName the object name of the service this connector belongs to
     */
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    /**
     * Return the Scheme.
     * @return the scheme
     */
    public String getScheme() {
        return this.scheme;
    }
    /**
     * Set the Scheme.
     * @param scheme the scheme to set
     */
    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    /**
     * Return the Connector type.
     * @return the Connector type
     */
    public String getConnectorType() {
        return this.connectorType;
    }

    /**
     * Set the Connector type.
     * @param connectorType the Connector type to set
     */
    public void setConnectorType(String connectorType) {
        this.connectorType = connectorType;
    }

    /**
     * Return the acceptCountText.
     * @return the acceptCount
     */
    public String getAcceptCountText() {
        return this.acceptCountText;
    }

    /**
     * Set the acceptCountText.
     * @param acceptCountText the acceptCount text
     */
    public void setAcceptCountText(String acceptCountText) {
        this.acceptCountText = acceptCountText;
    }

    /**
     * Return the connTimeOutText.
     * @return the connTimeOut
     */
    public String getConnTimeOutText() {
        return this.connTimeOutText;
    }

    /**
     * Set the connTimeOutText.
     * @param connTimeOutText the connTimeOut text
     */
    public void setConnTimeOutText(String connTimeOutText) {
        this.connTimeOutText = connTimeOutText;
    }

    /**
     * Return the bufferSizeText.
     * @return the bufferSize
     */
    public String getBufferSizeText() {
        return this.bufferSizeText;
    }

    /**
     * Set the bufferSizeText.
     * @param bufferSizeText the bufferSize to set
     */
    public void setBufferSizeText(String bufferSizeText) {
        this.bufferSizeText = bufferSizeText;
    }

    /**
     * Return the address.
     * @return the address
     */
    public String getAddress() {
        return this.address;
    }

    /**
     * Set the address.
     * @param address the address to set
     */
    public void setAddress(String address) {
        if (address != null && address.startsWith("/")) {
            address = address.substring(1);
        }
        this.address = address;
    }

    /**
     * Return the proxy Name.
     * @return the proxy name
     */
    public String getProxyName() {
        return this.proxyName;
    }

    /**
     * Set the proxy Name.
     * @param proxyName the proxy name to set
     */
    public void setProxyName(String proxyName) {
        this.proxyName = proxyName;
    }

    /**
     * Return the proxy Port NumberText.
     * @return the proxy port number
     */
    public String getProxyPortText() {
        return this.proxyPortText;
    }

    /**
     * Set the proxy Port NumberText.
     * @param proxyPortText the proxy port number to set
     */
    public void setProxyPortText(String proxyPortText) {
        this.proxyPortText = proxyPortText;
    }

    /**
     * Return the true/false value of client authentication.
     * @return the value of client authentication
     */
    public boolean isClientAuth() {
        return this.clientAuth;
    }

    /**
     * Set whether client authentication is supported or not.
     * @param clientAuthentication the client authentication to set or not
     */
    public void setClientAuth(boolean clientAuthentication) {
        this.clientAuth = clientAuthentication;
    }

    /**
     * @return the keyStore file name
     */
    public String getKeystoreFile() {
        return this.keystoreFile;
    }

    /**
     * @param keyStoreFileName the keyStore file name to set
     */
    public void setKeystoreFile(String keyStoreFileName) {
        this.keystoreFile = keyStoreFileName;
    }

    /**
     * @return the keyStore password
     */
    public String getKeystorePass() {
        return this.keystorePass;
    }

    /**
     * @param keyStorePassword the keyStore password to set
     */
    public void setKeystorePass(String keyStorePassword) {
        this.keystorePass = keyStorePassword;
    }

    /**
     * Return the Enable lookup Text.
     * @return true if loookups enabled
     */
    public boolean isEnableLookups() {
        return this.enableLookups;
    }

    /**
     * Set the Enable Lookup Text.
     * @param enableLookups Enable Lookup Text
     */
    public void setEnableLookups(boolean enableLookups) {
        this.enableLookups = enableLookups;
    }

    /**
     * Return the booleanVals.
     */
    public List getBooleanVals() {
        return this.booleanVals;
    }

    /**
     * Set the debugVals.
     */
    public void setBooleanVals(List booleanVals) {
        this.booleanVals = booleanVals;
    }

    /**
     * Return the min SpareThreads Text.
     * @return minSpareThreads value
     */
    public String getMinSpareThreadsText() {
        return this.minSpareThreadsText;
    }

    /**
     * Set the minSpareThreads Text.
     * @param minSpareThreadsText minSpareThreads value to set
     */
    public void setMinSpareThreadsText(String minSpareThreadsText) {
        this.minSpareThreadsText = minSpareThreadsText;
    }

    /**
     * Return the max SpareThreads Text.
     * @return maxSpareThreads value
     */
    public String getMaxSpareThreadsText() {
        return this.maxSpareThreadsText;
    }

    /**
     * Set the Max SpareThreads Text.
     * @param maxSpareThreadsText maxSpareThreads value to set
     */
    public void setMaxSpareThreadsText(String maxSpareThreadsText) {
        this.maxSpareThreadsText = maxSpareThreadsText;
    }

    /**
     * Return the max SpareThreads Text.
     * @return maxThreads value
     */
    public String getMaxThreadsText() {
        return this.maxThreadsText;
    }

    /**
     * Set the Max SpareThreads Text.
     * @param maxThreadsText maxSpareThreads value to set
     */
    public void setMaxThreadsText(String maxThreadsText) {
        this.maxThreadsText = maxThreadsText;
    }

    /**
     * Return the port text.
     * @return the port number
     */
    public String getPortText() {
        return this.portText;
    }

    /**
     * Set the port Text.
     * @param portText port to set
     */
    public void setPortText(String portText) {
        this.portText = portText;
    }

    /**
     * Return the port.
     * @return the redirect port number
     */
    public String getRedirectPortText() {
        return this.redirectPortText;
    }

    /**
     * Set the Redirect Port Text.
     * @param redirectPortText the redirect port to set
     */
    public void setRedirectPortText(String redirectPortText) {
        this.redirectPortText = redirectPortText;
    }

    /**
     * @return the connector name
     */
    public String getConnectorName() {
        return this.connectorName;
    }

    /**
     * @param connectorName connector name to set
     */
    public void setConnectorName(String connectorName) {
        this.connectorName = connectorName;
    }

    /**
     * Return the connectorTypeVals.
     * @return the connector types
     */
    public List getConnectorTypeVals() {
        return this.connectorTypeVals;
    }

    /**
     * Set the connectorTypeVals.
     * @param connectorTypeVals the connector types
     */
    public void setConnectorTypeVals(List connectorTypeVals) {
        this.connectorTypeVals = connectorTypeVals;
    }
    /**
     * @return the labels
     */
    public String getLabel() {
        StringBuffer sb = new StringBuffer(getPortText());
        if (getAddress() != null) {
            sb.append(" (");
            sb.append(getAddress());
            sb.append(")");
        }
        return sb.toString();
    }

    /**
     *
     * @return true if save action
     */
    public boolean isSave() {
        return save;
    }

    /**
     *
     * @param save set the save action
     */
    public void setSave(boolean save) {
        this.save = save;
    }

    /**
     * @return Returns the allowTrace.
     */
    public boolean isAllowTrace() {
        return allowTrace;
    }
    /**
     * @param allowTrace The allowTrace to set.
     */
    public void setAllowTrace(boolean allowTrace) {
        this.allowTrace = allowTrace;
    }

    /**
     * @return Returns the emptySessionPath.
     */
    public boolean isEmptySessionPath() {
        return emptySessionPath;
    }
    /**
     * @param emptySessionPath The emptySessionPath to set.
     */
    public void setEmptySessionPath(boolean emptySessionPath) {
        this.emptySessionPath = emptySessionPath;
    }

    /**
     * @return Returns the maxPostSize.
     */
    public String getMaxPostSizeText() {
        return maxPostSizeText;
    }


    /**
     * @param maxPostSize The maxPostSize to set.
     */
    public void setMaxPostSizeText(String maxPostSizeText) {
        this.maxPostSizeText = maxPostSizeText;
    }

    /**
     * @return Returns the protocol.
     */
    public String getProtocol() {
        return protocol;
    }

    /**
     * @return Returns the secure.
     */
    public boolean isSecure() {
        return secure;
    }


    /**
     * @param secure The secure to set.
     */
    public void setSecure(boolean secure) {
        this.secure = secure;
    }

    /**
     * @return Returns the uRIEncoding.
     */
    public String getURIEncoding() {
        return URIEncoding;
    }


    /**
     * @param encoding The uRIEncoding to set.
     */
    public void setURIEncoding(String encoding) {
        URIEncoding = encoding;
    }

    /**
     * @return Returns the useBodyEncodingForURI.
     */
    public boolean isUseBodyEncodingForURI() {
        return useBodyEncodingForURI;
    }


    /**
     * @param useBodyEncodingForURI The useBodyEncodingForURI to set.
     */
    public void setUseBodyEncodingForURI(boolean useBodyEncodingForURI) {
        this.useBodyEncodingForURI = useBodyEncodingForURI;
    }

    /**
     * @return Returns the xpoweredBy.
     */
    public boolean isXpoweredBy() {
        return xpoweredBy;
    }


    /**
     * @param xpoweredBy The xpoweredBy to set.
     */
    public void setXpoweredBy(boolean xpoweredBy) {
        this.xpoweredBy = xpoweredBy;
    }

    /**
     * @return Returns the compression.
     */
    public String getCompression() {
        return compression;
    }


    /**
     * @param compression The compression to set.
     */
    public void setCompression(String compression) {
        this.compression = compression;
    }

    /**
     * @return Returns the connectionLingerText.
     */
    public String getConnectionLingerText() {
        return connectionLingerText;
    }


    /**
     * @param connectionLingerText The connectionLingerText to set.
     */
    public void setConnectionLingerText(String connectionLingerText) {
        this.connectionLingerText = connectionLingerText;
    }

    /**
     * @return Returns the connectionUploadTimeoutText.
     */
    public String getConnectionUploadTimeoutText() {
        return connectionUploadTimeoutText;
    }


    /**
     * @param connectionUploadTimeoutText The connectionUploadTimeoutText to set.
     */
    public void setConnectionUploadTimeoutText(String connectionUploadTimeoutText) {
        this.connectionUploadTimeoutText = connectionUploadTimeoutText;
    }

    /**
     * @return Returns the disableUploadTimeout.
     */
    public boolean isDisableUploadTimeout() {
        return disableUploadTimeout;
    }


    /**
     * @param disableUploadTimeout The disableUploadTimeout to set.
     */
    public void setDisableUploadTimeout(boolean disableUploadTimeout) {
        this.disableUploadTimeout = disableUploadTimeout;
    }

    /**
     * @return Returns the maxHttpHeaderSize.
     */
    public String getMaxHttpHeaderSizeText() {
        return maxHttpHeaderSizeText;
    }


    /**
     * @param maxHttpHeaderSize The maxHttpHeaderSize to set.
     */
    public void setMaxHttpHeaderSizeText(String maxHttpHeaderSize) {
        this.maxHttpHeaderSizeText = maxHttpHeaderSize;
    }

    /**
     * @return Returns the maxKeepAliveRequests.
     */
    public String getMaxKeepAliveRequestsText() {
        return maxKeepAliveRequestsText;
    }


    /**
     * @param maxKeepAliveRequests The maxKeepAliveRequests to set.
     */
    public void setMaxKeepAliveRequestsText(String maxKeepAliveRequests) {
        this.maxKeepAliveRequestsText = maxKeepAliveRequests;
    }

    /**
     * @return Returns the strategy.
     */
    public String getStrategy() {
        return strategy;
    }


    /**
     * @param strategy The strategy to set.
     */
    public void setStrategy(String strategy) {
        this.strategy = strategy;
    }

    /**
     * @return Returns the threadPriority.
     */
    public String getThreadPriorityText() {
        return threadPriorityText;
    }


    /**
     * @param threadPriority The threadPriority to set.
     */
    public void setThreadPriorityText(String threadPriority) {
        this.threadPriorityText = threadPriority;
    }

    /**
     * @return Returns the tomcatAuthentication.
     */
    public boolean isTomcatAuthentication() {
        return tomcatAuthentication;
    }


    /**
     * @param tomcatAuthentication The tomcatAuthentication to set.
     */
    public void setTomcatAuthentication(boolean tomcatAuthentication) {
        this.tomcatAuthentication = tomcatAuthentication;
    }

    /**
     * @return Returns the algorithm.
     */
    public String getAlgorithm() {
        return algorithm;
    }


    /**
     * @param algorithm The algorithm to set.
     */
    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    /**
     * @return Returns the keystoreType.
     */
    public String getKeystoreType() {
        return keystoreType;
    }


    /**
     * @param keystoreType The keystoreType to set.
     */
    public void setKeystoreType(String keystoreType) {
        this.keystoreType = keystoreType;
    }

    /**
     * @return Returns the sslProtocol.
     */
    public String getSslProtocol() {
        return sslProtocol;
    }


    /**
     * @param sslProtocol The sslProtocol to set.
     */
    public void setSslProtocol(String sslProtocol) {
        this.sslProtocol = sslProtocol;
    }

    /**
     * @return Returns the ciphers.
     */
    public String getCiphers() {
        return ciphers;
    }


    /**
     * @param ciphers The ciphers to set.
     */
    public void setCiphers(String ciphers) {
        this.ciphers = ciphers;
    }

    /**
     * @param protocol The protocol to set.
     */
    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    /**
     * @return Returns the tcpNoDelay.
     */
    public boolean isTcpNoDelay() {
        return tcpNoDelay;
    }


    /**
     * @param tcpNoDelay The tcpNoDelay to set.
     */
    public void setTcpNoDelay(boolean tcpNoDelay) {
        this.tcpNoDelay = tcpNoDelay;
    }

    /**
     * @return Returns the outputBufferSizeText.
     */
    public String getOutputBufferSizeText() {
        return outputBufferSizeText;
    }

    /**
     * @param outputBufferSizeText The outputBufferSizeText to set.
     */
    public void setOutputBufferSizeText(String outputBufferSizeText) {
        this.outputBufferSizeText = outputBufferSizeText;
    }

}
