/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.versioning;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;
import org.ow2.jonas.webapp.taglib.LabelValueBean;

/**
 * Implement action for Versioning service management.
 * @author Adriana Danes
 */
public class EditVersioningServiceAction extends JonasBaseAction {

    @Override
    public ActionForward executeAction(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request,
            final HttpServletResponse response) throws IOException, ServletException {

        // Form used
        VersioningServiceForm oForm = (VersioningServiceForm) form;

        // Force the node selected in tree
        m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_SERVER) + WhereAreYou.NODE_SEPARATOR
            + "services" + WhereAreYou.NODE_SEPARATOR + "versioning", true);

        String domainName = m_WhereAreYou.getCurrentDomainName();
        try {
            // Object name used
            ObjectName on = JonasObjectName.versioning(domainName);
            oForm.setVersioningEnabled(getBooleanAttribute(on, "VersioningEnabled"));
            String pServer = m_WhereAreYou.getCurrentJonasServerName();
            List<LabelValueBean> policies = new ArrayList<LabelValueBean>();
            for (String policy : (String[]) JonasManagementRepr.getAttribute(on, "Policies", pServer)) {
                policies.add(new LabelValueBean(policy, policy));
            }
            oForm.setPolicyList(policies);
            oForm.setDefaultPolicy(getStringAttribute(on, "DefaultDeploymentPolicy"));
        }
        catch (Throwable t) {
            addGlobalError(t);
            saveErrors(request, m_Errors);
            return (mapping.findForward(GLOBAL_ERROR_FORWARD));
        }

        // Forward to the jsp.
        return (mapping.findForward("Versioning Service"));
    }

}
