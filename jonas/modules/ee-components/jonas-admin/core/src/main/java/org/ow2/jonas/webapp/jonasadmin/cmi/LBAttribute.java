/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

/**
 *
 */
package org.ow2.jonas.webapp.jonasadmin.cmi;
import java.io.Serializable;
/**
 * @author eyindanga
 *
 */
public class LBAttribute implements Serializable {
    /**
     * Serial UID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Attribute key
     */
    private String value;
    /**
     * Attribute value
     */
    private String key;

    /**
     * Default constructor
     */
    public LBAttribute(){

    }
    /**
     * Constructor using fields.
     * @param value
     * @param key
     */
    public LBAttribute(final String value, final String key) {
        super();
        this.value = value;
        this.key = key;
    }
    /**
     * @return
     */
    public String getValue() {
        return value;
    }
    /**
     * @param value
     */
    public void setValue(String value) {
        this.value = value;
    }
    /**
     * @return
     */
    public String getKey() {
        return key;
    }
    /**
     * @param key
     */
    public void setKey(String key) {
        this.key = key;
    }

}
