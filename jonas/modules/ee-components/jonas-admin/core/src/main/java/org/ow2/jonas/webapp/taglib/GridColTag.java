/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Michel-Ange ANTON
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.taglib;

import javax.servlet.jsp.JspException;

public class GridColTag extends GridTableBaseTag {

// ----------------------------------------------------- Properties Variables

    private boolean nowrap = false;
    private int colspan = -1;
    private int rowspan = -1;

    public boolean isNowrap() {
        return nowrap;
    }

    public void setNowrap(boolean nowrap) {
        this.nowrap = nowrap;
    }

    public int getColspan() {
        return colspan;
    }

    public void setColspan(int colspan) {
        this.colspan = colspan;
    }

    public int getRowspan() {
        return rowspan;
    }

    public void setRowspan(int rowspan) {
        this.rowspan = rowspan;
    }

// ----------------------------------------------------- Protected Methods

    /**
     * Return the HTML element.
     */
    protected String getHtmlElement() {
        return "td";
    }

    /**
     * Prepare the attributes of the HTML element
     */
    protected String prepareAttributes() throws JspException {
        StringBuffer sb = new StringBuffer();
        // Append nowrap
        if (isNowrap()) {
            sb.append(" nowrap");
        }
        // Append "colspan" parameter
        sb.append(prepareAttribute("colspan", colspan));
        // Append "rowspan" parameter
        sb.append(prepareAttribute("rowspan", rowspan));
        // Append Parent
        sb.append(super.prepareAttributes());

        return sb.toString();
    }

    protected String getDefaultBody() {
        return "&nbsp;";
    }

// ----------------------------------------------------- Public Methods

    /**
     * Release resources after Tag processing has finished.
     */
    public void release() {
        super.release();
        nowrap = false;
    }
}