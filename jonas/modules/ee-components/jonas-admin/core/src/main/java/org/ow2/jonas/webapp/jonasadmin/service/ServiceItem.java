/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service;

import org.ow2.jonas.lib.management.extensions.base.NameItem;

/**
 * @author Michel-Ange ANTON
 */
public class ServiceItem implements NameItem {

// --------------------------------------------------------- Properties Variables

    private String category = null;
    private String name = null;
    private boolean deployed = false;
    private String forward;
    private String propName = null;
    private String description = null;

// --------------------------------------------------------- Constructors

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getPropName() {
        return propName;
    }

    public void setPropName(final String propName) {
        this.propName = propName;
    }

    public ServiceItem(final String p_Name, final boolean p_Deployed, final String p_Forward) {
        setName(p_Name);
        setDeployed(p_Deployed);
        if (isDeployed()) {
            setForward(p_Forward);
        }
    }

// --------------------------------------------------------- Properties Methods

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public boolean isDeployed() {
        return deployed;
    }

    public void setDeployed(final boolean deployed) {
        this.deployed = deployed;
    }

    public String getForward() {
        return forward;
    }

    public void setForward(final String forward) {
        this.forward = forward;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(final String category) {
        this.category = category;
    }

    public boolean isContainer() {
        if ("container".equals(category)) {
            return true;
        }
        return false;
    }

    public boolean isMandatory() {
        if ("mandatory".equals(category)) {
            return true;
        }
        return false;
    }

    public boolean isWebservices() {
        if ("webservices".equals(category)) {
            return true;
        }
        return false;
    }
}
