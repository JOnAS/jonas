/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2003-2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.domain;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 * @author Adriana Danes
 */

public final class NewClusterForm extends ActionForm {

// ----------------------------------------------------- Properties Variables
	private String clusterName = null;
// --------------------------------------------------------- Public Methods

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        super.reset(mapping, request);
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors oErrors = new ActionErrors();
        if ((clusterName == null) || (clusterName.length() < 1)) {
            oErrors.add("clusterName", new ActionMessage("error.logger.domain.servername.required"));
        }
        return oErrors;
    }

//  ------------------------------------------------------------- Properties Methods

	/**
	 * @return Returns the clusterName.
	 */
	public String getClusterName() {
		return clusterName;
	}

	/**
	 * @param clusterName The clusterName to set.
	 */
	public void setClusterName(String serverName) {
		this.clusterName = serverName;
	}

}
