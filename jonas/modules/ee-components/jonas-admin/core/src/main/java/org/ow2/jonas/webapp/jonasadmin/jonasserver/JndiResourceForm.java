/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2003-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.jonasserver;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import org.ow2.jonas.webapp.jonasadmin.common.BeanComparator;

import org.apache.struts.action.ActionMapping;

/**
 * @author Michel-Ange ANTON
 */
public class JndiResourceForm extends BasicJonasServerForm {

// --------------------------------------------------------- Properties variables
    /**
     * registry protocol from carl.properties
     */
    private String registryProtocol = null;
    /**
     * Names of registered objects when protocol is not cmi
     */
    private ArrayList listNames = new ArrayList();
    /**
     * For cmi, in the list we have a 2 elements array were the 1st
     * element equal the name and the 2nd element equals the nodes list
     */
    private ArrayList listNamesAndNodes = new ArrayList();

// --------------------------------------------------------- Public Methods

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        super.reset(mapping, request);
        listNames = new ArrayList();
        listNamesAndNodes = new ArrayList();
        //v = new Vector();
    }

// --------------------------------------------------------- Properties Methods


    /**
     * @return the names of registered objects when protocol is not cmi
     */
    public ArrayList getListNames() {
        return listNames;
    }
    /**
     * @param pList the names of registered objects when protocol is not cmi
     */
    public void setListNames(List pList) {
        listNames.clear();
        listNames = new ArrayList(pList);
        Collections.sort(listNames, new BeanComparator());
    }
    /**
     * @return the registry protocol from carl.properties
     */
    public String getRegistryProtocol() {
        return registryProtocol;
    }
    /**
     * @param protocol the registry protocol from carl.properties
     */
    public void setRegistryProtocol(String protocol) {
        this.registryProtocol = protocol;
    }
    /**
     * @param pList for cmi, in the list we have a 2 elements array were the 1st
     * element equal the name and the 2nd element equals the nodes list
     */
    public void setVector(List pList) {
        listNamesAndNodes.clear();
        listNamesAndNodes = new ArrayList(pList);
        Collections.sort(listNamesAndNodes, new BeanComparator());
    }
    /**
     * @return for cmi, a list haveing 2 elements array were the 1st
     * element equals the name and the 2nd element equals the nodes list
     */
    public ArrayList getVector() {
        return this.listNamesAndNodes;
    }
}