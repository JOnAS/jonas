/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2009 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Michel-Ange ANTON
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.container;

import java.io.IOException;
import java.util.Map;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.webapp.jonasadmin.JonasAdminJmx;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;

/**
 *
 */

public class EditEjbCsStatisticAction extends JonasBaseAction {

    // --------------------------------------------------------- Public Methods

    /**
     */
    @Override
    public ActionForward executeAction(final ActionMapping p_Mapping, final ActionForm p_Form,
            final HttpServletRequest p_Request, final HttpServletResponse p_Response) throws IOException, ServletException {
        // Form used
        EjbContainersStatisticForm oForm = (EjbContainersStatisticForm) p_Form;
        // EjbContainersStatisticForm oForm = new EjbContainersStatisticForm();
        ContainerForm containerForm = (ContainerForm) m_Session.getAttribute("containerForm");
        p_Request.setAttribute("ejbContainersStatisticForm", oForm);
        String applySettingsTo = p_Request.getParameter("applySettingsTo");
        String editAllContainers = (String) m_Session.getAttribute("editAllContainers");

        String domainName = m_WhereAreYou.getCurrentDomainName();
        String serverName = m_WhereAreYou.getCurrentJonasServerName();
        try {
            /**
             * Edit statisctic for all containers ? This attribute should has
             * been set in EditContainerAction.
             */
            ObjectName oObjectName = null;
            if (containerForm != null) {
                oObjectName = containerForm.getObjectName();
            } else {
                oObjectName = ObjectName.getInstance(domainName + ":type=service,name=ejbContainers");
            }
            if (editAllContainers != null && "true".equalsIgnoreCase(editAllContainers)) {
                oObjectName = ObjectName.getInstance(domainName + ":type=service,name=ejbContainers");
            }
            Map nbs = JonasAdminJmx.getTotalEJB(domainName, serverName);
            int nbtotal = 0;
            int nb = (Integer) nbs.get("MessageDrivenBean");
            nbtotal = nbtotal + nb;
            oForm.setTotalCurrentNumberOfMDBType(nb);
            nb = (Integer) nbs.get("StatefulSessionBean");
            nbtotal = nbtotal + nb;
            oForm.setTotalCurrentNumberOfSBFType(nb);
            nb = (Integer) nbs.get("StatelessSessionBean");
            nbtotal = nbtotal + nb;
            oForm.setTotalCurrentNumberOfSBLType(nb);
            nb = (Integer) nbs.get("EntityBean");
            nbtotal = nbtotal + nb;
            oForm.setTotalCurrentNumberOfEntityType(nb);
            oForm.setTotalCurrentNumberOfBeanType(nbtotal);

            oForm.setTotalCurrentNumberOfCMPType(getIntegerAttribute(oObjectName, "TotalCurrentNumberOfCMPType"));
            oForm.setMonitoringEnabled(getBooleanAttribute(oObjectName, "MonitoringEnabled"));
            oForm.setNumberOfCalls(getIntegerAttribute(oObjectName, "NumberOfCalls"));
            oForm.setWarningThreshold(getIntegerAttribute(oObjectName, "WarningThreshold"));

            oForm.setAttributes(getAttributes(oObjectName, serverName));
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }finally {
            m_Session.setAttribute("editAllContainers", "false");
        }
        if (applySettingsTo != null) {
            oForm.setApplySettingsTo(applySettingsTo);
        }

        // Replace the normal forward if exists
        String sNextForward = (String) p_Request.getAttribute("NextForward");
        if (sNextForward == null) {
            sNextForward = "Ejb Containers Statistic";
        }
        // Forward to the jsp.
        return (p_Mapping.findForward(sNextForward));
    }
}
