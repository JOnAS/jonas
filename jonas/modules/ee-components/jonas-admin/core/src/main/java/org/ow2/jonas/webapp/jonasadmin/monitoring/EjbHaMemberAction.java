package org.ow2.jonas.webapp.jonasadmin.monitoring;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


public class EjbHaMemberAction extends JonasBaseAction {

	public ActionForward executeAction(ActionMapping p_Mapping,
			ActionForm p_Form, HttpServletRequest p_Request,
			HttpServletResponse p_Response) throws IOException,
			ServletException {

        String domainName = m_WhereAreYou.getCurrentDomainName();
        // Get member name from the 'member' parameter and cluster name from the 'clust' parameter
        String name = p_Request.getParameter("member");
        if (name == null) {
            addGlobalError(new Exception("EjbHaMemberAction: member parameter is null."));
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        String cluster = p_Request.getParameter("clust");
        if (cluster == null) {
            addGlobalError(new Exception("EjbHaClusterAction: clust parameter is null."));
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        // Form used
        EjbHaMemberForm oForm = (EjbHaMemberForm) p_Form;
        oForm.setName(name);
        try {
            ObjectName on = JonasObjectName.clusterMember(domainName, name, "EjbHaCluster", cluster);
            oForm.setState(getStringAttribute(on, "State"));
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }

		// Forward to the jsp.
		return (p_Mapping.findForward("EjbHaMember"));
	}
}
