/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Michel-Ange ANTON
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.container;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.management.extensions.base.api.J2EEMBeanAttributeInfo;
import org.ow2.jonas.webapp.jonasadmin.Jlists;

public class EjbContainersStatisticForm extends ActionForm {

    // --------------------------------------------------------- Properties
    // Variables

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private int totalCurrentNumberOfCMPType = 0;

    private int totalCurrentNumberOfSBFType = 0;

    private int currentNumberOfContainer = 0;

    private int totalCurrentNumberOfBMPType = 0;

    private int totalCurrentNumberOfMDBType = 0;

    private int totalCurrentNumberOfSBLType = 0;

    private int totalCurrentNumberOfEntityType = 0;

    private int totalCurrentNumberOfBeanType = 0;

    private boolean ejb3 = false;

    private boolean monitoringEnabled = false;

    private int warningThreshold = 0;

    private long averageProcessingTime = 0;

    /**
     * @return the averageProcessingTime
     */
    public long getAverageProcessingTime() {
        return averageProcessingTime;
    }

    /**
     * @param averageProcessingTime the averageProcessingTime to set
     */
    public void setAverageProcessingTime(final long averageProcessingTime) {
        this.averageProcessingTime = averageProcessingTime;
    }

    private String applySettingsTo = "none";

    private long numberOfCalls = 0;

    private boolean monitoringSettingsDefinedInDD = false;

    /**
     * Is the type already calculated ?
     */
    private boolean writable = true;

    /**
     * The MBean attribute descriptors.
     */
    private List<J2EEMBeanAttributeInfo> attributes = new ArrayList<J2EEMBeanAttributeInfo>();

    /**
     * The MBean attribute descriptors.
     */
    private List<J2EEMBeanAttributeInfo> namingAttributes = new ArrayList<J2EEMBeanAttributeInfo>();

    /**
     * The MBean attribute descriptors.
     */
    private List<J2EEMBeanAttributeInfo> monitoringAttributes = new ArrayList<J2EEMBeanAttributeInfo>();

    /**
     * deployment descriptor.
     */
    private J2EEMBeanAttributeInfo deploymentDescriptor = null;

    /**
     * JOnAS specific deployment descriptor.
     */
    private J2EEMBeanAttributeInfo jonasDeploymentDescriptor = null;

    // --------------------------------------------------------- Public Methods

    /**
     * @return the namingAttributes.
     */
    public List<J2EEMBeanAttributeInfo> getNamingAttributes() {
        return namingAttributes;
    }

    /**
     * @param namingAttributes the namingAttributes to set.
     */
    public void setNamingAttributes(final J2EEMBeanAttributeInfo[] namingAttributes) {
        this.namingAttributes.clear();
        for (J2EEMBeanAttributeInfo beanAttributeInfo : namingAttributes) {
            this.namingAttributes.add(beanAttributeInfo);
        }
    }

    /**
     * @param monitoringAttributes the namingAttributes to set.
     */
    public void setMonitoringAttributes(final J2EEMBeanAttributeInfo[] monitoringAttributes) {
        this.monitoringAttributes.clear();
        for (J2EEMBeanAttributeInfo beanAttributeInfo : monitoringAttributes) {
            this.monitoringAttributes.add(beanAttributeInfo);
        }
    }

    /**
     * @param monitoringAttributes the namingAttributes to set.
     */
    public void setMonitoringAttributes(final List<J2EEMBeanAttributeInfo> monitoringAttributes) {
        this.monitoringAttributes = monitoringAttributes;
    }

    /**
     * @return the monitoringAttributes
     */
    public List<J2EEMBeanAttributeInfo> getMonitoringAttributes() {
        return monitoringAttributes;
    }

    /**
     * @return the deploymentDescriptor.
     */
    public J2EEMBeanAttributeInfo getDeploymentDescriptor() {
        return deploymentDescriptor;
    }

    /**
     * @param deploymentDescriptor the deploymentDescriptor to set.
     */
    public void setDeploymentDescriptor(final J2EEMBeanAttributeInfo deploymentDescriptor) {
        this.deploymentDescriptor = deploymentDescriptor;
    }

    /**
     * @return the jonasDeploymentDescriptor.
     */
    public J2EEMBeanAttributeInfo getJonasDeploymentDescriptor() {
        return jonasDeploymentDescriptor;
    }

    /**
     * @param jonasDeploymentDescriptor the jonasDeploymentDescriptor to set.
     */
    public void setJonasDeploymentDescriptor(final J2EEMBeanAttributeInfo jonasDeploymentDescriptor) {
        this.jonasDeploymentDescriptor = jonasDeploymentDescriptor;
    }

    /**
     * Reset all properties to their default values.
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */

    @Override
    public void reset(final ActionMapping mapping, final HttpServletRequest request) {
        totalCurrentNumberOfCMPType = 0;
        totalCurrentNumberOfSBFType = 0;
        currentNumberOfContainer = 0;
        totalCurrentNumberOfBMPType = 0;
        totalCurrentNumberOfMDBType = 0;
        totalCurrentNumberOfSBLType = 0;
        totalCurrentNumberOfBeanType = 0;
    }

    /**
     * Validate the properties that have been set from this HTTP request, and
     * return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found. If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no recorded
     * error messages.
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    @Override
    public ActionErrors validate(final ActionMapping mapping, final HttpServletRequest request) {
        ActionErrors oErrors = new ActionErrors();
        return oErrors;
    }

    // --------------------------------------------------------- Properties
    // Methods

    public int getTotalCurrentNumberOfCMPType() {
        return totalCurrentNumberOfCMPType;
    }

    public int getTotalCurrentNumberOfSBFType() {
        return totalCurrentNumberOfSBFType;
    }

    public int getCurrentNumberOfContainer() {
        return currentNumberOfContainer;
    }

    public int getTotalCurrentNumberOfBMPType() {
        return totalCurrentNumberOfBMPType;
    }

    public int getTotalCurrentNumberOfMDBType() {
        return totalCurrentNumberOfMDBType;
    }

    public int getTotalCurrentNumberOfSBLType() {
        return totalCurrentNumberOfSBLType;
    }

    public int getTotalCurrentNumberOfBeanType() {
        return totalCurrentNumberOfBeanType;
    }

    public void setTotalCurrentNumberOfCMPType(final int totalCurrentNumberOfCMPType) {
        this.totalCurrentNumberOfCMPType = totalCurrentNumberOfCMPType;
    }

    public void setTotalCurrentNumberOfSBFType(final int totalCurrentNumberOfSBFType) {
        this.totalCurrentNumberOfSBFType = totalCurrentNumberOfSBFType;
    }

    public void setCurrentNumberOfContainer(final int currentNumberOfContainer) {
        this.currentNumberOfContainer = currentNumberOfContainer;
    }

    public void setTotalCurrentNumberOfBMPType(final int totalCurrentNumberOfBMPType) {
        this.totalCurrentNumberOfBMPType = totalCurrentNumberOfBMPType;
    }

    public void setTotalCurrentNumberOfMDBType(final int totalCurrentNumberOfMDBType) {
        this.totalCurrentNumberOfMDBType = totalCurrentNumberOfMDBType;
    }

    public void setTotalCurrentNumberOfSBLType(final int totalCurrentNumberOfSBLType) {
        this.totalCurrentNumberOfSBLType = totalCurrentNumberOfSBLType;
    }

    public void setTotalCurrentNumberOfBeanType(final int totalCurrentNumberOfBeanType) {
        this.totalCurrentNumberOfBeanType = totalCurrentNumberOfBeanType;
    }

    public int getTotalCurrentNumberOfEntityType() {
        return totalCurrentNumberOfEntityType;
    }

    public void setTotalCurrentNumberOfEntityType(final int totalCurrentNumberOfEntityType) {
        this.totalCurrentNumberOfEntityType = totalCurrentNumberOfEntityType;
    }

    public boolean getMonitoringEnabled() {
        return monitoringEnabled;
    }

    public void setMonitoringEnabled(final boolean monitoringEnabled) {
        this.monitoringEnabled = monitoringEnabled;
    }

    public int getWarningThreshold() {
        return warningThreshold;
    }

    public void setWarningThreshold(final int warningThreshold) {
        this.warningThreshold = warningThreshold;
    }

    public String getApplySettingsTo() {
        return applySettingsTo;
    }

    public void setApplySettingsTo(final String applySettingsTo) {
        this.applySettingsTo = applySettingsTo;
    }

    public List getBooleanValues() {
        return Jlists.getBooleanValues();
    }

    public long getNumberOfCalls() {
        return numberOfCalls;
    }

    public void setNumberOfCalls(final int numberOfCalls) {
        this.numberOfCalls = numberOfCalls;
    }

    public boolean isMonitoringSettingsDefinedInDD() {
        return monitoringSettingsDefinedInDD;
    }

    public void setMonitoringSettingsDefinedInDD(final boolean monitoringSettingsDefinedInDD) {
        this.monitoringSettingsDefinedInDD = monitoringSettingsDefinedInDD;
    }

    /**
     * @return the attributes.
     */
    public List<J2EEMBeanAttributeInfo> getAttributes() {
        return attributes;
    }

    /**
     * @param attributes the attributes to set.
     */
    public void setAttributes(final J2EEMBeanAttributeInfo[] attributes) {
        this.attributes.clear();
        this.monitoringAttributes.clear();
        this.namingAttributes.clear();
        for (J2EEMBeanAttributeInfo beanAttributeInfo : attributes) {
            /**
             * put deployment descriptors in dedicated fields.
             */
            if ("deploymentDescriptor".equalsIgnoreCase(beanAttributeInfo.getName())) {
                String value = beanAttributeInfo.getValue().toString();
                /**
                 * If the attribute value is consistent
                 */
                if (value != null && !value.contains("IMPLEMENTED")) {
                    deploymentDescriptor = beanAttributeInfo;
                }
            } else if ("jonasDeploymentDescriptor".equalsIgnoreCase(beanAttributeInfo.getName())) {
                String value = beanAttributeInfo.getValue().toString();
                if (value != null && !value.contains("IMPLEMENTED")) {
                    jonasDeploymentDescriptor = beanAttributeInfo;
                }
            } else {
                if (!"ejbs".equals(beanAttributeInfo.getName()) && !"server".equals(beanAttributeInfo.getName())
                        && !"objectName".equals(beanAttributeInfo.getName())
                        && !beanAttributeInfo.getName().contains("statistic") && !"id".equals(beanAttributeInfo.getName())
                        && !"stateManageable".equals(beanAttributeInfo.getName())
                        && !beanAttributeInfo.getName().contains("statistic")
                        && !"eventProvider".equals(beanAttributeInfo.getName())
                        && !"javaVMs".equals(beanAttributeInfo.getName()) && !beanAttributeInfo.getName().contains("NumberOf")
                        && !"earON".equals(beanAttributeInfo.getName())
                        && !"modelerType".equals(beanAttributeInfo.getName())) {
                    if (beanAttributeInfo.getName().contains("ame") || beanAttributeInfo.getName().contains("file")
                            || beanAttributeInfo.getName().contains("url") || beanAttributeInfo.getName().contains("Ear")
                            || beanAttributeInfo.getName().contains("ear") || beanAttributeInfo.getName().contains("ejb")
                            || beanAttributeInfo.getName().contains("java") || beanAttributeInfo.getName().contains("modeler")
                            || beanAttributeInfo.getName().contains("jar") || beanAttributeInfo.getName().contains("Jar")
                            || "available".equals(beanAttributeInfo.getName())) {
                        this.namingAttributes.add(beanAttributeInfo);
                    } else {
                        monitoringAttributes.add(beanAttributeInfo);
                    }
                }
                this.attributes.add(beanAttributeInfo);
            }
        }
    }

    /**
     * <code>true</code> if ejb3 container.
     * @return
     */
    public boolean isEjb3() {
        for (J2EEMBeanAttributeInfo attribute : attributes) {
            if ("warningThreshold".equals(attribute.getName())) {
                // the "warningThreshold" attribute is defined for ejb2 only.
                ejb3 = false;
                return ejb3;
            }
        }
        ejb3 = true;
        return ejb3;
    }

    /**
     * <code>true</code> if any attribute is writable.
     * @return
     */
    public boolean isWritable() {
        for (J2EEMBeanAttributeInfo attribute : attributes) {
            if (attribute.isWritable()) {
                // the "warningThreshold" attribute is defined for ejb2 only.
                writable = true;
                return writable;
            }
        }
        writable = false;
        return writable;
    }

}
