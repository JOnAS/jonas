/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.container;

import java.util.ArrayList;
import java.util.Collections;

import javax.servlet.http.HttpServletRequest;

import org.ow2.jonas.webapp.jonasadmin.JonasAdminJmx;
import org.ow2.jonas.webapp.jonasadmin.service.ModuleForm;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

/**
 * @author Michel-Ange ANTON
 */
public class WarForm extends ModuleForm {

// --------------------------------------------------------- Properties variables

    private String path = null;
    private String filename = null;
    private String contextRoot = null;
    private String hostName = null;
    private String warFile = null;
    private String warPath = null;
    private boolean java2DelegationModel = false;
    private String xmlContent = null;
    private String jonasXmlContent = null;
    private String[] servletsName = null;
    private ArrayList listServlets = new ArrayList();

// --------------------------------------------------------- Public Methods

    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        path = null;
        filename = null;

        contextRoot = null;
        hostName = null;
        warPath = null;
        java2DelegationModel = false;
        xmlContent = null;
        jonasXmlContent = null;
        servletsName = null;
        listServlets = new ArrayList();
    }

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     * @return List of errors
     */
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors oErrors = new ActionErrors();
        return oErrors;
    }

// --------------------------------------------------------- Properties Methods

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getWarPath() {
        return warPath;
    }

    public void setWarPath(java.net.URL p_WarUrl) {
        this.warPath = null;
        if (p_WarUrl != null) {
            this.warPath = p_WarUrl.getPath();
        }
    }

    public String getContextRoot() {
        return contextRoot;
    }

    public void setContextRoot(String contextRoot) {
        this.contextRoot = contextRoot;
    }

    public boolean getJava2DelegationModel() {
        return java2DelegationModel;
    }

    public void setJava2DelegationModel(boolean java2DelegationModel) {
        this.java2DelegationModel = java2DelegationModel;
    }

    public String getXmlContent() {
        return xmlContent;
    }

    public void setXmlContent(String xmlContent) {
        this.xmlContent = xmlContent;
    }

    public String getJonasXmlContent() {
        return jonasXmlContent;
    }

    public void setJonasXmlContent(String jonasXmlContent) {
        this.jonasXmlContent = jonasXmlContent;
    }

    public String[] getServletsName() {
        return servletsName;
    }

    public void setServletsName(String[] servletsName) {
        this.servletsName = servletsName;
        // Fill list
        listServlets = new ArrayList();
        for (int i = 0; i < this.servletsName.length; i++) {
            listServlets.add(this.servletsName[i]);
        }
        Collections.sort(listServlets);
    }

    public ArrayList getListServlets() {
        return listServlets;
    }
}