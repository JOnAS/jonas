package org.ow2.jonas.webapp.jonasadmin.jonasmqconnect;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class DestinationDeleteAction extends JonasBaseAction {

    public ActionForward executeAction(ActionMapping mapping, ActionForm form
            , HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        DestinationDeleteForm fBean = (DestinationDeleteForm) form;
        String[] selectedDestinations = fBean.getSelectedDestinations();
        ArrayList destinations = new ArrayList();
        String destinationsStr = "";
        if (selectedDestinations != null) {
            for (int i = 0; i < selectedDestinations.length; i++) {
                destinations.add(selectedDestinations[i]);
                destinationsStr += selectedDestinations[i] + ";";
            }
        }
        fBean.setDestinations(destinations);
        fBean.setDestinationsStr(destinationsStr);

        return mapping.findForward("JonasMqConnectDestinationDelete");
    }
}