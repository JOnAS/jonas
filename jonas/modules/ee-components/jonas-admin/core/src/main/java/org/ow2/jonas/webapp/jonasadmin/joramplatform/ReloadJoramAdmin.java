
/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.joramplatform;

import java.io.IOException;
import java.util.Set;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.util.JoramObjectName;


/**
 * @author Yannick braeuner
 */

public class ReloadJoramAdmin extends EditJoramBaseAction {

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param p_Mapping The ActionMapping used to select this instance
     * @param p_Form The optional ActionForm bean for this request (if any)
     * @param p_Request The HTTP request we are processing
     * @param p_Response The HTTP response we are creating
     *
     * @return An <code>ActionForward</code> instance or <code>null</code>
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */

    @Override
    public ActionForward executeAction(final ActionMapping p_Mapping, final ActionForm p_Form
            , final HttpServletRequest p_Request, final HttpServletResponse p_Response)
            throws IOException, ServletException {

        // Current JOnAS server
        String jonasServerName =  m_WhereAreYou.getCurrentJonasServerName();

            try {
                // Object name use
                Set adapterOns = JonasManagementRepr.queryNames(JoramObjectName.joramAdapter(), jonasServerName );
                if (adapterOns.isEmpty()) {
                    throw new Exception("No JORAM server available");
                }
                ObjectName joramPlatformOn = (ObjectName) adapterOns.iterator().next();
                // Call method
                String[] params = new String[1];
                params[0] = getPathToReloadJoramAdmin() + "/joramAdmin.xml";
                String[] signature = {"java.lang.String"};
                JonasManagementRepr.invoke(joramPlatformOn, "executeXMLAdminJMX", params, signature, jonasServerName);
                // Get the right local Joram id from the server
                String localJoramServerId = (String) p_Request.getSession().getAttribute("localId");
                // Refresh the Joram tree from the joram id server
                refreshJoramTree(p_Request, null);
            } catch (Throwable t) {
                addGlobalError(t);
                saveErrors(p_Request, m_Errors);
                return (p_Mapping.findForward("Global Error"));
            }

            // Forward to the jsp.
            return (p_Mapping.findForward("JoramPlatform"));
        }
}