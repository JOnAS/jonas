/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.xml;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.webapp.jonasadmin.deploy.BaseDeployAction;
import org.ow2.jonas.webapp.jonasadmin.xml.xs.ElementRestrictions;
import org.ow2.jonas.webapp.jonasadmin.xml.xs.SchemaRestrictions;
import org.ow2.jonas.webapp.jonasadmin.xml.xs.SchemaRestrictionsFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * Action to extract the XML parsed into a Document object from the archive
 * and forward to the form view JSP configuration page.
 *
 * @author Patrick Smith
 * @author Gregory Lapouchnian
 */
public class ArchiveUtilAction extends BaseDeployAction {

    /**
     * Executes the struts action.
     * @param p_Mapping the struts action mapping.
     * @param p_Form the struts action form.
     * @param p_Request the HttpServletRequest.
     * @param p_Response the HttpServletResponse.
     * @throws IOException
     * @throws ServletException
     * @return the action forward to forward to.
     */
    public ActionForward executeAction(ActionMapping p_Mapping,
            ActionForm p_Form, HttpServletRequest p_Request,
            HttpServletResponse p_Response) throws IOException,
            ServletException {

        String sForward = "Archive Config AJAX";
        ArchiveConfigForm oForm = (ArchiveConfigForm) p_Form;
        oForm.setChildren(new HashMap());

        String name = p_Request.getParameter("elementName");

        if (name != null && name.length() > 0) {

            SchemaRestrictionsFactory factory = new SchemaRestrictionsFactory();
            SchemaRestrictions restrictions = factory
                    .getSchemaRestrictions(SchemaRestrictions.RAR_TYPE);
            ElementRestrictions er = restrictions.getElementRestrictions(name);

            // set the list of allowed children
            Map children = new TreeMap();
            Iterator i = er.getChildren().iterator();
            while (i.hasNext()) {
                children.put(i.next(), "0");
            }

            oForm.setChildren(children);

            p_Response.setHeader("Cache-Control", "no-cache");
        }

        // Forward to the jsp.
        return (p_Mapping.findForward(sForward));
    }
}
