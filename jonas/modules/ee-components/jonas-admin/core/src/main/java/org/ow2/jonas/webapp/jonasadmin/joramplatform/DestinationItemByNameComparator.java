/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id: 
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.joramplatform;

import java.util.Comparator;

public class DestinationItemByNameComparator implements Comparator {

// --------------------------------------------------------- Public Methods

    public int compare(Object p_O1, Object p_O2) {
        DestinationItem o_1 = (DestinationItem) p_O1;
        DestinationItem o_2 = (DestinationItem) p_O2;
        return o_1.getName().compareToIgnoreCase(o_2.getName());
    }

    public boolean equals(Object p_Obj) {
        if (p_Obj instanceof DestinationItem) {
            return true;
        }
        return false;
    }
}
