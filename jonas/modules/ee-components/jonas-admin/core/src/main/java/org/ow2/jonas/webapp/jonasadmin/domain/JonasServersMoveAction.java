/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.domain;

import java.io.IOException;
import java.util.ArrayList;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Adriana Danes
 */

public class JonasServersMoveAction extends JonasBaseAction {

// --------------------------------------------------------- Public Methods
	public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
			, HttpServletRequest p_Request, HttpServletResponse p_Response)
	throws IOException, ServletException {

		// Form used
        ItemsServersForm oForm = (ItemsServersForm) p_Form;

        String currentOn = (String) m_Session.getAttribute("currentOn");

        // Actions
        //System.out.println("Apply Move action (" + oForm.getAction() + ") to items:");
        try {
        	ArrayList alSelected = new ArrayList();
            for (int i=0; i < oForm.getSelectedItems().length; i++) {
            	String selectedServerOn = oForm.getSelectedItems()[i];
            	ObjectName on = ObjectName.getInstance(selectedServerOn);
                //System.out.println(selectedServerOn);
                JonasServerItem item = new JonasServerItem(on);
                alSelected.add(item);
            }
            m_Session.setAttribute("listServersSelected", alSelected);
        }
        catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }

        // Forward to the Servers Confirm page
        return p_Mapping.findForward("JonasServersConfirmMove");
	}
}
