/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.resourceadapter;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.webapp.jonasadmin.deploy.BaseDeployAction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * This action is called when the user picks which type of Resource Archive to create.
 *
 * @author Patrick Smith
 */
public class ApplyTemplateCreateResourceAdapterAction extends BaseDeployAction {

// --------------------------------------------------------- Public Methods

    /**
     * The action to use when the Struts action is executed.
     * Directs the user to either template depending on which was selected.
     * @param p_Mapping The ActionMapping for the action.
     * @param p_Form The form used in this action.
     * @param p_Request HTTP Request for the action.
     * @param p_Response The HTTP Response.
     * @throws IOException if the there is a problem with the
     * template files, creation of the RARs.
     * @throws ServletException if the there is a servlet error.
     * @return A forward to the next Struts page.
     */
    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws IOException, ServletException {

        // Form used
        CreateResourceAdapterForm oForm = (CreateResourceAdapterForm) p_Form;
        m_Session.setAttribute("createResourceAdapterForm", oForm);
        if (oForm.getTemplate().equals("Other")) {
            oForm.setTemplate("Other");
            return (p_Mapping.findForward("Create Other Resource Adapter"));
        } else {
            oForm.setTemplate("JDBC");
            return (p_Mapping.findForward("Create JDBC Resource Adapter"));
        }
    }

// --------------------------------------------------------- Protected Methods

}
