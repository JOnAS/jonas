/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.cmi;

import java.util.Set;

import org.ow2.jonas.webapp.jonasadmin.monitoring.BaseClusterForm;

public class CmiClusterForm extends BaseClusterForm {
    /**
     * Action to be performed.
     */
    private String action = null;
    /**
     * Multicast Address.
     */
    private String mcastAddr = null;
    public String getMcastAddr() {
        return mcastAddr;
    }
    public void setMcastAddr(final String addr) {
        mcastAddr = addr;
    }

    private int mcastPort;
    public int getMcastPort() {
        return mcastPort;
    }
    public void setMcastPort(final int port) {
        mcastPort = port;
    }

    private String protocol;
    public String getProtocol() {
        return protocol;
    }
    public void setProtocol(final String proto) {
        protocol = proto;
    }
    /**
    *
    */
   private static final long serialVersionUID = 1L;
   /**
    * Cluster Name.
    */
    String name;

    /**
     * Cluster objects.
     */
    Set<String> objectList;

    /**
     * Default constructor.
     */
    public CmiClusterForm(){

    }
       /**
        * @param name
        * @param objectList
        */
       public CmiClusterForm(final String name, final Set<String> objectList) {
           this.name = name;
           this.objectList = objectList;
       }
       /**
        * @return the name
        */
       @Override
    public String getName() {
           return name;
       }
       /**
        * @param name the name to set
        */
       @Override
    public void setName(final String name) {
           this.name = name;
       }
       /**
        * @return the objectList
        */
       public Set<String> getObjectList() {
           return objectList;
       }
       /**
        * @param objectList the objectList to set
        */
       public void setObjectList(final Set<String> objectList) {
           this.objectList = objectList;
       }
    public String getAction() {
        return action;
    }
    public void setAction(final String action) {
        this.action = action;
    }

    private int delayToRefresh;

    public int getDelayToRefresh() {
        return delayToRefresh;
    }

    public void setDelayToRefresh(final int delayToRefresh) {
        this.delayToRefresh = delayToRefresh;
    }
}
