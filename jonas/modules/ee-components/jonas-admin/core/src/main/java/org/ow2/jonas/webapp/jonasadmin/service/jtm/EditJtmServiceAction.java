/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.jtm;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.lib.management.javaee.J2eeObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.ServiceName;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

/**
 * Principal action for JTM service management.
 * @author Adriana Danes
 */
public class EditJtmServiceAction extends JonasBaseAction {

    /**
     * Principal method
     * @param pMapping action mapping
     * @param pForm used form
     * @param pRequest request parameters
     * @param pResponse response parameters
     * @return action forward
     * @throws IOException action failed because a IOException
     * @throws ServletException a ServletException occured
     */
    @Override
    public ActionForward executeAction(final ActionMapping pMapping, final ActionForm pForm
        , final HttpServletRequest pRequest, final HttpServletResponse pResponse)
        throws IOException, ServletException {

        String serviceName = ServiceName.JTM.getName();
        // Force the node selected in tree
        m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_SERVER) + WhereAreYou.NODE_SEPARATOR
            + "services" + WhereAreYou.NODE_SEPARATOR + "transaction", true);

        if (!isActive(serviceName)) {
            return (pMapping.findForward("Jtm Service Stopped"));
        }
        // Form used
        JtmServiceForm oForm = new JtmServiceForm();
        m_Session.setAttribute("jtmServiceForm", oForm);
        try {
            // Object name used
            String currentDomainName = m_WhereAreYou.getCurrentDomainName();
            String currentJonasServerName = m_WhereAreYou.getCurrentJonasServerName();
            String jtaResourceName = "JTAResource";
            ObjectName jtaResourceObjectName = J2eeObjectName.JTAResource(currentDomainName, currentJonasServerName, jtaResourceName);

            String location;
            boolean collocated = getBooleanAttribute(jtaResourceObjectName, "localJtm");
            if (collocated) {
                // Collacated JTM
                location = m_Resources.getMessage("tab.jtm.collocated");
                oForm.setJtmLocation(location);
            } else {
                // Distant MOM
                location = m_Resources.getMessage("tab.jtm.distant");
                oForm.setJtmLocation(location);
            }

            String host = getStringAttribute(jtaResourceObjectName, "hostName");
            oForm.setJtmHost(host);

            int port = getIntegerAttribute(jtaResourceObjectName, "portNumber");
            String portValue = null;
            if (port == 0) {
                // Undefined value
                portValue = m_Resources.getMessage("tab.jtm.undefined");
                oForm.setJtmPort(portValue);
            } else {
                oForm.setJtmPort(Integer.toString(port));
            }

            int timeOut = getIntegerAttribute(jtaResourceObjectName, "timeOut");
            oForm.setTimeOutText(String.valueOf(timeOut));
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(pRequest, m_Errors);
            return (pMapping.findForward("Global Error"));
        }

        // Forward to the jsp.
        return (pMapping.findForward("Jtm Service"));
    }

}
