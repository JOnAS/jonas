/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.xml;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;

/**
 * Struts form used by the archive configuration pages.
 * @author Patrick Smith
 * @author Gregory Lapouchnian
 */

public class ArchiveConfigForm extends ActionForm {

    // --------------------------------------------------------- Constants

    /** The form was used. */
    public static int FORM = 0;

    /** Advanced mode was used. */
    public static int ADVANCED = 1;

    // --------------------------------------------------------- Properties variables

    /**
     * List of deployable archives that can be selected for configuration.
     */
    private ArrayList deployable;

    /**
     * The content of the archive's deployment descriptor used to create the form view.
     */
    private Document document;

    /**
     * The raw XML contents of the archive's deployment descriptor used in the advanced view.
     */
    private String xml;

    /**
     * A mapping of IDs used in the HTML form to Node objects they represent.
     */
    private Map mapping = new HashMap();

    /**
     * Map used to store the input of all the textfields when the user submits the form view.
     */
    private Map values = new TreeMap();

    /**
     * Submit button.
     */
    private String submit;

    /**
     * Which configuration type is being used right now. Form or Advanced.
     */
    private int configType;

    /**
     * Switch To ... button.
     */
    private String switchTo;

    /**
     * Cancel button.
     */
    private String cancel;

    /**
     * The name of the current archive.
     */
    private String archiveName;

    /**
     * The path to the XML file within the current archive.
     */
    private String pathName;

    /**
     * Used for returning values requested using the XMLHTTPRequest
     * on the UI side. These values are used to construct a select list
     * of allowed children for a given element.
     */
    private Map children;

    /**
     * Is this a domain operation.
     */
    private boolean isDomain;

    /**
     * The managed server's JONAS_BASE
     */
    private String jonasBase;
    // --------------------------------------------------------- Public Methods

    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */

    public void reset(ActionMapping mapping, HttpServletRequest request) {

    }

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     * @return the errors.
     */
    public ActionErrors validate(ActionMapping mapping,
            HttpServletRequest request) {
        ActionErrors oErrors = new ActionErrors();
        return oErrors;
    }

    /**
     * Returns a String represetnation of the values.
     * @return a string representation of the values.
     */
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(values.toString());

        return sb.toString();
    }

    /**
     * Resets this form.
     *
     */
    public void reset() {
        archiveName = null;
        pathName = null;
        document = null;
        xml = null;
        mapping = new HashMap();
        values = new TreeMap();
        jonasBase = null;
    }

    // --------------------------------------------------------- Properties Methods

    /**
     * Returns the document.
     * @return the document.
     */
    public Document getDocument() {
        return document;
    }

    /**
     * Sets the document of this form
     * @param document the value to set.
     */
    public void setDocument(Document document) {
        this.document = document;
    }

    /**
     * Sets a value within the values map.
     * @param key the key of the mapping.
     * @param value the value of the mapping.
     */
    public void setValues(String key, Object value) {
        values.put(key, value);
    }

    /**
     * Returns the object at key's spot of the mapping.
     * @param key the key for the mapping.
     * @return the value in the map that matches key.
     */
    public Object getValues(String key) {
        return values.get(key);
    }

    /**
     * Sets the values map.
     */
    public void setValuesMap(Map values) {
        this.values = values;
    }


    /**
     * Returns the values map.
     * @return the values map.
     */
    public Map getValuesMap() {
        return values;
    }

    /**
     * Sets the map.
     * @param mapping the value to set.
     */
    public void setMapping(Map mapping) {
        this.mapping = mapping;
    }

    /**
     * Returns the map.
     * @return the map.
     */
    public Map getMapping() {
        return this.mapping;
    }

    /**
     * Return the XML document as a string.
     * @return the XML document as a string.
     */
    public String getXml() {
        return xml;
    }

    /**
     * Sets the XML string.
     * @param xml the value to set.
     */
    public void setXml(String xml) {
        this.xml = xml;
    }

    /**
     * What submission form was done.
     * @return the of submission.
     */
    public String getSubmit() {
        return submit;
    }

    /**
     * Sets the submission.
     * @param submit the submission to set.
     */
    public void setSubmit(String submit) {
        this.submit = submit;
    }

    /**
     * Returns the config type.
     * @return the config type.
     */
    public int getConfigType() {
        return configType;
    }

    /**
     * Sets the config type.
     * @param configType the type to set.
     */
    public void setConfigType(int configType) {
        this.configType = configType;
    }

    /**
     * The target of switching done.
     * @return the target of switching.
     */
    public String getSwitchTo() {
        return switchTo;
    }

    /**
     * Sets the target of switching
     * @param switchTo the target to set.
     */
    public void setSwitchTo(String switchTo) {
        this.switchTo = switchTo;
    }

    /**
     * Returns the archive name in the form.
     * @return the archive name.
     */
    public String getArchiveName() {
        return archiveName;
    }

    /**
     * Sets the archive name for the form.
     * @param archiveName the archive name to set.
     */
    public void setArchiveName(String archiveName) {
        this.archiveName = archiveName;
    }

    /**
     * Returns the path of the XML file.
     * @return the path of the XML file.
     */
    public String getPathName() {
        return pathName;
    }

    /**
     * Sets the path.
     * @param pathName the path to set.
     */
    public void setPathName(String pathName) {
        this.pathName = pathName;
    }

    /**
     * The list of deployable archives.
     * @return the list of deployable archives.
     */
    public ArrayList getDeployable() {
        return deployable;
    }

    /**
     * Sets the list of deployable archives.
     * @param deployable the list to set.
     */
    public void setDeployable(ArrayList deployable) {
        this.deployable = deployable;
    }

    /**
     * The target of the cancel op.
     * @return the target of the cancel op.
     */
    public String getCancel() {
        return cancel;
    }

    /**
     * Sets the cancel string
     * @param cancel the string to set.
     */
    public void setCancel(String cancel) {
        this.cancel = cancel;
    }

    /**
     * Return the map of children
     * @return the map of children.
     */
    public Map getChildren() {
        return children;
    }

    /**
     * Sets the children map.
     * @param childValues the map to set.
     */
    public void setChildren(Map childValues) {
        this.children = childValues;
    }

    /**
     * Return if the action is a domain management operation.
     * @return if the action is a domain management operation.
     */
    public boolean getIsDomain() {
        return this.isDomain;
    }

    /**
     * Sets if the action is a domain management operation.
     * @param isDomain if the action is a domain management operation.
     */
    public void setIsDomain(boolean isDomain) {
        this.isDomain = isDomain;
    }

    public String getJonasBase() {
        return jonasBase;
    }

    public void setJonasBase(String jonasBase) {
        this.jonasBase = jonasBase;
    }

}
