package org.ow2.jonas.webapp.jonasadmin.jonasmqconnect.util;

import java.util.Iterator;
import java.util.Set;
import java.util.ArrayList;

import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;


public class MqObjectNames {

    /**
     * ObjectName of JOnAS MQ connector
     * @param domainName domain name
     * @param name connector name
     * @return ObjectName of JOnAS MQ connector
     */
    public static ObjectName getConnectorONByName(String domainName, String name) {
        try {
            return ObjectName.getInstance(domainName + ":type=JonasMQConnector,name=" + name);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * ObjectName of all JOnAS MQ connectors deployed in a given JOnAS server
     * @param domainName domain name
     * @param serverName server name
     * @return ObjectName of all JOnAS MQ connectors deployed in a given JOnAS server
     */
    public static ObjectName[] getConnectorsON(String domainName, String serverName) {
        try {
            ObjectName on = ObjectName.getInstance(domainName + ":type=JonasMQConnector,*");
            Set set = JonasManagementRepr.queryNames(on, serverName);
            ObjectName[] list = new ObjectName[set.size()];
            int j = 0;
            for (Iterator i = set.iterator(); i.hasNext();) {
                list[j++] = (ObjectName) i.next();
            }
            return list;
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
            return null;
        }
    }

    /**
     * ObjectName of all JOnAS MQ connectors deployed in a given JOnAS server
     * @param domainName domain name
     * @param serverName server name
     * @return ObjectName list of all JOnAS MQ connectors deployed in a given JOnAS server
     */
    public static ArrayList getConnectorsONList(String domainName, String serverName) {
        try {
            ObjectName ons = ObjectName.getInstance(domainName + ":type=JonasMQConnector,*");
            Set set = JonasManagementRepr.queryNames(ons, serverName);
            ArrayList list = new ArrayList();
            int j = 0;
            for (Iterator i = set.iterator(); i.hasNext();) {
                ObjectName on = (ObjectName) i.next();
                list.add(on.getKeyProperty("name"));
            }
            return list;
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
            return null;
        }
    }

    /**
     * ObjectName of a MQ destination created by deploying a given MQ connector
     * @param connector connector name
     * @param jndiName JNDI name of the MQ destination
     * @param domainName domain name
     * @return ObjectName of the MQ destination
     */
    public static ObjectName getDestinationON(String domainName, String jndiName, String connector) {
        try {
            return new ObjectName(domainName + ":type=MQDestination,name=" + jndiName
                    + ",JonasMQConnector=" + connector);
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        }
        return null;
    }

    /**
     * ObjectName of all the destination created by deploying a given MQ connector
     * @param connector connector name
     * @param domainName domain name
     * @return ObjectName of the MQ destination
     */
    public static ObjectName getDestinationsON(String domainName, String connector) {
        try {
            return new ObjectName(domainName + ":type=MQDestination,JonasMQConnector=" + connector + ",*");
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        }
        return null;
    }
    /**
     * ObjectName of Mdb
     * @param connectorON conector ObjectName
     * @param name the name of the requested MDB
     * @param serverName JOnAS server name
     * @return ObjectName of Mdb
     */
    public static ObjectName getMdbON(ObjectName connectorON, String name, String serverName) {
        try {
            Object[] params = {};
            String[] signature = {};
            String[] onMdbs = (String[]) JonasManagementRepr.invoke(
                    connectorON, "listConsumerObjectNames", params, signature, serverName);
            for (int i = 0; i < onMdbs.length; i++) {
                ObjectName on = new ObjectName(onMdbs[i]);
                String onName = on.getKeyProperty("name");
                if (onName.equals(name)) {
                    return on;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }


    /**
     * ObjectName of parent connector
     * @param on ObjectName of ..
     * @param domainName domain name
     * @return the ObjectName of the parent connector, or null
     */
    public static ObjectName getParentConnectorON(String domainName, ObjectName on) {

        /*
         * If on is a connector, simply return it
         */
        if ("JonasMQConnector".equals(on.getKeyProperty("type"))) {
            return on;
        }

        /*
         * Else guess the connector on
         */
        ObjectName connectorON = null;
        try {
            connectorON = ObjectName.getInstance(domainName + ":type=JonasMQConnector,name=" + on.getKeyProperty("JonasMQConnector"));
        } catch (MalformedObjectNameException e) {
            // TODO Auto-generated catch block
            e.printStackTrace(System.err);
        }
        return connectorON;
    }

}
