/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.resource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.webapp.jonasadmin.JonasAdminJmx;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;
import org.ow2.jonas.webapp.jonasadmin.service.db.DatasourceItem;
import org.ow2.jonas.webapp.jonasadmin.service.db.DatasourceItemByNameComparator;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Michel-Ange ANTON
 */

public class ListDatasourcesAction extends JonasBaseAction {

// --------------------------------------------------------- Public Methods

    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws IOException, ServletException {

        // no Form used
        try {
            WhereAreYou oWhere = (WhereAreYou) p_Request.getSession().getAttribute(WhereAreYou.
                    SESSION_NAME);
            String serverName = oWhere.getCurrentJonasServerName();
            String domainName = oWhere.getCurrentDomainName();
            ArrayList alDeployable = JonasAdminJmx.getDatasourceFilesDeployable(domainName, serverName);
            ArrayList alDeployed = JonasAdminJmx.getDatasourceFilesDeployed(domainName, serverName);

            ArrayList al = new ArrayList();
            String sName;
            boolean bDeployed;
            Iterator it = alDeployable.iterator();
            while (it.hasNext()) {
                sName = it.next().toString();
                bDeployed = alDeployed.contains(sName);
                al.add(new DatasourceItem(sName, bDeployed));
            }
            Collections.sort(al, new DatasourceItemByNameComparator());
            // Set list in the request
            p_Request.setAttribute("listDatasources", al);
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        // Forward to the jsp.
        return (p_Mapping.findForward("Datasources"));
    }
}
