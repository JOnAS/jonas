/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2003-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.catalina;

import javax.servlet.http.HttpServletRequest;

import org.ow2.jonas.webapp.jonasadmin.JonasTreeBuilder;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;
import org.ow2.jonas.webapp.jonasadmin.deploy.BaseDeployAction;
import org.ow2.jonas.webapp.taglib.TreeControl;
import org.ow2.jonas.webapp.taglib.TreeControlNode;


/**
 * @author Michel-Ange ANTON
 */
abstract public class CatalinaBaseAction extends BaseDeployAction {

// --------------------------------------------------------- Public Methods
    /**
     * Refresh the tree.
     *
     * @param p_Request Current instance
     * @throws Exception
     */
    protected void refreshTree(HttpServletRequest p_Request)
        throws Exception {
        // Refresh Security Tree
        refreshConnectorsTree(p_Request);
        // Refresh MBeans Tree
        refreshMBeansTree(p_Request);
        // Force display to refresh
        m_WhereAreYou.setTreeToRefresh(true);
    }

    /**
     * Refresh the connectors branch tree.
     *
     * @param p_Request Current instance
     * @throws Exception
     */
    protected void refreshConnectorsTree(HttpServletRequest p_Request)
        throws Exception {
        // Get the current node name
        String sCurrentNodeNameItem = getTreeBranchName(DEPTH_SERVER)
            + WhereAreYou.NODE_SEPARATOR + "protocols"
            + WhereAreYou.NODE_SEPARATOR + "connectors";
        // Get current tree
        TreeControl oControl = m_WhereAreYou.getTreeControl();
        // Get branch root node
        TreeControlNode oBranchRootNode = oControl.findNode(sCurrentNodeNameItem);
        // Enable auto-refresh mode
        oControl.enableAutoRefresh();
        // Remove old children
        TreeControlNode[] aoNodes = oBranchRootNode.findChildren();
        for (int i = 0; i < aoNodes.length; i++) {
            aoNodes[i].remove();
        }
        // Build node for the Service
        JonasTreeBuilder oBuilder = new JonasTreeBuilder();
        oBuilder.getCatalinaDetailConnectors(oBranchRootNode, m_Resources, p_Request);
        // Disable auto-refresh mode
        oControl.disableAutoRefresh();
    }

}
