/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2003-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.jonasserver;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.management.javaee.J2eeObjectName;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasAdminJmx;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;


/**
 * @author Adriana Danes <br>
 * Contributor Michel-Ange Anton
 */
public class ListRegistryAction extends JonasBaseAction {
    /**
     * Execute action for registry management.
     * @param pMapping mapping info
     * @param pForm form object
     * @param pRequest HTTP request
     * @param pResponse HTTP response
     *
     * @return An <code>ActionForward</code> instance or <code>null</code>
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */
    @Override
    public ActionForward executeAction(final ActionMapping pMapping, final ActionForm pForm
        , final HttpServletRequest pRequest, final HttpServletResponse pResponse)
        throws IOException, ServletException {

        // Form used
        JndiResourcesForm oForm = new JndiResourcesForm();

        String sDomainName = m_WhereAreYou.getCurrentDomainName();
        String  jonasServerName = m_WhereAreYou.getCurrentJonasServerName();

        // Force the node selected in tree
        m_WhereAreYou.selectNameNode("domain"
                + WhereAreYou.NODE_SEPARATOR
                + jonasServerName, true);

        try {
            String pDomainName = m_WhereAreYou.getCurrentDomainName();
            String pServerName = m_WhereAreYou.getCurrentJonasServerName();
            ObjectName resourceOn = null; // used if only one JNDI resource
            ObjectName ons = J2eeObjectName.JNDIResources(pDomainName, pServerName);
            Iterator itNames = JonasManagementRepr.queryNames(ons, pServerName).iterator();
            List lNames = null;
            int nbJNDIResources = 0;
            while (itNames.hasNext()) {
                JndiResourceItem item = new JndiResourceItem();
                ObjectName itOn = (ObjectName) itNames.next();
                item.setProviderUrl(getStringAttribute(itOn, "ProviderURL"));
                item.setProtocol(getStringAttribute(itOn, "Name"));
                item.setResourceON(itOn.toString());
                lNames = getListAttribute(itOn, "Names");
                oForm.addProvider(item);
                resourceOn = itOn;
                nbJNDIResources++;
            }

            if (nbJNDIResources == 1) {
                // test if the resource corresponds to the CMI protocol
                // in this case, the resource name is 'cmi'
                // and we have a CMI type MBean
                boolean cmi = false;
                ObjectName cmiOn = JonasObjectName.cmiServer(sDomainName, pServerName);
                if (JonasAdminJmx.hasMBeanName(cmiOn, pServerName)) {
                    cmi = true;
                }
                pRequest.setAttribute("cmi", "false");
                if ( cmi ) {
                    List namesAndNodes = CmiRegistryResource.getCmiRegistry(cmiOn, lNames, pServerName);
                    oForm.setVector(namesAndNodes);
                    pRequest.setAttribute("cmi", "true");
                    oForm.setRegistryProtocol("cmi");
                } else {
                    oForm.setListNames(lNames);
                    oForm.setRegistryProtocol(getStringAttribute(resourceOn, "Name"));
                }

            }
            if (JonasManagementRepr.isRegistered(JonasObjectName.webContainerService(sDomainName), pServerName)) {
                oForm.setPresentServletContainer(true);
            } else {
                oForm.setPresentServletContainer(false);
            }
            pRequest.setAttribute("jndiResourcesForm", oForm);
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(pRequest, m_Errors);
            return (pMapping.findForward("Global Error"));
        }
        // Forward to the jsp.
        return (pMapping.findForward("Registry"));
    }

}
