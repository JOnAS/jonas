package org.ow2.jonas.webapp.jonasadmin.monitoring;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.cmi.CmiClusterForm;

/**
 * Apply modifications of a CmiCluster.
 * @author Adriana.Danes@bull.net
 */
public class ApplyCmiClusterAction extends JonasBaseAction {

    @Override
    public ActionForward executeAction(final ActionMapping mapping, final ActionForm form,
            final HttpServletRequest request, final HttpServletResponse response)
            throws IOException, ServletException {
         // Form used
        CmiClusterForm oForm = (CmiClusterForm) form;
        // CmiClustrer type
        String clusterType = "CmiCluster";
        try {
            String domainName = m_WhereAreYou.getCurrentDomainName();
            String currentJonasServerName = m_WhereAreYou.getCurrentJonasServerName();
            String clusterName = oForm.getName();
            int delayToRefresh = oForm.getDelayToRefresh();
            // Object name used
            ObjectName cmiClusterOn = JonasObjectName.cluster(domainName, clusterName, clusterType);

            JonasManagementRepr.setAttribute(cmiClusterOn, "DelayToRefresh", new Integer(delayToRefresh), currentJonasServerName);

        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(request, m_Errors);
            return (mapping.findForward("Global Error"));
        }

        // Forward to action
        return (mapping.findForward("ActionCmiClusterInfo"));

    }

}
