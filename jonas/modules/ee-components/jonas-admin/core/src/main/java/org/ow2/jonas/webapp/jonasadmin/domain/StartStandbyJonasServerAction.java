/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 *  $
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.domain;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.lib.management.extensions.domain.DomainManagement;
import org.ow2.jonas.management.extensions.domain.api.IDomain;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;


/**
 * @author Adriana Danes
 */

public class StartStandbyJonasServerAction extends JonasBaseAction {

// --------------------------------------------------------- Public Methods
    @Override
    public ActionForward executeAction(final ActionMapping p_Mapping, final ActionForm p_Form
            , final HttpServletRequest p_Request, final HttpServletResponse p_Response)
    throws IOException, ServletException {

        // server to start
        String serverName = p_Request.getParameter("name");
        try {
            IDomain domainManagement = DomainManagement.getInstance();
            domainManagement.startServer(serverName, true);
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }

        // Forward to the jsp.
        return (p_Mapping.findForward("ActionEditDomain"));
    }
}
