/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.xml;

import java.io.IOException;
import java.util.HashMap;
import java.util.TreeMap;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.webapp.jonasadmin.JonasAdminJmx;
import org.ow2.jonas.webapp.jonasadmin.deploy.BaseDeployAction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 * Action to load the raw XML for a given archive and deployment descriptor and
 * forward to the advanced view JSP.
 *
 * @author Patrick Smith
 * @author Gregory Lapouchnian
 */
public class ArchiveConfigAdvancedAction extends BaseDeployAction {

    /**
     * Executes the struts action.
     * @param p_Mapping the struts action mapping.
     * @param p_Form the struts action form.
     * @param p_Request the HttpServletRequest.
     * @param p_Response the HttpServletResponse.
     * @throws IOException
     * @throws ServletException
     * @return the action forward to forward to.
     */
    public ActionForward executeAction(ActionMapping p_Mapping,
            ActionForm p_Form, HttpServletRequest p_Request,
            HttpServletResponse p_Response) throws IOException,
            ServletException {

        String sForward = "Archive Config Advanced";

        ArchiveConfigForm oForm = (ArchiveConfigForm) p_Form;

        String serverName = m_WhereAreYou.getCurrentJonasServerName();
        String domainName = m_WhereAreYou.getCurrentDomainName();

        // find out which XML file within the archive is being requested
        if (p_Request.getParameter("file") != null
                && p_Request.getParameter("file").length() > 0) {
            oForm.setPathName(p_Request.getParameter("file"));
        }

        // retrieve the XML document
        ObjectName on = JonasAdminJmx.getRarConfigObjectName(domainName, serverName);
        Object[] params = new Object[] {oForm.getArchiveName(),
                oForm.getPathName() };
        String[] sig = new String[] {"java.lang.String", "java.lang.String" };

        String xml;
        try {
            xml = (String) JonasManagementRepr.invoke(on, "extractXML", params,
                    sig, serverName);
            oForm.setXml(xml);
        } catch (Exception e) {
            m_Errors.add("error.archiveconfig.load.fail", new ActionMessage(
                    "error.archiveconfig.load.fail", oForm.getPathName()));
            saveErrors(p_Request, m_Errors);

            // forward back to the selection JSP
            return (p_Mapping.findForward("Archive Config Select"));
        }

        // reset the values, not to reuse anything from the previous document
        oForm.setValuesMap(new TreeMap());
        oForm.setMapping(new HashMap());

        // specify the two types of xml files that can be configured, these are
        // used as parameters to <html:link> when switching from file to file
        oForm.setValues("raXmlFileName", "META-INF/ra.xml");
        oForm.setValues("jonasRaXmlFileName", "META-INF/jonas-ra.xml");

        // save the contents of the document to the form
        oForm.setValues("fileName", oForm.getPathName());
        oForm.setValues("pathName", oForm.getArchiveName());
        oForm.setXml(xml);
        oForm.setIsDomain(isDomain());

        // Forward to the jsp.
        return (p_Mapping.findForward(sForward));
    }
}
