/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.db;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.management.extensions.container.ejb.EjbItem;
import org.ow2.jonas.lib.management.extensions.container.ejb.EjbItemByNameComparator;
import org.ow2.jonas.lib.management.javaee.J2eeObjectName;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * @author Michel-Ange ANTON
 */

public class EditDatasourceAction extends JonasBaseAction {

// --------------------------------------------------------- Public Methods

    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws IOException, ServletException {

        // Datasource to edit
        String sName = p_Request.getParameter("name");

        // Form used
        DatasourceForm oForm = null;
        if (sName != null) {
            // Editing a new
            oForm = new DatasourceForm();
            oForm.reset(p_Mapping, p_Request);
            m_Session.setAttribute("datasourceForm", oForm);
            oForm.setDatasourceName(sName);
        } else {
            // Editing that which is in session
            oForm = (DatasourceForm) m_Session.getAttribute("datasourceForm");
        }

        // Force the node selected in tree
        m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_SERVER) + WhereAreYou.NODE_SEPARATOR
            + "services" + WhereAreYou.NODE_SEPARATOR + "database" + WhereAreYou.NODE_SEPARATOR
            + oForm.getDatasourceName(), true);

        // Populate
        try {
            if (sName != null) {
                populate(oForm, m_WhereAreYou.getCurrentDomainName(), m_WhereAreYou.getCurrentJonasServerName());
            }
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        // Forward to the jsp.
        return (p_Mapping.findForward("Datasource"));
    }

// --------------------------------------------------------- Protected Methods

    protected void populate(DatasourceForm p_Form, String domainName, String serverName)
        throws Exception {

        // ObjectName used for JDBCDataSource
        ObjectName oObjectName = J2eeObjectName.getJDBCDataSource(domainName, serverName, p_Form.getDatasourceName());
        // JDBCDriver ObjectName
        String jdbcDriverON = getStringAttribute(oObjectName, "jdbcDriver");
        ObjectName onJdbcDriver = ObjectName.getInstance(jdbcDriverON);
        // Driver class name moved to JDBCDriver
        p_Form.setClassName(getStringAttribute(onJdbcDriver, "driverClassName"));
        p_Form.setDatasourceDescription(getStringAttribute(oObjectName, "description"));
        p_Form.setDatasourceName(getStringAttribute(oObjectName, "name"));
        p_Form.setDatasourceMapper(getStringAttribute(oObjectName, "mapperName"));
        p_Form.setDsName(getStringAttribute(oObjectName, "jndiName"));
        p_Form.setJdbcConnCheckLevel(toStringIntegerAttribute(oObjectName, "jdbcConnCheckLevel"));
        p_Form.setJdbcConnMaxAge(toStringIntegerAttribute(oObjectName, "jdbcConnMaxAge"));
        p_Form.setJdbcMaxOpenTime(toStringIntegerAttribute(oObjectName, "jdbcMaxOpenTime"));
        p_Form.setJdbcTestStatement(getStringAttribute(oObjectName, "jdbcTestStatement"));
        p_Form.setPassword(getStringAttribute(oObjectName, "userPassword"));
        p_Form.setUrl(getStringAttribute(oObjectName, "url"));
        p_Form.setUserName(getStringAttribute(oObjectName, "userName"));
        p_Form.setJdbcMaxConnPool(toStringIntegerAttribute(oObjectName, "jdbcMaxConnPool"));
        p_Form.setJdbcMinConnPool(toStringIntegerAttribute(oObjectName, "jdbcMinConnPool"));
        p_Form.setJdbcMaxWaitTime(toStringIntegerAttribute(oObjectName, "jdbcMaxWaitTime"));
        p_Form.setJdbcMaxWaiters(toStringIntegerAttribute(oObjectName, "jdbcMaxWaiters"));
        p_Form.setJdbcSamplingPeriod(toStringIntegerAttribute(oObjectName, "jdbcSamplingPeriod"));
        p_Form.setJdbcAdjustPeriod(toStringIntegerAttribute(oObjectName, "jdbcAdjustPeriod"));
        p_Form.setJdbcPstmtMax(toStringIntegerAttribute(oObjectName, "jdbcPstmtMax"));

        // Build list of Ejb which used this datasource
        ArrayList al = new ArrayList();
        String[] asParam = new String[1];
        String[] asSignature = new String[1];
        asSignature[0] = "java.lang.String";
        asParam[0] = p_Form.getDsName();
        if (JonasManagementRepr.isRegistered(JonasObjectName.ejbService(domainName), serverName)) {
            java.util.Iterator it = ((java.util.Set) JonasManagementRepr.invoke(JonasObjectName.
                ejbService(domainName), "getDataSourceDependence", asParam, asSignature, serverName)).iterator();
            while (it.hasNext()) {
                al.add(new EjbItem((ObjectName) it.next(), serverName));
            }
        }
        // Sort by name
        Collections.sort(al, new EjbItemByNameComparator());
        // Set list in form
        p_Form.setListUsedByEjb(al);
    }
}
