/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.mail;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.javaee.J2eeObjectName;
import org.ow2.jonas.mail.MailService;
import org.ow2.jonas.webapp.jonasadmin.JonasAdminJmx;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * Action for management of a Session Mail Factory
 * @author Adriana Danes
 */
public class ListMimePartMailFactoriesAction extends JonasBaseAction {

    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws IOException, ServletException {

        // Force the node selected in tree
        m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_SERVER) + WhereAreYou.NODE_SEPARATOR
            + "services" + WhereAreYou.NODE_SEPARATOR + "mail" + WhereAreYou.NODE_SEPARATOR
            + "mimepart", true);

        // no Form used
        try {
            String sName;
            String sJndiName;
            ObjectName oObjectName;
            ArrayList al = new ArrayList(); // list of items

            ArrayList alDeployed = JonasAdminJmx.getMimeMailPartFactoriesDeployed(m_WhereAreYou);
            Iterator it = alDeployed.iterator();
            String domainName = m_WhereAreYou.getCurrentDomainName();
            String serverName = m_WhereAreYou.getCurrentJonasServerName();
            while (it.hasNext()) {
                sName = it.next().toString();
                //oObjectName = JonasObjectName.mimeMailFactory(sName);
                oObjectName = J2eeObjectName.JavaMailResource(domainName, sName, serverName
                    , MailService.MIMEPART_PROPERTY_TYPE);
                sJndiName = getStringAttribute(oObjectName, "Name");
                al.add(new MailFactoryItem(sName, sJndiName));
            }

            // Set list in the request
            p_Request.setAttribute("listMailFactories", al);
        }
        catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }

        // Forward to the jsp.
        return (p_Mapping.findForward("MimePart Factories"));
    }
}
