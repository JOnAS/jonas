package org.ow2.jonas.webapp.jonasadmin.jonasmqconnect;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;
import org.ow2.jonas.webapp.jonasadmin.jonasmqconnect.util.MqObjectNames;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class DestinationAddConfirmAction extends JonasBaseAction {

    public ActionForward executeAction(ActionMapping mapping, ActionForm form
            , HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        WhereAreYou oWhere = (WhereAreYou) request.getSession().getAttribute(WhereAreYou.
                SESSION_NAME);
        String serverName = oWhere.getCurrentJonasServerName();
        String domainName = oWhere.getCurrentDomainName();

        DestinationAddConfirmForm fBean = (DestinationAddConfirmForm) form;
        try {
            //ObjectName mbName = ObjectNames.getConnectorON();
            ObjectName mbName = MqObjectNames.getConnectorONByName(
                domainName,
                (String) m_Session.getAttribute("mqconnector"));
            boolean isTopic = fBean.getType().toLowerCase().equals("topic");
            Object[] params = {isTopic ? Boolean.TRUE : Boolean.FALSE,
                    fBean.getName(), fBean.getProprieties()};
            String[] signature = {"boolean", "java.lang.String",
                    "java.lang.String"};
            /*
             * FWA begin
             */

            /*
             * FWA end
             */
            JonasManagementRepr.invoke(mbName, "createDestination", params,
                    signature, serverName);
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(request, m_Errors);
            return (mapping.findForward("Global Error"));
        }
        return mapping.findForward("JonasMqConnectDestinationsAction");
    }
}