/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.cmi;

import java.io.Serializable;
import java.util.Set;

public class ClusterAttribute implements Serializable{
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    String name;
    Set<String> objectList;
    /**
     * @param name
     * @param objectList
     */
    public ClusterAttribute(String name, Set<String> objectList) {
        this.name = name;
        this.objectList = objectList;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @return the objectList
     */
    public Set<String> getObjectList() {
        return objectList;
    }
    /**
     * @param objectList the objectList to set
     */
    public void setObjectList(Set<String> objectList) {
        this.objectList = objectList;
    }




}
