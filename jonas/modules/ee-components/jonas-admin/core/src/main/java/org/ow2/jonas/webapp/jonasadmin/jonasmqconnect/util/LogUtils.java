package org.ow2.jonas.webapp.jonasadmin.jonasmqconnect.util;


/**
 *
 * @author Yacine TOUIL
 */
public class LogUtils {

    /**
     * debug or not
     */
    private static boolean DEBUG = false;

    /**
     * print msg.
     */
    public static void print(String msg) {
        if (DEBUG) {
            // TO DO
            System.out.println(msg);
        }
    }

    /**
     * Private constructor
     */
    private LogUtils() {
    }
}
