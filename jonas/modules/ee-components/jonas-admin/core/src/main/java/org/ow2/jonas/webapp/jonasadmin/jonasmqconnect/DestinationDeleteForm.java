package org.ow2.jonas.webapp.jonasadmin.jonasmqconnect;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

public class DestinationDeleteForm  extends ActionForm {
    /**
     * debug or not
     */
    private boolean debug = false;

    /**
     * printe debug msg
     * @param s msg
     */
    private void debug(String s) {
        if (debug)
            System.out.println(s);
    }

    /**
     * Selected destinations
     */
    private String[] selectedDestinations;

    /**
     * Selected destinations in string
     */
    private String destinationsStr;

    /**
     * Selected destinations
     */
    private ArrayList destinations;

    /**
     * remove Physical Destination ?
     */
    private String deletePhysicalDestination = null;

    /**
     * Selected destinations
     * @return the selected destinations
     */
    public String[] getSelectedDestinations() {
        debug("DestinationDeleteForm#getSelectedDestinations()");
        return selectedDestinations;
    }

    /**
     * selected destinations
     * @param selectedDestinations the selected destinations to set
     */
    public void setSelectedDestinations(String[] selectedDestinations) {
        debug("DestinationDeleteForm#setSelectedDestinations("
                + selectedDestinations
                + ")");
        this.selectedDestinations = selectedDestinations;
    }

    /**
     * destinations
     * @return the destinations
     */
    public ArrayList getDestinations() {
        debug("DestinationDeleteForm#getDestinations()");
        return destinations;
    }

    /**
     * @param destinations the selected destinations to set
     */
    public void setDestinations(ArrayList destinations) {
        debug("DestinationDeleteForm#setDestinations("
                + destinations + ")");
        this.destinations = destinations;
    }

    /**
     * @return the removePhysicalDestination
     */
    public String getDeletePhysicalDestination() {
        debug("DestinationDeleteForm#getDeletePhysicalDestination()");
        return deletePhysicalDestination;
    }

    /**
     * @param deletePhysicalDestination the deletePhysicalDestination to set
     */
    public void setDeletePhysicalDestination(String deletePhysicalDestination) {
        debug("DestinationDeleteForm#setDeletePhysicalDestination("
                + deletePhysicalDestination
                + ")");
        this.deletePhysicalDestination = deletePhysicalDestination;
    }

    /**
     * get DestinationsStr
     * @return the destinationsStr
     */
    public String getDestinationsStr() {
        debug("DestinationDeleteForm#getDestinationsStr()");
        return destinationsStr;
    }

    /**
     * set DestinationsStr
     * @param destinationsStr destinations String
     */
    public void setDestinationsStr(String destinationsStr) {
        debug("DestinationDeleteForm#setDestinationsStr("
                + destinationsStr
                + ")");
        this.destinationsStr = destinationsStr;
    }
}
