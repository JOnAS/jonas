package org.ow2.jonas.webapp.jonasadmin.monitoring;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * Tomcat cluster monitoring action
 * @author Adriana.Danes@bull.net
 */
public class TomcatClusterAction extends JonasBaseAction {

    public ActionForward executeAction(ActionMapping p_Mapping,
            ActionForm p_Form, HttpServletRequest p_Request,
            HttpServletResponse p_Response) throws IOException,
            ServletException {
        String domainName = m_WhereAreYou.getCurrentDomainName();
        // Get cluster name from the 'clust' parameter
        String name = p_Request.getParameter("clust");
        if (name == null) {
            addGlobalError(new Exception("TomcatClusterAction: clust parameter is null."));
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        // cluster type
        String type = "TomcatCluster";
        // Form used
        TomcatClusterForm oForm = (TomcatClusterForm) p_Form;
        oForm.setName(name);
        try {
            ObjectName on = JonasObjectName.cluster(domainName, name, type);
            oForm.setState(getStringAttribute(on, "State"));
            // Set specific parameters
            oForm.setMcastAddr(getStringAttribute(on, "McastAddr"));
            oForm.setMcastDropTime(getLongAttribute(on, "McastDropTime"));
            oForm.setMcastFrequency(getLongAttribute(on, "McastFrequency"));
            oForm.setMcastPort(getIntegerAttribute(on, "McastPort"));
            oForm.setMcastSocketTimeout(getIntegerAttribute(on, "McastSocketTimeout"));
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }

        // Forward to the jsp.
        return (p_Mapping.findForward("TomcatCluster"));
    }
}
