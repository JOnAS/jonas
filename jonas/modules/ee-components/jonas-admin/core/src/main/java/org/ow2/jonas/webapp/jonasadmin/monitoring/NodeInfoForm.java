package org.ow2.jonas.webapp.jonasadmin.monitoring;

import org.apache.struts.action.ActionForm;

public class NodeInfoForm extends ActionForm {

    private String serverName = "N/A";
    private String hostName = "N/A";
    private String JOnASVersion = "N/A";
    private String javaVendor = "N/A";
    private String javaVersion = "N/A";
    private String state = "N/A";
    private int currentUsedMemory = -1;
    private int currentTotalMemory = -1;
    private int allThreadsCount = -1;
    private String protocols = "N/A";
    private String connectionUrl = null;
    // infos jdk5
    private String loadCPU = "N/A";
    // Tomcat
    /**
     * Set to true if Tomcat info are provided for the node
     */
    private boolean tomcat = false;
    private int maxThreadsByConnectorTomcat = -1;
    private int currentThreadCountByConnectorTomcat = -1;
    private int currentThreadBusyByConnectorTomcat = -1;
    private long bytesReceivedByConnectorTomcat = -1;
    private long bytesSentByConnectorTomcat = -1;
    private int errorCountByConnectorTomcat = -1;
    private long processingTimeByConnectorTomcat = -1;
    private int requestCountByConnectorTomcat = -1;
    //Tx
    private boolean transaction = false;
    private int totalBegunTransactions = -1;
    private int totalCommittedTransactions = -1;
    private int totalCurrentTransactions = -1;
    private int totalExpiredTransactions = -1;
    private int totalRolledbackTransactions = -1;
    // Worker Pool
    private boolean workers = false;
    private int currentWorkerPoolSize = -1;
    private int maxWorkerPoolSize = -1;
    private int minWorkerPoolSize = -1;
    // JCA
    private boolean jcaConnection = false;
    private int connectionFailuresJCAConnection = -1;
    private int connectionLeaksJCAConnection = -1;
    private int currentBusyJCAConnection = -1;
    private int currentOpenedJCAConnection = -1;
    private int rejectedOpenJCAConnection = -1;
    private int servedOpenJCAConnection = -1;
    private int waiterCountJCAConnection = -1;
    private long waitingTimeJCAConnection = -1;
    // JDBC
    private boolean jdbcDatasource = false;
    private int connectionFailuresJDBCDatasource = -1;
    private int connectionLeaksJDBCDatasource = -1;
    private int currentBusyJDBCDatasource = -1;
    private int currentOpenedJDBCDatasource = -1;
    private int rejectedOpenJDBCDatasource = -1;
    private int servedOpenJDBCDatasource = -1;
    private int waiterCountJDBCDatasource = -1;
    private long waitingTimeJDBCDatasource = -1;
    // JMS
    private boolean jmsJoram = false;
    private int jmsQueuesNbMsgsReceiveSinceCreation = -1;
    private int jmsQueuesNbMsgsSendToDMQSinceCreation = -1;
    private int jmsQueuesNbMsgsDeliverSinceCreation = -1;
    private int jmsTopicsNbMsgsReceiveSinceCreation = -1;
    private int jmsTopicsNbMsgsSendToDMQSinceCreation = -1;
    private int jmsTopicsNbMsgsDeliverSinceCreation = -1;
    private int jmsNbMsgsSendToDMQSinceCreation = -1;
    // EJB
    private int currentNumberOfEntity = -1;
    private int currentNumberOfMDB = -1;
    private int currentNumberOfSBF = -1;
    private int currentNumberOfSBL = -1;
    private int currentNumberOfEJB = -1;

    public void setServerName(String val){
        this.serverName = val;
    }

    public String getServerName(){
        return this.serverName;
    }

    public void setJOnASVersion(String val){
        this.JOnASVersion = val;
    }

    public String getJOnASVersion(){
        return this.JOnASVersion;
    }

    public void setJavaVersion(String val){
        this.javaVersion = val;
    }

    public String getJavaVersion(){
        return this.javaVersion;
    }

    public void setHostName(String val){
        this.hostName = val;
    }

    public String getHostName(){
        return this.hostName;
    }

    public void setJavaVendor(String val){
        this.javaVendor = val;
    }

    public String getJavaVendor(){
        return this.javaVendor;
    }

    public void setState(String val){
        this.state = val;
    }

    public String getState(){
        return this.state;
    }

    public void setLoadCPU(String val){
        this.loadCPU = val;
    }

    public String getLoadCPU(){
        return this.loadCPU;
    }

    public void setCurrentUsedMemory(int val){
        this.currentUsedMemory = val;
    }

    public int getCurrentUsedMemory(){
        return this.currentUsedMemory;
    }

    public void setCurrentTotalMemory(int val){
        this.currentTotalMemory = val;
    }

    public int getCurrentTotalMemory(){
        return this.currentTotalMemory;
    }

    public void setAllThreadsCount(int val){
        this.allThreadsCount = val;
    }

    public int getAllThreadsCount(){
        return this.allThreadsCount;
    }

    public long getBytesReceivedByConnectorTomcat() {
        return bytesReceivedByConnectorTomcat;
    }

    public void setBytesReceivedByConnectorTomcat(long l) {
        this.bytesReceivedByConnectorTomcat = l;
    }

    public long getBytesSentByConnectorTomcat() {
        return bytesSentByConnectorTomcat;
    }

    public void setBytesSentByConnectorTomcat(long bytesSentByConnectorTomcat) {
        this.bytesSentByConnectorTomcat = bytesSentByConnectorTomcat;
    }

    public int getConnectionFailuresJCAConnection() {
        return connectionFailuresJCAConnection;
    }

    public void setConnectionFailuresJCAConnection(
            int connectionFailuresJCAConnection) {
        this.connectionFailuresJCAConnection = connectionFailuresJCAConnection;
    }

    public int getConnectionLeaksJCAConnection() {
        return connectionLeaksJCAConnection;
    }

    public void setConnectionLeaksJCAConnection(int connectionLeaksJCAConnection) {
        this.connectionLeaksJCAConnection = connectionLeaksJCAConnection;
    }

    public int getCurrentBusyJCAConnection() {
        return currentBusyJCAConnection;
    }

    public void setCurrentBusyJCAConnection(int currentBusyJCAConnection) {
        this.currentBusyJCAConnection = currentBusyJCAConnection;
    }

    public int getCurrentNumberOfEJB() {
        return currentNumberOfEJB;
    }

    public void setCurrentNumberOfEJB(int currentNumberOfEJB) {
        this.currentNumberOfEJB = currentNumberOfEJB;
    }

    public int getCurrentNumberOfMDB() {
        return currentNumberOfMDB;
    }

    public void setCurrentNumberOfMDB(int currentNumberOfMDB) {
        this.currentNumberOfMDB = currentNumberOfMDB;
    }

    public int getCurrentNumberOfSBF() {
        return currentNumberOfSBF;
    }

    public void setCurrentNumberOfSBF(int currentNumberOfSBF) {
        this.currentNumberOfSBF = currentNumberOfSBF;
    }

    public int getCurrentNumberOfSBL() {
        return currentNumberOfSBL;
    }

    public void setCurrentNumberOfSBL(int currentNumberOfSBL) {
        this.currentNumberOfSBL = currentNumberOfSBL;
    }

    public int getCurrentOpenedJCAConnection() {
        return currentOpenedJCAConnection;
    }

    public void setCurrentOpenedJCAConnection(int currentOpenedJCAConnection) {
        this.currentOpenedJCAConnection = currentOpenedJCAConnection;
    }

    public int getCurrentThreadBusyByConnectorTomcat() {
        return currentThreadBusyByConnectorTomcat;
    }

    public void setCurrentThreadBusyByConnectorTomcat(
            int currentThreadBusyByConnectorTomcat) {
        this.currentThreadBusyByConnectorTomcat = currentThreadBusyByConnectorTomcat;
    }

    public int getCurrentThreadCountByConnectorTomcat() {
        return currentThreadCountByConnectorTomcat;
    }

    public void setCurrentThreadCountByConnectorTomcat(
            int currentThreadCountByConnectorTomcat) {
        this.currentThreadCountByConnectorTomcat = currentThreadCountByConnectorTomcat;
    }

    public int getCurrentWorkerPoolSize() {
        return currentWorkerPoolSize;
    }

    public void setCurrentWorkerPoolSize(int currentWorkerPoolSize) {
        this.currentWorkerPoolSize = currentWorkerPoolSize;
    }

    public int getErrorCountByConnectorTomcat() {
        return errorCountByConnectorTomcat;
    }

    public void setErrorCountByConnectorTomcat(int errorCountByConnectorTomcat) {
        this.errorCountByConnectorTomcat = errorCountByConnectorTomcat;
    }

    public int getJmsQueuesNbMsgsDeliverSinceCreation() {
        return jmsQueuesNbMsgsDeliverSinceCreation;
    }

    public void setJmsQueuesNbMsgsDeliverSinceCreation(
            int jmsQueuesNbMsgsDeliverSinceCreation) {
        this.jmsQueuesNbMsgsDeliverSinceCreation = jmsQueuesNbMsgsDeliverSinceCreation;
    }

    public int getJmsQueuesNbMsgsReceiveSinceCreation() {
        return jmsQueuesNbMsgsReceiveSinceCreation;
    }

    public void setJmsQueuesNbMsgsReceiveSinceCreation(
            int jmsQueuesNbMsgsReceiveSinceCreation) {
        this.jmsQueuesNbMsgsReceiveSinceCreation = jmsQueuesNbMsgsReceiveSinceCreation;
    }

    public int getJmsQueuesNbMsgsSendToDMQSinceCreation() {
        return jmsQueuesNbMsgsSendToDMQSinceCreation;
    }

    public void setJmsQueuesNbMsgsSendToDMQSinceCreation(
            int jmsQueuesNbMsgsSendToDMQSinceCreation) {
        this.jmsQueuesNbMsgsSendToDMQSinceCreation = jmsQueuesNbMsgsSendToDMQSinceCreation;
    }

    public int getJmsTopicsNbMsgsDeliverSinceCreation() {
        return jmsTopicsNbMsgsDeliverSinceCreation;
    }

    public void setJmsTopicsNbMsgsDeliverSinceCreation(
            int jmsTopicsNbMsgsDeliverSinceCreation) {
        this.jmsTopicsNbMsgsDeliverSinceCreation = jmsTopicsNbMsgsDeliverSinceCreation;
    }

    public int getJmsTopicsNbMsgsReceiveSinceCreation() {
        return jmsTopicsNbMsgsReceiveSinceCreation;
    }

    public void setJmsTopicsNbMsgsReceiveSinceCreation(
            int jmsTopicsNbMsgsReceiveSinceCreation) {
        this.jmsTopicsNbMsgsReceiveSinceCreation = jmsTopicsNbMsgsReceiveSinceCreation;
    }

    public int getJmsTopicsNbMsgsSendToDMQSinceCreation() {
        return jmsTopicsNbMsgsSendToDMQSinceCreation;
    }

    public void setJmsTopicsNbMsgsSendToDMQSinceCreation(
            int jmsTopicsNbMsgsSendToDMQSinceCreation) {
        this.jmsTopicsNbMsgsSendToDMQSinceCreation = jmsTopicsNbMsgsSendToDMQSinceCreation;
    }

    public int getMaxThreadsByConnectorTomcat() {
        return maxThreadsByConnectorTomcat;
    }

    public void setMaxThreadsByConnectorTomcat(int maxThreadsByConnectorTomcat) {
        this.maxThreadsByConnectorTomcat = maxThreadsByConnectorTomcat;
    }

    public int getMaxWorkerPoolSize() {
        return maxWorkerPoolSize;
    }

    public void setMaxWorkerPoolSize(int maxWorkerPoolSize) {
        this.maxWorkerPoolSize = maxWorkerPoolSize;
    }

    public int getMinWorkerPoolSize() {
        return minWorkerPoolSize;
    }

    public void setMinWorkerPoolSize(int minWorkerPoolSize) {
        this.minWorkerPoolSize = minWorkerPoolSize;
    }

    public long getProcessingTimeByConnectorTomcat() {
        return processingTimeByConnectorTomcat;
    }

    public void setProcessingTimeByConnectorTomcat(long value) {
        processingTimeByConnectorTomcat = value;
    }

    public String getProtocols() {
        return protocols;
    }

    public void setProtocols(String protocols) {
        this.protocols = protocols;
    }

    public int getRejectedOpenJCAConnection() {
        return rejectedOpenJCAConnection;
    }

    public void setRejectedOpenJCAConnection(int rejectedOpenJCAConnection) {
        this.rejectedOpenJCAConnection = rejectedOpenJCAConnection;
    }

    public int getRequestCountByConnectorTomcat() {
        return requestCountByConnectorTomcat;
    }

    public void setRequestCountByConnectorTomcat(int requestCountByConnectorTomcat) {
        this.requestCountByConnectorTomcat = requestCountByConnectorTomcat;
    }

    public int getTotalBegunTransactions() {
        return totalBegunTransactions;
    }

    public void setTotalBegunTransactions(int totalBegunTransactions) {
        this.totalBegunTransactions = totalBegunTransactions;
    }

    public int getTotalCommittedTransactions() {
        return totalCommittedTransactions;
    }

    public void setTotalCommittedTransactions(int totalCommittedTransactions) {
        this.totalCommittedTransactions = totalCommittedTransactions;
    }

    public int getTotalCurrentTransactions() {
        return totalCurrentTransactions;
    }

    public void setTotalCurrentTransactions(int totalCurrentTransactions) {
        this.totalCurrentTransactions = totalCurrentTransactions;
    }

    public int getTotalExpiredTransactions() {
        return totalExpiredTransactions;
    }

    public void setTotalExpiredTransactions(int totalExpiredTransactions) {
        this.totalExpiredTransactions = totalExpiredTransactions;
    }

    public int getTotalRolledbackTransactions() {
        return totalRolledbackTransactions;
    }

    public void setTotalRolledbackTransactions(int totalRolledbackTransactions) {
        this.totalRolledbackTransactions = totalRolledbackTransactions;
    }

    public int getWaiterCountJCAConnection() {
        return waiterCountJCAConnection;
    }

    public void setWaiterCountJCAConnection(int waiterCountJCAConnection) {
        this.waiterCountJCAConnection = waiterCountJCAConnection;
    }

    public long getWaitingTimeJCAConnection() {
        return waitingTimeJCAConnection;
    }

    public void setWaitingTimeJCAConnection(long waitingTimeJCAConnection) {
        this.waitingTimeJCAConnection = waitingTimeJCAConnection;
    }

    public String getConnectionUrl() {
        return connectionUrl;
    }

    public void setConnectionUrl(String connectionUrl) {
        this.connectionUrl = connectionUrl;
    }

    public boolean getTomcat() {
        return tomcat;
    }

    public void setTomcat(boolean tomcat) {
        this.tomcat = tomcat;
    }

    public boolean getTransaction() {
        return transaction;
    }

    public void setTransaction(boolean transaction) {
        this.transaction = transaction;
    }

    public boolean getWorkers() {
        return workers;
    }

    public void setWorkers(boolean workers) {
        this.workers = workers;
    }

    public int getConnectionFailuresJDBCDatasource() {
        return connectionFailuresJDBCDatasource;
    }

    public void setConnectionFailuresJDBCDatasource(
            int connectionFailuresJDBCDatasource) {
        this.connectionFailuresJDBCDatasource = connectionFailuresJDBCDatasource;
    }

    public int getConnectionLeaksJDBCDatasource() {
        return connectionLeaksJDBCDatasource;
    }

    public void setConnectionLeaksJDBCDatasource(int connectionLeaksJDBCDatasource) {
        this.connectionLeaksJDBCDatasource = connectionLeaksJDBCDatasource;
    }

    public int getCurrentBusyJDBCDatasource() {
        return currentBusyJDBCDatasource;
    }

    public void setCurrentBusyJDBCDatasource(int currentBusyJDBCDatasource) {
        this.currentBusyJDBCDatasource = currentBusyJDBCDatasource;
    }

    public int getCurrentOpenedJDBCDatasource() {
        return currentOpenedJDBCDatasource;
    }

    public void setCurrentOpenedJDBCDatasource(int currentOpenedJDBCDatasource) {
        this.currentOpenedJDBCDatasource = currentOpenedJDBCDatasource;
    }

    public boolean getJcaConnection() {
        return jcaConnection;
    }

    public void setJcaConnection(boolean jcaConnection) {
        this.jcaConnection = jcaConnection;
    }

    public boolean getJdbcDatasource() {
        return jdbcDatasource;
    }

    public void setJdbcDatasource(boolean jdbcDatasource) {
        this.jdbcDatasource = jdbcDatasource;
    }

    public int getRejectedOpenJDBCDatasource() {
        return rejectedOpenJDBCDatasource;
    }

    public void setRejectedOpenJDBCDatasource(int rejectedOpenJDBCDatasource) {
        this.rejectedOpenJDBCDatasource = rejectedOpenJDBCDatasource;
    }

    public int getServedOpenJCAConnection() {
        return servedOpenJCAConnection;
    }

    public void setServedOpenJCAConnection(int servedOpenJCAConnection) {
        this.servedOpenJCAConnection = servedOpenJCAConnection;
    }

    public int getServedOpenJDBCDatasource() {
        return servedOpenJDBCDatasource;
    }

    public void setServedOpenJDBCDatasource(int servedOpenJDBCDatasource) {
        this.servedOpenJDBCDatasource = servedOpenJDBCDatasource;
    }

    public int getWaiterCountJDBCDatasource() {
        return waiterCountJDBCDatasource;
    }

    public void setWaiterCountJDBCDatasource(int waiterCountJDBCDatasource) {
        this.waiterCountJDBCDatasource = waiterCountJDBCDatasource;
    }

    public long getWaitingTimeJDBCDatasource() {
        return waitingTimeJDBCDatasource;
    }

    public void setWaitingTimeJDBCDatasource(long waitingTimeJDBCDatasource) {
        this.waitingTimeJDBCDatasource = waitingTimeJDBCDatasource;
    }

    public int getCurrentNumberOfEntity() {
        return currentNumberOfEntity;
    }

    public void setCurrentNumberOfEntity(int currentNumberOfEntity) {
        this.currentNumberOfEntity = currentNumberOfEntity;
    }

    public boolean getJmsJoram() {
        return jmsJoram;
    }

    public void setJmsJoram(boolean joramJms) {
        this.jmsJoram = joramJms;
    }

	public int getJmsNbMsgsSendToDMQSinceCreation() {
		return jmsNbMsgsSendToDMQSinceCreation;
	}

	public void setJmsNbMsgsSendToDMQSinceCreation(
			int jmsNbMsgsSendToDMQSinceCreation) {
		this.jmsNbMsgsSendToDMQSinceCreation = jmsNbMsgsSendToDMQSinceCreation;
	}
}
