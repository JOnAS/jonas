/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.jtm;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * Form used to present the Transaction Manager properties
 * @author Adriana Danes
 */
public final class JtmServiceStatisticForm extends ActionForm {

// ------------------------------------------------------------- Properties Variables
    private int currentTransactions = 0;
    private int begunTransactions = 0;
    private int commitedTransactions = 0;
    private int rollBackedTransactions = 0;
    private int expiredTransactions = 0;
// ------------------------------------------------------------- Properties Methods
    public int getCurrentTransactions() {
        return currentTransactions;
    }
    public int getBegunTransactions() {
        return begunTransactions;
    }
    public int getCommitedTransactions() {
        return commitedTransactions;
    }
    public int getRollBackedTransactions() {
        return rollBackedTransactions;
    }
    public int getExpiredTransactions() {
        return expiredTransactions;
    }
    public void setCurrentTransactions(int currentTransactions) {
        this.currentTransactions = currentTransactions;
    }
    public void setBegunTransactions(int begunTransactions) {
        this.begunTransactions = begunTransactions;
    }
    public void setCommitedTransactions(int commitedTransactions) {
        this.commitedTransactions = commitedTransactions;
    }
    public void setRollBackedTransactions(int rollBackedTransactions) {
        this.rollBackedTransactions = rollBackedTransactions;
    }
    public void setExpiredTransactions(int expiredTransactions) {
        this.expiredTransactions = expiredTransactions;
    }
// ------------------------------------------------------------- Public Methods

    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {
    }

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        return new ActionErrors();
    }
}
