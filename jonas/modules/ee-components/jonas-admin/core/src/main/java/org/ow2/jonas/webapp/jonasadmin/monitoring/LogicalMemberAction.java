package org.ow2.jonas.webapp.jonasadmin.monitoring;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class LogicalMemberAction extends JonasBaseAction {

	public ActionForward executeAction(ActionMapping p_Mapping,
			ActionForm p_Form, HttpServletRequest p_Request,
			HttpServletResponse p_Response) throws IOException,
			ServletException {
		// Form used
		WhereAreYou oWhere = (WhereAreYou) p_Request.getSession().getAttribute(
				WhereAreYou.SESSION_NAME);
		String sDomainName = oWhere.getCurrentDomainName();

		LogicalMemberForm oForm = (LogicalMemberForm) p_Form;
		try {


		} catch (Throwable t) {
			addGlobalError(t);
			saveErrors(p_Request, m_Errors);
			return (p_Mapping.findForward("Global Error"));
		}

		// Forward to the jsp.
		return (p_Mapping.findForward("LogicalMember"));
	}
}
