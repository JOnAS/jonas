/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2003-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.container;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import java.io.IOException;
import java.util.Map;

import javax.management.ObjectName;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;

/**
 * @author S. Ali Tokmen
 */
public class ApplyWebAppVirtualCatalinaAction extends BaseWebAppAction {

    private static String sDefaultForward = "ActionEditWebApp";

    @Override
    public ActionForward executeAction(final ActionMapping p_Mapping, final ActionForm p_Form,
            final HttpServletRequest p_Request, final HttpServletResponse p_Response) throws IOException, ServletException {

        // Default forward
        ActionForward oForward = null;

        // Identify the requested action
        WebAppVirtualCatalinaForm oForm = (WebAppVirtualCatalinaForm) p_Form;

        // Do the action
        try {
            oForward = p_Mapping.findForward(sDefaultForward);

            String onString = p_Request.getParameter("on");
            boolean refresh = false;
            if (onString == null) {
                onString = oForm.getObjectName();
                refresh = true;
            }
            ObjectName on = ObjectName.getInstance(onString);
            String pServer = m_WhereAreYou.getCurrentJonasServerName();
            final String[] opSignature = {"java.lang.String", "java.lang.String"};
            Map<String, String> contexts = (Map<String, String>) JonasManagementRepr.getAttribute(on, "Contexts", pServer);

            for (String context : contexts.keySet()) {
                String policy = p_Request.getParameter(context);
                if (policy == null) {
                    if (refresh) {
                        policy = (String) contexts.get(context);
                    }
                }
                if (policy != null) {
                    String[] opParams = {context, policy};
                    JonasManagementRepr.invoke(on, "rebindContext", opParams, opSignature, pServer);
                } 
            }
        } catch (Throwable t) {
            t.printStackTrace();
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            oForward = p_Mapping.findForward("Global Error");
        }

        // Next Forward
        return oForward;
    }
}
