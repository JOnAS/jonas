/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.webapp.jonasadmin.service.workcleaner;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

/**
 * Implement action for WC service management.
 * @author Adriana Danes
 */
public class EditWorkcleanerServiceAction extends JonasBaseAction {

    @Override
    public ActionForward executeAction(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request,
            final HttpServletResponse response) throws IOException, ServletException {

        // Form used
        WorkcleanerServiceForm oForm = (WorkcleanerServiceForm) form;

        // Force the node selected in tree
        m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_SERVER) + WhereAreYou.NODE_SEPARATOR
            + "services" + WhereAreYou.NODE_SEPARATOR + "workcleaner", true);

        String domainName = m_WhereAreYou.getCurrentDomainName();

        try {
            // Object name used
            ObjectName objectName = JonasObjectName.workCleaner(domainName);
            int period = getIntAttribute(objectName, "executePeriod");
            oForm.setPeriod(period);
        }
        catch (Throwable t) {
            addGlobalError(t);
            saveErrors(request, m_Errors);
            return (mapping.findForward(GLOBAL_ERROR_FORWARD));
        }

        // Forward to the jsp.
        return (mapping.findForward("Workcleaner Service"));
    }

}
