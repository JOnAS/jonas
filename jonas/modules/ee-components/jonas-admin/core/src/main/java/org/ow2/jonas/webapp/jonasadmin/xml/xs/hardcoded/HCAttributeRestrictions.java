/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.xml.xs.hardcoded;

import org.ow2.jonas.webapp.jonasadmin.xml.xs.AttributeRestrictions;

/**
 * An implementation of HCAttributeRestrictions where the restrictions are not
 * retrieved from the schema but hard coded inside the class.
 *
 * @author Gregory Lapouchnian
 * @author Patrick Smith
 */
public class HCAttributeRestrictions implements AttributeRestrictions {

    /** The name of this attribute */
    private String name;

    /** If this attribute is optional */
    private boolean isOptional;

    /** The fixed value for this attribute. */
    private String fixed;

    /**
     * Create a new hardcoded attribute restriction
     * @param name the name of this attribute
     * @param optional is this attribute optional
     * @param fixed the fixed value for this attribute
     */
    public HCAttributeRestrictions(String name, boolean optional, String fixed) {
        this.name = name;
        this.isOptional = optional;
        this.fixed = fixed;
    }

    /**
     * Get the fixed value for this attribute.
     * @return the fixed value for this attribute.
     */
    public String getFixed() {
        return fixed;
    }

    /**
     * Sets the fixed value for this attribute.
     * @param fixed the fixed value for this attribute.
     */
    public void setFixed(String fixed) {
        this.fixed = fixed;
    }

    /**
     * Returns if this attribute is optional.
     * @return if this attribute is optional or not.
     */
    public boolean isOptional() {
        return isOptional;
    }

    /**
     * Sets if this attribute is optional.
     * @param isOptional if this attribute is optional.
     */
    public void setOptional(boolean isOptional) {
        this.isOptional = isOptional;
    }

    /**
     * Return the name of the attribute.
     * @return the element of this attribute.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of this element.
     * @param name the name of this element.
     */
    public void setName(String name) {
        this.name = name;
    }
}
