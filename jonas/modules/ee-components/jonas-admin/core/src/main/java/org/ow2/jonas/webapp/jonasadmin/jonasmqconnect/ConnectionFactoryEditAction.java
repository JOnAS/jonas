package org.ow2.jonas.webapp.jonasadmin.jonasmqconnect;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;
import org.ow2.jonas.webapp.jonasadmin.jonasmqconnect.util.MqObjectNames;
import org.ow2.jonas.webapp.jonasadmin.jonasmqconnect.util.PropertiesComparator;
import org.ow2.jonas.webapp.jonasadmin.jonasmqconnect.util.PropertiesUtil;
import org.ow2.jonas.webapp.jonasadmin.jonasmqconnect.util.Property;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ConnectionFactoryEditAction extends JonasBaseAction {

    public ActionForward executeAction(ActionMapping mapping, ActionForm form
            , HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        ConnectionFactoryEditForm fBean = (ConnectionFactoryEditForm) form;
        String type = fBean.getType(); // CF,TCF,QCF
        String operation = fBean.getOperation();
        ArrayList properties = new ArrayList();

        WhereAreYou oWhere = (WhereAreYou) request.getSession().getAttribute(WhereAreYou.
                SESSION_NAME);
        String serverName = oWhere.getCurrentJonasServerName();
        String domainName = oWhere.getCurrentDomainName();

        /*
         *
         */
        String connector = "jonasmq";
        String currentConnector = request.getParameter("connector");
        if (currentConnector != null) {
            connector = currentConnector;

        } else {
            currentConnector = (String) m_Session.getAttribute("mqconnector");
            if (currentConnector != null) {
                connector = currentConnector;
            }
        }
        m_Session.setAttribute("mqconnector", connector);
        try {
            //ObjectName mbName = ObjectNames.getConnectorON();
            ObjectName mbName = MqObjectNames.getConnectorONByName(domainName, connector);

            //Class cfClass = null;
            String methodName = "";
            if (type.equals("TCF")) {
                /* FWA commented out
                cfClass = Thread.currentThread().getClass().
                  forName("com.ibm.mq.jms.MQTopicConnectionFactory");
                  */
                methodName = "getTcfProperty";
            } else if (type.equals("QCF")) {
                /* FWA commented out
                cfClass = Thread.currentThread().getClass().
                  forName("com.ibm.mq.jms.MQQueueConnectionFactory");
                  */
                methodName = "getQcfProperty";
            } else {
                /* FWA commented out
                cfClass = Thread.currentThread().getClass().
                  forName("com.ibm.mq.jms.MQConnectionFactory");
                  */
                methodName = "getCfProperty";
            }
            /*
             * FWA BEGIN
             */
            String[] propertiesToSkip = new String []{"Version", "HostName",
                    "Channel", "Port", "TransportType", "QueueManager", "Reference", "Class", "ConnTag"};
            properties = PropertiesUtil.getJMSObjectProperties(mbName, type, methodName, propertiesToSkip, serverName, domainName);

            /* FWA : commented out : properties = PropertiesUtil.getClassProperties(mbName, cfClass,
                    "get"+methodName, new String []{"Version", "HostName",
                    "Channel", "Port", "TransportType", "QueueManager"},serverName);
                    */
            /*
             * FWA END
             */

            if ("apply".equals(operation)) {
                for (int i = 0; i < properties.size(); i++) {
                    Property property = (Property) properties.get(i);
                    property.setValue(request.getParameter(property.getName()));
                }

                PropertiesUtil.setClassProperties(mbName, /* cfClass, */
                        "set" + methodName, properties, serverName);
                properties = PropertiesUtil.getJMSObjectProperties(mbName, type, methodName, propertiesToSkip, serverName, domainName);
                /* FWA commented out
                properties = PropertiesUtil.getClassProperties(mbName, cfClass,
                        "get"+methodName, new String []{"Channel", "HostName",
                        "Port", "TransportType", "QueueManager", "Version"},
                        serverName);
                        */
            }
            Collections.sort(properties, new PropertiesComparator());
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(request, m_Errors);
            return (mapping.findForward("Global Error"));
        }
        fBean.setProperties(properties);
        return mapping.findForward("JonasMqConnectConnectionFactoryEdit");
    }
}