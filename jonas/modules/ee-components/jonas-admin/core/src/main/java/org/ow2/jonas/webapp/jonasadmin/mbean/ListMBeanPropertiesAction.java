/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.mbean;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.mbean.MbeanItem;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * List of all Infos for a MBeans.
 *
 * @author Michel-Ange ANTON
 */

public final class ListMBeanPropertiesAction extends ListMBeanDetailsAction {

// --------------------------------------------------------- Public Methods
    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws IOException, ServletException {

        try {
            // Save current action
            setAction(ACTION_PROPERTIES);
            // Parameter
            String sSelect = p_Request.getParameter("select");

            // Create a request attribute with our collection of MBeans
            ArrayList list = new ArrayList();
            // Get all infos of a MBean
            ObjectName on = new ObjectName(sSelect);
            MbeanItem oItem = MbeanItem.build(on);

            // Force the node selected in tree when the direct is used (MBeans list)
            StringBuffer sbBranch = new StringBuffer("domain*mbeans");
            sbBranch.append(WhereAreYou.NODE_SEPARATOR);
            sbBranch.append(sSelect);
            m_WhereAreYou.selectNameNode(sbBranch.toString(), true);

            // Loop for all properties
            String sKey;
            String sValue;
            Iterator it = on.getKeyPropertyList().keySet().iterator();
            while (it.hasNext()) {
                sKey = (String) it.next();
                sValue = on.getKeyProperty(sKey);
                list.add(new ViewMBeanProperties(sKey, sValue));
            }
            // Set the beans
            p_Request.setAttribute("MBean", oItem);
            p_Request.setAttribute("MBeanProperties", list);
        }
        catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        // Forward to the corresponding display page
        return p_Mapping.findForward("List MBean Properties");
    }

// --------------------------------------------------------- Inner Classes

    public class ViewMBeanProperties {
        private String key;
        private String value;

        public ViewMBeanProperties() {

        }

        public ViewMBeanProperties(String ps_Key, String ps_Value) {
            setKey(ps_Key);
            setValue(ps_Value);
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

}
