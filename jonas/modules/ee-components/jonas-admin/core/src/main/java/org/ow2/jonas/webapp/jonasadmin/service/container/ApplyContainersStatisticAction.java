/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2008 Bull S.A.S.
 * Contact: jonas-team@objectweb.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.container;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;

/**
 * Handles update of monitoring configuration for session beans.
 *
 * @author S. Ali Tokmen, Malek Chahine
 */
public class ApplyContainersStatisticAction extends JonasBaseAction {

 // --------------------------------------------------------- Public Methods

    @Override
    public ActionForward executeAction(final ActionMapping pMapping, final ActionForm pForm
            , final HttpServletRequest pRequest, final HttpServletResponse pResponse)
    throws IOException, ServletException {
        try {
            ContainerForm oForm = (ContainerForm) pForm;
            ObjectName oObjectName = oForm.getObjectName();
            setBooleanAttribute(oObjectName, "monitoringEnabled", oForm.getMonitoringEnabled());
            setIntegerAttribute(oObjectName, "warningThreshold", oForm.getWarningThreshold());
            /**
             * FIXME: Setting averageProcessingTime is not yet supported EJBModule(ejb2)
             * implementation.
             */
            //setLongAttribute(oObjectName, "averageProcessingTime", oForm.getAverageProcessingTime());
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(pRequest, m_Errors);
            return (pMapping.findForward("Global Error"));
        }
        return (pMapping.findForward("ActionEditContainerStatistic"));
    }
}


