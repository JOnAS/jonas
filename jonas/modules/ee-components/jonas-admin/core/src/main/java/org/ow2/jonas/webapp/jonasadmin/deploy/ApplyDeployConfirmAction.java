/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.deploy;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.webapp.jonasadmin.JonasAdminJmx;

/**
 * @author Michel-Ange Anton
 */

public class ApplyDeployConfirmAction extends BaseDeployAction {

// --------------------------------------------------------- Public Methods

    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws IOException, ServletException {

        ArrayList al;
        String[] asParam = new String[1];
        String[] asSignature = new String[1];
        asSignature[0] = "java.lang.String";

        // Form used
        DeployForm oForm = (DeployForm) p_Form;

        boolean undeployOp = false;
        
        // Undeploy with Remove list
        al = oForm.getListRemove();
        for (int i = 0; i < al.size(); i++) {
            try {
                String element = al.get(i).toString();
                String elementPath = JonasAdminJmx.findDeployedPath(oForm.getListDeployed(), element);
                undeploy(elementPath);
                undeployOp = true;
                if (isDeployment(element)) {
                    ArrayList listDeploy = getListDeployedFiles();
                    oForm.setListDeploy(listDeploy);
                }
            } catch (Exception e) {
                m_Errors.add("error.undeploy", new ActionMessage("error.undeploy", al.get(i).toString()
                    , e.getMessage()));
                saveErrors(p_Request, m_Errors);
            }
        }

        // Call garbage collector after undeployment
        if (al.size() > 0) {
            try {
                runGC();
            } catch (Throwable t) {
                addGlobalError(t);
                saveErrors(p_Request, m_Errors);
            }
        }

        // Deploy with Add list
        al = oForm.getListAdd();
        for (int i = 0; i < al.size(); i++) {
            try {
                deploy(al.get(i).toString());
            } catch (Exception e) {
                m_Errors.add("error.deploy", new ActionMessage("error.deploy", al.get(i).toString()
                    , e.getMessage()));
                saveErrors(p_Request, m_Errors);
            }
        }

        // Build correct lists after deployment
        try {
            // Load the new list of deployed files
            oForm.setListDeployed(getListDeployedFiles());
            // Build Add list (with items in error status else list empty)
            ArrayList alAdd = new ArrayList(oForm.getListDeploy());
            if (undeployOp) {
                alAdd.removeAll(oForm.getListDeployed());
            } else {
                alAdd.removeAll(JonasAdminJmx.getDeployed(oForm.getListDeployed()));
            }
            oForm.setListAdd(alAdd);
            // Build Remove list (with items in error status else list empty)
            ArrayList alRemove = new ArrayList(oForm.getListUndeploy());
            alRemove.retainAll(oForm.getListDeployed());
            oForm.setListRemove(alRemove);
            // Confirm status
            oForm.setConfirm((m_Errors.size() == 0) && ((alAdd.size() > 0) || (alRemove.size() > 0)));
            oForm.setConfirm(false);
            // Refresh Tree in memory
            refreshTree(p_Request);
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
        }

        // Forward to the jsp.
        return (p_Mapping.findForward("Deploy Confirm"));
    }
}
