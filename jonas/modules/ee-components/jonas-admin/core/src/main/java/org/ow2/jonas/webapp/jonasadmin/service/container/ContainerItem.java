/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2009 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.container;

import org.ow2.jonas.lib.management.extensions.base.NameItem;

/**
 * Used by the JonasTreeBuilder.
 * @author Michel-Ange ANTON
 * @author Adriana Danes
 */
public class ContainerItem implements NameItem {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
// --------------------------------------------------------- Properties Variables

    private String path = null;
    private String nodeName = null;
    private String name = null;
    private String file = null;
    private String objectName = null;
    private String parentName = null;


// --------------------------------------------------------- Constructors

    public ContainerItem() {
    }

    public ContainerItem(final String p_File, final String p_Path) {
        setPath(p_Path);
        setFile(p_File);
    }

    public ContainerItem(final String p_File, final String p_Path, final String p_ObjectName, final String p_Name) {
        setPath(p_Path);
        setFile(p_File);
        setObjectName(p_ObjectName);
        setName(p_Name);
    }

    public ContainerItem(final String p_File, final String p_Path, final String p_NodeName, final String p_ObjectName, final String p_Name) {
        setPath(p_Path);
        setFile(p_File);
        setNodeName(p_NodeName);
        setName(p_Name);
        setObjectName(p_ObjectName);
    }

// --------------------------------------------------------- Properties Methods

    public String getPath() {
        return path;
    }

    public void setPath(final String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(final String nodeName) {
        this.nodeName = nodeName;
    }

    public String getFile() {
        return file;
    }

    public void setFile(final String file) {
        this.file = file;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(final String objectName) {
        this.objectName = objectName;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(final String parentName) {
        this.parentName = parentName;
    }
}
