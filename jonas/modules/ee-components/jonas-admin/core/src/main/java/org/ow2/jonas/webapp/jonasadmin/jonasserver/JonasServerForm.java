/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.jonasserver;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

/**
 * Form bean for the Jonas server form page.
 * @author Michel-Ange ANTON
 */

public final class JonasServerForm extends BasicJonasServerForm {

// ------------------------------------------------------------- Properties Variables

    /**
     * The text for the Jonas server version.
     */
    private String jonasVersion = null;

    /**
     * The text for the Jonas server name.
     */
    private String jonasName = null;

    /**
     * The text for the Jonas server ORB.
     */
    private String protocols = null;

    private String versions = null;

    /**
     * The info for the servlet container used.
     */
    private String serverServletContainerInfo = null;

    /**
     * JOnAS server state
     */
    private String state = null;

// ------------------------------------------------------------- Properties Methods

    public String getJonasVersion() {
        return jonasVersion;
    }

    public void setJonasVersion(String jonasVersion) {
        this.jonasVersion = jonasVersion;
    }

    public String getJonasName() {
        return jonasName;
    }

    public void setJonasName(String jonasName) {
        this.jonasName = jonasName;
    }

    /**
     * Get the Jonas server ORB.
     * @return
     */
    public String getProtocols() {
        return protocols;
    }

    /**
     * Set the Jonas server ORB.
     * @param orb
     */
    public void setProtocols(String protocols) {
        this.protocols = protocols;
    }

    public String getServerServletContainerInfo() {
        return serverServletContainerInfo;
    }

    public void setServerServletContainerInfo(String serverServletContainerInfo) {
        this.serverServletContainerInfo = serverServletContainerInfo;
    }
// ------------------------------------------------------------- Public Methods

    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {

        jonasName = null;
        jonasVersion = null;
        protocols = null;
        versions = null;
    }

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     * @return action errors
     */
    public ActionErrors validate(final ActionMapping mapping, final HttpServletRequest request) {
        return new ActionErrors();
    }

    /**
     * @return Returns the versions.
     */
    public String getVersions() {
        return versions;
    }

    /**
     * @param versions The versions to set.
     */
    public void setVersions(String versions) {
        this.versions = versions;
    }

    /**
     * @return the state information
     */
    public String getState() {
        return state;
    }

    /**
     * Set state information
     * @param state value
     */
    public void setState(String state) {
        this.state = state;
    }

}
