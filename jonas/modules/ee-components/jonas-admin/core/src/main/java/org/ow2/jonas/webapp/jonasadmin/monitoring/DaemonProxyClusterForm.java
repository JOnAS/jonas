/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.webapp.jonasadmin.monitoring;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts.action.ActionForm;

/**
 * @author eyindanga
 *
 */
public class DaemonProxyClusterForm extends ActionForm {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /**
     * the cluster daemon's remote hostname
     */
    private String hostName = "N/A";
    /**
     * @uml.property  name="state"
     */
    private String state="STOPPED";
    /**
     * @uml.property  name="name"
     */
    private String name="CD";
    /**
     * @uml.property  name="srvList"
     */
    private ArrayList conServer=null;
    /**
     * The connector name.
     */
    String connectorName=null;
    /**
     * The jmx url.
     */
    String jmxUrl=null;
    /**
     * The connector protocol.
     */
    String connectorProtocol=null;
    //Management attributtes for remote host monitoring
    //Runtime Mbean
    String runTimeSpecVendor = null;
    String runTimeSpecVersion = null;
    String runTimeVmName = null;
    String runTimeVmVendor = null;
    String runTimeVmVersion = null;
    //OperatingSystem mbean
    String operatingSystemAvailableProcessors = null;
    String operatingSystemName = null;
    String operatingSystemVersion = null;
    //dynamic remote host informations
    String classLoadingLoadedClassCount = null;
    String classLoadingUnloadedClassCount = null;
    String threadingThreadCount = null;
    String threadingTotalStartedThreadCount = null;
    String operatingSystemArch = null;
    //dynamic memroy attributes and values for the VM
    public  List<ClusterdAttribute> vmDynMemory = null;
        //for host remote memory infos
    public  List<ClusterdAttribute> dynThread = null;
//	  for loading infos
    public  List<ClusterdAttribute> dynLoading = null;
//	  for loading infos
    public  List<ClusterdAttribute> dynOperatingSystem = null;
    //dynamic memroy attributes and values for the OS
    public  List<ClusterdAttribute> osDynMemory = null;




    /**
     * @param val
     * @uml.property  name="hostName"
     */
    public void setHostName(final String val){
        this.hostName = val;
    }

    public String getHostName(){
        return this.hostName;
    }

    /**
     * @return
     * @uml.property  name="state"
     */
    public String getState() {
        return state;
    }

    /**
     * @param state
     * @uml.property  name="state"
     */
    public void setState(final String state) {
        this.state = state;
    }

    /**
     * @return
     * @uml.property  name="name"
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     * @uml.property  name="name"
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return
     * @uml.property  name="srvList"
     */
    public ArrayList getConServer() {
        return conServer;
    }

    /**
     * @param srvList
     * @uml.property  name="srvList"
     */
    public void setConServer(final ArrayList srvList) {
        this.conServer = srvList;
    }

    public String getConnectorName() {
        return connectorName;
    }

    public void setConnectorName(final String connectorName) {
        this.connectorName = connectorName;
    }

    public String getConnectorProtocol() {
        return connectorProtocol;
    }

    public void setConnectorProtocol(final String connectorProtocol) {
        this.connectorProtocol = connectorProtocol;
    }

    public String getJmxUrl() {
        return jmxUrl;
    }

    public void setJmxUrl(final String jmxurl) {
        this.jmxUrl = jmxurl;
    }

    public String getOperatingSystemAvailableProcessors() {
        return operatingSystemAvailableProcessors;
    }

    public void setOperatingSystemAvailableProcessors(
            final String operatingSystemAvailableProcessors) {
        this.operatingSystemAvailableProcessors = operatingSystemAvailableProcessors;
    }

    public String getOperatingSystemName() {
        return operatingSystemName;
    }

    public void setOperatingSystemName(final String operatingSystemName) {
        this.operatingSystemName = operatingSystemName;
    }

    public String getOperatingSystemVersion() {
        return operatingSystemVersion;
    }

    public void setOperatingSystemVersion(final String operatingSystemVersion) {
        this.operatingSystemVersion = operatingSystemVersion;
    }
    public String getOperatingSystemArch() {
        return operatingSystemArch;
    }

    public void setOperatingSystemArch(final String operatingSystemArch) {
        this.operatingSystemArch = operatingSystemArch;
    }
    public String getRunTimeSpecVendor() {
        return runTimeSpecVendor;
    }

    public void setRunTimeSpecVendor(final String runTimeSpecVendor) {
        this.runTimeSpecVendor = runTimeSpecVendor;
    }

    public String getRunTimeSpecVersion() {
        return runTimeSpecVersion;
    }

    public void setRunTimeSpecVersion(final String runTimeSpecVersion) {
        this.runTimeSpecVersion = runTimeSpecVersion;
    }

    public String getRunTimeVmName() {
        return runTimeVmName;
    }

    public void setRunTimeVmName(final String runTimeVmName) {
        this.runTimeVmName = runTimeVmName;
    }

    public String getRunTimeVmVendor() {
        return runTimeVmVendor;
    }

    public void setRunTimeVmVendor(final String runTimeVmVendor) {
        this.runTimeVmVendor = runTimeVmVendor;
    }

    public String getRunTimeVmVersion() {
        return runTimeVmVersion;
    }

    public void setRunTimeVmVersion(final String runTimeVmVersion) {
        this.runTimeVmVersion = runTimeVmVersion;
    }

    public String getClassLoadingLoadedClassCount() {
        return classLoadingLoadedClassCount;
    }

    public void setClassLoadingLoadedClassCount(final String classLoadingLoadedClassCount) {
        this.classLoadingLoadedClassCount = classLoadingLoadedClassCount;
    }

    public String getClassLoadingUnloadedClassCount() {
        return classLoadingUnloadedClassCount;
    }

    public void setClassLoadingUnloadedClassCount(
            final String classLoadingUnloadedClassCount) {
        this.classLoadingUnloadedClassCount = classLoadingUnloadedClassCount;
    }

    public String getThreadingThreadCount() {
        return threadingThreadCount;
    }

    public void setThreadingThreadCount(final String threadingThreadCount) {
        this.threadingThreadCount = threadingThreadCount;
    }

    public String getThreadingTotalStartedThreadCount() {
        return threadingTotalStartedThreadCount;
    }

    public void setThreadingTotalStartedThreadCount(
            final String threadingTotalStartedThreadCount) {
        this.threadingTotalStartedThreadCount = threadingTotalStartedThreadCount;
    }

    public List<ClusterdAttribute> getDynLoading() {
        return dynLoading;
    }

    public void setDynLoading(final List<ClusterdAttribute> dynLoading) {
        this.dynLoading = dynLoading;
    }

    public List<ClusterdAttribute> getDynOperatingSystem() {
        return dynOperatingSystem;
    }

    public void setDynOperatingSystem(final List<ClusterdAttribute> dynOperatingSystem) {
        this.dynOperatingSystem = dynOperatingSystem;
    }

    public List<ClusterdAttribute> getDynThread() {
        return dynThread;
    }

    public void setDynThread(final List<ClusterdAttribute> dynThread) {
        this.dynThread = dynThread;
    }

    public List<ClusterdAttribute> getOsDynMemory() {
        return osDynMemory;
    }

    public void setOsDynMemory(final List<ClusterdAttribute> osDynMemory) {
        this.osDynMemory = osDynMemory;
    }

    public List<ClusterdAttribute> getVmDynMemory() {
        return vmDynMemory;
    }

    public void setVmDynMemory(final List<ClusterdAttribute> vmDynMemory) {
        this.vmDynMemory = vmDynMemory;
    }


}
