/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.resource;

import javax.servlet.http.HttpServletRequest;

import org.ow2.jonas.webapp.jonasadmin.Jlists;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author Michel-Ange Anton
 */
public class DatasourcePropertiesForm extends ActionForm {

// --------------------------------------------------------- Constants

// --------------------------------------------------------- Properties variables

    private String name = null;
    private String datasourceName = null;
    private String datasourceDescription = null;
    private String datasourceUrl = null;
    private String datasourceClassname = null;
    private String datasourceUsername = null;
    private String datasourcePassword = null;
    private String datasourceMapper = null;
    private String jdbcConnteststmt = null;
    private String jdbcConnchecklevel = null;
    private String jdbcConnmaxage = null;
    private String jdbcMaxopentime = null;
    private String jdbcMaxconpool = null;
    private String jdbcMinconpool = null;
    private String jdbcMaxwaittime = null;
    private String jdbcMaxwaiters = null;
    private String jdbcSamplingperiod = null;
    private String jdbcAdjustperiod = null;

    private java.util.List booleanValues = Jlists.getBooleanValues();
    private java.util.List checkingLevels = Jlists.getJdbcConnectionCheckingLevels();
    private String action = "edit";

// --------------------------------------------------------- Public Methods

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        // Reset datas
        name = null;
        datasourceName = null;
        datasourceDescription = null;
        datasourceUrl = null;
        datasourceClassname = null;
        datasourceUsername = null;
        datasourcePassword = null;
        datasourceMapper = null;

        jdbcConnmaxage = "1440";
        jdbcMaxopentime = "1440";
        jdbcConnchecklevel = "1";
        jdbcConnteststmt = "SELECT 1";
        jdbcMinconpool = "0";
        jdbcMaxconpool = "-1";
        jdbcMaxwaittime = "10";
        jdbcMaxwaiters = "1000";
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors oErrors = new ActionErrors();
        if ((name == null) || (name.length() == 0)) {
            oErrors.add("name"
                , new ActionMessage("error.resource.datasource.properties.name.required"));
        }
        if ((datasourceName == null) || (datasourceName.length() == 0)) {
            oErrors.add("datasourceName"
                , new ActionMessage("error.resource.datasource.properties.datasourceName.required"));
        }
        if ((datasourceClassname == null) || (datasourceClassname.length() == 0)) {
            oErrors.add("datasourceClassname"
                , new ActionMessage("error.resource.datasource.properties.datasourceClassname.required"));
        }
        validateInteger(oErrors, jdbcConnmaxage, "jdbcConnmaxage"
            , "error.resource.datasource.properties.jdbcConnmaxage.numberformat");
        validateInteger(oErrors, jdbcMaxopentime, "jdbcMaxopentime"
            , "error.resource.datasource.properties.jdbcMaxopentime.numberformat");
        validateInteger(oErrors, jdbcMinconpool, "jdbcMinconpool"
            , "error.resource.datasource.properties.jdbcMinconpool.numberformat");
        validateInteger(oErrors, jdbcMaxconpool, "jdbcMaxconpool"
            , "error.resource.datasource.properties.jdbcMaxconpool.numberformat");
        validateInteger(oErrors, jdbcMaxwaittime, "jdbcMaxwaittime"
            , "error.resource.datasource.properties.jdbcMaxwaitime.numberformat");
        validateInteger(oErrors, jdbcMaxwaiters, "jdbcMaxwaiters"
            , "error.resource.datasource.properties.jdbcMaxwaiters.numberformat");
        return oErrors;
    }

    protected void validateInteger(ActionErrors p_Errors, String p_Value, String p_Tag
        , String p_ResError) {
        try {
            Integer.parseInt(p_Value);
        } catch (NumberFormatException e) {
            p_Errors.add(p_Tag, new ActionMessage(p_ResError));
        }
    }

// --------------------------------------------------------- Properties Methods

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDatasourceName() {
        return datasourceName;
    }

    public void setDatasourceName(String datasourceName) {
        this.datasourceName = datasourceName;
    }

    public String getDatasourceDescription() {
        return datasourceDescription;
    }

    public void setDatasourceDescription(String datasourceDescription) {
        this.datasourceDescription = datasourceDescription;
    }

    public String getDatasourceUrl() {
        return datasourceUrl;
    }

    public void setDatasourceUrl(String datasourceUrl) {
        this.datasourceUrl = datasourceUrl;
    }

    public String getDatasourceClassname() {
        return datasourceClassname;
    }

    public void setDatasourceClassname(String datasourceClassname) {
        this.datasourceClassname = datasourceClassname;
    }

    public String getDatasourceUsername() {
        return datasourceUsername;
    }

    public void setDatasourceUsername(String datasourceUsername) {
        this.datasourceUsername = datasourceUsername;
    }

    public String getDatasourcePassword() {
        return datasourcePassword;
    }

    public void setDatasourcePassword(String datasourcePassword) {
        this.datasourcePassword = datasourcePassword;
    }

    public String getDatasourceMapper() {
        return datasourceMapper;
    }

    public void setDatasourceMapper(String datasourceMapper) {
        this.datasourceMapper = datasourceMapper;
    }

    public String getJdbcConnmaxage() {
        return jdbcConnmaxage;
    }

    public void setJdbcConnmaxage(String s) {
        jdbcConnmaxage = s;
    }

    public String getJdbcMaxopentime() {
        return jdbcMaxopentime;
    }

    public void setJdbcMaxopentime(String s) {
        jdbcMaxopentime = s;
    }

    public String getJdbcConnchecklevel() {
        return jdbcConnchecklevel;
    }

    public void setJdbcConnchecklevel(String jdbcConnchecklevel) {
        this.jdbcConnchecklevel = jdbcConnchecklevel;
    }

    public String getJdbcConnteststmt() {
        return jdbcConnteststmt;
    }

    public void setJdbcConnteststmt(String jdbcConnteststmt) {
        this.jdbcConnteststmt = jdbcConnteststmt;
    }

    public String getJdbcMinconpool() {
        return jdbcMinconpool;
    }

    public void setJdbcMinconpool(String jdbcMinconpool) {
        this.jdbcMinconpool = jdbcMinconpool;
    }

    public String getJdbcMaxconpool() {
        return jdbcMaxconpool;
    }

    public void setJdbcMaxconpool(String jdbcMaxconpool) {
        this.jdbcMaxconpool = jdbcMaxconpool;
    }

    public String getJdbcMaxwaittime() {
        return jdbcMaxwaittime;
    }

    public void setJdbcMaxwaittime(String jdbcMaxwaittime) {
        this.jdbcMaxwaittime = jdbcMaxwaittime;
    }

    public String getJdbcMaxwaiters() {
        return jdbcMaxwaiters;
    }

    public void setJdbcMaxwaiters(String jdbcMaxwaiters) {
        this.jdbcMaxwaiters = jdbcMaxwaiters;
    }

    public String getJdbcSamplingperiod() {
        return jdbcSamplingperiod;
    }

    public void setJdbcSamplingperiod(String s) {
        this.jdbcSamplingperiod = s;
    }

    public String getJdbcAdjustperiod() {
        return jdbcAdjustperiod;
    }

    public void setJdbcAdjustperiod(String s) {
        this.jdbcAdjustperiod = s;
    }

    public java.util.List getBooleanValues() {
        return booleanValues;
    }

    public java.util.List getCheckingLevels() {
        return checkingLevels;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

}
