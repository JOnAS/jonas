/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Michel-Ange ANTON
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.clusterd;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.ow2.jonas.webapp.jonasadmin.monitoring.graph.utils.GraphUtils;

public class MemoryForm extends ActionForm {

// --------------------------------------------------------- Properties Variables

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private int numberOfMeasures = 120;
    private int range = 10;
    private int widthGraphic;
    private int heightGraphic;
    private int timerGraphic;
    //the server name, or clusterd name
    String ownerName = null;

// --------------------------------------------------------- Public Methods

    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */

    @Override
    public void reset(final ActionMapping mapping, final HttpServletRequest request) {
        numberOfMeasures = 120;
        range = 10;
        refreshGraphic();
    }

    public void refreshGraphic() {
        widthGraphic = GraphUtils.getWidth(numberOfMeasures);
        heightGraphic = GraphUtils.getHeight();
        timerGraphic = 1000 * (range + 2);
    }

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    @Override
    public ActionErrors validate(final ActionMapping mapping, final HttpServletRequest request) {
        ActionErrors oErrors = new ActionErrors();
        if (numberOfMeasures <= 1) {
            oErrors.add("numberOfMeasures"
                , new ActionMessage("error.monitoring.memory.numberOfMeasures"));
        }
        if (range < 10) {
            oErrors.add("range", new ActionMessage("error.monitoring.memory.range"));
        }
        //System.out.println("oErrors.size() = "+oErrors.size());
        if (oErrors.size() == 0) {
            //System.out.println("sizingGraphic");
            refreshGraphic();
        }
        return oErrors;
    }

// --------------------------------------------------------- Properties Methods

    public int getNumberOfMeasures() {
        return numberOfMeasures;
    }

    public void setNumberOfMeasures(final int numberOfMeasures) {
        this.numberOfMeasures = numberOfMeasures;
    }

    public int getRange() {
        return range;
    }

    public void setRange(final int range) {
        this.range = range;
    }

    public int getWidthGraphic() {
        return widthGraphic;
    }

    public void setWidthGraphic(final int widthGraphic) {
        this.widthGraphic = widthGraphic;
    }

    public int getHeightGraphic() {
        return heightGraphic;
    }

    public void setHeightGraphic(final int heightGraphic) {
        this.heightGraphic = heightGraphic;
    }

    public int getTimerGraphic() {
        return timerGraphic;
    }

    public void setTimerGraphic(final int timerGraphic) {
        this.timerGraphic = timerGraphic;
    }

    /**
     * @return the ownerName
     */
    public String getOwnerName() {
        return ownerName;
    }

    /**
     * @param ownerName the ownerName to set
     */
    public void setOwnerName(final String ownerName) {
        this.ownerName = ownerName;
    }

}