/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.ejb;

import javax.management.ObjectName;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;


/**
 * @author Michel-Ange ANTON
 */
public class EditEjbEntityAction extends EditEjbAction {

// --------------------------------------------------------- Public Methods

// --------------------------------------------------------- Protected Methods

    /**
     * Return a <code>EjbEntityForm</code> instance associate to the EJB.
     *
     * @return A form instance
     */
    protected EjbForm getEjbForm() {
        return new EjbEntityForm();
    }

    /**
     * Fill all infos of EJB Entity in the <code>EjbEntityForm</code> instance.
     *
     * @param p_Form Instance to fill
     * @param p_ObjectName Instance to get infos
     * @throws Exception
     */
    protected void fillEjbInfo(boolean ejb3, EjbForm p_Form, ObjectName p_ObjectName, String serverName)
        throws Exception {
        fillEjbGlobalInfo(ejb3, p_Form, p_ObjectName);

        EjbEntityForm oForm = (EjbEntityForm) p_Form;
        oForm.setPassivationTimeOut(getIntegerAttribute(p_ObjectName, "passivationTimeOut"));
        oForm.setInactivityTimeOut(getIntegerAttribute(p_ObjectName, "inactivityTimeOut"));
        oForm.setDeadlockTimeOut(getIntegerAttribute(p_ObjectName, "deadlockTimeOut"));
        oForm.setReadTimeOut(getIntegerAttribute(p_ObjectName, "readTimeOut"));
        oForm.setPersistency(getStringAttribute(p_ObjectName, "persistency"));
        oForm.setShared(getBooleanAttribute(p_ObjectName, "shared"));
        oForm.setPrefetch(getBooleanAttribute(p_ObjectName, "prefetch"));
        oForm.setHardLimit(getBooleanAttribute(p_ObjectName, "hardLimit"));
        oForm.setLockPolicy(getStringAttribute(p_ObjectName, "lockPolicy"));

        Integer[] aiEntityCounters = (Integer[]) JonasManagementRepr.getAttribute(p_ObjectName
            , "entityCounters", serverName);
        oForm.setUsedInTxInstance(aiEntityCounters[0].intValue());
        oForm.setUsedOutTxInstance(aiEntityCounters[1].intValue());
        oForm.setUnusedInstance(aiEntityCounters[2].intValue());
        oForm.setPassivatedInstance(aiEntityCounters[3].intValue());
        oForm.setRemovedInstance(aiEntityCounters[4].intValue());
        oForm.setPkNumber(aiEntityCounters[5].intValue());
    }

    /**
     * The global forward to go.
     *
     * @return Forward
     */
    protected String getEjbForward() {
        return "Ejb Entity";
    }
}
