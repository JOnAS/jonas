/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.security;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * @author Michel-Ange ANTON
 */
public class EditLdapRealmAction extends JonasBaseAction {

// --------------------------------------------------------- Public Methods

    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws IOException, ServletException {

        WhereAreYou oWhere = (WhereAreYou) p_Request.getSession().getAttribute(WhereAreYou.
                SESSION_NAME);
        String domainName = oWhere.getCurrentDomainName();

        String sResource = p_Request.getParameter("resource");
        String sAction = p_Request.getParameter("action");

        // Form used
        LdapRealmForm oForm = null;
        if (sResource != null) {
            // Editing a new realm
            oForm = new LdapRealmForm();
            m_Session.setAttribute("ldapRealmForm", oForm);
            oForm.setResource(sResource);
            oForm.setAction("apply");
        } else {
            // Creating
            if ((sAction != null) && (sAction.equals("create"))) {
                oForm = new LdapRealmForm();
                oForm.reset(p_Mapping, p_Request);
                m_Session.setAttribute("ldapRealmForm", oForm);
                oForm.setAction("create");
            }
            // Editing a realm in session (Show after apply or save)
            else {
                oForm = (LdapRealmForm) m_Session.getAttribute("ldapRealmForm");
                oForm.reset(p_Mapping, p_Request);
            }
        }

        // Populate
        try {
            // Populate only if action is 'apply'
            if (oForm.getAction().equals("apply")) {
                ObjectName oObjectName = JonasObjectName.securityLdapFactory(domainName, oForm.getResource());
                oForm.setName(getStringAttribute(oObjectName, "Name"));
                oForm.setAuthenticationMode(getStringAttribute(oObjectName, "AuthenticationMode"));
                oForm.setBaseDn(getStringAttribute(oObjectName, "BaseDN"));
                oForm.setInitialContextFactory(getStringAttribute(oObjectName
                    , "InitialContextFactory"));
                oForm.setLanguage(getStringAttribute(oObjectName, "Language"));
                oForm.setProviderUrl(getStringAttribute(oObjectName, "ProviderUrl"));
                oForm.setReferral(getStringAttribute(oObjectName, "Referral"));
                oForm.setRoleDn(getStringAttribute(oObjectName, "RoleDN"));
                oForm.setRoleNameAttribute(getStringAttribute(oObjectName, "RoleNameAttribute"));
                oForm.setRoleSearchFilter(getStringAttribute(oObjectName, "RoleSearchFilter"));
                oForm.setSecurityAuthentication(getStringAttribute(oObjectName
                    , "SecurityAuthentication"));
                oForm.setSecurityCredentials(getStringAttribute(oObjectName, "SecurityCredentials"));
                oForm.setSecurityPrincipal(getStringAttribute(oObjectName, "SecurityPrincipal"));
                oForm.setSecurityProtocol(getStringAttribute(oObjectName, "SecurityProtocol"));
                oForm.setStateFactories(getStringAttribute(oObjectName, "StateFactories"));
                oForm.setUserDn(getStringAttribute(oObjectName, "UserDN"));
                oForm.setUserPasswordAttribute(getStringAttribute(oObjectName
                    , "UserPasswordAttribute"));
                oForm.setUserRolesAttribute(getStringAttribute(oObjectName, "UserRolesAttribute"));
                oForm.setUserSearchFilter(getStringAttribute(oObjectName, "UserSearchFilter"));
                oForm.setAlgorithm(getStringAttribute(oObjectName, "Algorithm"));
                // Force the node selected in tree
                m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_SERVER)
                    + WhereAreYou.NODE_SEPARATOR + "security" + WhereAreYou.NODE_SEPARATOR
                    + "factory.ldap" + WhereAreYou.NODE_SEPARATOR + oForm.getName(), true);
            }
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        // Forward to the jsp.
        return (p_Mapping.findForward("Ldap Realm"));
    }

// --------------------------------------------------------- Protected Methods

}
