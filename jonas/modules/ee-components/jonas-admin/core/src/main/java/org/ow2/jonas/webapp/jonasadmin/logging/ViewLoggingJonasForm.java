/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Michel-Ange ANTON
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.logging;

import java.util.Date;

import org.apache.struts.action.ActionForm;

public class ViewLoggingJonasForm  extends ActionForm {

    /**
     * true if LogBuffer MBean exists
     */
    private boolean isBuffer = true;
    /**
     * severeCount
     */
    private long severeCount = 0;

    /**
     * warningCount
     */
    private long warningCount = 0;

    /**
     * infoCount
     */
    private long infoCount = 0;

    /**
     * recordCount
     */
    private long recordCount = 0;


    /**
     * latestStamp
     */
    private long latestStamp = 0;

    /**
     * recent
     */
    private String recent = null;

    /**
     * oldest
     */
    private long oldestStamp = 0;
    /**
     * get severeCount
     * @return
     */
    public long getSevereCount() {
        return severeCount;
    }

    /**
     * set severeCount
     * @param severeCount
     */
    public void setSevereCount(long severeCount) {
        this.severeCount = severeCount;
    }

    /**
     * get warningCount
     * @return
     */
    public long getWarningCount() {
        return warningCount;
    }

    /**
     * set warningCount
     * @param warningCount
     */
    public void setWarningCount(long warningCount) {
        this.warningCount = warningCount;
    }

    /**
     * get infoCount
     * @return
     */
    public long getInfoCount() {
        return infoCount;
    }

    /**
     * set infoCount
     * @param infoCount
     */
    public void setInfoCount(long infoCount) {
        this.infoCount = infoCount;
    }

    /**
     * get recordCount
     * @return
     */
    public long getRecordCount() {
        return recordCount;
    }

    /**
     * set recordCount
     * @param recordCount
     */
    public void setRecordCount(long recordCount) {
        this.recordCount = recordCount;
    }


    /**
     * get latestStamp
     * @return
     */
    public long getLatestStamp() {
        return latestStamp;
    }

    public Date getLatestDate() {
        return new Date(latestStamp);
    }

    public Date getOldestDate() {
        return new Date(oldestStamp);
    }
    /**
     * set latestStamp
     * @param recordCount
     */
    public void setLatestStamp(long stamp) {
        this.latestStamp = stamp;
    }

    /**
     * get recent
     * @return
     */
    public String getRecent() {
        return recent;
    }

    /**
     * set recent
     * @param recent
     */
    public void setRecent(String recent) {
        this.recent = recent;
    }

    /**
     * @return true if LogBuffer MBean exists
     */
    public boolean getBuffer() {
        return isBuffer;
    }

    /**
     * @param isBuffer is true if LogBuffer MBean exists
     */
    public void setBuffer(boolean isBuffer) {
        this.isBuffer = isBuffer;
    }

    public long getOldestStamp() {
        return oldestStamp;
    }

    public void setOldestStamp(long oldestStamp) {
        this.oldestStamp = oldestStamp;
    }

}
