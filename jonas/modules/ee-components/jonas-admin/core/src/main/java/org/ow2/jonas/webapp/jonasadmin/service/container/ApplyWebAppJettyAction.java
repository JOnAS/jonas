/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.container;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.webapp.jonasadmin.JonasAdminException;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Michel-Ange ANTON
 */
public class ApplyWebAppJettyAction extends BaseWebAppAction {

    private static String sDefaultForward = "ActionEditWebAppJetty";

// --------------------------------------------------------- Protected Variables

// --------------------------------------------------------- Public Methods

    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws IOException, ServletException {

        // Current server
        WhereAreYou oWhere = (WhereAreYou) p_Request.getSession().getAttribute(WhereAreYou.
                SESSION_NAME);
        String jonasServerName = oWhere.getCurrentJonasServerName();

        // Default forward
        ActionForward oForward = null;

        // Identify the requested action
        WebAppJettyForm oForm = (WebAppJettyForm) p_Form;

        // Populate
        try {
            // Stop the web app
            if ("stop".equals(oForm.getAction())) {
                oForward = stopWebApplication(oForm, p_Mapping, jonasServerName);
            }
            // Start the web app
            else if ("start".equals(oForm.getAction())) {
                oForward = startWebApplication(oForm, p_Mapping, jonasServerName);
            }
        } catch (JonasAdminException e) {
            // Build error
            m_Errors.add("webapp", new ActionMessage(e.getId()));
            saveErrors(p_Request, m_Errors);
            // Return to the current page
            oForward = new ActionForward(p_Mapping.getInput());
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            oForward = p_Mapping.findForward("Global Error");
        }

        // Next Forward
        return oForward;
    }


// --------------------------------------------------------- Protected Methods

    /**
     * Start the current web application.
     * @param p_Form The current form
     * @param p_Mapping The current mapping
     * @return The forward to go to the next page
     * @throws Exception
     */
    protected ActionForward startWebApplication(WebAppJettyForm p_Form, ActionMapping p_Mapping, String jonasServerName)
        throws Exception {
        try {
            ObjectName onContext = new ObjectName(p_Form.getObjectName());
            JonasManagementRepr.invoke(onContext, "start", null, null, jonasServerName);
        } catch (Throwable t) {
            throw new JonasAdminException("error.webapp.start");
        }
        return p_Mapping.findForward(sDefaultForward);
    }

    /**
     * Stop the current web application.
     * @param p_Form The current form
     * @param p_Mapping The current mapping
     * @return The forward to go to the next page
     * @throws Exception
     */
    protected ActionForward stopWebApplication(WebAppJettyForm p_Form, ActionMapping p_Mapping, String jonasServerName)
        throws Exception {
        try {
            ObjectName onContext = new ObjectName(p_Form.getObjectName());
            JonasManagementRepr.invoke(onContext, "stop", null, null, jonasServerName);
        } catch (Throwable t) {
            throw new JonasAdminException("error.webapp.stop");
        }
        return p_Mapping.findForward(sDefaultForward);
    }
}
