/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 * 
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.taglib;

import java.io.IOException;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.ow2.jonas.webapp.jonasadmin.xml.XMLToFormUtil;
import org.w3c.dom.Document;

/**
 * A custom JSP tag to display a form view of an XML deployment descriptor.
 * 
 * @author Gregory Lapouchnian
 * @author Patrick Smith
 */
public class XMLEditorTag extends TagSupport {

	// ----------------------------------------------------- Instance Variables

	/**
	 * Mapping between IDs and Nodes within the Document tree.
	 */
	private Map mapping;

	/**
	 * The document to display in this tag.
	 */
	private Document document;

	/**
	 * The name of the archive from which the XML file is extracted.
	 */
	private String fileName;
	
	/**
	 * The path within the archive to the XML file.
	 */
	private String pathName;

	// ------------------------------------------------------------- Properties

	// --------------------------------------------------------- Public Methods

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getPathName() {
		return pathName;
	}

	public void setPathName(String pathName) {
		this.pathName = pathName;
	}

	public Document getDocument() {
		return document;
	}

	public void setDocument(Document xmlDocument) {
		this.document = xmlDocument;
	}

	public Map getMapping() {
		return mapping;
	}

	public void setMapping(Map mapping) {
		this.mapping = mapping;
	}

	public int doEndTag() throws JspException {
		StringBuffer sb = new StringBuffer();

		// get the form's HTML
		XMLToFormUtil util = new XMLToFormUtil();
		String form = util.documentToForm(document, mapping);

		// make the last input field ID available to the JSP page
		pageContext.setAttribute("lastNum", new Integer(util.getLastId()));

		// display the form
		sb.append(form);

		// Render this element to our writer
		JspWriter out = pageContext.getOut();
		try {
			out.print(sb.toString());
		} catch (IOException e) {
			throw new JspException("Exception in " + getClass().getName()
					+ " doEndTag():" + e.toString());
		}
		return EVAL_PAGE;
	}

}
