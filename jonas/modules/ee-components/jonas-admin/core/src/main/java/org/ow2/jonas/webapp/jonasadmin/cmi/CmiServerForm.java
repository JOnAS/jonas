/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.cmi;

import org.apache.struts.action.ActionForm;

public class CmiServerForm extends ActionForm {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /**
     * The action to perform
     */
    private String action;
    /**
     * The server Url.
     */
    private String url;
    /**
     * The server name.
     */
    private String name;
    /**
     * The server Object Name.
     */
    private String ObjectName;
    /**
     * Load factor for the server.
     */
    private Integer loadFactor;
    /**
     * Server protocol
     */
    private String protocol = null;
    /**
     * True if the server is blacklisted.
     */
    private Boolean isBlackListed = false;


    public String getAction() {
        return action;
    }
    public void setAction(String action) {
        this.action = action;
    }
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getObjectName() {
        return ObjectName;
    }
    public void setObjectName(String objectName) {
        ObjectName = objectName;
    }
    public Integer getLoadFactor() {
        return loadFactor;
    }
    public void setLoadFactor(Integer loadFactor) {
        this.loadFactor = loadFactor;
    }
    public Boolean getIsBlackListed() {
        return isBlackListed;
    }
    public void setIsBlackListed(Boolean isBlackListed) {
        this.isBlackListed = isBlackListed;
    }
    public String getProtocol() {
        return protocol;
    }
    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }




}
