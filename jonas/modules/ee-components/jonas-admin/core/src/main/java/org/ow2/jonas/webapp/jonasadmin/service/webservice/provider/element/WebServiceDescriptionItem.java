/**
 *==============================================================================
 * Copyright (C) 2001-2005 by Allesta, LLC. All rights reserved.
 * Copyright (C) 2007 Bull S.A.S.
 *==============================================================================
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *==============================================================================
 * $Id$
 *==============================================================================
 */

package org.ow2.jonas.webapp.jonasadmin.service.webservice.provider.element;

import java.util.Vector;

import org.ow2.jonas.lib.management.extensions.base.NameItem;


/**
 * The webservice-description element defines a WSDL document file and the set
 * of Port components associated with the WSDL ports defined in the WSDL
 * document. There may be multiple webservice-descriptions defined within a
 * module. All WSDL file ports must have a corresponding port-component element
 * defined.
 *
 * @author Allesta, LLC
 */
public class WebServiceDescriptionItem implements NameItem {
    // ~ Instance fields
    // ------------------------------------------------------------------

    private String webServiceDescriptionName = null;

    private String wsdlFile = null;

    private String wsdlUrl = null;

    private String jaxRpcMappingFile = null;

    private Vector portComponents = new Vector();

    // ~ Constructors
    // ---------------------------------------------------------------------

    /**
     * Creates a new WebServiceDescription object.
     *
     * @param name
     *            DOCUMENT ME!
     * @param wsdlFile
     *            DOCUMENT ME!
     * @param wsdlUrl
     *            TODO
     * @param jaxrpcMappingFile
     *            DOCUMENT ME!
     * @param portComponent
     *            DOCUMENT ME!
     */
    public WebServiceDescriptionItem(String name, String wsdlFile, String wsdlUrl, String jaxrpcMappingFile,
            PortComponentItem portComponent) {
        this.webServiceDescriptionName = name;
        this.wsdlFile = wsdlFile;
        this.wsdlUrl = wsdlUrl;
        this.jaxRpcMappingFile = jaxrpcMappingFile;
        this.portComponents.add(portComponent);
    }

    /**
     * Creates a new WebServiceDescription object.
     *
     * @param name
     *            DOCUMENT ME!
     * @param wsdlFile
     *            DOCUMENT ME!
     * @param wsdlUrl
     *            TODO
     * @param jaxrpcMappingFile
     *            DOCUMENT ME!
     * @param portComponents
     *            DOCUMENT ME!
     */
    public WebServiceDescriptionItem(String name, String wsdlFile, String wsdlUrl, String jaxrpcMappingFile,
            Vector portComponents) {
        this.webServiceDescriptionName = name;
        this.wsdlFile = wsdlFile;
        this.wsdlUrl = wsdlUrl;
        this.jaxRpcMappingFile = jaxrpcMappingFile;
        this.portComponents = portComponents;
    }

    // ~ Methods
    // --------------------------------------------------------------------------

    /**
     * DOCUMENT ME!
     *
     * @param jaxrpcMappingFile
     *            DOCUMENT ME!
     */
    public void setJaxRpcMappingFile(String jaxrpcMappingFile) {
        this.jaxRpcMappingFile = jaxrpcMappingFile;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getJaxRpcMappingFile() {
        return jaxRpcMappingFile;
    }

    /**
     * DOCUMENT ME!
     *
     * @param index
     *            DOCUMENT ME!
     * @param portComponent
     *            DOCUMENT ME!
     */
    public void setPortComponent(int index, PortComponentItem portComponent) {
        if ((portComponents.size() == 0) || (index >= portComponents.size())) {
            portComponents.add(portComponent);

            return;
        }

        portComponents.insertElementAt(portComponent, index);
    }

    /**
     * DOCUMENT ME!
     *
     * @param index
     *            DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public PortComponentItem getPortComponent(int index) {
        return (PortComponentItem) portComponents.get(index);
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public int getPortComponentCount() {
        return portComponents.size();
    }

    /**
     * DOCUMENT ME!
     *
     * @param webserviceDescriptionName
     *            DOCUMENT ME!
     */
    public void setWebServiceDescriptionName(String webserviceDescriptionName) {
        this.webServiceDescriptionName = webserviceDescriptionName;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getWebServiceDescriptionName() {
        return webServiceDescriptionName;
    }

    /**
     * DOCUMENT ME!
     *
     * @param wsdlFile
     *            DOCUMENT ME!
     */
    public void setWsdlFile(String wsdlFile) {
        this.wsdlFile = wsdlFile;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getWsdlFile() {
        return wsdlFile;
    }

    /**
     * DOCUMENT ME!
     *
     * @param index
     *            DOCUMENT ME!
     */
    public void removePortComponent(int index) {
        portComponents.remove(index);
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String toString() {
        StringBuffer sb = new StringBuffer();

        sb.append(super.toString());
        sb.append("webServiceDescriptionName=");
        sb.append(getWebServiceDescriptionName());

        if (getWsdlFile() != null) {
            sb.append(", ");
            sb.append("wsdlFile=");
            sb.append(getWsdlFile());
        }

        if (getJaxRpcMappingFile() != null) {
            sb.append(", ");
            sb.append("jaxRpcMappingFile=");
            sb.append(getJaxRpcMappingFile());
        }

        sb.append(", portComponents=");
        sb.append(portComponents);

        return sb.toString();
    }

    public String getWsdlUrl() {
        return wsdlUrl;
    }

    public void setWsdlUrl(String wsdlUrl) {
        this.wsdlUrl = wsdlUrl;
    }

    public String getName() {
        return this.webServiceDescriptionName;
    }
}
