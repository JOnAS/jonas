/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.resource;

import java.io.IOException;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;
import org.ow2.jonas.webapp.jonasadmin.deploy.BaseDeployAction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * @author Michel-Ange ANTON
 */

public class ApplyDatasourcePropertiesAction extends BaseDeployAction {

// --------------------------------------------------------- Public Methods

    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws IOException, ServletException {

        // Current server
        String serverName = m_WhereAreYou.getCurrentJonasServerName();
        String domainName = m_WhereAreYou.getCurrentDomainName();

        // Form used
        DatasourcePropertiesForm oForm = (DatasourcePropertiesForm) p_Form;
        try {
            // populate and deploy (load resource)
            populate(oForm, domainName, serverName);
            // refresh tree
            m_WhereAreYou.setCurrentJonasDeploymentType(WhereAreYou.DEPLOYMENT_DATASOURCE);
            refreshTree(p_Request);
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        // Remove form
        m_Session.removeAttribute("datasourcePropertiesForm");
        // Forward to the jsp.
        return (p_Mapping.findForward("ActionListDatasources"));
    }

// --------------------------------------------------------- Protected Methods

    private static final String DEF_CLASSNAME = "no class name";
    private static final String DEF_URL = "no url";
    private static final String DEF_DESCRIPTION = "no desc";
    private static final String DEF_USERNAME = "";
    private static final String DEF_PASSWORD = "";
    private static final String DEF_FACTORY = "none";
    private static final String DEF_MAPPERNAME = "rdb";

    protected void populate(DatasourcePropertiesForm p_Form, String domainName, String serverName)
        throws Exception {
        Properties oProps = new Properties();
        setStringAttribute(oProps, "datasource.name", p_Form.getDatasourceName());
        setStringAttribute(oProps, "datasource.description", p_Form.getDatasourceDescription()
            , DEF_DESCRIPTION);
        setStringAttribute(oProps, "datasource.url", p_Form.getDatasourceUrl(), DEF_URL);
        setStringAttribute(oProps, "datasource.classname", p_Form.getDatasourceClassname()
            , DEF_CLASSNAME);
        setStringAttribute(oProps, "datasource.username", p_Form.getDatasourceUsername()
            , DEF_USERNAME);
        setStringAttribute(oProps, "datasource.password", p_Form.getDatasourcePassword()
            , DEF_PASSWORD);
        setStringAttribute(oProps, "datasource.mapper", p_Form.getDatasourceMapper()
            , DEF_MAPPERNAME);
        setStringAttribute(oProps, "jdbc.connmaxage", p_Form.getJdbcConnmaxage());
        setStringAttribute(oProps, "jdbc.maxopentime", p_Form.getJdbcMaxopentime());
        setStringAttribute(oProps, "jdbc.connchecklevel", p_Form.getJdbcConnchecklevel());
        setStringAttribute(oProps, "jdbc.connteststmt", p_Form.getJdbcConnteststmt());
        setStringAttribute(oProps, "jdbc.minconpool", p_Form.getJdbcMinconpool());
        setStringAttribute(oProps, "jdbc.maxconpool", p_Form.getJdbcMaxconpool());
        setStringAttribute(oProps, "jdbc.maxwaittime", p_Form.getJdbcMaxwaittime());
        setStringAttribute(oProps, "jdbc.maxwaiters", p_Form.getJdbcMaxwaiters());
        setStringAttribute(oProps, "jdbc.samplingperiod", p_Form.getJdbcSamplingperiod());
        setStringAttribute(oProps, "jdbc.adjustperiod", p_Form.getJdbcAdjustperiod());

        // Load Datasource
        Object[] aoParam = {
            p_Form.getName(), oProps, new Boolean(false)};
        String[] asSign_3 = {
            "java.lang.String", "java.util.Properties", "java.lang.Boolean"};
        JonasManagementRepr.invoke(JonasObjectName.databaseService(domainName), "loadDataSource", aoParam
            , asSign_3, serverName);

    }
}
