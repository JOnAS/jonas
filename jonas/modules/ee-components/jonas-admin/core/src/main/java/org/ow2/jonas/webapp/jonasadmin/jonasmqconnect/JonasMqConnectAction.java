package org.ow2.jonas.webapp.jonasadmin.jonasmqconnect;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;
import org.ow2.jonas.webapp.jonasadmin.jonasmqconnect.util.MqObjectNames;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class JonasMqConnectAction extends JonasBaseAction {

    public ActionForward executeAction(ActionMapping mapping, ActionForm form
            , HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException {

        JonasMqConnectForm fBean = (JonasMqConnectForm) form;
        String connector = fBean.getConnector();
        if (connector == null) {
            connector = (String) m_Session.getAttribute("mqconnector");
        } else {
            m_Session.setAttribute("mqconnector", connector);
        }
        String operation = fBean.getOperation();
        if (operation == null) {
            operation = "view";
            fBean.setOperation(operation);
        }


        WhereAreYou oWhere = (WhereAreYou) request.getSession().getAttribute(WhereAreYou.
                SESSION_NAME);
        String serverName = oWhere.getCurrentJonasServerName();
        String domain = oWhere.getCurrentDomainName();

        //domain*jmq1*jonasmqconnect*jonasmqconnector*fwaMQJca
        // Force the node selection in tree
        String nodeName = "domain" + WhereAreYou.NODE_SEPARATOR
                            + serverName + WhereAreYou.NODE_SEPARATOR
                            + "jonasmqconnect" + WhereAreYou.NODE_SEPARATOR
                            + "jonasmqconnector" + WhereAreYou.NODE_SEPARATOR
                            + connector;
        m_WhereAreYou.selectNameNode(nodeName, true);

        try {
            ObjectName mbName = MqObjectNames.getConnectorONByName(domain, connector);
            if ("apply".equals(operation)) {
                JonasManagementRepr.setAttribute(mbName, "HostName",
                    request.getParameter("hostname"), serverName);
                JonasManagementRepr.setAttribute(mbName, "Channel",
                    request.getParameter("channel"), serverName);
                JonasManagementRepr.setAttribute(mbName, "Port",
                    request.getParameter("port"), serverName);
                JonasManagementRepr.setAttribute(mbName, "QueueManager",
                    request.getParameter("queueManager"), serverName);
                JonasManagementRepr.setAttribute(mbName, "TransportType",
                    request.getParameter("transportType"), serverName);
            }
            if ("saveDestinationConfig".equals(operation)) {
                Object[] params = {};
                String[] signature = {};
                JonasManagementRepr.invoke(mbName, "saveDestinationConfig", params,
                        signature, serverName);
                fBean.setOperation("view");
            }
            if ("loadDestinationConfig".equals(operation)) {
                Object[] params = {};
                String[] signature = {};
                JonasManagementRepr.invoke(mbName, "loadDestinationConfig", params,
                        signature, serverName);
                fBean.setOperation("view");
            }
            if ("refresh".equals(operation)) {
                Object[] params = {};
                String[] signature = {};
                JonasManagementRepr.invoke(mbName, "restartAdminQm", params,
                        signature, serverName);
                fBean.setOperation("view");
            }
            String hostname = (String) JonasManagementRepr.getAttribute(mbName,
                    "HostName", serverName);
            String channel = (String) JonasManagementRepr.getAttribute(mbName,
                    "Channel", serverName);
            String port = (String) JonasManagementRepr.getAttribute(mbName,
                    "Port", serverName);
            String queueManager = (String) JonasManagementRepr.getAttribute(
                    mbName, "QueueManager", serverName);
            String transportType = (String) JonasManagementRepr.getAttribute(
                    mbName, "TransportType", serverName);
            ((JonasMqConnectForm) form).setHostname(hostname);
            ((JonasMqConnectForm) form).setChannel(channel);
            ((JonasMqConnectForm) form).setPort(port);
            ((JonasMqConnectForm) form).setQueueManager(queueManager);
            ((JonasMqConnectForm) form).setTransportType(transportType);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return mapping.findForward("JonasMqConnectStart");
    }
}