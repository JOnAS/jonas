/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.resource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Properties;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.management.extensions.container.ejb.EjbItem;
import org.ow2.jonas.lib.management.extensions.container.ejb.EjbItemByNameComparator;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;


/**
 * @author Michel-Ange ANTON
 */

public class EditResourceAdapterCFAction extends JonasBaseAction {

// --------------------------------------------------------- Public Methods

    @Override
    public ActionForward executeAction(final ActionMapping p_Mapping, final ActionForm p_Form
        , final HttpServletRequest p_Request, final HttpServletResponse p_Response)
        throws IOException, ServletException {

        // Selected resource adapter
        String sObjectName = p_Request.getParameter("select");

        ObjectName oObjectName = null;
        String domainName = null;
        String serverName = null;

        // Form used
        ResourceAdapterCFForm oForm = null;
        // Build a new form
        if (sObjectName != null) {
            String sPath = null;
            try {
                // Recreate ObjectName
                oObjectName = ObjectName.getInstance(sObjectName);
                domainName = oObjectName.getDomain();
                serverName = oObjectName.getKeyProperty("J2EEServer");
                // Build a new form
                oForm = new ResourceAdapterCFForm();
                oForm.reset(p_Mapping, p_Request);
                if (oObjectName != null) {
                    // Object name used
                    oForm.setOName(oObjectName);
                    oForm.setName(getStringAttribute(oObjectName, "jndiName"));
                    oForm.setPath(getStringAttribute(oObjectName, "fileName"));
                    oForm.setDescription(getStringAttribute(oObjectName, "description"));
                    oForm.setListProperties((Properties) JonasManagementRepr.getAttribute(oObjectName
                            , "properties", serverName));
                    populate(oForm, oObjectName, domainName, serverName);
                }
                m_Session.setAttribute("resourceAdapterCFForm", oForm);
            } catch (Throwable t) {
                addGlobalError(t);
                saveErrors(p_Request, m_Errors);
                return (p_Mapping.findForward("Global Error"));
            }
        } else {
            // Used last form in session
            oForm = (ResourceAdapterCFForm) m_Session.getAttribute("resourceAdapterCFForm");
        }

        ResourceAdapterForm raForm = (ResourceAdapterForm) m_Session.getAttribute("resourceAdapterForm");

        // Force the node selected in tree
        m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_SERVER) + WhereAreYou.NODE_SEPARATOR
            + "services" + WhereAreYou.NODE_SEPARATOR + "resourceAdapter"
            + WhereAreYou.NODE_SEPARATOR + raForm.getFile(), true);

        // Forward to the jsp.
        return (p_Mapping.findForward("Resource AdapterCF"));
    }

    protected void populate(final ResourceAdapterCFForm p_Form, final ObjectName oObjectName, final String domainName, final String serverName)
        throws Exception {

        p_Form.setJdbcConnCheckLevel(toStringIntegerAttribute(oObjectName, "jdbcConnCheckLevel"));
        p_Form.setConnMaxAge(toStringIntegerAttribute(oObjectName, "connMaxAge"));
        p_Form.setMaxOpentime(toStringIntegerAttribute(oObjectName, "maxOpentime"));
        p_Form.setJdbcTestStatement(getStringAttribute(oObjectName, "jdbcTestStatement"));
        p_Form.setMaxSize(toStringIntegerAttribute(oObjectName, "maxSize"));
        p_Form.setMinSize(toStringIntegerAttribute(oObjectName, "minSize"));
        p_Form.setInitSize(toStringIntegerAttribute(oObjectName, "initSize"));
        p_Form.setMaxWaitTime(toStringIntegerAttribute(oObjectName, "maxWaitTime"));
        p_Form.setMaxWaiters(toStringIntegerAttribute(oObjectName, "maxWaiters"));
        p_Form.setSamplingPeriod(toStringIntegerAttribute(oObjectName, "samplingPeriod"));
        p_Form.setPstmtMax(toStringIntegerAttribute(oObjectName, "pstmtMax"));
        p_Form.setJdbcConnSetUp(getBooleanAttribute(oObjectName, "jdbcConnSetUp"));

        // Build list of Ejb which used this jndiName
        ArrayList al = new ArrayList();
        String[] asParam = new String[1];
        String[] asSignature = new String[1];
        asSignature[0] = "java.lang.String";
        asParam[0] = p_Form.getName();
        if (JonasManagementRepr.isRegistered(JonasObjectName.ejbService(domainName), serverName)) {
            java.util.Iterator it = ((java.util.Set) JonasManagementRepr.invoke(JonasObjectName.
                ejbService(domainName), "getDataSourceDependence", asParam, asSignature, serverName)).iterator();
            while (it.hasNext()) {
                al.add(new EjbItem((ObjectName) it.next(), serverName));
            }
        }
        // Sort by name
        Collections.sort(al, new EjbItemByNameComparator());
        // Set list in form
        p_Form.setListUsedByEjb(al);
    }

}
