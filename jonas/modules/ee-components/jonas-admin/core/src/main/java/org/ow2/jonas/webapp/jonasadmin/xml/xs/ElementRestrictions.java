/**
 * JOnAS: Java(TM) Open Application Server Copyright (C) 2005 Bull S.A. Contact:
 * jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.xml.xs;

import java.util.List;

/**
 * An interface to represent the element restrictions of a schema.
 *
 * @author Gregory Lapouchnian
 * @author Patrick Smith
 */
public interface ElementRestrictions {

    /**
     * Return if this element is simple with no attributes
     * @return true if this element is a simple element and has no attributes.
     */
    boolean isSimpleAndNoAttributes();

    /**
     * Returns the list of children
     * @return the list of children.
     */
    List getChildren();

    /**
     * Sets the list of children to the given list.
     * @param children the list of children.
     */
    void setChildren(List children);

    /**
     * Is this element a sequence?
     * @return true if this element is a sequence.
     */
    boolean isSequence();

    /**
     * Sets if this element is a sequence.
     * @param isSequence the value to set.
     */
    void setSequence(boolean isSequence);

    /**
     * Returns if this element is complex.
     * @return true if this element is complex.
     */
    boolean isComplex();

    /**
     * sets if this element is complex.
     * @param isComplex the value to set.
     */
    void setComplex(boolean isComplex);

    /**
     * Gets the name of this element
     * @return the name of this element.
     */
    String getName();

    /**
     * sets the name of this element
     * @param name the value to set.
     */
    void setName(String name);

    /**
     * Return the list of attributes for this element.
     * @return the list of attributes for this element.
     */
    List getAttributes();

    /**
     * Sets the list of attributes for this element.
     * @param attributes the value to set.
     */
    void setAttributes(List attributes);

    /**
     * Get the min occurances of this element.
     * @return the min occurances of this element.
     */
    int getMinOccurs();

    /**
     * Sets the min occurrances for this element.
     * @param min the min occurrances for this element.
     */
    void setMinOccurs(int min);

    /**
     * return the max occurrances for this element.
     * @return the max occurances for this element.
     */
    int getMaxOccurs();

    /**
     * Sets the max occurances for this element.
     * @param min the value to set.
     */
    void setMaxOccurs(int min);
}
