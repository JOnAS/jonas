/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.common;

import java.util.Comparator;

import javax.management.ObjectName;

/**
 * @author Michel-Ange ANTON
 */
public class ObjectNameComparator implements Comparator {

// --------------------------------------------------------- Public Methods

    public int compare(Object p_O1, Object p_O2) {
        //return p_O1.toString().compareTo(p_O2.toString());
        ObjectName o1 = (ObjectName) p_O1;
        ObjectName o2 = (ObjectName) p_O2;
        int iRet = o1.getDomain().compareToIgnoreCase(o2.getDomain());
        if (iRet == 0) {
            iRet = o1.getDomain().compareTo(o2.getDomain());
            if (iRet == 0) {
                iRet = o1.toString().compareToIgnoreCase(o2.toString());
            }
        }
        return iRet;
    }

    public boolean equals(Object p_Obj) {
        if (p_Obj instanceof ObjectName) {
            return (compare(this, p_Obj) == 0);
        }
        return false;
    }
}