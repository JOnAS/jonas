/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 */

package org.ow2.jonas.webapp.jonasadmin.joramplatform;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * Form bean for the Joram platform form page.
 * @author Frederic MAISTRE
 * @author Adriana Danes
 */

public final class JoramPlatformForm extends ActionForm {

// ------------------------------------------------------------- Properties Variables

    /**
     * local server's id
     * */
    private String localServer = null;
    /**
     * remote servers ids
     */
    private String[] remoteServers = null;

    /**
     * remote {servers id, defaultDMQ id}
     */
    private ItemServer[] remoteItemServers = null;

    /**
     * True if a Joram adapter RAR is loaded on the current server
     */
    private boolean adapterLoaded = false;
    /**
     * a3servers.xml content
     */
    private String configuration = null;

    private int localPort;

    private String localHost = null;

    private int defaultThreshold;

    private String defaultDMQ = null;

    // The properties below are provided only if the Joram resource adapter is
    // deployed on the current server
    /**
     * RA version (Joram version also)
     */
    private String version = null;
    /**
     * Duration in seconds during which connecting is attempted (connecting
     * might take time if the server is temporarily not reachable); the 0 value
     * is set for connecting only once and aborting if connecting failed.
     */
    private String connectingTimerTxt = null;
    /**
     * Duration in seconds during which a JMS transacted (non XA) session might
     * be pending; above that duration the session is rolled back and closed;
     * the 0 value means "no timer".
     */
    private String txPendingTimerTxt = null;
    /**
     * Period in milliseconds between two ping requests sent by the client
     * connection to the server; if the server does not receive any ping
     * request during more than 2 * cnxPendingTimer, the connection is
     * considered as dead and processed as required.
     */
    private String cnxPendingTimerTxt = null;

    /**
     * True: when deploying, the adapter starts a collocated JORAM server.
     * False: when deploying, the adapter connects to a remote JORAM server.
     */
    private boolean collocatedServer;
    // Following properties usefull when starting a collocated server
    /**
     * Directory where the a3servers.xml and joramAdmin.xml files are locate
     */
    private String configDir = null;
    /**
     * Name of the file describing the administration tasks to perform
     * (by default joramAdmin.xml)
     */
    private String adminFile = null;
    /**
     * Persistence mode of the collocated JORAM server
     */
    private boolean persistentServer;
    /**
     * Identifier of the JORAM server to start
     */
    private String serverIdTxt = null;
    /**
     * Name of the JORAM server to start
     */
    private String serverName = null;
    // The following properties usefull when manageing a remote server
    /**
     * Name of the host where the JORAM server runs
     */
    private String hostName = null;
    /**
     * Port the JORAM server is listening on
     */
    private String serverPortTxt = null;

    private long timeOutToAbortRequest;
// ------------------------------------------------------------- Public Methods

    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    @Override
    public void reset(final ActionMapping mapping, final HttpServletRequest request) {
        version = null;
        localServer = null;
        remoteServers = null;
        adapterLoaded = false;
        configuration = null;
    }

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    @Override
    public ActionErrors validate(final ActionMapping mapping, final HttpServletRequest request) {
        return new ActionErrors();
    }

    /**
     * @return Returns the localServer.
     */
    public String getLocalServer() {
        return localServer;
    }

    /**
     * @param localServer The localServer to set.
     */
    public void setLocalServer(final String localServer) {
        this.localServer = localServer;
    }

    /**
     * @return Returns the remoteServers.
     */
    public String[] getRemoteServers() {
        return remoteServers;
    }

    /**
     * @param remoteServers The remoteServers to set.
     */
    public void setRemoteServers(final String[] remoteServers) {
        this.remoteServers = remoteServers;
    }

    public ItemServer[] getRemoteItemServers() {
        return remoteItemServers;
    }

    public void setRemoteItemServers(final ItemServer[] remoteItemServers) {
        this.remoteItemServers = remoteItemServers;
    }

    /**
     * @return Returns the isAdapterLoaded.
     */
    public boolean getAdapterLoaded() {
        return adapterLoaded;
    }

    /**
     * @param isAdapterLoaded The isAdapterLoaded to set.
     */
    public void setAdapterLoaded(final boolean adapterLoaded) {
        this.adapterLoaded = adapterLoaded;
    }

    /**
     * @return Returns the configuration.
     */
    public String getConfiguration() {
        return configuration;
    }

    /**
     * @param configuration The configuration to set.
     */
    public void setConfiguration(final String configuration) {
        this.configuration = configuration;
    }

    /**
     * @return Returns the version.
     */
    public String getVersion() {
        return version;
    }

    /**
     * @param version The version to set.
     */
    public void setVersion(final String version) {
        this.version = version;
    }

    /**
     * @return Returns the localPort.
     */
    public int getLocalPort() {
        return localPort;
    }

    /**
     * @param localPort The localPort to set.
     */
    public void setLocalPort(final int localPort) {
        this.localPort = localPort;
    }

    /**
     * @return Returns the defaultThreshold.
     */
    public int getDefaultThreshold() {
        return defaultThreshold;
    }

    /**
     * @param defaultThreshold The defaultThreshold to set.
     */
    public void setDefaultThreshold(final int defaultThreshold) {
        this.defaultThreshold = defaultThreshold;
    }

    /**
     * @return Returns the collocatedServer.
     */
    public boolean isCollocatedServer() {
        return collocatedServer;
    }

    /**
     * @param collocatedServer The collocatedServer to set.
     */
    public void setCollocatedServer(final boolean collocatedServer) {
        this.collocatedServer = collocatedServer;
    }

    /**
     * @return Returns the configDir.
     */
    public String getConfigDir() {
        return configDir;
    }

    /**
     * @param configDir The configDir to set.
     */
    public void setConfigDir(final String configDir) {
        this.configDir = configDir;
    }

    /**
     * @return Returns the adminFile.
     */
    public String getAdminFile() {
        return adminFile;
    }

    /**
     * @param adminFile The adminFile to set.
     */
    public void setAdminFile(final String adminFile) {
        this.adminFile = adminFile;
    }

    /**
     * @return Returns the persistentServer.
     */
    public boolean isPersistentServer() {
        return persistentServer;
    }

    /**
     * @param persistentServer The persistentServer to set.
     */
    public void setPersistentServer(final boolean persistentServer) {
        this.persistentServer = persistentServer;
    }

    /**
     * @return Returns the serverIdTxt.
     */
    public String getServerIdTxt() {
        return serverIdTxt;
    }

    /**
     * @param serverIdTxt The serverIdTxt to set.
     */
    public void setServerIdTxt(final String serverIdTxt) {
        this.serverIdTxt = serverIdTxt;
    }

    /**
     * @return Returns the serverName.
     */
    public String getServerName() {
        return serverName;
    }

    /**
     * @param serverName The serverName to set.
     */
    public void setServerName(final String serverName) {
        this.serverName = serverName;
    }

    /**
     * @return Returns the hostName.
     */
    public String getHostName() {
        return hostName;
    }

    /**
     * @param hostName The hostName to set.
     */
    public void setHostName(final String hostName) {
        this.hostName = hostName;
    }

    /**
     * @return Returns the serverPortTxt.
     */
    public String getServerPortTxt() {
        return serverPortTxt;
    }

    /**
     * @param serverPortTxt The serverPortTxt to set.
     */
    public void setServerPortTxt(final String serverPortTxt) {
        this.serverPortTxt = serverPortTxt;
    }

    /**
     * @return Returns the cnxPendingTimerTxt.
     */
    public String getCnxPendingTimerTxt() {
        return cnxPendingTimerTxt;
    }

    /**
     * @param cnxPendingTimerTxt The cnxPendingTimerTxt to set.
     */
    public void setCnxPendingTimerTxt(final String cnxPendingTimerTxt) {
        this.cnxPendingTimerTxt = cnxPendingTimerTxt;
    }

    /**
     * @return Returns the connectingTimerTxt.
     */
    public String getConnectingTimerTxt() {
        return connectingTimerTxt;
    }

    /**
     * @param connectingTimerTxt The connectingTimerTxt to set.
     */
    public void setConnectingTimerTxt(final String connectingTimerTxt) {
        this.connectingTimerTxt = connectingTimerTxt;
    }

    /**
     * @return Returns the txPendingTimerTxt.
     */
    public String getTxPendingTimerTxt() {
        return txPendingTimerTxt;
    }

    /**
     * @param txPendingTimerTxt The txPendingTimerTxt to set.
     */
    public void setTxPendingTimerTxt(final String txPendingTimerTxt) {
        this.txPendingTimerTxt = txPendingTimerTxt;
    }

    /**
     * @return Returns the localHost.
     */
    public String getLocalHost() {
        return localHost;
    }

    /**
     * @param localHost The localHost to set.
     */
    public void setLocalHost(final String localHost) {
        this.localHost = localHost;
    }

    /**
     * @return Returns the timeOutToAbortRequest.
     */
    public long getTimeOutToAbortRequest() {
        return timeOutToAbortRequest;
    }

    /**
     * @param timeOutToAbortRequest The timeOutToAbortRequest to set.
     */
    public void setTimeOutToAbortRequest(final long timeOutToAbortRequest) {
        this.timeOutToAbortRequest = timeOutToAbortRequest;
    }

    /**
     * @return Returns the defaultDMQ.
     */
    public String getDefaultDMQ() {
        return defaultDMQ;
    }

    /**
     * @param defaultDMQ The defaultDMQ to set.
     */
    public void setDefaultDMQ(final String defaultDMQ) {
        this.defaultDMQ = defaultDMQ;
    }
}
