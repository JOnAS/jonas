/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.clusterd;

import org.apache.struts.action.ActionForm;

/**
 * @author eyindanga
 *
 */
public class ClusterdServerForm extends ActionForm {


    /**
     *
     */
    private static final long serialVersionUID = 5520164966947545047L;
    String name = null;
    String state = null;
    String javaHome = null;
    String jonasRoot = null;
    String jonasBase = null;
    String description = null;
    String xprem = null;
    String autoBoot = null;
    //current cluster daemon name
    String cdName = null;


    public String getJavaHome() {
        return javaHome;
    }
    public void setJavaHome(final String javaHome) {
        this.javaHome = javaHome;
    }
    public String getJonasBase() {
        return jonasBase;
    }
    public void setJonasBase(final String jonasBase) {
        this.jonasBase = jonasBase;
    }
    public String getJonasRoot() {
        return jonasRoot;
    }
    public void setJonasRoot(final String jonasRoot) {
        this.jonasRoot = jonasRoot;
    }
    public String getName() {
        return name;
    }
    public void setName(final String name) {
        this.name = name;
    }
    public String getState() {
        return state;
    }
    public void setState(final String state) {
        this.state = state;
    }
    public String getAutoBoot() {
        return autoBoot;
    }
    public void setAutoBoot(final String autoBoot) {
        this.autoBoot = autoBoot;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(final String description) {
        this.description = description;
    }
    /**
     *
     * @return extra parameter
     */
    public String getXprem() {
        return xprem;
    }
    /**
     * set extra parameter
     * @param xprem
     */
    public void setXprem(final String xprem) {
        this.xprem = xprem;
    }
    /**
     * @return the cdName
     */
    public String getCdName() {
        return cdName;
    }
    /**
     * @param cdName the cdName to set
     */
    public void setCdName(final String cdName) {
        this.cdName = cdName;
    }

}
