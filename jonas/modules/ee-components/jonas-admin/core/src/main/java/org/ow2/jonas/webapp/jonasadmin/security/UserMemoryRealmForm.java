/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Michel-Ange ANTON
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.security;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.ow2.jonas.webapp.jonasadmin.Jlists;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class UserMemoryRealmForm extends ActionForm {

// --------------------------------------------------------- Constants

// --------------------------------------------------------- Properties variables

    private String action = null;
    private String user = null;
    private String password = null;
    private String confirmPassword = null;
    private java.util.ArrayList listGroupsUser = new ArrayList();
    private java.util.ArrayList listGroupsRealm = new ArrayList();
    private java.util.ArrayList listGroupsUsed = new ArrayList();
    private java.util.ArrayList listGroupsNotused = new ArrayList();
    private java.util.ArrayList listRolesUser = new ArrayList();
    private java.util.ArrayList listRolesRealm = new ArrayList();
    private java.util.ArrayList listRolesUsed = new ArrayList();
    private java.util.ArrayList listRolesNotused = new ArrayList();
    private String groupsUsed = null;
    private String groupsNotused = null;
    private String rolesUsed = null;
    private String rolesNotused = null;
    private String[] groupsNotusedSelected = new String[0];
    private String[] groupsUsedSelected = new String[0];
    private String[] rolesNotusedSelected = new String[0];
    private String[] rolesUsedSelected = new String[0];

// --------------------------------------------------------- Public Methods

    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        password = null;
        confirmPassword = null;
        groupsUsed = null;
        groupsNotused = null;
        rolesUsed = null;
        rolesNotused = null;
        // Mandatory !
        groupsNotusedSelected = new String[0];
        groupsUsedSelected = new String[0];
        rolesNotusedSelected = new String[0];
        rolesUsedSelected = new String[0];
    }

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors oErrors = new ActionErrors();
        // Create errors
        if (action.equals("create") == true) {
            user = user.trim();
            if (user.length() == 0) {
                oErrors.add("user"
                    , new ActionMessage("error.security.factory.memory.realm.user.name.required"));
            }
            if (password.length() == 0) {
                oErrors.add("password"
                    , new ActionMessage("error.security.factory.memory.realm.user.password.required"));
            }
            if (confirmPassword.length() == 0) {
                oErrors.add("password"
                    , new ActionMessage("error.security.factory.memory.realm.user.confirmPassword.required"));
            }
        }
        // Create and apply errors (but this test exists only to not overload the errors)
        if (oErrors.size() == 0) {
            if (password.length() > 0) {
                if (!password.equals(confirmPassword)) {
                    oErrors.add("password"
                        , new ActionMessage("error.security.factory.memory.realm.user.password.change.different"));
                }
            }
            if ((confirmPassword.length() > 0) && (password.length() == 0)) {
                oErrors.add("password"
                    , new ActionMessage("error.security.factory.memory.realm.user.password.change.required"));
            }
        }
        // Replace the elements in their good place
        if (oErrors.size() > 0) {
            listRolesUsed = Jlists.getArrayList(rolesUsed, Jlists.SEPARATOR);
            listRolesNotused = Jlists.getArrayList(rolesNotused, Jlists.SEPARATOR);
            listGroupsUsed = Jlists.getArrayList(groupsUsed, Jlists.SEPARATOR);
            listGroupsNotused = Jlists.getArrayList(groupsNotused, Jlists.SEPARATOR);
        }
        return oErrors;
    }

// --------------------------------------------------------- Properties Methods

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public java.util.ArrayList getListGroupsUser() {
        return listGroupsUser;
    }

    public void setListGroupsUser(java.util.ArrayList listGroupsUser) {
        this.listGroupsUser = listGroupsUser;
    }

    public java.util.ArrayList getListGroupsRealm() {
        return listGroupsRealm;
    }

    public void setListGroupsRealm(java.util.ArrayList listGroupsRealm) {
        this.listGroupsRealm = listGroupsRealm;
    }

    public java.util.ArrayList getListGroupsUsed() {
        return listGroupsUsed;
    }

    public void setListGroupsUsed(java.util.ArrayList listGroupsUsed) {
        this.listGroupsUsed = listGroupsUsed;
    }

    public java.util.ArrayList getListGroupsNotused() {
        return listGroupsNotused;
    }

    public void setListGroupsNotused(java.util.ArrayList listGroupsNotused) {
        this.listGroupsNotused = listGroupsNotused;
    }

    public java.util.ArrayList getListRolesUser() {
        return listRolesUser;
    }

    public void setListRolesUser(java.util.ArrayList listRolesUser) {
        this.listRolesUser = listRolesUser;
    }

    public java.util.ArrayList getListRolesRealm() {
        return listRolesRealm;
    }

    public void setListRolesRealm(java.util.ArrayList listRolesRealm) {
        this.listRolesRealm = listRolesRealm;
    }

    public java.util.ArrayList getListRolesUsed() {
        return listRolesUsed;
    }

    public void setListRolesUsed(java.util.ArrayList listRolesUsed) {
        this.listRolesUsed = listRolesUsed;
    }

    public java.util.ArrayList getListRolesNotused() {
        return listRolesNotused;
    }

    public void setListRolesNotused(java.util.ArrayList listRolesNotused) {
        this.listRolesNotused = listRolesNotused;
    }

    public String getGroupsUsed() {
        return groupsUsed;
    }

    public void setGroupsUsed(String groupsUsed) {
        this.groupsUsed = groupsUsed;
    }

    public String getGroupsNotused() {
        return groupsNotused;
    }

    public void setGroupsNotused(String groupsNotused) {
        this.groupsNotused = groupsNotused;
    }

    public String getRolesUsed() {
        return rolesUsed;
    }

    public void setRolesUsed(String rolesUsed) {
        this.rolesUsed = rolesUsed;
    }

    public String getRolesNotused() {
        return rolesNotused;
    }

    public void setRolesNotused(String rolesNotused) {
        this.rolesNotused = rolesNotused;
    }

    public String[] getGroupsNotusedSelected() {
        return groupsNotusedSelected;
    }

    public void setGroupsNotusedSelected(String[] groupsNotusedSelected) {
        this.groupsNotusedSelected = groupsNotusedSelected;
    }

    public String[] getGroupsUsedSelected() {
        return groupsUsedSelected;
    }

    public void setGroupsUsedSelected(String[] groupsUsedSelected) {
        this.groupsUsedSelected = groupsUsedSelected;
    }

    public String[] getRolesNotusedSelected() {
        return rolesNotusedSelected;
    }

    public void setRolesNotusedSelected(String[] rolesNotusedSelected) {
        this.rolesNotusedSelected = rolesNotusedSelected;
    }

    public String[] getRolesUsedSelected() {
        return rolesUsedSelected;
    }

    public void setRolesUsedSelected(String[] rolesUsedSelected) {
        this.rolesUsedSelected = rolesUsedSelected;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

}