/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.jonasserver;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;


/**
 * @author  Michel-Ange ANTON
 */

public class EditJmxServerAction extends JonasBaseAction {

// --------------------------------------------------------- Public Methods
    /**
     * Execute action for JMX service management
     * @param pMapping mapping info
     * @param pForm form object
     * @param pRequest HTTP request
     * @param pResponse HTTP response
     *
     * @return An <code>ActionForward</code> instance or <code>null</code>
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */
    @Override
    public ActionForward executeAction(final ActionMapping pMapping, final ActionForm pForm
        , final HttpServletRequest pRequest, final HttpServletResponse pResponse)
        throws IOException, ServletException {

        String serverName = m_WhereAreYou.getCurrentJonasServerName();
        String domainName = m_WhereAreYou.getCurrentDomainName();

        // Force the node selected in tree
        m_WhereAreYou.selectNameNode("domain"
                + WhereAreYou.NODE_SEPARATOR
                + serverName, true);

        // Form used
        JmxServerForm oForm = (JmxServerForm) pForm;
        try {
            // Object name used
            ObjectName oObjectName = JonasObjectName.jmxService();
            // Copy scalar properties
            oForm.setMBeanServerId(getStringAttribute(oObjectName, "MBeanServerId"));
            oForm.setSpecificationName(getStringAttribute(oObjectName, "SpecificationName"));
            oForm.setSpecificationVersion(getStringAttribute(oObjectName, "SpecificationVersion"));
            oForm.setSpecificationVendor(getStringAttribute(oObjectName, "SpecificationVendor"));
            oForm.setImplementationName(getStringAttribute(oObjectName, "ImplementationName"));
            oForm.setImplementationVersion(getStringAttribute(oObjectName, "ImplementationVersion"));
            oForm.setImplementationVendor(getStringAttribute(oObjectName, "ImplementationVendor"));
            if (JonasManagementRepr.isRegistered(JonasObjectName.webContainerService(domainName), serverName)) {
                oForm.setPresentServletContainer(true);
            } else {
                oForm.setPresentServletContainer(false);
            }
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(pRequest, m_Errors);
            return (pMapping.findForward("Global Error"));
        }

        // Forward to the jsp.
        return (pMapping.findForward("JmxServer"));
    }

}
