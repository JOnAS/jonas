/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.jonasserver;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.management.javaee.J2eeObjectName;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.management.extensions.base.api.ManagementException;
import org.ow2.jonas.webapp.jonasadmin.JonasAdminJmx;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;


/**
 * @author Michel-Ange ANTON
 */

public class EditJonasServerAction extends JonasBaseAction {

// --------------------------------------------------------- Public Methods
    /**
     * Execute action for a JOnAS server instance management.
     * @param pMapping mapping info
     * @param pForm form object
     * @param pRequest HTTP request
     * @param pResponse HTTP response
     *
     * @return An <code>ActionForward</code> instance or <code>null</code>
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */
    @Override
    public ActionForward executeAction(final ActionMapping pMapping, final ActionForm pForm
        , final HttpServletRequest pRequest, final HttpServletResponse pResponse)
        throws IOException, ServletException {

        String param = pRequest.getParameter("select");
        String paramFlex = pRequest.getParameter("flex");
        boolean changeManagement = false;
        // target server ObjectName
        ObjectName oObjectName = null;
        try {
            ObjectName requestedObjectName = ObjectName.getInstance(param);
            String requestedJonasServerName = requestedObjectName.getKeyProperty("name");
            String currentJonasServerName = m_WhereAreYou.getCurrentJonasServerName();
            String domainName = m_WhereAreYou.getCurrentDomainName();
            ObjectName cmiOn = JonasObjectName.cmiServer(domainName, requestedJonasServerName);
            //is cmi enabled started ?
            pRequest.setAttribute("cmi", new Boolean(JonasAdminJmx.hasMBeanName(cmiOn, requestedJonasServerName)).toString());
            // Domain ObjectName
            ObjectName on = J2eeObjectName.J2EEDomain(domainName);
            // Check if we are on a master
            boolean isMaster = getBooleanAttribute(on, "master");
            pRequest.setAttribute("master", String.valueOf(isMaster));

            if (!currentJonasServerName.equals(requestedJonasServerName)) {
                // Update management context m_WhereAreYou
                m_WhereAreYou.refreshServers(pRequest, domainName, requestedJonasServerName);
                // update variable used below
                currentJonasServerName = m_WhereAreYou.getCurrentJonasServerName();
                if (currentJonasServerName.equals(requestedJonasServerName)) {
                    changeManagement = true;
                } else {
                    addGlobalError(new Error("Can't switch management context to server " + requestedJonasServerName));
                    saveErrors(pRequest, m_Errors);
                    return (pMapping.findForward("Global Error"));
                }
            }

            if (changeManagement) {
                refreshTree(pRequest);
            }

            // Force the node selected in tree
            m_WhereAreYou.selectNameNode("domain"
                    + WhereAreYou.NODE_SEPARATOR
                    + requestedJonasServerName, true);

            // Form used
            JonasServerForm oForm = (JonasServerForm) pForm;
            oObjectName = J2eeObjectName.J2EEServer(m_WhereAreYou.getCurrentDomainName()
                    , currentJonasServerName);
            oForm.setJonasName(currentJonasServerName);
            oForm.setJonasVersion(getStringAttribute(oObjectName, "serverVersion"));
            oForm.setProtocols(getStringAttribute(oObjectName, "protocols"));
            oForm.setVersions(getStringAttribute(oObjectName, "versions"));
            try {
                oForm.setState(getStringAttribute(oObjectName, "state"));
            } catch (ManagementException me) {
                oForm.setState("RUNNING");
            }
            if (JonasManagementRepr.isRegistered(JonasObjectName.webContainerService(domainName), currentJonasServerName)) {
                oForm.setPresentServletContainer(true);
                if (m_WhereAreYou.isCatalinaServer()) {
                    oForm.setServerServletContainerInfo("Apache Tomcat");
                    //oForm.setServerServletContainerInfo(ServerInfo.getServerInfo());
                }
            } else {
                oForm.setPresentServletContainer(false);
                m_Session.setAttribute("presentServletContainer", false);
            }
        } catch (Throwable t) {
            Throwable cause = t.getCause();
            if (cause instanceof javax.management.InstanceNotFoundException
                    && oObjectName != null) {
                t = new ManagementException(targetMassage(oObjectName), cause);
            }
            addGlobalError(t);
            saveErrors(pRequest, m_Errors);
            return (pMapping.findForward("Global Error"));
        }
        // Forward to the jsp.
        if (paramFlex != null && paramFlex.equals("true")) {
            return (pMapping.findForward("Main Index"));
        }
        return (pMapping.findForward("JonasServer"));
    }

    private String targetMassage(final ObjectName pObjectName) {
        StringBuffer buf = new StringBuffer();
        String domainName = pObjectName.getDomain();
        String serverName = pObjectName.getKeyProperty("name");
        buf = buf.append("It seems that server ");
        buf = buf.append(serverName);
        buf = buf.append(" it's not in domain ");
        buf = buf.append(domainName);
        buf = buf.append("-");
        return new String(buf);
    }

}
