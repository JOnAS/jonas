/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 */

package org.ow2.jonas.webapp.jonasadmin.joramplatform;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

/**
 * Form bean for the Joram local queue form page.
 * @author Frederic MAISTRE
 */

public final class JoramTopicForm extends DestinationForm {

// ------------------------------------------------------------- Properties Variables
    /**
     * The id of a topic's subscribers
     */
    private String subscriberIds = null;
    /**
     * The names of a topic's subscribers
     */
    private String subscribers = null;
    /**
     * number of susbscriptions
     */
    private int subscriptions = 0;

//  ------------------------------------------------------------- Properties Methods

    /**
     * @return Returns the subscriberIds.
     */
    public String getSubscriberIds() {
        return subscriberIds;
    }

    /**
     * @param subscriberIds The subscriberIds to set.
     */
    public void setSubscriberIds(String subscriberIds) {
        this.subscriberIds = subscriberIds;
    }

    /**
     * @return Returns the subscriptions.
     */
    public int getSubscriptions() {
        return subscriptions;
    }

    /**
     * @param subscriptions The subscriptions to set.
     */
    public void setSubscriptions(int subscriptions) {
        this.subscriptions = subscriptions;
    }

    /**
     * @return Returns the subscribers.
     */
    public String getSubscribers() {
        return subscribers;
    }

    /**
     * @param subscribers The subscribers to set.
     */
    public void setSubscribers(String subscribers) {
        this.subscribers = subscribers;
    }

// ------------------------------------------------------------- Public Methods

    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        super.reset(mapping, request);
        subscriberIds = null;
        subscriptions = 0;
    }

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        return super.validate(mapping, request);
    }
}
