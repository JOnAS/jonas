/*
 * $Header$
 * $Revision: 11749 $
 * $Date: 2007-10-08 11:47:44 +0200 (lun 08 oct 2007) $
 *
 * ====================================================================
 *
 * The Apache Software License, Version 1.1
 *
 * Copyright (c) 2001 The Apache Software Foundation.  All rights
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution, if
 *    any, must include the following acknowlegement:
 *       "This product includes software developed by the
 *        Apache Software Foundation (http://www.apache.org/)."
 *    Alternately, this acknowlegement may appear in the software itself,
 *    if and wherever such third-party acknowlegements normally appear.
 *
 * 4. The names "The Jakarta Project", "Tomcat", and "Apache Software
 *    Foundation" must not be used to endorse or promote products derived
 *    from this software without prior written permission. For written
 *    permission, please contact apache@apache.org.
 *
 * 5. Products derived from this software may not be called "Apache"
 *    nor may "Apache" appear in their names without prior written
 *    permission of the Apache Group.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE APACHE SOFTWARE FOUNDATION OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */

package org.ow2.jonas.webapp.taglib;

import java.io.Serializable;
import java.util.HashMap;

/**
 * <p>The overall data structure representing a <em>tree control</em>
 * that can be rendered by the <code>TreeControlTag</code> custom tag.
 * Each node of the tree is represented by an instance of
 * <code>TreeControlNode</code>.</p>
 *
 * @author Jazmin Jonson
 * @author Craig R. McClanahan
 * @version $Revision: 1.4
 */

public class TreeControl implements Serializable {
    public static final String ID_PREFIX = "treenode";

// ----------------------------------------------------------- Constructors

    /**
     * Construct a new instance with no predefined root node.
     */
    public TreeControl() {
        super();
        setRoot(null);
    }

    /**
     * Construct a new instance with the specified root node.
     *
     * @param root The new root node
     */
    public TreeControl(TreeControlNode root) {
        super();
        setRoot(root);
    }

// ----------------------------------------------------- Instance Variables

    /**
     * The collection of nodes that represent this tree, keyed by name.
     */
    protected HashMap registry = new HashMap();

    /**
     * The most recently selected node.
     */
    protected TreeControlNode selected = null;

    // Id
    protected int mi_Id = 0;

// ------------------------------------------------------------- Properties

    /**
     * The root node of the entire tree.
     */
    protected TreeControlNode root = null;

    public TreeControlNode getRoot() {
        return (this.root);
    }

    protected void setRoot(TreeControlNode root) {
        if (this.root != null) {
            removeNode(this.root);
        }
        if (root != null) {
            addNode(root);
        }
        root.setLast(true);
        this.root = root;
    }

    /**
     * The current displayable "width" of this tree (that is, the maximum
     * depth of the visible part of the tree).
     */
    public int getWidth() {
        if (root == null) {
            return (0);
        }
        else {
            return (getWidth(root));
        }
    }

// --------------------------------------------------------- Public Methods

    /**
     * Find and return the <code>TreeControlNode</code> for the specified
     * node name, if it exists; otherwise, return <code>null</code>.
     *
     * @param name Name of the <code>TreeControlNode</code> to be returned
     */
    public TreeControlNode findNode(String name) {
        synchronized (registry) {
            return ((TreeControlNode) registry.get(name));
        }
    }

    /**
     * Mark the specified node as the one-and-only currently selected one,
     * deselecting any previous node that was so marked.
     *
     * @param node Name of the node to mark as selected, or <code>null</code>
     *  if there should be no currently selected node
     */
    public void selectNode(String name) {
        if (selected != null) {
            selected.setSelected(false);
            selected = null;
        }
        selected = findNode(name);
        if (selected != null) {
            selected.setSelected(true);
        }
    }

    /**
     * Get the last node selected.
     *
     * @return the current node selected
     */
    public TreeControlNode getSelected() {
        return selected;
    }

    /**
     * Expand a branch in the tree of the selected node.
     */
    public void expandSelectedParents() {
        TreeControlNode oCur = getSelected();
        while (oCur != null){
            oCur = oCur.getParent();
            if (oCur != null) {
                oCur.setExpanded(true);
            }
        }
    }

    public String newId() {
        StringBuffer sbRet = new StringBuffer(ID_PREFIX);
        sbRet.append(mi_Id);
        mi_Id++;
        return sbRet.toString();
    }

// -------------------------------------------------------- Package Methods

    /**
     * Register the specified node in our registry of the complete tree.
     *
     * @param node The <code>TreeControlNode</code> to be registered
     *
     * @exception IllegalArgumentException if the name of this node
     *  is not unique
     */
    void addNode(TreeControlNode node)
        throws IllegalArgumentException {
        synchronized (registry) {
            String name = node.getName();
            if (registry.containsKey(name)) {
                throw new IllegalArgumentException("Name '" + name + "' is not unique");
            }
            node.setTree(this);
            registry.put(name, node);
            // Refresh expand info
            autoRefresh(node);
        }
    }

    /**
     * Calculate the width of the subtree below the specified node.
     *
     * @param node The node for which to calculate the width
     */
    int getWidth(TreeControlNode node) {
        int width = node.getWidth();
        if (!node.isExpanded()) {
            return (width);
        }
        TreeControlNode children[] = node.findChildren();
        for (int i = 0; i < children.length; i++) {
            int current = getWidth(children[i]);
            if (current > width) {
                width = current;
            }
        }
        return (width);
    }

    /**
     * Deregister the specified node, as well as all child nodes of this
     * node, from our registry of the complete tree.  If this node is not
     * present, no action is taken.
     *
     * @param node The <code>TreeControlNode</code> to be deregistered
     */
    void removeNode(TreeControlNode node) {
        synchronized (registry) {
            TreeControlNode children[] = node.findChildren();
            for (int i = 0; i < children.length; i++) {
                removeNode(children[i]);
            }
            TreeControlNode parent = node.getParent();
            if (parent != null) {
                parent.removeChild(node);
            }
            node.setParent(null);
            node.setTree(null);
            if (node == this.root) {
                this.root = null;
            }
            registry.remove(node.getName());
            // Save removed node in list
            addRemovedList(node);
        }
    }

    /**
     * List to save removed node used by the auto-refresh mode.
     *
     */
    private HashMap m_RemovedList = null;

    /**
     * Disable auto-refresh mode.
     */
    public void disableAutoRefresh() {
        if (m_RemovedList != null) {
            m_RemovedList.clear();
        }
        m_RemovedList = null;
    }

    /**
     * Enable auto-refresh mode.
     * When a set of children are refreshed (removed then added),
     * the expanded info is copied of the removed node to the added node.
     * The name is used to retreive the good node.
     * By default, the auto-refresh mode is disabled.
     * Be careful, enabled this mode before the remove of all nodes and
     * disabled it after the add.
     */
    public void enableAutoRefresh() {
        m_RemovedList = new HashMap();
    }

    /**
     * Add the removed node in the removed list.
     *
     * @param p_RemovedNode The removed node
     */
    void addRemovedList(TreeControlNode p_RemovedNode) {
        if (m_RemovedList != null) {
            m_RemovedList.put(p_RemovedNode.getName(), p_RemovedNode);
        }
    }

    /**
     * Search the added node in the removed list and if it's found, copy the expanded info.
     *
     * @param p_AddedNode The added node
     */
    protected void autoRefresh(TreeControlNode p_AddedNode) {
        if (m_RemovedList != null) {
            TreeControlNode oRemove = (TreeControlNode) m_RemovedList.get(p_AddedNode.getName());
            if (oRemove != null) {
                p_AddedNode.setExpanded(oRemove.isExpanded());
            }
        }
    }
}
