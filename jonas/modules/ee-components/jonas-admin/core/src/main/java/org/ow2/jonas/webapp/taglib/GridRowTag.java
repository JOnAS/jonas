/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Michel-Ange ANTON
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.taglib;

import javax.servlet.jsp.tagext.Tag;

public class GridRowTag extends GridTableBaseTag {

// ----------------------------------------------------- Properties

    private boolean changeStyle = false;

    public boolean isChangeStyle() {
        return changeStyle;
    }

    public void setChangeStyle(boolean changeStyle) {
        this.changeStyle = changeStyle;
    }

// ----------------------------------------------------- Public Methods

    /**
     * Release resources after Tag processing has finished.
     */
    public void release() {
        super.release();
        changeStyle = false;
    }

// ----------------------------------------------------- Protected Methods

    /**
     * Return the HTML element.
     */
    protected String getHtmlElement() {
        return "tr";
    }

    /**
     * Duplicate of <code>BaseHandlerTag.prepareStyles()" method.
     * Add the alternate class (odd or even) with .
     *
     * @return The prepared String for inclusion in the HTML tag.
     */
    protected String prepareStyles() {
        StringBuffer styles = new StringBuffer();
        if (getStyle() != null) {
            styles.append(" style=\"");
            styles.append(getStyle());
            styles.append("\"");
        }

        // Get the row class from the GridTag parent
        String sClass = getStyleClass();
        if (sClass == null) {
            sClass = getRowClass();
        }
        // Render class
        if (sClass != null) {
            styles.append(" class=\"");
            styles.append(sClass);
            styles.append("\"");
        }

        if (getStyleId() != null) {
            styles.append(" id=\"");
            styles.append(getStyleId());
            styles.append("\"");
        }
        if (getTitle() != null) {
            styles.append(" title=\"");
            styles.append(getTitle());
            styles.append("\"");
        }
        return styles.toString();
    }

    /**
     * Determine the Row Class from the GridTag.
     */
    protected String getRowClass() {
        // Determine if embedded in an GridTag
        Tag tag = findAncestorWithClass(this, GridTag.class);
        if (tag == null) {
            return null;
        }
        // Determine the current row number
        GridTag oTag = (GridTag) tag;
        return oTag.getRowStyle(changeStyle);
    }

}