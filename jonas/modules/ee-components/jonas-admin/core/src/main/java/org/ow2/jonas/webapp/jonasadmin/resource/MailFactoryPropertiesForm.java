/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id:
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.resource;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class MailFactoryPropertiesForm extends ActionForm {

// --------------------------------------------------------- Constants

// --------------------------------------------------------- Properties variables
    private String mailFactoryName = null;
    private String jndiName = null;
    private String type = null;
     // authentication props
    private String username = null;
    private String password = null;
    // mail session props
    private String sessionProps = null;
    // mimepart datasource message props
    private String to = null;
    private String subject = null;
    private String cc = null;
    private String bcc = null;

// --------------------------------------------------------- Public Methods

    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        mailFactoryName = null;
        jndiName = null;
        type = null;
        // authentication props
        username = null;
        password = null;
        // mail session props
        sessionProps = null;
        // mimepart datasource message props
        to = null;
        subject = null;
        cc = null;
        bcc = null;
    }

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors oErrors = new ActionErrors();
        if ((jndiName == null) || (jndiName.length() == 0)) {
            oErrors.add("jndiName", new ActionMessage("error.mailservice.mailfactory.emptyjndiname"));
        }
        if ((mailFactoryName == null) || (mailFactoryName.length() == 0)) {
            oErrors.add("jndiName", new ActionMessage("error.mailservice.mailfactory.emptyname"));
        }
        if ((type == null) || (type.length() == 0)) {
            oErrors.add("jndiName", new ActionMessage("error.mailservice.mailfactory.emptytype"));
        }
        return oErrors;
    }

// --------------------------------------------------------- Properties Methods

    public String getMailFactoryName() {
        return mailFactoryName;
    }

    public void setMailFactoryName(String mailFactoryName) {
        this.mailFactoryName = mailFactoryName;
    }

    public String getJndiName() {
        return jndiName;
    }

    public void setJndiName(String jndiName) {
        this.jndiName = jndiName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSessionProps() {
        return sessionProps;
    }

    public void setSessionProps(String sessionProps) {
        this.sessionProps = sessionProps;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getBcc() {
        return bcc;
    }

    public void setBcc(String bcc) {
        this.bcc = bcc;
    }

}
