/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.ejb;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.webapp.jonasadmin.Jlists;

/**
 * @author Michel-Ange ANTON
 */
public class EjbSessionForm extends EjbForm {
    // --------------------------------------------------------- Default for
    // session time out
    public static final String SESSION_TIME_OUT_DEFAULT = "0";

    // --------------------------------------------------------- Properties
    // Variables

    /**
     * MBean Ejb Session properties
     */
    private String sessionTimeOut = SESSION_TIME_OUT_DEFAULT;

    private int warningThreshold = 0;

    private long numberOfCalls = 0;

    private long totalBusinessProcessingTime = 0;

    private long totalProcessingTime = 0;

    private long averageBusinessProcessingTime = 0;

    private long averageProcessingTime = 0;

    private boolean monitoringEnabled = false;

    private boolean monitoringSettingsDefinedInDD = false;

    // --------------------------------------------------------- Public Methods

    /**
     * Reset all properties to their default values.
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */

    @Override
    public void reset(final ActionMapping mapping, final HttpServletRequest request) {
        super.reset(mapping, request);
        sessionTimeOut = SESSION_TIME_OUT_DEFAULT;
        this.setAction("apply");
    }

    // --------------------------------------------------------- Properties
    // Methods

    public String getSessionTimeOut() {
        return sessionTimeOut;
    }

    public void setSessionTimeOut(final String sessionTimeOut) {
        this.sessionTimeOut = sessionTimeOut;
    }

    public boolean getMonitoringSettingsDefinedInDD() {
        return monitoringSettingsDefinedInDD;
    }

    public void setMonitoringSettingsDefinedInDD(final boolean monitoringSettingsDefinedInDD) {
        this.monitoringSettingsDefinedInDD = monitoringSettingsDefinedInDD;
    }

    public boolean getMonitoringEnabled() {
        return monitoringEnabled;
    }

    public void setMonitoringEnabled(final boolean monitoringEnabled) {
        this.monitoringEnabled = monitoringEnabled;
    }

    public int getWarningThreshold() {
        return warningThreshold;
    }

    public void setWarningThreshold(final int warningThreshold) {
        this.warningThreshold = warningThreshold;
    }

    public long getNumberOfCalls() {
        return numberOfCalls;
    }

    public void setNumberOfCalls(final int numberOfCalls) {
        this.numberOfCalls = numberOfCalls;
    }

    public long getTotalBusinessProcessingTime() {
        return totalBusinessProcessingTime;
    }

    public void setTotalBusinessProcessingTime(final long totalBusinessProcessingTime) {
        this.totalBusinessProcessingTime = totalBusinessProcessingTime;
    }

    public long getTotalProcessingTime() {
        return totalProcessingTime;
    }

    public void setTotalProcessingTime(final long totalProcessingTime) {
        this.totalProcessingTime = totalProcessingTime;
    }

    public long getAverageBusinessProcessingTime() {
        return averageBusinessProcessingTime;
    }

    public void setAverageBusinessProcessingTime(final long averageBusinessProcessingTime) {
        this.averageBusinessProcessingTime = averageBusinessProcessingTime;
    }

    public long getAverageProcessingTime() {
        return averageProcessingTime;
    }

    public void setAverageProcessingTime(final long averageProcessingTime) {
        this.averageProcessingTime = averageProcessingTime;
    }

    @Override
    public List getBooleanValues() {
        return Jlists.getBooleanValues();
    }

    /**
     * Validate the properties that have been set from this HTTP request, and
     * return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found. If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no recorded
     * error messages.
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     * @return List of errors or null
     */
    @Override
    public ActionErrors validate(final ActionMapping mapping, final HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        numberCheck(errors, "sessionTimeOut", sessionTimeOut, true, 0, Integer.MAX_VALUE);
        return errors;
    }

}
