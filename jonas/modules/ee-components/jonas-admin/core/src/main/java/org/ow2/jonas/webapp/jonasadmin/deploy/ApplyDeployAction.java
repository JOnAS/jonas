/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.deploy;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.webapp.jonasadmin.Jlists;
import org.ow2.jonas.webapp.jonasadmin.JonasAdminJmx;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Michel-Ange Anton
 */
public class ApplyDeployAction extends BaseDeployAction {

// --------------------------------------------------------- Public Methods

    /**
     */
    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws IOException, ServletException {

        String sForward = "Deploy Confirm";

        // Form used
        DeployForm oForm = (DeployForm) p_Form;
        try {
            oForm.setListDeploy(Jlists.getArrayList(oForm.getDeploy(), Jlists.SEPARATOR));
            oForm.setListUndeploy(Jlists.getArrayList(oForm.getUndeploy(), Jlists.SEPARATOR));

            ArrayList alAdd = new ArrayList(oForm.getListDeploy());
            alAdd.removeAll(JonasAdminJmx.getDeployed(oForm.getListDeployed()));
            oForm.setListAdd(alAdd);

            ArrayList alRemove = new ArrayList(oForm.getListUndeploy());
            alRemove.retainAll(JonasAdminJmx.getDeployed(oForm.getListDeployed()));
            oForm.setListRemove(alRemove);

            oForm.setConfirm(((alAdd.size() > 0) || (alRemove.size() > 0)));
            if (!oForm.isConfirm()) {
                m_Errors.add("error.deploy.noselect", new ActionMessage("error.deploy.noselect"));
                saveErrors(p_Request, m_Errors);
                sForward = getForwardEdit();
            }
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        // Forward to the jsp.
        return (p_Mapping.findForward(sForward));
    }
}
