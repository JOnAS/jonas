/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.jonasserver;

import java.io.IOException;
import java.util.List;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasAdminJmx;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Adriana Danes
 */
public class EditRegistryAction extends JonasBaseAction {

    /**
     * Execute action for registry management
     * @param pMapping mapping info
     * @param pForm form object
     * @param pRequest HTTP request
     * @param pResponse HTTP response
     *
     * @return An <code>ActionForward</code> instance or <code>null</code>
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */
    public ActionForward executeAction(ActionMapping pMapping, ActionForm pForm
            , HttpServletRequest pRequest, HttpServletResponse pResponse)
    throws IOException, ServletException {

        // Force the node selected in tree
        m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_SERVER), true);

        // Selected resource
        String sObjectName = pRequest.getParameter("select");
        if (sObjectName != null) {
            try {
                ObjectName on = ObjectName.getInstance(sObjectName);
                JndiResourceForm oForm = new JndiResourceForm();
                // test if the resource corresponds to the CMI protocol
                // in this case, the resource name is 'cmi'
                // and we have a CMI type MBean
                String sDomainName = m_WhereAreYou.getCurrentDomainName();
                String sServerName = m_WhereAreYou.getCurrentJonasServerName();
                List lNames = getListAttribute(on, "Names");
                boolean cmi = false;
                ObjectName cmiOn = JonasObjectName.cmiServer(sDomainName, sServerName);
                if (JonasAdminJmx.hasMBeanName(cmiOn, sServerName)) {
                    cmi = true;
                }
                if (cmi) {
                    List namesAndNodes = CmiRegistryResource.getCmiRegistry(cmiOn, lNames, sServerName);
                    oForm.setVector(namesAndNodes);
                    oForm.setRegistryProtocol("cmi");
                } else {
                    oForm.setListNames(lNames);
                    oForm.setRegistryProtocol(getStringAttribute(on, "Name"));
                }
                pRequest.setAttribute("jndiResourceForm", oForm);
            } catch (Throwable t) {
                addGlobalError(t);
                saveErrors(pRequest, m_Errors);
                return (pMapping.findForward("Global Error"));
            }
        }

        // Forward to the jsp.
        return (pMapping.findForward("List Registry"));
    }
}
