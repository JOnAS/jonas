/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.mbean;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author Michel-Ange ANTON
 */

public class MbeanFilteringForm extends ActionForm {

// --------------------------------------------------------- Properties variables

    private boolean description = false;
    private boolean value = false;
    private String periodRow;
    private String selectedName;
    private String forward;

// --------------------------------------------------------- Public Methods

    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        description = false;
        value = false;
        periodRow = "2";
    }

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     * @return ActionErrors instance
     */
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors oErrors = new ActionErrors();
        return oErrors;
    }

// --------------------------------------------------------- Properties Methods

    public boolean isDescription() {
        return description;
    }

    public void setDescription(boolean description) {
        this.description = description;
        if (isDescription() == false) {
            periodRow = "2";
        }
        else {
            periodRow = "1";
        }
    }

    public boolean isValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    public String getPeriodRow() {
        return periodRow;
    }
    public String getSelectedName() {
        return selectedName;
    }
    public void setSelectedName(String selectedName) {
        this.selectedName = selectedName;
    }
    public String getForward() {
        return forward;
    }
    public void setForward(String forward) {
        this.forward = forward;
    }
}