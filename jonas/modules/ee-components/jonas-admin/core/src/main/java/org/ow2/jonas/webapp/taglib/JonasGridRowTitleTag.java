/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Michel-Ange ANTON
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.taglib;

import javax.servlet.jsp.JspException;

public class JonasGridRowTitleTag extends GridRowTag {

// ------------------------------------------------------------- Constants

    public static final String CLASS_TITLE = "panelTitle";

// ------------------------------------------------------------- Instance Variables

    private boolean mb_ForceStyleClass = false;

// ------------------------------------------------------------- Properties

    private String level = null;

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

// ----------------------------------------------------- Public Methods

    /**
     * Start of Tag processing
     *
     * @exception JspException if a JSP exception occurs
     */
    public int doStartTag()
        throws JspException {
        // Set default value
        mb_ForceStyleClass = false;
        if (getStyleClass() == null) {
            setStyleClass(getStyleClassLevel());
            mb_ForceStyleClass = true;
        }
        return super.doStartTag();
    }

    public void release() {
        super.release();
        level = null;
    }

    public String getStyleClassLevel() {
        if (level != null) {
            return new String((CLASS_TITLE + level));
        }
        return CLASS_TITLE;
    }

    /**
     * End of Tag Processing
     *
     * @exception JspException if a JSP exception occurs
     */
    public int doEndTag()
        throws JspException {
        int iRet = super.doEndTag();
        if (mb_ForceStyleClass == true) {
            mb_ForceStyleClass = false;
            setStyleClass(null);
        }
        return iRet;
    }
}