package org.ow2.jonas.webapp.jonasadmin.jonasmqconnect;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

public class ConnectionFactoryEditForm  extends ActionForm {
    /**
     * debug or not
     */
    private boolean debug = false;

    /**
     * printe debug msg
     * @param s msg
     */
    private void debug(String s) {
        if (debug)
            System.out.println(s);
    }

    /**
     * operation : CF,TCF,QCF
     */
    private String type;

    /**
     * operation : view,edit
     */
    private String operation;

    /**
     * Selected queues
     */
    private String[] selectedQueues;

    /**
     * Selected topics
     */
    private String[] selectedTopics;

    /**
     * jndiName
     */
    private String jndiName = "sampleTopic";

    /**
     * properties
     */
    private ArrayList properties;

    /**
     * properties name
     */
    private ArrayList propertiesName;

    /**
     * properties value
     */
    private ArrayList propertiesValue;

    /**
     * get jndiname
     * @return
     */
    public String getJndiName() {
        return jndiName;
    }

    /**
     * set jndiname
     * @param jndiName
     */
    public void setJndiName(String jndiName) {
        this.jndiName = jndiName;
    }

    /**
     * type: CF,TCF,QCF
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * type: CF,TCF,QCF
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Selected Queues
     * @return the selected queues
     */
    public String[] getSelectedQueues() {
        return selectedQueues;
    }

    /**
     * selected queues
     * @param selectedQueues the selected queues to set
     */
    public void setSelectedQueues(String[] selectedQueues) {
        this.selectedQueues = selectedQueues;
    }

    /**
     * Selected Topics
     * @return the selected topics
     */
    public String[] getSelectedTopics() {
        return selectedTopics;
    }

    /**
     * selected topics
     * @param selectedTopics the selected topics to set
     */
    public void setSelectedTopics(String[] selectedTopics) {
        this.selectedTopics = selectedTopics;
    }

    /**
     * @return the propertiesName
     */
    public ArrayList getPropertiesName() {
        return propertiesName;
    }

    /**
     * @param propertiesName the propertiesName to set
     */
    public void setPropertiesName(ArrayList propertiesName) {
        this.propertiesName = propertiesName;
    }

    /**
     * @return the propertiesValue
     */
    public ArrayList getPropertiesValue() {
        return propertiesValue;
    }

    /**
     * @param propertiesValue the propertiesValue to set
     */
    public void setPropertiesValue(ArrayList propertiesValue) {
        this.propertiesValue = propertiesValue;
    }

    /**
     * @return the properties
     */
    public ArrayList getProperties() {
        return properties;
    }

    /**
     * @param properties the properties to set
     */
    public void setProperties(ArrayList properties) {
        this.properties = properties;
    }

    /**
     * @return the operation
     */
    public String getOperation() {
        return operation;
    }

    /**
     * @param operation the operation to set
     */
    public void setOperation(String operation) {
        this.operation = operation;
    }
}
