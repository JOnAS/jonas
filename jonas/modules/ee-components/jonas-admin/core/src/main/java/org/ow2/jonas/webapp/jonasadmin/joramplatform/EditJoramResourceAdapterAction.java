/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 */

package org.ow2.jonas.webapp.jonasadmin.joramplatform;

import java.util.Set;

import javax.management.InstanceNotFoundException;
import javax.management.ObjectName;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.util.JoramObjectName;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * @author Adriana Danes
 */

public class EditJoramResourceAdapterAction extends EditJoramBaseAction {

// --------------------------------------------------------- Public Methods

    public ActionForward executeAction(ActionMapping pMapping, ActionForm pForm
        , HttpServletRequest pRequest, HttpServletResponse pResponse) {

        // Current JOnAS server
        String jonasServerName =  m_WhereAreYou.getCurrentJonasServerName();

        // Force the node selected in tree
        m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_DOMAIN) + WhereAreYou.NODE_SEPARATOR
            + "joramplatform", true);

        // Form used
        JoramAdapterForm oForm = (JoramAdapterForm) pForm;
        try {
            // Object name used
            Set adapterOns = JonasManagementRepr.queryNames(JoramObjectName.joramAdapter(), jonasServerName);
            if (adapterOns.isEmpty()) {
                throw new InstanceNotFoundException();
            }
            ObjectName joramAdapterON = (ObjectName) adapterOns.iterator().next();
            oForm.setCollocatedServer(getBooleanAttribute(joramAdapterON, "Collocated"));
            oForm.setConfigDir(getStringAttribute(joramAdapterON, "PlatformConfigDir"));
            oForm.setAdminFile(getStringAttribute(joramAdapterON, "AdminFileXML"));
            oForm.setPersistentServer(getBooleanAttribute(joramAdapterON, "PersistentPlatform"));
            oForm.setServerIdTxt(toStringShortAttribute(joramAdapterON, "ServerId"));
            oForm.setServerName(getStringAttribute(joramAdapterON, "Storage"));
            oForm.setHostName(getStringAttribute(joramAdapterON, "HostName"));
            oForm.setServerPortTxt(toStringIntegerAttribute(joramAdapterON, "ServerPort"));
            oForm.setCnxPendingTimerTxt(toStringIntegerAttribute(joramAdapterON, "CnxPendingTimer"));
            oForm.setConnectingTimerTxt(toStringIntegerAttribute(joramAdapterON, "ConnectingTimer"));
            oForm.setTxPendingTimerTxt(toStringIntegerAttribute(joramAdapterON, "TxPendingTimer"));
        } catch (Throwable t) {
            return (treatError(t, pMapping, pRequest));
        }

        // Forward to the jsp.
        return (pMapping.findForward("JoramAdapter"));
    }

}
