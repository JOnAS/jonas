package org.ow2.jonas.webapp.jonasadmin.jonasmqconnect.util;


public class ItemMdb implements java.io.Serializable {
    private String name;
    private String objectName;
    private String mqDestination;
    private int numberMsg;

    public ItemMdb(String name, String objectName, String mqDestination, int numberMsg) {
        this.name = name;
        this.objectName = objectName;
        this.mqDestination = mqDestination;
        this.numberMsg = numberMsg;
    }

    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name to set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return Returns the object name.
     */
    public String getObjectName() {
        return objectName;
    }

    /**
     * @param objectName The object name to set.
     */
    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    /**
     * @return Returns the mq destination name.
     */
    public String getMqDestination() {
        return mqDestination;
    }

    /**
     * @param mqDestination The mq destination name to set.
     */
    public void setMqDestination(String mqDestination) {
        this.mqDestination = mqDestination;
    }

    /**
     * @return Returns number of received message.
     */
    public int getNumberMsg() {
        return numberMsg;
    }

    /**
     * @param numberMsg number of received message to set.
     */
    public void setNumberMsg(int numberMsg) {
        this.numberMsg = numberMsg;
    }
}