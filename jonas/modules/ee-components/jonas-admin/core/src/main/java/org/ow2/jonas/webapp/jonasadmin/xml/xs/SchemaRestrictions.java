/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.xml.xs;

import java.util.Set;

/**
 * An interface to represent the schema restrictions from a schema.
 *
 * @author Gregory Lapouchnian
 * @author Patrick Smith
 */
public interface SchemaRestrictions {

    /** A rar typed archive */
    static int RAR_TYPE = 0;

    /** A war typed archive. */
    static int WAR_TYPE = 1;

    /** A jar typed archive. */
    static int JAR_TYPE = 2;

    /** An ear typed archive. */
    static int EAR_TYPE = 3;

    /**
     * Returns if the element given by name has an element restriction
     * @param name the name of the element to check.
     * @return if the element given by name has an element restriction.
     */
    boolean hasElementRestrictions(String name);

    /**
     * Returns the element restrictions for the element given by name.
     * @param name the element to get the restrictions for.
     * @return the element restrictions for the element given by name.
     */
    ElementRestrictions getElementRestrictions(String name);

    /**
     * Returns a set of the complex elements from this schema.
     * @return a set of the complex elements from this schema.
     */
    Set getComplexElements();

    //public String getElementDocumentation(String name);
}
