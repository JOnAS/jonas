/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Michel-Ange ANTON
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.taglib;

import javax.servlet.jsp.JspException;

public class GridTableBaseTag extends GridBaseTag {

// ----------------------------------------------------- Properties

    private String align = null;
    private String valign = null;
    private String width = null;
    private String height = null;

    /**
     *  Return the Alignment
     */
    public String getAlign() {
        return (this.align);
    }

    /**
     * Set the Alignment
     *
     * @param Value for Alignment
     */
    public void setAlign(String align) {
        this.align = align;
    }

    /**
     *  Return the Vertical Alignment
     */
    public String getValign() {
        return (this.valign);
    }

    /**
     * Set the Vertical Alignment
     *
     * @param Value for Vertical Alignment
     */

    public void setValign(String valign) {
        this.valign = valign;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

// ----------------------------------------------------- Protected Methods

    /**
     * Return the HTML element.
     */
    protected String getHtmlElement() {
        return "table";
    }

    /**
     * Prepare the attributes of the HTML element
     */
    protected String prepareAttributes() throws JspException {
        StringBuffer sb = new StringBuffer();

        // Append "width" parameter
        sb.append(prepareAttribute("width", width));
        // Append "height" parameter
        sb.append(prepareAttribute("height", height));
        // Append "align" parameter
        sb.append(prepareAttribute("align", align));
        // Append "valign" parameter
        sb.append(prepareAttribute("valign", valign));

        // Append Event Handler details
        sb.append(super.prepareAttributes());

        return sb.toString();

    }

// ----------------------------------------------------- Public Methods

    /**
     * Release resources after Tag processing has finished.
     */
    public void release() {
        super.release();
        align = null;
        valign = null;
        width = null;
        height = null;
    }
}