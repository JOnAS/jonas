/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.common;

import org.ow2.jonas.lib.management.extensions.base.NameItem;

/**
 * @author Adriana.Danes@objectweb.org
 *
 * ModuleItem objects are used to show management information concerning
 * modules in a J2EEApplication.
 */
public class ModuleItem implements NameItem {
    /**
     * Module name
     */
    private String name = null;
    /**
     * OBJECT_NAME of the J2EEDeployedObject corresponding to the module
     */
    private String objectName = null;
    /**
     * Path of the source file
     */
    private String filePath = null;

    /**
     * Default constructor
     */
    public ModuleItem() {
    }
    /**
     * Constructor
     * @param name the module's name
     * @param objectName the OBJECT_NAME of the J2EEDeployedObject corresponding to the module
     * @param filePath the path of the source file
     */
    public ModuleItem(String name, String objectName, String filePath) {
        this.name = name;
        this.objectName = objectName;
        this.filePath = filePath;
    }

    /**
     * @return Returns the module's name.
     */
    public String getName() {
        return name;
    }
    /**
     * @param name the module name
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @return Returns the OBJECT_NAME of the corresponding MBean.
     */
    public String getObjectName() {
        return objectName;
    }
    /**
     * @param objectName The OBJECT_NAME to set.
     */
    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }
    /**
     * @return Returns the filePath.
     */
    public String getFilePath() {
        return filePath;
    }
    /**
     * @param filePath The filePath to set.
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

}
