/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.resource;

import java.io.IOException;
import java.util.HashMap;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.webapp.jonasadmin.JonasAdminJmx;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;


/**
 * @author Eric Hardesty
 */

public class ApplyResourceAdapterCFAction extends JonasBaseAction {

// --------------------------------------------------------- Public Methods

    @Override
    public ActionForward executeAction(final ActionMapping p_Mapping, final ActionForm p_Form
        , final HttpServletRequest p_Request, final HttpServletResponse p_Response)
        throws IOException, ServletException {

        // Form used
        ResourceAdapterCFForm oForm = (ResourceAdapterCFForm) p_Form;
        try {
            String domainName = m_WhereAreYou.getCurrentDomainName();
            String serverName = m_WhereAreYou.getCurrentJonasServerName();
            populate(oForm);
            if (oForm.getAction().equals("save") == true) {
                save(oForm, domainName, serverName);
            }
        }
        catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        // Forward to the jsp and add the parameter 'name' with the good value.
        return new ActionForward(p_Mapping.findForward("Resource AdapterCF"));
    }

// --------------------------------------------------------- Protected Methods

    protected void populate(final ResourceAdapterCFForm p_Form)
        throws Exception {
        // Object name used
        ObjectName oObjectName = p_Form.getOName();
        // Populate
        setIntegerAttribute(oObjectName, "jdbcConnCheckLevel", p_Form.getJdbcConnCheckLevel());
        setIntegerAttribute(oObjectName, "connMaxAge", p_Form.getConnMaxAge());
        setIntegerAttribute(oObjectName, "maxOpentime", p_Form.getMaxOpentime());
        setStringAttribute(oObjectName, "jdbcTestStatement", p_Form.getJdbcTestStatement());
        setIntegerAttribute(oObjectName, "maxSize", p_Form.getMaxSize());
        setIntegerAttribute(oObjectName, "minSize", p_Form.getMinSize());
        setIntegerAttribute(oObjectName, "maxWaitTime", p_Form.getMaxWaitTime());
        setIntegerAttribute(oObjectName, "maxWaiters", p_Form.getMaxWaiters());
        setIntegerAttribute(oObjectName, "samplingPeriod", p_Form.getSamplingPeriod());
        //setIntegerAttribute(oObjectName, "pstmtMax ", p_Form.getPstmtMax());
        //setStringAttribute(oObjectName, "pstmtCachePolicy ", p_Form.getPstmtCachePolicy());
    }

    protected void save(final ResourceAdapterCFForm pForm, final String domainName, final String serverName) {
        // Create a map containing the configuration parameters' values
        HashMap configMap = new HashMap();
        configMap.put("jdbcConnCheckLevel", pForm.getJdbcConnCheckLevel());
        configMap.put("jdbcTestStatement", pForm.getJdbcTestStatement());
        configMap.put("connMaxAge", pForm.getConnMaxAge());
        configMap.put("maxOpentime", pForm.getMaxOpentime());
        configMap.put("maxSize", pForm.getMaxSize());
        configMap.put("minSize", pForm.getMinSize());
        configMap.put("maxWaitTime", pForm.getMaxWaitTime());
        configMap.put("maxWaiters", pForm.getMaxWaiters());
        configMap.put("samplingPeriod", pForm.getSamplingPeriod());
        configMap.put("pstmtMax", pForm.getPstmtMax());
        configMap.put("pstmtCachePolicy", pForm.getPstmtCachePolicy());
        // Get the RarConfigMbean
        ObjectName on = JonasAdminJmx.getRarConfigObjectName(domainName, serverName);
        Object[] params = new Object[] {pForm.getPath(), configMap};
        String[] sig = new String[] {"java.lang.String", "java.util.Map"};
        JonasManagementRepr.invoke(on, "updateXML", params, sig, serverName);
    }
}
