/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.mbean;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.management.MBeanInfo;
import javax.management.MBeanOperationInfo;
import javax.management.MBeanParameterInfo;
import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.management.extensions.base.mbean.MbeanItem;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * List of all Operations for a MBean.
 *
 * @author Michel-Ange ANTON
 */

public final class ListMBeanOperationsAction extends ListMBeanDetailsAction {

// --------------------------------------------------------- Public Methods

    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws IOException, ServletException {

        try {
            // Save current action
            setAction(ACTION_OPERATIONS);
            // Parameter
            String sSelect = p_Request.getParameter("select");
            // get server name
        	WhereAreYou oWhere = (WhereAreYou) p_Request.getSession().getAttribute(WhereAreYou.
                    SESSION_NAME);
            String serverName = oWhere.getCurrentJonasServerName();

            // Create a request attribute with our collection of MBeans
            ArrayList list = new ArrayList();

            // Get all infos of a MBean
            ObjectName on = new ObjectName(sSelect);
            MbeanItem oItem = MbeanItem.build(on);
            MBeanInfo oMBeanInfo = JonasManagementRepr.getMBeanInfo(on, serverName);
            // Get attributes infos
            MBeanOperationInfo[] aoMBeanOperationInfo = oMBeanInfo.getOperations();
            if (aoMBeanOperationInfo.length > 0) {
                // Loop to append each attribute node
                for (int i = 0; i < aoMBeanOperationInfo.length; i++) {
                    list.add(new ViewMBeanOperations(aoMBeanOperationInfo[i]));
                }
                // Sort
                Collections.sort(list, new MBeanOperationsByName());
            }
            // Set the beans
            p_Request.setAttribute("MBean", oItem);
            p_Request.setAttribute("MBeanOperations", list);
            // Active and save filtering display if not exists
            MbeanFilteringForm oForm = (MbeanFilteringForm) m_Session.getAttribute(
                "mbeanFilteringForm");
            if (oForm == null) {
                oForm = new MbeanFilteringForm();
                oForm.reset(p_Mapping, p_Request);
                m_Session.setAttribute("mbeanFilteringForm", oForm);
            }
            oForm.setSelectedName(sSelect);

            // Force the node selected in tree when the direct is used (MBeans list)
            StringBuffer sbBranch = new StringBuffer("domain*mbeans");
            sbBranch.append(WhereAreYou.NODE_SEPARATOR);
            sbBranch.append(sSelect);
            m_WhereAreYou.selectNameNode(sbBranch.toString(), true);
        }
        catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        // Forward to the corresponding display page
        return p_Mapping.findForward("List MBean Operations");
    }

// --------------------------------------------------------- Inner Classes

    public class ViewMBeanOperations {
        private String name;
        private String returnType;
        private String impact;
        private String parameters;
        private String description;

        public ViewMBeanOperations() {
        }

        public ViewMBeanOperations(MBeanOperationInfo po_Ope) {
            setName(po_Ope.getName());
            setReturnType(po_Ope.getReturnType());
            setDescription(po_Ope.getDescription());
            // Impact
            switch (po_Ope.getImpact()) {
                case MBeanOperationInfo.ACTION:
                    setImpact("Action");
                    break;
                case MBeanOperationInfo.ACTION_INFO:
                    setImpact("Action info");
                    break;
                case MBeanOperationInfo.INFO:
                    setImpact("Info");
                    break;
                default:
                    setImpact("Unknown");
            }
            // Parameters
            StringBuffer sb = new StringBuffer();
            MBeanParameterInfo[] oParams = po_Ope.getSignature();
            for (int i = 0; i < oParams.length; i++) {
                sb.append(oParams[i].getType());
                sb.append(" ");
                sb.append(oParams[i].getName());
                if (i < (oParams.length - 1)) {
                    sb.append(", ");
                }
            }
            setParameters(sb.toString());
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getReturnType() {
            return returnType;
        }

        public void setReturnType(String returnType) {
            this.returnType = returnType;
        }

        public String getImpact() {
            return impact;
        }

        public void setImpact(String impact) {
            this.impact = impact;
        }

        public String getParameters() {
            return parameters;
        }

        public void setParameters(String parameters) {
            this.parameters = parameters;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }

    public class MBeanOperationsByName implements Comparator {

        public int compare(Object p_O1, Object p_O2) {
            ViewMBeanOperations o1 = (ViewMBeanOperations) p_O1;
            ViewMBeanOperations o2 = (ViewMBeanOperations) p_O2;
            return o1.getName().compareToIgnoreCase(o2.getName());
        }

        public boolean equals(Object p_Obj) {
            if (p_Obj instanceof ViewMBeanOperations) {
                return true;
            }
            return false;
        }
    }

}
