/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.ejb;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

/**
 * @author Michel-Ange ANTON
 */
public class EjbEntityForm extends EjbForm {

// --------------------------------------------------------- Properties variables

    /**
     * MBean Ejb Entity properties
     */
    private int passivationTimeOut = 0;
    private int inactivityTimeOut = 0;
    private int deadlockTimeOut = 0;
    private int readTimeOut = 0;
    private boolean shared = false;
    private boolean prefetch = false;
    private boolean hardLimit = false;
    private String lockPolicy = null;
    private String persistency = null;

    private int usedInTxInstance = 0;
    private int usedOutTxInstance = 0;
    private int unusedInstance = 0;
    private int passivatedInstance = 0;
    private int removedInstance = 0;
    private int currentWaiters = 0;
    private int pkNumber = 0;

// --------------------------------------------------------- Public Methods

    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        super.reset(mapping, request);
        passivationTimeOut = 0;
        inactivityTimeOut = 0;
        deadlockTimeOut = 0;
        readTimeOut = 0;
        shared = false;
        prefetch = false;
        hardLimit = false;
        lockPolicy = null;
        persistency = null;

        usedInTxInstance = 0;
        usedOutTxInstance = 0;
        unusedInstance = 0;
        passivatedInstance = 0;
        removedInstance = 0;
        currentWaiters = 0;
        pkNumber = 0;
    }

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        return super.validate(mapping, request);
    }

// --------------------------------------------------------- Properties Methods
    public String getPersistency() {
        return persistency;
    }

    public void setPersistency(String persistency) {
        this.persistency = persistency;
    }

    public int getPassivationTimeOut() {
        return passivationTimeOut;
    }

    public int getInactivityTimeOut() {
        return inactivityTimeOut;
    }

    public int getDeadlockTimeOut() {
        return deadlockTimeOut;
    }

    public int getReadTimeOut() {
        return readTimeOut;
    }

    public String getLockPolicy() {
        return lockPolicy;
    }

    public void setLockPolicy(String lockPolicy) {
        this.lockPolicy = lockPolicy;
    }

    public boolean getShared() {
        return shared;
    }

    public boolean getPrefetch() {
        return prefetch;
    }
    
    public boolean getHardLimit() {
        return hardLimit;
    }

    public void setPassivationTimeOut(int passivationTimeOut) {
        this.passivationTimeOut = passivationTimeOut;
    }

    public void setInactivityTimeOut(int inactivityTimeOut) {
        this.inactivityTimeOut = inactivityTimeOut;
    }

    public void setDeadlockTimeOut(int deadlockTimeOut) {
        this.deadlockTimeOut = deadlockTimeOut;
    }
    
    public void setReadTimeOut(int readTimeOut) {
        this.readTimeOut = readTimeOut;
    }

    public void setShared(boolean shared) {
        this.shared = shared;
    }

    public void setPrefetch(boolean prefetch) {
        this.prefetch = prefetch;
    }

    public void setHardLimit(boolean hardLimit) {
        this.hardLimit = hardLimit;
    }

    public int getPassivatedInstance() {
        return passivatedInstance;
    }

    public void setPassivatedInstance(int passivatedInstance) {
        this.passivatedInstance = passivatedInstance;
    }

    public int getRemovedInstance() {
        return removedInstance;
    }

    public void setRemovedInstance(int removedInstance) {
        this.removedInstance = removedInstance;
    }

    public int getUnusedInstance() {
        return unusedInstance;
    }

    public void setUnusedInstance(int unusedInstance) {
        this.unusedInstance = unusedInstance;
    }

    public int getPkNumber() {
        return pkNumber;
    }

    public void setPkNumber(int pk) {
        this.pkNumber = pk;
    }

    public int getUsedInTxInstance() {
        return usedInTxInstance;
    }

    public void setUsedInTxInstance(int usedInTxInstance) {
        this.usedInTxInstance = usedInTxInstance;
    }

    public int getUsedOutTxInstance() {
        return usedOutTxInstance;
    }

    public void setUsedOutTxInstance(int usedOutTxInstance) {
        this.usedOutTxInstance = usedOutTxInstance;
    }
    
    public int getCurrentWaiters() {
        return currentWaiters;
    }
    
    public void setCurrentWaiters(int nb) {
        currentWaiters = nb;
    }

}
