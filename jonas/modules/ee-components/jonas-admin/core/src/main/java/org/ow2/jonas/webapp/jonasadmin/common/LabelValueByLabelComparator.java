/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.common;

import java.util.Comparator;

import org.ow2.jonas.webapp.taglib.LabelValueBean;



/**
 * @author Michel-Ange ANTON
 */
public class LabelValueByLabelComparator implements Comparator {

// --------------------------------------------------------- Public Methods

    public int compare(Object p_O1, Object p_O2) {
        LabelValueBean o1 = (LabelValueBean) p_O1;
        LabelValueBean o2 = (LabelValueBean) p_O2;
        return o1.getLabel().compareToIgnoreCase(o2.getLabel());
    }

    public boolean equals(Object p_Obj) {
        if (p_Obj instanceof LabelValueBean) {
            return (compare(this, p_Obj) == 0);
        }
        return false;
    }
}