/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Michel-Ange ANTON
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.security;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.ow2.jonas.webapp.jonasadmin.Jlists;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class GroupMemoryRealmForm extends ActionForm {

// --------------------------------------------------------- Constants

// --------------------------------------------------------- Properties variables

    private String action = null;
    private String group = null;
    private String description = null;
    private java.util.ArrayList listRolesGroup = new ArrayList();
    private java.util.ArrayList listRolesRealm = new ArrayList();
    private java.util.ArrayList listRolesUsed = new ArrayList();
    private java.util.ArrayList listRolesNotused = new ArrayList();
    private String rolesUsed = null;
    private String rolesNotused = null;
    private String[] rolesNotusedSelected = new String[0];
    private String[] rolesUsedSelected = new String[0];

// --------------------------------------------------------- Public Methods

    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        rolesUsed = null;
        rolesNotused = null;
        // Mandatory !
        rolesNotusedSelected = new String[0];
        rolesUsedSelected = new String[0];
    }

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors oErrors = new ActionErrors();
        // Create errors
        if (action.equals("create") == true) {
            group = group.trim();
            if (group.length() == 0) {
                oErrors.add("group"
                    , new ActionMessage("error.security.factory.memory.realm.group.name.required"));
            }
        }
        // Replace the elements in their good place
        if (oErrors.size() > 0) {
            listRolesUsed = Jlists.getArrayList(rolesUsed, Jlists.SEPARATOR);
            listRolesNotused = Jlists.getArrayList(rolesNotused, Jlists.SEPARATOR);
        }
        return oErrors;
    }

// --------------------------------------------------------- Properties Methods

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public java.util.ArrayList getListRolesGroup() {
        return listRolesGroup;
    }

    public void setListRolesGroup(java.util.ArrayList listRolesGroup) {
        this.listRolesGroup = listRolesGroup;
    }

    public java.util.ArrayList getListRolesRealm() {
        return listRolesRealm;
    }

    public void setListRolesRealm(java.util.ArrayList listRolesRealm) {
        this.listRolesRealm = listRolesRealm;
    }

    public java.util.ArrayList getListRolesUsed() {
        return listRolesUsed;
    }

    public void setListRolesUsed(java.util.ArrayList listRolesUsed) {
        this.listRolesUsed = listRolesUsed;
    }

    public java.util.ArrayList getListRolesNotused() {
        return listRolesNotused;
    }

    public void setListRolesNotused(java.util.ArrayList listRolesNotused) {
        this.listRolesNotused = listRolesNotused;
    }

    public String getRolesUsed() {
        return rolesUsed;
    }

    public void setRolesUsed(String rolesUsed) {
        this.rolesUsed = rolesUsed;
    }

    public String getRolesNotused() {
        return rolesNotused;
    }

    public void setRolesNotused(String rolesNotused) {
        this.rolesNotused = rolesNotused;
    }

    public String[] getRolesNotusedSelected() {
        return rolesNotusedSelected;
    }

    public void setRolesNotusedSelected(String[] rolesNotusedSelected) {
        this.rolesNotusedSelected = rolesNotusedSelected;
    }

    public String[] getRolesUsedSelected() {
        return rolesUsedSelected;
    }

    public void setRolesUsedSelected(String[] rolesUsedSelected) {
        this.rolesUsedSelected = rolesUsedSelected;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

}