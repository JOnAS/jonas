package org.ow2.jonas.webapp.jonasadmin.monitoring;

import org.apache.struts.action.ActionForm;

public class TomcatMemberForm extends ActionForm {
    /**
     * The member's name
     */
    private String name = null;
    /**
     * The member's host
     */
    private String host = null;
    /**
     * The member's state
     */
    private String state = null;

    // ---------- Receiver info ---------------------//
    /**
     * tcp listener address
     */
    private String tcpListenAddress = null;
    /**
     * tcp listener port
     */
    private int tcpListenPort;

    private int tcpThreadCount;

    private long tcpReceivedBytes;

    private String receiverInfo;

    // -------------- Sender info -------------------//
    private String replicationMode;

    private long ackTimeout;

    private boolean autoConnect;

    private boolean compress;

    private boolean doTransmitterProcessingStats;

    private boolean waitForAck;

    private String senderInfo;

    // -------------- Accessors -------------------//
    public String getSenderInfo() {
        return senderInfo;
    }
    public void setSenderInfo(String senderInfo) {
        this.senderInfo = senderInfo;
    }
    public String getReceiverInfo() {
        return receiverInfo;
    }
    public void setReceiverInfo(String receiverInfo) {
        this.receiverInfo = receiverInfo;
    }
    public long getTcpReceivedBytes() {
        return tcpReceivedBytes;
    }
    public void setTcpReceivedBytes(long tcpReceivedBytes) {
        this.tcpReceivedBytes = tcpReceivedBytes;
    }
    public int getTcpThreadCount() {
        return tcpThreadCount;
    }
    public void setTcpThreadCount(int tcpThreadCount) {
        this.tcpThreadCount = tcpThreadCount;
    }
    public String getHost() {
        return host;
    }
    public void setHost(String host) {
        this.host = host;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getTcpListenAddress() {
        return tcpListenAddress;
    }
    public void setTcpListenAddress(String tcpListenAddress) {
        this.tcpListenAddress = tcpListenAddress;
    }
    public int getTcpListenPort() {
        return tcpListenPort;
    }
    public void setTcpListenPort(int tcpListenPort) {
        this.tcpListenPort = tcpListenPort;
    }
    public long getAckTimeout() {
        return ackTimeout;
    }
    public void setAckTimeout(long ackTimeout) {
        this.ackTimeout = ackTimeout;
    }
    public boolean isAutoConnect() {
        return autoConnect;
    }
    public void setAutoConnect(boolean autoConnect) {
        this.autoConnect = autoConnect;
    }
    public boolean isDoTransmitterProcessingStats() {
        return doTransmitterProcessingStats;
    }
    public void setDoTransmitterProcessingStats(boolean doTransmitterProcessingStats) {
        this.doTransmitterProcessingStats = doTransmitterProcessingStats;
    }
    public String getReplicationMode() {
        return replicationMode;
    }
    public void setReplicationMode(String replicationMode) {
        this.replicationMode = replicationMode;
    }
    public boolean isWaitForAck() {
        return waitForAck;
    }
    public void setWaitForAck(boolean waitForAck) {
        this.waitForAck = waitForAck;
    }
    public boolean isCompress() {
        return compress;
    }
    public void setCompress(boolean compress) {
        this.compress = compress;
    }
    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }
}
