/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.logging;

import java.io.IOException;
import java.util.ArrayList;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.common.ItemsForm;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Michel-Ange ANTON
 */
public class ApplyLoggersAction extends JonasBaseAction {

// --------------------------------------------------------- Public Methods

    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws IOException, ServletException {

        // Form used
        ItemsForm oForm = (ItemsForm) p_Form;

        // Actions
        try {
            // Select
            ArrayList alSelected = new ArrayList();
            ObjectName on;
            for (int i = 0; i < oForm.getSelectedItemsArray().length; i++) {
				String selectedItem = oForm.getSelectedItemsArray()[i];
				String hostName = ObjectName.getInstance(selectedItem).getKeyProperty("host");
				String containerName = null;
				String type = null;
				String containerType = null;
                if (hostName == null) {
                    type = LoggerItem.LOGGER_CATALINA_ACCESS_ENGINE;
					containerType = m_Resources.getMessage("label.loggers.container.engine");
                } else {
                    type = LoggerItem.LOGGER_CATALINA_ACCESS_HOST;
					// For the moment it can be only a host container but latter
					// we will consider Context container case also
					containerName = hostName;
					containerType = m_Resources.getMessage("label.loggers.container.host");
                }
                alSelected.add(new LoggerItem(m_Resources.getMessage("logger.catalina.access.name")
                    , type, null, selectedItem, containerType, containerName));
            }
            oForm.setSelectedItemsList(alSelected);
        }
        catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }

        // Forward to the connector display page or the list if create
        return p_Mapping.findForward("Loggers Confirm");
    }
}
