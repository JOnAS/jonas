/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.jtm;

import org.ow2.jonas.lib.management.extensions.base.NameItem;

/**
 * @author Tony Ortiz
 */
public class TxXaresource implements NameItem {

// --------------------------------------------------------- Properties variables

    private String name = null;
    private String sindex = null;
    private String resmgr = null;
    private String xares = null;
    private String fullxid = null;
    private String xid = null;
    private String xidstate = null;
    private String objectName = null;

// --------------------------------------------------------- Public Methods

    public TxXaresource(String p_Sindex, String p_Resmgr, String p_Xares, String p_FullXid, String p_Xid, String p_Xidstate) {

        if (p_Sindex == null) {
            setSindex (p_Resmgr + '\n' + p_Xares + '\n' +  p_FullXid + '\n' + p_Xidstate);
        } else {
            setSindex(p_Sindex);
        }
        
        setResmgr(p_Resmgr);
        setXares(p_Xares);
        setFullxid(p_FullXid);
        setXid(p_Xid);
        setXidstate(p_Xidstate);
    }

// --------------------------------------------------------- Properties Methods
    public String getSindex() {
        return sindex;
    }

    public void setSindex(String sindex) {
        this.sindex = sindex;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getResmgr() {
        return resmgr;
    }

    public void setResmgr(String resmgr) {
        this.resmgr = resmgr;
    }

    public String getXares() {
        return xares;
    }

    public void setXares(String xares) {
        this.xares = xares;
    }

    public String getFullxid() {
        return fullxid;
    }

    public void setFullxid(String fullxid) {
        this.fullxid = fullxid;
    }

    public String getXid() {
        return xid;
    }

    public void setXid(String xid) {
        this.xid = xid;
    }

    public String getXidstate() {
        return xidstate;
    }

    public void setXidstate(String xidstate) {
        this.xidstate = xidstate;
    }

}