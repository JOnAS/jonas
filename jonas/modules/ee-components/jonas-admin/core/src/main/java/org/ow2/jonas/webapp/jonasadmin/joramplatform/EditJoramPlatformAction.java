/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 */

package org.ow2.jonas.webapp.jonasadmin.joramplatform;

import javax.management.ObjectName;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.util.JoramObjectName;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;


/**
 * @author Frederic MAISTRE
 */

public class EditJoramPlatformAction extends EditJoramBaseAction {

// --------------------------------------------------------- Public Methods

    @Override
    public ActionForward executeAction(final ActionMapping pMapping, final ActionForm pForm
        , final HttpServletRequest pRequest, final HttpServletResponse pResponse) {

        // Current JOnAS server
        String jonasServerName =  m_WhereAreYou.getCurrentJonasServerName();

        // Force the node selected in tree
        m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_DOMAIN) + WhereAreYou.NODE_SEPARATOR
            + "joramplatform", true);

        // Form used
        JoramPlatformForm oForm = new JoramPlatformForm();
        try {
            // Object name used
            ObjectName joramAdapterON = JoramObjectName.joramAdapter();
            boolean isAdapterLoaded = JonasManagementRepr.isRegistered(joramAdapterON, jonasServerName);
            // local server Id is given by ServerId in the JoramAdapter MBean
            Short localServerId = (Short) JonasManagementRepr.getAttribute(joramAdapterON, "ServerId", jonasServerName);
            // get the other (remote) Joram servers
            Short[] serversIds = (Short[]) JonasManagementRepr.getAttribute(joramAdapterON, "ServersIds", jonasServerName);
            int nbRemoteServers = serversIds.length - 1;
            String[] remoteServers = new String[nbRemoteServers];
            ItemServer[] remoteItemServers = new ItemServer[nbRemoteServers];
            if (serversIds != null) {
                int i = 0;
                for (int index = 0; index < serversIds.length; index++) {
                    Short id = serversIds[index];
                    if (!id.equals(localServerId)) {
                         remoteServers[i] = id.toString();
                         // check for DMQ
                         Object[] asParam = {
                                 id
                         };
                         String[] asSignature = {
                                 "short"
                         };
                         String dmqId = (String) JonasManagementRepr.invoke(joramAdapterON, "getDefaultDMQId", asParam, asSignature, jonasServerName);
                         String dmqName = null;
                         if (dmqId != null) {
                            ObjectName dmqOn = getDmqOn(dmqId, id.toString(), jonasServerName);
                            if (dmqOn != null) {
                                dmqName = (String) JonasManagementRepr.getAttribute(dmqOn, "AdminName", jonasServerName);
                            }
                         }
                         Integer threshold = (Integer) JonasManagementRepr.invoke(joramAdapterON, "getDefaultThreshold", asParam, asSignature, jonasServerName);
                         ItemServer itemServer = new ItemServer(id.toString(), dmqName);
                         itemServer.setDefaultThreshold(threshold.intValue());
                         remoteItemServers[i] = itemServer;
                         i++;
                    }
                }
            }
            oForm.setAdapterLoaded(isAdapterLoaded);
            oForm.setLocalServer(localServerId.toString());
            oForm.setRemoteServers(remoteServers);
            oForm.setRemoteItemServers(remoteItemServers);
            oForm.setConfiguration((String) JonasManagementRepr.getAttribute(joramAdapterON, "Configuration", jonasServerName));
            //oForm.setLocalPort(getIntegerAttribute(joramPlatformON, "LocalPort"));
            oForm.setLocalPort(getIntegerAttribute(joramAdapterON, "ServerPort"));
            //oForm.setLocalHost(getStringAttribute(joramPlatformON, "LocalHost"));
            oForm.setLocalHost(getStringAttribute(joramAdapterON, "HostName"));
            oForm.setDefaultThreshold(getIntegerAttribute(joramAdapterON, "DefaultThreshold"));
            if (isAdapterLoaded) {
                // Get configuration info from RA
                //oForm.setVersion(joramAdapterON.getKeyProperty("version"));
                oForm.setVersion(getStringAttribute(joramAdapterON, "ProviderVersion"));
                oForm.setCollocatedServer(getBooleanAttribute(joramAdapterON, "Collocated"));
                //oForm.setConfigDir(getStringAttribute(joramAdapterON, "PlatformConfigDir"));
                // TODO this is null
                oForm.setConfigDir("");
                oForm.setAdminFile(getStringAttribute(joramAdapterON, "AdminFileXML"));
                oForm.setPersistentServer(getBooleanAttribute(joramAdapterON, "PersistentPlatform"));
                oForm.setServerIdTxt(toStringShortAttribute(joramAdapterON, "ServerId"));
                oForm.setServerName(getStringAttribute(joramAdapterON, "Storage"));
                oForm.setHostName(getStringAttribute(joramAdapterON, "HostName"));
                oForm.setServerPortTxt(toStringIntegerAttribute(joramAdapterON, "ServerPort"));
                oForm.setCnxPendingTimerTxt(toStringIntegerAttribute(joramAdapterON, "CnxPendingTimer"));
                oForm.setConnectingTimerTxt(toStringIntegerAttribute(joramAdapterON, "ConnectingTimer"));
                oForm.setTxPendingTimerTxt(toStringIntegerAttribute(joramAdapterON, "TxPendingTimer"));
                oForm.setTimeOutToAbortRequest(getLongAttribute(joramAdapterON, "TimeOutToAbortRequest"));
                String defaultDmqId = getStringAttribute(joramAdapterON, "DefaultDMQId");
                ObjectName defaultDmqOn = getDmqOn(defaultDmqId, jonasServerName);
                if (defaultDmqOn != null) {
                    String defaultDmqName = (String) JonasManagementRepr.getAttribute(defaultDmqOn, "AdminName", jonasServerName);
                    oForm.setDefaultDMQ(defaultDmqName);
                }
            } else {
            // TODO
         }

                m_Session.setAttribute("joramPlatformForm", oForm);
                m_Session.setAttribute("localId", oForm.getLocalServer());
        } catch (Throwable t) {
            return (treatError(t, pMapping, pRequest));
        }

        // Forward to the jsp.
        return (pMapping.findForward("JoramPlatform"));
    }

}
