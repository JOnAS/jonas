/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.resource;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.webapp.jonasadmin.deploy.BaseDeployAction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * Helper for actions for the deployment of Mail Factories
 * @author Adriana Danes
 */
public class EditMailFactoryPropertiesAction extends BaseDeployAction {

    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws IOException, ServletException {

        String sAction = p_Request.getParameter("action");
        MailFactoryPropertiesForm oForm = new MailFactoryPropertiesForm();
        oForm.reset(p_Mapping, p_Request);
        m_Session.setAttribute("mailFactoryPropertiesForm", oForm);
        String name = "";
        Properties props = new Properties();
        try {
            populate(name, props, oForm);
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }

        // Forward to the jsp.
        return (p_Mapping.findForward("MailFactory Properties"));
    }
    /**
     * Populate a form with configuration properties read in a configuration file
     *
     * @param mailFactoryName the name of the mail factory
     * @param props properties to configure the mail factory
     * param pForm the form used to print-out the configuration properties
     */
    void populate(String mailFactoryName, Properties props, MailFactoryPropertiesForm pForm) throws Exception {
        // Set configuration attributes
        // - mail factory name
        pForm.setMailFactoryName(mailFactoryName);
        // - jndi name
        pForm.setJndiName(getStringAttribute(props, "mail.factory.name", pForm.getJndiName()));
        props.remove("mail.factory.name");
        // - type
        pForm.setType(getStringAttribute(props, "mail.factory.type", pForm.getType()));
        props.remove("mail.factory.type");

        // Set authentication properties
        pForm.setUsername(getStringAttribute(props, "mail.authentication.username", pForm.getUsername()));
        props.remove("mail.authentication.username");
        pForm.setPassword(getStringAttribute(props, "mail.authentication.password", pForm.getPassword()));
        props.remove("mail.authentication.password");

        // Set mime part properties
        pForm.setTo(getStringAttribute(props, "mail.to", pForm.getTo()));
        pForm.setSubject(getStringAttribute(props, "mail.subject", pForm.getSubject()));
        pForm.setCc(getStringAttribute(props, "mail.cc", pForm.getCc()));
        pForm.setBcc(getStringAttribute(props, "mail.bcc", pForm.getBcc()));
        props.remove("mail.to");
        props.remove("mail.subject");
        props.remove("mail.cc");
        props.remove("mail.bcc");

        // Set the rest of the properties as session properties
        String aName = null;
        String aValue = null;
        StringBuffer bufSessionProps = new StringBuffer();
        for(Enumeration pNames = props.propertyNames(); pNames.hasMoreElements() ; ) {
            aName = (String)pNames.nextElement();
            aValue = (String)props.getProperty(aName);
            bufSessionProps.append(aName);
            bufSessionProps.append("=");
            bufSessionProps.append(aValue);
            bufSessionProps.append(",");
            bufSessionProps.append("\n");
        }
        if (!props.isEmpty()) {
            // delete the last 2 chars (, and \n)
            bufSessionProps.delete(bufSessionProps.length() - 2, bufSessionProps.length() - 1);
        } else {
            // Actually no chars in sessionPropsBuf
            //System.out.println("no session props, append mail.host= ");
            bufSessionProps.append("mail.host=");
        }
        String sSessionProps = new String(bufSessionProps);
        pForm.setSessionProps(sSessionProps);
    }

    /**
     * Use a form's content to create the Properties object needed to create and load a Mail factory
     *
     */
    Properties getPropsFromForm(MailFactoryPropertiesForm p_Form, boolean mimepart) throws Exception {
        Properties oProps = new Properties();
        Properties sProps = getPropsFromString(p_Form.getSessionProps());
        oProps.putAll(sProps);
        setStringAttribute(oProps, "mail.factory.name", p_Form.getJndiName());
        setStringAttribute(oProps, "mail.factory.type", p_Form.getType());
        setStringAttribute(oProps, "mail.authentication.username", p_Form.getUsername());
        setStringAttribute(oProps, "mail.authentication.password", p_Form.getPassword());
        String aName = null;
        String aValue = null;
        for(Enumeration pNames = oProps.propertyNames(); pNames.hasMoreElements() ; ) {
            aName = (String) pNames.nextElement();
            aValue = (String) oProps.getProperty(aName);
        }
        if (mimepart == true) {
            setStringAttribute(oProps, "mail.to", p_Form.getTo());
            setStringAttribute(oProps, "mail.subject", p_Form.getSubject());
            setStringAttribute(oProps, "mail.cc", p_Form.getCc());
            setStringAttribute(oProps, "mail.bcc", p_Form.getBcc());
        }
        return oProps;
    }
}
