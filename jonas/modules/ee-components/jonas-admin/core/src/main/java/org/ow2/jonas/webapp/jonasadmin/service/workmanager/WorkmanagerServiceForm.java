package org.ow2.jonas.webapp.jonasadmin.service.workmanager;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class WorkmanagerServiceForm extends ActionForm {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String action = null;

    private int minPoolSize = 0;
    private int maxPoolSize = 0;
    private int currentPoolSize = 0;

    public int getMinPoolSize() {
        return minPoolSize;
    }
    public void setMinPoolSize(final int minPoolSize) {
        this.minPoolSize = minPoolSize;
    }
    public int getMaxPoolSize() {
        return maxPoolSize;
    }
    public void setMaxPoolSize(final int maxPoolSize) {
        this.maxPoolSize = maxPoolSize;
    }
    public int getCurrentPoolSize() {
        return currentPoolSize;
    }
    public void setCurrentPoolSize(final int currentPoolSize) {
        this.currentPoolSize = currentPoolSize;
    }
    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    @Override
    public void reset(final ActionMapping mapping, final HttpServletRequest request) {
        minPoolSize = 0;
        maxPoolSize = 0;
        currentPoolSize = 0;
    }
    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    @Override
    public ActionErrors validate(final ActionMapping mapping, final HttpServletRequest request) {
        return new ActionErrors();
        // TO DO voir DatasourceForm
    }
    public String getAction() {
        return action;
    }
    public void setAction(final String action) {
        this.action = action;
    }
}
