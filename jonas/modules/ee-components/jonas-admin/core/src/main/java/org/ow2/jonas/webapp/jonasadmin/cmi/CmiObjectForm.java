/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.cmi;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.management.Attribute;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

public class CmiObjectForm extends ActionForm {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    public String name = null ;

    /**
     * The interface name
     */
    public String itfName = null;
    Set<Attribute> properties = null;
    /**
     * Minimum pool size.
     */
    int minPoolSize = 0;

    /**
     * Maximum pool size.
     */
    int maxPoolSize = 0;

    /**
     * Maximum pool waiters.
     */
    int maxPoolWaiters = 0;


    /**
     * Pool timeout.
     */
    long poolTimeout = 0L;

    /**
     * @return the poolTimeout.
     */
    public long getPoolTimeout() {
        return poolTimeout;
    }
    /**
     * @param poolTimeout the poolTimeout to set.
     */
    public void setPoolTimeout(final long poolTimeout) {
        this.poolTimeout = poolTimeout;
    }
    //List of string representing server refs for the object
    List<String> serverRefs = new ArrayList<String>();
    /**
     * Simple properties fo lb policy
     */
    Set<ClusterPropertyAttribute> simpleProperties = new HashSet();
    /**
     * The object's cluster name
     */
    String clusterName = null;

    String action = null;
    /**
     * Checks if user has set a new Lb policy.
     */
    String newLbPolicyCheck = null;
    /**
     * Value of the new Lb policy class.
     */
    FormFile newLbPolicyClass = null;

    /**
     * Checks if user has set a new LB strategy.
     */
    String newLbStrategyCheck = null;
    /**
     * Value of the new Lb strategy.
     */
    FormFile newLbStrategy = null;
    /**
     * current value of the html:select
     */
    private String currentLBPolicySelect =  null;
    /**
     * current value of the html:select
     */
    private String currentLBStrategySelect  = null;
    /**
     * current value for the object.
     */
    private LBAttribute currentLBPolicy = new LBAttribute();
    /**
     * current value for the object.
     */
    private LBAttribute currentLBStrategy = new LBAttribute();


    /**
     * Current selected simple property(stringified). Used for presentations purposes.
     */
    private String currentSimpPropertySelect = null;


    /**
     * Value of current simple property. Used for presentations purposes.
     */
    String currentSimpPropertyValue = null;
    /**
     * @return the currentSimpPropertyValue
     */
    public String getCurrentSimpPropertyValue() {
        return currentSimpPropertyValue;
    }
    /**
     * @param currentSimpPropertyValue the currentSimpPropertyValue to set
     */
    public void setCurrentSimpPropertyValue(final String currentSimpPropertyValue) {
        this.currentSimpPropertyValue = currentSimpPropertyValue;
    }
    /**
     * @return the currentSimpPropertySelect
     */
    public String getCurrentSimpPropertySelect() {
        return currentSimpPropertySelect;
    }
    /**
     * @param currentSimpPropertySelect the currentSimpPropertySelect to set
     */
    public void setCurrentSimpPropertySelect(final String currentSimpPropertySelect) {
        this.currentSimpPropertySelect = currentSimpPropertySelect;
    }
    /**
     * @return the itfName
     */
    public String getItfName() {
        return itfName;
    }
    /**
     * @param itfName the itfName to set
     */
    public void setItfName(final String itfName) {
        this.itfName = itfName;
    }
    /**
     * @return the maxPoolSize
     */
    public int getMaxPoolSize() {
        return maxPoolSize;
    }
    /**
     * @param maxPoolSize the maxPoolSize to set
     */
    public void setMaxPoolSize(final int maxPoolSize) {
        this.maxPoolSize = maxPoolSize;
    }
    /**
     * @return the minPoolSize
     */
    public int getMinPoolSize() {
        return minPoolSize;
    }
    /**
     * @param minPoolSize the minPoolSize to set
     */
    public void setMinPoolSize(final int minPoolSize) {
        this.minPoolSize = minPoolSize;
    }

    /**
     * @return the minPoolSize
     */
    public int getMaxPoolWaiters() {
        return maxPoolWaiters;
    }
    /**
     * @param minPoolSize the minPoolSize to set
     */
    public void setMaxPoolWaiters(final int maxPoolWaiters) {
        this.maxPoolWaiters = maxPoolWaiters;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @param name the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }
    /**
     * @return the properties
     */
    public Set<Attribute> getProperties() {
        return properties;
    }
    /**
     * @param properties the properties to set
     */
    public void setProperties(final Set<Attribute> properties) {
        this.properties = properties;
    }
    /**
     * @return the serverRefs
     */
    public List<String> getServerRefs() {
        return serverRefs;
    }
    /**
     * @param serverRefs the serverRefs to set
     */
    public void setServerRefs(final List<String> serverRefs) {
        this.serverRefs = serverRefs;
    }
    /**
     * @return the simpleProperties
     */
    public Set<ClusterPropertyAttribute> getSimpleProperties() {
        return simpleProperties;
    }
    /**
     * @param simpleProperties the simpleProperties to set
     */
    public void setSimpleProperties(final Set<ClusterPropertyAttribute> simpleProperties) {
        this.simpleProperties = simpleProperties;
    }
    /**
     * @return the clusterName
     */
    public String getClusterName() {
        return clusterName;
    }
    /**
     * @param clusterName the clusterName to set
     */
    public void setClusterName(final String clusterName) {
        this.clusterName = clusterName;
    }
    /**
     * @return the action
     */
    public String getAction() {
        return action;
    }
    /**
     * @param action the action to set
     */
    public void setAction(final String action) {
        this.action = action;
    }
    /**
     * @return
     */
    public String getNewLbPolicyCheck() {
        return newLbPolicyCheck;
    }
    /**
     * @param newLbPolicyCheck
     */
    public void setNewLbPolicyCheck(final String newLbPolicyCheck) {
        this.newLbPolicyCheck = newLbPolicyCheck;
    }
    public FormFile getNewLbPolicyClass() {
        return newLbPolicyClass;
    }
    /**
     * @param newLbPolicyClass
     */
    public void setNewLbPolicyClass(final FormFile newLbPolicyClass) {
        this.newLbPolicyClass = newLbPolicyClass;
    }
    /**
     * @return
     */
    public String getNewLbStrategyCheck() {
        return newLbStrategyCheck;
    }
    /**
     * @param newLbStrategyCheck
     */
    public void setNewLbStrategyCheck(final String newLbStrategyCheck) {
        this.newLbStrategyCheck = newLbStrategyCheck;
    }
    /**
     * @return
     */
    public FormFile getNewLbStrategy() {
        return newLbStrategy;
    }
    /**
     * @param newLbStrategy
     */
    public void setNewLbStrategy(final FormFile newLbStrategy) {
        this.newLbStrategy = newLbStrategy;
    }
    public LBAttribute getCurrentLBPolicy() {
        return currentLBPolicy;
    }
    public void setCurrentLBPolicy(final LBAttribute currentLBPolicy) {
        this.currentLBPolicy = currentLBPolicy;
        this.currentLBPolicySelect = currentLBPolicy.getValue();
    }
    public LBAttribute getCurrentLBStrategy() {
        return currentLBStrategy;
    }
    public void setCurrentLBStrategy(final LBAttribute currentLBStrategy) {
        this.currentLBStrategy = currentLBStrategy;
        this.currentLBStrategySelect = currentLBStrategy.getValue();
    }
    public String getCurrentLBPolicySelect() {
        return currentLBPolicySelect;
    }
    public void setCurrentLBPolicySelect(final String currentLBPolicySelect) {
        this.currentLBPolicySelect = currentLBPolicySelect;
    }
    public String getCurrentLBStrategySelect() {
        return currentLBStrategySelect;
    }
    public void setCurrentLBStrategySelect(final String currentLBStrategySelect) {
        this.currentLBStrategySelect = currentLBStrategySelect;
    }
}
