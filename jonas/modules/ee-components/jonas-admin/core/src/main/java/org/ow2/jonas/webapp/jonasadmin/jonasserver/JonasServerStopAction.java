/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2009 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.jonasserver;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.management.javaee.J2eeObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;

/**
 * @author Michel-Ange Anton
 */
public class JonasServerStopAction extends JonasBaseAction {

// --------------------------------------------------------- Public Methods

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param pMapping The ActionMapping used to select this instance
     * @param pForm The optional ActionForm bean for this request (if any)
     * @param pRequest The HTTP request we are processing
     * @param pResponse The HTTP response we are creating
     *
     * @return An <code>ActionForward</code> instance or <code>null</code>
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */
    @Override
    public ActionForward executeAction(final ActionMapping pMapping, final ActionForm pForm
        , final HttpServletRequest pRequest, final HttpServletResponse pResponse)
        throws IOException, ServletException {
        try {
            // Object name used
            String serverName =  m_WhereAreYou.getCurrentJonasServerName();
            ObjectName oObjectName = J2eeObjectName.J2EEServer(m_WhereAreYou.getCurrentDomainName()
                , serverName);
            JonasManagementRepr.invoke(oObjectName, "stop", null, null, serverName);
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(pRequest, m_Errors);
            return (pMapping.findForward("Global Error"));
        }

        // Forward to the jsp.
        return (pMapping.findForward("JonasServer"));
    }
}
