/**
 * JOnAS: Java(TM) Open Application Server Copyright (C) 2005 Bull S.A. Contact:
 * jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.webapp.jonasadmin.xml.xs;

import org.ow2.jonas.webapp.jonasadmin.xml.xs.hardcoded.HCSchemaRestrictions;

/**
 * A factory to create a schema restriction implementation.
 *
 * @author Gregory Lapouchnian
 * @author Patrick Smith
 */
public class SchemaRestrictionsFactory {

    /**
     * Return a new implementation of a schema restriction.
     * @param type the type of archive.
     * @return an instance of an implementation of schema restriction.
     */
    public SchemaRestrictions getSchemaRestrictions(int type) {
        return new HCSchemaRestrictions(type);
    }
}
