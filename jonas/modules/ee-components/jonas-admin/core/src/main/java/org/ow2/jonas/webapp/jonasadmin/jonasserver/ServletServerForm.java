/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.jonasserver;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author Michel-Ange ANTON
 */

public final class ServletServerForm extends ActionForm {

// ------------------------------------------------------------- Properties Variables

    private String serverName = null;
    private String serverVersion = null;
    private boolean serverCatalina = false;
    private String serverCatalinaService = null;
    private String serverCatalinaEngine = null;
    private String serverCatalinaDefaultHost = null;
    private String forwardAfter = null;

// ------------------------------------------------------------- Public Methods

    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {
    }

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     * @return List of errors
     */
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        return new ActionErrors();
    }

// ------------------------------------------------------------- Properties Methods

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getServerVersion() {
        return serverVersion;
    }

    public void setServerVersion(String serverVersion) {
        this.serverVersion = serverVersion;
    }

    public String getServerCatalinaService() {
        return serverCatalinaService;
    }

    public void setServerCatalinaService(String serverCatalinaService) {
        this.serverCatalinaService = serverCatalinaService;
    }

    public boolean isServerCatalina() {
        return serverCatalina;
    }

    public void setServerCatalina(boolean serverCatalina) {
        this.serverCatalina = serverCatalina;
    }

    public String getServerCatalinaEngine() {
        return serverCatalinaEngine;
    }

    public void setServerCatalinaEngine(String serverCatalinaEngine) {
        this.serverCatalinaEngine = serverCatalinaEngine;
    }

    public String getServerCatalinaDefaultHost() {
        return serverCatalinaDefaultHost;
    }

    public void setServerCatalinaDefaultHost(String serverCatalinaDefaultHost) {
        this.serverCatalinaDefaultHost = serverCatalinaDefaultHost;
    }

    public String getForwardAfter() {
        return forwardAfter;
    }

    public void setForwardAfter(String forwardAfter) {
        this.forwardAfter = forwardAfter;
    }

}
