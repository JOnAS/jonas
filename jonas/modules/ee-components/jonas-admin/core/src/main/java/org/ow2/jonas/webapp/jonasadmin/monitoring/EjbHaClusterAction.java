package org.ow2.jonas.webapp.jonasadmin.monitoring;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


public class EjbHaClusterAction extends JonasBaseAction {

    public ActionForward executeAction(ActionMapping p_Mapping,
            ActionForm p_Form, HttpServletRequest p_Request,
            HttpServletResponse p_Response) throws IOException,
            ServletException {
        String domainName = m_WhereAreYou.getCurrentDomainName();
        // Get cluster name from the 'clust' parameter
        String name = p_Request.getParameter("clust");
        if (name == null) {
            addGlobalError(new Exception("EjbHaClusterAction: clust parameter is null."));
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }

        EjbHaClusterForm oForm = (EjbHaClusterForm) p_Form;
        oForm.setName(name);
        try {
            ObjectName on = JonasObjectName.cluster(domainName, name, "EjbHaCluster");
            String state = getStringAttribute(on, "State");
            oForm.setState(state);
            oForm.setMcastAddr(getStringAttribute(on, "McastAddr"));
            oForm.setMcastPort(getIntegerAttribute(on, "McastPort"));
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }

        // Forward to the jsp.
        return (p_Mapping.findForward("EjbHaCluster"));
    }
}
