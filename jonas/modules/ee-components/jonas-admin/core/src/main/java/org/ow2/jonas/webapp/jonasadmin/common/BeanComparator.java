/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.common;

import java.util.Comparator;

import org.apache.commons.beanutils.PropertyUtils;

/**
 * BeanComparator is a comparator for beans.
 * It's used to compare a bean with its own properties or with its own string value
 * (return by the <code>toString()</code> method).
 *
 * @author Michel-Ange ANTON
 */

public class BeanComparator implements Comparator {

// --------------------------------------------------------- Instance Variables

    private String[] m_Orders = null;

// --------------------------------------------------------- Constructors

    /**
     * Comparator by default (use the <code>toString()</code> methods).
     */
    public BeanComparator() {
        m_Orders = null;
    }

    /**
     * Comparator in order of the properties.
     *
     * @param p_Orders A array of the names of the properties
     */
    public BeanComparator(String[] p_Orders) {
        m_Orders = p_Orders;
    }

// --------------------------------------------------------- Public Methods

    /**
     * Compare two beans instances.
     *
     * @param p_O1 The first bean to compare
     * @param p_O2 The second bean to compare
     * @return 0 if equals, < 0 if first bean lower than second bean, > 0 if first bean upper than second bean.
     */
    public int compare(Object p_O1, Object p_O2) {
        if (m_Orders != null) {
            return compareOrder(p_O1, p_O2);
        }
        return compareDefault(p_O1, p_O2);
    }

    /**
     * Indentical bean.
     *
     * @param p_Obj The bean to compare.
     * @return True if identical.
     */
    public boolean equals(Object p_Obj) {
        if (this.getClass().getName().equals(p_Obj.getClass().getName())) {
            return (compare(this, p_Obj) == 0);
        }
        return false;
    }

    /**
     * Compare the null of two objects.
     *
     * @param p_O1 The first object to compare
     * @param p_O2 The second object to compare
     * @return 0 if the two are null or the two not null, < 0 if second object is null, > 0 if first object is null.
     */
    public static int compareNull(Object p_O1, Object p_O2) {
        int iRet = 0;
        if (p_O1 != p_O2) {
            if (p_O1 == null) {
                iRet = 1;
            }
            else if (p_O2 == null) {
                iRet = -1;
            }
        }
        return iRet;
    }

    /**
     * Compare two strings with the ignore case
     * but if is identical, compare normaly with case.
     *
     * @param p_O1 The first string to compare
     * @param p_O2 The second string to compare
     * @return 0 if equals, < 0 if first string lower than second string, > 0 if first string upper than second string.
     */
    public static int compareString(String p_S1, String p_S2) {
        int iRet = compareNull(p_S1, p_S2);
        if (iRet == 0) {
            try {
                iRet = p_S1.compareToIgnoreCase(p_S2);
                if (iRet == 0) {
                    iRet = p_S1.compareTo(p_S2);
                }
            }
            catch (NullPointerException e) {
                iRet = 0;
            }
        }
        return iRet;
    }

// --------------------------------------------------------- Protected Methods

    /**
     * Compare the beans like a string (used the <code>toString()</code> method of the object).
     * Used when order is unknown.
     *
     * @param p_O1 The first bean to compare
     * @param p_O2 The second bean to compare
     * @return 0 if equals, < 0 if first bean lower than second bean, > 0 if first bean upper than second bean.
     */
    protected int compareDefault(Object p_O1, Object p_O2) {
        int iRet = compareNull(p_O1, p_O2);
        if (iRet == 0) {
            iRet = compareString(p_O1.toString(), p_O2.toString());
        }
        return iRet;
    }

    /**
     * Compare the beans with the properties in order given.
     * Used when order is known.
     *
     * @param p_O1 The first bean to compare
     * @param p_O2 The second bean to compare
     * @return 0 if equals, < 0 if first bean lower than second bean, > 0 if first bean upper than second bean.
     */
    protected int compareOrder(Object p_O1, Object p_O2) {
        int iRet = 0;
        Object oValue1, oValue2;
        try {
            for (int i = 0; i < m_Orders.length; i++) {
                // Get values
                oValue1 = PropertyUtils.getProperty(p_O1, m_Orders[i]);
                oValue2 = PropertyUtils.getProperty(p_O2, m_Orders[i]);
                // Compare
                iRet = compareNull(oValue1, oValue2);
                if (iRet == 0) {
                    try {
                        iRet = compareString(oValue1.toString(), oValue2.toString());
                    }
                    catch (NullPointerException e) {
                        iRet = 0;
                    }
                }
                // Continue in order if always equals
                if (iRet != 0) {
                    break;
                }
            }
        }
        catch (Exception e) {
            System.err.println("[" + e.getClass().getName() + "] " + e.getMessage());
            iRet = 0;
        }
        return iRet;
    }
}