package org.ow2.jonas.webapp.jonasadmin.jonasmqconnect;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;
import org.ow2.jonas.webapp.jonasadmin.jonasmqconnect.util.MqObjectNames;
import org.ow2.jonas.webapp.jonasadmin.jonasmqconnect.util.PropertiesComparator;
import org.ow2.jonas.webapp.jonasadmin.jonasmqconnect.util.PropertiesUtil;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class MdbEditAction extends JonasBaseAction {

    public ActionForward executeAction(ActionMapping mapping, ActionForm form
            , HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        MdbEditForm fBean = (MdbEditForm) form;
        String name = fBean.getName();
        ArrayList properties;
        ObjectName mbName;

        WhereAreYou oWhere = (WhereAreYou) request.getSession().getAttribute(WhereAreYou.
                SESSION_NAME);
        String serverName = oWhere.getCurrentJonasServerName();
        String domainName = oWhere.getCurrentDomainName();

        try {
            ObjectName connectorON = MqObjectNames.getConnectorONByName(
                    domainName,
                    (String) m_Session.getAttribute("mqconnector"));
            mbName =  MqObjectNames.getMdbON(connectorON, name, serverName);
            ArrayList propertiesName = new ArrayList();
            propertiesName.add("Destination");
            propertiesName.add("DestinationType");
            propertiesName.add("DestinationProperties");
            propertiesName.add("MessageSelector");
            propertiesName.add("SubscriptionDurability");
            propertiesName.add("SubscriptionName");
            propertiesName.add("AcknowledgeMode");
            propertiesName.add("MaxMessages");
            propertiesName.add("PoolSize");
            propertiesName.add("InitPoolSize");
            propertiesName.add("MaxNumberOfWorks");
            propertiesName.add("MdbName");
            propertiesName.add("UserName");
            propertiesName.add("Password");
            propertiesName.add("ReconnectInterval");
            propertiesName.add("MaxReconnect");
            properties = PropertiesUtil.getProperties(mbName, "getProperty",
                propertiesName, serverName);
            Collections.sort(properties, new PropertiesComparator());
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(request, m_Errors);
            return (mapping.findForward("Global Error"));
        }
        fBean.setProperties(properties);
        return mapping.findForward("JonasMqConnectMdbEdit");
    }
}