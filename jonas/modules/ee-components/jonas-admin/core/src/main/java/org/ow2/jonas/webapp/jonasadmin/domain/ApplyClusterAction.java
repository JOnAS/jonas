/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.domain;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * Create a LogicalCluster by admin.
 * @author Adriana Danes
 */
public class ApplyClusterAction extends JonasBaseAction {

// --------------------------------------------------------- Public Methods
    @Override
    public ActionForward executeAction(final ActionMapping p_Mapping, final ActionForm p_Form
            , final HttpServletRequest p_Request, final HttpServletResponse p_Response)
    throws IOException, ServletException {

        // Form used
        NewClusterForm oForm = (NewClusterForm) p_Form;

        // Actions
        try {
            // ObjectName of the domain in which the new cluster has to be created
            ObjectName domainOn = m_WhereAreYou.getCurrentDomain();
            String serverName = m_WhereAreYou.getCurrentJonasServerName();
            String[] signature = {"java.lang.String" };
            String[] params = {oForm.getClusterName() };
            signature[0] = "java.lang.String";
            JonasManagementRepr.invoke(domainOn, "createCluster", params, signature, serverName);
            m_Session.setAttribute("mapChanged", new Boolean(true));
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }

        // Forward to the connector display page or the list if create
        return p_Mapping.findForward("ActionEditDomain");
    }
}
