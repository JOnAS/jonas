/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.deploy;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * Manage the list of modules that can be removed
 * @author Florent Benoit
 */

public class RemoveForm extends ActionForm {

    /**
     * List of removable modules
     */
    private List listRemovable = new ArrayList();

    /**
     * List of removed modules
     */
    private List listRemoved = new ArrayList();

    /**
     * List of modules to remove (select by user, based on removeSelected array)
     */
    private List listToBeRemoved = new ArrayList();

    /**
     * Array of user selected elements to be removed
     */
    private String[] removeSelected = new String[0];

    //private ArrayList listRemove = new ArrayList();
    private boolean confirm = false;

    /**
     * Is the current context a domain management action?
     */
    private boolean isDomain;

    /**
     * Is the current type of deployment currently configurable?
     */
    private boolean isConfigurable;
    
// --------------------------------------------------------- Public Methods

    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        removeSelected = new String[0];
    }

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors oErrors = new ActionErrors();
        return oErrors;
    }

    public List getListRemovable() {
        return listRemovable;
    }

    public void setListRemovable(ArrayList listRemovable) {
        this.listRemovable = listRemovable;
    }

    public String[] getRemoveSelected() {
        return removeSelected;
    }

    public void setRemoveSelected(String[] removeSelected) {
        this.removeSelected = removeSelected;
    }

    public boolean isConfirm() {
        return confirm;
    }

    public void setConfirm(boolean confirm) {
        this.confirm = confirm;
    }


    public List getListRemoved() {
        return listRemoved;
    }


    public void setListRemoved(List listRemoved) {
        this.listRemoved = listRemoved;
    }


    public List getListToBeRemoved() {
        return listToBeRemoved;
    }


    public void setListToBeRemoved(List listToBeRemoved) {
        this.listToBeRemoved = listToBeRemoved;
    }
    
    /**
     * Return if the action is a domain management operation.
     * @return if the action is a domain management operation.
     */
    public boolean getIsDomain() {
        return this.isDomain;
    }
    
    /**
     * Sets if the action is a domain management operation.
     * @param isDomain if the action is a domain management operation.
     */
    public void setIsDomain(boolean isDomain) {
        this.isDomain = isDomain;
    }
    
    /**
     * Return if the current deployment type is configurable.
     * @return if the current deployment type is configurable.
     */
    public boolean getIsConfigurable() {
        return this.isConfigurable;
    }
    
    /**
     * Sets if the current deployment type is configurable.
     * @param isDomain if current deployment type is configurable.
     */
    public void setIsConfigurable(boolean isConfigurable) {
        this.isConfigurable = isConfigurable;
    }
    

}
