/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Michel-Ange ANTON
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.resource;

import org.ow2.jonas.lib.management.extensions.base.NameItem;

public class ResourceItem implements NameItem {

// --------------------------------------------------------- Properties Variables

    private String path = null;
    private String nodeName = null;
    private String name = null;
    private String file = null;
    private String objectName = null;
    

// --------------------------------------------------------- Constructors

    public ResourceItem() {
    }

    public ResourceItem(String p_File, String p_Path) {
        setPath(p_Path);
        setFile(p_File);
    }

    public ResourceItem(String p_File, String p_Path, String p_ObjectName, String p_Name) {
        setPath(p_Path);
        setFile(p_File);
        setObjectName(p_ObjectName);       
        setName(p_Name);
    }

    public ResourceItem(String p_File, String p_Path, String p_NodeName, String p_ObjectName, String p_Name) {
        setPath(p_Path);
        setFile(p_File);
        setNodeName(p_NodeName);
        setName(p_Name);
        setObjectName(p_ObjectName);
    }

// --------------------------------------------------------- Properties Methods

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

}
