/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2003-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.catalina;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.webapp.jonasadmin.Jlists;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Michel-Ange ANTON
 */
public class CreateConnectorAction extends CatalinaBaseAction {

// --------------------------------------------------------- Public Methods

    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws IOException, ServletException {

        // Get the type of connector to create
        String sType = p_Request.getParameter("type");
        if (sType == null) {
            addGlobalError(new Exception("The parameter 'type' is null !!!!!"));
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }

        // Fill in the form values for display and editing
        ConnectorForm oForm = new ConnectorForm();
        m_Session.setAttribute("catalinaConnectorForm", oForm);
        oForm.reset(p_Mapping, p_Request);
        oForm.setAction("create");
        oForm.setBooleanVals(Jlists.getBooleanValues());

        if ("http".equalsIgnoreCase(sType) == true) {
            oForm.setConnectorType("HTTP");
            oForm.setScheme("http");
        }
        else if ("https".equalsIgnoreCase(sType) == true) {
            oForm.setConnectorType("HTTPS");
            oForm.setScheme("https");
        }
        else if ("ajp".equalsIgnoreCase(sType) == true) {
            oForm.setConnectorType("AJP");
            oForm.setScheme("http");
        }
        else {
            addGlobalError(new Exception("The parameter 'type' is unknown"));
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }

        // Forward to the connector display page
        return (p_Mapping.findForward("Catalina Connector"));
    }
}
