/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.jtm;

import org.ow2.jonas.lib.management.extensions.base.NameItem;

/**
 * @author Tony Ortiz
 */
public class TxRecovery implements NameItem {

// --------------------------------------------------------- Properties variables

    private String name = null;
    private String date = null;
    private String fulltrans = null;
    private String transaction = null;
    private String state = null;
    private String xidcount = null;

// --------------------------------------------------------- Public Methods

    public TxRecovery(String p_FullTrans, String p_Transaction, String p_Date, String p_Xidcount) {
        setFulltrans(p_FullTrans);
        setTransaction(p_Transaction);
        setDate(p_Date);
        setXidcount(p_Xidcount);
    }

// --------------------------------------------------------- Properties Methods

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    
    public String getFulltrans() {
        return fulltrans;
    }

    public void setFulltrans(String fulltrans) {
        this.fulltrans = fulltrans;
    }

    public String getTransaction() {
        return transaction;
    }

    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getXidcount() {
        return xidcount;
    }

    public void setXidcount(String xidcount) {
        this.xidcount = xidcount;
    }

}