/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2003-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.container;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.management.javaee.J2eeObjectName;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

/**
 * @author Michel-Ange ANTON
 * @author S. Ali Tokmen
 */
public class EditWebAppCatalinaAction extends BaseWebAppAction {

    @Override
    public ActionForward executeAction(final ActionMapping pMapping, final ActionForm pForm, final HttpServletRequest pRequest,
            final HttpServletResponse pResponse) throws IOException, ServletException {

        // Current server
        WhereAreYou oWhere = (WhereAreYou) pRequest.getSession().getAttribute(WhereAreYou.SESSION_NAME);
        String sServerName = oWhere.getCurrentJonasServerName();
        
        // Initialize form
        boolean bPopulate = initialize(pMapping, pRequest);
        String forward = "WebApp Catalina";

        try {
            ObjectName on = J2eeObjectName.getObjectName(mWebAppForm.getObjectName());
            
            // Force the node selected in tree
            if (mWebAppForm != null && mWebAppForm.getObjectName() != null) {
                m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_SERVER) + WhereAreYou.NODE_SEPARATOR + "services"
                        + WhereAreYou.NODE_SEPARATOR + "web" + WhereAreYou.NODE_SEPARATOR + mWebAppForm.getObjectName(), true);
            }

            // check Objectname
            String serverName = on.getKeyProperty("J2EEServer");
            if (!JonasManagementRepr.isRegistered(on, serverName)) {
                refreshServicesTree(pRequest);
                return (pMapping.findForward("ActionListWebContainers"));
            }
            
            // Populate
            if (bPopulate) {
                // Populate WebApp
                populateWebApp(mWebAppForm.getObjectName(), mWebAppForm);

                // Before, we used the JOnAS War MBean
                //      ObjectName on = findJonasMbeanWar(m_WebAppForm.getPathContext());
                // Use informations in the Catalina WebModule MBean
                if (on != null) {
                    // Populate War
                    if (mWebAppForm instanceof WebAppVirtualCatalinaForm) {
                        forward = "WebApp Virtual Catalina";
                        WebAppVirtualCatalinaForm oWebAppForm = (WebAppVirtualCatalinaForm) mWebAppForm;
                        pRequest.setAttribute("contexts", oWebAppForm.getContexts());
                        pRequest.setAttribute("policies", oWebAppForm.getPolicies());
                        pRequest.setAttribute("nbSessions", oWebAppForm.getNbSessions());
                        pRequest.setAttribute("objectNames", oWebAppForm.getObjectNames());
                        pRequest.setAttribute("objectName", oWebAppForm.getObjectName());
                    } else {
                        mWarForm = createWarForm(pMapping, pRequest);
                        populateWar(on, mWarForm, sServerName);
                    }
                }
            }
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(pRequest, m_Errors);
            return (pMapping.findForward("Global Error"));
        }

        // Forward to the jsp.
        return (pMapping.findForward(forward));
    }

}
