/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.webservice;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;
import org.ow2.jonas.webapp.jonasadmin.service.webservice.provider.JOnASProvider;
import org.ow2.jonas.webapp.jonasadmin.service.webservice.provider.element.HandlerItem;
import org.ow2.jonas.webapp.jonasadmin.service.webservice.provider.element.PortComponentItem;
import org.ow2.jonas.webapp.jonasadmin.service.webservice.provider.element.WebServiceDescriptionItem;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Vivek Lakshmanan
 */
public class HandlersDetailsAction extends JonasBaseAction {

    /**
     * Handler Details page.
     */
    private static final String HANDLERS_DETAILS_FORWARD = "Handlers Details";

    public ActionForward executeAction(ActionMapping p_Mapping,
                                       ActionForm p_Form,
                                       HttpServletRequest p_Request,
                                       HttpServletResponse p_Response)
                         throws IOException, ServletException {
        // Id for the portcomponent selected by the user.
        String wsPCId = p_Request.getParameter("pcselect");
        // Selected web service description
        String wsdId = p_Request.getParameter("wsdselect");
        // Handler chosen by the user.
        String handlerId = p_Request.getParameter("handlerselect");

        try {
            JOnASProvider jonasProvider = new JOnASProvider(m_WhereAreYou.getCurrentJonasServerName());
            WebServiceDescriptionItem wsd = jonasProvider.getWebServiceDescription(wsdId);
            PortComponentItem pc = null;
            int wsdPortComponentCount = wsd.getPortComponentCount();
            // Find the port component selected by the user.
            for (int i = 0; i < wsdPortComponentCount; i++) {
                PortComponentItem tmp = (PortComponentItem) wsd.getPortComponent(i);
                if (wsPCId.equals(tmp.getName())) {
                    pc = tmp;
                    break;
                }
            }
            HandlerItem handler = null;
            for (int i = 0; i < pc.getHandlerCount(); i++) {
                if (pc.getHandler(i).getName().equals(handlerId)) {
                    handler = pc.getHandler(i);
                }
            }
            // HashMap to hold parameters for forwards to
            // ActionPortComponentsDetails.
            HashMap forwardMap = new HashMap();
            forwardMap.put("pcselect", wsPCId);
            forwardMap.put("wsdselect", wsdId);
            p_Request.setAttribute("paramMap", forwardMap);

            // Create ArrayLists for lists to be displayed about soap
            // headers, soap roles and init-params.
            ArrayList soapHeaders = new ArrayList();
            for (int i = 0; i < handler.getSoapHeaderCount(); i++) {
                soapHeaders.add(handler.getSoapHeader(i).toString());
            }

            ArrayList soapRoles = new ArrayList();
            for (int i = 0; i < handler.getSoapRoleCount(); i++) {
                soapRoles.add(handler.getSoapRole(i));
            }

            ArrayList initParams = new ArrayList();
            for (int i = 0; i < handler.getInitParamCount(); i++) {
                initParams.add(handler.getInitParam(i));
            }

            p_Request.setAttribute("portComponent", pc);
            p_Request.setAttribute("webServiceDescription", wsd);
            p_Request.setAttribute("handler", handler);
            p_Request.setAttribute("soapHeaders", soapHeaders);
            p_Request.setAttribute("soapRoles", soapRoles);
            p_Request.setAttribute("initParams", initParams);
            // Force the node selected in tree
            m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_SERVER)
                    + WhereAreYou.NODE_SEPARATOR + "services"
                    + WhereAreYou.NODE_SEPARATOR + "WebService"
                    + WhereAreYou.NODE_SEPARATOR + wsd.getName()
                    + WhereAreYou.NODE_SEPARATOR + pc.getName()
                    + WhereAreYou.NODE_SEPARATOR + handler.getName(), true);

        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward(GLOBAL_ERROR_FORWARD));
        }

        // Forward to jsp.
        return (p_Mapping.findForward(HANDLERS_DETAILS_FORWARD));
    }

}
