/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.security;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.ow2.jonas.webapp.jonasadmin.Jlists;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

/**
 * @author Michel-Ange ANTON
 */
public class LdapRealmForm extends FactoryRealmForm {

// --------------------------------------------------------- Constants

// --------------------------------------------------------- Properties variables

    private String authenticationMode = null;
    private String baseDn = null;
    private String initialContextFactory = null;
    private String language = null;
    private String providerUrl = null;
    private String referral = null;
    private String roleDn = null;
    private String roleNameAttribute = null;
    private String roleSearchFilter = null;
    private String securityAuthentication = null;
    private String securityCredentials = null;
    private String securityPrincipal = null;
    private String securityProtocol = null;
    private String stateFactories = null;
    private String userDn = null;
    private String userPasswordAttribute = null;
    private String userRolesAttribute = null;
    private String userSearchFilter = null;
    private String algorithm = null;

    private List securityAuthenticationLdapValues = Jlists.getSecurityAuthenticationLdapValues();
    private List authenticationModeLdapValues = Jlists.getAuthenticationModeLdapValues();
    private List securityAlgorithms = Jlists.getSecurityAlgorithms();

// --------------------------------------------------------- Public Methods

    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        super.reset(mapping, request);

        authenticationMode = "bind";
        baseDn = null;
        initialContextFactory = "com.sun.jndi.ldap.LdapCtxFactory";
        language = null;
        providerUrl = "ldap://localhost:389";
        referral = null;
        roleDn = null;
        roleNameAttribute = "cn";
        roleSearchFilter = "uniqueMember={0}";
        securityAuthentication = "simple";
        securityCredentials = null;
        securityPrincipal = null;
        securityProtocol = null;
        stateFactories = null;
        userDn = null;
        userPasswordAttribute = "userPassword";
        userRolesAttribute = "memberOf";
        userSearchFilter = "uid={0}";
    }

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     * @return List of errors
     */
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors oErrors = new ActionErrors();
        if ((getName() == null) || (getName().length() == 0)) {
            oErrors.add("name", new ActionMessage("error.security.factory.ldap.realm.name.required"));
        }
        if ((baseDn == null) || (baseDn.length() == 0)) {
            oErrors.add("baseDn"
                , new ActionMessage("error.security.factory.ldap.realm.baseDn.required"));
        }
        if ((initialContextFactory == null) || (initialContextFactory.length() == 0)) {
            oErrors.add("initialContextFactory"
                , new ActionMessage("error.security.factory.ldap.realm.initialContextFactory.required"));
            initialContextFactory = "com.sun.jndi.ldap.LdapCtxFactory";
        }
        if ((providerUrl == null) || (providerUrl.length() == 0)) {
            oErrors.add("providerUrl"
                , new ActionMessage("error.security.factory.ldap.realm.providerUrl.required"));
            providerUrl = "ldap://localhost:389";
        }
        if ((roleNameAttribute == null) || (roleNameAttribute.length() == 0)) {
            oErrors.add("roleNameAttribute"
                , new ActionMessage("error.security.factory.ldap.realm.roleNameAttribute.required"));
            roleNameAttribute = "cn";
        }
        if ((roleSearchFilter == null) || (roleSearchFilter.length() == 0)) {
            oErrors.add("roleSearchFilter"
                , new ActionMessage("error.security.factory.ldap.realm.roleSearchFilter.required"));
            roleSearchFilter = "uniqueMember={0}";
        }
        if ((userPasswordAttribute == null) || (userPasswordAttribute.length() == 0)) {
            oErrors.add("userPasswordAttribute"
                , new ActionMessage("error.security.factory.ldap.realm.userPasswordAttribute.required"));
            userPasswordAttribute = "userPassword";
        }
        if ((userRolesAttribute == null) || (userRolesAttribute.length() == 0)) {
            oErrors.add("userRolesAttribute"
                , new ActionMessage("error.security.factory.ldap.realm.userRolesAttribute.required"));
            userRolesAttribute = "memberOf";
        }
        if ((userSearchFilter == null) || (userSearchFilter.length() == 0)) {
            oErrors.add("userSearchFilter"
                , new ActionMessage("error.security.factory.ldap.realm.userSearchFilter.required"));
            userSearchFilter = "uid={0}";
        }
        return oErrors;
    }

// --------------------------------------------------------- Properties Methods

    public String getAuthenticationMode() {
        return authenticationMode;
    }

    public void setAuthenticationMode(String authenticationMode) {
        this.authenticationMode = authenticationMode;
    }

    public String getBaseDn() {
        return baseDn;
    }

    public void setBaseDn(String baseDn) {
        this.baseDn = baseDn;
    }

    public String getInitialContextFactory() {
        return initialContextFactory;
    }

    public void setInitialContextFactory(String initialContextFactory) {
        this.initialContextFactory = initialContextFactory;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getProviderUrl() {
        return providerUrl;
    }

    public void setProviderUrl(String providerUrl) {
        this.providerUrl = providerUrl;
    }

    public String getReferral() {
        return referral;
    }

    public void setReferral(String referral) {
        this.referral = referral;
    }

    public String getRoleDn() {
        return roleDn;
    }

    public void setRoleDn(String roleDn) {
        this.roleDn = roleDn;
    }

    public String getRoleNameAttribute() {
        return roleNameAttribute;
    }

    public void setRoleNameAttribute(String roleNameAttribute) {
        this.roleNameAttribute = roleNameAttribute;
    }

    public String getRoleSearchFilter() {
        return roleSearchFilter;
    }

    public void setRoleSearchFilter(String roleSearchFilter) {
        this.roleSearchFilter = roleSearchFilter;
    }

    public String getSecurityAuthentication() {
        return securityAuthentication;
    }

    public void setSecurityAuthentication(String securityAuthentication) {
        this.securityAuthentication = securityAuthentication;
    }

    public String getSecurityCredentials() {
        return securityCredentials;
    }

    public void setSecurityCredentials(String securityCredentials) {
        this.securityCredentials = securityCredentials;
    }

    public String getSecurityPrincipal() {
        return securityPrincipal;
    }

    public void setSecurityPrincipal(String securityPrincipal) {
        this.securityPrincipal = securityPrincipal;
    }

    public String getSecurityProtocol() {
        return securityProtocol;
    }

    public void setSecurityProtocol(String securityProtocol) {
        this.securityProtocol = securityProtocol;
    }

    public String getStateFactories() {
        return stateFactories;
    }

    public void setStateFactories(String stateFactories) {
        this.stateFactories = stateFactories;
    }

    public String getUserDn() {
        return userDn;
    }

    public void setUserDn(String userDn) {
        this.userDn = userDn;
    }

    public String getUserPasswordAttribute() {
        return userPasswordAttribute;
    }

    public void setUserPasswordAttribute(String userPasswordAttribute) {
        this.userPasswordAttribute = userPasswordAttribute;
    }

    public String getUserRolesAttribute() {
        return userRolesAttribute;
    }

    public void setUserRolesAttribute(String userRolesAttribute) {
        this.userRolesAttribute = userRolesAttribute;
    }

    public String getUserSearchFilter() {
        return userSearchFilter;
    }

    public void setUserSearchFilter(String userSearchFilter) {
        this.userSearchFilter = userSearchFilter;
    }

    public List getSecurityAuthenticationLdapValues() {
        return securityAuthenticationLdapValues;
    }

    public List getAuthenticationModeLdapValues() {
        return authenticationModeLdapValues;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    public List getSecurityAlgorithms() {
        return securityAlgorithms;
    }

}