/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2003-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.container;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.webapp.jonasadmin.JonasAdminException;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

/**
 * @author Michel-Ange ANTON
 */
public class ApplyWebAppCatalinaAction extends BaseWebAppAction {

    private static String sDefaultForward = "ActionEditWebAppCatalina";

    // --------------------------------------------------------- Protected Variables

    // --------------------------------------------------------- Public Methods

    @Override
    public ActionForward executeAction(final ActionMapping p_Mapping, final ActionForm p_Form, final HttpServletRequest p_Request,
            final HttpServletResponse p_Response) throws IOException, ServletException {

        // Current server
        WhereAreYou oWhere = (WhereAreYou) p_Request.getSession().getAttribute(WhereAreYou.SESSION_NAME);
        String sServerName = oWhere.getCurrentJonasServerName();

        // Default forward
        ActionForward oForward = null;

        // Identify the requested action
        WebAppCatalinaForm oForm = (WebAppCatalinaForm) p_Form;

        // Populate
        try {
            // Perform a "Create WebApp" transaction (if requested)
            if ("edit".equals(oForm.getAction())) {
                oForward = populateMbean(oForm, p_Mapping.findForward(sDefaultForward), p_Mapping, p_Request);
            }
            // Reload the web app
            else if ("reload".equals(oForm.getAction())) {
                oForward = reloadWebApplication(oForm, p_Mapping, sServerName);
            }
            // Stop the web app
            else if ("stop".equals(oForm.getAction())) {
                oForward = stopWebApplication(oForm, p_Mapping, sServerName);
            }
            // Start the web app
            else if ("start".equals(oForm.getAction())) {
                oForward = startWebApplication(oForm, p_Mapping, sServerName);
            }
        } catch (JonasAdminException e) {
            // Build error
            m_Errors.add("webapp", new ActionMessage(e.getId()));
            saveErrors(p_Request, m_Errors);
            // Return to the current page
            oForward = new ActionForward(p_Mapping.getInput());
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            oForward = p_Mapping.findForward("Global Error");
        }

        // Next Forward
        return oForward;
    }

    /**
     * Populate the Mbean web application (Context, Manager, Loader).
     *
     * @param p_Form The current form
     * @param p_Forward The current forward
     * @param p_Mapping The current mapping
     * @param p_Request The current request
     * @return If 'save' is requested then return the new forward to save else
     *         return the current forward
     * @throws Exception
     */
    protected ActionForward populateMbean(final WebAppCatalinaForm p_Form, final ActionForward p_Forward, final ActionMapping p_Mapping,
            final HttpServletRequest p_Request) throws Exception {
        ActionForward oForward = p_Forward;
        // Context
        ObjectName on = ObjectName.getInstance(p_Form.getObjectName());
        // setBooleanAttribute(on, "available", p_Form.isAvailable());
        setBooleanAttribute(on, "cookies", p_Form.isCookies());
        setBooleanAttribute(on, "crossContext", p_Form.isCrossContext());
        setBooleanAttribute(on, "reloadable", p_Form.isReloadable());
        setBooleanAttribute(on, "swallowOutput", p_Form.isSwallowOutput());
        setBooleanAttribute(on, "override", p_Form.isOverride());
        // Save in configuration file
        if (p_Form.isSave()) {
            // ObjectName onServer = CatalinaObjectName.catalinaServer();
            // JonasManagementRepr.invoke(onServer, "store", null, null);
            p_Form.setSave(false);
            p_Request.setAttribute("forward", p_Forward.getName());
            oForward = p_Mapping.findForward("ActionEditServletServer");
        }
        return oForward;
    }

    /**
     * Reload the current web application.
     *
     * @param p_Form The current form
     * @param p_Mapping The current mapping
     * @return The forward to go to the next page
     * @throws Exception
     */
    protected ActionForward reloadWebApplication(final WebAppCatalinaForm p_Form, final ActionMapping p_Mapping, final String jonasServerName)
            throws Exception {
        try {
            ObjectName onContext = new ObjectName(p_Form.getObjectName());
            JonasManagementRepr.invoke(onContext, "reload", null, null, jonasServerName);
        } catch (Throwable t) {
            throw new JonasAdminException("error.webapp.reload");
        }
        // Forward
        ActionForward oForward = null;
        if (p_Form.getName().equals(m_WhereAreYou.getApplicationContextPath())) {
            oForward = p_Mapping.findForward("Main Index");
        } else {
            oForward = p_Mapping.findForward(sDefaultForward);
        }
        return oForward;
    }

    /**
     * Start the current web application.
     *
     * @param p_Form The current form
     * @param p_Mapping The current mapping
     * @return The forward to go to the next page
     * @throws Exception
     */
    protected ActionForward startWebApplication(final WebAppCatalinaForm p_Form, final ActionMapping p_Mapping, final String jonasServerName)
            throws Exception {
        try {
            ObjectName onContext = new ObjectName(p_Form.getObjectName());
            JonasManagementRepr.invoke(onContext, "start", null, null, jonasServerName);
        } catch (Throwable t) {
            throw new JonasAdminException("error.webapp.start");
        }
        return p_Mapping.findForward(sDefaultForward);
    }

    /**
     * Stop the current web application.
     *
     * @param p_Form The current form
     * @param p_Mapping The current mapping
     * @return The forward to go to the next page
     * @throws Exception
     */
    protected ActionForward stopWebApplication(final WebAppCatalinaForm p_Form, final ActionMapping p_Mapping, final String jonasServerName)
            throws Exception {
        try {
            ObjectName onContext = new ObjectName(p_Form.getObjectName());
            JonasManagementRepr.invoke(onContext, "stop", null, null, jonasServerName);
        } catch (Throwable t) {
            throw new JonasAdminException("error.webapp.stop");
        }
        return p_Mapping.findForward(sDefaultForward);
    }
}
