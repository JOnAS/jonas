/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.db;
import org.ow2.jonas.lib.management.extensions.base.NameItem;

/**
 * @author Michel-Ange ANTON
 */
public class DatasourceItem implements NameItem {

// --------------------------------------------------------- Properties Variables

    private String name = null;
    private boolean deployed = false;
    private String jndiName = null;
    private int jdbcConnectionOpened = 0;

// --------------------------------------------------------- Constructors

    public DatasourceItem() {
    }

    public DatasourceItem(String p_Name, boolean p_Deployed) {
        setName(p_Name);
        setDeployed(p_Deployed);
    }

    public DatasourceItem(String p_Name, String p_JndiName, int p_JdbcConnectionOpened
        , boolean p_Deployed) {
        setName(p_Name);
        setJndiName(p_JndiName);
        setJdbcConnectionOpened(p_JdbcConnectionOpened);
        setDeployed(p_Deployed);
    }

// --------------------------------------------------------- Properties Methods

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDeployed() {
        return deployed;
    }

    public void setDeployed(boolean deployed) {
        this.deployed = deployed;
    }

    public String getJndiName() {
        return jndiName;
    }

    public void setJndiName(String jndiName) {
        this.jndiName = jndiName;
    }

    public int getJdbcConnectionOpened() {
        return jdbcConnectionOpened;
    }

    public void setJdbcConnectionOpened(int jdbcConnectionOpened) {
        this.jdbcConnectionOpened = jdbcConnectionOpened;
    }
}