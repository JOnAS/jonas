/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.clusterd;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.lib.management.extensions.server.ServerItem;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

public class ServerConfigModifiedAction extends JonasBaseAction {

    @Override
    public ActionForward executeAction(final ActionMapping p_Mapping, final ActionForm p_Form, final HttpServletRequest p_Request,
            final HttpServletResponse p_Response) throws IOException, ServletException {
        // form for affecting a server to clusterd control
        ClusterdServerForm oForm = (ClusterdServerForm) p_Form;
        WhereAreYou oWhere = (WhereAreYou) p_Request.getSession().getAttribute(WhereAreYou.SESSION_NAME);
        try {
            ArrayList al = (ArrayList) m_Session.getAttribute("freeServersList");
            int i = 0;
            ServerItem srv = null;
            boolean stop = false;
            // let's find the serverItem to set its configuration
            while (i < al.size() && !stop) {
                srv = (ServerItem) al.get(i);
                stop = srv.getName().equals(oForm.getName());
                i++;
            }
            // we should always find the serverItem
            if (stop) {
                // set properties for clusterd affectation
                srv.setIsConfiguredForClusterd(true);
                srv.setDescription(oForm.getDescription());
                srv.setJavaHome(oForm.getJavaHome());
                srv.setJonasRoot(oForm.getJonasRoot());
                srv.setAutoBoot(oForm.getAutoBoot());
                srv.setXprem(oForm.getXprem());
                srv.setClusterDaemonName(oWhere.getCurrentClusterDaemonName());
                // This server is now configured for clusterd affectation.
                m_WhereAreYou.addSrvConfiguredForClusterd(srv);
            }
            // set freeServersList attribute for the forward destination
            p_Request.setAttribute("freeServersList", al);
            p_Request.setAttribute("CdName", oWhere.getCurrentClusterDaemonName());

        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        // forward destination displays current clusterd infos
        return (p_Mapping.findForward("ActionCreateJonasServer"));
    }

}
