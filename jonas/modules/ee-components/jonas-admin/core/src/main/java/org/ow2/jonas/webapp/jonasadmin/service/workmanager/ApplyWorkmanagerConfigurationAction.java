/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.webapp.jonasadmin.service.workmanager;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;

/**
 * Implement action for WM service reconfiguration.
 * @author Adriana Danes
 */
public class ApplyWorkmanagerConfigurationAction extends JonasBaseAction {

    @Override
    public ActionForward executeAction(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request,
            final HttpServletResponse response) throws IOException, ServletException {

        String domainName = m_WhereAreYou.getCurrentDomainName();
        String jonasServerName = m_WhereAreYou.getCurrentJonasServerName();

        ObjectName serviceOn = JonasObjectName.workManager(domainName);
        WorkmanagerServiceForm oForm = (WorkmanagerServiceForm) form;
        String sAction = oForm.getAction();
        if ("apply".equals(sAction)) {
            setIntegerAttribute(serviceOn, "minPoolSize", oForm.getMinPoolSize());
            setIntegerAttribute(serviceOn, "maxPoolSize", oForm.getMaxPoolSize());
        }

        if (oForm.getAction().equals("save")) {
            JonasManagementRepr.invoke(serviceOn, "saveConfig", null, null, jonasServerName);
        }

        // Forward to the jsp.
        return (mapping.findForward("Workmanager Service"));
    }

}
