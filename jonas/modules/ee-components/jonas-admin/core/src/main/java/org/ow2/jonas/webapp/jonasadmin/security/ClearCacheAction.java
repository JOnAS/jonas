/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.security;

import java.io.IOException;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;

import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ClearCacheAction extends JonasBaseAction {


 // --------------------------------------------------------- Public Methods

     public ActionForward executeAction(final ActionMapping p_Mapping, final ActionForm p_Form
         , final HttpServletRequest p_Request, final HttpServletResponse p_Response)
         throws IOException, ServletException {

         WhereAreYou oWhere = (WhereAreYou) p_Request.getSession().getAttribute(WhereAreYou.
                 SESSION_NAME);
         String serverName = oWhere.getCurrentJonasServerName();
         String domainName = oWhere.getCurrentDomainName();
         

         // Get the forward parameter
         String sForward = "ActionEditMemoryRealm";
         
         try {
            
         // Get realm parameter
         String realmname = p_Request.getParameter("realm");
         if (realmname != null) {
             ObjectName on = JonasObjectName.securityMemoryFactory(domainName, realmname);
             JonasManagementRepr.invoke(on, "clearCache", null, null, serverName);
         }

         
         } catch (Throwable t) {
             addGlobalError(t);
             saveErrors(p_Request, m_Errors);
             return (p_Mapping.findForward("Global Error"));
         }
         // Forward to the actino.
         return (p_Mapping.findForward(sForward));
     }

 // --------------------------------------------------------- Protected Methods

}
