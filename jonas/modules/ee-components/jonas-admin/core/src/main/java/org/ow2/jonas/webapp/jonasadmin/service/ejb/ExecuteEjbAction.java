/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2003-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.ejb;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Michel-Ange ANTON
 */

public class ExecuteEjbAction extends JonasBaseAction {

// --------------------------------------------------------- Protected variables

    protected String mAction = null;
    protected String mForward = null;

// --------------------------------------------------------- Public Methods

    /**
     * Execute a action.
     *
     * @param pMapping <code>ActionForward</code> instance
     * @param pForm <code>ActionForm</code> instance
     * @param pRequest <code>HttpServletRequest</code> instance
     * @param pResponse <code>HttpServletResponse</code> instance
     * @return <code>ActionForward</code> instance
     * @throws IOException
     * @throws ServletException
     */
    public ActionForward executeAction(ActionMapping pMapping, ActionForm pForm
        , HttpServletRequest pRequest, HttpServletResponse pResponse)
        throws IOException, ServletException {

        // Current server
        WhereAreYou oWhere = (WhereAreYou) pRequest.getSession().getAttribute(WhereAreYou.
                SESSION_NAME);
        String serverName = oWhere.getCurrentJonasServerName();

        // Get Http parameters
        mAction = pRequest.getParameter("action");
        // Form used
        EjbForm oForm = (EjbForm) m_Session.getAttribute("ejbForm");
        // Type Ejb used
        //m_Type = getIntegerType(oForm);

        try {
            // Execute a method of Ejb
            executeEjb(oForm, serverName);
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(pRequest, m_Errors);
            return (pMapping.findForward("Global Error"));
        }

        // Forward to the jsp or the action
        pRequest.setAttribute("select", oForm.getObjectName());
        return (pMapping.findForward(mForward));
    }

// --------------------------------------------------------- Protected Methods

    /**
     * Dispath executing on Ejb.
     *
     * @param pForm Current form
     * @throws Exception Could not execute the requested action
     */
    protected void executeEjb(EjbForm pForm, String serverName)
        throws Exception {
        // Object name used
        String sObjectName = pForm.getObjectName();
        ObjectName on = new ObjectName(sObjectName);
        String mType = pForm.getType();  // j2eeType
        if (mType.equals("EntityBean")) {
            // set forward String
            mForward = "ActionEditEjbEntity";
            EjbEntityForm eForm = (EjbEntityForm) pForm;
            String mPersistency = eForm.getPersistency();
            if (mPersistency.startsWith("Cont")) {
                if ("synchronize".equals(mAction)) {
                    JonasManagementRepr.invoke(on, "synchronize", null, null, serverName);
                } else if ("reduceCache".equals(mAction)) {
                    JonasManagementRepr.invoke(on, "reduceCache", null, null, serverName);
                }
            }
        } else if (mType.equals("StatelessSessionBean")) {
            mForward = "ActionEditEjbSbl";
            if ("reduceCache".equals(mAction)) {
                JonasManagementRepr.invoke(on, "reduceCache", null, null, serverName);
            }
        }
    }

}
