/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.joramplatform;


public class ItemDestination extends ItemBase implements java.io.Serializable {
    private String type = null;
    private boolean used = false;
    /** The destination has a corresponding MBean registered in the JMX server */
    private boolean registered = false;
    /**
     * The corresponding MBean is really corresponding to the destination as
     * the id is correct
     */
    private boolean manageable = false;
    /**
     * @return Returns the type.
     */
    public String getType() {
        return type;
    }
    /**
     * @param type The type to set.
     */
    public void setType(String type) {
        this.type = type;
    }
    /**
     * @return Returns the used.
     */
    public boolean isUsed() {
        return used;
    }
    /**
     * @param used The used to set.
     */
    public void setUsed(boolean used) {
        this.used = used;
    }
    /**
     * @return true if the destination has a corresponding MBean
     */
    public boolean isRegistered() {
        return registered;
    }
    /**
     *
     * @param registered is true if the destination has a corresponding MBean
     */
    public void setRegistered(boolean registered) {
        this.registered = registered;
    }
	public boolean isManageable() {
		return manageable;
	}
	public void setManageable(boolean manageable) {
		this.manageable = manageable;
	}
}