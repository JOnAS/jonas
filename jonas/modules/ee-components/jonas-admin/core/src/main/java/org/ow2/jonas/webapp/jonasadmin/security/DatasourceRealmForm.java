/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.security;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.ow2.jonas.webapp.jonasadmin.Jlists;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

/**
 * @author Michel-Ange ANTON
 */
public class DatasourceRealmForm extends FactoryRealmForm {

// --------------------------------------------------------- Constants

// --------------------------------------------------------- Properties variables

    private String dsName = null;
    private String roleTable = null;
    private String roleTableRolenameCol = null;
    private String roleTableUsernameCol = null;
    private String userTable = null;
    private String userTablePasswordCol = null;
    private String userTableUsernameCol = null;
    private String algorithm = null;

    private List securityAlgorithms = Jlists.getSecurityAlgorithms();

// --------------------------------------------------------- Public Methods

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     * @return List of errors
     */
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors oErrors = new ActionErrors();

        if ((getName() == null) || (getName().length() == 0)) {
            oErrors.add("name"
                , new ActionMessage("error.security.factory.datasource.realm.name.required"));
        }
        if ((dsName == null) || (dsName.length() == 0)) {
            oErrors.add("dsName"
                , new ActionMessage("error.security.factory.datasource.realm.dsName.required"));
        }
        if ((roleTable == null) || (roleTable.length() == 0)) {
            oErrors.add("roleTable"
                , new ActionMessage("error.security.factory.datasource.realm.roleTable.required"));
        }
        if ((roleTableRolenameCol == null) || (roleTableRolenameCol.length() == 0)) {
            oErrors.add("roleTableRolenameCol"
                , new ActionMessage("error.security.factory.datasource.realm.roleTableRolenameCol.required"));
        }
        if ((roleTableUsernameCol == null) || (roleTableUsernameCol.length() == 0)) {
            oErrors.add("roleTableUsernameCol"
                , new ActionMessage("error.security.factory.datasource.realm.roleTableUsernameCol.required"));
        }
        if ((userTable == null) || (userTable.length() == 0)) {
            oErrors.add("userTable"
                , new ActionMessage("error.security.factory.datasource.realm.userTable.required"));
        }
        if ((userTablePasswordCol == null) || (userTablePasswordCol.length() == 0)) {
            oErrors.add("userTablePasswordCol"
                , new ActionMessage("error.security.factory.datasource.realm.userTablePasswordCol.required"));
        }
        if ((userTableUsernameCol == null) || (userTableUsernameCol.length() == 0)) {
            oErrors.add("userTableUsernameCol"
                , new ActionMessage("error.security.factory.datasource.realm.userTableUsernameCol.required"));
        }

        return oErrors;
    }

// --------------------------------------------------------- Properties Methods

    public String getDsName() {
        return dsName;
    }

    public void setDsName(String dsName) {
        this.dsName = dsName;
    }

    public String getRoleTable() {
        return roleTable;
    }

    public void setRoleTable(String roleTable) {
        this.roleTable = roleTable;
    }

    public String getRoleTableRolenameCol() {
        return roleTableRolenameCol;
    }

    public void setRoleTableRolenameCol(String roleTableRolenameCol) {
        this.roleTableRolenameCol = roleTableRolenameCol;
    }

    public String getRoleTableUsernameCol() {
        return roleTableUsernameCol;
    }

    public void setRoleTableUsernameCol(String roleTableUsernameCol) {
        this.roleTableUsernameCol = roleTableUsernameCol;
    }

    public String getUserTable() {
        return userTable;
    }

    public void setUserTable(String userTable) {
        this.userTable = userTable;
    }

    public String getUserTablePasswordCol() {
        return userTablePasswordCol;
    }

    public void setUserTablePasswordCol(String userTablePasswordCol) {
        this.userTablePasswordCol = userTablePasswordCol;
    }

    public String getUserTableUsernameCol() {
        return userTableUsernameCol;
    }

    public void setUserTableUsernameCol(String userTableUsernameCol) {
        this.userTableUsernameCol = userTableUsernameCol;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    public List getSecurityAlgorithms() {
        return securityAlgorithms;
    }

}