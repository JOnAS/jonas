/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.container;

import java.io.IOException;
import java.util.ArrayList;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.management.extensions.base.mbean.CatalinaObjectName;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Michel-Ange ANTON
 */
public class ApplyWebsConfirmAction extends BaseWebAppAction {

    /**
     * Signature for the <code>removeConnector</code> operation.
     */
    private String removeContextTypes[] = {
        "java.lang.String", // Object name
    };

// --------------------------------------------------------- Public Methods

    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws IOException, ServletException {

        // Current server
        WhereAreYou oWhere = (WhereAreYou) p_Request.getSession().getAttribute(WhereAreYou.
                SESSION_NAME);
        String jonasServerName = oWhere.getCurrentJonasServerName();

        // Form used
        ArrayList alSelected = (ArrayList) m_Session.getAttribute("listWebContainersSelected");

        // Actions
        try {
            String[] values = new String[1];
            String operation = "removeContext";
            // Look up the Catalina MBeanFactory
            ObjectName onFactory = CatalinaObjectName.catalinaFactory();
            // Remove connectors
            for (int i = 0; i < alSelected.size(); i++) {
                WebAppItem o = (WebAppItem) alSelected.get(i);
                values[0] = o.getObjectName();
                JonasManagementRepr.invoke(onFactory, operation, values, removeContextTypes, jonasServerName);
            }
            // refresh tree
            refreshTree(p_Request);
            //System.out.println("TODO -> refreshTree");


        }
        catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }

        // Forward to the connector display page or the list if create
        return p_Mapping.findForward("ActionListWebContainers");
    }
}
