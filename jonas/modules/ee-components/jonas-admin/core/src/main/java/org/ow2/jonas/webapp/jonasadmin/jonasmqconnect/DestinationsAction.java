package org.ow2.jonas.webapp.jonasadmin.jonasmqconnect;

import java.io.IOException;
import java.util.ArrayList;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;
import org.ow2.jonas.webapp.jonasadmin.jonasmqconnect.util.ItemDestination;
import org.ow2.jonas.webapp.jonasadmin.jonasmqconnect.util.LogUtils;
import org.ow2.jonas.webapp.jonasadmin.jonasmqconnect.util.MqObjectNames;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class DestinationsAction extends JonasBaseAction {

    public ActionForward executeAction(ActionMapping mapping, ActionForm form
            , HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException {

        DestinationsForm fBean = (DestinationsForm) form;
        WhereAreYou oWhere = (WhereAreYou) request.getSession().getAttribute(WhereAreYou.
                SESSION_NAME);
        String serverName = oWhere.getCurrentJonasServerName();
        String domainName = oWhere.getCurrentDomainName();

        /*
         * The "connector" session attribute should be set
         */
        String connector = (String) m_Session.getAttribute("mqconnector");
        //domain*jmq1*jonasmqconnect*jonasmqconnector*fwaMQJca
        // Force the node selection in tree
        String nodeName = "domain" + WhereAreYou.NODE_SEPARATOR
                            + serverName + WhereAreYou.NODE_SEPARATOR
                            + "jonasmqconnect" + WhereAreYou.NODE_SEPARATOR
                            + "jonasmqconnector" + WhereAreYou.NODE_SEPARATOR
                            + connector;
        m_WhereAreYou.selectNameNode(nodeName, true);

        try {
            //ObjectName mbName = ObjectNames.getConnectorON();
            ObjectName mbName = MqObjectNames.getConnectorONByName(domainName, connector);
            Object[] params = {};
            String[] signature = {};
            String[] onDestinations = (String[]) JonasManagementRepr.invoke(
                    mbName, "listDestinationObjectNames", params, signature, serverName);
            ArrayList destinationsArray = new ArrayList();
            for (int i = 0; i < onDestinations.length; i++) {
                ObjectName onDest = new ObjectName(onDestinations[i]);
                params = new Object[]{};
                signature = new String[]{};
                boolean isTopic = ((Boolean) JonasManagementRepr.getAttribute(
                        onDest, "IsTopic", serverName)).booleanValue();

                String baseName = "";

                signature = new String[]{"java.lang.String"};
                if (isTopic) {
                    baseName = (String) JonasManagementRepr.invoke(onDest,
                        "getProperty", new Object[]{"BaseTopicName"}, signature, serverName);
                } else {
                    baseName = (String) JonasManagementRepr.invoke(onDest,
                        "getProperty", new Object[]{"BaseQueueName"}, signature, serverName);
                }
                String name = onDest.getKeyProperty("name");
                ItemDestination item = new ItemDestination(name,
                        (isTopic) ? "topic" : "queue", baseName, onDest);
                destinationsArray.add(item);
            }

            m_Session.setAttribute("mqdestinations", destinationsArray);
        } catch (Exception ex) {
            LogUtils.print(ex.getMessage());
        }
        return mapping.findForward("JonasMqConnectDestinations");
    }
}