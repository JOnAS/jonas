/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.jtm;

import java.io.IOException;
import java.util.ArrayList;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.management.javaee.J2eeObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Tony Ortiz
 */
public class ResetTransactionsAction extends JonasBaseAction {
    /**
     * Principal method
     * @param pMapping action mapping
     * @param pForm used form
     * @param pRequest request parameters
     * @param pResponse response parameters
     * @return action forward
     * @throws IOException action failed because a IOException
     * @throws ServletException a ServletException occured
     */
    public ActionForward executeAction(ActionMapping pMapping, ActionForm pForm
        , HttpServletRequest pRequest, HttpServletResponse pResponse)
        throws IOException, ServletException {

        try {
            // Get container list
            String mys;
            String sDate;
            String sTransaction;
            String sResource;
            String sState;
            int sXidcount = 0;
            Object txObject;
            String [] myTxInfo;
            int    txInfoSize;
            ArrayList al = new ArrayList();

            // Object name used
            String currentDomainName = m_WhereAreYou.getCurrentDomainName();
            String currentJonasServerName = m_WhereAreYou.getCurrentJonasServerName();
            String jtaResourceName = "JTAResource";
            ObjectName jtaResourceObjectName = J2eeObjectName.JTAResource(currentDomainName, currentJonasServerName, jtaResourceName);

            txObject = JonasManagementRepr.invoke(jtaResourceObjectName, "getAllActiveTx", null, null, currentJonasServerName);
            myTxInfo = (String []) txObject;

            if (txObject != null) {
                txInfoSize = myTxInfo.length;

                for (int i = 0; i < txInfoSize; i++) {
                    mys = myTxInfo[i];
                    int myix1 = mys.indexOf("????");
                    sDate = mys.substring(0, myix1);
                    int myix2 = mys.indexOf("????", myix1 + 4);
                    sTransaction = mys.substring(myix1 + 4, myix2);
                    int myix3 = mys.indexOf("????", myix2 + 4);
                    sResource = mys.substring(myix2 + 4, myix3);
                    sState = mys.substring(myix3 + 4);
                    al.add(new TxItem (sDate, sTransaction, sResource, sState));
                }
             }

             // Set list in the request
             pRequest.setAttribute("listTransactionsEntries", al);
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(pRequest, m_Errors);
            return (pMapping.findForward("Global Error"));
        }

        // Forward to action
        return (pMapping.findForward("Jtm Monitor"));
    }

}
