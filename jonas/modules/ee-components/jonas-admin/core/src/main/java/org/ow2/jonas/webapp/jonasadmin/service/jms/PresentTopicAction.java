/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.jms;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.management.extensions.container.ejb.EjbItem;
import org.ow2.jonas.lib.management.extensions.container.ejb.EjbItemByNameComparator;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * Action presenting a given JMS Topic destination's attributes.
 * @author Adriana Danes
 */
public class PresentTopicAction extends JonasBaseAction {
    /**
     */
    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws IOException, ServletException {

        // Selected Topic
        String topicName = p_Request.getParameter("name");

        // Form used
        TopicForm oForm = null;
        if (topicName != null) {
            // Build a new form
            oForm = new TopicForm();
            oForm.reset(p_Mapping, p_Request);
            m_Session.setAttribute("topicForm", oForm);
            oForm.setName(topicName);
        }
        else {
            // Used last form in session
            oForm = (TopicForm) m_Session.getAttribute("topicForm");
        }

        // Force the node selected in tree
        m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_SERVER) + WhereAreYou.NODE_SEPARATOR
            + "services" + WhereAreYou.NODE_SEPARATOR + "jms" + WhereAreYou.NODE_SEPARATOR
            + "topic" + WhereAreYou.NODE_SEPARATOR + oForm.getName(), true);

        // Current server
        WhereAreYou oWhere = (WhereAreYou) p_Request.getSession().getAttribute(WhereAreYou.
                SESSION_NAME);
        String jonasServerName = oWhere.getCurrentJonasServerName();
        String domainName = m_WhereAreYou.getCurrentDomainName();

        // Populuate
        try {
            if (topicName != null) {
                ArrayList al = new ArrayList();
                String[] asParam = new String[1];
                String[] asSignature = new String[1];
                asSignature[0] = "java.lang.String";
                asParam[0] = topicName;
                ObjectName ejbServiceMB = JonasObjectName.ejbService(domainName);
                if (JonasManagementRepr.isRegistered(ejbServiceMB, jonasServerName)) {
                    java.util.Iterator it = ((java.util.Set) JonasManagementRepr.invoke(
                        ejbServiceMB, "getJmsDestinationDependence", asParam
                        , asSignature, jonasServerName)).iterator();
                    while (it.hasNext()) {
                        al.add(new EjbItem((ObjectName) it.next(), jonasServerName));
                    }
                    // Sort by name
                    Collections.sort(al, new EjbItemByNameComparator());
                }
                // Set list in form
                oForm.setListUsedByEjb(al);

                // Set monitoting info
                ObjectName jmsServiceMB = JonasObjectName.jmsService(domainName);
                int subscriptions = ((Integer) JonasManagementRepr.invoke(jmsServiceMB
                    , "getSubscriptions", asParam, asSignature, jonasServerName)).intValue();
                oForm.setSubscriptions(subscriptions);
            }
        }
        catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }

        // Forward to the jsp.
        return (p_Mapping.findForward("Topic"));
    }
}
