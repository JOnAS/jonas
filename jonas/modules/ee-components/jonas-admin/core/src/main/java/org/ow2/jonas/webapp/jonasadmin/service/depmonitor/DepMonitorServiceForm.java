package org.ow2.jonas.webapp.jonasadmin.service.depmonitor;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts.action.ActionForm;

public class DepMonitorServiceForm extends ActionForm {

    private boolean developmentMode;
    private String directories = null;
    private String action = null;
    private int monitorInterval = 0;
    
    public int getMonitorInterval() {
        return monitorInterval;
    }

    public void setMonitorInterval(int monitorInterval) {
        this.monitorInterval = monitorInterval;
    }

    /**
     * Represent boolean (true, false) values for enableLookups etc.
     */
    private List booleanVals = new ArrayList();

    public DepMonitorServiceForm() {
    }

    public boolean isDevelopmentMode() {
        return developmentMode;
    }

    public void setDevelopmentMode(final boolean developmentMode) {
        this.developmentMode = developmentMode;
    }

    public List getBooleanVals() {
        return booleanVals;
    }

    public void setBooleanVals(final List booleanVals) {
        this.booleanVals = booleanVals;
    }

    public String getAction() {
        return action;
    }

    public void setAction(final String action) {
        this.action = action;
    }

    public String getDirectories() {
        return directories;
    }

    public void setDirectories(final String directories) {
        this.directories = directories;
    }

}
