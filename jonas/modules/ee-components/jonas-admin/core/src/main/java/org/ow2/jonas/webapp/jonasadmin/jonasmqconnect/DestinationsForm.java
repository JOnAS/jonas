package org.ow2.jonas.webapp.jonasadmin.jonasmqconnect;

import org.apache.struts.action.ActionForm;

public class DestinationsForm extends ActionForm {
    /**
     * debug or not
     */
    private boolean debug = false;

    /**
     * printe debug msg
     * @param s msg
     */
    private void debug(String s) {
        if (debug)
            System.out.println(s);
    }

    /**
     * Selected destinations
     */
    private String[] selectedDestinations;

    /**
     * Selected destinations
     * @return the selected destinations
     */
    public String[] getSelectedDestinations() {
        debug("DestinationsForm#getSelectedDestinations()");
        return selectedDestinations;
    }

    /**
     * selected destinations
     * @param selectedDestinations the selected destinations to set
     */
    public void setSelectedDestinations(String[] selectedDestinations) {
        debug("DestinationsForm#setSelectedDestinations("
                + selectedDestinations
                + ")");
        this.selectedDestinations = selectedDestinations;
    }
}
