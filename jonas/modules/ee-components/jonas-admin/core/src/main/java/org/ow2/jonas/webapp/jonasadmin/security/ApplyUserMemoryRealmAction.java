/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Michel-Ange ANTON
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.security;

import java.io.IOException;
import java.util.ArrayList;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.Jlists;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 *
 */

public class ApplyUserMemoryRealmAction extends BaseMemoryRealmAction {

// --------------------------------------------------------- Public Methods

    /**
     */
    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws IOException, ServletException {

        WhereAreYou oWhere = (WhereAreYou) p_Request.getSession().getAttribute(WhereAreYou.
                SESSION_NAME);
        String serverName = oWhere.getCurrentJonasServerName();
        String domainName = oWhere.getCurrentDomainName();

        // Realm Form used
        MemoryRealmForm oRealmForm = getForm(p_Mapping, p_Request);

        // Form used
        UserMemoryRealmForm oForm = (UserMemoryRealmForm) p_Form;

        oForm.setListRolesUsed(Jlists.getArrayList(oForm.getRolesUsed(), Jlists.SEPARATOR));
        oForm.setListRolesNotused(Jlists.getArrayList(oForm.getRolesNotused(), Jlists.SEPARATOR));
        oForm.setListGroupsUsed(Jlists.getArrayList(oForm.getGroupsUsed(), Jlists.SEPARATOR));
        oForm.setListGroupsNotused(Jlists.getArrayList(oForm.getGroupsNotused(), Jlists.SEPARATOR));

        ArrayList alAddRoles = new ArrayList(oForm.getListRolesUsed());
        alAddRoles.removeAll(oForm.getListRolesUser());
        ArrayList alRemoveRoles = new ArrayList(oForm.getListRolesNotused());
        alRemoveRoles.retainAll(oForm.getListRolesUser());
        ArrayList alAddGroups = new ArrayList(oForm.getListGroupsUsed());
        alAddGroups.removeAll(oForm.getListGroupsUser());
        ArrayList alRemoveGroups = new ArrayList(oForm.getListGroupsNotused());
        alRemoveGroups.retainAll(oForm.getListGroupsUser());

        // Populate MBean
        try {
            ObjectName onUser = null;
            // Create a new user
            if (oForm.getAction().equals("create")) {
                String sEncrypted = encryptPassword(domainName, oForm.getPassword(), "MD5", serverName);
                ObjectName onRealm = JonasObjectName.securityMemoryFactory(domainName, oRealmForm.getResource());
                String[] asParam = {
                    oForm.getUser(), sEncrypted};
                String[] asSignature = {
                    "java.lang.String", "java.lang.String"};
                JonasManagementRepr.invoke(onRealm, "addUser", asParam, asSignature, serverName);
                // Search created user
                onUser = JonasObjectName.user(domainName, oRealmForm.getResource(), oForm.getUser());
            }
            else {
                // Modify existing user
                onUser = JonasObjectName.user(domainName, oRealmForm.getResource(), oForm.getUser());
                // Password
                if (oForm.getPassword().length() > 0) {
                    // Encrypt and set password
                    setStringAttribute(onUser, "Password", encryptPassword(domainName, oForm.getPassword()
                        , "MD5", serverName));
                }
            }
            // Roles
            if (alAddRoles.size() > 0) {
                for (int i = 0; i < alAddRoles.size(); i++) {
                    String[] asParam = {
                        alAddRoles.get(i).toString()};
                    String[] asSignature = {
                        "java.lang.String"};
                    JonasManagementRepr.invoke(onUser, "addRole", asParam, asSignature, serverName);
                }
            }
            if (alRemoveRoles.size() > 0) {
                for (int i = 0; i < alRemoveRoles.size(); i++) {
                    String[] asParam = {
                        alRemoveRoles.get(i).toString()};
                    String[] asSignature = {
                        "java.lang.String"};
                    JonasManagementRepr.invoke(onUser, "removeRole", asParam, asSignature, serverName);
                }
            }
            // Groups
            if (alAddGroups.size() > 0) {
                for (int i = 0; i < alAddGroups.size(); i++) {
                    String[] asParam = {
                        alAddGroups.get(i).toString()};
                    String[] asSignature = {
                        "java.lang.String"};
                    JonasManagementRepr.invoke(onUser, "addGroup", asParam, asSignature, serverName);
                }
            }
            if (alRemoveGroups.size() > 0) {
                for (int i = 0; i < alRemoveGroups.size(); i++) {
                    String[] asParam = {
                        alRemoveGroups.get(i).toString()};
                    String[] asSignature = {
                        "java.lang.String"};
                    JonasManagementRepr.invoke(onUser, "removeGroup", asParam, asSignature, serverName);
                }
            }

        }
        catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        // Forward to the jsp.
        return (p_Mapping.findForward("ActionEditMemoryRealmUsers"));
    }

// --------------------------------------------------------- Protected Methods

}
