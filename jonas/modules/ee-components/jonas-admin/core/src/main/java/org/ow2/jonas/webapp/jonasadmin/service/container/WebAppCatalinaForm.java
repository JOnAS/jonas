/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2003-2004 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.container;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

/**
 * @author Michel-Ange ANTON
 */
public class WebAppCatalinaForm extends WebAppForm {

// --------------------------------------------------------- Properties variables

    private String host = null;
    private boolean cookies = false;
    private boolean reloadable = false;
    private boolean swallowOutput = false;
    private boolean crossContext = false;
    private boolean override = false;
    private String docBase = null;
    private int state = 0;

// --------------------------------------------------------- Public Methods

    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        super.reset(mapping, request);

        cookies = true;
        reloadable = false;
        swallowOutput = false;
        crossContext = false;
        override = false;
    }

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     * @return List of errors
     */
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors oErrors = super.validate(mapping, request);
        //numberCheck(oErrors, "sessionTimeout", sessionTimeout, true, 1, 65535);
        return oErrors;
    }

    /*
     * Helper method to check that it is a required number and
     * is a valid integer within the given range. (min, max).
     *
     * @param  field  The field name in the form for which this error occured.
     * @param  numText  The string representation of the number.
     * @param rangeCheck  Boolean value set to true of reange check should be performed.
     *
     * @param  min  The lower limit of the range
     * @param  max  The upper limit of the range
     *
     */
    public void numberCheck(ActionErrors p_Errors, String field, String numText, boolean rangeCheck
        , int min, int max) {
        // Check for 'is required'
        if ((numText == null) || (numText.length() < 1)) {
            p_Errors.add(field, new ActionMessage("error.webapp.setting." + field + ".required"));
        } else {

            // check for 'must be a number' in the 'valid range'
            try {
                int num = Integer.parseInt(numText);
                // perform range check only if required
                if (rangeCheck) {
                    if ((num < min) || (num > max)) {
                        p_Errors.add(field
                            , new ActionMessage("error.webapp.setting." + field + ".range"));
                    }
                }
            } catch (NumberFormatException e) {
                p_Errors.add(field, new ActionMessage("error.webapp.setting." + field + ".format"));
            }
        }
    }

// --------------------------------------------------------- Properties Methods
/*
    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }
*/
    public boolean isCookies() {
        return cookies;
    }

    public void setCookies(boolean cookies) {
        this.cookies = cookies;
    }

    public boolean isReloadable() {
        return reloadable;
    }

    public void setReloadable(boolean reloadable) {
        this.reloadable = reloadable;
    }

    public boolean isSwallowOutput() {
        return swallowOutput;
    }

    public void setSwallowOutput(boolean swallowOutput) {
        this.swallowOutput = swallowOutput;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public boolean isCrossContext() {
        return crossContext;
    }

    public void setCrossContext(boolean crossContext) {
        this.crossContext = crossContext;
    }

    public String getDocBase() {
        return docBase;
    }

    public void setDocBase(String docBase) {
        this.docBase = docBase;
    }

    public boolean isOverride() {
        return override;
    }

    public void setOverride(boolean override) {
        this.override = override;
    }
    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }


}