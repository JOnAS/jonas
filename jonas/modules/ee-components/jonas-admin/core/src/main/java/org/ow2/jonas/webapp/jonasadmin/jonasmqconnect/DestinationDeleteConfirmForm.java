package org.ow2.jonas.webapp.jonasadmin.jonasmqconnect;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class DestinationDeleteConfirmForm  extends ActionForm {
    /**
     * debug or not
     */
    private boolean debug = false;

    /**
     * printe debug msg
     * @param s msg
     */
    private void debug(String s) {
        if (debug)
            System.out.println(s);
    }

    // ------------------------------------------------------- Public Methods

    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {
//    	selectedDestinations = null;
    }

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     * @return List of errors
     */
    public ActionErrors validate(ActionMapping mapping,
            HttpServletRequest request) {
        ActionErrors oErrors = new ActionErrors();
//        if ((selectedDestinations == null) ||
//        		(selectedDestinations.length == 0)) {
//            oErrors.add("selectedDestinations",
//            	new ActionMessage(
//        "error.joansmqconnect.resource.jms.create.selecteddestinations.empty"));
//        }
//        if ((deletePhysicalDestination == null) ||
//        		(selectedDestinations.length == 0)) {
//            oErrors.add("deletePhysicalDestination",
//            	new ActionMessage(
//            			"error.joansmqconnect.resource.jms.create." +
//            	"deletephysicaldestination.empty"));
//        }
        return oErrors;
    }

    // ----------------------------------------------------- Properties Methods

    /**
     * Selected destinations in string
     */
    private String destinationsStr;

    /**
     * remove Physical Destination ?
     */
    private String deletePhysicalDestination = null;

    /**
     * @return the removePhysicalDestination
     */
    public String getDeletePhysicalDestination() {
        debug("DestinationDeleteConfirmForm#getDeletePhysicalDestination()");
        return deletePhysicalDestination;
    }

    /**
     * @param deletePhysicalDestination the removePhysicalDestination to set
     */
    public void setDeletePhysicalDestination(String deletePhysicalDestination) {
        debug("DestinationDeleteConfirmForm#setDeletePhysicalDestination("
                + deletePhysicalDestination
                + ")");
        this.deletePhysicalDestination = deletePhysicalDestination;
    }

    /**
     * get DestinationsStr
     * @return DestinationsStr
     */
    public String getDestinationsStr() {
        debug("DestinationDeleteConfirmForm#getDestinationsStr()");
        return destinationsStr;
    }

    /**
     * set DestinationsStr
     * @param destinationsStr
     */
    public void setDestinationsStr(String destinationsStr) {
        debug("DestinationDeleteConfirmForm#setDestinationsStr("
                + destinationsStr
                + ")");
        this.destinationsStr = destinationsStr;
    }
}
