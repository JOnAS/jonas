/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.taglib;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;



/**
 * @author Michel-Ange ANTON
 */
public class TitleContentTag extends WhereAreYouTag {

// ----------------------------------------------------- Constants

// ----------------------------------------------------- Instance Variables

    protected String m_Body = null;

// ------------------------------------------------------------- Properties
    private String image = null;
    private String title = null;
    private boolean usingParent = false;
    private boolean tomThumb = false;
    private boolean tomThumbIcons = false;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isUsingParent() {
        return usingParent;
    }

    public void setUsingParent(boolean usingParent) {
        this.usingParent = usingParent;
    }

    public boolean isTomThumb() {
        return tomThumb;
    }

    public void setTomThumb(boolean tomThumb) {
        this.tomThumb = tomThumb;
    }

    public boolean isTomThumbIcons() {
        return tomThumbIcons;
    }

    public void setTomThumbIcons(boolean tomThumbIcons) {
        this.tomThumbIcons = tomThumbIcons;
    }

// --------------------------------------------------------- Public Methods

    /**
     * Render this instant actions control.
     *
     * @return Constant EVAL_PAGE
     * @exception JspException if a processing error occurs
     */
    public int doEndTag()
        throws JspException {
        JspWriter out = pageContext.getOut();
        try {
            render(out);
        }
        catch (IOException e) {
            throw new JspException(e);
        }
        return (EVAL_PAGE);
    }

    public int doAfterBody()
        throws JspException {
        String sBody = bodyContent.getString();
        if (sBody != null) {
            sBody = sBody.trim();
            if (sBody.length() > 0) {
                this.m_Body = sBody;
            }
        }
        return (SKIP_BODY);
    }

    /**
     * Release all state information set by this tag.
     */
    public void release() {
        this.image = null;
        this.title = null;
        m_Body = null;
    }

// -------------------------------------------------------- Protected Methods

    protected void render(JspWriter out)
        throws IOException, JspException {
        String sImageDisplay = null;
        String sTitleDisplay = null;
        ArrayList alTomThumb = null;
        //String sThumbDisplay = null;

        // Prepare WhereAreYou infos
        if (isUsingWhere()) {
            TreeControlNode oNode = getSelectedTreeControlNode();
            if (oNode != null) {
                sImageDisplay = getImagesRoot() + "/" + oNode.getIcon();
                sTitleDisplay = oNode.getLabel();
                // Prepare label of parent node
                if (isUsingParent() == true) {
                    TreeControlNode oParent = oNode.getParent();
                    if (oParent != null) {
                        sTitleDisplay = oParent.getLabel() + " : " + oNode.getLabel();
                    }
                }
                // Prepare Tom Thumb list
                if (isTomThumb() == true) {
                    alTomThumb = getTomThumbList(oNode);
                }
            }
        }
        // Prepare parameters
        if (image != null) {
            sImageDisplay = image;
        }
        if (title != null) {
            sTitleDisplay = title;
        }
        if (m_Body != null) {
            sTitleDisplay = m_Body;
            m_Body = null;
        }
        // Display
        if ((sImageDisplay != null) || (sTitleDisplay != null)) {
            String sTomThumb = null;
            // Build Tom Thumb display
            if (alTomThumb != null) {
                if (alTomThumb.size() > 0) {
                    sTomThumb = renderTomThumb(alTomThumb);
                }
            }
            // Display Tom Thumb
            if (sTomThumb != null) {
                out.println(
                    "<table height=\"50\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">");
                out.print("<tr valign=\"top\">");
                out.print("<td class=\"contentTitleTomThumb\" colspan=\"3\">");
                out.print(sTomThumb);
                out.print("</td>");
                out.println("</tr>");
            }
            else {
                // No Tom Thumb to display
                out.println(
                    "<table height=\"30\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">");
            }
            // Display icon
            out.println("<tr valign=\"top\">");
            if (sImageDisplay != null) {
                out.print("<td width=\"1%\" valign=\"top\" class=\"contentTitleImage\"><img src=\"");
                out.print(sImageDisplay);
                out.println("\" border=\"0\"></td>");
                out.println("<td width=\"1%\" valign=\"top\" class=\"contentTitleImage\">&nbsp;</td>");
            }
            // Display title
            if (sTitleDisplay != null) {
                out.print("<td class=\"contentTitle\">");
                out.print(sTitleDisplay);
                out.println("</td>");
            }
            out.println("</tr>");
            out.println("</table>");
        }
    }

    protected String renderTomThumb(ArrayList p_TomThumb) {
        StringBuffer sb = new StringBuffer();
        ItemTomThumb oThumb;
        for (int i = p_TomThumb.size() - 1; i >= 0; i--) {
            oThumb = (ItemTomThumb) p_TomThumb.get(i);
            if (oThumb.getLink() != null) {
                sb.append("<a href=\"");
                sb.append(oThumb.getLink());
                sb.append("\" class=\"contentTitleTomThumb\">");
                if ((isTomThumbIcons()== true) && (oThumb.getIcon() != null)) {
                    sb.append("<img src=\"");
                    sb.append(getImagesRoot());
                    sb.append("/");
                    sb.append(oThumb.getIcon());
                    sb.append("\" border=\"0\"> ");
                }
                sb.append(oThumb.getLabel());
                sb.append("</a>");
            }
            else {
                if ((isTomThumbIcons()== true) && (oThumb.getIcon() != null)) {
                    sb.append("<img src=\"");
                    sb.append(oThumb.getIcon());
                    sb.append("\" border=\"0\"> ");
                }
                sb.append("<span class=\"contentTitleTomThumbNotLink\">");
                sb.append(oThumb.getLabel());
                sb.append("</span>");
            }
            if (i > 0) {
                sb.append(" > ");
            }
        }
        return sb.toString();
    }

    protected ArrayList getTomThumbList(TreeControlNode p_Node) {
        TreeControlNode oParent;
        ItemTomThumb oThumb;

        TreeControlNode oNode = p_Node;
        ArrayList alTomThumb = new ArrayList();
        // Loop on each parent
        do {
            // Get parent
            oParent = oNode.getParent();
            // Verify parent exist
            if (oParent != null) {
                // Verify parent is not empty or root node
                if (("ROOT-NODE".equalsIgnoreCase(oParent.getName()) == false) && (oParent.getLabel() != null)) {
                    // Get Thumb infos
                    oThumb = new ItemTomThumb();
                    oThumb.setLabel(oParent.getLabel());
                    oThumb.setIcon(oParent.getIcon());
                    if (oParent.getAction() != null) {
                        oThumb.setLink(((HttpServletResponse) pageContext.getResponse()).encodeURL(((
                            HttpServletRequest) pageContext.getRequest()).getContextPath() + "/"
                            + oParent.getAction()));
                    }
                    // Add to thumb's list
                    alTomThumb.add(oThumb);
                }
            }
            // Next parent
            oNode = oParent;
        }
        while (oParent != null);

        return alTomThumb;
    }

    class ItemTomThumb {
        private String label = null;
        private String link = null;
        private String icon = null;

        public ItemTomThumb() {

        }

        public ItemTomThumb(String p_Label, String p_Link, String p_Icon) {
            setLabel(p_Label);
            setLink(p_Link);
            setIcon(p_Icon);
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }

    }
}
