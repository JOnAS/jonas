/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.xml.xs.hardcoded;

import java.util.List;

import org.ow2.jonas.webapp.jonasadmin.xml.xs.ElementRestrictions;


/**
 * Represents schema restrictions for a single element. The restrictions are
 * hard coded.
 *
 * @author Gregory Lapouchnian
 * @author Patrick Smith
 */
/**
 * @author pasmith
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class HCElementRestrictions implements ElementRestrictions {

    /** The name of the element. */
    private String name;

    /** is this element complex */
    private boolean isComplex = false;

    /** is this element a sequence */
    private boolean isSequence = false;

    /** The children of this element. */
    private List children;

    /** The attributes of this element.*/
    private List attributes;

    /**
     * Makes a new Hard coded element restriction.
     * @param name the name of the element.
     * @param complex is this element complex.
     * @param children the children of this element.
     * @param attrs the attributes of this element.
     */
    public HCElementRestrictions(String name, boolean complex, List children,
            List attrs) {
        this.name = name;
        this.isComplex = complex;
        this.children = children;
        this.attributes = attrs;
    }

    /**
     * Is this element a simple type with no attributes.
     * @return true if this element is simple with no attributes, false otherwise.
     */
    public boolean isSimpleAndNoAttributes() {
        return (children == null && attributes == null);
    }

    /**
     * Returns a list of children for this element.
     * @return a list of children for this element.
     */
    public List getChildren() {
        return children;
    }

    /**
     * Set the list of children for this element.
     * @param children the list of children for this element.
     */
    public void setChildren(List children) {
        if (isComplex()) {
            this.children = children;
        }
    }

    /**
     * Is this element complex.
     * @return true if this element is complex.
     */
    public boolean isComplex() {
        return isComplex;
    }

    /**
     * Sets if this element is complex or not.
     * @param isComplex if this element is complex or not.
     */
    public void setComplex(boolean isComplex) {
        this.isComplex = isComplex;
    }

    /**
     * Returns the name of this element
     * @return the name of this element.
     */
    public String getName() {
        return name;
    }

    /**
     * sets the name of this element.
     * @param name the name of this element.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns the list of attributes for this element.
     * @return the list of attributes for this element.
     */
    public List getAttributes() {
        return attributes;
    }

    /**
     * Sets the list of elements for this element.
     * @param attributes the list of attributs to set for this element.
     */
    public void setAttributes(List attributes) {
        this.attributes = attributes;
    }

    /**
     * Returns the min occurances for this element.
     * @return the min occurances for this element.
     */
    public int getMinOccurs() {
        return 0;
    }

    /**
     * Sets the min occurances for this element.
     * @param min the min occurances for this element.
     */
    public void setMinOccurs(int min) {
        // TODO Auto-generated method stub
    }

    /**
     * Returns the max occurances for this element.
     * @return the max occurances for this element.
     */
    public int getMaxOccurs() {
        return 0;
    }

    /**
     * Sets the max occurances for this element.
     * @param min the max occurances for this element.
     */
    public void setMaxOccurs(int min) {
        // TODO Auto-generated method stub
    }

    /**
     * Returns if this element is a sequence or not.
     * @return true if this element is a sequence.
     */
    public boolean isSequence() {
        return isSequence;
    }

    /**
     * Sets if this element is a sequence.
     * @param isSequence the value to set if this element is a sequence.
     */
    public void setSequence(boolean isSequence) {
        this.isSequence = isSequence;
    }
}
