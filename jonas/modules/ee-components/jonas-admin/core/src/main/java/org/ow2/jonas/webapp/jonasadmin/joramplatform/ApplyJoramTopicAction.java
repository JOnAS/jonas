/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 */

package org.ow2.jonas.webapp.jonasadmin.joramplatform;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Adriana Danes
 */

public class ApplyJoramTopicAction extends EditJoramBaseAction {

// --------------------------------------------------------- Public Methods

    public ActionForward executeAction(ActionMapping pMapping, ActionForm pForm
        , HttpServletRequest pRequest, HttpServletResponse pResponse)
        throws IOException, ServletException {

        try {
            // Form used
            JoramTopicForm oForm = (JoramTopicForm) pForm;
            String serverName = m_WhereAreYou.getCurrentJonasServerName();
            updateReadablesWriteables(oForm, serverName);
            populateDestination(oForm.getOName(), oForm, serverName);
        } catch (Throwable t) {
            return (treatError(t, pMapping, pRequest));
        }


        return (pMapping.findForward("JoramTopic"));
    }

}
