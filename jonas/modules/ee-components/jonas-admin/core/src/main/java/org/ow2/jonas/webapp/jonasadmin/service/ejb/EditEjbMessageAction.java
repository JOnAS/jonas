/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Michel-Ange ANTON
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.ejb;

import javax.management.ObjectName;

public class EditEjbMessageAction extends EditEjbAction {

// --------------------------------------------------------- Protected Methods

    /**
     * Get a <code>ObjectName</code> instance for the Ejb given
     * with the parameters of the HTPP request.
     *
     * @return The ObjectName instance
     * @throws Exception
     */
    /**
     * Return a <code>EjbMessageForm</code> instance associate to the EJB.
     *
     * @return A form instance
     */
    protected EjbForm getEjbForm() {
        return new EjbMessageForm();
    }

    /**
     * Fill all infos of EJB in the <code>EjbMessageForm</code> instance.
     *
     * @param p_Form Instance to fill
     * @param p_ObjectName Instance to get infos
     * @throws Exception
     */
    protected void fillEjbInfo(boolean ejb3, EjbForm p_Form, ObjectName p_ObjectName, String serverName)
        throws Exception {
        fillEjbGlobalInfo(ejb3, p_Form, p_ObjectName);

        EjbMessageForm oForm = (EjbMessageForm) p_Form;

        oForm.setMdbJMSAssociateDestinationName(p_ObjectName.getKeyProperty("name"));
        if (oForm.getMdbJMSAssociateDestinationName() == null) {
            oForm.setMdbJMSAssociateDestinationName(getStringAttribute(p_ObjectName,
                "mdbJMSAssociateDestinationName"));
        }
    }

    /**
     * The global forward to go.
     *
     * @return Forward
     */
    protected String getEjbForward() {
        return "Ejb Message";
    }

}
