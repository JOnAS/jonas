package org.ow2.jonas.webapp.jonasadmin.monitoring;

import java.io.IOException;
import java.util.Iterator;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasAdminJmx;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


public class JkMemberAction extends JonasBaseAction {

    public ActionForward executeAction(ActionMapping p_Mapping,
            ActionForm p_Form, HttpServletRequest p_Request,
            HttpServletResponse p_Response) throws IOException,
            ServletException {
        String domainName = m_WhereAreYou.getCurrentDomainName();
        // Get member name from the 'member' parameter and cluster name from the 'clust' parameter
        String name = p_Request.getParameter("member");
        if (name == null) {
            addGlobalError(new Exception("JkMemberAction: member parameter is null."));
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        String cluster = p_Request.getParameter("clust");
        if (cluster == null) {
            addGlobalError(new Exception("JkClusterAction: clust parameter is null."));
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        // Form used
        JkMemberForm oForm = (JkMemberForm) p_Form;
        oForm.setName(name);
        // cluster type
        String type = "JkCluster";
        try {
            ObjectName on = JonasObjectName.clusterMember(domainName, name, type, cluster);
            oForm.setHost(getStringAttribute(on, "Host"));
            oForm.setState(getStringAttribute(on, "State"));
            oForm.setLbfactor(getIntegerAttribute(on, "Lbfactor"));
            oForm.setPort(getIntegerAttribute(on, "Port"));
            oForm.setType(getStringAttribute(on, "Type"));
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }

        // Forward to the jsp.
        return (p_Mapping.findForward("JkMember"));
    }
}
