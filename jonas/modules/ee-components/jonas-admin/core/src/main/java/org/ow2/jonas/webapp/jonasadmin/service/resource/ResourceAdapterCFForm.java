/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.resource;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Properties;

import javax.management.ObjectName;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.webapp.jonasadmin.Jlists;
import org.ow2.jonas.webapp.jonasadmin.common.LabelValueByLabelComparator;
import org.ow2.jonas.webapp.taglib.LabelValueBean;


/**
 * @author Michel-Ange ANTON
 */
public class ResourceAdapterCFForm extends ActionForm {

// --------------------------------------------------------- Properties variables

    private String action = null;
    private String description = null;
    private String name = null;
    private ArrayList listProperties = new ArrayList();
    private ObjectName oName = null;
    private String path = null;

    private String jdbcTestStatement = "1";
    private java.util.List checkingLevels = Jlists.getJdbcConnectionCheckingLevels();
    private String currentOpened = null;
    private String currentBusy = null;
    private String busyMaxRecent = null;
    private String busyMinRecent = null;
    private String currentInTx = null;
    private String openedCount = null;
    private String connectionFailures = null;
    private String connectionLeaks = null;
    private String currentWaiters = null;
    private String waitersHigh = null;
    private String waitersHighRecent = null;
    private String waiterCount = null;
    private String waitingTime = null;
    private String waitingHigh = null;
    private String waitingHighRecent = null;
    private String servedOpen = null;
    private String rejectedOpen = null;
    private String rejectedFull = null;
    private String rejectedTimeout = null;
    private String rejectedOther = null;
    private String jdbcConnCheckLevel = "0";
    private String connMaxAge = "0";
    private String maxOpentime = "0";
    private String initSize = "0";
    private String maxSize = "0";
    private String minSize = "0";
    private String maxWaitTime = "0";
    private String maxWaiters = "0";
    private String samplingPeriod = "0";
    private String pstmtMax = "0";
    private String pstmtCachePolicy = "List";

    private boolean jdbcConnSetUp = false;

    public boolean isJdbcConnSetUp() {
        return jdbcConnSetUp;
    }

    public void setJdbcConnSetUp(final boolean jdbcConnSetUp) {
        this.jdbcConnSetUp = jdbcConnSetUp;
    }

    private ArrayList listUsedByEjb = new ArrayList();

// --------------------------------------------------------- Public Methods

    @Override
    public void reset(final ActionMapping mapping, final HttpServletRequest request) {
    }

    @Override
    public ActionErrors validate(final ActionMapping mapping, final HttpServletRequest request) {
        ActionErrors oErrors = new ActionErrors();
        return oErrors;
    }

// --------------------------------------------------------- Properties Methods

    public String getAction() {
        return action;
    }

    public void setAction(final String action) {
        this.action = action;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String desc) {
        description = desc;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(final String path) {
        this.path = path;
    }

    public ArrayList getListProperties() {
        return listProperties;
    }

    public void setListProperties(final Properties properties) {
        this.listProperties.clear();
        // Fill list with properties (keys and values)
        String sKey;
        Enumeration oEnum = properties.keys();
        while (oEnum.hasMoreElements()) {
            sKey = oEnum.nextElement().toString();
            this.listProperties.add(new LabelValueBean(sKey, properties.getProperty(sKey, "")));
        }
        Collections.sort(this.listProperties, new LabelValueByLabelComparator());
    }

    public void setOName(final ObjectName oName) {
        this.oName = oName;
    }

    public ObjectName getOName() {
        return oName;
    }

    // Statistics
    public String getCurrentOpened() {
        return currentOpened;
    }

    public void setCurrentOpened(final String s) {
        currentOpened = s;
    }

    public String getCurrentBusy() {
        return currentBusy;
    }

    public void setCurrentBusy(final String s) {
        currentBusy = s;
    }

    public String getCurrentInTx() {
        return currentInTx;
    }

    public void setCurrentInTx(final String s) {
        currentInTx = s;
    }

    public String getOpenedCount() {
        return openedCount;
    }

    public void setOpenedCount(final String s) {
        openedCount = s;
    }

    public String getConnectionFailures() {
        return connectionFailures;
    }

    public void setConnectionFailures(final String s) {
        connectionFailures = s;
    }

    public String getConnectionLeaks() {
        return connectionLeaks;
    }

    public void setConnectionLeaks(final String s) {
        connectionLeaks = s;
    }

    public String getCurrentWaiters() {
        return currentWaiters;
    }

    public void setCurrentWaiters(final String s) {
        currentWaiters = s;
    }

    public String getWaitersHigh() {
        return waitersHigh;
    }

    public void setWaitersHigh(final String s) {
        waitersHigh = s;
    }

    public String getWaitersHighRecent() {
        return waitersHighRecent;
    }

    public void setWaitersHighRecent(final String s) {
        waitersHighRecent = s;
    }

    public String getBusyMaxRecent() {
        return busyMaxRecent;
    }

    public void setBusyMaxRecent(final String s) {
        busyMaxRecent = s;
    }

    public String getBusyMinRecent() {
        return busyMinRecent;
    }

    public void setBusyMinRecent(final String s) {
        busyMinRecent = s;
    }

    public String getWaiterCount() {
        return waiterCount;
    }

    public void setWaiterCount(final String s) {
        waiterCount = s;
    }

    public String getWaitingTime() {
        return waitingTime;
    }

    public void setWaitingTime(final String s) {
        waitingTime = s;
    }

    public String getWaitingHigh() {
        return waitingHigh;
    }

    public void setWaitingHigh(final String s) {
        waitingHigh = s;
    }

    public String getWaitingHighRecent() {
        return waitingHighRecent;
    }

    public void setWaitingHighRecent(final String s) {
        waitingHighRecent = s;
    }

    public String getServedOpen() {
        return servedOpen;
    }

    public void setServedOpen(final String s) {
        servedOpen = s;
    }

    public String getRejectedOpen() {
        return rejectedOpen;
    }

    public void setRejectedOpen(final String s) {
        rejectedOpen = s;
    }

    public String getRejectedFull() {
        return rejectedFull;
    }

    public void setRejectedFull(final String s) {
        rejectedFull = s;
    }

    public String getRejectedTimeout() {
        return rejectedTimeout;
    }

    public void setRejectedTimeout(final String s) {
        rejectedTimeout = s;
    }

    public String getRejectedOther() {
        return rejectedOther;
    }

    public void setRejectedOther(final String s) {
        rejectedOther = s;
    }

    public String getJdbcConnCheckLevel() {
        return jdbcConnCheckLevel;
    }

    public void setJdbcConnCheckLevel(final String jdbcConnCheckLevel) {
        this.jdbcConnCheckLevel = jdbcConnCheckLevel;
    }

    public String getConnMaxAge() {
        return connMaxAge;
    }

    public void setConnMaxAge(final String s) {
        connMaxAge = s;
    }

    public String getMaxOpentime() {
        return maxOpentime;
    }

    public void setMaxOpentime(final String s) {
        maxOpentime = s;
    }

    public String getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(final String maxSize) {
        this.maxSize = maxSize;
    }

    public String getMinSize() {
        return minSize;
    }

    public void setMinSize(final String minSize) {
        this.minSize = minSize;
    }

    public String getMaxWaitTime() {
        return maxWaitTime;
    }

    public void setMaxWaitTime(final String maxWaitTime) {
        this.maxWaitTime = maxWaitTime;
    }

    public String getMaxWaiters() {
        return maxWaiters;
    }

    public void setMaxWaiters(final String s) {
        this.maxWaiters = s;
    }

    public String getSamplingPeriod() {
        return samplingPeriod;
    }

    public void setSamplingPeriod(final String s) {
        this.samplingPeriod = s;
    }

    public String getJdbcTestStatement() {
        return jdbcTestStatement;
    }

    public void setJdbcTestStatement(final String jdbcTestStatement) {
        this.jdbcTestStatement = jdbcTestStatement;
    }

    public java.util.List getCheckingLevels() {
        return checkingLevels;
    }

    public ArrayList getListUsedByEjb() {
        return listUsedByEjb;
    }

    public void setListUsedByEjb(final ArrayList listUsedByEjb) {
        this.listUsedByEjb = listUsedByEjb;
    }

    /**
     * @return Returns the initSize.
     */
    public String getInitSize() {
        return initSize;
    }

    /**
     * @param initSize The initSize to set.
     */
    public void setInitSize(final String initSize) {
        this.initSize = initSize;
    }

    /**
     * @return Returns the pstmtMax.
     */
    public String getPstmtMax() {
        return pstmtMax;
    }

    /**
     * @param pstmtMax The pstmtMax to set.
     */
    public void setPstmtMax(final String pstmtMax) {
        this.pstmtMax = pstmtMax;
    }

    /**
     * @return Returns the pstmtCachePolicy.
     */
    public String getPstmtCachePolicy() {
        return pstmtCachePolicy;
    }

    /**
     * @param pstmtMax The pstmtMax to set.
     */
    public void setPstmtCachePolicy(final String pstmtCachePolicy) {
        this.pstmtCachePolicy = pstmtCachePolicy;
    }


}
