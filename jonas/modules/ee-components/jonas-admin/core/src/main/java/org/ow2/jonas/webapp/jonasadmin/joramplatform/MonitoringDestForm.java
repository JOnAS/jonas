/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 */

package org.ow2.jonas.webapp.jonasadmin.joramplatform;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * Form bean for the Joram queue and topic pages
 * @author Adriana Danes
 */

public class MonitoringDestForm extends ActionForm {

// ------------------------------------------------------------- Properties Variables
    /**
     *
     */
    private long nbMsgsReceiveSinceCreation;
    private long nbMsgsSendToDMQSinceCreation;
    private long nbMsgsDeliverSinceCreation;
    //private Date creationDate;
    private String name = null;
    private String type = null;
//  ------------------------------------------------------------- Properties Methods

    /**
     * @return Returns the nbMsgsReceiveSinceCreation.
     */
    public long getNbMsgsReceiveSinceCreation() {
        return nbMsgsReceiveSinceCreation;
    }

    /**
     * @param nbMsgsReceiveSinceCreation The nbMsgsReceiveSinceCreation to set.
     */
    public void setNbMsgsReceiveSinceCreation(long nbMsgsReceiveSinceCreation) {
        this.nbMsgsReceiveSinceCreation = nbMsgsReceiveSinceCreation;
    }

    /**
     * @return Returns the nbMsgsSendToDMQSinceCreation.
     */
    public long getNbMsgsSendToDMQSinceCreation() {
        return nbMsgsSendToDMQSinceCreation;
    }

    /**
     * @param nbMsgsSendToDMQSinceCreation The nbMsgsSendToDMQSinceCreation to set.
     */
    public void setNbMsgsSendToDMQSinceCreation(long nbMsgsSendToDMQSinceCreation) {
        this.nbMsgsSendToDMQSinceCreation = nbMsgsSendToDMQSinceCreation;
    }

    /**
     * @return Returns the nbMsgsDeliverSinceCreation.
     */
    public long getNbMsgsDeliverSinceCreation() {
        return nbMsgsDeliverSinceCreation;
    }

    /**
     * @param nbMsgsDeliverSinceCreation The nbMsgsDeliverSinceCreation to set.
     */
    public void setNbMsgsDeliverSinceCreation(long nbMsgsDeliverSinceCreation) {
        this.nbMsgsDeliverSinceCreation = nbMsgsDeliverSinceCreation;
    }

    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name to set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return Returns the type.
     */
    public String getType() {
        return type;
    }

    /**
     * @param type The type to set.
     */
    public void setType(String type) {
        this.type = type;
    }
// ------------------------------------------------------------- Public Methods

    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        nbMsgsReceiveSinceCreation = 0;
        nbMsgsSendToDMQSinceCreation = 0;
        nbMsgsDeliverSinceCreation = 0;
        name = null;
        type = null;
    }

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        return new ActionErrors();
    }

}
