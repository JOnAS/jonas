/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.jonasserver;

import java.io.IOException;

import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Adriana DANES
 */

public class EditJvmAction extends JonasBaseAction {

// --------------------------------------------------------- Public Methods

    /**
     * Execute action for JVM management
     * @param pMapping mapping info
     * @param pForm form object
     * @param pRequest HTTP request
     * @param pResponse HTTP response
     *
     * @return An <code>ActionForward</code> instance or <code>null</code>
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */
    public ActionForward executeAction(ActionMapping pMapping, ActionForm pForm
        , HttpServletRequest pRequest, HttpServletResponse pResponse)
        throws IOException, ServletException {
        // Force the node selected in tree
        m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_SERVER), true);

        ActionMessages oErrors = new ActionMessages();

        // The MBean of current Jonas server
        ObjectName onServer = m_WhereAreYou.getCurrentJonasServer();
        String serverName = m_WhereAreYou.getCurrentJonasServerName();
        String domainName = m_WhereAreYou.getCurrentDomainName();
        // JVM MBean
        ObjectName oObjectName = null;
        // List of JVMs - in reality we have only one
        String[] jvms = (String []) JonasManagementRepr.getAttribute(onServer, "javaVMs", serverName);
        try {
            if (jvms.length > 0) {
                oObjectName = new ObjectName(jvms[0]);
            } else {
                oErrors.add("JVMs", new ActionMessage("error.server.jonas.jvms"));
                saveErrors(pRequest, oErrors);
                return (pMapping.findForward("Global Error"));
            }
        } catch (MalformedObjectNameException e) {
            addGlobalError(e);
            saveErrors(pRequest, m_Errors);
            return (pMapping.findForward("Global Error"));
        }

        // Form used
        JvmForm oForm = (JvmForm) pForm;
        try {
            // Copy scalar properties
            oForm.setJavaVendor(getStringAttribute(oObjectName, "javaVendor"));
            oForm.setJavaVersion(getStringAttribute(oObjectName, "javaVersion"));
            oForm.setNode(getStringAttribute(oObjectName, "node"));
            if (JonasManagementRepr.isRegistered(JonasObjectName.webContainerService(domainName), serverName)) {
                oForm.setPresentServletContainer(true);
            } else {
                oForm.setPresentServletContainer(false);
            }
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(pRequest, m_Errors);
            return (pMapping.findForward("Global Error"));
        }

        // Forward to the jsp.
        return (pMapping.findForward("Jvm"));
    }

}
