package org.ow2.jonas.webapp.jonasadmin.monitoring;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.lib.management.domain.DomainMonitor;
import org.ow2.jonas.lib.management.domain.J2EEDomain;
import org.ow2.jonas.lib.management.domain.proxy.server.ServerProxy;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;

/**
 * This action display all inormations on JOnAS server.
 * @author eyindanga.
 *
 */
public class DisplayNodeInfoAction extends JonasBaseAction {

    /* (non-Javadoc).
     * @see org.ow2.jonas.webapp.jonasadmin.JonasBaseAction#executeAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public ActionForward executeAction(final ActionMapping p_Mapping,
            final ActionForm p_Form, final HttpServletRequest p_Request,
            final HttpServletResponse p_Response) throws IOException,
            ServletException {
        // Form used
        String serverName = p_Request.getParameter("node");
        if (serverName == null) {
            serverName = m_WhereAreYou.getCurrentJonasServerName();
        }
        NodeInfoForm oForm = (NodeInfoForm) p_Form;
        try {

            /*
             * Server related info
             */
            //  oForm.setServerName(p_Request.getParameter("node"));
            oForm.setServerName(serverName);
            DomainMonitor dm = J2EEDomain.getInstance().getDomainMonitor();
            ServerProxy srvProxy = dm.findServerProxy(serverName);
            try {
                oForm.setState(srvProxy.getState());
            } catch (RuntimeException e) {
                // Attibute missing
                oForm.setState("Unavailable");
            }
            try {
                oForm.setHostName(srvProxy.getHostName());
            } catch (RuntimeException e) {
                // Attibute missing
                oForm.setHostName("Unavailable");
            }
            try {
                oForm.setAllThreadsCount(srvProxy.getAllThreadsCount());
            } catch (RuntimeException e) {
                // Attibute missing
                oForm.setAllThreadsCount(-1);
            }
            try {
                oForm.setJavaVersion(srvProxy.getJavaVersion());
            } catch (RuntimeException e) {
                // Attibute missing
                oForm.setJavaVersion("Unavailable");
            }
            try {
                oForm.setJavaVendor(srvProxy.getJavaVendor());
            } catch (RuntimeException e) {
                // Attibute missing
                oForm.setJavaVendor("Unavailable");
            }
            try {
                oForm.setCurrentTotalMemory(srvProxy.getCurrentTotalMemory().intValue());
            } catch (RuntimeException e) {
                oForm.setCurrentTotalMemory(-1);
            }
            try {
                oForm.setCurrentUsedMemory(srvProxy.getCurrentUsedMemory().intValue());
            } catch (RuntimeException e) {
                // Attibute missing
                 oForm.setCurrentUsedMemory(-1);
            }
            try {
                oForm.setJOnASVersion(srvProxy.getJOnASVersion());
            } catch (RuntimeException e) {
                // Attibute missing
                oForm.setJOnASVersion(null);
            }
            try {
                oForm.setProtocols(srvProxy.getProtocols());
            } catch (RuntimeException e) {
                // Attibute missing
                oForm.setProtocols(null);
            }
            try {
                oForm.setConnectionUrl(srvProxy.getConnectionUrl());
            } catch (RuntimeException e) {
                // Attibute missing
                oForm.setConnectionUrl(null);
            }
            /*
             * JDK5
             */

            /*
             * Tomcat
             */
            try {
                oForm.setTomcat(srvProxy.getTomcat());
                if (oForm.getTomcat()) {
                    oForm.setMaxThreadsByConnectorTomcat(srvProxy.getMaxThreadsByConnectorTomcat());
                    oForm.setCurrentThreadCountByConnectorTomcat(srvProxy.getCurrentThreadCountByConnectorTomcat());
                    oForm.setCurrentThreadBusyByConnectorTomcat(srvProxy.getCurrentThreadBusyByConnectorTomcat());
                    oForm.setBytesReceivedByConnectorTomcat(srvProxy.getBytesReceivedByConnectorTomcat());
                    oForm.setBytesSentByConnectorTomcat(srvProxy.getBytesSentByConnectorTomcat());
                    oForm.setErrorCountByConnectorTomcat(srvProxy.getErrorCountByConnectorTomcat());
                    oForm.setProcessingTimeByConnectorTomcat(srvProxy.getProcessingTimeByConnectorTomcat());
                    oForm.setRequestCountByConnectorTomcat(srvProxy.getRequestCountByConnectorTomcat());
                }
            } catch (RuntimeException e) {
                // Attibute missing
                 oForm.setMaxThreadsByConnectorTomcat(-1);
                 oForm.setCurrentThreadCountByConnectorTomcat(-1);
                 oForm.setCurrentThreadBusyByConnectorTomcat(-1);
                 oForm.setBytesReceivedByConnectorTomcat(-1);
                 oForm.setBytesSentByConnectorTomcat(-1);
                 oForm.setErrorCountByConnectorTomcat(-1);
                 oForm.setProcessingTimeByConnectorTomcat(-1);
                 oForm.setRequestCountByConnectorTomcat(-1);
            }
            /*
             * Tx
             */
            try {
                oForm.setTransaction(srvProxy.getTransaction());
                if (oForm.getTransaction()) {
                    oForm.setTotalBegunTransactions(srvProxy.getTotalBegunTransactions());
                    oForm.setTotalCommittedTransactions(srvProxy.getTotalCommittedTransactions());
                    oForm.setTotalCurrentTransactions(srvProxy.getTotalCurrentTransactions());
                    oForm.setTotalExpiredTransactions(srvProxy.getTotalExpiredTransactions());
                    oForm.setTotalRolledbackTransactions(srvProxy.getTotalRolledbackTransactions());
                }
            } catch (RuntimeException e) {
                // Attibute missing
                oForm.setTotalBegunTransactions(-1);
                oForm.setTotalCommittedTransactions(-1);
                oForm.setTotalCurrentTransactions(-1);
                oForm.setTotalExpiredTransactions(-1);
                oForm.setTotalRolledbackTransactions(-1);

            }
            /*
             * Worker Pool
             */
            try {
                oForm.setWorkers(srvProxy.getWorkers());
                if (oForm.getWorkers()) {
                    oForm.setCurrentWorkerPoolSize(srvProxy.getCurrentWorkerPoolSize());
                    oForm.setMaxWorkerPoolSize(srvProxy.getMaxWorkerPoolSize());
                    oForm.setMinWorkerPoolSize(srvProxy.getMinWorkerPoolSize());
                }
            } catch (RuntimeException e) {
                // Attibute missing
                oForm.setCurrentWorkerPoolSize(-1);
                oForm.setMaxWorkerPoolSize(-1);
                oForm.setMinWorkerPoolSize(-1);
            }
            /*
             * JCA
             */
            try {
                oForm.setJcaConnection(srvProxy.getJcaConnection());
                if (oForm.getJcaConnection()) {
                    oForm.setConnectionFailuresJCAConnection(srvProxy.getConnectionFailuresJCAConnection());
                    oForm.setConnectionLeaksJCAConnection(srvProxy.getConnectionLeaksJCAConnection());
                    oForm.setCurrentBusyJCAConnection(srvProxy.getCurrentBusyJCAConnection());
                    oForm.setCurrentOpenedJCAConnection(srvProxy.getCurrentOpenedJCAConnection());
                    oForm.setRejectedOpenJCAConnection(srvProxy.getRejectedOpenJCAConnection());
                    oForm.setServedOpenJCAConnection(srvProxy.getServedOpenJCAConnection());
                    oForm.setWaiterCountJCAConnection(srvProxy.getWaiterCountJCAConnection());
                    oForm.setWaitingTimeJCAConnection(srvProxy.getWaitingTimeJCAConnection());
                }
            } catch (Exception e) {
                // Attibute missing
                oForm.setConnectionFailuresJCAConnection(-1);
                oForm.setConnectionLeaksJCAConnection(-1);
                oForm.setCurrentBusyJCAConnection(-1);
                oForm.setCurrentOpenedJCAConnection(-1);
                oForm.setRejectedOpenJCAConnection(-1);
                oForm.setServedOpenJCAConnection(-1);
                oForm.setWaiterCountJCAConnection(-1);
                oForm.setWaitingTimeJCAConnection(-1);
            }
            /*
             * JDBC
             */
            try {
                oForm.setJdbcDatasource(srvProxy.getJdbcDatasource());
                if (oForm.getJdbcDatasource()) {
                    oForm.setConnectionFailuresJDBCDatasource(srvProxy.getConnectionFailuresJDBCResource());
                    oForm.setConnectionLeaksJDBCDatasource(srvProxy.getConnectionLeaksJDBCResource());
                    oForm.setCurrentBusyJDBCDatasource(srvProxy.getCurrentBusyJDBCResource());
                    oForm.setCurrentOpenedJDBCDatasource(srvProxy.getCurrentOpenedJDBCResource());
                    oForm.setRejectedOpenJDBCDatasource(srvProxy.getRejectedOpenJDBCResource());
                    oForm.setServedOpenJDBCDatasource(srvProxy.getServedOpenJDBCResource());
                    oForm.setWaiterCountJDBCDatasource(srvProxy.getWaiterCountJDBCResource());
                    oForm.setWaitingTimeJDBCDatasource(srvProxy.getWaitingTimeJDBCResource());
                }
            } catch (RuntimeException e) {
                // Attibute missing
                oForm.setConnectionFailuresJDBCDatasource(-1);
                oForm.setConnectionLeaksJDBCDatasource(-1);
                oForm.setCurrentBusyJDBCDatasource(-1);
                oForm.setCurrentOpenedJDBCDatasource(-1);
                oForm.setRejectedOpenJDBCDatasource(-1);
                oForm.setServedOpenJDBCDatasource(-1);
                oForm.setWaiterCountJDBCDatasource(-1);
                oForm.setWaitingTimeJDBCDatasource(-1);
            }
            /*
             * JMS
             */
            try {
                oForm.setJmsJoram(srvProxy.getJmsJoram());
                if (oForm.getJmsJoram()) {
                    oForm.setJmsQueuesNbMsgsDeliverSinceCreation(srvProxy.getJmsQueuesNbMsgsDeliverSinceCreation());
                    oForm.setJmsQueuesNbMsgsReceiveSinceCreation(srvProxy.getJmsQueuesNbMsgsReceiveSinceCreation());
                    oForm.setJmsQueuesNbMsgsSendToDMQSinceCreation(srvProxy.getJmsQueuesNbMsgsSendToDMQSinceCreation());

                    oForm.setJmsTopicsNbMsgsDeliverSinceCreation(srvProxy.getJmsTopicsNbMsgsDeliverSinceCreation());
                    oForm.setJmsTopicsNbMsgsReceiveSinceCreation(srvProxy.getJmsTopicsNbMsgsReceiveSinceCreation());
                    oForm.setJmsTopicsNbMsgsSendToDMQSinceCreation(srvProxy.getJmsTopicsNbMsgsSendToDMQSinceCreation());
                    oForm.setJmsNbMsgsSendToDMQSinceCreation(oForm.getJmsQueuesNbMsgsSendToDMQSinceCreation()+ oForm.getJmsTopicsNbMsgsSendToDMQSinceCreation());
                }
            } catch (RuntimeException e) {
                // Attibute missing
                 oForm.setJmsQueuesNbMsgsDeliverSinceCreation(-1);
                 oForm.setJmsQueuesNbMsgsReceiveSinceCreation(-1);
                 oForm.setJmsQueuesNbMsgsSendToDMQSinceCreation(-1);

                 oForm.setJmsTopicsNbMsgsDeliverSinceCreation(-1);
                 oForm.setJmsTopicsNbMsgsReceiveSinceCreation(-1);
                 oForm.setJmsTopicsNbMsgsSendToDMQSinceCreation(-1);
                 oForm.setJmsNbMsgsSendToDMQSinceCreation(-1);
            }
            /*
             * EJB
             */
            try {
                oForm.setCurrentNumberOfEJB(srvProxy.getCurrentNumberOfEJB());
                oForm.setCurrentNumberOfEntity(srvProxy.getCurrentNumberOfEntityBean());
                oForm.setCurrentNumberOfMDB(srvProxy.getCurrentNumberOfMDB());
                oForm.setCurrentNumberOfSBF(srvProxy.getCurrentNumberOfSBF());
                oForm.setCurrentNumberOfSBL(srvProxy.getCurrentNumberOfSBL());
            } catch (RuntimeException e) {
                // Attibute missing
                 oForm.setCurrentNumberOfEJB(-1);
                 oForm.setCurrentNumberOfEntity(-1);
                 oForm.setCurrentNumberOfMDB(-1);
                 oForm.setCurrentNumberOfSBF(-1);
                 oForm.setCurrentNumberOfSBL(-1);
            }

        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }

        // Forward to the jsp.
        return (p_Mapping.findForward("DisplayNode"));
    }
}

