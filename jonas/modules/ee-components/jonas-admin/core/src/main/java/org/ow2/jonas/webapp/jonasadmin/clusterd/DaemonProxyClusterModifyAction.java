/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.webapp.jonasadmin.clusterd;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.lib.management.domain.DomainMonitor;
import org.ow2.jonas.lib.management.domain.proxy.server.ServerProxy;
import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.management.extensions.server.ServerItem;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;
import org.ow2.jonas.webapp.jonasadmin.common.BeanComparator;
import org.ow2.jonas.webapp.jonasadmin.monitoring.DaemonProxyClusterForm;


public class DaemonProxyClusterModifyAction extends JonasBaseAction {
    @Override
    public ActionForward executeAction(final ActionMapping p_Mapping,
            final ActionForm p_Form, final HttpServletRequest p_Request,
            final HttpServletResponse p_Response) throws IOException,
            ServletException {
        // Form used
        WhereAreYou oWhere = (WhereAreYou) p_Request.getSession().getAttribute(
                WhereAreYou.SESSION_NAME);
        String sDomainName = oWhere.getCurrentDomainName();
        String serverName = oWhere.getAdminJonasServerName();
        DaemonProxyClusterForm oForm = (DaemonProxyClusterForm) p_Form;
        try {
            String name = p_Request.getParameter("node");
            if(name == null)
            {
                //that means the request has been forwarded by another Action Class
                name = oWhere.getCurrentClusterDaemonName();

            }

            ObjectName on = JonasObjectName.clusterDaemonProxy(sDomainName, name);
            oWhere.setCurrentClusterDaemon(on);
            oWhere.setCurrentClusterDaemonName(name);

            oForm.setName((String) JonasManagementRepr.getAttribute(on,
                    "Name", serverName));

            oForm.setState((String) JonasManagementRepr.getAttribute(on,
                    "State", serverName));

//			getting requested cluster daemon Administred Jonas servers...
            DomainMonitor dm = DomainMonitor.getInstance();

//			Get servers running in this domain
            ArrayList al = new ArrayList();
            //are all the servers already started ?
            boolean allStarted = true;
            for (Iterator it = dm.getServerList().iterator(); it.hasNext();) {
                ServerProxy proxy = (ServerProxy) it.next();
                String cdName = proxy.getClusterDaemonName();

                if(cdName != null)
                {
                    if(cdName.equals(name))
                    {
                        ObjectName myOn = ObjectName.getInstance(proxy.getJ2eeObjectName());
                        al.add(new ServerItem(myOn, proxy.getState(), proxy.getClusterDaemonName()));
                        allStarted = (allStarted && proxy.getState().equals("RUNNING"));
                    }
                }
            }
            Collections.sort(al, new BeanComparator(new String[] {"name"}));
            p_Request.setAttribute("listServers", al);
            p_Request.setAttribute("allStarted", allStarted);


//			oForm.setSrvList((Collection)JonasManagementRepr.getAttribute(on,
//			"SrvList", serverName));


            //      	System.out.println("Url de connextion"+(String)JonasManagementRepr.getAttribute(on,"connectionUrl", serverName));



//			Force the node selected in tree
            m_WhereAreYou.selectNameNode("domain"
                    + WhereAreYou.NODE_SEPARATOR
                    + name, true);




        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }

        // Forward to the jsp.
        return (p_Mapping.findForward("DaemonProxyClusterModify"));
    }
}
