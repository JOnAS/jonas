/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Michel-Ange ANTON
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.taglib;

import javax.servlet.jsp.JspException;

import org.apache.struts.taglib.TagUtils;

public class JonasSubmitTag extends JonasButtonTag {

// --------------------------------------------------------  Instances Variables

    protected String ms_MyFunctionName = null;
    protected String ms_JavascriptName = null;

// --------------------------------------------------------- Properties Variables

    private String form = null;
    private String value = "Apply";

// --------------------------------------------------------- Properties Methods

    public String getForm() {
        return form;
    }

    public void setForm(String form) {
        this.form = form;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

// --------------------------------------------------------- Public Methods

    /**
     * Render the beginning.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doStartTag()
        throws JspException {
        TagUtils tagUtils = TagUtils.getInstance();

        ms_MyFunctionName = "MySubmit";
        ms_JavascriptName = "submit";

        StringBuffer sb = new StringBuffer();
        if (getDisabled() == false) {
            // Add the Javascript begining
            sb.append(prepareJavascript());
            //
            href = prepareCallJavascript();
        }
        tagUtils.write(pageContext, sb.toString());

        return (super.doStartTag());
    }

    /**
     * Render the end.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doEndTag()
        throws JspException {
        // Replace body by value
        if (value != null) {
            text = value;
        }
        return super.doEndTag();
    }

    public void release() {
        super.release();
        form = null;
        value = null;
        ms_MyFunctionName = null;
        ms_JavascriptName = null;
    }

// --------------------------------------------------------- Protected Methods

    protected String prepareCallJavascript() {
        if (form == null) {
            return "javascript:" + ms_MyFunctionName + "()";
        }
        else {
            return "javascript:" + ms_MyFunctionName + form + "()";
        }
    }

    protected String prepareJavascript() {
        StringBuffer sb = new StringBuffer();
        sb.append("<script language=\"JavaScript\" type=\"text/javascript\">");
        sb.append("function ");
        if (form == null) {
            sb.append(ms_MyFunctionName).append("() {document.forms[0]." + ms_JavascriptName
                + "();}");
        }
        else {
            sb.append(ms_MyFunctionName);
            sb.append(form);
            sb.append("() {document.");
            sb.append(form);
            sb.append("." + ms_JavascriptName + "();}");
        }
        sb.append("</script>");

        return sb.toString();
    }
}