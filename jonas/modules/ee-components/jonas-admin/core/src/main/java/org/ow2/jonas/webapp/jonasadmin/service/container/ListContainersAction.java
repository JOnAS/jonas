/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2009 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.container;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.management.javaee.J2eeObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasAdminJmx;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

/**
 * @author Michel-Ange ANTON
 * @author Adriana Danes
 */

public class ListContainersAction extends JonasBaseAction {

    // --------------------------------------------------------- Public Methods

    @Override
    public ActionForward executeAction(final ActionMapping p_Mapping, final ActionForm p_Form, final HttpServletRequest p_Request,
            final HttpServletResponse p_Response) throws IOException, ServletException {

        // Force the node selected in tree
        m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_SERVER) + WhereAreYou.NODE_SEPARATOR + "services"
                + WhereAreYou.NODE_SEPARATOR + "ejbContainers", true);

        // Force the deployment type to use the refreshing method
        m_WhereAreYou.setCurrentJonasDeploymentType(WhereAreYou.DEPLOYMENT_JAR);

        String s_refresh = p_Request.getParameter("refresh");
        boolean refresh = Boolean.valueOf(s_refresh).booleanValue();
        // This attribute is used in EditEjbCsStatisticAction, to know whether
        // to display infos on all containers or a selected one.
        m_Session.setAttribute("editAllContainers", "true");
        // no Form used
        try {
            // Refresh in case deploy/undeploy operation arrived.
            if (refresh) {
                refreshServicesTree(p_Request);
            }

            // Get container list
            String sFile;
            String sPath;
            String sName;
            String parentName;
            ArrayList<ContainerItem> al = new ArrayList<ContainerItem>();
            String domainName = m_WhereAreYou.getCurrentDomainName();
            String serverName = m_WhereAreYou.getCurrentJonasServerName();
            ObjectName onContainers = J2eeObjectName.getEJBModules(domainName, serverName);
            Iterator itNames = JonasManagementRepr.queryNames(onContainers, serverName).iterator();
            while (itNames.hasNext()) {
                ObjectName it_on = (ObjectName) itNames.next();
                URL url = (URL) JonasManagementRepr.getAttribute(it_on, "url", serverName);
                String path = url.getPath();
                sFile = JonasAdminJmx.extractFilename(path);
                sName = it_on.getKeyProperty("name");
                parentName = it_on.getKeyProperty(WhereAreYou.J2EE_APPLICATION_KEY);
                if (parentName != null && !"none".equals(parentName) && !"null".equals(parentName)) {
                    sName = parentName.concat(WhereAreYou.NODE_NAME_SEPARATOR + sName);
                }
                boolean add = al.add(new ContainerItem(sFile, path, it_on.toString(), sName));
            }

            Collections.sort(al, new ContainerItemByFile());
            // Set list in the request
            p_Request.setAttribute("listContainers", al);
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        // Forward to the jsp.
        return (p_Mapping.findForward("Containers"));
    }
}
