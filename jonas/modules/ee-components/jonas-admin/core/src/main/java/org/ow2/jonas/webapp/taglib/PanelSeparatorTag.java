/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Michel-Ange ANTON
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class PanelSeparatorTag extends TagSupport {

// --------------------------------------------------------- Public Methods

    /**
     * .
     *
     * @exception JspException if a processing error occurs
     */
    public int doEndTag()
        throws JspException {
        StringBuffer sb = new StringBuffer();
        // Render Separator
        sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
        sb.append(
            "<tr class=\"panelSeparator\"><td height=\"5\" class=\"panelSeparator\">&nbsp;</td></tr>");
        sb.append("</table>");

        // Render this element to our writer
        JspWriter out = pageContext.getOut();
        try {
            out.print(sb.toString());
        }
        catch (IOException e) {
            throw new JspException("Exception in " + getClass().getName() + " doEndTag():"
                + e.toString());
        }
        return EVAL_PAGE;
    }
}