/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.webservice;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;
import org.ow2.jonas.webapp.jonasadmin.service.webservice.provider.JOnASProvider;
import org.ow2.jonas.webapp.jonasadmin.service.webservice.provider.element.PortComponentItem;
import org.ow2.jonas.webapp.jonasadmin.service.webservice.provider.element.WebServiceDescriptionItem;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Vivek Lakshmanan
 */

public class PortComponentsDetailsAction extends JonasBaseAction {

    /**
     * PortComponents Details page.
     */
    private static final String PORT_COMPONENTS_DETAILS_FORWARD = "PortComponents Details";

    // --------------------------------------------------------- Public Methods

    public ActionForward executeAction(ActionMapping p_Mapping,
                                       ActionForm p_Form,
                                       HttpServletRequest p_Request,
                                       HttpServletResponse p_Response)
                         throws IOException, ServletException {

        // The webservice description selected by the user.
        String wsdId = p_Request.getParameter("wsdselect");
        // The port component selected by the user.
        String pcId = p_Request.getParameter("pcselect");

        try {
            JOnASProvider jonasProvider = new JOnASProvider(m_WhereAreYou.getCurrentJonasServerName());
            WebServiceDescriptionItem wsd = jonasProvider.getWebServiceDescription(wsdId);
            PortComponentItem pc = null;
            for (int i = 0; i < wsd.getPortComponentCount(); i++) {
                if (wsd.getPortComponent(i).getName().equals(pcId)) {
                    pc = wsd.getPortComponent(i);
                    break;
                }
            }

            // Set the selected description in the request.
            p_Request.setAttribute("selectedWebServiceDescription", wsd);
            p_Request.setAttribute("selectedPortComponent", pc);
            p_Request.setAttribute("implBean", pc.getServiceImplBean());
            HashMap map = new HashMap();
            map.put("wsdselect", wsd.getName());
            map.put("pcselect", pc.getName());
            p_Request.setAttribute("paramMap", map);

            // Force the node selected in tree
            m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_SERVER) + WhereAreYou.NODE_SEPARATOR
                    + "services" + WhereAreYou.NODE_SEPARATOR + "WebService" + WhereAreYou.NODE_SEPARATOR
                    + wsd.getName() + WhereAreYou.NODE_SEPARATOR + pc.getName(), true);
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward(GLOBAL_ERROR_FORWARD));
        }
        // Forward to the jsp.

        return (p_Mapping.findForward(PORT_COMPONENTS_DETAILS_FORWARD));
    }

}
