/**
 *==============================================================================
 * Copyright (C) 2001-2005 by Allesta, LLC. All rights reserved.
 * Copyright (C) 2007 Bull S.A.S.
 *==============================================================================
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *==============================================================================
 * $Id$
 *==============================================================================
 */

package org.ow2.jonas.webapp.jonasadmin.service.webservice.provider.element;

import java.util.Vector;

import javax.xml.namespace.QName;

import org.ow2.jonas.lib.management.extensions.base.NameItem;


/**
 * The port-component element associates a WSDL port with a Web service
 * interface and implementation. It defines the name of the port as a component,
 * optional description, optional display name, optional iconic representations,
 * WSDL port QName, Service Endpoint Interface, Service Implementation Bean.
 * Used in: webservices
 *
 * @author Allesta, LLC
 */
public class PortComponentItem implements NameItem {
    // ~ Instance fields
    // ------------------------------------------------------------------

    private String portComponentName = null;

    private QName wsdlPort = null;

    private String serviceEndpointInterface = null;

    private Vector handlers = new Vector();

    private String portUrl = null;

    private ServiceImplBean serviceImplBean = null;

    // ~ Constructors
    // ---------------------------------------------------------------------

    /**
     * Creates a new PortComponent object.
     *
     * @param portUrl
     *            TODO
     */
    public PortComponentItem(String name, QName wsdlPort, String enpoint, ServiceImplBean serviceImplBean,
            String portUrl) {
        this.portComponentName = name;
        this.wsdlPort = wsdlPort;
        this.serviceEndpointInterface = enpoint;
        this.serviceImplBean = serviceImplBean;
        this.portUrl = portUrl;
    }

    /**
     * DOCUMENT ME!
     *
     * @param index
     *            DOCUMENT ME!
     * @param handler
     *            DOCUMENT ME!
     */
    public void setHandler(int index, HandlerItem handler) {
        if ((handlers.size() == 0) || (index >= handlers.size())) {
            handlers.add(handler);

            return;
        }

        handlers.insertElementAt(handler, index);
    }

    /**
     * DOCUMENT ME!
     *
     * @param index
     *            DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public HandlerItem getHandler(int index) {
        return (HandlerItem) handlers.get(index);
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public int getHandlerCount() {
        return handlers.size();
    }

    /**
     * DOCUMENT ME!
     *
     * @param portComponentName
     *            DOCUMENT ME!
     */
    public void setPortComponentName(String portComponentName) {
        this.portComponentName = portComponentName;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getPortComponentName() {
        return portComponentName;
    }

    /**
     * DOCUMENT ME!
     *
     * @param serviceEndpointInterface
     *            DOCUMENT ME!
     */
    public void setServiceEndpointInterface(String serviceEndpointInterface) {
        this.serviceEndpointInterface = serviceEndpointInterface;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getServiceEndpointInterface() {
        return serviceEndpointInterface;
    }

    /**
     * DOCUMENT ME!
     *
     * @param serviceImplBean
     *            DOCUMENT ME!
     */
    public void setServiceImplBean(ServiceImplBean serviceImplBean) {
        this.serviceImplBean = serviceImplBean;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public ServiceImplBean getServiceImplBean() {
        return serviceImplBean;
    }

    /**
     * DOCUMENT ME!
     *
     * @param wsdlPort
     *            DOCUMENT ME!
     */
    public void setWsdlPort(QName wsdlPort) {
        this.wsdlPort = wsdlPort;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public QName getWsdlPort() {
        return wsdlPort;
    }

    /**
     * DOCUMENT ME!
     *
     * @param index
     *            DOCUMENT ME!
     */
    public void removeHandler(int index) {
        handlers.remove(index);
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String toString() {
        StringBuffer sb = new StringBuffer();

        sb.append(super.toString());
        sb.append("portComponentName=");
        sb.append(getPortComponentName());

        sb.append(", wsdlPort=");
        sb.append(wsdlPort);

        sb.append(", serviceEndpointInterface=");
        sb.append(getServiceEndpointInterface());

        sb.append(", serviceImplBean=");
        sb.append(getServiceImplBean());

        if (handlers.size() > 0) {
            sb.append(", handlers=");
            sb.append(handlers);
        }

        return sb.toString();
    }

    public String getPortUrl() {
        return portUrl;
    }

    public void setPortUrl(String portUrl) {
        this.portUrl = portUrl;
    }

    public String getName() {
        return portComponentName;
    }
}
