/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.resource;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.management.ObjectName;

import javax.servlet.http.HttpServletRequest;

import org.ow2.jonas.deployment.rar.RequiredConfigPropertyDesc;
import org.ow2.jonas.webapp.jonasadmin.Jlists;
import org.ow2.jonas.webapp.jonasadmin.JonasAdminJmx;
import org.ow2.jonas.webapp.jonasadmin.common.LabelValueByLabelComparator;
import org.ow2.jonas.webapp.taglib.LabelValueBean;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;


/**
 * @author Michel-Ange ANTON
 */
public class ResourceAdapterAOForm extends ActionForm {

// --------------------------------------------------------- Properties variables

    private String description = null;
    private String name = null;
    private ArrayList listProperties = new ArrayList();
    private ObjectName oName = null;

    private ArrayList listUsedByMdb = new ArrayList();

// --------------------------------------------------------- Public Methods

    public void reset(ActionMapping mapping, HttpServletRequest request) {
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors oErrors = new ActionErrors();
        return oErrors;
    }

// --------------------------------------------------------- Properties Methods

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList getListProperties() {
        return listProperties;
    }

    public void setListProperties(Properties properties) {
        this.listProperties.clear();
        // Fill list with properties (keys and values)
        String sKey;
        Enumeration oEnum = properties.keys();
        while (oEnum.hasMoreElements()) {
            sKey = oEnum.nextElement().toString();
            this.listProperties.add(new LabelValueBean(sKey, properties.getProperty(sKey, "")));
        }
        Collections.sort(this.listProperties, new LabelValueByLabelComparator());
    }


    public void setOName(ObjectName oName) {
        this.oName = oName;
    }
    
    public ObjectName getOName() {
        return oName;
    }

    public ArrayList getListUsedByMdb() {
        return listUsedByMdb;
    }

    public void setListUsedByMdb(ArrayList listUsedByMdb) {
        this.listUsedByMdb = listUsedByMdb;
    }
    
}