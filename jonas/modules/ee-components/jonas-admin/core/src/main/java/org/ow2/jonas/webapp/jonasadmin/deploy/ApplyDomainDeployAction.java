/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.deploy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 * @author Patrick Smith
 * @author Gregory Lapouchnian
 * @author Adriana Danes
 */
public class ApplyDomainDeployAction extends BaseDeployAction {

    // --------------------------------------------------------- Public Methods

    /**
     */
    public ActionForward executeAction(ActionMapping p_Mapping,
            ActionForm p_Form, HttpServletRequest p_Request,
            HttpServletResponse p_Response) throws IOException,
            ServletException {

        String sForwardDeploy = "Domain Deploy Confirm";
        String sForwardUndeploy = "Domain Undeploy Confirm";
        String sForward = null;

        // Form used
        DomainDeployForm oForm = (DomainDeployForm) p_Form;
        oForm.setDeploymentInProgress(false);
        oForm.setDeploymentCompleted(false);

        try {

            // set the array lists of selected apps and targets based on
            // the selections returned by struts
            ArrayList appList = new ArrayList();
            ArrayList targetList = new ArrayList();
            ArrayList targetNamesList = new ArrayList();

            ArrayList allPossibleTargets = oForm.getListTargetNames();

            int clustIndex = 0;
            boolean foundClust = false;
            for (int i = 0; i < oForm.getTargetSelected().length; i++) {
                //System.out.println(oForm.getTargetSelected()[i]);
                String selectedTargetON = oForm.getTargetSelected()[i];
                if (!targetList.contains(selectedTargetON)) {
                    targetList.add(selectedTargetON);
                    ObjectName selectedTargetOn = ObjectName.getInstance(selectedTargetON);
                    String targetNameToPrint = selectedTargetOn.getKeyProperty("name");
                    targetNamesList.add(targetNameToPrint);
                }
            }

            for (int i = 0; i < oForm.getDeploySelected().length; i++) {
                appList.add(oForm.getDeploySelected()[i]);
            }

            oForm.setListDeploy(appList);
            oForm.setListTargetsSelected(targetList);
            oForm.setListTargetSelectedNames(targetNamesList);

            // make sure that the action and the replacement option are transferred
            // to the next action
            oForm.setSelectedAction(oForm.getSelectedOption());
            oForm.setReplacementOption(oForm.getReplaceOnTarget());

            // Fill up report with initial empty value for each application
            Map blankReport = new TreeMap();
            for (int i = 0; i < appList.size(); i++) {
                HashMap appReport = new HashMap();
                blankReport.put(appList.get(i), appReport);
            }
            oForm.setReports(blankReport);

            // at least one entry must be selected from the list of servers and
            // from the list of applications
            oForm.setConfirm(oForm.getDeploySelected().length > 0
                    && oForm.getTargetSelected().length > 0);

            if (!oForm.isConfirm()) {
                m_Errors.add("error.domain.deploy.noselect",
                             new ActionMessage("error.domain.deploy.noselect"));
                saveErrors(p_Request, m_Errors);
                sForward = getForwardEdit();
            }

        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }

        if (oForm.getSelectedAction().equals(DomainDeployForm.UNDEPLOY)) {
            sForward = sForwardUndeploy;
        } else {
            sForward = sForwardDeploy;
        }
        // Forward to the jsp.
        return (p_Mapping.findForward(sForward));
    }

}
