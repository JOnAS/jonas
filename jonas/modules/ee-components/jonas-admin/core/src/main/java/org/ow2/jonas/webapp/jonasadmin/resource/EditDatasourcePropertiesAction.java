/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.resource;

import java.io.IOException;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * @author Michel-Ange ANTON
 */

public class EditDatasourcePropertiesAction extends JonasBaseAction {

// --------------------------------------------------------- Public Methods

    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws IOException, ServletException {

        // Current server
        String serverName = m_WhereAreYou.getCurrentJonasServerName();
        String domainName = m_WhereAreYou.getCurrentDomainName();

        // Datasource to create
        String sAction = p_Request.getParameter("action");
        if (sAction == null) {
            sAction ="edit";
        }
        // Datasource to edit
        String sName = p_Request.getParameter("name");

        DatasourcePropertiesForm oForm = null;
        oForm = new DatasourcePropertiesForm();
        m_Session.setAttribute("datasourcePropertiesForm", oForm);
        oForm.reset(p_Mapping, p_Request);
        oForm.setAction(sAction);

        // Populate
        if (sName != null) {
            try {
                populate(sName, oForm, domainName, serverName);
            }
            catch (Throwable t) {
                addGlobalError(t);
                saveErrors(p_Request, m_Errors);
                return (p_Mapping.findForward("Global Error"));
            }
        }
        // Forward to the jsp.
        return (p_Mapping.findForward("Datasource Properties"));
    }

// --------------------------------------------------------- Protected Methods

    protected void populate(String p_Name, DatasourcePropertiesForm p_Form, String domainName, String serverName)
        throws Exception {

        String[] asParam = new String[1];
        String[] asSignature = new String[1];
        asParam[0] = p_Name;
        asSignature[0] = "java.lang.String";

        Properties oProps = (Properties) JonasManagementRepr.invoke(JonasObjectName.databaseService(domainName)
            , "getDataSourcePropertiesFile", asParam, asSignature, serverName);
        // Populate
        p_Form.setName(p_Name);
        p_Form.setDatasourceName(getStringAttribute(oProps, "datasource.name"
            , p_Form.getDatasourceName()));
        p_Form.setDatasourceDescription(getStringAttribute(oProps, "datasource.description"
            , p_Form.getDatasourceDescription()));
        p_Form.setDatasourceUrl(getStringAttribute(oProps, "datasource.url"
            , p_Form.getDatasourceUrl()));
        p_Form.setDatasourceClassname(getStringAttribute(oProps, "datasource.classname"
            , p_Form.getDatasourceClassname()));
        p_Form.setDatasourceUsername(getStringAttribute(oProps, "datasource.username"
            , p_Form.getDatasourceUsername()));
        p_Form.setDatasourcePassword(getStringAttribute(oProps, "datasource.password"
            , p_Form.getDatasourcePassword()));
        p_Form.setDatasourceMapper(getStringAttribute(oProps, "datasource.mapper"
            , p_Form.getDatasourceMapper()));
        p_Form.setJdbcConnmaxage(getStringAttribute(oProps, "jdbc.connmaxage"
            , p_Form.getJdbcConnmaxage()));
        p_Form.setJdbcMaxopentime(getStringAttribute(oProps, "jdbc.maxopentime"
            , p_Form.getJdbcMaxopentime()));
        p_Form.setJdbcConnchecklevel(getStringAttribute(oProps, "jdbc.connchecklevel"
            , p_Form.getJdbcConnchecklevel()));
        p_Form.setJdbcConnteststmt(getStringAttribute(oProps, "jdbc.connteststmt"
            , p_Form.getJdbcConnteststmt()));
        p_Form.setJdbcMinconpool(getStringAttribute(oProps, "jdbc.minconpool"
            , p_Form.getJdbcMinconpool()));
        p_Form.setJdbcMaxconpool(getStringAttribute(oProps, "jdbc.maxconpool"
            , p_Form.getJdbcMaxconpool()));
        p_Form.setJdbcMaxwaittime(getStringAttribute(oProps, "jdbc.maxwaittime"
            , p_Form.getJdbcMaxwaittime()));
        p_Form.setJdbcMaxwaiters(getStringAttribute(oProps, "jdbc.maxwaiters"
            , p_Form.getJdbcMaxwaiters()));
        p_Form.setJdbcSamplingperiod(getStringAttribute(oProps, "jdbc.samplingperiod"
            , p_Form.getJdbcSamplingperiod()));
        p_Form.setJdbcAdjustperiod(getStringAttribute(oProps, "jdbc.adjustperiod"
                , p_Form.getJdbcAdjustperiod()));
    }
}
