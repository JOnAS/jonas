/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.cmi;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasAdminJmx;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class CmiServerAction extends JonasBaseAction {
    @Override
    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form, HttpServletRequest p_Request, HttpServletResponse p_Response) throws IOException, ServletException {
        String sDomainName = m_WhereAreYou.getCurrentDomainName();
        String serverName = m_WhereAreYou.getCurrentJonasServerName();
        String forwardName = "displayCmiServerInfo";
        p_Request.setAttribute("htmlTextLoadFactor", "htmlText");
        CmiServerForm cmiServerForm = (CmiServerForm) p_Form;
        CmiForm currentCmiForm = (CmiForm) m_Session.getAttribute("currentCmiForm");
        try {
            String serverUrl = p_Request.getParameter("providerUrl");
            if (serverUrl == null) {
                serverUrl = cmiServerForm.getUrl();
            }
            ObjectName cmiOn = JonasObjectName.cmiServer(sDomainName, serverName);
            if (JonasAdminJmx.hasMBeanName(cmiOn, serverName)) {
                    //display server info
                if (cmiServerForm.getAction() != null) {
                    if ("loadfactor".equals(cmiServerForm.getAction())) {
                         String[] signature = {"java.lang.String", "java.lang.String"};
                         String[] params = {cmiServerForm.getUrl(), cmiServerForm.getLoadFactor().toString()};
                         JonasManagementRepr.invoke(cmiOn, "setLoadFactor", params, signature, serverName);
                         p_Request.setAttribute("htmlTextLoadFactor", "htmlTextChanged");
                     }else {
                         //unknown action.
                         ;
                     }
                }

                //always get server Infos.
                String[] sign = {"java.lang.String"};
                String[] parameters = {serverUrl};
                cmiServerForm.setIsBlackListed((Boolean)JonasManagementRepr.invoke(cmiOn, "isServerBlackListed", parameters, sign, serverName));
                cmiServerForm.setUrl(serverUrl);
                cmiServerForm.setProtocol(serverUrl.substring(0, serverUrl.indexOf(":")));
                Set<Protocol> currentProto = currentCmiForm.getProtocols();
                /**
                 * use this to get informations that change rarely.
                 */
                Provider currentServer = null;
                for (Protocol protocol : currentProto) {
                    if (protocol.getName().equals(cmiServerForm.getProtocol())) {
                        List providers = protocol.getProviders();
                        for (Iterator iterator = providers.iterator(); iterator
                                .hasNext();) {
                            currentServer = (Provider) iterator.next();
                            if (currentServer.getUrl().equals(serverUrl)) {
                                break;
                            }
                        }
                        break;
                    }
                }
                cmiServerForm.setLoadFactor((Integer)JonasManagementRepr.invoke(cmiOn, "getLoadFactor", parameters, sign, serverName));

                if (currentServer != null) {
                    /**
                     * server name is already set in ApplyCmiAction. let get it
                     */
                    cmiServerForm.setName(currentServer.getName());
                }


            }else {
//				cmi mbean is not registered
                p_Request.setAttribute("newcmi", false);
            }
        }catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        // Forward to the jsp.
        return (p_Mapping.findForward(forwardName));
    }
}
