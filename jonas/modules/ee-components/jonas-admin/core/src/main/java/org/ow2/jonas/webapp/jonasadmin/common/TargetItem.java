package org.ow2.jonas.webapp.jonasadmin.common;

import javax.management.ObjectName;

import org.ow2.jonas.lib.management.extensions.base.mbean.ObjectNameItem;

public class TargetItem extends ObjectNameItem {
    /**
     * Target state
     */
    private String state = null;

    public TargetItem(final ObjectName pObjectName) {
        super(pObjectName);
    }

    public String getState() {
        return state;
    }

    public void setState(final String state) {
        this.state = state;
    }


}
