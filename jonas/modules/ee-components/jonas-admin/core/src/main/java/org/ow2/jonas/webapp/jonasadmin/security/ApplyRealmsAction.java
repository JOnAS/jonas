/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2009 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ow2.jonas.lib.management.extensions.base.RealmItem;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;

/**
 * @author Michel-Ange ANTON
 */

public class ApplyRealmsAction extends JonasBaseAction {

    // --------------------------------------------------------- Public Methods

    @Override
    public ActionForward executeAction(final ActionMapping p_Mapping, final ActionForm p_Form, final HttpServletRequest p_Request,
            final HttpServletResponse p_Response) throws IOException, ServletException {

        String sForward = null;

        // Form used
        ItemsRealmsForm oForm = (ItemsRealmsForm) p_Form;
        // Display list
        ArrayList alRealms = (ArrayList) m_Session.getAttribute("listRealms");

        // Sort selected items array to correct use of Arrays.binarySearch
        // method
        Arrays.sort(oForm.getSelectedItems());

        // Actions
        try {
            // Search type realm
            ArrayList alSelected = new ArrayList();
            if (alRealms != null) {
                int iPos;
                RealmItem oRealmItem = null;
                for (int i = 0; i < alRealms.size(); i++) {
                    oRealmItem = (RealmItem) alRealms.get(i);
                    iPos = Arrays.binarySearch(oForm.getSelectedItems(), oRealmItem.getName());
                    if (iPos >= 0) {
                        alSelected.add(new RealmItem(oRealmItem.getName(), oRealmItem.getType()));
                    }
                }
            }
            // Set selected realm
            oForm.setSelectedRealmItem(alSelected);
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }

        // Forward to the jsp.
        return (p_Mapping.findForward("Confirm Realms"));
    }

    // --------------------------------------------------------- Protected
    // Methods

}
