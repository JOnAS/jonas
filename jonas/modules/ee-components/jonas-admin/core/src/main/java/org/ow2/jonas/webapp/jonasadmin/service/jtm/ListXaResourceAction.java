/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.jtm;

import java.io.IOException;
import java.util.ArrayList;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.management.javaee.J2eeObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Tony Ortiz
 */

public class ListXaResourceAction extends JonasBaseAction {

    private String myasparam = null;

// --------------------------------------------------------- Public Methods

    public ActionForward executeAction(ActionMapping pMapping, ActionForm pForm
        , HttpServletRequest pRequest, HttpServletResponse pResponse)
        throws IOException, ServletException {

        // Reset the selected items in case the user is flip-flopping
        // between tabs on jsp pages
        try {
            ItemsXaResourceForm xForm = (ItemsXaResourceForm) m_Session.getAttribute("itemsXaResourceForm");
            xForm.setAction(null);
            xForm.setSelectedItems(new String[0]);
        } catch (Exception e) {
            // ignore, we may not have yet invoked ItemxXaResourceForm
            ;
        }

        // Force the node selected in tree
        m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_SERVER) + WhereAreYou.NODE_SEPARATOR
            + "services" + WhereAreYou.NODE_SEPARATOR + "transaction", true);

        // Form used
        JtmServiceXAResourceForm oForm = (JtmServiceXAResourceForm) pForm;

        try {
            // Get container list
            String[] asParam = new String[1];
            String[] asSignature = {"java.lang.String"};
            String mys;
            String sResmgr;
            String sResource;
            String sFullXid;
            String sXid;
            String sXidstate;
            String sXidcount;
            Object txObject;
            String [] myTxInfo;
            int    txInfoSize;
            ArrayList al = new ArrayList();

            // transaction to edit
            String sName = pRequest.getParameter("fulltrans");

            if (sName == null) {
                 sName = myasparam;
            } else {
                 myasparam = sName;
            }

            asParam[0] = sName;

            // Object name used
            String currentDomainName = m_WhereAreYou.getCurrentDomainName();
            String currentJonasServerName = m_WhereAreYou.getCurrentJonasServerName();
            String jtaResourceName = "JTAResource";
            ObjectName jtaResourceObjectName = J2eeObjectName.JTAResource(currentDomainName, currentJonasServerName, jtaResourceName);

            txObject = JonasManagementRepr.invoke(jtaResourceObjectName, "getAllXAResource", asParam, asSignature, currentJonasServerName);

            myTxInfo = (String []) txObject;

            if (txObject != null) {
                txInfoSize = myTxInfo.length;

                for (int i = 0; i < txInfoSize; i++) {
                    mys = myTxInfo[i];
                    int myix1 = mys.indexOf("????");
                    sResmgr = mys.substring(0, myix1);
                    int myix2 = mys.indexOf("????", myix1 + 4);
                    sResource = mys.substring(myix1 + 4, myix2);
                    int myix3 = mys.indexOf("????", myix2 + 4);
                    sFullXid = mys.substring(myix2 + 4, myix3);
                    int myix4 = mys.indexOf("????", myix3 + 4);
                    sXid = mys.substring(myix3 + 4, myix4);
                    sXidstate = mys.substring(myix4 + 4);
                    al.add(new TxXaresource (null, sResmgr, sResource, sFullXid, sXid, sXidstate));
                }
             }

             // Set list in the request
             pRequest.setAttribute("listXaResourceEntries", al);
        }
        catch (Throwable t) {
            addGlobalError(t);
            saveErrors(pRequest, m_Errors);
            return (pMapping.findForward("Global Error"));
        }

        // Forward to the jsp.
        return (pMapping.findForward("Jtm XAResource"));
    }
}
