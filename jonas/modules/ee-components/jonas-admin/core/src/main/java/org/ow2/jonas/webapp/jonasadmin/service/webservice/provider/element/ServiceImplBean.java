/**
 *==============================================================================
 * Copyright (C) 2001-2005 by Allesta, LLC. All rights reserved.
 * Copyright (C) 2007 Bull S.A.S.
 *==============================================================================
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *==============================================================================
 * $Id$
 *==============================================================================
 */

package org.ow2.jonas.webapp.jonasadmin.service.webservice.provider.element;

/**
 * The service-impl-bean element defines the Web service implementation. A
 * service implementation can be an EJB bean class or JAX-RPC web component.
 * Existing EJB implementations are exposed as a Web service using an ejb-link.
 * Used in: port-component!
 *
 * @author Allesta, LLC
 */
public class ServiceImplBean {
    // ~ Static fields/initializers
    // -------------------------------------------------------

    public static final int SERVLET_BEAN = 0;

    public static final int EJB_BEAN = 1;

    // ~ Instance fields
    // ------------------------------------------------------------------

    private int beanType = 0;

    private String implBeanLink = null;

    // ~ Constructors
    // ---------------------------------------------------------------------

    /**
     * Creates a new ServiceImplBean object.
     *
     * @param beanType
     *            DOCUMENT ME!
     */
    public ServiceImplBean(int beanType, String implBeanLink) {
        this.beanType = beanType;
        this.implBeanLink = implBeanLink;
    }

    // ~ Methods
    // --------------------------------------------------------------------------

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean isEjbBean() {
        return (this.beanType == EJB_BEAN);
    }

    /**
     * DOCUMENT ME!
     *
     * @param ejbLink
     *            DOCUMENT ME!
     */
    public void setImplBeanLink(String ejbLink) {
        this.implBeanLink = ejbLink;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getImplBeanLink() {
        return implBeanLink;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean isServletBean() {
        return (this.beanType == SERVLET_BEAN);
    }
}
