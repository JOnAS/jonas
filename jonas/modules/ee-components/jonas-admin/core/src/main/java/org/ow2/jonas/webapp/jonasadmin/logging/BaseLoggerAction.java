/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.logging;

import javax.management.ObjectName;
import javax.servlet.http.HttpServletRequest;

import org.ow2.jonas.lib.management.javaee.J2eeObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasTreeBuilder;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;
import org.ow2.jonas.webapp.jonasadmin.deploy.BaseDeployAction;
import org.ow2.jonas.webapp.taglib.TreeControl;
import org.ow2.jonas.webapp.taglib.TreeControlNode;


/**
 * @author Michel-Ange ANTON
 */
abstract public class BaseLoggerAction extends BaseDeployAction {

// --------------------------------------------------------- Protected Methods

    /**
     * Refresh the tree.
     *
     * @param p_Request Current instance
     * @throws Exception
     */
    protected void refreshTree(HttpServletRequest p_Request)
        throws Exception {
        // Refresh Logger Tree
        refreshLoggersTree(p_Request);
        // Refresh MBeans Tree
        refreshMBeansTree(p_Request);
        // Force display to refresh
        m_WhereAreYou.setTreeToRefresh(true);
    }

    /**
     * Refresh the loggers branch tree.
     *
     * @param p_Request Current instance
     * @throws Exception
     */
    protected void refreshLoggersTree(HttpServletRequest p_Request)
        throws Exception {
        // Get the domain and server name
        String sDomainLabel = "domain";
        ObjectName oObjectName = J2eeObjectName.J2EEServer(m_WhereAreYou.getCurrentDomainName()
            , m_WhereAreYou.getCurrentJonasServerName());
        String sServerName = getStringAttribute(oObjectName, "serverName");
        // Get the node name
        String sCurrentNodeNameItem = sDomainLabel + WhereAreYou.NODE_SEPARATOR + sServerName + WhereAreYou.NODE_SEPARATOR + "logging";
        // Get current tree
        TreeControl oControl = m_WhereAreYou.getTreeControl();
        // Get branch root node
        TreeControlNode oBranchRootNode = oControl.findNode(sCurrentNodeNameItem);
        // Enable auto-refresh mode
        oControl.enableAutoRefresh();
        // Remove old children
        TreeControlNode[] aoNodes = oBranchRootNode.findChildren();
        for (int i = 0; i < aoNodes.length; i++) {
            aoNodes[i].remove();
        }
        // Build node for the Service
        JonasTreeBuilder oBuilder = new JonasTreeBuilder();
        oBuilder.getLoggers(oBranchRootNode, m_Resources, p_Request);
        // Disable auto-refresh mode
        oControl.disableAutoRefresh();
    }
}
