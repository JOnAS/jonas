/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.resourceadapter;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.ow2.jonas.webapp.jonasadmin.Jlists;

/**
 * The form used for the create resource adapter functionality.
 *
 * @author Patrick Smith
 */
public class CreateResourceAdapterForm extends ActionForm {

// --------------------------------------------------------- Constants

// --------------------------------------------------------- Properties variables

    private String rarName = null;
    private String displayName = null;
    private String description = null;
    private String vendorName = null;
    private String specVersion = null;
    private String eisType = null;
    private String template = "JDBC";
    private boolean isDomain = false;

    private String jndiName = null;
    private String nativeLib = null;
    private String logEnabled = null;
    private String logTopic = null;
    private int poolInit = 0;
    private int poolMin = 0;
    private int poolMax = 100;
    private int poolMaxAge = 0;
    private int pstmtMax = 10;
    private String pstmtCachePolicy = null;

    private int poolMaxOpenTime = 0;
    private int poolMaxWaiters = 0;
    private int poolMaxWaittime = 0;
    private int poolSamplingPeriod = 30;
    private int checkLevel = 0;
    private String dsClass = null;
    private String URL = null;
    private String user = null;
    private String password = null;
    private String loginTimeout = null;
    private String isolationLevel = null;
    private String mapperName = null;
    private String configLogTopic = null;

    private List booleanValues = Jlists.getBooleanValues();
    private List checkingLevels = Jlists.getJdbcConnectionCheckingLevels();
    private List specVersions = Jlists.getSpecVersion();

//  --------------------------------------------------------- Public Methods

    /**
     * Returns the spec version.
     * @return Returns the specVersions.
     */
    public List getSpecVersions() {
        return specVersions;
    }

    /**
     * Set the spec version for the form.
     * @param specVersions The specVersions to set.
     */
    public void setSpecVersions(final List specVersions) {
        this.specVersions = specVersions;
    }


    /**
     * Reset the form back to its default values.
     * @param mapping the ActionMapping used for the struts actions.
     * @param request the HttpServletRequest used in the execution of the struts actions.
     */
    @Override
    public void reset(final ActionMapping mapping, final HttpServletRequest request) {
        // Reset datas
        rarName = null;
        displayName = null;
        description = null;
        vendorName = null;
        specVersion = null;
        eisType = null;
        isDomain = false;

        jndiName = null;
        nativeLib = null;
        logEnabled = null;
        logTopic = null;
        poolInit = 0;
        poolMin = 0;
        poolMax = 100;
        poolMaxAge = 0;
        pstmtMax = 10;
        pstmtCachePolicy = null;
        poolMaxOpenTime = 0;
        poolMaxWaiters = 0;
        poolMaxWaittime = 0;
        poolSamplingPeriod = 30;
        checkLevel = 0;
        dsClass = null;
        URL = null;
        user = null;
        password = null;
        loginTimeout = null;
        isolationLevel = null;
        mapperName = null;
        configLogTopic = null;
    }

    /**
     * Validates the values in the form.
     * @param mapping the ActionMapping used in the struts actions.
     * @param request the HttpServletRequest used for the struts actions.
     * @return The errors generated from the validation.
     */
    @Override
    public ActionErrors validate(final ActionMapping mapping,
            final HttpServletRequest request) {

        ActionErrors oErrors = new ActionErrors();
        if ((rarName == null) || (rarName.length() == 0)) {
            oErrors.add("name", new ActionMessage(
                    "error.resourceadapter.rarname.required"));
        }
        if (template.equals("Other")) {
            if ((displayName == null) || (displayName.length() == 0)) {
                oErrors.add("datasourceName", new ActionMessage(
                        "error.resourceadapter.displayname.required"));
            }
            if ((description == null) || (description.length() == 0)) {
                oErrors.add("datasourceClassname", new ActionMessage(
                        "error.resourceadapter.description.required"));
            }
            if ((vendorName == null) || (vendorName.length() == 0)) {
                oErrors.add("datasourceClassname", new ActionMessage(
                        "error.resourceadapter.vendorname.required"));
            }
            if ((specVersion == null) || (specVersion.length() == 0)) {
                oErrors.add("datasourceClassname", new ActionMessage(
                        "error.resourceadapter.specversion.required"));
            }
            if ((eisType == null) || (eisType.length() == 0)) {
                oErrors.add("datasourceClassname", new ActionMessage(
                        "error.resourceadapter.eistype.required"));
            }
        }
        return oErrors;
    }

// --------------------------------------------------------- Properties Methods

    /**
     * Return the name of the RAR file.
     * @return the name of the RAR file.
     */
    public String getRarName() {
        return this.rarName;
    }

    /**
     * Sets the name of the RAR file.
     * @param rarName the new name of the RAR file.
     */
    public void setRarName(final String rarName) {
        this.rarName = rarName;
    }

    /**
     * Returns the display name for this RAR.
     * @return the RAR's display name.
     */
    public String getDisplayName() {
        return this.displayName;
    }

    /**
     * Sets the display name for this RAR.
     * @param displayName The new display name.
     */
    public void setDisplayName(final String displayName) {
        this.displayName = displayName;
    }

    /**
     * Return the description for this RAR.
     * @return the description for this RAR.
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Sets the description used for this RAR.
     * @param description The new description for this RAR.
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Returns the vendor name used for this RAR.
     * @return The vendor name used for this RAR.
     */
    public String getVendorName() {
        return vendorName;
    }

    /**
     * Sets the vendor name used for this RAR.
     * @param vendorName The new vendor name for this RAR.
     */
    public void setVendorName(final String vendorName) {
        this.vendorName = vendorName;
    }

    /**
     * Returns the spec version for this RAR.
     * @return the spec version used for this RAR.
     */
    public String getSpecVersion() {
        return this.specVersion;
    }

    /**
     * Sets the spec version to use for this RAR.
     * @param specVersion the new Spec version for this RAR.
     */
    public void setSpecVersion(final String specVersion) {
        this.specVersion = specVersion;
    }

    /**
     * Returns the EIS Type for this RAR.
     * @return The EIS type for this RAR.
     */
    public String getEisType() {
        return this.eisType;
    }

    /**
     * Sets the EIS Type for this RAR.
     * @param eisType The new EIS type to use for thsi RAR.
     */
    public void setEisType(final String eisType) {
        this.eisType = eisType;
    }

    /**
     * Returns which template this new RAR should be based on.
     * @return which template this new RAR should be based on.
     */
    public String getTemplate() {
        return this.template;
    }

    /**
     * Sets which template to use for this RAR.
     * @param template The new template to use.
     */
    public void setTemplate(final String template) {
        this.template = template;
    }

    /**
     * Returns this RAR's JNDI name.
     * @return This RAR's JNDI name.
     */
    public String getJndiName() {
        return this.jndiName;
    }

    /**
     * Sets the JNDI name for this RAR.
     * @param jndiName The JNDI name for this RAR.
     */
    public void setJndiName(final String jndiName) {
        this.jndiName = jndiName;
    }

    /**
     * Returns the Native Lib to use for this RAR.
     * @return The native Lib to use for this RAR.
     */
    public String getNativeLib() {
        return this.nativeLib;
    }

    /**
     * Sets the native lib to use for this RAR.
     * @param nativeLib the new Native lib to use for this RAR.
     */
    public void setNativeLib(final String nativeLib) {
        this.nativeLib = nativeLib;
    }

    /**
     * If this RAR should be log enabled.
     * @return if this RAR should be log enabled.
     */
    public String getLogEnabled() {
        return this.logEnabled;
    }

    /**
     * Sets if this RAR should be log enabled.
     * @param logEnabled
     */
    public void setLogEnabled(final String logEnabled) {
        this.logEnabled = logEnabled;
    }

    /**
     * Get the log topic for this RAR.
     * @return the log topic for this RAR.
     */
    public String getLogTopic() {
        return this.logTopic;
    }

    /**
     * Sets the log topic for this RAR.
     * @param logTopic the new log topic for this RAR.
     */
    public void setLogTopic(final String logTopic) {
        this.logTopic = logTopic;
    }

    /**
     * Returns the pool init value for this RAR.
     * @return the pool init value to use for this RAR.
     */
    public int getPoolInit() {
        return this.poolInit;
    }

    /**
     * Sets a new pool init value for this RAR.
     * @param poolInit the new pool init value for this RAR.
     */
    public void setPoolInit(final int poolInit) {
        this.poolInit = poolInit;
    }

    /**
     * Returns the pool min value to use for this RAR.
     * @return the pool min value to use for this RAR.
     */
    public int getPoolMin() {
        return this.poolMin;
    }

    /**
     * Sets the pool min value to use.
     * @param poolMin The new pool min value to use for this RAR.
     */
    public void setPoolMin(final int poolMin) {
        this.poolMin = poolMin;
    }

    /**
     * Returns the pool max value used for this RAR.
     * @return The pool max value used for this RAR.
     */
    public int getPoolMax() {
        return this.poolMax;
    }

    /**
     * Sets the pool max value of this new RAR.
     * @param poolMax The new pool max value of this RAR.s
     */
    public void setPoolMax(final int poolMax) {
        this.poolMax = poolMax;
    }

    /**
     * Returns the pool max age for this new RAR.
     * @return the pool max age value used for this RAR.
     */
    public int getPoolMaxAge() {
        return this.poolMaxAge;
    }

    /**
     * Sets the pool max age to use for this new RAR.
     * @param poolMaxAge The new pool max age to use for this RAR.
     */
    public void setPoolMaxAge(final int poolMaxAge) {
        this.poolMaxAge = poolMaxAge;
    }

    /**
     * The PSTMT Max value for this RAR.
     * @return the PSTMT max value used for this RAR.
     */
    public int getPstmtMax() {
        return this.pstmtMax;
    }

    /**
     * Sets the new PSTMT max value to use for this RAR.
     * @param pstmtMax the new PSTMT max value to use.
     */
    public void setPstmtMax(final int pstmtMax) {
        this.pstmtMax = pstmtMax;
    }

    /**
     * The PSTMT cache policy value for this RAR.
     * @return the PSTMT cache policy value used for this RAR.
     */
    public String getPstmtCachePolicy() {
        return this.pstmtCachePolicy;
    }

    /**
     * Sets the new PSTMT cache policy to use for this RAR.
     * @param pstmtCachePolicy the new PSTMT cache policy to use.
     */
    public void setPstmtCachePolicy(final String pstmtCachePolicy) {
        this.pstmtCachePolicy = pstmtCachePolicy;
    }


    /**
     * Returns the pool max open time value used for this RAR.
     * @return The pool max open time value used for this RAR.
     */
    public int getPoolMaxOpenTime() {
        return this.poolMaxOpenTime;
    }

    /**
     * Sets the pool max open time value used for this RAR.
     * @param poolMaxOpenTime The new pool max open time value to use.
     */
    public void setPoolMaxOpenTime(final int poolMaxOpenTime) {
        this.poolMaxOpenTime = poolMaxOpenTime;
    }

    /**
     * Returns the pool max waiters value used by this RAR.
     * @return The pool max waiters value used.
     */
    public int getPoolMaxWaiters() {
        return this.poolMaxWaiters;
    }

    /**
     * Sets the pool max waiters value used by this RAR.
     * @param poolMaxWaiters the new pool max waiters value to use.
     */
    public void setPoolMaxWaiters(final int poolMaxWaiters) {
        this.poolMaxWaiters = poolMaxWaiters;
    }

    /**
     * Get the pool max wait time value used by this RAR.
     * @return The pool max wait time value.
     */
    public int getPoolMaxWaittime() {
        return this.poolMaxWaittime;
    }

    /**
     * Sets the poool max wait time value used.
     * @param poolMaxWaittime The new Pool max wait time value to use.
     */
    public void setPoolMaxWaittime(final int poolMaxWaittime) {
        this.poolMaxWaittime = poolMaxWaittime;
    }

    /**
     * Get the pool sampling period value used for this RAR.
     * @return The pool sampling period used.
     */
    public int getPoolSamplingPeriod() {
        return this.poolSamplingPeriod;
    }

    /**
     * Sets the pool sampling period value used for this new RAR.
     * @param poolSamplingPeriod The new pool sampling period value to use.
     */
    public void setPoolSamplingPeriod(final int poolSamplingPeriod) {
        this.poolSamplingPeriod = poolSamplingPeriod;
    }

    /**
     * REturns the check level value for this new RAR.
     * @return the check level value.
     */
    public int getCheckLevel() {
        return this.checkLevel;
    }

    /**
     * Sets the new check level value to use for this RAR.
     * @param checkLevel the new check level value to use.
     */
    public void setCheckLevel(final int checkLevel) {
        this.checkLevel = checkLevel;
    }

    /**
     * Returns the DS Class used for this new RAR.
     * @return The DS class used for this new RAR.
     */
    public String getDsClass() {
        return this.dsClass;
    }

    /**
     * Sets the DS Class to use for this new RAR.
     * @param dsClass The new DS Class value to use for this RAR.
     */
    public void setDsClass(final String dsClass) {
        this.dsClass = dsClass;
    }

    /**
     * Returns the URL used in this RAR.
     * @return the URL value used in this RAR.
     */
    public String getURL() {
        return this.URL;
    }

    /**
     * Sets the new URL value to use for this RAR.
     * @param URL the new URL value to use for this RAR.
     */
    public void setURL(final String URL) {
        this.URL = URL;
    }

    /**
     * Returns the username used by this RAR.
     * @return the username used by this RAR.
     */
    public String getUser() {
        return this.user;
    }

    /**
     * Set a new username to use in this RAR.
     * @param user The new user name  value for this RAR.
     */
    public void setUser(final String user) {
        this.user = user;
    }

    /**
     * Returns the password for the user of this RAR.
     * @return the password for the user of this RAR.
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * Set a new password for the user of this RAR.
     * @param password the new password to use for this RAR.
     */
    public void setPassword(final String password) {
        this.password = password;
    }

    /**
     * Returns the login timeout for this RAR.
     * @return The login timeout for this RAR.
     */
    public String getLoginTimeout() {
        return this.loginTimeout;
    }

    /**
     * Sets a new login timeout value for this RAR.
     * @param loginTimeout the new login timeout value for this RAR.
     */
    public void setLoginTimeout(final String loginTimeout) {
        this.loginTimeout = loginTimeout;
    }

    /**
     * Returns the isolation level used by this RAR.
     * @return the isolation level used by this RAR.
     */
    public String getIsolationLevel() {
        return this.isolationLevel;
    }

    /**
     * Sets a new isolation level for this RAR to use.
     * @param isolationLevel the new isolation level to use for this RAR.
     */
    public void setIsolationLevel(final String isolationLevel) {
        this.isolationLevel = isolationLevel;
    }

    /**
     * Returns the mapper name for this RAR.
     * @return the mapper name for this RAR.
     */
    public String getMapperName() {
        return this.mapperName;
    }

    /**
     * Sets a new mapper name for this RAR to use.
     * @param mapperName the new mapper name for this RAR to use.
     */
    public void setMapperName(final String mapperName) {
        this.mapperName = mapperName;
    }

    /**
     * Returns the config log topic for this RAR.
     * @return the config log topic for this RAR.
     */
    public String getConfigLogTopic() {
        return this.configLogTopic;
    }

    /**
     * Sets a new config log topic for this RAR.
     * @param configLogTopic the new config log topic to use for this RAR.
     */
    public void setConfigLogTopic(final String configLogTopic) {
        this.configLogTopic = configLogTopic;
    }

    /**
     * Return if the action is a domain management operation.
     * @return if the action is a domain management operation.
     */
    public boolean getIsDomain() {
        return this.isDomain;
    }

    /**
     * Sets if the action is a domain management operation.
     * @param isDomain if the action is a domain management operation.
     */
    public void setIsDomain(final boolean isDomain) {
        this.isDomain = isDomain;
    }
    /**
     * @return Returns the booleanValues.
     */
    public List getBooleanValues() {
        return booleanValues;
    }
    /**
     * @param booleanValues The booleanValues to set.
     */
    public void setBooleanValues(final List booleanValues) {
        this.booleanValues = booleanValues;
    }
    /**
     * @return Returns the checkingLevels.
     */
    public List getCheckingLevels() {
        return checkingLevels;
    }
    /**
     * @param checkingLevels The checkingLevels to set.
     */
    public void setCheckingLevels(final List checkingLevels) {
        this.checkingLevels = checkingLevels;
    }
}
