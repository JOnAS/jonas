package org.ow2.jonas.webapp.jonasadmin.monitoring;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * Joram cluster monitoring action
 * @author Adriana.Danes@bull.net
 */
public class JoramClusterAction extends JonasBaseAction {

    public ActionForward executeAction(ActionMapping p_Mapping,
            ActionForm p_Form, HttpServletRequest p_Request,
            HttpServletResponse p_Response) throws IOException,
            ServletException {
        // Current server
        String serverName = m_WhereAreYou.getCurrentJonasServerName();
        String domainName = m_WhereAreYou.getCurrentDomainName();

        // Get cluster name from the 'clust' parameter
        String name = p_Request.getParameter("clust");
        if (name == null) {
            addGlobalError(new Exception("JoramClusterAction: clust parameter is null."));
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        // cluster type
        String type = "JoramCluster";
        // Form used
        JoramClusterForm oForm = (JoramClusterForm) p_Form;
        oForm.setName(name);
        try {
            ObjectName on = JonasObjectName.cluster(domainName, name, type);
            String state = (String) JonasManagementRepr.getAttribute(on, "State", serverName);
            oForm.setState(state);

        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }

        // Forward to the jsp.
        return (p_Mapping.findForward("JoramCluster"));
    }
}
