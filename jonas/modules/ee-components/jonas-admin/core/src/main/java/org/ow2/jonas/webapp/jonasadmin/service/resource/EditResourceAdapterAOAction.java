/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.resource;

import java.io.IOException;
import java.util.Properties;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Michel-Ange ANTON
 */

public class EditResourceAdapterAOAction extends JonasBaseAction {

// --------------------------------------------------------- Public Methods

    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws IOException, ServletException {

        // Selected resource adapter
        String sObjectName = p_Request.getParameter("select");

        ObjectName oObjectName = null;
        String domainName = null;
        String serverName = null;

        // Form used
        ResourceAdapterAOForm oForm = null;
        // Build a new form
        if (sObjectName != null) {
            try {
                // Recreate ObjectName
                oObjectName = ObjectName.getInstance(sObjectName);
                domainName = oObjectName.getDomain();
                serverName = oObjectName.getKeyProperty("J2EEServer");
                // Build a new form
                oForm = new ResourceAdapterAOForm();
                oForm.reset(p_Mapping, p_Request);
                if (oObjectName != null) {
                    // Object name used
                    oForm.setOName(oObjectName);
                    oForm.setName(getStringAttribute(oObjectName, "jndiName"));
                    oForm.setDescription(getStringAttribute(oObjectName, "description"));
                    oForm.setListProperties((Properties) JonasManagementRepr.getAttribute(oObjectName
                            , "properties", serverName));
                }
                m_Session.setAttribute("resourceAdapterAOForm", oForm);
            } catch (Throwable t) {
                addGlobalError(t);
                saveErrors(p_Request, m_Errors);
                return (p_Mapping.findForward("Global Error"));
            }
        } else {
            // Used last form in session
            oForm = (ResourceAdapterAOForm) m_Session.getAttribute("resourceAdapterAOForm");
        }

        ResourceAdapterForm raForm = (ResourceAdapterForm) m_Session.getAttribute("resourceAdapterForm");

        // Force the node selected in tree
        m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_SERVER) + WhereAreYou.NODE_SEPARATOR
            + "services" + WhereAreYou.NODE_SEPARATOR + "resourceAdapter"
            + WhereAreYou.NODE_SEPARATOR + raForm.getFile(), true);

        // Forward to the jsp.
        return (p_Mapping.findForward("Resource AdapterAO"));
    }
}
