/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Michel-Ange ANTON
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;
import javax.servlet.jsp.tagext.Tag;

public class PanelTag extends BodyTagSupport {

// ----------------------------------------------------- Instance Variables

    /**
     * The databody that will be rendered for this table row.
     */
    private String body = null;

// --------------------------------------------------------- Public Methods

    /**
     * Process the start of this tag.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doStartTag()
        throws JspException {
        // Initialize the holder for our databody text
        this.body = null;
        // Do no further processing for now
        return (EVAL_BODY_BUFFERED);
    }

    /**
     * Process the body text of this tag (if any).
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doAfterBody()
        throws JspException {
        String data = bodyContent.getString();
        if (data != null) {
            data = data.trim();
            if (data.length() > 0) {
                this.body = data;
            }
        }
        return (SKIP_BODY);
    }

    /**
     * .
     *
     * @exception JspException if a processing error occurs
     */
    public int doEndTag()
        throws JspException {
        // Find our parent TabsTag instance
        Tag parent = findAncestorWithClass(this, TabsTag.class);
        if ((parent == null) || !(parent instanceof TabsTag)) {
            throw new JspException("Must be nested in a TabsTag instance");
        }
        TabsTag oTabsTag = (TabsTag) parent;

        // Register the information for the action represented by
        // this action
        //HttpServletResponse response = (HttpServletResponse) pageContext.getResponse();
        oTabsTag.setBody(body);

        return (EVAL_PAGE);
    }

    /**
     * Release all state information set by this tag.
     */
    public void release() {
        this.body = null;
    }

}
