/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.cmi;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @author eyindanga
 *
 */
public class Provider implements Serializable {
    /**
     * Serial Id.
     */
    private static final long serialVersionUID = 1L;
    /**
     * The provider url.
     */
    String url;

    /**
     * The server name.
     */
    String name;

    /**
     * Object contained in the registry.
     */
    Set<String> listNames = new HashSet<String>();

    private boolean isBlackListed;


    /**Gets Provider url.
     * @return
     */
    public String getUrl() {
        return url;
    }
    /**
     * Sets the provider url.
     */
    public void setUrl(final String url) {
        this.url = url;
    }
    /**
     * Gets the names of the objects contained in the registry.
     */
    public Set<String> getListNames() {
        return listNames;
    }
    /**
     * Sets the names of the objects contained in the registry.
     */
    public void setListNames(final Set<String> listNames) {
        this.listNames = listNames;
    }
    public String getName() {
        return name;
    }
    public void setName(final String name) {
        this.name = name;
    }
    public boolean getIsBlackListed() {
        return isBlackListed;
    }
    public void setIsBlackListed(final boolean isBlacklisted) {
        this.isBlackListed = isBlacklisted;
    }
}
