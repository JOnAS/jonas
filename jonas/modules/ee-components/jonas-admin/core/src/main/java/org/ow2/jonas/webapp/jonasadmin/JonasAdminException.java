/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin;

import org.ow2.jonas.lib.management.extensions.base.AdminException;

/**
 * JOnAS Administration Application Exception.
 * Generic exception for all troubles of the JOnAS Administration Application.
 *
 * @author Michel-Ange ANTON
 */

public class JonasAdminException extends AdminException {
// --------------------------------------------------------- Protected variables

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /**
     *  Error ID
     */
    protected String m_Id = null;

// --------------------------------------------------------- Public Methods

    public JonasAdminException(final String p_Id, final String p_Message, final Throwable p_Throwable) {
        super(p_Id, p_Message, p_Throwable);
    }

    public JonasAdminException(final String p_Id, final String p_Message) {
        super(p_Id, p_Message);
    }

    public JonasAdminException(final String p_Id) {
        super(p_Id);
    }

    /**
     * Accessor to the error ID.
     * @return String The error ID
     */
    @Override
    public String getId() {
        return m_Id;
    }

}