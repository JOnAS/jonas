package org.ow2.jonas.webapp.jonasadmin.jonasmqconnect.util;

import javax.management.ObjectName;


public class ItemDestination implements java.io.Serializable {
    private String name;
    private String type;
    private String baseName;
    private ObjectName mbeanName;
    private String connector;

    public ItemDestination(String name, String type, String baseName,ObjectName objectName) {
        this.name = name;
        this.type = type;
        this.baseName = baseName;
        this.mbeanName = objectName;
        this.connector = objectName.getKeyProperty("JonasMQConnector");
    }

    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name to set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return Returns the type.
     */
    public String getType() {
        return type;
    }

    /**
     * @param type The type to set.
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return Returns the baseName.
     */
    public String getBaseName() {
        return baseName;
    }

    /**
     * @param baseName The baseName to set.
     */
    public void setBaseName(String baseName) {
        this.baseName = baseName;
    }


    /**
     * @return Returns the baseName.
     */
    public ObjectName getMBeanName() {
        return mbeanName;
    }

    /**
     * @return Returns the connector name.
     */
    public String getConnector() {
        return connector;
    }

}