/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.cmi;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author eyindanga.
 *
 */
public class Protocol implements Serializable {
    /**
     * Serial Id.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Protocol name.
     */
    private String name;
    /**
     * Protocol icon.
     */
    private String icon = null;
    /**
     * Protocol provider urls. The master view should show all providers for a given protocol
     */
    private ArrayList<Provider> providers = new ArrayList<Provider>();
    /**
     * ObjectName of the protocol.
     */
    private String objectName = null;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    /**
     *
     * @return
     */
    public String getIcon() {
        return icon;
    }
    /**
     * @param icon
     */
    public void setIcon(String icon) {
        this.icon = icon;
    }
    /**
     * @return The providers for the given url.
     */
    public ArrayList<Provider> getProviders() {
        return providers;
    }
    /**
     * Set the providers.
     * @param providers
     */
    public void setProviders(ArrayList<Provider> providers) {
        this.providers = providers;
    }
    public String getObjectName() {
        return objectName;
    }
    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }
}
