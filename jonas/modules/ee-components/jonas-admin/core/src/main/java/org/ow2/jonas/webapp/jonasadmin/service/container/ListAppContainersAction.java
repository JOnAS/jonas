/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service.container;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.management.javaee.J2eeObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasAdminJmx;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;
import org.ow2.jonas.webapp.jonasadmin.WhereAreYou;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Michel-Ange ANTON
 */

public class ListAppContainersAction extends JonasBaseAction {

// --------------------------------------------------------- Public Methods

    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws IOException, ServletException {

        // Force the node selected in tree
        m_WhereAreYou.selectNameNode(getTreeBranchName(DEPTH_SERVER) + WhereAreYou.NODE_SEPARATOR
            + "services" + WhereAreYou.NODE_SEPARATOR + "ear", true);

        // Force the deployment type to use the refreshing method
        m_WhereAreYou.setCurrentJonasDeploymentType(WhereAreYou.DEPLOYMENT_EAR);
        
        String s_refresh = (String) p_Request.getParameter("refresh");
        boolean refresh = Boolean.valueOf(s_refresh).booleanValue();
        
        // no Form used
        try {
            // Refresh in case deploy/undeploy operation arrived.
            if (refresh) {
                refreshServicesTree(p_Request);
            }
            
            // Get container list
            String sPath;
            String sFile;
            String sName;
            ArrayList al = new ArrayList();
            String p_DomainName = m_WhereAreYou.getCurrentDomainName();
            String p_ServerName = m_WhereAreYou.getCurrentJonasServerName();
            ObjectName onContainers = J2eeObjectName.J2EEApplications(p_DomainName, p_ServerName);
            Iterator itNames = JonasManagementRepr.queryNames(onContainers, p_ServerName).iterator();
            while (itNames.hasNext()) {
                ObjectName it_on = (ObjectName) itNames.next();
                sPath = ((java.net.URL) JonasManagementRepr.getAttribute(it_on, "earUrl",p_ServerName )).toString();
                //sPath = itNames.next().toString();
                sFile = JonasAdminJmx.extractFilename(sPath);
                sName = it_on.getKeyProperty("name");
                al.add(new ContainerItem(sFile, sPath, it_on.toString(), sName));
            }
            Collections.sort(al, new ContainerItemByFile());
            // Set list in the request
            p_Request.setAttribute("listAppContainers", al);
        }
        catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        // Forward to the jsp.
        return (p_Mapping.findForward("App Containers"));
    }
}
