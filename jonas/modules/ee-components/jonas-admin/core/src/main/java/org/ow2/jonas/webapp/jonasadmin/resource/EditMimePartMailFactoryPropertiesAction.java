/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.resource;

import java.io.IOException;
import java.util.Properties;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.util.JonasObjectName;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;



/**
 *
 */
public class EditMimePartMailFactoryPropertiesAction extends EditMailFactoryPropertiesAction {

// --------------------------------------------------------- Public Methods

    /**
     */
    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
        , HttpServletRequest p_Request, HttpServletResponse p_Response)
        throws IOException, ServletException {

        // Current server
        String serverName = m_WhereAreYou.getCurrentJonasServerName();
        String domainName = m_WhereAreYou.getCurrentDomainName();

        // Mail factory to edit
        String sName = p_Request.getParameter("name");

        // Form used
        MailFactoryPropertiesForm oForm = (MailFactoryPropertiesForm) m_Session.getAttribute("mailFactoryPropertiesForm");
        if (oForm == null) {
            // Create form
            oForm = new MailFactoryPropertiesForm();
            m_Session.setAttribute("mailFactoryPropertiesForm", oForm);
        }

        try {
            ObjectName mailServMBean = JonasObjectName.mailService(domainName);
            String[] asParam = new String[1];
            String[] asSignature = new String[1];
            asParam[0] = sName;
            asSignature[0] = "java.lang.String";
            Properties oProps = (Properties) JonasManagementRepr.invoke(mailServMBean, "getMailFactoryPropertiesFile", asParam, asSignature, serverName);
            populate(sName, oProps, oForm);
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }

        // Forward to the jsp.
        return (p_Mapping.findForward("MimePart Properties"));
    }

}
