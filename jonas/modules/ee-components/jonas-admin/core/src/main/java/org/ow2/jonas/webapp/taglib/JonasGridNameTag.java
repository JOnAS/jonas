/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Michel-Ange ANTON
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.taglib;

import javax.servlet.jsp.JspException;

public class JonasGridNameTag extends GridColTag {

// ----------------------------------------------------- Properties

    private boolean bold = true;

    public boolean isBold() {
        return bold;
    }

    public void setBold(boolean bold) {
        this.bold = bold;
    }

// ----------------------------------------------------- Public Methods

    /**
     * Start of Tag processing
     *
     * @exception JspException if a JSP exception occurs
     */
    public int doStartTag()
        throws JspException {
        setNowrap(true);
        setAlign("right");
        setValign("top");
        setWidth("19%");
        return super.doStartTag();
    }

    public void release() {
        super.release();
    }

// ----------------------------------------------------- Protected Methods

    protected String prepareAfterTag() {
        StringBuffer sb = new StringBuffer("<td width=\"1%\"");
        if (getStyleClass() != null) {
            sb.append(" class=\"");
            sb.append(getStyleClass());
            sb.append("\"");
        }
        sb.append(">&nbsp;</td>");
        return sb.toString();
    }

    protected String prepareBeforeBody() {
        if (bold == true) {
            return "<b>";
        }
        return "";
    }

    protected String prepareAfterBody() {
        if (bold == true) {
            return "</b>";
        }
        return "";
    }
}