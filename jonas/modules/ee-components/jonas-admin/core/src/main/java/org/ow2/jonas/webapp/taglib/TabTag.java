/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.taglib;

import java.net.MalformedURLException;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;
import javax.servlet.jsp.tagext.Tag;

import org.apache.struts.taglib.TagUtils;


/**
 * @author Michel-Ange ANTON
 */
public class TabTag extends BodyTagSupport {

// ----------------------------------------------------- Instance Variables
    private String m_Body = null;
    private String m_Url = null;

// ----------------------------------------------------- Properties

    private boolean selected = false;
    private String href = null;
    private String forward = null;
    private String forwardControl = null;
    /**
     * Accessor Href property.
     */
    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    /**
     * Accessor Selected property.
     */
    public boolean getSelected() {
        return (this.selected);
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    /**
     * Accessor Forward property.
     */
    public String getForward() {
        return forward;
    }

    public void setForward(String forward) {
        this.forward = forward;
    }

    /**
     * Accessor ForwardControl property.
     */
    public String getForwardControl() {
        return forwardControl;
    }

    public void setForwardControl(String forwardControl) {
        this.forwardControl = forwardControl;
    }

    /**
     * The anchor to be added to the end of the generated hyperlink.
     */
    protected String anchor = null;

    public String getAnchor() {
        return (this.anchor);
    }

    public void setAnchor(String anchor) {
        this.anchor = anchor;
    }

    /**
     * The link name for named links.
     */
    protected String linkName = null;

    public String getLinkName() {
        return (this.linkName);
    }

    public void setLinkName(String linkName) {
        this.linkName = linkName;
    }

    /**
     * The JSP bean name for query parameters.
     */
    protected String name = null;

    public String getName() {
        return (this.name);
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * The context-relative page URL (beginning with a slash) to which
     * this hyperlink will be rendered.
     */
    protected String page = null;

    public String getPage() {
        return (this.page);
    }

    public void setPage(String page) {
        this.page = page;
    }

    /**
     * The single-parameter request parameter name to generate.
     */
    protected String paramId = null;

    public String getParamId() {
        return (this.paramId);
    }

    public void setParamId(String paramId) {
        this.paramId = paramId;
    }

    /**
     * The single-parameter JSP bean name.
     */
    protected String paramName = null;

    public String getParamName() {
        return (this.paramName);
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    /**
     * The single-parameter JSP bean property.
     */
    protected String paramProperty = null;

    public String getParamProperty() {
        return (this.paramProperty);
    }

    public void setParamProperty(String paramProperty) {
        this.paramProperty = paramProperty;
    }

    /**
     * The single-parameter JSP bean scope.
     */
    protected String paramScope = null;

    public String getParamScope() {
        return (this.paramScope);
    }

    public void setParamScope(String paramScope) {
        this.paramScope = paramScope;
    }

    /**
     * The JSP bean property name for query parameters.
     */
    protected String property = null;

    public String getProperty() {
        return (this.property);
    }

    public void setProperty(String property) {
        this.property = property;
    }

    /**
     * The scope of the bean specified by the name property, if any.
     */
    protected String scope = null;

    public String getScope() {
        return (this.scope);
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    /**
     * The window target.
     */
    protected String target = null;

    public String getTarget() {
        return (this.target);
    }

    public void setTarget(String target) {
        this.target = target;
    }

    /**
     * Include transaction token (if any) in the hyperlink?
     */
    protected boolean transaction = false;

    public boolean getTransaction() {
        return (this.transaction);
    }

    public void setTransaction(boolean transaction) {
        this.transaction = transaction;
    }

// --------------------------------------------------------- Public Methods

    public int doStartTag()
        throws JspException {
        // Initialize the older body
        m_Body = null;

        // Do no further processing for now
        return (EVAL_BODY_BUFFERED);
    }

    public int doAfterBody()
        throws JspException {
        String sBody = bodyContent.getString();
        if (sBody != null) {
            sBody = sBody.trim();
            if (sBody.length() > 0) {
                this.m_Body = sBody;
            }
        }
        return (SKIP_BODY);
    }

    /**
     * Record this tab with our surrounding TabsTag instance.
     *
     * @exception JspException if a processing error occurs
     */
    public int doEndTag()
        throws JspException {

        // Find our parent TabsTag instance
        Tag parent = findAncestorWithClass(this, TabsTag.class);
        if ((parent == null) || !(parent instanceof TabsTag)) {
            throw new JspException("Must be nested in a TabsTag instance");
        }
        TabsTag oTabs = (TabsTag) parent;

        // Generate the hyperlink URL
		TagUtils tagUtils = TagUtils.getInstance();
        Map oParams = tagUtils.computeParameters(pageContext, paramId, paramName, paramProperty
            , paramScope, name, property, scope, transaction);
        m_Url = null;
        try {
            m_Url = tagUtils.computeURL(pageContext, forward, href, page, null, null, oParams, anchor, false);
         } catch (MalformedURLException e) {
            // none
        }
        // Verify forwardControl parameter
        if ((m_Url == null) && (forwardControl != null) && (oTabs.isUsingWhere() == true)) {
            try {
                // Get the selected node
                TreeControlNode oNode = oTabs.getSelectedTreeControlNode();
                if (oNode != null) {
                    try {
                        String sForward = tagUtils.computeURL(pageContext, forwardControl, null, null, null, null, null, null, false);
                        int iPos = oNode.getAction().indexOf("?");
                        m_Url = sForward + oNode.getAction().substring(iPos);
                    } catch (MalformedURLException e) {
                        // none
                    }
                }
            } catch (Exception ex) {
                // none
            }
        }
        // Verify Url
        if (m_Url == null) {
            m_Url = new String("");
        }
        // Verify body label
        if (m_Body == null) {
            m_Body = new String("");
        }
        // Register the information for the action represented by
        // this action
        oTabs.addTab(m_Body, m_Url, selected);

        return (EVAL_PAGE);
    }

    /**
     * Release all state information set by this tag.
     */
    public void release() {
        this.m_Body = null;
        this.href = null;
        this.forward = null;
        this.forwardControl = null;
    }
}
