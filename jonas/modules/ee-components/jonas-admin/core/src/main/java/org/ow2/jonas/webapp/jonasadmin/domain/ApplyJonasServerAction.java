/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2005 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.domain;

import java.io.IOException;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ow2.jonas.lib.management.extensions.base.JonasManagementRepr;
import org.ow2.jonas.lib.util.JonasObjectName;
import org.ow2.jonas.webapp.jonasadmin.JonasBaseAction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * This action is called when confirming a server add the domain.
 * @author Adriana Danes
 * @author S. Ali Tokmen
 */
public class ApplyJonasServerAction extends JonasBaseAction {

//	--------------------------------------------------------- Public Methods
    public ActionForward executeAction(ActionMapping p_Mapping, ActionForm p_Form
            , HttpServletRequest p_Request, HttpServletResponse p_Response)
    throws IOException, ServletException {
        String currentServerName = m_WhereAreYou.getCurrentJonasServerName();
        String domainName = m_WhereAreYou.getCurrentDomainName();
        // Form used
        NewServerForm oForm = (NewServerForm) p_Form;
        // Server to add
        String serverName = null;
        String serverURL = null;
        String clusterDaemon = null;
        String username = null;
        String password = null;
        String[] selecetedServerNames = null;
        //
        boolean isCluster = oForm.isCluster();
        if (isCluster) {
            selecetedServerNames = oForm.getSelectedItems();
        } else {
            // Server to add - name, connection URL, cluster daemon
            serverName = oForm.getServerName();
            serverURL = oForm.getServerURL();
            clusterDaemon = oForm.getServerCld();
            if (clusterDaemon == null || clusterDaemon.length() < 1) {
                clusterDaemon = null;
            }
            username = oForm.getServerUsername();
            password = oForm.getServerPassword();
            if (username == null || password == null || username.length() < 1 || password.length() < 1) {
                username = null;
                password = null;
            }
        }
        // Reset form
        oForm.reset(p_Mapping, p_Request);

        String clusterName = (String) m_Session.getAttribute("currentCluster");
        String clusterType = "LogicalCluster";
        try {
            ObjectName on = JonasObjectName.cluster(domainName, clusterName, clusterType);
            String[] signature = {"java.lang.String", "[Ljava.lang.String;", "java.lang.String", "java.lang.String", "java.lang.String"};
            Object[] params = new Object[5];
            if (serverName != null) {
                params[0] = serverName;
                String[] urls = {serverURL};
                params[1] = urls;
                params[2] = clusterDaemon;
                params[3] = username;
                params[4] = password;
                JonasManagementRepr.invoke(on, "addServer", params, signature, currentServerName);
            } else {
                params[1] = null;
                params[2] = null;
                params[3] = null;
                params[4] = null;
                for (int i = 0; i < selecetedServerNames.length; i++) {
                    params[0] = selecetedServerNames[i];
                    JonasManagementRepr.invoke(on, "addServer", params, signature, currentServerName);
                }
            }
            m_Session.setAttribute("mapChanged", new Boolean(true));
        } catch (Throwable t) {
            addGlobalError(t);
            saveErrors(p_Request, m_Errors);
            return (p_Mapping.findForward("Global Error"));
        }
        // Forward to the connector display page or the list if create
        return p_Mapping.findForward("ActionEditDomain");
    }
}