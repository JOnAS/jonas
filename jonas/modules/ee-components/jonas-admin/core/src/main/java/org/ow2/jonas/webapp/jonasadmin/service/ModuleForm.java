/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.webapp.jonasadmin.service;

import java.net.URL;

import javax.servlet.http.HttpServletRequest;

import org.ow2.jonas.webapp.jonasadmin.JonasAdminJmx;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;


/**
 * @author Adriana Danes
 */
public class ModuleForm extends ActionForm {

// --------------------------------------------------------- Properties variables
    /**
     * True if the module is included in a EAR
     */
    private boolean inEar = false;
    /**
     * File name of the EAR containing the module
     */
    private String earFilename = null;
    /**
     * Path of the EAR file containing the module
     */
    private String earPath = null;
    /**
     * J2EEApplication MBean OBJECT_NAME
     */
    private String earON = null;
// --------------------------------------------------------- Public Methods

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors oErrors = new ActionErrors();
        return oErrors;
    }

// --------------------------------------------------------- Properties Methods

    public String getEarFilename() {
        return earFilename;
    }

    public String getEarPath() {
        return earPath;
    }

    public void setEarPath(URL p_Url) {
        this.earPath = null;
        if (p_Url != null) {
            this.earPath = p_Url.getPath();
            this.earFilename = JonasAdminJmx.extractFilename(this.earPath);
        }
    }

    public boolean isInEar() {
        return inEar;
    }

    public void setInEar(boolean inEar) {
        this.inEar = inEar;
    }

    public String getEarON() {
        return earON;
    }

    public void setEarON(String earON) {
        this.earON = earON;
    }


}