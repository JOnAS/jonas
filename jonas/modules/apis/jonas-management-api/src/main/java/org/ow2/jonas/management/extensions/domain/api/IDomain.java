/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.management.extensions.domain.api;

import org.ow2.jonas.management.extensions.base.api.IBaseManagement;

/**
 * This interface provides a description of management operations
 * provided for domain management in a master server.
 * @author Adriana Danes
 * @author Oualaa Hani
 */
public interface IDomain extends IBaseManagement {

    /**
     * Get the name of the server providing the ManagementEntryEndpoint.
     * @return the local server name.
     */
    String getServerName();
    /**
     * Get the domain name.
     * @return the domain name.
     */
    String getDomainName();

    /**
     * @return the current server's host.
     */
    String getServerHost();

    /**
     * @return the current server's port if the web service activated.
     */
    String getServerPort();

    /**
     * Return True if the managed server is master.
     * @param serverName managed server name
     */
    boolean isMaster(final String serverName);

    /**
     * Return True if the current server is a master.
     */
    boolean isMaster();

    /**
     * Return the type of a cluster in the current domain.
     * @param clusterName the cluster's name
     */
    String getClusterType(final String clusterName);

    /**
     * @return the servers name that are belonging to the domain.
     */
    String[] getServerNames();

    /**
     * @return the server names that are belonging to a cluster.
     * @param clusterName the cluster name
     */
    String[] getServerNames(final String clusterName);

    /**
     * @return the clusters objectname in the domain.
     */
    String[] getClusters();

    /**
     * @return the clusterdaemons objectname in the domain.
     */
    String[] getclusterDaemons();

    /**
     * @return the state of a server in the domain
     * @param serverName the server's name
     */
    String getServerState(final String serverName);

    /**
     * Return the cluster daemon of a server in the domain.
     * @param serverName the server's name
     */
    String getServerClusterdaemon(final String serverName);

    /**
     * @return the state of a cluster in the domain.
     * @param clusterName the cluster's name
     */
    String getClusterState(final String clusterName);

    /**
     * @return the state of a cluster daemon in the domain.
     * @param clusterdaemonName the cluster daemon's name.
     */
    String getClusterdaemonState(final String clusterDaemonName);

    /**
     * @return name of clusters in the domain.
     */
    String[] getClustersNames();

    /**
     * @return name of clustersDaemon in the domain.
     */
    String[] getClusterDaemonNames();

    /**
     * Add server in domain.
     * @param serverName.
     * @param serverURL the URL is a JMX URL.
     * @param userName this parameter is optional.
     * @param password this parameter is optional.
     * @param clusterDaemon this parameter means if the server takes part of a clusterdaemon. It's optional.
     */
    void addServer(final String serverName, final String serverURL, final String userName, final String password,final String clusterDaemon);

    /**
     * Remove server from domain.
     * @param serversToRemove List of servers to remove.
     */
    void removeServers (final String[] serversToRemove);

    /**
     * Start server.
     * @param serverName.
     * @param standby <code>true</code> to activate standby mode.
     */
    void startServer(final String serverName, boolean standby);

    /**
     * Stop server.
     * @param serverName server's name.
     * @param standby <code>true</code> to activate standby mode.
     */
    void stopServer(final String serverName, boolean standby);

    /**
     * Create logical cluster.
     * @param clusterName
     */
    void addCluster(final String clusterName);

    /**
     * Get the name of the servers in the domain except the ones that are belonging to this cluster.
     * @param clusterName the cluster name
     * @return name of the servers that can be attached to the cluster
     */
    String[] getServersNotInCluster(final String clusterName);

}
