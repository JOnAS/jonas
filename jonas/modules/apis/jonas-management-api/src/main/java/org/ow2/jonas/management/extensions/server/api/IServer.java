/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.management.extensions.server.api;

/**
 * This interface provides a description of management operations provided for a
 * server instance management in a domain.
 * @author Adriana Danes
 * @author Oualaa Hani
 * @author Thomas kouassi
 */
public interface IServer {

    // Global
    /**
     * Get the memory used by a server in the domain.
     * @param pServer the server's name.
     * @return the used memory
     */
    long getCurrentUsedMemory(final String pServer);

    /**
     * Get the number of threads used by a server in the domain.
     * @param pServer the server's name.
     * @return the number of threads
     */
    int getAllThreadsCount(final String pServer);

    /**
     * Get the total memory of the server in the domain.
     * @param pServer the server's name.
     * @return the total memory
     */
    Long getCurrentTotalMemory(final String pServer);

    /**
     * Get the JVM vendor.
     * @param pServer the server's name.
     * @return the JVM vendor
     */
    String getJavaVendor(final String pServer);

    /**
     * Get the Java home
     * @param server the server's name.
     * @return the javaHome
     */
    String getJavaHome(final String serverName);

    /**
     * Get the java version.
     * @param pServer the server's name.
     * @return the java version
     */
    String getJavaVersion(final String pServer);

    /**
     * Get the version of a server in the domain.
     * @param pServer the server's name.
     * @return the server's version
     */
    String getServerVersion(final String pServer);

    /**
     * Get the the protocols used bye the server.
     * @param pServer the server's name.
     * @return the protocols used bye the server
     */
    String getProtocols(final String pServer);

    /**
     * Get the Jonas Base used by the server
     * @param pServer the server's name
     * @return the jonas Base sed bye the server
     */
    String getJonasBase(final String pServerName);

    /**
     * Get the Jonas Root used by the server
     * @param pServer the server's name
     * @return the Jonas Root used bye the server
     */
    String getJonasRoot(final String pServerName);

    /**
     * Get the Xprem used by the server
     * @param pServer the server's name
     * @return the Xprem used bye the server
     */
    String getXprem(final String pServerName);

    /**
     * Get the Auto boot used by the server
     * @param pServer the server's name
     * @return the Auto boot used bye the server
     */
    String getAutoBoot(final String pServerName);

    /**
     * Get the Description
     * @param pServer the server's name
     * @return the server Description
     */
    String getDescription(final String pServerName);

    /**
     * Get the JMX connection URL of the server.
     * @param pServer the server's name.
     * @return the JMX connection URL of the server
     */
    String getConnectionUrl(final String pServer);

    // Transactions

    /**
     * @return total begun transactions.
     * @param pServer Server name
     */
    int getTotalBegunTransactions(final String pServer);

    /**
     * @return total commited transactions.
     * @param pServer Server name
     */
    int getTotalCommittedTransactions(final String pServer);

    /**
     * @return total current transactions.
     * @param pServer Server name
     */
    int getTotalCurrentTransactions(final String pServer);

    /**
     * @return total expired transactions.
     * @param pServer Server name
     */
    int getTotalExpiredTransactions(final String pServer);

    /**
     * @return total global transactions.
     * @param pServer Server name
     */
    int getTotalRolledbackTransactions(final String pServer);

    // Tomcat

    /**
     * @return the max threads by connector tomcat.
     * @param pServer Server name
     */
    int getMaxThreadsByConnectorTomcat(final String server);

    /**
     * @return current threads count by tomcat connector
     * @param pServer Server name
     */
    int getCurrentThreadCountByConnectorTomcat(final String server);

    /**
     * @param server Server name.
     * @return host name
     */
     String getHostName(final String serverName);

    /**
     * @return current threads busy by connector tomcat.
     * @param pServer Server name
     */
    int getCurrentThreadBusyByConnectorTomcat(final String server);

    /**
     * @return bytes received by connector tomcat.
     * @param pServer Server name
     */
    long getBytesReceivedByConnectorTomcat(final String server);

    /**
     * @return bytes sent by connector tomcat.
     * @param pServer Server name
     */
    long getBytesSentByConnectorTomcat(final String server);

    /**
     * @return error count by connector tomcat.
     * @param pServer Server name
     */
    int getErrorCountByConnectorTomcat(final String server);

    /**
     * @return processing time by connector tomcat.
     * @param pServer Server name
     */
    long getProcessingTimeByConnectorTomcat(final String server);

    /**
     * @return request count by connector tomcat.
     * @param pServer Server name
     */
    int getRequestCountByConnectorTomcat(final String server);

    // EJBs

    /**
     * @return current number of EJB.
     * @param pServer Server name
     */
    int getCurrentNumberOfEJB(final String server);

    /**
     * @return current number of Entity bean.
     * @param pServer Server name
     */
    int getCurrentNumberOfEntityBean(final String server);

    /**
     * @return current number of state full bean.
     * @param pServer Server name
     */
    int getCurrentNumberOfSBF(final String server);

    /**
     * @return current number of state less bean.
     * @param pServer Server name
     */
    int getCurrentNumberOfSBL(final String server);

    /**
     * @return current number of mbean.
     * @param pServer Server name
     */
    int getCurrentNumberOfMDB(final String server);

}
