/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.management.extensions.container.api;

import java.util.List;
import java.util.Map;

import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;

/**
 * Interface for containers management.
 * @author eyindanga
 *
 */
public interface IContainerManagement {
    /**
     * Gets ObjectName for J2EE Module.
     * @param ejbObjectName
     * @return
     * @throws MalformedObjectNameException
     */
    ObjectName getEJBModuleObjectName(final String ejbObjectName) throws MalformedObjectNameException;

    /**
     * Gets all the EntityBean MBeans in a module which is deployed in a given server
     * @param pObjectName pObjectName
     * @param serverName name of the administrated server.
     * @return List of entity beans.
     */
    List<?> getEntityBeans(final ObjectName pObjectName, final String serverName);

    /**
     * Gets all the Stateful Session MBeans in a module which is deployed in a given server
     * @param pObjectName pObjectName
     * @param serverName name of the administrated server.
     * @return List of entity beans.
     */
    List<?> getStatefulSessionBeans(final ObjectName pObjectName, final String serverName);

    /**
     * Gets all the Stateless Session MBeans in a module which is deployed in a given server
     * @param pObjectName pObjectName
     * @param serverName name of the administrated server.
     * @return List of entity beans.
     */
    List<?> getStatelessSessionBeans(final ObjectName pObjectName, final String serverName);

    /**
     * Gets all the Message Driven MBeans in a module which is deployed in a given server
     * @param pObjectName pObjectName
     * @param serverName name of the administrated server.
     * @return List of entity beans.
     */
    List<?> getMessageDrivenBeans(final ObjectName pObjectName, final String serverName);

    /**
     * Get the total number of EJBs in a EJBModule
     * @param moduleOn the EJBModule's ObjectName
     * @return map with the number of EJBs of different types where the key is the JSR77 type name
     */
    public Map getTotalEJB(final ObjectName moduleOn);
}
