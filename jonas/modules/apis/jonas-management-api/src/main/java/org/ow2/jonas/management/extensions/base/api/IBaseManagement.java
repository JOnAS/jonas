/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.management.extensions.base.api;

import java.util.List;

import javax.management.MBeanServerConnection;
import javax.management.ObjectName;

public interface IBaseManagement {

    /**
     * @return The current server name.
     */
    String getServerName();

    /**
     * @return The current domain name.
     */
    String getDomainName();

    /**
     * Get the String attribute value of an MBean in the current MBean Server.
     * @param objectName The MBean's ObjectName
     * @param attrName The attribute name
     * @return The value of the attribute
     */
    String getStringAttribute(final ObjectName objectName, final String attrName) throws ManagementException;

    /**
     * Get the integer attribute value of an MBean in the current MBean Server.
     * @param objectName The MBean's ObjectName
     * @param attrName The attribute name
     * @return The value of the attribute
     */
    int getIntegerAttribute(final ObjectName objectName, final String attrName) throws ManagementException;

    /**
     * Return the value of a key property in an OBJECT_NAME.
     * @param objectName the OBJECT_NAME (String form)
     * @param keyName key property name
     * @return key value
     */
    String getKeyValue(final String objectName, final String keyName) throws ManagementException;

    /**
     * Return the values of a key property in String OBJECT_NAMEs.
     * @param objectNames the OBJECT_NAMEs
     * @param keyName key name
     * @return array of key values
     */
    String[] getKeyValues(final String[] objectNames, final String keyName) throws ManagementException;

    /**
     * Get management attributes.
     * @param objectName
     * @param serverName
     * @return attributes for the given objectName.
     */
    J2EEMBeanAttributeInfo[] getAttributes(final ObjectName objectName, final String serverName) throws ManagementException;

    /**
     * Gets the value of a specific attribute of a named MBean.
     * @param on The ObjectName of the MBean.
     * @param attribute A String specifying the name of the attribute to be
     *        retrieved.
     * @param serverName The server name
     * @return The value of the attribute.
     * @throws ManagementException management operation failed
     */
    Object getAttribute(final ObjectName on, final String attribute, final String serverName) throws ManagementException;

    /**
     * Implementation of the <code>isRegistered</code> method to be applied to a
     * server in the domain.
     * @return True if the MBean is already registered in the MBean server,
     *         false otherwise or if an exception is catched.
     * @param on ObjectName of the MBean we are looking for
     * @param serverName The server name
     * @throws ManagementException management operation failed
     */
    boolean isRegistered(final ObjectName on, final String serverName) throws ManagementException;

    /**
     * Implementation of the <code>invoke</code> method to be applied to a
     * server in the domain.
     * @param on the ObjectName of the MBean that is the target of the invoke.
     * @param operation operation to invoke
     * @param param invoke parameters
     * @param signature invoke parameters signature
     * @param serverName The server's name
     * @return The object returned by the operation
     * @throws ManagementException management operation failed
     */
    Object invoke(final ObjectName on, final String operation, final Object[] param, final String[] signature,
            final String serverName) throws ManagementException;

    /**
     * Return the list of <code>ObjectName</code> Mbean gotten by the query in
     * the current MbeanServer.
     * @param p_On Query Mbeans to search
     * @return The list of <code>ObjectName</code>
     * @throws ManagementException
     */
    List getListMbean(final ObjectName p_On, final String serverName) throws ManagementException;

    /**
     * Return the MBeanServer connection corresponding to a given server in the
     * current domain. The connection for servers in the domain are provided by
     * the DomainMonitor.
     * @param serverName The managed server name
     * @return The MBeanServerConnection corresponding to the managed server
     * @throws ManagementException Couldn't get the connection
     */
    MBeanServerConnection getServerConnection(final String serverName) throws ManagementException;

    /**
     * Sets the value of a specific attribute of a named MBean.
     * @param on The ObjectName of the MBean.
     * @param serverName The server name
     * @param attribute A String specifying the name of the attribute to be set.
     * @param value The value to set to the attribute.
     * @throws ManagementException management operation failed
     */
    void setAttribute(final ObjectName on, final String attribute, final Object value, final String serverName)
            throws ManagementException;

    /**
     * Return the MBeanServer connection corresponding to a current server in
     * the current domain. The connection for servers in the domain are provided
     * by the DomainMonitor.
     * @return The MBeanServerConnection corresponding to the managed server
     * @throws ManagementException Couldn't get the connection
     */
    MBeanServerConnection getServerConnection() throws ManagementException;

    /**
     * Sets the value of a specific attribute of a named MBean within the
     * current server.
     * @param on The ObjectName of the MBean.
     * @param attribute A String specifying the name of the attribute to be set.
     * @param value The value to set to the attribute.
     * @throws ManagementException management operation failed
     */
    void setAttribute(final ObjectName on, final String attribute, final Object value) throws ManagementException;

    /**
     * Get the ObjectName of the Tomcat Realm MBean.
     * @param domainName the domain's name
     * @param serverName the server's name
     * @return ObjectName of the Tomcat Realm MBean.
     * @throws ManagementException
     */
    ObjectName getTomcatRealm(final String domainName, final String serverName)  throws ManagementException;

    /**
     * Get realm items of the given type.
     * @param realmType realm type.
     * @param sSecurityRealmUsed the used security realm.
     * @param domainName the domain's name
     * @param serverName server's name
     * @return real items of the given type
     * @throws ManagementException any.
     */
    List<?> getRealmItems(final String realmType, final String sSecurityRealmUsed, final String domainName,
            final String serverName) throws ManagementException;

    List<?> getTomcatRealmItems(final String usedSecurityRealm, final String domainName, final String serverName) throws ManagementException;

    /**
     * Get realm items of the given type within the current server of the
     * current domain.
     * @param realmType realm type.
     * @param sSecurityRealmUsed the used security realm.
     * @return realm items of the given type
     * @throws ManagementException any.
     */
    List<?> getRealmItems(final String realmType, final String sSecurityRealmUsed) throws ManagementException;
}
