/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.management.extensions.server.api;

import java.util.List;

/**
 * Server management API. Defines management operations for a server target.
 * These operations are based on MBeans associated to the managed server (J2EEServer for example).
 * @author Adriana Danes
 *
 */
public interface IServerManagement {
    /**
     * Get threads information for a given server managed in the domain.
     * @param serverName the server's name
     * @return Thread stack info.
     * @throws Exception  when operation invocation fails.
     */
    List<List<String>> getServerThreadsInformation(final String serverName) throws Exception;

    /**
     * Get threads information for a server identified by the a JMX Url.
     * @param jmxUrl the server's jmx url.
     * @param username the user name in case the jmx server is protected by user/password.
     * @param password the password in case the jmx server is protected by user/password.
     * @throws Exception when operation invocation fails
     */
    List<List<String>> getServerThreadsInformation(final String jmxUrl, final String username, final String password) throws Exception;

    /**
     * Deploy a module on a target server.
     * @param fileName Name of the module's file.
     * @param serverName Target server name.
     * @throws Exception deploy operation failed.
     */
    void deploy(final String fileName, final String serverName) throws Exception;

    /**
     * Uneploy a module on a target server.
     * @param fileName Name of the module's file.
     * @param serverName Target server name.
     * @throws Exception undeploy operation failed.
     */
    void undeploy(final String fileName, final String serverName) throws Exception;

    /**
     * Remove a module on a target server.
     * @param fileName Name of the module's file.
     * @param serverName Target server name.
     * @return true if the file successfully deleted
     * @throws Exception remove operation failed.
     */
    boolean remove(final String fileName, final String serverName) throws Exception;

    /**
     * Return true if a given service is in 'development' mode for a given server managed in the domain.
     * @param serviceName The service name, for example "depmonitor".
     * @param serverName Target server name.
     * @return true if a given service is in 'development' mode, false otherwise
     * @throws Exception
     */
    boolean developmentMode(final String serviceName, final String serverName) throws Exception;

}
