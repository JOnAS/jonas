/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.management.extensions.cluster.api;

import org.ow2.jonas.management.extensions.base.api.IBaseManagement;

/**
 * This interface provides a description of management operations provided for
 * cluster management in a master server.
 * @author Adriana Danes
 * @author Oualaa Hani
 * @author THOMAS KOUASSI
 */
public interface ICluster extends IBaseManagement {

    /**
     * Add server to cluster.
     * @param clusterName cluster name
     * @param serverName server name
     * @param clusterDaemon cluster daemon name
     * @param serverURL server url
     */

    void addServerToCluster(final String clusterName, final String serverName, final String clusterDaemon,
            final String serverURL);

    /**
     * start all servers in the cluster.
     * @param clusterName
     */
    void startAllServers(final String clusterName);

    /**
     * stop all servers in the cluster.
     * @param clusterName
     */
    void stopAllServers(final String clusterName);

    /**
     * @param clusterName.
     * @return the multicast address
     */
    String getMcastAddr(final String clusterName);

    /**
     * @param clusterName.
     * @return the multicast port
     */
    int getMcastPort(final String clusterName);

    /**
     * @param clusterName.
     * @return the delay used by clients to update their cluster view
     */
    int getDelayToRefresh(final String clusterName);
}
