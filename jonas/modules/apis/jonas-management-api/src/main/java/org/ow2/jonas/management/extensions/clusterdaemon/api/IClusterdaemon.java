/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or 1any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.management.extensions.clusterdaemon.api;

/**
 * This interface provides a description of management operations provided for
 * clusterdaemon management in a master server.
 * @author Adriana Danes
 * @author Oualaa Hani
 * @author Thomas KOUASSI
 */
public interface IClusterdaemon {

    /**
     * @return list of servers controlled by a cluster daemon.
     * @param clusterdaemonName name of the cluster daemon to manage
     */
    String[] getControlledServersNames(final String clusterdaemonName);

    /**
     * @return the state of a cluster daemon in the domain.
     * @param clusterdaemonName name of the cluster daemon to manage
     */
    String getClusterdaemonState(final String clusterdaemonName);

    /**
     * @param clusterdaemonName name of the cluster daemon to manage.
     * @return true if the cluster daemon is in RUNNING state, false otherwise
     */
    boolean isRunning(final String clusterdaemonName);

    /**
     * Start a server controlled by a cluster daemon.
     * @param clusterdaemonName name of the cluster daemon
     * @param serverName name of the server to start
     * @return true if successfully started
     */
    boolean startServer(final String clusterdaemonName, final String serverName);

    /**
     * Stop a server controlled by a cluster daemon.
     * @param clusterdaemonName name of the cluster daemon
     * @param serverName name of the server to stop
     * @return true if successfully stopped
     */

    boolean stopServer(final String clusterdaemonName, final String serverName);

    /**
     * Add a server to cluster daemon control.
     * @param clusterDaemonName the cluster Daemon name
     * @param serverName the server name
     * @param description server description
     * @param javaHome path to JRE
     * @param jonasRoot path to bin repository
     * @param jonasBase path to lib repository
     * @param xprem extra parameter e.g: -Djava.net.preferIPv4Stack=true
     * @param autoBoot true if the server is launched when cluster daemon starts
     * @param jonasCmd user command
     * @param saveIt true to flush the clusterd configuration
     */

    void addServer(final String clusterDaemonName, final String serverName, final String description, final String jonasRoot,
            final String jonasBase, final String javaHome, final String xprem, final String autoBoot, final String jonasCmd,
            final String saveIt);

    /**
     * Remove this server from cluster daemon control.
     * @param serverName the server to remove
     * @param saveIt true to flush the clusterd configuration
     */
    void removeServer(final String clusterDaemonName, final String serverName, final String saveIt);

    /**
     * Ask Cluster Daemon to start all the Servers from cluster daemon control.
     * @param clusterdaemonName name of the cluster daemon
     * @param otherParams the servers to start
     */
    void startAllServers(final String clusterDaemonName, final String otherParams);

    /**
     * Ask Cluster Daemon to stop all the Servers from cluster daemon control.
     * @param clusterdaemonName name of the cluster daemon
     * @param otherParams the servers to stop
     */
    void stopAllServers(final String clusterDaemonName, final String otherParams);

    /**
     * Get the OperatingSystemAvailableProcessorsof a cluster Daemon Name in the
     * domain.
     * @param clusterDaemonName the cluster Daemon Name's name.
     * @return the OperatingSystemAvailableProcessors of the cluster Daemon
     */
    String getOperatingSystemAvailableProcessors(final String clusterDaemonName);

    /**
     * Get the OperatingSystemName a cluster Daemon Name in the domain.
     * @param clusterDaemonName the cluster Daemon Name's name.
     * @return the operatingSystemName of the cluster Daemon
     */
    String getOperatingSystemName(final String clusterDaemonName);

    /**
     * Get the OperatingSystemVersion a cluster Daemon Name in the domain.
     * @param clusterDaemonName the cluster Daemon Name's name.
     * @return the OperatingSystemVersion of the cluster Daemon
     */
    String getOperatingSystemVersion(final String clusterDaemonName);

    /**
     * Get the RunTimeSpecVendor a cluster Daemon Name in the domain.
     * @param clusterDaemonName the cluster Daemon Name's name.
     * @return the RunTimeSpecVendor of the cluster Daemon
     */
    String getRunTimeSpecVendor(final String clusterDaemonName);

    /**
     * Get the RunTimeSpecVersion a cluster Daemon Name in the domain.
     * @param clusterDaemonName the cluster Daemon Name's name.
     * @return the RunTimeSpecVersion of the cluster Daemon
     */
    String getRunTimeSpecVersion(final String clusterDaemonName);

    /**
     * Get the RunTimeVmName a cluster Daemon Name in the domain.
     * @param clusterDaemonName the cluster Daemon Name's name.
     * @return the RunTimeVmName of the cluster Daemon
     */
    String getRunTimeVmName(final String clusterDaemonName);

    /**
     * Get the RunTimeVmVendor a cluster Daemon Name in the domain.
     * @param clusterDaemonName the cluster Daemon Name's name.
     * @return the runTimeVmVendor of the cluster Daemon
     */
    String getRunTimeVmVendor(final String clusterDaemonName);

    /**
     * Get the RunTimeVmVersion a cluster Daemon Name in the domain.
     * @param clusterDaemonName the cluster Daemon Name's name.
     * @return the RunTimeVmVersion of the cluster Daemon
     */
    String getRunTimeVmVersion(final String clusterDaemonName);

    /**
     * Get the OperatingSystemArch a cluster Daemon Name in the domain.
     * @param clusterDaemonName the cluster Daemon Name's name.
     * @return the OperatingSystem Architecture
     */
    String getOperatingSystemArch(final String clusterDaemonName);

    /**
     * Get the vmCurrentUsedMemory a cluster Daemon Name in the domain.
     * @param clusterDaemonName the cluster Daemon Name's name. Getting remote
     *        Vm used Memory
     * @return the value of current used memory of the cluster Daemon
     */
    String getVmCurrentUsedMemory(final String clusterDaemonName);

    /**
     * Get the vmTotalMemory a cluster Daemon Name in the domain.
     * @param clusterDaemonName the cluster Daemon Name's name. Getting remote
     *        Vm Total Memory
     * @return the value of Vm Total memory of the cluster Daemon
     */
    String getVmTotalMemory(final String clusterDaemonName);

    /**
     * Getting remote Vm's Current used Heap memory
     * @param clusterDaemonName the cluster Daemon Name's name.
     * @return the value of Vm's Current used Heap memory
     */
    String getVmCurrentUsedHeapMemory(final String clusterDaemonName);

    /**
     * Getting remote Vm's Current used non Heap memory
     * @param clusterDaemonName the cluster Daemon Name's name.
     * @return the value of Vm's Current used non Heap memory of the cluster
     *         Daemon
     */
    String getVmCurrentUsedNonHeapMemory(final String clusterDaemonName);

    /**
     * Getting Operating system Current used space.
     * @param clusterDaemonName the cluster Daemon Name's name.
     * @return the value of Operating system Current used space of the cluster
     *         Daemon
     */
    String getOsCurrentUsedSpace(final String clusterDaemonName);

    /**
     * Getting Operating system Total space
     * @param clusterDaemonName the cluster Daemon Name's name.
     * @return the value of Operating system Total space of the cluster Daemon
     */
    String getOsTotalSpace(final String clusterDaemonName);

}
