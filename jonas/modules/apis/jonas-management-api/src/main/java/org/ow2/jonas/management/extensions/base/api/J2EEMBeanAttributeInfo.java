/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.management.extensions.base.api;

import java.io.Serializable;

import javax.management.MBeanAttributeInfo;

/**
 * Attribute infos. Used for example javaee containers and modules.
 * @author eyindanga
 */
public class J2EEMBeanAttributeInfo implements Serializable {
    /**
     * The value of the property.
     */
    private Object value;

    /**
     * Label to display.
     */
    private String label;

    /**
     * Serial UID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Attributes infos.
     */

    MBeanAttributeInfo mbeanAttributeInfo = null;

    /**
     * Default constructor.
     */
    public J2EEMBeanAttributeInfo() {

    }

    /**
     * Constructor using <code>MBeanAttributeInfo</code>
     * @param mbeaAttributeInfo attribute infos.
     */
    public J2EEMBeanAttributeInfo(final MBeanAttributeInfo mbeaAttributeInfo) {
        this.mbeanAttributeInfo = mbeaAttributeInfo;
    }

    /**
     * @return the value.
     */
    public Object getValue() {
        return value;
    }

    /**
     * @param value the value to set.
     */
    public void setValue(final Object value) {
        this.value = value;
    }

    /**
     * @return the mbeAttributeInfo.
     */
    public MBeanAttributeInfo getMbeanAttributeInfo() {
        return mbeanAttributeInfo;
    }

    /**
     * @param mbeAttributeInfo the mbeAttributeInfo to set.
     */
    public void setMbeanAttributeInfo(final MBeanAttributeInfo mbeAttributeInfo) {
        this.mbeanAttributeInfo = mbeAttributeInfo;
    }

    /**
     * Get attribute description.
     * @return attribute description.
     */
    public String getDescription() {
        return mbeanAttributeInfo.getDescription();
    }

    /**
     * Get attribute name.
     * @return attribute name.
     */
    public String getName() {
        return mbeanAttributeInfo.getName();
    }

    /**
     * Get attribute type.
     * @return attribute type.
     */
    public String getType() {
        return mbeanAttributeInfo.getType();
    }

    /**
     * @return <code>true</code> if attribute is IS.
     */
    public boolean isIs() {
        return mbeanAttributeInfo.isIs();
    }

    /**
     * @return <code>true</code> if attribute is readable.
     */
    public boolean isReadable() {
        return mbeanAttributeInfo.isReadable();
    }

    /**
     * @return <code>true</code> if attribute is writable.
     */
    public boolean isWritable() {
        return mbeanAttributeInfo.isWritable();
    }

    /**
     * @return the label.
     */
    public String getLabel() {
        if (label == null) {
            computeLabel();
        }
        return label;
    }

    /**
     * @param label the label to set.
     */
    public void setLabel(final String label) {
        this.label = label;
    }
    /**
     * Compute the label field.
     */
    private void computeLabel() {
        if (mbeanAttributeInfo != null &&  mbeanAttributeInfo.getName()!= null) {
            String lbl = mbeanAttributeInfo.getName();
            /**
             * TODO: Refine label computing from attribute name.
             */
            try {
                label = lbl.substring(0, lbl.indexOf("_"));
            } catch (Exception e) {
                label = lbl;
            }


        }

    }

}
