/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Philippe Coq
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.deployment.api;


/**
 * This interface provides JNDI environment reference elements
 * for J2EE deployment descriptors.
 * @author Philippe Coq
 */
public interface IJNDIEnvRefsGroupDesc {

    /**
     * Get  resource environment references.
     * @return array of resource environment reference descriptors
     */
    IResourceEnvRefDesc[] getResourceEnvRefDesc();

    /**
     * Get resource manager connection factory references.
     * @return array of resource reference descriptors
     */
    IResourceRefDesc[] getResourceRefDesc();

    /**
     * Get environment entries.
     * @return array of  environment entries descriptors
     */
    IEnvEntryDesc[] getEnvEntryDesc();

    /**
     * Get EJB references.
     * @return array of EJB  reference descriptors
     */
    IEJBRefDesc[] getEjbRefDesc();

    /**
     * Get ejb local references.
     * @return array of ejb local reference descriptors
     */
    IEJBLocalRefDesc[] getEjbLocalRefDesc();

    /**
     * Get service references.
     * @return array of service references descriptors
     */
    IServiceRefDesc[] getServiceRefDesc();

    /**
     * Get message destination references.
     * @return array of message destination references descriptors
     */
    IMessageDestinationRefDesc[] getMessageDestinationRefDesc();

}
