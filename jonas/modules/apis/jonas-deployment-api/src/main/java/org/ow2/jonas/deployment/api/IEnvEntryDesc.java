/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.api;

/**
 * This interface represents the description of an EnvEntry object.
 * @author Philippe Durieux
 */
public interface IEnvEntryDesc {

    /**
     * Get the name of the environemt entry.
     * @return Name for environment entry
     */
    String getName();

    /**
     * Get the fully-qualified Java type of the environemt entry.
     * Type is needed since value is optional
     * The possibles values are:
     *   java.lang.Boolean
     *   java.lang.Character
     *   java.lang.String
     *   java.lang.Integer
     *   java.lang.Double
     *   java.lang.Byte
     *   java.lang.Short
     *   java.lang.Long
     *   java.lang.Float
     * @return Class the fully-qualified Java type of the environemt entry.
     */
    Class getType();

    /**
     * Assessor for existence of value for the descriptor.
     * @return true if a value is available
     */
    boolean hasValue();

    /**
     * Get the value of the environment entry.
     * @return value for the environment entry (must be set)
     */
    Object getValue();

    /**
     * @return Env-Entry lookup name.
     */
    String getLookupName();

}
