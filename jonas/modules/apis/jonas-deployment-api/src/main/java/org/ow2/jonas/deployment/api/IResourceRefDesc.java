/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.api;

public interface IResourceRefDesc {

    /**
     * To indicate that the application manage the authentication.
     */
    final int APPLICATION_AUTH = 0;

    /**
     * To indicate that the container manage the authentication.
     */
    final int CONTAINER_AUTH = 1;

    /**
     * Get resource ref. name
     * @return Name of the resource ref.
     */
    String getName();

    /**
     * Get resource ref. type name
     * @return Class name of the resource ref.
     */
    String getTypeName();

    /**
     * Get the authentication of the resource ref.
     * @return Authentication value within APPLICATION_AUTH, CONTAINER_AUTH
     */
    int getAuthentication();

    /**
     * Assessor for JDBC resource
     * @return true if the resource is Jdbc compliant
     */
    boolean isJdbc();

    /**
     * Get the jndi name of the resource ref.
     * @return String representation of the JNDI name
     */
    String getJndiName();

}
