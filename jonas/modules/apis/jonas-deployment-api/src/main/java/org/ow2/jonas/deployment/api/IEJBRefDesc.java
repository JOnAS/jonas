/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.api;

public interface IEJBRefDesc {

    /**
     * Get the name of the ejb-ref.
     * @return String representation of the ejb-ref-name.
     */
    String getEjbRefName();

    /**
     * Get the ejb-ref-type.
     * @return String representation of the ejb-ref-type.
     */
    String getEjbRefType();

    /**
     * Get the ejb-link.
     * @return String representation of the ejb-link
     */
    String getEjbLink();

    /**
     * Get the jndi name of the ejb-ref.
     * @return String representation of the JNDI name
     */
    String getJndiName();

    /**
     * @return the fully qualified name of the enterprise bean's home interface
     */
    String getHome();

    /**
     * @return the fully qualified name of the enterprise bean's remote interface
     */
    String getRemote();

}
