/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.deployment.api;

import java.net.URL;
import java.util.Hashtable;
import java.util.List;

import javax.xml.namespace.QName;

public interface IServiceRefDesc {

    /**
     * Return the list of PortComponentRef.
     * @return the list of PortComponentRef
     */
    List getPortComponentRefs();

    /**
     * Return the list of Handler.
     * @return the list of Handler
     */
    List getHandlerRefs();

    /**
     * Return the name used for Service interface lookup.
     * @return the service-ref-name value
     */
    String getServiceRefName();

    /**
     * Return the Class object representing the service-interface.
     * @return the Class object representing the service-interface.
     */
    Class getServiceInterface();

    /**
     * Return the WSDLFile object describing the WebService.
     * @return the WSDLFile object describing the WebService.
     */
    //WSDLFile getWSDLFile();

    /**
     * Return the MappingFile object.
     * @return the MappingFile object.
     */
    //MappingFile getMappingFile();

    /**
     * Return all the params of the ServiceRefDesc as an Hashtable.
     * @return all the params of the ServiceRefDesc as an Hashtable.
     */
    Hashtable getParams();

    /**
     * Return the value of the specified parameter.
     * @param name the parameter to retrieve
     * @return the value of the specified parameter
     */
    String getParam(final String name);

    /**
     * Return the name of WSDL inside of the module.
     * @return the name of WSDL inside of the module
     */
    String getWsdlFileName();

    /**
     * Return the QName identifying the service in the WSDL. can return null if
     * WSDL not defined.
     * @return Return the QName identifying the service in the WSDL (can be
     *         null).
     */
    QName getServiceQName();

    /**
     * @return Returns the alternate WSDL URL (may be null).
     */
    URL getAlternateWsdlURL();

    /**
     * @return Returns the URL where the uptodate WSDL can be found.
     */
    URL getLocalWSDLURL();

    /**
     * @return Returns the URL where the mapping file can be found.
     */
    URL getMappingFileURL();

}