/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.tm;


import org.ow2.jonas.service.Service;

import javax.resource.spi.XATerminator;
import javax.transaction.NotSupportedException;
import javax.transaction.SystemException;
import javax.transaction.TransactionSynchronizationRegistry;
import javax.transaction.UserTransaction;
import javax.transaction.xa.XAException;
import javax.transaction.xa.Xid;



/**
 * Transaction Service interface.
 */
public interface TransactionService extends Service {

    /**
     * Gets the TransactionManager.
     */
    TransactionManager getTransactionManager();

    /**
     * @return Returns the {@link UserTransaction} object.
     */
    UserTransaction getUserTransaction();

    /**
     * @return Returns the TransactionSynchronizationRegistry object.
     */
    TransactionSynchronizationRegistry getTransactionSynchronizationRegistry();

    /**
     * Gets the inflow transaction object that represents the transaction context of
     * the calling thread. If the calling thread is
     * not associated with an inflow transaction, a null object reference
     * is returned.
     * @return a XATerminator handle
     * @throws XAException
     */
    XATerminator getXATerminator() throws XAException;

    /**
     * Start ResourceManager Recovery
     * @throws XAException
     */
    void startResourceManagerRecovery() throws XAException;

    /**
     * Sets the default transaction timeout.
     * @param t default transaction timeout.
     */
    void setTimeout(int t);

    /**
     * Attach the current calling {@link Thread} to the given {@link Xid}.
     * @param xid Transaction id.
     * @param timeout Transaction timeout.
     * @throws SystemException if the TM cannot attach the given {@link Xid}.
     * @throws NotSupportedException if the TM cannot attach the given {@link Xid}.
     */
    void attachTransaction(Xid xid, long timeout) throws NotSupportedException, SystemException;

    /**
     * Detach the current {@link Thread} of the running Transaction.
     */
    void detachTransaction();

}

