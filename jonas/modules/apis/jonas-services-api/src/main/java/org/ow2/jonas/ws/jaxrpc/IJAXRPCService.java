/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
*/

package org.ow2.jonas.ws.jaxrpc;


import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.Reference;

import org.ow2.jonas.deployment.api.IServiceRefDesc;
import org.ow2.jonas.service.Service;
import org.ow2.util.ee.deploy.api.deployable.IDeployable;




/**
 * WebServices Service interface.
 *
 * @author Guillaume Sauthier
 */
public interface IJAXRPCService extends Service {

    /**
     * classloader Context param.
     */
    String CLASSLOADER_CTX_PARAM = "classloader";

    /**
     * parentObjectName Context Param.
     */
    String PARENT_OBJECTNAME_CTX_PARAM = "parentObjectName";

    /**
     * isInEar Context Param.
     */
    String ISINEAR_CTX_PARAM = "isInEar";

    /**
     * warURL Context Param.
     */
    String WARURL_CTX_PARAM = "warURL";

    /**
     * Directory (as a File) where is unpacked the deployed archive.
     */
    String UNPACK_DIRECTORY_CTX_PARAM = "unpackDir";

    /**
     * explore the context classloader to find Web service deployment
     * descriptor (for endpoints) and service-ref element within standard
     * deployment descriptor (for clients). It registers each endpoints in WS
     * engine publishing relative WSDLs, and it instanciates and binds in the
     * registry each clients classes.
     *
     * @param ctx the context containing the configuration  to deploy the
     *        wars.<BR> This context contains the following parameters :<BR> -
     *        jarUrls the list of the urls of the jars to deploy.<BR> -
     *        warUrls the list of the urls of the wars to deploy.<BR> -
     *        parentClassLoader the parent classLoader of the wars.<BR> -
     *        earClassLoader the ear classLoader of the j2ee app.<BR> - altDDs
     *        the optional URI of deployment descriptor.<BR>
     *
     * @throws WSException if an error occurs during  the deployment.
     */
    void deployWebServices(Context ctx) throws WSException;

    /**
     * Remove the WebServices descriptors associated to the given ClassLoader.
     *
     * @param cl the key ClassLoader
     */
    void removeCache(ClassLoader cl);

    /**
     * Complete the WebServices Deployment (add informations in web environment).
     * @param ctx Context containing the key ClassLoader
     * @throws WSException When Endpoints URLs binding fails
     */
    void completeWSDeployment(Context ctx) throws WSException;

    /**
     * buildServiceRef from WS ref descriptor.
     * @param rd WS ref descriptor
     */
    Reference buildServiceRef(IServiceRefDesc rd, ClassLoader loader) throws NamingException;

    /**
     * Undeploy the WebServices : unpublish WSDL + unregister MBeans.
     * @param ctx Context containing undeployment informations
     * @throws WSException When undeployment fails
     */
    void undeployWebServices(Context ctx) throws WSException;

    /**
     * Automatic WSGen is enabled ?
     * @return true if automatic WSGen needs to be Called.
     */
    boolean isAutoWsGenEngaged();

    /**
     * Apply WSGen on the given deployable.
     * @param deployable the deployable to use
     * @return the modified file or the original file if WSGen has not been launched.
     * @throws WSException If WSGen cannot be applied.
     */
    String applyWSGen(final IDeployable<?> deployable) throws WSException;

}
