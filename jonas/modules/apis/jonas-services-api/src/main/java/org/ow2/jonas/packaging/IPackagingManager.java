/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.packaging;

import java.net.URL;
import java.util.List;

/**
 * JOnAS Service interface for the packaging service.
 * @author Mohammed Boukada
 */
public interface IPackagingManager {

    /**
     * Generate an addon
     * @param urlCloudApplication URL of cloud-application.xml
     * @param tenantId tenant identifier of the application
     */
    List<String> generateAddon(URL urlCloudApplication, String tenantId) throws Exception;

    /**
     * Generate an addon
     * @param urlCloudApplication URL of cloud-application.xml
     * @param tenantId tenant identifier of the application
     * @param outputDir
     */
    List<String> generateAddon(URL urlCloudApplication, String tenantId, String outputDir) throws Exception;

    /**
     * Generate an addon
     * @param urlCloudApplication URL of cloud-application.xml
     * @param tenantId tenant identifier of the application
     * @param urlEnvironmentTemplate URL of environment-template.xml
     * @param urlMappingTopology URL of deployment.xml
     */
    List<String> generateAddon(URL urlCloudApplication, String tenantId, URL urlEnvironmentTemplate, URL urlMappingTopology) throws Exception;

    /**
     * Generate an addon
     * @param urlCloudApplication URL of cloud-application.xml
     * @param tenantId tenant identifier of the application
     * @param urlEnvironmentTemplate URL of environment-template.xml
     * @param urlMappingTopology URL of deployment.xml
     * @param outputDir
     */
    List<String> generateAddon(URL urlCloudApplication, String tenantId, URL urlEnvironmentTemplate, URL urlMappingTopology,
                               String outputDir) throws Exception;
    /**
     * Generate an addon
     * @param xmlCloudApplication cloud-application.xml content
     * @param tenantId tenant identifier of the application
     */
    List<String> generateAddon(String xmlCloudApplication, String tenantId) throws Exception;

    /**
     * Generate an addon
     * @param xmlCloudApplication cloud-application.xml content
     * @param tenantId tenant identifier of the application
     * @param outputDir
     */
    List<String> generateAddon(String xmlCloudApplication, String tenantId, String outputDir) throws Exception;

    /**
     * Generate an addon
     * @param xmlCloudApplication cloud-application.xml content
     * @param tenantId tenant identifier of the application
     * @param xmlEnvironmentTemplate environment-template.xml content
     * @param xmlMappingTopology deployment.xml content
     */
    List<String> generateAddon(String xmlCloudApplication, String tenantId, String xmlEnvironmentTemplate,
                               String xmlMappingTopology) throws Exception;

    /**
     * Generate an addon
     * @param xmlCloudApplication cloud-application.xml content
     * @param tenantId tenant identifier of the application
     * @param xmlEnvironmentTemplate environment-template.xml content
     * @param xmlMappingTopology deployment.xml content
     * @param outputDir
     * @return List of zips locations
     */
    List<String> generateAddon(String xmlCloudApplication, String tenantId, String xmlEnvironmentTemplate,
                                      String xmlMappingTopology, String outputDir) throws Exception;
}
