/**
 * JOnAS: Java Open Application Server
 * Copyright (C) 2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.jaxws;

import java.io.IOException;
import java.io.InputStream;

/**
 * An {@link IWSRequest} represents a request to an {@link IWebServiceEndpoint}.
 *
 * @author Guillaume Sauthier
 */
public interface IWSRequest {


    /**
     * @return the {@link InputStream} with the web service request
     *         message's content.
     * @throws IOException when {@link InputStream} cannot be returned.
     */
    InputStream getInputStream() throws IOException;

    String getRemoteAddr();
    String getHeader(String name);
    /**
     * @return the MIME Content-Type header.
     */
    String getContentType();

    /**
     * Permits to use the {@link IWSRequest} interface as a storage place.
     * @param type key
     * @param value the object to be stored
     * @param <T> interface of the object
     * @return the old value if any was set before, or null.
     */
    <T> T setAttribute(Class<T> type, T value);

    /**
     * Returns an object of the corresponding interface, or null if none was found
     * @param <T> interface of the object
     * @param type expected interface type used as a key
     * @return an object of the corresponding interface, or null if none was found
     */
    <T> T getAttribute(Class<T> type);
}
