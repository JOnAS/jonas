/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ejb2;

import java.net.URL;

import javax.ejb.Timer;
import javax.naming.Context;

import org.ow2.jonas.service.Service;
import org.ow2.jonas.service.ServiceException;


/**
 * EJB Service interface.
 */
public interface EJBService extends Service {

    /**
     * Create a JOnAS Container for all the beans that are described in a .xml file,
     * or belong to .jar file.
     * @param ctx JNDI context in which is found the container configuration.
     * @return The ObjectName of the MBean associated to the container (i.e. to the deployed module)
     * @throws Exception if Container creation was unable to complete.
     */
    String createContainer(Context ctx) throws Exception;

    /**
     * Create a JOnAS Container for all the beans that are described in a .xml file,
     * or belong to .jar file.
     * @param fileName EjbJar filename.
     * @throws Exception when the EjbJar cannot be deployed.
     * @return The ObjectName of the MBean associated to the container (i.e. to the deployed module)
     */
    String createContainer(String fileName) throws Exception;

    /**
     * Test if the specified file is already deployed (if a container
     * is created for this jar).
     * @param fileName the name of the jar file
     * @return true if the jar was deployed, false otherwise
     * TODO redundant with EJBServiceMBean.isJarLoaded()
     *      Used in J2EEServer, J2EESERMBean (mbeans-descriptors.xml)
     */
    Boolean isJarDeployed(String fileName);

    /**
     * Test if the specified jar identified with its work name is already deployed
     * (if a container is created for this jar).
     * @param workFileName the internal name of the jar file (working copy)
     * @return true if the jar was deployed, false otherwise
     */
    boolean isJarDeployedByWorkName(String workFileName);

    /**
     * Removes a  JOnAS Container.
     * @param fileName JOnAS container to remove.
     * @throws Exception when the EjbJar cannot be undeployed.
     */
    void removeContainer(String fileName) throws Exception;

    /**
     * Synchronized all entity bean containers.
     * @param passivate passivate instances after synchronization.
     */
    void syncAllEntities(boolean passivate);

    /**
     * Deploy the given ejb-jars of an ear file with the specified parent
     * classloader (ear classloader). (This method is only used for
     * the ear applications, not for the ejb-jar applications).
     * @param ctx the context containing the configuration
     * to deploy the ejbjars.<BR>
     * This context contains the following parameters :<BR>
     *    - earRootUrl the root of the ear application.<BR>
     *    - earClassLoader the ear classLoader of the ear application.<BR>
     *    - ejbClassLoader the ejb classLoader for the ejbjars.<BR>
     *    - jarURLs the list of the urls of the ejb-jars to deploy.<BR>
     *    - roleNames the role names of the security-role.<BR>
     * @throws ServiceException if an error occurs during the deployment.
     */
    void deployJars(Context ctx)throws ServiceException;

    /**
     * Undeploy the given ejb-jars of an ear file with the specified parent
     * classloader (ear classloader). (This method is only used for
     * the ear applications, not for the ejb-jar applications).
     * @param urls the list of the urls of the ejb-jars to deploy.
     */
    void unDeployJars(URL[] urls);

    /**
     * Make a cleanup of the cache of deployment descriptor. This method must
     * be invoked after the ear deployment by the EAR service.
     * the deployment of an ear by .
     * @param earClassLoader the ClassLoader of the ear application to
     * remove from the cache.
     */
    void removeCache(ClassLoader earClassLoader);

    /**
     * Check that GenIC have been applied on the given ejb-jar file
     * If it was not done, it run GenIC against the file.
     * @param fileName given EJB-JAR file.
     * @param urls Array of URLs used as CLASSPATH during EJB compilation
     */
    void checkGenIC(String fileName, URL[] urls);

    /**
     * Returns the ContextID associated to this container filename.
     * Used by the {@link EARService} for security purpose.
     * @param containerFileName container filename
     * @return Returns the ContextID associated to this container filename.
     */
    String getContainerContextID(String containerFileName);

    /**
     * Get a new {@link Timer}.
     * @param info {@link JTimerHandleInfo} instance used for {@link Timer}
     * creation.
     * @return a newly created {@link Timer}
     */
    Timer getTimer(JTimerHandleInfo info);

    /**
     * Restart a {@link Timer} given some input infos.
     * @param info {@link JTimerHandleInfo} instance used for {@link Timer}
     * restart.
     * @return a newly restarted {@link Timer}.
     */
    Timer restartTimer(JTimerHandleInfo info);
}

