/**
 * JOnAS: Java Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.jaxws.ejb;

import org.ow2.jonas.ws.jaxws.IWebservicesModule;
import org.ow2.jonas.ws.jaxws.WSException;
import org.ow2.jonas.ws.jaxws.IWebservicesContainer;

/**
 * The IEJBWsModuleWebDeployer is responsible to hook up an ejb endpoint
 * inside a web container.
 * That means creating one (or more) Web context(s), generating required servlets declarations,
 * servlet mappings, security stuff, ...
 *
 * @author Guillaume Sauthier
 */
public interface IWebDeployer {

    /**
     * Deploy the given module, with all its endpoints in the web
     * container, creating appropriate contexts when required.
     * @param module the webservices module to deploy
     * @throws WSException if deployment went wrong.
     */
    void deploy(IWebservicesModule<? extends IWebservicesContainer<? extends IEJBWebserviceEndpoint>> module) throws WSException;

    /**
     * Undeploy the given module, with all its endpoints from the
     * web container. An implementation should catch most of the
     * Exceptions and try to cleanupas much as possible.
     * @param module the webservices module to undeploy
     */
    void undeploy(IWebservicesModule<? extends IWebservicesContainer<? extends IEJBWebserviceEndpoint>> module);
}
