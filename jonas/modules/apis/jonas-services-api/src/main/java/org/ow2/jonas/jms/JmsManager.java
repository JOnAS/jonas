/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hoper that irt will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.jms;

import java.util.Enumeration;

import javax.jms.ConnectionFactory;
import javax.jms.Queue;
import javax.jms.QueueConnectionFactory;
import javax.jms.Topic;
import javax.jms.TopicConnectionFactory;
import javax.jms.XAConnectionFactory;
import javax.jms.XAQueueConnectionFactory;
import javax.jms.XATopicConnectionFactory;

import org.ow2.jonas.tm.TransactionManager;


/**
 * JMS Manager interface.
 * Implemented by the jms module (jonas/lib/jms/JmsManagerImpl)
 * This interface allows other jonas module to be independant
 * of jonas jms implementation.
 * @author Philippe Coq
 * Contributor(s):
 *        Jeff Mesnil: for JORAM 3.0 integration
 *        Frederic Maistre: for JORAM 3.4  (JMS 1.1) integration
 */
public interface JmsManager {

    /**
     * Initialisation of JmsManager
     *
     * @param class cl class implementing administration process
     * @param boolean true for launching the MOM in the same JVM
     * @param String connexion url to the MOM (in case of remote mode)
     * @param TransactionManager tm
     * @exception  Exception must be thrown if the MOM is unreachable
     */
    public void init(Class cl, boolean collocated, String url, TransactionManager tm) throws Exception;

   /**
     * Terminate the administering process
     */
    public void stop() throws Exception;

    /**
     * Create a Queue and bind it in the registry
     */
    public Queue createQueue(String name) throws Exception;

    /**
     *  Get Queue (creates it if not exist)
     */
    public Queue getQueue(String name) throws Exception;

    /**
     *  Get Queue Names
     */
    public Enumeration getQueuesNames();

    /**
     * Create a Topic and bind it in the registry
     */
    public Topic createTopic(String name) throws Exception;

    /**
     *  Get Topic (creates it if not exist)
     */
    public Topic getTopic(String name) throws Exception;

    /**
     *  Get Topic Names
     */
    public Enumeration getTopicsNames();

    /**
     *  Get the unique ConnectionFactory
     */
    public ConnectionFactory getConnectionFactory();

    /**
     *  Get the unique XAConnectionFactory
     */
    public XAConnectionFactory getXAConnectionFactory();

    /**
     *  Get the unique TopicConnectionFactory
     */
     public TopicConnectionFactory getTopicConnectionFactory();

    /**
     *  Get the unique XATopicConnectionFactory
     */
    public XATopicConnectionFactory getXATopicConnectionFactory();

    /**
     *  Get the unique QueueConnectionFactory
     */
    public QueueConnectionFactory getQueueConnectionFactory();

    /**
     *  Get the unique XAQueueConnectionFactory
     */
     public XAQueueConnectionFactory getXAQueueConnectionFactory();
}

