/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ejb3;

import java.net.URL;
import java.util.Map;

import org.ow2.easybeans.api.EZBContainer;
import org.ow2.easybeans.api.EZBServer;
import org.ow2.easybeans.api.naming.EZBNamingStrategy;
import org.ow2.easybeans.deployment.api.EZBInjectionHolder;
import org.ow2.easybeans.resolver.api.EZBApplicationJNDIResolver;
import org.ow2.easybeans.resolver.api.EZBJNDIResolver;
import org.ow2.easybeans.persistence.api.EZBPersistenceUnitManager;
import org.ow2.easybeans.persistence.api.PersistenceXmlFileAnalyzerException;
import org.ow2.jonas.service.Service;
import org.ow2.util.ee.deploy.api.deployable.EARDeployable;
import org.ow2.util.ee.deploy.api.deployable.EJB3Deployable;
import org.ow2.util.ee.deploy.api.deployable.WARDeployable;
import org.ow2.util.archive.api.IArchive;

/**
 * EZB Service interface.
 * @author Guillaume Sauthier
 */
public interface IEasyBeansService extends Service {

    /**
     * TODO Remove it when EarDeployer will use EasyBeans Deployer.
     * @return the Embedded instance used by this service.
     */
    EZBServer getEasyBeansServer();

    /**
     * Adds the given container.
     * @param ejbContainer the given container
     */
    void addContainer(final EZBContainer ejbContainer);

    /**
     * Remove the given container.
     * @param ejbContainer the given container
     */
    void removeContainer(final EZBContainer ejbContainer);

    /**
     * Test if the specified unpack name is already deployed or not. This method
     * is defined in the {@link IEasyBeansService} interface.
     * @param unpackName the name of the EJB3 file.
     * @return true if the EJB3 is deployed, else false.
     */
    boolean isEJB3DeployedByWorkName(final String unpackName);
    
    /**
     * Register Embedded as an OSGi service.
     */
    void registerEmbeddedService();


    /**
     * Allow to build a classloader that provide JPA classtransformers and bytecode modifications.
     * @param urls the array of URLs to use
     * @param parentClassLoader the parent classloader
     * @return a classloader.
     */
    ClassLoader buildByteCodeEnhancementClassLoader(final URL[] urls,
                                                    final ClassLoader parentClassLoader);

    /**
     * @param persistenceUnitManager the Persistence Unit Manager (if any)
     * @param jndiResolver the JNDI resolver (if any) 
     * @return a new Injection holder.
     */
    EZBInjectionHolder buildInjectionHolder(final EZBPersistenceUnitManager persistenceUnitManager,
                                            final EZBJNDIResolver jndiResolver);

    /**
     * @return a new JNDI application resolver.
     */
    EZBApplicationJNDIResolver buildApplicationJNDIResolver();

    /**
     * Gets the persistence unit manager for the given archive and classloader.
     * @param archive  the archive
     * @param appClassLoader the classloader used as deployable
     * @return the given persistence unit manager
     */
    EZBPersistenceUnitManager getPersistenceUnitManager(final IArchive archive,
                                                        final ClassLoader appClassLoader) throws PersistenceXmlFileAnalyzerException;

    /**
     * Gets the persistence unit manager for the given EAR and classloader.
     * @param earDeployable  the ear deployable
     * @param appClassLoader the classloader used as deployable
     * @return the given persistence unit manager
     */
    EZBPersistenceUnitManager getPersistenceUnitManager(final EARDeployable earDeployable,
                                                        final ClassLoader appClassLoader) throws PersistenceXmlFileAnalyzerException;

     /**
     * Build a new Strategy for the given prefix and the old strategy.
     * @param prefix the given prefix
     * @param oldNamingStrategy the strategy
     * @return the new strategy
     */
    EZBNamingStrategy getNamingStrategy(final String prefix,
                                        final EZBNamingStrategy oldNamingStrategy);
 
    
    /**
     * Return a container if there are EJBs inside the webApp.
     * @param warDeployable
     * @param properties
     * @return EJB3 container if found
     */
    EZBContainer getEJBContainerFromWar(WARDeployable warDeployable, Map<?, ?> properties);
}
