/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 France Telecom R&D
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.jndi.checker.api;

import java.util.List;

/**
 * Manage the resources that can be checked during a call on the same thread.
 * @author Florent Benoit
 */
public interface IResourceCheckerManager extends IResourceChecker {

        /**
     * Enlist a new resource.
     * @param resource the given resource that can be checked.
     */
    void enlistResource(final IResourceChecker resource);

    /**
     * Delist a resource.
     * @param resource the given resource.
     */
    void delistResource(final IResourceChecker resource);

    /**
     * Delist all resources that are enlisted.
     */
    void delistAll();

    /**
     * Push a new list on the stack.
     */
    void push();

    /**
     * Pop the list on the stacK.
     */
    void pop();

    /**
     * @return list of resources enlisted on the current thread.
     */
    List<IResourceChecker> getResources();

}
