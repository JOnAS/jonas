/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.publish;

import java.io.File;

/**
 * The PublicationInfo is a data holder storing information about WSDL publication.
 *
 * @author Guillaume Sauthier
 */
public class PublicationInfo {

    /**
     * The original WSDL Definition's filename.
     */
    private String originalWsdlFilename;

    /**
     * A (possibly null) directory (may be relative or absolute) where the
     * WSDL definition will be copied.
     */
    private File publicationDirectory;

    /**
      * @return the orginial WSDM filename.
     */
    public String getOriginalWsdlFilename() {
        return originalWsdlFilename;
    }

    /**
     * Set the WSDL filename (must not be null or empty).
     * @param originalWsdlFilename filename
     */
    public void setOriginalWsdlFilename(final String originalWsdlFilename) {

        // Check input
        if ((originalWsdlFilename == null) || ("".equals(originalWsdlFilename))) {
            throw new IllegalStateException("Original WSDL filename cannot be null or empty !");
        }

        // Remove extra META-INF/wsdl or WEB-INF/wsdl if any
        if (originalWsdlFilename.startsWith("META-INF/wsdl/")) {
            this.originalWsdlFilename = originalWsdlFilename.substring("META-INF/wsdl/".length());
        } else if (originalWsdlFilename.startsWith("WEB-INF/wsdl/")) {
            this.originalWsdlFilename = originalWsdlFilename.substring("WEB-INF/wsdl/".length());
        } else {
            this.originalWsdlFilename = originalWsdlFilename;
        }
    }

    /**
     * @return the directory where the WSDL will be copied.
     */
    public File getPublicationDirectory() {
        return publicationDirectory;
    }

    /**
     * Set the directory where the WSDL will be copied.
     * @param publicationDirectory the directory where the WSDL will be copied.
     */
    public void setPublicationDirectory(final File publicationDirectory) {
        this.publicationDirectory = publicationDirectory;
    }
}
