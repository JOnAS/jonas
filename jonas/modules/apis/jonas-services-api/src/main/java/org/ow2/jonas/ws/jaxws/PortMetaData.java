/**
 * JOnAS: Java Open Application Server
 * Copyright (C) 2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.jaxws;

/**
 * The {@link PortMetaData} class store deploy time / runtime information
 * about the webservice.
 * It's associated to an <code>IWebServiceEndpoint</code>.
 * @author Guillaume Sauthier
 */
public class PortMetaData {

    /**
     * Requested url-mapping pattern.
     */
    private String urlPattern = null;

    /**
     * Requested context-root of the web-app (only for EJB, or null).
     */
    private String contextRoot = null;

    /**
     * Host's name.
     */
    private String hostname = null;

    /**
     * content of Handler.xml
     */
    private String handlerXML = null;

    /**
     *  location of wsdl file
     */
    private String wsdlLocationUnresolved = null;

    /**
     * Naive endpoint URL.
     */
    private String endpointURL = null;

    /**
     * @return the urlPattern
     */
    public String getUrlPattern() {
        return urlPattern;
    }

    /**
     * @param urlPattern the urlPattern to set
     */
    public void setUrlPattern(final String urlPattern) {
        this.urlPattern = urlPattern;
    }

    /**
     * @return the contextRoot
     */
    public String getContextRoot() {
        return contextRoot;
    }

    /**
     * @param contextRoot the contextRoot to set
     */
    public void setContextRoot(final String contextRoot) {
        this.contextRoot = contextRoot;
    }

    /**
     * @return the hostname
     */
    public String getHostname() {
        return hostname;
    }

    /**
     * @param hostname the hostname to set
     */
    public void setHostname(final String hostname) {
        this.hostname = hostname;
    }

    /**
     * @return the HandlerXML
     */
    public String getHandlerXML() {
        return handlerXML;
    }

    /**
     * @param handlerXML the handler xml to set
     */
    public void setHandlerXML(final String handlerXML) {
        this.handlerXML = handlerXML;
    }


    /**
     * @param wsdlLocation the wsdl location to set
     */
    public void setWSDLLocation(final String wsdlLocation) {
        this.wsdlLocationUnresolved = wsdlLocation;
    }

    /**
     * @return the wsdl location
     */
    public String getWSDLLocation() {
        return this.wsdlLocationUnresolved;
    }

    /**
     * @return the local endpoint URL that can be used to access this endpoint (may be null).
     */
    public String getEndpointURL() {
        return endpointURL;
    }

    /**
     * @param endpointURL the local endpoint URL that can be used to access this endpoint.
     */
    public void setEndpointURL(String endpointURL) {
        this.endpointURL = endpointURL;
    }

    @Override
    public String toString() {
        return "PortMetaData[" +
                "urlPattern='" + urlPattern + '\'' +
                ", contextRoot='" + contextRoot + '\'' +
                ", hostname='" + hostname + '\'' +
                ", handlerXML='" + handlerXML + '\'' +
                ", endpointURL='" + endpointURL + '\'' +
                ']';
    }
}
