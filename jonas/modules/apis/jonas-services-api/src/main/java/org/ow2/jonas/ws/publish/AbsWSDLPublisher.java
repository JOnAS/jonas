/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.publish;

import java.util.List;

/**
 * Abstract class for WSDL Publisher. It defines some common methods.
 * @author Florent Benoit
 */
public abstract class AbsWSDLPublisher implements WSDLPublisher {

    /**
     * publish the given list of wsdl
     * @param publishableDefinitionList a list of data containing the WSDL file to be
     *        published and the publication info
     * @throws WSDLPublisherException if an error occurs at the publishing.
     */
    public void publish(final List<PublishableDefinition> publishableDefinitionList) throws WSDLPublisherException {
        if (publishableDefinitionList != null) {
            for (PublishableDefinition publishableDefinition : publishableDefinitionList) {
                publish(publishableDefinition);
            }
        }
    }

}
