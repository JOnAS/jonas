/**
 * JOnAS: Java Open Application Server
 * Copyright (C) 2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.jaxws;

import org.ow2.jonas.service.Service;
import org.ow2.util.ee.metadata.common.api.struct.IJaxwsWebServiceRef;
import org.ow2.util.ee.metadata.war.api.IWarClassMetadata;

import javax.naming.NamingException;
import javax.naming.Reference;
import javax.servlet.ServletContext;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

/**
 * The JAX-WS Service is used to abstract endpoint (server side) and
 * references (client side) creation.
 * This interface needs to be implemented by any JAX-WS provider that
 * we want to support in JOnAS.
 *
 * Warning, this interface is subject to changes.
 * @author Guillaume Sauthier
 */
public interface IJAXWSService extends Service {

    /**
     * The key used to store the webservices related metadata in Context.
     */
    static final String KEY_WEB_SERVICES_METADATAS = "webservices-metadatas";

    /**
     * Construct a Reference dedicated to the given @WebServiceRef metadata.
     * @param serviceRef the reference described by the user.
     * @return a JNDI Naming {@link Reference} that can be bound in a JNDI Context.
     */
    Reference createNamingReference(final IJaxwsWebServiceRef serviceRef) throws NamingException;

    /**
     * Creates a new POJO Web service endpoint from classes metadata.
     * @param metadata the class own metadata (annotation + XML)
     * @param loader the ClassLoader to be used to load classes from the metadata
     * @param servletContext the webapp ServletContext (used to locate web resource)
     * @return A POJO typed IWebServiceEndpoint based on metadata
     * @throws WSException if it was impossible to create the endpoint (CNFE, ...)
     */
    IWebServiceEndpoint createPOJOWebServiceEndpoint(final IWarClassMetadata metadata,
                                                     final ClassLoader loader,
                                                     final ServletContext servletContext)
            throws WSException;

    /**
     * Finalize the deployment of POJO endpoints contained in the given ServletContext.
     * @param context ServletContext.
     */
    void finalizePOJODeployment(ServletContext context);

    /**
     * Stop and undeploy the POJO endpoints contained in the given ServletContext.
     * @param context ServletContext.
     */
    void undeployPOJOEndpoints(ServletContext context);


    SOAPHandler<SOAPMessageContext> getClientAuditHandler();
    SOAPHandler<SOAPMessageContext> getEndpointAuditHandler();
}
