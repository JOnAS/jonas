/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.publish;

import javax.wsdl.Definition;

/**
 * This class wrap a WSDL Definition and a PublicationInfo instance that will be
 * sent to the publisher.
 * @author Florent Benoit
 */
public class PublishableDefinition {

    /**
     * WSDL definition.
     */
    private Definition definition = null;

    /**
     * Publication Info.
     */
    private PublicationInfo publicationInfo = null;

    /**
     * @return WSDl definition
     */
    public Definition getDefinition() {
        return definition;
    }

    /**
     * Sets the WSDL definition.
     * @param definition the given definition
     */
    public void setDefinition(final Definition definition) {
        this.definition = definition;
    }

    /**
     * @return the publication info
     */
    public PublicationInfo getPublicationInfo() {
        return publicationInfo;
    }

    /**
     * Sets the publication info.
     * @param publicationInfo the publication info
     */
    public void setPublicationInfo(final PublicationInfo publicationInfo) {
        this.publicationInfo = publicationInfo;
    }
}
