/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.tm;

import javax.transaction.SystemException;
import javax.transaction.Transaction;

public interface Enlistable {

    /**
     * This method is used by the transaction manager to perform callbacks
     * on the resource manager when a transaction begins.
     *
     * @param transaction the transaction that has begun
     * @throws SystemException if an exception occurs
     */
    void enlistConnection(Transaction transaction) throws SystemException;

    /**
     * Delist Connection from the Transaction.
     * May be called when Tx is suspended, or when going out of a Stateful bean.
     *
     * @param transaction the transaction that has begun
     * @throws SystemException if an exception occurs
     */
    void delistConnection(Transaction transaction) throws SystemException;
}
