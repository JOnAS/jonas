/**
 * JOnAS: Java Open Application Server
 * Copyright (C) 2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.jaxws;

import java.io.IOException;
import java.io.OutputStream;

/**
 * An {@link IWSResponse} represents a web service response to an
 * {@link IWebServiceEndpoint}.
 *
 * @author Guillaume Sauthier
 */
public interface IWSResponse {

    /**
     * @return the {@link OutputStream} to be used for response.
     * @throws IOException when an {@link OutputStream} is not available
     */
    OutputStream getOutputStream() throws IOException;
    void setStatus(int code);
    void setHeader(String name, String value);
    void flushBuffer() throws IOException;
    void setContentType(String type);
}
