/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.tm;

import java.util.List;
import java.util.Properties;

import javax.transaction.xa.XAException;
import javax.transaction.xa.XAResource;


/**
 * JOnAS Transaction Manager interface.
 * Add some methods that lakes in the standard JTA interfaces.
 * - late connection enlistment
 */
public interface TransactionManager extends javax.transaction.TransactionManager {

    public void notifyConnectionOpen(Enlistable mce);

    public void notifyConnectionClose(Enlistable mce);

    public void notifyConnectionError(Enlistable mce);

    public void pushConnectionList(List cl);

    public List popConnectionList();

    public boolean nonJotmTransactionContext();

    /**
     * Register a TransactionResourceManager to the TM
     * @param rmname
     * @param xares XAResource registered
     * @param info
     * @param prop
     * @param trm Object to be called back by the TM or null
     * @throws XAException
     */
    public void registerResourceManager(String rmname,
                                        XAResource xares,
                                        String info,
                                        Properties prop,
                                        TxResourceManager trm)
    throws XAException;

    /**
     * @return true if Recovery is enabled
     */
    public boolean isRecoveryEnabled();

}
