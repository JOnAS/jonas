/*
 * Copyright (C) 2012  Bull S. A. S.
 * Bull, Rue Jean Jaures, B.P.68, 78340, Les Clayes-sous-Bois
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation
 * version 2.1 of the License.
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA  02110-1301, USA.
 */

package org.ow2.jonas.datasource;

import java.util.Map;

/**
 * @author Loic Albertin
 */
public interface DataSourceService {

    /**
     * Deploy a DataSource using the given parameters and returns the JNDI name of the created DataSource.
     * A caching mechanism is implemented and calling this method twice with same parameters will return the same JNDI name.
     *
     * @param className       DataSource implementation class name
     * @param description     Description of the data source
     * @param url             A JDBC url  If the url property is specified along with other standard DataSource properties such as
     *                        serverName and portNumber, the more specific properties will take precedence and the url will be ignored
     * @param user            User name for connection authentications
     * @param password        password for connection authentications
     * @param databaseName    Name of a database on a server
     * @param portNumber      Port number where a server is listening for requests
     * @param serverName      Database server name
     * @param isolationLevel  Isolation level for connections.
     * @param transactional   Indicates whether a connection is transactional or not
     * @param initialPoolSize Number of connections that should be created when a connection pool is initialized
     * @param maxPoolSize     Maximum number of connections that should be concurrently allocated for a connection pool
     * @param minPoolSize     Minimum number of connections that should be allocated for a connection pool
     * @param maxIdleTime     The number of seconds that a physical connection should remain unused in the pool before the connection is
     *                        closed for a connection pool
     * @param maxStatements   The total number of statements that a connection pool should keep open. A value of 0 indicates that
     *                        the caching of statements is disabled for a connection pool
     * @param loginTimeout    The maximum time in seconds that this data source will wait while attempting to connect to a database. A
     *                        value of 0 specifies that the timeout is the default system timeout if there is one,
     *                        otherwise it specifies that there is no timeout
     * @param properties      Used to specify vendor specific properties and less commonly used DataSource properties
     *
     * @return the JNDI name under which the DataSource is registered
     */
    String deployDataSource(final String className, final String description, final String url,
            final String user, final String password, final String databaseName, final int portNumber,
            final String serverName, final int isolationLevel, final boolean transactional, final int initialPoolSize,
            final int maxPoolSize, final int minPoolSize, final int maxIdleTime, final int maxStatements,
            final int loginTimeout, final Map<String, String> properties);

    //TODO see if we need to undeploy datasources
}
