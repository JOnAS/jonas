/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.web;

import java.net.URL;
import java.net.URLClassLoader;
import java.rmi.RemoteException;

import org.ow2.jonas.addon.deploy.api.config.IAddonConfig;
import org.ow2.jonas.service.ConfigService;
import org.ow2.util.ee.deploy.api.deployable.EARDeployable;

import javax.naming.Context;

import org.ow2.jonas.service.Service;
import org.ow2.util.ee.deploy.api.deployable.WARDeployable;

/**
 * JOnAS WEB Container Service interface.
 * This interface provides a description of a web container service.
 * @author Ludovic Bert
 * @author Florent Benoit
 */
public interface JWebContainerService extends ConfigService  {

    /**
     * Deploy the given wars of an ear file with the specified parent
     * classloader (ejb classloader or ear classloader). (This method
     * is only used for the ear applications, not for the
     * web applications).
     * @param ctx the context containing the configuration
     * to deploy the wars.<BR>
     * This context contains the following parameters :<BR>
     *    - urls the list of the urls of the wars to deploy.<BR>
     *    - earURL the URL of the ear application file.<BR>
     *    - parentClassLoader the parent classLoader of the wars.<BR>
     *    - earClassLoader the ear classLoader of the j2ee app.<BR>
     *    - altDDs the optional URI of deployment descriptor.<BR>
     *    - contextRoots the optional context root of the wars.<BR>
     * @throws JWebContainerServiceException if an error occurs during
     * the deployment.
     */
    void deployWars(Context ctx) throws JWebContainerServiceException;

    /**
     * Undeploy the given wars of an ear file with the specified parent
     * classloader (ejb classloader or ear classloader). (This method
     * is only used for the ear applications, not for the
     * war applications).
     * @param urls the list of the urls of the wars to undeploy.
     */
    void unDeployWars(URL[] urls);

    /**
     * Make a cleanup of the cache of deployment descriptor. This method must
     * be invoked after the ear deployment by the EAR service.
     * the deployment of an ear by .
     * @param earClassLoader the ClassLoader of the ear application to
     * remove from the cache.
     */
    void removeCache(ClassLoader earClassLoader);

    /**
     * Return the Default host name of the web container.
     * @return the Default host name of the web container.
     * @throws JWebContainerServiceException when it is impossible to get the Default Host.
     */
    String getDefaultHost() throws JWebContainerServiceException;

    /**
     * Return the Default HTTP port number of the web container (can
     * be null if multiple HTTP connector has been set).
     * @return the Default HTTP port number of the web container.
     * @throws JWebContainerServiceException when it is impossible to get the Default Http port.
     */
    String getDefaultHttpPort() throws JWebContainerServiceException;

    /**
     * Return the Default HTTPS port number of the web container (can
     * be null if multiple HTTPS connector has been set).
     * @return the Default HTTPS port number of the web container.
     * @throws JWebContainerServiceException when it is impossible to get the Default Https port.
     */
    String getDefaultHttpsPort() throws JWebContainerServiceException;

    /**
     * Return the class loader of the given warURL. Unpack the associated war
     * and build the loader if it's not in the cache.
     * @param warURL the url of the war we want to get the loader
     * @param earDeployable the EAR deployable (could be null)
     * the war. May be null in non ear case.
     * @param ejbClassLoader the ejb class loader of the ear.
     * May be null in non ear case.
     * @return the class loader of the given warURL.
     * @throws JWebContainerServiceException if the process failed.
     */
    URLClassLoader getClassLoader(URL warURL, final EARDeployable earDeployable, ClassLoader ejbClassLoader) throws JWebContainerServiceException;

    /**
     * @param warURL the URL of the webapp
     * @return Returns the ClassLoader used to link a JNDI environnment to a webapp
     */
    ClassLoader getContextLinkedClassLoader(URL warURL);

    /**
     * Register a WAR by delegating the operation to the registerWar() method.
     * This is used for JMX management.
     * @param fileName the name of the war to deploy.
     * @throws RemoteException if rmi call failed.
     * @throws JWebContainerServiceException if the registration failed.
     */
    @Deprecated
    void registerWar(String fileName) throws RemoteException, JWebContainerServiceException;

    /**
     * Register a WAR by delegating the operation to the registerWar() method.
     * @param deployable the webapp to be deployed
     * @throws JWebContainerServiceException if the registration failed.
     */
    void registerWar(WARDeployable deployable) throws JWebContainerServiceException;

    /**
     * Test if the specified filename is already deployed or not.
     * @param fileName the name of the war file.
     * @return true if the war is deployed, else false.
     */
    boolean isWarLoaded(String fileName);

    /**
     * Unregister a WAR by delegating the operation to the unRegisterWar()
     * method. This is used for JMX management.
     * @param fileName the name of the war to undeploy.
     * @throws RemoteException if rmi call failed.
     * @throws JWebContainerServiceException if the unregistration failed.
     */
    @Deprecated
    void unRegisterWar(String fileName) throws RemoteException, JWebContainerServiceException;

    /**
     * Un-register a WAR by delegating the operation to the unRegisterWar(Context)
     * method.
     * @param deployable the webapp to be un-deployed.
     * @throws JWebContainerServiceException if the un-registration failed.
     */
    void unRegisterWar(WARDeployable deployable) throws JWebContainerServiceException;

    /**
     * Test if the specified unpack name is already deployed or not.
     * @param unpackName the name of the ear file.
     * @return true if the ear is deployed, else false.
     */
    boolean isWarDeployedByWorkName(final String unpackName);

}
