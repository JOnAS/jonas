/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.mail.factory;

import java.util.Properties;

import javax.naming.Referenceable;

/**
 * Represents a JavaMail resource.
 * @author Guillaume Sauthier
 */
public interface JavaMail extends Referenceable {

    /**
     * Return the jndi name of this object.
     * @return the jndi name
     */
    String getName();

    /**
     * Set the jndi name of this object.
     * @param name the jndi name
     */
    void setName(String name);

    /**
     * Return the name of this mail factory.
     * @return name of this mail factory.
     */
    String getFactoryName();

    /**
     * Return the type of the factory.
     * @return the type of the mail factory
     */
    String getType();

    /**
     * @return Returns the mailSessionProperties.
     */
    Properties getMailSessionProperties();

    /**
     * @param mailSessionProperties The mailSessionProperties to set.
     */
    void setMailSessionProperties(Properties mailSessionProperties);

    /**
     * Retrieves the session properties of this object.
     * @return the session properties of this object.
     */
    Properties getSessionProperties();

    /**
     * Set the session properties.
     * @param props the session properties.
     */
    void setSessionProperties(Properties props);

    /**
     * Retrieves the authentication properties of this object.
     * @return the authentication properties of this object.
     */
    Properties getAuthenticationProperties();

    /**
     * Set the authentication properties.
     * @param props the authentication properties.
     */
    void setAuthenticationProperties(Properties props);

}