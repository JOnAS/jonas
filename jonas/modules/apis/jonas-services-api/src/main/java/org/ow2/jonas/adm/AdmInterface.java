/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): ____________________________________.
 * Contributor(s): ______________________________________.
 * JOnAS 2.4 Murad Meghani (Murad.Meghani@compuware.com) killServer and stopServer
 * JOnAS 2.5 2002.06 Florent Benoit & Ludovic Bert :
 *                   Methods for wars and ear files
 * Dean Jennings = add deployFile
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.adm;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Properties;
import java.util.Vector;

import org.ow2.jonas.adm.AdmException;


/*
 * JOnAS Administration Remote Interface.
 * This is used basically by jonas admin.
 * @author Philippe Coq
 * Contributor(s):
 * Murad Meghani (Murad.Meghani@compuware.com) killServer and stopServer
 * Florent Benoit & Ludovic Bert : Methods for wars and ear files
 */
public interface AdmInterface extends Remote {

    public static final int TYPE_EJB = 1;
    public static final int TYPE_WAR = 2;
    public static final int TYPE_EAR = 3;
    public static final int TYPE_RAR = 4;
    public static final int TYPE_CAR = 5;

    public static final int STATUS_RUNNING = 1;
    public static final int STATUS_STOPPED = 2;
    public static final int STATUS_ALL = 0;

    /**
     * Adm JNDI name suffix.
     */
    public static final String ADMNAME_SUFFIX = "_Adm";

    /**
     * JOnAS instance is NotReady.
     */
    public static final int NOT_READY = 0;

    /**
     * JOnAS instance is Ready.
     */
    public static final int READY = 1;

    /**
     * JOnAS instance is Stopped.
     */
    public static final int STOPPED = 2;


    /**
     * Deploy a given ear file with the help of the ear service.
     * @param fileName the name of the ear file.
     * @throws RemoteException if rmi call failed.
     * @throws AdmException if the deployment failed.
     */
    public void addEar(String fileName) throws RemoteException, AdmException;

    /**
     * Deploy a given rar file with the help of the resource service.
     * @param fileName the name of the rar file.
     * @throws RemoteException if rmi call failed.
     * @throws AdmException if the deployment failed.
     */
    public void addRar(String fileName) throws RemoteException, AdmException;

    /**
     * Deploy a given war file with the help of the web container service.
     * @param fileName the name of the war file.
     * @throws RemoteException if rmi call failed.
     * @throws AdmException if the deployment failed.
     */
    public void addWar(String fileName) throws RemoteException, AdmException;

    /**
     * UnDeploy a given ear file with the help of the ear service.
     * @param fileName the name of the ear file.
     * @throws RemoteException if rmi call failed.
     * @throws AdmException if the undeployment failed.
     */
    public void removeEar(String fileName) throws RemoteException, AdmException;

    /**
     * UnDeploy a given rar file with the help of the resource service.
     * @param fileName the name of the rar file.
     * @throws RemoteException if rmi call failed.
     * @throws AdmException if the undeployment failed.
     */
    public void removeRar(String fileName) throws RemoteException, AdmException;

    /**
     * UnDeploy a given war file with the help of the web container service.
     * @param fileName the name of the war file.
     * @throws RemoteException if rmi call failed.
     * @throws AdmException if the undeployment failed.
     */
    public void removeWar(String fileName) throws RemoteException, AdmException;

    /**
     * Test if the specified filename is already deployed or not
     * @param fileName the name of the ear file.
     * @return true if the ear is deployed, else false.
     * @throws RemoteException if rmi call failed.
     */
    boolean isEarLoaded(String fileName) throws RemoteException, AdmException;

    /**
     * Test if the specified filename is already deployed or not
     * @param fileName the name of the rar file.
     * @return true if the rar is deployed, else false.
     * @throws RemoteException if rmi call failed.
     */
    boolean isRarLoaded(String fileName) throws RemoteException, AdmException;


    /**
     * Test if the specified filename is already deployed or not
     * @param fileName the name of the war file.
     * @return true if the war is deployed, else false.
     * @throws RemoteException if rmi call failed.
     */
    boolean isWarLoaded(String fileName) throws RemoteException, AdmException;


    void addBeans(String fileName) throws RemoteException;
    void removeBeans(String fileName) throws RemoteException;
    boolean isLoaded(String fileName) throws RemoteException;
    String [] listBeans() throws RemoteException;
    String dumpCustom() throws RemoteException;
    Vector listContext() throws RemoteException;
    Properties listEnv() throws RemoteException;
    void stopServer() throws RemoteException;
    void killServer() throws RemoteException;
    boolean isEJBContainer() throws RemoteException;
    int getServerState() throws RemoteException;
    void setTransactionTimeout(int timeout) throws RemoteException;
    void runGC() throws RemoteException;
    void syncAllEntities(boolean passivate) throws RemoteException;
    String [] getTopics() throws RemoteException;
    String getTopicLevel(String topic) throws RemoteException;
    void setTopicLevel(String topic, String l) throws RemoteException;
    void deployFileOn(String filename, String [] target) throws RemoteException;
    void stopRemoteServers(String [] target) throws RemoteException;
    void startRemoteServers(String [] target) throws RemoteException;

}
