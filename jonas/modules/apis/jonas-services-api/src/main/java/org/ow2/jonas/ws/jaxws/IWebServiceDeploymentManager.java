/**
 * JOnAS: Java Open Application Server
 * Copyright (C) 2008-2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.jaxws;

/**
 * An {@link IWebServiceDeploymentManager} is an object that knows how to
 * expose an {@link IWebServiceEndpoint} using some protocol. It is also
 * responsible of its undeployment.<br />
 *
 * For example, a deployment manager that we will probably have to
 * implement is a web container based deployment manager: It will know
 * what to do with the given endpoint to expose it through HTTP (by
 * using a Servlet or anything else that can intercept the request/response
 * flow and invoke the endpoint).
 *
 * @author Guillaume Sauthier
 */
public interface IWebServiceDeploymentManager {

    /**
     * Add support for a given {@link IWebServiceEndpoint} (i.e. deploy).
     *
     * <bold>POJO based web services deployment example:</bold><br />
     * As POJO web services are located inside a web-application (.war), a
     * Context needs to be configured or updated to allow execution
     * of the POJO web service.<br />
     * For example, if a POJO webservice is declared using the
     * <tt>WEB-INF/web.xml</tt>:<br />
     * <pre>
     *   &lt;servlet>
     *     &lt;servlet-name>My POJO WebService&lt;/servlet-name>
     *     &lt;servlet-class>my.company.app.ServiceImplementationBean&lt;/servlet-class>
     *   &lt;/servlet>
     * </pre>
     *
     * Then the <tt>servlet</tt> element's value has to be replaced
     * by a real Servlet implementation.
     * Some other changes are probably needed but not described here
     * as they are implementation specific.<br />
     *
      * <bold>EJB based web service deployment example:</bold><br />
      * The web service has to be deployed in a web container (servlet
      * container), an implementation of this method will create a new
      * Context for the endpoint and then will configure it.
      *
     * @param endpoint the {@link IWebServiceEndpoint} to deploy.
     *
     * @throws WSException if deployment fails or if the {@link IWebServiceEndpoint}
     *         is already registered.
     */
    void registerWSEndpoint(IWebServiceEndpoint endpoint) throws WSException;

    /**
     * Remove support for the given {@link IWebServiceEndpoint}.
     * After a call to this method, the given {@link IWebServiceEndpoint}
     * will not be accessible anymore.
     *
     * @param endpoint the {@link IWebServiceEndpoint} to undeploy.
     *
     * @throws WSException if undeployment fails or if the {@link IWebServiceEndpoint}
     *         is not registered.
     */
    void unregisterWSEndpoint(IWebServiceEndpoint endpoint) throws WSException;
}
