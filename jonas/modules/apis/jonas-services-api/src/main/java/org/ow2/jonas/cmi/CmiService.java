/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007,2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.cmi;

import javax.ejb.EJBObject;

import org.ow2.jonas.service.Service;
import org.ow2.util.event.api.IEventListener;

/**
 * CMI Service interface.
 * It provides a way to use CMI with JOnAS.
 * @author Loris Bouzonnet
 */
public interface CmiService extends Service {

    /**
     * Add a bean to the cluster.
     * @param jndiName name of the bean
     * @param clusterConfigMapping The cluster configuration
     * @param homeClass class of the home interface
     * @param remoteClass class of the remote interface
     * @param classLoader the classloader used by the container of this bean
     * @param stateful true if the bean has a state
     * @param clusterReplicated true if the bean is replicated (ha service is
     *        required)
     * @param env a given environment.
     * @throws Exception if the provided policy of load-balancing is not valid
     */
    void addClusteredObject(
            String jndiName,
            Object clusterConfig,
            Class<?> homeClass,
            Class<? extends EJBObject> remoteClass,
            ClassLoader classLoader,
            boolean stateful,
            boolean clusterReplicated)
    throws Exception;

    /**
     * Remove the given clustered object.
     * @param jndiName Name of the object to remove
     * @throws Exception any
     */
    void removeClusteredObject(String jndiName) throws Exception;

    /**
     * Gets Listener for Bean Events.
     * @return The bean Event Listener.
     * @throws Exception any.
     */
    IEventListener getBeanEventListener() throws Exception;

    /**
     * Set the bean event listener.
     * @param eventListener The bean event listener to set.
     * @throws Exception any.
     */
    void setBeanEventListener(IEventListener eventListener) throws Exception;

}
