/**
 * JOnAS: Java Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.jaxws;

import java.util.Collection;

/**
 * The IWebservicesModule represents all the webservices container included in an archive (ejbjar or webapp).
 *
 * @author Guillaume Sauthier
 */
public interface IWebservicesModule<T extends IWebservicesContainer<? extends IWebServiceEndpoint>> {
    /**
     * Add the given container.
     * @param container added container
     */
    void addContainer(T container);

    /**
     * Remove a given container.
     * @param container removed container
     */
    void removeContainer(T container);

    /**
     * Find a container with the given name.
     * @param name name of the container
     * @return the container or null if none was found.
     */
    T findContainer(String name);

    /**
     * @return All the containers in this module
     */
    Collection<T> getContainers();

    /**
     * Starts the module.
     * Recursively starts inner containers.
     */
    void start();

    /**
     * Stop the module.
     * Recursively stops inner containers.
     */
    void stop();

    /**
     * @return the name of this module
     */
    String getName();
}
