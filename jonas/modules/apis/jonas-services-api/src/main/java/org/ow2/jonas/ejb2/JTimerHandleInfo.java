/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ejb2;

import java.io.Serializable;

/**
 * Hold TimerHandle datas. Used to retrieve a given Timer.
 * @author Guillaume Sauthier
 */
public class JTimerHandleInfo implements Serializable {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = 3580043842820893364L;

    /**
     * The EjbJar containing the wanted Timer.
     */
    private String containerId = null;

    /**
     * The Bean containing the Timer.
     */
    private String beanId = null;

    /**
     * Timer period.
     */
    private long period;

    /**
     * Timer start time.
     */
    private long startTime;

    /**
     * Timer duration.
     */
    private long duration;

    /**
     * Serializable info.
     */
    private Serializable info;

    /**
     * Encoded PK. If CMP2 is not used, no encoding is done.
     */
    private Serializable pk;

    /**
     * @return the beanId
     */
    public String getBeanId() {
        return beanId;
    }

    /**
     * @param beanId the beanId to set
     */
    public void setBeanId(final String beanId) {
        this.beanId = beanId;
    }

    /**
     * @return the containerId
     */
    public String getContainerId() {
        return containerId;
    }

    /**
     * @param containerId the containerId to set
     */
    public void setContainerId(final String containerId) {
        this.containerId = containerId;
    }

    /**
     * @return the duration
     */
    public long getDuration() {
        return duration;
    }

    /**
     * @param duration the duration to set
     */
    public void setDuration(final long duration) {
        this.duration = duration;
    }

    /**
     * @return the info
     */
    public Serializable getInfo() {
        return info;
    }

    /**
     * @param info the info to set
     */
    public void setInfo(final Serializable info) {
        this.info = info;
    }

    /**
     * @return the period
     */
    public long getPeriod() {
        return period;
    }

    /**
     * @param period the period to set
     */
    public void setPeriod(final long period) {
        this.period = period;
    }

    /**
     * @return the pk
     */
    public Serializable getPk() {
        return pk;
    }

    /**
     * @param pk the pk to set
     */
    public void setPk(final Serializable pk) {
        this.pk = pk;
    }

    /**
     * @return the startTime
     */
    public long getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(final long startTime) {
        this.startTime = startTime;
    }

    /**
     * Test equality between 2 {@link JTimerHandleInfo} instances.
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof JTimerHandleInfo)) {
            return false;
        }
        JTimerHandleInfo other = (JTimerHandleInfo) obj;

        if (!other.beanId.equals(beanId)
            || !other.containerId.equals(containerId)
            || other.duration != duration
            || !other.info.equals(info)
            || other.period != period
            || !other.pk.equals(pk)
            || other.startTime != startTime) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        long compound = duration + period + startTime;
        return (beanId + containerId + info + pk + compound).hashCode();
    }

}
