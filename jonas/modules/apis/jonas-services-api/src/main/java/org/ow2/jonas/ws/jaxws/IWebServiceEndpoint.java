/**
 * JOnAS: Java Open Application Server
 * Copyright (C) 2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.jaxws;

/**
 * An {@link IWebServiceEndpoint} is a container around a concrete endpoint
 * implementation (Axis2 or CXF for example, but not limited to). It is mapped
 * to a <code>wsdl:port</code> and is associated to a WSDL that
 * provides a technical description of this web service.<br />
 *
 * It provides a management interface that an administrator can use to
 * "explore" the web service. <br />
 *
 * It also has a runtime interface that has to be used to invoke the web service.
 * This runtime interface is a simple invoke method with 2 parameters
 * ({@link IWSRequest} and {@link IWSResponse}) wrapping input and output
 * messages streams. The {@link IWSRequest} and {@link IWSResponse} interfaces
 * has to be implemented to adapt a given InputStream to the WS stream
 * (like wrapping a HttpServletRequest and a HttpServletResponse for a webcontainer).
 *
 * @author Guillaume Sauthier
 */
public interface IWebServiceEndpoint {

    /**
     * Supported endpoint types: can be EJB (Stateless Session Beans)
     * or POJO.
     */
    enum EndpointType {
        POJO, EJB
    }

    /**
     * @return this {@link IWebServiceEndpoint}'s implementation
     *         type (EJB or POJO)
     */
    EndpointType getType();

    /**
     * @return the port identifier of this endpoint.
     */
    PortIdentifier getIdentifier();

    /**
     * Invoke this web service endpoint. SOAP Message content (XML) can be
     * found in the {@link IWSRequest} object.
     * The response has to be written back into the {@link IWSResponse} object.
     *
     *
     * @param request Request wrapper
     * @param response Response wrapper
     * @throws WSException thrown if anything goes wrong in the
     *         invocation processing
     */
    void invoke(IWSRequest request, IWSResponse response) throws WSException;

    /**
     * Starts the {@link IWebServiceEndpoint}. Can be used for initialization
     * code.
     */
    void start();

    /**
     * Stops the {@link IWebServiceEndpoint}.
     * Any resource used by the endpoint should be cleared after this method's
     * call.
     */
    void stop();

    // management operations

    /**
     * Returns the WSDL for this endpoint.
     */
    //Object getWSDL();

    /**
     * Give access to the port's metadata:
     * <ul>
     *   <li>context-root</li>
     *   <li>url-pattern</li>
     *   <li>hostname</li>
     *   <li>...</li>
     * </ul>
     * @return the metadata informations associated with this endpoint.
     */
    PortMetaData getPortMetaData();

   /**
     * Prints info about the endPoint.
     */
    void displayInfos(); 
}
