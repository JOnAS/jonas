/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.packaging;

import java.io.File;

/**
 * Some constants for Packaging Service
 * @author Mohammed Boukada
 */
public interface IPackagingStructure {

    /**
     * ZIP file extension
     */
    String ZIP_EXTENSION = ".zip";

    /**
     * XML file extension
     */
    String XML_EXTENSION = ".xml";

    /**
     * Work directory
     */
    String WORK_DIRECTORY = "work";

    /**
     * Addons directory
     */
    String ADDONS_DIR = WORK_DIRECTORY + File.separator + "addons";

    /**
     * Deployable directory of an Addon
     */
    String DEPLOY_DIRECTORY = "deploy";

    /**
     * Multitenant repository prefix
     */
    String REPOSITORY_DIR = "repository";

    /**
     * Multitenant deployment plan prefix
     */
    String DEPLOYMENT_PLAN_PREFIX = "plan";

    /**
     * Cloud application xml file name
     */
    String CLOUD_APPLICATION = "cloud-application";

    /**
     * Environment template xml file name
     */
    String ENVRIONMENT_TEMPLATE = "environment-template";

    /**
     * Deployment xml file name
     */
    String DEPLOYMENT = "deployment";

    /**
     * JVM versions for jonas-addon.xml
     */
    String JVM_VERSIONS = "[1.0,2.0]";

    /**
     * Licence for jonas-addon.xml
     */
    String LICENCE = "LGPL";

    /**
     * Author for jonas-addon.xml
     */
    String AUTHOR = "The JOnAS Team";
}
