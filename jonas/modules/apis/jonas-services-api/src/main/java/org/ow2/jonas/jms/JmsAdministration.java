/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.jms;

import javax.jms.Queue;
import javax.jms.Topic;
import javax.jms.XAConnectionFactory;
import javax.jms.XAQueueConnectionFactory;
import javax.jms.XATopicConnectionFactory;


/**
 * JMS Administration interface.
 * must be implemented for each jms provider
 * JOnAS uses this interface to access the JMS provider.
 * @author Philippe Coq
 * Contributor(s):
 *        Jeff Mesnil: for JORAM 3.0 integration
 *        Frederic Maistre: for JORAM 3.4  (JMS 1.1) integration
 * 03/05/25 : Adriana Danes : add JMS resource monitoring
 */
public interface JmsAdministration {

    /**
     * Jms Administrator is created with newInstance().
     * initialization is done later with this method.
     * The MOM will be started if collocated.
     * This method should create an XAConnectionFactory,
     * a XATopicConnectionFactory and a XAQueueConnectionFactory
     * @param boolean true for if the MOM in run in the current JVM
     * @param String url connexion that must be used.
     */
    public void start(boolean collocated, String url) throws Exception;

    /**
     * Stop the Jms Administrator
     */
    public void stop();

    /**
     * Get the XAConnectionFactory
     */
    public XAConnectionFactory getXAConnectionFactory();

    /**
     * Get the XATopicConnectionFactory
     */
    public XATopicConnectionFactory getXATopicConnectionFactory();

    /**
     * Get the XAQueueConnectionFactory
     */
    public XAQueueConnectionFactory getXAQueueConnectionFactory();

    /**
     * Create a Topic
     */
    public Topic createTopic(String name) throws Exception;

    /**
     * Create a Queue
     */
    public Queue createQueue(String name) throws Exception;

    /**
     * Delete a destination.
     */
    public void deleteDestination(String name) throws Exception;

    /**
     * Get number of pending messages on a queue
     */
    public int getPendingMessages(javax.jms.Queue queue) throws Exception;

    /**
     * Get number of pending requests on a queue
     */
    public int getPendingRequests(javax.jms.Queue queue) throws Exception;

    /**
     * Get number of subscriptions on a topic
     */
    public int getSubscriptions(javax.jms.Topic topic) throws Exception;
}
