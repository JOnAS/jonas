/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.jaxws.ejb;

import java.util.Map;

import org.ow2.jonas.ws.jaxws.IWebServiceEndpoint;
import org.ow2.jonas.ws.jaxws.ejb.context.IContextNamingInfo;

/**
 * The IEJBWebserviceEndpoint contains metadata specific to EJB endpoints. 
 *
 * @author Guillaume Sauthier
 */
public interface IEJBWebserviceEndpoint extends IWebServiceEndpoint {

    /**
     * Get the data used to construct a web context.
     * @return the Context naming info structure.
     */
    IContextNamingInfo getContextNamingInfo();

    /**
     * Get the data structure used to secure a bean's endpoint.
     * @return security constraint structure
     */
    ISecurityConstraint getSecurityConstraint();

    /**
     * @return a map of values accessibles after web context deployment.
     */
    Map<String, Object> getDeploymentInfos();

    /**
     * Key of the Map for accessing context name value.
     */
    static final String KEY_CONTEXT_NAME = "context-name";

}
