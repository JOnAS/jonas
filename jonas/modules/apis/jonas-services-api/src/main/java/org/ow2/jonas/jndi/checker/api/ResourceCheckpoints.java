/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 France Telecom R&D
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.jndi.checker.api;

/**
 * Defines the checkpoint where the resource is checked.
 * @author Florent Benoit
 */
public enum ResourceCheckpoints {

    /**
     * Unknown location.
     */
    UNKNOWN,

    /**
     * EJB Post Invoke.
     */
    EJB_POST_INVOKE,

    /**
     * EJB before passivation.
     */
    EJB_PRE_PASSIVATE,

    /**
     * EJB before removal.
     */
    EJB_PRE_DESTROY,

    /**
     * After Servlet call.
     */
    HTTP_AFTER_CALL,

    /**
     * After commit.
     */
    TRANSACTION_COMMITTED,

    /**
     * After rollback.
     */
    TRANSACTION_ROLLEDBACK

}
