/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id:MailService.java 10050 2007-03-07 10:45:32Z sauthieg $
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.mail;

//import java.util
import java.util.Properties;

import org.ow2.jonas.mail.factory.JavaMail;
import org.ow2.jonas.service.Service;

//import jonas

/**
 * This interface provides a description of the JavaMail service.
 * @author Ludovic Bert
 * @author Florent Benoit
 */
public interface MailService extends Service {

    /**
     * Session marker.
     */
    String SESSION_PROPERTY_TYPE = "javax.mail.Session";

    /**
     * MimePart DataSource marker.
     */
    String MIMEPART_PROPERTY_TYPE = "javax.mail.internet.MimePartDataSource";

    /**
     * JOnAS-specific properties (name).
     */
    String PROPERTY_NAME = "mail.factory.name";

    /**
     * JOnAS-specific properties (type).
     */
    String PROPERTY_TYPE = "mail.factory.type";

    /**
     * Create a mail factory with the specified properties and register it into
     * the registry. The created object is an instance of one of the 2 classes :
     * JavaMailSession or JavaMailMimePartDS.
     * @param name the mail factory name
     * @param props the properties used to configure the mail factory.
     * @throws MailServiceException if the creation or the registration of the
     *         factory failed.
     */
    void createMailFactory(String name, Properties props) throws MailServiceException;

    /**
     * This method is used when a Mail Factory configuration is modified via
     * jonasAdmin. In this case, the updated JavaMail object (JavaMailSession or
     * JavaMailMimePartDS object) must be rebound in JNDI
     * @param factory the factory
     * @throws MailServiceException if the recreation of the factory failed.
     */
    void recreateJavaMailFactory(JavaMail factory) throws MailServiceException;

    /**
     * This method is used when a particular Mail Factory configuration
     * operation is done via jonasAdmin : when the JNDI name of this resource is
     * modified. In this case, the initial JavaMail object (JavaMailSession or
     * JavaMailMimePartDS object) must be unbound and the updated JavaMail
     * object must be reloaded. Also, the Mail Service private data structures
     * must be updated.
     * @param oldName old name of the factory
     * @param factory the new factory
     * @throws MailServiceException if the rename of the the factory failed.
     */
    void renameJavaMailFactory(String oldName, JavaMail factory) throws MailServiceException;

    /**
     * Unregister the factory with the given name.
     * @param factoryName the name of the factory to unbind.
     * @throws MailServiceException if the unregistration of the factory
     * failed.
     */
    void unbindMailFactory(final String factoryName) throws MailServiceException;

    /**
     * Unregister all the binding factories on the server.
     * @throws MailServiceException if the unregistration of the factories
     *         failed.
     */
    void unbindMailFactories() throws MailServiceException;

}