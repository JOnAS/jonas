/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): ____________________________________.
 * Contributor(s):
 * JOnAS 2.4 Sebastien Chassande-Barrioz (sebastien.chassande@inrialpes.fr)
 * JOnAS 3.0 Eric Hardesty (Eric.Hardesty@bull.com)
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.resource;

import java.net.URL;
import java.util.List;

import javax.naming.Context;

import org.ow2.jonas.service.Service;


/**
 * JCA resource service implmentation
 * @author Philippe Coq
 * Contributor(s):
 * JOnAS 2.4 Sebastien Chassande-Barrioz (sebastien.chassande@inrialpes.fr)
 * JOnAS 3.0 Eric Hardesty (Eric.Hardesty@bull.com)
 * JOnAS 4.0 Adriana Danes (JSR 77 + use of Jakarta Modeler Component : http://jakarta.apache.org/commons/modeler)
 *           Eric Hardesty (J2CA 1.5)
 *
 */
public interface ResourceService extends Service {

    /**
     * Create a Resource Adapter
     * @param ctx Context that defines the:
     *     1) name of the resource adapter.
     *     2) information to determine if the rar is in an ear
     *
     *        the configuration information for this resource adapter
     *        must be found in the xml files in the rar file
     *        being deployed
     * @return The ObjectName of the MBean associated to the deployed J2EE Application
     * @throws Exception if the create of the RAR object fails.
     */
    String createResourceAdapter(Context ctx) throws Exception;

    /**
     * Deploy an RAR, used by management applications via J2EEServer managed object
     * @param fileName the fileName of the rar which must be be deployed.
     * @return The ObjectName of the MBean associated to the deployed J2EE Application
     * @throws Exception if the deployment of the RAR failed.
     */
    String deployRar(String fileName) throws Exception;

    /**
     * Deploy the given rars of an ear file with the specified parent
     * classloader (ear classloader). (This method is only used for
     * the ear applications).
     * @param ctx Context that defines the:
     * This context contains the following parameters :<BR>
     *    - urls the list of the urls of the rars to deploy.<BR>
     *    - earRootURL the URL of the ear application file.<BR>
     *    - earClassLoader the ear classLoader of the j2ee app.<BR>
     *    - altDDs the optional URI of deployment descriptor.<BR>
     * @throws ResourceServiceException if an error occurs during the deployment.
     */
    void deployRars(Context ctx) throws ResourceServiceException;

    /**
     * Test if the specified filename is already deployed or not.
     * @param fileName the name of the rar file.
     * @return true if the rar is deployed, else false.
     */
    boolean isRarLoaded(String fileName);

    /**
     * Test if the specified filename is already deployed or not.
     * @param fileName the name of the rar file.
     * @return true if the rar is deployed, else false.
     */
    Boolean isRarDeployed(String fileName);

    /**
     * Test if the specified unpack name is already deployed or not. This
     * method is defined in the RarService interface.
     * @param unpackName the name of the rar file.
     * @return true if the rar is deployed, else false.
     */
    boolean isRarDeployedByUnpackName(String unpackName);

    /**
     * Undeploy an RAR, used by management applications via J2EEServer managed object
     * @param fileName the fileName of the rar which must be be undeployed.
     * @throws Exception if the undeployment of the RAR failed.
     */
    void unDeployRar(String fileName) throws Exception;

    /**
     * Undeploy the given rars of an ear file with the specified parent
     * classloader (ear classloader). (This method is only used for
     * the ear applications).
     * @param urls the list of the urls of the rars to deploy.
     * @param earUrl ear's URL
     */
    void unDeployRars(URL[] urls, URL earUrl);

    /**
     * Get a Rar by its jndi name
     */
    org.ow2.jonas.resource.Rar getRar(String jndiName);

}


