/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.jaxws.ejb;

import java.util.List;

/**
 * The ISecurityConstraint gives info about how a Web context, supporting
 * an EJB endpoint, has to be secured.
 *
 * @author Guillaume Sauthier
 */
public interface ISecurityConstraint {

    /**
     * @return the secured Url pattern.
     */
    String getUrlPattern();

    /**
     * @return the list of protected HTTP methods.
     */
    List<String> getHttpMethods();

    /**
     * @return the required guarantee.
     */
    String getTransportGuarantee();

    /**
     * @return the list of references roles in the EJB.
     */
    List<String> getRoleNames();

    /**
     * @return the authentication method's name.
     */
    String getAuthMethod();

    /**
     * @return the name of the realm to be used for authentication.
     */
    String getRealmName();

    // TODO add getJaasEntry
}
