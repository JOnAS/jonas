/*
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Philippe Durieux
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.resource;

import java.util.List;

import javax.resource.spi.ActivationSpec;
import javax.resource.spi.ResourceAdapter;

/**
 * This interface is used for example in the ejb container library.
 * It avoids a dependency on the Resource Service.
 * @author durieuxp
 */
public interface Rar {

    /**
     * Get the messagelistenerType
     * @param jndiname jndi name of the Rar
     * @return the String messagelistenerType
     */
    public String getInterface(String jndiname);

    /**
     * @return The associated ResourceAdapter
     */
    public ResourceAdapter getResourceAdapter();

    /**
     * Configure ActivationSpec
     * @param as ActivationSpec to be configured
     * @param acp List of activation properties
     * @param jacp List of JOnAS activation properties
     * @param jndiname String destination desired
     * @param ejbName ejb Name
     */
    public void configureAS(ActivationSpec as,
                            List acp,
                            List jacp,
                            String jndiname,
                            String ejbName) throws Exception;
}
