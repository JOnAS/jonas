/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.smartclient;

/**
 * JOnAS Service interface for the Smartclient service. This service lets
 * remote clients download classes necessary for connecting to JOnAS services
 * (JNDI context factories, EJB3 interceptors, ...) directly from the JOnAS
 * server they're dealing with.
 *
 * This way, the heavy clients only need to include a lightweight JAR file for
 * the Smartclient client and are always guaranteed to have the good versions
 * of all components.
 *
 * Check the EasyBeans documentation on "SmartClient" for details.
 *
 * @author S. Ali Tokmen
 */
public interface SmartclientServiceBase {

    /**
     * @return true if the EasyBeans Smartclient is active, false otherwise.
     */
    boolean isActive();

    /**
     * @return The port the smart client is listening on.
     */
    int getPort();

}
