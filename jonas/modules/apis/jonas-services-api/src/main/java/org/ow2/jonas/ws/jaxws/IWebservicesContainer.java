/**
 * JOnAS: Java Open Application Server
 * Copyright (C) 2009 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.ws.jaxws;

import java.util.Collection;

/**
 * The IWebservicesContainer is a group of webservices endpoints sharing the same WSDL definition.
 *
 * @author Guillaume Sauthier
 */
public interface IWebservicesContainer<T extends IWebServiceEndpoint> {
    /**
     * Add a new endpoint in the group.
     * @param endpoint newly added endpoint.
     */
    void addEndpoint(T endpoint);

    /**
     * Remove an endpoint from the group.
     * @param endpoint removed endpoint.
     */
    void removeEndpoint(T endpoint);

    /**
     * Given a service/port pair, try to find a matching endpoint URL.
     * @param identifier Port identifier
     * @return an endpoint URL or null if no match was found.
     */
    String findUpdatedURL(PortIdentifier identifier);

    /**
     * @return the name of this container (WSDL location)
     */
    String getName();

    /**
     * @return All the inner endpoints.
     */
    Collection<T> getEndpoints();

    /**
     * Starts the container.
     * Recursively starts inner endpoints.
     */
    void start();

    /**
     * Stops the container.
     * Recursively stops inner endpoints.
     */
    void stop();

    /**
     * Set the WSDL publication directory for this WSDL.
     * @param wsdlPublicationDirectory directory
     */
    void setWsdlPublicationDirectory(String wsdlPublicationDirectory);

    /**
     * @return the WSDL publication directory for this WSDL (may be null).
     */
    String getWsdlPublicationDirectory();
}
