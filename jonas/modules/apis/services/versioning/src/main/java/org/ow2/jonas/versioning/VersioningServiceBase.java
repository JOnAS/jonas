/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * Application Versioning System
 * Copyright (C) 2008 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.versioning;

import java.net.URL;

/**
 * Interface for the versioning service, which only contains methods for the
 * versioning service and doesn't inherit from any other interface. This
 * interface will be exposed via JMX.
 * @author Frederic Germaneau
 * @author S. Ali Tokmen
 */
public interface VersioningServiceBase {

    /**
     * Policy for a default context. All users that cannot access other contexts
     * will access the default context.
     */
    String DEFAULT = "Default";

    /**
     * Policy for a disabled context. Only old users on that context are allowed
     * to access it, new users never get redirected to such a context.
     */
    String DISABLED = "Disabled";

    /**
     * Policy for a reserved context. Only users that know the exact address of
     * this context can access it.
     */
    String RESERVED = "Reserved";

    /**
     * Policy for a private context.
     */
    String PRIVATE = "Private";

    /**
     * Possible policies.
     */
    String[] POLICIES = {DEFAULT, DISABLED, RESERVED, PRIVATE};

    /**
     * @return Possible policies.
     */
    String[] getPolicies();

    /**
     * @return Whether versioning is enabled.
     */
    boolean isVersioningEnabled();

    /**
     * @return The default deployment policy.
     */
    String getDefaultDeploymentPolicy();

    /**
     * @param defaultPolicy Default deployment policy to set.
     * @throws IllegalArgumentException If defaultPolicy is not in
     *         {@link VersioningServiceBase#POLICIES}.
     */
    void setDefaultDeploymentPolicy(String defaultPolicy) throws IllegalArgumentException;

    /**
     * @return The filtered content types. Each type is separated with a comma.
     *         See {@see VersioningServiceBase#getFilteredContentTypesArray()}
     *         if you need to access the content types for using them in an
     *         application (the usage that other method is recommended for
     *         performance reasons).
     */
    String getFilteredContentTypes();

    /**
     * @param filteredContentTypes The filtered content types to set. Each type
     *        must be separated with a comma, any spacing character between
     *        each type is omitted.
     */
    void setFilteredContentTypes(String filteredContentTypes);

    /**
     * @return The filtered content types, each type is another element of the
     *         returned array. DO NOT MODIFY THE RETURNED ARRAY !!
     */
    String[] getFilteredContentTypesArray();

    /**
     * @param url JAR, WAR or EAR file to read the version number from.
     * @return Version number of url, null if none found.
     */
    String getVersionNumber(URL url);

    /**
     * @param url JAR, WAR or EAR file to read the version ID from.
     * @return Version ID of url, null if none found.
     */
    String getVersionID(URL url);

    /**
     * @param url JAR or EAR object to create a JNDI prefix from.
     * @return JNDI prefix for url, null if none found.
     */
    String getPrefix(URL url);

    /**
     * @param url JAR, WAR or EAR object to get the base name for. The base name
     *        is used when creating the JNDI naming prefix.
     * @return Base name for URL, null if none found.
     */
    String getBaseName(URL url);
}
