/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * Application Versioning System
 * Copyright (C) 2008 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.versioning;

import java.io.File;

import org.ow2.jonas.service.Service;
import org.ow2.util.ee.deploy.api.deployable.IDeployable;

/**
 * JOnAS Service interface for the versioning service. This interface combines
 * the versioning service's JMX interface with the JOnAS Service interface. It
 * also adds internal methods for managing contexts, obtaining version
 * information from files and watching JNDI bindings.
 *
 * @author Frederic Germaneau
 * @author S. Ali Tokmen
 */
public interface VersioningService extends Service, VersioningServiceBase {

    /**
     * List of allowed special characters in version identifiers and JNDI
     * prefixes. If a non-alphanumeric character other than these is found, it
     * will be replaced by a '-'.
     */
    String ALLOWED_SPECIAL_CHARS = "-_,.";

    /**
     * @param file JAR, WAR or EAR file to read the version number from.
     * @return Version number of file, null if none found.
     */
    String getVersionNumber(File file);

    /**
     * @param deployable JAR, WAR or EAR object to read the version number from.
     * @return Version number of deployable, null if none found.
     */
    String getVersionNumber(IDeployable<?> deployable);

    /**
     * @param file JAR, WAR or EAR file to read the version ID from.
     * @return Version ID of file, null if none found.
     */
    String getVersionID(File file);

    /**
     * @param deployable JAR, WAR or EAR object to read the version ID from.
     * @return Version ID of deployable, null if none found.
     */
    String getVersionID(IDeployable<?> deployable);

    /**
     * @param file JAR or EAR object to create a JNDI prefix from.
     * @return JNDI prefix for file, null if none found.
     */
    String getPrefix(File file);

    /**
     * @param deployable JAR or EAR object to create a JNDI prefix from.
     * @return JNDI prefix for deployable, null if none found.
     */
    String getPrefix(IDeployable<?> deployable);

    /**
     * Creates JNDI binding management beans for a given archive.
     * @param deployable JAR, WAR or EAR object to read the JNDI prefix from.
     */
    void createJNDIBindingMBeans(IDeployable<?> deployable);

    /**
     * Removes JNDI binding management beans that are not in the JNDI
     * directory anymore.
     */
    void garbageCollectJNDIBindingMBeans();

    /**
     * @param versioningEnabled Whether versioning is enabled.
     */
    void setVersioningEnabled(boolean versioningEnabled);
}
