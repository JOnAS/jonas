/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * Application Versioning System
 * Copyright (C) 2008 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.versioning;

import java.util.Map;

/**
 * JMX Management Interface for any virtual context.
 * @author Frederic Germaneau
 * @author S. Ali Tokmen
 */
public interface VirtualContextJMXInterface {
    /**
     * Checks whether a given context has been registered.
     * @param versionedPath Versioned path of the context.
     * @return true if found, false otherwise.
     */
    boolean hasContext(String versionedPath);

    /**
     * @return All versioned contexts with their policies for this virtual
     *         context.
     */
    Map<String, String> getContexts();

    /**
     * Rebinds a context.
     * @param versionedPath Versioned path of the context.
     * @param policy New policy.
     * @return true if succeeded, false otherwise.
     */
    boolean rebindContext(String versionedPath, String policy);

    /**
     * Removes this virtual context.
     * @return true if succeeded, false otherwise.
     */
    boolean removeVirtualContext();
}
