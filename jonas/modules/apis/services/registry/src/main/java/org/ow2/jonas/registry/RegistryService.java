/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.registry;

import java.net.URI;
import java.util.List;

import javax.naming.InitialContext;

import org.ow2.jonas.service.Service;


/**
 * Registry Service interface.
 * @author Julien Lehembre (Libelis)
 */
public interface RegistryService extends Service {

    /**
     * @return Returns a {@link java.util.List} of currently active protocols
     * names (rmi, irmi, iiop, ...).
     */
    List<String> getActiveProtocolNames();

    /**
     * @param protocolName protocol name
     * @return Returns the given protocol provider URL.
     */
    URI getProviderURL(String protocolName);

    /**
     * @param protocolName protocol name
     * @return Returns the given protocol port where objects are
     * exported. Returns <code>0</code> if unset.
     */
    int getExportedObjectPort(String protocolName);

    /**
     * @param protocolName protocol name
     * @return Returns the {@link javax.naming.spi.InitialContextFactory} classname
     * of the given protocol.
     */
    String getInitialContextFactoryName(String protocolName);

    /**
     * @return Returns the default protocol name.
     */
    String getDefaultProtocolName();

    /**
     * Set the current protocol to the given parameter.
     * @param protocolName the new default protocol name.
     */
    void setDefaultProtocol(String protocolName);

    /**
     * Gets the initial context.
     * @return the initial context
     */
    InitialContext getRegistryContext();
}
