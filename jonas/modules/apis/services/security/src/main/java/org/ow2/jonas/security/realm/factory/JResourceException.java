/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.security.realm.factory;

/**
 * The class JResourceException indicates conditions that a reasonable
 * application might want to catch.
 * @author Ludovic Bert
 * @author Florent Benoit
 */
public class JResourceException extends Exception {

    /**
     * Uid for serializable class.
     */
    private static final long serialVersionUID = -4879978965028070035L;

    /**
     * Constructs a new JResourceException with the specified message.
     * @param message the detail message.
     */
    public JResourceException(String message) {
        super(message);
    }

    /**
     * Constructs a new JResourceException exception with the specified detail message and
     * cause.  <p>Note that the detail message associated with
     * <code>cause</code> is <i>not</i> automatically incorporated in
     * this runtime exception's detail message.
     * @param  message the detail message.
     * @param  cause the cause
     */
    public JResourceException(String message, Throwable cause) {
        super(message, cause);
    }

}