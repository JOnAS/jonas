/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.security.realm.principal;

import java.util.ArrayList;

import org.ow2.jonas.security.realm.lib.HashPassword;


/**
 * This interface represents a user in a security realm.
 * @author Guillaume Sauthier
 */
public interface JUser {

    /**
     * Set the name of this user
     * @param name Name of the user
     */
    void setName(String name);

    /**
     * Get the name of this user
     * @return the name of this user
     */
    String getName();

    /**
     * Get the password of this user
     * @return the password of this user
     */
    String getPassword();

    /**
     * Set the password of this user
     * @param password password of the user
     */
    void setPassword(String password);

    /**
     * Set the hashed password of this user
     * @return hashPassword hashed password of this user
     */
    HashPassword getHashPassword();

    /**
     * Set the groups of the user
     * @param groups the comma separated list of the groups of the user
     */
    void setGroups(String groups);

    /**
     * Get the groups
     * @return the comma separated list of groups
     */
    String getGroups();

    /**
     * Get the groups
     * @return the array of the groups
     */
    String[] getArrayGroups();

    /**
     * Set the roles of the user
     * @param roles the comma separated list of the roles of the user
     */
    void setRoles(String roles);

    /**
     * Add the specified group to this user
     * @param group the group to add
     */
    void addGroup(String group);

    /**
     * Add a role to this user
     * @param role the given role
     */
    void addRole(String role);

    /**
     * Remove a group from this user
     * @param group the given group
     */
    void removeGroup(String group);

    /**
     * Remove a role from this user
     * @param role the given role
     */
    void removeRole(String role);

    /**
     * Get the roles
     * @return the array of the roles
     */
    String getRoles();

    /**
     * Set the combined roles of this user
     * @param combinedRoles combined of the user
     */
    void setCombinedRoles(ArrayList combinedRoles);

    /**
     * Get the combined roles of this user
     * @return the combined of the user
     */
    ArrayList getCombinedRoles();

    /**
     * Get the roles
     * @return the array of the roles
     */
    String[] getArrayRoles();

    /**
     * String representation of the user
     * @return the xml representation of the user
     */
    String toXML();

}