/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.security.realm.factory;

import java.util.ArrayList;
import java.util.Hashtable;

import javax.naming.NamingException;
import javax.naming.Reference;

import org.ow2.jonas.jmx.JmxService;
import org.ow2.jonas.security.SecurityService;
import org.ow2.jonas.security.realm.principal.JUser;


/**
 * This interface represents JOnAS realm factory objects.
 * @author Guillaume Sauthier
 */
public interface JResource {

    /**
     * Set the name of this resource.
     * @param name Name of the resource
     */
    void setName(String name);

    /**
     * Get the name of this resource.
     * @return the name of this resource
     */
    String getName();

    /**
     * Set the jmx service reference
     * @param jmxService the jmx service reference
     */
    void setJmxService(JmxService jmxService);

    /**
     * Set the domain name
     * @param domain the domain name
     */
    void setDomainName(String domain);

    /**
     * Set the security service reference
     * @param sec security service reference
     */
    void setSecurityService(SecurityService sec);

    /**
     * Retrieves the Reference of the object. The Reference contains the factory
     * used to create this object and the optional parameters used to configure
     * the factory.
     * @return the non-null Reference of the object.
     * @throws javax.naming.NamingException if a naming exception was encountered while
     *         retrieving the reference.
     */
    Reference getReference() throws NamingException;

    /**
     * Remove all the Mbeans used by this resource.
     * @throws org.ow2.jonas.security.realm.factory.JResourceException if the MBeans can not be removed
     */
    void removeMBeans() throws JResourceException;

    /**
     * Check if a user is found and return it.
     * @param name the wanted user name
     * @return the user found or null
     * @throws org.ow2.jonas.security.realm.factory.JResourceException if there is an error during the search
     */
    JUser findUser(String name) throws JResourceException;

    /**
     * Check if the given credential is the right credential for the given user.
     * @param user user to check its credentials
     * @param credentials the given credentials
     * @return true if the credential is valid for this user
     */
    boolean isValidUser(JUser user, String credentials);

    /**
     * Get all the roles (from the roles and from the groups) of the given user.
     * @param user the given user
     * @return the array list of all the roles for a given user
     * @throws org.ow2.jonas.security.realm.factory.JResourceException if it fails
     */
    ArrayList<String> getArrayListCombinedRoles(JUser user) throws JResourceException;

    /**
     * Return users.
     * @return Return users
     */
    Hashtable<String, JUser> getUsers();

    /**
     * @param users The users to set.
     */
    void setUsers(Hashtable<String, JUser> users);

    /**
     * Clear the cache.
     */
    void clearCache();

}