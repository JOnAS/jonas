/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer: Florent Benoit
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.security.realm.lib;

import java.io.Serializable;

/**
 * This class implements a way for storing the hash of a password and the
 * correspondig algorithm
 * @author Florent Benoit
 */
public class HashPassword implements Serializable {

    /**
     * Hash password
     */
    private String password;

    /**
     * Algorithm used
     */
    private String algorithm;

    /**
     * Constructor
     * @param password the hash of the password
     * @param algorithm algorithm of the encoded password
     */
    public HashPassword(String password, String algorithm) {
        this.password = password;
        this.algorithm = algorithm;
    }

    /**
     * Return the hash of the password
     * @return the hash of the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Return the algorithm used for this password
     * @return the algorithm used for this password
     */
    public String getAlgorithm() {
        return algorithm;
    }

}