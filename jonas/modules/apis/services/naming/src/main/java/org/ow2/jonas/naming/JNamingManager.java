/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.naming;

import javax.naming.Context;
import javax.naming.NamingException;
import java.util.Hashtable;
import javax.naming.InitialContext;


/**
 * Provide naming services for JOnAS containers. Containers use this
 * interface for binding the environment entries, remote object references,
 * resource factories and for managing the naming context of the current
 * thread.
 */
public interface JNamingManager {

    /**
     * Return the Context associated with the current thread.
     *
     * @return Context for current thread.
     * @throws NamingException If context namespace does not exist.
     */
    Context getComponentContext() throws NamingException;

    /**
     * Associate this Context with the current thread.
     * This method should be called in preinvoke/postinvoke
     * and when we build the bean environment.
     *
     * @param ctx Context to associate with the current thread.
     * @return Previous context setting for current thread.
     */
    Context setComponentContext(Context ctx);

    /**
     * Set back the context with the given value.
     * Don't return the previous context, use setComponentContext() method for this.
     * @param ctx the context to associate to the current thread.
     */
    void resetComponentContext(Context ctx);

    /**
     * Associate the specified ComponentContext with the given classloader.
     * @param ctx the context to associate to the classloader.
     * @param cl the classloader which is bind to the context.
     */
    void setComponentContext(Context ctx, ClassLoader cl);

    /**
     * Set the context used by client container (per JVM instead of per thread).
     * @param ctx the context to set
     */
    void setClientContainerComponentContext(Context ctx);

    /**
     * Return the ComponentContext associated with the given classloader.
     * @param cl the classloader which is bind to the context.
     * @return the ComponentContext associated with the given classloader.
     */
    Context getComponentContext(ClassLoader cl);

    /**
     * Remove the ComponentContext associated with the given classloader.
     * @param cl the classloader which is bind to the context.
     */
    void unSetComponentContext(ClassLoader cl);

    /**
     * Gets the initial context.
     * @return the initial context
     */
    InitialContext getInitialContext();

    /**
     * @return the environment associated to this initial context
     */
    Hashtable getEnv();

    /**
     * Set the unique instance of the JComponentContextFactory.
     * @param factory the component context factory
     */
    void setJComponentContextFactory(JComponentContextFactory factory);


}
