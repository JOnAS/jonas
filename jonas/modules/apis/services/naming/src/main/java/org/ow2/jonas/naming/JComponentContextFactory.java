/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.naming;

import javax.naming.Context;
import javax.naming.NamingException;

/**
 * A <code>JComponentContextFactory</code> is a factory for JOnAS Component's Context.
 *
 * @author Guillaume Sauthier
 * @see org.ow2.jonas.lib.naming.ComponentContext
 */
public interface JComponentContextFactory {

    /**
     * Create {@link Context} for component environments.
     * The returned context is a Java EE Component Context.
     * It contains pre-defined references (according to
     * the registered {@link JComponentContextFactoryDelegate}):
     * <ul>
     * <li><code>java:comp/ORB</code></li>
     * <li><code>java:comp/HandleDelegate</code></li>
     * <li><code>java:comp/UserTransaction</code></li>
     * </ul>
     *
     * @param id the Context ID.
     *
     * @return Naming {@link Context} for component environment
     *
     * @throws NamingException If exception encountered when creating namespace.
     */
    Context createComponentContext(String id) throws NamingException;

    /**
     * Create {@link Context} for component environments. The returned context
     * is a Java EE Component Context.
     *
     * @param id            the Context ID.
     * @param moduleContext the application context shared by all the components
     *                      in a module
     * @param appContext    the application context shared by all the applications
     *
     * @return Naming {@link Context} for component environment
     *
     * @throws NamingException If exception encountered when creating namespace.
     */
    Context createComponentContext(final String id, final Context moduleContext, final Context appContext) throws NamingException;

    /**
     * Add the given {@link JComponentContextFactoryDelegate} to this NamingManager instance.
     *
     * @param extension Added delegate
     *
     * @throws NamingException if the delegates is not added.
     */
    void addDelegate(JComponentContextFactoryDelegate extension) throws NamingException;

    /**
     * Remove the given {@link JComponentContextFactoryDelegate} from this NamingManager instance.
     *
     * @param extension Removed delegate
     *
     * @throws NamingException if the delegates is not removed.
     */
    void removeDelegate(JComponentContextFactoryDelegate extension) throws NamingException;
}
