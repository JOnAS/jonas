/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.naming;

import javax.naming.Context;
import javax.naming.NamingException;

/**
 * The <code>JComponentContextFactoryDelegate</code> interface is used to
 * separate the NamingManager implementation of other container
 * implementation details concerning the component Context content.
 * <br/>
 * Example: Add the <code>java:comp/ORB</code> CORBA ORB instance in the Context.
 * @author Guillaume Sauthier
 */
public interface JComponentContextFactoryDelegate {

    /**
     * Gives the possibility to modify the <code>java:comp</code> component Context.
     * @param componentContext <code>java:comp/</code> component context to be modified.
     * @throws NamingException thrown if something goes wrong
     *         during the {@link javax.naming.Context} update.
     */
    void modify(Context componentContext) throws NamingException;

    /**
     * Undo the changes done by this delegate on the given java:comp context.
     * @param componentContext <code>java:comp/</code> component context to be modified.
     * @throws NamingException thrown if something goes wrong
     *         during the {@link javax.naming.Context} update.
     */
    void undo(Context componentContext) throws NamingException;
}
