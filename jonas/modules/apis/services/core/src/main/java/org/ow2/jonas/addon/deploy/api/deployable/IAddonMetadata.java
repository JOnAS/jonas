/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.addon.deploy.api.deployable;

import java.io.File;
import java.util.List;

import org.ow2.jonas.properties.ServiceProperties;

/**
 * Represents metadata of an Addon
 * @author Jeremy Cazaux
 */
public interface IAddonMetadata {

    /**
     * Name element
     */
    public static final String NAME_ELEMENT = "name";

    /**
     * Description element
     */
    public static final String DESCRIPTION_ELEMENT = "description";

    /**
     * Tennant id element
     */
    public static final String TENNANT_ID_ELEMENT = "tenant-id";

    /**
     * Instance element
     */
    public static final String INSTANCE_ELEMENT = "instance";

    /**
     * Licence element
     */
    public static final String LICENCE_ELEMENT = "licence";

    /**
     * JOnAS version element
     */
    public static final String JONAS_VERSION_ELEMENT = "jonas-version";

    /**
     * Autostart element
     */
    public static final String AUTOSTART_ELEMENT = "autostart";

    /**
     * JVM element
     */
    public static final String JVM_VERSION_ELEMENT = "jvm-version";

    /**
     * Provides element
     */
    public static final String PROVIDES_ELEMENT = "provides";

    /**
     * Requirements element
     */
    public static final String REQUIREMENTS_ELEMENT = "requirements";

    /**
     * Properties element
     */
    public static final String PROPERTIES_ELEMENT = "properties";

    /**
     * Author element
     */
    public static final String AUTHOR_ELEMENT = "author";

    /**
     * Separator between two provide properties
     */
    public static final String PROVIDES_SEPARATOR = " ";

    /**
     * Separator between two requirement properties
     */
    public static final String REQUIREMENT_SEPARATOR = " ";

    /**
     * @return the name
     */
    String getName();

    /**
     * @return the description
     */
    String getDescription();

    /**
     * @return the tenant-id
     */
    String getTenantId();

    /**
     * @return the instance
     */
    String getInstance();

    /**
     * @return the author
     */
    String getAuthor();

    /**
     * @return the licence
     */
    String getLicence();

    /**
     * Check if the given jonas version is support
     * @param jonasVersion  The jonas version to check
     */
    boolean isJOnASVersionSupported(final String jonasVersion);

    /**
     * Check if the given jvm version is support
     * @param jvmVersion  The jvm version to check
     */
    boolean isJvmVersionSupported(final String jvmVersion);

    /**
     * @return the autostart property
     */
    Boolean getAutostart();

    /**
     * @return provides properties for the resolver
     */
    List<String> getProvides();

    /**
     * @return requirements properties
     */
    List<String> getRequirements();

    /**
     * @return properties of the addon
     */
    ServiceProperties getServiceProperties();

    /**
     * @return the metadata file
     */
    File getMetaDataFile();

    /**
     * @return the name of the JOnAS service associated to the addon. Null if the addon is not a JOnAS service
     */
    String getService();

    /**
     * @return the name of the implementation of the JOnAS service
     */
    String getImplementation();

    /**
     * @return true if the addon represents a JOnAS service
     */
    boolean isJOnASService();
}
