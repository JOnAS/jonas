/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull S.A.S
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.workcleaner;


/**
 * Give access to a WorkCleaner service implementation.
 * @author Francois Fornaciari
 */
public interface WorkCleanerService {

    /**
     * Register a new clean task.
     * @param cleanTask the task to add
     * @throws org.ow2.jonas.workcleaner.WorkCleanerException if the task cannot be registred
     */
    void registerTask(final CleanTask cleanTask) throws WorkCleanerException;

    /**
     * Unregister a clean task.
     * @param cleanTask The task to unregister
     * @throws org.ow2.jonas.workcleaner.WorkCleanerException If the task cannot be unregistered
     */
    void unregisterTask(final CleanTask cleanTask) throws WorkCleanerException;

    /**
     * Execute the registered tasks.
     */
    void executeTasks();
}
