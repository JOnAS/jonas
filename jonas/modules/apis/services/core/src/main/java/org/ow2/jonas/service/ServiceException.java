/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): ____________________________________.
 * Contributor(s): ______________________________________.
 *
 * 01/06/15 Regis Le Brettevillois - Libelis / JOnAS team - Evidian
 *          Creation.
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.jonas.service;

/**
 * Exception thrown by JOnAS Services.
 */
public class ServiceException extends RuntimeException {

    /**
     * Constructor specifying only the cause message.
     * @param message Exception description
     */
    public ServiceException(String message) {
        this(message, null);
    }

    /**
     * Constructor with message description + cause
     * @param message Exception description
     * @param throwable cause of the Exception
     */
    public ServiceException(String message, Throwable throwable) {
        super(message, throwable);
    }

    /**
     * @return Returns the message description of the Exception
     */
    public String getMessage() {
        String msg = super.getMessage();
        if (getCause() != null) {
            msg += ": " + getCause();
        }
        return msg;
    }

    /**
     * @return Returns a String representation of the Exception
     */
    public String toString() {
        return getClass().getName() + " : " + getMessage();
    }

}
