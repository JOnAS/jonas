/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2012 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.multitenant;

import org.ow2.jonas.service.Service;
import org.ow2.util.ee.deploy.api.deployable.IDeployable;

/**
 * JOnAS Service interface for the multitenant service.
 * @author Mohammed Boukada
 */
public interface MultitenantService extends Service {
    /**
     * Policy for a default context. All users that cannot access other contexts
     * will access the default context.
     */
    String DEFAULT = "Default";

    /**
     * Policy for a disabled context. Only old users on that context are allowed
     * to access it, new users never get redirected to such a context.
     */
    String DISABLED = "Disabled";

    /**
     * Policy for a reserved context. Only users that know the exact address of
     * this context can access it.
     */
    String RESERVED = "Reserved";

    /**
     * Policy for a private context.
     */
    String PRIVATE = "Private";

    /**
     * Possible policies.
     */
    String[] POLICIES = {DEFAULT, DISABLED, RESERVED, PRIVATE};

    /**
     * @return The default deployment policy.
     */
    String getDefaultDeploymentPolicy();

    /**
     * Returns the default tenantId value
     * @return the default tenantId value
     */
    String getDefaultTenantID();

    /**
     * Creates JNDI binding management beans for a given tenant identifier.
     * @param deployable JAR, WAR or EAR object.
     * @param tenantId tenant identifier of the application which will prefix JNDI names.
     */
    void createJNDIBindingMBeans(final IDeployable<?> deployable, final String tenantId);

    /**
     * Removes JNDI binding management beans that are not in the JNDI directory
     * anymore.
     */
    void garbageCollectJNDIBindingMBeans();

    /**
     * Add tenantId as extension to the earDeployable
     * @param deployable application deployable
     * @param tenantId tenant identifier to add
     */
    void addTenantIdDeployableInfo(IDeployable deployable, String tenantId);

    /**
     * Gets tenantIdInfo stored in the deployable
     * @param deployable application deployable
     * @return tenantIdInfo
     */
    String getTenantIdDeployableInfo(IDeployable deployable);

    /**
     * Tell if an application is multitenant
     * @param deployable application deployable
     * @return is multitenant, or not.
     */
    boolean isMultitenant(IDeployable deployable);

    /**
     * Gets tenantContext from TenantCurrent
     * @return tenantContext
     */
    Object getTenantContext();

    /**
     * Gets tenant id from TenantContext
     * @return tenant id
     */
    String getTenantIdFromContext();

    /**
     * Set tenant id in the TenantContext
     * @param tenantId to add in TenantContext
     */
    void setTenantIdInContext(String tenantId);
    
    /**
     * Set TenantContext in the Thread
     * @param ctx
     */
    void setTenantContext(Object ctx);

    /**
     * Set application instance name in TenantContext
     * @param instanceName
     */
    void setInstanceNameInContext(String instanceName);

    /**
     * Get application instance name from TenantContext
     * @return
     */
    String getInstanceNameFromContext();

    /**
     * Set TenantId and Instance Name in TenantContext
     * @param tenantId
     * @param instanceName
     */
    void setTenantIdAndInstanceNameInContext(String tenantId, String instanceName);
}
