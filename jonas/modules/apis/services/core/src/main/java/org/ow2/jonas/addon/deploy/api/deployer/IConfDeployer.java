/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.addon.deploy.api.deployer;

import org.ow2.jonas.addon.deploy.api.deployable.IAddonDeployable;
import org.ow2.util.ee.deploy.api.deployer.DeployerException;

/**
 * Represents a configuration deployer
 * @author Jérémy Cazaux
 */
public interface IConfDeployer {

    /**
     * install the configuration of an Addon
     * @param deployable The unpacked addon deployable
     */
    void install(IAddonDeployable deployable) throws DeployerException;

    /**
     * Add access to the configuration of an Addon through the OSGi registry
     * @param addonName The name of the addon to start
     */
    void start(String addonName) throws DeployerException;

    /**
     * Remove access to the configuration of an Addon
     * @param addonName The name of the addon to start
     */
    void stop(String addonName) throws DeployerException;

    /**
     * uninstall the configuration of an Addon
     * @param addonName The name of the addon to uninstall
     */
    void uninstall(String addonName) throws DeployerException;

    /**
     * deploy the configuration of an Addon
     * @param unpackedDeployable The unpacked addon to deploy
     */
    void deploy(IAddonDeployable unpackedDeployable) throws DeployerException;

    /**
     * Undeploy the configuration of an Addon
     * Retrieve the old JOnAS configuration
     * @param unpackedDeployable The unpacked deployable to undeploy
     */
    void undeploy(IAddonDeployable unpackedDeployable);

    /**
     * @param addonDeployer The {@link IAddonDeployer} to set
     */
    void setAddonDeployer(IAddonDeployer addonDeployer);
}
