/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.addon.deploy.api.deployer;

import java.util.List;

import org.ow2.jonas.addon.deploy.api.deployable.IAddonDeployable;
import org.ow2.jonas.multitenant.MultitenantService;
import org.ow2.util.ee.deploy.api.deployable.IDeployable;
import org.ow2.util.ee.deploy.api.deployer.DeployerException;
import org.ow2.util.ee.deploy.api.deployer.IDeployer;

/**
 * Represents an Addon Deployer
 * @author Jeremy Cazaux
 */
public interface IAddonDeployer extends IDeployer<IAddonDeployable> {
    /**
     * Install an addon into the deployment system
     * @param deployable The {@link IDeployable} to install
     * @param installFromACommand True if the addon will be installed from a command and not from the deployment system
     * @return the unpacked deployable
     * @throws DeployerException if the addon could not be installed
     */
    IDeployable<IAddonDeployable> install(IDeployable<IAddonDeployable> deployable, boolean installFromACommand) throws DeployerException;

    /**
     * @param addonName The name of the addon to start
     * @throws DeployerException if the addon could not be started
     */
    void start(String addonName) throws DeployerException;

    /**
     * @param addonName The name of the addon to stop
     * @throws DeployerException if the addon could not be stopped
     */
    void stop(String addonName) throws DeployerException;

    /**
     * @param addonName The name of the addon to uninstall
     * @throws DeployerException if the addon could not be uninstalled
     */
    void uninstall(String addonName) throws DeployerException;

    /**
     * Try to resolve all unresolved Addons
     */
    void resolve();

    /**
     * @return the list of {@link IAddonDeployable} installed in the addon system
     */
    List<IAddonDeployable> getAddons();

    /**
     * @param addonId The id of the addon to retrieve
     * @return the addon with the specified identifier.
     */
    IAddonDeployable getAddon(final long addonId);

    /**
     * @param addonName The name of the addon to retrieve
     * @return the addon with the specified name.
     */
    IAddonDeployable getAddon(final String addonName);

    /**
     * Test if the specified unpack name is already deployed or not.
     * @param unpackName the name of the Addon file.
     * @return true if the Addon is deployed, else false.
     */
    boolean isAddonDeployedByWorkName(final String unpackName);

    /**
     * Set Multitenant service impl
     * @param multitenantService
     */
    void setMultitenantService(MultitenantService multitenantService);

    /**
     * Unset Multitenant service impl
     */
    void unsetMultitenantService();
}
