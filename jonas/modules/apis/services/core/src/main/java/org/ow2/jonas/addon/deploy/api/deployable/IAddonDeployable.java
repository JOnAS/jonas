/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.addon.deploy.api.deployable;

import java.io.File;
import java.util.List;

import org.osgi.framework.Bundle;
import org.ow2.util.ee.deploy.api.deployable.IDeployable;

/**
 * Represents an Addon Deployable
 * @author Jeremy Cazaux
 */
public interface IAddonDeployable extends IDeployable<IAddonDeployable> {

    /**
     * Configuration directory of an Addon
     */
    String CONF_DIRECTORY = "conf";

    /**
     * Bin directory of an Addon
     */
    String BIN_DIRECTORY = "bin";

    /**
     * Deployable directory of an Addon
     */
    String DEPLOY_DIRECTORY = "deploy";

    /**
     * Repository directory of an Addon
     */
    String REPOSITORY_DIRECTORY = "repository";

    /**
     * Ant directory of an Addon
     */
    String ANT_DIRECTORY = "ant";

    /**
     * Addon metadata filename
     */
    String METADATA_FILENAME = "jonas-addon.xml";

    /**
     * META-INF directory name
     */
    String META_INF = "META-INF";

    /**
     * The metadata file of an Addon deployable
     */
    String JONAS_ADDON_METADATA = META_INF + File.separator + METADATA_FILENAME;

    /**
     * The addon metadata zip entry
     */
    String JONAS_ADDON_METADATA_ZIP_ENTRY = META_INF + "/" + METADATA_FILENAME;

    /**
     * The Addon is not installed
     */
    int UNINSTALLED = Bundle.UNINSTALLED;

    /**
     * The Addon is installed but not yet resolved.
     */
    int INSTALLED = Bundle.INSTALLED;

    /**
     * The Addon is resolved and is able to be started
     */
    int RESOLVED = Bundle.RESOLVED;

    /**
     * The Addon is starting
     */
    int STARTING = Bundle.STARTING;

    /**
     * The Addon is stopping
     */
    int STOPPING = Bundle.STOPPING;

    /**
     * The Addon is now running
     */
    int ACTIVE = Bundle.ACTIVE;

    /**
     * @return the name of the addon
     */
    String getName();

    /**
     * @return the state of the addon
     */
    int getState();

    /**
     * @param state Update the state of the addon with the given value
     */
    void updateState(int state);

    /**
     * @return the list of known deployables
     */
    List<ISortableDeployable> getKnownDeployables();

    /**
     * @param sortableDeployable Sortable deployable to add to sortable deployable list
     */
    void addDeployable(final ISortableDeployable sortableDeployable);

    /**
     * @param sortableDeployable The {@link ISortableDeployable} to remove the list
     */
    void removeDeployable(final ISortableDeployable sortableDeployable);

    /**
     * @return just the list of unknown deployables
     */
    List<ISortableDeployable> getUnknownDeployables();

    /**
     * @return the {@link IAddonMetadata} of the addon
     */
    IAddonMetadata getMetadata();

    /**
     * @param addonMetadata The {@link IAddonMetadata} of the addon to set
     */
    void setMetadata(IAddonMetadata addonMetadata);

    /**
     * @return the list of exported capabilities
     */
    List<String> getCapabilities();

    /**
     * @return the list of requirements
     */
    List<String> getRequirements();

    /**
     * @return this addon's unique identifier
     */
    Long getAddonId();

    /**
     * @return the parent addon
     */
    IAddonDeployable getParent();

    /**
     * @return the configuration directory
     */
    File getConfigurationDirectory();

    /**
     * @return the list of configuration files
     */
    List<String> getConfigurationFileNames();
}
