/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.management;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Management interface of the J2EEServer states.
 * @author Francois Fornaciari
 */
public interface J2EEServerService {

    /**
     * OSGi event error key
     */
    public static final String EVENT_ERROR_KEY = "error";

    /**
     * OSGi event deployable name key
     */
    public static final String EVENT_DEPLOYABLE_NAME_KEY = "name";

    /**
     * OSGi event deployable path key
     */
    public static final String EVENT_DEPLOYABLE_PATH_KEY = "path";

    /**
     * OSGi event deployable state key
     */
    public static final String EVENT_DEPLOYABLE_STATE_KEY = "state";

    /**
     * OSGi event deployable source key
     */
    public static final String EVENT_DEPLOYABLE_SOURCE_KEY = "source";

    /**
     * OSGi event deployable current time key
     */
    public static final String EVENT_DEPLOYABLE_CURRENT_TIME = "currentTime";

    /**
     * Is the server STARTING.
     */
    boolean isStarting();

    /**
     * Is the server RUNNING.
     */
    boolean isRunning();

    /**
     * Is the server FAILED.
     */
    boolean isFailed();

    /**
     * Is the server STOPPING.
     */
    boolean isStopping();

    /**
     * Is the server STOPPED.
     */
    boolean isStopped();

    /**
     * Set the server state to STARTING and perform a notification.
     */
    void setStarting();

    /**
     * Set the server state to RUNNING and perform a notification.
     */
    void setRunning();

    /**
     * Set the server state to FAILED and perform a notification.
     */
    void setFailed();

    /**
     * Set the server state to STOPPING and perform a notification.
     */
    void setStopping();

    /**
     * Set the server state to STOPPED and perform a notification.
     */
    void setStopped();

    /**
     * Get the directory in which J2EEServer uploads files
     * @return directory in which J2EEServer uploads files
     */
    String getUploadDirectory();

    /**
     * Start a service.
     * @param service the service name.
     * @throws Exception If the startup of the service fails
     */
    void startService(final String service) throws Exception;
    /**
     * Stop a service.
     * @param service the service name.
     * @throws Exception If the stop of the service fails
     */
    void stopService(final String service) throws Exception;

    /**
     * @return the {@link Map} of a pair of  <deployable short name, deployable state></deployable>.
     */
    Map<String, DeployableState> getDeployables();

    /**
     * Deploy the given file.
     * @param filename
     */
    void deploy(String filename);

    /**
     * Undeploy the given file.
     * @param filename
     */
    void undeploy(String filename);

}
