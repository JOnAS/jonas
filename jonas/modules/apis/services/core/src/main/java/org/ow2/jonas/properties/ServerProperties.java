/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id:ServerProperties.java 11801 2007-10-17 16:12:15Z fornacif $
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.properties;

import java.util.Properties;

/**
 * Interface used to retrieve the configuration of the Server.
 * @author durieuxp
 */
public interface ServerProperties {

    /**
     * Get value of a property.
     * @param key property name
     * @return value as a String
     */
    String getValue(String key);

    /**
     * Returns the value of the related property. With default values.
     * @param key the search key
     * @param defaultVal if the key is not found return this default value
     * @return property value
     */
    String getValue(String key, String defaultVal);

    /**
     * Returns the value of the related property as boolean.
     * @param key the wanted key
     * @param def default run if not found
     * @return property value, true or false.
     */
    boolean getValueAsBoolean(String key, boolean def);

    /**
     * Returns the value of the related property as String [].
     * The method returns null if the property is not found.
     * @param key the wanted key
     * @return property value, null if not exist
     */
    String[] getValueAsArray(String key);

    /**
     * Retrieve the Domain Name.
     * @return Domain Name
     */
    String getDomainName();

    /**
     * Retrieve the Server Name.
     * @return Server Name
     */
    String getServerName();

    /**
     * Retrieve the configuration file name.
     * @return configuration file name
     */
    String getPropFileName();

    /**
     * Returns JOnAS environment as configured with files properties only.
     * @return JOnAS properties
     */
    Properties getConfigFileEnv();

    /**
     * @return the VERSIONS file content as a String.
     */
    String getVersionsFile();

    /**
     * Needed to determine if the current server is a master.
     * @return true of this server is a master, false otherwise
     */
    boolean isMaster();

    /**
     * Needed to determine if the current server is in development mode.
     * @return true of this server is in development mode, false otherwise
     */
    boolean isDevelopment();

    /**
     * @return the work directory.
     */
    String getWorkDirectory();
}
