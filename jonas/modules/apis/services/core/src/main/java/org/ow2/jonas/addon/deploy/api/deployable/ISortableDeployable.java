/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.addon.deploy.api.deployable;

import org.ow2.util.ee.deploy.api.deployable.IDeployable;

/**
 * Class that stores a file and the associated deployable and that can be
 * sorted
 * @author Jeremy Cazaux
 */
public interface ISortableDeployable {

    /**
     * @return the priority of the deployable
     */
    Integer getPriority();

    /**
     * @return the deployable
     */
    IDeployable getDeployable();

    /**
     * @param deployable The deployable to set
     */
    void setDeployable(final IDeployable deployable);

    /**
     * @param priority The priority of the deployable
     */
    void setPriority(final Integer priority);
}
