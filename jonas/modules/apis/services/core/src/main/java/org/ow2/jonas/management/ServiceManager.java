/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.management;

import java.util.List;

import org.ow2.util.ee.deploy.api.deployable.IDeployable;

/**
 * Interface for managing the JOnAS services.
 * @author Francois Fornaciari
 */
public interface ServiceManager {

    /**
     * Start a given JOnAS service.
     * @param serviceName the service name
     * @param deployOSGiResources true if OSGi resources of the service need to be deployed
     * @throws Exception If the startup of the service fails
     */
    void startService(String serviceName, boolean deployOSGiResources) throws Exception;

    /**
     * Start all required services for a given deployable.
     * @param deployable The deployable to analyse
     */
    void startRequiredServices(final IDeployable<?> deployable);

    /**
     * Stop a given JOnAS service.
     * @param serviceName the service name
     * @throws Exception If the stop of the service fails
     */
    void stopService(String serviceName) throws Exception;

    /**
     * Start optional JOnAS services defined in the server configuration.
     * Some JOnAS services may requires other services which will also be started.
     */
    void startServices();

    /**
     * Stop all optional started JOnAS services.
     */
    void stopServices();

    /**
     * Return the list of all JOnAS services.
     * @return The list of all JOnAS services
     */
    List<String> getAllServices();

    /**
     * Return the list of optional JOnAS services.
     * @return The list of optional JOnAS services
     */
    List<String> getOptionalServices();

    /**
     * Return the description of a given JOnAS service.
     * @param serviceName the service name
     * @return The description of a given JOnAS service
     */
    String getServiceDescription(final String serviceName);

    /**
     * Return the state of a given JOnAS service.
     * @param serviceName the service name
     * @return The state of a given JOnAS service
     */
    String getServiceState(final String serviceName);
    
    /**
     * Disable the service states check. Called when the server reaches the RUNNING state.
     */
    void disableServiceStatesCheck();
}
