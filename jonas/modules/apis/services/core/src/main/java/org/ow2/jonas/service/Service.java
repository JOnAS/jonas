/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999 Bull S.A.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): ____________________________________.
 * Contributor(s): ______________________________________.
 *
 * 01/06/15 Regis Le Brettevillois - Libelis / JOnAS team - Evidian
 *          Creation.
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.service;

import javax.naming.Context;

/**
 * This interface defines a Service.
 * Objects which implement this interface must have a public constructor
 * with a string parameter which is the name of the org.ow2.jonas.service.
 */
public interface Service {

    /**
     * Initialize the org.ow2.jonas.service.
     * @param ctx configuration of the org.ow2.jonas.service
     * @throws ServiceException when init fails.
     * @deprecated Replaced in favor of IoC.
     */
    @Deprecated
    void init(Context ctx) throws ServiceException;

    /**
     * Start the org.ow2.jonas.service.
     * @throws ServiceException when start fails.
     */
    void start() throws ServiceException;

    /**
     * Stop the org.ow2.jonas.service.
     * @throws ServiceException when stop fails.
     */
    void stop() throws ServiceException;

    /**
     * @return Returns true if the org.ow2.jonas.service is started, false otherwise
     */
    boolean isStarted();

    /**
     * Set the org.ow2.jonas.service's name.
     * @param name the org.ow2.jonas.service's name
     */
    void setName(String name);

    /**
     * @return Returns the org.ow2.jonas.service's name
     */
    String getName();
}