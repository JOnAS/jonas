/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.workcleaner;

import java.io.File;
import java.util.Vector;

/**
 * Interface which permits to store or load the association between the name of a package and the timestamped work
 * copy associated.
 * @author Jeremy Cazaux
 */
public interface IDeployerLog<T extends LogEntry> {

    /**
     * Return the entries of the file.
     * @return a vector of LogEntry item.
     */
    Vector<T> getEntries();

    /**
     * Remove the given entry and return the entries of the file.
     * @param entry the LogEntry which must be remove.
     * @return the new vector of LogEntry item.
     * @throws org.ow2.jonas.workcleaner.DeployerLogException if the remove can't be done
     */
    Vector<T> removeEntry(final T entry) throws DeployerLogException;

    /**
     * Add the entry and return the new entries.
     * @param logEntry the entry to add
     * @throws org.ow2.jonas.workcleaner.DeployerLogException if the add can't be done
     */
    Vector<T> addEntry(T logEntry) throws DeployerLogException;

    /**
     * @param original Original File
     * @return the entry which match with the orginal file
     */
    T getEntry(File original);

}
