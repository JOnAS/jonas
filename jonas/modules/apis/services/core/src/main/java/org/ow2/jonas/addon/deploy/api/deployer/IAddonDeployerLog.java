/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2011 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.jonas.addon.deploy.api.deployer;

import java.io.File;
import java.util.Vector;

import org.ow2.jonas.addon.deploy.api.util.IAddonLogEntry;
import org.ow2.jonas.workcleaner.DeployerLogException;
import org.ow2.jonas.workcleaner.IDeployerLog;
import org.ow2.jonas.workcleaner.LogEntry;

/**
 * Interface for the addon deployer log
 * @author Jeremy Cazaux
 */
public interface IAddonDeployerLog<T extends LogEntry> extends IDeployerLog<T> {

    /**
     * Add the entry and return the new entries.
     * @param name the name of the addon
     * @param state the state of the addon
     * @param isInstalledFromACommand True if the addon has been installed from a shelbie command.Otherwise, the addon
     *                                has been installed from the deployment system
     * @param original the name of the file
     * @param copy the copy of the file
     * @return the new vector of IAddonLogEntry item.
     * @throws DeployerLogException if the add can't be done
     */
    Vector<IAddonLogEntry> addEntry(final String name, int state, boolean isInstalledFromACommand, final File original, final File copy) throws DeployerLogException;

    /**
     * Update the state field of an entry
     * @param name the name of the addon
     * @param state the state value to update
     */
    void updateEntry(final String name, final int state) throws DeployerLogException;
}
