/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2006-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 2.1 of the License, or any later
 * version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.discovery;

/**
 * This is a helper class that define the possible state attribute values in a
 * DiscoveryEvent class instance.
 *
 * @author Adriana Danes
 */
public class DiscoveryState {

    /**
     * Used when the DiscovetyEvent is emitted to notify a server start-up.
     */
    public static final String RUNNING = "running";

    /**
     * Used when the DiscovetyEvent is emitted to notify a server shut-down.
     */
    public static final String STOPPING = "stopping";

    /**
     * Used when the DiscovetyEvent is emitted to notify a server start-up
     */
    public static final String STARTUP = "starting up";

    /**
     * Another JOnAS instance started with a name already bound in the domain.
     */
    public static final String DUPLICATE_NAME = "duplicate server name found";
}
