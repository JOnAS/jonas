/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 1999-2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.jmx;

import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXServiceURL;

import org.ow2.jonas.service.Service;
import org.ow2.jonas.service.ServiceException;


/**
 * JMX Service interface.
 *
 */
public interface JmxService extends Service {

    /**
     * @return The reference of the MBean server
     */
    MBeanServer getJmxServer();

    /**
     * @return The Connection interface to the MBean server
     */
    MBeanServerConnection getJmxServerConnection();

    /**
     * Register an MBean on the JOnAS MBeanServer.
     * @param mbean MBean to be registered
     * @param objectName Stringified ObjectName of the MBean to be registered
     */
    void registerMBean(Object mbean, String objectName);

    /**
     * Register an MBean on the JOnAS MBeanServer.
     * @param mbean MBean to be registered
     * @param objectName ObjectName of the MBean to be registered
     */
    void registerMBean(Object mbean, ObjectName objectName);

    /**
     * Unregister an MBean from the JOnAS MBeanServer.
     * @param objectName the MBean's ObjectName
     */
    void unregisterMBean(ObjectName objectName);

    /**
     * Register a Model MBean on the JOnAS MBeanServer.
     * @param mbean MBean to be registered
     * @param objectName Stringified ObjectName of the MBean to be registered
     * @exception Exception throwed when registering a modeler MBean
     */
    void registerModelMBean(Object mbean, String objectName) throws Exception;

    /**
     * Register a Model MBean on the JOnAS MBeanServer.
     * @param mbean MBean to be registered
     * @param objectName ObjectName of the MBean to be registered
     * @exception Exception throwed when registering a modeler MBean
     */
    void registerModelMBean(Object mbean, ObjectName objectName) throws Exception;

    /**
     * Unegister a Model MBean from the JOnAS MBeanServer.
     * @param objectName the Mbean ObjectName
     */
    void unregisterModelMBean(ObjectName objectName);

    /**
     * Load additional mbean descriptors.
     * @param packageName name of the package containing the descriptors file
     * @param cl class loader containing the resource
     */
    void loadDescriptors(String packageName, ClassLoader cl);

    /**
     * @return Returns a {@link javax.management.remote.JMXServiceURL} array containing the adresses
     *         associated to the JMX connector servers
     */
    JMXServiceURL[] getConnectorServerURLs();

    /**
     * @return The current server name
     */
    String getJonasServerName();

    /**
     * @return The current management domain name
     */
    String getDomainName();


    /**
     * Register the instance as a ModelMBean using the delegate.
     * @param <T> instance Type
     * @param instance Object instance to be managed
     * @return the MBean's OBJECT_NAME
     * @throws Exception if registration fails.
     */
    <T> String registerMBean(final T instance) throws Exception;

    /**
     * Unregister the given Object.
     * @param <T> instance Type
     * @param instance Instance to be deregistered.
     * @throws Exception if unregistration fails.
     */
    <T> void unregisterMBean(final T instance) throws Exception;

    /**
     * @param <T> instance Type
     * @param instance Object instance to be managed
     * @return Returns the instance ObjectName.
     * @throws ServiceException if registration fails.
     */
    <T> String getObjectName(final T instance) throws ServiceException;

    /**
     * Add an interceptor to invocationHandler
     * @param interceptor
     */
    public void addInterceptor(Interceptor interceptor);

    /**
     * Remove an interceptor from invocation handler
     * @param interceptor
     */
    public void removeInterceptor(Interceptor interceptor);

}
