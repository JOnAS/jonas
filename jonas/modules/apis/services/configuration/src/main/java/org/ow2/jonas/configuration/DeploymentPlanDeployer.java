/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2008
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.configuration;

import org.ow2.util.ee.deploy.api.deployer.DeployerException;

/**
 * Allows to deploy or undeploy all bundles for a particular deployment plan.
 * @author Francois Fornaciari
 */
public interface DeploymentPlanDeployer {

    /**
     * Try to deploy all bundles for a given deployment plan.
     * @param name The deployment plan abstract name
     * @throws DeployerException If the deployment of the deployment plan fails
     */
    void deploy(final String name) throws DeployerException;

    /**
     * Try to undeploy all bundles for a given deployment plan.
     * @param name The deployment plan abstract name
     * @throws DeployerException If the undeployment of the deployment plan fails
     */
    void undeploy(final String name)throws DeployerException;
}
