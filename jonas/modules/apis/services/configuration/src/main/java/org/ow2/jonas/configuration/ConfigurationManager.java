/**
 * JOnAS: Java(TM) Open Application Server
 * Copyright (C) 2007 Bull S.A.S.
 * Contact: jonas-team@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.jonas.configuration;

import org.ow2.jonas.properties.ServerProperties;

import java.util.Dictionary;
import java.util.List;

/**
 * This interface provides methods to manage the JOnAS service configurations.
 * @author Francois Fornaciari
 */
public interface ConfigurationManager {
    /**
     * Halt server by stopping the system bundle.
     * @throws Exception If an exception occurs during server halting
     */
    void haltServer() throws Exception;

    /**
     * Create or update the service configuration for the given service.
     * @param service the service name
     * @throws Exception If an exception occurs during configuration updates
     */
    void updateServiceConfiguration(final String service) throws Exception;

    /**
     * Delete the service configuration for the given service.
     * @param service the service name
     * @throws Exception If an exception occurs during configuration deletions
     */
    void deleteServiceConfiguration(final String service) throws Exception;

    /**
     * Return the list of JOnAS services.
     * @return All JOnAS services
     */
    List<String> getAllServices();

    /**
     * Return the list of JOnAS mandatory services.
     * @return All JOnAS mandatory services
     */
    List<String> getMandatoryServices();

    /**
     * Return the list of JOnAS optional services.
     * @return All JOnAS optional services
     */
    List<String> getOptionalServices();

    /**
     * Return true if the given service matches a JOnAS service
     * @param service The service name
     * @return True if the given service matches a JOnAS service
     */
    boolean matchService(final String service);

    /**
     * Get all properties for a given JOnAS service name.
     * @param service The given service name
     * @return A <code>Dictionary</code> value representing all properties for a JOnAS service name
     */
    Dictionary<String, Object> getServiceProperties(final String service);

}
